﻿using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;
using System.Web;
using ViewModel.ViewModel.Exam;

namespace DAL.DAL.ExaminationModule
{
    public partial class ExamService : IExamService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        //private readonly string SqlDataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public ExamService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods

        #region ExamTerm

        public KSModel.Models.ExamTerm GetExamTermById(int ExamTermId, bool IsTrack = false)
        {
            if (ExamTermId == 0)
                throw new ArgumentNullException("ExamTerm");

            var ExamTerm = new ExamTerm();
            if (IsTrack)
                ExamTerm = (from s in db.ExamTerms where s.ExamTermId == ExamTermId select s).FirstOrDefault();
            else
                ExamTerm = (from s in db.ExamTerms.AsNoTracking() where s.ExamTermId == ExamTermId select s).FirstOrDefault();

            return ExamTerm;
        }

        public List<KSModel.Models.ExamTerm> GetAllExamTerms(ref int totalcount, string ExamTerm = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamTerms.ToList();
            if (!String.IsNullOrEmpty(ExamTerm))
                query = query.Where(e => e.ExamTerm1.ToLower().Contains(ExamTerm.ToLower())).ToList();

            totalcount = query.Count;
            var ExamTerms = new PagedList<KSModel.Models.ExamTerm>(query, PageIndex, PageSize);
            return ExamTerms;
        }

        public void InsertExamTerm(KSModel.Models.ExamTerm ExamTerm)
        {
            if (ExamTerm == null)
                throw new ArgumentNullException("ExamTerm");

            db.ExamTerms.Add(ExamTerm);
            db.SaveChanges();
        }

        public void UpdateExamTerm(KSModel.Models.ExamTerm ExamTerm)
        {
            if (ExamTerm == null)
                throw new ArgumentNullException("ExamTerm");

            db.Entry(ExamTerm).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamTerm(int ExamTermId = 0)
        {
            if (ExamTermId == 0)
                throw new ArgumentNullException("ExamTerm");

            var ExamTerm = (from s in db.ExamTerms where s.ExamTermId == ExamTermId select s).FirstOrDefault();
            db.ExamTerms.Remove(ExamTerm);
            db.SaveChanges();
        }

        #endregion

        #region ExamActivity

        public KSModel.Models.ExamActivity GetExamActivityById(int ExamActivityId, bool IsTrack = false)
        {
            if (ExamActivityId == 0)
                throw new ArgumentNullException("ExamActivity");

            var ExamActivity = new ExamActivity();
            if (IsTrack)
                ExamActivity = (from s in db.ExamActivities where s.ExamActivityId == ExamActivityId select s).FirstOrDefault();
            else
                ExamActivity = (from s in db.ExamActivities.AsNoTracking() where s.ExamActivityId == ExamActivityId select s).FirstOrDefault();

            return ExamActivity;
        }

        public List<KSModel.Models.ExamActivity> GetAllExamActivities(ref int totalcount, string ExamActivity = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamActivities.ToList();
            if (!String.IsNullOrEmpty(ExamActivity))
                query = query.Where(e => e.ExamActivity1.ToLower().Contains(ExamActivity.ToLower())).ToList();

            totalcount = query.Count;
            var ExamActivities = new PagedList<KSModel.Models.ExamActivity>(query, PageIndex, PageSize);
            return ExamActivities;
        }

        public void InsertExamActivity(KSModel.Models.ExamActivity ExamActivity)
        {
            if (ExamActivity == null)
                throw new ArgumentNullException("ExamActivity");

            db.ExamActivities.Add(ExamActivity);
            db.SaveChanges();
        }

        public void UpdateExamActivity(KSModel.Models.ExamActivity ExamActivity)
        {
            if (ExamActivity == null)
                throw new ArgumentNullException("ExamActivity");

            db.Entry(ExamActivity).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamActivity(int ExamActivityId = 0)
        {
            if (ExamActivityId == 0)
                throw new ArgumentNullException("ExamActivity");

            var ExamActivity = (from s in db.ExamActivities where s.ExamActivityId == ExamActivityId select s).FirstOrDefault();
            db.ExamActivities.Remove(ExamActivity);
            db.SaveChanges();
        }

        #endregion

        #region ExamTermActivity

        public ExamTermActivity GetExamTermActivityById(int ExamTermActivityId, bool IsTrack = false)
        {
            if (ExamTermActivityId == 0)
                throw new ArgumentNullException("ExamTermActivity");

            var ExamTermActivity = new ExamTermActivity();
            if (IsTrack)
                ExamTermActivity = (from s in db.ExamTermActivities where s.ExamTermActivityId == ExamTermActivityId select s).FirstOrDefault();
            else
                ExamTermActivity = (from s in db.ExamTermActivities.AsNoTracking() where s.ExamTermActivityId == ExamTermActivityId select s).FirstOrDefault();

            return ExamTermActivity;
        }

        public List<ExamTermActivity> GetAllExamTermActivities(ref int totalcount, int? ExamTermId = null, int? ExamActivityId = null, 
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamTermActivities.ToList();
            if (ExamTermId > 0)
                query = query.Where(e => e.ExamTermId == ExamTermId).ToList();
            if (ExamActivityId > 0)
                query = query.Where(e => e.ExamActivityId == ExamActivityId).ToList();

            totalcount = query.Count;
            var ExamTermActivities = new PagedList<KSModel.Models.ExamTermActivity>(query, PageIndex, PageSize);
            return ExamTermActivities;
        }

        public void InsertExamTermActivity(ExamTermActivity ExamTermActivity)
        {
            if (ExamTermActivity == null)
                throw new ArgumentNullException("ExamTermActivity");

            db.ExamTermActivities.Add(ExamTermActivity);
            db.SaveChanges();
        }

        public void UpdateExamTermActivity(ExamTermActivity ExamTermActivity)
        {
            if (ExamTermActivity == null)
                throw new ArgumentNullException("ExamTermActivity");

            db.Entry(ExamTermActivity).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamTermActivity(int ExamTermActivityId = 0)
        {
            if (ExamTermActivityId == 0)
                throw new ArgumentNullException("ExamActivity");

            var ExamTermActivity = (from s in db.ExamTermActivities where s.ExamTermActivityId == ExamTermActivityId select s).FirstOrDefault();
            db.ExamTermActivities.Remove(ExamTermActivity);
            db.SaveChanges();
        }

        #endregion

        #region ExamTermStandard

        public ExamTermStandard GetExamTermStandardById(int ExamTermStandardId, bool IsTrack = false)
        {
            if (ExamTermStandardId == 0)
                throw new ArgumentNullException("ExamTermStandard");

            var ExamTermStandard = new ExamTermStandard();
            if (IsTrack)
                ExamTermStandard = (from s in db.ExamTermStandards where s.ExamTermStandardId == ExamTermStandardId select s).FirstOrDefault();
            else
                ExamTermStandard = (from s in db.ExamTermStandards.AsNoTracking() where s.ExamTermStandardId == ExamTermStandardId select s).FirstOrDefault();

            return ExamTermStandard;
        }

        public List<ExamTermStandard> GetAllExamTermStandards(ref int totalcount, int? ExamTermId = null, int? ExamStandardId = null, 
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamTermStandards.ToList();
            if (ExamTermId > 0)
                query = query.Where(e => e.ExamTermId == ExamTermId).ToList();

            if (ExamStandardId > 0)
                query = query.Where(e => e.ExamStandardId == ExamStandardId).ToList();

            totalcount = query.Count;
            var ExamTermStandards = new PagedList<KSModel.Models.ExamTermStandard>(query, PageIndex, PageSize);
            return ExamTermStandards;
        }

        public void InsertExamTermStandard(ExamTermStandard ExamTermStandard)
        {
            if (ExamTermStandard == null)
                throw new ArgumentNullException("ExamTermStandard");

            db.ExamTermStandards.Add(ExamTermStandard);
            db.SaveChanges();
        }

        public void UpdateExamTermStandard(ExamTermStandard ExamTermStandard)
        {
            if (ExamTermStandard == null)
                throw new ArgumentNullException("ExamTermStandard");

            db.Entry(ExamTermStandard).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamTermStandard(int ExamTermStandardId = 0)
        {
            if (ExamTermStandardId == 0)
                throw new ArgumentNullException("ExamTermStandard");

            var ExamTermStandard = (from s in db.ExamTermStandards where s.ExamTermStandardId == ExamTermStandardId select s).FirstOrDefault();
            db.ExamTermStandards.Remove(ExamTermStandard);
            db.SaveChanges();
        }

        #endregion

        #region ExamTimeSlot

        public KSModel.Models.ExamTimeSlot GetExamTimeSlotById(int ExamTimeSlotId, bool IsTrack = false)
        {
            if (ExamTimeSlotId == 0)
                throw new ArgumentNullException("ExamTimeSlot");

            var ExamTimeSlot = new ExamTimeSlot();
            if (IsTrack)
                ExamTimeSlot = (from s in db.ExamTimeSlots where s.ExamTimeSlotId == ExamTimeSlotId select s).FirstOrDefault();
            else
                ExamTimeSlot = (from s in db.ExamTimeSlots.AsNoTracking() where s.ExamTimeSlotId == ExamTimeSlotId select s).FirstOrDefault();

            return ExamTimeSlot;
        }

        public List<KSModel.Models.ExamTimeSlot> GetAllExamTimeSlots(ref int totalcount, string ExamTimeSlot = "", DateTime? TimeFrom = null,
            DateTime? TimeTo = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamTimeSlots.ToList();
            if (!String.IsNullOrEmpty(ExamTimeSlot))
                query = query.Where(e => e.ExamTimeSlot1.ToLower().Contains(ExamTimeSlot.ToLower())).ToList();
            if (TimeFrom.HasValue)
                query = query.Where(e => e.TimeFrom == TimeFrom.Value.TimeOfDay).ToList();
            if (TimeTo.HasValue)
                query = query.Where(e => e.TimeTo == TimeTo.Value.TimeOfDay).ToList();

            totalcount = query.Count;
            var ExamTimeSlots = new PagedList<KSModel.Models.ExamTimeSlot>(query, PageIndex, PageSize);
            return ExamTimeSlots;
        }

        public void InsertExamTimeSlot(KSModel.Models.ExamTimeSlot ExamTimeSlot)
        {
            if (ExamTimeSlot == null)
                throw new ArgumentNullException("ExamTimeSlot");

            db.ExamTimeSlots.Add(ExamTimeSlot);
            db.SaveChanges();
        }

        public void UpdateExamTimeSlot(KSModel.Models.ExamTimeSlot ExamTimeSlot)
        {
            if (ExamTimeSlot == null)
                throw new ArgumentNullException("ExamTimeSlot");

            db.Entry(ExamTimeSlot).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamTimeSlot(int ExamTimeSlotId = 0)
        {
            if (ExamTimeSlotId == 0)
                throw new ArgumentNullException("ExamTimeSlot");

            var ExamTimeSlot = (from s in db.ExamTimeSlots where s.ExamTimeSlotId == ExamTimeSlotId select s).FirstOrDefault();
            db.ExamTimeSlots.Remove(ExamTimeSlot);
            db.SaveChanges();
        }

        #endregion

        #region ExamDateSheet

        public KSModel.Models.ExamDateSheet GetExamDateSheetById(int ExamDateSheetId, bool IsTrack = false)
        {
            if (ExamDateSheetId == 0)
                throw new ArgumentNullException("ExamDateSheet");

            var ExamDateSheet = new ExamDateSheet();
            if (IsTrack)
                ExamDateSheet = (from s in db.ExamDateSheets where s.ExamDateSheetId == ExamDateSheetId select s).FirstOrDefault();
            else
                ExamDateSheet = (from s in db.ExamDateSheets.AsNoTracking() where s.ExamDateSheetId == ExamDateSheetId select s).FirstOrDefault();

            return ExamDateSheet;
        }

        public List<KSModel.Models.ExamDateSheet> GetAllExamDateSheets(ref int totalcount, string ExamDateSheet = "", int? ExamTermActivityId = null,
            DateTime? FromDate = null, DateTime? TillDate = null, bool? IsPublish = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamDateSheets.ToList();
            if (!String.IsNullOrEmpty(ExamDateSheet))
                query = query.Where(e => e.ExamDateSheet1.ToLower().Contains(ExamDateSheet.ToLower())).ToList();
            if (ExamTermActivityId != null && ExamTermActivityId > 0)
                query = query.Where(e => e.ExamTermActivityId == ExamTermActivityId).ToList();
            if (FromDate.HasValue)
                query = query.Where(e => e.FromDate >= FromDate).ToList();
            if (TillDate.HasValue)
                query = query.Where(e => e.TillDate <= TillDate).ToList();

            if (IsPublish != null)
            {
                if ((bool)IsPublish)
                    query = query.Where(e => e.Ispublish == true).ToList();
                else
                    query = query.Where(e => e.Ispublish == false).ToList();
            }
            else
                query = query.Where(e => e.Ispublish == true || e.Ispublish == false || e.Ispublish == null).ToList();

            totalcount = query.Count;
            var ExamDateSheets = new PagedList<KSModel.Models.ExamDateSheet>(query, PageIndex, PageSize);
            return ExamDateSheets;
        }

        public void InsertExamDateSheet(KSModel.Models.ExamDateSheet ExamDateSheet)
        {
            if (ExamDateSheet == null)
                throw new ArgumentNullException("ExamDateSheet");

            db.ExamDateSheets.Add(ExamDateSheet);
            db.SaveChanges();
        }

        public void UpdateExamDateSheet(KSModel.Models.ExamDateSheet ExamDateSheet)
        {
            if (ExamDateSheet == null)
                throw new ArgumentNullException("ExamDateSheet");

            db.Entry(ExamDateSheet).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamDateSheet(int ExamDateSheetId = 0)
        {
            if (ExamDateSheetId == 0)
                throw new ArgumentNullException("ExamDateSheet");

            var ExamDateSheet = (from s in db.ExamDateSheets where s.ExamDateSheetId == ExamDateSheetId select s).FirstOrDefault();
            db.ExamDateSheets.Remove(ExamDateSheet);
            db.SaveChanges();
        }

        #endregion

        #region ExamPublishDetail

        public ExamPublishDetail GetExamPublishDetailById(int ExamPublishId, bool IsTrack = false)
        {
            if (ExamPublishId == 0)
                throw new ArgumentNullException("ExamPublishDetail");

            var ExamPublishDetail = new ExamPublishDetail();
            if (IsTrack)
                ExamPublishDetail = (from s in db.ExamPublishDetails where s.ExamPublishId == ExamPublishId select s).FirstOrDefault();
            else
                ExamPublishDetail = (from s in db.ExamPublishDetails.AsNoTracking() where s.ExamPublishId == ExamPublishId select s).FirstOrDefault();

            return ExamPublishDetail;
        }

        public List<ExamPublishDetail> GetAllExamPublishDetails(ref int totalcount, int? ExamDateSheetId = null, DateTime? PublishDate = null,
            DateTime? UnPublishDate = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamPublishDetails.ToList();
            if (ExamDateSheetId != null && ExamDateSheetId > 0)
                query = query.Where(e => e.ExamDateSheetId == ExamDateSheetId).ToList();
            if (PublishDate.HasValue)
                query = query.Where(e => e.ExamPublishDate == PublishDate).ToList();
            if (UnPublishDate.HasValue)
                query = query.Where(e => e.ExamUnpublishDate == UnPublishDate).ToList();

            totalcount = query.Count;
            var ExamPublishDetails = new PagedList<KSModel.Models.ExamPublishDetail>(query, PageIndex, PageSize);
            return ExamPublishDetails;
        }

        public void InsertExamPublishDetail(ExamPublishDetail ExamPublishDetail)
        {
            if (ExamPublishDetail == null)
                throw new ArgumentNullException("ExamPublishDetail");

            db.ExamPublishDetails.Add(ExamPublishDetail);
            db.SaveChanges();
        }

        public void UpdateExamPublishDetail(ExamPublishDetail ExamPublishDetail)
        {
            if (ExamPublishDetail == null)
                throw new ArgumentNullException("ExamPublishDetail");

            db.Entry(ExamPublishDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamPublishDetail(int ExamPublishId = 0)
        {
            if (ExamPublishId == 0)
                throw new ArgumentNullException("ExamPublishDetail");

            var ExamPublishDetail = (from s in db.ExamPublishDetails where s.ExamPublishId == ExamPublishId select s).FirstOrDefault();
            db.ExamPublishDetails.Remove(ExamPublishDetail);
            db.SaveChanges();
        }

        #endregion

        #region ExamDateSheetDetail

        public KSModel.Models.ExamDateSheetDetail GetExamDateSheetDetailById(int ExamDateSheetDetailId, bool IsTrack = false)
        {
            if (ExamDateSheetDetailId == 0)
                throw new ArgumentNullException("ExamDateSheetDetail");

            var ExamDateSheetDetail = new ExamDateSheetDetail();
            if (IsTrack)
                ExamDateSheetDetail = (from s in db.ExamDateSheetDetails where s.ExamDateSheetDetailId == ExamDateSheetDetailId select s).FirstOrDefault();
            else
                ExamDateSheetDetail = (from s in db.ExamDateSheetDetails.AsNoTracking() where s.ExamDateSheetDetailId == ExamDateSheetDetailId select s).FirstOrDefault();

            return ExamDateSheetDetail;
        }

        public List<KSModel.Models.ExamDateSheetDetail> GetAllExamDateSheetDetails(ref int totalcount, int? ExamDateSheetId = null,
            int? StandardId = null, int? SubjectId = null, int? ExamTimeSlotId = null, int? MarksPatternId = null, DateTime? ExamDate = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamDateSheetDetails.ToList();
            if (ExamDateSheetId > 0)
                query = query.Where(e => e.ExamDateSheetId == ExamDateSheetId).ToList();
            if (StandardId > 0)
                query = query.Where(e => e.StandardId == StandardId).ToList();
            if (SubjectId > 0)
                query = query.Where(e => e.SubjectId == SubjectId).ToList();
            if (ExamTimeSlotId > 0)
                query = query.Where(e => e.ExamTimeSlotId == ExamTimeSlotId).ToList();
            if (MarksPatternId > 0)
                query = query.Where(e => e.MarksPatternId == MarksPatternId).ToList();
            if (ExamDate.HasValue)
                query = query.Where(e => e.ExamDate == ExamDate).ToList();

            totalcount = query.Count;
            var ExamDateSheetDetails = new PagedList<KSModel.Models.ExamDateSheetDetail>(query, PageIndex, PageSize);
            return ExamDateSheetDetails;
        }

        public void InsertExamDateSheetDetail(KSModel.Models.ExamDateSheetDetail ExamDateSheetDetail)
        {
            if (ExamDateSheetDetail == null)
                throw new ArgumentNullException("ExamDateSheetDetail");

            db.ExamDateSheetDetails.Add(ExamDateSheetDetail);
            db.SaveChanges();
        }

        public void UpdateExamDateSheetDetail(KSModel.Models.ExamDateSheetDetail ExamDateSheetDetail)
        {
            if (ExamDateSheetDetail == null)
                throw new ArgumentNullException("ExamDateSheetDetail");

            db.Entry(ExamDateSheetDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamDateSheetDetail(int ExamDateSheetDetailId = 0)
        {
            if (ExamDateSheetDetailId == 0)
                throw new ArgumentNullException("ExamDateSheetDetail");

            var ExamDateSheetDetail = (from s in db.ExamDateSheetDetails where s.ExamDateSheetDetailId == ExamDateSheetDetailId select s).FirstOrDefault();
            db.ExamDateSheetDetails.Remove(ExamDateSheetDetail);
            db.SaveChanges();
        }

        #endregion

        #region ExamClassDateSheetDetail

        public KSModel.Models.ExamClassDateSheetDetail GetExamClassDateSheetDetailById(int ExamClassDateSheetDetailId, bool IsTrack = false)
        {
            if (ExamClassDateSheetDetailId == 0)
                throw new ArgumentNullException("ExamClassDateSheetDetail");

            var ExamClassDateSheetDetail = new ExamClassDateSheetDetail();
            if (IsTrack)
                ExamClassDateSheetDetail = (from s in db.ExamClassDateSheetDetails where s.ExamClassDateSheetDetailId == ExamClassDateSheetDetailId select s).FirstOrDefault();
            else
                ExamClassDateSheetDetail = (from s in db.ExamClassDateSheetDetails.AsNoTracking() where s.ExamClassDateSheetDetailId == ExamClassDateSheetDetailId select s).FirstOrDefault();

            return ExamClassDateSheetDetail;
        }

        public List<KSModel.Models.ExamClassDateSheetDetail> GetAllExamClassDateSheetDetails(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamClassDateSheetDetails.ToList();
           
            totalcount = query.Count;
            var ExamClassDateSheetDetails = new PagedList<KSModel.Models.ExamClassDateSheetDetail>(query, PageIndex, PageSize);
            return ExamClassDateSheetDetails;
        }

        public void InsertExamClassDateSheetDetail(KSModel.Models.ExamClassDateSheetDetail ExamClassDateSheetDetail)
        {
            if (ExamClassDateSheetDetail == null)
                throw new ArgumentNullException("ExamClassDateSheetDetail");

            db.ExamClassDateSheetDetails.Add(ExamClassDateSheetDetail);
            db.SaveChanges();
        }

        public void UpdateExamClassDateSheetDetail(KSModel.Models.ExamClassDateSheetDetail ExamClassDateSheetDetail)
        {
            if (ExamClassDateSheetDetail == null)
                throw new ArgumentNullException("ExamClassDateSheetDetail");

            db.Entry(ExamClassDateSheetDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamClassDateSheetDetail(int ExamClassDateSheetDetailId = 0)
        {
            if (ExamClassDateSheetDetailId == 0)
                throw new ArgumentNullException("ExamClassDateSheetDetail");

            var ExamClassDateSheetDetail = (from s in db.ExamClassDateSheetDetails where s.ExamClassDateSheetDetailId == ExamClassDateSheetDetailId select s).FirstOrDefault();
            db.ExamClassDateSheetDetails.Remove(ExamClassDateSheetDetail);
            db.SaveChanges();
        }

        #endregion

        #region ExamResultDate

        public KSModel.Models.ExamResultDate GetExamResultDateById(int ExamResultDateId, bool IsTrack = false)
        {
            if (ExamResultDateId == 0)
                throw new ArgumentNullException("ExamResultDate");

            var ExamResultDate = new ExamResultDate();
            if (IsTrack)
                ExamResultDate = (from s in db.ExamResultDates where s.ExamResultDateId == ExamResultDateId select s).FirstOrDefault();
            else
                ExamResultDate = (from s in db.ExamResultDates.AsNoTracking() where s.ExamResultDateId == ExamResultDateId select s).FirstOrDefault();

            return ExamResultDate;
        }

        public List<KSModel.Models.ExamResultDate> GetAllExamResultDates(ref int totalcount, int? ExamDateSheetId = null, int? StandardId = null,
            DateTime? ResultDate = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamResultDates.ToList();
            if (ExamDateSheetId != null && ExamDateSheetId > 0)
                query = query.Where(e => e.ExamDateSheetId == ExamDateSheetId).ToList();
            if (StandardId > 0)
                query = query.Where(e => e.StandardId == StandardId).ToList();
            if (ResultDate.HasValue)
                query = query.Where(e => e.ExamResultDate1 == ResultDate).ToList();

            totalcount = query.Count;
            var ExamResultDates = new PagedList<KSModel.Models.ExamResultDate>(query, PageIndex, PageSize);
            return ExamResultDates;
        }

        public void InsertExamResultDate(KSModel.Models.ExamResultDate ExamResultDate)
        {
            if (ExamResultDate == null)
                throw new ArgumentNullException("ExamResultDate");

            db.ExamResultDates.Add(ExamResultDate);
            db.SaveChanges();
        }

        public void UpdateExamResultDate(KSModel.Models.ExamResultDate ExamResultDate)
        {
            if (ExamResultDate == null)
                throw new ArgumentNullException("ExamResultDate");

            db.Entry(ExamResultDate).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamResultDate(int ExamResultDateId = 0)
        {
            if (ExamResultDateId == 0)
                throw new ArgumentNullException("ExamResultDate");

            var ExamResultDate = (from s in db.ExamResultDates where s.ExamResultDateId == ExamResultDateId select s).FirstOrDefault();
            db.ExamResultDates.Remove(ExamResultDate);
            db.SaveChanges();
        }

        #endregion

        #region ExamResult

        public KSModel.Models.ExamResult GetExamResultById(int ExamResultId, bool IsTrack = false)
        {
            if (ExamResultId == 0)
                throw new ArgumentNullException("ExamResult");

            var ExamResult = new ExamResult();
            if (IsTrack)
                ExamResult = (from s in db.ExamResults where s.ExamResultId == ExamResultId select s).FirstOrDefault();
            else
                ExamResult = (from s in db.ExamResults.AsNoTracking() where s.ExamResultId == ExamResultId select s).FirstOrDefault();

            return ExamResult;
        }

        public List<KSModel.Models.ExamResult> GetAllExamResults(ref int totalcount, int? ExamDateSheetDetailId = null, int? StudentId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamResults.ToList();
            if (ExamDateSheetDetailId != null && ExamDateSheetDetailId > 0)
                query = query.Where(e => e.ExamDateSheetDetailId == ExamDateSheetDetailId).ToList();
            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();

            totalcount = query.Count;
            var ExamResults = new PagedList<KSModel.Models.ExamResult>(query, PageIndex, PageSize);
            return ExamResults;
        }

        public void InsertExamResult(KSModel.Models.ExamResult ExamResult)
        {
            if (ExamResult == null)
                throw new ArgumentNullException("ExamResult");

            db.ExamResults.Add(ExamResult);
            db.SaveChanges();
        }

        public void InsertExamResultInList(IList<ExamResult> ExamResultList)
        {
            if (ExamResultList == null || ExamResultList.Count == 0)
                throw new ArgumentNullException("ExamResult");

            db.ExamResults.AddRange(ExamResultList);
            db.SaveChanges();
        }

        public void UpdateExamResult(KSModel.Models.ExamResult ExamResult)
        {
            if (ExamResult == null)
                throw new ArgumentNullException("ExamResult");

            db.Entry(ExamResult).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void UpdateExamResultInList(IList<ExamResultModel> ExamResultList)
        {
            if (ExamResultList == null || ExamResultList.Count==0)
                throw new ArgumentNullException("ExamResult");
            var selectExamResultListIds = ExamResultList.Select(f => f.ExamResultId).ToArray();
            var exmrsltlist = db.ExamResults.Where(h => selectExamResultListIds.Contains(h.ExamResultId)).ToList();
            foreach (var item in exmrsltlist)
            {
                var examresult = ExamResultList.Where(f => f.ExamResultId == item.ExamResultId).FirstOrDefault();
                if (examresult != null)
                {
                    item.ObtainedMarks = examresult.ObtainedMarks != null ? Convert.ToString(examresult.ObtainedMarks) : "";
                    item.IntAsmtMarks = examresult.ObtainedIAMarks;
                    item.Remarks = examresult.Remarks;
                    item.IsAbsent = examresult.IsAbsent;
                    examresult.ExamDateSheetDetailId =(int)item.ExamDateSheetDetailId;
                }
            }
            db.SaveChanges();
        }

        public void DeleteExamResult(int ExamResultId = 0)
        {
            if (ExamResultId == 0)
                throw new ArgumentNullException("ExamResult");

            var ExamResult = (from s in db.ExamResults where s.ExamResultId == ExamResultId select s).FirstOrDefault();
            db.ExamResults.Remove(ExamResult);
            db.SaveChanges();
        }

        #endregion

        #region ExamResultWithheld

        public KSModel.Models.ExamResultWithheld GetExamResultWithheldById(int ExamResultWithheldId, bool IsTrack = false)
        {
            if (ExamResultWithheldId == 0)
                throw new ArgumentNullException("ExamResultWithheld");

            var ExamResultWithheld = new ExamResultWithheld();
            if (IsTrack)
                ExamResultWithheld = (from s in db.ExamResultWithhelds where s.ExamResultWithheldId == ExamResultWithheldId select s).FirstOrDefault();
            else
                ExamResultWithheld = (from s in db.ExamResultWithhelds.AsNoTracking() where s.ExamResultWithheldId == ExamResultWithheldId select s).FirstOrDefault();

            return ExamResultWithheld;
        }

        public List<KSModel.Models.ExamResultWithheld> GetAllExamResultWithhelds(ref int totalcount, 
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamResultWithhelds.ToList();
           

            totalcount = query.Count;
            var ExamResultWithhelds = new PagedList<KSModel.Models.ExamResultWithheld>(query, PageIndex, PageSize);
            return ExamResultWithhelds;
        }

        public void InsertExamResultWithheld(KSModel.Models.ExamResultWithheld ExamResultWithheld)
        {
            if (ExamResultWithheld == null)
                throw new ArgumentNullException("ExamResultWithheld");

            db.ExamResultWithhelds.Add(ExamResultWithheld);
            db.SaveChanges();
        }

        public void UpdateExamResultWithheld(KSModel.Models.ExamResultWithheld ExamResultWithheld)
        {
            if (ExamResultWithheld == null)
                throw new ArgumentNullException("ExamResultWithheld");

            db.Entry(ExamResultWithheld).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamResultWithheld(int ExamResultWithheldId = 0)
        {
            if (ExamResultWithheldId == 0)
                throw new ArgumentNullException("ExamResultWithheld");

            var ExamResultWithheld = (from s in db.ExamResultWithhelds where s.ExamResultWithheldId == ExamResultWithheldId select s).FirstOrDefault();
            db.ExamResultWithhelds.Remove(ExamResultWithheld);
            db.SaveChanges();
        }

        #endregion

        #region Grade

        public KSModel.Models.Grade GetGradeById(int GradeId, bool IsTrack = false)
        {
            if (GradeId == 0)
                throw new ArgumentNullException("Grade");

            var Grade = new Grade();
            if (IsTrack)
                Grade = (from s in db.Grades where s.GradeId == GradeId select s).FirstOrDefault();
            else
                Grade = (from s in db.Grades.AsNoTracking() where s.GradeId == GradeId select s).FirstOrDefault();

            return Grade;
        }

        public List<KSModel.Models.Grade> GetAllGrades(ref int totalcount, string Grade = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Grades.ToList();
            if (!String.IsNullOrEmpty(Grade))
                query = query.Where(e => e.Grade1.ToLower().Contains(Grade.ToLower())).ToList();

            totalcount = query.Count;
            var Grades = new PagedList<KSModel.Models.Grade>(query, PageIndex, PageSize);
            return Grades;
        }

        public void InsertGrade(KSModel.Models.Grade Grade)
        {
            if (Grade == null)
                throw new ArgumentNullException("Grade");

            db.Grades.Add(Grade);
            db.SaveChanges();
        }

        public void UpdateGrade(KSModel.Models.Grade Grade)
        {
            if (Grade == null)
                throw new ArgumentNullException("Grade");

            db.Entry(Grade).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteGrade(int GradeId = 0)
        {
            if (GradeId == 0)
                throw new ArgumentNullException("Grade");

            var Grade = (from s in db.Grades where s.GradeId == GradeId select s).FirstOrDefault();
            db.Grades.Remove(Grade);
            db.SaveChanges();
        }

        #endregion

        #region MarksPattern 

        public KSModel.Models.MarksPattern GetMarksPatternById(int MarksPatternId, bool IsTrack = false)
        {
            if (MarksPatternId == 0)
                throw new ArgumentNullException("MarksPattern");

            var MarksPattern = new MarksPattern();
            if (IsTrack)
                MarksPattern = (from s in db.MarksPatterns where s.MarksPatternId == MarksPatternId select s).FirstOrDefault();
            else
                MarksPattern = (from s in db.MarksPatterns.AsNoTracking() where s.MarksPatternId == MarksPatternId select s).FirstOrDefault();

            return MarksPattern;
        }
        public List<KSModel.Models.MarksPattern> GetAllMarksPatternDetailsnotrack()
        {
            
            var MarksPatternDetail = new List<KSModel.Models.MarksPattern>();
            MarksPatternDetail = (from s in db.MarksPatterns.AsNoTracking() select s).ToList();
            return MarksPatternDetail;
        }
        public List<KSModel.Models.MarksPattern> GetAllMarksPatterns(ref int totalcount, int? GradePatternId = null, int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.MarksPatterns.ToList();
            if (GradePatternId != null && GradePatternId > 0)
                query = query.Where(e => e.GradePatternId == GradePatternId).ToList();

            totalcount = query.Count;
            var MarksPatterns = new PagedList<KSModel.Models.MarksPattern>(query, PageIndex, PageSize);
            return MarksPatterns;
        }

        public void InsertMarksPattern(KSModel.Models.MarksPattern MarksPattern)
        {
            if (MarksPattern == null)
                throw new ArgumentNullException("MarksPattern");

            db.MarksPatterns.Add(MarksPattern);
            db.SaveChanges();
        }

        public void UpdateMarksPattern(KSModel.Models.MarksPattern MarksPattern)
        {
            if (MarksPattern == null)
                throw new ArgumentNullException("MarksPattern");

            db.Entry(MarksPattern).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteMarksPattern(int MarksPatternId = 0)
        {
            if (MarksPatternId == 0)
                throw new ArgumentNullException("MarksPattern");

            var MarksPattern = (from s in db.MarksPatterns where s.MarksPatternId == MarksPatternId select s).FirstOrDefault();
            db.MarksPatterns.Remove(MarksPattern);
            db.SaveChanges();
        }

        #endregion

        #region GradePattern

        public KSModel.Models.GradePattern GetGradePatternById(int GradePatternId, bool IsTrack = false)
        {
            if (GradePatternId == 0)
                throw new ArgumentNullException("GradePattern");

            var GradePattern = new GradePattern();
            if (IsTrack)
                GradePattern = (from s in db.GradePatterns where s.GradePatternId == GradePatternId select s).FirstOrDefault();
            else
                GradePattern = (from s in db.GradePatterns.AsNoTracking() where s.GradePatternId == GradePatternId select s).FirstOrDefault();

            return GradePattern;
        }

        public List<KSModel.Models.GradePattern> GetAllGradePatterns(ref int totalcount, string GradePattern = "", int? BoardId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.GradePatterns.ToList();
            if (!String.IsNullOrEmpty(GradePattern))
                query = query.Where(e => e.GradePattern1.ToLower().Contains(GradePattern.ToLower())).ToList();
            if (BoardId != null && BoardId > 0)
                query = query.Where(g => g.BoardId == BoardId).ToList();

            totalcount = query.Count;
            var GradePatterns = new PagedList<KSModel.Models.GradePattern>(query, PageIndex, PageSize);
            return GradePatterns;
        }

        public void InsertGradePattern(KSModel.Models.GradePattern GradePattern)
        {
            if (GradePattern == null)
                throw new ArgumentNullException("GradePattern");

            db.GradePatterns.Add(GradePattern);
            db.SaveChanges();
        }

        public void UpdateGradePattern(KSModel.Models.GradePattern GradePattern)
        {
            if (GradePattern == null)
                throw new ArgumentNullException("GradePattern");

            db.Entry(GradePattern).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteGradePattern(int GradePatternId = 0)
        {
            if (GradePatternId == 0)
                throw new ArgumentNullException("GradePattern");

            var GradePattern = (from s in db.GradePatterns where s.GradePatternId == GradePatternId select s).FirstOrDefault();
            db.GradePatterns.Remove(GradePattern);
            db.SaveChanges();
        }

        #endregion

        #region ExamSubjectGroupType

        public KSModel.Models.ExamSubjectGroupType GetExamSubjectGroupTypeById(int ExamSubjectGroupTypeId, bool IsTrack = false)
        {
            if (ExamSubjectGroupTypeId == 0)
                throw new ArgumentNullException("ExamSubjectGroupType");

            var ExamSubjectGroupType = new ExamSubjectGroupType();
            if (IsTrack)
                ExamSubjectGroupType = (from s in db.ExamSubjectGroupTypes where s.ExamSubjectGroupTypeId == ExamSubjectGroupTypeId select s).FirstOrDefault();
            else
                ExamSubjectGroupType = (from s in db.ExamSubjectGroupTypes.AsNoTracking() where s.ExamSubjectGroupTypeId == ExamSubjectGroupTypeId select s).FirstOrDefault();

            return ExamSubjectGroupType;
        }

        public List<KSModel.Models.ExamSubjectGroupType> GetAllExamSubjectGroupTypes(ref int totalcount, 
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamSubjectGroupTypes.ToList();
          
            totalcount = query.Count;
            var ExamSubjectGroupTypes = new PagedList<KSModel.Models.ExamSubjectGroupType>(query, PageIndex, PageSize);
            return ExamSubjectGroupTypes;
        }

        public void InsertExamSubjectGroupType(KSModel.Models.ExamSubjectGroupType ExamSubjectGroupType)
        {
            if (ExamSubjectGroupType == null)
                throw new ArgumentNullException("ExamSubjectGroupType");

            db.ExamSubjectGroupTypes.Add(ExamSubjectGroupType);
            db.SaveChanges();
        }

        public void UpdateExamSubjectGroupType(KSModel.Models.ExamSubjectGroupType ExamSubjectGroupType)
        {
            if (ExamSubjectGroupType == null)
                throw new ArgumentNullException("ExamSubjectGroupType");

            db.Entry(ExamSubjectGroupType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamSubjectGroupType(int ExamSubjectGroupTypeId = 0)
        {
            if (ExamSubjectGroupTypeId == 0)
                throw new ArgumentNullException("ExamSubjectGroupType");

            var ExamSubjectGroupType = (from s in db.ExamSubjectGroupTypes where s.ExamSubjectGroupTypeId == ExamSubjectGroupTypeId select s).FirstOrDefault();
            db.ExamSubjectGroupTypes.Remove(ExamSubjectGroupType);
            db.SaveChanges();
        }

        #endregion

        #region GradePatternDetail

        public KSModel.Models.GradePatternDetail GetGradePatternDetailById(int GradePatternDetailId, bool IsTrack = false)
        {
            if (GradePatternDetailId == 0)
                throw new ArgumentNullException("GradePatternDetail");

            var GradePatternDetail = new GradePatternDetail();
            if (IsTrack)
                GradePatternDetail = (from s in db.GradePatternDetails where s.GradePatternDetailId == GradePatternDetailId select s).FirstOrDefault();
            else
                GradePatternDetail = (from s in db.GradePatternDetails.AsNoTracking() where s.GradePatternDetailId == GradePatternDetailId select s).FirstOrDefault();

            return GradePatternDetail;
        }
        public List<KSModel.Models.GradePatternDetail> GetAllGradePatternDetailsnotrack()
        {

            var GradePatternDetail = new List<KSModel.Models.GradePatternDetail>();
            GradePatternDetail = (from s in db.GradePatternDetails.AsNoTracking() select s).ToList();
            return GradePatternDetail;
        }
        public List<KSModel.Models.GradePatternDetail> GetAllGradePatternDetails(ref int totalcount, int? GradePatternId = null, int? GradeId = null,
            DateTime? EffectiveDate = null, DateTime? EndDate = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.GradePatternDetails.ToList();
            if (GradePatternId != null && GradePatternId > 0)
                query = query.Where(e => e.GradePatternId == GradePatternId).ToList();
            if (GradeId != null && GradeId > 0)
                query = query.Where(e => e.GradeId == GradeId).ToList();
            if (EffectiveDate.HasValue)
                query = query.Where(e => e.EffectiveDate == EffectiveDate).ToList();
            if (EndDate.HasValue)
                query = query.Where(e => e.EndDate == EndDate).ToList();

            totalcount = query.Count;
            var GradePatternDetails = new PagedList<KSModel.Models.GradePatternDetail>(query, PageIndex, PageSize);
            return GradePatternDetails;
        }

        public void InsertGradePatternDetail(KSModel.Models.GradePatternDetail GradePatternDetail)
        {
            if (GradePatternDetail == null)
                throw new ArgumentNullException("GradePatternDetail");

            db.GradePatternDetails.Add(GradePatternDetail);
            db.SaveChanges();
        }

        public void UpdateGradePatternDetail(KSModel.Models.GradePatternDetail GradePatternDetail)
        {
            if (GradePatternDetail == null)
                throw new ArgumentNullException("GradePatternDetail");

            db.Entry(GradePatternDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteGradePatternDetail(int GradePatternDetailId = 0)
        {
            if (GradePatternDetailId == 0)
                throw new ArgumentNullException("GradePatternDetail");

            var GradePatternDetail = (from s in db.GradePatternDetails where s.GradePatternDetailId == GradePatternDetailId select s).FirstOrDefault();
            db.GradePatternDetails.Remove(GradePatternDetail);
            db.SaveChanges();
        }

        #endregion

        #region ExamTermEndSkill
        public KSModel.Models.ExamTermEndSkill GetExamTermEndSkillById(int ExamTermEndSkillId, bool IsTrack = false)
        {
            if (ExamTermEndSkillId == 0)
                throw new ArgumentNullException("ExamTermEndSkill");

            var ExamTermEndSkill = new ExamTermEndSkill();
            if (IsTrack)
                ExamTermEndSkill = (from s in db.ExamTermEndSkills where s.ExamTermEndSkillId == ExamTermEndSkillId select s).FirstOrDefault();
            else
                ExamTermEndSkill = (from s in db.ExamTermEndSkills.AsNoTracking() where s.ExamTermEndSkillId == ExamTermEndSkillId select s).FirstOrDefault();

            return ExamTermEndSkill;
        }

        public List<KSModel.Models.ExamTermEndSkill> GetAllExamTermEndSkills(ref int totalcount, string ExamTermEndSkill = "",int? ExamTermEndSkillTypeId=null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamTermEndSkills.ToList();
            if (!String.IsNullOrEmpty(ExamTermEndSkill))
                query = query.Where(e => e.ExamTermEndSkill1.ToLower().Contains(ExamTermEndSkill.ToLower())).ToList();
            if (ExamTermEndSkillTypeId != null)
            {
                query = query.Where(e => e.ExamTermEndSkillTypeId == ExamTermEndSkillTypeId).ToList();
            }
            totalcount = query.Count;
            var ExamTermEndSkills = new PagedList<KSModel.Models.ExamTermEndSkill>(query, PageIndex, PageSize);
            return ExamTermEndSkills;
        }

        public void InsertExamTermEndSkill(KSModel.Models.ExamTermEndSkill ExamTermEndSkill)
        {
            if (ExamTermEndSkill == null)
                throw new ArgumentNullException("ExamTermEndSkill");

            db.ExamTermEndSkills.Add(ExamTermEndSkill);
            db.SaveChanges();
        }

        public void UpdateExamTermEndSkill(KSModel.Models.ExamTermEndSkill ExamTermEndSkill)
        {
            if (ExamTermEndSkill == null)
                throw new ArgumentNullException("ExamTermEndSkill");

            db.Entry(ExamTermEndSkill).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamTermEndSkill(int ExamTermEndSkillId = 0)
        {
            if (ExamTermEndSkillId == 0)
                throw new ArgumentNullException("ExamTermEndSkill");

            var ExamTermEndSkill = (from s in db.ExamTermEndSkills where s.ExamTermEndSkillId == ExamTermEndSkillId select s).FirstOrDefault();
            db.ExamTermEndSkills.Remove(ExamTermEndSkill);
            db.SaveChanges();
        }
        #endregion

        #region ExamStandardTermEndSkill
        public KSModel.Models.ExamStandardTermEndSkill GetExamStandardTermEndSkillById(int ExamStandardTermEndSkillId, bool IsTrack = false)
        {
            if (ExamStandardTermEndSkillId == 0)
                throw new ArgumentNullException("ExamStandardTermEndSkill");

            var ExamStandardTermEndSkill = new ExamStandardTermEndSkill();
            if (IsTrack)
                ExamStandardTermEndSkill = (from s in db.ExamStandardTermEndSkills where s.ExamStandardTermEndSkillId == ExamStandardTermEndSkillId select s).FirstOrDefault();
            else
                ExamStandardTermEndSkill = (from s in db.ExamStandardTermEndSkills.AsNoTracking() where s.ExamStandardTermEndSkillId == ExamStandardTermEndSkillId select s).FirstOrDefault();

            return ExamStandardTermEndSkill;
        }

        public List<KSModel.Models.ExamStandardTermEndSkill> GetAllExamStandardTermEndSkills(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamStandardTermEndSkills.ToList();
            
            totalcount = query.Count;
            var ExamStandardTermEndSkills = new PagedList<KSModel.Models.ExamStandardTermEndSkill>(query, PageIndex, PageSize);
            return ExamStandardTermEndSkills;
        }

        public void InsertExamStandardTermEndSkill(KSModel.Models.ExamStandardTermEndSkill ExamStandardTermEndSkill)
        {
            if (ExamStandardTermEndSkill == null)
                throw new ArgumentNullException("ExamStandardTermEndSkill");

            db.ExamStandardTermEndSkills.Add(ExamStandardTermEndSkill);
            db.SaveChanges();
        }

        public void UpdateExamStandardTermEndSkill(KSModel.Models.ExamStandardTermEndSkill ExamStandardTermEndSkill)
        {
            if (ExamStandardTermEndSkill == null)
                throw new ArgumentNullException("ExamStandardTermEndSkill");

            db.Entry(ExamStandardTermEndSkill).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamStandardTermEndSkill(int ExamStandardTermEndSkillId = 0)
        {
            if (ExamStandardTermEndSkillId == 0)
                throw new ArgumentNullException("ExamStandardTermEndSkill");

            var ExamStandardTermEndSkill = (from s in db.ExamStandardTermEndSkills where s.ExamStandardTermEndSkillId == ExamStandardTermEndSkillId select s).FirstOrDefault();
            db.ExamStandardTermEndSkills.Remove(ExamStandardTermEndSkill);
            db.SaveChanges();
        }
        #endregion

        #region ExamTermEndSkillType

        public KSModel.Models.ExamTermEndSkillType GetExamTermEndSkillTypeById(int ExamTermEndSkillTypeId, bool IsTrack = false)
        {
            if (ExamTermEndSkillTypeId == 0)
                throw new ArgumentNullException("ExamTermEndSkillType");

            var ExamTermEndSkillType = new ExamTermEndSkillType();
            if (IsTrack)
                ExamTermEndSkillType = (from s in db.ExamTermEndSkillTypes where s.ExamTermEndSkillTypeId == ExamTermEndSkillTypeId select s).FirstOrDefault();
            else
                ExamTermEndSkillType = (from s in db.ExamTermEndSkillTypes.AsNoTracking() where s.ExamTermEndSkillTypeId == ExamTermEndSkillTypeId select s).FirstOrDefault();

            return ExamTermEndSkillType;
        }

        public List<KSModel.Models.ExamTermEndSkillType> GetAllExamTermEndSkillTypes(ref int totalcount, string ExamTermEndSkillType = "",string TypeCode="", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamTermEndSkillTypes.ToList();
            if (!String.IsNullOrEmpty(ExamTermEndSkillType))
                query = query.Where(e => e.ExamTermEndSkillType1.ToLower()==(ExamTermEndSkillType.ToLower())).ToList();
            if (!String.IsNullOrEmpty(TypeCode))
                query = query.Where(e => (!string.IsNullOrEmpty(e.TypeCode)) && e.TypeCode.ToLower()==TypeCode.ToLower()).ToList();

            totalcount = query.Count;
            var ExamTermEndSkillTypes = new PagedList<KSModel.Models.ExamTermEndSkillType>(query, PageIndex, PageSize);
            return ExamTermEndSkillTypes;
        }

        public void InsertExamTermEndSkillType(KSModel.Models.ExamTermEndSkillType ExamTermEndSkillType)
        {
            if (ExamTermEndSkillType == null)
                throw new ArgumentNullException("ExamTermEndSkillType");

            db.ExamTermEndSkillTypes.Add(ExamTermEndSkillType);
            db.SaveChanges();
        }

        public void UpdateExamTermEndSkillType(KSModel.Models.ExamTermEndSkillType ExamTermEndSkillType)
        {
            if (ExamTermEndSkillType == null)
                throw new ArgumentNullException("ExamTermEndSkillType");

            db.Entry(ExamTermEndSkillType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamTermEndSkillType(int ExamTermEndSkillTypeId = 0)
        {
            if (ExamTermEndSkillTypeId == 0)
                throw new ArgumentNullException("ExamTermEndSkillType");

            var ExamTermEndSkillType = (from s in db.ExamTermEndSkillTypes where s.ExamTermEndSkillTypeId == ExamTermEndSkillTypeId select s).FirstOrDefault();
            db.ExamTermEndSkillTypes.Remove(ExamTermEndSkillType);
            db.SaveChanges();
        }

        #endregion

        #region ExamTermEndSkillsDetail

        public KSModel.Models.ExamTermEndSkillsDetail GetExamTermEndSkillsDetailById(int ExamTermEndSkillDetailId, bool IsTrack = false)
        {
            if (ExamTermEndSkillDetailId == 0)
                throw new ArgumentNullException("ExamTermEndSkillsDetail");

            var ExamTermEndSkillsDetail = new ExamTermEndSkillsDetail();
            if (IsTrack)
                ExamTermEndSkillsDetail = (from s in db.ExamTermEndSkillsDetails where s.ExamTermEndSkillDetailId == ExamTermEndSkillDetailId select s).FirstOrDefault();
            else
                ExamTermEndSkillsDetail = (from s in db.ExamTermEndSkillsDetails.AsNoTracking() where s.ExamTermEndSkillDetailId == ExamTermEndSkillDetailId select s).FirstOrDefault();

            return ExamTermEndSkillsDetail;
        }

        public IList<KSModel.Models.ExamTermEndSkillsDetail> GetAllExamTermEndSkillsDetails(ref int totalcount, int? ExamTermEndSkillsId = null,
            string ExamTermEndSkillsDetail = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamTermEndSkillsDetails.ToList();
            if (!String.IsNullOrEmpty(ExamTermEndSkillsDetail))
                query = query.Where(e => e.ExamTermEndSkillDetail.ToLower() == ExamTermEndSkillsDetail.ToLower()).ToList();
            if (ExamTermEndSkillsId > 0)
                query = query.Where(e => e.ExamTermEndSkillsId == ExamTermEndSkillsId).ToList();

            totalcount = query.Count;
            var ExamTermEndSkillsDetails = new PagedList<KSModel.Models.ExamTermEndSkillsDetail>(query, PageIndex, PageSize);
            return ExamTermEndSkillsDetails;
        }

        public void InsertExamTermEndSkillsDetail(KSModel.Models.ExamTermEndSkillsDetail ExamTermEndSkillsDetail)
        {
            if (ExamTermEndSkillsDetail == null)
                throw new ArgumentNullException("ExamTermEndSkillsDetail");

            db.ExamTermEndSkillsDetails.Add(ExamTermEndSkillsDetail);
            db.SaveChanges();
        }

        public void UpdateExamTermEndSkillsDetail(KSModel.Models.ExamTermEndSkillsDetail ExamTermEndSkillsDetail)
        {
            if (ExamTermEndSkillsDetail == null)
                throw new ArgumentNullException("ExamTermEndSkillsDetail");

            db.Entry(ExamTermEndSkillsDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamTermEndSkillsDetail(int ExamTermEndSkillDetailId = 0)
        {
            if (ExamTermEndSkillDetailId == 0)
                throw new ArgumentNullException("ExamTermEndSkillsDetail");

            var ExamTermEndSkillsDetail = (from s in db.ExamTermEndSkillsDetails where s.ExamTermEndSkillDetailId == ExamTermEndSkillDetailId select s).FirstOrDefault();
            db.ExamTermEndSkillsDetails.Remove(ExamTermEndSkillsDetail);
            db.SaveChanges();
        }

        #endregion

        #region ExamTermActivitySkills

        public ExamTermActivitySkill GetExamTermActivitySkillsById(int ExamTermActivitySkillsId, bool IsTrack = false)
        {
            if (ExamTermActivitySkillsId == 0)
                throw new ArgumentNullException("ExamTermActivitySkill");

            var ExamTermActivitySkill = new ExamTermActivitySkill();
            if (IsTrack)
                ExamTermActivitySkill = (from s in db.ExamTermActivitySkills where s.ExamTermActivitySkillId == ExamTermActivitySkillsId select s).FirstOrDefault();
            else
                ExamTermActivitySkill = (from s in db.ExamTermActivitySkills.AsNoTracking() where s.ExamTermActivityId == ExamTermActivitySkillsId select s).FirstOrDefault();

            return ExamTermActivitySkill;
        }

        public List<ExamTermActivitySkill> GetAllExamTermActivitySkills(ref int totalcount, int? ExamTermActivityId = null, int? ExamTermEndSkillsId = null, int? StudentId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamTermActivitySkills.ToList();
            if (ExamTermActivityId > 0)
                query = query.Where(e => e.ExamTermActivityId == ExamTermActivityId).ToList();
            //if (ExamTermEndSkillsId > 0) Database Changes Effect By Sukhjinder Singh
            //    query = query.Where(e => e.ExamTermEndSkillsId == ExamTermEndSkillsId).ToList();
            if (StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();

            totalcount = query.Count;
            var ExamTermActivitySkills = new PagedList<KSModel.Models.ExamTermActivitySkill>(query, PageIndex, PageSize);
            return ExamTermActivitySkills;
        }

        public void InsertExamTermActivitySkill(ExamTermActivitySkill ExamTermActivitySkill)
        {
            if (ExamTermActivitySkill == null)
                throw new ArgumentNullException("ExamTermActivitySkill");

            db.ExamTermActivitySkills.Add(ExamTermActivitySkill);
            db.SaveChanges();
        }

        public void InsertExamTermActivitySkillInList(IList<ExamTermActivitySkill> ExamTermActivitySkillList)
        {
            if (ExamTermActivitySkillList == null || ExamTermActivitySkillList.Count()==0)
                throw new ArgumentNullException("ExamTermActivitySkill");

            db.ExamTermActivitySkills.AddRange(ExamTermActivitySkillList);
            db.SaveChanges();
        }

        public void UpdateExamTermActivitySkill(ExamTermActivitySkill ExamTermActivitySkill)
        {
            if (ExamTermActivitySkill == null)
                throw new ArgumentNullException("ExamTermActivitySkill");

            db.Entry(ExamTermActivitySkill).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamTermActivitySkill(int ExamTermActivitySkillId = 0)
        {
            if (ExamTermActivitySkillId == 0)
                throw new ArgumentNullException("ExamTermActivitySkillId");

            var ExamTermActivitySkills = (from s in db.ExamTermActivitySkills where s.ExamTermActivitySkillId == ExamTermActivitySkillId select s).FirstOrDefault();
            db.ExamTermActivitySkills.Remove(ExamTermActivitySkills);
            db.SaveChanges();
        }

        public void DeleteExamTermActivitySkillInArray(int[] ExamTermActivitySkillId = null)
        {
            if (ExamTermActivitySkillId != null && ExamTermActivitySkillId.Count() == 0)
                throw new ArgumentNullException("ExamTermActivitySkillId");

            var AllExamTermActivitySkills = db.ExamTermActivitySkills.Where(f => ExamTermActivitySkillId.Contains((int)f.ExamTermActivitySkillId)).ToList();
            if (AllExamTermActivitySkills.Count > 0)
            {
                db.ExamTermActivitySkills.RemoveRange(AllExamTermActivitySkills);
                db.SaveChanges();
            }
        }
        #endregion

        #region ExamTermActivityDetail

        public ExamTermActivityDetail GetExamTermActivityDetailById(int ExamTermActivityDetailId, bool IsTrack = false)
        {
            if (ExamTermActivityDetailId == 0)
                throw new ArgumentNullException("ExamTermActivityDetail");

            var ExamTermActivityDetail = new ExamTermActivityDetail();
            if (IsTrack)
                ExamTermActivityDetail = (from s in db.ExamTermActivityDetails where s.ExamTermActivityDetailId == ExamTermActivityDetailId select s).FirstOrDefault();
            else
                ExamTermActivityDetail = (from s in db.ExamTermActivityDetails.AsNoTracking() where s.ExamTermActivityDetailId == ExamTermActivityDetailId select s).FirstOrDefault();

            return ExamTermActivityDetail;
        }

        public List<ExamTermActivityDetail> GetAllExamTermActivityDetails(ref int totalcount, int? ExamTermActivitySkillsId = null,int? ClassSubjectId=null,int? ExamtermendSkillId=null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamTermActivityDetails.ToList();
            if (ExamTermActivitySkillsId > 0)
            {
                query = query.Where(m => m.ExamTermActivitySkillsId == ExamTermActivitySkillsId).ToList();
            }
            if (ClassSubjectId > 0)
            {
                query = query.Where(m => m.ClassSubjectId == ClassSubjectId).ToList();
            }
            if (ExamtermendSkillId > 0)
            {
                query = query.Where(m => m.ExamTermEndSkillsId == ExamtermendSkillId).ToList();
            }

            totalcount = query.Count;
            var ExamTermActivityDetails = new PagedList<KSModel.Models.ExamTermActivityDetail>(query, PageIndex, PageSize);
            return ExamTermActivityDetails;
        }

        public void InsertExamTermActivityDetail(ExamTermActivityDetail ExamTermActivityDetail)
        {
            if (ExamTermActivityDetail == null)
                throw new ArgumentNullException("ExamTermActivityDetail");

            db.ExamTermActivityDetails.Add(ExamTermActivityDetail);
            db.SaveChanges();
        }
        public void InsertExamTermActivityDetailInList(IList<ExamTermActivityDetail> ExamTermActivityDetailList)
        {
            if (ExamTermActivityDetailList == null || ExamTermActivityDetailList.Count() == 0)
                throw new ArgumentNullException("ExamTermActivitySkill");

            db.ExamTermActivityDetails.AddRange(ExamTermActivityDetailList);
            db.SaveChanges();
        }

        public void UpdateExamTermActivityDetail(ExamTermActivityDetail ExamTermActivityDetail)
        {
            if (ExamTermActivityDetail == null)
                throw new ArgumentNullException("ExamTermActivityDetail");

            db.Entry(ExamTermActivityDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamTermActivityDetail(int ExamTermActivityDetailId = 0)
        {
            if (ExamTermActivityDetailId == 0)
                throw new ArgumentNullException("ExamActivityDetail");

            var ExamTermActivityDetail = (from s in db.ExamTermActivityDetails where s.ExamTermActivityDetailId == ExamTermActivityDetailId select s).FirstOrDefault();
            db.ExamTermActivityDetails.Remove(ExamTermActivityDetail);
            db.SaveChanges();
        }
        public void DeleteExamTermActivityDetailInArray(int[] ExamTermActivityDetailId = null)
        {
            if (ExamTermActivityDetailId!= null && ExamTermActivityDetailId.Count()==0)
                throw new ArgumentNullException("ExamActivityDetail");
            var AllExamTermActitvityDetails = db.ExamTermActivityDetails.Where(f=> ExamTermActivityDetailId.Contains((int)f.ExamTermActivityDetailId)).ToList();
            if (AllExamTermActitvityDetails.Count > 0)
            {
                db.ExamTermActivityDetails.RemoveRange(AllExamTermActitvityDetails);
                db.SaveChanges();
            }
        }

        #endregion

        //#region ExamTermActivitySkillsDetail

        //public ExamTermActivitySkillsDetail GetExamTermActivitySkillsDetailById(int ExamTermActivitySkillsDetailId, bool IsTrack = false)
        //{
        //    if (ExamTermActivitySkillsDetailId == 0)
        //        throw new ArgumentNullException("ExamTermActivitySkillsDetail");

        //    var ExamTermActivitySkillsDetail = new ExamTermActivitySkillsDetail();
        //    if (IsTrack)
        //        ExamTermActivitySkillsDetail = (from s in db.ExamTermActivitySkillsDetails where s.ExamTermActivitySkillsDetailId == ExamTermActivitySkillsDetailId select s).FirstOrDefault();
        //    else
        //        ExamTermActivitySkillsDetail = (from s in db.ExamTermActivitySkillsDetails.AsNoTracking() where s.ExamTermActivitySkillsDetailId == ExamTermActivitySkillsDetailId select s).FirstOrDefault();

        //    return ExamTermActivitySkillsDetail;
        //}

        //public IList<ExamTermActivitySkillsDetail> GetAllExamTermActivitySkillsDetails(ref int totalcount, int? ExamTermActivityDetailId = null,
        //    int? ExamTermEndSkillsDetailId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        //{
        //    var query = db.ExamTermActivitySkillsDetails.ToList();
        //    if (ExamTermActivityDetailId > 0)
        //        query = query.Where(m => m.ExamTermActivityDetailId == ExamTermActivityDetailId).ToList();
        //    if (ExamTermEndSkillsDetailId > 0)
        //        query = query.Where(m => m.ExamTermEndSkillsDetailId == ExamTermEndSkillsDetailId).ToList();

        //    totalcount = query.Count;
        //    var ExamTermActivitySkillsDetails = new PagedList<KSModel.Models.ExamTermActivitySkillsDetail>(query, PageIndex, PageSize);
        //    return ExamTermActivitySkillsDetails;
        //}

        //public void InsertExamTermActivitySkillsDetail(ExamTermActivitySkillsDetail ExamTermActivitySkillsDetail)
        //{
        //    if (ExamTermActivitySkillsDetail == null)
        //        throw new ArgumentNullException("ExamTermActivitySkillsDetail");

        //    db.ExamTermActivitySkillsDetails.Add(ExamTermActivitySkillsDetail);
        //    db.SaveChanges();
        //}

        //public void UpdateExamTermActivitySkillsDetail(ExamTermActivitySkillsDetail ExamTermActivitySkillsDetail)
        //{
        //    if (ExamTermActivitySkillsDetail == null)
        //        throw new ArgumentNullException("ExamTermActivitySkillsDetail");

        //    db.Entry(ExamTermActivitySkillsDetail).State = System.Data.Entity.EntityState.Modified;
        //    db.SaveChanges();
        //}

        //public void DeleteExamTermActivitySkillsDetail(int ExamTermActivitySkillsDetailId = 0)
        //{
        //    if (ExamTermActivitySkillsDetailId == 0)
        //        throw new ArgumentNullException("ExamTermActivitySkillsDetail");

        //    var ExamTermActivitySkillsDetail = (from s in db.ExamTermActivitySkillsDetails where s.ExamTermActivitySkillsDetailId == ExamTermActivitySkillsDetailId select s).FirstOrDefault();
        //    db.ExamTermActivitySkillsDetails.Remove(ExamTermActivitySkillsDetail);
        //    db.SaveChanges();
        //}


        //#endregion

        #region ExamReportCard
        public DataTable GetReportCartHtmlWithTokens(ref int totalcount, int? SessionId = null, int? ClassId = null,
                        int? StudentId = null, int? StandardId = null,
                        int? StudentClassId = null, int? TermCount = null, string PrintText = "")
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_ReportCard", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@StudentId", StudentId);
                    sqlComm.Parameters.AddWithValue("@ClassId", ClassId);
                    sqlComm.Parameters.AddWithValue("@StandardId", StandardId);
                    sqlComm.Parameters.AddWithValue("@StudentClassId", StudentClassId);
                    sqlComm.Parameters.AddWithValue("@TermCount", TermCount);
                    sqlComm.Parameters.AddWithValue("@PrintText", PrintText != null ? PrintText : "");
                   
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            return dt;
        }

        public DataTable GetReportCartExcel(ref int totalcount, int? SessionId = null, int? ClassId = null,
                       int? StudentId = null, int? StandardId = null,
                       int? StudentClassId = null, int? TermCount = null, string PrintText = "")
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_ReportCard_Computation", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@StudentId", StudentId);
                    sqlComm.Parameters.AddWithValue("@ClassId", ClassId);
                    sqlComm.Parameters.AddWithValue("@StandardId", StandardId);
                    sqlComm.Parameters.AddWithValue("@StudentClassId", StudentClassId);
                    sqlComm.Parameters.AddWithValue("@TermCount", TermCount);
                    sqlComm.Parameters.AddWithValue("@PrintText", PrintText != null ? PrintText : "");

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            return dt;
        }
        public DataTable GetReportCartHtmlWithTokensViewResult(ref int totalcount, int? SessionId = null, int? ClassId = null,
                        int? StudentId = null, int? StandardId = null,
                        int? StudentClassId = null, int? ExamdateSheetId = null)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("[sp_ReportCardViewResult]", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@StudentId", StudentId);
                    sqlComm.Parameters.AddWithValue("@ClassId", ClassId);
                    sqlComm.Parameters.AddWithValue("@StandardId", StandardId);
                    sqlComm.Parameters.AddWithValue("@StudentClassId", StudentClassId);
                    sqlComm.Parameters.AddWithValue("@ExDateSheetId", ExamdateSheetId);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            return dt;
        }
        #endregion

        #region ExamReportCardTemplate
        public KSModel.Models.ExamReportCardTemplate GetExamReportCardTemplateById(int ExamReportCardTemplateId, bool IsTrack = false)
        {
            if (ExamReportCardTemplateId == 0)
                throw new ArgumentNullException("ExamReportCardTemplate");

            var ExamReportCardTemplate = new ExamReportCardTemplate();
            if (IsTrack)
                ExamReportCardTemplate = (from s in db.ExamReportCardTemplates where s.ExamReportCardTemplateId == ExamReportCardTemplateId select s).FirstOrDefault();
            else
                ExamReportCardTemplate = (from s in db.ExamReportCardTemplates.AsNoTracking() where s.ExamReportCardTemplateId == ExamReportCardTemplateId select s).FirstOrDefault();

            return ExamReportCardTemplate;
        }

        public List<KSModel.Models.ExamReportCardTemplate> GetAllExamReportCardTemplates(ref int totalcount, bool? IsActive = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamReportCardTemplates.ToList();
            if (IsActive != null)
                query = query.Where(e => e.IsActive == IsActive).ToList();
            
            totalcount = query.Count;
            var ExamReportCardTemplate = new PagedList<KSModel.Models.ExamReportCardTemplate>(query, PageIndex, PageSize);
            return ExamReportCardTemplate;
        }

        public void InsertExamReportCardTemplate(KSModel.Models.ExamReportCardTemplate ExamReportCardTemplate)
        {
            if (ExamReportCardTemplate == null)
                throw new ArgumentNullException("ExamReportCardTemplate");

            db.ExamReportCardTemplates.Add(ExamReportCardTemplate);
            db.SaveChanges();
        }

        public void UpdateExamReportCardTemplate(KSModel.Models.ExamReportCardTemplate ExamReportCardTemplate)
        {
            if (ExamReportCardTemplate == null)
                throw new ArgumentNullException("ExamReportCardTemplate");

            db.Entry(ExamReportCardTemplate).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamReportCardTemplate(int ExamReportCardTemplateId = 0)
        {
            if (ExamReportCardTemplateId == 0)
                throw new ArgumentNullException("ExamReportCardTemplate");

            var ExamReportCardTemplate = (from s in db.ExamReportCardTemplates where s.ExamReportCardTemplateId == ExamReportCardTemplateId select s).FirstOrDefault();
            db.ExamReportCardTemplates.Remove(ExamReportCardTemplate);
            db.SaveChanges();
        }
        #endregion

        #region ExamReportCardTempMapping
        public KSModel.Models.ExamReportCardTempMapping GetExamReportCardTempMappingById(int ExamReportCardTempMappingId, bool IsTrack = false)
        {
            if (ExamReportCardTempMappingId == 0)
                throw new ArgumentNullException("ExamReportCardTempMapping");

            var ExamReportCardTempMapping = new ExamReportCardTempMapping();
            if (IsTrack)
                ExamReportCardTempMapping = (from s in db.ExamReportCardTempMappings where s.ExamReportCardTempMappingId == ExamReportCardTempMappingId select s).FirstOrDefault();
            else
                ExamReportCardTempMapping = (from s in db.ExamReportCardTempMappings.AsNoTracking() where s.ExamReportCardTempMappingId == ExamReportCardTempMappingId select s).FirstOrDefault();

            return ExamReportCardTempMapping;
        }

        public List<KSModel.Models.ExamReportCardTempMapping> GetAllExamReportCardTempMappings(ref int totalcount,
                        int? ExamDateSheetId = null,int? StandardId = null,int? ExamReportCardTemplateId=null,
                        int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamReportCardTempMappings.ToList();
            if (ExamDateSheetId != null)
                query = query.Where(e => e.ExamDateSheetId == ExamDateSheetId).ToList();
            if (StandardId != null)
                query = query.Where(e => e.StandardId == StandardId).ToList();
            if (ExamReportCardTemplateId != null)
                query = query.Where(e => e.ExamReportCardTemplateId == ExamReportCardTemplateId).ToList();


            totalcount = query.Count;
            var ExamReportCardTempMapping = new PagedList<KSModel.Models.ExamReportCardTempMapping>(query, PageIndex, PageSize);
            return ExamReportCardTempMapping;
        }

        public void InsertExamReportCardTempMapping(KSModel.Models.ExamReportCardTempMapping ExamReportCardTempMapping)
        {
            if (ExamReportCardTempMapping == null)
                throw new ArgumentNullException("ExamReportCardTempMapping");

            db.ExamReportCardTempMappings.Add(ExamReportCardTempMapping);
            db.SaveChanges();
        }

        public void UpdateExamReportCardTempMapping(KSModel.Models.ExamReportCardTempMapping ExamReportCardTempMapping)
        {
            if (ExamReportCardTempMapping == null)
                throw new ArgumentNullException("ExamReportCardTempMapping");

            db.Entry(ExamReportCardTempMapping).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamReportCardTempMapping(int ExamReportCardTempMappingId = 0)
        {
            if (ExamReportCardTempMappingId == 0)
                throw new ArgumentNullException("ExamReportCardTempMapping");

            var ExamReportCardTempMapping = (from s in db.ExamReportCardTempMappings where s.ExamReportCardTempMappingId == ExamReportCardTempMappingId select s).FirstOrDefault();
            db.ExamReportCardTempMappings.Remove(ExamReportCardTempMapping);
            db.SaveChanges();
        }
        #endregion


        #endregion
        [SecurityCritical]
        public bool GetPublishResultStatus(int DateSheetId)
        {
            var PRS = db.ExamResultDates.Where(m => m.ExamDateSheetId == DateSheetId).FirstOrDefault();
            if(PRS!=null)
            {
                if(PRS.ResultPublishDate!=null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        #region ExamStudentAttendance

        public KSModel.Models.ExamStudentAttendance GetExamStudentAttendanceById(int ExamStudentAttendanceId, bool IsTrack = false)
        {
            if (ExamStudentAttendanceId == 0)
                throw new ArgumentNullException("ExamStudentAttendance");

            var ExamStudentAttendance = new ExamStudentAttendance();
            if (IsTrack)
                ExamStudentAttendance = (from s in db.ExamStudentAttendances where s.ExamStudentAttendanceId == ExamStudentAttendanceId select s).FirstOrDefault();
            else
                ExamStudentAttendance = (from s in db.ExamStudentAttendances.AsNoTracking() where s.ExamStudentAttendanceId == ExamStudentAttendanceId select s).FirstOrDefault();

            return ExamStudentAttendance;
        }

        public List<KSModel.Models.ExamStudentAttendance> GetAllExamStudentAttendances(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamStudentAttendances.ToList();
            
            totalcount = query.Count;
            var ExamStudentAttendances = new PagedList<KSModel.Models.ExamStudentAttendance>(query, PageIndex, PageSize);
            return ExamStudentAttendances;
        }

        public void InsertExamStudentAttendance(KSModel.Models.ExamStudentAttendance ExamStudentAttendance)
        {
            if (ExamStudentAttendance == null)
                throw new ArgumentNullException("ExamStudentAttendance");

            db.ExamStudentAttendances.Add(ExamStudentAttendance);
            db.SaveChanges();
        }
        public void InsertExamStudentAttendanceInList(IList<ExamStudentAttendance> ExamStudentAttendanceList)
        {
            if (ExamStudentAttendanceList == null|| ExamStudentAttendanceList.Count==0)
                throw new ArgumentNullException("ExamStudentAttendance");

            db.ExamStudentAttendances.AddRange(ExamStudentAttendanceList);
            db.SaveChanges();
        }

        public void UpdateExamStudentAttendance(KSModel.Models.ExamStudentAttendance ExamStudentAttendance)
        {
            if (ExamStudentAttendance == null)
                throw new ArgumentNullException("ExamStudentAttendance");

            db.Entry(ExamStudentAttendance).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void UpdateExamStudentAttendanceInList(IList<ExamStudentAttendance> ExamStudentAttendanceList)
        {
            if (ExamStudentAttendanceList == null || ExamStudentAttendanceList.Count ==0)
                throw new ArgumentNullException("ExamStudentAttendance");

            foreach (var ExamStudentAttendance in ExamStudentAttendanceList)
            {
                db.Entry(ExamStudentAttendance).State = System.Data.Entity.EntityState.Modified;
            }
            db.SaveChanges();
        }

        public void DeleteExamStudentAttendance(int ExamStudentAttendanceId = 0)
        {
            if (ExamStudentAttendanceId == 0)
                throw new ArgumentNullException("ExamStudentAttendance");

            var ExamStudentAttendance = (from s in db.ExamStudentAttendances where s.ExamStudentAttendanceId == ExamStudentAttendanceId select s).FirstOrDefault();
            db.ExamStudentAttendances.Remove(ExamStudentAttendance);
            db.SaveChanges();
        }
        public void DeleteExamStudentAttendanceInArray(int[] ExamStudentAttendance = null)
        {
            if (ExamStudentAttendance != null && ExamStudentAttendance.Count() == 0)
                throw new ArgumentNullException("ExamStudentAttendance");

            var AllExamStudentAttendances = db.ExamStudentAttendances.Where(f => ExamStudentAttendance.Contains((int)f.ExamStudentAttendanceId)).ToList();
            if (AllExamStudentAttendances.Count > 0)
            {
                db.ExamStudentAttendances.RemoveRange(AllExamStudentAttendances);
                db.SaveChanges();
            }
        }
        #endregion

        #region ExamStandardSubjectGroup

        public KSModel.Models.ExamStandardSubjectGroup GetExamStandardSubjectGroupById(int ExamStandardSubjectGroupId, bool IsTrack = false)
        {
            if (ExamStandardSubjectGroupId == 0)
                throw new ArgumentNullException("ExamStandardSubjectGroup");

            var ExamStandardSubjectGroup = new ExamStandardSubjectGroup();
            if (IsTrack)
                ExamStandardSubjectGroup = (from s in db.ExamStandardSubjectGroups where s.ExamStandardSubjectGroupId == ExamStandardSubjectGroupId select s).FirstOrDefault();
            else
                ExamStandardSubjectGroup = (from s in db.ExamStandardSubjectGroups.AsNoTracking() where s.ExamStandardSubjectGroupId == ExamStandardSubjectGroupId select s).FirstOrDefault();

            return ExamStandardSubjectGroup;
        }

        public List<KSModel.Models.ExamStandardSubjectGroup> GetAllExamStandardSubjectGroups(ref int totalcount,  int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamStandardSubjectGroups.ToList();
         
            totalcount = query.Count;
            var ExamStandardSubjectGroups = new PagedList<KSModel.Models.ExamStandardSubjectGroup>(query, PageIndex, PageSize);
            return ExamStandardSubjectGroups;
        }

        public void InsertExamStandardSubjectGroup(KSModel.Models.ExamStandardSubjectGroup ExamStandardSubjectGroup)
        {
            if (ExamStandardSubjectGroup == null)
                throw new ArgumentNullException("ExamStandardSubjectGroup");

            db.ExamStandardSubjectGroups.Add(ExamStandardSubjectGroup);
            db.SaveChanges();
        }

        public void UpdateExamStandardSubjectGroup(KSModel.Models.ExamStandardSubjectGroup ExamStandardSubjectGroup)
        {
            if (ExamStandardSubjectGroup == null)
                throw new ArgumentNullException("ExamStandardSubjectGroup");

            db.Entry(ExamStandardSubjectGroup).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamStandardSubjectGroup(int ExamStandardSubjectGroupId = 0)
        {
            if (ExamStandardSubjectGroupId == 0)
                throw new ArgumentNullException("ExamStandardSubjectGroup");

            var ExamStandardSubjectGroup = (from s in db.ExamStandardSubjectGroups where s.ExamStandardSubjectGroupId == ExamStandardSubjectGroupId select s).FirstOrDefault();
            db.ExamStandardSubjectGroups.Remove(ExamStandardSubjectGroup);
            db.SaveChanges();
        }

        #endregion

        #region ExamDateSheetSubjectSkill

        public KSModel.Models.ExamDateSheetSubjectSkill GetExamDateSheetSubjectSkillById(int ExamDateSheetSubjectSkillId, bool IsTrack = false)
        {
            if (ExamDateSheetSubjectSkillId == 0)
                throw new ArgumentNullException("ExamDateSheetSubjectSkill");

            var ExamDateSheetSubjectSkill = new ExamDateSheetSubjectSkill();
            if (IsTrack)
                ExamDateSheetSubjectSkill = (from s in db.ExamDateSheetSubjectSkills where s.ExamDateSheetSubjectSkillId == ExamDateSheetSubjectSkillId select s).FirstOrDefault();
            else
                ExamDateSheetSubjectSkill = (from s in db.ExamDateSheetSubjectSkills.AsNoTracking() where s.ExamDateSheetSubjectSkillId == ExamDateSheetSubjectSkillId select s).FirstOrDefault();

            return ExamDateSheetSubjectSkill;
        }

        public List<KSModel.Models.ExamDateSheetSubjectSkill> GetAllExamDateSheetSubjectSkills(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamDateSheetSubjectSkills.ToList();

            totalcount = query.Count;
            var ExamDateSheetSubjectSkills = new PagedList<KSModel.Models.ExamDateSheetSubjectSkill>(query, PageIndex, PageSize);
            return ExamDateSheetSubjectSkills;
        }

        public void InsertExamDateSheetSubjectSkill(KSModel.Models.ExamDateSheetSubjectSkill ExamDateSheetSubjectSkill)
        {
            if (ExamDateSheetSubjectSkill == null)
                throw new ArgumentNullException("ExamDateSheetSubjectSkill");

            db.ExamDateSheetSubjectSkills.Add(ExamDateSheetSubjectSkill);
            db.SaveChanges();
        }

        public void UpdateExamDateSheetSubjectSkill(KSModel.Models.ExamDateSheetSubjectSkill ExamDateSheetSubjectSkill)
        {
            if (ExamDateSheetSubjectSkill == null)
                throw new ArgumentNullException("ExamDateSheetSubjectSkill");

            db.Entry(ExamDateSheetSubjectSkill).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamDateSheetSubjectSkill(int ExamDateSheetSubjectSkillId = 0)
        {
            if (ExamDateSheetSubjectSkillId == 0)
                throw new ArgumentNullException("ExamDateSheetSubjectSkill");

            var ExamDateSheetSubjectSkill = (from s in db.ExamDateSheetSubjectSkills where s.ExamDateSheetSubjectSkillId == ExamDateSheetSubjectSkillId select s).FirstOrDefault();
            db.ExamDateSheetSubjectSkills.Remove(ExamDateSheetSubjectSkill);
            db.SaveChanges();
        }

        #endregion

        #region ExamAdmitCardNotes

        public ExamAdmitCardNote GetExamAdmitCardNotesById(int ExamAdmitCardNotesId, bool IsTrack = false)
        {
            if (ExamAdmitCardNotesId == 0)
                throw new ArgumentNullException("ExamAdmitCardNotes");

            var ExamAdmitCardNotes = new ExamAdmitCardNote();
            if (IsTrack)
                ExamAdmitCardNotes = (from s in db.ExamAdmitCardNotes where s.ExamAdmitCardNotesId == ExamAdmitCardNotesId select s).FirstOrDefault();
            else
                ExamAdmitCardNotes = (from s in db.ExamAdmitCardNotes.AsNoTracking() where s.ExamAdmitCardNotesId == ExamAdmitCardNotesId select s).FirstOrDefault();

            return ExamAdmitCardNotes;
        }

        public List<ExamAdmitCardNote> GetAllExamAdmitCardNotess(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamAdmitCardNotes.ToList();

            totalcount = query.Count;
            var ExamAdmitCardNotess = new PagedList<ExamAdmitCardNote>(query, PageIndex, PageSize);
            return ExamAdmitCardNotess;
        }

        public void InsertExamAdmitCardNotes(ExamAdmitCardNote ExamAdmitCardNotes)
        {
            if (ExamAdmitCardNotes == null)
                throw new ArgumentNullException("ExamAdmitCardNotes");

            db.ExamAdmitCardNotes.Add(ExamAdmitCardNotes);
            db.SaveChanges();
        }

        public void UpdateExamAdmitCardNotes(ExamAdmitCardNote ExamAdmitCardNotes)
        {
            if (ExamAdmitCardNotes == null)
                throw new ArgumentNullException("ExamAdmitCardNotes");

            db.Entry(ExamAdmitCardNotes).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExamAdmitCardNotes(int ExamAdmitCardNotesId = 0)
        {
            if (ExamAdmitCardNotesId == 0)
                throw new ArgumentNullException("ExamAdmitCardNotes");

            var ExamAdmitCardNotes = (from s in db.ExamAdmitCardNotes where s.ExamAdmitCardNotesId == ExamAdmitCardNotesId select s).FirstOrDefault();
            db.ExamAdmitCardNotes.Remove(ExamAdmitCardNotes);
            db.SaveChanges();
        }

        #endregion

        #region ExamTermGroup
        public List<ExamTermGroup> GetAllExamTermGroups(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamTermGroups.ToList();

            totalcount = query.Count;
            var ExamTermGroups = new PagedList<ExamTermGroup>(query, PageIndex, PageSize);
            return ExamTermGroups;
        }
        #endregion
    }
}