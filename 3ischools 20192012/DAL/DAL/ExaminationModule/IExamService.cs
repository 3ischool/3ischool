﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using ViewModel.ViewModel.Exam;

namespace DAL.DAL.ExaminationModule
{

    public partial interface IExamService
    {

        #region ExamTerm

        ExamTerm GetExamTermById(int ExamTermId, bool IsTrack = false);

        List<ExamTerm> GetAllExamTerms(ref int totalcount, string ExamTerm = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamTerm(ExamTerm ExamTerm);

        void UpdateExamTerm(ExamTerm ExamTerm);

        void DeleteExamTerm(int ExamTermId = 0);

        #endregion

        #region ExamActivity

        ExamActivity GetExamActivityById(int ExamActivityId, bool IsTrack = false);

        List<ExamActivity> GetAllExamActivities(ref int totalcount, string ExamActivity = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamActivity(ExamActivity ExamActivity);

        void UpdateExamActivity(ExamActivity ExamActivity);

        void DeleteExamActivity(int ExamActivityId = 0);

        #endregion

        #region ExamTermActivity

        ExamTermActivity GetExamTermActivityById(int ExamTermActivityId, bool IsTrack = false);

        List<ExamTermActivity> GetAllExamTermActivities(ref int totalcount, int? ExamTermId = null, int? ExamActivityId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamTermActivity(ExamTermActivity ExamTermActivity);

        void UpdateExamTermActivity(ExamTermActivity ExamTermActivity);

        void DeleteExamTermActivity(int ExamTermActivityId = 0);

        #endregion

        #region ExamTermStandard

        ExamTermStandard GetExamTermStandardById(int ExamTermStandardId, bool IsTrack = false);

        List<ExamTermStandard> GetAllExamTermStandards(ref int totalcount, int? ExamTermId = null, int? ExamStandardId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamTermStandard(ExamTermStandard ExamTermStandard);

        void UpdateExamTermStandard(ExamTermStandard ExamTermStandard);

        void DeleteExamTermStandard(int ExamTermStandardId = 0);

        #endregion

        #region ExamTimeSlot

        ExamTimeSlot GetExamTimeSlotById(int ExamTimeSlotId, bool IsTrack = false);

        List<ExamTimeSlot> GetAllExamTimeSlots(ref int totalcount, string ExamTimeSlot = "", DateTime? TimeFrom = null, DateTime? TimeTo = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamTimeSlot(ExamTimeSlot ExamTimeSlot);

        void UpdateExamTimeSlot(ExamTimeSlot ExamTimeSlot);

        void DeleteExamTimeSlot(int ExamTimeSlotId = 0);

        #endregion

        #region ExamDateSheet

        ExamDateSheet GetExamDateSheetById(int ExamDateSheetId, bool IsTrack = false);

        List<ExamDateSheet> GetAllExamDateSheets(ref int totalcount, string ExamDateSheet = "", int? ExamTermActivityId = null,
            DateTime? FromDate = null, DateTime? TillDate = null, bool? IsPublish = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamDateSheet(ExamDateSheet ExamDateSheet);

        void UpdateExamDateSheet(ExamDateSheet ExamDateSheet);

        void DeleteExamDateSheet(int ExamDateSheetId = 0);

        #endregion

        #region ExamPublishDetail

        ExamPublishDetail GetExamPublishDetailById(int ExamPublishId, bool IsTrack = false);

        List<ExamPublishDetail> GetAllExamPublishDetails(ref int totalcount, int? ExamDateSheetId = null,
            DateTime? PublishDate = null, DateTime? UnPublishDate = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamPublishDetail(ExamPublishDetail ExamPublishDetail);

        void UpdateExamPublishDetail(ExamPublishDetail ExamPublishDetail);

        void DeleteExamPublishDetail(int ExamPublishId = 0);

        #endregion

        #region ExamDateSheetDetail

        ExamDateSheetDetail GetExamDateSheetDetailById(int ExamDateSheetDetailId, bool IsTrack = false);

        List<ExamDateSheetDetail> GetAllExamDateSheetDetails(ref int totalcount, int? ExamDateSheetId = null,
            int? StandardId = null, int? SubjectId = null, int? ExamTimeSlotId = null, int? MarksPatternId = null, DateTime? ExamDate = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamDateSheetDetail(ExamDateSheetDetail ExamDateSheetDetail);

        void UpdateExamDateSheetDetail(ExamDateSheetDetail ExamDateSheetDetail);

        void DeleteExamDateSheetDetail(int ExamDateSheetDetailId = 0);

        #endregion

        #region ExamClassDateSheetDetail

        ExamClassDateSheetDetail GetExamClassDateSheetDetailById(int ExamClassDateSheetDetailId, bool IsTrack = false);

        List<ExamClassDateSheetDetail> GetAllExamClassDateSheetDetails(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamClassDateSheetDetail(ExamClassDateSheetDetail ExamClassDateSheetDetail);

        void UpdateExamClassDateSheetDetail(ExamClassDateSheetDetail ExamClassDateSheetDetail);

        void DeleteExamClassDateSheetDetail(int ExamClassDateSheetDetailId = 0);

        #endregion

        #region ExamResultDate

        ExamResultDate GetExamResultDateById(int ExamResultDateId, bool IsTrack = false);

        List<ExamResultDate> GetAllExamResultDates(ref int totalcount, int? ExamDateSheetId = null, int? StandardId = null,
            DateTime? ResultDate = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamResultDate(ExamResultDate ExamResultDate);

        void UpdateExamResultDate(ExamResultDate ExamResultDate);

        void DeleteExamResultDate(int ExamResultDateId = 0);

        #endregion

        #region ExamResult

        ExamResult GetExamResultById(int ExamResultId, bool IsTrack = false);

        List<ExamResult> GetAllExamResults(ref int totalcount, int? ExamDateSheetDetailId = null, int? StudentId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamResult(ExamResult ExamResult);
        void InsertExamResultInList(IList<ExamResult> ExamResultList);
        void UpdateExamResult(ExamResult ExamResult);

        void UpdateExamResultInList(IList<ExamResultModel> ExamResultList);

        void DeleteExamResult(int ExamResultId = 0);

        #endregion

        #region ExamResultWithheld

        ExamResultWithheld GetExamResultWithheldById(int ExamResultWithheldId, bool IsTrack = false);

        List<ExamResultWithheld> GetAllExamResultWithhelds(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamResultWithheld(ExamResultWithheld ExamResultWithheld);

        void UpdateExamResultWithheld(ExamResultWithheld ExamResultWithheld);

        void DeleteExamResultWithheld(int ExamResultWithheldId = 0);

        #endregion

        #region Grade

        Grade GetGradeById(int GradeId, bool IsTrack = false);

        List<Grade> GetAllGrades(ref int totalcount, string Grade = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertGrade(Grade Grade);

        void UpdateGrade(Grade Grade);

        void DeleteGrade(int GradeId = 0);

        #endregion

        #region MarksPattern

        MarksPattern GetMarksPatternById(int MarksPatternId, bool IsTrack = false);
        List<KSModel.Models.MarksPattern> GetAllMarksPatternDetailsnotrack();
        List<MarksPattern> GetAllMarksPatterns(ref int totalcount, int? GradePatternId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertMarksPattern(MarksPattern MarksPattern);

        void UpdateMarksPattern(MarksPattern MarksPattern);

        void DeleteMarksPattern(int MarksPatternId = 0);

        #endregion

        #region GradePattern

        GradePattern GetGradePatternById(int GradePatternId, bool IsTrack = false);

        List<GradePattern> GetAllGradePatterns(ref int totalcount, string GradePattern = "", int? BoardId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertGradePattern(GradePattern GradePattern);

        void UpdateGradePattern(GradePattern GradePattern);

        void DeleteGradePattern(int GradePatternId = 0);

        #endregion

        #region ExamSubjectGroupType

        ExamSubjectGroupType GetExamSubjectGroupTypeById(int ExamSubjectGroupTypeId, bool IsTrack = false);

        List<ExamSubjectGroupType> GetAllExamSubjectGroupTypes(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamSubjectGroupType(ExamSubjectGroupType ExamSubjectGroupType);

        void UpdateExamSubjectGroupType(ExamSubjectGroupType ExamSubjectGroupType);

        void DeleteExamSubjectGroupType(int ExamSubjectGroupTypeId = 0);

        #endregion

        #region GradePatternDetail

        GradePatternDetail GetGradePatternDetailById(int GradePatternDetailId, bool IsTrack = false);
        List<KSModel.Models.GradePatternDetail> GetAllGradePatternDetailsnotrack();
        List<GradePatternDetail> GetAllGradePatternDetails(ref int totalcount, int? GradePatternId = null, int? GradeId = null,
            DateTime? EffectiveDate = null, DateTime? EndDate = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertGradePatternDetail(GradePatternDetail GradePatternDetail);

        void UpdateGradePatternDetail(GradePatternDetail GradePatternDetail);

        void DeleteGradePatternDetail(int GradePatternDetailId = 0);

        #endregion

        #region ExamTermEndSkill
        KSModel.Models.ExamTermEndSkill GetExamTermEndSkillById(int ExamTermEndSkillId, bool IsTrack = false);


        List<KSModel.Models.ExamTermEndSkill> GetAllExamTermEndSkills(ref int totalcount, string ExamTermEndSkill = "", int? ExamTermEndSkillTypeId = null, int PageIndex = 0, int PageSize = int.MaxValue);


        void InsertExamTermEndSkill(KSModel.Models.ExamTermEndSkill ExamTermEndSkill);


        void UpdateExamTermEndSkill(KSModel.Models.ExamTermEndSkill ExamTermEndSkill);


        void DeleteExamTermEndSkill(int ExamTermEndSkillId = 0);

        #endregion
        #region ExamStandardTermEndSkill
        KSModel.Models.ExamStandardTermEndSkill GetExamStandardTermEndSkillById(int ExamStandardTermEndSkillId, bool IsTrack = false);


        List<KSModel.Models.ExamStandardTermEndSkill> GetAllExamStandardTermEndSkills(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);


        void InsertExamStandardTermEndSkill(KSModel.Models.ExamStandardTermEndSkill ExamStandardTermEndSkill);


        void UpdateExamStandardTermEndSkill(KSModel.Models.ExamStandardTermEndSkill ExamStandardTermEndSkill);


        void DeleteExamStandardTermEndSkill(int ExamStandardTermEndSkillId = 0);

        #endregion

        #region ExamTermEndSkillType

        KSModel.Models.ExamTermEndSkillType GetExamTermEndSkillTypeById(int ExamTermEndSkillTypeId, bool IsTrack = false);


        List<KSModel.Models.ExamTermEndSkillType> GetAllExamTermEndSkillTypes(ref int totalcount, string ExamTermEndSkillType = "", string TypeCode = "", int PageIndex = 0, int PageSize = int.MaxValue);


        void InsertExamTermEndSkillType(KSModel.Models.ExamTermEndSkillType ExamTermEndSkillType);


        void UpdateExamTermEndSkillType(KSModel.Models.ExamTermEndSkillType ExamTermEndSkillType);


        void DeleteExamTermEndSkillType(int ExamTermEndSkillTypeId = 0);

        #endregion

        #region ExamTermEndSkillsDetail

        KSModel.Models.ExamTermEndSkillsDetail GetExamTermEndSkillsDetailById(int ExamTermEndSkillDetailId, bool IsTrack = false);

        IList<KSModel.Models.ExamTermEndSkillsDetail> GetAllExamTermEndSkillsDetails(ref int totalcount, int? ExamTermEndSkillsId = null,
            string ExamTermEndSkillsDetail = "", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamTermEndSkillsDetail(KSModel.Models.ExamTermEndSkillsDetail ExamTermEndSkillsDetail);

        void UpdateExamTermEndSkillsDetail(KSModel.Models.ExamTermEndSkillsDetail ExamTermEndSkillsDetail);

        void DeleteExamTermEndSkillsDetail(int ExamTermEndSkillDetailId = 0);

        #endregion

        #region ExamTermActivitySkills

        ExamTermActivitySkill GetExamTermActivitySkillsById(int ExamTermActivitySkillsId, bool IsTrack = false);


        List<ExamTermActivitySkill> GetAllExamTermActivitySkills(ref int totalcount, int? ExamTermActivityId = null, int? ExamTermEndSkillsId = null, int? StudentId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);


        void InsertExamTermActivitySkill(ExamTermActivitySkill ExamTermActivitySkill);

        void InsertExamTermActivitySkillInList(IList<ExamTermActivitySkill> ExamTermActivitySkill);
        void UpdateExamTermActivitySkill(ExamTermActivitySkill ExamTermActivitySkill);


        void DeleteExamTermActivitySkill(int ExamTermActivitySkillId = 0);

        void DeleteExamTermActivitySkillInArray(int[] ExamTermActivitySkillId = null);
        #endregion

        #region ExamTermActivityDetail

        ExamTermActivityDetail GetExamTermActivityDetailById(int ExamTermActivityDetailId, bool IsTrack = false);

        List<ExamTermActivityDetail> GetAllExamTermActivityDetails(ref int totalcount, int? ExamTermActivitySkillsId = null, int? ClassSubjectId = null, int? ExamtermendSkillId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamTermActivityDetail(ExamTermActivityDetail ExamTermActivityDetail);

        void InsertExamTermActivityDetailInList(IList<ExamTermActivityDetail> ExamTermActivityDetailList);
        void UpdateExamTermActivityDetail(ExamTermActivityDetail ExamTermActivityDetail);

        void DeleteExamTermActivityDetail(int ExamTermActivityDetailId = 0);
        void DeleteExamTermActivityDetailInArray(int[] ExamTermActivityDetailId = null);

        #endregion

        //#region ExamTermActivitySkillsDetail

        //ExamTermEndSkillsDetail GetExamTermActivitySkillsDetailById(int ExamTermActivitySkillsDetailId, bool IsTrack = false);

        //IList<ExamTermActivitySkillsDetail> GetAllExamTermActivitySkillsDetails(ref int totalcount, int? ExamTermActivityDetailId = null,
        //    int? ExamTermEndSkillsDetailId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        //void InsertExamTermActivitySkillsDetail(ExamTermActivitySkillsDetail ExamTermActivitySkillsDetail);

        //void UpdateExamTermActivitySkillsDetail(ExamTermActivitySkillsDetail ExamTermActivitySkillsDetail);

        //void DeleteExamTermActivitySkillsDetail(int ExamTermActivitySkillsDetailId = 0);

        //#endregion

        #region ExamReportCardTemplate
        KSModel.Models.ExamReportCardTemplate GetExamReportCardTemplateById(int ExamReportCardTemplateId, bool IsTrack = false);

        List<KSModel.Models.ExamReportCardTemplate> GetAllExamReportCardTemplates(ref int totalcount, bool? IsActive = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamReportCardTemplate(KSModel.Models.ExamReportCardTemplate ExamReportCardTemplate);

        void UpdateExamReportCardTemplate(KSModel.Models.ExamReportCardTemplate ExamReportCardTemplate);

        void DeleteExamReportCardTemplate(int ExamReportCardTemplateId = 0);

        #endregion

        #region ExamReportCard
        DataTable GetReportCartHtmlWithTokens(ref int totalcount, int? SessionId = null, int? ClassId = null,
                        int? StudentId = null, int? StandardId = null,
                        int? StudentClassId = null, int? TermCount = null, string PrintText = "");
        DataTable GetReportCartExcel(ref int totalcount, int? SessionId = null, int? ClassId = null,
                       int? StudentId = null, int? StandardId = null,
                       int? StudentClassId = null, int? TermCount = null, string PrintText = "");
        DataTable GetReportCartHtmlWithTokensViewResult(ref int totalcount, int? SessionId = null, int? ClassId = null,
                      int? StudentId = null, int? StandardId = null,
                      int? StudentClassId = null, int? ExamdateSheetId = null);
        #endregion

        #region ExamReportCardTempMapping
        KSModel.Models.ExamReportCardTempMapping GetExamReportCardTempMappingById(int ExamReportCardTempMappingId, bool IsTrack = false);

        List<KSModel.Models.ExamReportCardTempMapping> GetAllExamReportCardTempMappings(ref int totalcount,
                        int? ExamDateSheetId = null, int? StandardId = null, int? ExamReportCardTemplateId = null,
                        int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamReportCardTempMapping(KSModel.Models.ExamReportCardTempMapping ExamReportCardTempMapping);

        void UpdateExamReportCardTempMapping(KSModel.Models.ExamReportCardTempMapping ExamReportCardTempMapping);

        void DeleteExamReportCardTempMapping(int ExamReportCardTempMappingId = 0);
        #endregion

        [SecurityCritical]
        bool GetPublishResultStatus(int DateSheetId);


        #region ExamStudentAttendance

        ExamStudentAttendance GetExamStudentAttendanceById(int ExamStudentAttendanceId, bool IsTrack = false);

        List<ExamStudentAttendance> GetAllExamStudentAttendances(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamStudentAttendance(ExamStudentAttendance ExamStudentAttendance);

        void InsertExamStudentAttendanceInList(IList<ExamStudentAttendance> ExamStudentAttendanceList);
        void UpdateExamStudentAttendance(ExamStudentAttendance ExamStudentAttendance);

        void UpdateExamStudentAttendanceInList(IList<ExamStudentAttendance> ExamStudentAttendanceList);

        void DeleteExamStudentAttendance(int ExamStudentAttendanceId = 0);

        void DeleteExamStudentAttendanceInArray(int[] ExamStudentAttendance = null);
        #endregion

        #region ExamStandardSubjectGroup

        ExamStandardSubjectGroup GetExamStandardSubjectGroupById(int ExamStandardSubjectGroupId, bool IsTrack = false);

        List<ExamStandardSubjectGroup> GetAllExamStandardSubjectGroups(ref int totalcount, 
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamStandardSubjectGroup(ExamStandardSubjectGroup ExamStandardSubjectGroup);

        void UpdateExamStandardSubjectGroup(ExamStandardSubjectGroup ExamStandardSubjectGroup);

        void DeleteExamStandardSubjectGroup(int ExamStandardSubjectGroupId = 0);

        #endregion

        #region ExamDateSheetSubjectSkill

        KSModel.Models.ExamDateSheetSubjectSkill GetExamDateSheetSubjectSkillById(int ExamDateSheetSubjectSkillId, bool IsTrack = false);

        List<KSModel.Models.ExamDateSheetSubjectSkill> GetAllExamDateSheetSubjectSkills(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamDateSheetSubjectSkill(KSModel.Models.ExamDateSheetSubjectSkill ExamDateSheetSubjectSkill);

       void UpdateExamDateSheetSubjectSkill(KSModel.Models.ExamDateSheetSubjectSkill ExamDateSheetSubjectSkill);

       void DeleteExamDateSheetSubjectSkill(int ExamDateSheetSubjectSkillId = 0);

        #endregion

        #region ExamAdmitCardNotes

        ExamAdmitCardNote GetExamAdmitCardNotesById(int ExamAdmitCardNotesId, bool IsTrack = false);

        List<ExamAdmitCardNote> GetAllExamAdmitCardNotess(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExamAdmitCardNotes(ExamAdmitCardNote ExamAdmitCardNotes);

        void UpdateExamAdmitCardNotes(ExamAdmitCardNote ExamAdmitCardNotes);

        void DeleteExamAdmitCardNotes(int ExamAdmitCardNotesId = 0);

        #endregion

        #region ExamTermGroup
        List<ExamTermGroup> GetAllExamTermGroups(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);
        #endregion  
    }
}
