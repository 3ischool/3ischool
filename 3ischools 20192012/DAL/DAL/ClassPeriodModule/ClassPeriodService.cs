﻿using DAL.DAL.Common;
using KSModel.Models;
using ViewModel.ViewModel.TimeTable;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DAL.DAL.ClassPeriodModule
{
    public partial class ClassPeriodService : IClassPeriodService
    {
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }



        private readonly IConnectionService _IConnectionService;

        public ClassPeriodService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods

        #region Period

        public KSModel.Models.Period GetPeriodById(int PeriodId, bool IsActive = false)
        {
           

            if (PeriodId == 0)
                throw new ArgumentNullException("Period");

            var Period = new Period();
            if (IsActive)
                Period = (from s in db.Periods where s.PeriodId == PeriodId select s).FirstOrDefault();
            else
                Period = (from s in db.Periods.AsNoTracking() where s.PeriodId == PeriodId select s).FirstOrDefault();

            return Period;
        }

        public List<KSModel.Models.Period> GetAllPeriods(ref int totalcount, string Period = "", int? PeriodIndex = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
           

            var query = db.Periods.ToList();
            if (!String.IsNullOrEmpty(Period))
                query = query.Where(p => p.Period1.ToLower().Contains(Period.ToLower())).ToList();
            if (PeriodIndex != null && PeriodIndex > 0)
                query = query.Where(p => p.PeriodIndex == PeriodIndex).ToList();

            totalcount = query.Count;
            var Periods = new PagedList<KSModel.Models.Period>(query, PageIndex, PageSize);
            return Periods;
        }
        //made for further changes when periods get mapped with class 
        //public List<KSModel.Models.Period> GetPeriodsByClass(ref int totalcount, string Period = "", int? PeriodIndex = null,
        //   int PageIndex = 0, int PageSize = int.MaxValue)
        //{
        //    var query = db.Periods.ToList();
        //    if (!String.IsNullOrEmpty(Period))
        //        query = query.Where(p => p.Period1.ToLower().Contains(Period.ToLower())).ToList();
        //    if (PeriodIndex != null && PeriodIndex > 0)
        //        query = query.Where(p => p.PeriodIndex == PeriodIndex).ToList();

        //    totalcount = query.Count;
        //    var Periods = new PagedList<KSModel.Models.Period>(query, PageIndex, PageSize);
        //    return Periods;
        //}
        public void InsertPeriod(KSModel.Models.Period Period)
        {
           

            if (Period == null)
                throw new ArgumentNullException("Period");

            db.Periods.Add(Period);
            db.SaveChanges();
        }

        public void UpdatePeriod(KSModel.Models.Period Period)
        {
           

            if (Period == null)
                throw new ArgumentNullException("Period");

            db.Entry(Period).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeletePeriod(int PeriodId = 0)
        {
           

            if (PeriodId == 0)
                throw new ArgumentNullException("Period");

            var Period = (from s in db.Periods where s.PeriodId == PeriodId select s).FirstOrDefault();
            db.Periods.Remove(Period);
            db.SaveChanges();
        }

        #endregion

        #region ClassPeriod

        public KSModel.Models.ClassPeriod GetClassPeriodById(int ClassPeriodId, bool IsActive = false)
        {
           

            if (ClassPeriodId == 0)
                throw new ArgumentNullException("ClassPeriod");

            var ClassPeriod = new ClassPeriod();
            if (IsActive)
                ClassPeriod = (from s in db.ClassPeriods where s.ClassPeriodId == ClassPeriodId select s).FirstOrDefault();
            else
                ClassPeriod = (from s in db.ClassPeriods.AsNoTracking() where s.ClassPeriodId == ClassPeriodId select s).FirstOrDefault();

            return ClassPeriod;
        }

        public List<KSModel.Models.ClassPeriod> GetAllClassPeriods(ref int totalcount, int? PeriodId = null, int? ClassSubjectId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
           

            var query = db.ClassPeriods.ToList();
            if (PeriodId != null && PeriodId > 0)
                query = query.Where(e => e.PeriodId == PeriodId).ToList();
            if (ClassSubjectId != null && ClassSubjectId > 0)
                query = query.Where(e => e.ClassSubjectId == ClassSubjectId).ToList();

            totalcount = query.Count;
            var ClassPeriods = new PagedList<KSModel.Models.ClassPeriod>(query, PageIndex, PageSize);
            return ClassPeriods;
        }

        public void InsertClassPeriod(KSModel.Models.ClassPeriod ClassPeriod)
        {
           

            if (ClassPeriod == null)
                throw new ArgumentNullException("ClassPeriod");

            db.ClassPeriods.Add(ClassPeriod);
            db.SaveChanges();
        }

        public void UpdateClassPeriod(KSModel.Models.ClassPeriod ClassPeriod)
        {
           

            if (ClassPeriod == null)
                throw new ArgumentNullException("ClassPeriod");

            db.Entry(ClassPeriod).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteClassPeriod(int ClassPeriodId = 0)
        {
           

            if (ClassPeriodId == 0)
                throw new ArgumentNullException("ClassPeriod");

            var ClassPeriod = (from s in db.ClassPeriods where s.ClassPeriodId == ClassPeriodId select s).FirstOrDefault();
            db.ClassPeriods.Remove(ClassPeriod);
            db.SaveChanges();
        }

        #endregion

        #region Time Table

        public TimeTable GetTimeTableById(int TimeTableId, bool IsActive = false)
        {
           

            if (TimeTableId == 0)
                throw new ArgumentNullException("TimeTable");

            var TimeTable = new TimeTable();
            if (IsActive)
                TimeTable = (from s in db.TimeTables where s.TimeTableId == TimeTableId select s).FirstOrDefault();
            else
                TimeTable = (from s in db.TimeTables.AsNoTracking() where s.TimeTableId == TimeTableId select s).FirstOrDefault();

            return TimeTable;
        }

        public List<TimeTable> GetAllTimeTables(ref int totalcount, DateTime? EffectiveFrom = null, DateTime? EffectiveTill = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
           

            var query = db.TimeTables.ToList();
            if (EffectiveFrom.HasValue)
                query = query.Where(e => e.EffectiveFrom <= EffectiveFrom).ToList();
            if (EffectiveTill.HasValue)
                query = query.Where(e => e.EffectiveTill >= EffectiveTill).ToList();

            totalcount = query.Count;
            var TimeTables = new PagedList<KSModel.Models.TimeTable>(query, PageIndex, PageSize);
            return TimeTables;
        }

        public void InsertTimeTable(TimeTable TimeTable)
        {
           

            if (TimeTable == null)
                throw new ArgumentNullException("TimeTable");

            db.TimeTables.Add(TimeTable);
            db.SaveChanges();
        }

        public void UpdateTimeTabled(TimeTable TimeTable)
        {
           

            if (TimeTable == null)
                throw new ArgumentNullException("TimeTable");

            db.Entry(TimeTable).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTimeTable(int TimeTableId = 0)
        {
           

            if (TimeTableId == 0)
                throw new ArgumentNullException("TimeTable");

            var TimeTable = (from s in db.TimeTables where s.TimeTableId == TimeTableId select s).FirstOrDefault();
            db.TimeTables.Remove(TimeTable);
            db.SaveChanges();
        }

        public IList<TimeTableGridModel> GetTimeTablegridViewBySP(int Classid, int[] VirtualClassId,int TimeTableId)
        {
           

            var virtaulclassstring = string.Join("", VirtualClassId);
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("Sp_TimeTableView", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    sqlComm.Parameters.AddWithValue("@Classid", Classid == null ? 0 : Classid);
                    sqlComm.Parameters.AddWithValue("@VirtualClassId", VirtualClassId.Count()>0 ?virtaulclassstring: null  );
                    sqlComm.Parameters.AddWithValue("@TimeTableId", TimeTableId == null ? 0 : TimeTableId);
                 
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<TimeTableGridModel> GetAllTimeTableGrid = new List<TimeTableGridModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                TimeTableGridModel TimeTableGridModel = new TimeTableGridModel();
                TimeTableGridModel.Class = Convert.ToString(dt.Rows[i]["Class"]);
                TimeTableGridModel.Period = Convert.ToString(dt.Rows[i]["Period"]);
                TimeTableGridModel.PeriodId = Convert.ToInt32(dt.Rows[i]["PeriodId"]);
                TimeTableGridModel.TeacherSubject = Convert.ToString(dt.Rows[i]["Skill"]) == "" ? Convert.ToString(dt.Rows[i]["Subject"])  : Convert.ToString(dt.Rows[i]["Subject"])+" " + Convert.ToString(dt.Rows[i]["Skill"]);
                TimeTableGridModel.SubjectId = Convert.ToString(dt.Rows[i]["SubjectId"]);
                TimeTableGridModel.SkillId = Convert.ToString(dt.Rows[i]["SkillId"]);
                TimeTableGridModel.StaffName = Convert.ToString(dt.Rows[i]["StaffName"]);
                TimeTableGridModel.Day = Convert.ToString(dt.Rows[i]["Day"]);
                TimeTableGridModel.DayId = Convert.ToInt32(dt.Rows[i]["DayId"]);
                TimeTableGridModel.ClassPeriodDetailId = Convert.ToInt32(dt.Rows[i]["ClassPeriodDetailId"]);
                TimeTableGridModel.ClassPeriodId = Convert.ToInt32(dt.Rows[i]["ClassPeriodId"]);

                GetAllTimeTableGrid.Add(TimeTableGridModel);
            }

            return GetAllTimeTableGrid;

        }

        #endregion

        #region ClassPeriodDetail

        public KSModel.Models.ClassPeriodDetail GetClassPeriodDetailById(int ClassPeriodDetailId, bool IsActive = false)
        {
           

            if (ClassPeriodDetailId == 0)
                throw new ArgumentNullException("ClassPeriodDetail");

            var ClassPeriodDetail = new ClassPeriodDetail();
            if(IsActive)
                ClassPeriodDetail = (from s in db.ClassPeriodDetails where s.ClassPeriodDetailId == ClassPeriodDetailId select s).FirstOrDefault();
            else
                ClassPeriodDetail = (from s in db.ClassPeriodDetails.AsNoTracking() where s.ClassPeriodDetailId == ClassPeriodDetailId select s).FirstOrDefault();

            return ClassPeriodDetail;
        }

        public List<KSModel.Models.ClassPeriodDetail> GetAllClassPeriodDetails(ref int totalcount, int? ClassPeriodId = null, int? TeacherId = null,
            int? TimeTableId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
           

            var query = db.ClassPeriodDetails.ToList();
            if (ClassPeriodId != null && ClassPeriodId > 0)
                query = query.Where(e => e.ClassPeriodId == ClassPeriodId).ToList();
            if (TeacherId != null && TeacherId > 0)
                query = query.Where(e => e.TeacherId == TeacherId).ToList();
            if (TimeTableId != null && TimeTableId > 0)
                query = query.Where(e => e.TimeTableId == TimeTableId).ToList();

            totalcount = query.Count;
            var ClassPeriodDetails = new PagedList<KSModel.Models.ClassPeriodDetail>(query, PageIndex, PageSize);
            return ClassPeriodDetails;
        }

        public void InsertClassPeriodDetail(KSModel.Models.ClassPeriodDetail ClassPeriodDetail)
        {
           

            if (ClassPeriodDetail == null)
                throw new ArgumentNullException("ClassPeriodDetail");

            db.ClassPeriodDetails.Add(ClassPeriodDetail);
            db.SaveChanges();
        }

        public void UpdateClassPeriodDetail(KSModel.Models.ClassPeriodDetail ClassPeriodDetail)
        {
           

            if (ClassPeriodDetail == null)
                throw new ArgumentNullException("ClassPeriodDetail");

            db.Entry(ClassPeriodDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteClassPeriodDetail(int ClassPeriodDetailId = 0)
        {
           

            if (ClassPeriodDetailId == 0)
                throw new ArgumentNullException("ClassPeriodDetail");

            var ClassPeriodDetail = (from s in db.ClassPeriodDetails where s.ClassPeriodDetailId == ClassPeriodDetailId select s).FirstOrDefault();
            db.ClassPeriodDetails.Remove(ClassPeriodDetail);
            db.SaveChanges();
        }

        #endregion

        #region Day Master

        public DayMaster GetDayMasterById(int DayMasterId, bool IsActive = false)
        {
           

            if (DayMasterId == 0)
                throw new ArgumentNullException("DayMaster");

            var DayMaster = new DayMaster();
            if (IsActive)
                DayMaster = (from s in db.DayMasters where s.DayId == DayMasterId select s).FirstOrDefault();
            else
                DayMaster = (from s in db.DayMasters.AsNoTracking() where s.DayId == DayMasterId select s).FirstOrDefault();

            return DayMaster;
        }

        public List<DayMaster> GetAllDayMasters(ref int totalcount, string Dayname = null, string Dayabr = null, 
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
           

            var query = db.DayMasters.ToList();
            if (!string.IsNullOrEmpty(Dayname))
                query = query.Where(e => e.DayName == Dayname).ToList();
            if (!string.IsNullOrEmpty(Dayabr))
                query = query.Where(e => e.DayAbr == Dayabr).ToList();

            totalcount = query.Count;
            var DayMasters = new PagedList<KSModel.Models.DayMaster>(query, PageIndex, PageSize);
            return DayMasters;
        }

        public void InsertDayMaster(DayMaster DayMaster)
        {
           

            if (DayMaster == null)
                throw new ArgumentNullException("DayMaster");

            db.DayMasters.Add(DayMaster);
            db.SaveChanges();
        }

        public void UpdateDayMaster(DayMaster DayMaster)
        {

           

            if (DayMaster == null)
                throw new ArgumentNullException("DayMaster");

            db.Entry(DayMaster).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteDayMaster(int DayMasterId = 0)
        {
           

            if (DayMasterId == 0)
                throw new ArgumentNullException("DayMaster");

            var DayMaster = (from s in db.DayMasters where s.DayId == DayMasterId select s).FirstOrDefault();
            db.DayMasters.Remove(DayMaster);
            db.SaveChanges();
        }

        #endregion

        #region Class Period Day

        public ClassPeriodDay GetClassPeriodDayById(int ClassPeriodDayId, bool IsActive = false)
        {
           

            if (ClassPeriodDayId == 0)
                throw new ArgumentNullException("ClassPeriodDay");

            var ClassPeriodDay = new ClassPeriodDay();
            if (IsActive)
                ClassPeriodDay = (from s in db.ClassPeriodDays where s.DayDetailId == ClassPeriodDayId select s).FirstOrDefault();
            else
                ClassPeriodDay = (from s in db.ClassPeriodDays.AsNoTracking() where s.DayDetailId == ClassPeriodDayId select s).FirstOrDefault();

            return ClassPeriodDay;
        }

        public List<ClassPeriodDay> GetAllClassPeriodDays(ref int totalcount, int? DayId = null, int? ClassPeriodDetailId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
           

            var query = db.ClassPeriodDays.ToList();
            if (DayId != null && DayId > 0)
                query = query.Where(e => e.DayId == DayId).ToList();
            if (ClassPeriodDetailId != null && ClassPeriodDetailId > 0)
                query = query.Where(e => e.ClassPeriodDetailId == ClassPeriodDetailId).ToList();

            totalcount = query.Count;
            var ClassPeriodDays = new PagedList<KSModel.Models.ClassPeriodDay>(query, PageIndex, PageSize);
            return ClassPeriodDays;
        }

        public void InsertClassPeriodDay(ClassPeriodDay ClassPeriodDay)
        {
           

            if (ClassPeriodDay == null)
                throw new ArgumentNullException("ClassPeriodDay");

            db.ClassPeriodDays.Add(ClassPeriodDay);
            db.SaveChanges();
        }

        public void UpdateClassPeriodDay(ClassPeriodDay ClassPeriodDay)
        {
           

            if (ClassPeriodDay == null)
                throw new ArgumentNullException("ClassPeriodDay");

            db.Entry(ClassPeriodDay).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteClassPeriodDay(int ClassPeriodDayId = 0)
        {
           

            if (ClassPeriodDayId == 0)
                throw new ArgumentNullException("ClassPeriodDay");

            var ClassPeriodDay = (from s in db.ClassPeriodDays where s.DayDetailId == ClassPeriodDayId select s).FirstOrDefault();
            db.ClassPeriodDays.Remove(ClassPeriodDay);
            db.SaveChanges();
        }

        #endregion

        #region TimeTableAdjustment

        public TimeTableAdjustment GetTimeTableAdjustmentById(int TimeTableAdjustmentId, bool IsActive = false)
        {
           

            if (TimeTableAdjustmentId == 0)
                throw new ArgumentNullException("TimeTableAdjustment");

            var TimeTableAdjustment = new TimeTableAdjustment();
            if (IsActive)
                TimeTableAdjustment = (from s in db.TimeTableAdjustments where s.TimeTableAdjustmentId == TimeTableAdjustmentId select s).FirstOrDefault();
            else
                TimeTableAdjustment = (from s in db.TimeTableAdjustments.AsNoTracking() where s.TimeTableAdjustmentId == TimeTableAdjustmentId select s).FirstOrDefault();

            return TimeTableAdjustment;
        }

        public List<TimeTableAdjustment> GetAllTimeTableAdjustments(ref int totalcount, int? ClassPeriodDetailId = null,
            int? ClassPeriodId = null, int? TeacherId = null, DateTime? Date = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
           

            var query = db.TimeTableAdjustments.ToList();
            //if (ClassPeriodDetailId != null && ClassPeriodDetailId > 0)
            //    query = query.Where(e => e.ClassPeriodDetailId == ClassPeriodDetailId).ToList();
            if (ClassPeriodId != null && ClassPeriodId > 0)
                query = query.Where(e => e.ClassPeriodId == ClassPeriodId).ToList();
            if (TeacherId != null && TeacherId > 0)
                query = query.Where(e => e.TeacherId == TeacherId).ToList();
            if (Date.HasValue)
                query = query.Where(e => e.AdjustmentDate == Date).ToList();

            totalcount = query.Count;
            var TimeTableAdjustments = new PagedList<KSModel.Models.TimeTableAdjustment>(query, PageIndex, PageSize);
            return TimeTableAdjustments;
        }

        public void InsertTimeTableAdjustment(TimeTableAdjustment TimeTableAdjustment)
        {
           

            if (TimeTableAdjustment == null)
                throw new ArgumentNullException("TimeTableAdjustment");

            db.TimeTableAdjustments.Add(TimeTableAdjustment);
            db.SaveChanges();
        }

        public void UpdateTimeTableAdjustment(TimeTableAdjustment TimeTableAdjustment)
        {
           

            if (TimeTableAdjustment == null)
                throw new ArgumentNullException("TimeTableAdjustment");

            db.Entry(TimeTableAdjustment).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTimeTableAdjustment(int TimeTableAdjustmentId = 0)
        {
           

            if (TimeTableAdjustmentId == 0)
                throw new ArgumentNullException("TimeTableAdjustment");

            var TimeTableAdjustment = (from s in db.TimeTableAdjustments where s.TimeTableAdjustmentId == TimeTableAdjustmentId select s).FirstOrDefault();
            db.TimeTableAdjustments.Remove(TimeTableAdjustment);
            db.SaveChanges();
        }

        #endregion


        #region Time Table Period Quota

        public TimeTablePeriodQuota GetTimeTablePeriodQuotaById(int TimeTablePeriodQuotaId, bool IsActive = false)
        {


            if (TimeTablePeriodQuotaId == 0)
                throw new ArgumentNullException("TimeTablePeriodQuota");

            var TimeTablePeriodQuota = new TimeTablePeriodQuota();
            if (IsActive)
                TimeTablePeriodQuota = (from s in db.TimeTablePeriodQuotas where s.TimeTablePeriodQuotaId == TimeTablePeriodQuotaId select s).FirstOrDefault();
            else
                TimeTablePeriodQuota = (from s in db.TimeTablePeriodQuotas.AsNoTracking() where s.TimeTablePeriodQuotaId == TimeTablePeriodQuotaId select s).FirstOrDefault();

            return TimeTablePeriodQuota;
        }

        public List<TimeTablePeriodQuota> GetAllTimeTablePeriodQuotas(ref int totalcount,int PageIndex = 0, int PageSize = int.MaxValue)
        {


            var query = db.TimeTablePeriodQuotas.ToList();
            totalcount = query.Count;
            var TimeTablePeriodQuotas = new PagedList<KSModel.Models.TimeTablePeriodQuota>(query, PageIndex, PageSize);
            return TimeTablePeriodQuotas;
        }

        public void InsertTimeTablePeriodQuota(TimeTablePeriodQuota TimeTablePeriodQuota)
        {


            if (TimeTablePeriodQuota == null)
                throw new ArgumentNullException("TimeTablePeriodQuota");

            db.TimeTablePeriodQuotas.Add(TimeTablePeriodQuota);
            db.SaveChanges();
        }

        public void UpdateTimeTablePeriodQuotad(TimeTablePeriodQuota TimeTablePeriodQuota)
        {


            if (TimeTablePeriodQuota == null)
                throw new ArgumentNullException("TimeTablePeriodQuota");

            db.Entry(TimeTablePeriodQuota).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTimeTablePeriodQuota(int TimeTablePeriodQuotaId = 0)
        {


            if (TimeTablePeriodQuotaId == 0)
                throw new ArgumentNullException("TimeTablePeriodQuota");

            var TimeTablePeriodQuota = (from s in db.TimeTablePeriodQuotas where s.TimeTablePeriodQuotaId == TimeTablePeriodQuotaId select s).FirstOrDefault();
            db.TimeTablePeriodQuotas.Remove(TimeTablePeriodQuota);
            db.SaveChanges();
        }


        public IList<StaffPeriodDetailModel> GetStaffPeriodDetailBySP()
        {


            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_GetStaffByTimeTable", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<StaffPeriodDetailModel> GetAllTimeTableStaffPeriodQuota = new List<StaffPeriodDetailModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                StaffPeriodDetailModel StaffPeriodDetailModel = new StaffPeriodDetailModel();
                StaffPeriodDetailModel.TimeTableId = Convert.ToInt32(dt.Rows[i]["TimeTableId"]);
                StaffPeriodDetailModel.StaffId = Convert.ToInt32(dt.Rows[i]["StaffId"]);
                StaffPeriodDetailModel.StaffName = Convert.ToString(dt.Rows[i]["Teacher"]);
                StaffPeriodDetailModel.Monday = Convert.ToInt32(dt.Rows[i]["Mon"]);
                StaffPeriodDetailModel.Tuesday = Convert.ToInt32(dt.Rows[i]["Tue"]);
                StaffPeriodDetailModel.Wednesday = Convert.ToInt32(dt.Rows[i]["Wed"]);
                StaffPeriodDetailModel.Thursday = Convert.ToInt32(dt.Rows[i]["Thu"]);
                StaffPeriodDetailModel.Friday = Convert.ToInt32(dt.Rows[i]["Fri"]);
                StaffPeriodDetailModel.Saturday = Convert.ToInt32(dt.Rows[i]["Sat"]);

                GetAllTimeTableStaffPeriodQuota.Add(StaffPeriodDetailModel);
            }

            return GetAllTimeTableStaffPeriodQuota;

        }

        #endregion
       

        #region Time Table Quota Detail

        public TimeTableQuotaDetail GetTimeTableQuotaDetailById(int TimeTableQuotaDetailId, bool IsActive = false)
        {


            if (TimeTableQuotaDetailId == 0)
                throw new ArgumentNullException("TimeTableQuotaDetail");

            var TimeTableQuotaDetail = new TimeTableQuotaDetail();
            if (IsActive)
                TimeTableQuotaDetail = (from s in db.TimeTableQuotaDetails where s.TimeTableQuotaDetailId == TimeTableQuotaDetailId select s).FirstOrDefault();
            else
                TimeTableQuotaDetail = (from s in db.TimeTableQuotaDetails.AsNoTracking() where s.TimeTableQuotaDetailId == TimeTableQuotaDetailId select s).FirstOrDefault();

            return TimeTableQuotaDetail;
        }

        public List<TimeTableQuotaDetail> GetAllTimeTableQuotaDetails(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {


            var query = db.TimeTableQuotaDetails.ToList();
            totalcount = query.Count;
            var TimeTableQuotaDetails = new PagedList<KSModel.Models.TimeTableQuotaDetail>(query, PageIndex, PageSize);
            return TimeTableQuotaDetails;
        }

        public void InsertTimeTableQuotaDetail(TimeTableQuotaDetail TimeTableQuotaDetail)
        {


            if (TimeTableQuotaDetail == null)
                throw new ArgumentNullException("TimeTableQuotaDetail");

            db.TimeTableQuotaDetails.Add(TimeTableQuotaDetail);
            db.SaveChanges();
        }

        public void UpdateTimeTableQuotaDetaild(TimeTableQuotaDetail TimeTableQuotaDetail)
        {


            if (TimeTableQuotaDetail == null)
                throw new ArgumentNullException("TimeTableQuotaDetail");

            db.Entry(TimeTableQuotaDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTimeTableQuotaDetail(int TimeTableQuotaDetailId = 0)
        {


            if (TimeTableQuotaDetailId == 0)
                throw new ArgumentNullException("TimeTableQuotaDetail");

            var TimeTableQuotaDetail = (from s in db.TimeTableQuotaDetails where s.TimeTableQuotaDetailId == TimeTableQuotaDetailId select s).FirstOrDefault();
            db.TimeTableQuotaDetails.Remove(TimeTableQuotaDetail);
            db.SaveChanges();
        }

        #endregion

        
        #region ClassTest

        public KSModel.Models.ClassTest GetClassTestById(int ClassTestId, bool IsActive = false)
        {


            if (ClassTestId == 0)
                throw new ArgumentNullException("ClassTest");

            var ClassTest = new ClassTest();
            if (IsActive)
                ClassTest = (from s in db.ClassTests where s.ClassTestId == ClassTestId select s).FirstOrDefault();
            else
                ClassTest = (from s in db.ClassTests.AsNoTracking() where s.ClassTestId == ClassTestId select s).FirstOrDefault();

            return ClassTest;
        }

        public List<KSModel.Models.ClassTest> GetAllClassTests(ref int totalcount, string ClassTest = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {


            var query = db.ClassTests.ToList();
            if (!String.IsNullOrEmpty(ClassTest))
                query = query.Where(p => p.TestTitle.ToLower().Contains(ClassTest.ToLower())).ToList();
          

            totalcount = query.Count;
            var ClassTests = new PagedList<KSModel.Models.ClassTest>(query, PageIndex, PageSize);
            return ClassTests;
        }
       
        public void InsertClassTest(KSModel.Models.ClassTest ClassTest)
        {


            if (ClassTest == null)
                throw new ArgumentNullException("ClassTest");

            db.ClassTests.Add(ClassTest);
            db.SaveChanges();
        }

        public void UpdateClassTest(KSModel.Models.ClassTest ClassTest)
        {


            if (ClassTest == null)
                throw new ArgumentNullException("ClassTest");

            db.Entry(ClassTest).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteClassTest(int ClassTestId = 0)
        {


            if (ClassTestId == 0)
                throw new ArgumentNullException("ClassTest");

            var ClassTest = (from s in db.ClassTests where s.ClassTestId == ClassTestId select s).FirstOrDefault();
            db.ClassTests.Remove(ClassTest);
            db.SaveChanges();
        }

        #endregion

        #endregion
    }
}