﻿using Common.Logging;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using KSModel.Models;
using Quartz;
using Quartz.Impl.Matchers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Web;
using System.Web.Configuration;

namespace DAL.DAL.Task
{
    [Quartz.DisallowConcurrentExecutionAttribute()]
    public class MyScheduler : IJob
    {
        public MyScheduler()
        {
        }

        public void Execute(IJobExecutionContext context)
        {
            ConnectionService ConnectionService = new ConnectionService();
            HttpContext ccontext = HttpContext.Current;
            string DataSource = ConnectionService.Connectionstring(Convert.ToString(ccontext.Session["SchoolDB"]));

            if (!string.IsNullOrEmpty(DataSource))
            {
                KS_ChildEntities db = new KS_ChildEntities(DataSource);
                try
                {
                    // transfer activity log from temp to main
                    ActivityLog activityLog = new ActivityLog();
                    var tempactivitylogs = db.TempActivityLogs.Take(20).ToList();
                    foreach (var row in tempactivitylogs)
                    {
                        activityLog = new ActivityLog();
                        activityLog.RecordId = row.RecordId;
                        activityLog.TableId = row.TableId;
                        db.ActivityLogs.Add(activityLog);
                        db.SaveChanges();

                        // transfer activity log detail from temp to main
                        ActivityLogDetail activityLogDetail = new ActivityLogDetail();
                        var tempactivitylogdetails = db.TempActivityLogDetails.FirstOrDefault();
                        if (tempactivitylogdetails != null)
                        {
                            activityLogDetail = new ActivityLogDetail();
                            activityLogDetail.ActivityLogId = activityLog.ActivityLogId;
                            activityLogDetail.ActivityLogTypeId = tempactivitylogdetails.ActivityLogTypeId;
                            activityLogDetail.LogDateTime = tempactivitylogdetails.LogDateTime;
                            activityLogDetail.UserAgent = tempactivitylogdetails.UserAgent;
                            activityLogDetail.UserId = tempactivitylogdetails.UserId;
                            activityLogDetail.UserIp = tempactivitylogdetails.UserIp;
                            activityLogDetail.LogDescription = tempactivitylogdetails.LogDescription;

                            db.ActivityLogDetails.Add(activityLogDetail);
                            db.SaveChanges();

                            db.Database.ExecuteSqlCommand("Delete from TempActivityLogDetail where ActivityLogDetailId = " + tempactivitylogdetails.ActivityLogDetailId);
                        }
                    }

                    if (tempactivitylogs.Count > 0)
                        db.Database.ExecuteSqlCommand("Delete top (20) from TempActivityLog");

                }
                catch (Exception ex)
                {
                    ErrorLog log = new ErrorLog();
                    log.CreatedOnUtc = DateTime.UtcNow;
                    log.FullMessage = ex.Source;
                    log.LogLevelId = Convert.ToInt32(DAL.ErrorLogModule.LogLevel.Error);
                    log.PageUrl = "DataTransferScheduler";
                    log.ReferrerUrl = "DataTransferScheduler";
                    log.ShortMessage = ex.Message;
                    log.UserId = null;
                    var ipaddress = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork);
                    log.IpAddress = ipaddress.ToString();

                    db.ErrorLogs.Add(log);
                    db.SaveChanges();
                }
            }
        }
    }
}