﻿using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DAL.DAL.Task
{
    public class QuartzInitializer : IQuartzInitializer
    {
        private readonly IJobFactory _jobFactory;

        public QuartzInitializer(IJobFactory jobFactory)
        {
            _jobFactory = jobFactory;
        }

        public void Start()
        {
            // construct a scheduler factory
            var time = 720;
            ISchedulerFactory schedFact = new StdSchedulerFactory();

            // get a scheduler
            IScheduler sched = schedFact.GetScheduler();
            sched.JobFactory = _jobFactory;

            // construct job info
            IJobDetail job = JobBuilder.Create<MyScheduler>().WithIdentity("job1", "group1").RequestRecovery().Build();
            ITrigger trigger = TriggerBuilder.Create().WithIdentity("trigger1", "group1")
                                .StartNow().WithSimpleSchedule(s => s.WithIntervalInSeconds(time).RepeatForever()).Build();

            sched.Start();
            sched.ScheduleJob(job, trigger);
        }
    }
}