﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.DAL.ErrorLogModule
{
    public enum LogLevel
    {
        Debug = 10,
        Information = 20,
        Warning = 30,
        Error = 40,
        Fatal = 50
    }
}