﻿using DAL.DAL.Common;
using DAL.DAL.UserModule;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.DAL.MessagingModule
{
    public class MessagingService : IMessagingService
    {
         public int count = 0;
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        //private readonly string SqlDataSource;
         private string DataSource
         {
             get
             {
                 var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                 return dtsource;
             }
         }

         private KS_ChildEntities Context;
         public KS_ChildEntities db
         {

             get
             {
                 if (Context == null)
                 {
                     Context = new KS_ChildEntities(DataSource);
                     return Context;
                 }
                 return Context;
             }
         }
         private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;
        private readonly IUserService _IUserService;

        public MessagingService(IConnectionService IConnectionService, IUserService IUserService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
            this._IUserService = IUserService;
        }

        #region MsgToken

        public MsgToken GetMsgTokenById(int MsgTokenId, bool IsTrack = false)
        {
            if (MsgTokenId == 0)
                throw new ArgumentNullException("MsgToken");

            var msgtoken = new MsgToken();
            if (IsTrack)
                msgtoken = (from s in db.MsgTokens where s.MsgTokenId == MsgTokenId select s).FirstOrDefault();
            else
                msgtoken = (from s in db.MsgTokens.AsNoTracking() where s.MsgTokenId == MsgTokenId select s).FirstOrDefault();
            return msgtoken;
        }

        public IList<MsgToken> GetAllEmailMsgTokenList(ref int totalcount, string TokenName = "", bool IsEmail = false, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MsgTokens.ToList();


            if (!String.IsNullOrEmpty(TokenName))
                query = query.Where(e => TokenName.ToLower()==e.TokenName.ToLower()).ToList();

            if (IsEmail)
                query = query.Where(e => e.IsEmail == true).ToList();
            else
                query = query.Where(e => e.IsEmail == false).ToList();

          

            query = query.OrderByDescending(M => M.MsgTokenId).ToList();
            totalcount = query.Count;
            var MsgToken = new PagedList<KSModel.Models.MsgToken>(query, PageIndex, PageSize);
            return MsgToken;
        }

        public IList<MsgToken> GetAllSMSMsgTokenList(ref int totalcount, string TokenName = "", bool IsSMS = false, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MsgTokens.ToList();


            if (!String.IsNullOrEmpty(TokenName))
                query = query.Where(e => TokenName.ToLower() == e.TokenName.ToLower()).ToList();


            if (IsSMS)
                query = query.Where(e => e.IsSMS == true).ToList();
            else
                query = query.Where(e => e.IsSMS == false).ToList();

            query = query.OrderByDescending(M => M.MsgTokenId).ToList();
            totalcount = query.Count;
            var MsgToken = new PagedList<KSModel.Models.MsgToken>(query, PageIndex, PageSize);
            return MsgToken;
        }

        public void InsertMsgToken(MsgToken MsgToken)
        {
            if (MsgToken == null)
                throw new ArgumentNullException("MsgToken");

            db.MsgTokens.Add(MsgToken);
            db.SaveChanges();
        }

        public void UpdateMsgToken(MsgToken MsgToken)
        {
            if (MsgToken == null)
                throw new ArgumentNullException("MsgToken");

            db.Entry(MsgToken).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteMsgToken(int MsgTokenId = 0)
        {
            if (MsgTokenId == 0)
                throw new ArgumentNullException("MsgToken");

            var MsgToken = (from s in db.MsgTokens where s.MsgTokenId == MsgTokenId select s).FirstOrDefault();
            db.MsgTokens.Remove(MsgToken);
            db.SaveChanges();
        }
        #endregion

        #region MsgEmailTemplate

        public MsgEmailTemplate GetMsgEmailTemplateById(int MsgEmailTemplateId, bool IsTrack = false)
        {
            if (MsgEmailTemplateId == 0)
                throw new ArgumentNullException("MsgEmailTemplate");

            var msgEmailTemplate = new MsgEmailTemplate();
            if (IsTrack)
                msgEmailTemplate = (from s in db.MsgEmailTemplates where s.MsgEmailTemplateId == MsgEmailTemplateId select s).FirstOrDefault();
            else
                msgEmailTemplate = (from s in db.MsgEmailTemplates.AsNoTracking() where s.MsgEmailTemplateId == MsgEmailTemplateId select s).FirstOrDefault();
            return msgEmailTemplate;
        }

        public IList<MsgEmailTemplate> GetAllMsgEmailTemplateList(ref int totalcount, string Name = "", string BccEmailAddress = "", string CCEmailAddress = "",
            string Subject = "", string Body = "", bool IsActive = false, bool IsSendImmediately = false,
            int? DelayBeforeSend = null, int? DelayPeriodId = null, int? MsgDurationId = null,
            int? MsgFrequencyId = null, int? EmailAccountId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MsgEmailTemplates.ToList();


            if (!String.IsNullOrEmpty(Name))
                query = query.Where(e => Name.ToLower() == e.Name.ToLower()).ToList();

            if (!String.IsNullOrEmpty(BccEmailAddress))
                query = query.Where(e => BccEmailAddress.ToLower() == e.BccEmailAddress.ToLower()).ToList();

            if (!String.IsNullOrEmpty(CCEmailAddress))
                query = query.Where(e => CCEmailAddress.ToLower() == e.CCEmailAddress.ToLower()).ToList();

            if (!String.IsNullOrEmpty(Subject))
                query = query.Where(e => Subject.ToLower() == e.Subject.ToLower()).ToList();

            if (!String.IsNullOrEmpty(Body))
                query = query.Where(e => Body.ToLower() == e.Body.ToLower()).ToList();

            if (IsActive)
                query = query.Where(e => e.IsActive == IsActive).ToList();

            //if (IsSendImmediately)
            //    query = query.Where(e => e.IsSendImmediately == true).ToList();
            //else
            //    query = query.Where(e => e.IsSendImmediately == false).ToList();

            if (DelayBeforeSend != null && DelayBeforeSend > 0)
                query = query.Where(e => e.DelayBeforeSend == DelayBeforeSend).ToList();

            if (DelayPeriodId != null && DelayPeriodId > 0)
                query = query.Where(e => e.DelayPeriodId == DelayPeriodId).ToList();

            if (MsgDurationId != null && MsgDurationId > 0)
                query = query.Where(e => e.MsgDurationId == MsgDurationId).ToList();

            if (MsgFrequencyId != null && MsgFrequencyId > 0)
                query = query.Where(e => e.MsgFrequencyId == MsgFrequencyId).ToList();

            if (EmailAccountId != null && EmailAccountId > 0)
                query = query.Where(e => e.EmailAccountId == EmailAccountId).ToList();

            query = query.OrderByDescending(M => M.MsgEmailTemplateId).ToList();
            totalcount = query.Count;
            var MsgEmailTemplate = new PagedList<KSModel.Models.MsgEmailTemplate>(query, PageIndex, PageSize);
            return MsgEmailTemplate;
        }

        public void InsertMsgEmailTemplate(MsgEmailTemplate MsgEmailTemplate)
        {
            if (MsgEmailTemplate == null)
                throw new ArgumentNullException("MsgEmailTemplate");

            db.MsgEmailTemplates.Add(MsgEmailTemplate);
            db.SaveChanges();
        }

        public void UpdateMsgEmailTemplate(MsgEmailTemplate MsgEmailTemplate)
        {
            if (MsgEmailTemplate == null)
                throw new ArgumentNullException("MsgEmailTemplate");

            db.Entry(MsgEmailTemplate).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteMsgEmailTemplate(int MsgEmailTemplateId = 0)
        {
            if (MsgEmailTemplateId == 0)
                throw new ArgumentNullException("MsgEmailTemplate");

            var MsgEmailTemplate = (from s in db.MsgEmailTemplates where s.MsgEmailTemplateId == MsgEmailTemplateId select s).FirstOrDefault();
            db.MsgEmailTemplates.Remove(MsgEmailTemplate);
            db.SaveChanges();
        }
        #endregion

        #region MsgEmailAccount

        public MsgEmailAccount GetMsgEmailAccountById(int MsgSenderMailId, bool IsTrack = false)
        {
            if (MsgSenderMailId == 0)
                throw new ArgumentNullException("MsgEmailAccount");

            var msgEmailAccount = new MsgEmailAccount();
            if (IsTrack)
                msgEmailAccount = (from s in db.MsgEmailAccounts where s.MsgSenderMailId == MsgSenderMailId select s).FirstOrDefault();
            else
                msgEmailAccount = (from s in db.MsgEmailAccounts.AsNoTracking() where s.MsgSenderMailId == MsgSenderMailId select s).FirstOrDefault();
            return msgEmailAccount;
        }

        public IList<MsgEmailAccount> GetAllMsgEmailAccountList(ref int totalcount , string Email = "", string DisplayName = "",
            string Host = "", int? Port = null, string UserName = "", string Password = "", bool EnableSsl = false, bool UseDefaultCredentials = false,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MsgEmailAccounts.ToList();

            if (!String.IsNullOrEmpty(Email))
                query = query.Where(e => Email.ToLower() == e.Email.ToLower()).ToList();

            if (!String.IsNullOrEmpty(DisplayName))
                query = query.Where(e => DisplayName.ToLower() == e.DisplayName.ToLower()).ToList();

            if (!String.IsNullOrEmpty(Host))
                query = query.Where(e => Host.ToLower() == e.Host.ToLower()).ToList();

            if (Port != null && Port > 0)
                query = query.Where(e => e.Port == Port).ToList();

            if (!String.IsNullOrEmpty(UserName))
                query = query.Where(e => UserName.ToLower() == e.UserName.ToLower()).ToList();

            if (!String.IsNullOrEmpty(Password))
                query = query.Where(e => Password.ToLower() == e.Password.ToLower()).ToList();

            //if (EnableSsl)
            //    query = query.Where(e => e.EnableSsl == true).ToList();
            //else
            //    query = query.Where(e => e.EnableSsl == false).ToList();

            if (UseDefaultCredentials)
                query = query.Where(e => e.UseDefaultCredentials == true).ToList();
            else
                query = query.Where(e => e.UseDefaultCredentials == false).ToList();

            query = query.OrderByDescending(M => M.MsgSenderMailId).ToList();
            totalcount = query.Count;
            var MsgEmailAccount = new PagedList<KSModel.Models.MsgEmailAccount>(query, PageIndex, PageSize);
            return MsgEmailAccount;
        }

        public void InsertMsgEmailAccount(MsgEmailAccount MsgEmailAccount)
        {
            if (MsgEmailAccount == null)
                throw new ArgumentNullException("MsgEmailAccount");

            db.MsgEmailAccounts.Add(MsgEmailAccount);
            db.SaveChanges();
        }

        public void UpdateMsgEmailAccount(MsgEmailAccount MsgEmailAccount)
        {
            if (MsgEmailAccount == null)
                throw new ArgumentNullException("MsgEmailAccount");

            db.Entry(MsgEmailAccount).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteMsgEmailAccount(int MsgSenderMailId = 0)
        {
            if (MsgSenderMailId == 0)
                throw new ArgumentNullException("MsgEmailAccount");

            var MsgEmailAccount = (from s in db.MsgEmailAccounts where s.MsgSenderMailId == MsgSenderMailId select s).FirstOrDefault();
            db.MsgEmailAccounts.Remove(MsgEmailAccount);
            db.SaveChanges();
        }

        #endregion

        #region MsgSMSTemplate

        public MsgSMSTemplate GetMsgSMSTemplateById(int MsgSMSTemplateId, bool IsTrack = false)
        {
            if (MsgSMSTemplateId == 0)
                throw new ArgumentNullException("MsgSMSTemplate");

            var msgSMSTemplate = new MsgSMSTemplate();
            if (IsTrack)
                msgSMSTemplate = (from s in db.MsgSMSTemplates where s.MsgSMSTemplateId == MsgSMSTemplateId select s).FirstOrDefault();
            else
                msgSMSTemplate = (from s in db.MsgSMSTemplates.AsNoTracking() where s.MsgSMSTemplateId == MsgSMSTemplateId select s).FirstOrDefault();
            return msgSMSTemplate;
        }

        public IList<MsgSMSTemplate> GetAllMsgSMSTemplateList(ref int totalcount, string Name = "", string Body = "",
            bool IsActive = false, string SendTo = "", 
            int? MsgDurationId = null, int? MsgFrequencyId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MsgSMSTemplates.ToList();


            if (!String.IsNullOrEmpty(Name))
                query = query.Where(e => Name.ToLower() == e.Name.ToLower()).ToList();

            if (!String.IsNullOrEmpty(Body))
                query = query.Where(e => Body.ToLower() == e.Body.ToLower()).ToList();

            //if (IsActive)
            //    query = query.Where(e => e.IsActive == true).ToList();
            //else
            //    query = query.Where(e => e.IsActive == false).ToList();

            if (!String.IsNullOrEmpty(SendTo))
                query = query.Where(e => SendTo.ToLower() == e.SendTo.ToLower()).ToList();

            if (MsgDurationId != null && MsgDurationId > 0)
                query = query.Where(e => e.MsgDurationId == MsgDurationId).ToList();

            if (MsgFrequencyId != null && MsgFrequencyId > 0)
                query = query.Where(e => e.MsgFrequencyId == MsgFrequencyId).ToList();

            query = query.OrderByDescending(M => M.MsgSMSTemplateId).ToList();
            totalcount = query.Count;
            var MsgSMSTemplate = new PagedList<KSModel.Models.MsgSMSTemplate>(query, PageIndex, PageSize);
            return MsgSMSTemplate;
        }

        public void InsertMsgSMSTemplate(MsgSMSTemplate MsgSMSTemplate)
        {
            if (MsgSMSTemplate == null)
                throw new ArgumentNullException("MsgSMSTemplate");

            db.MsgSMSTemplates.Add(MsgSMSTemplate);
            db.SaveChanges();
        }

        public void UpdateMsgSMSTemplate(MsgSMSTemplate MsgSMSTemplate)
        {
            if (MsgSMSTemplate == null)
                throw new ArgumentNullException("MsgSMSTemplate");

            db.Entry(MsgSMSTemplate).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteMsgSMSTemplate(int MsgSMSTemplateId = 0)
        {
            if (MsgSMSTemplateId == 0)
                throw new ArgumentNullException("MsgSMSTemplate");

            var MsgSMSTemplate = (from s in db.MsgSMSTemplates where s.MsgSMSTemplateId == MsgSMSTemplateId select s).FirstOrDefault();
            db.MsgSMSTemplates.Remove(MsgSMSTemplate);
            db.SaveChanges();
        }

        #endregion

        #region MsgSMSAccount

        public MsgSMSAccount GetMsgSMSAccountById(int MsgSMSAccountId, bool IsTrack = false)
        {
            if (MsgSMSAccountId == 0)
                throw new ArgumentNullException("MsgSMSAccount");

            var msgSMSAccount = new MsgSMSAccount();
            if (IsTrack)
                msgSMSAccount = (from s in db.MsgSMSAccounts where s.MsgSMSAccountId == MsgSMSAccountId select s).FirstOrDefault();
            else
                msgSMSAccount = (from s in db.MsgSMSAccounts.AsNoTracking() where s.MsgSMSAccountId == MsgSMSAccountId select s).FirstOrDefault();
            return msgSMSAccount;
        }

        public IList<MsgSMSAccount> GetAllMsgSMSAccountList(ref int totalcount, string AuthKey = "", string SenderId = "", string SMSUri = "", string Route = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MsgSMSAccounts.ToList();


            if (!String.IsNullOrEmpty(AuthKey))
                query = query.Where(e => AuthKey.ToLower() == e.AuthKey.ToLower()).ToList();

            if (!String.IsNullOrEmpty(SenderId))
                query = query.Where(e => SenderId.ToLower() == e.SenderId.ToLower()).ToList();

            if (!String.IsNullOrEmpty(SMSUri))
                query = query.Where(e => SMSUri.ToLower() == e.SMSUri.ToLower()).ToList();

            if (!String.IsNullOrEmpty(Route))
                query = query.Where(e => Route.ToLower() == e.Route.ToLower()).ToList();

            query = query.OrderByDescending(M => M.MsgSMSAccountId).ToList();
            totalcount = query.Count;
            var MsgSMSAccount = new PagedList<KSModel.Models.MsgSMSAccount>(query, PageIndex, PageSize);
            return MsgSMSAccount;
        }

        public void InsertMsgSMSAccount(MsgSMSAccount MsgSMSAccount)
        {
            if (MsgSMSAccount == null)
                throw new ArgumentNullException("MsgSMSAccount");

            db.MsgSMSAccounts.Add(MsgSMSAccount);
            db.SaveChanges();
        }

        public void UpdateMsgSMSAccount(MsgSMSAccount MsgSMSAccount)
        {
            if (MsgSMSAccount == null)
                throw new ArgumentNullException("MsgSMSAccount");

            db.Entry(MsgSMSAccount).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteMsgSMSAccount(int MsgSMSAccountId = 0)
        {
            if (MsgSMSAccountId == 0)
                throw new ArgumentNullException("MsgSMSAccount");

            var MsgSMSAccount = (from s in db.MsgSMSAccounts where s.MsgSMSAccountId == MsgSMSAccountId select s).FirstOrDefault();
            db.MsgSMSAccounts.Remove(MsgSMSAccount);
            db.SaveChanges();
        }

        #endregion
    }
}