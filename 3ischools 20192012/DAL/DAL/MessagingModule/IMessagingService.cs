﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSModel.Models;

namespace DAL.DAL.MessagingModule
{
    public partial interface IMessagingService
    {
        #region MsgToken

        MsgToken GetMsgTokenById(int MsgTokenId, bool IsTrack = false);

        IList<MsgToken> GetAllEmailMsgTokenList(ref int totalcount, string TokenName = "", bool IsEmail = false,
            int PageIndex = 0, int PageSize = int.MaxValue);
        IList<MsgToken> GetAllSMSMsgTokenList(ref int totalcount, string TokenName = "",
            bool IsSMS = false, int PageIndex = 0, int PageSize = int.MaxValue);
        void InsertMsgToken(MsgToken MsgToken);

        void UpdateMsgToken(MsgToken MsgToken);

        void DeleteMsgToken(int MsgTokenId = 0);

        #endregion

        #region MsgEmailTemplate

        MsgEmailTemplate GetMsgEmailTemplateById(int MsgEmailTemplateId, bool IsTrack = false);

        IList<MsgEmailTemplate> GetAllMsgEmailTemplateList(ref int totalcount, string Name = "",
                                                            string BccEmailAddress = "", string CCEmailAddress = "", string Subject = "",
                                                            string Body = "", bool IsActive = false, bool IsSendImmediately = false,
                                                            int? DelayBeforeSend = null, int? DelayPeriodId = null, int? MsgDurationId = null,
                                                            int? MsgFrequencyId = null, int? EmailAccountId = null,
                                                            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertMsgEmailTemplate(MsgEmailTemplate MsgEmailTemplate);

        void UpdateMsgEmailTemplate(MsgEmailTemplate MsgEmailTemplate);

        void DeleteMsgEmailTemplate(int MsgEmailTemplateId = 0);

        #endregion

        #region MsgEmailAccount

        MsgEmailAccount GetMsgEmailAccountById(int MsgSenderMailId, bool IsTrack = false);

        IList<MsgEmailAccount> GetAllMsgEmailAccountList(ref int totalcount,
                                                            string Email = "", string DisplayName = "", string Host = "",
                                                            int? Port = null, string UserName = "", string Password = "",
                                                            bool EnableSsl = false, bool UseDefaultCredentials = false,
                                                            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertMsgEmailAccount(MsgEmailAccount MsgEmailAccount);

        void UpdateMsgEmailAccount(MsgEmailAccount MsgEmailAccount);

        void DeleteMsgEmailAccount(int MsgSenderMailId = 0);

        #endregion

        #region MsgSMSTemplate

        MsgSMSTemplate GetMsgSMSTemplateById(int MsgSMSTemplateId, bool IsTrack = false);

        IList<MsgSMSTemplate> GetAllMsgSMSTemplateList(ref int totalcount, string Name = "",
                                                            string Body = "", bool IsActive = false,
                                                            string SendTo = "", int? MsgDurationId = null, int? MsgFrequencyId = null,
                                                            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertMsgSMSTemplate(MsgSMSTemplate MsgSMSTemplate);

        void UpdateMsgSMSTemplate(MsgSMSTemplate MsgSMSTemplate);

        void DeleteMsgSMSTemplate(int MsgSMSTemplateId = 0);

        #endregion

        #region MsgSMSAccount

        MsgSMSAccount GetMsgSMSAccountById(int MsgSMSAccountId, bool IsTrack = false);

        IList<MsgSMSAccount> GetAllMsgSMSAccountList(ref int totalcount, string AuthKey = "",
                                                            string SenderId = "", string SMSUri = "", string Route = "",
                                                            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertMsgSMSAccount(MsgSMSAccount MsgSMSAccount);

        void UpdateMsgSMSAccount(MsgSMSAccount MsgSMSAccount);

        void DeleteMsgSMSAccount(int MsgSMSAccountId = 0);

        #endregion

    }
}
