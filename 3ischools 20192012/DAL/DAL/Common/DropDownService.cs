﻿//using BAL.BAL.Common;
//using BAL.BAL.Message;
//using BAL.BAL.Reporting;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.CertificateModule;
using DAL.DAL.ClassPeriodModule;
using DAL.DAL.CommunicationModule;
using DAL.DAL.ContactModule;
using DAL.DAL.DocumentModule;
using DAL.DAL.ExaminationModule;
using DAL.DAL.FeeModule;
using DAL.DAL.GuardianModule;
using DAL.DAL.HomeWorkModule;
using DAL.DAL.LateFeeModule;
using DAL.DAL.MessagingModule;
using DAL.DAL.MessengerModule;
using DAL.DAL.ReportingModule;
using DAL.DAL.Schedular;
using DAL.DAL.SettingService;
using DAL.DAL.StaffModule;
using DAL.DAL.StockModule;
using DAL.DAL.StudentModule;
using DAL.DAL.TransportModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.ViewModel.Common;
using ViewModel.ViewModel.Exam;
using ViewModel.ViewModel.Fee;
using ViewModel.ViewModel.Messaging;
using ViewModel.ViewModel.Reporting;

namespace DAL.DAL.Common
{
    public partial class DropDownService : IDropDownService
    {

        #region fields

        public int count = 0;
        public IList<SelectListItem> Dropdownlist { get; set; }
        public List<SelectListItem> Dropdownlst { get; set; }
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly ICommunicationService _ICommunicationService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStudentMasterService _IStudentMasterService;
        private readonly IStudentService _IStudentService;
        private readonly IGuardianService _IGuardianService;
        private readonly IContactService _IContactService;
        private readonly IDocumentService _IDocumentService;
        private readonly IFeeService _IFeeService;
        private readonly IStudentAddOnService _IStudentAddOnService;
        private readonly ISubjectService _ISubjectService;
        private readonly ITransportService _ITransportService;
        private readonly IBusService _IBusService;
        private readonly ILateFeeService _ILateFeeService;
        private readonly IStaffService _IStaffService;
        private readonly IMessengerService _IMessengerService;
        private readonly IExamService _IExamService;
        private readonly ISchedularService _ISchedularService;
        private readonly ICertificateService _ICertificateService;
        private readonly IClassPeriodService _IClassPeriodService;
        private readonly IModuleVersionService _IModuleVersionService;
        private readonly IUserManagementService _IUserManagementService;
        private readonly IReportService _IReportService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IMessagingService _IMessagingService;
        private readonly IHomeWorkService _IHomeWorkService;
        private readonly IStockService _IStockService;
        private readonly ICommonMethodService _ICommonMethodService;
        private readonly ICustomEncryption _ICustomEncryption;
        
        #endregion

        #region ctor

        public DropDownService(IAddressMasterService IAddressMasterService,
            ICommunicationService ICommunicationService,
            IActivityLogMasterService IActivityLogMasterService,
            ICatalogMasterService ICatalogMasterService,
            IStudentMasterService IStudentMasterService,
            IGuardianService IGuardianService,
            IContactService IContactService,
            IDocumentService IDocumentService,
            IFeeService IFeeService,
            IStudentAddOnService IStudentAddOnService,
            ISubjectService ISubjectService,
            ITransportService ITransportService,
            IBusService IBusService,
            ILateFeeService ILateFeeService,
            IStaffService IStaffService,
            IMessengerService IMessengerService,
            IExamService IExamService,
            ISchedularService ISchedularService,
            IClassPeriodService IClassPeriodService,
            ICertificateService ICertificateService,
            IModuleVersionService IModuleVersionService,
            IUserManagementService IUserManagementService,
            IReportService IReportService,
            ISchoolSettingService ISchoolSettingService,
            IMessagingService IMessagingService,
            IHomeWorkService IHomeWorkService, IStockService IStockService, ICommonMethodService ICommonMethodService,
            ICustomEncryption ICustomEncryption,
            IStudentService IStudentService)
        {
            this._IAddressMasterService = IAddressMasterService;
            this._ICommunicationService = ICommunicationService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IStudentMasterService = IStudentMasterService;
            this._IGuardianService = IGuardianService;
            this._IContactService = IContactService;
            this._IDocumentService = IDocumentService;
            this._IFeeService = IFeeService;
            this._IStudentAddOnService = IStudentAddOnService;
            this._ISubjectService = ISubjectService;
            this._ITransportService = ITransportService;
            this._IBusService = IBusService;
            this._ILateFeeService = ILateFeeService;
            this._IStaffService = IStaffService;
            this._IMessengerService = IMessengerService;
            this._IExamService = IExamService;
            this._ISchedularService = ISchedularService;
            this._IClassPeriodService = IClassPeriodService;
            this._ICertificateService = ICertificateService;
            this._IModuleVersionService = IModuleVersionService;
            this._IUserManagementService = IUserManagementService;
            this._IReportService = IReportService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._IMessagingService = IMessagingService;
            this._IHomeWorkService = IHomeWorkService;
            this._IStockService = IStockService;
            this._ICommonMethodService = ICommonMethodService;
            this._ICustomEncryption = ICustomEncryption;
            Dropdownlist = new List<SelectListItem>();
            Dropdownlst = new List<SelectListItem>();
            this._IStudentService = IStudentService;
        }

        #endregion

        #region methods

        #region Address module select list

        public IList<System.Web.Mvc.SelectListItem> AddressCountryList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IAddressMasterService.GetAllAddressCountries(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Country, Value = item.CountryId.ToString() });

            return Dropdownlist;
        }

        public IList<System.Web.Mvc.SelectListItem> AddressCityList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IAddressMasterService.GetAllAddressCitys(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.City, Value = item.CityId.ToString() });

            return Dropdownlist;
        }

        public IList<System.Web.Mvc.SelectListItem> AddressStateList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IAddressMasterService.GetAllAddressStates(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.State, Value = item.StateId.ToString() });

            return Dropdownlist;
        }

        public IList<System.Web.Mvc.SelectListItem> AddressTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IAddressMasterService.GetAllAddressTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.AddressType1, Value = item.AddressTypeId.ToString() });

            return Dropdownlist;
        }

        public IList<System.Web.Mvc.SelectListItem> ContactTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IAddressMasterService.GetAllContactTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ContactType1, Value = item.ContactTypeId.ToString() });

            return Dropdownlist;
        }

        #endregion

        #region Communication Module

        public IList<System.Web.Mvc.SelectListItem> CommunicationWayList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICommunicationService.GetAllCommunicationWays(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.CommunicationWay1, Value = item.CommunicationWayId.ToString() });

            return Dropdownlist;
        }

        public IList<System.Web.Mvc.SelectListItem> CommunicationTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICommunicationService.GetAllCommunicationTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.CommunicationType1, Value = item.CommunicationTypeId.ToString() });

            return Dropdownlist;
        }

        #endregion

        #region Activity Log Module

        public IList<System.Web.Mvc.SelectListItem> TableTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IActivityLogMasterService.GetAllTableTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.TableType1, Value = item.TableTypeId.ToString() });

            return Dropdownlist;
        }

        public IList<System.Web.Mvc.SelectListItem> ActivityLogTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IActivityLogMasterService.GetAllActivityLogTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ActivityLogType1, Value = item.ActivityLogTypeId.ToString() });

            return Dropdownlist;
        }

        #endregion

        #region Student module select list

        public IList<SelectListItem> StudentStatusList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IStudentMasterService.GetAllStudentStatus(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.StudentStatus, Value = item.StudentStatusId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> CustodyRightList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllCustodyRights(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.CustodyRight1, Value = item.CustodyRightId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> BloodGroupList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllBloodGroups(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.BloodGroup1, Value = item.BloodGroupId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> NationalityList()
        {
            Dropdownlist = new List<SelectListItem>();
            foreach (var item in _ICatalogMasterService.GetAllNationalitys(ref count))
            {
                if (item.Nationality1 == "Indian")
                    Dropdownlist.Add(new SelectListItem { Selected = true, Text = item.Nationality1, Value = item.NationalityId.ToString() });
                else
                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Nationality1, Value = item.NationalityId.ToString() });
            }
            return Dropdownlist;
        }

        public IList<SelectListItem> StudentCategoryList()
        {
            Dropdownlist = new List<SelectListItem>();
            foreach (var item in _IStudentMasterService.GetAllStudentCategorys(ref count))
            {
                if (item.StudentCategory1 == "General")
                    Dropdownlist.Add(new SelectListItem { Selected = true, Text = item.StudentCategory1, Value = item.StudentCategoryId.ToString() });
                else
                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.StudentCategory1, Value = item.StudentCategoryId.ToString() });
            }
            return Dropdownlist;
        }
        public IList<SelectListItem> GetHouseList()
        {
            Dropdownlist = new List<SelectListItem>();
            foreach (var item in _ICatalogMasterService.GetAllHouses(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.House1, Value = item.HouseId.ToString() });
            return Dropdownlist;
        }
        public IList<SelectListItem> ExtraActivityList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IStudentMasterService.GetAllExtraActivitys(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ExtraActivity1, Value = item.ExtraActivityId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> GenderList()
        {
            Dropdownlist = new List<SelectListItem>();
            foreach (var item in _ICatalogMasterService.GetAllGenders(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Gender1, Value = item.GenderId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> ReligionList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllReligions(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Religion1, Value = item.ReligionId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> RelationList()
        {
            Dropdownlist = new List<SelectListItem>();
            var relationtypeid = _IGuardianService.GetAllRelationTypes(ref count, "Sibling").FirstOrDefault();
            if (relationtypeid != null)
            {
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
                foreach (var item in _IGuardianService.GetAllRelations(ref count, RelationtypeId: relationtypeid.RelationTypeId))
                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Relation1, Value = item.RelationId.ToString() });
            }
            return Dropdownlist;
        }

        public IList<SelectListItem> BoardList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllBoards(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Board1, Value = item.BoardId.ToString() });

            return Dropdownlist;
        }

        #endregion

        #region Gurdian module select list

        public IList<SelectListItem> GuardianTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            var relationtype = _IGuardianService.GetAllRelationTypes(ref count, "Guardian").FirstOrDefault();
            if (relationtype != null)
            {
                foreach (var item in _IGuardianService.GetAllRelations(ref count, RelationtypeId: relationtype.RelationTypeId))
                {
                    if (item.Relation1 == "Father")
                        Dropdownlist.Add(new SelectListItem { Selected = true, Text = item.Relation1, Value = item.RelationId.ToString() });
                    else
                        Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Relation1, Value = item.RelationId.ToString() });
                }
            }
            return Dropdownlist;
        }

        public IList<SelectListItem> QualificationList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllQualifications(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Qualification1, Value = item.QualificationId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> OccupationList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllOccupations(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Occupation1, Value = item.OccupationId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> IncomeSlabList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IGuardianService.GetAllIncomeSlabs(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.SlabName, Value = item.SlabId.ToString() });

            return Dropdownlist;
        }
        #endregion

        #region Contact Info module select list

        public IList<SelectListItem> ContactInfoTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            foreach (var item in _IContactService.GetAllContactInfoTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ContactInfoType1, Value = item.ContactInfoTypeId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> StatusList()
        {
            Dropdownlist = new List<SelectListItem>();
            //Dropdownlist.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "Active", Value = "1" });
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "In-Active", Value = "0" });

            return Dropdownlist;
        }

        public IList<SelectListItem> FilterList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "Active", Value = "1" });
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "In-Active", Value = "0" });

            return Dropdownlist;
        }

        #endregion

        #region Document Info module select list

        public IList<SelectListItem> DocumentTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IDocumentService.GetAllDocumentTypes(ref count))
            {
                if (item.DocumentType1.ToLower() != "image")
                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.DocumentType1, Value = item.DocumentTypeId.ToString() });
            }
            return Dropdownlist;
        }
        public IList<SelectListItem> MandatoryDocumentTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "All", Value = "" });
            foreach (var item in _IDocumentService.GetAllDocumentTypes(ref count).Where(m=>m.IsMandatory==true))
            {
                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.DocumentType1, Value = item.DocumentTypeId.ToString() });
            }
            return Dropdownlist;
        }
        public IList<SelectListItem> StaffDocumentTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IStaffService.GetAllStaffDocumentTypes(ref count))
            {
                if (item.DocumentType.ToLower() != "image")
                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.DocumentType, Value = item.DocumentTypeId.ToString() });
            }
            return Dropdownlist;
        }

        #endregion

        #region Fee module select list

        public IList<SelectListItem> TrsTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IFeeService.GetAllTrsTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.TrsType1, Value = item.TrsTypeId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> FeeFrequencyList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IFeeService.GetAllFeeFrequencies(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.FeeFrequency1, Value = item.FeeFrequencyId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> FeeTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IFeeService.GetAllFeeTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.FeeType1, Value = item.FeeTypeId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> GroupFeeTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IFeeService.GetAllGroupFeeTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.FeeClassGroup.FeeClassGroup1 + "(" + item.FeeType.FeeType1 + ")", Value = item.GroupFeeTypeId.ToString() });

            return Dropdownlist;
        }

        public IList<GroupFeeTypeDropDownTable> GroupFeeTypeDropTableList()
        {
            var nDropdownlist = new List<GroupFeeTypeDropDownTable>();
            foreach (var item in _IFeeService.GetAllGroupFeeTypes(ref count))
                nDropdownlist.Add(new GroupFeeTypeDropDownTable { FeeClassGroup= item.FeeClassGroup.FeeClassGroup1 ,FeeType=  item.FeeType.FeeType1 ,GroupFeeType= item.FeeClassGroup.FeeClassGroup1 + "(" + item.FeeType.FeeType1 + ")",GroupFeeTypeId=item.GroupFeeTypeId.ToString() });

            return nDropdownlist;
        }

        public IList<SelectListItem> FeeClassGroupList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IFeeService.GetAllFeeClassGroups(ref count))
            {
                if (!string.IsNullOrEmpty(item.FeeClassGroup1) &&  item.FeeClassGroup1.Contains("All"))
                    continue;

                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.FeeClassGroup1, Value = item.FeeClassGroupId.ToString() });
            }

            return Dropdownlist;
        }

        public IList<SelectListItem> FeeHeadTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IFeeService.GetAllFeeHeadTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.FeeHeadType1, Value = item.FeeHeadTypeId.ToString() });

            return Dropdownlist;
        }


        public IList<SelectListItem> FeeHeadList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IFeeService.GetAllFeeHeads(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.FeeHead1, Value = item.FeeHeadId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> PaymentModeList()
        {
            Dropdownlist = new List<SelectListItem>();
            foreach (var item in _IFeeService.GetAllPaymentModes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.PaymentMode1, Value = item.PaymentModeId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> PaymentModeListwithselectitem()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "", Value = "0" });

            foreach (var item in _IFeeService.GetAllPaymentModes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.PaymentMode1, Value = item.PaymentModeId.ToString() });

            return Dropdownlist;
        }

       public IList<SelectListItem> AmounttypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = true, Text = "Amount", Value = "0" });
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "Percent", Value = "1" });
            return Dropdownlist;
        }

        public IList<SelectListItem> FeePeriodList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IFeeService.GetAllFeePeriods(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.FeePeriodDescription, Value = item.FeePeriodId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> RecieptTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IFeeService.GetAllReceiptTypes_Updated(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ReceiptType1, Value = item.ReceiptTypeId.ToString() });

            return Dropdownlist;
        }
        public IList<SelectListItem> RecieptTypeListWithAddon()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IFeeService.GetAllReceiptTypesWithAddons(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ReceiptType1, Value = item.ReceiptTypeId.ToString() });

            return Dropdownlist;
        }
        public IList<SelectListItem> RecieptTypeListForGroupFeehead()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IFeeService.GetAllReceiptTypesgroupFeehead(ref count, groupfeehead:true))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ReceiptType1, Value = item.ReceiptTypeId.ToString() });

            return Dropdownlist;
        }

        #endregion

        #region Student AddOn module select list

        public IList<SelectListItem> StudentFeeAddOnTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IStudentAddOnService.GetAllStudentFeeAddonTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.StudentFeeAddonType1, Value = item.StudentFeeAddonTypeId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> ConsessionTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IStudentAddOnService.GetAllConsessionTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ConsessionType1, Value = item.ConsessionTypeId.ToString() });

            return Dropdownlist;
        }
        public IList<SelectListItem> ConsessionTagList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IStudentAddOnService.GetAllConcessionTypeTags(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ConcessionTypeTag1, Value = item.ConcessionTypeTagId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> ConsessionList(int ReceiptTypeId = 0, bool GeneralConcession = true)
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IStudentAddOnService.GetAllConsessions(ref count, Status: true, ReceipttypeId: ReceiptTypeId, GeneralConcession: GeneralConcession))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Consession1, Value = item.ConsessionId.ToString() });

            return Dropdownlist;
        }

        #endregion

        #region Class module select list

        public IList<SelectListItem> StandardList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllStandardMasters(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Standard, Value = item.StandardId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> ClassList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllClassMasters(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Class, Value = item.ClassId.ToString() });

            return Dropdownlist;
        }
        public IList<SelectListItem> ClassListSessionWise(int SessionId=0)
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IStudentService.GetAllStudentClasss(ref count).Where(m => m.SessionId == SessionId).Select(n=>n.ClassMaster).Distinct().OrderBy(m=>m.SortingIndex).ToList())
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Class, Value = item.ClassId.ToString() });

            return Dropdownlist;
        }
        public IList<SelectListItem> ClassListFeeStructure()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            var classlist=_ICatalogMasterService.GetAllClassMasters(ref count).ToList();
            var FeeClassGroupIdList=_IFeeService.GetAllGroupFeeHeadDetails(ref count).
                Select(m=>m.GroupFeeHead.GroupFeeType.FeeClassGroup).Select(m=>m.FeeClassGroupId).ToList();
            var standardlisthavefeestucture=_IFeeService.GetAllFeeClassGroupDetails(ref count).Where(m=>FeeClassGroupIdList.Contains((int)m.FeeClassGroupId)).Select(m=>m.StandardId).ToList();
            classlist = classlist.Where(m => standardlisthavefeestucture.Contains(m.StandardId)).ToList();
            foreach (var item in classlist)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Class, Value = item.ClassId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> ClassListContainsAll()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "All", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllClassMasters(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Class, Value = item.ClassId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> SessionList(string Session = "", bool IsDefault = false, int SessionId = 0)
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllSessions(ref count, Session, Isdeafult: IsDefault, SessionId: SessionId))
            {
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Session1, Value = item.SessionId.ToString() });
            }
            return Dropdownlist;
        }


        public IList<SelectListItem> GetSessionListStartWithcurrent()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllNewSessions(ref count))
            {
                //if (item.Session1 == "2016-2017")
                //{
                //}
                //else
                //{
                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Session1, Value = item.SessionId.ToString() });
                
            }
            return Dropdownlist;
        }
        public IList<SelectListItem> GetSessionListpreviousthancurrent()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllNewSessions(ref count))
            {
                if (item.StartDate >= DateTime.Now)
                {

                }
                else
                {
                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Session1, Value = item.SessionId.ToString() });
                }
            }
            return Dropdownlist;
        } 
        public IList<SelectListItem> SectionList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllSections(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Section1, Value = item.SectionId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> HouseList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllHouses(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.House1, Value = item.HouseId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> PeriodList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IClassPeriodService.GetAllPeriods(ref count).OrderBy(p => p.PeriodIndex))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Period1, Value = item.PeriodId.ToString() });

            return Dropdownlist;
        }
        public IList<SelectListItem> GetMiscHeadList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IFeeService.GetAllMiscHeads(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.FeeHead1, Value = item.FeeHeadId.ToString() });

            return Dropdownlist;
        }
        #endregion

        #region Attendance module select list
        //dropdown in case of future attendance
        public IList<SelectListItem> FutureAttendanceStatusList(List<AttendanceStatu> AllStatus, DateTime date, Attendance attendance = null)
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            if (date > DateTime.UtcNow && attendance != null)
            {
                foreach (var item in AllStatus)
                {
                    if (item.AttendanceStatusId == attendance.AttendanceStatusId)
                    {
                        var optionitem = Dropdownlist.SingleOrDefault(x => x.Value == "");
                        Dropdownlist.Remove(optionitem);
                        Dropdownlist.Add(new SelectListItem { Selected = true, Text = item.AttendanceStatus, Value = item.AttendanceStatusId.ToString() });
                    }
                    else
                        Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.AttendanceStatus, Value = item.AttendanceStatusId.ToString() });
                }
            }
            else
            {
                foreach (var item in AllStatus)
                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.AttendanceStatus, Value = item.AttendanceStatusId.ToString() });
            }

            return Dropdownlist;
        }

        public IList<SelectListItem> AttendanceStatusList()
        {
            Dropdownlist = new List<SelectListItem>();
            //Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ISubjectService.GetAllAttendanceStatus(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.AttendanceStatus, Value = item.AttendanceStatusId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> StaffAttendanceStatusList()
        {
            Dropdownlist = new List<SelectListItem>();
            //Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IStaffService.GetStaffAllAttendanceStatus(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.AttendanceStatus, Value = item.AttendanceStatusId.ToString() });

            return Dropdownlist;
        }

        //make dropdown for the allattendance in case of a class incharge
        public IList<SelectListItem> AttendanceStatusAbbrList(List<AttendanceStatu> AllStatus, Attendance attendance = null, AttendancePeriodWise periodWiseAttendance = null, int? periodId = null)
        {
            int count = 0;
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "--", Value = "" });
            foreach (var item in AllStatus)
            {
                if (attendance != null && periodWiseAttendance == null)
                {
                    if (periodId > 0)
                    {
                        if (attendance.AttendanceStatu.AttendanceAbbr == "P")
                            Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.AttendanceAbbr, Value = item.AttendanceStatusId.ToString() });
                        else
                        {
                            count = ++count;
                            if (count == 1)
                                Dropdownlist.Add(new SelectListItem { Selected = false, Text = attendance.AttendanceStatu.AttendanceAbbr, Value = attendance.AttendanceStatusId.ToString() });
                        }
                    }
                    else
                    {
                        if (item.AttendanceStatusId == attendance.AttendanceStatusId)
                        {
                            var optionitem = Dropdownlist.SingleOrDefault(x => x.Value == "");
                            Dropdownlist.Remove(optionitem);
                            Dropdownlist.Add(new SelectListItem { Selected = true, Text = item.AttendanceAbbr, Value = item.AttendanceStatusId.ToString() });
                        }
                        else
                            Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.AttendanceAbbr, Value = item.AttendanceStatusId.ToString() });
                    }
                }
                else if (attendance != null && periodWiseAttendance != null)
                {
                    if (periodId > 0)
                    {
                        if (attendance.AttendanceStatu.AttendanceAbbr == "P")
                        {
                            if (item.AttendanceStatusId == periodWiseAttendance.AttendanceStatusId)
                            {
                                var optionitem = Dropdownlist.SingleOrDefault(x => x.Value == "");
                                Dropdownlist.Remove(optionitem);
                                Dropdownlist.Add(new SelectListItem { Selected = true, Text = item.AttendanceAbbr, Value = item.AttendanceStatusId.ToString() });
                            }
                            else
                                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.AttendanceAbbr, Value = item.AttendanceStatusId.ToString() });
                        }
                        else
                        {
                            count = ++count;
                            if (count == 1)
                            {
                                var optionitem = Dropdownlist.SingleOrDefault(x => x.Value == "");
                                Dropdownlist.Remove(optionitem);
                                if (attendance.AttendanceStatu.AttendanceAbbr == periodWiseAttendance.AttendanceStatu.AttendanceAbbr)
                                {
                                    Dropdownlist.Add(new SelectListItem { Selected = true, Text = periodWiseAttendance.AttendanceStatu.AttendanceAbbr, Value = periodWiseAttendance.AttendanceStatusId.ToString() });
                                }
                                else
                                {
                                    Dropdownlist.Add(new SelectListItem { Selected = true, Text = periodWiseAttendance.AttendanceStatu.AttendanceAbbr, Value = periodWiseAttendance.AttendanceStatusId.ToString() });
                                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = attendance.AttendanceStatu.AttendanceAbbr, Value = attendance.AttendanceStatusId.ToString() });
                                }
                            }
                        }
                    }
                    else
                    {
                        if (item.AttendanceStatusId == attendance.AttendanceStatusId)
                        {
                            var optionitem = Dropdownlist.SingleOrDefault(x => x.Value == "");
                            Dropdownlist.Remove(optionitem);
                            Dropdownlist.Add(new SelectListItem { Selected = true, Text = item.AttendanceAbbr, Value = item.AttendanceStatusId.ToString() });
                        }
                        else
                            Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.AttendanceAbbr, Value = item.AttendanceStatusId.ToString() });
                    }
                }
                else
                {
                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.AttendanceAbbr, Value = item.AttendanceStatusId.ToString() });
                }
            }
            return Dropdownlist;
        }

        #endregion

        #region Subject module select list

        public IList<SelectListItem> SubjectTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ISubjectService.GetAllSubjectTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.SubjectType1, Value = item.SubjectTypeId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> SubjectList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ISubjectService.GetAllSubjects(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Subject1, Value = item.SubjectId.ToString() });

            return Dropdownlist;
        }

        #endregion

        #region Teacher module select list

        public IList<SelectListItem> TeachersList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IStaffService.GetAllStaffs(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.FName + " " + item.LName, Value = item.StaffId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> TeacheringStaffList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IStaffService.GetAllStaffs(ref count).Where(x=>x.StaffType.IsTeacher == true && x.IsActive == true))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.FName + " " + item.LName, Value = item.StaffId.ToString() });

            return Dropdownlist;
        }

        #endregion

        #region Transport module select list

        public IList<SelectListItem> BusList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IBusService.GetAllBuses(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.BusNo, Value = item.BusId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> BusRouteList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IBusService.GetAllBusRoutes(ref count).Where(f=>(!string.IsNullOrEmpty(f.BusRoute1))).OrderBy(f=>f.BusRoute1))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.BusRoute1, Value = item.BusRouteId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> TptTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ITransportService.GetAllTptTypes(ref count).Where(f => (!string.IsNullOrEmpty(f.TptType1))).OrderBy(f=>f.TptType1))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.TptType1, Value = item.TptTypeId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> TptTypeDetailList(int TptTypeId)
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ITransportService.GetAllTptTypeDetails(ref count, TptTypeId: TptTypeId))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.TptName, Value = item.TptTypeDetailId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> VehicleList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ITransportService.GetAllVehicleTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.VehicleType1, Value = item.VehicleTypeId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> BoardingPointList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IBusService.GetAllBoardingPoints(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.BoardingPoint1, Value = item.BoardingPointId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> DriverList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IBusService.GetAllTptDrivers(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.DriverName, Value = item.TptDriverId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> BusAttendantList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _IBusService.GetAllTptAttendants(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.AttendantName, Value = item.TptAttendantId.ToString() });

            return Dropdownlist;
        }
        #endregion

        #region Late Fee module select list

        public IList<SelectListItem> LateFeeTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in _ILateFeeService.GetAllLateFeeTypes(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.LateFeeType1, Value = item.LateFeeTypeId.ToString() });

            return Dropdownlist;
        }

        #endregion

        #region Messenger Module select list

        public IList<SelectListItem> PriorityList()
        {
            Dropdownlist = new List<SelectListItem>();
            var prioritylist = _IMessengerService.GetAllMessagePriorityList(ref count);
            //Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "0" });
            foreach (var item in prioritylist)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Priority, Value = item.PriorityId.ToString() });

            return Dropdownlist;
        }
        public IList<CustomSelectListItem> LanguageList()
        {
           var Dropdownlist = new List<CustomSelectListItem>();
            var Languagelist = _ICatalogMasterService.GetAllLanguages(ref count);
            //Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "0" });
            foreach (var item in Languagelist)
                Dropdownlist.Add(new CustomSelectListItem { Selected = false, Data = item.LanguageAbbr, Text = item.Language1, Value = item.LanguageId.ToString() });

            return Dropdownlist;
        }
        public IList<SelectListItem> MsgPermissionsList(int? usertypeid = null)
        {
            if (usertypeid != null && usertypeid > 0)
            {
                Dropdownlist = new List<SelectListItem>();
                var msggroupid = _IAddressMasterService.GetContactTypeById((int)usertypeid).MsgGroupId;
                var msgpermissionidlist = _IMessengerService.GetAllMsgGroupPermissionList(ref count, MsgGroupId: msggroupid).Select(m => (int)m.MsgPermissionId).Distinct().ToArray();

                var msgpermission = _IMessengerService.GetAllMsgPermissionList(ref count, MsgPermissionId: msgpermissionidlist);

                Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "0" });
                foreach (var item in msgpermission)
                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.MsgPermission1, Value = item.MsgPermissionId.ToString() });
            }
            return Dropdownlist;
        }

        #endregion

        #region Examinantion Module select list

        public IList<SelectListItem> ExamTermList()
        {
            Dropdownlist = new List<SelectListItem>();
            var ExamTerms = _IExamService.GetAllExamTerms(ref count);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in ExamTerms)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ExamTerm1, Value = item.ExamTermId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> ExamActivityList()
        {
            Dropdownlist = new List<SelectListItem>();
            var ExamActivities = _IExamService.GetAllExamActivities(ref count);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in ExamActivities)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ExamActivity1, Value = item.ExamActivityId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> ExamActivityAbbrList()
        {
            Dropdownlist = new List<SelectListItem>();
            var ExamActivities = _IExamService.GetAllExamActivities(ref count);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in ExamActivities)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ExamActivityAbbr, Value = item.ExamActivityId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> ExamTermActivityList()
        {
            Dropdownlist = new List<SelectListItem>();
            var ExamTermActivities = _IExamService.GetAllExamTermActivities(ref count);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in ExamTermActivities)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ExamTerm.ExamTerm1 + "-" + item.ExamActivity.ExamActivity1, Value = item.ExamTermActivityId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> ExamTermActivityAbbrList()
        {
            Dropdownlist = new List<SelectListItem>();
            var ExamTermActivities = _IExamService.GetAllExamTermActivities(ref count);

            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in ExamTermActivities)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ExamTerm.ExamTerm1 + "-" + item.ExamActivity.ExamActivityAbbr, Value = item.ExamTermActivityId.ToString() });

            return Dropdownlist;
        }

        public IList<ExamActivitiesModel> ExamTermActivityAbbrListTable(int ExamTermGroupId=0,bool IsExamDateSheet=false)
        {
          var  nDropdownlist = new List<ExamActivitiesModel>();
            var ExamTermActivities = _IExamService.GetAllExamTermActivities(ref count);

            if (ExamTermGroupId > 0)
                ExamTermActivities = ExamTermActivities.Where(f => f.ExamTermId != null && f.ExamTerm.ExamTermGroupId != null && f.ExamTerm.ExamTermGroupId == ExamTermGroupId).ToList();
            if (IsExamDateSheet) {
                var ExamDateSheetTermActivities = _IExamService.GetAllExamDateSheets(ref count).Where(f => f.ExamTermActivityId != null).Select(f => f.ExamTermActivityId).ToArray();
                if (ExamDateSheetTermActivities != null && ExamDateSheetTermActivities.Count() > 0)
                    ExamTermActivities = ExamTermActivities.Where(f => ExamDateSheetTermActivities.Contains(f.ExamTermActivityId)).ToList();
                else
                    ExamTermActivities.Clear();
            }
            foreach (var item in ExamTermActivities)
                nDropdownlist.Add
                    (new ExamActivitiesModel{
                    ExamTermActivity = item.ExamTerm.ExamTerm1 + "-" + item.ExamActivity.ExamActivityAbbr,
                        filterExamTermActivity = item.ExamTerm.ExamTerm1 + " " + item.ExamActivity.ExamActivity1,
                        ExamTermActivityId = item.ExamTermActivityId.ToString(),
                        Examterm = item.ExamTerm.ExamTerm1,
                        ExamActivity = item.ExamActivity.ExamActivity1,
                    ClassGroup=item.ExamTerm.ExamTermGroup.ExamTermGroupName
                    });

            return nDropdownlist;
        }

        public IList<SelectListItem> ExamTermGroupList()
        {
          Dropdownlist = new List<SelectListItem>();
            var ExamTermActivities = _IExamService.GetAllExamTermActivities(ref count).Where(f => f.ExamTermId != null && f.ExamTerm.ExamTermGroupId != null).ToList();
            var ExamtermGroupIds = ExamTermActivities.Select(f => f.ExamTerm.ExamTermGroupId).Distinct().ToArray();

            IList<ExamTermGroup> AllExamTermGroups = new List<ExamTermGroup>();
            if (ExamtermGroupIds != null && ExamtermGroupIds.Count() > 0)
                AllExamTermGroups = _IExamService.GetAllExamTermGroups(ref count).Where(f => ExamtermGroupIds.Contains(f.ExamTermGroupId)).ToList();
            else
                AllExamTermGroups.Clear();

            Dropdownlist.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            foreach (var item in AllExamTermGroups)
                Dropdownlist.Add(new SelectListItem{Value=item.ExamTermGroupId.ToString(),Text= item.ExamTermGroupName});

            return Dropdownlist;
        }

        public IList<SelectListItem> ExamTimeSlotList()
        {
            Dropdownlist = new List<SelectListItem>();
            var ExamTimeSlots = _IExamService.GetAllExamTimeSlots(ref count);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in ExamTimeSlots)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ExamTimeSlot1, Value = item.ExamTimeSlotId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> GradeList()
        {
            Dropdownlist = new List<SelectListItem>();
            var ExamTimeSlots = _IExamService.GetAllGrades(ref count).OrderBy(m => m.Grade1);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in ExamTimeSlots)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Grade1, Value = item.GradeId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> GradePatternList()
        {
            Dropdownlist = new List<SelectListItem>();
            var GradePatterns = _IExamService.GetAllGradePatterns(ref count);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in GradePatterns)
            {
                if (item.BoardId > 0)
                {
                    var board = _ICatalogMasterService.GetBoardById((int)item.BoardId);
                    if (board.IsDefault != null && (bool)board.IsDefault)
                        Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.GradePattern1, Value = item.GradePatternId.ToString() });
                }
            }

            return Dropdownlist;
        }

        public IList<SelectListItem> MarksPatternList()
        {
            Dropdownlist = new List<SelectListItem>();
            var MarksPatterns = _IExamService.GetAllMarksPatterns(ref count);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in MarksPatterns)
            {
                var markspattern = item.MaxMarks + "-" + item.PassMarks;
                if (item.GradePattern != null)
                    markspattern += "(" + item.GradePattern.GradePattern1 + ")";

                Dropdownlist.Add(new SelectListItem { Selected = false, Text = markspattern, Value = item.MarksPatternId.ToString() });
            }
            return Dropdownlist;
        }

        #endregion

        #region Schedular Module select list

        public IList<SelectListItem> EventTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            foreach (var item in _ISchedularService.GetAllEventTypeList(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.EventType1, Value = item.EventTypeId.ToString() });

            return Dropdownlist;
        }
        #endregion

        #region Certificate Module select list

        public IList<SelectListItem> IssueDocTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            foreach (var item in _ICertificateService.GetAllIssueDocType(ref count))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.IssueDocType1, Value = item.IssueDocTypeId.ToString() });

            return Dropdownlist;
        }

        #endregion

        #region Module Version

        public IList<SelectListItem> ModuleList()
        {
            Dropdownlist = new List<SelectListItem>();
            var Modules = _IModuleVersionService.GetAllModules(ref count);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in Modules)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Module1, Value = item.ModuleId.ToString() });

            return Dropdownlist;
        }

        #endregion

        #region App Form

        public IList<SelectListItem> AppFormActionTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            var AppFormActionTypes = _IUserManagementService.GetAllAppFormActionTypes(ref count);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in AppFormActionTypes)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.FormActionType, Value = item.FormActionTypeId.ToString() });

            return Dropdownlist;
        }

        #endregion

        #region Reports

        public IList<SelectListItem> PagePositionList()
        {
            Dropdownlist = new List<SelectListItem>();
            var RptPageNoPositions = _IReportService.GetAllRptPageNoPositions(ref count);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in RptPageNoPositions)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.RptPageNoPosition1, Value = item.RptPageNoPositionId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> FilterControlList()
        {
            var Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            var RptFilterControlTypes = _IReportService.GetAllRptFilterControlTypes(ref count);
            foreach (var item in RptFilterControlTypes)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ControlDisplayName, Value = item.RptFilterControlTypeId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> SortingMethodList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist = Enum.GetValues(typeof(SortingMethod)).Cast<SortingMethod>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();

            return Dropdownlist;
        }

        public IList<SelectListItem> TableMasterList()
        {
            var Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            var Tables = _IActivityLogMasterService.GetAllTableMasters(ref count);
            foreach (var item in Tables)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.TableName, Value = item.TableId.ToString() });

            return Dropdownlist;
        }

        public IList<SelectListItem> TableMasterNameList()
        {
            var Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            var Tables = _IActivityLogMasterService.GetAllTableMasters(ref count);
            foreach (var item in Tables)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.TableName, Value = item.TableName });

            return Dropdownlist;
        }

        #endregion

        #region Messaging

        public IList<SelectListItem> DelayPeriodList()
        {
            Dropdownlist = new List<SelectListItem>();
            Dropdownlist = Enum.GetValues(typeof(MsgDelayPeriodEnum)).Cast<MsgDelayPeriodEnum>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();

            return Dropdownlist;
        }

        public IList<SelectListItem> MessageDurationList()
        {
            Dropdownlist = new List<SelectListItem>();
            var msgduration = Convert.ToInt32(_ISchoolSettingService.GetAttributeValue("MsgDurationDaysAgo"));

            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "0" });
            for (int i = 1; i <= msgduration; i++)
            {
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = i.ToString(), Value = i.ToString() });
            }

            return Dropdownlist;
        }

        public IList<SelectListItem> EmailAccountList()
        {
            Dropdownlist = new List<SelectListItem>();
            var emailAccountList = (_IMessagingService.GetAllMsgEmailAccountList(ref count, UseDefaultCredentials: false));
            var isdefaultemailaccount = _IMessagingService.GetAllMsgEmailAccountList(ref count, UseDefaultCredentials: true).FirstOrDefault();
            if (isdefaultemailaccount != null)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = isdefaultemailaccount.DisplayName, Value = isdefaultemailaccount.MsgSenderMailId.ToString() });

            foreach (var eal in emailAccountList)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = eal.DisplayName, Value = eal.MsgSenderMailId.ToString() });

            return Dropdownlist;
        }

        #endregion

        #region Staff select list
        public IList<SelectListItem> DepartmentList()
        {
            Dropdownlist = new List<SelectListItem>();
            var allDepartments = _IStaffService.GetAllDepartments(ref count);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in allDepartments)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Department1, Value = item.DepartmentId.ToString() });
            return Dropdownlist;
        }

        public IList<SelectListItem> DesignationList(int StaffTypeId = 0)
        {
            Dropdownlist = new List<SelectListItem>();
            var allDesignations = _IStaffService.GetAllDesignations(ref count, StafftypeId: StaffTypeId);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in allDesignations)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Designation1, Value = item.DesignationId.ToString() });
            return Dropdownlist;
        }
        public IList<SelectListItem> StaffTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            var allStaffTypes = _IStaffService.GetAllStaffTypes(ref count);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in allStaffTypes)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.StaffType1, Value = item.StaffTypeId.ToString() });
            return Dropdownlist;
        }

        public IList<SelectListItem> AllActiveStaffs(bool IsEmpCodeVisible = false)
        {
            Dropdownlist = new List<SelectListItem>();
            var allStaff = _IStaffService.GetAllStaffs(ref count,IsActive: true).OrderBy(x=>x.FName);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });

            foreach (var item in allStaff)
            {
                string StaffName = "";
                StaffName = _ICommonMethodService.TrimStaffName(item,StaffName);
                if (IsEmpCodeVisible)
                    StaffName = StaffName + " (" + item.EmpCode + ")";

                Dropdownlist.Add(new SelectListItem { Selected = false, Text = StaffName, 
                            Value = _ICustomEncryption.base64e(item.StaffId.ToString()) });
            }
            return Dropdownlist;
        }

        #endregion

        #region Home Work Type Select List
        public IList<SelectListItem> HomeWorkTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            var allHWTypes = _IHomeWorkService.GetAllHWTypes(ref count);
            foreach (var item in allHWTypes)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.HWType1, Value = item.HWTypeId.ToString() });
            return Dropdownlist;
        }
        #endregion

        #region Product Select List
        public List<SelectListItem> ProductGroupList()
        {
            Dropdownlst = new List<SelectListItem>();
            var allPG = _IStockService.GetAllProductGroup(ref count);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allPG)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.ProductGroup1, Value = item.ProductGroupId.ToString() });
            return Dropdownlst;
        }
        public List<SelectListItem> ProductTypeList()
        {
            Dropdownlst = new List<SelectListItem>();
            var allPT = _IStockService.GetAllProductType(ref count);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allPT)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.ProductType1, Value = item.ProductTypeId.ToString() });
            return Dropdownlst;
        }
        public List<SelectListItem> ProductUnitList()
        {
            Dropdownlst = new List<SelectListItem>();
            var allPG = _IStockService.GetAllUnit(ref count);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allPG)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.Unit1, Value = item.UnitId.ToString() });
            return Dropdownlst;
        }
        public List<SelectListItem> ProductAttributeList(int ProductId)
        {
            Dropdownlst = new List<SelectListItem>();
            var allPA = _IStockService.GetAllAttributes(ref count, ProductId);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allPA)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.ProductAttribute1, Value = item.ProductAttributeId.ToString() });
            return Dropdownlst;
        }
        public List<SelectListItem> ProductAttributeValueList(int ProductAtrributeId = 0)
        {
            Dropdownlst = new List<SelectListItem>();
            var allPA = _IStockService.GetAllAttributeValues(ref count, ProductAtrributeId);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allPA)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.ProductAttributeValue1, Value = item.ProductAttributeValueId.ToString() });
            return Dropdownlst;
        }
        #endregion

        #region"Package Select List"
        public List<SelectListItem> GetAllClasses()
        {
            Dropdownlst = new List<SelectListItem>();
            var allPG = _IStockService.GetAllClasses(ref count);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allPG)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.Class, Value = item.ClassId.ToString() });
            return Dropdownlst;
        }
        public List<SelectListItem> GetAllProducts()
        {
            Dropdownlst = new List<SelectListItem>();
            var allPG = _IStockService.GetAllProducts(ref count);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allPG)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.ProductName, Value = item.ProductId.ToString() });
            return Dropdownlst;
        }


        #endregion

        #region"Vouchers"
        public List<SelectListItem> ProductList(int groupid = 0, int producttypeid = 0)
        {
            Dropdownlst = new List<SelectListItem>();
            var allP = _IStockService.GetAllProducts(ref count, ProductGroupId: groupid, ProductTypeId: producttypeid);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allP)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.ProductName, Value = item.ProductId.ToString() });
            return Dropdownlst;
        }
        public List<SelectListItem> StoreList(int Userid)
        {
            Dropdownlst = new List<SelectListItem>();
            var allS = _IStockService.GetAllStores(ref count, UserId: Userid);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allS)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.StoreName, Value = item.StoreId.ToString() });
            return Dropdownlst;
        }
        public List<SelectListItem> VoucherTypeList()
        {
            Dropdownlst = new List<SelectListItem>();
            var allS = _IStockService.GetAllVoucherTypes();
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allS)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.TrsDoc1, Value = item.TrsDocId.ToString() });
            return Dropdownlst;
        }
        public List<SelectListItem> AccountTypeList()
        {
            Dropdownlst = new List<SelectListItem>();
            var allS = _IStockService.GetAllAccountTypes(ref count);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allS)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.AccountType, Value = item.AccountTypeId.ToString() });
            return Dropdownlst;
        }
        public List<SelectListItem> AccountList(int AccounTypeId)
        {
            Dropdownlst = new List<SelectListItem>();
            var allS = _IStockService.GetAllaccounts(ref count, AccounTypeId);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allS)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.Account, Value = item.AccountId.ToString() });
            return Dropdownlst;
        }
        public List<SelectListItem> PackageList()
        {
            Dropdownlst = new List<SelectListItem>();
            var allS = _IStockService.GetAllPackages(ref count);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allS)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.StockPackages, Value = item.StockPackagesId.ToString() });
            return Dropdownlst;
        }
        public List<SelectListItem> TaxTypeList()
        {
            Dropdownlst = new List<SelectListItem>();
            var allS = _IStockService.GetAllTaxtypes(ref count);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allS)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.TaxType1, Value = item.TaxTypeId.ToString() });
            return Dropdownlst;
        }
        #endregion


        #region FeeClassGroupType

        public IList<SelectListItem> FeeClassGroupTypeList()
        {
            Dropdownlist = new List<SelectListItem>();
            var FeeClassGroupType = _IFeeService.GetAllFeeClassGroupTypes(ref count);
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var item in FeeClassGroupType)
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.ClassGroupType, Value = item.FeeClassGroupTypeId.ToString() });

            return Dropdownlist;
        }

        #endregion

        //public class customselectList : SelectListItem
        //{
        //    public string Data { get; set; }
        //}

        #region"AttendancePunch"
        public IList<SelectListItem> DeviceTypeList()
        {
            Dropdownlst = new List<SelectListItem>();
            var allP = _IStudentAddOnService.GetDeviceTypeList(ref count);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in allP)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.AttendanceDevice, Value = item.AttendanceDeviceId.ToString() });
            return Dropdownlst;
        }
        #endregion
        #region"TriggerType"
        public IList<SelectListItem> TriggerTypeList()
        {
            Dropdownlst = new List<SelectListItem>();
            var TriggerType = _IMessengerService.GetAllTriggerTypeList(ref count);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in TriggerType)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.TriggerType1, Value = item.TriggerTypeId.ToString() });
            return Dropdownlst;
        }
        #endregion
        #region"TriggerMaster"
        public IList<SelectListItem> TriggerMasterList()
        {
            Dropdownlst = new List<SelectListItem>();
            var TriggerMaster = _IMessengerService.GetAllTriggerMasterList(ref count);
            Dropdownlst.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var item in TriggerMaster)
                Dropdownlst.Add(new SelectListItem { Selected = false, Text = item.TriggerName, Value = item.TriggerTypeId.ToString() });
            return Dropdownlst;
        }
        #endregion
        #endregion

        #region CommonLists
        public List<string> MonthsList(DateTime date1, DateTime date2)
        {
            var listOfMonths = new List<string>();
            if (date1.CompareTo(date2) == 1)
            {
                var temp = date2;
                date2 = date1;
                date1 = temp;
            }

            var mosSt = date1.Month;
            var mosEn = date2.Month;
            var yearSt = date1.Year;
            var yearEn = date2.Year;

            while (mosSt <= mosEn || yearSt < yearEn)
            {
                var temp = new DateTime(yearSt, mosSt, 1);
                listOfMonths.Add(temp.ToString("MMMM", CultureInfo.InvariantCulture));
                mosSt++;
                if (mosSt < 13) continue;
                yearSt++;
                mosSt = 1;
            }

            return listOfMonths;
        }

        public List<DateTime> GetAllDatesAndInitializeTickets(DateTime startingDate, DateTime endingDate, int month)
        {
            List<DateTime> allDates = new List<DateTime>();
            for (DateTime i = startingDate; i <= endingDate; i = i.AddDays(1))
            {
                if (i.Month == month)
                    allDates.Add(i);
            }
            return allDates.ToList();
        }

        public  List<int> GetDatesAndInitializeTickets(DateTime startingDate, DateTime endingDate)
        {
            List<int> allmonths = new List<int>();
            for (DateTime i = startingDate; i <= endingDate; i = i.AddDays(1))
            {

                allmonths.Add(i.Month);
            }
            return allmonths.Distinct().ToList();
        }

        public IList<DateTime> GetSundayCountBetweenDates(ref int Count, DateTime StartDate, DateTime EndDate)
        {
            IList<DateTime> SundayList = new List<DateTime>();

            TimeSpan diff = EndDate - StartDate;
            int days = diff.Days;
            for (var i = 0; i <= days; i++)
            {
                var testDate = StartDate.AddDays(i);
                switch (testDate.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        Count++;
                        SundayList.Add(testDate);
                        break;
                }
            }

            return SundayList;
        }

        public List<DateTime> GetMonthCountBetweenDates(DateTime StartDate, DateTime EndDate)
        {
            List<DateTime> MonthList = new List<DateTime>();
            int months = (EndDate.Year - StartDate.Year) * 12 + EndDate.Month - StartDate.Month;
            for (var i = 0; i <= months; i++)
            {
                var testmonth = StartDate.AddMonths(i);
                MonthList.Add(testmonth);
            }
            return MonthList;
        }
        #endregion
    }
}