﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DAL.Common
{
    public partial interface IConnectionService
    {
        string Connectionstring(string database, string password = "");

        string SqlConnectionstring(string datab, string password = "");
        void intializeConection(out string DataSource, out KS_ChildEntities db, out string SqlDataSource);
    }
}
