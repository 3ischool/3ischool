﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.ViewModel.Messaging;

namespace DAL.DAL.Message
{
    public interface IWorkflowMessageService
    {
        /// <summary>
        /// Sends notification message
        /// </summary>
        /// <param name="SenderInfo">SenderInfo instance</param>
        int SendEmailNotificationMessage(SenderInfo senderInfo, string TemplateName);
        string SendMessageNotification(string[] regID, string title = "", string message="", string Devicetype="",
            string collapseKey="");
        string SendMessageNotification();
    }
}
