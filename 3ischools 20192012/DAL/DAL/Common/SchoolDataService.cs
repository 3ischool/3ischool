﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KSModel.Models;
using System.Web.Helpers;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Drawing.Imaging;
namespace DAL.DAL.Common
{
    public class SchoolDataService : ISchoolDataService
    {

        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public SchoolDataService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }
        public List<KSModel.Models.SchoolData> GetSchoolDataDetail(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SchoolDatas.ToList();
            totalcount = query.Count;
            var SchoolData = new PagedList<KSModel.Models.SchoolData>(query, PageIndex, PageSize);
            return SchoolData;
        }

        public void InsertSchoolData(KSModel.Models.SchoolData SchoolData)
        {
            if (SchoolData == null)
                throw new ArgumentNullException("SchoolData");

            db.SchoolDatas.Add(SchoolData);
            db.SaveChanges();
        }

        public void UpdateSchoolData(KSModel.Models.SchoolData SchoolData)
        {
            if (SchoolData == null)
                throw new ArgumentNullException("SchoolData");

            db.Entry(SchoolData).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSchoolData(int SchoolDataId = 0)
        {
            if (SchoolDataId == 0)
                throw new ArgumentNullException("SchoolData");

            var SchoolData = (from s in db.SchoolDatas where s.SchooldDataId == SchoolDataId select s).FirstOrDefault();
            db.SchoolDatas.Remove(SchoolData);
            db.SaveChanges();
        }


        public System.Web.Helpers.WebImage ResizeImage(HttpPostedFileBase file, string ImageType = "", int height = 0,
            int width = 0,string ImageExtension = "")
        {
            WebImage img = new WebImage(file.InputStream);
            if (height > 0 && width > 0)
                img.Resize(width, height, true);
            else
                img.Resize(150, 60, true);

            return img;
        }

        //public Image ResizeImage(Image image, Size size, bool preserveAspectRatio = true)
        //{
        //    int newWidth;
        //    int newHeight;
        //    if (preserveAspectRatio)
        //    {
        //        int originalWidth = image.Width;
        //        int originalHeight = image.Height;
        //        float percentWidth = (float)size.Width / (float)originalWidth;
        //        float percentHeight = (float)size.Height / (float)originalHeight;
        //        float percent = percentHeight < percentWidth ? percentHeight : percentWidth;
        //        newWidth = (int)(originalWidth * percent);
        //        newHeight = (int)(originalHeight * percent);
        //    }
        //    else
        //    {
        //        newWidth = size.Width;
        //        newHeight = size.Height;
        //    }

        //    Image newImage = new Bitmap(newWidth, newHeight);

        //    using (Graphics graphicsHandle = Graphics.FromImage(newImage))
        //    {
        //        graphicsHandle.InterpolationMode = InterpolationMode.HighQualityBicubic;
        //        graphicsHandle.DrawImage(image, 0, 0, newWidth, newHeight);
        //    }

        //    return newImage;
        //}
    }
}