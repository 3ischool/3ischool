﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSModel.Models;

namespace DAL.DAL.Common
{
    public partial interface ITempOTP
    {
        void InsertTempOtp(temp_otp_detail temp_otp_detail);
    }
}
