﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Text;
using System.Net.Http;
using DAL.DAL.SMSAPIModule;
using System.Text.RegularExpressions;
using System.Web.Configuration;

namespace DAL.DAL.Common
{
    public class SMSSender : ISMSSender
    {
        #region fields

        private readonly ISMSAPIService _ISMSAPIService;

        #endregion

        #region ctor

        public SMSSender(ISMSAPIService SMSAPIService
         )
        {
            this._ISMSAPIService = SMSAPIService;
        }

        #endregion
        //public bool SendSMS(string mobileNumber, string message)
        //{
        //    //Your authentication key
        //    string authKey = "100067AUMuvZMzUX56752b81";
        //    //Multiple mobiles numbers separated by comma
        //    //string mobileNumber = "9999999";
        //    //Sender ID,While using route4 sender id should be 6 characters long.
        //    string senderId = "KSmart";
        //    //Your message to send, Add URL encoding here.
        //    message = HttpUtility.UrlEncode(message);
        //  //  string ApiString = "http://bulksms.bagful.net/api/sendhttp.php?authkey=%pass%&message=%msg%";
        //    string route = "4";
        //    //Prepare you post parameters
        //    StringBuilder sbPostData = new StringBuilder();
        //    sbPostData.AppendFormat("authkey={0}", authKey);
        //    sbPostData.AppendFormat("&mobiles={0}", mobileNumber);
        //    sbPostData.AppendFormat("&message={0}", message);
        //    sbPostData.AppendFormat("&sender={0}", senderId);
        //    sbPostData.AppendFormat("&route={0}", route);
        //    sbPostData.AppendFormat("&country={0}", "91");

        //    try
        //    {
        //        //Call Send SMS API
        //        string sendSMSUri = "http://bulksms.bagful.net/api/sendhttp.php";
        //        //Create HTTPWebrequest
        //        HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
        //        //Prepare and Add URL Encoded data
        //        UTF8Encoding encoding = new UTF8Encoding();
        //        byte[] data = encoding.GetBytes(sbPostData.ToString());
        //        //Specify post method
        //        httpWReq.Method = "POST";
        //        httpWReq.ContentType = "application/x-www-form-urlencoded";
        //        httpWReq.ContentLength = data.Length;
        //        using (Stream stream = httpWReq.GetRequestStream())
        //        {
        //            stream.Write(data, 0, data.Length);
        //        }
        //        //Get the response
        //        HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
        //        StreamReader reader = new StreamReader(response.GetResponseStream());
        //        string responseString = reader.ReadToEnd();

        //        //Close the response
        //        reader.Close();
        //        response.Close();

        //        return true;
        //    }
        //    catch (SystemException ex)
        //    {
        //        return false;
        //    }
        //}

        public bool SendSMS(string mobileNumber, string message, bool IsOTP, out string Errormessage, out string content)
        {

            message = HttpUtility.UrlEncode(message);
            //  string ApiString = "http://bulksms.bagful.net/api/sendhttp.php?authkey=%pass%&message=%msg%";
            var count = 0;
            var MainUrl = "";
            var SMSAPI = _ISMSAPIService.GetAllSMSAPIs(ref count).Where(m => m.IsActive == true).FirstOrDefault();
            if (SMSAPI != null)
            {
                MainUrl = SMSAPI.APIFullUrlString;
                var mobilenoslist = mobileNumber.Split(',').ToList<string>();
                var mobilenolistwithcountrycode = new List<string>();
                foreach (var item in mobilenoslist)
                {
                    if ((bool)SMSAPI.AddCountryCode)
                    {
                        mobilenolistwithcountrycode.Add(SMSAPI.CountryCode + item);
                    }
                    else
                    {
                        mobilenolistwithcountrycode.Add(item);
                    }
                }
                string messagepattern = @"%message%";
                string mobilepattern = @"%mobileno%";
                string replacemessage = message;
                string replacemobiles = String.Join(",", mobilenolistwithcountrycode);
                string result = Regex.Replace(MainUrl, messagepattern, replacemessage);
                MainUrl = Regex.Replace(result, mobilepattern, replacemobiles);
            }

            if (MainUrl != "")
            {
                try
                {
                    HttpClient client = new HttpClient();
                    client.Timeout = new TimeSpan(0, 10, 1);
                    //MainUrl = MainUrl; // url with parameter
                    client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync(MainUrl).Result;
                    if (SMSAPI.IsAPIWebHookApplicable != null && (bool)SMSAPI.IsAPIWebHookApplicable)
                    {
                        content = response.Content.ReadAsStringAsync().Result;
                    }
                    else
                    {
                        content = "";
                    }
                    Errormessage = "";
                    return true;
                }
                catch (SystemException ex)
                {
                    content = "";
                    Errormessage = "";
                    return false;
                }
            }
            else
            {
                if (IsOTP)
                {
                    //Your authentication key
                    string authKey = Convert.ToString(WebConfigurationManager.AppSettings["OTPAuthKey"]); //"100067AUMuvZMzUX56752b81";
                    //Multiple mobiles numbers separated by comma
                    //string mobileNumber = "9999999";
                    //Sender ID,While using route4 sender id should be 6 characters long.
                    string senderId = Convert.ToString(WebConfigurationManager.AppSettings["OTPSenderId"]); //"KSmart";
                    //Your message to send, Add URL encoding here.
                    message = HttpUtility.UrlEncode(message);
                    //  string ApiString = "http://bulksms.bagful.net/api/sendhttp.php?authkey=%pass%&message=%msg%";
                    string route = Convert.ToString(WebConfigurationManager.AppSettings["OTPRoute"]); //"4";
                    //Prepare you post parameters
                    StringBuilder sbPostData = new StringBuilder();
                    sbPostData.AppendFormat("authkey={0}", authKey);
                    sbPostData.AppendFormat("&mobiles={0}", "91" + mobileNumber);
                    sbPostData.AppendFormat("&message={0}", message);
                    sbPostData.AppendFormat("&sender={0}", senderId);
                    sbPostData.AppendFormat("&route={0}", route);
                    sbPostData.AppendFormat("&country={0}", "91");

                    try
                    {
                        //Call Send SMS API
                        string sendSMSUri = Convert.ToString(WebConfigurationManager.AppSettings["OTPSendUrl"]); //"http://bulksms.bagful.net/api/sendhttp.php";
                        //Create HTTPWebrequest
                        HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                        //Prepare and Add URL Encoded data
                        UTF8Encoding encoding = new UTF8Encoding();
                        byte[] data = encoding.GetBytes(sbPostData.ToString());
                        //Specify post method
                        httpWReq.Method = "POST";
                        httpWReq.ContentType = "application/x-www-form-urlencoded";
                        httpWReq.ContentLength = data.Length;
                        using (Stream stream = httpWReq.GetRequestStream())
                        {
                            stream.Write(data, 0, data.Length);
                        }
                        //Get the response
                        HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                        StreamReader reader = new StreamReader(response.GetResponseStream());
                        string responseString = reader.ReadToEnd();

                        //Close the response
                        reader.Close();
                        response.Close();
                        Errormessage = "";
                        content = "";
                        return true;
                    }
                    catch (SystemException ex)
                    {
                        Errormessage = "";
                        content = "";
                        return false;
                    }
                }
                else
                {
                    Errormessage = "Please Implement SMS Api";
                    content = "";
                    return false;
                }
            }



        }
    }
}