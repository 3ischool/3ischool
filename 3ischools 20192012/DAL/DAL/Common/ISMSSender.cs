﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DAL.Common
{
    public interface ISMSSender
    {
        bool SendSMS(string mobileNumber, string message, bool IsOTP, out string Errormessage, out string content);
    }
}
