﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using ViewModel.ViewModel.Common;
using ViewModel.ViewModel.Exam;
using ViewModel.ViewModel.Fee;

namespace DAL.DAL.Common
{
    public partial interface IDropDownService
    {
        #region Address module select list

        IList<SelectListItem> AddressCountryList();

        IList<SelectListItem> AddressCityList();

        IList<SelectListItem> AddressStateList();

        IList<SelectListItem> AddressTypeList();

        IList<SelectListItem> ContactTypeList();

        #endregion

        #region Communication Module Select List

        IList<SelectListItem> CommunicationWayList();

        IList<SelectListItem> CommunicationTypeList();

        #endregion

        #region Activity Log Module select list

        IList<SelectListItem> TableTypeList();

        IList<SelectListItem> ActivityLogTypeList();

        #endregion

        #region Student module select list
        IList<SelectListItem> ClassListContainsAll();

        IList<SelectListItem> StudentStatusList();

        IList<SelectListItem> CustodyRightList();

        IList<SelectListItem> BloodGroupList();

        IList<SelectListItem> NationalityList();

        IList<SelectListItem> StudentCategoryList();

        IList<SelectListItem> ExtraActivityList();

        IList<SelectListItem> GenderList();

        IList<SelectListItem> ReligionList();

        IList<SelectListItem> RelationList();

        IList<SelectListItem> BoardList();
        IList<SelectListItem> GetHouseList();
        #endregion

        #region Gurdian module select list

        IList<SelectListItem> GuardianTypeList();

        IList<SelectListItem> QualificationList();

        IList<SelectListItem> OccupationList();
        IList<SelectListItem> IncomeSlabList();

        #endregion

        #region Contact Info module select list

        IList<SelectListItem> ContactInfoTypeList();

        IList<SelectListItem> StatusList();

        IList<SelectListItem> FilterList();

        #endregion

        #region Document Info module select list

        IList<SelectListItem> DocumentTypeList();
        IList<SelectListItem> MandatoryDocumentTypeList();
        IList<SelectListItem> StaffDocumentTypeList();
        #endregion

        #region Fee module select list

        IList<SelectListItem> TrsTypeList();

        IList<SelectListItem> FeeFrequencyList();

        IList<SelectListItem> FeeTypeList();

        IList<SelectListItem> GroupFeeTypeList();
        IList<GroupFeeTypeDropDownTable> GroupFeeTypeDropTableList();

        IList<SelectListItem> FeeClassGroupList();

        IList<SelectListItem> FeeHeadTypeList();

        IList<SelectListItem> FeeHeadList();

        IList<SelectListItem> PaymentModeList();
        IList<SelectListItem> PaymentModeListwithselectitem();
        IList<SelectListItem> FeePeriodList();

        IList<SelectListItem> RecieptTypeList();

        IList<SelectListItem> RecieptTypeListWithAddon();

        IList<SelectListItem> RecieptTypeListForGroupFeehead();
        #endregion

        #region Student AddOn module select list

        IList<SelectListItem> StudentFeeAddOnTypeList();

        IList<SelectListItem> ConsessionTypeList();
        IList<SelectListItem> ConsessionTagList();
        IList<SelectListItem> ConsessionList(int ReceiptTypeId = 0, bool GeneralConcession=true);

        IList<SelectListItem> AmounttypeList();

        #endregion

        #region Attendance module select list
        IList<SelectListItem> AttendanceStatusList();
        IList<SelectListItem> StaffAttendanceStatusList();
        IList<SelectListItem> AttendanceStatusAbbrList(List<AttendanceStatu> AllStatus, Attendance attendance = null, AttendancePeriodWise periodWiseAttendance = null, int? periodId = null);
        IList<SelectListItem> FutureAttendanceStatusList(List<AttendanceStatu> AllStatus, DateTime date, Attendance attendance = null);
        #endregion

        #region Class module select list

        IList<SelectListItem> StandardList();

        IList<SelectListItem> ClassList();
        IList<SelectListItem> ClassListSessionWise(int SessionId = 0);
        IList<SelectListItem> ClassListFeeStructure();

        IList<SelectListItem> SessionList(string Session = "", bool IsDefault = false, int SessionId = 0);

        IList<SelectListItem> GetSessionListStartWithcurrent();

        IList<SelectListItem> GetSessionListpreviousthancurrent();

        IList<SelectListItem> SectionList();

        IList<SelectListItem> HouseList();

        IList<SelectListItem> PeriodList();

        IList<SelectListItem> GetMiscHeadList();


        #endregion

        #region Subject module select list

        IList<SelectListItem> SubjectTypeList();

        IList<SelectListItem> SubjectList();

        #endregion

        #region Teacher module select list

        IList<SelectListItem> TeachersList();

        IList<SelectListItem> TeacheringStaffList();

        #endregion

        #region Transport module select list

        IList<SelectListItem> BusList();

        IList<SelectListItem> BusRouteList();

        IList<SelectListItem> TptTypeList();

        IList<SelectListItem> TptTypeDetailList(int TptTypeId);

        IList<SelectListItem> VehicleList();

        IList<SelectListItem> BoardingPointList();

        IList<SelectListItem> DriverList();

        IList<SelectListItem> BusAttendantList();
        #endregion

        #region Late Fee module select list

        IList<SelectListItem> LateFeeTypeList();

        #endregion

        #region Messenger Module

        IList<SelectListItem> MsgPermissionsList(int? usertypeid = null);

        IList<SelectListItem> PriorityList();
        IList<CustomSelectListItem> LanguageList();
        #endregion

        #region Examinantion Module select list

        IList<SelectListItem> ExamTermList();

        IList<SelectListItem> ExamActivityList();

        IList<SelectListItem> ExamActivityAbbrList();

        IList<SelectListItem> ExamTermActivityList();

        IList<SelectListItem> ExamTermActivityAbbrList();

        IList<ExamActivitiesModel> ExamTermActivityAbbrListTable(int ExamTermGroupId = 0, bool IsExamDateSheet = false);
        IList<SelectListItem> ExamTermGroupList();
        

         IList<SelectListItem> ExamTimeSlotList();

        IList<SelectListItem> GradeList();

        IList<SelectListItem> GradePatternList();

        IList<SelectListItem> MarksPatternList();

        #endregion

        #region Schedular Module select list

        IList<SelectListItem> EventTypeList();

        #endregion

        #region Certificate Module select list

        IList<SelectListItem> IssueDocTypeList();

        #endregion

        #region Module Version

        IList<SelectListItem> ModuleList();

        #endregion

        #region App Form

        IList<SelectListItem> AppFormActionTypeList();

        #endregion

        #region Reports

        IList<SelectListItem> PagePositionList();

        IList<SelectListItem> FilterControlList();

        IList<SelectListItem> SortingMethodList();

        IList<SelectListItem> TableMasterList(); // value with Id

        IList<SelectListItem> TableMasterNameList(); // value with name

        #endregion

        #region Messaging

        IList<SelectListItem> DelayPeriodList();
        IList<SelectListItem> MessageDurationList();
        IList<SelectListItem> EmailAccountList();

        #endregion

        #region Staff select list
        IList<SelectListItem> DepartmentList();
        IList<SelectListItem> DesignationList(int StaffTypeId=0);
        IList<SelectListItem> StaffTypeList();
        IList<SelectListItem> AllActiveStaffs(bool IsEmpCodeVisible = false);
        #endregion

        #region Home Work type Select List
        IList<SelectListItem> HomeWorkTypeList();
        #endregion

        #region Product Select List
        List<SelectListItem> ProductGroupList();
        List<SelectListItem> ProductTypeList();
        List<SelectListItem> ProductUnitList();
        List<SelectListItem> ProductAttributeList(int ProductId);
        List<SelectListItem> ProductAttributeValueList(int ProductAtrributeId=0);
        #endregion

        #region"Package"
        List<SelectListItem> GetAllClasses();
        List<SelectListItem> GetAllProducts();
        #endregion

        #region"Vouchers"
        List<SelectListItem> ProductList(int groupid=0,int producttypeid=0);
        List<SelectListItem> StoreList(int Userid);
        List<SelectListItem> VoucherTypeList();
        List<SelectListItem> AccountTypeList();
        List<SelectListItem> AccountList(int AccounTypeId);
        List<SelectListItem> PackageList();
        List<SelectListItem> TaxTypeList();
        
        #endregion

        #region"UserType"
        //List<SelectListItem> ();
        #endregion

        #region "AttendancePunch"
        IList<SelectListItem> DeviceTypeList();
        #endregion

        #region CommonLists
        List<string> MonthsList(DateTime date1, DateTime date2);

        List<DateTime> GetAllDatesAndInitializeTickets(DateTime startingDate, DateTime endingDate, int month);

        List<int> GetDatesAndInitializeTickets(DateTime startingDate, DateTime endingDate);

        IList<DateTime> GetSundayCountBetweenDates(ref int Count, DateTime StartDate, DateTime EndDate);

        List<DateTime> GetMonthCountBetweenDates(DateTime StartDate, DateTime EndDate);
        #endregion

        #region trigger
        IList<SelectListItem> TriggerMasterList();
        IList<SelectListItem> TriggerTypeList();
        #endregion

        #region FeeClassGroupType

        IList<SelectListItem> FeeClassGroupTypeList();

        #endregion
    }
}
