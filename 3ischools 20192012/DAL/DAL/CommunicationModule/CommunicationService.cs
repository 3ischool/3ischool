﻿using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DAL.DAL.CommunicationModule
{
    public partial class CommunicationService : ICommunicationService
    {
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }


        private readonly IConnectionService _IConnectionService;

        public CommunicationService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region  Methods

        #region  CommunicationType

        public KSModel.Models.CommunicationType GetCommunicationTypeById(int CommunicationTypeId)
        {
           

            if (CommunicationTypeId == 0)
                throw new ArgumentNullException("CommunicationType");

            var CommunicationType = (from s in db.CommunicationTypes.AsNoTracking() where s.CommunicationTypeId == CommunicationTypeId select s).FirstOrDefault();
            return CommunicationType;
        }

        public List<KSModel.Models.CommunicationType> GetAllCommunicationTypes(ref int totalcount, string CommunicationType = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
           

            var query = db.CommunicationTypes.ToList();
            if (!String.IsNullOrEmpty(CommunicationType))
                query = query.Where(e => e.CommunicationType1.ToLower().Contains(CommunicationType.ToLower())).ToList();

            totalcount = query.Count;
            var CommunicationTypes = new PagedList<KSModel.Models.CommunicationType>(query, PageIndex, PageSize);
            return CommunicationTypes;
        }

        public void InsertCommunicationType(KSModel.Models.CommunicationType CommunicationType)
        {
           

            if (CommunicationType == null)
                throw new ArgumentNullException("CommunicationType");

            db.CommunicationTypes.Add(CommunicationType);
            db.SaveChanges();
        }

        public void UpdateCommunicationType(KSModel.Models.CommunicationType CommunicationType)
        {
           

            if (CommunicationType == null)
                throw new ArgumentNullException("CommunicationType");

            db.Entry(CommunicationType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteCommunicationType(int CommunicationTypeId = 0)
        {
           

            if (CommunicationTypeId == 0)
                throw new ArgumentNullException("CommunicationType");

            var communicationType = (from s in db.CommunicationTypes where s.CommunicationTypeId == CommunicationTypeId select s).FirstOrDefault();
            db.CommunicationTypes.Remove(communicationType);
            db.SaveChanges();
        }

        #endregion

        #region CommunicationWay

        public KSModel.Models.CommunicationWay GetCommunicationWayById(int CommunicationWayId)
        {
           

            if (CommunicationWayId == 0)
                throw new ArgumentNullException("CommunicationWay");

            var communicationWay = (from s in db.CommunicationWays.AsNoTracking() where s.CommunicationWayId == CommunicationWayId select s).FirstOrDefault();
            return communicationWay;
        }

        public List<KSModel.Models.CommunicationWay> GetAllCommunicationWays(ref int totalcount, string CommunicationWay = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
           

            var query = db.CommunicationWays.ToList();
            if (!String.IsNullOrEmpty(CommunicationWay))
                query = query.Where(e => e.CommunicationWay1.ToLower().Contains(CommunicationWay.ToLower())).ToList();

            totalcount = query.Count;
            var CommunicationWays = new PagedList<KSModel.Models.CommunicationWay>(query, PageIndex, PageSize);
            return CommunicationWays;
        }

        public void InsertCommunicationWay(KSModel.Models.CommunicationWay CommunicationWay)
        {
           

            if (CommunicationWay == null)
                throw new ArgumentNullException("CommunicationWay");

            db.CommunicationWays.Add(CommunicationWay);
            db.SaveChanges();
        }

        public void UpdateCommunicationWay(KSModel.Models.CommunicationWay CommunicationWay)
        {
           

            if (CommunicationWay == null)
                throw new ArgumentNullException("CommunicationWay");

            db.Entry(CommunicationWay).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteCommunicationWay(int CommunicationWayId = 0)
        {
           

            if (CommunicationWayId == 0)
                throw new ArgumentNullException("CommunicationWay");

            var communicationWay = (from s in db.CommunicationWays where s.CommunicationWayId == CommunicationWayId select s).FirstOrDefault();
            db.CommunicationWays.Remove(communicationWay);
            db.SaveChanges();
        }

        #endregion

        #region Communication

        public KSModel.Models.Communication GetCommunicationById(int CommunicationId)
        {
           

            if (CommunicationId == 0)
                throw new ArgumentNullException("Communication");

            var communication = (from s in db.Communications.AsNoTracking() where s.CommunicationId == CommunicationId select s).FirstOrDefault();
            return communication;
        }

        public List<KSModel.Models.Communication> GetAllCommunications(ref int totalcount, int? TypeId = null, int? wayId = null,
            int? contactpersonid = null, int? refid = null, DateTime? date = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
           

            var query = db.Communications.ToList();
            if (TypeId != null && TypeId > 0)
                query = query.Where(e => e.CommunicationTypeId == TypeId).ToList();
            if (wayId != null && wayId > 0)
                query = query.Where(e => e.CommunicationWayId == wayId).ToList();
            if (contactpersonid != null && contactpersonid > 0)
                query = query.Where(e => e.ContactPersionId == contactpersonid).ToList();
            if (refid != null && refid > 0)
                query = query.Where(e => e.CommunicationId == refid).ToList();
            if (date.HasValue)
                query = query.Where(e => e.CommunicationDate == date).ToList();

            totalcount = query.Count;
            var Communications = new PagedList<KSModel.Models.Communication>(query, PageIndex, PageSize);
            return Communications;
        }

        public void InsertCommunication(KSModel.Models.Communication Communication)
        {
           

            if (Communication == null)
                throw new ArgumentNullException("Communication");

            db.Communications.Add(Communication);
            db.SaveChanges();
        }

        public void UpdateCommunication(KSModel.Models.Communication Communication)
        {
           

            if (Communication == null)
                throw new ArgumentNullException("Communication");

            db.Entry(Communication).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteCommunication(int CommunicationId = 0)
        {
           

            if (CommunicationId == 0)
                throw new ArgumentNullException("Communication");

            var communication = (from s in db.Communications where s.CommunicationId == CommunicationId select s).FirstOrDefault();
            db.Communications.Remove(communication);
            db.SaveChanges();
        }

        #endregion

        #endregion

    }
}