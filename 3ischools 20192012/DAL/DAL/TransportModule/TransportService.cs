﻿using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DAL.DAL.TransportModule
{
    public partial class TransportService : ITransportService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }
        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public TransportService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region  Methods

        #region TptType

        public KSModel.Models.TptType GetTptTypeById(int TptTypeId, bool IsTrack = false)
        {
            if (TptTypeId == 0)
                throw new ArgumentNullException("TptType");

            var TptType = new TptType();
            if (IsTrack)
                TptType = (from s in db.TptTypes where s.TptTypeId == TptTypeId select s).FirstOrDefault();
            else
                TptType = (from s in db.TptTypes.AsNoTracking() where s.TptTypeId == TptTypeId select s).FirstOrDefault();

            return TptType;
        }

        public List<KSModel.Models.TptType> GetAllTptTypes(ref int totalcount, string TptType = "", int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.TptTypes.ToList();
            if (!String.IsNullOrEmpty(TptType))
                query = query.Where(e => e.TptType1.ToLower().Contains(TptType.ToLower())).ToList();

            totalcount = query.Count;
            var TptTypes = new PagedList<KSModel.Models.TptType>(query, PageIndex, PageSize);
            return TptTypes;
        }

        public void InsertTptType(KSModel.Models.TptType TptType)
        {
            if (TptType == null)
                throw new ArgumentNullException("TptType");

            db.TptTypes.Add(TptType);
            db.SaveChanges();
        }

        public void UpdateTptType(KSModel.Models.TptType TptType)
        {
            if (TptType == null)
                throw new ArgumentNullException("TptType");

            db.Entry(TptType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTptType(int TptTypeId = 0)
        {
            if (TptTypeId == 0)
                throw new ArgumentNullException("TptType");

            var TptType = (from s in db.TptTypes where s.TptTypeId == TptTypeId select s).FirstOrDefault();
            db.TptTypes.Remove(TptType);
            db.SaveChanges();
        }

        #endregion

        #region TptTypeDetail

        public TptTypeDetail GetTptTypeDetailById(int TptTypeDetailId, bool IsTrack = false)
        {
            if (TptTypeDetailId == 0)
                throw new ArgumentNullException("TptTypeDetail");

            var TptTypeDetail = new TptTypeDetail();
            if (IsTrack)
                TptTypeDetail = (from s in db.TptTypeDetails where s.TptTypeDetailId == TptTypeDetailId select s).FirstOrDefault();
            else
                TptTypeDetail = (from s in db.TptTypeDetails.AsNoTracking() where s.TptTypeDetailId == TptTypeDetailId select s).FirstOrDefault();

            return TptTypeDetail;
        }

        public List<TptTypeDetail> GetAllTptTypeDetails(ref int totalcount, int? TptTypeId = null, string TptName = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TptTypeDetails.ToList();
            if (!String.IsNullOrEmpty(TptName))
                query = query.Where(e => e.TptName.ToLower() == TptName.ToLower()).ToList();
            if (TptTypeId > 0)
                query = query.Where(t => t.TptTypeId == TptTypeId).ToList();

            totalcount = query.Count;
            var TptTypeDetails = new PagedList<KSModel.Models.TptTypeDetail>(query, PageIndex, PageSize);
            return TptTypeDetails;
        }

        public void InsertTptTypeDetail(TptTypeDetail TptTypeDetail)
        {
            if (TptTypeDetail == null)
                throw new ArgumentNullException("TptTypeDetail");

            db.TptTypeDetails.Add(TptTypeDetail);
            db.SaveChanges();
        }

        public void UpdateTptTypeDetail(TptTypeDetail TptTypeDetail)
        {
            if (TptTypeDetail == null)
                throw new ArgumentNullException("TptTypeDetail");

            db.Entry(TptTypeDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTptTypeDetail(int TptTypeDetailId = 0)
        {
            if (TptTypeDetailId == 0)
                throw new ArgumentNullException("TptTypeDetail");

            var TptTypeDetail = (from s in db.TptTypeDetails where s.TptTypeDetailId == TptTypeDetailId select s).FirstOrDefault();
            db.TptTypeDetails.Remove(TptTypeDetail);
            db.SaveChanges();
        }

        #endregion

        #region TptFacility

        public KSModel.Models.TptFacility GetTptFacilityById(int TptFacilityId, bool IsTrack = false)
        {
            if (TptFacilityId == 0)
                throw new ArgumentNullException("TptFacility");

            var TptFacility = new TptFacility();
            if (IsTrack)
                TptFacility = (from s in db.TptFacilities where s.TptFacilityId == TptFacilityId select s).FirstOrDefault();
            else
                TptFacility = (from s in db.TptFacilities.AsNoTracking() where s.TptFacilityId == TptFacilityId select s).FirstOrDefault();

            return TptFacility;
        }

        public List<TptFacility> GetAllTptFacilitys(ref int totalcount, int? TptTypeDetailId = null, int? BusRoutrId = null,
            int? BoardingPointId = null, int? VehicletypeId = null, string VehicleNo = "", bool? ParkingRequired = null,
            int? StudentId = null, bool? Status = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TptFacilities.ToList();
            if (TptTypeDetailId != null && TptTypeDetailId > 0)
                query = query.Where(e => e.TptTypeDetailId == TptTypeDetailId).ToList();
            if (BusRoutrId != null && BusRoutrId > 0)
                query = query.Where(e => e.BusRouteId == BusRoutrId).ToList();
            if (BoardingPointId != null && BoardingPointId > 0)
                query = query.Where(e => e.BoardingPointId == BoardingPointId).ToList();
            if (VehicletypeId != null && VehicletypeId > 0)
                query = query.Where(e => e.VehicleTypeId == VehicletypeId).ToList();
            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();
            if (!String.IsNullOrEmpty(VehicleNo))
                query = query.Where(e => e.VehicleNo.ToLower() == VehicleNo.ToLower()).ToList();
            if (ParkingRequired != null)
                query = query.Where(e => e.ParkingRequired == ParkingRequired).ToList();
            if (Status != null)
                query = query.Where(e => e.Status == Status).ToList();
            // order by date
            query = query.OrderByDescending(t => t.EffectiveDate).OrderByDescending(t => t.TptFacilityId).ToList();

            totalcount = query.Count;
            var TptFacilities = new PagedList<KSModel.Models.TptFacility>(query, PageIndex, PageSize);
            return TptFacilities;
        }

        public void InsertTptFacility(KSModel.Models.TptFacility TptFacility)
        {
            if (TptFacility == null)
                throw new ArgumentNullException("TptFacility");

            db.TptFacilities.Add(TptFacility);
            db.SaveChanges();
        }

        public void UpdateTptFacility(KSModel.Models.TptFacility TptFacility)
        {
            if (TptFacility == null)
                throw new ArgumentNullException("TptFacility");

            db.Entry(TptFacility).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTptFacility(int TptFacilityId = 0)
        {
            if (TptFacilityId == 0)
                throw new ArgumentNullException("TptFacility");

            var TptFacility = (from s in db.TptFacilities where s.TptFacilityId == TptFacilityId select s).FirstOrDefault();
            db.TptFacilities.Remove(TptFacility);
            db.SaveChanges();
        }

        #endregion

        #region VehicleType

        public KSModel.Models.VehicleType GetVehicleTypeById(int VehicleTypeId, bool IsTrack = false)
        {
            if (VehicleTypeId == 0)
                throw new ArgumentNullException("VehicleType");

            var VehicleType = new VehicleType();
            if (IsTrack)
                VehicleType = (from s in db.VehicleTypes where s.VehicleTypeId == VehicleTypeId select s).FirstOrDefault();
            else
                VehicleType = (from s in db.VehicleTypes.AsNoTracking() where s.VehicleTypeId == VehicleTypeId select s).FirstOrDefault();

            return VehicleType;
        }

        public List<KSModel.Models.VehicleType> GetAllVehicleTypes(ref int totalcount, string VehicleType = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.VehicleTypes.ToList();
            if (!String.IsNullOrEmpty(VehicleType))
                query = query.Where(e => e.VehicleType1.ToLower().Contains(VehicleType.ToLower())).ToList();

            totalcount = query.Count;
            var VehicleTypes = new PagedList<KSModel.Models.VehicleType>(query, PageIndex, PageSize);
            return VehicleTypes;
        }

        public void InsertVehicleType(KSModel.Models.VehicleType VehicleType)
        {
            if (VehicleType == null)
                throw new ArgumentNullException("VehicleType");

            db.VehicleTypes.Add(VehicleType);
            db.SaveChanges();
        }

        public void UpdateVehicleType(KSModel.Models.VehicleType VehicleType)
        {
            if (VehicleType == null)
                throw new ArgumentNullException("VehicleType");

            db.Entry(VehicleType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteVehicleType(int VehicleTypeId = 0)
        {
            if (VehicleTypeId == 0)
                throw new ArgumentNullException("VehicleType");

            var VehicleType = (from s in db.VehicleTypes where s.VehicleTypeId == VehicleTypeId select s).FirstOrDefault();
            db.VehicleTypes.Remove(VehicleType);
            db.SaveChanges();
        }

        #endregion

        #endregion


        #region OwnershipTpt

        public List<TptOwnerShip> GetAllTptOwnerShips(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TptOwnerShips.ToList();
            totalcount = query.Count;
            var OwnerShipTypes = new PagedList<KSModel.Models.TptOwnerShip>(query, PageIndex, PageSize);
            return OwnerShipTypes;
        }
        #endregion

    }
}