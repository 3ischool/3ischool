﻿using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace DAL.DAL.TransportModule
{
    public partial class BusService : IBusService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;
        //private readonly string SqlDataSource;

        public BusService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
        }

        #region  Methods

        #region Bus

        public KSModel.Models.Bus GetBusById(int BusId, bool IsTrack = false)
        {
            if (BusId == 0)
                throw new ArgumentNullException("Bus");

            var Bus = new Bus();
            if (IsTrack)
                Bus = (from s in db.Buses where s.BusId == BusId select s).FirstOrDefault();
            else
                Bus = (from s in db.Buses.AsNoTracking() where s.BusId == BusId select s).FirstOrDefault();

            return Bus;
        }

        public List<KSModel.Models.Bus> GetAllBuses(ref int totalcount, string BusNo = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Buses.ToList();
            if (!String.IsNullOrEmpty(BusNo))
                query = query.Where(e => e.BusNo.ToLower().Contains(BusNo.ToLower())).ToList();

            totalcount = query.Count;
            var Buses = new PagedList<KSModel.Models.Bus>(query, PageIndex, PageSize);
            return Buses;
        }

        public void InsertBus(KSModel.Models.Bus Bus)
        {
            if (Bus == null)
                throw new ArgumentNullException("Bus");

            db.Buses.Add(Bus);
            db.SaveChanges();
        }

        public void UpdateBus(KSModel.Models.Bus Bus)
        {
            if (Bus == null)
                throw new ArgumentNullException("Bus");

            db.Entry(Bus).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBus(int BusId = 0)
        {
            if (BusId == 0)
                throw new ArgumentNullException("Bus");

            var Bus = (from s in db.Buses where s.BusId == BusId select s).FirstOrDefault();
            db.Buses.Remove(Bus);
            db.SaveChanges();
        }

        #endregion

        #region BusRoute

        public KSModel.Models.BusRoute GetBusRouteById(int BusRouteId, bool IsTrack = false)
        {
            if (BusRouteId == 0)
                throw new ArgumentNullException("BusRoute");

            var BusRoute = new BusRoute();
            if (IsTrack)
                BusRoute = (from s in db.BusRoutes where s.BusRouteId == BusRouteId select s).FirstOrDefault();
            else
                BusRoute = (from s in db.BusRoutes.AsNoTracking() where s.BusRouteId == BusRouteId select s).FirstOrDefault();

            return BusRoute;
        }
        public List<KSModel.Models.BusRoute> GetBusRouteNoTracking()
        {
            var query = db.BusRoutes.ToList();

            var BusRoute = new List<BusRoute>();

            BusRoute = (from s in db.BusRoutes.AsNoTracking() select s).ToList();

            return BusRoute;
        }

        public List<KSModel.Models.BusRoute> GetAllBusRoutes(ref int totalcount, string BusRoute = "",
            string Routeno = "", bool? Routestatus = true, DateTime? EffectiveDate = null, int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.BusRoutes.ToList();
            if (!String.IsNullOrEmpty(BusRoute))
                query = query.Where(e => e.BusRoute1.ToLower().Contains(BusRoute.ToLower())).ToList();
            if (!String.IsNullOrEmpty(Routeno))
                query = query.Where(e => e.RouteNo.ToLower().Contains(Routeno.ToLower())).ToList();
            if (!(bool)Routestatus)
                query = query.Where(e => e.RouteStatus == Routestatus).ToList();
            //if(EffectiveDate.HasValue)
            //    query = query.Where(b=>b.E)

            totalcount = query.Count;
            var BusRoutes = new PagedList<KSModel.Models.BusRoute>(query, PageIndex, PageSize);
            return BusRoutes;
        }

        public void InsertBusRoute(KSModel.Models.BusRoute BusRoute)
        {
            if (BusRoute == null)
                throw new ArgumentNullException("BusRoute");

            db.BusRoutes.Add(BusRoute);
            db.SaveChanges();
        }

        public void UpdateBusRoute(KSModel.Models.BusRoute BusRoute)
        {
            if (BusRoute == null)
                throw new ArgumentNullException("BusRoute");

            db.Entry(BusRoute).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBusRoutes(int BusRouteId = 0)
        {
            if (BusRouteId == 0)
                throw new ArgumentNullException("BusRoute");

            var BusRoute = (from s in db.BusRoutes where s.BusRouteId == BusRouteId select s).FirstOrDefault();
            db.BusRoutes.Remove(BusRoute);
            db.SaveChanges();
        }

        #endregion

        #region BusRouteDetail

        public KSModel.Models.BusRouteDetail GetBusRouteDetailById(int BusRouteDetailId, bool IsTrack = false)
        {
            if (BusRouteDetailId == 0)
                throw new ArgumentNullException("BusRouteDetail");

            var BusRouteDetail = new BusRouteDetail();
            if (IsTrack)
                BusRouteDetail = (from s in db.BusRouteDetails where s.BusRouteDetailId == BusRouteDetailId select s).FirstOrDefault();
            else
                BusRouteDetail = (from s in db.BusRouteDetails.AsNoTracking() where s.BusRouteDetailId == BusRouteDetailId select s).FirstOrDefault();

            return BusRouteDetail;
        }

        public List<KSModel.Models.BusRouteDetail> GetAllBusRouteDetails(ref int totalcount, DateTime? EffectiveDate = null,
            int? BusRouteId = null, int? BoardingPointId = null, int? BoardingIndex = null, bool? status = null,
             DateTime? EndDate = null, decimal? Distance = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.BusRouteDetails.ToList();
            if (EffectiveDate.HasValue)
                query = query.Where(e => e.EffectiveDate == EffectiveDate).ToList();
            if (BusRouteId != null && BusRouteId > 0)
                query = query.Where(e => e.BusRouteId == BusRouteId).ToList();
            if (BoardingPointId != null && BoardingPointId > 0)
                query = query.Where(e => e.BoardingPointId == BoardingPointId).ToList();
            if (BoardingIndex != null && BoardingIndex > 0)
                query = query.Where(e => e.BoardingIndex == BoardingIndex).ToList();
            if (status != null)
                query = query.Where(e => e.Status == status).ToList();
            if (EndDate.HasValue)
                query = query.Where(e => e.EndDate == EndDate).ToList();
            if (Distance != null && Distance > 0)
                query = query.Where(e => e.Distance == Distance).ToList();

            totalcount = query.Count;
            var BusRouteDetails = new PagedList<KSModel.Models.BusRouteDetail>(query, PageIndex, PageSize);
            return BusRouteDetails;
        }

        public void InsertBusRouteDetail(KSModel.Models.BusRouteDetail BusRouteDetail)
        {
            if (BusRouteDetail == null)
                throw new ArgumentNullException("BusRouteDetail");

            db.BusRouteDetails.Add(BusRouteDetail);
            db.SaveChanges();
        }

        public void UpdateBusRouteDetail(KSModel.Models.BusRouteDetail BusRouteDetail)
        {
            if (BusRouteDetail == null)
                throw new ArgumentNullException("BusRouteDetail");

            db.Entry(BusRouteDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBusRouteDetail(int BusRouteDetailId = 0)
        {
            if (BusRouteDetailId == 0)
                throw new ArgumentNullException("BusRouteDetail");

            var BusRouteDetail = (from s in db.BusRouteDetails where s.BusRouteDetailId == BusRouteDetailId select s).FirstOrDefault();
            db.BusRouteDetails.Remove(BusRouteDetail);
            db.SaveChanges();
        }

        #endregion

        #region TptRouteDetail

        public TptRouteDetail GetTptRouteDetailById(int TptRouteDetailId, bool IsTrack = false)
        {
            if (TptRouteDetailId == 0)
                throw new ArgumentNullException("TptRouteDetail");

            var TptRouteDetail = new TptRouteDetail();
            if (IsTrack)
                TptRouteDetail = (from s in db.TptRouteDetails where s.TptRouteDetailId == TptRouteDetailId select s).FirstOrDefault();
            else
                TptRouteDetail = (from s in db.TptRouteDetails.AsNoTracking() where s.TptRouteDetailId == TptRouteDetailId select s).FirstOrDefault();

            return TptRouteDetail;
        }

        public List<TptRouteDetail> GetAllTptRouteDetails(ref int totalcount, int? BusRouteId = null, DateTime? EffectiveDate = null,
            DateTime? EndDate = null, int? BusId = null, int? DriverId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TptRouteDetails.ToList();

            if (BusRouteId != null && BusRouteId > 0)
                query = query.Where(e => e.BusRouteId == BusRouteId).ToList();
            if (BusId != null && BusId > 0)
                query = query.Where(e => e.BusId == BusId).ToList();
            if (DriverId != null && DriverId > 0)
                query = query.Where(e => e.DriverId == DriverId).ToList();
            if (EffectiveDate.HasValue)
                query = query.Where(e => e.EffectiveDate == EffectiveDate).ToList();
            if (EndDate.HasValue)
                query = query.Where(e => e.EndDate == EndDate).ToList();

            totalcount = query.Count;
            var TptRouteDetails = new PagedList<KSModel.Models.TptRouteDetail>(query, PageIndex, PageSize);
            return TptRouteDetails;
        }

        public DataTable GetAllRouteStudents(ref int totalcount, int? BusRouteId = null, int? SessionId = null)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_getBusRouteStudents", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@BusRouteId", BusRouteId);
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            return dt;
        }

        public void InsertTptRouteDetail(TptRouteDetail TptRouteDetail)
        {
            if (TptRouteDetail == null)
                throw new ArgumentNullException("TptRouteDetail");

            db.TptRouteDetails.Add(TptRouteDetail);
            db.SaveChanges();
        }

        public void UpdateTptRouteDetail(TptRouteDetail TptRouteDetail)
        {
            if (TptRouteDetail == null)
                throw new ArgumentNullException("TptRouteDetail");

            db.Entry(TptRouteDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTptRouteDetail(int TptRouteDetailId = 0)
        {
            if (TptRouteDetailId == 0)
                throw new ArgumentNullException("TptRouteDetail");

            var TptRouteDetail = (from s in db.TptRouteDetails where s.TptRouteDetailId == TptRouteDetailId select s).FirstOrDefault();
            db.TptRouteDetails.Remove(TptRouteDetail);
            db.SaveChanges();
        }

        #endregion

        #region BoardingPoint

        public KSModel.Models.BoardingPoint GetBoardingPointById(int BoardingPointId, bool IsTrack = false)
        {
            if (BoardingPointId == 0)
                throw new ArgumentNullException("BoardingPoint");

            var BoardingPoint = new BoardingPoint();
            if (IsTrack)
                BoardingPoint = (from s in db.BoardingPoints where s.BoardingPointId == BoardingPointId select s).FirstOrDefault();
            else
                BoardingPoint = (from s in db.BoardingPoints.AsNoTracking() where s.BoardingPointId == BoardingPointId select s).FirstOrDefault();

            return BoardingPoint;
        }

        public List<KSModel.Models.BoardingPoint> GetAllBoardingPoints(ref int totalcount, string Boardingpoint = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.BoardingPoints.ToList();
            if (!String.IsNullOrEmpty(Boardingpoint))
                query = query.Where(e => e.BoardingPoint1.ToLower().Contains(Boardingpoint.ToLower())).ToList();

            totalcount = query.Count;
            var BoardingPoints = new PagedList<KSModel.Models.BoardingPoint>(query, PageIndex, PageSize);
            return BoardingPoints;
        }

        public void InsertBoardingPoint(KSModel.Models.BoardingPoint BoardingPoint)
        {
            if (BoardingPoint == null)
                throw new ArgumentNullException("BoardingPoint");

            db.BoardingPoints.Add(BoardingPoint);
            db.SaveChanges();
        }

        public void UpdateBoardingPoint(KSModel.Models.BoardingPoint BoardingPoint)
        {
            if (BoardingPoint == null)
                throw new ArgumentNullException("BoardingPoint");

            db.Entry(BoardingPoint).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBoardingPoint(int BoardingPointId = 0)
        {
            if (BoardingPointId == 0)
                throw new ArgumentNullException("BoardingPoint");

            var BoardingPoint = (from s in db.BoardingPoints where s.BoardingPointId == BoardingPointId select s).FirstOrDefault();
            db.BoardingPoints.Remove(BoardingPoint);
            db.SaveChanges();
        }

        #endregion

        #region BoardingPointFee

        public KSModel.Models.BoardingPointFee GetBoardingPointFeeById(int BoardingPointFeeId, bool IsTrack = false)
        {
            if (BoardingPointFeeId == 0)
                throw new ArgumentNullException("BoardingPointFee");

            var BoardingPointFee = new BoardingPointFee();
            if (IsTrack)
                BoardingPointFee = (from s in db.BoardingPointFees where s.BoardingPointFeeId == BoardingPointFeeId select s).FirstOrDefault();
            else
                BoardingPointFee = (from s in db.BoardingPointFees.AsNoTracking() where s.BoardingPointFeeId == BoardingPointFeeId select s).FirstOrDefault();

            return BoardingPointFee;
        }

        public List<KSModel.Models.BoardingPointFee> GetAllBoardingPointFees(ref int totalcount, int BoardingPointId = 0,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.BoardingPointFees.ToList();

            if(BoardingPointId>0)
            {
                query = query.Where(m => m.BoardingPointId == BoardingPointId).ToList();
            }
           
            totalcount = query.Count;
            var BoardingPointFees = new PagedList<KSModel.Models.BoardingPointFee>(query, PageIndex, PageSize);
            return BoardingPointFees;
        }

        public void InsertBoardingPointFee(KSModel.Models.BoardingPointFee BoardingPointFee)
        {
            if (BoardingPointFee == null)
                throw new ArgumentNullException("BoardingPointFee");

            db.BoardingPointFees.Add(BoardingPointFee);
            db.SaveChanges();
        }

        public void UpdateBoardingPointFee(KSModel.Models.BoardingPointFee BoardingPointFee)
        {
            if (BoardingPointFee == null)
                throw new ArgumentNullException("BoardingPointFee");

            db.Entry(BoardingPointFee).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBoardingPointFee(int BoardingPointFeeId = 0)
        {
            if (BoardingPointFeeId == 0)
                throw new ArgumentNullException("BoardingPointFee");

            var BoardingPointFee = (from s in db.BoardingPointFees where s.BoardingPointFeeId == BoardingPointFeeId select s).FirstOrDefault();
            db.BoardingPointFees.Remove(BoardingPointFee);
            db.SaveChanges();
        }

        #endregion

        #region TptDriver

        public TptDriver GetTptDriverById(int TptDriverId, bool IsTrack = false)
        {
            if (TptDriverId == 0)
                throw new ArgumentNullException("TptDriver");

            var TptDriver = new TptDriver();
            if (IsTrack)
                TptDriver = (from s in db.TptDrivers where s.TptDriverId == TptDriverId select s).FirstOrDefault();
            else
                TptDriver = (from s in db.TptDrivers.AsNoTracking() where s.TptDriverId == TptDriverId select s).FirstOrDefault();

            return TptDriver;
        }

        public List<TptDriver> GetAllTptDrivers(ref int totalcount, string DriverName = "", string LicenseNo = "",
            DateTime? LicenseExpiryDate = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TptDrivers.ToList();
            if (!String.IsNullOrEmpty(DriverName))
                query = query.Where(e => e.DriverName.ToLower().Contains(DriverName.ToLower())).ToList();
            if (!String.IsNullOrEmpty(LicenseNo))
                query = query.Where(e => e.DriverLicenseNo == LicenseNo).ToList();
            if (LicenseExpiryDate.HasValue)
                query = query.Where(e => e.LicenseExpiryDate == LicenseExpiryDate).ToList();

            totalcount = query.Count;
            var TptDrivers = new PagedList<KSModel.Models.TptDriver>(query, PageIndex, PageSize);
            return TptDrivers;
        }

        public void InsertTptDriver(TptDriver TptDriver)
        {
            if (TptDriver == null)
                throw new ArgumentNullException("TptDriver");

            db.TptDrivers.Add(TptDriver);
            db.SaveChanges();
        }

        public void UpdateTptDriver(TptDriver TptDriver)
        {
            if (TptDriver == null)
                throw new ArgumentNullException("TptDriver");

            db.Entry(TptDriver).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTptDriver(int TptDriverId = 0)
        {
            if (TptDriverId == 0)
                throw new ArgumentNullException("TptDriver");

            var TptDriver = (from s in db.TptDrivers where s.TptDriverId == TptDriverId select s).FirstOrDefault();
            db.TptDrivers.Remove(TptDriver);
            db.SaveChanges();
        }

        #endregion

        #region Bus route selectlist

        public IList<SelectListItem> BusRouteBoardingPoints(string TptType = "")
        {
            IList<SelectListItem> BusRouteBoardingPoints = new List<SelectListItem>();
            BusRouteBoardingPoints.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });

            // all boarding points
            var boardingpoints = db.BoardingPoints.ToList();

            var query = db.BusRouteDetails.ToList();
            query = query.Where(b => (b.BusRoute.EndDate > DateTime.UtcNow || b.BusRoute.EndDate == null)).ToList();
            // check Tpt type
            switch (TptType)
            {
                case ("Pick"):
                    query = query.Where(b => b.PickupTime != null).ToList();
                    var commonpickpoints = query.Select(b => b.BoardingPointId).Distinct().ToArray();
                    boardingpoints = boardingpoints.Where(b => commonpickpoints.Contains(b.BoardingPointId)).ToList();
                    break;
                case ("Drop"):
                    query = query.Where(b => b.DropTime != null).ToList();
                    var commondroppoints = query.Select(b => b.BoardingPointId).Distinct().ToArray();
                    boardingpoints = boardingpoints.Where(b => commondroppoints.Contains(b.BoardingPointId)).ToList();
                    break;
                default:
                    // check common Boarding point has pick also drop
                    var pickpoints = query.Where(b => b.PickupTime != null).Select(b => b.BoardingPointId).ToArray();
                    var droppoints = query.Where(b => b.DropTime != null).Select(b => b.BoardingPointId).ToArray();
                    // get common
                    var commonpoints = pickpoints.Where(droppoints.Contains);
                    boardingpoints = boardingpoints.Where(b => commonpoints.Contains(b.BoardingPointId)).ToList();
                    break;
            }

            if (boardingpoints.Count > 0)
            {
                foreach (var busrouteBoardingPoint in boardingpoints)
                    BusRouteBoardingPoints.Add(new SelectListItem { Selected = false, Text = busrouteBoardingPoint.BoardingPoint1, Value = busrouteBoardingPoint.BoardingPointId.ToString() });
            }

            return BusRouteBoardingPoints;
        }

        // bus route autocomplete 
        public IList<SelectListItem> BusRouteByBoardingPoint(int BoardingPointId, string TptType, DateTime? EffectiveDate = null)
        {
            if (BoardingPointId == 0)
                throw new ArgumentNullException("BoardingPoint");

            IList<SelectListItem> BusRoutes = new List<SelectListItem>();
            BusRoutes.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });

            var query = db.BusRouteDetails.ToList();
            query = query.Where(b => b.BoardingPointId == BoardingPointId && (b.BusRoute.EndDate > DateTime.UtcNow || b.BusRoute.EndDate == null)).ToList();
            // check Tpt type
            switch (TptType)
            {
                case ("Pick"):
                    query = query.Where(b => b.PickupTime != null).ToList();
                    break;
                case ("Drop"):
                    query = query.Where(b => b.DropTime != null).ToList();
                    break;
                default:
                    // check common Boarding point has pick also drop
                    query = query.Where(b => b.PickupTime != null || b.DropTime != null).ToList();
                    break;
            }

            if (query.Count > 0)
            {
                var busrouteIds = query.Select(b => b.BusRouteId).ToArray();
                var busroutes = db.BusRoutes.Where(b => busrouteIds.Contains(b.BusRouteId)).ToList();
                if (busroutes.Count > 0)
                {
                    //busroutes = busroutes.Where(m => m.EffectiveDate.Value.Date <= DateTime.Now.Date).ToList();

                    if (EffectiveDate != null)
                    {
                        busroutes = busroutes.Where(m => m.EffectiveDate.Value.Date <= EffectiveDate.Value.Date).ToList();
                        busroutes = busroutes.Where(m => m.EndDate == null || m.EndDate.Value.Date >= EffectiveDate.Value.Date).ToList();
                    }
                    else
                    {
                        busroutes = busroutes.Where(m => m.EffectiveDate.Value.Date <= DateTime.Now.Date).ToList();
                    }
                }

                foreach (var busroute in busroutes)
                    BusRoutes.Add(new SelectListItem { Selected = false, Text = busroute.BusRoute1, Value = busroute.BusRouteId.ToString() });
            }

            return BusRoutes;
        }

        #endregion

        #region"TptAttendant"
      public List<TptAttendant> GetAllTptAttendants(ref int totalcount, string AttendantName = "", string MobileNo = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TptAttendants.ToList();
            if (!String.IsNullOrEmpty(AttendantName))
                query = query.Where(e => e.AttendantName.ToLower().Contains(AttendantName.ToLower())).ToList();
            if (!String.IsNullOrEmpty(MobileNo))
                query = query.Where(e => e.ContactNo == MobileNo).ToList();

            totalcount = query.Count;
            var TptAttendant = new PagedList<KSModel.Models.TptAttendant>(query, PageIndex, PageSize);
            return TptAttendant;
        }
        #endregion

        #region TptRouteDriver

        public TptRouteDriver GetTptRouteDriverById(int TptRouteDriverId, bool IsTrack = false)
        {
            if (TptRouteDriverId == 0)
                throw new ArgumentNullException("TptRouteDriver");

            var TptRouteDriver = new TptRouteDriver();
            if (IsTrack)
                TptRouteDriver = (from s in db.TptRouteDrivers where s.TptRouteDriverId == TptRouteDriverId select s).FirstOrDefault();
            else
                TptRouteDriver = (from s in db.TptRouteDrivers.AsNoTracking() where s.TptRouteDriverId == TptRouteDriverId select s).FirstOrDefault();

            return TptRouteDriver;
        }

        public List<TptRouteDriver> GetAllTptRouteDrivers(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TptRouteDrivers.ToList();

            totalcount = query.Count;
            var TptRouteDrivers = new PagedList<KSModel.Models.TptRouteDriver>(query, PageIndex, PageSize);
            return TptRouteDrivers;
        }

        public void InsertTptRouteDriver(TptRouteDriver TptRouteDriver)
        {
            if (TptRouteDriver == null)
                throw new ArgumentNullException("TptRouteDriver");

            db.TptRouteDrivers.Add(TptRouteDriver);
            db.SaveChanges();
        }

        public void UpdateTptRouteDriver(TptRouteDriver TptRouteDriver)
        {
            if (TptRouteDriver == null)
                throw new ArgumentNullException("TptRouteDriver");

            db.Entry(TptRouteDriver).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTptRouteDriver(int TptRouteDriverId = 0)
        {
            if (TptRouteDriverId == 0)
                throw new ArgumentNullException("TptRouteDriver");

            var TptRouteDriver = (from s in db.TptRouteDrivers where s.TptRouteDriverId == TptRouteDriverId select s).FirstOrDefault();
            db.TptRouteDrivers.Remove(TptRouteDriver);
            db.SaveChanges();
        }

        #endregion

        #region TptRouteAttendant

        public TptRouteAttendant GetTptRouteAttendantById(int TptRouteAttendantId, bool IsTrack = false)
        {
            if (TptRouteAttendantId == 0)
                throw new ArgumentNullException("TptRouteAttendant");

            var TptRouteAttendant = new TptRouteAttendant();
            if (IsTrack)
                TptRouteAttendant = (from s in db.TptRouteAttendants where s.TptRouteAttendantId == TptRouteAttendantId select s).FirstOrDefault();
            else
                TptRouteAttendant = (from s in db.TptRouteAttendants.AsNoTracking() where s.TptRouteAttendantId == TptRouteAttendantId select s).FirstOrDefault();

            return TptRouteAttendant;
        }

        public List<TptRouteAttendant> GetAllTptRouteAttendants(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TptRouteAttendants.ToList();

            totalcount = query.Count;
            var TptRouteAttendants = new PagedList<KSModel.Models.TptRouteAttendant>(query, PageIndex, PageSize);
            return TptRouteAttendants;
        }

        public void InsertTptRouteAttendant(TptRouteAttendant TptRouteAttendant)
        {
            if (TptRouteAttendant == null)
                throw new ArgumentNullException("TptRouteAttendant");

            db.TptRouteAttendants.Add(TptRouteAttendant);
            db.SaveChanges();
        }

        public void UpdateTptRouteAttendant(TptRouteAttendant TptRouteAttendant)
        {
            if (TptRouteAttendant == null)
                throw new ArgumentNullException("TptRouteAttendant");

            db.Entry(TptRouteAttendant).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTptRouteAttendant(int TptRouteAttendantId = 0)
        {
            if (TptRouteAttendantId == 0)
                throw new ArgumentNullException("TptRouteAttendant");

            var TptRouteAttendant = (from s in db.TptRouteAttendants where s.TptRouteAttendantId == TptRouteAttendantId select s).FirstOrDefault();
            db.TptRouteAttendants.Remove(TptRouteAttendant);
            db.SaveChanges();
        }

        #endregion
        #endregion

    }
}