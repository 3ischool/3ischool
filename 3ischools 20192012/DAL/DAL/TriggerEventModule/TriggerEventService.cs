﻿using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.DAL.TriggerEventModule
{
    public partial class TriggerEventService : ITriggerEventService
    {
        public int count = 0;
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        //private readonly string SqlDataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;
        public TriggerEventService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods

        #region TriggerEvent
        public KSModel.Models.TriggerEvent GetTriggerEventById(int TriggerEventId, bool IsTrack = false)
        {
            if (TriggerEventId == 0)
                throw new ArgumentNullException("TriggerEvent");

            var TriggerEvents = new TriggerEvent();
            if (IsTrack)
                TriggerEvents = (from s in db.TriggerEvents where s.TriggerEventId == TriggerEventId select s).FirstOrDefault();
            else
                TriggerEvents = (from s in db.TriggerEvents.AsNoTracking() where s.TriggerEventId == TriggerEventId select s).FirstOrDefault();
            return TriggerEvents;
        }

        public IList<KSModel.Models.TriggerEvent> GetAllTriggerEventList(ref int totalcount,  int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TriggerEvents.ToList();
            
            totalcount = query.Count;
            var TriggerEventList = new PagedList<KSModel.Models.TriggerEvent>(query, PageIndex, PageSize);
            return TriggerEventList;
        }

        public void InsertTriggerEvent(KSModel.Models.TriggerEvent TriggerEvent)
        {

            if (TriggerEvent == null)
                throw new ArgumentNullException("TriggerEvent");

            db.TriggerEvents.Add(TriggerEvent);
            db.SaveChanges();
        }

        public void UpdateTriggerEvent(KSModel.Models.TriggerEvent TriggerEvent)
        {
            if (TriggerEvent == null)
                throw new ArgumentNullException("TriggerEvent");

            db.Entry(TriggerEvent).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTriggerEvent(int TriggerEventId = 0)
        {
            if (TriggerEventId == 0)
                throw new ArgumentNullException("TriggerEventId");

            var TriggerEvent = (from s in db.TriggerEvents where s.TriggerEventId == TriggerEventId select s).FirstOrDefault();
            db.TriggerEvents.Remove(TriggerEvent);
            db.SaveChanges();
        }


        #endregion

        

        #endregion

    }
}