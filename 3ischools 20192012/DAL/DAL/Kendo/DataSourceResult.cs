﻿using System.Collections;

namespace DAL.DAL.Kendo
{
    public class DataSourceResult
    {
        public int ExtraData { get; set; }

        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }
        public string Value4 { get; set; }
        public string Value5 { get; set; }
        public IEnumerable Data { get; set; }
        public object Errors { get; set; }
        public int Total { get; set; }
    }
}
