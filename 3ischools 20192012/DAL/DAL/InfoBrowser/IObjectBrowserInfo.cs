﻿using ViewModel.ViewModel.InfoBrowser;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DAL.DAL.InfoBrowser
{
    public partial interface IObjectBrowserInfo
    {
        List<Filterdata> GetdataFromMastertables(string TableName,string ColumnNames);
        DataTable GetUserDatafromStoreProcedure(string StoreprocedureName="", string Class = "", string House = "", string City = "", string SessionId = "", string StandardId = "", string StudentCategory = "");
    }
}