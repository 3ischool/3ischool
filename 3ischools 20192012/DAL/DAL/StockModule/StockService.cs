﻿using DAL.DAL.Common;
using KSModel.Models;
using ViewModel.ViewModel.Stock;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DAL.DAL.StockModule
{
    public partial class StockService : IStockService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        //private readonly string SqlDataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;
        public StockService(IConnectionService IConnectionService)
        {
            //HttpContext context = HttpContext.Current;
            this._IConnectionService = IConnectionService;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }
        #region"Methods"
        public List<ProductInfo> GetAllStocksBysP(ref int totalcount, string ProductName = "", int ProductGroupId = 0,
                     int ProductTypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue, int ProductId = 0, int ProcedureType = 1)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_GetAllProducts", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ProductName", ProductName);
                    sqlComm.Parameters.AddWithValue("@ProductypeId", ProductTypeId);
                    sqlComm.Parameters.AddWithValue("@ProductGroupId", ProductGroupId);
                    sqlComm.Parameters.AddWithValue("@ProcedureType", ProcedureType);
                    sqlComm.Parameters.AddWithValue("@ProductId", ProductId);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<ProductInfo> GetAllProducts = new List<ProductInfo>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ProductInfo productlist = new ProductInfo();
                productlist.productid = (dt.Rows[i]["ProductId"].ToString());
                productlist.ProductName = Convert.ToString(dt.Rows[i]["ProductName"]);
                productlist.ProductAbbreviation = Convert.ToString(dt.Rows[i]["ProductAbbr"]);
                productlist.ProductDescription = Convert.ToString(dt.Rows[i]["ProductDescription"]);
                productlist.OpeningBalance = Convert.ToString(dt.Rows[i]["OpeningBalance"]);
                productlist.MinStockqty = Convert.ToString(dt.Rows[i]["MinStockQty"]);
                productlist.MaxStockqty = Convert.ToString(dt.Rows[i]["MaxStockQty"]);
                productlist.ProductTypeName = Convert.ToString(dt.Rows[i]["ProductType"]);
                productlist.ProductGroupName = Convert.ToString(dt.Rows[i]["ProductGroup"]);
                productlist.UnitName = Convert.ToString(dt.Rows[i]["Unit"]);
                productlist.ProductTypeId = Convert.ToString(dt.Rows[i]["BusinessProductTypeId"]);
                productlist.ProductGroupId = Convert.ToString(dt.Rows[i]["ProductGroupId"]);
                productlist.Unitid = Convert.ToString(dt.Rows[i]["UnitId"]);
                GetAllProducts.Add(productlist);
            }
            totalcount = GetAllProducts.Count;

            GetAllProducts = new PagedList<ProductInfo>(GetAllProducts, PageIndex, PageSize);
            return GetAllProducts;
        }

        public List<Product> GetAllProducts(ref int totalcount, string ProductName = "", string ProductAbbr = "", string ProductDescription = "",
                              int? ProductTypeId = 0, int? ProductGroupId = 0, int PageIndex = 0, int PageSize = int.MaxValue,
                               string ProductType = "", string ProductGroup = "", bool Isfilter = false)
        {
            var query = db.Products.ToList();

            if (!String.IsNullOrEmpty(ProductName))
            {
                if (Isfilter)
                {
                    query = query.Where(e => e.ProductName.ToLower().Contains(ProductName.ToLower())).ToList();
                }
                else
                {
                    query = query.Where(e => e.ProductName.ToLower() == ProductName.ToLower()).ToList();
                }
            }
            if (!String.IsNullOrEmpty(ProductAbbr))
                query = query.Where(e => e.ProductAbbr.ToLower().Contains(ProductAbbr.ToLower())).ToList();
            if (!String.IsNullOrEmpty(ProductDescription))
                query = query.Where(e => e.ProductDescription.ToLower() == ProductDescription.ToLower()).ToList();
            if ( ProductTypeId > 0)
                query = query.Where(e => e.ProductTypeId == ProductTypeId).ToList();
            if (!String.IsNullOrEmpty(ProductType))
            {
                var product_type = db.ProductTypes.Where(x => x.ProductType1.ToLower().Contains(ProductType.ToLower())).FirstOrDefault();
                query = query.Where(e => e.ProductTypeId == product_type.ProductTypeId).ToList();
            }

            if ( ProductGroupId>0)
                query = query.Where(e => e.ProductGroupId == ProductGroupId).ToList();
            if (!String.IsNullOrEmpty(ProductGroup))
            {
                var product_type = db.ProductGroups.Where(x => x.ProductGroup1.ToLower().Contains(ProductGroup.ToLower())).FirstOrDefault();
                query = query.Where(e => e.ProductGroupId == product_type.ProductGroupId).ToList();
            }

            var ProductMaster = new PagedList<Product>(query, PageIndex, PageSize);
            return ProductMaster;
        }

        public KSModel.Models.Product GetProductById(int? ProductId, bool IsTrack = false)
        {
            if (ProductId == 0)
                throw new ArgumentNullException("Product");

            var Product = new Product();

            if (IsTrack)
                Product = db.Products.Where(m => m.ProductId == ProductId).FirstOrDefault();
            else
                Product = db.Products.AsNoTracking().Where(m => m.ProductId == ProductId).FirstOrDefault();

            return Product;
        }
        public ProductInfo GetProductInfoById(int? ProductId, bool IsTrack = false)
        {
            ProductInfo product = new ProductInfo();
            if (ProductId == 0)
                throw new ArgumentNullException("Product");
            var Ratehistory = db.ProductRateHistories.AsNoTracking().Where(m => m.ProductId == ProductId).ToList();
            List<RateHistoryDetail> RateHistry = new List<RateHistoryDetail>();
            if (Ratehistory != null)
            {
                foreach (var item in Ratehistory)
                {
                    RateHistoryDetail RHDetail = new RateHistoryDetail();
                    RHDetail.PurRate = Convert.ToDecimal(item.PurRate.ToString());
                    RHDetail.MRP = Convert.ToDecimal(item.MRP.ToString());
                    DateTime dttime = Convert.ToDateTime(item.EffectiveFrom);
                    RHDetail.EffectiveFrom = dttime.ToString("dd/MM/yyyy");
                    DateTime DtTill = Convert.ToDateTime(item.EffectiveTill);
                    RHDetail.EffectiveTill = DtTill.ToString("dd/MM/yyyy");
                    RateHistry.Add(RHDetail);
                }
            }
            product.RateHistoryDetail = RateHistry;
            var SpecificationDetail = db.ProductDetails.AsNoTracking().Where(m => m.ProductId == ProductId).ToList();
            List<ProductSpecificationDetail> SpecDetail = new List<ProductSpecificationDetail>();
            if (SpecificationDetail != null)
            {
                foreach (var item in SpecificationDetail)
                {
                    string attrName = "";
                    ProductSpecificationDetail PSDetail = new ProductSpecificationDetail();
                    PSDetail.ProductAttributeValue = item.ProductAttributeValue;
                    var PrdAttr = db.ProductAttributes.AsNoTracking().Where(m => m.ProductAttributeId == item.ProductAttributeId).FirstOrDefault();
                    if (PrdAttr != null)
                    {
                        attrName = PrdAttr.ProductAttribute1;
                    }
                    PSDetail.ProductAttributeName = attrName;
                    SpecDetail.Add(PSDetail);
                }
            }
            product.SpecificationDetail = SpecDetail;

            var Unitconversiondetail = db.ProductUnitConversions.AsNoTracking().Where(m => m.ProductId == ProductId).ToList();
            List<ProductUnitConversionDetail> UnitConDetail = new List<ProductUnitConversionDetail>();
            if (Unitconversiondetail != null)
            {
                foreach (var item in Unitconversiondetail)
                {
                    string unitname = "";
                    ProductUnitConversionDetail UCDetail = new ProductUnitConversionDetail();
                    UCDetail.Conversionfactor = Convert.ToString(item.ConversionFactor);
                    var UnitAttr = db.Units.AsNoTracking().Where(m => m.UnitId == item.CoversionUnitId).FirstOrDefault();
                    if (UnitAttr != null)
                    {
                        unitname = UnitAttr.Unit1;
                    }
                    UCDetail.ConversionUnitName = unitname;
                    UnitConDetail.Add(UCDetail);
                }
            }
            product.UnitConversionDetail = UnitConDetail;
            return product;
        }

        public void InsertProduct(KSModel.Models.Product ProductM)
        {
            if (ProductM == null)
                throw new ArgumentNullException("Product");

            db.Products.Add(ProductM);
            db.SaveChanges();
        }
        public void UpdateProduct(KSModel.Models.Product ProductM)
        {
            if (ProductM == null)
                throw new ArgumentNullException("Product");

            db.Entry(ProductM).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public List<KSModel.Models.ProductGroup> GetAllProductGroup(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.ProductGroups.ToList();
            totalcount = query.Count;
            var ProductGroup = new PagedList<KSModel.Models.ProductGroup>(query, PageIndex, PageSize);
            return ProductGroup;
        }
        public List<KSModel.Models.ProductType> GetAllProductType(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.ProductTypes.ToList();

            totalcount = query.Count;
            var Producttype = new PagedList<KSModel.Models.ProductType>(query, PageIndex, PageSize);
            return Producttype;
        }
        public List<KSModel.Models.Unit> GetAllUnit(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.Units.ToList();

            totalcount = query.Count;
            var unit = new PagedList<KSModel.Models.Unit>(query, PageIndex, PageSize);
            return unit;
        }
        public bool GetDeleteStatus(Boolean IsAuthToDelete, string ProductId = null)
        {
            bool isauth = false;
            if (IsAuthToDelete == false)
            {
                isauth = false;
            }
            else
            {
                var query = db.ProductTransactions.ToList();
                if (ProductId != null)
                {
                    query = query.Where(e => e.ProductId == Convert.ToInt32(ProductId)).ToList();
                }
                int totalcount = query.Count;
                if (totalcount > 0)
                {
                    isauth = false;
                }
                else
                { isauth = true; }
            }
            return isauth;
        }

        public void DeleteProduct(int ProductId)
        {
            if (ProductId == 0)
                throw new ArgumentNullException("Product");
            // Delete Image of Particular Record
            var PImages = (from s in db.ProductImages
                           where s.ProductId == ProductId
                           select s).FirstOrDefault();
            if (PImages != null)
            {
                db.ProductImages.Remove(PImages);
                db.SaveChanges();
            }
            // Delete Rate History of Particular record
            var PRH = (from s in db.ProductRateHistories
                       where s.ProductId == ProductId
                       select s).ToList();
            if (PRH != null)
            {
                db.ProductRateHistories.RemoveRange(PRH);
                db.SaveChanges();
            }
            // Delete Product Detail
            var PD = (from s in db.ProductDetails
                      where s.ProductId == ProductId
                      select s).ToList();
            if (PD != null)
            {
                db.ProductDetails.RemoveRange(PD);
                db.SaveChanges();

            }

            // delete Product Unit Coversion
            var PUC = (from s in db.ProductUnitConversions
                       where s.ProductId == ProductId
                       select s).ToList();
            if (PUC != null)
            {
                db.ProductUnitConversions.RemoveRange(PUC);
                db.SaveChanges();
            }
            // Delete Product
            var Product = (from s in db.Products
                           where s.ProductId == ProductId
                           select s).FirstOrDefault();
            if (Product != null)
            {
                db.Products.Remove(Product);
                db.SaveChanges();
            }
        }

        public string GetGroupName(int ProGroupId)
        {
            string Groupname = "";
            var Product = db.ProductGroups.Where(m => m.ProductGroupId == ProGroupId).FirstOrDefault();
            Groupname = Product.ProductGroup1;
            return Groupname;
        }
        public string GetProductType(int ProTypeId)
        {
            string Typename = "";
            var Product = db.ProductTypes.Where(m => m.ProductTypeId == ProTypeId).FirstOrDefault();
            Typename = Product.ProductType1;
            return Typename;
        }
        public string GetUnit(int UnitId)
        {
            string Unitname = "";
            var Product = db.Units.Where(m => m.UnitId == UnitId).FirstOrDefault();
            Unitname = Product.Unit1;
            return Unitname;
        }
        public List<ProductInfo> GetAllRateHistoryBysP(ref int totalcount, int ProductId, int PageIndex = 0, int PageSize = int.MaxValue, int ProcedureType = 2)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_GetAllProducts", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ProductName", "");
                    sqlComm.Parameters.AddWithValue("@ProductypeId", "0");
                    sqlComm.Parameters.AddWithValue("@ProductGroupId", "0");
                    sqlComm.Parameters.AddWithValue("@ProcedureType", ProcedureType);
                    sqlComm.Parameters.AddWithValue("@ProductId", ProductId);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<ProductInfo> GetAllRateHistory = new List<ProductInfo>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ProductInfo productlist = new ProductInfo();
                productlist.productid = (dt.Rows[i]["ProductId"].ToString());
                productlist.ProductRateHistoryId = (dt.Rows[i]["ProductRateHistoryId"].ToString());
                productlist.MRP = Convert.ToDecimal(dt.Rows[i]["MRP"]);
                productlist.PurRate = Convert.ToDecimal(dt.Rows[i]["PurRate"]);
                productlist.EffectiveFrom = Convert.ToString(dt.Rows[i]["EffectiveFrom"]);
                productlist.EffectiveTill = Convert.ToString(dt.Rows[i]["EffectiveTill"]);

                GetAllRateHistory.Add(productlist);
            }
            totalcount = GetAllRateHistory.Count;

            GetAllRateHistory = new PagedList<ProductInfo>(GetAllRateHistory, PageIndex, PageSize);
            return GetAllRateHistory;
        }

        public void InserRateHistory(ProductRateHistory PRH)
        {
            if (PRH == null)
                throw new ArgumentNullException("ProductRateHistory");

            db.ProductRateHistories.Add(PRH);
            db.SaveChanges();
        }
        public void UpdateEffectiveTillDate(int ProductId)
        {
            var query = db.ProductRateHistories.AsNoTracking().Where(p => p.ProductId == ProductId && p.EffectiveTill == null).FirstOrDefault();
            if (query != null)
            {
                KSModel.Models.ProductRateHistory PRH = new ProductRateHistory();
                PRH.ProductId = ProductId;
                PRH.ProductRateHistoryId = query.ProductRateHistoryId;
                PRH.EffectiveTill = DateTime.Now;
                PRH.EffectiveFrom = query.EffectiveFrom;
                PRH.MRP = query.MRP;
                PRH.PurRate = query.PurRate;
                db.Entry(PRH).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public List<ProductInfo> GetAllProductSpecificationBysP(ref int totalcount, int ProductId, int PageIndex = 0, int PageSize = int.MaxValue, int ProcedureType = 3)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_GetAllProducts", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ProductName", "");
                    sqlComm.Parameters.AddWithValue("@ProductypeId", "0");
                    sqlComm.Parameters.AddWithValue("@ProductGroupId", "0");
                    sqlComm.Parameters.AddWithValue("@ProcedureType", ProcedureType);
                    sqlComm.Parameters.AddWithValue("@ProductId", ProductId);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<ProductInfo> GetAllProSpec = new List<ProductInfo>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ProductInfo productlist = new ProductInfo();
                productlist.productid = (dt.Rows[i]["ProductId"].ToString());
                productlist.ProductDetailId = (dt.Rows[i]["ProductDetailId"].ToString());
                productlist.ProductAttributeId = Convert.ToString(dt.Rows[i]["ProductAttributeId"]);
                productlist.ProductAttributeValueName = Convert.ToString(dt.Rows[i]["ProductAttributeValue"]);
                productlist.ProductAttributename = Convert.ToString(dt.Rows[i]["ProductAttribute"]);
                productlist.ProductAttributeValueId = Convert.ToString(dt.Rows[i]["ProductAttributeValueId"]);
                GetAllProSpec.Add(productlist);
            }
            totalcount = GetAllProSpec.Count;

            GetAllProSpec = new PagedList<ProductInfo>(GetAllProSpec, PageIndex, PageSize);
            return GetAllProSpec;
        }

        public void InsertOrUpdateProductSpecificationbySp(int ProductId = 0, int DetailId = 0, string AttributeValue = "", int AttributeId = 0)
        {
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_InsertOrUpdateAttributes", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ProductId", ProductId);
                    sqlComm.Parameters.AddWithValue("@DetailId", DetailId);
                    sqlComm.Parameters.AddWithValue("@AttributeValue", AttributeValue);
                    sqlComm.Parameters.AddWithValue("@AttributeId", AttributeId);
                    sqlComm.ExecuteNonQuery();
                }
            }
        }

        public bool InsertProductAttributeValue(ProductAttributeValue PAV)
        {
            if (PAV == null)
                throw new ArgumentNullException("ProductAttributeValue");
            bool isexist = false;
            var valexist = db.ProductAttributeValues.AsNoTracking().Where(e => e.ProductAttributeValue1.ToLower() == PAV.ProductAttributeValue1.ToLower()).FirstOrDefault();
            if (valexist == null)
            {
                db.ProductAttributeValues.Add(PAV);
                db.SaveChanges();
                isexist = false;
            }
            else
            {
                isexist = true;
            }
            return isexist;
        }

        public void InserorUpdateProductSpecification(ProductDetail PD)
        {
            if (PD == null)
                throw new ArgumentNullException("ProductDetail");

            if (PD.ProductDetailId > 0)
            {
                db.Entry(PD).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                db.ProductDetails.Add(PD);
                db.SaveChanges();
            }

        }

        public KSModel.Models.ProductDetail GetProductDetailById(int? DetailId, bool IsTrack = false)
        {
            if (DetailId == 0)
                throw new ArgumentNullException("Product Detail");

            var ProductDetail = new ProductDetail();

            if (IsTrack)
                ProductDetail = db.ProductDetails.Where(m => m.ProductDetailId == DetailId).FirstOrDefault();
            else
                ProductDetail = db.ProductDetails.AsNoTracking().Where(m => m.ProductDetailId == DetailId).FirstOrDefault();

            return ProductDetail;
        }
        public List<KSModel.Models.ProductAttribute> GetAllAttributes(ref int totalcount, int ProductId = 0, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.ProductAttributes.ToList();
            var query1 = db.ProductDetails.Where(p => p.ProductId == ProductId).Select(p => p.ProductAttributeId);
            if (ProductId > 0)
            {
                query = query.Where(m => !query1.Contains(m.ProductAttributeId)).ToList();
            }
            totalcount = query.Count;
            var unit = new PagedList<KSModel.Models.ProductAttribute>(query, PageIndex, PageSize);
            return unit;
        }

        public List<KSModel.Models.ProductAttributeValue> GetAllAttributeValues(ref int totalcount, int ProductAttributeId = 0, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.ProductAttributeValues.ToList();

            if (ProductAttributeId > 0)
            {
                query = query.Where(m => m.ProductAttributeId == ProductAttributeId).ToList();
            }
            totalcount = query.Count;
            var unit = new PagedList<KSModel.Models.ProductAttributeValue>(query, PageIndex, PageSize);
            return unit;
        }
        public void DeleteProductDetail(int detailId)
        {
            if (detailId == 0)
                throw new ArgumentNullException("Product Detail");

            // Delete Product Detail
            var PD = (from s in db.ProductDetails
                      where s.ProductDetailId == detailId
                      select s).FirstOrDefault();
            db.ProductDetails.Remove(PD);
            db.SaveChanges();

        }


        public List<ProductInfo> GetAllProductUnitConversionBysP(ref int totalcount, int ProductId, int PageIndex = 0, int PageSize = int.MaxValue, int ProcedureType = 4)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_GetAllProducts", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ProductName", "");
                    sqlComm.Parameters.AddWithValue("@ProductypeId", "0");
                    sqlComm.Parameters.AddWithValue("@ProductGroupId", "0");
                    sqlComm.Parameters.AddWithValue("@ProcedureType", ProcedureType);
                    sqlComm.Parameters.AddWithValue("@ProductId", ProductId);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<ProductInfo> GetAllProUC = new List<ProductInfo>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ProductInfo productlist = new ProductInfo();
                productlist.productid = (dt.Rows[i]["ProductId"].ToString());
                productlist.ProductUnitConversionId = (dt.Rows[i]["ProductUnitConversionId"].ToString());
                productlist.ConversionUnitId = Convert.ToString(dt.Rows[i]["CoversionUnitId"]);
                productlist.Conversionfactor = Convert.ToString(dt.Rows[i]["ConversionFactor"]);
                productlist.ConversionUnitName = Convert.ToString(dt.Rows[i]["Unit"]);

                GetAllProUC.Add(productlist);
            }
            totalcount = GetAllProUC.Count;

            GetAllProUC = new PagedList<ProductInfo>(GetAllProUC, PageIndex, PageSize);
            return GetAllProUC;
        }
        public void InsertProductUnitConversion(ProductUnitConversion PUC)
        {
            if (PUC == null)
                throw new ArgumentNullException("ProductUnitConversion");

            db.ProductUnitConversions.Add(PUC);
            db.SaveChanges();
        }
        public void DeleteProductUnitConversion(int UnitConversionId)
        {
            if (UnitConversionId == 0)
                throw new ArgumentNullException("Product Unit Conversion");

            // Delete Product Detail
            var PUC = (from s in db.ProductUnitConversions
                       where s.ProductUnitConversionId == UnitConversionId
                       select s).FirstOrDefault();
            db.ProductUnitConversions.Remove(PUC);
            db.SaveChanges();

        }
        public void DeleteProductRateHistory(int HistoryId)
        {
            if (HistoryId == 0)
                throw new ArgumentNullException("ProductRateHistory");

            // Delete Product Detail
            var PRH = (from s in db.ProductRateHistories
                       where s.ProductRateHistoryId == HistoryId
                       select s).FirstOrDefault();
            db.ProductRateHistories.Remove(PRH);
            db.SaveChanges();

        }
        public void InsertProductImage(ProductImage PI)
        {
            if (PI == null)
                throw new ArgumentNullException("ProductImage");

            db.ProductImages.Add(PI);
            db.SaveChanges();
        }
        public void UpdateProductImage(ProductImage PI)
        {
            if (PI == null)
                throw new ArgumentNullException("Image");

            db.Entry(PI).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public KSModel.Models.ProductImage GetProductImageById(int? ProductId, bool IsTrack = false)
        {
            if (ProductId == 0)
                throw new ArgumentNullException("ProductImage");

            var Product = new ProductImage();

            if (IsTrack)
                Product = db.ProductImages.Where(m => m.ProductId == ProductId).FirstOrDefault();
            else
                Product = db.ProductImages.AsNoTracking().Where(m => m.ProductId == ProductId).FirstOrDefault();

            return Product;
        }
        public List<ClassMaster> GetAllClasses(ref int totalcount)
        {
            var query = db.ClassMasters.OrderBy(x => x.StandardMaster.StandardIndex).ToList();
            totalcount = query.Count;
            return query;
        }
        public List<Product> GetAllProducts(ref int totalcount)
        {
            var query = db.Products.ToList();
            totalcount = query.Count;
            return query;
        }
        public int InsertorUpdatePackage(StockPackage StockPack)
        {
            int PackageId = 0;
            if (StockPack == null)
                throw new ArgumentNullException("Stock Packages");

            if (StockPack.StockPackagesId == 0)
            {
                StockPackage stockPackages = new StockPackage();
                stockPackages.StockPackagesId = StockPack.StockPackagesId;
                stockPackages.StockPackages = StockPack.StockPackages;
                db.StockPackages.Add(stockPackages);
                db.SaveChanges();
                PackageId = stockPackages.StockPackagesId;
            }
            else
            {
                db.Entry(StockPack).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                PackageId = StockPack.StockPackagesId;
            }
            return PackageId;
        }
        public void InsertStockPackageClass(StockPackageClass StockCls)
        {
            if (StockCls == null)
                throw new ArgumentNullException("Stock Package Class");
            var Classes = db.StockPackageClasses.AsNoTracking().Where(m => m.ClassId == StockCls.ClassId && m.StockpackageId == StockCls.StockpackageId).FirstOrDefault();
            if (Classes == null)
            {
                db.StockPackageClasses.Add(StockCls);
                db.SaveChanges();
            }
        }
        public void InsertorUpdateStockPackageDetail(StockPackageDetail StockDetail)
        {
            if (StockDetail == null)
                throw new ArgumentNullException("Stock Package Detail");
            if (StockDetail.PackageDetailId > 0)
            {
                db.Entry(StockDetail).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                db.StockPackageDetails.Add(StockDetail);
                db.SaveChanges();
            }
        }

        public List<ProductInfo> GetAllPackagesBysP(ref int totalcount, string PackageName = ""
          , int PageIndex = 0, int PageSize = int.MaxValue, int ProcedureType = 1)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("Sp_GetAllpackages", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@PackageName", PackageName);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<ProductInfo> GetAllPackages = new List<ProductInfo>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ProductInfo packagelist = new ProductInfo();
                packagelist.PackageId = (dt.Rows[i]["StockPackagesId"].ToString());
                packagelist.PackageName = Convert.ToString(dt.Rows[i]["StockPackages"]);
                packagelist.NumberofProducts = Convert.ToString(dt.Rows[i]["TotalProducts"]);
                packagelist.NumberofClasses = Convert.ToString(dt.Rows[i]["TotalClasses"]);
                packagelist.Quantity = Convert.ToString(dt.Rows[i]["TotalQuantity"]);
                GetAllPackages.Add(packagelist);
            }
            totalcount = GetAllPackages.Count;

            GetAllPackages = new PagedList<ProductInfo>(GetAllPackages, PageIndex, PageSize);
            return GetAllPackages;
        }

        public ProductInfo GetPackageById(int? PackageId, bool IsTrack = false)
        {
            ProductInfo packageinfo = new ProductInfo();
            if (PackageId == 0)
                throw new ArgumentNullException("Product");

            var Package = new KSModel.Models.StockPackage();

            if (IsTrack)
                Package = db.StockPackages.Where(m => m.StockPackagesId == PackageId).FirstOrDefault();
            else
                Package = db.StockPackages.AsNoTracking().Where(m => m.StockPackagesId == PackageId).FirstOrDefault();
            if (Package != null)
            {
                packageinfo.PackageName = Package.StockPackages;
            }

            List<KSModel.Models.StockPackageClass> Packageclasses = new List<KSModel.Models.StockPackageClass>();
            if (IsTrack)
                Packageclasses = db.StockPackageClasses.Where(m => m.StockpackageId == PackageId).ToList();
            else
                Packageclasses = db.StockPackageClasses.AsNoTracking().Where(m => m.StockpackageId == PackageId).ToList();


            if (Packageclasses != null)
            {
                string[] classes = new string[Packageclasses.Count];
                string[] Classnames = new string[Packageclasses.Count];

                for (int i = 0; i < Packageclasses.Count; i++)
                {
                    classes[i] = Convert.ToString(Packageclasses[i].ClassId);
                    int classid = Convert.ToInt32(Packageclasses[i].ClassId);
                    var classMaster = db.ClassMasters.AsNoTracking().Where(m => m.ClassId == classid).FirstOrDefault();
                    if (classMaster != null)
                        Classnames[i] = classMaster.Class;
                }

                packageinfo.ClassIds = classes;
                packageinfo.Classnames = Classnames;
            }

            List<KSModel.Models.StockPackageDetail> PackageDet = new List<KSModel.Models.StockPackageDetail>();
            if (IsTrack)
                PackageDet = db.StockPackageDetails.Where(m => m.StockPackageId == PackageId).ToList();
            else
                PackageDet = db.StockPackageDetails.AsNoTracking().Where(m => m.StockPackageId == PackageId).ToList();
            if (PackageDet != null)
            {
                int length = PackageDet.Count;
                List<PackageDetail> pckDetail = new List<PackageDetail>();
                for (int i = 0; i < length; i++)
                {
                    string Productname = "";
                    int packid = Convert.ToInt32(PackageDet[i].ProductId);
                    var product = db.Products.AsNoTracking().Where(m => m.ProductId == packid).FirstOrDefault();
                    if (product != null)
                    {
                        Productname = product.ProductName;
                    }
                    pckDetail.Add(new PackageDetail { PackageDetailId = Convert.ToString(PackageDet[i].PackageDetailId), productid = Convert.ToString(PackageDet[i].ProductId), Quantity = Convert.ToString(PackageDet[i].Quantity), ProductName = Productname });
                }
                packageinfo.PackageDetail = pckDetail;
            }
            return packageinfo;
        }
        public void DeletePackage(int PackageId)
        {
            if (PackageId == 0)
                throw new ArgumentNullException("ProductRateHistory");

            // Delete Product Detail
            var PCKC = (from s in db.StockPackageClasses
                        where s.StockpackageId == PackageId
                        select s).ToList();
            if (PCKC != null)
            {
                db.StockPackageClasses.RemoveRange(PCKC);
                db.SaveChanges();
            }

            var PCKD = (from s in db.StockPackageDetails
                        where s.StockPackageId == PackageId
                        select s).ToList();
            if (PCKD != null)
            {
                db.StockPackageDetails.RemoveRange(PCKD);
                db.SaveChanges();
            }
            var PCK = (from s in db.StockPackages
                       where s.StockPackagesId == PackageId
                       select s).FirstOrDefault();
            db.StockPackages.Remove(PCK);
            db.SaveChanges();

        }

        public List<ProductsDetailList> GetProductDetailList(int VoucherId = 0, int ProcedureType = 1)
        {
            List<ProductsDetailList> productdetaillist = new List<ProductsDetailList>();

            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("spGetVoucherDetails", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@VoucherId", VoucherId);
                    sqlComm.Parameters.AddWithValue("@ProcedureType", ProcedureType);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ProductsDetailList productDetail = new ProductsDetailList();
                productDetail.ProductId = dt.Rows[i]["ProductId"].ToString();
                productDetail.ProductName = dt.Rows[i]["ProductName"].ToString();
                productDetail.Quantity = dt.Rows[i]["Quantity"].ToString();
                productDetail.Price = dt.Rows[i]["NetAmount"].ToString();
                productDetail.GSTRate = dt.Rows[i]["GSTRate"].ToString();
                productDetail.CGSTValue = dt.Rows[i]["CGSTValue"].ToString();
                productDetail.SGSTValue = dt.Rows[i]["SGSTValue"].ToString();
                productDetail.IGSTValue = dt.Rows[i]["IGSTValue"].ToString();
                productDetail.CessValue = dt.Rows[i]["CessValue"].ToString();
                productDetail.TaxableAmount = dt.Rows[i]["TaxableValue"].ToString();
                productdetaillist.Add(productDetail);
            }
            return productdetaillist;
        }

        public List<Inv_Store> GetAllStores(ref int count, int UserId)
        {
            List<Inv_Store> StoreList = new List<Inv_Store>();
            var list3 = (from S in db.Stores
                         join IUS in db.Inv_StoreUser
                         on S.StoreId equals IUS.StoreId // join on some property
                         select new { S.StoreId, S.StoreName }).ToList();
            foreach(var item in list3)
            {
                StoreList.Add(new Inv_Store { StoreId = item.StoreId.ToString(), StoreName = item.StoreName });
            }
            return StoreList;
        }

        public List<AccountTypeMaster> GetAllAccountTypes(ref int count)
        {
            List<AccountTypeMaster> Accounttypelist = new List<AccountTypeMaster>();
            var query = db.ContactTypes.Where(m=>m.ContactType1!="Admin").ToList();
           foreach(var item in query)
           {
               Accounttypelist.Add(new AccountTypeMaster { AccountTypeId = item.ContactTypeId.ToString(), AccountType = item.ContactType1 });
           }
           return Accounttypelist;
        }
        public  List<AccountListD> GetAllaccounts(ref int count, int AccounTypeId = 0)
        {
            List<AccountListD> Accountlist = new List<AccountListD>();
            string accounttype = "";
            if(AccounTypeId>0)
            {
                var query = db.ContactTypes.Where(m => m.ContactTypeId == AccounTypeId).FirstOrDefault();
                if (query != null)
                {
                    accounttype = query.ContactType1;
                }
            }
           var query1 = db.SchoolUsers.ToList();
           int accounttypeid = 0;
           accounttypeid = Convert.ToInt32(AccounTypeId);
            if(AccounTypeId>0)
            {
                if(accounttype.ToString().ToLower()=="student")
                {
                    var list3 = (from SU in db.SchoolUsers
                                 join S in db.Students
                                 on SU.UserContactId equals S.StudentId // join on some property
                                 where SU.UserTypeId == accounttypeid
                                 select new { SU.UserId, S.FName,S.LName  }).ToList();
                    foreach (var item in list3)
                    {
                        Accountlist.Add(new AccountListD { AccountId = item.UserId.ToString(), Account = item.FName + " " + item.LName });
                    }
                }
                else if (accounttype.ToString().ToLower() == "teacher" || accounttype.ToString().ToLower() == "teaching" || accounttype.ToString().ToLower() == "non-teaching")
                {
                    var list3 = (from SU in db.SchoolUsers
                                 join S in db.Staffs
                                 on SU.UserContactId equals S.StaffId // join on some property
                                 where SU.UserTypeId == accounttypeid
                                 select new { SU.UserId, S.FName, S.LName }).ToList();
                    foreach (var item in list3)
                    {
                        Accountlist.Add(new AccountListD { AccountId = item.UserId.ToString(), Account = item.FName + " " + item.LName });
                    }
                }
                else if (accounttype.ToString().ToLower() == "guardian")
                {
                    var list3 = (from SU in db.SchoolUsers
                                 join S in db.Guardians
                                 on SU.UserContactId equals S.GuardianId // join on some property
                                 where SU.UserTypeId == accounttypeid
                                 select new { SU.UserId, S.FirstName, S.LastName }).ToList();
                    foreach (var item in list3)
                    {
                        Accountlist.Add(new AccountListD { AccountId = item.UserId.ToString(), Account = item.FirstName + " " + item.LastName });
                    }
                }
                
            }
           
            return Accountlist;
        }
        public List<KSModel.Models.TaxType> GetAllTaxtypes(ref int count)
        {
           var taxtypelist = db.TaxTypes.ToList();
            return taxtypelist;
        }
        public VoucherInfo GetVoucherDetail(int VoucherId = 0, int ProcedureType = 2)
        {
            VoucherInfo voucherinfodetail = new VoucherInfo();
            var Voucherdetail = db.InventoryVouchers.Where(m=> m.InventoryVoucherId == VoucherId).FirstOrDefault();
            if (Voucherdetail!=null)
            {
                voucherinfodetail.Voucher = Voucherdetail.VoucherNo;
                DateTime dttime = Convert.ToDateTime(Voucherdetail.VoucherDate);
                voucherinfodetail.VoucherDate = dttime.Date;
                voucherinfodetail.AccountId = Voucherdetail.AccountId.ToString();
                voucherinfodetail.AccountTypeId = Voucherdetail.AccountTypeId.ToString();
                DateTime dtDocDate = Convert.ToDateTime(Voucherdetail.DocDate);
                voucherinfodetail.DocumentDate = dtDocDate.Date;
                voucherinfodetail.DocumentNo = Voucherdetail.DocumentNo;
                voucherinfodetail.StoreId = Voucherdetail.TransStoreId.ToString();
                voucherinfodetail.TaxTypeId = Voucherdetail.TaxTypeId.ToString();
                voucherinfodetail.VoucherTypeId = Voucherdetail.TrsDocId.ToString();
            }
            return voucherinfodetail;
        }
        public List<KSModel.Models.TrsDoc> GetAllVoucherTypes()
        {
            var vouchertype = db.TrsDocs.ToList();
            return vouchertype;
        }
        public List<KSModel.Models.StockPackage> GetAllPackages(ref int count)
        {
            var Packagelist = db.StockPackages.ToList();
            return Packagelist;
        }
        public List<KSModel.Models.StockPackageClass> GetAllStockPackageClasses(ref int count)
        {
            var StockPackageClasslist = db.StockPackageClasses.ToList();
            return StockPackageClasslist;
        }
        #endregion
    }
}