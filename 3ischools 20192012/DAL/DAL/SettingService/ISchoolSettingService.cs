﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DAL.SettingService
{
    public partial interface ISchoolSettingService
    {
        #region SchoolSetting

        void InsertSchoolSetting(SchoolSetting SchoolSetting);

        void UpdateSchoolSetting(SchoolSetting SchoolSetting);

        SchoolSetting GetSchoolSettingByAttribute(string AttributeName);

        string GetAttributeValue(string AttributeName);

        SchoolSetting GetorSetSchoolData(string AttrName, string AttrValue, 
                                            int? AppFormId = null, string Desc = "");

        SchoolSetting GetSetUpdateSchoolData(string AttrName, string AttrValue,
                                           int? AppFormId = null, string Desc = "");

        #endregion

    }
}
