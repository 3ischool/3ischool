﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.DAL.StudentModule
{
    public enum AdmnStatus
    {
        
        Waiting = 10,

        Rejected = 20,

        Admitted=30
    }
}