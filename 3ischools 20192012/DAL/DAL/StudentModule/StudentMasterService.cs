﻿using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DAL.DAL.StudentModule
{
    public partial class StudentMasterService : IStudentMasterService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public StudentMasterService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods

        #region StudentStatu

        public StudentStatu GetStudentStatuById(int StudentStatuId, bool IsTrack = false)
        {
            if (StudentStatuId == 0)
                throw new ArgumentNullException("StudentStatus");

            var StudentStatu = new StudentStatu();
            if (IsTrack)
                StudentStatu = (from s in db.StudentStatus.AsNoTracking() where s.StudentStatusId == StudentStatuId select s).FirstOrDefault();
            else
                StudentStatu = (from s in db.StudentStatus.AsNoTracking() where s.StudentStatusId == StudentStatuId select s).FirstOrDefault();

            return StudentStatu;
        }

        public List<StudentStatu> GetAllStudentStatus(ref int totalcount, string Status = "", int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.StudentStatus.ToList();
            if (!String.IsNullOrEmpty(Status))
                query = query.Where(e => e.StudentStatus.ToLower().Contains(Status.ToLower())).ToList();

            totalcount = query.Count;
            var StudentStatus = new PagedList<KSModel.Models.StudentStatu>(query, PageIndex, PageSize);
            return StudentStatus;
        }

        public void InsertStudentStatu(StudentStatu StudentStatu)
        {
            if (StudentStatu == null)
                throw new ArgumentNullException("StudentStatus");

            db.StudentStatus.Add(StudentStatu);
            db.SaveChanges();
        }

        public void UpdateStudentStatu(StudentStatu StudentStatu)
        {
            if (StudentStatu == null)
                throw new ArgumentNullException("StudentStatus");

            db.Entry(StudentStatu).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStudentStatu(int StudentStatuId = 0)
        {
            if (StudentStatuId == 0)
                throw new ArgumentNullException("StudentStatus");

            var StudentStatu = (from s in db.StudentStatus where s.StudentStatusId == StudentStatuId select s).FirstOrDefault();
            db.StudentStatus.Remove(StudentStatu);
            db.SaveChanges();
        }

        #endregion

        #region StudentStatusDetail

        public StudentStatusDetail GetStudentStatusDetailById(int StudentStatusDetailId, bool IsTrack = false)
        {
            if (StudentStatusDetailId == 0)
                throw new ArgumentNullException("StudentStatusDetail");

            var StudentStatusDetail = new StudentStatusDetail();
            if (IsTrack)
                StudentStatusDetail = (from s in db.StudentStatusDetails.AsNoTracking() where s.StudentStatusDetailId == StudentStatusDetailId select s).FirstOrDefault();
            else
                StudentStatusDetail = (from s in db.StudentStatusDetails.AsNoTracking() where s.StudentStatusDetailId == StudentStatusDetailId select s).FirstOrDefault();

            return StudentStatusDetail;
        }

        public List<StudentStatusDetail> GetAllStudentStatusDetail(ref int totalcount, int? StudentId = null, DateTime? StatusChangeDate = null, int? StudentStatusId = null, int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.StudentStatusDetails.ToList();

            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();

            if (StatusChangeDate.HasValue)
                query = query.Where(e => e.StatusChangeDate == StatusChangeDate).ToList();

            if (StudentStatusId != null && StudentStatusId > 0)
                query = query.Where(e => e.StudentStatusId == StudentStatusId).ToList();

            totalcount = query.Count;
            var StudentStatusDetail = new PagedList<KSModel.Models.StudentStatusDetail>(query, PageIndex, PageSize);
            return StudentStatusDetail;
        }

        public void InsertStudentStatusDetail(StudentStatusDetail StudentStatusDetail)
        {
            if (StudentStatusDetail == null)
                throw new ArgumentNullException("StudentStatusDetail");

            db.StudentStatusDetails.Add(StudentStatusDetail);
            db.SaveChanges();
        }

        public void UpdateStudentStatusDetail(StudentStatusDetail StudentStatusDetail)
        {
            if (StudentStatusDetail == null)
                throw new ArgumentNullException("StudentStatusDetail");

            db.Entry(StudentStatusDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStudentStatusDetail(int StudentStatusDetailId = 0)
        {
            if (StudentStatusDetailId == 0)
                throw new ArgumentNullException("StudentStatusDetail");

            var StudentStatusDetail = (from s in db.StudentStatusDetails where s.StudentStatusDetailId == StudentStatusDetailId select s).FirstOrDefault();
            db.StudentStatusDetails.Remove(StudentStatusDetail);
            db.SaveChanges();
        }

        #endregion

        #region StudentCategory

        public StudentCategory GetStudentCategoryById(int StudentCategoryId, bool IsTrack = false)
        {
            if (StudentCategoryId == 0)
                throw new ArgumentNullException("StudentCategory");

            var StudentCategory = new StudentCategory();
            if (IsTrack)
                StudentCategory = (from s in db.StudentCategories where s.StudentCategoryId == StudentCategoryId select s).FirstOrDefault();
            else
                StudentCategory = (from s in db.StudentCategories.AsNoTracking() where s.StudentCategoryId == StudentCategoryId select s).FirstOrDefault();

            return StudentCategory;
        }

        public List<StudentCategory> GetAllStudentCategorys(ref int totalcount, string StudentCategory = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StudentCategories.ToList();
            if (!String.IsNullOrEmpty(StudentCategory))
                query = query.Where(e => e.StudentCategory1.ToLower().Contains(StudentCategory.ToLower())).ToList();

            totalcount = query.Count;
            var StudentCategories = new PagedList<KSModel.Models.StudentCategory>(query, PageIndex, PageSize);
            return StudentCategories;
        }

        public void InsertStudentCategory(StudentCategory StudentCategory)
        {
            if (StudentCategory == null)
                throw new ArgumentNullException("StudentCategory");

            db.StudentCategories.Add(StudentCategory);
            db.SaveChanges();
        }

        public void UpdateStudentCategory(StudentCategory StudentCategory)
        {
            if (StudentCategory == null)
                throw new ArgumentNullException("StudentCategory");

            db.Entry(StudentCategory).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStudentCategory(int StudentCategoryId = 0)
        {
            if (StudentCategoryId == 0)
                throw new ArgumentNullException("StudentCategory");

            var StudentCategory = (from s in db.StudentCategories where s.StudentCategoryId == StudentCategoryId select s).FirstOrDefault();
            db.StudentCategories.Remove(StudentCategory);
            db.SaveChanges();
        }

        #endregion

        #region SiblingInfo

        public SiblingInfo GetSiblingInfoById(int SiblingInfoId, bool IsTrack = false)
        {
            if (SiblingInfoId == 0)
                throw new ArgumentNullException("SiblingInfo");

            var SiblingInfo = new SiblingInfo();
            if (IsTrack)
                SiblingInfo = (from s in db.SiblingInfoes where s.SiblingInfoId == SiblingInfoId select s).FirstOrDefault();
            else
                SiblingInfo = (from s in db.SiblingInfoes.AsNoTracking() where s.SiblingInfoId == SiblingInfoId select s).FirstOrDefault();

            return SiblingInfo;
        }

        public List<SiblingInfo> GetAllSiblingInfos(ref int totalcount, int? StudentId = null, int? SiblingId = null,
            int? RelationId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SiblingInfoes.ToList();
            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();
            if (SiblingId != null && SiblingId > 0)
                query = query.Where(e => e.SiblingId == SiblingId).ToList();
            if (RelationId != null && RelationId > 0)
                query = query.Where(e => e.RelationId == RelationId).ToList();

            totalcount = query.Count;
            var SiblingInfoes = new PagedList<KSModel.Models.SiblingInfo>(query, PageIndex, PageSize);
            return SiblingInfoes;
        }

        public void InsertSiblingInfo(SiblingInfo SiblingInfo)
        {
            if (SiblingInfo == null)
                throw new ArgumentNullException("SiblingInfo");

            db.SiblingInfoes.Add(SiblingInfo);
            db.SaveChanges();
        }

        public void UpdateSiblingInfo(SiblingInfo SiblingInfo)
        {
            if (SiblingInfo == null)
                throw new ArgumentNullException("SiblingInfo");

            db.Entry(SiblingInfo).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSiblingInfo(int SiblingInfoId = 0)
        {
            if (SiblingInfoId == 0)
                throw new ArgumentNullException("SiblingInfo");

            var SiblingInfo = (from s in db.SiblingInfoes where s.SiblingInfoId == SiblingInfoId select s).FirstOrDefault();
            db.SiblingInfoes.Remove(SiblingInfo);
            db.SaveChanges();
        }

        #endregion

        #region ExtraActivity

        public KSModel.Models.ExtraActivity GetExtraActivityById(int ExtraActivityId, bool IsTrack = false)
        {
            if (ExtraActivityId == 0)
                throw new ArgumentNullException("ExtraActivity");

            var ExtraActivity = new ExtraActivity();
            if (IsTrack)
                ExtraActivity = (from s in db.ExtraActivities where s.ExtraActivityId == ExtraActivityId select s).FirstOrDefault();
            else
                ExtraActivity = (from s in db.ExtraActivities.AsNoTracking() where s.ExtraActivityId == ExtraActivityId select s).FirstOrDefault();

            return ExtraActivity;
        }

        public List<KSModel.Models.ExtraActivity> GetAllExtraActivitys(ref int totalcount, string ExtraActivity = "", int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.ExtraActivities.ToList();
            if (!String.IsNullOrEmpty(ExtraActivity))
                query = query.Where(e => e.ExtraActivity1.ToLower().Contains(ExtraActivity.ToLower())).ToList();

            totalcount = query.Count;
            var ExtraActivities = new PagedList<KSModel.Models.ExtraActivity>(query, PageIndex, PageSize);
            return ExtraActivities;
        }

        public void InsertExtraActivity(KSModel.Models.ExtraActivity ExtraActivity)
        {
            if (ExtraActivity == null)
                throw new ArgumentNullException("ExtraActivity");

            db.ExtraActivities.Add(ExtraActivity);
            db.SaveChanges();
        }

        public void UpdateExtraActivity(KSModel.Models.ExtraActivity ExtraActivity)
        {
            if (ExtraActivity == null)
                throw new ArgumentNullException("ExtraActivity");

            db.Entry(ExtraActivity).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExtraActivity(int ExtraActivityId = 0)
        {
            if (ExtraActivityId == 0)
                throw new ArgumentNullException("ExtraActivity");

            var ExtraActivity = (from s in db.ExtraActivities where s.ExtraActivityId == ExtraActivityId select s).FirstOrDefault();
            db.ExtraActivities.Remove(ExtraActivity);
            db.SaveChanges();
        }

        #endregion

        #region StudentActivity

        public StudentActivity GetStudentActivityById(int StudentActivityId, bool IsTrack = false)
        {
            if (StudentActivityId == 0)
                throw new ArgumentNullException("StudentActivity");

            var StudentActivity = new StudentActivity();
            if (IsTrack)
                StudentActivity = (from s in db.StudentActivities where s.StudentActivityId == StudentActivityId select s).FirstOrDefault();
            else
                StudentActivity = (from s in db.StudentActivities.AsNoTracking() where s.StudentActivityId == StudentActivityId select s).FirstOrDefault();

            return StudentActivity;
        }

        public List<StudentActivity> GetAllStudentActivitys(ref int totalcount, int? StudentId = null,
            int? ExtraActivityId = null, DateTime? EffectiveDate = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StudentActivities.ToList();
            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();
            if (ExtraActivityId != null && ExtraActivityId > 0)
                query = query.Where(e => e.ExtraActivityId == ExtraActivityId).ToList();
            if (EffectiveDate.HasValue)
                query = query.Where(e => e.EffectiveDate == EffectiveDate).ToList();

            totalcount = query.Count;
            var StudentActivities = new PagedList<KSModel.Models.StudentActivity>(query, PageIndex, PageSize);
            return StudentActivities;
        }

        public void InsertStudentActivity(StudentActivity StudentActivity)
        {
            if (StudentActivity == null)
                throw new ArgumentNullException("StudentActivity");

            db.StudentActivities.Add(StudentActivity);
            db.SaveChanges();
        }

        public void UpdateStudentActivity(StudentActivity StudentActivity)
        {
            if (StudentActivity == null)
                throw new ArgumentNullException("StudentActivity");

            db.Entry(StudentActivity).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStudentActivity(int StudentActivityId = 0)
        {
            if (StudentActivityId == 0)
                throw new ArgumentNullException("StudentActivity");

            var StudentActivity = (from s in db.StudentActivities where s.StudentActivityId == StudentActivityId select s).FirstOrDefault();
            db.StudentActivities.Remove(StudentActivity);
            db.SaveChanges();
        }

        #endregion

        #region studentfullname

        public  string GetStudentFullName(int studentid)
        {
            var studentname = "";
            var student = db.Students.Where(p => p.StudentId == studentid).FirstOrDefault();

            if (student.Gender.Gender1.ToLower() == "female")
            {
                if (student.CustodyRight != null && (student.CustodyRight.CustodyRight1.ToLower() == "all" || student.CustodyRight.CustodyRight1.ToLower() == "guardian" || student.CustodyRight.CustodyRight1.ToLower() == "father"))
                {
                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "father"))
                        studentname = student.FName + " " + student.LName + " " + "D/o" + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault().FirstName + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault().LastName;
                    else if (student.Guardians.FirstOrDefault() != null)
                        studentname = student.FName + " " + student.LName + " " + "D/o" + " " + student.Guardians.FirstOrDefault().FirstName + " " + student.Guardians.FirstOrDefault().LastName;
                    else
                        studentname = student.FName + " " + student.LName;
                }
                else
                {
                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "mother"))
                        studentname = student.FName + " " + student.LName + " " + "D/o" + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault().FirstName + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault().LastName;
                    else if (student.Guardians.FirstOrDefault() != null)
                        studentname = student.FName + " " + student.LName + " " + "D/o" + " " + student.Guardians.FirstOrDefault().FirstName + " " + student.Guardians.FirstOrDefault().LastName;
                    else
                        studentname = student.FName + " " + student.LName;
                }
            }
            else
            {
                if (student.CustodyRight != null && (student.CustodyRight.CustodyRight1.ToLower() == "all" || student.CustodyRight.CustodyRight1.ToLower() == "guardian" || student.CustodyRight.CustodyRight1.ToLower() == "father"))
                {
                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "father"))
                        studentname = student.FName + " " + student.LName + " " + "S/o" + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault().FirstName + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault().LastName;
                    else if (student.Guardians.FirstOrDefault() != null)
                        studentname = student.FName + " " + student.LName + " " + "S/o" + " " + student.Guardians.FirstOrDefault().FirstName + " " + student.Guardians.FirstOrDefault().LastName;
                    else
                        studentname = student.FName + " " + student.LName;
                }
                else
                {
                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "mother"))
                        studentname = student.FName + " " + student.LName + " " + "S/o" + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault().FirstName + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault().LastName;
                    else if (student.Guardians.FirstOrDefault() != null)
                        studentname = student.FName + " " + student.LName + " " + "S/o" + " " + student.Guardians.FirstOrDefault().FirstName + " " + student.Guardians.FirstOrDefault().LastName;
                    else
                        studentname = student.FName + " " + student.LName;
                }
            }
            return studentname;
        }

        #endregion


        #endregion

    }
}