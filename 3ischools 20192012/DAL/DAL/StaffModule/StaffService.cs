﻿using DAL.DAL.Common;
using KSModel.Models;
using ViewModel.ViewModel.Staff;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.UI.WebControls;

namespace DAL.DAL.StaffModule
{
    public partial class StaffService : IStaffService
    {
        
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        //private readonly string SqlDataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }
        
        private readonly IConnectionService _IConnectionService;


        public StaffService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
          
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
        }

        #region Methods

        #region StaffType

        public StaffType GetStaffTypeById(int StaffTypeId, bool IsTrack = false)
        {
            if (StaffTypeId == 0)
                throw new ArgumentNullException("StaffType");
            var StaffType = new StaffType();
            if (IsTrack)
                StaffType = (from s in db.StaffTypes where s.StaffTypeId == StaffTypeId select s).FirstOrDefault();
            else
                StaffType = (from s in db.StaffTypes.AsNoTracking() where s.StaffTypeId == StaffTypeId select s).FirstOrDefault();
            return StaffType;
        }

        public List<KSModel.Models.StaffType> GetAllStaffTypes(ref int totalcount, string StaffType = "", 
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffTypes.ToList();
            if (!String.IsNullOrEmpty(StaffType))
                query = query.Where(e => e.StaffType1.ToLower().Contains(StaffType.ToLower())).ToList();
          
            totalcount = query.Count;
            var StaffTypes = new PagedList<KSModel.Models.StaffType>(query, PageIndex, PageSize);
            return StaffTypes;
        }
        public List<KSModel.Models.StaffType> GetAllStaffTypesUpdated(ref int totalcount,  bool IsTeacher = false,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffTypes.ToList();
            query = query.Where(e => e.IsTeacher == IsTeacher).ToList();
            totalcount = query.Count;
            var StaffTypes = new PagedList<KSModel.Models.StaffType>(query, PageIndex, PageSize);
            return StaffTypes;
        }


        public void InsertStaffType(KSModel.Models.StaffType StaffType)
        {
            if (StaffType == null)
                throw new ArgumentNullException("StaffType");

            db.StaffTypes.Add(StaffType);
            db.SaveChanges();
        }

        public void UpdateStaffType(KSModel.Models.StaffType StaffType)
        {
            if (StaffType == null)
                throw new ArgumentNullException("StaffType");

            db.Entry(StaffType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStaffType(int StaffTypeId = 0)
        {
            if (StaffTypeId == 0)
                throw new ArgumentNullException("StaffType");

            var StaffType = (from s in db.StaffTypes where s.StaffTypeId == StaffTypeId select s).FirstOrDefault();
            db.StaffTypes.Remove(StaffType);
            db.SaveChanges();
        }

        #endregion

        #region Staff

        public Staff GetStaffById(int StaffId, bool IsTrack = false)
        {
            if (StaffId == 0)
                throw new ArgumentNullException("Staff");
            var Staff = new Staff();
            if (IsTrack)
                Staff = (from s in db.Staffs where s.StaffId == StaffId select s).FirstOrDefault();
            else
                Staff = (from s in db.Staffs.AsNoTracking() where s.StaffId == StaffId select s).FirstOrDefault();

            return Staff;
        }

        public IList<KSModel.Models.Staff> GetAllStaffs(ref int totalcount, string Fname = "", string Lname = "", string Fullname = "",
            bool? IsActive = null, int? StaffTypeId = null, int? DepartmentId = null,
            int? DesignationId = null, int? PunchMcId = null, string EmpCode = "", DateTime? DOJ = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Staffs.AsQueryable();

            if (!String.IsNullOrEmpty(Fname))
                query = query.Where(e => e.FName.ToLower().Contains(Fname.ToLower()));
            if (!String.IsNullOrEmpty(Lname))
                query = query.Where(e => e.LName != null && e.LName.ToLower().Contains(Lname.ToLower()));
            if (!String.IsNullOrEmpty(Fullname))
                query = query.Where(e => (e.FName + " " + e.LName).ToLower().Contains(Fullname.ToLower()));
            if (!String.IsNullOrEmpty(EmpCode))
                query = query.Where(e => e.EmpCode.ToLower().Contains(EmpCode.ToLower()));
            if (StaffTypeId != null && StaffTypeId > 0)
                query = query.Where(e => e.StaffTypeId == StaffTypeId);
            if (DesignationId != null && DesignationId > 0)
                query = query.Where(e => e.DesignationId == DesignationId);
            if (DepartmentId != null && DepartmentId > 0)
                query = query.Where(e => e.DepartmentId == DepartmentId);
            if (DOJ.HasValue)
                query = query.Where(e => (e.DOJ <= DOJ) || (e.DOJ==null ));
           
            if (PunchMcId != null && PunchMcId > 0)
                query = query.Where(e => e.PunchMcId == PunchMcId);

            if (IsActive == true)
                query = query.Where(s => s.IsActive == true);
            else if (IsActive == false)
                query = query.Where(s => s.IsActive == false);
            else
                query = query.Where(s => s.IsActive == true || s.IsActive == false || s.IsActive == null);

            totalcount = query.Count();
            var Staffs = new PagedList<KSModel.Models.Staff>(query.ToList(), PageIndex, PageSize);
            return Staffs;
        }


        public List<StaffAttendanceReportModel> GetStaffForAttendanceReport(ref int totalcount, int? ListType = null,
                                   DateTime? Date = null,string AttendanceStatusIds = "",int PageIndex = 0, int PageSize = int.MaxValue)
        {
            // call store procedure for Advanced serach of books
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_getStaffAttendance", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ListType", ListType);
                    sqlComm.Parameters.AddWithValue("@Date", Date);
                    sqlComm.Parameters.AddWithValue("@AttendanceStatusId", AttendanceStatusIds);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<StaffAttendanceReportModel> GetAllStaffs = new List<StaffAttendanceReportModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                StaffAttendanceReportModel stafflist = new StaffAttendanceReportModel();
                stafflist.StaffId = Convert.ToInt32(dt.Rows[i]["StaffId"]);
                stafflist.StaffName = Convert.ToString(dt.Rows[i]["StaffName"]);
                stafflist.EmpCode = Convert.ToString(dt.Rows[i]["EmpCode"]);
                stafflist.Designation = Convert.ToString(dt.Rows[i]["Designation"]);
                stafflist.AttendanceStatus = Convert.ToString(dt.Rows[i]["AttendanceStatus"]);
                stafflist.AttendanceAbbr = Convert.ToString(dt.Rows[i]["AttendanceAbbr"]);
                stafflist.AttendanceInTime = Convert.ToString(dt.Rows[i]["InTime"]);
                stafflist.AttendanceOutTime = Convert.ToString(dt.Rows[i]["OutTime"]);
                if(Convert.ToString(dt.Rows[i]["AttendanceStatusId"])!=null && Convert.ToString(dt.Rows[i]["AttendanceStatusId"])!="")
                    stafflist.AttendanceStatusId= Convert.ToInt32(dt.Rows[i]["AttendanceStatusId"]);
                stafflist.IsAuto = "";
                if (!DBNull.Value.Equals(dt.Rows[i]["IsAuto"]))
                {
                    var val = Convert.ToBoolean(dt.Rows[i]["IsAuto"]);
                    stafflist.AttendanceMode = val ? "Automatic" : "Manual";
                    stafflist.IsAuto = val.ToString();
                }

                stafflist.IsOutAuto = "";
                if (!DBNull.Value.Equals(dt.Rows[i]["IsOutAuto"]))
                {
                    var val = Convert.ToBoolean(dt.Rows[i]["IsOutAuto"]);
                    stafflist.IsOutAuto = val.ToString();
                }


                GetAllStaffs.Add(stafflist);
            }
            totalcount = GetAllStaffs.Count;

            GetAllStaffs = new PagedList<StaffAttendanceReportModel>(GetAllStaffs, PageIndex, PageSize);
            return GetAllStaffs;


        }

        public void InsertStaff(KSModel.Models.Staff Staff)
        {
            if (Staff == null)
                throw new ArgumentNullException("Staff");

            db.Staffs.Add(Staff);
            db.SaveChanges();
        }

        public void UpdateStaff(KSModel.Models.Staff Staff)
        {
            if (Staff == null)
                throw new ArgumentNullException("Staff");

            db.Entry(Staff).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStaff(int StaffId = 0)
        {
            if (StaffId == 0)
                throw new ArgumentNullException("Staff");

            var Staff = (from s in db.Staffs where s.StaffId == StaffId select s).FirstOrDefault();
            db.Staffs.Remove(Staff);
            db.SaveChanges();
        }

        public string FullName(string Fname, string Lname)
        {
            StringBuilder fullname = new StringBuilder();
            if (!String.IsNullOrEmpty(Fname))
                fullname.Append(Fname);
            if (!String.IsNullOrEmpty(Lname))
                fullname.Append(Lname);

            return fullname.ToString();
        }

        #endregion

        #region StaffQualification

        public KSModel.Models.StaffQualification GetStaffQualificationById(int StaffQualificationId, bool IsTrack = false)
        {
            if (StaffQualificationId == 0)
                throw new ArgumentNullException("StaffQualification");
            var StaffQualification = new KSModel.Models.StaffQualification();
            if (IsTrack)
                StaffQualification = (from s in db.StaffQualifications where s.StaffQualificationId == StaffQualificationId select s).FirstOrDefault();
            else
                StaffQualification = (from s in db.StaffQualifications.AsNoTracking() where s.StaffQualificationId == StaffQualificationId select s).FirstOrDefault();

            return StaffQualification;
        }

        public List<KSModel.Models.StaffQualification> GetAllStaffQualifications(ref int totalcount,int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffQualifications.ToList();

           
            totalcount = query.Count;
            var StaffQualifications = new PagedList<KSModel.Models.StaffQualification>(query, PageIndex, PageSize);
            return StaffQualifications;
        }


        public void InsertStaffQualification(KSModel.Models.StaffQualification StaffQualification)
        {
            if (StaffQualification == null)
                throw new ArgumentNullException("StaffQualification");

            db.StaffQualifications.Add(StaffQualification);
            db.SaveChanges();
        }

        public void UpdateStaffQualification(KSModel.Models.StaffQualification StaffQualification)
        {
            if (StaffQualification == null)
                throw new ArgumentNullException("StaffQualification");

            db.Entry(StaffQualification).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStaffQualification(int StaffQualificationId = 0)
        {
            if (StaffQualificationId == 0)
                throw new ArgumentNullException("StaffQualification");

            var StaffQualification = (from s in db.StaffQualifications where s.StaffQualificationId == StaffQualificationId select s).FirstOrDefault();
            db.StaffQualifications.Remove(StaffQualification);
            db.SaveChanges();
        }



        #endregion
        #region StaffExperience

        public KSModel.Models.StaffExperience GetStaffExperienceById(int StaffExperienceId, bool IsTrack = false)
        {
            if (StaffExperienceId == 0)
                throw new ArgumentNullException("StaffExperience");
            var StaffExperience = new KSModel.Models.StaffExperience();
            if (IsTrack)
                StaffExperience = (from s in db.StaffExperiences where s.StaffExperienceId == StaffExperienceId select s).FirstOrDefault();
            else
                StaffExperience = (from s in db.StaffExperiences.AsNoTracking() where s.StaffExperienceId == StaffExperienceId select s).FirstOrDefault();

            return StaffExperience;
        }

        public List<KSModel.Models.StaffExperience> GetAllStaffExperiences(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffExperiences.ToList();


            totalcount = query.Count;
            var StaffExperiences = new PagedList<KSModel.Models.StaffExperience>(query, PageIndex, PageSize);
            return StaffExperiences;
        }


        public void InsertStaffExperience(KSModel.Models.StaffExperience StaffExperience)
        {
            if (StaffExperience == null)
                throw new ArgumentNullException("StaffExperience");

            db.StaffExperiences.Add(StaffExperience);
            db.SaveChanges();
        }

        public void UpdateStaffExperience(KSModel.Models.StaffExperience StaffExperience)
        {
            if (StaffExperience == null)
                throw new ArgumentNullException("StaffExperience");

            db.Entry(StaffExperience).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStaffExperience(int StaffExperienceId = 0)
        {
            if (StaffExperienceId == 0)
                throw new ArgumentNullException("StaffExperience");

            var StaffExperience = (from s in db.StaffExperiences where s.StaffExperienceId == StaffExperienceId select s).FirstOrDefault();
            db.StaffExperiences.Remove(StaffExperience);
            db.SaveChanges();
        }



        #endregion



        #region AttendanceStatus

        public StaffAttendanceStatu GetStaffAttendanceStatusById(int AttendanceStatusId, bool IsTrack = false)
        {
            if (AttendanceStatusId == 0)
                throw new ArgumentNullException("StaffAttendanceStatus");

            StaffAttendanceStatu AttendanceStatus = new StaffAttendanceStatu();
            if (IsTrack)
                AttendanceStatus = (from s in db.StaffAttendanceStatus where s.AttendanceStatusId == AttendanceStatusId select s).FirstOrDefault();
            else
                AttendanceStatus = (from s in db.StaffAttendanceStatus.AsNoTracking() where s.AttendanceStatusId == AttendanceStatusId select s).FirstOrDefault();

            return AttendanceStatus;
        }

        public List<StaffAttendanceStatu> GetStaffAllAttendanceStatus(ref int totalcount, string AttnStatus = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffAttendanceStatus.ToList();
            if (!String.IsNullOrEmpty(AttnStatus))
                query = query.Where(e => e.AttendanceStatus.ToLower().Contains(AttnStatus.ToLower())).ToList();

            totalcount = query.Count;
            var AttendanceStatus = new PagedList<KSModel.Models.StaffAttendanceStatu>(query, PageIndex, PageSize);
            return AttendanceStatus;
        }

        public void InsertStaffAttendanceStatus(StaffAttendanceStatu AttendanceStatus)
        {
            if (AttendanceStatus == null)
                throw new ArgumentNullException("StaffAttendanceStatus");

            db.StaffAttendanceStatus.Add(AttendanceStatus);
            db.SaveChanges();
        }

        public void UpdateStaffAttendanceStatus(StaffAttendanceStatu AttendanceStatus)
        {
            if (AttendanceStatus == null)
                throw new ArgumentNullException("StaffAttendanceStatus");

            db.Entry(AttendanceStatus).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStaffAttendanceStatus(int AttendanceStatusId = 0)
        {
            if (AttendanceStatusId == 0)
                throw new ArgumentNullException("StaffAttendanceStatus");

            var AttendanceStatus = (from s in db.StaffAttendanceStatus where s.AttendanceStatusId == AttendanceStatusId select s).FirstOrDefault();
            db.StaffAttendanceStatus.Remove(AttendanceStatus);
            db.SaveChanges();
        }

        #endregion

        #region TeacherClass

        public KSModel.Models.TeacherClass GetTeacherClassById(int TeacherClassId, bool IsTrack = false)
        {
            if (TeacherClassId == 0)
                throw new ArgumentNullException("TeacherClass");
            var TeacherClass = new TeacherClass();
            if (IsTrack)
                TeacherClass = (from s in db.TeacherClasses where s.TeacherClassId == TeacherClassId select s).FirstOrDefault();
            else
                TeacherClass = (from s in db.TeacherClasses.AsNoTracking() where s.TeacherClassId == TeacherClassId select s).FirstOrDefault();
            return TeacherClass;
        }

        public List<KSModel.Models.TeacherClass> GetAllTeacherClasss(ref int totalcount, int? TeacherId = null, int? StandardId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TeacherClasses.ToList();
            if (TeacherId != null && TeacherId > 0)
                query = query.Where(e => e.TeacherId == TeacherId).ToList();
            if (StandardId != null && StandardId > 0)
                query = query.Where(e => e.StandardId == StandardId).ToList();

            totalcount = query.Count;
            var TeacherClasses = new PagedList<KSModel.Models.TeacherClass>(query, PageIndex, PageSize);
            return TeacherClasses;
        }

        public void InsertTeacherClass(KSModel.Models.TeacherClass TeacherClass)
        {
            if (TeacherClass == null)
                throw new ArgumentNullException("TeacherClass");

            db.TeacherClasses.Add(TeacherClass);
            db.SaveChanges();
        }

        public void UpdateTeacherClass(KSModel.Models.TeacherClass TeacherClass)
        {
            if (TeacherClass == null)
                throw new ArgumentNullException("TeacherClass");

            db.Entry(TeacherClass).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTeacherClass(int TeacherClassId = 0)
        {
            if (TeacherClassId == 0)
                throw new ArgumentNullException("TeacherClass");

            var TeacherClass = (from s in db.TeacherClasses where s.TeacherClassId == TeacherClassId select s).FirstOrDefault();
            db.TeacherClasses.Remove(TeacherClass);
            db.SaveChanges();
        }

        #endregion

        #region ClassTeacher

        public KSModel.Models.ClassTeacher GetClassTeacherById(int ClassTeacherId, bool IsTrack = false)
        {
            if (ClassTeacherId == 0)
                throw new ArgumentNullException("ClassTeacher");
            var ClassTeacher = new ClassTeacher();
            if (IsTrack)
                ClassTeacher = (from s in db.ClassTeachers where s.ClassTeacherId == ClassTeacherId select s).FirstOrDefault();
            else
                ClassTeacher = (from s in db.ClassTeachers.AsNoTracking() where s.ClassTeacherId == ClassTeacherId select s).FirstOrDefault();
            return ClassTeacher;
        }

        public List<KSModel.Models.ClassTeacher> GetAllClassTeachers(ref int totalcount, int? ClassId = null, int? TeacherId = null,
            DateTime? EffectiveDate = null, DateTime? EndDate = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ClassTeachers.ToList();
            if (ClassId != null && ClassId > 0)
                query = query.Where(e => e.ClassId == ClassId).ToList();
            if (TeacherId != null && TeacherId > 0)
                query = query.Where(e => e.TeacherId == TeacherId).ToList();
            if (EffectiveDate.HasValue)
                query = query.Where(e => e.EffectiveDate == EffectiveDate).ToList();
            if (EndDate.HasValue)
                query = query.Where(e => e.EndDate == EndDate).ToList();

            totalcount = query.Count;
            var ClassTeachers = new PagedList<KSModel.Models.ClassTeacher>(query, PageIndex, PageSize);
            return ClassTeachers;
        }

        public void InsertClassTeacher(KSModel.Models.ClassTeacher ClassTeacher)
        {
            if (ClassTeacher == null)
                throw new ArgumentNullException("ClassTeacher");

            db.ClassTeachers.Add(ClassTeacher);
            db.SaveChanges();
        }

        public void UpdateClassTeacher(KSModel.Models.ClassTeacher ClassTeacher)
        {
            if (ClassTeacher == null)
                throw new ArgumentNullException("ClassTeacher");

            db.Entry(ClassTeacher).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteClassTeacher(int ClassTeacherId = 0)
        {
            if (ClassTeacherId == 0)
                throw new ArgumentNullException("ClassTeacher");

            var ClassTeacher = (from s in db.ClassTeachers where s.ClassTeacherId == ClassTeacherId select s).FirstOrDefault();
            db.ClassTeachers.Remove(ClassTeacher);
            db.SaveChanges();
        }

        #endregion

        #region TeacherSubject

        public KSModel.Models.TeacherSubject GetTeacherSubjectById(int TeacherSubjectId, bool IsTrack = false)
        {
            if (TeacherSubjectId == 0)
                throw new ArgumentNullException("TeacherSubject");
            var TeacherSubject = new TeacherSubject();
            if (IsTrack)
                TeacherSubject = (from s in db.TeacherSubjects where s.TeacherSubjectId == TeacherSubjectId select s).FirstOrDefault();
            else
                TeacherSubject = (from s in db.TeacherSubjects.AsNoTracking() where s.TeacherSubjectId == TeacherSubjectId select s).FirstOrDefault();
            return TeacherSubject;
        }

        public List<KSModel.Models.TeacherSubject> GetAllTeacherSubjects(ref int totalcount, int? SubjectId = null, int? TeacherId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TeacherSubjects.ToList();
            if (SubjectId != null && SubjectId > 0)
                query = query.Where(e => e.SubjectId == SubjectId).ToList();
            if (TeacherId != null && TeacherId > 0)
                query = query.Where(e => e.TeacherId == TeacherId).ToList();

            totalcount = query.Count;
            var TeacherSubjects = new PagedList<KSModel.Models.TeacherSubject>(query, PageIndex, PageSize);
            return TeacherSubjects;
        }

        public void InsertTeacherSubject(KSModel.Models.TeacherSubject TeacherSubject)
        {
            if (TeacherSubject == null)
                throw new ArgumentNullException("TeacherSubject");

            db.TeacherSubjects.Add(TeacherSubject);
            db.SaveChanges();
        }

        public void UpdateTeacherSubject(KSModel.Models.TeacherSubject TeacherSubject)
        {
            if (TeacherSubject == null)
                throw new ArgumentNullException("TeacherSubject");

            db.Entry(TeacherSubject).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTeacherSubject(int TeacherSubjectId = 0)
        {
            if (TeacherSubjectId == 0)
                throw new ArgumentNullException("TeacherSubject");

            var TeacherSubject = (from s in db.TeacherSubjects where s.TeacherSubjectId == TeacherSubjectId select s).FirstOrDefault();
            db.TeacherSubjects.Remove(TeacherSubject);
            db.SaveChanges();
        }

        #endregion

        #region TeacherSubjectAssignment

        public KSModel.Models.TeacherSubjectAssign GetTeacherSubjectAssignById(int TeacherSubjectAssignId, bool IsTrack = false)
        {
            if (TeacherSubjectAssignId == 0)
                throw new ArgumentNullException("TeacherSubjectAssign");
            var TeacherSubjectAssign = new TeacherSubjectAssign();
            if (IsTrack)
                TeacherSubjectAssign = (from s in db.TeacherSubjectAssigns where s.TeacherSubjectAssignId == TeacherSubjectAssignId select s).FirstOrDefault();
            else
                TeacherSubjectAssign = (from s in db.TeacherSubjectAssigns.AsNoTracking() where s.TeacherSubjectAssignId == TeacherSubjectAssignId select s).FirstOrDefault();
            return TeacherSubjectAssign;
        }

        public List<KSModel.Models.TeacherSubjectAssign> GetAllTeacherSubjectAssigns(ref int totalcount, int? TeacherId = null, 
             int? ClassId = null, int? VirtualId = null, int? SubjectId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TeacherSubjectAssigns.ToList();
            if (SubjectId != null && SubjectId > 0)
                query = query.Where(e => e.SubjectId == SubjectId).ToList();
            if (TeacherId != null && TeacherId > 0)
                query = query.Where(e => e.TeacherId == TeacherId).ToList();
            if (VirtualId != null && VirtualId > 0)
                query = query.Where(e => e.VirtualClassId == VirtualId).ToList();
            if (ClassId != null && ClassId > 0)
                query = query.Where(e => e.ClassId == ClassId).ToList();

            totalcount = query.Count;
            var TeacherSubjectAssigns = new PagedList<KSModel.Models.TeacherSubjectAssign>(query, PageIndex, PageSize);
            return TeacherSubjectAssigns;
        }

        public void InsertTeacherSubjectAssign(KSModel.Models.TeacherSubjectAssign TeacherSubjectAssign)
        {
            if (TeacherSubjectAssign == null)
                throw new ArgumentNullException("TeacherSubjectAssign");

            db.TeacherSubjectAssigns.Add(TeacherSubjectAssign);
            db.SaveChanges();
        }

        public void UpdateTeacherSubjectAssign(KSModel.Models.TeacherSubjectAssign TeacherSubjectAssign)
        {
            if (TeacherSubjectAssign == null)
                throw new ArgumentNullException("TeacherSubjectAssign");

            db.Entry(TeacherSubjectAssign).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTeacherSubjectAssign(int TeacherSubjectAssignId = 0)
        {
            if (TeacherSubjectAssignId == 0)
                throw new ArgumentNullException("TeacherSubjectAssign");

            var TeacherSubjectAssign = (from s in db.TeacherSubjectAssigns where s.TeacherSubjectAssignId == TeacherSubjectAssignId 
                                            select s).FirstOrDefault();
            db.TeacherSubjectAssigns.Remove(TeacherSubjectAssign);
            db.SaveChanges();
        }

        #endregion

        #region StaffDocument
        public List<StaffDocumentType> GetAllStaffDocumentTypes(ref int totalcount, string DocumentType = "", bool IsmultipleAllowed = false, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffDocumentTypes.ToList();
            if (!String.IsNullOrEmpty(DocumentType))
                query = query.Where(e => e.DocumentType.ToLower().Contains(DocumentType.ToLower())).ToList();
            if (IsmultipleAllowed)
                query = query.Where(e => e.IsMultipleAllowed == IsmultipleAllowed).ToList();
            totalcount = query.Count;
            var DocumentTypes = new PagedList<KSModel.Models.StaffDocumentType>(query, PageIndex, PageSize);
            return DocumentTypes;
        }
        public StaffDocument GetStaffDocumentById(int StaffDocumentId, bool IsTrack = false)
        {
            if (StaffDocumentId == 0)
                throw new ArgumentNullException("StaffDocument");

            var StaffDocument = new StaffDocument();
            if (IsTrack)
                StaffDocument = (from s in db.StaffDocuments where s.DocumentId == StaffDocumentId select s).FirstOrDefault();
            else
                StaffDocument = (from s in db.StaffDocuments.AsNoTracking() where s.DocumentId == StaffDocumentId select s).FirstOrDefault();

            return StaffDocument;
        }

        public List<StaffDocument> GetAllStaffDocuments(ref int totalcount, int? DocumentTypeId = null, string Image = "",
            int? StaffId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffDocuments.ToList();
            if (!String.IsNullOrEmpty(Image))
                query = query.Where(e => e.Image.ToLower() == Image.ToLower()).ToList();
            if (DocumentTypeId != null && DocumentTypeId > 0)
                query = query.Where(e => e.DocumentTypeId == DocumentTypeId).ToList();
            if (StaffId != null && StaffId > 0)
                query = query.Where(e => e.StaffId == StaffId).ToList();

            totalcount = query.Count;
            var StaffDocuments = new PagedList<KSModel.Models.StaffDocument>(query, PageIndex, PageSize);
            return StaffDocuments;
        }

        public void InsertStaffDocument(StaffDocument StaffDocument)
        {
            if (StaffDocument == null)
                throw new ArgumentNullException("StaffDocument");

            db.StaffDocuments.Add(StaffDocument);
            db.SaveChanges();
        }

        public void UpdateStaffDocument(StaffDocument StaffDocument)
        {
            if (StaffDocument == null)
                throw new ArgumentNullException("StaffDocument");

            db.Entry(StaffDocument).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStaffDocument(int StaffDocumentId = 0)
        {
            if (StaffDocumentId == 0)
                throw new ArgumentNullException("StaffDocument");

            var StaffDocument = (from s in db.StaffDocuments where s.DocumentId == StaffDocumentId select s).FirstOrDefault();
            db.StaffDocuments.Remove(StaffDocument);
            db.SaveChanges();
        }
        #endregion

        #region StaffLeaveApplication
        public StaffLeaveApplication GetStaffLeaveApplicationbyLeavDate(StaffLeaveApplication StaffLeaveApplication)
        {
            StaffLeaveApplication LeaveApplication = new StaffLeaveApplication();
            var LeaveApplications = db.StaffLeaveApplications.Where(m => (m.LeaveFrom >= StaffLeaveApplication.LeaveFrom 
                && m.LeaveFrom <= StaffLeaveApplication.LeaveTill) || (m.LeaveTill >= StaffLeaveApplication.LeaveFrom 
                && m.LeaveTill <= StaffLeaveApplication.LeaveTill) || (m.LeaveFrom >= StaffLeaveApplication.LeaveFrom 
                && m.LeaveTill <= StaffLeaveApplication.LeaveTill) || (m.LeaveFrom <= StaffLeaveApplication.LeaveFrom 
                && m.LeaveTill >= StaffLeaveApplication.LeaveTill)).Where(x=>x.StaffId == StaffLeaveApplication.StaffId).FirstOrDefault();

               // LeaveApplication = (from s in db.StaffLeaveApplications where s.LeaveFrom == StaffLeaveApplication.LeaveFrom && s.StaffId==StaffLeaveApplication.StaffId select s).FirstOrDefault();          
            return LeaveApplications;
        }
        public StaffLeaveApplication GetStaffLeaveApplicationById(int StaffLeaveApplicationId, bool IsTrack = false)
        {
            if (StaffLeaveApplicationId == 0)
                throw new ArgumentNullException("StaffLeaveApplication");

            StaffLeaveApplication LeaveApplication = new StaffLeaveApplication();
            if (IsTrack)
                LeaveApplication = (from s in db.StaffLeaveApplications where s.StaffLeaveApplicationId == StaffLeaveApplicationId select s).FirstOrDefault();
            else
                LeaveApplication = (from s in db.StaffLeaveApplications.AsNoTracking() where s.StaffLeaveApplicationId == StaffLeaveApplicationId select s).FirstOrDefault();

            return LeaveApplication;
        }

        public List<StaffLeaveApplication> GetAllStaffLeaveApplication(ref int totalcount, int? StaffId = default(int?), 
            DateTime? LeaveFrom = default(DateTime?), DateTime? LeaveTill = default(DateTime?),
            DateTime? AppliedDate = default(DateTime?), string Reason = null, bool? IsApproved = null,
            int? LeaveTypeId = default(int?), int? ApprovedBy = default(int?), 
            DateTime? ApprovedOn = default(DateTime?), int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffLeaveApplications.ToList();
            if (!String.IsNullOrEmpty(Reason))
                query = query.Where(e => e.Reason.ToLower().Contains(Reason.ToLower())).ToList();
            if (StaffId != null && StaffId > 0)
                query = query.Where(e => e.StaffId == StaffId).ToList();
            if (LeaveTypeId != null && LeaveTypeId > 0)
                query = query.Where(e => e.LeaveTypeId == LeaveTypeId).ToList();
            if (ApprovedBy != null && ApprovedBy > 0)
                query = query.Where(e => e.ApprovedBy == ApprovedBy).ToList();
            if (LeaveFrom.HasValue)
                query = query.Where(e => e.LeaveFrom == LeaveFrom).ToList();
            if (LeaveTill.HasValue)
                query = query.Where(e => e.LeaveTill == LeaveTill).ToList();
            if (AppliedDate.HasValue)
                query = query.Where(e => e.AppliedDate == AppliedDate).ToList();
            if (IsApproved != null)
                query = query.Where(e => e.IsApproved == IsApproved).ToList();

            //if (ApprovedOn.HasValue)
            //    query.Where(e => e.ApprovedOn == ApprovedOn).ToList();

            totalcount = query.Count;
            var staffleaveapplications = new PagedList<KSModel.Models.StaffLeaveApplication>(query, PageIndex, PageSize);
            return staffleaveapplications;
        }

        public void InsertStaffLeaveApplication(StaffLeaveApplication StaffLeave)
        {
            if (StaffLeave == null)
                throw new ArgumentNullException("StaffLeaveApplication");

            db.StaffLeaveApplications.Add(StaffLeave);
            db.SaveChanges();
        }

        public void UpdateStaffLeaveApplication(StaffLeaveApplication StaffLeave)
        {
            if (StaffLeave == null)
                throw new ArgumentNullException("StaffLeaveApplication");

            db.Entry(StaffLeave).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStaffLeaveApplication(int StaffLeaveApplicationId = 0)
        {
            if (StaffLeaveApplicationId == 0)
                throw new ArgumentNullException("StaffLeaveApplication");

            var StaffLeaveApplication = (from s in db.StaffLeaveApplications where s.StaffLeaveApplicationId == StaffLeaveApplicationId select s).FirstOrDefault();
            db.StaffLeaveApplications.Remove(StaffLeaveApplication);
            db.SaveChanges();
        }
        #endregion

        #region Department
        public Department GetDepartmentById(int DepartmentId, bool IsTrack = false)
        {
            if (DepartmentId == 0)
                throw new ArgumentNullException("Department");

            Department Department = new Department();
            if (IsTrack)
                Department = (from s in db.Departments where s.DepartmentId == DepartmentId select s).FirstOrDefault();
            else
                Department = (from s in db.Departments.AsNoTracking() where s.DepartmentId == DepartmentId select s).FirstOrDefault();

            return Department;
        }

        public List<Department> GetAllDepartments(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Departments.ToList();
            totalcount = query.Count;
            var Departments = new PagedList<KSModel.Models.Department>(query, PageIndex, PageSize);
            return Departments;
        }

        public void InsertDepartment(Department Department)
        {
            if (Department == null)
                throw new ArgumentNullException("Department");

            db.Departments.Add(Department);
            db.SaveChanges();
        }

        public void UpdateDepartment(Department Department)
        {
            if (Department == null)
                throw new ArgumentNullException("Department");

            db.Entry(Department).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteDepartment(int DepartmentId = 0)
        {
            if (DepartmentId == 0)
                throw new ArgumentNullException("Department");

            var Department = (from s in db.Departments where s.DepartmentId == DepartmentId select s).FirstOrDefault();
            db.Departments.Remove(Department);
            db.SaveChanges();
        }
        #endregion

        #region Designation

        public Designation GetDesignationById(int DesignationId, bool IsTrack = false)
        {
            if (DesignationId == 0)
                throw new ArgumentNullException("Designation");

            Designation Designation = new Designation();
            if (IsTrack)
                Designation = (from s in db.Designations where s.DesignationId == DesignationId select s).FirstOrDefault();
            else
                Designation = (from s in db.Designations.AsNoTracking() where s.DesignationId == DesignationId select s).FirstOrDefault();

            return Designation;
        }

        public List<Designation> GetAllDesignations(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue, int StafftypeId = 0)
        {
            var query = db.Designations.ToList();
            if(StafftypeId>0)
            {

                query = query.Where(p => p.StaffTypeId == StafftypeId).ToList();
            }
            totalcount = query.Count;
            var Designations = new PagedList<KSModel.Models.Designation>(query, PageIndex, PageSize);
            return Designations;
        }

        public void InsertDesignation(Designation Designation)
        {
            if (Designation == null)
                throw new ArgumentNullException("Designation");

            db.Designations.Add(Designation);
            db.SaveChanges();
        }

        public void UpdateDesignation(Designation Designation)
        {
            if (Designation == null)
                throw new ArgumentNullException("Designation");

            db.Entry(Designation).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteDesignation(int DesignationId = 0)
        {
            if (DesignationId == 0)
                throw new ArgumentNullException("Designation");

            var Designation = (from s in db.Designations where s.DesignationId == DesignationId select s).FirstOrDefault();
            db.Designations.Remove(Designation);
            db.SaveChanges();
        }

        #region StaffAttendance

        public StaffAttendance GetStaffAttendanceById(int StaffAttendanceId, bool IsTrack = false)
        {
            if (StaffAttendanceId == 0)
                throw new ArgumentNullException("StaffAttendance");

            StaffAttendance Attendance = new StaffAttendance();
            if (IsTrack)
                Attendance = (from s in db.StaffAttendances where s.StaffAttendanceId == StaffAttendanceId select s).FirstOrDefault();
            else
                Attendance = (from s in db.StaffAttendances.AsNoTracking() where s.StaffAttendanceId == StaffAttendanceId select s).FirstOrDefault();
            return Attendance;
        }

        public IList<StaffAttendance> GetAllStaffAttendances(ref int totalcount, int? StaffId = null, DateTime? AttendanceDate = null,
            int? AttendanceStatusId = null,int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffAttendances.AsQueryable();
            if (StaffId != null && StaffId > 0)
                query = query.Where(e => e.StaffId == StaffId);
            if (AttendanceDate.HasValue)
                query = query.Where(e => e.AttendanceDate == AttendanceDate);
            if (AttendanceStatusId != null && AttendanceStatusId > 0)
                query = query.Where(e => e.AttendanceStatusId == AttendanceStatusId);

            totalcount = query.Count();
            var Attendances = new PagedList<KSModel.Models.StaffAttendance>(query.ToList(), PageIndex, PageSize);
            return Attendances;
        }


        public DataSet GetSummaryRept(ref int totalcount, string Measurement = "")
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_getStaffAttendanceSummaryRep", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.Add(new SqlParameter("@Meaurement", Measurement));
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    conn.Close();
                }
            }
            return ds;

        }

        public void InsertStaffAttendance(StaffAttendance Attendance)
        {
            if (Attendance == null)
                throw new ArgumentNullException("StaffAttendance");

            db.StaffAttendances.Add(Attendance);
            db.SaveChanges();
        }

        public void UpdateStaffAttendance(StaffAttendance Attendance)
        {
            if (Attendance == null)
                throw new ArgumentNullException("StaffAttendance");

            db.Entry(Attendance).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStaffAttendance(int StaffAttendanceId = 0)
        {
            if (StaffAttendanceId == 0)
                throw new ArgumentNullException("StaffAttendance");

            var Attendance = (from s in db.StaffAttendances where s.StaffAttendanceId == StaffAttendanceId select s).FirstOrDefault();
            db.StaffAttendances.Remove(Attendance);
            db.SaveChanges();
        }

        #endregion

        #endregion

        #region StaffCategory

        public StaffCategory GetStaffCategoryById(int StaffCategoryId, bool IsTrack = false)
        {
            if (StaffCategoryId == 0)
                throw new ArgumentNullException("StaffCategory");

            StaffCategory StaffCategory = new StaffCategory();
            if (IsTrack)
                StaffCategory = (from s in db.StaffCategories where s.Id == StaffCategoryId select s).FirstOrDefault();
            else
                StaffCategory = (from s in db.StaffCategories.AsNoTracking() where s.Id == StaffCategoryId select s).FirstOrDefault();

            return StaffCategory;
        }

        public List<StaffCategory> GetAllStaffCategories(ref int totalcount, string StaffCategory = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffCategories.ToList();
            if (!string.IsNullOrEmpty(StaffCategory))
                query = query.Where(s => s.CategoryName.ToLower() == StaffCategory.ToLower()).ToList();

            totalcount = query.Count;
            var StaffCategories = new PagedList<KSModel.Models.StaffCategory>(query, PageIndex, PageSize);
            return StaffCategories;
        }

        public void InsertStaffCategory(StaffCategory StaffCategory)
        {
            if (StaffCategory == null)
                throw new ArgumentNullException("StaffCategory");

            db.StaffCategories.Add(StaffCategory);
            db.SaveChanges();
        }

        public void UpdateStaffCategory(StaffCategory StaffCategory)
        {
            if (StaffCategory == null)
                throw new ArgumentNullException("StaffCategory");

            db.Entry(StaffCategory).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStaffCategory(int StaffCategoryId = 0)
        {
            if (StaffCategoryId == 0)
                throw new ArgumentNullException("Designation");

            var StaffCategory = (from s in db.StaffCategories where s.Id == StaffCategoryId select s).FirstOrDefault();
            db.StaffCategories.Remove(StaffCategory);
            db.SaveChanges();
        }

        #endregion

        #region StaffStatusDetail
        public StaffStatusDetail GetStaffStatusDetailById(int StaffStatusId)
        {
            if (StaffStatusId == 0)
                throw new ArgumentNullException("StaffStatusDetail");

            StaffStatusDetail StaffStatus = new StaffStatusDetail();

            if (StaffStatusId != 0 && StaffStatusId != null)
                StaffStatus = (from s in db.StaffStatusDetails where s.StaffStatusDetailId == StaffStatusId select s).FirstOrDefault();
            else
                StaffStatus = (from s in db.StaffStatusDetails.AsNoTracking() where s.StaffStatusDetailId == StaffStatusId select s).FirstOrDefault();

            return StaffStatus;
        }

        public void InsertStaffStatus(StaffStatusDetail StaffStatusDetail)
        {
            if (StaffStatusDetail == null)
                throw new ArgumentNullException("StaffStatusDetail");

            db.StaffStatusDetails.Add(StaffStatusDetail);
            db.SaveChanges();
        }

        public void UpdateStaffStatus(StaffStatusDetail StaffStatusDetail)
        {
            if (StaffStatusDetail == null)
                throw new ArgumentNullException("StaffStatusDetail");

            db.Entry(StaffStatusDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStaffStatus(int StaffStatusId = 0)
        {
            if (StaffStatusId == 0)
                throw new ArgumentNullException("StaffStatusDetail");

            var StaffStatusDetail = (from s in db.StaffStatusDetails where s.StaffStatusDetailId == StaffStatusId select s).FirstOrDefault();
            db.StaffStatusDetails.Remove(StaffStatusDetail);
            db.SaveChanges();
        }

        public List<StaffStatusDetail> GetAllStaffStatusDetail(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffStatusDetails.ToList();

            totalcount = query.Count;
            var StaffStatusDetail = new PagedList<KSModel.Models.StaffStatusDetail>(query.ToList(), PageIndex, PageSize);
            return StaffStatusDetail;
        }
        #endregion

        #endregion

        public DataTable ExportStaffListWithFilter(ref int totalcount, int? StaffTypeId = null, int? DepartmentId = null, int? DesignationId = null,string SearchBox=null, string EmpCode="", string IsActive = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                string isactivestaff = "";
                if (IsActive == "true")
                    isactivestaff = "1";
                if (IsActive == "false")
                    isactivestaff = "0";
                using (SqlCommand sqlComm = new SqlCommand("sp_ExportStaffListWithfilters", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@StaffTypeId", StaffTypeId);
                    sqlComm.Parameters.AddWithValue("@DepartmentId", DepartmentId);
                    sqlComm.Parameters.AddWithValue("@DesignationId", DesignationId);
                    sqlComm.Parameters.AddWithValue("@SearchBox", SearchBox);
                    sqlComm.Parameters.AddWithValue("@EmpCode", EmpCode);
                    sqlComm.Parameters.AddWithValue("@IsActive", isactivestaff);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            
            return dt;
        }
        public List<StaffTimeTable> StaffAssignedTimeTable(ref int totalcount,
          int PageIndex = 0, int PageSize = int.MaxValue, int StaffId = 0)
        {
            // call store procedure for Advanced serach of books
            DataTable dt = new DataTable();
            if (StaffId > 0)
            {
                using (SqlConnection conn = new SqlConnection(SqlDataSource))
                {
                    conn.Open();
                    SqlCommand sqlComm = new SqlCommand();
                    sqlComm = new SqlCommand("SELECT Timetable.EffectiveFrom as TimeTableFrom, Timetable.EffectiveTill as TimeTableTill,CASE WHEN ClassMaster.Class IS NOT NULL THEN ClassMaster.Class  WHEN ClassMaster.Class IS NULL THEN VC.VirtualClass END Class, Period.Period, Subject.Subject, RTRIM(Staff.FName + ' ' + ISNULL(Staff.LName, '')) AS Teacher, DayMaster.DayName, ClassPeriodDetail.ClassPeriodDetailId FROM    ClassPeriodDetail INNER JOIN TimeTable ON ClassPeriodDetail.TimeTableId = TimeTable.TimeTableId LEFT JOIN ClassSubject CS LEFT JOIN VirtualClass VC on VC.VirtualClassId = CS.VirtualClassId INNER JOIN ClassPeriod ON CS.ClassSubjectId = ClassPeriod.ClassSubjectId LEFT JOIN ClassMaster ON CS.ClassId = ClassMaster.ClassId ON ClassPeriodDetail.ClassPeriodId = ClassPeriod.ClassPeriodId INNER JOIN Staff ON ClassPeriodDetail.TeacherId = Staff.StaffId INNER JOIN ClassPeriodDay ON ClassPeriodDetail.ClassPeriodDetailId = ClassPeriodDay.ClassPeriodDetailId INNER JOIN Subject ON CS.SubjectId = Subject.SubjectId INNER JOIN Period ON ClassPeriod.PeriodId = Period.PeriodId INNER JOIN DayMaster ON ClassPeriodDay.DayId = DayMaster.DayId where Staff.StaffId = "+StaffId+" and TimeTable.TimeTableId in ( select TimeTableId from TimeTable where EffectiveFrom >= getdate() or EffectiveTill >= getdate())", conn);
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = sqlComm;
                    adapter.Fill(dt);
                    conn.Close();
                }
            }
            List<StaffTimeTable> GetAllStaffTimeTableList = new List<StaffTimeTable>();
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    StaffTimeTable stafflist = new StaffTimeTable();
                    stafflist.TimeTableFrom = "";
                    stafflist.TimeTableTill = "";
                    if (dt.Rows[i]["TimeTableFrom"] != null && dt.Rows[i]["TimeTableFrom"] != "")
                    {
                        var ttfrm = Convert.ToDateTime(dt.Rows[i]["TimeTableFrom"]);
                            stafflist.TimeTableFrom = ttfrm.Date.ToString("dd/MM/yyyy");
                    }
                    if (dt.Rows[i]["TimeTableTill"] != null && dt.Rows[i]["TimeTableTill"] != "")
                    {
                        var tttill = Convert.ToDateTime(dt.Rows[i]["TimeTableTill"]);
                        stafflist.TimeTableTill = tttill.Date.ToString("dd/MM/yyyy");
                    }
                    stafflist.Class = Convert.ToString(dt.Rows[i]["Class"]);
                    stafflist.Period = Convert.ToString(dt.Rows[i]["Period"]);
                    stafflist.Subject = Convert.ToString(dt.Rows[i]["Subject"]);
                    stafflist.Teacher = Convert.ToString(dt.Rows[i]["Teacher"]);
                    stafflist.DayName = Convert.ToString(dt.Rows[i]["DayName"]);
                    stafflist.ClassPeriodDetail = Convert.ToString(dt.Rows[i]["ClassPeriodDetailId"]);
                    GetAllStaffTimeTableList.Add(stafflist);
                }
            }
            totalcount = GetAllStaffTimeTableList.Count;
            GetAllStaffTimeTableList = new PagedList<StaffTimeTable>(GetAllStaffTimeTableList, PageIndex, PageSize);
            return GetAllStaffTimeTableList;

        }

        #region StaffBankDetail
        public StaffBankDetail GetStaffBankDetailById(int StaffBankDetailId)
        {
            if (StaffBankDetailId == 0)
                throw new ArgumentNullException("StaffBankDetail");

            StaffBankDetail StaffStatus = new StaffBankDetail();

            if (StaffBankDetailId != 0 && StaffBankDetailId != null)
                StaffStatus = (from s in db.StaffBankDetails where s.StaffBankDetailId == StaffBankDetailId select s).FirstOrDefault();
            else
                StaffStatus = (from s in db.StaffBankDetails.AsNoTracking() where s.StaffBankDetailId == StaffBankDetailId select s).FirstOrDefault();

            return StaffStatus;
        }

        public void InsertStaffBankDetail(StaffBankDetail StaffBankDetail)
        {
            if (StaffBankDetail == null)
                throw new ArgumentNullException("StaffBankDetail");

            db.StaffBankDetails.Add(StaffBankDetail);
            db.SaveChanges();
        }

        public void UpdateStaffBankDetail(StaffBankDetail StaffBankDetail)
        {
            if (StaffBankDetail == null)
                throw new ArgumentNullException("StaffBankDetail");

            db.Entry(StaffBankDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStaffBankDetail(int StaffBankDetailId = 0)
        {
            if (StaffBankDetailId == 0)
                throw new ArgumentNullException("StaffBankDetail");

            var StaffBankDetail = (from s in db.StaffBankDetails where s.StaffBankDetailId == StaffBankDetailId select s).FirstOrDefault();
            db.StaffBankDetails.Remove(StaffBankDetail);
            db.SaveChanges();
        }

        public List<StaffBankDetail> GetAllStaffBankDetail(ref int totalcount, int StaffId = 0,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffBankDetails.ToList();
            if (StaffId > 0)
                query = query.Where(f => f.StaffId == StaffId).ToList();

            totalcount = query.Count;
            var StaffBankDetail = new PagedList<KSModel.Models.StaffBankDetail>(query.ToList(), PageIndex, PageSize);
            return StaffBankDetail;
        }
        #endregion

        #region StaffServiceTypeDetail
        public StaffServiceTypeDetail GetStaffServiceTypeDetailById(int StaffServiceTypeDetailId)
        {
            if (StaffServiceTypeDetailId == 0)
                throw new ArgumentNullException("StaffServiceTypeDetail");

            StaffServiceTypeDetail StaffStatus = new StaffServiceTypeDetail();

            if (StaffServiceTypeDetailId != 0 && StaffServiceTypeDetailId != null)
                StaffStatus = (from s in db.StaffServiceTypeDetails where s.StaffServiceTypeDetailId == StaffServiceTypeDetailId select s).FirstOrDefault();
            else
                StaffStatus = (from s in db.StaffServiceTypeDetails.AsNoTracking() where s.StaffServiceTypeDetailId == StaffServiceTypeDetailId select s).FirstOrDefault();

            return StaffStatus;
        }

        public void InsertStaffServiceTypeDetail(StaffServiceTypeDetail StaffServiceTypeDetail)
        {
            if (StaffServiceTypeDetail == null)
                throw new ArgumentNullException("StaffServiceTypeDetail");

            db.StaffServiceTypeDetails.Add(StaffServiceTypeDetail);
            db.SaveChanges();
        }

        public void UpdateStaffServiceTypeDetail(StaffServiceTypeDetail StaffServiceTypeDetail)
        {
            if (StaffServiceTypeDetail == null)
                throw new ArgumentNullException("StaffServiceTypeDetail");

            db.Entry(StaffServiceTypeDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStaffServiceTypeDetail(int StaffServiceTypeDetailId = 0)
        {
            if (StaffServiceTypeDetailId == 0)
                throw new ArgumentNullException("StaffServiceTypeDetail");

            var StaffServiceTypeDetail = (from s in db.StaffServiceTypeDetails where s.StaffServiceTypeDetailId == StaffServiceTypeDetailId select s).FirstOrDefault();
            db.StaffServiceTypeDetails.Remove(StaffServiceTypeDetail);
            db.SaveChanges();
        }

        public List<StaffServiceTypeDetail> GetAllStaffServiceTypeDetail(ref int totalcount, int StaffId = 0,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffServiceTypeDetails.ToList();
            if (StaffId > 0)
                query = query.Where(f => f.StaffId == StaffId).ToList();

            totalcount = query.Count;
            var StaffServiceTypeDetail = new PagedList<KSModel.Models.StaffServiceTypeDetail>(query.ToList(), PageIndex, PageSize);
            return StaffServiceTypeDetail;
        }
        #endregion

        #region StaffServiceType
        public StaffServiceType GetStaffServiceTypeById(int StaffServiceTypeId)
        {
            if (StaffServiceTypeId == 0)
                throw new ArgumentNullException("StaffServiceType");

            StaffServiceType StaffStatus = new StaffServiceType();

            if (StaffServiceTypeId != 0 && StaffServiceTypeId != null)
                StaffStatus = (from s in db.StaffServiceTypes where s.StaffServiceTypeId == StaffServiceTypeId select s).FirstOrDefault();
            else
                StaffStatus = (from s in db.StaffServiceTypes.AsNoTracking() where s.StaffServiceTypeId == StaffServiceTypeId select s).FirstOrDefault();

            return StaffStatus;
        }

        public void InsertStaffServiceType(StaffServiceType StaffServiceType)
        {
            if (StaffServiceType == null)
                throw new ArgumentNullException("StaffServiceType");

            db.StaffServiceTypes.Add(StaffServiceType);
            db.SaveChanges();
        }

        public void UpdateStaffServiceType(StaffServiceType StaffServiceType)
        {
            if (StaffServiceType == null)
                throw new ArgumentNullException("StaffServiceType");

            db.Entry(StaffServiceType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStaffServiceType(int StaffServiceTypeId = 0)
        {
            if (StaffServiceTypeId == 0)
                throw new ArgumentNullException("StaffServiceType");

            var StaffServiceType = (from s in db.StaffServiceTypes where s.StaffServiceTypeId == StaffServiceTypeId select s).FirstOrDefault();
            db.StaffServiceTypes.Remove(StaffServiceType);
            db.SaveChanges();
        }

        public List<StaffServiceType> GetAllStaffServiceType(ref int totalcount, bool? IsActive = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StaffServiceTypes.ToList();
            if (IsActive != null)
                query = query.Where(f => f.IsActive == IsActive).ToList();

            totalcount = query.Count;
            var StaffServiceType = new PagedList<KSModel.Models.StaffServiceType>(query.ToList(), PageIndex, PageSize);
            return StaffServiceType;
        }
        #endregion
    }
}