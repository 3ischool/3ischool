﻿using KSModel.Models;
using ViewModel.ViewModel.Staff;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DAL.StaffModule
{
    public partial interface IStaffService
    {
        #region StaffType

        StaffType GetStaffTypeById(int StaffTypeId, bool IsTrack = false);

        List<StaffType> GetAllStaffTypes(ref int totalcount, string StaffType = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        List<StaffType> GetAllStaffTypesUpdated(ref int totalcount, bool IsTeacher = false,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStaffType(StaffType StaffType);

        void UpdateStaffType(StaffType StaffType);

        void DeleteStaffType(int StaffTypeId = 0);

        #endregion

        #region Staff

        Staff GetStaffById(int StaffId, bool IsTrack = false);

        IList<Staff> GetAllStaffs(ref int totalcount, string Fname = "", string Lname = "", string Fullname = "", bool? IsActive = null,
            int? StaffTypeId = null, int? DepartmentId = null, int? DesignationId = null, int? PunchMcId = null,
            string EmpCode = "", DateTime? DOJ = null, int PageIndex = 0, int PageSize = int.MaxValue);

        List<StaffAttendanceReportModel> GetStaffForAttendanceReport(ref int totalcount, int? ListType = null,
                                   DateTime? Date = null, string AttendanceStatusIds = "", int PageIndex = 0, int PageSize = int.MaxValue);

        DataTable ExportStaffListWithFilter(ref int totalcount, int? StaffTypeId = null, int? DepartmentId = null, int? DesignationId = null, string SearchBox = null, string EmpCode = "",string IsActive=null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStaff(Staff Staff);

        void UpdateStaff(Staff Staff);

        void DeleteStaff(int StaffId = 0);

        string FullName(string Fname, string Lname);

        #endregion

        #region StaffQualification

        KSModel.Models.StaffQualification GetStaffQualificationById(int StaffQualificationId, bool IsTrack = false);

        List<KSModel.Models.StaffQualification> GetAllStaffQualifications(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);


        void InsertStaffQualification(KSModel.Models.StaffQualification StaffQualification);

        void UpdateStaffQualification(KSModel.Models.StaffQualification StaffQualification);

        void DeleteStaffQualification(int StaffQualificationId = 0);



        #endregion
        #region StaffExperience

        KSModel.Models.StaffExperience GetStaffExperienceById(int StaffExperienceId, bool IsTrack = false);

        List<KSModel.Models.StaffExperience> GetAllStaffExperiences(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);


        void InsertStaffExperience(KSModel.Models.StaffExperience StaffExperience);

        void UpdateStaffExperience(KSModel.Models.StaffExperience StaffExperience);

        void DeleteStaffExperience(int StaffExperienceId = 0);



        #endregion

        #region StaffLeaveApplication
        StaffLeaveApplication GetStaffLeaveApplicationById(int StaffLeaveApplicationId, bool IsTrack = false);

        List<StaffLeaveApplication> GetAllStaffLeaveApplication(ref int totalcount, int? StaffId = null,
            DateTime? LeaveFrom = null, DateTime? LeaveTill = null, DateTime? AppliedDate = null,
            string Reason = null, bool? IsApproved = null, int? LeaveTypeId = null, int? ApprovedBy = null,
            DateTime? ApprovedOn = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStaffLeaveApplication(StaffLeaveApplication StaffLeave);

        void UpdateStaffLeaveApplication(StaffLeaveApplication StaffLeave);

        void DeleteStaffLeaveApplication(int StaffLeaveApplicationId = 0);

        StaffLeaveApplication GetStaffLeaveApplicationbyLeavDate(StaffLeaveApplication StaffLeaveApplication);

        #endregion

        #region AttendanceStatus

        StaffAttendanceStatu GetStaffAttendanceStatusById(int AttendanceStatusId, bool IsTrack = false);

        List<StaffAttendanceStatu> GetStaffAllAttendanceStatus(ref int totalcount, string AttnStatus = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStaffAttendanceStatus(StaffAttendanceStatu AttendanceStatus);

        void UpdateStaffAttendanceStatus(StaffAttendanceStatu AttendanceStatus);

        void DeleteStaffAttendanceStatus(int AttendanceStatusId = 0);

        #endregion

        #region StaffAttnedance
        StaffAttendance GetStaffAttendanceById(int StaffAttendanceId, bool IsTrack = false);

        IList<StaffAttendance> GetAllStaffAttendances(ref int totalcount, int? StaffId = null, DateTime? AttendanceDate = null, int? AttendanceStatusId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        DataSet GetSummaryRept(ref int totalcount, string Measurement = "");

        void InsertStaffAttendance(StaffAttendance Attendance);

        void UpdateStaffAttendance(StaffAttendance Attendance);

        void DeleteStaffAttendance(int StaffAttendanceId = 0);
        #endregion

        #region TeacherClass

        TeacherClass GetTeacherClassById(int TeacherClassId, bool IsTrack = false);

        List<TeacherClass> GetAllTeacherClasss(ref int totalcount, int? TeacherId = null, int? StandardId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTeacherClass(TeacherClass TeacherClass);

        void UpdateTeacherClass(TeacherClass TeacherClass);

        void DeleteTeacherClass(int TeacherClassId = 0);

        #endregion

        #region TeacherSubjectAssignment
        KSModel.Models.TeacherSubjectAssign GetTeacherSubjectAssignById(int TeacherSubjectAssignId, bool IsTrack = false);

        List<KSModel.Models.TeacherSubjectAssign> GetAllTeacherSubjectAssigns(ref int totalcount, int? TeacherId = null, int? ClassId = null,
           int? VirtualId = null,int? SubjectId = null,int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTeacherSubjectAssign(KSModel.Models.TeacherSubjectAssign TeacherSubjectAssign);

        void UpdateTeacherSubjectAssign(KSModel.Models.TeacherSubjectAssign TeacherSubjectAssign);

        void DeleteTeacherSubjectAssign(int TeacherSubjectAssignId = 0);
        #endregion

        #region ClassTeacher

        ClassTeacher GetClassTeacherById(int ClassTeacherId, bool IsTrack = false);

        List<ClassTeacher> GetAllClassTeachers(ref int totalcount, int? ClassId = null, int? TeacherId = null,
            DateTime? EffectiveDate = null, DateTime? EndDate = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertClassTeacher(ClassTeacher ClassTeacher);

        void UpdateClassTeacher(ClassTeacher ClassTeacher);

        void DeleteClassTeacher(int ClassTeacherId = 0);

        #endregion

        #region TeacherSubject

        TeacherSubject GetTeacherSubjectById(int TeacherSubjectId, bool IsTrack = false);

        List<TeacherSubject> GetAllTeacherSubjects(ref int totalcount, int? SubjectId = null, int? TeacherId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTeacherSubject(TeacherSubject TeacherSubject);

        void UpdateTeacherSubject(TeacherSubject TeacherSubject);

        void DeleteTeacherSubject(int TeacherSubjectId = 0);

        #endregion

        #region StaffDocument

        StaffDocument GetStaffDocumentById(int StaffDocumentId, bool IsTrack = false);
        List<StaffDocumentType> GetAllStaffDocumentTypes(ref int totalcount, string DocumentType = "", bool IsmultipleAllowed = false,
          int PageIndex = 0, int PageSize = int.MaxValue);
        List<StaffDocument> GetAllStaffDocuments(ref int totalcount, int? DocumentTypeId = null, string Image = "",
            int? StaffId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStaffDocument(StaffDocument StaffDocument);

        void UpdateStaffDocument(StaffDocument StaffDocument);

        void DeleteStaffDocument(int StaffDocumentId = 0);

        #endregion

        #region Department
        Department GetDepartmentById(int DepartmentId, bool IsTrack = false);

        List<Department> GetAllDepartments(ref int totalcount,
          int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertDepartment(Department Department);

        void UpdateDepartment(Department Department);

        void DeleteDepartment(int DepartmentId = 0);
        #endregion

        #region Designation
        Designation GetDesignationById(int DesignationId, bool IsTrack = false);

        List<Designation> GetAllDesignations(ref int totalcount,
          int PageIndex = 0, int PageSize = int.MaxValue, int StafftypeId = 0);

        void InsertDesignation(Designation Designation);

        void UpdateDesignation(Designation Designation);

        void DeleteDesignation(int DesignationId = 0);
        #endregion

        #region StaffCategory

        StaffCategory GetStaffCategoryById(int StaffCategoryId, bool IsTrack = false);

        List<StaffCategory> GetAllStaffCategories(ref int totalcount, string StaffCategory = "",
          int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStaffCategory(StaffCategory StaffCategory);

        void UpdateStaffCategory(StaffCategory StaffCategory);

        void DeleteStaffCategory(int StaffCategoryId = 0);

        #endregion

        #region StaffStatusDetail
        StaffStatusDetail GetStaffStatusDetailById(int StaffStatusId);

        void InsertStaffStatus(StaffStatusDetail StaffStatusDetail);

        void UpdateStaffStatus(StaffStatusDetail StaffStatusDetail);

        void DeleteStaffStatus(int StaffStatusId = 0);

        List<StaffStatusDetail> GetAllStaffStatusDetail(ref int totalcount,
           int PageIndex = 0, int PageSize = int.MaxValue);

        #endregion

        #region StaffTimeTable
        List<StaffTimeTable> StaffAssignedTimeTable(ref int totalcount,
          int PageIndex = 0, int PageSize = int.MaxValue, int StaffId = 0);

        #endregion

        #region StaffBankDetail
        StaffBankDetail GetStaffBankDetailById(int StaffBankDetailId);

        void InsertStaffBankDetail(StaffBankDetail StaffBankDetail);

        void UpdateStaffBankDetail(StaffBankDetail StaffBankDetail);

        void DeleteStaffBankDetail(int StaffBankDetailId = 0);

        List<StaffBankDetail> GetAllStaffBankDetail(ref int totalcount,int StaffId=0,
            int PageIndex = 0, int PageSize = int.MaxValue);
        #endregion

        #region StaffServiceTypeDetail
        StaffServiceTypeDetail GetStaffServiceTypeDetailById(int StaffServiceTypeDetailId);

        void InsertStaffServiceTypeDetail(StaffServiceTypeDetail StaffServiceTypeDetail);


        void UpdateStaffServiceTypeDetail(StaffServiceTypeDetail StaffServiceTypeDetail);


        void DeleteStaffServiceTypeDetail(int StaffServiceTypeDetailId = 0);


        List<StaffServiceTypeDetail> GetAllStaffServiceTypeDetail(ref int totalcount, int StaffId = 0,
            int PageIndex = 0, int PageSize = int.MaxValue);

        #endregion

        #region StaffServiceType
        StaffServiceType GetStaffServiceTypeById(int StaffServiceTypeId);

        void InsertStaffServiceType(StaffServiceType StaffServiceType);

        void UpdateStaffServiceType(StaffServiceType StaffServiceType);

        void DeleteStaffServiceType(int StaffServiceTypeId = 0);

        List<StaffServiceType> GetAllStaffServiceType(ref int totalcount, bool? IsActive = null,
            int PageIndex = 0, int PageSize = int.MaxValue);
        
        #endregion
    }

}
