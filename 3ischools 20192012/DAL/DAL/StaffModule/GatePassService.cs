﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KSModel.Models;
using DAL.DAL.Common;

namespace DAL.DAL.StaffModule
{
    public partial class GatePassService : IGatePassService
    {

        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public GatePassService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }


        #region GatePassType

        public GatePassType GetGatePassTypeById(int GatePassTypeId, bool IsTrack = false)
        {
            if (GatePassTypeId == 0)
                throw new ArgumentNullException("GatePassType");

            GatePassType GatePassType = new GatePassType();
            if (IsTrack)
                GatePassType = (from s in db.GatePassTypes where s.GatePassTypeId == GatePassTypeId select s).FirstOrDefault();
            else
                GatePassType = (from s in db.GatePassTypes.AsNoTracking() where s.GatePassTypeId == GatePassTypeId select s).FirstOrDefault();

            return GatePassType;
        }

        public List<GatePassType> GetAllGatePassType(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.GatePassTypes.ToList();
            totalcount = query.Count;
            var GatePassTypes = new PagedList<KSModel.Models.GatePassType>(query, PageIndex, PageSize);
            return GatePassTypes;
        }

        public void InsertGatePassType(GatePassType GatePassType)
        {
            if (GatePassType == null)
                throw new ArgumentNullException("GatePassType");

            db.GatePassTypes.Add(GatePassType);
            db.SaveChanges();
        }

        public void UpdateGatePassType(GatePassType GatePassType)
        {
            if (GatePassType == null)
                throw new ArgumentNullException("GatePassType");

            db.Entry(GatePassType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteGatePassType(int GatePassTypeId = 0)
        {
            if (GatePassTypeId == 0)
                throw new ArgumentNullException("GatePassType");

            var GatePassType = (from s in db.GatePassTypes where s.GatePassTypeId == GatePassTypeId select s).FirstOrDefault();
            db.GatePassTypes.Remove(GatePassType);
            db.SaveChanges();
        }

        #endregion


        #region GatePass

        public GatePass GetGatePassById(int GatePassId, bool IsTrack = false)
        {
            if (GatePassId == 0)
                throw new ArgumentNullException("GatePass");

            GatePass GatePass = new GatePass();
            if (IsTrack)
                GatePass = (from s in db.GatePasses where s.GatePassId == GatePassId select s).FirstOrDefault();
            else
                GatePass = (from s in db.GatePasses.AsNoTracking() where s.GatePassId == GatePassId select s).FirstOrDefault();

            return GatePass;
        }

        public List<GatePass> GetAllGatePass(ref int totalcount, int? PassTypeId = null, string ContactPersonId = "", 
            int? StudentId = null, int? StaffId = null, DateTime? IssueDate = null, DateTime? ToDate = null, 
            TimeSpan? InTime = null, TimeSpan? OutTime = null,int? SessionId = null ,int? classId= null,
            int PageIndex = 0, int PageSize = int.MaxValue, string visitor = "")
        {

            var query = db.GatePasses.ToList();
            if (PassTypeId != null && PassTypeId > 0)
                query = query.Where(e => e.GatePassTypeId == PassTypeId).ToList();
            if (visitor != "" && visitor != null)
            {
                if (query.Count > 0)
                {
                    var reqids = db.GatePassDetails.Where(p => p.PersonName.ToLower() == visitor.ToLower()).Select(p => p.GatePassId).ToList();
                    query = query.Where(p => reqids.Contains(p.GatePassId)).ToList();
                }
            }
            if (ContactPersonId != null && ContactPersonId != "")
                query = query.Where(e => e.ConcernedPersonId == ContactPersonId).ToList();
            if (StudentId != null && StudentId > 0)
            {
                query = query.Where(e => e.StudentId == StudentId).ToList();
            }
            if (StaffId != null && StaffId > 0)
                query = query.Where(e => e.StaffId == StaffId).ToList();
            if (IssueDate != null && ToDate != null)
                query = query.Where(e => e.IssueDate >= IssueDate && e.IssueDate <= ToDate).ToList();
            else if (IssueDate != null)
                query = query.Where(e => e.IssueDate == IssueDate).ToList();
            else if (ToDate != null)
                query = query.Where(e => e.IssueDate == ToDate).ToList();
            if (InTime != null && OutTime != null)
                query = query.Where(e => (e.TimeIn >= InTime || e.TimeIn == null) && (e.TimeOut <= OutTime || e.TimeOut == null)).ToList();
            else
            {
                if (InTime != null)
                    query = query.Where(e => e.TimeIn == InTime).ToList();
                if (OutTime != null)
                    query = query.Where(e => e.TimeOut == OutTime).ToList();
            }
            if (SessionId != null)
                query = query.Where(e => e.SessionId == SessionId).ToList();

            if (classId != null)
            {
                var studentids = db.StudentClasses.Where(x => x.ClassId == classId).Select(x => x.StudentId).ToArray();
                query = query.Where(e => studentids.Contains(e.StudentId)).Distinct().ToList();
            }

            totalcount = query.Count;

            var GatePassees = new PagedList<KSModel.Models.GatePass>(query, PageIndex, PageSize);
            return GatePassees;
        }

        public void InsertGatePass(GatePass GatePass)
        {
            if (GatePass == null)
                throw new ArgumentNullException("GatePass");

            db.GatePasses.Add(GatePass);
            db.SaveChanges();
        }

        public void UpdateGatePass(GatePass GatePass)
        {
            if (GatePass == null)
                throw new ArgumentNullException("GatePass");

            db.Entry(GatePass).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteGatePass(int GatePassId = 0)
        {
            if (GatePassId == 0)
                throw new ArgumentNullException("GatePass");

            var GatePass = (from s in db.GatePasses where s.GatePassId == GatePassId select s).FirstOrDefault();
            db.GatePasses.Remove(GatePass);
            db.SaveChanges();
        }

        #endregion


        #region GatePassDetail

        public GatePassDetail GatePassDetailById(int GatePassDetailId, bool IsTrack = false)
        {
            if (GatePassDetailId == 0)
                throw new ArgumentNullException("GatePassDetail");

            GatePassDetail GatePassDetail = new GatePassDetail();
            if (IsTrack)
                GatePassDetail = (from s in db.GatePassDetails where s.GatePassDetailId == GatePassDetailId select s).FirstOrDefault();
            else
                GatePassDetail = (from s in db.GatePassDetails.AsNoTracking() where s.GatePassDetailId == GatePassDetailId select s).FirstOrDefault();

            return GatePassDetail;
        }

        public List<GatePassDetail> GetAllGatePassDetail(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.GatePassDetails.ToList();
            totalcount = query.Count;
            var GatePassDetail = new PagedList<KSModel.Models.GatePassDetail>(query, PageIndex, PageSize);
            return GatePassDetail;
        }

        public void InsertGatePassDetail(GatePassDetail GatePassDetail)
        {
            if (GatePassDetail == null)
                throw new ArgumentNullException("GatePassDetail");

            db.GatePassDetails.Add(GatePassDetail);
            db.SaveChanges();
        }

        public void UpdateGatePassDetail(GatePassDetail GatePassDetail)
        {
            if (GatePassDetail == null)
                throw new ArgumentNullException("GatePassDetail");

            db.Entry(GatePassDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteGatePassDetail(int GatePassDetailId = 0)
        {
            if (GatePassDetailId == 0)
                throw new ArgumentNullException("GatePassDetail");

            var GatePassDetail = (from s in db.GatePassDetails where s.GatePassDetailId == GatePassDetailId select s).FirstOrDefault();
            db.GatePassDetails.Remove(GatePassDetail);
            db.SaveChanges();
        }
        #endregion

        #region VisitorCard

        public VisitorCard GetVisitorCardById(int VisitorCardId, bool IsTrack = false)
        {
            if (VisitorCardId == 0)
                throw new ArgumentNullException("VisitorCard");

            VisitorCard VisitorCard = new VisitorCard();
            if (IsTrack)
                VisitorCard = (from s in db.VisitorCards where s.VisitorCardId == VisitorCardId select s).FirstOrDefault();
            else
                VisitorCard = (from s in db.VisitorCards.AsNoTracking() where s.VisitorCardId == VisitorCardId select s).FirstOrDefault();

            return VisitorCard;
        }


        public List<VisitorCard> GetAllVisitorCard(ref int totalcount,bool IsActive=true, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.VisitorCards.ToList();
            totalcount = query.Count;

            if (query.Count > 0)
            {
            if(IsActive)
                query = query.Where(f => f.IsActive != false).ToList();
            }
            var VisitorCard = new PagedList<KSModel.Models.VisitorCard>(query, PageIndex, PageSize);
            return VisitorCard;
        }

        public void InsertVisitorCard(VisitorCard VisitorCard)
        {
            if (VisitorCard == null)
                throw new ArgumentNullException("VisitorCard");

            db.VisitorCards.Add(VisitorCard);
            db.SaveChanges();
        }

        public void UpdateVisitorCard(VisitorCard VisitorCard)
        {
            if (VisitorCard == null)
                throw new ArgumentNullException("VisitorCard");

            db.Entry(VisitorCard).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteVisitorCard(int VisitorCardId = 0)
        {
            if (VisitorCardId == 0)
                throw new ArgumentNullException("VisitorCard");

            var VisitorCard = (from s in db.VisitorCards where s.VisitorCardId == VisitorCardId select s).FirstOrDefault();
            db.VisitorCards.Remove(VisitorCard);
            db.SaveChanges();
        }


        #endregion


        #region GatePassAuthority

        public GatePassAuthority GatePassAuthorityById(int GatePassAuthorityId, bool IsTrack = false)
        {
            if (GatePassAuthorityId == 0)
                throw new ArgumentNullException("GatePassAuthority");

            GatePassAuthority GatePassAuthority = new GatePassAuthority();
            if (IsTrack)
                GatePassAuthority = (from s in db.GatePassAuthorities where s.GatePassAuthorityId == GatePassAuthorityId select s).FirstOrDefault();
            else
                GatePassAuthority = (from s in db.GatePassAuthorities.AsNoTracking() where s.GatePassAuthorityId == GatePassAuthorityId select s).FirstOrDefault();

            return GatePassAuthority;
        }

        public List<GatePassAuthority> GetAllGatePassAuthority(ref int totalcount, int AuthorityId = 0, int GatePassTypeId = 0, int ContactTypeId = 0, bool IsActive = false, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.GatePassAuthorities.ToList();
            totalcount = query.Count;

            if (AuthorityId != null && AuthorityId > 0)
                query = query.Where(e => e.AuthorityId == AuthorityId).ToList();
            if (GatePassTypeId != null && GatePassTypeId > 0)
                query = query.Where(e => e.GatePassTypeId == GatePassTypeId).ToList();
            if (ContactTypeId != null && ContactTypeId > 0)
                query = query.Where(e => e.ContactTypeId == ContactTypeId).ToList();

            if (IsActive == true)
                query = query.Where(s => s.IsActive == true).ToList();
            //else if (IsActive == false)
            //    query = query.Where(s => s.IsActive == false).ToList();
            //else
            //    query = query.Where(s => s.IsActive == true || s.IsActive == false || s.IsActive == null).ToList();


            var GatePassAuthority = new PagedList<KSModel.Models.GatePassAuthority>(query, PageIndex, PageSize);
            return GatePassAuthority;
        }

        public void InsertGatePassAuthority(GatePassAuthority GatePassAuthority)
        {
            if (GatePassAuthority == null)
                throw new ArgumentNullException("GatePassAuthority");

            db.GatePassAuthorities.Add(GatePassAuthority);
            db.SaveChanges();
        }

        public void UpdateGatePassAuthority(GatePassAuthority GatePassAuthority)
        {
            if (GatePassAuthority == null)
                throw new ArgumentNullException("GatePassAuthority");

            db.Entry(GatePassAuthority).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteGatePassAuthority(int GatePassAuthorityId = 0)
        {
            if (GatePassAuthorityId == 0)
                throw new ArgumentNullException("GatePassAuthority");

            var GatePassAuthority = (from s in db.GatePassAuthorities where s.GatePassAuthorityId == GatePassAuthorityId select s).FirstOrDefault();
            db.GatePassAuthorities.Remove(GatePassAuthority);
            db.SaveChanges();
        }

        #endregion

    }
}