﻿using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.DAL.UserModule
{
    public class UserManagementService : IUserManagementService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public UserManagementService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods

        #region AppForm

        public AppForm GetAppFormById(int AppFormId, bool IsTrack = false)
        {
            if (AppFormId == 0)
                throw new ArgumentNullException("AppForm");

            var AppForm = new AppForm();
            if (IsTrack)
                AppForm = (from s in db.AppForms where s.AppFormId == AppFormId select s).FirstOrDefault();
            else
                AppForm = (from s in db.AppForms.AsNoTracking() where s.AppFormId == AppFormId select s).FirstOrDefault();

            return AppForm;
        }

        public List<AppForm> GetAllAppForms(ref int totalcount, string AppForm = "", int? ModuleId = null, 
            int? SortingIndex = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AppForms.ToList();
            if (!String.IsNullOrEmpty(AppForm))
                query = query.Where(n => n.AppForm1.ToLower() == AppForm.ToLower()).ToList();
            if (ModuleId > 0)
                query = query.Where(n => n.ModuleId == ModuleId).ToList();
            if (SortingIndex > 0)
                query = query.Where(n => n.SortingIndex == SortingIndex).ToList();

            totalcount = query.Count;
            var AppForms = new PagedList<KSModel.Models.AppForm>(query, PageIndex, PageSize);
            return AppForms;
        }

        public void InsertAppForm(AppForm AppForm)
        {
            if (AppForm == null)
                throw new ArgumentNullException("AppForm");

            db.AppForms.Add(AppForm);
            db.SaveChanges();
        }

        public void UpdateAppForm(AppForm AppForm)
        {
            if (AppForm == null)
                throw new ArgumentNullException("AppForm");

            db.Entry(AppForm).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAppForm(int AppFormId = 0)
        {
            if (AppFormId == 0)
                throw new ArgumentNullException("AppForm");

            var AppForm = (from s in db.AppForms where s.AppFormId == AppFormId select s).FirstOrDefault();
            db.AppForms.Remove(AppForm);
            db.SaveChanges();
        }

        #endregion

        #region AppFormException

        public AppFormException GetAppFormExceptionById(int AppFormExceptionId, bool IsTrack = false)
        {
            if (AppFormExceptionId == 0)
                throw new ArgumentNullException("AppFormException");

            var AppFormException = new AppFormException();
            if (IsTrack)
                AppFormException = (from s in db.AppFormExceptions where s.AppFormExceptionId == AppFormExceptionId select s).FirstOrDefault();
            else
                AppFormException = (from s in db.AppFormExceptions.AsNoTracking() where s.AppFormExceptionId == AppFormExceptionId select s).FirstOrDefault();

            return AppFormException;
        }

        public List<AppFormException> GetAllAppFormExceptions(ref int totalcount, string AppFormEvent = "", string AppFormException = "",
            int? AppFormId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AppFormExceptions.ToList();
            if (!String.IsNullOrEmpty(AppFormEvent))
                query = query.Where(n => n.AppFormEvent.ToLower() == AppFormEvent.ToLower()).ToList();
            if (!String.IsNullOrEmpty(AppFormException))
                query = query.Where(n => n.AppFormException1.ToLower() == AppFormException.ToLower()).ToList();

            totalcount = query.Count;
            var AppFormExceptions = new PagedList<KSModel.Models.AppFormException>(query, PageIndex, PageSize);
            return AppFormExceptions;
        }

        public void InsertAppFormException(AppFormException AppFormException)
        {
            if (AppFormException == null)
                throw new ArgumentNullException("AppFormException");

            db.AppFormExceptions.Add(AppFormException);
            db.SaveChanges();
        }

        public void UpdateAppFormException(AppFormException AppFormException)
        {
            if (AppFormException == null)
                throw new ArgumentNullException("AppFormException");

            db.Entry(AppFormException).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAppFormException(int AppFormExceptionId = 0)
        {
            if (AppFormExceptionId == 0)
                throw new ArgumentNullException("AppFormException");

            var AppFormException = (from s in db.AppFormExceptions where s.AppFormExceptionId == AppFormExceptionId select s).FirstOrDefault();
            db.AppFormExceptions.Remove(AppFormException);
            db.SaveChanges();
        }

        #endregion

        #region AppFormField

        public AppFormField GetAppFormFieldById(int AppFormFieldId, bool IsTrack = false)
        {
            if (AppFormFieldId == 0)
                throw new ArgumentNullException("AppFormField");

            var AppFormField = new AppFormField();
            if (IsTrack)
                AppFormField = (from s in db.AppFormFields where s.AppFormFieldId == AppFormFieldId select s).FirstOrDefault();
            else
                AppFormField = (from s in db.AppFormFields.AsNoTracking() where s.AppFormFieldId == AppFormFieldId select s).FirstOrDefault();

            return AppFormField;
        }

        public List<AppFormField> GetAllAppFormFields(ref int totalcount, string FieldName = "",
            int? SortingIndex = null, int? AppFormId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AppFormFields.ToList();
            if (!String.IsNullOrEmpty(FieldName))
                query = query.Where(n => n.FieldName.ToLower() == FieldName.ToLower()).ToList();
            if (SortingIndex > 0)
                query = query.Where(n => n.SortingIndex == SortingIndex).ToList();

            totalcount = query.Count;
            var AppFormFields = new PagedList<KSModel.Models.AppFormField>(query, PageIndex, PageSize);
            return AppFormFields;
        }

        public void InsertAppFormField(AppFormField AppFormField)
        {
            if (AppFormField == null)
                throw new ArgumentNullException("AppFormField");

            db.AppFormFields.Add(AppFormField);
            db.SaveChanges();
        }

        public void UpdateAppFormField(AppFormField AppFormField)
        {
            if (AppFormField == null)
                throw new ArgumentNullException("AppFormField");

            db.Entry(AppFormField).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAppFormField(int AppFormFieldId = 0)
        {
            if (AppFormFieldId == 0)
                throw new ArgumentNullException("AppFormField");

            var AppFormField = (from s in db.AppFormFields where s.AppFormFieldId == AppFormFieldId select s).FirstOrDefault();
            db.AppFormFields.Remove(AppFormField);
            db.SaveChanges();
        }

        #endregion

        #region AppFormStep

        public AppFormStep GetAppFormStepById(int AppFormStepId, bool IsTrack = false)
        {
            if (AppFormStepId == 0)
                throw new ArgumentNullException("AppFormStep");

            var AppFormStep = new AppFormStep();
            if (IsTrack)
                AppFormStep = (from s in db.AppFormSteps where s.AppFormStepId == AppFormStepId select s).FirstOrDefault();
            else
                AppFormStep = (from s in db.AppFormSteps.AsNoTracking() where s.AppFormStepId == AppFormStepId select s).FirstOrDefault();

            return AppFormStep;
        }

        public List<AppFormStep> GetAllAppFormSteps(ref int totalcount, int? StepNo = null, int? AppFormId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AppFormSteps.ToList();
            if (StepNo > 0)
                query = query.Where(n => n.StepNo == StepNo).ToList();
            if (AppFormId > 0)
                query = query.Where(n => n.AppFormId == AppFormId).ToList();

            totalcount = query.Count;
            var AppFormSteps = new PagedList<KSModel.Models.AppFormStep>(query, PageIndex, PageSize);
            return AppFormSteps;
        }

        public void InsertAppFormStep(AppFormStep AppFormStep)
        {
            if (AppFormStep == null)
                throw new ArgumentNullException("AppFormStep");

            db.AppFormSteps.Add(AppFormStep);
            db.SaveChanges();
        }

        public void UpdateAppFormStep(AppFormStep AppFormStep)
        {
            if (AppFormStep == null)
                throw new ArgumentNullException("AppFormStep");

            db.Entry(AppFormStep).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAppFormStep(int AppFormStepId = 0)
        {
            if (AppFormStepId == 0)
                throw new ArgumentNullException("AppFormStep");

            var AppFormStep = (from s in db.AppFormSteps where s.AppFormStepId == AppFormStepId select s).FirstOrDefault();
            db.AppFormSteps.Remove(AppFormStep);
            db.SaveChanges();
        }

        #endregion

        #region AppFormStepDetail

        public AppFormStepDetail GetAppFormStepDetailById(int AppFormStepDetailId, bool IsTrack = false)
        {
            if (AppFormStepDetailId == 0)
                throw new ArgumentNullException("AppFormStepDetail");

            var AppFormStepDetail = new AppFormStepDetail();
            if (IsTrack)
                AppFormStepDetail = (from s in db.AppFormStepDetails where s.AppFormStepDetailId == AppFormStepDetailId select s).FirstOrDefault();
            else
                AppFormStepDetail = (from s in db.AppFormStepDetails.AsNoTracking() where s.AppFormStepDetailId == AppFormStepDetailId select s).FirstOrDefault();

            return AppFormStepDetail;
        }

        public List<AppFormStepDetail> GetAllAppFormStepDetails(ref int totalcount, int? NextStepNo = null,
            int? AppFormStepId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AppFormStepDetails.ToList();
            if (NextStepNo > 0)
                query = query.Where(n => n.NextStepNo == NextStepNo).ToList();
            if (AppFormStepId > 0)
                query = query.Where(n => n.AppFormStepId == AppFormStepId).ToList();

            totalcount = query.Count;
            var AppFormStepDetails = new PagedList<KSModel.Models.AppFormStepDetail>(query, PageIndex, PageSize);
            return AppFormStepDetails;
        }

        public void InsertAppFormStepDetail(AppFormStepDetail AppFormStepDetail)
        {
            if (AppFormStepDetail == null)
                throw new ArgumentNullException("AppFormStepDetail");

            db.AppFormStepDetails.Add(AppFormStepDetail);
            db.SaveChanges();
        }

        public void UpdateAppFormStepDetail(AppFormStepDetail AppFormStepDetail)
        {
            if (AppFormStepDetail == null)
                throw new ArgumentNullException("AppFormStepDetail");

            db.Entry(AppFormStepDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAppFormStepDetail(int AppFormStepDetailId = 0)
        {
            if (AppFormStepDetailId == 0)
                throw new ArgumentNullException("AppFormStepDetail");

            var AppFormStepDetail = (from s in db.AppFormStepDetails where s.AppFormStepDetailId == AppFormStepDetailId select s).FirstOrDefault();
            db.AppFormStepDetails.Remove(AppFormStepDetail);
            db.SaveChanges();
        }

        #endregion

        #region AppFormInfo

        public AppFormInfo GetAppFormInfoById(int AppFormInfoId, bool IsTrack = false)
        {
            if (AppFormInfoId == 0)
                throw new ArgumentNullException("AppFormInfo");

            var AppFormInfo = new AppFormInfo();
            if (IsTrack)
                AppFormInfo = (from s in db.AppFormInfoes where s.AppFormInfoId == AppFormInfoId select s).FirstOrDefault();
            else
                AppFormInfo = (from s in db.AppFormInfoes.AsNoTracking() where s.AppFormInfoId == AppFormInfoId select s).FirstOrDefault();

            return AppFormInfo;
        }

        public List<AppFormInfo> GetAllAppFormInfos(ref int totalcount, int? SortingIndex = null, int? AppFormId = null, 
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AppFormInfoes.ToList();
            if (SortingIndex > 0)
                query = query.Where(n => n.SortingIndex == SortingIndex).ToList();
            if (AppFormId > 0)
                query = query.Where(n => n.AppFormId == AppFormId).ToList();

            totalcount = query.Count;
            var AppFormInfoes = new PagedList<KSModel.Models.AppFormInfo>(query, PageIndex, PageSize);
            return AppFormInfoes;
        }

        public void InsertAppFormInfo(AppFormInfo AppFormInfo)
        {
            if (AppFormInfo == null)
                throw new ArgumentNullException("AppFormInfo");

            db.AppFormInfoes.Add(AppFormInfo);
            db.SaveChanges();
        }

        public void UpdateAppFormInfo(AppFormInfo AppFormInfo)
        {
            if (AppFormInfo == null)
                throw new ArgumentNullException("AppFormInfo");

            db.Entry(AppFormInfo).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAppFormInfo(int AppFormInfoId = 0)
        {
            if (AppFormInfoId == 0)
                throw new ArgumentNullException("AppFormInfo");

            var AppFormInfo = (from s in db.AppFormInfoes where s.AppFormInfoId == AppFormInfoId select s).FirstOrDefault();
            db.AppFormInfoes.Remove(AppFormInfo);
            db.SaveChanges();
        }

        #endregion

        #region AppFormValidation

        public AppFormValidation GetAppFormValidationById(int AppFormValidationId, bool IsTrack = false)
        {
            if (AppFormValidationId == 0)
                throw new ArgumentNullException("AppFormValidation");

            var AppFormValidation = new AppFormValidation();
            if (IsTrack)
                AppFormValidation = (from s in db.AppFormValidations where s.AppFormValidationId == AppFormValidationId select s).FirstOrDefault();
            else
                AppFormValidation = (from s in db.AppFormValidations.AsNoTracking() where s.AppFormValidationId == AppFormValidationId select s).FirstOrDefault();

            return AppFormValidation;
        }

        public List<AppFormValidation> GetAllAppFormValidations(ref int totalcount, int? SortingIndex = null, 
            string ValidationName = "", int? AppFormId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AppFormValidations.ToList();
            if (!String.IsNullOrEmpty(ValidationName))
                query = query.Where(n => n.ValidationName.ToLower() == ValidationName.ToLower()).ToList();
            if (SortingIndex > 0)
                query = query.Where(n => n.SortingIndex == SortingIndex).ToList();
            if (AppFormId > 0)
                query = query.Where(n => n.AppFormId == AppFormId).ToList();

            totalcount = query.Count;
            var AppFormValidations = new PagedList<KSModel.Models.AppFormValidation>(query, PageIndex, PageSize);
            return AppFormValidations;
        }

        public void InsertAppFormValidation(AppFormValidation AppFormValidation)
        {
            if (AppFormValidation == null)
                throw new ArgumentNullException("AppFormValidation");

            db.AppFormValidations.Add(AppFormValidation);
            db.SaveChanges();
        }

        public void UpdateAppFormValidation(AppFormValidation AppFormValidation)
        {
            if (AppFormValidation == null)
                throw new ArgumentNullException("AppFormValidation");

            db.Entry(AppFormValidation).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAppFormValidation(int AppFormValidationId = 0)
        {
            if (AppFormValidationId == 0)
                throw new ArgumentNullException("AppFormValidation");

            var AppFormValidation = (from s in db.AppFormValidations where s.AppFormValidationId == AppFormValidationId select s).FirstOrDefault();
            db.AppFormValidations.Remove(AppFormValidation);
            db.SaveChanges();
        }

        #endregion

        #region AppFormActionType

        public AppFormActionType GetAppFormActionTypeById(int AppFormActionTypeId, bool IsTrack = false)
        {
            if (AppFormActionTypeId == 0)
                throw new ArgumentNullException("AppFormActionType");

            var AppFormActionType = new AppFormActionType();
            if (IsTrack)
                AppFormActionType = (from s in db.AppFormActionTypes where s.FormActionTypeId == AppFormActionTypeId select s).FirstOrDefault();
            else
                AppFormActionType = (from s in db.AppFormActionTypes.AsNoTracking() where s.FormActionTypeId == AppFormActionTypeId select s).FirstOrDefault();

            return AppFormActionType;
        }

        public List<AppFormActionType> GetAllAppFormActionTypes(ref int totalcount, string FormActionType = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AppFormActionTypes.ToList();
            if (!String.IsNullOrEmpty(FormActionType))
                query = query.Where(n => n.FormActionType.ToLower() == FormActionType.ToLower()).ToList();

            totalcount = query.Count;
            var AppFormActionTypes = new PagedList<KSModel.Models.AppFormActionType>(query, PageIndex, PageSize);
            return AppFormActionTypes;
        }

        public void InsertAppFormActionType(AppFormActionType AppFormActionType)
        {
            if (AppFormActionType == null)
                throw new ArgumentNullException("AppFormActionType");

            db.AppFormActionTypes.Add(AppFormActionType);
            db.SaveChanges();
        }

        public void UpdateAppFormActionType(AppFormActionType AppFormActionType)
        {
            if (AppFormActionType == null)
                throw new ArgumentNullException("AppFormActionType");

            db.Entry(AppFormActionType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAppFormActionType(int AppFormActionTypeId = 0)
        {
            if (AppFormActionTypeId == 0)
                throw new ArgumentNullException("AppFormActionType");

            var AppFormActionType = (from s in db.AppFormActionTypes where s.FormActionTypeId == AppFormActionTypeId select s).FirstOrDefault();
            db.AppFormActionTypes.Remove(AppFormActionType);
            db.SaveChanges();
        }

        #endregion

        #region AppFormAction

        public AppFormAction GetAppFormActionById(int AppFormActionId, bool IsTrack = false)
        {
            if (AppFormActionId == 0)
                throw new ArgumentNullException("AppFormAction");

            var AppFormAction = new AppFormAction();
            if (IsTrack)
                AppFormAction = (from s in db.AppFormActions where s.AppFormActionId == AppFormActionId select s).FirstOrDefault();
            else
                AppFormAction = (from s in db.AppFormActions.AsNoTracking() where s.AppFormActionId == AppFormActionId select s).FirstOrDefault();

            return AppFormAction;
        }

        public List<AppFormAction> GetAllAppFormActions(ref int totalcount, int? FormActionTypeId = null, int? AppFormId = null,
            string ActionName = "", int? SortingIndex = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AppFormActions.ToList();
            if (!String.IsNullOrEmpty(ActionName))
                query = query.Where(n => n.ActionName.ToLower() == ActionName.ToLower()).ToList();
            if (FormActionTypeId > 0)
                query = query.Where(n => n.FormActionTypeId == FormActionTypeId).ToList();
            if (SortingIndex > 0)
                query = query.Where(n => n.SortingIndex == SortingIndex).ToList();
            if (AppFormId > 0)
                query = query.Where(n => n.AppFormId == AppFormId).ToList();

            totalcount = query.Count;
            var AppFormActions = new PagedList<KSModel.Models.AppFormAction>(query, PageIndex, PageSize);
            return AppFormActions;
        }

        public void InsertAppFormAction(AppFormAction AppFormAction)
        {
            if (AppFormAction == null)
                throw new ArgumentNullException("AppFormAction");

            db.AppFormActions.Add(AppFormAction);
            db.SaveChanges();
        }

        public void UpdateAppFormAction(AppFormAction AppFormAction)
        {
            if (AppFormAction == null)
                throw new ArgumentNullException("AppFormAction");

            db.Entry(AppFormAction).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAppFormAction(int AppFormActionId = 0)
        {
            if (AppFormActionId == 0)
                throw new ArgumentNullException("AppFormAction");

            var AppFormAction = (from s in db.AppFormActions where s.AppFormActionId == AppFormActionId select s).FirstOrDefault();
            db.AppFormActions.Remove(AppFormAction);
            db.SaveChanges();
        }

        #endregion

        #region AppFormGrid

        public AppFormGrid GetAppFormGridById(int AppFormGridId, bool IsTrack = false)
        {
            if (AppFormGridId == 0)
                throw new ArgumentNullException("AppFormGrid");

            var AppFormGrid = new AppFormGrid();
            if (IsTrack)
                AppFormGrid = (from s in db.AppFormGrids where s.AppFormGridId == AppFormGridId select s).FirstOrDefault();
            else
                AppFormGrid = (from s in db.AppFormGrids.AsNoTracking() where s.AppFormGridId == AppFormGridId select s).FirstOrDefault();

            return AppFormGrid;
        }

        public List<AppFormGrid> GetAllAppFormGrids(ref int totalcount, int? AppFormId = null,
            string GridTitle = "", string SubTitlePrefix = "", string SubTitleSuffix = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AppFormGrids.ToList();

            if (!String.IsNullOrEmpty(GridTitle))
                query = query.Where(n => n.GridTitle.ToLower() == GridTitle.ToLower()).ToList();
            if (AppFormId > 0)
                query = query.Where(n => n.AppFormId == AppFormId).ToList();
            if (!String.IsNullOrEmpty(SubTitlePrefix))
                query = query.Where(n => n.SubTitlePrefix.ToLower() == SubTitlePrefix.ToLower()).ToList();
            if (!String.IsNullOrEmpty(SubTitleSuffix))
                query = query.Where(n => n.SubTitleSuffix.ToLower() == SubTitleSuffix.ToLower()).ToList();

            totalcount = query.Count;

            var AppFormGrids = new PagedList<KSModel.Models.AppFormGrid>(query, PageIndex, PageSize);
            return AppFormGrids;
        }

        public void InsertAppFormGrid(AppFormGrid AppFormGrid)
        {
            if (AppFormGrid == null)
                throw new ArgumentNullException("AppFormGrid");

            db.AppFormGrids.Add(AppFormGrid);
            db.SaveChanges();
        }

        public void UpdateAppFormGrid(AppFormGrid AppFormGrid)
        {
            if (AppFormGrid == null)
                throw new ArgumentNullException("AppFormGrid");

            db.Entry(AppFormGrid).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAppFormGrid(int AppFormGridId = 0)
        {
            if (AppFormGridId == 0)
                throw new ArgumentNullException("AppFormGrid");

            var AppFormGrid = (from s in db.AppFormGrids where s.AppFormGridId == AppFormGridId select s).FirstOrDefault();
            db.AppFormGrids.Remove(AppFormGrid);
            db.SaveChanges();
        }

        #endregion

        #region UserRoleActionPermission

        public UserRoleActionPermission GetUserRoleActionPermissionById(int UserRoleActionPermissionId, bool IsTrack = false)
        {
            if (UserRoleActionPermissionId == 0)
                throw new ArgumentNullException("UserRoleActionPermission");

            var UserRoleActionPermission = new UserRoleActionPermission();
            if (IsTrack)
                UserRoleActionPermission = (from s in db.UserRoleActionPermissions where s.UserRoleActionPermissionId == UserRoleActionPermissionId select s).FirstOrDefault();
            else
                UserRoleActionPermission = (from s in db.UserRoleActionPermissions.AsNoTracking() where s.UserRoleActionPermissionId == UserRoleActionPermissionId select s).FirstOrDefault();

            return UserRoleActionPermission;
        }

        public IList<UserRoleActionPermission> GetAllUserRoleActionPermissionLessColumns(ref int totalcount, int? UserRoleId = null)
        {
            var query = db.UserRoleActionPermissions.ToList();
            if (UserRoleId > 0)
                query = query.Where(n => n.UserRoleId == UserRoleId).ToList();

            totalcount = query.Count;
            return query.ToList();
        }

        public List<UserRoleActionPermission> GetAllUserRoleActionPermissions(ref int totalcount, int? UserRoleId = null,
            int? AppFormId = null, int? AppFormActionId = null, bool? IsPermitted = null, bool? IsForm = null, 
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.UserRoleActionPermissions.ToList();
            if (UserRoleId > 0)
                query = query.Where(n => n.UserRoleId == UserRoleId).ToList();
            if (AppFormId > 0)
                query = query.Where(n => n.AppFormId == AppFormId).ToList();
            if (AppFormActionId > 0)
                query = query.Where(n => n.AppFormActionId == AppFormActionId).ToList();
            if (IsPermitted != null)
                query = query.Where(n => n.IsPermitted == IsPermitted).ToList();
            if (IsForm != null)
                query = query.Where(n => n.IsForm == IsForm).ToList();

            totalcount = query.Count;
            var UserRoleActionPermissions = new PagedList<KSModel.Models.UserRoleActionPermission>(query, PageIndex, PageSize);
            return UserRoleActionPermissions;
        }

        public void InsertUserRoleActionPermission(UserRoleActionPermission UserRoleActionPermission)
        {
            if (UserRoleActionPermission == null)
                throw new ArgumentNullException("UserRoleActionPermission");

            db.UserRoleActionPermissions.Add(UserRoleActionPermission);
            db.SaveChanges();
        }

        public void UpdateUserRoleActionPermission(UserRoleActionPermission UserRoleActionPermission)
        {
            if (UserRoleActionPermission == null)
                throw new ArgumentNullException("UserRoleActionPermission");

            db.Entry(UserRoleActionPermission).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteUserRoleActionPermission(int UserRoleActionPermissionId = 0)
        {
            if (UserRoleActionPermissionId == 0)
                throw new ArgumentNullException("UserRoleActionPermission");

            var UserRoleActionPermission = (from s in db.UserRoleActionPermissions where s.UserRoleActionPermissionId == UserRoleActionPermissionId select s).FirstOrDefault();
            db.UserRoleActionPermissions.Remove(UserRoleActionPermission);
            db.SaveChanges();
        }

        #endregion

        #endregion



        

        

        

        

        

        

        

        

        
    }
}