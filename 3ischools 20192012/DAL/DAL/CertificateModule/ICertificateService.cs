﻿using System;
using KSModel.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.ViewModel.IssueDocument;
using System.Data;

namespace DAL.DAL.CertificateModule
{
    public partial interface ICertificateService
    {

        #region Issue Doc

        IssueDoc GetIssueDocById(int IssueDocId, bool IsTrack = false);

        IList<KSModel.Models.IssueDoc> GetAllIssueDocnotrack(bool IsTrack = false);

        IList<IssueDoc> GetAllIssueDoc(ref int totalcount, int? IssueDocId = null, DateTime? IssueDate = null, int? DocNo = null, int? IssueDocTypeId = null,
                                           int? SessionId = null, int? StudentId = null, int? DocTemplateId = null,
                                           bool IsPrinted = false, int PageIndex = 0, int PageSize = int.MaxValue);


        IList<IssueDocInfoViewModel> GetAllIssueDocsBySP(ref int totalcount, int? IssueDocId = null, int? ClassId = null, string IssueFromDate = "",
                            string IssueToDate = "", int? StudentId = null, string AdmissionNo = "", int? IssueDocTypeId = null,
                            int? SessionId = null, string DocNo = "", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertIssueDoc(IssueDoc IssueDoc);

        void UpdateIssueDoc(IssueDoc IssueDoc);

        void DeleteIssueDoc(int IssueDocId = 0);

        #endregion

        #region IssueDocFeeHead
        KSModel.Models.IssueDocFeeHead GetIssueDocFeeHeadById(int IssueDocFeeHeadId, bool IsTrack = false);

        IList<KSModel.Models.IssueDocFeeHead> GetAllIssueDocFeeHeads(ref int totalcount, int? IssueDocId = null,
                        int? FeeHeadId = null, decimal? Amount = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertIssueDocFeeHead(KSModel.Models.IssueDocFeeHead IssueDocFeeHead);

        void UpdateIssueDocFeeHead(KSModel.Models.IssueDocFeeHead IssueDocFeeHead);

        void DeleteIssueDocFeeHead(int IssueDocFeeHeadId = 0);

        void DeleteIssueDocFeeHeadByIssueDoc(int IssueDocId = 0, int? FeeHeadId = null);
        #endregion

        #region Issue DocType

        IssueDocType GetIssueDocTypeById(int IssueDocTypeId, bool IsTrack = false);

        IList<IssueDocType> GetAllIssueDocType(ref int totalcount, string IssueDocType = "", int PageIndex = 0,
                                                   int PageSize = int.MaxValue, bool? IsLikeFilter = null);

        DataTable GetAllIssueDocTokensListbySp(ref int totalcount, int? SessionId = null, int? StudentId = null,
                                                 int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertIssueDocType(IssueDocType IssueDocType);

        void UpdateIssueDocType(IssueDocType IssueDocType);

        void DeleteIssueDocType(int IssueDocTypeId = 0);

        #endregion

        #region Issue DocTypeCols

        IssueDocTypeCol GetIssueDocTypeColById(int IssueDocTypeColId, bool IsTrack = false);

        IList<IssueDocTypeCol> GetAllIssueDocTypeCol(ref int totalcount, string ColName = "", string ColCode = "",
            bool? IsTC = null, int? IssueDocTypeId = null, bool? IsFormfield = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertIssueDocTypeCol(IssueDocTypeCol IssueDocTypeCol);

        void UpdateIssueDocTypeCol(IssueDocTypeCol IssueDocTypeCol);

        void DeleteIssueDocTypeCol(int IssueDocTypeColId = 0);

        void DeleteIssueDocTypeColWithDocType(int IssueDocTypeId = 0);

        #endregion

        #region Issue DocDetail

        IssueDocDetail GetIssueDocDetailById(int IssueDocDetailId, bool IsTrack = false);

        IList<IssueDocDetail> GetAllIssueDocDetail(ref int totalcount, int? IssueDocId = null, int? IssueDocTypeColId = null, string ColValue = "",
                                                       int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertIssueDocDetail(IssueDocDetail IssueDocDetail);

        void UpdateIssueDocDetail(IssueDocDetail IssueDocDetail);

        void DeleteIssueDocDetail(int IssueDocDetailId = 0);

        void DeleteIssueDocDetailWithDocId(int IssueDocId = 0);

        void SaveIssueDocDetailWithSP(int StudentId, int SessionId, int IssueDocId, string IssueDocType);

        #endregion

        #region Issue Doc Template

        IssueDocTemplate GetIssueDocTemplateById(int IssueDocTemplateId, bool IsTrack = false);

        IList<IssueDocTemplate> GetAllIssueDocTemplates(ref int totalcount, int? IssueDocTypeId = null, string TemplateTitle = "",
                                                    int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertIssueDocTemplate(IssueDocTemplate IssueDocTemplate);

        void UpdateIssueDocTemplate(IssueDocTemplate IssueDocTemplate);

        void DeleteIssueDocTemplate(int IssueDocTemplateId = 0);

        #endregion

        #region Issue Doc Template Detail

        IssueDocTemplateDetail GetIssueDocTemplateDetailById(int IssueDocTemplateDetailId, bool IsTrack = false);

        IList<IssueDocTemplateDetail> GetAllIssueDocTemplateDetails(ref int totalcount, int? IssueDocTemplateId = null,
            DateTime? EffectiveDate = null, DateTime? EndDate = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertIssueDocTemplateDetail(IssueDocTemplateDetail IssueDocTemplateDetail);

        void UpdateIssueDocTemplateDetail(IssueDocTemplateDetail IssueDocTemplateDetail);

        void DeleteIssueDocTemplateDetail(int IssueDocTemplateDetailId = 0);

        #endregion

        #region Issue Doc HTML Template

        IssueDocHTMLTemplate GetIssueDocHTMLTemplateById(int IssueDocHTMLTemplateId, bool IsTrack = false);

        IList<IssueDocHTMLTemplate> GetAllIssueDocHTMLTemplates(ref int totalcount, string HTMLTitle = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertIssueDocHTMLTemplate(IssueDocHTMLTemplate IssueDocHTMLTemplate);

        void UpdateIssueDocHTMLTemplate(IssueDocHTMLTemplate IssueDocHTMLTemplate);

        void DeleteIssueDocHTMLTemplate(int IssueDocHTMLTemplateId = 0);

        #endregion

        #region Issue Doc HTML Template

        IssueDocTemplateHTML GetIssueDocTemplateHTMLById(int IssueDocTemplateHTMLId, bool IsTrack = false);

        IList<IssueDocTemplateHTML> GetAllIssueDocTemplateHTMLs(ref int totalcount, int? IssueDocTemplateId = null, int? IssueDocHTMLTemplateId = null,
            DateTime? EffectiveDate = null, DateTime? EndDate = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertIssueDocTemplateHTML(IssueDocTemplateHTML IssueDocTemplateHTML);

        void UpdateIssueDocTemplateHTML(IssueDocTemplateHTML IssueDocTemplateHTML);

        void DeleteIssueDocTemplateHTML(int IssueDocTemplateHTMLId = 0);

        #endregion

    }
}
