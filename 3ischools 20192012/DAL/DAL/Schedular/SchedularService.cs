﻿using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.DAL.Schedular
{
    public partial class SchedularService : ISchedularService
    {
        public int count = 0;
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        //private readonly string SqlDataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;
        public SchedularService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods

        #region Event
        public KSModel.Models.Event GetEventById(int EventId, bool IsTrack = false)
        {
            if (EventId == 0)
                throw new ArgumentNullException("Event");

            var events = new Event();
            if (IsTrack)
                events = (from s in db.Events where s.EventId == EventId select s).FirstOrDefault();
            else
                events = (from s in db.Events.AsNoTracking() where s.EventId == EventId select s).FirstOrDefault();
            return events;
        }

        public IList<KSModel.Models.Event> GetAllEventList(ref int totalcount, string EventTitle = "", int? ClassId = null,
            bool IsAllSchool = false, DateTime? StartDate = null, DateTime? EndDate = null,
            TimeSpan? StartTime = null, TimeSpan? EndTime = null, string Description = "", bool IsActive = false, bool IsDeleted = false, string Venue = "",
            int? EventTypeId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Events.ToList();
            if (!String.IsNullOrEmpty(EventTitle))
                query = query.Where(e => e.EventTitle.ToLower().Contains(EventTitle.ToLower().Trim())).ToList();

            //if (ClassId != null && ClassId > 0)
            //    query = query.Where(e => e.ClassId == ClassId).ToList();

            if (IsAllSchool)
                query = query.Where(e => e.IsAllSchool == IsAllSchool).ToList();

            var IsGreaterThanEqualTo=false;

            if(EventTypeId!=null &&EventTypeId>0){
                var EventCols = GetAllEventColsList(ref count).Where(f =>f.EventTypeId==EventTypeId&& f.ColName.ToLower().Contains("enddate"));
                if (EventCols.Count() > 0)
                    IsGreaterThanEqualTo = true;
            }


            if (IsGreaterThanEqualTo)
            {
                if (StartDate.HasValue)
                {
                    query = query.Where(e => e.StartDate >= StartDate).ToList();
                }
                if (EndDate.HasValue)
                {
                    query = query.Where(e => e.EndDate <= EndDate).ToList();
                }
            }
            else
            {
                if (StartDate.HasValue)
                {
                    query = query.Where(e => e.StartDate >= StartDate).ToList();
                }
                if (EndDate.HasValue)
                {
                    query = query.Where(e => e.StartDate <= EndDate).ToList();
                }
            }
           

            if (StartTime.HasValue && EndTime.HasValue)
            {
                query = query.Where(e => e.StartTime >= StartTime && e.EndTime <= EndTime).ToList();
            }


            if (!String.IsNullOrEmpty(Description))
                query = query.Where(e => e.Description.ToLower().Contains(Description.ToLower().Trim())).ToList();

            if (IsActive)
                query = query.Where(e => e.IsActive == IsActive).ToList();

            if (!String.IsNullOrEmpty(Venue))
                query = query.Where(e => e.Venue.ToLower().Contains(Venue.ToLower().Trim())).ToList();


            if (EventTypeId != null && EventTypeId > 0)
                query = query.Where(e => e.EventTypeId == EventTypeId).ToList();

            query = query.OrderByDescending(M => M.EventId).ToList();
            query = query.Where(m => m.IsDeleted == false).ToList();
            totalcount = query.Count;
            var EventList = new PagedList<KSModel.Models.Event>(query, PageIndex, PageSize);
            return EventList;
        }

        public void InsertEvent(KSModel.Models.Event Event)
        {

            if (Event == null)
                throw new ArgumentNullException("Event");

            db.Events.Add(Event);
            db.SaveChanges();
        }

        public void UpdateEvent(KSModel.Models.Event Event)
        {
            if (Event == null)
                throw new ArgumentNullException("Event");

            db.Entry(Event).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteEvent(int EventId = 0)
        {
            if (EventId == 0)
                throw new ArgumentNullException("EventId");

            var Event = (from s in db.Events where s.EventId == EventId select s).FirstOrDefault();
            db.Events.Remove(Event);
            db.SaveChanges();
        }
        #endregion

        #region Event Type
        public KSModel.Models.EventType GetEventTypeById(int EventTypeId, bool IsTrack = false)
        {
            if (EventTypeId == 0)
                throw new ArgumentNullException("EventType");

            var EventType = new EventType();
            if (IsTrack)
                EventType = (from s in db.EventTypes where s.EventTypeId == EventTypeId select s).FirstOrDefault();
            else
                EventType = (from s in db.EventTypes.AsNoTracking() where s.EventTypeId == EventTypeId select s).FirstOrDefault();
            return EventType;
        }

        public IList<KSModel.Models.EventType> GetAllEventTypeList(ref int totalcount, string EventType = "",string EventCode="", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.EventTypes.ToList();

            if (!String.IsNullOrEmpty(EventType))
                query = query.Where(e => EventType.ToLower().Contains(e.EventType1.ToLower())).ToList();
            //query = query.OrderByDescending(M => M.EventTypeId).ToList();
            totalcount = query.Count;
            if(!string.IsNullOrEmpty(EventCode))
                query = query.Where(e => EventCode.ToLower().Contains(e.EventCode.ToLower())).ToList();

            var EventTypeList = new PagedList<KSModel.Models.EventType>(query, PageIndex, PageSize);
            return EventTypeList;
        }

        public void InsertEventType(KSModel.Models.EventType EventType)
        {
            if (EventType == null)
                throw new ArgumentNullException("EventType");

            db.EventTypes.Add(EventType);
            db.SaveChanges();
        }

        public void UpdateEventType(KSModel.Models.EventType EventType)
        {
            if (EventType == null)
                throw new ArgumentNullException("EventType");

            db.Entry(EventType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteEventType(int EventTypeId = 0)
        {
            if (EventTypeId == 0)
                throw new ArgumentNullException("EventType");

            var EventType = (from s in db.EventTypes where s.EventTypeId == EventTypeId select s).FirstOrDefault();
            db.EventTypes.Remove(EventType);
            db.SaveChanges();
        }
        #endregion

        #region EventCols
        public KSModel.Models.EventCol GetEventColsId(int EventColsId, bool IsTrack = false)
        {
            if (EventColsId == 0)
                throw new ArgumentNullException("EventCols");

            var EventCol = new EventCol();
            if (IsTrack)
                EventCol = (from s in db.EventCols where s.EventColsId == EventColsId select s).FirstOrDefault();
            else
                EventCol = (from s in db.EventCols.AsNoTracking() where s.EventColsId == EventColsId select s).FirstOrDefault();
            return EventCol;
        }

        public IList<KSModel.Models.EventCol> GetAllEventColsList(ref int totalcount, string ColName = "", int? EventTypeId = null,
            bool IsMandatory = false, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.EventCols.ToList();

            if (!String.IsNullOrEmpty(ColName))
                query = query.Where(e => ColName.ToLower().Contains(e.ColName.ToLower())).ToList();

            if (EventTypeId != null && EventTypeId > 0)
                query = query.Where(e => e.EventTypeId == EventTypeId).ToList();

            if (IsMandatory)
                query = query.Where(e => e.IsMandatory == IsMandatory).ToList();

            totalcount = query.Count;
            var EventColList = new PagedList<KSModel.Models.EventCol>(query, PageIndex, PageSize);
            return EventColList;
        }

        public void InsertEventCol(KSModel.Models.EventCol EventCol)
        {
            if (EventCol == null)
                throw new ArgumentNullException("EventCol");

            db.EventCols.Add(EventCol);
            db.SaveChanges();
        }

        public void UpdateEventCol(KSModel.Models.EventCol EventCol)
        {
            if (EventCol == null)
                throw new ArgumentNullException("EventCol");

            db.Entry(EventCol).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteEventCol(int EventColsId = 0)
        {
            if (EventColsId == 0)
                throw new ArgumentNullException("EventCol");

            var EventCol = (from s in db.EventCols where s.EventColsId == EventColsId select s).FirstOrDefault();
            db.EventCols.Remove(EventCol);
            db.SaveChanges();
        }
        #endregion

        #region Event Image
        public KSModel.Models.EventImage GetEventImageById(int EventImageId, bool IsTrack = false)
        {
            if (EventImageId == 0)
                throw new ArgumentNullException("EventImage");

            var EventImages = new EventImage();
            if (IsTrack)
                EventImages = (from s in db.EventImages where s.EventImageId == EventImageId select s).FirstOrDefault();
            else
                EventImages = (from s in db.EventImages.AsNoTracking() where s.EventImageId == EventImageId select s).FirstOrDefault();
            return EventImages;
        }

        public IList<KSModel.Models.EventImage> GetAllEventImageList(ref int totalcount, string ImageCaption = "", string ImageName = "", int? EventId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.EventImages.ToList();

            if (!String.IsNullOrEmpty(ImageCaption))
                query = query.Where(e => ImageCaption.ToLower().Contains(e.ImageCaption.ToLower())).ToList();

            if (!String.IsNullOrEmpty(ImageName))
                query = query.Where(e => ImageName.ToLower().Contains(e.ImageName.ToLower())).ToList();

            if (EventId != null && EventId > 0)
                query = query.Where(e => e.EventId == EventId).ToList();

            query = query.OrderByDescending(M => M.EventImageId).ToList();
            totalcount = query.Count;
            var EventImageList = new PagedList<KSModel.Models.EventImage>(query, PageIndex, PageSize);
            return EventImageList;
        }

        public void InsertEventImage(KSModel.Models.EventImage EventImage)
        {
            if (EventImage == null)
                throw new ArgumentNullException("EventImage");

            db.EventImages.Add(EventImage);
            db.SaveChanges();
        }

        public void UpdateEventImage(KSModel.Models.EventImage EventImage)
        {
            if (EventImage == null)
                throw new ArgumentNullException("EventImage");

            db.Entry(EventImage).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteEventImage(int EventImageId = 0)
        {
            if (EventImageId == 0)
                throw new ArgumentNullException("EventImage");

            var EventImage = (from s in db.EventImages where s.EventImageId == EventImageId select s).FirstOrDefault();
            db.EventImages.Remove(EventImage);
            db.SaveChanges();
        }
        #endregion

        #region EventDetail
        public KSModel.Models.EventDetail GetEventDetail(int EventDetailId, bool IsTrack = false)
        {
            if (EventDetailId == 0)
                throw new ArgumentNullException("EventDetail");

            var EventDetail = new EventDetail();
            if (IsTrack)
                EventDetail = (from s in db.EventDetails where s.EventDetailId == EventDetailId select s).FirstOrDefault();
            else
                EventDetail = (from s in db.EventDetails.AsNoTracking() where s.EventDetailId == EventDetailId select s).FirstOrDefault();
            return EventDetail;
        }

        public IList<KSModel.Models.EventDetail> GetAllEventDetailList(ref int totalcount, int? EventId = null, int? ClassId = null,
             int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.EventDetails.ToList();

            if (EventId != null && EventId > 0)
                query = query.Where(e => e.EventId == EventId).ToList();

            if (ClassId != null && ClassId > 0)
                query = query.Where(e => e.ClassId == ClassId).ToList();

            totalcount = query.Count;
            var EventDetailList = new PagedList<KSModel.Models.EventDetail>(query, PageIndex, PageSize);
            return EventDetailList;
        }

        public void InsertEventDetail(KSModel.Models.EventDetail EventDetail)
        {
            if (EventDetail == null)
                throw new ArgumentNullException("EventDetail");

            db.EventDetails.Add(EventDetail);
            db.SaveChanges();
        }

        public void UpdateEventDetail(KSModel.Models.EventDetail EventDetail)
        {
            if (EventDetail == null)
                throw new ArgumentNullException("EventDetail");

            db.Entry(EventDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteEventDetail(int EventDetailId = 0)
        {
            if (EventDetailId == 0)
                throw new ArgumentNullException("EventDetail");

            var EventDetail = (from s in db.EventDetails where s.EventDetailId == EventDetailId select s).FirstOrDefault();
            db.EventDetails.Remove(EventDetail);
            db.SaveChanges();
        }
        #endregion

        #region EventCircularDoc
        public KSModel.Models.EventCircularDoc GetEventCircularDoc(int CircularDocId, bool IsTrack = false)
        {
            if (CircularDocId == 0)
                throw new ArgumentNullException("EventDetail");

            var EventCircularDoc = new EventCircularDoc();
            if (IsTrack)
                EventCircularDoc = (from s in db.EventCircularDocs where s.CircularDocId == CircularDocId select s).FirstOrDefault();
            else
                EventCircularDoc = (from s in db.EventCircularDocs.AsNoTracking() where s.CircularDocId == CircularDocId select s).FirstOrDefault();
            return EventCircularDoc;
        }

        public IList<KSModel.Models.EventCircularDoc> GetAllEventCircularDocs(ref int totalcount, int? EventId = null, string FileName = "",
                                                                 int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.EventCircularDocs.ToList();

            if (EventId != null && EventId > 0)
                query = query.Where(e => e.EventId == EventId).ToList();

            if (!string.IsNullOrEmpty(FileName))
                query = query.Where(e => e.FileName.ToLower() == FileName.ToLower()).ToList();

            totalcount = query.Count;
            var EventCircularDocList = new PagedList<KSModel.Models.EventCircularDoc>(query, PageIndex, PageSize);
            return EventCircularDocList;
        }

        public void InsertEventCircularDoc(KSModel.Models.EventCircularDoc EventCircularDoc)
        {
            if (EventCircularDoc == null)
                throw new ArgumentNullException("EventCircularDoc");

            db.EventCircularDocs.Add(EventCircularDoc);
            db.SaveChanges();
        }

        public void UpdateEventCircularDoc(KSModel.Models.EventCircularDoc EventCircularDoc)
        {
            if (EventCircularDoc == null)
                throw new ArgumentNullException("EventCircularDoc");

            db.Entry(EventCircularDoc).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteEventCircularDoc(int CircularDocId = 0)
        {
            if (CircularDocId == 0)
                throw new ArgumentNullException("EventCircularDoc");

            var EventCircularDoc = (from s in db.EventCircularDocs where s.CircularDocId == CircularDocId select s).FirstOrDefault();
            db.EventCircularDocs.Remove(EventCircularDoc);
            db.SaveChanges();
        }
        #endregion

        #endregion

    }
}