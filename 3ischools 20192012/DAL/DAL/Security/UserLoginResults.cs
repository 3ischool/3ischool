﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.DAL.Security
{
    public enum UserLoginResults
    {
        /// <summary>
        /// Login successful
        /// </summary>
        Successful = 1,
        /// <summary>
        /// Customer dies not exist (email or username)
        /// </summary>
        CustomerNotExist = 2,
        /// <summary>
        /// Wrong password
        /// </summary>
        WrongPassword = 3,
        //for not active user
        UserNotActive = 4,
        //for user left
        UserLeft = 5,

        NotAuthorizedIP = 6,
        //for not active user
        TryAgainLater = 7,

        InActiveUserRole = 8,
        PasswordExpired=9
    }
}