﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSModel.Models;
using System.Data;
using ViewModel.ViewModel.Reporting;
//using BAL.BAL.Reporting;

namespace DAL.DAL.ReportingModule
{
    public interface IReportService
    {
        #region Report Master

        RptMaster GetRptMasterById(int RptMasterId, bool IsTrack = false);

        List<RptMaster> GetAllRptMasters(ref int totalcount, DateTime? CreationDate = null, string RptMasterName = null, string RptSPName = null,
            string GenericName = null, bool? IsActive = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertRptMaster(RptMaster RptMaster);

        void UpdateRptMaster(RptMaster RptMaster);

        void DeleteRptMaster(int RptMasterId = 0);

        DataTable GetStandardReportCols(string StoreProcedureName, List<StoreProcedureParameter> paralist);

        #endregion

        #region Report Parameter

        RptParameter GetRptParameterById(int RptParameterId, bool IsTrack = false);

        List<RptParameter> GetAllRptParameters(ref int totalcount, int? RptMasterId = null, string ParameterName = null,
             int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertRptParameter(RptParameter RptParameter);

        void UpdateRptParameter(RptParameter RptParameter);

        void DeleteRptParameter(int RptParameterId = 0);

        #endregion

        #region Report PageNoPosition

        RptPageNoPosition GetRptPageNoPositionById(int RptPageNoPositionId, bool IsTrack = false);

        List<RptPageNoPosition> GetAllRptPageNoPositions(ref int totalcount, string RptPageNoPosition = null, int PageIndex = 0,
            int PageSize = int.MaxValue);

        void InsertRptPageNoPosition(RptPageNoPosition RptPageNoPosition);

        void UpdateRptPageNoPosition(RptPageNoPosition RptPageNoPosition);

        void DeleteRptPageNoPosition(int RptPageNoPositionId = 0);

        #endregion

        #region Report Layout

        RptLayout GetRptLayoutById(int RptLayoutId, bool IsTrack = false);

        List<RptLayout> GetAllRptLayouts(ref int totalcount, int? RptMasterId = null, bool? IsPageTotal = null, bool? IsTotalOpening = null,
             bool? IsPageNo = null, int? PageNoPositionId = null, DateTime? CurrentDate = null, TimeSpan? CurrentTime = null,
            bool? IsOrgAddress = null, bool? IsOrgName = null, bool? IsOrgLogo = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertRptLayout(RptLayout RptLayout);

        void UpdateRptLayout(RptLayout RptLayout);

        void DeleteRptLayout(int RptLayoutId = 0);

        #endregion

        #region Report FilterControlType

        RptFilterControlType GetRptFilterControlTypeById(int RptFilterControlTypeId, bool IsTrack = false);

        List<RptFilterControlType> GetAllRptFilterControlTypes(ref int totalcount, string FilterControlType = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertRptFilterControlType(RptFilterControlType RptFilterControlType);

        void UpdateRptFilterControlType(RptFilterControlType RptFilterControlType);

        void DeleteRptFilterControlType(int RptFilterControlTypeId = 0);

        #endregion

        #region Report Standard Col

        RptStandardCol GetRptStandardColById(int RptStandardColId, bool IsTrack = false);

        List<RptStandardCol> GetAllRptStandardCols(ref int totalcount, int? RptMasterId = null, string ColName = null, int? ColIndex = null,
             bool? IsFilter = null, int? FilterControlTypeId = null, bool? IsVisible = null, bool? IsSorted = null,
            int? SortingIndex = null, bool? IsGroup = null, bool? IsSum = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertRptStandardCol(RptStandardCol RptStandardCol);

        void UpdateRptStandardCol(RptStandardCol RptStandardCol);

        void DeleteRptStandardCol(int RptStandardColId = 0);

        #endregion

        #region Report Generic Col

        RptGenericCol GetRptGenericColById(int RptGenericColId, bool IsTrack = false);

        List<RptGenericCol> GetAllRptGenericCols(ref int totalcount, int? RptMasterId = null, string ColName = null, int? ColIndex = null,
             bool? IsFilter = null, bool? IsVisible = null, bool? IsGroup = null, bool? IsSum = null, int PageIndex = 0,
            int PageSize = int.MaxValue);

        void InsertRptGenericCol(RptGenericCol RptGenericCol);

        void UpdateRptGenericCol(RptGenericCol RptGenericCol);

        void DeleteRptGenericCol(int RptGenericColId = 0);

        #endregion

        #region Report CustomMaster

        RptCustomMaster GetRptCustomMasterById(int RptCustomMasterId, bool IsTrack = false);

        List<RptCustomMaster> GetAllRptCustomMasters(ref int totalcount, int? RptMasterId = null, int? UserId = null,
             int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertRptCustomMaster(RptCustomMaster RptCustomMaster);

        void UpdateRptCustomMaster(RptCustomMaster RptCustomMaster);

        void DeleteRptCustomMaster(int RptCustomMasterId = 0);

        #endregion

        #region Report Custom Col

        RptCustomCol GetRptCustomColById(int RptCustomColId, bool IsTrack = false);

        List<RptCustomCol> GetAllRptCustomCols(ref int totalcount, int? RptCustomMasterId = null, string ColName = null, int? ColIndex = null,
            bool? IsVisible = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertRptCustomCol(RptCustomCol RptCustomCol);

        void UpdateRptCustomCol(RptCustomCol RptCustomCol);

        void DeleteRptCustomCol(int RptCustomColId = 0);

        #endregion

    }
}
