﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSModel.Models;

namespace DAL.DAL.AddressModule
{
    public partial interface IAddressService
    {
        #region  Address

        Address GetAddressById(int AddressId,bool IsTrack = false);

        List<Address> GetAllAddresss(ref int totalcount , int? contactId = null, int? addressTypeId = null, int? contacttypeId = null,
            string plotno = null, string sector_street = null, string Locality = null ,  string landmark = null , int? cityid = null,
            string zip = "", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAddress(Address Address);

        void UpdateAddress(Address Address);

        void DeleteAddress(int AddressId = 0);

        #endregion 
    }
}
