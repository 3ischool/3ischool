﻿using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DAL.DAL.AddressModule
{
    public partial class AddressService : IAddressService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public AddressService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region  Methods

        #region Address

        public KSModel.Models.Address GetAddressById(int AddressId, bool IsTrack = false)
        {
            if (AddressId == 0)
                throw new ArgumentNullException("Address");

            var Address = new Address();
            if(IsTrack)
                Address = (from s in db.Addresses where s.AddressId == AddressId select s).FirstOrDefault();
            else
                Address = (from s in db.Addresses.AsNoTracking() where s.AddressId == AddressId select s).FirstOrDefault();

            return Address;
        }

        public List<KSModel.Models.Address> GetAllAddresss(ref int totalcount, int? contactId = null, int? addressTypeId = null,
            int? contacttypeId = null, string plotno = null, string sector_street = null, string Locality = null, string landmark = null, 
            int? cityid = null, string zip = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Addresses.ToList();
            if (contactId != null && contactId > 0)
                query = query.Where(e => e.ContactId == contactId).ToList();
            if (addressTypeId != null && addressTypeId > 0)
                query = query.Where(e => e.AddressTypeId == addressTypeId).ToList();
            if (contacttypeId != null && contacttypeId > 0)
                query = query.Where(e => e.ContactTypeId == contacttypeId).ToList();
            if (cityid != null && cityid > 0)
                query = query.Where(e => e.CityId == cityid).ToList();
            if (plotno != null && plotno != "")
                query = query.Where(e => e.PlotNo == plotno).ToList();
            if (!String.IsNullOrEmpty(sector_street))
                query = query.Where(e => e.Sector_Street.ToLower().Contains(sector_street.ToLower())).ToList();
            if (!String.IsNullOrEmpty(Locality))
                query = query.Where(e => e.Locality.ToLower().Contains(Locality.ToLower())).ToList();
            if (!String.IsNullOrEmpty(landmark))
                query = query.Where(e => e.Landmark.ToLower().Contains(landmark.ToLower())).ToList();
            if (!String.IsNullOrEmpty(zip))
                query = query.Where(e => e.Zip == zip).ToList();

            totalcount = query.Count;
            var Addresses = new PagedList<KSModel.Models.Address>(query, PageIndex, PageSize);
            return Addresses;
        }

        public void InsertAddress(KSModel.Models.Address Address)
        {
            if (Address == null)
                throw new ArgumentNullException("Address");

            db.Addresses.Add(Address);
            db.SaveChanges();
        }

        public void UpdateAddress(KSModel.Models.Address Address)
        {
            if (Address == null)
                throw new ArgumentNullException("Address");

            db.Entry(Address).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAddress(int AddressId = 0)
        {
            if (AddressId == 0)
                throw new ArgumentNullException("Address");

            var Address = (from s in db.Addresses where s.AddressId == AddressId select s).FirstOrDefault();
            db.Addresses.Remove(Address);
            db.SaveChanges();
        }

        #endregion

        #endregion
    }
}