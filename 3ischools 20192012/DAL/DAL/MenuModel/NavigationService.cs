﻿using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.DAL.MenuModel
{
    public partial class NavigationService : INavigationService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public NavigationService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods

        #region NavigationItem

        public KSModel.Models.NavigationItem GetNavigationItemById(int NavigationItemId, bool IsTrack = false)
        {
            if (NavigationItemId == 0)
                throw new ArgumentNullException("NavigationItem");

            var NavigationItem = new NavigationItem();
            if (IsTrack)
                NavigationItem = (from s in db.NavigationItems where s.NavigationItemId == NavigationItemId select s).FirstOrDefault();
            else
                NavigationItem = (from s in db.NavigationItems.AsNoTracking() where s.NavigationItemId == NavigationItemId select s).FirstOrDefault();

            return NavigationItem;
        }

        public List<KSModel.Models.NavigationItem> GetAllNavigationItems(ref int totalcount, string NavigationItem = "", string QueryString = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.NavigationItems.ToList();
            if (!String.IsNullOrEmpty(NavigationItem))
                query = query.Where(n => n.NavigationItem1.ToLower().Contains(NavigationItem.ToLower())).ToList();
            if (!String.IsNullOrEmpty(QueryString))
                query = query.Where(n => n.QueryString == QueryString).ToList();

            totalcount = query.Count;
            var NavigationItems = new PagedList<KSModel.Models.NavigationItem>(query, PageIndex, PageSize);
            return NavigationItems;
        }

        public void InsertNavigationItem(KSModel.Models.NavigationItem NavigationItem)
        {
            if (NavigationItem == null)
                throw new ArgumentNullException("NavigationItem");

            db.NavigationItems.Add(NavigationItem);
            db.SaveChanges();
        }

        public void UpdateNavigationItem(KSModel.Models.NavigationItem NavigationItem)
        {
            if (NavigationItem == null)
                throw new ArgumentNullException("NavigationItem");

            db.Entry(NavigationItem).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteNavigationItem(int NavigationItemId = 0)
        {
            if (NavigationItemId == 0)
                throw new ArgumentNullException("NavigationItem");

            var NavigationItem = (from s in db.NavigationItems where s.NavigationItemId == NavigationItemId select s).FirstOrDefault();
            db.NavigationItems.Remove(NavigationItem);
            db.SaveChanges();
        }

        #endregion

        #region UserRoleNavigationItem

        public UserRoleNavigationItem GetUserRoleNavigationItemById(int UserRoleNavigationItemId, bool IsTrack = false)
        {
            if (UserRoleNavigationItemId == 0)
                throw new ArgumentNullException("UserRoleNavigationItem");

            var UserRoleNavigationItem = new UserRoleNavigationItem();
            if (IsTrack)
                UserRoleNavigationItem = (from s in db.UserRoleNavigationItems where s.UserRoleNavigationItemId == UserRoleNavigationItemId select s).FirstOrDefault();
            else
                UserRoleNavigationItem = (from s in db.UserRoleNavigationItems.AsNoTracking() where s.UserRoleNavigationItemId == UserRoleNavigationItemId select s).FirstOrDefault();

            return UserRoleNavigationItem;
        }

        public List<UserRoleNavigationItem> GetAllUserRoleNavigationItems(ref int totalcount, int? UserRoleId = null,
            int? NavigationItemId = null, bool? IsActive = null, 
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.UserRoleNavigationItems.ToList();
         
            if (UserRoleId > 0)
                query = query.Where(n => n.UserRoleId == UserRoleId).ToList();
            if (NavigationItemId > 0)
                query = query.Where(n => n.NavigationItemId == NavigationItemId).ToList();
          
            if (IsActive != null)
                query = query.Where(n => n.IsActive == IsActive).ToList();

            totalcount = query.Count;
            var UserRoleNavigationItems = new PagedList<KSModel.Models.UserRoleNavigationItem>(query, PageIndex, PageSize);
            return UserRoleNavigationItems;
        }

        public void InsertUserRoleNavigationItem(UserRoleNavigationItem UserRoleNavigationItem)
        {
            if (UserRoleNavigationItem == null)
                throw new ArgumentNullException("UserRoleNavigationItem");

            db.UserRoleNavigationItems.Add(UserRoleNavigationItem);
            db.SaveChanges();
        }

        public void UpdateUserRoleNavigationItem(UserRoleNavigationItem UserRoleNavigationItem)
        {
            if (UserRoleNavigationItem == null)
                throw new ArgumentNullException("UserRoleNavigationItem");

            db.Entry(UserRoleNavigationItem).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteUserRoleNavigationItem(int UserRoleNavigationItemId = 0)
        {
            if (UserRoleNavigationItemId == 0)
                throw new ArgumentNullException("UserRoleNavigationItem");

            var UserRoleNavigationItem = (from s in db.UserRoleNavigationItems where s.UserRoleNavigationItemId == UserRoleNavigationItemId select s).FirstOrDefault();
            db.UserRoleNavigationItems.Remove(UserRoleNavigationItem);
            db.SaveChanges();
        }

        #endregion

        #region UserMobNavigationItem

        public UserMobNavigationItem GetUserMobNavigationItemById(int UserMobNavigationItemId, bool IsTrack = false)
        {
            if (UserMobNavigationItemId == 0)
                throw new ArgumentNullException("UserMobNavigationItem");

            var UserMobNavigationItem = new UserMobNavigationItem();
            if (IsTrack)
                UserMobNavigationItem = (from s in db.UserMobNavigationItems where s.MobUserNavigationItemId == UserMobNavigationItemId select s).FirstOrDefault();
            else
                UserMobNavigationItem = (from s in db.UserMobNavigationItems.AsNoTracking() where s.MobUserNavigationItemId == UserMobNavigationItemId select s).FirstOrDefault();

            return UserMobNavigationItem;
        }

        public List<UserMobNavigationItem> GetAllUserMobNavigationItems(ref int totalcount, string NavigationItem = "", int? UserRoleId = null,
            int? NavigationItemId = null, int? RefId = null, int? SortingIndex = null, int? NavigationLevel = null, bool? IsActive = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.UserMobNavigationItems.ToList();
            if (!String.IsNullOrEmpty(NavigationItem))
                query = query.Where(n => n.NavigationItem.ToLower().Contains(NavigationItem.ToLower())).ToList();
            if (UserRoleId > 0)
                query = query.Where(n => n.UserRoleId == UserRoleId).ToList();
            if (NavigationItemId > 0)
                query = query.Where(n => n.NavigationItemId == NavigationItemId).ToList();
            if (RefId > 0)
                query = query.Where(n => n.RefId == RefId).ToList();
            if (SortingIndex > 0)
                query = query.Where(n => n.SortingIndex == SortingIndex).ToList();
            if (NavigationLevel > 0)
                query = query.Where(n => n.NavigationLevel == NavigationLevel).ToList();
            if (IsActive != null)
                query = query.Where(n => n.IsActive == IsActive).ToList();

            totalcount = query.Count;
            var UserMobNavigationItems = new PagedList<KSModel.Models.UserMobNavigationItem>(query, PageIndex, PageSize);
            return UserMobNavigationItems;
        }

        public void InsertUserMobNavigationItem(UserMobNavigationItem UserMobNavigationItem)
        {
            if (UserMobNavigationItem == null)
                throw new ArgumentNullException("UserMobNavigationItem");

            db.UserMobNavigationItems.Add(UserMobNavigationItem);
            db.SaveChanges();
        }

        public void UpdateUserMobNavigationItem(UserMobNavigationItem UserMobNavigationItem)
        {
            if (UserMobNavigationItem == null)
                throw new ArgumentNullException("UserMobNavigationItem");

            db.Entry(UserMobNavigationItem).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteUserMobNavigationItem(int UserMobNavigationItemId = 0)
        {
            if (UserMobNavigationItemId == 0)
                throw new ArgumentNullException("UserMobNavigationItem");

            var UserMobNavigationItem = (from s in db.UserMobNavigationItems where s.MobUserNavigationItemId == UserMobNavigationItemId select s).FirstOrDefault();
            db.UserMobNavigationItems.Remove(UserMobNavigationItem);
            db.SaveChanges();
        }

        #endregion

        #endregion

    }
}