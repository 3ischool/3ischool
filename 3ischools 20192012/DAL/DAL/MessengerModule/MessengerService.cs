﻿using DAL.DAL.AddressModule;
using DAL.DAL.Common;
using DAL.DAL.GuardianModule;
using DAL.DAL.StaffModule;
using DAL.DAL.StudentModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Messenger;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace DAL.DAL.MessengerModule
{

    public partial class MessengerService : IMessengerService
    {

        public int count = 0;
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        //private readonly string SqlDataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;
        private readonly IUserService _IUserService;
        private readonly IStaffService _IStaffService;
        private readonly IStudentService _IStudentService;
        private readonly IGuardianService _IGuardianService;
        private readonly IAddressMasterService _IAddressMasterService;
        public string ArrayIds = "";
        private readonly ICustomEncryption _ICustomEncryption;



        public MessengerService(IConnectionService IConnectionService,
            IUserService IUserService,
            IStaffService IStaffService,
            IStudentService IStudentService,
            IGuardianService IGuardianService,
            IAddressMasterService IAddressMasterService,
            ICustomEncryption ICustomEncryption)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
            this._IUserService = IUserService;
            this._IStaffService = IStaffService;
            this._IStudentService = IStudentService;
            this._IGuardianService = IGuardianService;
            this._IAddressMasterService = IAddressMasterService;
            this._ICustomEncryption = ICustomEncryption;
        }

        #region Methods

        #region Messages

        public int GetLatestMessageId()
        {
            var messages = (from s in db.Messages.AsNoTracking() select s).ToList();
            var messageid = messages.OrderByDescending(m => m.MessageId).FirstOrDefault().MessageId;
            return messageid;
        }

        public KSModel.Models.Message GetMessageById(int MessageId, bool IsTrack = false)
        {
            if (MessageId == 0)
                throw new ArgumentNullException("Message");

            var message = new KSModel.Models.Message();
            if (IsTrack)
                message = (from s in db.Messages where s.MessageId == MessageId select s).FirstOrDefault();
            else
                message = (from s in db.Messages.AsNoTracking() where s.MessageId == MessageId select s).FirstOrDefault();
            return message;
        }
        //public SMSNotification GetSMSNotificationById(int MessageId, bool IsTrack = false)
        //{
        //    if (MessageId == 0)
        //        throw new ArgumentNullException("SMSNotification");

        //    var message = new SMSNotification();
        //    if (IsTrack)
        //        message = (from s in db.SMSNotifications where s.MessageId == MessageId select s).FirstOrDefault();
        //    else
        //        message = (from s in db.SMSNotifications.AsNoTracking() where s.MessageId == MessageId select s).FirstOrDefault();
        //    return message;
        //}
        public IList<KSModel.Models.Message> GetAllMessageList(ref int totalcount, int[] Messageid = null, int[] Sender_Id = null, int? SenderId = null, bool IsActive = false, bool IsDeleted = false, string MessageTitle = "",
            string MessageDescription = "", DateTime? fromdate = null, DateTime? todate = null, DateTime? MessageTime = null, int? RefId = null, int? PriorityId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Messages.ToList();

            if (Messageid != null)
                query = query.Where(e => Messageid.Contains((int)e.MessageId)).ToList();
            if (Sender_Id != null)
                query = query.Where(e => Sender_Id.Contains((int)e.SenderId)).ToList();
            if (SenderId != null && SenderId > 0)
                query = query.Where(e => e.SenderId == SenderId).ToList();
            if (IsActive)
                query = query.Where(e => e.InActive == true).ToList();
            else
                query = query.Where(e => e.InActive == false).ToList();
            if (IsDeleted)
                query = query.Where(e => e.IsDeleted == true).ToList();
            else
                query = query.Where(e => e.IsDeleted == false).ToList();
            if (!String.IsNullOrEmpty(MessageTitle))
                query = query.Where(e => MessageTitle.ToLower().Contains(e.MessageTitle.ToLower())).ToList();

            if (!String.IsNullOrEmpty(MessageDescription))
                query = query.Where(e => MessageDescription.ToLower().Contains(e.MessageDescription.ToLower())).ToList();

            if (MessageTime.HasValue)
                query = query.Where(e => e.MessageTime == MessageTime).ToList();
            if (fromdate.HasValue)
            {
                query = query.Where(e => e.MessageTime >= fromdate).ToList();
            }
            if (todate.HasValue)
            {
                query = query.Where(e => e.MessageTime <= todate).ToList();
            }
            if (RefId != null && RefId > 0)
                query = query.Where(e => e.RefId == RefId).ToList();

            if (PriorityId != null && PriorityId > 0)
                query = query.Where(e => e.PriorityId == PriorityId).ToList();
            query = query.OrderByDescending(M => M.MessageId).ToList();
            totalcount = query.Count;
            var MessageList = new PagedList<KSModel.Models.Message>(query, PageIndex, PageSize);
            return MessageList;
        }

        public IList<KSModel.Models.Message> GetAllMessageListInbox(ref int totalcount, int[] Messageid = null, int[] Sender_Id = null, int? SenderId = null, bool IsActive = false, bool IsDeleted = false, string MessageTitle = "",
           string MessageDescription = "", DateTime? fromdate = null, DateTime? todate = null, DateTime? MessageTime = null, int? RefId = null, int? PriorityId = null,
           int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Messages.ToList();

            if (Messageid != null)
                query = query.Where(e => Messageid.Contains((int)e.MessageId)).ToList();
            if (Sender_Id != null)
                query = query.Where(e => Sender_Id.Contains((int)e.SenderId)).ToList();
            if (SenderId != null && SenderId > 0)
                query = query.Where(e => e.SenderId == SenderId).ToList();


            if (!String.IsNullOrEmpty(MessageTitle))
                query = query.Where(e => e.MessageTitle.ToLower().Contains(MessageTitle.ToLower().Trim())).ToList();

            if (!String.IsNullOrEmpty(MessageDescription))
                query = query.Where(e => e.MessageDescription.ToLower().Contains(MessageDescription.ToLower().Trim())).ToList();

            if (MessageTime.HasValue)
                query = query.Where(e => e.MessageTime == MessageTime).ToList();
            if (fromdate.HasValue)
            {
                query = query.Where(e => e.MessageTime.Value.Date >= fromdate.Value.Date).ToList();
            }
            if (todate.HasValue)
            {
                query = query.Where(e => e.MessageTime.Value.Date <= todate.Value.Date).ToList();
            }
            if (RefId != null && RefId > 0)
                query = query.Where(e => e.RefId == RefId).ToList();

            if (PriorityId != null && PriorityId > 0)
                query = query.Where(e => e.PriorityId == PriorityId).ToList();
            query = query.OrderByDescending(M => M.MessageId).ToList();
            totalcount = query.Count;
            var MessageList = new PagedList<KSModel.Models.Message>(query, PageIndex, PageSize);
            return MessageList;
        }

        public DataTable GetAllUnreadMsgsCount(ref int totalcount, int? Userid = null)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_getGetAllUnreadMsges", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@UserId", Userid);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            return dt;
        }
        
        public IList<inboxoutboxmessage> GetInboxOutboxMessages(ref int count, int? senderid = null, bool Inactive = false, bool Isdeleted = false, string recipientname = "", string messagedes = "", string messagetitle = "", DateTime? fromdate = null, DateTime? todate = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            DataTable dt = new DataTable();

            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_outboxmessages", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@userid", senderid);
                    sqlComm.Parameters.AddWithValue("@recipient", recipientname);
                    sqlComm.Parameters.AddWithValue("@messagedes", messagedes);
                    sqlComm.Parameters.AddWithValue("@title", messagedes);
                    if (fromdate.HasValue)
                        sqlComm.Parameters.AddWithValue("@fromdate", fromdate);
                    if (todate.HasValue)
                        sqlComm.Parameters.AddWithValue("@todate", todate);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            IList<inboxoutboxmessage> GetAllMessageList = new List<inboxoutboxmessage>();
            inboxoutboxmessage messagelist = new inboxoutboxmessage();
            string Username = "";
            string currentusername = "";
            IList<string> outboxuserstring = new List<string>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                currentusername = "";
                Username = "";
                outboxuserstring = new List<string>();

                messagelist = new inboxoutboxmessage();
                //messagelist.messageid = Convert.ToInt32(dt.Rows[i][""]);
                messagelist.MessageDescription = dt.Rows[i]["MessageDescription"].ToString();
                messagelist.MessageTitle = dt.Rows[i]["MessageTitle"].ToString();
                messagelist.messagetime = Convert.ToDateTime(dt.Rows[i]["MessageTime"]);
                if (dt.Rows[i]["IsRead"] == null || dt.Rows[i]["IsRead"].ToString()=="")
                    messagelist.IsRead = false;
                else
                    messagelist.IsRead = Convert.ToBoolean(dt.Rows[i]["IsRead"]);

                if (dt.Rows[i]["RecipientInfo"].ToString() != "Selected" && dt.Rows[i]["RecipientInfo"].ToString() != "Single")
                    messagelist.currentusername = dt.Rows[i]["RecipientInfo"].ToString();
                else
                {
                    currentusername = dt.Rows[i]["UserDet"].ToString().TrimEnd(',');
                    // split user names and get actual name
                    var userarray = currentusername.Split(',');
                    foreach (var item in userarray)
                    {
                        var message_receiver = _IUserService.GetUserById(Convert.ToInt32(item));
                        //messages sent by user
                        var receiveruser_contacttype = message_receiver.ContactType.ContactType1;
                        switch (receiveruser_contacttype)
                        {
                            case "Student":
                                var studentdet = _IStudentService.GetStudentById((int)message_receiver.UserContactId);
                                Username = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : message_receiver.UserName;
                                break;
                            case "Teacher":
                                var staffdet = _IStaffService.GetStaffById((int)message_receiver.UserContactId);
                                Username = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : message_receiver.UserName;
                                break;
                            case "Non-Teaching":
                                var nonstaffdet = _IStaffService.GetStaffById((int)message_receiver.UserContactId);
                                Username = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : message_receiver.UserName;
                                break;
                            case "Guardian":
                                var guardiandet = _IGuardianService.GetGuardianById((int)message_receiver.UserContactId);
                                Username = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : message_receiver.UserName;
                                break;
                            case "Admin":
                                Username = message_receiver.UserName;
                                break;
                        }
                        outboxuserstring.Add(Username);
                    }

                    messagelist.currentusername = String.Join(",", outboxuserstring);
                }

                messagelist.messageid = Convert.ToInt32(dt.Rows[i]["MessageId"]);
                messagelist.Inactive = Convert.ToBoolean((dt.Rows[i]["InActive"]));
                messagelist.Isdeleted = Convert.ToBoolean((dt.Rows[i]["IsDeleted"]));
                var meesagesenderid = GetMessageById(messagelist.messageid).SenderId;

                var message_sender = _IUserService.GetUserById(meesagesenderid);
                //messages sent by user
                var user_contacttype = message_sender.ContactType.ContactType1;
                Username = "";
                switch (user_contacttype)
                {
                    case "Student":
                        var studentdet = _IStudentService.GetStudentById((int)message_sender.UserContactId);
                        Username = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : message_sender.UserName;
                        break;
                    case "Teacher":
                        var staffdet = _IStaffService.GetStaffById((int)message_sender.UserContactId);
                        Username = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : message_sender.UserName;
                        break;
                    case "Non-Teaching":
                        var nonstaffdet = _IStaffService.GetStaffById((int)message_sender.UserContactId);
                        Username = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : message_sender.UserName;
                        break;
                    case "Guardian":
                        var guardiandet = _IGuardianService.GetGuardianById((int)message_sender.UserContactId);
                        Username = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : message_sender.UserName;
                        break;
                    case "Admin":
                        Username = message_sender.UserName;
                        break;
                }
                messagelist.username = Username;

                GetAllMessageList.Add(messagelist);
            }
            GetAllMessageList = (from m in GetAllMessageList where m.Inactive == Inactive select m).ToList();
            GetAllMessageList = (from m in GetAllMessageList where m.Isdeleted == Isdeleted select m).ToList();

            count = GetAllMessageList.Count;

            GetAllMessageList = GetAllMessageList.OrderByDescending(M => M.messageid).ToList();
            GetAllMessageList = new PagedList<inboxoutboxmessage>(GetAllMessageList, PageIndex, PageSize);
            return GetAllMessageList;
        }

        public IList<MessagesListModel> GetInboxOutboxSMSNotifications(ref int count, int? senderid = null, bool Inactive = false, bool Isdeleted = false, string recipientname = "", string messagedes = "", string messagetitle = "", DateTime? fromdate = null, DateTime? todate = null, string RecipientStudentType = "", int ContactType=0, int PageIndex = 0, int PageSize = int.MaxValue, string users = "")
        {
            DataTable dt = new DataTable();
            var contactType = "";
            if (ContactType > 0)
            {
                 contactType = _IAddressMasterService.GetContactTypeById(ContactType).ContactType1;
               
            }
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_SMSNotificationoutboxmessages", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@userid", senderid);
                    sqlComm.Parameters.AddWithValue("@recipient", recipientname);
                    //sqlComm.Parameters.AddWithValue("@messagedes", messagedes);
                    //sqlComm.Parameters.AddWithValue("@title", messagedes);
                    if (fromdate.HasValue)
                        sqlComm.Parameters.AddWithValue("@fromdate", fromdate);
                    if (todate.HasValue)
                        sqlComm.Parameters.AddWithValue("@todate", todate);
                    sqlComm.Parameters.AddWithValue("@RecipientStudentType", RecipientStudentType);
                    sqlComm.Parameters.AddWithValue("@contactType", contactType);
                    //sqlComm.Parameters.AddWithValue("@users", users);
                    
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            IList<MessagesListModel> GetAllMessageList = new List<MessagesListModel>();
            MessagesListModel messagelist = new MessagesListModel();
            string Username = "";
            string currentusername = "";
            IList<string> outboxuserstring = new List<string>();
          var messageslist=  new PagedList<DataRow>(dt.AsEnumerable().ToList(), PageIndex, PageSize);

            foreach(var m in messageslist)
            {
                currentusername = "";
                Username = "";
                outboxuserstring = new List<string>();

                messagelist = new MessagesListModel();
               
                messagelist.MessageDescription = m["MessageDescription"].ToString();
               
                messagelist.KendoMessageDatetime = Convert.ToDateTime(m["MessageTime"]).ToLocalTime();
                //messagelist.RecipientType = m["RecipientType"].ToString();
                //messagelist.Contacttype = m["ContactType"].ToString();
               
                if (m["RecipientInfo"].ToString() != "Selected" && m["RecipientInfo"].ToString() != "Single")
                    messagelist.currentusername = m["RecipientInfo"].ToString();
                else
                {
                    currentusername = m["UserDet"].ToString().TrimEnd(',');
                    // split user names and get actual name
                    var userarray = currentusername.Split(',');
                    foreach (var item in userarray)
                    {
                        //var message_receiver = _IUserService.GetUserById(Convert.ToInt32(item));
                        //messages sent by user
                        var receiveruser_contacttype = m["ContactType"].ToString();
                        switch (receiveruser_contacttype)
                        {
                            case "Student":
                                var studentdet = _IStudentService.GetStudentById(Convert.ToInt32(item));
                                Username = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : "";
                                break;
                            case "Teacher":
                                var staffdet = _IStaffService.GetStaffById(Convert.ToInt32(item));
                                Username = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : "";
                                break;
                            case "Non-Teaching":
                                var nonstaffdet = _IStaffService.GetStaffById(Convert.ToInt32(item));
                                Username = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : "";
                                break;
                            case "Guardian":
                                var guardiandet = _IGuardianService.GetGuardianById(Convert.ToInt32(item));
                                Username = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : "";
                                break;
                                //case "Admin":
                                //    Username = message_receiver.UserName;
                                //    break;
                        }
                        outboxuserstring.Add(Username);
                    }

                    messagelist.currentusername = String.Join(",", outboxuserstring);
                }
                if (!string.IsNullOrEmpty(messagelist.currentusername) && messagelist.currentusername.Length > 30)
                {
                    messagelist.currentusername = messagelist.currentusername.Substring(0, 30) + "...";
                    messagelist.fullcurrentusername = messagelist.currentusername;
                }
                else
                {
                    messagelist.currentusername = messagelist.currentusername;
                    messagelist.fullcurrentusername = messagelist.currentusername;
                }

                messagelist.EncMessageId = _ICustomEncryption.base64e(m["MessageId"].ToString());
                messagelist.MessageId = Convert.ToInt32(m["MessageId"]);
                //messagelist.Inactive = Convert.ToBoolean((dt.Rows[i]["InActive"]));
                //messagelist.Isdeleted = Convert.ToBoolean((dt.Rows[i]["IsDeleted"]));
                var meesagesenderid = GetSMSNotificationById(Convert.ToInt32(m["MessageId"])).SenderId;

                var message_sender = _IUserService.GetUserById(meesagesenderid);
                //messages sent by user
                var user_contacttype = message_sender.ContactType.ContactType1;
                Username = "";
                switch (user_contacttype)
                {
                    case "Student":
                        var studentdet = _IStudentService.GetStudentById((int)message_sender.UserContactId);
                        Username = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : message_sender.UserName;
                        break;
                    case "Teacher":
                        var staffdet = _IStaffService.GetStaffById((int)message_sender.UserContactId);
                        Username = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : message_sender.UserName;
                        break;
                    case "Non-Teaching":
                        var nonstaffdet = _IStaffService.GetStaffById((int)message_sender.UserContactId);
                        Username = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : message_sender.UserName;
                        break;
                    case "Guardian":
                        var guardiandet = _IGuardianService.GetGuardianById((int)message_sender.UserContactId);
                        Username = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : message_sender.UserName;
                        break;
                    case "Admin":
                        Username = message_sender.UserName;
                        break;
                }
             //   messagelist.username = Username;
                if (!string.IsNullOrEmpty(Username) && Username.Length > 12)
                {
                    messagelist.sendernames = Username.Substring(0, 12) + "...";
                    messagelist.fullsendernames = Username;
                }
                else
                {
                    messagelist.sendernames = Username;
                    messagelist.fullsendernames = Username;
                }

                GetAllMessageList.Add(messagelist);
            }
           
            count = dt.AsEnumerable().Count();
           

            GetAllMessageList = GetAllMessageList.OrderByDescending(M => M.MessageId).ToList();
            GetAllMessageList = new PagedList<MessagesListModel>(GetAllMessageList, PageIndex, PageSize);
            return GetAllMessageList;
        }
      
        public void InsertMessage(KSModel.Models.Message Message)
        {

            if (Message == null)
                throw new ArgumentNullException("Message");

            db.Messages.Add(Message);
            db.SaveChanges();
        }

        public void UpdateMessage(KSModel.Models.Message Message)
        {
            if (Message == null)
                throw new ArgumentNullException("Message");

            db.Entry(Message).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteMessage(int MessageId = 0)
        {
            if (MessageId == 0)
                throw new ArgumentNullException("Message");

            var Message = (from s in db.Messages where s.MessageId == MessageId select s).FirstOrDefault();
            db.Messages.Remove(Message);
            db.SaveChanges();
        }

        #endregion

        #region Message Recipient

        public KSModel.Models.MessageRecipient GetMessageRecipientById(int MessageRecipientId, bool IsTrack = false)
        {
            if (MessageRecipientId == 0)
                throw new ArgumentNullException("MessageRecipient");

            var messageRecipient = new MessageRecipient();
            if (IsTrack)
                messageRecipient = (from s in db.MessageRecipients where s.MessageRecipientId == MessageRecipientId select s).FirstOrDefault();
            else
                messageRecipient = (from s in db.MessageRecipients.AsNoTracking() where s.MessageRecipientId == MessageRecipientId select s).FirstOrDefault();
            return messageRecipient;
        }

        public IList<KSModel.Models.MessageRecipient> GetAllMessageRecipientList(ref int totalcount, int? RecipientId = null, int? MessageId = null, bool? IsActive = null, bool IsDeleted = false, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MessageRecipients.ToList();

            if (RecipientId != null && RecipientId > 0)
                query = query.Where(e => e.RecipientId == RecipientId).ToList();
            if (MessageId != null && MessageId > 0)
                query = query.Where(e => e.MessageId == MessageId).ToList();
            if (IsActive != null)
            {
                if ((bool)IsActive)
                    query = query.Where(e => e.InActive == true).ToList();
                else if (!(bool)IsActive)
                    query = query.Where(e => e.InActive == false).ToList();
            }
            if (IsDeleted)
                query = query.Where(e => e.IsDeleted == true).ToList();
            else
                query = query.Where(e => e.IsDeleted == false).ToList();

            totalcount = query.Count;
            var MessageRecipientsList = new PagedList<KSModel.Models.MessageRecipient>(query, PageIndex, PageSize);
            return MessageRecipientsList;
        }
        public IList<KSModel.Models.MessageRecipient> GetAllMessageRecipientListall(ref int totalcount, int? RecipientId = null, int? MessageId = null, bool IsDeleted = false, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MessageRecipients.ToList();

            if (RecipientId != null && RecipientId > 0)
                query = query.Where(e => e.RecipientId == RecipientId).ToList();
            if (MessageId != null && MessageId > 0)
                query = query.Where(e => e.MessageId == MessageId).ToList();
            if (IsDeleted)
                query = query.Where(e => e.IsDeleted == true).ToList();
            else
                query = query.Where(e => e.IsDeleted == false).ToList();

            totalcount = query.Count;
            var MessageRecipientsList = new PagedList<KSModel.Models.MessageRecipient>(query, PageIndex, PageSize);
            return MessageRecipientsList;
        }

        public void InsertMessageRecipient(KSModel.Models.MessageRecipient MessageRecipient)
        {
            if (MessageRecipient == null)
                throw new ArgumentNullException("MessageRecipient");

            db.MessageRecipients.Add(MessageRecipient);
            db.SaveChanges();
        }

        public void UpdateMessageRecipient(KSModel.Models.MessageRecipient MessageRecipient)
        {
            if (MessageRecipient == null)
                throw new ArgumentNullException("MessageRecipient");

            db.Entry(MessageRecipient).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteMessageRecipient(int MessageRecipientId = 0)
        {
            if (MessageRecipientId == 0)
                throw new ArgumentNullException("MessageRecipient");

            var MessageRecipient = (from s in db.MessageRecipients where s.MessageRecipientId == MessageRecipientId select s).FirstOrDefault();
            db.MessageRecipients.Remove(MessageRecipient);
            db.SaveChanges();
        }

        #endregion

        #region Message Priority

        public IList<KSModel.Models.MessagePriority> GetAllMessagePriorityList(ref int totalcount, int? PriorityId = null, string Priority = "", string PriorityCol = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MessagePriorities.ToList();
            if (PriorityId != null && PriorityId > 0)
                query = query.Where(e => e.PriorityId == PriorityId).ToList();
            if (!String.IsNullOrEmpty(Priority))
                query = query.Where(e => Priority.ToLower().Contains(e.Priority.ToLower())).ToList();
            if (!String.IsNullOrEmpty(PriorityCol))
                query = query.Where(e => PriorityCol.ToLower().Contains(e.PriorityCol.ToLower())).ToList();

            totalcount = query.Count;
            var MessagePrioritiesList = new PagedList<KSModel.Models.MessagePriority>(query, PageIndex, PageSize);
            return MessagePrioritiesList;
        }

        #endregion

        #region Message Group

        public IList<KSModel.Models.MsgGroup> GetAllMesgGroupList(ref int totalcount, int? MsgGroupId = null, string MsgGroup = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MsgGroups.ToList();
            if (MsgGroupId != null && MsgGroupId > 0)
                query = query.Where(e => e.MsgGroupId == MsgGroupId).ToList();
            if (!String.IsNullOrEmpty(MsgGroup))
                query = query.Where(e => MsgGroup.ToLower().Contains(e.MsgGroup1.ToLower())).ToList();

            totalcount = query.Count;
            var MsGroupList = new PagedList<KSModel.Models.MsgGroup>(query, PageIndex, PageSize);
            return MsGroupList;
        }

        #endregion

        #region Message Permission

        public IList<KSModel.Models.MsgPermission> GetAllMsgPermissionList(ref int totalcount, int[] MsgPermissionId = null, string MsgPermission = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MsgPermissions.ToList();
            if (MsgPermissionId != null && MsgPermissionId.Count() > 0)
                query = (from n in query where MsgPermissionId.Contains(n.MsgPermissionId) select n).ToList();
            if (!String.IsNullOrEmpty(MsgPermission))
                query = query.Where(e => MsgPermission.ToLower().Contains(e.MsgPermission1.ToLower())).ToList();

            totalcount = query.Count;
            var MsgPermissionList = new PagedList<KSModel.Models.MsgPermission>(query, PageIndex, PageSize);
            return MsgPermissionList;
        }

        #endregion

        #region Message Group Permission

        public KSModel.Models.MsgGroupPermission GetMsgGroupPermissionById(int MsgGroupPermissionId, bool IsTrack = false)
        {
            if (MsgGroupPermissionId == 0)
                throw new ArgumentNullException("MsgGroupPermission");

            var MsgGroupPermission = new MsgGroupPermission();
            if (IsTrack)
                MsgGroupPermission = (from s in db.MsgGroupPermissions where s.MsgGroupPermissionId == MsgGroupPermissionId select s).FirstOrDefault();
            else
                MsgGroupPermission = (from s in db.MsgGroupPermissions.AsNoTracking() where s.MsgGroupPermissionId == MsgGroupPermissionId select s).FirstOrDefault();
            return MsgGroupPermission;
        }

       

        public void InsertMsgGroupPermission(KSModel.Models.MsgGroupPermission MsgGroupPermission)
        {
            if (MsgGroupPermission == null)
                throw new ArgumentNullException("MsgGroupPermission");

            db.MsgGroupPermissions.Add(MsgGroupPermission);
            db.SaveChanges();
        }

        public void UpdateMsgGroupPermission(KSModel.Models.MsgGroupPermission MsgGroupPermission)
        {
            if (MsgGroupPermission == null)
                throw new ArgumentNullException("MsgGroupPermission");

            db.Entry(MsgGroupPermission).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteMsgGroupPermission(int MsgGroupPermissionId = 0)
        {
            if (MsgGroupPermissionId == 0)
                throw new ArgumentNullException("MsgGroupPermission");

            var MsgGroupPermission = (from s in db.MsgGroupPermissions where s.MsgGroupPermissionId == MsgGroupPermissionId select s).FirstOrDefault();
            db.MsgGroupPermissions.Remove(MsgGroupPermission);
            db.SaveChanges();
        }

        public IList<KSModel.Models.MsgGroupPermission> GetAllMsgGroupPermissionList(ref int totalcount, int? MsgGroupPermissionId = null, int? RecipientId = null, int? MsgGroupId = null, int? MsgPermissionId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MsgGroupPermissions.ToList();
            if (MsgGroupPermissionId != null && MsgGroupPermissionId > 0)
                query = query.Where(e => e.MsgGroupPermissionId == MsgGroupPermissionId).ToList();
            if (RecipientId != null && RecipientId > 0)
                query = query.Where(e => e.RecipientId == RecipientId).ToList();
            if (MsgGroupId != null && MsgGroupId > 0)
                query = query.Where(e => e.MsgGroupId == MsgGroupId).ToList();
            if (MsgPermissionId != null && MsgPermissionId > 0)
                query = query.Where(e => e.MsgPermissionId == MsgPermissionId).ToList();

            totalcount = query.Count;
            var MsgGroupPermissionList = new PagedList<KSModel.Models.MsgGroupPermission>(query, PageIndex, PageSize);
            return MsgGroupPermissionList;
        }

        #endregion


        #region "Message History"
        public List<MessageHistory> GetMessageHistoryBySp(ref int totalcount, string From = null, string To = null,DateTime? FromDate=null,DateTime? Todate=null,string Messagetitle="",
           int PageIndex = 0, int PageSize = int.MaxValue, int ProcedureType = 1)
        {
            List<MessageHistory> msgHistoryList = new List<MessageHistory>();
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("spGetMessageHistory", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ProcedureType", ProcedureType);
                    sqlComm.Parameters.AddWithValue("@SenderId", From);
                    sqlComm.Parameters.AddWithValue("@Recipientd", To);
                    sqlComm.Parameters.AddWithValue("@MessageTitle", Messagetitle);
                    sqlComm.Parameters.AddWithValue("@FromDate", FromDate);
                    sqlComm.Parameters.AddWithValue("@ToDate", Todate);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            for (int i = 0; i < dt.Rows.Count;i++ )
            {
                MessageHistory msgHistory = new MessageHistory();
                msgHistory.MessageId = dt.Rows[i]["MessageId"].ToString();
                msgHistory.ReceiverId = dt.Rows[i]["RecipientId"].ToString();
                msgHistory.SenderId = dt.Rows[i]["SenderId"].ToString();
                msgHistory.SenderName = dt.Rows[i]["SenderName"].ToString();
                msgHistory.ReceiverName = dt.Rows[i]["ReceiverName"].ToString();
                msgHistory.Messagetitle = dt.Rows[i]["MessageTitle"].ToString();
                msgHistory.MessageTime = dt.Rows[i]["MessageTime"].ToString();
                msgHistoryList.Add(msgHistory);
            }
            totalcount = msgHistoryList.Count;
            msgHistoryList = new PagedList<MessageHistory>(msgHistoryList, PageIndex, PageSize);
            return msgHistoryList;
        }

        public MessageHistory GetMessageHistoryById(int MessageId,int SenderId,int ProcedureType =2)
        {
            MessageHistory MsgHistory = new MessageHistory();
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("spGetMessageHistory", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ProcedureType", ProcedureType);
                    sqlComm.Parameters.AddWithValue("@SenderId", SenderId);
                    sqlComm.Parameters.AddWithValue("@MessageIds", MessageId);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            if(dt.Rows.Count>0)
            {
                MsgHistory.MessageId = dt.Rows[0]["MessageId"].ToString();
                MsgHistory.SenderName = dt.Rows[0]["SenderName"].ToString();
                MsgHistory.ReceiverName = dt.Rows[0]["ReceiverName"].ToString();
                MsgHistory.MessageDescription = dt.Rows[0]["MessageDescription"].ToString();
                MsgHistory.MessageTime = dt.Rows[0]["MessageTime"].ToString();
                MsgHistory.Messagetitle = dt.Rows[0]["MessageTitle"].ToString();
                MsgHistory.MessageReceiver= dt.Rows[0]["Receiver"].ToString();
                MsgHistory.MessageSender= dt.Rows[0]["Sender"].ToString();
            }
            return MsgHistory;
        }
        public List<ReplyMessageHistory> GetReplyList(string MessageId, int SenderId = 0, int ProcedureTye = 3)
        {
            List<ReplyMessageHistory> ReplyList = new List<ReplyMessageHistory>();
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("spGetMessageHistory", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ProcedureType", ProcedureTye);
                    sqlComm.Parameters.AddWithValue("@SenderId", SenderId);
                    sqlComm.Parameters.AddWithValue("@MessageIds", MessageId);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            for (int i = 0; i < dt.Rows.Count;i++ )
            {
                ReplyMessageHistory rmsgHistory = new ReplyMessageHistory();
                rmsgHistory.MessageId = dt.Rows[i]["MessageId"].ToString();
                rmsgHistory.SenderName = dt.Rows[i]["SenderName"].ToString();
                rmsgHistory.Message = dt.Rows[i]["MessageDescription"].ToString();
                rmsgHistory.MessageTime = dt.Rows[i]["MessageTime"].ToString();
                rmsgHistory.Status = dt.Rows[i]["status"].ToString();
                ReplyList.Add(rmsgHistory);
            }
            return ReplyList; 
        }
       
        public string recurrsiveIds(int? Param)
        {  
           
            try
            {
                
                if (Param != 0 && Param != null)
                {
                    ArrayIds +="'"+ Param.ToString()+"'" + ",";
                    var ids = db.Messages.Where(m => m.RefId == Param).ToList();
                    if (ids.Count() > 0)
                    {
                        foreach (var id in ids)
                        {
                            recurrsiveIds(id.MessageId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return ArrayIds.TrimEnd(',');
        }

        #endregion

        #region SMSNotification


        public SMSNotification GetSMSNotificationById(int MessageId, bool IsTrack = false)
        {
            if (MessageId == 0)
                throw new ArgumentNullException("Message");

            var message = new SMSNotification();
            if (IsTrack)
                message = (from s in db.SMSNotifications where s.MessageId == MessageId select s).FirstOrDefault();
            else
                message = (from s in db.SMSNotifications.AsNoTracking() where s.MessageId == MessageId select s).FirstOrDefault();
            return message;
        }
        public void InsertSMSNotification(SMSNotification Message)
        {

            if (Message == null)
                throw new ArgumentNullException("Message");

            db.SMSNotifications.Add(Message);
            db.SaveChanges();
        }

        public void UpdateSMSNotification(SMSNotification Message)
        {
            if (Message == null)
                throw new ArgumentNullException("Message");

            db.Entry(Message).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSMSNotification(int MessageId = 0)
        {
            if (MessageId == 0)
                throw new ArgumentNullException("Message");

            var Message = (from s in db.SMSNotifications where s.MessageId == MessageId select s).FirstOrDefault();
            db.SMSNotifications.Remove(Message);
            db.SaveChanges();
        }

        #endregion

        #region SMSNotification Recipient

        public KSModel.Models.SMSNotificationRecipient GetSMSNotificationRecipientById(int MessageRecipientId, bool IsTrack = false)
        {
            if (MessageRecipientId == 0)
                throw new ArgumentNullException("MessageRecipient");

            var messageRecipient = new SMSNotificationRecipient();
            if (IsTrack)
                messageRecipient = (from s in db.SMSNotificationRecipients where s.MessageRecipientId == MessageRecipientId select s).FirstOrDefault();
            else
                messageRecipient = (from s in db.SMSNotificationRecipients.AsNoTracking() where s.MessageRecipientId == MessageRecipientId select s).FirstOrDefault();
            return messageRecipient;
        }
        public IList<KSModel.Models.SMSNotificationRecipient> GetAllSMSNotificationRecipientListall(ref int totalcount, int? RecipientId = null, int? MessageId = null, bool IsDeleted = false, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SMSNotificationRecipients.ToList();

            if (RecipientId != null && RecipientId > 0)
                query = query.Where(e => e.RecipientId == RecipientId).ToList();
            if (MessageId != null && MessageId > 0)
                query = query.Where(e => e.MessageId == MessageId).ToList();
            //if (IsDeleted)
            //    query = query.Where(e => e.IsDeleted == true).ToList();
            //else
            //    query = query.Where(e => e.IsDeleted == false).ToList();

            totalcount = query.Count;
            var MessageRecipientsList = new PagedList<KSModel.Models.SMSNotificationRecipient>(query, PageIndex, PageSize);
            return MessageRecipientsList;
        }


        public void InsertSMSNotificationRecipient(KSModel.Models.SMSNotificationRecipient MessageRecipient)
        {
            if (MessageRecipient == null)
                throw new ArgumentNullException("MessageRecipient");

            db.SMSNotificationRecipients.Add(MessageRecipient);
            db.SaveChanges();
        }

        public void UpdateSMSNotificationRecipient(KSModel.Models.SMSNotificationRecipient MessageRecipient)
        {
            if (MessageRecipient == null)
                throw new ArgumentNullException("MessageRecipient");

            db.Entry(MessageRecipient).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSMSNotificationRecipient(int MessageRecipientId = 0)
        {
            if (MessageRecipientId == 0)
                throw new ArgumentNullException("MessageRecipient");

            var MessageRecipient = (from s in db.SMSNotificationRecipients where s.MessageRecipientId == MessageRecipientId select s).FirstOrDefault();
            db.SMSNotificationRecipients.Remove(MessageRecipient);
            db.SaveChanges();
        }

        #endregion

        #region SMSAPIUniqueKey

        public KSModel.Models.SMSAPIUniqueKey GetSMSAPIUniqueKeyById(int SMSAPIUniqueKeyId, bool IsTrack = false)
        {
            if (SMSAPIUniqueKeyId == 0)
                throw new ArgumentNullException("SMSAPIUniqueKey");

            var SMSAPIUniqueKey = new SMSAPIUniqueKey();
            if (IsTrack)
                SMSAPIUniqueKey = (from s in db.SMSAPIUniqueKeys where s.SMSAPIUniqueKeyId == SMSAPIUniqueKeyId select s).FirstOrDefault();
            else
                SMSAPIUniqueKey = (from s in db.SMSAPIUniqueKeys.AsNoTracking() where s.SMSAPIUniqueKeyId == SMSAPIUniqueKeyId select s).FirstOrDefault();
            return SMSAPIUniqueKey;
        }

        public IList<KSModel.Models.SMSAPIUniqueKey> GetAllSMSAPIUniqueKeyList(ref int totalcount, string SMSAPIUniqueKey = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SMSAPIUniqueKeys.ToList();

            if (!string.IsNullOrEmpty(SMSAPIUniqueKey))
                query = query.Where(m => m.SMSAPIUniqueKey1 == SMSAPIUniqueKey).ToList();

            totalcount = query.Count;
            var SMSAPIUniqueKeyList = new PagedList<KSModel.Models.SMSAPIUniqueKey>(query, PageIndex, PageSize);
            return SMSAPIUniqueKeyList;
        }
        public IList<KSModel.Models.SMSAPIUniqueKey> GetAllSMSAPIUniqueKeyListall(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SMSAPIUniqueKeys.ToList();

            totalcount = query.Count;
            var SMSAPIUniqueKeyList = new PagedList<KSModel.Models.SMSAPIUniqueKey>(query, PageIndex, PageSize);
            return SMSAPIUniqueKeyList;
        }

        public void InsertSMSAPIUniqueKey(KSModel.Models.SMSAPIUniqueKey SMSAPIUniqueKey)
        {
            if (SMSAPIUniqueKey == null)
                throw new ArgumentNullException("SMSAPIUniqueKey");

            db.SMSAPIUniqueKeys.Add(SMSAPIUniqueKey);
            db.SaveChanges();
        }

        public void UpdateSMSAPIUniqueKey(KSModel.Models.SMSAPIUniqueKey SMSAPIUniqueKey)
        {
            if (SMSAPIUniqueKey == null)
                throw new ArgumentNullException("SMSAPIUniqueKey");

            db.Entry(SMSAPIUniqueKey).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSMSAPIUniqueKey(int SMSAPIUniqueKeyId = 0)
        {
            if (SMSAPIUniqueKeyId == 0)
                throw new ArgumentNullException("SMSAPIUniqueKey");

            var SMSAPIUniqueKey = (from s in db.SMSAPIUniqueKeys where s.SMSAPIUniqueKeyId == SMSAPIUniqueKeyId select s).FirstOrDefault();
            db.SMSAPIUniqueKeys.Remove(SMSAPIUniqueKey);
            db.SaveChanges();
        }

        #endregion

        #region SMSAPIUniqueKeyDetail

        public KSModel.Models.SMSAPIUniqueKeyDetail GetSMSAPIUniqueKeyDetailById(int SMSAPIUniqueKeyDetailId, bool IsTrack = false)
        {
            if (SMSAPIUniqueKeyDetailId == 0)
                throw new ArgumentNullException("SMSAPIUniqueKeyDetail");

            var SMSAPIUniqueKeyDetail = new SMSAPIUniqueKeyDetail();
            if (IsTrack)
                SMSAPIUniqueKeyDetail = (from s in db.SMSAPIUniqueKeyDetails where s.SMSAPIUniqueKeyDetailId == SMSAPIUniqueKeyDetailId select s).FirstOrDefault();
            else
                SMSAPIUniqueKeyDetail = (from s in db.SMSAPIUniqueKeyDetails.AsNoTracking() where s.SMSAPIUniqueKeyDetailId == SMSAPIUniqueKeyDetailId select s).FirstOrDefault();
            return SMSAPIUniqueKeyDetail;
        }

        public IList<KSModel.Models.SMSAPIUniqueKeyDetail> GetAllSMSAPIUniqueKeyDetailList(ref int totalcount, int SMSAPIUniqueKeyId=0, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SMSAPIUniqueKeyDetails.ToList();

            if (SMSAPIUniqueKeyId > 0)
                query = query.Where(m => m.SMSAPIUniqueKeyId == SMSAPIUniqueKeyId).ToList();

            totalcount = query.Count;
            var SMSAPIUniqueKeyDetailList = new PagedList<KSModel.Models.SMSAPIUniqueKeyDetail>(query, PageIndex, PageSize);
            return SMSAPIUniqueKeyDetailList;
        }
        public IList<KSModel.Models.SMSAPIUniqueKeyDetail> GetAllSMSAPIUniqueKeyDetailListall(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SMSAPIUniqueKeyDetails.ToList();

            totalcount = query.Count;
            var SMSAPIUniqueKeyDetailList = new PagedList<KSModel.Models.SMSAPIUniqueKeyDetail>(query, PageIndex, PageSize);
            return SMSAPIUniqueKeyDetailList;
        }

        public void InsertSMSAPIUniqueKeyDetail(KSModel.Models.SMSAPIUniqueKeyDetail SMSAPIUniqueKeyDetail)
        {
            if (SMSAPIUniqueKeyDetail == null)
                throw new ArgumentNullException("SMSAPIUniqueKeyDetail");

            db.SMSAPIUniqueKeyDetails.Add(SMSAPIUniqueKeyDetail);
            db.SaveChanges();
        }

        public void UpdateSMSAPIUniqueKeyDetail(KSModel.Models.SMSAPIUniqueKeyDetail SMSAPIUniqueKeyDetail)
        {
            if (SMSAPIUniqueKeyDetail == null)
                throw new ArgumentNullException("SMSAPIUniqueKeyDetail");

            db.Entry(SMSAPIUniqueKeyDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSMSAPIUniqueKeyDetail(int SMSAPIUniqueKeyDetailId = 0)
        {
            if (SMSAPIUniqueKeyDetailId == 0)
                throw new ArgumentNullException("SMSAPIUniqueKeyDetail");

            var SMSAPIUniqueKeyDetail = (from s in db.SMSAPIUniqueKeyDetails where s.SMSAPIUniqueKeyDetailId == SMSAPIUniqueKeyDetailId select s).FirstOrDefault();
            db.SMSAPIUniqueKeyDetails.Remove(SMSAPIUniqueKeyDetail);
            db.SaveChanges();
        }

        #endregion

        #region MessageQueue

        public KSModel.Models.MessageQueue GetMessageQueueById(int MessageQueueId, bool IsTrack = false)
        {
            if (MessageQueueId == 0)
                throw new ArgumentNullException("MessageQueue");

            var MessageQueue = new MessageQueue();
            if (IsTrack)
                MessageQueue = (from s in db.MessageQueues where s.MessageQueueId == MessageQueueId select s).FirstOrDefault();
            else
                MessageQueue = (from s in db.MessageQueues.AsNoTracking() where s.MessageQueueId == MessageQueueId select s).FirstOrDefault();
            return MessageQueue;
        }

        public IList<KSModel.Models.MessageQueue> GetAllMessageQueueList(ref int totalcount, int? StudentId = 0, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MessageQueues.ToList();

            if (StudentId!=null && StudentId > 0)
                query = query.Where(f => f.StudentId == StudentId).ToList();
            
            totalcount = query.Count;
            var MessageQueueList = new PagedList<KSModel.Models.MessageQueue>(query, PageIndex, PageSize);
            return MessageQueueList;
        }

        public void InsertMessageQueue(KSModel.Models.MessageQueue MessageQueue)
        {
            if (MessageQueue == null)
                throw new ArgumentNullException("MessageQueue");

            db.MessageQueues.Add(MessageQueue);
            db.SaveChanges();
        }

        public void UpdateMessageQueue(KSModel.Models.MessageQueue MessageQueue)
        {
            if (MessageQueue == null)
                throw new ArgumentNullException("MessageQueue");

            db.Entry(MessageQueue).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteMessageQueue(int MessageQueueId = 0)
        {
            if (MessageQueueId == 0)
                throw new ArgumentNullException("MessageQueue");

            var MessageQueue = (from s in db.MessageQueues where s.MessageQueueId == MessageQueueId select s).FirstOrDefault();
            db.MessageQueues.Remove(MessageQueue);
            db.SaveChanges();
        }

        #endregion
        #endregion

        #region trigger
        #region TriggerType
        public KSModel.Models.TriggerType GetTriggerTypeById(int TriggerTypeId, bool IsTrack = false)
        {
            if (TriggerTypeId == 0)
                throw new ArgumentNullException("TriggerType");

            var TriggerType = new TriggerType();
            if (IsTrack)
                TriggerType = (from s in db.TriggerTypes where s.TriggerTypeId == TriggerTypeId select s).FirstOrDefault();
            else
                TriggerType = (from s in db.TriggerTypes.AsNoTracking() where s.TriggerTypeId == TriggerTypeId select s).FirstOrDefault();
            return TriggerType;
        }

        public IList<KSModel.Models.TriggerType> GetAllTriggerTypeList(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TriggerTypes.ToList();

            totalcount = query.Count;
            var TriggerTypeList = new PagedList<KSModel.Models.TriggerType>(query, PageIndex, PageSize);
            return TriggerTypeList;
        }

        public void InsertTriggerType(KSModel.Models.TriggerType TriggerType)
        {
            if (TriggerType == null)
                throw new ArgumentNullException("TriggerType");

            db.TriggerTypes.Add(TriggerType);
            db.SaveChanges();
        }

        public void UpdateTriggerType(KSModel.Models.TriggerType TriggerType)
        {
            if (TriggerType == null)
                throw new ArgumentNullException("TriggerType");

            db.Entry(TriggerType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTriggerType(int TriggerTypeId = 0)
        {
            if (TriggerTypeId == 0)
                throw new ArgumentNullException("TriggerType");

            var TriggerType = (from s in db.TriggerTypes where s.TriggerTypeId == TriggerTypeId select s).FirstOrDefault();
            db.TriggerTypes.Remove(TriggerType);
            db.SaveChanges();
        }
        #endregion
        #region TriggerMaster
        public KSModel.Models.TriggerMaster GetTriggerMasterById(int TriggerMasterId, bool IsTrack = false)
        {
            if (TriggerMasterId == 0)
                throw new ArgumentNullException("TriggerMaster");

            var TriggerMaster = new TriggerMaster();
            if (IsTrack)
                TriggerMaster = (from s in db.TriggerMasters where s.TriggerId == TriggerMasterId select s).FirstOrDefault();
            else
                TriggerMaster = (from s in db.TriggerMasters.AsNoTracking() where s.TriggerId == TriggerMasterId select s).FirstOrDefault();
            return TriggerMaster;
        }

        public IList<KSModel.Models.TriggerMaster> GetAllTriggerMasterList(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TriggerMasters.ToList();

            totalcount = query.Count;
            var TriggerMasterList = new PagedList<KSModel.Models.TriggerMaster>(query, PageIndex, PageSize);
            return TriggerMasterList;
        }

        public void InsertTriggerMaster(KSModel.Models.TriggerMaster TriggerMaster)
        {
            if (TriggerMaster == null)
                throw new ArgumentNullException("TriggerMaster");

            db.TriggerMasters.Add(TriggerMaster);
            db.SaveChanges();
        }

        public void UpdateTriggerMaster(KSModel.Models.TriggerMaster TriggerMaster)
        {
            if (TriggerMaster == null)
                throw new ArgumentNullException("TriggerMaster");

            db.Entry(TriggerMaster).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTriggerMaster(int TriggerMasterId = 0)
        {
            if (TriggerMasterId == 0)
                throw new ArgumentNullException("TriggerMaster");

            var TriggerMaster = (from s in db.TriggerMasters where s.TriggerId == TriggerMasterId select s).FirstOrDefault();
            db.TriggerMasters.Remove(TriggerMaster);
            db.SaveChanges();
        }
        #endregion

        #region TriggerMsgTemplate
        public KSModel.Models.TriggerMsgTemplate GetTriggerMsgTemplateById(int TriggerMsgTemplateId, bool IsTrack = false)
        {
            if (TriggerMsgTemplateId == 0)
                throw new ArgumentNullException("TriggerMsgTemplate");

            var TriggerMsgTemplate = new TriggerMsgTemplate();
            if (IsTrack)
                TriggerMsgTemplate = (from s in db.TriggerMsgTemplates where s.TriggerMsgTemplateId == TriggerMsgTemplateId select s).FirstOrDefault();
            else
                TriggerMsgTemplate = (from s in db.TriggerMsgTemplates.AsNoTracking() where s.TriggerMsgTemplateId == TriggerMsgTemplateId select s).FirstOrDefault();
            return TriggerMsgTemplate;
        }

        public IList<KSModel.Models.TriggerMsgTemplate> GetAllTriggerMsgTemplateList(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TriggerMsgTemplates.ToList();

            totalcount = query.Count;
            var TriggerMsgTemplateList = new PagedList<KSModel.Models.TriggerMsgTemplate>(query, PageIndex, PageSize);
            return TriggerMsgTemplateList;
        }

        public void InsertTriggerMsgTemplate(KSModel.Models.TriggerMsgTemplate TriggerMsgTemplate)
        {
            if (TriggerMsgTemplate == null)
                throw new ArgumentNullException("TriggerMsgTemplate");

            db.TriggerMsgTemplates.Add(TriggerMsgTemplate);
            db.SaveChanges();
        }

        public void UpdateTriggerMsgTemplate(KSModel.Models.TriggerMsgTemplate TriggerMsgTemplate)
        {
            if (TriggerMsgTemplate == null)
                throw new ArgumentNullException("TriggerMsgTemplate");

            db.Entry(TriggerMsgTemplate).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTriggerMsgTemplate(int TriggerMsgTemplateId = 0)
        {
            if (TriggerMsgTemplateId == 0)
                throw new ArgumentNullException("TriggerMsgTemplate");

            var TriggerMsgTemplate = (from s in db.TriggerMsgTemplates where s.TriggerMsgTemplateId == TriggerMsgTemplateId select s).FirstOrDefault();
            db.TriggerMsgTemplates.Remove(TriggerMsgTemplate);
            db.SaveChanges();
        }
        #endregion

        #region TriggerTemplate
        public KSModel.Models.TriggerTemplate GetTriggerTemplateById(int TriggerTemplateId, bool IsTrack = false)
        {
            if (TriggerTemplateId == 0)
                throw new ArgumentNullException("TriggerTemplate");

            var TriggerTemplate = new TriggerTemplate();
            if (IsTrack)
                TriggerTemplate = (from s in db.TriggerTemplates where s.TriggerTemplateId == TriggerTemplateId select s).FirstOrDefault();
            else
                TriggerTemplate = (from s in db.TriggerTemplates.AsNoTracking() where s.TriggerTemplateId == TriggerTemplateId select s).FirstOrDefault();
            return TriggerTemplate;
        }

        public IList<KSModel.Models.TriggerTemplate> GetAllTriggerTemplateList(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TriggerTemplates.ToList();

            totalcount = query.Count;
            var TriggerTemplateList = new PagedList<KSModel.Models.TriggerTemplate>(query, PageIndex, PageSize);
            return TriggerTemplateList;
        }

        public void InsertTriggerTemplate(KSModel.Models.TriggerTemplate TriggerTemplate)
        {
            if (TriggerTemplate == null)
                throw new ArgumentNullException("TriggerTemplate");

            db.TriggerTemplates.Add(TriggerTemplate);
            db.SaveChanges();
        }

        public void UpdateTriggerTemplate(KSModel.Models.TriggerTemplate TriggerTemplate)
        {
            if (TriggerTemplate == null)
                throw new ArgumentNullException("TriggerTemplate");

            db.Entry(TriggerTemplate).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTriggerTemplate(int TriggerTemplateId = 0)
        {
            if (TriggerTemplateId == 0)
                throw new ArgumentNullException("TriggerTemplate");

            var TriggerTemplate = (from s in db.TriggerTemplates where s.TriggerTemplateId == TriggerTemplateId select s).FirstOrDefault();
            db.TriggerTemplates.Remove(TriggerTemplate);
            db.SaveChanges();
        }
        #endregion

        #region TriggerShooter
        public KSModel.Models.TriggerShooter GetTriggerShooterById(int TriggerShooterId, bool IsTrack = false)
        {
            if (TriggerShooterId == 0)
                throw new ArgumentNullException("TriggerShooter");

            var TriggerShooter = new TriggerShooter();
            if (IsTrack)
                TriggerShooter = (from s in db.TriggerShooters where s.TriggerShooterId == TriggerShooterId select s).FirstOrDefault();
            else
                TriggerShooter = (from s in db.TriggerShooters.AsNoTracking() where s.TriggerShooterId == TriggerShooterId select s).FirstOrDefault();
            return TriggerShooter;
        }

        public IList<KSModel.Models.TriggerShooter> GetAllTriggerShooterList(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TriggerShooters.ToList();

            totalcount = query.Count;
            var TriggerShooterList = new PagedList<KSModel.Models.TriggerShooter>(query, PageIndex, PageSize);
            return TriggerShooterList;
        }

        public void InsertTriggerShooter(KSModel.Models.TriggerShooter TriggerShooter)
        {
            if (TriggerShooter == null)
                throw new ArgumentNullException("TriggerShooter");

            db.TriggerShooters.Add(TriggerShooter);
            db.SaveChanges();
        }

        public void UpdateTriggerShooter(KSModel.Models.TriggerShooter TriggerShooter)
        {
            if (TriggerShooter == null)
                throw new ArgumentNullException("TriggerShooter");

            db.Entry(TriggerShooter).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTriggerShooter(int TriggerShooterId = 0)
        {
            if (TriggerShooterId == 0)
                throw new ArgumentNullException("TriggerShooter");

            var TriggerShooter = (from s in db.TriggerShooters where s.TriggerShooterId == TriggerShooterId select s).FirstOrDefault();
            db.TriggerShooters.Remove(TriggerShooter);
            db.SaveChanges();
        }
        #endregion

        #region TriggerEvent
        public KSModel.Models.TriggerEvent GetTriggerEventById(int TriggerEventId, bool IsTrack = false)
        {
            if (TriggerEventId == 0)
                throw new ArgumentNullException("TriggerEvent");

            var TriggerEvent = new TriggerEvent();
            if (IsTrack)
                TriggerEvent = (from s in db.TriggerEvents where s.TriggerEventId == TriggerEventId select s).FirstOrDefault();
            else
                TriggerEvent = (from s in db.TriggerEvents.AsNoTracking() where s.TriggerEventId == TriggerEventId select s).FirstOrDefault();
            return TriggerEvent;
        }

        public IList<KSModel.Models.TriggerEvent> GetAllTriggerEventList(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TriggerEvents.ToList();

            totalcount = query.Count;
            var TriggerEventList = new PagedList<KSModel.Models.TriggerEvent>(query, PageIndex, PageSize);
            return TriggerEventList;
        }

        public void InsertTriggerEvent(KSModel.Models.TriggerEvent TriggerEvent)
        {
            if (TriggerEvent == null)
                throw new ArgumentNullException("TriggerEvent");

            db.TriggerEvents.Add(TriggerEvent);
            db.SaveChanges();
        }

        public void UpdateTriggerEvent(KSModel.Models.TriggerEvent TriggerEvent)
        {
            if (TriggerEvent == null)
                throw new ArgumentNullException("TriggerEvent");

            db.Entry(TriggerEvent).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTriggerEvent(int TriggerEventId = 0)
        {
            if (TriggerEventId == 0)
                throw new ArgumentNullException("TriggerEvent");

            var TriggerEvent = (from s in db.TriggerEvents where s.TriggerEventId == TriggerEventId select s).FirstOrDefault();
            db.TriggerEvents.Remove(TriggerEvent);
            db.SaveChanges();
        }
        #endregion
        #endregion

        #region SMSCustomGroup

        public KSModel.Models.SMSCustomGroup GetSMSCustomGroupById(int SMSCustomGroupId, bool IsTrack = false)
        {
            if (SMSCustomGroupId == 0)
                throw new ArgumentNullException("SMSCustomGroup");

            var SMSCustomGroup = new SMSCustomGroup();
            if (IsTrack)
                SMSCustomGroup = (from s in db.SMSCustomGroups where s.SMSCustomGroupId == SMSCustomGroupId select s).FirstOrDefault();
            else
                SMSCustomGroup = (from s in db.SMSCustomGroups.AsNoTracking() where s.SMSCustomGroupId == SMSCustomGroupId select s).FirstOrDefault();
            return SMSCustomGroup;
        }

        public IList<KSModel.Models.SMSCustomGroup> GetAllSMSCustomGroupList(ref int totalcount, int? SMSCustomGroupId = 0,bool IsActive=true, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SMSCustomGroups.AsQueryable();

            if (SMSCustomGroupId != null && SMSCustomGroupId > 0)
                query = query.Where(f => f.SMSCustomGroupId == SMSCustomGroupId);
            if(IsActive==true)
                query = query.Where(f => f.IsActive==true);

            totalcount = query.Count();
            var SMSCustomGroupList = new PagedList<KSModel.Models.SMSCustomGroup>(query.ToList(), PageIndex, PageSize);
            return SMSCustomGroupList;
        }

        public void InsertSMSCustomGroup(KSModel.Models.SMSCustomGroup SMSCustomGroup)
        {
            if (SMSCustomGroup == null)
                throw new ArgumentNullException("SMSCustomGroup");

            db.SMSCustomGroups.Add(SMSCustomGroup);
            db.SaveChanges();
        }

        public void UpdateSMSCustomGroup(KSModel.Models.SMSCustomGroup SMSCustomGroup)
        {
            if (SMSCustomGroup == null)
                throw new ArgumentNullException("SMSCustomGroup");

            db.Entry(SMSCustomGroup).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSMSCustomGroup(int SMSCustomGroupId = 0)
        {
            if (SMSCustomGroupId == 0)
                throw new ArgumentNullException("SMSCustomGroup");

            var SMSCustomGroup = (from s in db.SMSCustomGroups where s.SMSCustomGroupId == SMSCustomGroupId select s).FirstOrDefault();
            db.SMSCustomGroups.Remove(SMSCustomGroup);
            db.SaveChanges();
        }

        #endregion

        #region SMSCustomGroupRecipient

        public KSModel.Models.SMSCustomGroupRecipient GetSMSCustomGroupRecipientById(int SMSCustomGroupRecipientId, bool IsTrack = false)
        {
            if (SMSCustomGroupRecipientId == 0)
                throw new ArgumentNullException("SMSCustomGroupRecipient");

            var SMSCustomGroupRecipient = new SMSCustomGroupRecipient();
            if (IsTrack)
                SMSCustomGroupRecipient = (from s in db.SMSCustomGroupRecipients where s.SMSCustomGroupRecipientId == SMSCustomGroupRecipientId select s).FirstOrDefault();
            else
                SMSCustomGroupRecipient = (from s in db.SMSCustomGroupRecipients.AsNoTracking() where s.SMSCustomGroupRecipientId == SMSCustomGroupRecipientId select s).FirstOrDefault();
            return SMSCustomGroupRecipient;
        }

        public IList<KSModel.Models.SMSCustomGroupRecipient> GetAllSMSCustomGroupRecipientList(ref int totalcount, int? SMSCustomGroupRecipientId = 0,int? SMSCustomGroupId = 0, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SMSCustomGroupRecipients.AsEnumerable();

            if (SMSCustomGroupRecipientId != null && SMSCustomGroupRecipientId > 0)
                query = query.Where(f => f.SMSCustomGroupRecipientId == SMSCustomGroupRecipientId).ToList();
            if (SMSCustomGroupId != null && SMSCustomGroupId > 0)
                query = query.Where(f => f.SMSCustomGroupId == SMSCustomGroupId).ToList();

          
            totalcount = query.Count();
            var SMSCustomGroupRecipientList = new PagedList<KSModel.Models.SMSCustomGroupRecipient>(query.ToList(), PageIndex, PageSize);
            return SMSCustomGroupRecipientList;
        }

        public void InsertSMSCustomGroupRecipient(KSModel.Models.SMSCustomGroupRecipient SMSCustomGroupRecipient)
        {
            if (SMSCustomGroupRecipient == null)
                throw new ArgumentNullException("SMSCustomGroupRecipient");

            db.SMSCustomGroupRecipients.Add(SMSCustomGroupRecipient);
            db.SaveChanges();
        }

        public void UpdateSMSCustomGroupRecipient(KSModel.Models.SMSCustomGroupRecipient SMSCustomGroupRecipient)
        {
            if (SMSCustomGroupRecipient == null)
                throw new ArgumentNullException("SMSCustomGroupRecipient");

            db.Entry(SMSCustomGroupRecipient).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSMSCustomGroupRecipient(int SMSCustomGroupRecipientId = 0)
        {
            if (SMSCustomGroupRecipientId == 0)
                throw new ArgumentNullException("SMSCustomGroupRecipient");

            var SMSCustomGroupRecipient = (from s in db.SMSCustomGroupRecipients where s.SMSCustomGroupRecipientId == SMSCustomGroupRecipientId select s).FirstOrDefault();
            db.SMSCustomGroupRecipients.Remove(SMSCustomGroupRecipient);
            db.SaveChanges();
        }

        #endregion
    }
    public class inboxoutboxmessage
    {
        public int messageid { get; set; }
        public string MessageDescription { get; set; }
        public string MessageTitle { get; set; }
        public string MessageFullTitle { get; set; }
        public string username { get; set; }
        public DateTime messagetime { get; set; }
        public bool Inactive { get; set; }
        public int SenderId { get; set; }
        public bool Isdeleted { get; set; }
        public string currentusername { get; set; }
        public string sendertype { get; set; }
        public bool IsRead { get; set; }
        public string RecipientType { get; set; }
        public string Contacttype { get; set; }
        public bool CanReply { get; set; }
    }
     
}