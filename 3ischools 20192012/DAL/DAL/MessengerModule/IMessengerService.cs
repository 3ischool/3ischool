﻿using KSModel.Models;
using ViewModel.ViewModel.Messenger;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DAL.MessengerModule
{
    public partial interface IMessengerService
    {

        #region Message

        int GetLatestMessageId();
        IList<inboxoutboxmessage> GetInboxOutboxMessages(ref int count, int? senderid = null, bool Inactive = false, bool Isdeleted = false, string recipientname = "", string messagedes = "",string messagetitle="", DateTime? fromdate = null, DateTime? todate = null, int PageIndex = 0, int PageSize = int.MaxValue);
        IList<MessagesListModel> GetInboxOutboxSMSNotifications(ref int count, int? senderid = null, bool Inactive = false, bool Isdeleted = false, string recipientname = "", string messagedes = "", string messagetitle = "", DateTime? fromdate = null, DateTime? todate = null, string RecipientStudentType="", int ContactType=0, int PageIndex = 0, int PageSize = int.MaxValue ,string users="");
        KSModel.Models.Message GetMessageById(int MessageId, bool IsTrack = false);

        IList<KSModel.Models.Message> GetAllMessageList(ref int totalcount, int[] Messageid = null, int[] Sender_Id = null, int? SenderId = null,
            bool IsActive = false, bool IsDeleted = false,
            string MessageTitle = "", string MessageDescription = "", DateTime? fromdate = null, DateTime? todate = null,
            DateTime? MessageTime = null,
            int? RefId = null, int? PriorityId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        DataTable GetAllUnreadMsgsCount(ref int totalcount, int? Userid = null);

        IList<KSModel.Models.Message> GetAllMessageListInbox(ref int totalcount, int[] Messageid = null, int[] Sender_Id = null, int? SenderId = null,
         bool IsActive = false, bool IsDeleted = false,
         string MessageTitle = "", string MessageDescription = "", DateTime? fromdate = null, DateTime? todate = null,
         DateTime? MessageTime = null,
         int? RefId = null, int? PriorityId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertMessage(KSModel.Models.Message Message);

        void UpdateMessage(KSModel.Models.Message Message);

        void DeleteMessage(int MessageId = 0);

        #endregion

        #region Message Recipient

        MessageRecipient GetMessageRecipientById(int MessageRecipientId, bool IsTrack = false);

        IList<MessageRecipient> GetAllMessageRecipientList(ref int totalcount, int? RecipientId = null,
            int? MessageId = null,bool? IsActive = null, bool IsDeleted = false, int PageIndex = 0, int PageSize = int.MaxValue);
        IList<KSModel.Models.MessageRecipient> GetAllMessageRecipientListall(ref int totalcount, int? RecipientId = null,
            int? MessageId = null, bool IsDeleted = false, int PageIndex = 0, int PageSize = int.MaxValue);
       
        void InsertMessageRecipient(MessageRecipient MessageRecipient);

        void UpdateMessageRecipient(MessageRecipient MessageRecipient);

        void DeleteMessageRecipient(int MessageRecipientId = 0);

        #endregion

        #region Message Priority
        IList<MessagePriority> GetAllMessagePriorityList(ref int totalcount, int? PriorityId = null, string Priority = "", string PriorityCol = "", int PageIndex = 0, int PageSize = int.MaxValue);
        #endregion

        #region Message Group

       IList<KSModel.Models.MsgGroup> GetAllMesgGroupList(ref int totalcount, int? MsgGroupId = null, string MsgGroup = "", int PageIndex = 0, int PageSize = int.MaxValue);
       
        #endregion
           
        #region Message Permission

       IList<KSModel.Models.MsgPermission> GetAllMsgPermissionList(ref int totalcount, int[] MsgPermissionId = null, string MsgPermission = "", int PageIndex = 0, int PageSize = int.MaxValue);

        #endregion

        #region Message Group Permission
        KSModel.Models.MsgGroupPermission GetMsgGroupPermissionById(int MsgGroupPermissionId, bool IsTrack = false);

        void InsertMsgGroupPermission(KSModel.Models.MsgGroupPermission MsgGroupPermission);

        void UpdateMsgGroupPermission(KSModel.Models.MsgGroupPermission MsgGroupPermission);

        void DeleteMsgGroupPermission(int MsgGroupPermissionId = 0);
        IList<KSModel.Models.MsgGroupPermission> GetAllMsgGroupPermissionList(ref int totalcount, int? MsgGroupPermissionId = null, int? RecipientId = null, int? MsgGroupId = null, int? MsgPermissionId = null, int PageIndex = 0, int PageSize = int.MaxValue);
       
        #endregion

        #region"Message History"
        List<MessageHistory> GetMessageHistoryBySp(ref int totalcount, string From = null, string To = null, DateTime? FromDate = null, DateTime? Todate = null,string Messagetitle="", 
           int PageIndex = 0, int PageSize = int.MaxValue, int ProcedureType=1);

        MessageHistory GetMessageHistoryById(int MessageId, int SenderId, int ProcedureType = 2);

        List<ReplyMessageHistory> GetReplyList(string MessageId, int SenderId = 0, int ProcedureTye = 3);
        string recurrsiveIds(int? Param);
        #endregion

        #region SMSNotifications

        SMSNotification GetSMSNotificationById(int MessageId, bool IsTrack = false);

        void InsertSMSNotification(SMSNotification Message);

        void UpdateSMSNotification(SMSNotification Message);

        void DeleteSMSNotification(int MessageId = 0);

        #endregion

        #region SMSNotification Recipient

        SMSNotificationRecipient GetSMSNotificationRecipientById(int MessageRecipientId, bool IsTrack = false);

        void InsertSMSNotificationRecipient(SMSNotificationRecipient MessageRecipient);

        void UpdateSMSNotificationRecipient(SMSNotificationRecipient MessageRecipient);

        void DeleteSMSNotificationRecipient(int MessageRecipientId = 0);
        //SMSNotification GetSMSNotificationById(int MessageId, bool IsTrack = false);
        IList<KSModel.Models.SMSNotificationRecipient> GetAllSMSNotificationRecipientListall(ref int totalcount, int? RecipientId = null,
            int? MessageId = null, bool IsDeleted = false, int PageIndex = 0, int PageSize = int.MaxValue);
        #endregion

        #region SMSAPIUniqueKey

        SMSAPIUniqueKey GetSMSAPIUniqueKeyById(int SMSAPIUniqueKeyId, bool IsTrack = false);

        IList<SMSAPIUniqueKey> GetAllSMSAPIUniqueKeyList(ref int totalcount, string SMSAPIUniqueKey=null, int PageIndex = 0, int PageSize = int.MaxValue);
        IList<KSModel.Models.SMSAPIUniqueKey> GetAllSMSAPIUniqueKeyListall(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertSMSAPIUniqueKey(SMSAPIUniqueKey SMSAPIUniqueKey);

        void UpdateSMSAPIUniqueKey(SMSAPIUniqueKey SMSAPIUniqueKey);

        void DeleteSMSAPIUniqueKey(int SMSAPIUniqueKeyId = 0);

        #endregion
        #region SMSAPIUniqueKeyDetail

        SMSAPIUniqueKeyDetail GetSMSAPIUniqueKeyDetailById(int SMSAPIUniqueKeyDetailId, bool IsTrack = false);

        IList<SMSAPIUniqueKeyDetail> GetAllSMSAPIUniqueKeyDetailList(ref int totalcount, int SMSAPIUniqueKeyId=0, int PageIndex = 0, int PageSize = int.MaxValue);
        IList<KSModel.Models.SMSAPIUniqueKeyDetail> GetAllSMSAPIUniqueKeyDetailListall(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertSMSAPIUniqueKeyDetail(SMSAPIUniqueKeyDetail SMSAPIUniqueKeyDetail);

        void UpdateSMSAPIUniqueKeyDetail(SMSAPIUniqueKeyDetail SMSAPIUniqueKeyDetail);

        void DeleteSMSAPIUniqueKeyDetail(int SMSAPIUniqueKeyDetailId = 0);

        #endregion

        #region MessageQueue

        KSModel.Models.MessageQueue GetMessageQueueById(int MessageQueueId, bool IsTrack = false);

        IList<KSModel.Models.MessageQueue> GetAllMessageQueueList(ref int totalcount,int? StudentId=0, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertMessageQueue(KSModel.Models.MessageQueue MessageQueue);

        void UpdateMessageQueue(KSModel.Models.MessageQueue MessageQueue);

        void DeleteMessageQueue(int MessageQueueId = 0);
        #endregion

        #region trigger

        #region TriggerType

        TriggerType GetTriggerTypeById(int TriggerTypeId, bool IsTrack = false);

        IList<TriggerType> GetAllTriggerTypeList(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTriggerType(TriggerType TriggerType);

        void UpdateTriggerType(TriggerType TriggerType);

        void DeleteTriggerType(int TriggerTypeId = 0);

        #endregion

        #region TriggerMaster

        TriggerMaster GetTriggerMasterById(int TriggerMasterId, bool IsTrack = false);

        IList<TriggerMaster> GetAllTriggerMasterList(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTriggerMaster(TriggerMaster TriggerMaster);

        void UpdateTriggerMaster(TriggerMaster TriggerMaster);

        void DeleteTriggerMaster(int TriggerMasterId = 0);

        #endregion

        #region TriggerMsgTemplate

        TriggerMsgTemplate GetTriggerMsgTemplateById(int TriggerMsgTemplateId, bool IsTrack = false);

        IList<TriggerMsgTemplate> GetAllTriggerMsgTemplateList(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTriggerMsgTemplate(TriggerMsgTemplate TriggerMsgTemplate);

        void UpdateTriggerMsgTemplate(TriggerMsgTemplate TriggerMsgTemplate);

        void DeleteTriggerMsgTemplate(int TriggerMsgTemplateId = 0);

        #endregion

        #region TriggerTemplate

        TriggerTemplate GetTriggerTemplateById(int TriggerTemplateId, bool IsTrack = false);

        IList<TriggerTemplate> GetAllTriggerTemplateList(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTriggerTemplate(TriggerTemplate TriggerTemplate);

        void UpdateTriggerTemplate(TriggerTemplate TriggerTemplate);

        void DeleteTriggerTemplate(int TriggerTemplateId = 0);

        #endregion

        #region TriggerShooter

        TriggerShooter GetTriggerShooterById(int TriggerShooterId, bool IsTrack = false);

        IList<TriggerShooter> GetAllTriggerShooterList(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTriggerShooter(TriggerShooter TriggerShooter);

        void UpdateTriggerShooter(TriggerShooter TriggerShooter);

        void DeleteTriggerShooter(int TriggerShooterId = 0);

        #endregion

        #region TriggerEvent

        TriggerEvent GetTriggerEventById(int TriggerEventId, bool IsTrack = false);

        IList<TriggerEvent> GetAllTriggerEventList(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTriggerEvent(TriggerEvent TriggerEvent);

        void UpdateTriggerEvent(TriggerEvent TriggerEvent);

        void DeleteTriggerEvent(int TriggerEventId = 0);

        #endregion

        #endregion

        #region SMSCustomGroup

        KSModel.Models.SMSCustomGroup GetSMSCustomGroupById(int SMSCustomGroupId, bool IsTrack = false);
        IList<KSModel.Models.SMSCustomGroup> GetAllSMSCustomGroupList(ref int totalcount, int? SMSCustomGroupId = 0,bool IsActive=true, int PageIndex = 0, int PageSize = int.MaxValue);
        void InsertSMSCustomGroup(KSModel.Models.SMSCustomGroup SMSCustomGroup);
        void UpdateSMSCustomGroup(KSModel.Models.SMSCustomGroup SMSCustomGroup);
        void DeleteSMSCustomGroup(int SMSCustomGroupId = 0);

        #endregion

        #region SMSCustomGroupRecipient

        KSModel.Models.SMSCustomGroupRecipient GetSMSCustomGroupRecipientById(int SMSCustomGroupRecipientId, bool IsTrack = false);
        IList<KSModel.Models.SMSCustomGroupRecipient> GetAllSMSCustomGroupRecipientList(ref int totalcount, int? SMSCustomGroupRecipientId = 0, int? SMSCustomGroupId = 0, int PageIndex = 0, int PageSize = int.MaxValue);
        void InsertSMSCustomGroupRecipient(KSModel.Models.SMSCustomGroupRecipient SMSCustomGroupRecipient);
        void UpdateSMSCustomGroupRecipient(KSModel.Models.SMSCustomGroupRecipient SMSCustomGroupRecipient);
        void DeleteSMSCustomGroupRecipient(int SMSCustomGroupRecipientId = 0);

        #endregion
    }
}
