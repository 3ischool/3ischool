﻿using DAL.DAL.Common;
using KSModel.Models;
using ViewModel.ViewModel.PublicViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DAL.DAL.PublicViewModule
{
    public partial class PublicViewInformation : IPublicViewInformation
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        //private readonly string SqlDataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public PublicViewInformation(IConnectionService IConnectionService)
        {
            //HttpContext context = HttpContext.Current;
            this._IConnectionService = IConnectionService;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region StudentAttendance
        public List<StudentAttendanceModel> GetAllStudentAttendanceBySp(ref int totalcount, int? ClassId = null,int? SessionId=null,
                    int MaterialTypeId = 0, DateTime? SelectedDate = null,int PageIndex = 0, int PageSize = int.MaxValue, 
                    int BookId = 0)
        {
            DataTable dt = new DataTable();

            string attendanceStatus_Ids = "";
            var getAttendanceStatus = db.AttendanceStatus.Where(x => x.AttendanceAbbr.ToLower() != "p").Select(x => x.AttendanceStatusId).ToArray();
            if (getAttendanceStatus.Count() > 0)
            {
                foreach(var Id in getAttendanceStatus)
                {
                    attendanceStatus_Ids += Id + ",";
                }
                attendanceStatus_Ids = attendanceStatus_Ids.Trim(',');
            }

            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_getStudentAttendance", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ClassId", ClassId);
                    sqlComm.Parameters.AddWithValue("@Date", SelectedDate.Value.Date);
                    sqlComm.Parameters.AddWithValue("@CurSessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@AttendanceStatusId", attendanceStatus_Ids);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<StudentAttendanceModel> StudentsList = new List<StudentAttendanceModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                StudentAttendanceModel stus = new StudentAttendanceModel();
                stus.ClassId = Convert.ToString(dt.Rows[i]["ClassId"]);
                stus.StudentName = Convert.ToString(dt.Rows[i]["StudentName"]);
                stus.Class = Convert.ToString(dt.Rows[i]["Class"]);
                stus.RollNo = Convert.ToString(dt.Rows[i]["RollNo"]);
                stus.AttendanceAbbr = Convert.ToString(dt.Rows[i]["AttendanceAbbr"]);
                StudentsList.Add(stus);
            }
            totalcount = StudentsList.Count;

            StudentsList = new PagedList<StudentAttendanceModel>(StudentsList, PageIndex, PageSize);
            return StudentsList;
        }

        #endregion
    }
}