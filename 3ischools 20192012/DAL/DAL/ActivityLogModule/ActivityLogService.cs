﻿using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DAL.DAL.ActivityLogModule
{
    public partial class ActivityLogService : IActivityLogService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }


        public int count = 0;
        public int deletecount = 20;
        private readonly ILogService _Logger;
        private readonly IWebHelper _IWebHelper;
        private readonly IConnectionService _IConnectionService;

        public ActivityLogService(ILogService Logger,
            IWebHelper IWebHelper,
            IConnectionService IConnectionService)
        {
            this._Logger = Logger;
            this._IWebHelper = IWebHelper;
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region  Methods

        #region  Activity Log

        public KSModel.Models.ActivityLog GetActivityLogById(int ActivityLogId)
        {
            if (ActivityLogId == 0)
                throw new ArgumentNullException("ActivityLog");

            var activityLog = (from s in db.ActivityLogs.AsNoTracking() where s.ActivityLogId == ActivityLogId select s).FirstOrDefault();
            return activityLog;
        }

        public List<KSModel.Models.ActivityLog> GetAllActivityLogs(ref int totalcount, int? tableId = null, int? recordId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ActivityLogs.ToList();
            if (tableId != null && tableId > 0)
                query = query.Where(e => e.TableId == tableId).ToList();
            if (recordId != null && recordId > 0)
                query = query.Where(e => e.RecordId == recordId).ToList();

            totalcount = query.Count;
            var ActivityLogs = new PagedList<KSModel.Models.ActivityLog>(query, PageIndex, PageSize);
            return ActivityLogs;
        }

        public void InsertActivityLog(KSModel.Models.ActivityLog ActivityLog)
        {
            if (ActivityLog == null)
                throw new ArgumentNullException("ActivityLog");

            db.ActivityLogs.Add(ActivityLog);
            db.SaveChanges();
        }

        public void UpdateActivityLog(KSModel.Models.ActivityLog ActivityLog)
        {
            if (ActivityLog == null)
                throw new ArgumentNullException("ActivityLog");

            db.Entry(ActivityLog).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteActivityLog(int ActivityLogId = 0)
        {
            if (ActivityLogId == 0)
                throw new ArgumentNullException("ActivityLog");

            var activityLog = (from s in db.ActivityLogs where s.ActivityLogId == ActivityLogId select s).FirstOrDefault();
            db.ActivityLogs.Remove(activityLog);
            db.SaveChanges();
        }

        #endregion

        #region Activity Log Detail

        public KSModel.Models.ActivityLogDetail GetActivityLogDetailById(int ActivityLogDetailId)
        {
            if (ActivityLogDetailId == 0)
                throw new ArgumentNullException("ActivityLogDetail");

            var activityLogDetail = (from s in db.ActivityLogDetails.AsNoTracking() where s.ActivityLogId == ActivityLogDetailId select s).FirstOrDefault();
            return activityLogDetail;
        }

        public List<KSModel.Models.ActivityLogDetail> GetAllActivityLogDetails(ref int totalcount, int? userId = null, int? ActivityLogId = null,
            int? ActivityLogTypeId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ActivityLogDetails.ToList();
            if (userId != null && userId > 0)
                query = query.Where(e => e.UserId == userId).ToList();
            if (ActivityLogId != null && ActivityLogId > 0)
                query = query.Where(e => e.ActivityLogId == ActivityLogId).ToList();
            if (ActivityLogTypeId != null && ActivityLogTypeId > 0)
                query = query.Where(e => e.ActivityLogTypeId == ActivityLogTypeId).ToList();

            totalcount = query.Count;
            var ActivityLogDetails = new PagedList<KSModel.Models.ActivityLogDetail>(query, PageIndex, PageSize);
            return ActivityLogDetails;
        }

        public void InsertActivityLogDetail(KSModel.Models.ActivityLogDetail ActivityLogDetail)
        {
            if (ActivityLogDetail == null)
                throw new ArgumentNullException("ActivityLogDetail");

            db.ActivityLogDetails.Add(ActivityLogDetail);
            db.SaveChanges();
        }

        public void UpdateActivityLogDetail(KSModel.Models.ActivityLogDetail ActivityLogDetail)
        {
            if (ActivityLogDetail == null)
                throw new ArgumentNullException("ActivityLogDetail");

            db.Entry(ActivityLogDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteActivityLogDetail(int ActivityLogDetailId = 0)
        {
            if (ActivityLogDetailId == 0)
                throw new ArgumentNullException("ActivityLogDetail");

            var activityLogDetail = (from s in db.ActivityLogDetails where s.ActivityLogId == ActivityLogDetailId select s).FirstOrDefault();
            db.ActivityLogDetails.Remove(activityLogDetail);
            db.SaveChanges();
        }

        #endregion

        #region Temp Activity Log

        public KSModel.Models.TempActivityLog GetTempActivityLogById(int TempActivityLogId)
        {
            if (TempActivityLogId == 0)
                throw new ArgumentNullException("TempActivityLog");

            var TempactivityLog = (from s in db.TempActivityLogs.AsNoTracking() where s.ActivityLogId == TempActivityLogId select s).FirstOrDefault();
            return TempactivityLog;
        }

        public List<KSModel.Models.TempActivityLog> GetAllTempActivityLogs(ref int totalcount, int? tableId = null, int? recordId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TempActivityLogs.ToList();
            if (tableId != null && tableId > 0)
                query = query.Where(e => e.TableId == tableId).ToList();
            if (recordId != null && recordId > 0)
                query = query.Where(e => e.RecordId == recordId).ToList();

            totalcount = query.Count;
            var TempActivityLogs = new PagedList<KSModel.Models.TempActivityLog>(query, PageIndex, PageSize);
            return TempActivityLogs;
        }

        public void InsertTempActivityLog(KSModel.Models.TempActivityLog TempActivityLog)
        {
            if (TempActivityLog == null)
                throw new ArgumentNullException("TempActivityLog");

            db.TempActivityLogs.Add(TempActivityLog);
            db.SaveChanges();
        }

        public void InsertTempActivityLogInList(IList<TempActivityLog> TempActivityLog)
        {
            if (TempActivityLog == null|| TempActivityLog.Count==0)
                throw new ArgumentNullException("TempActivityLog");

            db.TempActivityLogs.AddRange(TempActivityLog);
            db.SaveChanges();
        }

        public void UpdateTempActivityLog(KSModel.Models.TempActivityLog TempActivityLog)
        {
            if (TempActivityLog == null)
                throw new ArgumentNullException("TempActivityLog");

            db.Entry(TempActivityLog).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTempActivityLog(int TempActivityLogId = 0)
        {
            if (TempActivityLogId == 0)
                throw new ArgumentNullException("TempActivityLog");

            var tempActivityLog = (from s in db.TempActivityLogs where s.ActivityLogId == TempActivityLogId select s).FirstOrDefault();
            db.TempActivityLogs.Remove(tempActivityLog);
            db.SaveChanges();
        }

        #endregion

        #region Temp Activity Log Detail

        public KSModel.Models.TempActivityLogDetail GetTempActivityLogDetailById(int TempActivityLogDetailId)
        {
            if (TempActivityLogDetailId == 0)
                throw new ArgumentNullException("TempActivityLogDetail");

            var TempactivityLogDetail = (from s in db.TempActivityLogDetails.AsNoTracking() where s.ActivityLogId == TempActivityLogDetailId select s).FirstOrDefault();
            return TempactivityLogDetail;
        }

        public List<KSModel.Models.TempActivityLogDetail> GetAllTempActivityLogDetails(ref int totalcount, int? userId = null, int? ActivityLogId = null, int? ActivityLogTypeId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TempActivityLogDetails.ToList();
            if (userId != null && userId > 0)
                query = query.Where(e => e.UserId == userId).ToList();
            if (ActivityLogId != null && ActivityLogId > 0)
                query = query.Where(e => e.ActivityLogId == ActivityLogId).ToList();
            if (ActivityLogTypeId != null && ActivityLogTypeId > 0)
                query = query.Where(e => e.ActivityLogTypeId == ActivityLogTypeId).ToList();

            totalcount = query.Count;
            var TempActivityLogDetails = new PagedList<KSModel.Models.TempActivityLogDetail>(query, PageIndex, PageSize);
            return TempActivityLogDetails;
        }

        public void InsertTempActivityLogDetail(KSModel.Models.TempActivityLogDetail TempActivityLogDetail)
        {
            if (TempActivityLogDetail == null)
                throw new ArgumentNullException("TempActivityLogDetail");

            db.TempActivityLogDetails.Add(TempActivityLogDetail);
            db.SaveChanges();
        }

        public void InsertTempActivityLogDetailInList(IList<TempActivityLogDetail> TempActivityLogDetail)
        {
            if (TempActivityLogDetail == null|| TempActivityLogDetail.Count==0)
                throw new ArgumentNullException("TempActivityLogDetail");

            db.TempActivityLogDetails.AddRange(TempActivityLogDetail);
            db.SaveChanges();
        }

        public void UpdateTempActivityLogDetail(KSModel.Models.TempActivityLogDetail TempActivityLogDetail)
        {
            if (TempActivityLogDetail == null)
                throw new ArgumentNullException("TempActivityLogDetail");

            db.Entry(TempActivityLogDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTempActivityLogDetail(int TempActivityLogDetailId = 0)
        {
            if (TempActivityLogDetailId == 0)
                throw new ArgumentNullException("TempActivityLogDetail");

            var tempActivityLogDetail = (from s in db.TempActivityLogDetails where s.ActivityLogId == TempActivityLogDetailId select s).FirstOrDefault();
            db.TempActivityLogDetails.Remove(tempActivityLogDetail);
            db.SaveChanges();
        }

        #endregion

        #region save actvity log

        public void SaveActivityLog(int RecordId, int tableId, int LogtypeId, SchoolUser User, string logdescription,
            string useragent)
        {
            int? nulluserid = null;
            try
            {
                TempActivityLog ActivityLog = new TempActivityLog();
                ActivityLog.RecordId = RecordId;
                ActivityLog.TableId = tableId;
                InsertTempActivityLog(ActivityLog);

                TempActivityLogDetail ActivityLogDetail = new TempActivityLogDetail();
                ActivityLogDetail.ActivityLogId = ActivityLog.ActivityLogId;
                ActivityLogDetail.ActivityLogTypeId = LogtypeId;
                ActivityLogDetail.LogDateTime = DateTime.UtcNow;
                ActivityLogDetail.UserAgent = useragent;
                ActivityLogDetail.UserId = User != null ? User.UserId : nulluserid;
                ActivityLogDetail.UserIp = _IWebHelper.GetCurrentIpAddress();
                ActivityLogDetail.LogDescription = logdescription;
                InsertTempActivityLogDetail(ActivityLogDetail);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, User);
            }
        }
        #endregion

        #endregion

    }
}