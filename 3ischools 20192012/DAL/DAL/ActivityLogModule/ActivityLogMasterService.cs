﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.DAL.Common;
using System.Web.Configuration;

namespace DAL.DAL.ActivityLogModule
{
    public partial class ActivityLogMasterService : IActivityLogMasterService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public ActivityLogMasterService(IConnectionService IConnectionService)
        {
            HttpContext context = HttpContext.Current;
            this._IConnectionService = IConnectionService;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region  Methods

        #region  Table type

        public KSModel.Models.TableType GetTableTypeById(int TableTypeId)
        {
            if (TableTypeId == 0)
                throw new ArgumentNullException("TableType");

            var TableType = (from s in db.TableTypes.AsNoTracking() where s.TableTypeId == TableTypeId select s).FirstOrDefault();
            return TableType;
        }

        public List<KSModel.Models.TableType> GetAllTableTypes(ref int totalcount, string TableType = "", int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.TableTypes.ToList();
            if (!String.IsNullOrEmpty(TableType))
                query = query.Where(e => e.TableType1.ToLower().Contains(TableType.ToLower())).ToList();

            totalcount = query.Count;
            var TableTypes = new PagedList<KSModel.Models.TableType>(query, PageIndex, PageSize);
            return TableTypes;
        }

        public void InsertTableType(KSModel.Models.TableType TableType)
        {
            if (TableType == null)
                throw new ArgumentNullException("TableType");

            db.TableTypes.Add(TableType);
            db.SaveChanges();
        }

        public void UpdateTableType(KSModel.Models.TableType TableType)
        {
            if (TableType == null)
                throw new ArgumentNullException("TableType");

            db.Entry(TableType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTableType(int TableTypeId = 0)
        {
            if (TableTypeId == 0)
                throw new ArgumentNullException("TableType");

            var tableType = (from s in db.TableTypes where s.TableTypeId == TableTypeId select s).FirstOrDefault();
            db.TableTypes.Remove(tableType);
            db.SaveChanges();
        }

        #endregion

        #region Table Master

        public KSModel.Models.TableMaster GetTableMasterById(int TableMasterId)
        {
            if (TableMasterId == 0)
                throw new ArgumentNullException("TableMaster");

            var tableMaster = (from s in db.TableMasters.AsNoTracking() where s.TableId == TableMasterId select s).FirstOrDefault();
            return tableMaster;
        }

        public List<KSModel.Models.TableMaster> GetAllTableMasters(ref int totalcount, int? TableTypeId = null, string TableName = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TableMasters.ToList();
            if (TableTypeId != null && TableTypeId > 0)
                query = query.Where(e => e.TableTypeId == TableTypeId).ToList();
            if (!String.IsNullOrEmpty(TableName))
                query = query.Where(e => e.TableName.ToLower().Contains(TableName.ToLower())).ToList();

            totalcount = query.Count;
            var TableMasters = new PagedList<KSModel.Models.TableMaster>(query, PageIndex, PageSize);
            return TableMasters;
        }

        public void InsertTableMaster(KSModel.Models.TableMaster TableMaster)
        {
            if (TableMaster == null)
                throw new ArgumentNullException("TableMaster");

            db.TableMasters.Add(TableMaster);
            db.SaveChanges();
        }

        public void UpdateTableMaster(KSModel.Models.TableMaster TableMaster)
        {
            if (TableMaster == null)
                throw new ArgumentNullException("TableMaster");

            db.Entry(TableMaster).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTableMaster(int TableMasterId = 0)
        {
            if (TableMasterId == 0)
                throw new ArgumentNullException("TableMaster");

            var tableMaster = (from s in db.TableMasters where s.TableId == TableMasterId select s).FirstOrDefault();
            db.TableMasters.Remove(tableMaster);
            db.SaveChanges();
        }

        #endregion

        #region Table Col Master

        public TableCol GetTableColById(int TableColId)
        {
            if (TableColId == 0)
                throw new ArgumentNullException("TableCol");

            var TableCol = (from s in db.TableCols.AsNoTracking() where s.TableColId == TableColId select s).FirstOrDefault();
            return TableCol;
        }

        public List<TableCol> GetAllTableCols(ref int totalcount, int? TableMasterId = null, string TableCol = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TableCols.ToList();
            if (TableMasterId != null && TableMasterId > 0)
                query = query.Where(e => e.TableId == TableMasterId).ToList();
            if (!String.IsNullOrEmpty(TableCol))
                query = query.Where(e => e.ActualColName.ToLower() == TableCol.ToLower()).ToList();

            totalcount = query.Count;
            var TableCols = new PagedList<KSModel.Models.TableCol>(query, PageIndex, PageSize);
            return TableCols;
        }

        #endregion

        #region Table Master Detail

        public TableMasterDetail GetTableMasterDetailById(int TableMasterDetailId)
        {
            if (TableMasterDetailId == 0)
                throw new ArgumentNullException("TableMasterDetail");

            var tableMasterDetail = (from s in db.TableMasterDetails.AsNoTracking() where s.TableDetailId == TableMasterDetailId select s).FirstOrDefault();
            return tableMasterDetail;
        }

        public List<TableMasterDetail> GetAllTableMasterDetails(ref int totalcount, int? TableMasterId = null, DateTime? EffectiveDate = null, bool? IsLog = false, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TableMasterDetails.ToList();
            if (TableMasterId != null && TableMasterId > 0)
                query = query.Where(e => e.TableId == TableMasterId).ToList();
            if (EffectiveDate.HasValue)
                query = query.Where(e => e.EffectiveDate <= EffectiveDate.Value.Date).ToList();
            if (IsLog == true)
                query = query.Where(e => e.IsLog == IsLog).ToList();

            totalcount = query.Count;
            var tableMasterDetails = new PagedList<KSModel.Models.TableMasterDetail>(query, PageIndex, PageSize);
            return tableMasterDetails;
        }

        public void InsertTableMasterDetail(TableMasterDetail TableMasterDetail)
        {
            if (TableMasterDetail == null)
                throw new ArgumentNullException("TableMasterDetail");

            db.TableMasterDetails.Add(TableMasterDetail);
            db.SaveChanges();
        }

        public void UpdateTableMasterDetail(TableMasterDetail TableMasterDetail)
        {
            if (TableMasterDetail == null)
                throw new ArgumentNullException("TableMasterDetail");

            db.Entry(TableMasterDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTableMasterDetail(int TableMasterDetailId = 0)
        {
            if (TableMasterDetailId == 0)
                throw new ArgumentNullException("TableMasterDetail");

            var tableMasterDetail = (from s in db.TableMasterDetails where s.TableDetailId == TableMasterDetailId select s).FirstOrDefault();
            db.TableMasterDetails.Remove(tableMasterDetail);
            db.SaveChanges();
        }

        #endregion

        #region Activity Log Type

        public KSModel.Models.ActivityLogType GetActivityLogTypeById(int ActivityLogTypeId)
        {
            if (ActivityLogTypeId == 0)
                throw new ArgumentNullException("ActivityLogType");

            var activityLogType = (from s in db.ActivityLogTypes.AsNoTracking() where s.ActivityLogTypeId == ActivityLogTypeId select s).FirstOrDefault();
            return activityLogType;
        }

        public List<KSModel.Models.ActivityLogType> GetAllActivityLogTypes(ref int totalcount, string ActivityLogType = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ActivityLogTypes.ToList();
            if (!String.IsNullOrEmpty(ActivityLogType))
                query = query.Where(e => e.ActivityLogType1.ToLower().Contains(ActivityLogType.ToLower())).ToList();

            totalcount = query.Count;
            var ActivityLogTypes = new PagedList<KSModel.Models.ActivityLogType>(query, PageIndex, PageSize);
            return ActivityLogTypes;
        }

        public void InsertActivityLogType(KSModel.Models.ActivityLogType ActivityLogType)
        {
            if (ActivityLogType == null)
                throw new ArgumentNullException("ActivityLogType");

            db.ActivityLogTypes.Add(ActivityLogType);
            db.SaveChanges();
        }

        public void UpdateActivityLogType(KSModel.Models.ActivityLogType ActivityLogType)
        {
            if (ActivityLogType == null)
                throw new ArgumentNullException("ActivityLogType");

            db.Entry(ActivityLogType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteActivityLogType(int ActivityLogTypeId = 0)
        {
            if (ActivityLogTypeId == 0)
                throw new ArgumentNullException("ActivityLogType");

            var activityLogType = (from s in db.ActivityLogTypes where s.ActivityLogTypeId == ActivityLogTypeId select s).FirstOrDefault();
            db.ActivityLogTypes.Remove(activityLogType);
            db.SaveChanges();
        }

        #endregion

        #region SingleEntryTable

        public SingleEntryTable GetSingleEntryTableById(int SingleEntryTableId)
        {
            if (SingleEntryTableId == 0)
                throw new ArgumentNullException("SingleEntryTable");

            var SingleEntryTable = (from s in db.SingleEntryTables.AsNoTracking() where s.SingleEntryTableId == SingleEntryTableId select s).FirstOrDefault();
            return SingleEntryTable;
        }

        public List<SingleEntryTable> GetAllSingleEntryTables(ref int totalcount, int? TableId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SingleEntryTables.ToList();
            if (TableId > 0)
                query = query.Where(e => e.TableId == TableId).ToList();

            totalcount = query.Count;
            var SingleEntryTables = new PagedList<KSModel.Models.SingleEntryTable>(query, PageIndex, PageSize);
            return SingleEntryTables;
        }

        #endregion

        #endregion

    }
}