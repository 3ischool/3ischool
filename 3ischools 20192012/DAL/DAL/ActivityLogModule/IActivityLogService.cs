﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSModel.Models;

namespace DAL.DAL.ActivityLogModule
{
    public partial interface IActivityLogService
    {
        #region  Activity Log

        ActivityLog GetActivityLogById(int ActivityLogId);

        List<ActivityLog> GetAllActivityLogs(ref int totalcount, int? tableId = null, int? recordId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertActivityLog(ActivityLog ActivityLog);

        void UpdateActivityLog(ActivityLog ActivityLog);

        void DeleteActivityLog(int ActivityLogId = 0);

        #endregion

        #region Activity Log Detail

        ActivityLogDetail GetActivityLogDetailById(int ActivityLogDetailId);

        List<ActivityLogDetail> GetAllActivityLogDetails(ref int totalcount, int? userId = null, int? ActivityLogId = null, int? ActivityLogTypeId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertActivityLogDetail(ActivityLogDetail ActivityLogDetail);

        void UpdateActivityLogDetail(ActivityLogDetail ActivityLogDetail);

        void DeleteActivityLogDetail(int ActivityLogDetailId = 0);

        #endregion

        #region Temp Activity Log

        TempActivityLog GetTempActivityLogById(int TempActivityLogId);

        List<TempActivityLog> GetAllTempActivityLogs(ref int totalcount, int? tableId = null, int? recordId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTempActivityLog(TempActivityLog TempActivityLog);

        void InsertTempActivityLogInList(IList<TempActivityLog> TempActivityLog);
        void UpdateTempActivityLog(TempActivityLog TempActivityLog);

        void DeleteTempActivityLog(int TempActivityLogId = 0);

        #endregion

        #region Temp Activity Log Detail

        TempActivityLogDetail GetTempActivityLogDetailById(int TempActivityLogDetailId);

        List<TempActivityLogDetail> GetAllTempActivityLogDetails(ref int totalcount, int? userId = null, int? ActivityLogId = null, int? ActivityLogTypeId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTempActivityLogDetail(TempActivityLogDetail TempActivityLogDetail);
        void InsertTempActivityLogDetailInList(IList<TempActivityLogDetail> TempActivityLogDetail);
        void UpdateTempActivityLogDetail(TempActivityLogDetail TempActivityLogDetail);

        void DeleteTempActivityLogDetail(int TempActivityLogDetailId = 0);

        #endregion

        #region save activity log

        void SaveActivityLog(int RecordId, int tableId, int LogtypeId, SchoolUser User, string logdescription, string useragent);
     
        #endregion
    }
}
