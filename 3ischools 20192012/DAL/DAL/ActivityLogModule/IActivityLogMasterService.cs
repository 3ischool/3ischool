﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.DAL.ActivityLogModule
{
    public partial interface IActivityLogMasterService
    {
        #region  Table type

        TableType GetTableTypeById(int TableTypeId);

        List<TableType> GetAllTableTypes(ref int totalcount, string TableType = "", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTableType(TableType TableType);

        void UpdateTableType(TableType TableType);

        void DeleteTableType(int TableTypeId = 0);

        #endregion 
        
        #region Table Master

        TableMaster GetTableMasterById(int TableMasterId);

        List<TableMaster> GetAllTableMasters(ref int totalcount, int? TableTypeId = null, string TableName = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTableMaster(TableMaster TableMaster);

        void UpdateTableMaster(TableMaster TableMaster);

        void DeleteTableMaster(int TableMasterId = 0);

        #endregion

        #region Table Col Master

        TableCol GetTableColById(int TableColId);

        List<TableCol> GetAllTableCols(ref int totalcount, int? TableMasterId = null, string TableCol = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        #endregion

        #region Table Master Detail

        TableMasterDetail GetTableMasterDetailById(int TableMasterDetailId);

        List<TableMasterDetail> GetAllTableMasterDetails(ref int totalcount, int? TableMasterId = null, DateTime? EffectiveDate = null, bool? IsLog = false,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTableMasterDetail(TableMasterDetail TableMasterDetail);

        void UpdateTableMasterDetail(TableMasterDetail TableMasterDetail);

        void DeleteTableMasterDetail(int TableMasterDetailId = 0);

        #endregion

        #region Activity Log Type

        ActivityLogType GetActivityLogTypeById(int ActivityLogTypeId);

        List<ActivityLogType> GetAllActivityLogTypes(ref int totalcount, string ActivityLogType = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertActivityLogType(ActivityLogType ActivityLogType);

        void UpdateActivityLogType(ActivityLogType ActivityLogType);

        void DeleteActivityLogType(int ActivityLogTypeId = 0);

        #endregion

        #region SingleEntryTable

        SingleEntryTable GetSingleEntryTableById(int SingleEntryTableId);

        List<SingleEntryTable> GetAllSingleEntryTables(ref int totalcount, int? TableId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        #endregion
    }
}
