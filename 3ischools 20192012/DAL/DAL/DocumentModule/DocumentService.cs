﻿using DAL.DAL.Common;
using KSModel.Models;
using ViewModel.ViewModel.Student;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace DAL.DAL.DocumentModule
{

    //public  class BaseClass
    //{
    //    public  readonly KS_ChildEntities db;
    //    public readonly string DataSource;
    //    public readonly IConnectionService _IConnectionService;
    //    public readonly string SqlDataSource;
    //    public BaseClass() { }
    //    public BaseClass(IConnectionService IConnectionService)
    //    {
    //        this._IConnectionService = IConnectionService;
    //        HttpContext context = HttpContext.Current;
    //        this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
    //        this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
    //        this.db = new KS_ChildEntities(DataSource);
    //    }
    //}

    public partial class DocumentService : IDocumentService
    {
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private IConnectionService _IConnectionService;

        //public DocumentService()
        //{ }
        public DocumentService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));

            //this.db = new KS_ChildEntities(DataSource);

            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));

        }
        //public void intializeConection()
        //{


        //    DataSource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

        //    db = new KS_ChildEntities(DataSource);

        //    SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));


        //}

        #region Methods

        #region DocumentType

        public DocumentType GetDocumentTypeById(int DocumentTypeId, bool IsTrack = false)
        {


            if (DocumentTypeId == 0)
                throw new ArgumentNullException("DocumentType");

            var DocumentType = new DocumentType();
            if (IsTrack)
                DocumentType = (from s in db.DocumentTypes where s.DocumentTypeId == DocumentTypeId select s).FirstOrDefault();
            else
                DocumentType = (from s in db.DocumentTypes.AsNoTracking() where s.DocumentTypeId == DocumentTypeId select s).FirstOrDefault();

            return DocumentType;
        }

        public List<DocumentType> GetAllDocumentTypes(ref int totalcount, string DocumentType = "", int PageIndex = 0,
            int PageSize = int.MaxValue)
        {

            var query = db.DocumentTypes.ToList();
            if (!String.IsNullOrEmpty(DocumentType))
                query = query.Where(e => e.DocumentType1.ToLower().Contains(DocumentType.ToLower())).ToList();

            totalcount = query.Count;
            var DocumentTypes = new PagedList<KSModel.Models.DocumentType>(query, PageIndex, PageSize);
            return DocumentTypes;
        }


        public List<StudentPendingDocumentReportModel> GetStudentPendingDocumentTypeBySP(ref int totalcount, string DocumentType = "", int? ClassId = null,
                                                         int? SessionId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {

            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_getPendingDocumentReport", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@ClassId", ClassId);
                    sqlComm.Parameters.AddWithValue("@DocumentType", DocumentType);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }

            List<StudentPendingDocumentReportModel> GetAllDocuments = new List<StudentPendingDocumentReportModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                StudentPendingDocumentReportModel PendingDoclist = new StudentPendingDocumentReportModel();
                PendingDoclist.ClassId = Convert.ToInt32(dt.Rows[i]["ClassId"]);
                PendingDoclist.Class = Convert.ToString(dt.Rows[i]["Class"]);
                PendingDoclist.StudentName = Convert.ToString(dt.Rows[i]["StudentName"]);

                PendingDoclist.ParentName = "";
                if (!DBNull.Value.Equals(dt.Rows[i]["ParentName"]))
                    PendingDoclist.ParentName = Convert.ToString(dt.Rows[i]["ParentName"]);

                PendingDoclist.AdmnNo = Convert.ToString(dt.Rows[i]["AdmissionNumber"]);

                PendingDoclist.PendingDocuments = "";
                if (!DBNull.Value.Equals(dt.Rows[i]["documents"]))
                    PendingDoclist.PendingDocuments = Convert.ToString(dt.Rows[i]["documents"]);

                GetAllDocuments.Add(PendingDoclist);
            }
            totalcount = GetAllDocuments.Count;

            GetAllDocuments = new PagedList<StudentPendingDocumentReportModel>(GetAllDocuments, PageIndex, PageSize);
            return GetAllDocuments;
        }

        public void InsertDocumentType(DocumentType DocumentType)
        {

            if (DocumentType == null)
                throw new ArgumentNullException("DocumentType");

            db.DocumentTypes.Add(DocumentType);
            db.SaveChanges();
        }

        public void UpdateDocumentType(DocumentType DocumentType)
        {

            if (DocumentType == null)
                throw new ArgumentNullException("DocumentType");

            db.Entry(DocumentType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteDocumentType(int DocumentTypeId = 0)
        {

            if (DocumentTypeId == 0)
                throw new ArgumentNullException("DocumentType");

            var DocumentType = (from s in db.DocumentTypes where s.DocumentTypeId == DocumentTypeId select s).FirstOrDefault();
            db.DocumentTypes.Remove(DocumentType);
            db.SaveChanges();
        }

        #endregion

        #region AttachedDocument

        public AttachedDocument GetAttachedDocumentById(int AttachedDocumentId, bool IsTrack = false)
        {

            if (AttachedDocumentId == 0)
                throw new ArgumentNullException("AttachedDocument");

            var AttachedDocument = new AttachedDocument();
            if (IsTrack)
                AttachedDocument = (from s in db.AttachedDocuments where s.DocumentId == AttachedDocumentId select s).FirstOrDefault();
            else
                AttachedDocument = (from s in db.AttachedDocuments.AsNoTracking() where s.DocumentId == AttachedDocumentId select s).FirstOrDefault();

            return AttachedDocument;
        }

        public List<AttachedDocument> GetAllAttachedDocuments(ref int totalcount, int? DocumentTypeId = null, string Image = "",
            int? StudentId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {

            var query = db.AttachedDocuments.ToList();
            if (!String.IsNullOrEmpty(Image))
                query = query.Where(e => e.Image.ToLower() == Image.ToLower()).ToList();
            if (DocumentTypeId != null && DocumentTypeId > 0)
                query = query.Where(e => e.DocumentTypeId == DocumentTypeId).ToList();
            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();

            totalcount = query.Count;
            var AttachedDocuments = new PagedList<KSModel.Models.AttachedDocument>(query, PageIndex, PageSize);
            return AttachedDocuments;
        }

        public string GetImageByStudentId(ref int totalcount,
           int? StudentId = null)
        {

            var query = db.AttachedDocuments.Where(m => m.DocumentType.DocumentType1 != "image" && m.StudentId == StudentId).FirstOrDefault();
            var image = "";
            if (query != null)
            {
                image = query.Image;
            }



            return image;
        }

        public void InsertAttachedDocument(AttachedDocument AttachedDocument)
        {

            if (AttachedDocument == null)
                throw new ArgumentNullException("AttachedDocument");

            db.AttachedDocuments.Add(AttachedDocument);
            db.SaveChanges();
        }

        public void UpdateAttachedDocument(AttachedDocument AttachedDocument)
        {

            if (AttachedDocument == null)
                throw new ArgumentNullException("AttachedDocument");

            db.Entry(AttachedDocument).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAttachedDocument(int AttachedDocumentId = 0)
        {

            if (AttachedDocumentId == 0)
                throw new ArgumentNullException("AttachedDocument");

            var AttachedDocument = (from s in db.AttachedDocuments where s.DocumentId == AttachedDocumentId select s).FirstOrDefault();
            db.AttachedDocuments.Remove(AttachedDocument);
            db.SaveChanges();
        }

        #endregion

        #region DocumentGroup
        public DocumentGroup GetDocumentGroupById(int DocumentGroupId, bool IsTrack = false)
        {

            if (DocumentGroupId == 0)
                throw new ArgumentNullException("DocumentGroup");

            var DocumentGroup = new DocumentGroup();
            if (IsTrack)
                DocumentGroup = (from s in db.DocumentGroups where s.DocumentGroupId == DocumentGroupId select s).FirstOrDefault();
            else
                DocumentGroup = (from s in db.DocumentGroups.AsNoTracking() where s.DocumentGroupId == DocumentGroupId select s).FirstOrDefault();

            return DocumentGroup;
        }

        public List<DocumentGroup> GetAllDocumentGroups(ref int totalcount, string DocumentGroup = "",
                            int PageIndex = 0, int PageSize = int.MaxValue)
        {

            var query = db.DocumentGroups.ToList();
            if (!String.IsNullOrEmpty(DocumentGroup))
                query = query.Where(e => e.DocumentGroup1.ToLower() == DocumentGroup.ToLower()).ToList();

            totalcount = query.Count;
            var DocumentGroups = new PagedList<KSModel.Models.DocumentGroup>(query, PageIndex, PageSize);
            return DocumentGroups;
        }

        public void InsertDocumentGroup(DocumentGroup DocumentGroup)
        {

            if (DocumentGroup == null)
                throw new ArgumentNullException("DocumentGroup");

            db.DocumentGroups.Add(DocumentGroup);
            db.SaveChanges();
        }

        public void UpdateDocumentGroup(DocumentGroup DocumentGroup)
        {

            if (DocumentGroup == null)
                throw new ArgumentNullException("DocumentGroup");

            db.Entry(DocumentGroup).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteDocumentGroup(int DocumentGroupId = 0)
        {

            if (DocumentGroupId == 0)
                throw new ArgumentNullException("DocumentGroup");

            var DocumentGroup = (from s in db.DocumentGroups where s.DocumentGroupId == DocumentGroupId select s).FirstOrDefault();
            db.DocumentGroups.Remove(DocumentGroup);
            db.SaveChanges();
        }

        #endregion

        #region DocumentDetail
        public DocumentDetail GetDocumentDetailById(int DocumentDetailId, bool IsTrack = false)
        {

            if (DocumentDetailId == 0)
                throw new ArgumentNullException("DocumentDetail");

            var DocumentDetail = new DocumentDetail();
            if (IsTrack)
                DocumentDetail = (from s in db.DocumentDetails where s.DocumentDetailId == DocumentDetailId select s).FirstOrDefault();
            else
                DocumentDetail = (from s in db.DocumentDetails.AsNoTracking() where s.DocumentDetailId == DocumentDetailId select s).FirstOrDefault();

            return DocumentDetail;
        }

        public List<DocumentDetail> GetAllDocumentDetail(ref int totalcount, int DocumentId = 0, DateTime? IssuedOn = null,
        DateTime? ReminderDate = null, int ReminderType = 0, DateTime? ExpiryDate = null, DateTime? UploadedOn = null, string Remarks = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {

            var query = db.DocumentDetails.ToList();
            if (IssuedOn != null)
                query = query.Where(e => e.IssuedOn == IssuedOn).ToList();

            if (UploadedOn != null)
                query = query.Where(e => e.UploadedOn == UploadedOn).ToList();

            if (ReminderDate != null)
                query = query.Where(e => e.ReminderDate == ReminderDate).ToList();

            if (ExpiryDate != null)
                query = query.Where(e => e.ExpiryDate == ExpiryDate).ToList();

            if (!String.IsNullOrEmpty(Remarks))
                query = query.Where(e => e.Remarks.ToLower() == Remarks.ToLower()).ToList();

            if (DocumentId > 0)
                query = query.Where(e => e.DocumentId == DocumentId).ToList();

            totalcount = query.Count;
            var DocumentDetails = new PagedList<KSModel.Models.DocumentDetail>(query, PageIndex, PageSize);
            return DocumentDetails;
        }

        public void InsertDocumentDetail(DocumentDetail DocumentDetail)
        {

            if (DocumentDetail == null)
                throw new ArgumentNullException("DocumentDetail");

            db.DocumentDetails.Add(DocumentDetail);
            db.SaveChanges();
        }

        public void UpdateDocumentDetail(DocumentDetail DocumentDetail)
        {

            if (DocumentDetail == null)
                throw new ArgumentNullException("DocumentDetail");

            db.Entry(DocumentDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteDocumentDetail(int DocumentDetailId = 0)
        {

            if (DocumentDetailId == 0)
                throw new ArgumentNullException("DocumentGroup");

            var DocumentDetail = (from s in db.DocumentDetails where s.DocumentDetailId == DocumentDetailId select s).FirstOrDefault();
            db.DocumentDetails.Remove(DocumentDetail);
            db.SaveChanges();
        }

        #endregion

        #region DocumentCategory
        public DocumentCategory GetDocumentCategoryById(int DocumentCategoryId, bool IsTrack = false)
        {

            if (DocumentCategoryId == 0)
                throw new ArgumentNullException("DocumentCategory");

            var DocumentCategory = new DocumentCategory();
            if (IsTrack)
                DocumentCategory = (from s in db.DocumentCategories where s.DocumentCategoryId == DocumentCategoryId select s).FirstOrDefault();
            else
                DocumentCategory = (from s in db.DocumentCategories.AsNoTracking() where s.DocumentCategoryId == DocumentCategoryId select s).FirstOrDefault();

            return DocumentCategory;
        }

        public IList<DocumentCategory> GetAllDocumentCategory(ref int totalcount, int DocumentCategoryId = 0, bool? IsActive = null, string DocumentCategory = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {

            var query = db.DocumentCategories.ToList();


            if (!String.IsNullOrEmpty(DocumentCategory))
                query = query.Where(e => e.DocumentCategory1.ToLower() == DocumentCategory.ToLower()).ToList();

            if (DocumentCategoryId > 0)
                query = query.Where(e => e.DocumentCategoryId == DocumentCategoryId).ToList();

            totalcount = query.Count;
            var DocumentCategories = new PagedList<KSModel.Models.DocumentCategory>(query, PageIndex, PageSize);
            return DocumentCategories;
        }

        public void InsertDocumentCategory(DocumentCategory DocumentCategory)
        {

            if (DocumentCategory == null)
                throw new ArgumentNullException("DocumentCategory");

            db.DocumentCategories.Add(DocumentCategory);
            db.SaveChanges();
        }

        public void UpdateDocumentCategory(DocumentCategory DocumentCategory)
        {

            if (DocumentCategory == null)
                throw new ArgumentNullException("DocumentCategory");

            db.Entry(DocumentCategory).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteDocumentCategory(int DocumentCategoryId = 0)
        {

            if (DocumentCategoryId == 0)
                throw new ArgumentNullException("DocumentCategory");

            var DocumentCategory = (from s in db.DocumentCategories where s.DocumentCategoryId == DocumentCategoryId select s).FirstOrDefault();
            db.DocumentCategories.Remove(DocumentCategory);
            db.SaveChanges();
        }

        #endregion

        #region Document
        public Document GetDocumentById(int DocumentId, bool IsTrack = false)
        {

            if (DocumentId == 0)
                throw new ArgumentNullException("Document");

            var Document = new Document();
            if (IsTrack)
                Document = (from s in db.Documents where s.DocumentId == DocumentId select s).FirstOrDefault();
            else
                Document = (from s in db.Documents.AsNoTracking() where s.DocumentId == DocumentId select s).FirstOrDefault();

            return Document;
        }

        public IList<Document> GetAllDocument(ref int totalcount, int DocumentCategoryId = 0, bool? IsActive = null, string DocumentTitle = "", string DocumentDescription = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {

            var query = db.Documents.ToList();


            if (!String.IsNullOrEmpty(DocumentTitle))
                query = query.Where(e => e.DocumentTitle.ToLower() == DocumentTitle.ToLower()).ToList();

            if (!String.IsNullOrEmpty(DocumentDescription))
                query = query.Where(e => e.DocumentDescription.ToLower() == DocumentDescription.ToLower()).ToList();

            if (DocumentCategoryId > 0)
                query = query.Where(e => e.DocumentCategoryId == DocumentCategoryId).ToList();

            totalcount = query.Count;
            var Documents = new PagedList<KSModel.Models.Document>(query, PageIndex, PageSize);
            return Documents;
        }

        public void InsertDocument(Document Document)
        {

            if (Document == null)
                throw new ArgumentNullException("Document");

            db.Documents.Add(Document);
            db.SaveChanges();
        }

        public void UpdateDocument(Document Document)
        {

            if (Document == null)
                throw new ArgumentNullException("Document");

            db.Entry(Document).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteDocument(int DocumentId = 0)
        {

            if (DocumentId == 0)
                throw new ArgumentNullException("Document");

            var Document = (from s in db.Documents where s.DocumentId == DocumentId select s).FirstOrDefault();
            db.Documents.Remove(Document);
            db.SaveChanges();
        }

        #endregion

        #region DocReminderType
        public DocReminderType GetDocReminderTypeById(int ReminderTypeId, bool IsTrack = false)
        {

            if (ReminderTypeId == 0)
                throw new ArgumentNullException("DocReminderType");

            var DocReminderType = new DocReminderType();
            if (IsTrack)
                DocReminderType = (from s in db.DocReminderTypes where s.ReminderTypeId == ReminderTypeId select s).FirstOrDefault();
            else
                DocReminderType = (from s in db.DocReminderTypes.AsNoTracking() where s.ReminderTypeId == ReminderTypeId select s).FirstOrDefault();

            return DocReminderType;
        }

        public List<DocReminderType> GetAllDocReminderTypes(ref int totalcount,
            string ReminderType = "", bool? IsActive = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {

            var query = db.DocReminderTypes.ToList();

            if (!String.IsNullOrEmpty(ReminderType))
                query = query.Where(e => e.ReminderType.ToLower() == ReminderType.ToLower()).ToList();

            if (IsActive != null)
                query = query.Where(e => e.IsActive == IsActive).ToList();

            totalcount = query.Count;
            var DocReminderType = new PagedList<KSModel.Models.DocReminderType>(query, PageIndex, PageSize);
            return DocReminderType;
        }

        public void InsertDocReminderType(DocReminderType DocReminderType)
        {

            if (DocReminderType == null)
                throw new ArgumentNullException("DocReminderType");

            db.DocReminderTypes.Add(DocReminderType);
            db.SaveChanges();
        }

        public void UpdateDocReminderType(DocReminderType DocReminderType)
        {

            if (DocReminderType == null)
                throw new ArgumentNullException("DocReminderType");

            db.Entry(DocReminderType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteDocReminderType(int ReminderTypeId = 0)
        {

            if (ReminderTypeId == 0)
                throw new ArgumentNullException("DocReminderType");

            var DocReminderType = (from s in db.DocReminderTypes where s.ReminderTypeId == ReminderTypeId select s).FirstOrDefault();
            db.DocReminderTypes.Remove(DocReminderType);
            db.SaveChanges();
        }

        #endregion

        #endregion
    }
}