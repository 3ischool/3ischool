﻿using DAL.DAL.Common;
using KSModel.Models;
using ViewModel.ViewModel.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using DAL.DAL.Kendo;
//using BAL.BAL.Common;
using DAL.DAL.StockModule;
using DAL.DAL.StaffModule;

namespace DAL.DAL.CatalogMaster
{
    public partial class CatalogMasterService : ICatalogMasterService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;
        //private readonly string SqlDataSource;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IStockService _IStockService;
        private readonly IStaffService _IStaffService;

        public CatalogMasterService(IConnectionService IConnectionService, ICustomEncryption ICustomEncryption,
             IStockService IStockService, IStaffService IStaffService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            this._ICustomEncryption = ICustomEncryption;
            this._IStockService = IStockService;
            this._IStaffService = IStaffService;
        }

        #region Methods

        #region CustodyRight

        public KSModel.Models.CustodyRight GetCustodyRightById(int CustodyRightId)
        {
            if (CustodyRightId == 0)
                throw new ArgumentNullException("CustodyRight");

            var CustodyRight = (from s in db.CustodyRights.AsNoTracking() where s.CustodyRightId == CustodyRightId select s).FirstOrDefault();
            return CustodyRight;
        }

        public List<KSModel.Models.CustodyRight> GetAllCustodyRights(ref int totalcount, string CustodyRight = "", int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.CustodyRights.ToList();
            if (!String.IsNullOrEmpty(CustodyRight))
                query = query.Where(e => e.CustodyRight1.ToLower() == CustodyRight.ToLower()).ToList();

            totalcount = query.Count;
            var CustodyRights = new PagedList<KSModel.Models.CustodyRight>(query, PageIndex, PageSize);
            return CustodyRights;
        }

        public void InsertCustodyRight(KSModel.Models.CustodyRight CustodyRight)
        {
            if (CustodyRight == null)
                throw new ArgumentNullException("CustodyRight");

            db.CustodyRights.Add(CustodyRight);
            db.SaveChanges();
        }

        public void UpdateCustodyRight(KSModel.Models.CustodyRight CustodyRight)
        {
            if (CustodyRight == null)
                throw new ArgumentNullException("CustodyRight");

            db.Entry(CustodyRight).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteCustodyRight(int CustodyRightId = 0)
        {
            if (CustodyRightId == 0)
                throw new ArgumentNullException("CustodyRight");

            var CustodyRight = (from s in db.CustodyRights where s.CustodyRightId == CustodyRightId select s).FirstOrDefault();
            db.CustodyRights.Remove(CustodyRight);
            db.SaveChanges();
        }

        #endregion

        #region Board

        public KSModel.Models.Board GetBoardById(int BoardId)
        {
            if (BoardId == 0)
                throw new ArgumentNullException("Board");

            var Board = (from s in db.Boards.AsNoTracking() where s.BoardId == BoardId select s).FirstOrDefault();
            return Board;
        }

        public List<KSModel.Models.Board> GetAllBoards(ref int totalcount, string Board = "", int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.Boards.ToList();
            if (!String.IsNullOrEmpty(Board))
                query = query.Where(e => e.Board1.ToLower().Contains(Board.ToLower())).ToList();

            totalcount = query.Count;
            var Boards = new PagedList<KSModel.Models.Board>(query, PageIndex, PageSize);
            return Boards;
        }

        public void InsertBoard(KSModel.Models.Board Board)
        {
            if (Board == null)
                throw new ArgumentNullException("Board");

            db.Boards.Add(Board);
            db.SaveChanges();
        }

        public void UpdateBoard(KSModel.Models.Board Board)
        {
            if (Board == null)
                throw new ArgumentNullException("Board");

            db.Entry(Board).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBoard(int BoardId = 0)
        {
            if (BoardId == 0)
                throw new ArgumentNullException("Board");

            var Board = (from s in db.Boards where s.BoardId == BoardId select s).FirstOrDefault();
            db.Boards.Remove(Board);
            db.SaveChanges();
        }

        #endregion

        #region StandardMaster

        public KSModel.Models.StandardMaster GetStandardMasterById(int StandardMasterId)
        {
            if (StandardMasterId == 0)
                throw new ArgumentNullException("StandardMaster");

            var StandardMaster = (from s in db.StandardMasters.AsNoTracking() where s.StandardId == StandardMasterId select s).FirstOrDefault();
            return StandardMaster;
        }

        public List<KSModel.Models.StandardMaster> GetAllStandardMasters(ref int totalcount, string Standard = "", decimal? standardindex = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StandardMasters.ToList();
            if (!String.IsNullOrEmpty(Standard))
                query = query.Where(e => e.Standard.ToLower().Contains(Standard.ToLower())).ToList();
            if (standardindex != null && standardindex > 0)
                query = query.Where(s => s.StandardIndex == standardindex).ToList();
            query = query.OrderBy(s => s.StandardIndex).ToList();

            totalcount = query.Count;
            var StandardMasters = new PagedList<KSModel.Models.StandardMaster>(query, PageIndex, PageSize);
            return StandardMasters;
        }

        public void InsertStandardMaster(KSModel.Models.StandardMaster StandardMaster)
        {
            if (StandardMaster == null)
                throw new ArgumentNullException("FeeClassGroup");

            db.StandardMasters.Add(StandardMaster);
            db.SaveChanges();
        }

        public void UpdateStandardMaster(KSModel.Models.StandardMaster StandardMaster)
        {
            if (StandardMaster == null)
                throw new ArgumentNullException("StandardMaster");

            db.Entry(StandardMaster).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStandardMaster(int StandardMasterId = 0)
        {
            if (StandardMasterId == 0)
                throw new ArgumentNullException("StandardMaster");

            var StandardMaster = (from s in db.StandardMasters where s.StandardId == StandardMasterId select s).FirstOrDefault();
            db.StandardMasters.Remove(StandardMaster);
            db.SaveChanges();
        }

        #endregion

        #region Section

        public KSModel.Models.Section GetSectionById(int SectionId)
        {
            if (SectionId == 0)
                throw new ArgumentNullException("Section");

            var Section = (from s in db.Sections.AsNoTracking() where s.SectionId == SectionId select s).FirstOrDefault();
            return Section;
        }

        public List<KSModel.Models.Section> GetAllSections(ref int totalcount, string Section = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Sections.ToList();
            if (!String.IsNullOrEmpty(Section))
                query = query.Where(e => e.Section1.ToLower().Contains(Section.ToLower())).ToList();

            totalcount = query.Count;
            var Sections = new PagedList<KSModel.Models.Section>(query, PageIndex, PageSize);
            return Sections;
        }

        public void InsertSection(KSModel.Models.Section Section)
        {
            if (Section == null)
                throw new ArgumentNullException("Section");

            db.Sections.Add(Section);
            db.SaveChanges();
        }

        public void UpdateSection(KSModel.Models.Section Section)
        {
            if (Section == null)
                throw new ArgumentNullException("Section");

            db.Entry(Section).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSection(int SectionId = 0)
        {
            if (SectionId == 0)
                throw new ArgumentNullException("Section");

            var Section = (from s in db.Sections where s.SectionId == SectionId select s).FirstOrDefault();
            db.Sections.Remove(Section);
            db.SaveChanges();
        }

        #endregion

        #region Session

        public KSModel.Models.Session GetSessionById(int SessionId, bool IsTrack = false)
        {
            if (SessionId == 0)
                throw new ArgumentNullException("Session");
            var Session = new Session();
            if (IsTrack)
                Session = (from s in db.Sessions where s.SessionId == SessionId select s).FirstOrDefault();
            else
                Session = (from s in db.Sessions.AsNoTracking() where s.SessionId == SessionId select s).FirstOrDefault();

            return Session;
        }

        public List<KSModel.Models.Session> GetAllSessions(ref int totalcount, string Session = "", DateTime? StartDate = null,
            DateTime? Enddate = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isdeafult = false, int SessionId = 0)
        {
            var query = db.Sessions.ToList();
            if (Isdeafult == true)
            {
                query = query.Where(m => m.IsDefualt == true).ToList();
            }
            if (SessionId > 0)
            {
                query = query.Where(m => m.SessionId == SessionId).ToList();
            }
            if (!String.IsNullOrEmpty(Session))
                query = query.Where(e => e.Session1.ToLower() == Session.ToLower()).ToList();
            if (StartDate.HasValue)
                query = query.Where(s => s.StartDate <= StartDate).ToList();
            if (Enddate.HasValue)
                query = query.Where(s => s.EndDate >= Enddate).ToList();

            totalcount = query.Count;
            var Sessions = new PagedList<KSModel.Models.Session>(query, PageIndex, PageSize);
            return Sessions;
        }
        public List<KSModel.Models.Session> GetAllNewSessions(ref int count, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Sessions.ToList();

            count = query.Count;
            var Sessions = new PagedList<KSModel.Models.Session>(query, PageIndex, PageSize);
            return Sessions;
        }

        public Session GetCurrentSession(int? DaysBeforeAdmnOpening = null)
        {
            Session crntsession = new Session();
            //DateTime? CurrentSessionFromDate;
            //DateTime? CurrentSessionToDate;
            int count = 0;
            // today date
            var today = DateTime.UtcNow;
            //if (today.Month > 3) // means new session start
            //{
            //    CurrentSessionFromDate = Convert.ToDateTime("01-April-" + today.Year);
            //    CurrentSessionToDate = Convert.ToDateTime("31-March-" + (today.Year + 1));
            //    crntsession = GetAllSessions(ref count, StartDate: CurrentSessionFromDate, Enddate: CurrentSessionToDate).FirstOrDefault();
            //}
            //else // prev session
            //{
            //    CurrentSessionFromDate = Convert.ToDateTime("01-April-" + (today.Year - 1));
            //    CurrentSessionToDate = Convert.ToDateTime("31-March-" + today.Year);
            crntsession = GetAllSessions(ref count).Where(p => p.StartDate.Value.Date <= today.Date && p.EndDate.Value.Date >= today.Date).FirstOrDefault();
            //}

            if (DaysBeforeAdmnOpening > 0)
            {
                var currentdate = DateTime.UtcNow.Date;
                var daysdiff = crntsession.EndDate.Value.Date - currentdate;
                if (daysdiff.Days > DaysBeforeAdmnOpening)
                    return crntsession;
                else
                {
                    // get next session
                    var allsessions = GetAllSessions(ref count);
                    var nextsession = allsessions.Where(s => s.SessionId > crntsession.SessionId).OrderBy(s => s.SessionId).FirstOrDefault();
                    return nextsession;
                }
            }

            return crntsession;
        }

        public Session GetSessionDetail(int Session)
        {
            if (Session > 0)
            {
                var session = db.Sessions.Where(m => m.SessionId == Session).FirstOrDefault();
                return session;
            }
            return null;
        }

        public Session GetPreviousSession()
        {
            Session prevsession = new Session();
            DateTime? CurrentSessionFromDate;
            DateTime? CurrentSessionToDate;
            int count = 0;
            // today date
            var today = DateTime.UtcNow;
            if (today.Month > 3) // means new session start
            {
                CurrentSessionFromDate = Convert.ToDateTime("01-April-" + (today.Year - 1));
                CurrentSessionToDate = Convert.ToDateTime("31-March-" + (today.Year));
                prevsession = GetAllSessions(ref count, StartDate: CurrentSessionFromDate, Enddate: CurrentSessionToDate).FirstOrDefault();
            }
            else // prev session
            {
                CurrentSessionFromDate = Convert.ToDateTime("01-April-" + (today.Year - 2));
                CurrentSessionToDate = Convert.ToDateTime("31-March-" + (today.Year - 1));
                prevsession = GetAllSessions(ref count, StartDate: CurrentSessionFromDate, Enddate: CurrentSessionToDate).FirstOrDefault();
            }

            return prevsession;
        }

        public bool DateInCurrentSession(DateTime fromdate, DateTime? todate = null)
        {
            bool IsinCureentSession = true;
            DateTime? CurrentSessionFromDate;
            DateTime? CurrentSessionToDate;
            // today date
            var today = DateTime.UtcNow;
            if (today.Month > 3) // means new session
            {
                CurrentSessionFromDate = Convert.ToDateTime("01-April-" + today.Year);
                CurrentSessionToDate = Convert.ToDateTime("31-March-" + (today.Year + 1));
            }
            else
            {
                CurrentSessionFromDate = Convert.ToDateTime("01-April-" + (today.Year - 1));
                CurrentSessionToDate = Convert.ToDateTime("31-March-" + today.Year);
            }

            if (CurrentSessionFromDate <= fromdate && CurrentSessionToDate >= fromdate)
            {
                if (todate.HasValue)
                {
                    if (CurrentSessionFromDate <= todate.Value && CurrentSessionToDate >= todate.Value)
                        IsinCureentSession = true;
                    else
                        IsinCureentSession = false;
                }
            }
            else
                IsinCureentSession = false;

            return IsinCureentSession;
        }

        public void InsertSession(KSModel.Models.Session Session)
        {
            if (Session == null)
                throw new ArgumentNullException("Session");

            db.Sessions.Add(Session);
            db.SaveChanges();
        }

        public void UpdateSession(KSModel.Models.Session Session)
        {
            if (Session == null)
                throw new ArgumentNullException("Session");

            db.Entry(Session).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSession(int SessionId = 0)
        {
            if (SessionId == 0)
                throw new ArgumentNullException("Session");

            var Session = (from s in db.Sessions where s.SessionId == SessionId select s).FirstOrDefault();
            db.Sessions.Remove(Session);
            db.SaveChanges();
        }

        #endregion

        #region SessionAdmissionDetail

        public SessionAdmissionDetail GetSessionAdmissionDetailById(int SessionAdmissionDetailId, bool IsTrack = false)
        {
            if (SessionAdmissionDetailId == 0)
                throw new ArgumentNullException("SessionAdmissionDetail");

            var SessionAdmissionDetail = new SessionAdmissionDetail();
            if (IsTrack)
                SessionAdmissionDetail = (from s in db.SessionAdmissionDetails where s.SessionAdmnDetId == SessionAdmissionDetailId select s).FirstOrDefault();
            else
                SessionAdmissionDetail = (from s in db.SessionAdmissionDetails.AsNoTracking() where s.SessionAdmnDetId == SessionAdmissionDetailId select s).FirstOrDefault();

            return SessionAdmissionDetail;
        }

        public List<SessionAdmissionDetail> GetAllSessionAdmissionDetails(ref int totalcount, int? SessionId = null, int? StandardId = null,
            DateTime? AdmnDate = null, int? Seats = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SessionAdmissionDetails.ToList();
            if (SessionId > 0)
                query = query.Where(e => e.SessionId == SessionId).ToList();
            if (StandardId > 0)
                query = query.Where(e => e.StandardId == StandardId).ToList();
            if (AdmnDate.HasValue)
                query = query.Where(s => s.AdmnOpenDate <= AdmnDate && s.AdmnClosingDate >= AdmnDate).ToList();

            totalcount = query.Count;
            var SessionAdmissionDetails = new PagedList<KSModel.Models.SessionAdmissionDetail>(query, PageIndex, PageSize);
            return SessionAdmissionDetails;
        }

        public void InsertSessionAdmissionDetail(SessionAdmissionDetail SessionAdmissionDetail)
        {
            if (SessionAdmissionDetail == null)
                throw new ArgumentNullException("SessionAdmissionDetail");

            db.SessionAdmissionDetails.Add(SessionAdmissionDetail);
            db.SaveChanges();
        }

        public void UpdateSessionAdmissionDetail(SessionAdmissionDetail SessionAdmissionDetail)
        {
            if (SessionAdmissionDetail == null)
                throw new ArgumentNullException("SessionAdmissionDetail");

            db.Entry(SessionAdmissionDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSessionAdmissionDetail(int SessionAdmissionDetailId = 0)
        {
            if (SessionAdmissionDetailId == 0)
                throw new ArgumentNullException("SessionAdmissionDetail");

            var SessionAdmissionDetail = (from s in db.SessionAdmissionDetails where s.SessionAdmnDetId == SessionAdmissionDetailId select s).FirstOrDefault();
            db.SessionAdmissionDetails.Remove(SessionAdmissionDetail);
            db.SaveChanges();
        }

        #endregion

        #region Gender

        public KSModel.Models.Gender GetGenderById(int GenderId)
        {
            if (GenderId == 0)
                throw new ArgumentNullException("Gender");

            var Gender = (from s in db.Genders.AsNoTracking() where s.GenderId == GenderId select s).FirstOrDefault();
            return Gender;
        }

        public List<KSModel.Models.Gender> GetAllGenders(ref int totalcount, string Gender = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Genders.ToList();
            if (!String.IsNullOrEmpty(Gender))
                query = query.Where(e => e.Gender1.ToLower().Contains(Gender.ToLower())).ToList();

            totalcount = query.Count;
            var Genders = new PagedList<KSModel.Models.Gender>(query, PageIndex, PageSize);
            return Genders;
        }

        public void InsertGender(KSModel.Models.Gender Gender)
        {
            if (Gender == null)
                throw new ArgumentNullException("Gender");

            db.Genders.Add(Gender);
            db.SaveChanges();
        }

        public void UpdateGender(KSModel.Models.Gender Gender)
        {
            if (Gender == null)
                throw new ArgumentNullException("Gender");

            db.Entry(Gender).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteGender(int GenderId = 0)
        {
            if (GenderId == 0)
                throw new ArgumentNullException("Gender");

            var Gender = (from s in db.Genders where s.GenderId == GenderId select s).FirstOrDefault();
            db.Genders.Remove(Gender);
            db.SaveChanges();
        }

        #endregion

        #region BloodGroup

        public KSModel.Models.BloodGroup GetBloodGroupById(int BloodGroupId)
        {
            if (BloodGroupId == 0)
                throw new ArgumentNullException("BloodGroup");

            var BloodGroup = (from s in db.BloodGroups.AsNoTracking() where s.BloodGroupId == BloodGroupId select s).FirstOrDefault();
            return BloodGroup;
        }

        public List<KSModel.Models.BloodGroup> GetAllBloodGroups(ref int totalcount, string BloodGroup = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.BloodGroups.ToList();
            if (!String.IsNullOrEmpty(BloodGroup))
                query = query.Where(e => e.BloodGroup1.ToLower().Contains(BloodGroup.ToLower())).ToList();

            totalcount = query.Count;
            var BloodGroups = new PagedList<KSModel.Models.BloodGroup>(query, PageIndex, PageSize);
            return BloodGroups;
        }

        public void InsertBloodGroup(KSModel.Models.BloodGroup BloodGroup)
        {
            if (BloodGroup == null)
                throw new ArgumentNullException("BloodGroup");

            db.BloodGroups.Add(BloodGroup);
            db.SaveChanges();
        }

        public void UpdateBloodGroup(KSModel.Models.BloodGroup BloodGroup)
        {
            if (BloodGroup == null)
                throw new ArgumentNullException("BloodGroup");

            db.Entry(BloodGroup).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBloodGroup(int BloodGroupId = 0)
        {
            if (BloodGroupId == 0)
                throw new ArgumentNullException("BloodGroup");

            var BloodGroup = (from s in db.BloodGroups where s.BloodGroupId == BloodGroupId select s).FirstOrDefault();
            db.BloodGroups.Remove(BloodGroup);
            db.SaveChanges();
        }

        #endregion

        #region Nationality

        public KSModel.Models.Nationality GetNationalityById(int NationalityId)
        {
            if (NationalityId == 0)
                throw new ArgumentNullException("Nationality");

            var Nationality = (from s in db.Nationalities.AsNoTracking() where s.NationalityId == NationalityId select s).FirstOrDefault();
            return Nationality;
        }

        public List<KSModel.Models.Nationality> GetAllNationalitys(ref int totalcount, string Nationality = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Nationalities.ToList();
            if (!String.IsNullOrEmpty(Nationality))
                query = query.Where(e => e.Nationality1.ToLower().Contains(Nationality.ToLower())).ToList();

            totalcount = query.Count;
            var Nationalities = new PagedList<KSModel.Models.Nationality>(query, PageIndex, PageSize);
            return Nationalities;
        }

        public void InsertNationality(KSModel.Models.Nationality Nationality)
        {
            if (Nationality == null)
                throw new ArgumentNullException("Nationality");

            db.Nationalities.Add(Nationality);
            db.SaveChanges();
        }

        public void UpdateNationality(KSModel.Models.Nationality Nationality)
        {
            if (Nationality == null)
                throw new ArgumentNullException("Nationality");

            db.Entry(Nationality).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteNationality(int NationalityId = 0)
        {
            if (NationalityId == 0)
                throw new ArgumentNullException("Nationality");

            var Nationality = (from s in db.Nationalities where s.NationalityId == NationalityId select s).FirstOrDefault();
            db.Nationalities.Remove(Nationality);
            db.SaveChanges();
        }

        #endregion

        #region Qualification

        public KSModel.Models.Qualification GetQualificationById(int QualificationId)
        {
            if (QualificationId == 0)
                throw new ArgumentNullException("Qualification");

            var Qualification = (from s in db.Qualifications.AsNoTracking() where s.QualificationId == QualificationId select s).FirstOrDefault();
            return Qualification;
        }

        public List<KSModel.Models.Qualification> GetAllQualifications(ref int totalcount, string Qualification = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Qualifications.ToList();
            if (!String.IsNullOrEmpty(Qualification))
                query = query.Where(e => e.Qualification1.ToLower().Contains(Qualification.ToLower())).ToList();

            totalcount = query.Count;
            var Qualifications = new PagedList<KSModel.Models.Qualification>(query, PageIndex, PageSize);
            return Qualifications;
        }

        public void InsertQualification(KSModel.Models.Qualification Qualification)
        {
            if (Qualification == null)
                throw new ArgumentNullException("Qualification");

            db.Qualifications.Add(Qualification);
            db.SaveChanges();
        }

        public void UpdateQualification(KSModel.Models.Qualification Qualification)
        {
            if (Qualification == null)
                throw new ArgumentNullException("Qualification");

            db.Entry(Qualification).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteQualification(int QualificationId = 0)
        {
            if (QualificationId == 0)
                throw new ArgumentNullException("Qualification");

            var Qualification = (from s in db.Qualifications where s.QualificationId == QualificationId select s).FirstOrDefault();
            db.Qualifications.Remove(Qualification);
            db.SaveChanges();
        }

        #endregion

        #region Occupation

        public KSModel.Models.Occupation GetOccupationById(int OccupationId)
        {
            if (OccupationId == 0)
                throw new ArgumentNullException("Occupation");

            var Occupation = (from s in db.Occupations.AsNoTracking() where s.OccupationId == OccupationId select s).FirstOrDefault();
            return Occupation;
        }

        public List<KSModel.Models.Occupation> GetAllOccupations(ref int totalcount, string Occupation = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Occupations.ToList();
            if (!String.IsNullOrEmpty(Occupation))
                query = query.Where(e => e.Occupation1.ToLower().Contains(Occupation.ToLower())).ToList();

            totalcount = query.Count;
            var Occupations = new PagedList<KSModel.Models.Occupation>(query, PageIndex, PageSize);
            return Occupations;
        }

        public void InsertOccupation(KSModel.Models.Occupation Occupation)
        {
            if (Occupation == null)
                throw new ArgumentNullException("Occupation");

            db.Occupations.Add(Occupation);
            db.SaveChanges();
        }

        public void UpdateOccupation(KSModel.Models.Occupation Occupation)
        {
            if (Occupation == null)
                throw new ArgumentNullException("Occupation");

            db.Entry(Occupation).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteOccupation(int OccupationId = 0)
        {
            if (OccupationId == 0)
                throw new ArgumentNullException("Occupation");

            var Occupation = (from s in db.Occupations where s.OccupationId == OccupationId select s).FirstOrDefault();
            db.Occupations.Remove(Occupation);
            db.SaveChanges();
        }

        #endregion

        #region ClassMaster

        public KSModel.Models.ClassMaster GetClassMasterById(int ClassMasterId, bool istrack = false)
        {
            if (ClassMasterId == 0)
                throw new ArgumentNullException("ClassMaster");
            var ClassMaster = new ClassMaster();
            if (istrack)
                ClassMaster = (from s in db.ClassMasters where s.ClassId == ClassMasterId select s).FirstOrDefault();
            else
                ClassMaster = (from s in db.ClassMasters.AsNoTracking() where s.ClassId == ClassMasterId select s).FirstOrDefault();
            return ClassMaster;
        }
        public List<KSModel.Models.ClassMaster> GetClassMasterByClassName(string ClassName)
        {
            if (String.IsNullOrEmpty(ClassName))
                throw new ArgumentNullException("ClassName");

            var ClassMaster = (from s in db.ClassMasters.AsNoTracking() where s.Class == ClassName select s).ToList();
            return ClassMaster;
        }

        public List<KSModel.Models.ClassMaster> GetAllClassMasters(ref int totalcount, int? StandardId = null,
            int? SectionId = null, string ClassName = "", decimal? StandardIndex = null,int? SortingIndex=null, int PageIndex = 0, int PageSize = int.MaxValue, IEnumerable<Sort> sort = null)
        {
            var query = db.ClassMasters.ToList().AsQueryable().Sort(sort).ToList();
            if (StandardId != null && StandardId > 0)
                query = query.Where(e => e.StandardId == StandardId).ToList();
            if (SectionId != null && SectionId > 0)
                query = query.Where(e => e.SectionId == SectionId).ToList();
            if (ClassName != "" && ClassName != null)
                query = query.Where(e => e.Class == ClassName).ToList();
            if (SortingIndex != null && SortingIndex > 0)
                query = query.Where(e => e.SortingIndex == SortingIndex).ToList();
            if (StandardIndex != null && StandardIndex > 0)
                query = query.Where(e => e.StandardMaster.StandardIndex == StandardIndex).ToList();
            query = query.OrderBy(c => c.StandardMaster.StandardIndex).ToList();

            totalcount = query.Count;
            var ClassMasters = new PagedList<KSModel.Models.ClassMaster>(query, PageIndex, PageSize);
            return ClassMasters;
        }
        public IList<ClassMasterModel> GetAllClassMasters_Sp(ref int totalcount, int? StandardId = null,int sessionId=0,
            int? SectionId = null, string ClassName = "", decimal? StandardIndex = null,int? SortingIndex = null, int PageIndex = 0, int PageSize = int.MaxValue, IEnumerable<Sort> sort = null)
        {
            int cunt = 0;
            if (sessionId == 0)
                sessionId = GetAllSessions(ref cunt).Where(m => m.IsDefualt == true).FirstOrDefault().SessionId;
         
DataTable dt = new DataTable();

            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("spGetClassList", conn))
                {

                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@SessionId", sessionId);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            IList<ClassMasterModel> GetAllClassList = new List<ClassMasterModel>();
            ClassMasterModel ClassModel = new ClassMasterModel();
            var count = 0;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ClassModel = new ClassMasterModel();
                ClassModel.Class = dt.Rows[i]["Class"] != null ? Convert.ToString(dt.Rows[i]["Class"]) : "";
                ClassModel.Standard = dt.Rows[i]["Standard"] != null ? Convert.ToString(dt.Rows[i]["Standard"]) : "";
                ClassModel.Section = dt.Rows[i]["Section"] != null ? Convert.ToString(dt.Rows[i]["Section"]) : "";
                ClassModel.SectionId = dt.Rows[i]["SectionId"] != null ? Convert.ToInt32(dt.Rows[i]["SectionId"]) : 0;
                ClassModel.StudentCount = dt.Rows[i]["StudentCount"] != null ? Convert.ToString(dt.Rows[i]["StudentCount"]) : "";
                ClassModel.ClassId = dt.Rows[i]["ClassId"] != null ? Convert.ToInt32(dt.Rows[i]["ClassId"]) : 0;
                ClassModel.StandardId = dt.Rows[i]["StandardId"] != null ? Convert.ToInt32(dt.Rows[i]["StandardId"]) : 0;
                
                ClassModel.strIndex = dt.Rows[i]["SortingIndex"].ToString();

                if (!string.IsNullOrEmpty(ClassModel.strIndex))
                    ClassModel.Index = Convert.ToInt32(ClassModel.strIndex);

                ClassModel.ClassDeletionFlag = dt.Rows[i]["ClassDeletionFlag"] != null ? Convert.ToBoolean(dt.Rows[i]["ClassDeletionFlag"]) : false;
                count = 0;
                
                ClassModel.IsAllSectionsBound = false;
                var sectionList = GetAllSections(ref count);
                var classmasterList = GetAllClassMasters(ref count);

                var sectionrecordpending = 0;
                foreach (var secl in sectionList)
                {
                    var entryexists = classmasterList.Where(m => m.SectionId == secl.SectionId && m.StandardId == ClassModel.StandardId).ToList();
                    if (entryexists.Count > 0)
                    {

                    }
                    else
                    {
                        sectionrecordpending++;
                    }
                    if (sectionrecordpending > 0)
                    {
                        ClassModel.IsAllSectionsBound = true;
                    }

                }
                //FeeList.ReceiptNo = dt.Rows[i]["ReceiptNo"] != null ? Convert.ToInt32(dt.Rows[i]["ReceiptNo"]) : 0;
                //FeeList.RollNo = dt.Rows[i]["RollNo"] != null ? Convert.ToString(dt.Rows[i]["RollNo"]) : "";
                //FeeList.AdmissionNo = dt.Rows[i]["AdmissionNo"] != null ? dt.Rows[i]["AdmissionNo"].ToString() : "";
                //FeeList.StudentName = dt.Rows[i]["StudentName"] != null ? dt.Rows[i]["StudentName"].ToString() : "";
                //FeeList.IsHostler = dt.Rows[i]["IsHostler"] is DBNull ? false : Convert.ToBoolean((dt.Rows[i]["IsHostler"]));
                //FeeList.RecieptType = dt.Rows[i]["RecieptType"] != null ? dt.Rows[i]["RecieptType"].ToString() : "";
                //FeeList.Class = dt.Rows[i]["Class"] != null ? dt.Rows[i]["Class"].ToString() : "";
                //FeeList.PaymentMethod = dt.Rows[i]["PaymentMethod"] != null ? dt.Rows[i]["PaymentMethod"].ToString() : "";
                //FeeList.ChequeNo = dt.Rows[i]["ChequeNo"] != null ? dt.Rows[i]["ChequeNo"].ToString() : "";
                //FeeList.Details = dt.Rows[i]["Details"] != null ? dt.Rows[i]["Details"].ToString() : "";
                //if (dt.Rows[i]["Amount"] == null)
                //{
                //    FeeList.Amount = 0;
                //}
                //else if (dt.Rows[i]["Amount"].ToString() == "")
                //{
                //    FeeList.Amount = 0;
                //}
                //else
                //{
                //    FeeList.Amount = Convert.ToDecimal(dt.Rows[i]["Amount"].ToString());
                //}
                //FeeList.IsOld = dt.Rows[i]["IsOld"] is DBNull ? false : Convert.ToBoolean((dt.Rows[i]["IsOld"]));

                ClassModel.EncClassId = _ICustomEncryption.base64e(ClassModel.ClassId.ToString());
                GetAllClassList.Add(ClassModel);
            }
            if (GetAllClassList.Count > 0)
            {
                if (StandardId != null && StandardId > 0)
                {
                    GetAllClassList = GetAllClassList.Where(m => m.StandardId == StandardId).ToList();
                }
                if (SectionId != null && SectionId > 0)
                {
                    GetAllClassList = GetAllClassList.Where(m => m.SectionId == SectionId).ToList();
                }
                  if (SortingIndex != null && SortingIndex > 0)
                {
                    GetAllClassList = GetAllClassList.Where(m => m.Index == SortingIndex).ToList();
                }
            }

            //count = GetAllClassList.Count;
            GetAllClassList = GetAllClassList.AsQueryable().Sort(sort).ToList();
            GetAllClassList = new PagedList<ClassMasterModel>(GetAllClassList, PageIndex, PageSize);
            return GetAllClassList;
        }
        public void InsertClassMaster(KSModel.Models.ClassMaster ClassMaster)
        {
            if (ClassMaster == null)
                throw new ArgumentNullException("ClassMaster");

            db.ClassMasters.Add(ClassMaster);
            db.SaveChanges();
        }

        public void UpdateClassMaster(KSModel.Models.ClassMaster ClassMaster)
        {
            if (ClassMaster == null)
                throw new ArgumentNullException("ClassMaster");

            db.Entry(ClassMaster).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteClassMaster(int ClassMasterId = 0)
        {
            if (ClassMasterId == 0)
                throw new ArgumentNullException("ClassMaster");

            var ClassMaster = (from s in db.ClassMasters where s.ClassId == ClassMasterId select s).FirstOrDefault();
            db.ClassMasters.Remove(ClassMaster);
            db.SaveChanges();
        }


        public List<VirtualClass> GetAllVirtualClasses(ref int totalcount, string RoomNo = "", string VirtualClass = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.VirtualClasses.ToList();
            if (RoomNo != null && RoomNo!="")
                query = query.Where(e => e.RoomNo == RoomNo).ToList();
            if (VirtualClass != null && VirtualClass !="")
                query = query.Where(e => e.VirtualClass1 == VirtualClass).ToList();

            query = query.OrderBy(c => c.VirtualClassId).ToList();

            totalcount = query.Count;
            var VirtualClasss = new PagedList<KSModel.Models.VirtualClass>(query, PageIndex, PageSize);
            return VirtualClasss;
        }

        public List<VirtualClassBinding> GetAllVirtualClassBindings(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.VirtualClassBindings.ToList();
          
            totalcount = query.Count;
            var VirtualClassBindings = new PagedList<KSModel.Models.VirtualClassBinding>(query, PageIndex, PageSize);
            return VirtualClassBindings;
        }

        public void InsertVirtualClassBindings(KSModel.Models.VirtualClassBinding VirtualClassBinding)
        {
            if (VirtualClassBinding == null)
                throw new ArgumentNullException("VirtualClassBinding");

            db.VirtualClassBindings.Add(VirtualClassBinding);
            db.SaveChanges();
        }

        public void UpdateVirtualClassBinding(VirtualClassBinding VirtualClassBinding)
        {
            if (VirtualClassBinding == null)
                throw new ArgumentNullException("VirtualClassBinding");

            db.Entry(VirtualClassBinding).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteVirtualClassBinding(int VirtualClassBindingId = 0)
        {
            if (VirtualClassBindingId == 0)
                throw new ArgumentNullException("VirtualClassBinding");

            var VirtualClassBinding = (from s in db.VirtualClassBindings where s.VirtualClassBindingId == VirtualClassBindingId select s).FirstOrDefault();
            db.VirtualClassBindings.Remove(VirtualClassBinding);
            db.SaveChanges();
        }



        public List<VirtualClassStudentId> GetAllVirtualClassStudents(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.VirtualClassStudentIds.ToList();

            totalcount = query.Count;
            var GetAllVirtualClassStudents = new PagedList<KSModel.Models.VirtualClassStudentId>(query, PageIndex, PageSize);
            return GetAllVirtualClassStudents;
        }

        public void InsertVirtualClassStudents(VirtualClassStudentId VirtualClassStudent)
        {
            if (VirtualClassStudent == null)
                throw new ArgumentNullException("VirtualClassStudent");

            db.VirtualClassStudentIds.Add(VirtualClassStudent);
            db.SaveChanges();
        }

        public void UpdateVirtualClassStudents(VirtualClassStudentId VirtualClassStudent)
        {
            if (VirtualClassStudent == null)
                throw new ArgumentNullException("VirtualClassStudentId");

            db.Entry(VirtualClassStudent).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteVirtualClassStudents(int VirtualClassStudentId = 0)
        {
            if (VirtualClassStudentId == 0)
                throw new ArgumentNullException("VirtualClassStudentId");

            var VirtualClassStudent = (from s in db.VirtualClassStudentIds where s.VirtualClassStudentId1 == VirtualClassStudentId select s).FirstOrDefault();
            db.VirtualClassStudentIds.Remove(VirtualClassStudent);
            db.SaveChanges();
        }



        public List<VirtualClassSession> GetAllVirtualClassSessions(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.VirtualClassSessions.ToList();

            totalcount = query.Count;
            var VirtualClassSession = new PagedList<KSModel.Models.VirtualClassSession>(query, PageIndex, PageSize);
            return VirtualClassSession;
        }

        public void InsertVirtualClassSession(VirtualClassSession VirtualClassSession)
        {
            if (VirtualClassSession == null)
                throw new ArgumentNullException("VirtualClassSession");

            db.VirtualClassSessions.Add(VirtualClassSession);
            db.SaveChanges();
        }

        public void UpdateVirtualClassSession(VirtualClassSession VirtualClassSession)
        {
            if (VirtualClassSession == null)
                throw new ArgumentNullException("VirtualClassSession");

            db.Entry(VirtualClassSession).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteVirtualClassSession(int VirtualClassSessionId = 0)
        {
            if (VirtualClassSessionId == 0)
                throw new ArgumentNullException("VirtualClassSession");

            var VirtualClassSession = (from s in db.VirtualClassSessions where s.VirtualClassSessionId == VirtualClassSessionId select s).FirstOrDefault();
            db.VirtualClassSessions.Remove(VirtualClassSession);
            db.SaveChanges();
        }

        #endregion

        #region House

        public KSModel.Models.House GetHouseById(int HouseId)
        {
            if (HouseId == 0)
                throw new ArgumentNullException("FeeClassGroup");

            var House = (from s in db.Houses.AsNoTracking() where s.HouseId == HouseId select s).FirstOrDefault();
            return House;
        }

        public List<KSModel.Models.House> GetAllHouses(ref int totalcount, string House = "", string HouseColour = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Houses.ToList();
            if (!String.IsNullOrEmpty(House))
                query = query.Where(e => e.House1.ToLower().Contains(House.ToLower())).ToList();

            totalcount = query.Count;
            var Houses = new PagedList<KSModel.Models.House>(query, PageIndex, PageSize);
            return Houses;
        }

        public void InsertHouse(KSModel.Models.House House)
        {
            if (House == null)
                throw new ArgumentNullException("House");

            db.Houses.Add(House);
            db.SaveChanges();
        }

        public void UpdateHouse(KSModel.Models.House House)
        {
            if (House == null)
                throw new ArgumentNullException("House");

            db.Entry(House).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteHouse(int HouseId = 0)
        {
            if (HouseId == 0)
                throw new ArgumentNullException("House");

            var House = (from s in db.Houses where s.HouseId == HouseId select s).FirstOrDefault();
            db.Houses.Remove(House);
            db.SaveChanges();
        }

        #endregion

        #region religion

        public Religion GetReligionById(int ReligionId)
        {
            if (ReligionId == 0)
                throw new ArgumentNullException("Religion");

            var Religion = (from s in db.Religions.AsNoTracking() where s.ReligionId == ReligionId select s).FirstOrDefault();
            return Religion;
        }

        public List<Religion> GetAllReligions(ref int totalcount, string Religion = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Religions.ToList();
            if (!String.IsNullOrEmpty(Religion))
                query = query.Where(e => e.Religion1.ToLower().Contains(Religion.ToLower())).ToList();

            totalcount = query.Count;
            var Religions = new PagedList<KSModel.Models.Religion>(query, PageIndex, PageSize);
            return Religions;
        }

        public void InsertReligion(Religion Religion)
        {
            if (Religion == null)
                throw new ArgumentNullException("Religion");

            db.Religions.Add(Religion);
            db.SaveChanges();
        }

        public void UpdateReligion(Religion Religion)
        {
            if (Religion == null)
                throw new ArgumentNullException("Religion");

            db.Entry(Religion).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteReligion(int ReligionId = 0)
        {
            if (ReligionId == 0)
                throw new ArgumentNullException("Religion");

            var Religion = (from s in db.Religions where s.ReligionId == ReligionId select s).FirstOrDefault();
            db.Religions.Remove(Religion);
            db.SaveChanges();
        }

        #endregion

        #region Language

        public KSModel.Models.Language GetLanguageById(int LanguageId)
        {
            if (LanguageId == 0)
                throw new ArgumentNullException("Language");

            var Language = (from s in db.Languages.AsNoTracking() where s.LanguageId == LanguageId select s).FirstOrDefault();
            return Language;
        }

        public List<KSModel.Models.Language> GetAllLanguages(ref int totalcount, string Language = "", int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.Languages.ToList();
            if (!String.IsNullOrEmpty(Language))
                query = query.Where(e => e.Language1.ToLower().Contains(Language.ToLower())).ToList();

            totalcount = query.Count;
            var Languages = new PagedList<KSModel.Models.Language>(query, PageIndex, PageSize);
            return Languages;
        }

        public void InsertLanguage(KSModel.Models.Language Language)
        {
            if (Language == null)
                throw new ArgumentNullException("Language");

            db.Languages.Add(Language);
            db.SaveChanges();
        }

        public void UpdateLanguage(KSModel.Models.Language Language)
        {
            if (Language == null)
                throw new ArgumentNullException("Language");

            db.Entry(Language).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteLanguage(int LanguageId = 0)
        {
            if (LanguageId == 0)
                throw new ArgumentNullException("Language");

            var Language = (from s in db.Languages where s.LanguageId == LanguageId select s).FirstOrDefault();
            db.Languages.Remove(Language);
            db.SaveChanges();
        }

        #endregion

        #region CommonText

        public CommonText GetCommonTextById(int CommonTextId)
        {
            if (CommonTextId == 0)
                throw new ArgumentNullException("CommonText");

            var CommonText = (from s in db.CommonTexts.AsNoTracking() where s.CommonTextId == CommonTextId select s).FirstOrDefault();
            return CommonText;
        }

        public List<CommonText> GetAllCommonTexts(ref int totalcount, string FormName = "", string TextFieldName = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.CommonTexts.ToList();
            if (!String.IsNullOrEmpty(FormName))
                query = query.Where(e => e.FormName.ToLower().Contains(FormName.ToLower())).ToList();
            if (!String.IsNullOrEmpty(TextFieldName))
                query = query.Where(e => e.TextFieldName.ToLower().Contains(TextFieldName.ToLower())).ToList();

            totalcount = query.Count;
            var CommonTexts = new PagedList<KSModel.Models.CommonText>(query, PageIndex, PageSize);
            return CommonTexts;
        }

        public void InsertCommonText(CommonText CommonText)
        {
            if (CommonText == null)
                throw new ArgumentNullException("CommonText");

            db.CommonTexts.Add(CommonText);
            db.SaveChanges();
        }

        public void UpdateCommonText(CommonText CommonText)
        {
            if (CommonText == null)
                throw new ArgumentNullException("CommonText");

            db.Entry(CommonText).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteCommonText(int CommonTextId = 0)
        {
            if (CommonTextId == 0)
                throw new ArgumentNullException("CommonText");

            var CommonText = (from s in db.CommonTexts where s.CommonTextId == CommonTextId select s).FirstOrDefault();
            db.CommonTexts.Remove(CommonText);
            db.SaveChanges();
        }

        #endregion

        #endregion

        #region
        public KSModel.Models.StudentCategory GetStudentCategoryById(int StudentCategoryId)
        {
            if (StudentCategoryId == 0)
                throw new ArgumentNullException("StudentCategory");

            var StudentCategory = (from s in db.StudentCategories.AsNoTracking() where s.StudentCategoryId == StudentCategoryId select s).FirstOrDefault();
            return StudentCategory;
        }
        #endregion


        #region CommonMethods
        public string TrimStudentName(Student stu)
        {
            string Studentname = "";
            if (!string.IsNullOrEmpty(stu.FName))
                stu.FName = stu.FName.Trim();

            if (!string.IsNullOrEmpty(stu.LName))
                stu.LName = stu.LName.Trim();

            Studentname = stu.FName + " " + stu.LName;
            return Studentname;
        }

        public IList<DateTime> GetMonthCountBetweenDates(ref int Count, DateTime StartDate, DateTime EndDate)
        {
            IList<DateTime> MonthList = new List<DateTime>();
            int months = (EndDate.Year - StartDate.Year) * 12 + EndDate.Month - StartDate.Month;
            for (var i = 0; i <= months; i++)
            {
                Count++;
                var testmonth = StartDate.AddMonths(i);
                MonthList.Add(testmonth);
            }
            return MonthList;
        }

        public IList<DateTime> GetSundayCountBetweenDates(ref int Count, DateTime StartDate, DateTime EndDate)
        {
            IList<DateTime> SundayList = new List<DateTime>();

            TimeSpan diff = EndDate - StartDate;
            int days = diff.Days;
            for (var i = 0; i <= days; i++)
            {
                var testDate = StartDate.AddDays(i);
                switch (testDate.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        Count++;
                        SundayList.Add(testDate);
                        break;
                }
            }

            return SundayList;
        }
        #endregion
    }
}