﻿//using BAL.BAL.SPClasses;
using KSModel.Models;
using ViewModel.ViewModel.Library;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.ViewModel.Fee;

namespace DAL.DAL.LibraryModule
{
    public partial interface IBookMasterService
    {
        #region BookMaster

        BookMaster GetBookMasterById(int? BookId, bool IsTrack = false);

        List<BookMaster> GetAllBookMasters(ref int totalcount, string BookTitle = "", string ISBN = "", string BookAbbr = "",
                              int? MaterialTypeId = null, string SubTitle = "", int PageIndex = 0, int PageSize = int.MaxValue,
                               string MaterialType = "", bool Isfilter = false);

        List<BookAdditionalInfoViewModel> GetAllBookMastersBySp(ref int totalcount, string BookTitle = "", string ISBN = "", string BookAbbr = "",
                     int MaterialTypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue, int BookId = 0, string UniqueId = "",
           string LUI = "", string AccessionNo = "");

         List<BookDefaulterList> SeacrhAllBookMastersBySp(ref int totalcount, string Searchtext = "", int PageIndex = 0, int PageSize = int.MaxValue);

        List<BookAdditionalInfoViewModel> GetAllBookMastersByLinq(ref int totalcount, string BookTitle = "", string ISBN = "", string BookAbbr = "",
                      int MaterialTypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue, int BookId = 0, string UniqueId = "",
            string LUI = "", string AccessionNo = "");

        dynamic GetAllBooksGridData(ref int totalcount, string BookTitle = "", string ISBN = "", string BookAbbr = "",
                      int MaterialTypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue, int BookId = 0, string UniqueId = "",
            string LUI = "", string AccessionNo = "");

        List<BookDefaulterList> AvancedSearchBookMasters(ref int totalcount, string SubTitle = "", string ISBN = "", string MaterialType = "",
             string AttributeValues = "", string Attribute = "",string AdvancedSearch="", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertBookMaster(BookMaster BookMaster);

        void UpdateBookMaster(BookMaster BookMaster);

        void DeleteBookMaster(int BookId = 0);

        #endregion

        #region BookMaterialType

        BookMaterialType GetBookMaterialTypeById(int? MaterialTypeId, bool IsTrack = false);

        List<BookMaterialType> GetAllBookMaterialTypes(ref int totalcount, string MaterialType = "",
                                    int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);

        void InsertBookMaterialType(BookMaterialType BookMaterialType);

        void UpdateBookMaterialType(BookMaterialType BookMaterialType);

        void DeleteBookMaterialType(int MaterialTypeId = 0);

        #endregion

        #region BookMasterAttribute

        BookMasterAttribute GetBookMasterAttributeById(int? BookMasterAttributeId, bool IsTrack = false);

        List<BookMasterAttribute> GetAllBookMasterAttributes(ref int totalcount, string MasterAttribute = "", int? AttrMaxLength = null,
                                                  bool? IsActive = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);

        void InsertBookMasterAttribute(BookMasterAttribute BookMasterAttribute);

        void UpdateBookMasterAttribute(BookMasterAttribute BookMasterAttribute);

        void DeleteBookMasterAttribute(int BookMasterAttributeId = 0);

        #endregion

        #region BookMasterData

        BookMasterData GetBookDataById(int? BookMasterDataId, bool IsTrack = false);

        List<BookMasterData> GetAllBookData(ref int totalcount, int? BookMasterAttributeId = null, string AttributeValue = "",
                                           bool? IsActive = false, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);

        void InsertBookData(BookMasterData BookMasterData);

        void UpdateBookData(BookMasterData BookMasterData);

        void DeleteBookData(int BookMasterDataId = 0);

        #endregion

        #region BookMasterDetail

        BookMasterDetail GetBookDetailById(int? BookMasterDetailId, bool IsTrack = false);

        List<BookMasterDetail> GetAllBookDetails(ref int totalcount, int? BookMasterDataId = null,
                                            int? BookId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        DataSet GetAllBookDetColumns(ref int totalcount, bool ShowCopies = false);

        void InsertBookDetail(BookMasterDetail BookMasterDetail);

        void UpdateBookDetail(BookMasterDetail BookMasterDetail);

        void DeleteBookDetail(int BookMasterDetailId = 0);

        void DeleteBookDetailByBookId(int BookId = 0);

        #endregion

        #region BookAttribute

        BookAttribute GetBookAttributeById(int? BookAttributeId, bool IsTrack = false);

        List<BookAttribute> GetAllBookAttributes(ref int totalcount, string Attribute = "", int? AttrMaxLength = null
                                , bool? IsActive = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);

        void InsertBookAttribute(BookAttribute BookAttribute);

        void UpdateBookAttribute(BookAttribute BookAttribute);

        void DeleteBookAttribute(int BookId = 0);

        #endregion

        #region BookAttributeValue

        BookAttributeValue GetBookAttributeValueById(int? BookAttributeValueId, bool IsTrack = false);

        List<BookAttributeValue> GetAllBookAttributeValues(ref int totalcount, string AttributeValue = "", int? BookAttributeId = null
                                          , int? BookId = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);

        void InsertBookAttributeValue(BookAttributeValue BookAttributeValue);

        void UpdateBookAttributeValue(BookAttributeValue BookAttributeValue);

        void DeleteBookAttributeValueByBookId(int BookId = 0);

        void DeleteBookAttributeValue(int BookAttributeValueId = 0);

        #endregion

        #region BookEdition

        BookEdition GetBookEditionById(int? BookEditionId, bool IsTrack = false);

        List<BookEdition> GetAllBookEditions(ref int totalcount, string Edition = "", string Volume = "", int? PageNo=null, int? BookId = null,
                                    DateTime? PublishingDate = null, int? BookSellerId = null, int PageIndex = 0,
                                    int PageSize = int.MaxValue, bool Isfilter = false);

        void InsertBookEdition(BookEdition BookEdition);

        void UpdateBookEdition(BookEdition BookEdition);

        void DeleteBookEdition(int BookEditionId = 0);

        #endregion

        #region BookSeller

        BookSeller GetBookSellerById(int? BookSellerId, bool IsTrack = false);

        List<BookSeller> GetAllBookSellers(ref int totalcount, string BookSellerName = "", int? CityId = null,
                                    int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);

        void InsertBookSeller(BookSeller BookSeller);

        void UpdateBookSeller(BookSeller BookSeller);

        void DeleteBookSeller(int BookEditionId = 0);

        #endregion

        #region BookIdentity

        BookIdentity GetBookIdentityById(int? BookIdentityId, bool IsTrack = false);

        List<BookIdentity> GetAllBookIdentities(ref int totalcount, int? BookId = null, int BookSellerId = 0, string UniqueId = "",
                           DateTime? EntryDate = null, string BarCode = "", string LUI = "", int? BookEditionId = null, int? LocationId = null
                           , int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false,bool IsLost=false, bool IsInactive = false);

        List<KSModel.Models.BookIdentity> GetAllBookIdentitiesINTransactions(ref int totalcount, int? BookId = null
                                                         , int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertBookIdentity(BookIdentity BookIdentity);

        int GetBookUniqueId(int? BookId);

        void UpdateBookIdentity(BookIdentity BookIdentity);

        void DeleteBookIdentity(int BookIdentityId = 0);

        #endregion

        #region BookLocationAttribute

        BookLocationAttribute GetBookLocationAttributeById(int? BookLocationAttributeId, bool IsTrack = false);

        List<BookLocationAttribute> GetAllBookLocationAttributes(ref int totalcount, int? AttrMaxLength = null, string LocationAttribute = "",
                                     bool? IsActive = null, int? SortingIndex = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);

        void InsertBookLocationAttribute(BookLocationAttribute BookLocationAttribute);

        void UpdateBookLocationAttribute(BookLocationAttribute BookLocationAttribute);

        void DeleteBookLocationAttribute(int BookLocationAttributeId = 0);

        #endregion

        #region BookLocationData

        BookLocationData GetBookLocationDataById(int? BookLocationDataId, bool IsTrack = false);

        List<BookLocationData> GetAllBookLocationDatas(ref int totalcount, int? BookLocationAttributeId = null, string AttributeValue = "",
                                     bool? IsActive = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);

        void InsertBookLocationData(BookLocationData BookLocationData);

        void UpdateBookLocationData(BookLocationData BookLocationData);

        void DeleteBookLocationData(int BookLocationDataId = 0);

        #endregion

        #region BookLocationDetail

        BookLocationDetail GetBookLocationDetailById(int? BookLocationDetailId, bool IsTrack = false);

        List<BookLocationDetail> GetAllBookLocationDetails(ref int totalcount, int? BookLocationDataId = null,
            int? BookIdentityId = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);

        void InsertBookLocationDetail(BookLocationDetail BookLocationDetail);

        void UpdateBookLocationDetail(BookLocationDetail BookLocationDetail);

        void DeleteBookLocationDetail(int BookLocationDetailId = 0);

        void DeleteBookLocationDetailByIdentityId(int BookIdentityId = 0);

        #endregion

        #region BookTransaction

        BookTransaction GetBookTransactionById(int? BookTransactionId, bool IsTrack = false);

        List<BookTransaction> GetAllBookTransactions(ref int totalcount, DateTime? TransactionDate = null,
            int? BookMemberTypeId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        List<BookTransaction> GetAllIssuedBooks(ref int totalcount, int? BookTrsTypeId = null, DateTime? TransactionDate = null,
           int? BookMemberTypeId = null, string SearchText = "", DateTime? ToDateFilter = null, DateTime? FromDateFilter = null, int? DateFilterId = null,
            int? BookId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertBookTransaction(BookTransaction BookTransaction);

        void UpdateBookTransaction(BookTransaction BookTransaction);

        void DeleteBookTransaction(int BookTransactionId = 0);

        #endregion

        #region BookTransactionType

        BookTrsType GetBookTrsTypeById(int? BookTrsTypeId, bool IsTrack = false);

        List<BookTrsType> GetAllBookTrsTypes(ref int totalcount, string BookTrsType = "",
                                            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertBookTrsType(BookTrsType BookTransactionType);

        void UpdateBookTrsType(BookTrsType BookTransactionType);

        void DeleteBookTrsType(int BookTrsTypeId = 0);

        #endregion

        #region BookMemberType

        BookMemberType GetBookMemberTypeById(int? BookMemberTypeId, bool IsTrack = false);

        List<BookMemberType> GetAllBookMemberTypes(ref int totalcount, string BookMemberType = "", int? AllowedDays = null,
                                   bool? IsActive = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertBookMemberType(BookMemberType BookMemberType);

        void UpdateBookMemberType(BookMemberType BookMemberType);

        void DeleteBookMemberType(int BookMemberTypeId = 0);

        #endregion

        #region BookStatus

        BookStatu GetBookStatusById(int? BookStatusId, bool IsTrack = false);
        

        List<BookStatu> GetAllBookStatuss(ref int totalcount, string BookStatus = "", int? AllowedDays = null,
                                  int PageIndex = 0, int PageSize = int.MaxValue);
        
        void InsertBookStatus(BookStatu BookStatus);

        void UpdateBookStatus(BookStatu BookStatus);

        void DeleteBookStatus(int BookStatusId = 0);

        #endregion
    }
}
