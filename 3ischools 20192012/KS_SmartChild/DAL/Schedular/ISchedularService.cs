﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.Schedular
{
   public partial interface ISchedularService
    {
        #region Event
        Event GetEventById(int EventId, bool IsTrack = false);

        IList<Event> GetAllEventList(ref int totalcount,
            string EventTitle = "", int? ClassId = null, bool IsAllSchool = false,  DateTime? StartDate = null,
            DateTime? EndDate = null,
            TimeSpan? StartTime = null, TimeSpan? EndTime = null,
             string Description = "", bool IsActive = false, bool IsDeleted = false, string Venue = "",
            int? EventTypeId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertEvent(Event Event);

        void UpdateEvent(Event Event);

        void DeleteEvent(int EventId = 0);

        #endregion

        #region Event Type
        EventType GetEventTypeById(int EventTypeId, bool IsTrack = false);

        IList<EventType> GetAllEventTypeList(ref int totalcount,
            string EventType = "",string EventCode="", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertEventType(EventType EventType);

        void UpdateEventType(EventType EventType);

        void DeleteEventType(int EventTypeId = 0);
        #endregion

        #region EventCols
        EventCol GetEventColsId(int EventColsId, bool IsTrack = false);

        IList<EventCol> GetAllEventColsList(ref int totalcount,
            string ColName = "", int? EventTypeId = null, bool IsMandatory = false, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertEventCol(EventCol EventCol);

        void UpdateEventCol(EventCol EventCol);

        void DeleteEventCol(int EventColsId = 0);

        #endregion

        #region Event Image
        EventImage GetEventImageById(int EventImageId, bool IsTrack = false);

        IList<EventImage> GetAllEventImageList(ref int totalcount,
            string ImageCaption = "", string ImageName = "", int? EventId = null, int PageIndex = 0, int PageSize = int.MaxValue);
       
        void InsertEventImage(EventImage EventImage);

        void UpdateEventImage(EventImage EventImage);

        void DeleteEventImage(int EventImageId = 0);
        #endregion

        #region EventDetail
        KSModel.Models.EventDetail GetEventDetail(int EventDetailId, bool IsTrack = false);

        IList<KSModel.Models.EventDetail> GetAllEventDetailList(ref int totalcount, int? EventId = null, int? ClassId = null,
             int PageIndex = 0, int PageSize = int.MaxValue);
       
        void InsertEventDetail(KSModel.Models.EventDetail EventDetail);

        void UpdateEventDetail(KSModel.Models.EventDetail EventDetail);

        void DeleteEventDetail(int EventDetailId = 0);
        #endregion

        #region EventCircularDoc
        KSModel.Models.EventCircularDoc GetEventCircularDoc(int CircularDocId, bool IsTrack = false);

        IList<KSModel.Models.EventCircularDoc> GetAllEventCircularDocs(ref int totalcount, int? EventId = null, string FileName = "",
                                                                int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertEventCircularDoc(KSModel.Models.EventCircularDoc EventCircularDoc);

        void UpdateEventCircularDoc(KSModel.Models.EventCircularDoc EventCircularDoc);

        void DeleteEventCircularDoc(int CircularDocId = 0);
        #endregion
    }
}
