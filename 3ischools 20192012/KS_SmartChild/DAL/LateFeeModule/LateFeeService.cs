﻿using KS_SmartChild.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace KS_SmartChild.DAL.LateFeeModule
{
    public partial class LateFeeService : ILateFeeService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public LateFeeService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods

        #region LateFeeType

        public LateFeeType GetLateFeeTypeById(int LateFeeTypeId)
        {
            if (LateFeeTypeId == 0)
                throw new ArgumentNullException("LateFeeType");

            var LateFeeType = (from s in db.LateFeeTypes.AsNoTracking() where s.LateFeeTypeId == LateFeeTypeId select s).FirstOrDefault();
            return LateFeeType;
        }

        public List<LateFeeType> GetAllLateFeeTypes(ref int totalcount, string LateFeeType = "", int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.LateFeeTypes.ToList();
            if (!String.IsNullOrEmpty(LateFeeType))
                query = query.Where(e => e.LateFeeType1.ToLower().Contains(LateFeeType.ToLower())).ToList();

            totalcount = query.Count;
            var LateFeeTypes = new PagedList<KSModel.Models.LateFeeType>(query, PageIndex, PageSize);
            return LateFeeTypes;
        }

        public void InsertLateFeeType(LateFeeType LateFeeType)
        {
            if (LateFeeType == null)
                throw new ArgumentNullException("LateFeeType");

            db.LateFeeTypes.Add(LateFeeType);
            db.SaveChanges();
        }

        public void UpdateLateFeeType(LateFeeType LateFeeType)
        {
            if (LateFeeType == null)
                throw new ArgumentNullException("LateFeeType");

            db.Entry(LateFeeType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteLateFeeType(int LateFeeTypeId = 0)
        {
            if (LateFeeTypeId == 0)
                throw new ArgumentNullException("LateFeeType");

            var LateFeeType = (from s in db.LateFeeTypes where s.LateFeeTypeId == LateFeeTypeId select s).FirstOrDefault();
            db.LateFeeTypes.Remove(LateFeeType);
            db.SaveChanges();
        }

        #endregion

        #region LateFee


        public LateFee GetLateFeeById(int LateFeeId, bool IsTrack = false)
        {
            if (LateFeeId == 0)
                throw new ArgumentNullException("LateFee");
            var LateFee = new LateFee();
            if (IsTrack)
                LateFee = (from s in db.LateFees where s.LateFeeId == LateFeeId select s).FirstOrDefault();
            else
                LateFee = (from s in db.LateFees.AsNoTracking() where s.LateFeeId == LateFeeId select s).FirstOrDefault();
            return LateFee;
        }

        public List<LateFee> GetAllLateFees(ref int totalcount, int? LateFeeTypeId = null, decimal? Fromdays = null,
            decimal? Todays = null, decimal? FeeAmt = null, int? ReceiptTypeId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.LateFees.ToList();
            if (LateFeeTypeId != null && LateFeeTypeId > 0)
                query = query.Where(e => e.LateFeeTypeId == LateFeeTypeId).ToList();
            if (Fromdays != null && Fromdays > 0)
                query = query.Where(e => e.FromDays == Fromdays).ToList();
            if (Todays != null && Todays > 0)
                query = query.Where(e => e.ToDays == Todays).ToList();
            if (FeeAmt != null && FeeAmt > 0)
                query = query.Where(e => e.LateFeeAmount == FeeAmt).ToList();
            if (ReceiptTypeId != null && ReceiptTypeId > 0)
                query = query.Where(x => x.ReceiptTypeId == ReceiptTypeId).ToList();

            totalcount = query.Count;
            var LateFees = new PagedList<KSModel.Models.LateFee>(query, PageIndex, PageSize);
            return LateFees;
        }

        public void InsertLateFee(LateFee LateFee)
        {
            if (LateFee == null)
                throw new ArgumentNullException("LateFee");

            db.LateFees.Add(LateFee);
            db.SaveChanges();
        }

        public void UpdateLateFee(LateFee LateFee)
        {
            if (LateFee == null)
                throw new ArgumentNullException("LateFee");

            db.Entry(LateFee).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteLateFee(int LateFeeId = 0)
        {
            if (LateFeeId == 0)
                throw new ArgumentNullException("LateFee");

            var LateFee = (from s in db.LateFees where s.LateFeeId == LateFeeId select s).FirstOrDefault();
            db.LateFees.Remove(LateFee);
            db.SaveChanges();
        }

        #endregion

        #endregion
    }
}