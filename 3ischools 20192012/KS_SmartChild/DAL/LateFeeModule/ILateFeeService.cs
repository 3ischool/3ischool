﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.LateFeeModule
{
    public partial interface ILateFeeService
    {
        #region LateFeeType

        LateFeeType GetLateFeeTypeById(int LateFeeTypeId);

        List<LateFeeType> GetAllLateFeeTypes(ref int totalcount, string LateFeeType = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertLateFeeType(LateFeeType LateFeeType);

        void UpdateLateFeeType(LateFeeType LateFeeType);

        void DeleteLateFeeType(int LateFeeTypeId = 0);

        #endregion

        #region LateFee

        LateFee GetLateFeeById(int LateFeeId, bool IsTrack = false);

        List<LateFee> GetAllLateFees(ref int totalcount, int? LateFeeTypeId = null, decimal? Fromdays = null, decimal? Todays = null,
            decimal? FeeAmt = null, int? ReceiptTypeId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertLateFee(LateFee LateFee);

        void UpdateLateFee(LateFee LateFee);

        void DeleteLateFee(int LateFeeId = 0);

        #endregion

    }
}
