﻿using KS_SmartChild.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.DAL.SettingService
{
    public partial class SchoolSettingService : ISchoolSettingService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public SchoolSettingService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region SchoolSetting

        public void InsertSchoolSetting(SchoolSetting SchoolSetting)
        {
            if (SchoolSetting == null)
                throw new ArgumentNullException("SchoolSetting");

            db.SchoolSettings.Add(SchoolSetting);
            db.SaveChanges();
        }

        public void UpdateSchoolSetting(SchoolSetting SchoolSetting)
        {
            if (SchoolSetting == null)
                throw new ArgumentNullException("SchoolSetting");

            db.Entry(SchoolSetting).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public SchoolSetting GetSchoolSettingByAttribute(string AttributeName)
        {
            if (string.IsNullOrEmpty(AttributeName))
                throw new ArgumentNullException("SchoolSetting");

            var query = db.SchoolSettings.ToList();
            var attribute = query.Where(s => s.AttributeName.ToLower() == AttributeName.ToLower()).FirstOrDefault();
            if (attribute != null)
                return attribute;
            else
                return null;
        }

        public string GetAttributeValue(string AttributeName)
        {
            if (string.IsNullOrEmpty(AttributeName))
                throw new ArgumentNullException("SchoolSetting");

            string Attributeresult = string.Empty;
            var query = db.SchoolSettings.ToList();
            var attribute = query.Where(s => s.AttributeName.ToLower() == AttributeName.ToLower()).FirstOrDefault();
            if (attribute != null)
                Attributeresult = attribute.AttributeValue;

            return Attributeresult;
        }

        public SchoolSetting GetorSetSchoolData(string AttrName, string AttrValue, int? AppFormId = null, string Desc = "")
        {
            // Custom Object 
            SchoolSetting GlobalSettingDataObj = new SchoolSetting();

            string Description = "";
            if (!string.IsNullOrEmpty(Desc))
                Description = Desc;
            else
                Description = AttrName;

            var IsAttrExist = db.SchoolSettings.Where(g => g.AttributeName.ToLower() == AttrName.ToLower());
            if (IsAttrExist.Count() > 0) // if exist return
            {
                GlobalSettingDataObj.AttributeName = IsAttrExist.FirstOrDefault().AttributeName;
                GlobalSettingDataObj.AttributeValue = IsAttrExist.FirstOrDefault().AttributeValue;
            }
            else // Firstly Insert , then return
            {
                SchoolSetting GlobalSettingObj = new SchoolSetting();
                GlobalSettingObj.AttributeName = AttrName;
                GlobalSettingObj.AttributeValue = AttrValue;
                GlobalSettingObj.Description = Description;
                GlobalSettingObj.AppFormId = AppFormId;

                db.SchoolSettings.Add(GlobalSettingObj);
                db.SaveChanges();

                GlobalSettingDataObj.AttributeName = GlobalSettingObj.AttributeName;
                GlobalSettingDataObj.AttributeValue = GlobalSettingObj.AttributeValue;
            }

            return GlobalSettingDataObj;
        }


        public SchoolSetting GetSetUpdateSchoolData(string AttrName, string AttrValue, int? AppFormId = null, string Desc = "")
        {
            // Custom Object 
            SchoolSetting GlobalSettingDataObj = new SchoolSetting();

            string Description = "";
            if (!string.IsNullOrEmpty(Desc))
                Description = Desc;
            else
                Description = AttrName;

            var IsAttrExist = db.SchoolSettings.Where(g => g.AttributeName.ToLower() == AttrName.ToLower());
            if (IsAttrExist.Count() > 0) // if exist return
            {
                var getVal = db.SchoolSettings.Where(x => x.SchoolSettingid == IsAttrExist.FirstOrDefault().SchoolSettingid).FirstOrDefault();
                if (getVal != null)
                {
                    getVal.AttributeName = AttrName;
                    getVal.AttributeValue = AttrValue;
                    getVal.Description = Desc;
                    db.Entry(getVal).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();

                    GlobalSettingDataObj.AttributeName = getVal.AttributeName;
                    GlobalSettingDataObj.AttributeValue = getVal.AttributeValue;
                }
            }
            else // Firstly Insert , then return
            {
                SchoolSetting GlobalSettingObj = new SchoolSetting();
                GlobalSettingObj.AttributeName = AttrName;
                GlobalSettingObj.AttributeValue = AttrValue;
                GlobalSettingObj.Description = Description;
                GlobalSettingObj.AppFormId = AppFormId;

                db.SchoolSettings.Add(GlobalSettingObj);
                db.SaveChanges();

                GlobalSettingDataObj.AttributeName = GlobalSettingObj.AttributeName;
                GlobalSettingDataObj.AttributeValue = GlobalSettingObj.AttributeValue;
            }

            return GlobalSettingDataObj;
        }

        #endregion

    }
}