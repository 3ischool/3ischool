﻿using KS_SmartChild.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace KS_SmartChild.DAL.ContactModule
{
    public partial class ContactService : IContactService
    {
       
        private readonly IConnectionService _IConnectionService;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }


        public ContactService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region  Methods

        #region Contact Info Type

        public KSModel.Models.ContactInfoType GetContactInfoTypeById(int ContactInfoTypeId, bool IsTrack = false)
        {
           
            if (ContactInfoTypeId == 0)
                throw new ArgumentNullException("ContactInfoType");

            var contactInfoType = new ContactInfoType();
            if(IsTrack)
                contactInfoType = (from s in db.ContactInfoTypes where s.ContactInfoTypeId == ContactInfoTypeId select s).FirstOrDefault();
            else
                contactInfoType = (from s in db.ContactInfoTypes.AsNoTracking() where s.ContactInfoTypeId == ContactInfoTypeId select s).FirstOrDefault();
           
            return contactInfoType;
        }

        public List<KSModel.Models.ContactInfoType> GetAllContactInfoTypes(ref int totalcount, string ContactInfoType = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
           

            var query = db.ContactInfoTypes.ToList();
            if (!String.IsNullOrEmpty(ContactInfoType))
                query = query.Where(e => e.ContactInfoType1.ToLower().Contains(ContactInfoType.ToLower())).ToList();

            totalcount = query.Count;
            var ContactInfoTypes = new PagedList<KSModel.Models.ContactInfoType>(query, PageIndex, PageSize);
            return ContactInfoTypes;
        }

        public void InsertContactInfoType(KSModel.Models.ContactInfoType ContactInfoType)
        {
           

            if (ContactInfoType == null)
                throw new ArgumentNullException("ContactInfoType");

            db.ContactInfoTypes.Add(ContactInfoType);
            db.SaveChanges();
        }

        public void UpdateContactInfoType(KSModel.Models.ContactInfoType ContactInfoType)
        {
           

            if (ContactInfoType == null)
                throw new ArgumentNullException("ContactInfoType");

            db.Entry(ContactInfoType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteContactInfoType(int ContactInfoTypeId = 0)
        {
           

            if (ContactInfoTypeId == 0)
                throw new ArgumentNullException("ContactInfoType");

            var contactInfoType = (from s in db.ContactInfoTypes where s.ContactInfoTypeId == ContactInfoTypeId select s).FirstOrDefault();
            db.ContactInfoTypes.Remove(contactInfoType);
            db.SaveChanges();
        }

        #endregion

        #region Contact Info

        public KSModel.Models.ContactInfo GetContactInfoById(int ContactInfoId, bool IsTrack = false)
        {
           

            if (ContactInfoId == 0)
                throw new ArgumentNullException("ContactInfo");

            var contactInfo = new ContactInfo();
            if(IsTrack)
                contactInfo = (from s in db.ContactInfoes where s.ContactInfoId == ContactInfoId select s).FirstOrDefault();
            else
                contactInfo = (from s in db.ContactInfoes.AsNoTracking() where s.ContactInfoId == ContactInfoId select s).FirstOrDefault();
            
            return contactInfo;
        }

        public List<KSModel.Models.ContactInfo> GetAllContactInfos(ref int totalcount, string ContactInfo = null, int? ContactInfoTypeId = null, int? ContactId = null,
            int? ContactTypeId = null, bool? status = false, bool? Isdefault = false, int[] ContactIds = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
           

            var query = db.ContactInfoes.ToList();
            if (!string.IsNullOrEmpty(ContactInfo))
                query = query.Where(c => c.ContactInfo1 == ContactInfo).ToList();
            if (ContactInfoTypeId != null && ContactInfoTypeId > 0)
                query = query.Where(e => e.ContactInfoTypeId == ContactInfoTypeId).ToList();
            if (ContactId != null && ContactId > 0)
                query = query.Where(e => e.ContactId == ContactId).ToList();
            if (ContactTypeId != null && ContactTypeId > 0)
                query = query.Where(e => e.ContactTypeId == ContactTypeId).ToList();
            if (status == true)
                query = query.Where(e => e.Status == status).ToList();
            if (Isdefault == true)
                query = query.Where(e => e.IsDefault == Isdefault).ToList();
            if (ContactIds != null && ContactIds.Count() > 0)
                query = query.Where(e => ContactIds.Contains((int)e.ContactId)).ToList();

            totalcount = query.Count;
            var ContactInfoes = new PagedList<KSModel.Models.ContactInfo>(query, PageIndex, PageSize);
            return ContactInfoes;
        }

        public void InsertContactInfo(KSModel.Models.ContactInfo ContactInfo)
        {
           

            if (ContactInfo == null)
                throw new ArgumentNullException("ContactInfo");

            db.ContactInfoes.Add(ContactInfo);
            db.SaveChanges();
        }

        public void UpdateContactInfo(KSModel.Models.ContactInfo ContactInfo)
        {
           

            if (ContactInfo == null)
                throw new ArgumentNullException("ContactInfo");

            db.Entry(ContactInfo).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteContactInfo(int ContactInfoId = 0)
        {
           

            if (ContactInfoId == 0)
                throw new ArgumentNullException("ContactInfo");

            var contactInfo = (from s in db.ContactInfoes where s.ContactInfoId == ContactInfoId select s).FirstOrDefault();
            db.ContactInfoes.Remove(contactInfo);
            db.SaveChanges();
        }

        #endregion

        #region ContactPerson

        public KSModel.Models.ContactPerson GetContactPersonById(int ContactPersonId, bool IsTrack = false)
        {
           

            if (ContactPersonId == 0)
                throw new ArgumentNullException("ContactPerson");

            var contactPerson = new ContactPerson();
            if(IsTrack)
                contactPerson = (from s in db.ContactPersons where s.ContactPersonId == ContactPersonId select s).FirstOrDefault();
            else
                contactPerson = (from s in db.ContactPersons.AsNoTracking() where s.ContactPersonId == ContactPersonId select s).FirstOrDefault();
            
            return contactPerson;
        }

        public List<KSModel.Models.ContactPerson> GetAllContactPersons(ref int totalcount, string ContactPerson = "", int? schoolId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
           

            var query = db.ContactPersons.ToList();
            if (!String.IsNullOrEmpty(ContactPerson))
                query = query.Where(e => e.ContactPerson1.ToLower().Contains(ContactPerson.ToLower())).ToList();
            if (schoolId != null && schoolId > 0)
                query = query.Where(e => e.SchoolId == schoolId).ToList();

            totalcount = query.Count;
            var ContactPersons = new PagedList<KSModel.Models.ContactPerson>(query, PageIndex, PageSize);
            return ContactPersons;
        }

        public void InsertContactPerson(KSModel.Models.ContactPerson ContactPerson)
        {
           

            if (ContactPerson == null)
                throw new ArgumentNullException("ContactPerson");

            db.ContactPersons.Add(ContactPerson);
            db.SaveChanges();
        }

        public void UpdateContactPerson(KSModel.Models.ContactPerson ContactPerson)
        {
           

            if (ContactPerson == null)
                throw new ArgumentNullException("ContactPerson");

            db.Entry(ContactPerson).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteContactPerson(int ContactPersonId = 0)
        {
           

            if (ContactPersonId == 0)
                throw new ArgumentNullException("ContactPerson");

            var contactPerson = (from s in db.ContactPersons where s.ContactPersonId == ContactPersonId select s).FirstOrDefault();
            db.ContactPersons.Remove(contactPerson);
            db.SaveChanges();
        }

        #endregion

        #endregion

    }
}