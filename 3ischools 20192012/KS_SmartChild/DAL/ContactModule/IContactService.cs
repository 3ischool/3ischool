﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSModel.Models;

namespace KS_SmartChild.DAL.ContactModule
{
    public partial interface IContactService
    {
        #region  ContactInfoType

        ContactInfoType GetContactInfoTypeById(int ContactInfoTypeId,bool IsTrack = false);

        List<ContactInfoType> GetAllContactInfoTypes(ref int totalcount, string ContactInfoType = "", int PageIndex = 0,
            int PageSize = int.MaxValue);

        void InsertContactInfoType(ContactInfoType ContactInfoType);

        void UpdateContactInfoType(ContactInfoType ContactInfoType);

        void DeleteContactInfoType(int ContactInfoTypeId = 0);

        #endregion

        #region ContactInfo

        ContactInfo GetContactInfoById(int ContactInfoId, bool IsTrack = false);

        List<ContactInfo> GetAllContactInfos(ref int totalcount, string ContactInfo = null, int? ContactInfoTypeId = null, int? ContactId = null, int? ContactTypeId = null,
            bool? status = false, bool? Isdefault = false, int[] ContactIds = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertContactInfo(ContactInfo ContactInfo);

        void UpdateContactInfo(ContactInfo ContactInfo);

        void DeleteContactInfo(int ContactInfoId = 0);

        #endregion

        #region ContactPerson

        ContactPerson GetContactPersonById(int ContactPersonId, bool IsTrack = false);

        List<ContactPerson> GetAllContactPersons(ref int totalcount, string ContactPerson = "", int? schoolId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertContactPerson(ContactPerson ContactPerson);

        void UpdateContactPerson(ContactPerson ContactPerson);

        void DeleteContactPerson(int ContactPersonId = 0);

        #endregion
    }
}
