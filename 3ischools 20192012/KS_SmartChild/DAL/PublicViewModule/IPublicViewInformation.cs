﻿using KS_SmartChild.ViewModel.PublicViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.PublicViewModule
{
    public partial interface IPublicViewInformation
    {

        #region StudentAttendacne

        List<StudentAttendanceModel> GetAllStudentAttendanceBySp(ref int totalcount, int? ClassId = null, int? SessionId = null,
                    int MaterialTypeId = 0, DateTime? SelectedDate = null, int PageIndex = 0, int PageSize = int.MaxValue, int BookId = 0);


        #endregion
    }
}
