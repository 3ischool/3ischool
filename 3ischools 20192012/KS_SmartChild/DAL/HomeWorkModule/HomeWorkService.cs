﻿using KS_SmartChild.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace KS_SmartChild.DAL.HomeWorkModule
{
    public partial class HomeWorkService : IHomeWorkService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public HomeWorkService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region HW Type
        public KSModel.Models.HWType GetHWTypeById(int HWTypeId)
        {
            if (HWTypeId == 0)
                throw new ArgumentNullException("HWType");

            var HWType = (from s in db.HWTypes.AsNoTracking() where s.HWTypeId == HWTypeId select s).FirstOrDefault();
            return HWType;
        }

        public List<KSModel.Models.HWType> GetAllHWTypes(ref int totalcount, string HWType = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.HWTypes.ToList();
            if (!String.IsNullOrEmpty(HWType))
                query = query.Where(e => e.HWType1.ToLower().Contains(HWType.ToLower())).ToList();

            totalcount = query.Count;
            var HWTypes = new PagedList<KSModel.Models.HWType>(query, PageIndex, PageSize);
            return HWTypes;
        }

        public void InsertHWType(KSModel.Models.HWType HWType)
        {
            if (HWType == null)
                throw new ArgumentNullException("HWType");

            db.HWTypes.Add(HWType);
            db.SaveChanges();
        }

        public void UpdateHWType(KSModel.Models.HWType HWType)
        {
            if (HWType == null)
                throw new ArgumentNullException("HWType");

            db.Entry(HWType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteHWType(int HWTypeId = 0)
        {
            if (HWTypeId == 0)
                throw new ArgumentNullException("HWType");

            var HWType = (from s in db.HWTypes where s.HWTypeId == HWTypeId select s).FirstOrDefault();
            db.HWTypes.Remove(HWType);
            db.SaveChanges();
        }
        #endregion

        #region HW

        public KSModel.Models.HW GetHWById(int HWId, bool IsTrack = false)
        {
            if (HWId == 0)
                throw new ArgumentNullException("HW");

            HW HW = new HW();
            if (IsTrack)
                HW = (from s in db.HWs where s.HWId == HWId select s).FirstOrDefault();
            else
                HW = (from s in db.HWs.AsNoTracking() where s.HWId == HWId select s).FirstOrDefault();

            return HW;
        }

        public List<KSModel.Models.HW> GetAllHWs(ref int totalcount, int? HWtypeId = null, int? ClassSubjectId = null,
            int? StaffId = null, DateTime? Date = null, int? SubjectId = null, int? ClassId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.HWs.ToList();

            if (HWtypeId != null && HWtypeId > 0)
                query = query.Where(e => e.HWTypeId == HWtypeId).ToList();

            if (ClassSubjectId != null && ClassSubjectId > 0)
                query = query.Where(e => e.ClassSubjectId == ClassSubjectId).ToList();

            if (ClassId != null && ClassId > 0)
                query = query.Where(e => e.ClassSubject.ClassId == ClassId).ToList();

            if (StaffId != null && StaffId > 0)
                query = query.Where(e => e.StaffId == StaffId).ToList();

            if (Date.HasValue)
                query = query.Where(e => EntityFunctions.TruncateTime(e.Date) == Date.Value.Date).ToList();

            if (SubjectId != null && SubjectId > 0)
                query = query.Where(e => e.ClassSubject.SubjectId == SubjectId).ToList();

            totalcount = query.Count;
            var Hw = new PagedList<KSModel.Models.HW>(query, PageIndex, PageSize);
            return Hw;
        }

        public void InsertHW(KSModel.Models.HW HW)
        {
            if (HW == null)
                throw new ArgumentNullException("HW");

            db.HWs.Add(HW);
            db.SaveChanges();
        }

        public void UpdateHW(KSModel.Models.HW HW)
        {
            if (HW == null)
                throw new ArgumentNullException("HW");

            db.Entry(HW).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteHW(int HWId = 0)
        {
            if (HWId == 0)
                throw new ArgumentNullException("HW");

            var HW = (from s in db.HWs where s.HWId == HWId select s).FirstOrDefault();
            db.HWs.Remove(HW);
            db.SaveChanges();
        }

        #endregion

        #region HW Detail

        public KSModel.Models.HWDetail GetHWDetailById(int HWDetailId, bool IsTrack = false)
        {
            if (HWDetailId == 0)
                throw new ArgumentNullException("HWDetail");

            HWDetail HWDetail = new HWDetail();
            if (IsTrack)
                HWDetail = (from s in db.HWDetails where s.HWDetailId == HWDetailId select s).FirstOrDefault();
            else
                HWDetail = (from s in db.HWDetails.AsNoTracking() where s.HWDetailId == HWDetailId select s).FirstOrDefault();

            return HWDetail;
        }

        public List<KSModel.Models.HWDetail> GetAllHWDetails(ref int totalcount, string Description = "", string FileName = "", string Remarks = "", int? HWId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.HWDetails.ToList();
            if (!String.IsNullOrEmpty(Description))
                query = query.Where(e => e.Description.ToLower().Contains(Description.ToLower())).ToList();
            if (!String.IsNullOrEmpty(FileName))
                query = query.Where(e => e.FileName.ToLower().Contains(FileName.ToLower())).ToList();
            if (!String.IsNullOrEmpty(Remarks))
                query = query.Where(e => e.Remarks.ToLower().Contains(Remarks.ToLower())).ToList();
            if (HWId > 0)
                query = query.Where(e => e.HWId == HWId).ToList();
            totalcount = query.Count;
            var HWDetails = new PagedList<KSModel.Models.HWDetail>(query, PageIndex, PageSize);
            return HWDetails;
        }

        public void InsertHWDetail(KSModel.Models.HWDetail HWDetail)
        {
            if (HWDetail == null)
                throw new ArgumentNullException("HWDetail");

            db.HWDetails.Add(HWDetail);
            db.SaveChanges();
        }

        public void UpdateHWDetail(KSModel.Models.HWDetail HWDetail)
        {
            if (HWDetail == null)
                throw new ArgumentNullException("HWDetail");

            db.Entry(HWDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteHWDetail(int HWDetailId = 0)
        {
            if (HWDetailId == 0)
                throw new ArgumentNullException("HWDetail");

            var HWDetail = (from s in db.HWDetails where s.HWDetailId == HWDetailId select s).FirstOrDefault();
            db.HWDetails.Remove(HWDetail);
            db.SaveChanges();
        }

        #endregion
    }
}