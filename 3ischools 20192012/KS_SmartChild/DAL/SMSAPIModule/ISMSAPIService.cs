﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.SMSAPIModule
{
    public partial interface ISMSAPIService
    {
        #region SMSAPI

        SMSAPI GetSMSAPIById(int SMSAPIId);

        List<SMSAPI> GetAllSMSAPIs(ref int totalcount, string userid = null, string senderid = null, int? schoolId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertSMSAPI(SMSAPI SMSAPI);

        void UpdateSMSAPI(SMSAPI SMSAPI);

        void DeleteSMSAPI(int SMSAPIId = 0);

        #endregion
    }
}
