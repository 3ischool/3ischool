﻿using KS_SmartChild.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace KS_SmartChild.DAL.SMSAPIModule
{
    public partial class SMSAPIService : ISMSAPIService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public SMSAPIService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods
         
        #region SMSAPI

        public KSModel.Models.SMSAPI GetSMSAPIById(int SMSAPIId)
        {
            if (SMSAPIId == 0)
                throw new ArgumentNullException("SMSAPI");

            var sMSAPI = (from s in db.SMSAPIs.AsNoTracking() where s.SMSAPIId == SMSAPIId select s).FirstOrDefault();
            return sMSAPI;
        }

        public List<KSModel.Models.SMSAPI> GetAllSMSAPIs(ref int totalcount, string userid = null, string senderid = null,
            int? schoolId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SMSAPIs.ToList();
            if (!String.IsNullOrEmpty(userid))
                query = query.Where(e => e.APIUserId.ToLower().Contains(userid.ToLower())).ToList();
            if (!String.IsNullOrEmpty(senderid))
                query = query.Where(e => e.APISenderId.ToLower().Contains(senderid.ToLower())).ToList();
            if (schoolId != null && schoolId > 0)
                //query = query.Where(e => e.SchoolId == schoolId).ToList();

            totalcount = query.Count;
            var SMSAPIs = new PagedList<KSModel.Models.SMSAPI>(query, PageIndex, PageSize);
            return SMSAPIs;
        }

        public void InsertSMSAPI(KSModel.Models.SMSAPI SMSAPI)
        {
            if (SMSAPI == null)
                throw new ArgumentNullException("SMSAPI");

            db.SMSAPIs.Add(SMSAPI);
            db.SaveChanges();
        }

        public void UpdateSMSAPI(KSModel.Models.SMSAPI SMSAPI)
        {
            if (SMSAPI == null)
                throw new ArgumentNullException("SMSAPI");

            db.Entry(SMSAPI).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSMSAPI(int SMSAPIId = 0)
        {
            if (SMSAPIId == 0)
                throw new ArgumentNullException("SMSAPIId");

            var SMSAPI = (from s in db.SMSAPIs where s.SMSAPIId == SMSAPIId select s).FirstOrDefault();
            db.SMSAPIs.Remove(SMSAPI);
            db.SaveChanges();
        }

        #endregion

        #endregion
    }
}