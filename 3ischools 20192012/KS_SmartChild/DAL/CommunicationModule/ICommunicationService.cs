﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSModel.Models;

namespace KS_SmartChild.DAL.CommunicationModule
{
    public partial interface ICommunicationService
    {
        #region  CommunicationType

        CommunicationType GetCommunicationTypeById(int CommunicationTypeId);

        List<CommunicationType> GetAllCommunicationTypes(ref int totalcount, string CommunicationType = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertCommunicationType(CommunicationType CommunicationType);

        void UpdateCommunicationType(CommunicationType CommunicationType);

        void DeleteCommunicationType(int CommunicationTypeId = 0);

        #endregion

        #region CommunicationWay

        CommunicationWay GetCommunicationWayById(int CommunicationWayId);

        List<CommunicationWay> GetAllCommunicationWays(ref int totalcount, string CommunicationWay = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertCommunicationWay(CommunicationWay CommunicationWay);

        void UpdateCommunicationWay(CommunicationWay CommunicationWay);

        void DeleteCommunicationWay(int CommunicationWayId = 0);

        #endregion

        #region Communication

        Communication GetCommunicationById(int CommunicationId);

        List<Communication> GetAllCommunications(ref int totalcount, int? TypeId = null, int? wayId = null, int? contactpersonid = null,
            int? refid = null, DateTime? date = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertCommunication(Communication Communication);

        void UpdateCommunication(Communication Communication);

        void DeleteCommunication(int CommunicationId = 0);

        #endregion
    }
}
