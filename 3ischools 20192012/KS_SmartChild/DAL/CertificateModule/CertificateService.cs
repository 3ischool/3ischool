﻿using KS_SmartChild.DAL.Common;
using KS_SmartChild.DAL.UserModule;
using KSModel.Models;
using KS_SmartChild.ViewModel.IssueDocument;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace KS_SmartChild.DAL.CertificateModule
{
    public partial class CertificateService : ICertificateService
    {
        #region fields

        public int count = 0;
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        //private readonly string SqlDataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;
        private readonly IUserService _IUserService;

        #endregion

        #region Ctor

        public CertificateService(IConnectionService IConnectionService, IUserService IUserService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
            this._IUserService = IUserService;
        }

        #endregion

        #region  Issue Doc

        public KSModel.Models.IssueDoc GetIssueDocById(int IssueDocId, bool IsTrack = false)
        {
            if (IssueDocId == 0)
                throw new ArgumentNullException("IssueDoc");

            var IssueDoc = new IssueDoc();
            if (IsTrack)
                IssueDoc = (from s in db.IssueDocs where s.IssueDocId == IssueDocId select s).FirstOrDefault();
            else
                IssueDoc = (from s in db.IssueDocs.AsNoTracking() where s.IssueDocId == IssueDocId select s).FirstOrDefault();
            return IssueDoc;
        }

        public IList<KSModel.Models.IssueDoc> GetAllIssueDocnotrack(bool IsTrack = false)
        {

            var query = new List<KSModel.Models.IssueDoc>();
            query = (from s in db.IssueDocs.AsNoTracking() select s).ToList();
            return query;
        }

        public IList<KSModel.Models.IssueDoc> GetAllIssueDoc(ref int totalcount, int? IssueDocId = null, DateTime? IssueDate = null,
            string DocNo = "", int? IssueDocTypeId = null, int? SessionId = null, int? StudentId = null,
            int? DocTemplateId = null, bool IsPrinted = false, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.IssueDocs.ToList();

            if (IssueDocId != null && IssueDocId > 0)
                query = query.Where(e => e.IssueDocId == (int)IssueDocId).ToList();

            if (IssueDate.HasValue)
                query = query.Where(e => e.IssueDate == IssueDate).ToList();

            if (!String.IsNullOrEmpty(DocNo))
                query = query.Where(e => e.DocNo.ToLower() == DocNo.ToLower()).ToList();

            if (IssueDocTypeId != null && IssueDocTypeId > 0)
                query = query.Where(e => e.IssueDocTypeId == (int)IssueDocTypeId).ToList();

            if (SessionId != null && SessionId > 0)
                query = query.Where(e => e.SessionId == (int)SessionId).ToList();

            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == (int)StudentId).ToList();

            if (DocTemplateId != null && DocTemplateId > 0)
                query = query.Where(e => e.DocTemplateId == (int)DocTemplateId).ToList();


            //if (IsPrinted)
            //    query = query.Where(e => e.IsPrinted == true).ToList();
            //else
            //    query = query.Where(e => e.IsPrinted == false).ToList();

            query = query.OrderByDescending(M => M.IssueDocId).ToList();
            totalcount = query.Count;
            var IssueDoc = new PagedList<KSModel.Models.IssueDoc>(query, PageIndex, PageSize);
            return IssueDoc;
        }

        public IList<IssueDocInfoViewModel> GetAllIssueDocsBySP(ref int totalcount, int? IssueDocId = null, int? ClassId = null, string IssueFromDate = "",
                            string IssueToDate = "", int? StudentId = null, string AdmissionNo = "", int? IssueDocTypeId = null,
                            int? SessionId = null, string DocNo = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_GetAllIssueDocs", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ClassID", ClassId == null ? 0 : ClassId);
                    sqlComm.Parameters.AddWithValue("@StudentID", StudentId == null ? 0 : StudentId);
                    sqlComm.Parameters.AddWithValue("@AdminNo", AdmissionNo == null ? "" : AdmissionNo);
                    sqlComm.Parameters.AddWithValue("@IssueDocTypeId", IssueDocTypeId == null ? 0 : IssueDocTypeId);
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId == null ? 0 : SessionId);
                    sqlComm.Parameters.AddWithValue("@DocNo", DocNo == null ? "" : DocNo);
                    sqlComm.Parameters.AddWithValue("@IssueFromDate", IssueFromDate == null ? "" : IssueFromDate);
                    sqlComm.Parameters.AddWithValue("@IssueToDate", IssueToDate == null ? "" : IssueToDate);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<IssueDocInfoViewModel> GetAllIssuedDocList = new List<IssueDocInfoViewModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                IssueDocInfoViewModel Doclist = new IssueDocInfoViewModel();
                Doclist.IssueDocId = Convert.ToString(dt.Rows[i]["IssueDocId"]);
                Doclist.ClassId = Convert.ToString(dt.Rows[i]["ClassId"]);
                Doclist.Class = Convert.ToString(dt.Rows[i]["Class"]);
                Doclist.DocNo = Convert.ToString(dt.Rows[i]["DocNo"]);
                Doclist.StudentId = Convert.ToString(dt.Rows[i]["StudentId"]);
                Doclist.StudentAdmiNo = Convert.ToString(dt.Rows[i]["AdmnNo"]);
                Doclist.SessionId = Convert.ToString(dt.Rows[i]["SessionId"]);
                Doclist.Session = Convert.ToString(dt.Rows[i]["Session"]);
                Doclist.IssueDocTypeId = Convert.ToString(dt.Rows[i]["IssueDocTypeId"]);
                Doclist.IssueDocType = Convert.ToString(dt.Rows[i]["IssueDocType"]);
                Doclist.DocTemplateId = Convert.ToString(dt.Rows[i]["DocTemplateId"]);
                Doclist.PayableAmount = Convert.ToString(dt.Rows[i]["PayableAmount"]);
                Doclist.PaidAmount = Convert.ToString(dt.Rows[i]["PaidAmount"]);
                Doclist.IssueDate = Convert.ToString(dt.Rows[i]["IssueDate"]);
                Doclist.IsPrinted = Convert.ToBoolean(dt.Rows[i]["IsPrinted"]);
                Doclist.IsFeeCertificate = Convert.ToString(dt.Rows[i]["IsFeeCertificate"]);

                GetAllIssuedDocList.Add(Doclist);
            }
            totalcount = GetAllIssuedDocList.Count;

            GetAllIssuedDocList = new PagedList<IssueDocInfoViewModel>(GetAllIssuedDocList, PageIndex, PageSize);
            return GetAllIssuedDocList;
        }

        public void InsertIssueDoc(KSModel.Models.IssueDoc IssueDoc)
        {
            if (IssueDoc == null)
                throw new ArgumentNullException("IssueDoc");

            db.IssueDocs.Add(IssueDoc);
            db.SaveChanges();
        }

        public void UpdateIssueDoc(KSModel.Models.IssueDoc IssueDoc)
        {
            if (IssueDoc == null)
                throw new ArgumentNullException("IssueDoc");

            db.Entry(IssueDoc).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteIssueDoc(int IssueDocId = 0)
        {
            if (IssueDocId == 0)
                throw new ArgumentNullException("IssueDoc");

            var IssueDoc = (from s in db.IssueDocs where s.IssueDocId == IssueDocId select s).FirstOrDefault();
            db.IssueDocs.Remove(IssueDoc);
            db.SaveChanges();
        }

        #endregion

        #region IssueDocFeeHead
        public KSModel.Models.IssueDocFeeHead GetIssueDocFeeHeadById(int IssueDocFeeHeadId, bool IsTrack = false)
        {
            if (IssueDocFeeHeadId == 0)
                throw new ArgumentNullException("IssueDocFeeHead");

            var IssueDocFeeHead = new IssueDocFeeHead();
            if (IsTrack)
                IssueDocFeeHead = (from s in db.IssueDocFeeHeads where s.IssueDocFeeHeadId == IssueDocFeeHeadId select s).FirstOrDefault();
            else
                IssueDocFeeHead = (from s in db.IssueDocFeeHeads.AsNoTracking() where s.IssueDocFeeHeadId == IssueDocFeeHeadId select s).FirstOrDefault();
            return IssueDocFeeHead;
        }

        public IList<KSModel.Models.IssueDocFeeHead> GetAllIssueDocFeeHeads(ref int totalcount, int? IssueDocId = null,
                        int? FeeHeadId = null, decimal? Amount = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.IssueDocFeeHeads.ToList();

            if (IssueDocId != null)
                query = query.Where(e => e.IssueDocId == IssueDocId).ToList();
            if (FeeHeadId != null)
                query = query.Where(e => e.FeeHeadId == FeeHeadId).ToList();
            if (Amount != null)
                query = query.Where(e => e.Amount == Amount).ToList();

            totalcount = query.Count;
            var IssueDocFeeHeads = new PagedList<KSModel.Models.IssueDocFeeHead>(query, PageIndex, PageSize);
            return IssueDocFeeHeads;
        }

        public void InsertIssueDocFeeHead(KSModel.Models.IssueDocFeeHead IssueDocFeeHead)
        {
            if (IssueDocFeeHead == null)
                throw new ArgumentNullException("IssueDocFeeHead");

            db.IssueDocFeeHeads.Add(IssueDocFeeHead);
            db.SaveChanges();
        }

        public void UpdateIssueDocFeeHead(KSModel.Models.IssueDocFeeHead IssueDocFeeHead)
        {
            if (IssueDocFeeHead == null)
                throw new ArgumentNullException("IssueDocFeeHead");

            db.Entry(IssueDocFeeHead).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteIssueDocFeeHead(int IssueDocFeeHeadId = 0)
        {
            if (IssueDocFeeHeadId == 0)
                throw new ArgumentNullException("IssueDocFeeHead");

            var IssueDoFeeHead = (from s in db.IssueDocFeeHeads where s.IssueDocFeeHeadId == IssueDocFeeHeadId select s).FirstOrDefault();
            db.IssueDocFeeHeads.Remove(IssueDoFeeHead);
            db.SaveChanges();
        }

        public void DeleteIssueDocFeeHeadByIssueDoc(int IssueDocId = 0, int? FeeHeadId = null)
        {
            if (IssueDocId == 0)
                throw new ArgumentNullException("IssueDocFeeHead");

            var IssueDoFeeHead = new IssueDocFeeHead();
            var getAllIssuedocFeeHeads = GetAllIssueDocFeeHeads(ref count, IssueDocId: IssueDocId);
            foreach (var docId in getAllIssuedocFeeHeads)
            {
                if (FeeHeadId != null)
                {
                    IssueDoFeeHead = (from s in db.IssueDocFeeHeads
                                      where s.IssueDocFeeHeadId == docId.IssueDocFeeHeadId
                                      && s.IssueDocId == docId.IssueDocId && s.FeeHeadId == FeeHeadId
                                      select s).FirstOrDefault();
                }
                else
                {
                    IssueDoFeeHead = (from s in db.IssueDocFeeHeads
                                      where s.IssueDocFeeHeadId == docId.IssueDocFeeHeadId
                                      && s.IssueDocId == docId.IssueDocId
                                      select s).FirstOrDefault();
                }

                db.IssueDocFeeHeads.Remove(IssueDoFeeHead);
            }
            db.SaveChanges();
        }
        #endregion

        #region Issue Doc Type

        public KSModel.Models.IssueDocType GetIssueDocTypeById(int IssueDocTypeId, bool IsTrack = false)
        {
            if (IssueDocTypeId == 0)
                throw new ArgumentNullException("IssueDocType");

            var IssueDocType = new IssueDocType();
            if (IsTrack)
                IssueDocType = (from s in db.IssueDocTypes where s.IssueDocTypeId == IssueDocTypeId select s).FirstOrDefault();
            else
                IssueDocType = (from s in db.IssueDocTypes.AsNoTracking() where s.IssueDocTypeId == IssueDocTypeId select s).FirstOrDefault();
            return IssueDocType;
        }

        public IList<KSModel.Models.IssueDocType> GetAllIssueDocType(ref int totalcount, string IssueDocType = "",
            int PageIndex = 0, int PageSize = int.MaxValue, bool? IsLikeFilter = null)
        {
            var query = db.IssueDocTypes.ToList();

            if (!String.IsNullOrEmpty(IssueDocType))
            {
                if (IsLikeFilter != null)
                {
                    if ((bool)IsLikeFilter)
                        query = query.Where(e => e.IssueDocType1.ToLower().Contains(IssueDocType.ToLower())).ToList();
                    else
                        query = query.Where(e => e.IssueDocType1.ToLower() == IssueDocType.ToLower()).ToList();
                }
            }
            totalcount = query.Count;
            var IssueDocTypes = new PagedList<KSModel.Models.IssueDocType>(query, PageIndex, PageSize);
            return IssueDocTypes;
        }


        public DataTable GetAllIssueDocTokensListbySp(ref int totalcount, int? SessionId = null,int? StudentId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            // call store procedure for Advanced serach of books
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_GetCertificateStudentTempCols", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@StudentId", StudentId);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<TokenList> GetAllTokenList = new List<TokenList>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                TokenList tokenslist = new TokenList();
                tokenslist.StudentId = Convert.ToString(dt.Rows[i]["StudentId"]);
                tokenslist.AdmnDate = Convert.ToString(dt.Rows[i]["AdmnDate"]);
                tokenslist.RegNo = Convert.ToString(dt.Rows[i]["RegNo"]);
                tokenslist.Nationality = Convert.ToString(dt.Rows[i]["Nationality"]);
                tokenslist.IssueDocSrNo = Convert.ToString(dt.Rows[i]["IssueDocSrNo"]);
                tokenslist.AdmnNo = Convert.ToString(dt.Rows[i]["AdmnNo"]);
                tokenslist.StudentName = Convert.ToString(dt.Rows[i]["StudentName"]);
                tokenslist.DOB = Convert.ToString(dt.Rows[i]["DOB"]);
                tokenslist.AadharCardNo = Convert.ToString(dt.Rows[i]["AadharCardNo"]);
                tokenslist.Session = Convert.ToString(dt.Rows[i]["Session"]);
                tokenslist.AadharEnrlNo = Convert.ToString(dt.Rows[i]["AadharEnrlNo"]);
                tokenslist.Status = Convert.ToString(dt.Rows[i]["Status"]);
                tokenslist.GuardianName = Convert.ToString(dt.Rows[i]["GuardianName"]);
                tokenslist.Class = Convert.ToString(dt.Rows[i]["Class"]);
                tokenslist.Standard = Convert.ToString(dt.Rows[i]["Standard"]);
                tokenslist.RollNo = Convert.ToString(dt.Rows[i]["RollNo"]);
                tokenslist.Contact = Convert.ToString(dt.Rows[i]["Contact"]);
                tokenslist.BloodGroup = Convert.ToString(dt.Rows[i]["BloodGroup"]);
                tokenslist.FatherName = Convert.ToString(dt.Rows[i]["FatherName"]);
                tokenslist.FatherNumber = Convert.ToString(dt.Rows[i]["FatherNumber"]);
                tokenslist.MotherName = Convert.ToString(dt.Rows[i]["MotherName"]);
                tokenslist.MotherNumber = Convert.ToString(dt.Rows[i]["MotherNumber"]);
                tokenslist.SchoolName = Convert.ToString(dt.Rows[i]["SchoolName"]);
                tokenslist.Document = Convert.ToString(dt.Rows[i]["Document"]);
                tokenslist.Gender = Convert.ToString(dt.Rows[i]["Gender"]);
                tokenslist.Religion = Convert.ToString(dt.Rows[i]["Religion"]);
                tokenslist.Category = Convert.ToString(dt.Rows[i]["Category"]);
                tokenslist.House = Convert.ToString(dt.Rows[i]["House"]);
                tokenslist.Address = Convert.ToString(dt.Rows[i]["Address"]);
                GetAllTokenList.Add(tokenslist);
            }
            totalcount = GetAllTokenList.Count;
            GetAllTokenList = new PagedList<TokenList>(GetAllTokenList, PageIndex, PageSize);
            return dt;
        }

        public void InsertIssueDocType(KSModel.Models.IssueDocType IssueDocType)
        {
            if (IssueDocType == null)
                throw new ArgumentNullException("IssueDocType");

            db.IssueDocTypes.Add(IssueDocType);
            db.SaveChanges();
        }

        public void UpdateIssueDocType(KSModel.Models.IssueDocType IssueDocType)
        {
            if (IssueDocType == null)
                throw new ArgumentNullException("IssueDocType");

            db.Entry(IssueDocType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteIssueDocType(int IssueDocTypeId = 0)
        {
            if (IssueDocTypeId == 0)
                throw new ArgumentNullException("IssueDocType");

            var IssueDocType = (from s in db.IssueDocTypes where s.IssueDocTypeId == IssueDocTypeId select s).FirstOrDefault();
            db.IssueDocTypes.Remove(IssueDocType);
            db.SaveChanges();
        }

        #endregion

        #region Issue Doc Type Cols

        public KSModel.Models.IssueDocTypeCol GetIssueDocTypeColById(int IssueDocTypeColId, bool IsTrack = false)
        {
            if (IssueDocTypeColId == 0)
                throw new ArgumentNullException("IssueDocTypeCol");

            var IssueDocTypeCol = new IssueDocTypeCol();
            if (IsTrack)
                IssueDocTypeCol = (from s in db.IssueDocTypeCols where s.IssueDocTypeColsId == IssueDocTypeColId select s).FirstOrDefault();
            else
                IssueDocTypeCol = (from s in db.IssueDocTypeCols.AsNoTracking() where s.IssueDocTypeColsId == IssueDocTypeColId select s).FirstOrDefault();
            return IssueDocTypeCol;
        }

        public IList<KSModel.Models.IssueDocTypeCol> GetAllIssueDocTypeCol(ref int totalcount, string ColName = "", string ColCode = "",
            bool? IsTC = null, int? IssueDocTypeId = null, bool? IsFormfield = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.IssueDocTypeCols.ToList();

            if (!String.IsNullOrEmpty(ColName))
                query = query.Where(e => e.ColName.ToLower() == ColName.ToLower()).ToList();
            if (!String.IsNullOrEmpty(ColCode))
                query = query.Where(e => e.ColCode.ToLower() == ColCode.ToLower()).ToList();
            if (IsTC != null)
            {
                if ((bool)IsTC)
                    query = query.Where(i => i.IsTC == true).ToList();
                else
                    query = query.Where(i => i.IsTC == false).ToList();
            }
            if (IsFormfield != null)
            {
                if ((bool)IsFormfield)
                    query = query.Where(i => i.IsFormField == true).ToList();
                else
                    query = query.Where(i => i.IsFormField == false).ToList();
            }

            if (IssueDocTypeId != null)
                query = query.Where(i => i.IssueDocTypeId == IssueDocTypeId).ToList();

            query = query.OrderByDescending(M => M.IssueDocTypeColsId).ToList();
            totalcount = query.Count;
            var IssueDocTypeCol = new PagedList<KSModel.Models.IssueDocTypeCol>(query, PageIndex, PageSize);
            return IssueDocTypeCol;
        }

        public void InsertIssueDocTypeCol(KSModel.Models.IssueDocTypeCol IssueDocTypeCol)
        {
            if (IssueDocTypeCol == null)
                throw new ArgumentNullException("IssueDocTypeCol");

            db.IssueDocTypeCols.Add(IssueDocTypeCol);
            db.SaveChanges();
        }

        public void UpdateIssueDocTypeCol(KSModel.Models.IssueDocTypeCol IssueDocTypeCol)
        {
            if (IssueDocTypeCol == null)
                throw new ArgumentNullException("IssueDocTypeCol");

            db.Entry(IssueDocTypeCol).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteIssueDocTypeCol(int IssueDocTypeColId = 0)
        {
            if (IssueDocTypeColId == 0)
                throw new ArgumentNullException("IssueDocTypeCol");

            var IssueDocTypeCol = (from s in db.IssueDocTypeCols where s.IssueDocTypeColsId == IssueDocTypeColId select s).FirstOrDefault();
            db.IssueDocTypeCols.Remove(IssueDocTypeCol);
            db.SaveChanges();
        }

        public void DeleteIssueDocTypeColWithDocType(int IssueDocTypeId = 0)
        {
            if (IssueDocTypeId == 0)
                throw new ArgumentNullException("IssueDocTypeId");
            var IssueDocColsList = (from s in db.IssueDocTypeCols where s.IssueDocTypeId == IssueDocTypeId select s).ToList();
            foreach (var docCOls in IssueDocColsList)
            {
                var IssueDocTypeCol = (from s in db.IssueDocTypeCols
                                       where s.IssueDocTypeColsId == docCOls.IssueDocTypeColsId
                                       && s.IssueDocTypeId == IssueDocTypeId
                                       select s).FirstOrDefault();

                db.IssueDocTypeCols.Remove(IssueDocTypeCol);
            }
            db.SaveChanges();
        }

        #endregion

        #region Issue Doc Detail

        public KSModel.Models.IssueDocDetail GetIssueDocDetailById(int IssueDocDetailId, bool IsTrack = false)
        {
            if (IssueDocDetailId == 0)
                throw new ArgumentNullException("IssueDocDetail");

            var IssueDocDetail = new IssueDocDetail();
            if (IsTrack)
                IssueDocDetail = (from s in db.IssueDocDetails where s.IssueDocDetailId == IssueDocDetailId select s).FirstOrDefault();
            else
                IssueDocDetail = (from s in db.IssueDocDetails.AsNoTracking() where s.IssueDocDetailId == IssueDocDetailId select s).FirstOrDefault();
            return IssueDocDetail;
        }

        public IList<KSModel.Models.IssueDocDetail> GetAllIssueDocDetail(ref int totalcount, int? IssueDocId = null, int? IssueDocTypeColId = null, string ColValue = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.IssueDocDetails.ToList();

            if (IssueDocId != null && IssueDocId > 0)
                query = query.Where(e => e.IssueDocId == (int)IssueDocId).ToList();

            if (IssueDocTypeColId != null && IssueDocTypeColId > 0)
                query = query.Where(e => e.IssueDocTypeColId == (int)IssueDocTypeColId).ToList();

            if (!String.IsNullOrEmpty(ColValue))
                query = query.Where(e => ColValue.ToLower().Contains(e.ColValue.ToLower())).ToList();

            query = query.OrderByDescending(M => M.IssueDocDetailId).ToList();
            totalcount = query.Count;
            var IssueDoc = new PagedList<KSModel.Models.IssueDocDetail>(query, PageIndex, PageSize);
            return IssueDoc;
        }

        public void InsertIssueDocDetail(KSModel.Models.IssueDocDetail IssueDocDetail)
        {
            if (IssueDocDetail == null)
                throw new ArgumentNullException("IssueDocDetail");

            db.IssueDocDetails.Add(IssueDocDetail);
            db.SaveChanges();
        }

        public void UpdateIssueDocDetail(KSModel.Models.IssueDocDetail IssueDocDetail)
        {
            if (IssueDocDetail == null)
                throw new ArgumentNullException("IssueDocDetail");

            db.Entry(IssueDocDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteIssueDocDetail(int IssueDocDetailId = 0)
        {
            if (IssueDocDetailId == 0)
                throw new ArgumentNullException("IssueDocDetail");

            var IssueDocDetail = (from s in db.IssueDocDetails where s.IssueDocDetailId == IssueDocDetailId select s).FirstOrDefault();
            db.IssueDocDetails.Remove(IssueDocDetail);
            db.SaveChanges();
        }

        public void DeleteIssueDocDetailWithDocId(int IssueDocId = 0)
        {
            if (IssueDocId == 0)
                throw new ArgumentNullException("IssueDocTypeId");

            var IssueDocDetailList = (from s in db.IssueDocDetails where s.IssueDocId == (int)IssueDocId select s);
            foreach (var docdet in IssueDocDetailList)
            {
                var IssueDocDetail = (from s in db.IssueDocDetails 
                                      where s.IssueDocId == docdet.IssueDocId 
                                            && s.IssueDocTypeColId ==  docdet.IssueDocTypeColId
                                            && s.IssueDocDetailId == docdet.IssueDocDetailId
                                      select s).FirstOrDefault();


                db.IssueDocDetails.Remove(IssueDocDetail);
            }
            db.SaveChanges();
        }

        public void SaveIssueDocDetailWithSP(int StudentId, int SessionId, int IssueDocId, string IssueDocType)
        {
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_insertissuedocdetail", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@studentid", StudentId);
                    sqlComm.Parameters.AddWithValue("@issuedocid", IssueDocId);
                    sqlComm.Parameters.AddWithValue("@issuedoctype", IssueDocType);
                    sqlComm.Parameters.AddWithValue("@sessionId", SessionId);

                    SqlDataAdapter da = new SqlDataAdapter();
                    sqlComm.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }

        #endregion

        #region Issue Doc Template

        public IssueDocTemplate GetIssueDocTemplateById(int IssueDocTemplateId, bool IsTrack = false)
        {
            if (IssueDocTemplateId == 0)
                throw new ArgumentNullException("IssueDocTemplate");

            var IssueDocTemplate = new IssueDocTemplate();
            if (IsTrack)
                IssueDocTemplate = (from s in db.IssueDocTemplates where s.IssueDocTemplateId == IssueDocTemplateId select s).FirstOrDefault();
            else
                IssueDocTemplate = (from s in db.IssueDocTemplates.AsNoTracking() where s.IssueDocTemplateId == IssueDocTemplateId select s).FirstOrDefault();
            return IssueDocTemplate;
        }

        public IList<IssueDocTemplate> GetAllIssueDocTemplates(ref int totalcount, int? IssueDocTypeId = null, string TemplateTitle = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.IssueDocTemplates.ToList();

            if (IssueDocTypeId != null && IssueDocTypeId > 0)
                query = query.Where(e => e.IssueDocTypeId == IssueDocTypeId).ToList();
            if (!String.IsNullOrEmpty(TemplateTitle))
                query = query.Where(e => e.TemplateTitle.ToLower() == TemplateTitle.ToLower()).ToList();

            totalcount = query.Count;
            var IssueDocTemplates = new PagedList<KSModel.Models.IssueDocTemplate>(query, PageIndex, PageSize);
            return IssueDocTemplates;
        }

        public void InsertIssueDocTemplate(IssueDocTemplate IssueDocTemplate)
        {
            if (IssueDocTemplate == null)
                throw new ArgumentNullException("IssueDocTemplate");

            db.IssueDocTemplates.Add(IssueDocTemplate);
            db.SaveChanges();
        }

        public void UpdateIssueDocTemplate(IssueDocTemplate IssueDocTemplate)
        {
            if (IssueDocTemplate == null)
                throw new ArgumentNullException("IssueDocTemplate");

            db.Entry(IssueDocTemplate).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteIssueDocTemplate(int IssueDocTemplateId = 0)
        {
            if (IssueDocTemplateId == 0)
                throw new ArgumentNullException("IssueDocTemplate");

            var IssueDocTemplate = (from s in db.IssueDocTemplates where s.IssueDocTemplateId == IssueDocTemplateId select s).FirstOrDefault();
            db.IssueDocTemplates.Remove(IssueDocTemplate);
            db.SaveChanges();
        }

        #endregion

        #region Issue Doc Template Detail

        public IssueDocTemplateDetail GetIssueDocTemplateDetailById(int IssueDocTemplateDetailId, bool IsTrack = false)
        {
            if (IssueDocTemplateDetailId == 0)
                throw new ArgumentNullException("IssueDocTemplateDetail");

            var IssueDocTemplateDetail = new IssueDocTemplateDetail();
            if (IsTrack)
                IssueDocTemplateDetail = (from s in db.IssueDocTemplateDetails where s.IssueDocTemplateDetailId == IssueDocTemplateDetailId select s).FirstOrDefault();
            else
                IssueDocTemplateDetail = (from s in db.IssueDocTemplateDetails.AsNoTracking() where s.IssueDocTemplateDetailId == IssueDocTemplateDetailId select s).FirstOrDefault();
            return IssueDocTemplateDetail;
        }

        public IList<IssueDocTemplateDetail> GetAllIssueDocTemplateDetails(ref int totalcount, int? IssueDocTemplateId = null,
            DateTime? EffectiveDate = null, DateTime? EndDate = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.IssueDocTemplateDetails.ToList();
            if (IssueDocTemplateId != null && IssueDocTemplateId > 0)
                query = query.Where(e => e.IssueDocTemplateId == IssueDocTemplateId).ToList();
            if (EffectiveDate.HasValue)
                query = query.Where(e => e.EffectiveDate == EffectiveDate).ToList();
            if (EndDate.HasValue)
                query = query.Where(e => e.EndDate == EndDate).ToList();

            totalcount = query.Count;
            var IssueDocTemplateDetails = new PagedList<KSModel.Models.IssueDocTemplateDetail>(query, PageIndex, PageSize);
            return IssueDocTemplateDetails;
        }

        public void InsertIssueDocTemplateDetail(IssueDocTemplateDetail IssueDocTemplateDetail)
        {
            if (IssueDocTemplateDetail == null)
                throw new ArgumentNullException("IssueDocTemplateDetail");

            db.IssueDocTemplateDetails.Add(IssueDocTemplateDetail);
            db.SaveChanges();
        }

        public void UpdateIssueDocTemplateDetail(IssueDocTemplateDetail IssueDocTemplateDetail)
        {
            if (IssueDocTemplateDetail == null)
                throw new ArgumentNullException("IssueDocTemplateDetail");

            db.Entry(IssueDocTemplateDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteIssueDocTemplateDetail(int IssueDocTemplateDetailId = 0)
        {
            if (IssueDocTemplateDetailId == 0)
                throw new ArgumentNullException("IssueDocTemplateDetail");

            var IssueDocTemplateDetail = (from s in db.IssueDocTemplateDetails where s.IssueDocTemplateDetailId == IssueDocTemplateDetailId select s).FirstOrDefault();
            db.IssueDocTemplateDetails.Remove(IssueDocTemplateDetail);
            db.SaveChanges();
        }

        #endregion

        #region Issue Doc HTML Template

        public IssueDocHTMLTemplate GetIssueDocHTMLTemplateById(int IssueDocHTMLTemplateId, bool IsTrack = false)
        {
            if (IssueDocHTMLTemplateId == 0)
                throw new ArgumentNullException("IssueDocHTMLTemplate");

            var IssueDocHTMLTemplate = new IssueDocHTMLTemplate();
            if (IsTrack)
                IssueDocHTMLTemplate = (from s in db.IssueDocHTMLTemplates where s.IssueDocHTMLTemplateId == IssueDocHTMLTemplateId select s).FirstOrDefault();
            else
                IssueDocHTMLTemplate = (from s in db.IssueDocHTMLTemplates.AsNoTracking() where s.IssueDocHTMLTemplateId == IssueDocHTMLTemplateId select s).FirstOrDefault();
            return IssueDocHTMLTemplate;
        }

        public IList<IssueDocHTMLTemplate> GetAllIssueDocHTMLTemplates(ref int totalcount, string HTMLTitle = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.IssueDocHTMLTemplates.ToList();
            if (!String.IsNullOrEmpty(HTMLTitle))
                query = query.Where(e => e.HTMLTitle.ToLower() == HTMLTitle.ToLower()).ToList();

            totalcount = query.Count;
            var IssueDocHTMLTemplates = new PagedList<KSModel.Models.IssueDocHTMLTemplate>(query, PageIndex, PageSize);
            return IssueDocHTMLTemplates;
        }

        public void InsertIssueDocHTMLTemplate(IssueDocHTMLTemplate IssueDocHTMLTemplate)
        {
            if (IssueDocHTMLTemplate == null)
                throw new ArgumentNullException("IssueDocHTMLTemplate");

            db.IssueDocHTMLTemplates.Add(IssueDocHTMLTemplate);
            db.SaveChanges();
        }

        public void UpdateIssueDocHTMLTemplate(IssueDocHTMLTemplate IssueDocHTMLTemplate)
        {
            if (IssueDocHTMLTemplate == null)
                throw new ArgumentNullException("IssueDocHTMLTemplate");

            db.Entry(IssueDocHTMLTemplate).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteIssueDocHTMLTemplate(int IssueDocHTMLTemplateId = 0)
        {
            if (IssueDocHTMLTemplateId == 0)
                throw new ArgumentNullException("IssueDocHTMLTemplate");

            var IssueDocHTMLTemplate = (from s in db.IssueDocHTMLTemplates where s.IssueDocHTMLTemplateId == IssueDocHTMLTemplateId select s).FirstOrDefault();
            db.IssueDocHTMLTemplates.Remove(IssueDocHTMLTemplate);
            db.SaveChanges();
        }

        #endregion

        #region Issue Doc Template HTML

        public IssueDocTemplateHTML GetIssueDocTemplateHTMLById(int IssueDocTemplateHTMLId, bool IsTrack = false)
        {
            if (IssueDocTemplateHTMLId == 0)
                throw new ArgumentNullException("IssueDocTemplateHTML");

            var IssueDocTemplateHTML = new IssueDocTemplateHTML();
            if (IsTrack)
                IssueDocTemplateHTML = (from s in db.IssueDocTemplateHTMLs where s.IssueDocTemplateHTMLId == IssueDocTemplateHTMLId select s).FirstOrDefault();
            else
                IssueDocTemplateHTML = (from s in db.IssueDocTemplateHTMLs.AsNoTracking() where s.IssueDocTemplateHTMLId == IssueDocTemplateHTMLId select s).FirstOrDefault();
            return IssueDocTemplateHTML;
        }

        public IList<IssueDocTemplateHTML> GetAllIssueDocTemplateHTMLs(ref int totalcount, int? IssueDocTemplateId = null,
            int? IssueDocHTMLTemplateId = null, DateTime? EffectiveDate = null, DateTime? EndDate = null, int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.IssueDocTemplateHTMLs.ToList();

            if (IssueDocTemplateId != null && IssueDocTemplateId > 0)
                query = query.Where(e => e.IssueDocTemplateId == IssueDocTemplateId).ToList();
            if (IssueDocHTMLTemplateId != null && IssueDocHTMLTemplateId > 0)
                query = query.Where(e => e.IssueDocHTMLTemplateId == IssueDocHTMLTemplateId).ToList();
            if (EffectiveDate.HasValue)
                query = query.Where(e => e.EffectiveDate == EffectiveDate).ToList();
            if (EndDate.HasValue)
                query = query.Where(e => e.EndDate == EndDate).ToList();

            totalcount = query.Count;
            var IssueDocTemplateHTMLs = new PagedList<KSModel.Models.IssueDocTemplateHTML>(query, PageIndex, PageSize);
            return IssueDocTemplateHTMLs;
        }

        public void InsertIssueDocTemplateHTML(IssueDocTemplateHTML IssueDocTemplateHTML)
        {
            if (IssueDocTemplateHTML == null)
                throw new ArgumentNullException("IssueDocTemplateHTML");

            db.IssueDocTemplateHTMLs.Add(IssueDocTemplateHTML);
            db.SaveChanges();
        }

        public void UpdateIssueDocTemplateHTML(IssueDocTemplateHTML IssueDocTemplateHTML)
        {
            if (IssueDocTemplateHTML == null)
                throw new ArgumentNullException("IssueDocTemplateHTML");

            db.Entry(IssueDocTemplateHTML).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteIssueDocTemplateHTML(int IssueDocTemplateHTMLId = 0)
        {
            if (IssueDocTemplateHTMLId == 0)
                throw new ArgumentNullException("IssueDocTemplateHTML");

            var IssueDocTemplateHTML = (from s in db.IssueDocTemplateHTMLs where s.IssueDocTemplateHTMLId == IssueDocTemplateHTMLId select s).FirstOrDefault();
            db.IssueDocTemplateHTMLs.Remove(IssueDocTemplateHTML);
            db.SaveChanges();
        }

        #endregion

    }
}