﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.Task
{
    public interface IQuartzInitializer
    {
        void Start();
    }
}
