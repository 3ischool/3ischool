﻿using KS_SmartChild.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace KS_SmartChild.DAL.AddressModule
{
    public partial class AddressMasterService : IAddressMasterService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public AddressMasterService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region  Methods

        #region  AddressCountry

        public KSModel.Models.AddressCountry GetAddressCountryById(int AddressCountryId, bool IsTrack = false)
        {
            if (AddressCountryId == 0)
                throw new ArgumentNullException("AddressCountry");

            var addressCountry = new AddressCountry();
            if(IsTrack)
                addressCountry = (from s in db.AddressCountries where s.CountryId == AddressCountryId select s).FirstOrDefault();
            else
                addressCountry = (from s in db.AddressCountries.AsNoTracking() where s.CountryId == AddressCountryId select s).FirstOrDefault();
            
            return addressCountry;
        }

        public List<KSModel.Models.AddressCountry> GetAllAddressCountries(ref int totalcount, string country = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AddressCountries.ToList();
            if (!String.IsNullOrEmpty(country))
                query = query.Where(e => e.Country.ToLower().Contains(country.ToLower())).ToList();

            totalcount = query.Count;
            var AddressCountries = new PagedList<KSModel.Models.AddressCountry>(query, PageIndex, PageSize);
            return AddressCountries;
        }

        public void InsertAddressCountry(KSModel.Models.AddressCountry AddressCountry)
        {
            if (AddressCountry == null)
                throw new ArgumentNullException("AddressCountry");

            db.AddressCountries.Add(AddressCountry);
            db.SaveChanges();
        }

        public void UpdateAddressCountry(KSModel.Models.AddressCountry AddressCountry)
        {
            if (AddressCountry == null)
                throw new ArgumentNullException("AddressCountry");

            db.Entry(AddressCountry).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAddressCountry(int AddressCountryId = 0)
        {
            if (AddressCountryId == 0)
                throw new ArgumentNullException("AddressCountry");

            var addressCountry = (from s in db.AddressCountries where s.CountryId == AddressCountryId select s).FirstOrDefault();
            db.AddressCountries.Remove(addressCountry);
            db.SaveChanges();
        }

        #endregion

        #region State

        public KSModel.Models.AddressState GetAddressStateById(int AddressStateId, bool IsTrack = false)
        {
            if (AddressStateId == 0)
                throw new ArgumentNullException("AddressState");

            var addressState = new AddressState();
            if(IsTrack)
                addressState = (from s in db.AddressStates where s.CountryId == AddressStateId select s).FirstOrDefault();
            else
                addressState = (from s in db.AddressStates.AsNoTracking() where s.CountryId == AddressStateId select s).FirstOrDefault();

            return addressState;
        }

        public List<KSModel.Models.AddressState> GetAllAddressStates(ref int totalcount, int? countryId = null, string state = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AddressStates.ToList();
            if (countryId != null && countryId > 0)
                query = query.Where(e => e.CountryId == countryId).ToList();
            if (!String.IsNullOrEmpty(state))
                query = query.Where(e => e.State.ToLower().Contains(state.ToLower())).ToList();
            
            totalcount = query.Count;
            var AddressStates = new PagedList<KSModel.Models.AddressState>(query, PageIndex, PageSize);
            return AddressStates;
        }

        public void InsertAddressState(KSModel.Models.AddressState AddressState)
        {
            if (AddressState == null)
                throw new ArgumentNullException("AddressState");

            db.AddressStates.Add(AddressState);
            db.SaveChanges();
        }

        public void UpdateAddressState(KSModel.Models.AddressState AddressState)
        {
            if (AddressState == null)
                throw new ArgumentNullException("AddressState");

            db.Entry(AddressState).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAddressState(int AddressStateId = 0)
        {
            if (AddressStateId == 0)
                throw new ArgumentNullException("AddressState");

            var addressState = (from s in db.AddressStates where s.CountryId == AddressStateId select s).FirstOrDefault();
            db.AddressStates.Remove(addressState);
            db.SaveChanges();
        }

        #endregion

        #region City

        public KSModel.Models.AddressCity GetAddressCityById(int AddressCityId, bool IsTrack = false)
        {
            if (AddressCityId == 0)
                throw new ArgumentNullException("AddressCity");

            var addressCity = new AddressCity();
            if(IsTrack)
                addressCity = (from s in db.AddressCities where s.CityId == AddressCityId select s).FirstOrDefault();
            else
                addressCity = (from s in db.AddressCities.AsNoTracking() where s.CityId == AddressCityId select s).FirstOrDefault();

            return addressCity;
        }

        public List<KSModel.Models.AddressCity> GetAllAddressCitys(ref int totalcount, int? stateId = null, string city = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AddressCities.ToList();
            if (!String.IsNullOrEmpty(city))
                query = query.Where(e => e.City.ToLower().Contains(city.ToLower())).ToList();
            if (stateId != null && stateId > 0)
                query = query.Where(e => e.StateId == stateId).ToList();

            totalcount = query.Count;
            var AddressCities = new PagedList<KSModel.Models.AddressCity>(query, PageIndex, PageSize);
            return AddressCities;
        }

        public void InsertAddressCity(KSModel.Models.AddressCity AddressCity)
        {
            if (AddressCity == null)
                throw new ArgumentNullException("AddressCity");

            db.AddressCities.Add(AddressCity);
            db.SaveChanges();
        }

        public void UpdateAddressCity(KSModel.Models.AddressCity AddressCity)
        {
            if (AddressCity == null)
                throw new ArgumentNullException("AddressCity");

            db.Entry(AddressCity).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAddressCity(int AddressCityId = 0)
        {
            if (AddressCityId == 0)
                throw new ArgumentNullException("AddressCity");

            var addressCity = (from s in db.AddressCities where s.CityId == AddressCityId select s).FirstOrDefault();
            db.AddressCities.Remove(addressCity);
            db.SaveChanges();
        }

        #endregion

        #region Address Type

        public KSModel.Models.AddressType GetAddressTypeById(int AddressTypeId, bool IsTrack = false)
        {
            if (AddressTypeId == 0)
                throw new ArgumentNullException("AddressType");

            var addressType = new AddressType();
            if(IsTrack)
                addressType = (from s in db.AddressTypes where s.AddressTypeId == AddressTypeId select s).FirstOrDefault();
            else
                addressType = (from s in db.AddressTypes.AsNoTracking() where s.AddressTypeId == AddressTypeId select s).FirstOrDefault();

            return addressType;
        }

        public List<KSModel.Models.AddressType> GetAllAddressTypes(ref int totalcount, string addresstype = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AddressTypes.ToList();
            if (!String.IsNullOrEmpty(addresstype))
                query = query.Where(e => e.AddressType1.ToLower().Contains(addresstype.ToLower())).ToList();

            totalcount = query.Count;
            var AddressTypes = new PagedList<KSModel.Models.AddressType>(query, PageIndex, PageSize);
            return AddressTypes;
        }

        public void InsertAddressType(KSModel.Models.AddressType AddressType)
        {
            if (AddressType == null)
                throw new ArgumentNullException("AddressType");

            db.AddressTypes.Add(AddressType);
            db.SaveChanges();
        }

        public void UpdateAddressType(KSModel.Models.AddressType AddressType)
        {
            if (AddressType == null)
                throw new ArgumentNullException("AddressType");

            db.Entry(AddressType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAddressType(int AddressTypeId = 0)
        {
            if (AddressTypeId == 0)
                throw new ArgumentNullException("AddressType");

            var addressType = (from s in db.AddressTypes where s.AddressTypeId == AddressTypeId select s).FirstOrDefault();
            db.AddressTypes.Remove(addressType);
            db.SaveChanges();
        }

        #endregion

        #region Contact Type

        public KSModel.Models.ContactType GetContactTypeById(int ContactTypeId, bool IsTrack = false)
        {
            if (ContactTypeId == 0)
                throw new ArgumentNullException("Contactype");

            var contactType = new ContactType();
            if(IsTrack)
                contactType = (from s in db.ContactTypes where s.ContactTypeId == ContactTypeId select s).FirstOrDefault();
            else
                contactType = (from s in db.ContactTypes.AsNoTracking() where s.ContactTypeId == ContactTypeId select s).FirstOrDefault();

            return contactType;
        }

        public IList<KSModel.Models.ContactType> GetAllContactTypes(ref int totalcount, string contactType = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ContactTypes.AsEnumerable();
            if (!String.IsNullOrEmpty(contactType))
                query = query.Where(e => e.ContactType1.ToLower().Contains(contactType.ToLower()));

            totalcount = query.Count();
            var contactTypes = new PagedList<KSModel.Models.ContactType>(query.ToList(), PageIndex, PageSize);
            return contactTypes;
        }

        public void InsertContactType(KSModel.Models.ContactType ContactType)
        {
            if (ContactType == null)
                throw new ArgumentNullException("ContactType");

            db.ContactTypes.Add(ContactType);
            db.SaveChanges();
        }

        public void UpdateContactType(KSModel.Models.ContactType ContactType)
        {
            if (ContactType == null)
                throw new ArgumentNullException("ContactType");

            db.Entry(ContactType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteContactType(int ContactTypeId = 0)
        {
            if (ContactTypeId == 0)
                throw new ArgumentNullException("ContactType");

            var contactType = (from s in db.ContactTypes where s.ContactTypeId == ContactTypeId select s).FirstOrDefault();
            db.ContactTypes.Remove(contactType);
            db.SaveChanges();
        }

        #endregion

        #endregion

    }
}