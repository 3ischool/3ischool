﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSModel.Models;

namespace KS_SmartChild.DAL.AddressModule
{
    public partial interface IAddressMasterService
    {
        #region  Address Country

        AddressCountry GetAddressCountryById(int AddressCountryId, bool IsTrack = false);

        List<AddressCountry> GetAllAddressCountries(ref int totalcount, string country = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAddressCountry(AddressCountry AddressCountry);

        void UpdateAddressCountry(AddressCountry AddressCountry);

        void DeleteAddressCountry(int AddressCountryId = 0);

        #endregion

        #region State

        AddressState GetAddressStateById(int AddressStateId, bool IsTrack = false);

        List<AddressState> GetAllAddressStates(ref int totalcount, int? countryId = null, string state = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAddressState(AddressState AddressState);

        void UpdateAddressState(AddressState AddressState);

        void DeleteAddressState(int AddressStateId = 0);

        #endregion

        #region City

        AddressCity GetAddressCityById(int AddressCityId, bool IsTrack = false);

        List<AddressCity> GetAllAddressCitys(ref int totalcount, int? stateId = null, string city = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAddressCity(AddressCity AddressCity);

        void UpdateAddressCity(AddressCity AddressCity);

        void DeleteAddressCity(int AddressCityId = 0);

        #endregion

        #region Address Type

        AddressType GetAddressTypeById(int AddressTypeId, bool IsTrack = false);

        List<AddressType> GetAllAddressTypes(ref int totalcount, string addresstype = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAddressType(AddressType AddressType);

        void UpdateAddressType(AddressType AddressType);

        void DeleteAddressType(int AddressTypeId = 0);

        #endregion

        #region Contact Type

        ContactType GetContactTypeById(int ContactTypeId, bool IsTrack = false);

        IList<ContactType> GetAllContactTypes(ref int totalcount, string contactType = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertContactType(ContactType ContactType);

        void UpdateContactType(ContactType ContactType);

        void DeleteContactType(int ContactTypeId = 0);

        #endregion
    }
}
