﻿using KS_SmartChild.DAL.Kendo;
using KSModel.Models;
using KS_SmartChild.ViewModel.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.CatalogMaster
{
    public partial interface ICatalogMasterService
    {
        #region Student Sub-Modules

        #region Session

        Session GetSessionById(int SessionId, bool IsTrack = false);

        List<Session> GetAllSessions(ref int totalcount, string Session = "", DateTime? StartDate = null, DateTime? Enddate = null,
            int PageIndex = 0, int PageSize = int.MaxValue, bool Isdeafult = false, int SessionId = 0);

        List<Session> GetAllNewSessions(ref int count, int PageIndex = 0, int PageSize = int.MaxValue);

        Session GetCurrentSession(int? DaysBeforeAdmnOpening = null);

        Session GetSessionDetail(int Session);

        Session GetPreviousSession();

        bool DateInCurrentSession(DateTime fromdate, DateTime? todate = null);

        void InsertSession(Session Session);

        void UpdateSession(Session Session);

        void DeleteSession(int SessionId = 0);

        #endregion

        #region SessionAdmissionDetail

        SessionAdmissionDetail GetSessionAdmissionDetailById(int SessionAdmissionDetailId, bool IsTrack = false);

        List<SessionAdmissionDetail> GetAllSessionAdmissionDetails(ref int totalcount, int? SessionId = null, int? StandardId = null,
           DateTime? AdmnDate = null, int? Seats = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertSessionAdmissionDetail(SessionAdmissionDetail SessionAdmissionDetail);

        void UpdateSessionAdmissionDetail(SessionAdmissionDetail SessionAdmissionDetail);

        void DeleteSessionAdmissionDetail(int SessionAdmissionDetailId = 0);

        #endregion

        #region CustodyRight

        CustodyRight GetCustodyRightById(int CustodyRightId);

        List<CustodyRight> GetAllCustodyRights(ref int totalcount, string CustodyRight = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertCustodyRight(CustodyRight CustodyRight);

        void UpdateCustodyRight(CustodyRight CustodyRight);

        void DeleteCustodyRight(int CustodyRightId = 0);

        #endregion

        #region Gender

        Gender GetGenderById(int GenderId);

        List<Gender> GetAllGenders(ref int totalcount, string Gender = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertGender(Gender Gender);

        void UpdateGender(Gender Gender);

        void DeleteGender(int GenderId = 0);

        #endregion

        #region BloodGroup

        BloodGroup GetBloodGroupById(int BloodGroupId);

        List<BloodGroup> GetAllBloodGroups(ref int totalcount, string BloodGroup = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertBloodGroup(BloodGroup BloodGroup);

        void UpdateBloodGroup(BloodGroup BloodGroup);

        void DeleteBloodGroup(int BloodGroupId = 0);

        #endregion

        #region Nationality

        Nationality GetNationalityById(int NationalityId);

        List<Nationality> GetAllNationalitys(ref int totalcount, string Nationality = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertNationality(Nationality Nationality);

        void UpdateNationality(Nationality Nationality);

        void DeleteNationality(int NationalityId = 0);

        #endregion

        #region House

        House GetHouseById(int HouseId);

        List<House> GetAllHouses(ref int totalcount, string House = "", string HouseColour = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertHouse(House House);

        void UpdateHouse(House House);

        void DeleteHouse(int HouseId = 0);

        #endregion

        #region Religion

        Religion GetReligionById(int ReligionId);

        List<Religion> GetAllReligions(ref int totalcount, string Religion = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertReligion(Religion Religion);

        void UpdateReligion(Religion Religion);

        void DeleteReligion(int ReligionId = 0);

        #endregion

        #endregion

        #region class sub-modules

        #region ClassMaster

        ClassMaster GetClassMasterById(int ClassMasterId, bool istrack = false);

        List<KSModel.Models.ClassMaster> GetClassMasterByClassName(string ClassName);

        List<ClassMaster> GetAllClassMasters(ref int totalcount, int? StandardId = null, int? SectionId = null, string ClassName = "",
           decimal? StandardIndex = null,int? SortingIndex=0, int PageIndex = 0, int PageSize = int.MaxValue, IEnumerable<Sort> sort = null);
        IList<ClassMasterModel> GetAllClassMasters_Sp(ref int totalcount, int? StandardId = null, int sessionId = 0,
            int? SectionId = null, string ClassName = "", decimal? StandardIndex = null, int? SortingIndex = null, int PageIndex = 0, int PageSize = int.MaxValue, IEnumerable<Sort> sort = null);
        void InsertClassMaster(ClassMaster ClassMaster);

        void UpdateClassMaster(ClassMaster ClassMaster);

        void DeleteClassMaster(int ClassMasterId = 0);


        List<VirtualClass> GetAllVirtualClasses(ref int totalcount, string RoomNo = "", string VirtualClass = "",
          int PageIndex = 0, int PageSize = int.MaxValue);


        List<VirtualClassBinding> GetAllVirtualClassBindings(ref int totalcount,
           int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertVirtualClassBindings(VirtualClassBinding VirtualClassBinding);

        void UpdateVirtualClassBinding(VirtualClassBinding VirtualClassBinding);

        void DeleteVirtualClassBinding(int VirtualClassBindingId = 0);


        List<VirtualClassStudentId> GetAllVirtualClassStudents(ref int totalcount,
          int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertVirtualClassStudents(VirtualClassStudentId VirtualClassStudent);

        void UpdateVirtualClassStudents(VirtualClassStudentId VirtualClassStudent);

        void DeleteVirtualClassStudents(int VirtualClassStudentId = 0);



        List<VirtualClassSession> GetAllVirtualClassSessions(ref int totalcount,
         int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertVirtualClassSession(VirtualClassSession VirtualClassSession);

        void UpdateVirtualClassSession(VirtualClassSession VirtualClassSession);

        void DeleteVirtualClassSession(int VirtualClassSessionId = 0);
        

        #endregion

        #region StandardMaster

        StandardMaster GetStandardMasterById(int StandardMasterId);

        List<StandardMaster> GetAllStandardMasters(ref int totalcount, string Standard = "", decimal? standardindex = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStandardMaster(StandardMaster StandardMaster);

        void UpdateStandardMaster(StandardMaster StandardMaster);

        void DeleteStandardMaster(int StandardMasterId = 0);

        #endregion

        #region Section

        Section GetSectionById(int SectionId);

        List<Section> GetAllSections(ref int totalcount, string Section = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertSection(Section Section);

        void UpdateSection(Section Section);

        void DeleteSection(int SectionId = 0);

        #endregion

        #endregion

        #region Gurdian sub-module

        #region Qualification

        Qualification GetQualificationById(int QualificationId);

        List<Qualification> GetAllQualifications(ref int totalcount, string Qualification = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertQualification(Qualification Qualification);

        void UpdateQualification(Qualification Qualification);

        void DeleteQualification(int QualificationId = 0);

        #endregion

        #region Occupation

        Occupation GetOccupationById(int OccupationId);

        List<Occupation> GetAllOccupations(ref int totalcount, string Occupation = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertOccupation(Occupation Occupation);

        void UpdateOccupation(Occupation Occupation);

        void DeleteOccupation(int OccupationId = 0);

        #endregion

        #endregion

        #region Board

        Board GetBoardById(int BoardId);

        List<Board> GetAllBoards(ref int totalcount, string Board = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertBoard(Board Board);

        void UpdateBoard(Board Board);

        void DeleteBoard(int BoardId = 0);

        #endregion

        #region CommonText

        CommonText GetCommonTextById(int BoardId);

        List<CommonText> GetAllCommonTexts(ref int totalcount, string FormName = "", string TextFieldName = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertCommonText(CommonText CommonText);

        void UpdateCommonText(CommonText CommonText);

        void DeleteCommonText(int CommonTextId = 0);

        #endregion

        #region Language

        Language GetLanguageById(int LanguageId);

        List<Language> GetAllLanguages(ref int totalcount, string Language = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertLanguage(Language Language);

        void UpdateLanguage(Language Language);

        void DeleteLanguage(int LanguageId = 0);

        #endregion

        #region
        StudentCategory GetStudentCategoryById(int StudentCategoryId);
        #endregion

        #region commonMethods
        string TrimStudentName(Student stu);

        IList<DateTime> GetMonthCountBetweenDates(ref int Count, DateTime StartDate, DateTime EndDate);

        IList<DateTime> GetSundayCountBetweenDates(ref int Count, DateTime StartDate, DateTime EndDate);
        #endregion

    }
}
