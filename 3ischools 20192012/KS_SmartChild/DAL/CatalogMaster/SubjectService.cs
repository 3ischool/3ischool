﻿using KS_SmartChild.DAL.ClassPeriodModule;
using KS_SmartChild.DAL.Common;
using KSModel.Models;
using KS_SmartChild.ViewModel.Attendence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI.WebControls;

namespace KS_SmartChild.DAL.CatalogMaster
{
    public partial class SubjectService : ISubjectService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;
        //private readonly string SqlDataSource;
        public SubjectService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods

        #region Subject

        public KSModel.Models.Subject GetSubjectById(int SubjectId, bool IsTrack = false)
        {
            if (SubjectId == 0)
                throw new ArgumentNullException("Subject");

            Subject Subject = new Subject();
            if (IsTrack)
                Subject = (from s in db.Subjects where s.SubjectId == SubjectId select s).FirstOrDefault();
            else
                Subject = (from s in db.Subjects.AsNoTracking() where s.SubjectId == SubjectId select s).FirstOrDefault();

            return Subject;
        }

        public List<KSModel.Models.Subject> GetAllSubjects(ref int totalcount, string Subject = "", string SubjectCode = null, bool? IsCore = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Subjects.ToList();
            if (!String.IsNullOrEmpty(Subject))
                query = query.Where(e => e.Subject1.ToLower().Contains(Subject.ToLower())).ToList();
            if (!String.IsNullOrEmpty(SubjectCode))
                query = query.Where(e => e.SubjectCode != null && e.SubjectCode.ToLower().Contains(SubjectCode.ToLower())).ToList();
            if (IsCore != null)
            {
                if ((bool)IsCore)
                    query = query.Where(e => e.IsCore == IsCore).ToList();
                else
                    query = query.Where(e => e.IsCore == IsCore || e.IsCore == null).ToList();
            }
            else
                query = query.Where(e => e.IsCore == true || e.IsCore == false || e.IsCore == null).ToList();

            totalcount = query.Count;
            var Subjects = new PagedList<KSModel.Models.Subject>(query, PageIndex, PageSize);
            return Subjects;
        }

        public void InsertSubject(KSModel.Models.Subject Subject)
        {
            if (Subject == null)
                throw new ArgumentNullException("Subject");

            db.Subjects.Add(Subject);
            db.SaveChanges();
        }

        public void UpdateSubject(KSModel.Models.Subject Subject)
        {
            if (Subject == null)
                throw new ArgumentNullException("Subject");

            db.Entry(Subject).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSubject(int SubjectId = 0)
        {
            if (SubjectId == 0)
                throw new ArgumentNullException("Subject");

            var Subject = (from s in db.Subjects where s.SubjectId == SubjectId select s).FirstOrDefault();
            db.Subjects.Remove(Subject);
            db.SaveChanges();
        }

        #endregion

        #region Skill

        public Skill GetSkillById(int SkillId, bool IsTrack = false)
        {
            if (SkillId == 0)
                throw new ArgumentNullException("Subject");

            Skill Skill = new Skill();
            if (IsTrack)
                Skill = (from s in db.Skills where s.SkillId == SkillId select s).FirstOrDefault();
            else
                Skill = (from s in db.Skills.AsNoTracking() where s.SkillId == SkillId select s).FirstOrDefault();

            return Skill;
        }

        public List<Skill> GetAllSkills(ref int totalcount, string Skill = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Skills.ToList();
            if (!String.IsNullOrEmpty(Skill))
                query = query.Where(e => e.Skill1.ToLower().Contains(Skill.ToLower())).ToList();

            totalcount = query.Count;
            var Skills = new PagedList<KSModel.Models.Skill>(query, PageIndex, PageSize);
            return Skills;
        }

        public void InsertSkill(Skill Skill)
        {
            if (Skill == null)
                throw new ArgumentNullException("Skill");

            db.Skills.Add(Skill);
            db.SaveChanges();
        }

        public void UpdateSkill(Skill Skill)
        {
            if (Skill == null)
                throw new ArgumentNullException("Skill");

            db.Entry(Skill).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSkill(int SkillId = 0)
        {
            if (SkillId == 0)
                throw new ArgumentNullException("Skill");

            var Skill = (from s in db.Skills where s.SkillId == SkillId select s).FirstOrDefault();
            db.Skills.Remove(Skill);
            db.SaveChanges();
        }

        #endregion

        #region SubjectSkill

        public SubjectSkill GetSubjectSkillById(int SubjectSkillId, bool IsTrack = false)
        {
            if (SubjectSkillId == 0)
                throw new ArgumentNullException("Subject");

            SubjectSkill SubjectSkill = new SubjectSkill();
            if (IsTrack)
                SubjectSkill = (from s in db.SubjectSkills where s.SubjectSkillId == SubjectSkillId select s).FirstOrDefault();
            else
                SubjectSkill = (from s in db.SubjectSkills.AsNoTracking() where s.SubjectSkillId == SubjectSkillId select s).FirstOrDefault();

            return SubjectSkill;
        }

        public List<SubjectSkill> GetAllSubjectSkills(ref int totalcount, string SubjectSkill = "", int? SubjectId = null, int? SkillId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SubjectSkills.ToList();
            if (!String.IsNullOrEmpty(SubjectSkill))
                query = query.Where(e => e.SubjectSkill1.ToLower().Contains(SubjectSkill.ToLower())).ToList();
            if (SubjectId != null && SubjectId > 0)
                query = query.Where(e => e.SubjectId == SubjectId).ToList();
            if (SkillId != null && SkillId > 0)
                query = query.Where(e => e.SkillId == SkillId).ToList();

            totalcount = query.Count;
            var SubjectSkills = new PagedList<KSModel.Models.SubjectSkill>(query, PageIndex, PageSize);
            return SubjectSkills;
        }

        public void InsertSubjectSkill(SubjectSkill SubjectSkill)
        {
            if (SubjectSkill == null)
                throw new ArgumentNullException("SubjectSkill");

            db.SubjectSkills.Add(SubjectSkill);
            db.SaveChanges();
        }

        public void UpdateSubjectSkill(SubjectSkill SubjectSkill)
        {
            if (SubjectSkill == null)
                throw new ArgumentNullException("SubjectSkill");

            db.Entry(SubjectSkill).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSubjectSkill(int SubjectSkillId = 0)
        {
            if (SubjectSkillId == 0)
                throw new ArgumentNullException("SubjectSkill");

            var SubjectSkill = (from s in db.SubjectSkills where s.SubjectSkillId == SubjectSkillId select s).FirstOrDefault();
            db.SubjectSkills.Remove(SubjectSkill);
            db.SaveChanges();
        }

        #endregion

        #region SubjectType

        public KSModel.Models.SubjectType GetSubjectTypeById(int SubjectTypeId, bool IsTrack = false)
        {
            if (SubjectTypeId == 0)
                throw new ArgumentNullException("SubjectType");

            SubjectType SubjectType = new SubjectType();
            if (IsTrack)
                SubjectType = (from s in db.SubjectTypes where s.SubjectTypeId == SubjectTypeId select s).FirstOrDefault();
            else
                SubjectType = (from s in db.SubjectTypes.AsNoTracking() where s.SubjectTypeId == SubjectTypeId select s).FirstOrDefault();

            return SubjectType;
        }

        public List<KSModel.Models.SubjectType> GetAllSubjectTypes(ref int totalcount, string SubjectType = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SubjectTypes.ToList();
            if (!String.IsNullOrEmpty(SubjectType))
                query = query.Where(e => e.SubjectType1.ToLower().Contains(SubjectType.ToLower())).ToList();

            totalcount = query.Count;
            var SubjectTypes = new PagedList<KSModel.Models.SubjectType>(query, PageIndex, PageSize);
            return SubjectTypes;
        }

        public void InsertSubjectType(KSModel.Models.SubjectType SubjectType)
        {
            if (SubjectType == null)
                throw new ArgumentNullException("SubjectType");

            db.SubjectTypes.Add(SubjectType);
            db.SaveChanges();
        }

        public void UpdateSubjectType(KSModel.Models.SubjectType SubjectType)
        {
            if (SubjectType == null)
                throw new ArgumentNullException("SubjectType");

            db.Entry(SubjectType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSubjectType(int SubjectTypeId = 0)
        {
            if (SubjectTypeId == 0)
                throw new ArgumentNullException("SubjectType");

            var SubjectType = (from s in db.SubjectTypes where s.SubjectTypeId == SubjectTypeId select s).FirstOrDefault();
            db.SubjectTypes.Remove(SubjectType);
            db.SaveChanges();
        }

        #endregion

        #region  StandardSubject

        public StandardSubject GetStandardSubjectById(int StandardSubjectId, bool IsTrack = false)
        {
            if (StandardSubjectId == 0)
                throw new ArgumentNullException("StandardSubject");

            StandardSubject StandardSubject = new StandardSubject();
            if (IsTrack)
                StandardSubject = (from s in db.StandardSubjects where s.StandardSubjectId == StandardSubjectId select s).FirstOrDefault();
            else
                StandardSubject = (from s in db.StandardSubjects.AsNoTracking() where s.StandardSubjectId == StandardSubjectId select s).FirstOrDefault();

            return StandardSubject;
        }

        public List<StandardSubject> GetAllStandardSubjects(ref int totalcount, int? StandardId = null, int? SubjectId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StandardSubjects.ToList();
            if (StandardId != null && StandardId > 0)
                query = query.Where(e => e.StandardId == StandardId).ToList();
            if (SubjectId != null && SubjectId > 0)
                query = query.Where(e => e.SubjectId == SubjectId).ToList();

            totalcount = query.Count;
            var StandardSubjects = new PagedList<KSModel.Models.StandardSubject>(query, PageIndex, PageSize);
            return StandardSubjects;
        }

        public void InsertStandardSubject(StandardSubject StandardSubject)
        {
            if (StandardSubject == null)
                throw new ArgumentNullException("StandardSubject");

            db.StandardSubjects.Add(StandardSubject);
            db.SaveChanges();
        }

        public void UpdateStandardSubject(StandardSubject StandardSubject)
        {
            if (StandardSubject == null)
                throw new ArgumentNullException("StandardSubject");

            db.Entry(StandardSubject).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStandardSubject(int StandardSubjectId = 0)
        {
            if (StandardSubjectId == 0)
                throw new ArgumentNullException("StandardSubject");

            var StandardSubject = (from s in db.StandardSubjects where s.StandardSubjectId == StandardSubjectId select s).FirstOrDefault();
            db.StandardSubjects.Remove(StandardSubject);
            db.SaveChanges();
        }

        #endregion

        #region ClassSubject

        public ClassSubject GetClassSubjectById(int ClassSubjectId, bool IsTrack = false)
        {
            if (ClassSubjectId == 0)
                throw new ArgumentNullException("ClassSubject");

            ClassSubject ClassSubject = new ClassSubject();
            if (IsTrack)
                ClassSubject = (from s in db.ClassSubjects where s.ClassSubjectId == ClassSubjectId select s).FirstOrDefault();
            else
                ClassSubject = (from s in db.ClassSubjects.AsNoTracking() where s.ClassSubjectId == ClassSubjectId select s).FirstOrDefault();

            return ClassSubject;
        }

        public List<ClassSubject> GetAllClassSubjects(ref int totalcount, int? ClassId = null, int? VirtualClassId = null, int? SubjectId = null, int? SubjectTypeId = null,
            bool? IsActive = null, int? SkillId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ClassSubjects.ToList();
            if (ClassId != null && ClassId > 0)
                query = query.Where(e => e.ClassId == ClassId).ToList();
            if (SubjectId != null && SubjectId > 0)
                query = query.Where(e => e.SubjectId == SubjectId).ToList();
            if (SubjectTypeId != null && SubjectTypeId > 0)
                query = query.Where(e => e.SubjectTypeId == SubjectTypeId).ToList();
            if (VirtualClassId != null && VirtualClassId > 0)
                query = query.Where(e => e.VirtualClassId == VirtualClassId).ToList();
            if (IsActive != null)
            {
                if ((bool)IsActive)
                    query = query.Where(c => c.IsActive == true).ToList();
                else
                    query = query.Where(c => c.IsActive == false).ToList();
            }
            else
                query = query.Where(c => c.IsActive == null || c.IsActive == true || c.IsActive == false).ToList();
            if (SkillId != null && SkillId > 0)
                query = query.Where(e => e.SkillId == SkillId).ToList();

            totalcount = query.Count;
            var ClassSubjects = new PagedList<KSModel.Models.ClassSubject>(query, PageIndex, PageSize);
            return ClassSubjects;
        }

        public void InsertClassSubject(ClassSubject ClassSubject)
        {
            if (ClassSubject == null)
                throw new ArgumentNullException("ClassSubject");

            db.ClassSubjects.Add(ClassSubject);
            db.SaveChanges();
        }

        public void UpdateClassSubject(ClassSubject ClassSubject)
        {
            if (ClassSubject == null)
                throw new ArgumentNullException("ClassSubject");

            db.Entry(ClassSubject).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteClassSubject(int ClassSubjectId = 0)
        {
            if (ClassSubjectId == 0)
                throw new ArgumentNullException("ClassSubject");

            var ClassSubject = (from s in db.ClassSubjects where s.ClassSubjectId == ClassSubjectId select s).FirstOrDefault();
            db.ClassSubjects.Remove(ClassSubject);
            db.SaveChanges();
        }

        #endregion

        #region StudentClassSubjects

        public StudentClassSubject GetStudentClassSubjectId(int StudentClassSubjectId, bool IsTrack = false)
        {
            if (StudentClassSubjectId == 0)
                throw new ArgumentNullException("StudentClassSubject");

            StudentClassSubject StudentClassSubject = new StudentClassSubject();
            if (IsTrack)
                StudentClassSubject = (from s in db.StudentClassSubjects where s.StudentClassSubjectsId == StudentClassSubjectId select s).FirstOrDefault();
            else
                StudentClassSubject = (from s in db.StudentClassSubjects.AsNoTracking() where s.StudentClassSubjectsId == StudentClassSubjectId select s).FirstOrDefault();

            return StudentClassSubject;
        }

        public List<StudentClassSubject> GetAllStudentClassSubjects(ref int totalcount, int? StudentClassId = null, 
                                        int? ClassSubjectId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StudentClassSubjects.ToList();
            if (StudentClassId != null && StudentClassId > 0)
                query = query.Where(e => e.StudentClassId == StudentClassId).ToList();
            if (ClassSubjectId != null && ClassSubjectId > 0)
                query = query.Where(e => e.ClassSubjectId == ClassSubjectId).ToList();

            totalcount = query.Count;
            var StudentClassSubjects = new PagedList<KSModel.Models.StudentClassSubject>(query, PageIndex, PageSize);
            return StudentClassSubjects;
        }

        public void InsertStudentClassSubject(StudentClassSubject StudentClassSubject)
        {
            if (StudentClassSubject == null)
                throw new ArgumentNullException("StudentClassSubject");

            db.StudentClassSubjects.Add(StudentClassSubject);
            db.SaveChanges();
        }

        public void UpdateStudentClassSubject(StudentClassSubject StudentClassSubject)
        {
            if (StudentClassSubject == null)
                throw new ArgumentNullException("StudentClassSubject");

            db.Entry(StudentClassSubject).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStudentClassSubject(int StudentClassSubjectId = 0)
        {
            if (StudentClassSubjectId == 0)
                throw new ArgumentNullException("StudentClassSubject");

            var StudentClassSubject = (from s in db.StudentClassSubjects 
                                        where s.StudentClassSubjectsId == StudentClassSubjectId select s).FirstOrDefault();
            db.StudentClassSubjects.Remove(StudentClassSubject);
            db.SaveChanges();
        }

        public void DeleteStudentClassSubjectThroStuClassId(int StudentClassId = 0)
        {
            if (StudentClassId == 0)
                throw new ArgumentNullException("StudentClassSubject");

            var StudentClasses = (from s in db.StudentClassSubjects
                                       where s.StudentClassId == StudentClassId select s).ToList();
            foreach (var ids in StudentClasses)
            {
                var StudentClassSubject = (from s in db.StudentClassSubjects
                                           where s.StudentClassId == StudentClassId && s.StudentClassSubjectsId == ids.StudentClassSubjectsId
                                          select s).FirstOrDefault();

                db.StudentClassSubjects.Remove(StudentClassSubject);
            }
            db.SaveChanges();
        }

        public void DeleteStudentClassSubjectsList(List<StudentClassSubject> studentClassSubjects)
        {

            //IEnumerable<StudentClassSubject> list = from s in db.StudentClassSubjects
            //                           where  s.StudentClassSubjectsId == ids.StudentClassSubjectsId).toList();
            // Use Remove Range function to delete all records at once
            db.StudentClassSubjects.RemoveRange(studentClassSubjects);
            // Save changes
            db.SaveChanges();
         
        }

        public void AddStudentClassSubjectsList(List<StudentClassSubject> studentClassSubjects)
        {

            //IEnumerable<StudentClassSubject> list = from s in db.StudentClassSubjects
            //                           where  s.StudentClassSubjectsId == ids.StudentClassSubjectsId).toList();
            // Use Remove Range function to delete all records at once
            db.StudentClassSubjects.AddRange(studentClassSubjects);
            // Save changes
            db.SaveChanges();

        }

        #endregion

        #region ClassSubjectDetail

        public ClassSubjectDetail GetClassSubjectDetailById(int ClassSubjectDetailId, bool IsTrack = false)
        {
            if (ClassSubjectDetailId == 0)
                throw new ArgumentNullException("ClassSubjectDetail");

            ClassSubjectDetail ClassSubjectDetail = new ClassSubjectDetail();
            if (IsTrack)
                ClassSubjectDetail = (from s in db.ClassSubjectDetails where s.ClassSubjectDetailId == ClassSubjectDetailId select s).FirstOrDefault();
            else
                ClassSubjectDetail = (from s in db.ClassSubjectDetails.AsNoTracking() where s.ClassSubjectDetailId == ClassSubjectDetailId select s).FirstOrDefault();

            return ClassSubjectDetail;
        }

        public List<ClassSubjectDetail> GetAllClassSubjectDetails(ref int totalcount, int? ClassSubjectId = null, int? EntryType = null,
            DateTime? EntryDate = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ClassSubjectDetails.ToList();
            if (ClassSubjectId != null && ClassSubjectId > 0)
                query = query.Where(e => e.ClassSubjectID == ClassSubjectId).ToList();
            if (EntryType != null && EntryType > 0)
                query = query.Where(e => e.EntryType == EntryType).ToList();
            if (EntryDate.HasValue)
                query = query.Where(e => e.EntryDate == EntryDate).ToList();

            totalcount = query.Count;
            var ClassSubjectDetails = new PagedList<KSModel.Models.ClassSubjectDetail>(query, PageIndex, PageSize);
            return ClassSubjectDetails;
        }

        public void InsertClassSubjectDetail(ClassSubjectDetail ClassSubjectDetail)
        {
            if (ClassSubjectDetail == null)
                throw new ArgumentNullException("ClassSubjectDetail");

            db.ClassSubjectDetails.Add(ClassSubjectDetail);
            db.SaveChanges();
        }

        public void UpdateClassSubjectDetail(ClassSubjectDetail ClassSubjectDetail)
        {
            if (ClassSubjectDetail == null)
                throw new ArgumentNullException("ClassSubjectDetail");

            db.Entry(ClassSubjectDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteClassSubjectDetail(int ClassSubjectDetailId = 0)
        {
            if (ClassSubjectDetailId == 0)
                throw new ArgumentNullException("ClassSubjectDetail");

            var ClassSubjectDetail = (from s in db.ClassSubjectDetails where s.ClassSubjectDetailId == ClassSubjectDetailId select s).FirstOrDefault();
            db.ClassSubjectDetails.Remove(ClassSubjectDetail);
            db.SaveChanges();
        }

        #endregion

        #region Attendance

        public Attendance GetAttendanceById(int AttendanceId, bool IsTrack = false)
        {
            if (AttendanceId == 0)
                throw new ArgumentNullException("Attendance");

            Attendance Attendance = new Attendance();
            if (IsTrack)
                Attendance = (from s in db.Attendances where s.AttendanceId == AttendanceId select s).FirstOrDefault();
            else
                Attendance = (from s in db.Attendances.AsNoTracking() where s.AttendanceId == AttendanceId select s).FirstOrDefault();
            return Attendance;
        }

        public IList<Attendance> GetAllAttendances(ref int totalcount, int? StudentId = null, DateTime? Date = null,
            int? AttendanceStatusId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Attendances.ToList();
            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();
            if (Date.HasValue)
                query = query.Where(e => e.AttendanceDate == Date.Value.Date).ToList();
            if (AttendanceStatusId != null && AttendanceStatusId > 0)
                query = query.Where(e => e.AttendanceStatusId == AttendanceStatusId).ToList();

            totalcount = query.Count();
            var Attendances = new PagedList<KSModel.Models.Attendance>(query, PageIndex, PageSize);
            return Attendances;
        }

        public void InsertAttendance(Attendance Attendance)
        {
            if (Attendance == null)
                throw new ArgumentNullException("Attendance");

            db.Attendances.Add(Attendance);
            db.SaveChanges();
        }

        public void UpdateAttendance(Attendance Attendance)
        {
            if (Attendance == null)
                throw new ArgumentNullException("Attendance");

            db.Entry(Attendance).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAttendance(int AttendanceId = 0)
        {
            if (AttendanceId == 0)
                throw new ArgumentNullException("Attendance");

            var Attendance = (from s in db.Attendances where s.AttendanceId == AttendanceId select s).FirstOrDefault();
            db.Attendances.Remove(Attendance);
            db.SaveChanges();
        }

        #endregion

        #region AttendanceStatus

        public AttendanceStatu GetAttendanceStatusById(int AttendanceStatusId, bool IsTrack = false)
        {
            if (AttendanceStatusId == 0)
                throw new ArgumentNullException("AttendanceStatus");

            AttendanceStatu AttendanceStatus = new AttendanceStatu();
            if (IsTrack)
                AttendanceStatus = (from s in db.AttendanceStatus where s.AttendanceStatusId == AttendanceStatusId select s).FirstOrDefault();
            else
                AttendanceStatus = (from s in db.AttendanceStatus.AsNoTracking() where s.AttendanceStatusId == AttendanceStatusId select s).FirstOrDefault();

            return AttendanceStatus;
        }

        public List<AttendanceStatu> GetAllAttendanceStatus(ref int totalcount, string AttnStatus = "", int? FromPeriodId = null, int? TillPeriodId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AttendanceStatus.ToList();
            if (!String.IsNullOrEmpty(AttnStatus))
                query = query.Where(e => e.AttendanceStatus.ToLower().Contains(AttnStatus.ToLower())).ToList();
            if (FromPeriodId != null && FromPeriodId > 0)
                query = query.Where(a => a.FromPeriodId == FromPeriodId).ToList();
            if (TillPeriodId != null && TillPeriodId > 0)
                query = query.Where(a => a.TillPeriodId == TillPeriodId).ToList();

            totalcount = query.Count;
            var AttendanceStatus = new PagedList<KSModel.Models.AttendanceStatu>(query, PageIndex, PageSize);
            return AttendanceStatus;
        }

        public void InsertAttendanceStatus(AttendanceStatu AttendanceStatus)
        {
            if (AttendanceStatus == null)
                throw new ArgumentNullException("AttendanceStatus");

            db.AttendanceStatus.Add(AttendanceStatus);
            db.SaveChanges();
        }

        public void UpdateAttendanceStatus(AttendanceStatu AttendanceStatus)
        {
            if (AttendanceStatus == null)
                throw new ArgumentNullException("AttendanceStatus");

            db.Entry(AttendanceStatus).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAttendanceStatus(int AttendanceStatusId = 0)
        {
            if (AttendanceStatusId == 0)
                throw new ArgumentNullException("AttendanceStatus");

            var AttendanceStatus = (from s in db.AttendanceStatus where s.AttendanceStatusId == AttendanceStatusId select s).FirstOrDefault();
            db.AttendanceStatus.Remove(AttendanceStatus);
            db.SaveChanges();
        }

        #endregion

        #region  AttendancePeriodWise

        public AttendancePeriodWise GetAttendancePeriodWiseById(int AttendancePeriodWiseId, bool IsTrack = false)
        {
            if (AttendancePeriodWiseId == 0)
                throw new ArgumentNullException("AttendancePeriodWise");

            AttendancePeriodWise AttendancePeriodWise = new AttendancePeriodWise();
            if (IsTrack)
                AttendancePeriodWise = (from s in db.AttendancePeriodWises where s.AttendancePeriodId == AttendancePeriodWiseId select s).FirstOrDefault();
            else
                AttendancePeriodWise = (from s in db.AttendancePeriodWises.AsNoTracking() where s.AttendancePeriodId == AttendancePeriodWiseId select s).FirstOrDefault();

            return AttendancePeriodWise;
        }

        public List<AttendancePeriodWise> GetAllAttendancePeriodWise(ref int totalcount, int? AttendanceId = null, int? PeriodId = null, int? AttendanceStatusId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AttendancePeriodWises.ToList();
            if (AttendanceId != null && AttendanceId > 0)
                query = query.Where(e => e.AttendanceId == AttendanceId).ToList();
            if (PeriodId != null && PeriodId > 0)
                query = query.Where(e => e.PeriodId == PeriodId).ToList();
            if (AttendanceStatusId != null && AttendanceStatusId > 0)
                query = query.Where(e => e.AttendanceStatusId == AttendanceStatusId).ToList();

            totalcount = query.Count;
            var AttendancePeriodWises = new PagedList<KSModel.Models.AttendancePeriodWise>(query, PageIndex, PageSize);
            return AttendancePeriodWises;
        }

        public void InsertAttendancePeriodWise(AttendancePeriodWise AttendancePeriodWise)
        {
            if (AttendancePeriodWise == null)
                throw new ArgumentNullException("AttendancePeriodWise");

            db.AttendancePeriodWises.Add(AttendancePeriodWise);
            db.SaveChanges();
        }

        public void UpdateAttendancePeriodWise(AttendancePeriodWise AttendancePeriodWise)
        {
            if (AttendancePeriodWise == null)
                throw new ArgumentNullException("AttendancePeriodWise");

            db.Entry(AttendancePeriodWise).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAttendancePeriodWise(int AttendancePeriodWiseId = 0)
        {
            if (AttendancePeriodWiseId == 0)
                throw new ArgumentNullException("AttendancePeriodWise");

            var AttendancePeriodWise = (from s in db.AttendancePeriodWises where s.AttendancePeriodId == AttendancePeriodWiseId select s).FirstOrDefault();
            db.AttendancePeriodWises.Remove(AttendancePeriodWise);
            db.SaveChanges();
        }



        #endregion

        #region StudentLeaveApplication
        public StudentLeaveApplication GetStudentLeaveApplicationById(int StudentLeaveApplicationId, bool IsTrack = false)
        {
            if (StudentLeaveApplicationId == 0)
                throw new ArgumentNullException("StudentLeaveApplication");

            StudentLeaveApplication LeaveApplication = new StudentLeaveApplication();
            if (IsTrack)
                LeaveApplication = (from s in db.StudentLeaveApplications where s.StudentLeaveApplicationId == StudentLeaveApplicationId select s).FirstOrDefault();
            else
                LeaveApplication = (from s in db.StudentLeaveApplications.AsNoTracking() where s.StudentLeaveApplicationId == StudentLeaveApplicationId select s).FirstOrDefault();

            return LeaveApplication;
        }

        public List<StudentLeaveApplication> GetAllStudentLeaveApplication(ref int totalcount, int? StudentId = default(int?), 
            DateTime? LeaveFrom = default(DateTime?),DateTime? LeaveTill = default(DateTime?),
            DateTime? AppliedDate = default(DateTime?), string Reason = null, int? LeaveTypeId = default(int?),
            int? ApprovedBy = default(int?), DateTime? ApprovedOn = default(DateTime?), 
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StudentLeaveApplications.ToList();
            if (!String.IsNullOrEmpty(Reason))
                query = query.Where(e => e.Reason.ToLower().Contains(Reason.ToLower())).ToList();
            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();
            if (LeaveTypeId != null && LeaveTypeId > 0)
                query = query.Where(e => e.LeaveTypeId == LeaveTypeId).ToList();
            if (ApprovedBy != null && ApprovedBy > 0)
                query = query.Where(e => e.ApprovedBy == ApprovedBy).ToList();
            if (LeaveFrom.HasValue)
                query.Where(e => e.LeaveFrom == LeaveFrom).ToList();
            if (LeaveTill.HasValue)
                query.Where(e => e.LeaveTill == LeaveTill).ToList();
            if (AppliedDate.HasValue)
                query.Where(e => e.AppliedDate == AppliedDate).ToList();
            //if (ApprovedOn.HasValue)
            //    query.Where(e => e.ApprovedOn == ApprovedOn).ToList();

            totalcount = query.Count;
            var studentleaveapplications = new PagedList<KSModel.Models.StudentLeaveApplication>(query, PageIndex, PageSize);
            return studentleaveapplications;
        }

        public void InsertStudentLeaveApplication(StudentLeaveApplication StudentLeave)
        {
            if (StudentLeave == null)
                throw new ArgumentNullException("StudentLeaveApplication");

            db.StudentLeaveApplications.Add(StudentLeave);
            db.SaveChanges();
        }

        public void UpdateStudentLeaveApplication(StudentLeaveApplication StudentLeave)
        {
            if (StudentLeave == null)
                throw new ArgumentNullException("StudentLeaveApplication");

            db.Entry(StudentLeave).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStudentLeaveApplication(int StudentLeaveApplicationId = 0)
        {
            if (StudentLeaveApplicationId == 0)
                throw new ArgumentNullException("StudentLeaveApplication");

            var StudentLeaveApplication = (from s in db.StudentLeaveApplications where s.StudentLeaveApplicationId == StudentLeaveApplicationId select s).FirstOrDefault();
            db.StudentLeaveApplications.Remove(StudentLeaveApplication);
            db.SaveChanges();
        }
        #endregion

        #region AttendanceCard



        public AttendanceDeviceLocation GetAttendaceLocationDevicebyId(string qsid)
        {
            int id = Convert.ToInt32(qsid);
            var abc = db.AttendanceDeviceLocations.Where(p => p.AttendanceDeviceLocationId == id).SingleOrDefault();
            return abc;
        }
        //for AttendanceDevice
        public List<AttendanceDevice> GetAllDevice(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var abc = new List<AttendanceDevice>();
            abc = db.AttendanceDevices.ToList();
            return abc;
        }
        public List<AttendanceLocation> GetAllLocations(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var abc = new List<AttendanceLocation>();
            abc = db.AttendanceLocations.ToList();
            return abc;

        }





        #endregion

        #region AttendanceDeviceType
        public List<AttendanceDeviceType> GetAllAttendanceDeviceType(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AttendanceDeviceTypes.ToList();

            totalcount = query.Count;
            var AttendanceDeviceTypes = new PagedList<KSModel.Models.AttendanceDeviceType>(query, PageIndex, PageSize);
            return AttendanceDeviceTypes;
        }

        public KSModel.Models.AttendanceDeviceType GetAttendanceDeviceTypeById(int AttendanceDeviceTypeId, bool IsTrack = false)
        {
            if (AttendanceDeviceTypeId == 0)
                throw new ArgumentNullException("Subject");

            AttendanceDeviceType AttendanceDeviceType = new AttendanceDeviceType();
            if (IsTrack)
                AttendanceDeviceType = (from s in db.AttendanceDeviceTypes where s.AttendanceDeviceTypeId == AttendanceDeviceTypeId select s).FirstOrDefault();
            else
                AttendanceDeviceType = (from s in db.AttendanceDeviceTypes.AsNoTracking() where s.AttendanceDeviceTypeId == AttendanceDeviceTypeId select s).FirstOrDefault();

            return AttendanceDeviceType;
        }
        //for AttendanceDeviceLocation
        public void DeleteAttendanceDeviceLocation(string id)
        {
            int ids = Convert.ToInt32(id);
            var AttendanceDevice = (from s in db.AttendanceDeviceLocations where s.AttendanceDeviceLocationId == ids select s).FirstOrDefault();
            db.AttendanceDeviceLocations.Remove(AttendanceDevice);
            db.SaveChanges();
        }

        public void InsertAttendanceDeviceLocation(AttendanceDeviceLocation AttendanceDevicelocation)
        {
            if (AttendanceDevicelocation == null)
                throw new ArgumentNullException("AttendanceDevicelocation");

            db.AttendanceDeviceLocations.Add(AttendanceDevicelocation);
            db.SaveChanges();
        }

        public void UpdateAttendanceDeviceLocation(AttendanceDeviceLocation Attendancedevicelocation)
        {
            if (Attendancedevicelocation == null)
                throw new ArgumentNullException("Attendancedevicelocation");
            db.Entry(Attendancedevicelocation).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();

        }

        public List<AttendanceCardGridModel> GetAllAttendanceDeviceLocation()
        {
            var abc = new List<AttendanceCardGridModel>();
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("GetAllAttendanceDeviceLocation", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<AttendanceCardGridModel> GetallAttendance = new List<AttendanceCardGridModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AttendanceCardGridModel AttndncCardModel = new AttendanceCardGridModel();
                AttndncCardModel.EncAttendanceDeviceLocationId = dt.Rows[i]["AttendanceDeviceLocationId"].ToString();
                AttndncCardModel.Location = dt.Rows[i]["Location"].ToString();
                AttndncCardModel.EffectiveDate = dt.Rows[i]["EffectiveDate"].ToString();
                AttndncCardModel.EndDate = dt.Rows[i]["EndDate"].ToString();
                AttndncCardModel.Device = dt.Rows[i]["AttendanceDeviceType"].ToString();
                GetallAttendance.Add(AttndncCardModel);
            }
            return GetallAttendance;
        }

        public List<KSModel.Models.AttendanceDeviceLocation> GetAllAttendanceDeviceLocations(ref int totalcount,
          int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AttendanceDeviceLocations.ToList();

            totalcount = query.Count;
            var AttendanceDeviceLocations = new PagedList<KSModel.Models.AttendanceDeviceLocation>(query, PageIndex, PageSize);
            return AttendanceDeviceLocations;
        }

        public DataTable GetDevicebyDeviceID()
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_GetDevicebyDeviceID", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            return dt;
        }
        #endregion

        #region AttendanceDevice

        public KSModel.Models.AttendanceDevice GetAttendanceDeviceById(int AttendanceDeviceId, bool IsTrack = false)
        {
            if (AttendanceDeviceId == 0)
                throw new ArgumentNullException("AttendanceDevice");

            AttendanceDevice AttendanceDevice = new AttendanceDevice();
            if (IsTrack)
                AttendanceDevice = (from s in db.AttendanceDevices where s.AttendanceDeviceId == AttendanceDeviceId select s).FirstOrDefault();
            else
                AttendanceDevice = (from s in db.AttendanceDevices.AsNoTracking() where s.AttendanceDeviceId == AttendanceDeviceId select s).FirstOrDefault();

            return AttendanceDevice;
        }

        public List<KSModel.Models.AttendanceDevice> GetAllAttendanceDevices(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AttendanceDevices.ToList();

            totalcount = query.Count;
            var AttendanceDevices = new PagedList<KSModel.Models.AttendanceDevice>(query, PageIndex, PageSize);
            return AttendanceDevices;
        }

        public void InsertAttendanceDevice(KSModel.Models.AttendanceDevice AttendanceDevice)
        {
            if (AttendanceDevice == null)
                throw new ArgumentNullException("AttendanceDevice");

            db.AttendanceDevices.Add(AttendanceDevice);
            db.SaveChanges();
        }

        public void UpdateAttendanceDevice(KSModel.Models.AttendanceDevice AttendanceDevice)
        {
            if (AttendanceDevice == null)
                throw new ArgumentNullException("AttendanceDevice");

            db.Entry(AttendanceDevice).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAttendanceDevice(int AttendanceDeviceId = 0)
        {
            if (AttendanceDeviceId == 0)
                throw new ArgumentNullException("AttendanceDevice");

            var AttendanceDevice = (from s in db.AttendanceDevices where s.AttendanceDeviceId == AttendanceDeviceId select s).FirstOrDefault();
            db.AttendanceDevices.Remove(AttendanceDevice);
            db.SaveChanges();
        }

        #endregion

        #region ContactCardType

        public KSModel.Models.ContactCardType GetContactCardTypeById(int ContactCardTypeId, bool IsTrack = false)
        {
            if (ContactCardTypeId == 0)
                throw new ArgumentNullException("ContactCardType");

            ContactCardType ContactCardType = new ContactCardType();
            if (IsTrack)
                ContactCardType = (from s in db.ContactCardTypes where s.ContactCardTypeId == ContactCardTypeId select s).FirstOrDefault();
            else
                ContactCardType = (from s in db.ContactCardTypes.AsNoTracking() where s.ContactCardTypeId == ContactCardTypeId select s).FirstOrDefault();

            return ContactCardType;
        }

        public List<KSModel.Models.ContactCardType> GetAllContactCardTypes(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ContactCardTypes.ToList();

            totalcount = query.Count;
            var ContactCardTypes = new PagedList<KSModel.Models.ContactCardType>(query, PageIndex, PageSize);
            return ContactCardTypes;
        }

        public void InsertContactCardType(KSModel.Models.ContactCardType ContactCardType)
        {
            if (ContactCardType == null)
                throw new ArgumentNullException("ContactCardType");

            db.ContactCardTypes.Add(ContactCardType);
            db.SaveChanges();
        }

        public void UpdateContactCardType(KSModel.Models.ContactCardType ContactCardType)
        {
            if (ContactCardType == null)
                throw new ArgumentNullException("ContactCardType");

            db.Entry(ContactCardType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteContactCardType(int ContactCardTypeId = 0)
        {
            if (ContactCardTypeId == 0)
                throw new ArgumentNullException("ContactCardType");

            var ContactCardType = (from s in db.ContactCardTypes where s.ContactCardTypeId == ContactCardTypeId select s).FirstOrDefault();
            db.ContactCardTypes.Remove(ContactCardType);
            db.SaveChanges();
        }

        #endregion

        #region AttendanceCardType

        public KSModel.Models.AttendanceCardType GetAttendanceCardTypeById(int AttendanceCardTypeId, bool IsTrack = false)
        {
            if (AttendanceCardTypeId == 0)
                throw new ArgumentNullException("AttendanceCardType");

            AttendanceCardType AttendanceCardType = new AttendanceCardType();
            if (IsTrack)
                AttendanceCardType = (from s in db.AttendanceCardTypes where s.AttendanceCardTypeId == AttendanceCardTypeId select s).FirstOrDefault();
            else
                AttendanceCardType = (from s in db.AttendanceCardTypes.AsNoTracking() where s.AttendanceCardTypeId == AttendanceCardTypeId select s).FirstOrDefault();

            return AttendanceCardType;
        }

        public List<KSModel.Models.AttendanceCardType> GetAllAttendanceCardTypes(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AttendanceCardTypes.ToList();

            totalcount = query.Count;
            var AttendanceCardTypes = new PagedList<KSModel.Models.AttendanceCardType>(query, PageIndex, PageSize);
            return AttendanceCardTypes;
        }

        public void InsertAttendanceCardType(KSModel.Models.AttendanceCardType AttendanceCardType)
        {
            if (AttendanceCardType == null)
                throw new ArgumentNullException("AttendanceCardType");

            db.AttendanceCardTypes.Add(AttendanceCardType);
            db.SaveChanges();
        }

        public void UpdateAttendanceCardType(KSModel.Models.AttendanceCardType AttendanceCardType)
        {
            if (AttendanceCardType == null)
                throw new ArgumentNullException("AttendanceCardType");

            db.Entry(AttendanceCardType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAttendanceCardType(int AttendanceCardTypeId = 0)
        {
            if (AttendanceCardTypeId == 0)
                throw new ArgumentNullException("AttendanceCardType");

            var AttendanceCardType = (from s in db.AttendanceCardTypes where s.AttendanceCardTypeId == AttendanceCardTypeId select s).FirstOrDefault();
            db.AttendanceCardTypes.Remove(AttendanceCardType);
            db.SaveChanges();
        }

        #endregion

        #region AttendanceUserCard

        public KSModel.Models.AttendanceUserCard GetAttendanceUserCardById(int AttendanceUserCardId, bool IsTrack = false)
        {
            if (AttendanceUserCardId == 0)
                throw new ArgumentNullException("AttendanceUserCard");

            AttendanceUserCard AttendanceUserCard = new AttendanceUserCard();
            if (IsTrack)
                AttendanceUserCard = (from s in db.AttendanceUserCards where s.AttendanceUserCardId == AttendanceUserCardId select s).FirstOrDefault();
            else
                AttendanceUserCard = (from s in db.AttendanceUserCards.AsNoTracking() where s.AttendanceUserCardId == AttendanceUserCardId select s).FirstOrDefault();

            return AttendanceUserCard;
        }

        public List<KSModel.Models.AttendanceUserCard> GetAllAttendanceUserCards(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AttendanceUserCards.ToList();

            totalcount = query.Count;
            var AttendanceUserCards = new PagedList<KSModel.Models.AttendanceUserCard>(query, PageIndex, PageSize);
            return AttendanceUserCards;
        }

        public void InsertAttendanceUserCard(KSModel.Models.AttendanceUserCard AttendanceUserCard)
        {
            if (AttendanceUserCard == null)
                throw new ArgumentNullException("AttendanceUserCard");

            db.AttendanceUserCards.Add(AttendanceUserCard);
            db.SaveChanges();
        }

        public void UpdateAttendanceUserCard(KSModel.Models.AttendanceUserCard AttendanceUserCard)
        {
            if (AttendanceUserCard == null)
                throw new ArgumentNullException("AttendanceUserCard");

            db.Entry(AttendanceUserCard).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAttendanceUserCard(int AttendanceUserCardId = 0)
        {
            if (AttendanceUserCardId == 0)
                throw new ArgumentNullException("AttendanceUserCard");

            var AttendanceUserCard = (from s in db.AttendanceUserCards where s.AttendanceUserCardId == AttendanceUserCardId select s).FirstOrDefault();
            db.AttendanceUserCards.Remove(AttendanceUserCard);
            db.SaveChanges();
        }

        #endregion

        #region AttendanceCard

        public KSModel.Models.AttendanceCard GetAttendanceCardById(int AttendanceCardId, bool IsTrack = false)
        {
            if (AttendanceCardId == 0)
                throw new ArgumentNullException("AttendanceCard");

            AttendanceCard AttendanceCard = new AttendanceCard();
            if (IsTrack)
                AttendanceCard = (from s in db.AttendanceCards where s.AttendanceCardId == AttendanceCardId select s).FirstOrDefault();
            else
                AttendanceCard = (from s in db.AttendanceCards.AsNoTracking() where s.AttendanceCardId == AttendanceCardId select s).FirstOrDefault();

            return AttendanceCard;
        }

        public List<KSModel.Models.AttendanceCard> GetAllAttendanceCards(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AttendanceCards.ToList();

            totalcount = query.Count;
            var AttendanceCards = new PagedList<KSModel.Models.AttendanceCard>(query, PageIndex, PageSize);
            return AttendanceCards;
        }

        public void InsertAttendanceCard(KSModel.Models.AttendanceCard AttendanceCard)
        {
            if (AttendanceCard == null)
                throw new ArgumentNullException("AttendanceCard");

            db.AttendanceCards.Add(AttendanceCard);
            db.SaveChanges();
        }

        public void UpdateAttendanceCard(KSModel.Models.AttendanceCard AttendanceCard)
        {
            if (AttendanceCard == null)
                throw new ArgumentNullException("AttendanceCard");

            db.Entry(AttendanceCard).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAttendanceCard(int AttendanceCardId = 0)
        {
            if (AttendanceCardId == 0)
                throw new ArgumentNullException("AttendanceCard");

            var AttendanceCard = (from s in db.AttendanceCards where s.AttendanceCardId == AttendanceCardId select s).FirstOrDefault();
            db.AttendanceCards.Remove(AttendanceCard);
            db.SaveChanges();
        }

        #endregion

        #region AttendancePunch
        public IList<KSModel.Models.Staff> GetAttendancePunchMachineById(int? ExcelPunchId = null)
        {
            var query = db.Staffs.ToList();

            if (ExcelPunchId != null)
                query = query.Where(x => x.PunchMcId == ExcelPunchId).ToList();

            return query.ToList();
        }

        public IList<KSModel.Models.AttendancePunch> GetAllAttendancePunch(ref int totalcount,DateTime? PunchTime=null,
                                    int? PunchTypeId=null,int? StaffId = null,
                                    int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.AttendancePunches.ToList();

            if (PunchTime != null)
                query = query.Where(x => x.PunchTime == PunchTime).ToList();
            if( StaffId != null)
                query = query.Where(x => x.UserId == StaffId).ToList();
            
            totalcount = query.Count;
            var AttendancePunchs = new PagedList<KSModel.Models.AttendancePunch>(query, PageIndex, PageSize);
            return AttendancePunchs;
        }

        public void InsertAttendancePunch(KSModel.Models.AttendancePunch AttendancePunch)
        {
            if (AttendancePunch == null)
                throw new ArgumentNullException("AttendancePunch");

            db.AttendancePunches.Add(AttendancePunch);
            db.SaveChanges();
        }

        public void UpdateAttendancePunch(KSModel.Models.AttendancePunch AttendancePunch)
        {
            if (AttendancePunch == null)
                throw new ArgumentNullException("AttendancePunch");

            db.Entry(AttendancePunch).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteAttendancePunch(int PunchId = 0)
        {
            if (PunchId == 0)
                throw new ArgumentNullException("AttendancePunch");

            var AttendancePunch = (from s in db.AttendancePunches where s.PunchId == PunchId select s).FirstOrDefault();
            db.AttendancePunches.Remove(AttendancePunch);
            db.SaveChanges();
        }

        #endregion


        #region LessonPlan

        public KSModel.Models.LessonPlan GetLessonPlanById(int LessonPlanId, bool IsTrack = false)
        {
            if (LessonPlanId == 0)
                throw new ArgumentNullException("LessonPlan");

            LessonPlan LessonPlan = new LessonPlan();
            if (IsTrack)
                LessonPlan = (from s in db.LessonPlans where s.LessonPlanId == LessonPlanId select s).FirstOrDefault();
            else
                LessonPlan = (from s in db.LessonPlans.AsNoTracking() where s.LessonPlanId == LessonPlanId select s).FirstOrDefault();

            return LessonPlan;
        }

        public List<KSModel.Models.LessonPlan> GetAllLessonPlans(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.LessonPlans.ToList();

            totalcount = query.Count;
            var LessonPlans = new PagedList<KSModel.Models.LessonPlan>(query, PageIndex, PageSize);
            return LessonPlans;
        }

        public void InsertLessonPlan(KSModel.Models.LessonPlan LessonPlan)
        {
            if (LessonPlan == null)
                throw new ArgumentNullException("LessonPlan");

            db.LessonPlans.Add(LessonPlan);
            db.SaveChanges();
        }

        public void UpdateLessonPlan(KSModel.Models.LessonPlan LessonPlan)
        {
            if (LessonPlan == null)
                throw new ArgumentNullException("LessonPlan");

            db.Entry(LessonPlan).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteLessonPlan(int LessonPlanId = 0)
        {
            if (LessonPlanId == 0)
                throw new ArgumentNullException("LessonPlan");

            var LessonPlan = (from s in db.LessonPlans where s.LessonPlanId == LessonPlanId select s).FirstOrDefault();
            db.LessonPlans.Remove(LessonPlan);
            db.SaveChanges();
        }

        #endregion
        #endregion
    }
}