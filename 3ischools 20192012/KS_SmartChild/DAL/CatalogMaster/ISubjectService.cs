﻿using KSModel.Models;
using KS_SmartChild.ViewModel.Attendence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.CatalogMaster
{
    public partial interface ISubjectService
    {
        #region Subject

        Subject GetSubjectById(int SubjectId, bool IsTrack = false);

        List<Subject> GetAllSubjects(ref int totalcount, string Subject = "", string SubjectCode = null, bool? IsCore = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertSubject(Subject Subject);

        void UpdateSubject(Subject Subject);

        void DeleteSubject(int SubjectId = 0);

        #endregion

        #region Skill

        Skill GetSkillById(int SkillId, bool IsTrack = false);

        List<Skill> GetAllSkills(ref int totalcount, string Skill = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertSkill(Skill Skill);

        void UpdateSkill(Skill Skill);

        void DeleteSkill(int SkillId = 0);

        #endregion

        #region SubjectSkill

        SubjectSkill GetSubjectSkillById(int SubjectSkillId, bool IsTrack = false);

        List<SubjectSkill> GetAllSubjectSkills(ref int totalcount, string SubjectSkill = "", int? SubjectId = null, int? SkillId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertSubjectSkill(SubjectSkill SubjectSkill);

        void UpdateSubjectSkill(SubjectSkill SubjectSkill);

        void DeleteSubjectSkill(int SubjectSkillId = 0);

        #endregion

        #region SubjectType

        SubjectType GetSubjectTypeById(int SubjectTypeId, bool IsTrack = false);

        List<SubjectType> GetAllSubjectTypes(ref int totalcount, string SubjectType = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertSubjectType(SubjectType SubjectType);

        void UpdateSubjectType(SubjectType SubjectType);

        void DeleteSubjectType(int SubjectTypeId = 0);

        #endregion

        #region  StandardSubject

        StandardSubject GetStandardSubjectById(int StandardSubjectId, bool IsTrack = false);

        List<StandardSubject> GetAllStandardSubjects(ref int totalcount, int? StandardId = null, int? SubjectId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStandardSubject(StandardSubject StandardSubject);

        void UpdateStandardSubject(StandardSubject StandardSubject);

        void DeleteStandardSubject(int StandardSubjectId = 0);

        #endregion

        #region ClassSubject

        ClassSubject GetClassSubjectById(int ClassSubjectId, bool IsTrack = false);

        List<ClassSubject> GetAllClassSubjects(ref int totalcount, int? ClassId = null, int? VirtualClassId = null, int? SubjectId = null, int? SubjectTypeId = null,
            bool? IsActive = null, int? SkillId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertClassSubject(ClassSubject ClassSubject);

        void UpdateClassSubject(ClassSubject ClassSubject);

        void DeleteClassSubject(int ClassSubjectId = 0);

        #endregion

        #region ClassSubjectDetail

        ClassSubjectDetail GetClassSubjectDetailById(int ClassSubjectDetailId, bool IsTrack = false);

        List<ClassSubjectDetail> GetAllClassSubjectDetails(ref int totalcount, int? ClassSubjectId = null, int? EntryType = null,
            DateTime? EntryDate = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertClassSubjectDetail(ClassSubjectDetail ClassSubjectDetail);

        void UpdateClassSubjectDetail(ClassSubjectDetail ClassSubjectDetail);

        void DeleteClassSubjectDetail(int ClassSubjectDetailId = 0);

        #endregion

        #region StudentClassSubjects

        StudentClassSubject GetStudentClassSubjectId(int StudentClassSubjectId, bool IsTrack = false);

        List<StudentClassSubject> GetAllStudentClassSubjects(ref int totalcount, int? StudentClassId = null, int? ClassSubjectId = null,
                                                            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudentClassSubject(StudentClassSubject StudentClassSubject);

        void UpdateStudentClassSubject(StudentClassSubject StudentClassSubject);

        void DeleteStudentClassSubject(int StudentClassSubjectId = 0);

        void DeleteStudentClassSubjectThroStuClassId(int StudentClassId = 0);
        void DeleteStudentClassSubjectsList(List<StudentClassSubject> studentClassSubjects);
        void AddStudentClassSubjectsList(List<StudentClassSubject> studentClassSubjects);
        #endregion

        #region Attendance

        Attendance GetAttendanceById(int AttendanceId, bool IsTrack = false);

        IList<Attendance> GetAllAttendances(ref int totalcount, int? StudentId = null, DateTime? Date = null, int? AttendanceStatusId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAttendance(Attendance Attendance);

        void UpdateAttendance(Attendance Attendance);

        void DeleteAttendance(int AttendanceId = 0);

        #endregion

        #region StudentLeaveApplication
        StudentLeaveApplication GetStudentLeaveApplicationById(int StudentLeaveApplicationId, bool IsTrack = false);

        List<StudentLeaveApplication> GetAllStudentLeaveApplication(ref int totalcount, int? StudentId = null, DateTime? LeaveFrom = null, DateTime? LeaveTill = null, DateTime? AppliedDate = null, string Reason = null, int? LeaveTypeId = null, int? ApprovedBy = null, DateTime? ApprovedOn = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudentLeaveApplication(StudentLeaveApplication StudentLeave);

        void UpdateStudentLeaveApplication(StudentLeaveApplication StudentLeave);

        void DeleteStudentLeaveApplication(int StudentLeaveApplicationId = 0);

        #endregion

        #region AttendanceStatus

        AttendanceStatu GetAttendanceStatusById(int AttendanceStatusId, bool IsTrack = false);

        List<AttendanceStatu> GetAllAttendanceStatus(ref int totalcount, string AttnStatus = "", int? FromPeriodId = null, int? TillPeriodId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAttendanceStatus(AttendanceStatu AttendanceStatus);

        void UpdateAttendanceStatus(AttendanceStatu AttendanceStatus);

        void DeleteAttendanceStatus(int AttendanceStatusId = 0);

        #endregion

        #region AttendanceCard



        AttendanceDeviceLocation GetAttendaceLocationDevicebyId(string qsid);
        KSModel.Models.AttendanceDeviceType GetAttendanceDeviceTypeById(int AttendanceDeviceTypeId, bool IsTrack = false);
        List<AttendanceDevice> GetAllDevice(ref int totalcount,
           int PageIndex = 0, int PageSize = int.MaxValue);
        List<AttendanceLocation> GetAllLocations(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);
        //for AttendanceDeviceLocation
        //List<AttendanceDeviceLocation> GetAllAttendanceDeviceLocation(ref int totalcount,
        //   int PageIndex = 0, int PageSize = int.MaxValue);
        void DeleteAttendanceDeviceLocation(string id);
        void UpdateAttendanceDeviceLocation(AttendanceDeviceLocation Attendancedevicelocation);
        List<AttendanceCardGridModel> GetAllAttendanceDeviceLocation();
        List<KSModel.Models.AttendanceDeviceLocation> GetAllAttendanceDeviceLocations(ref int totalcount,
         int PageIndex = 0, int PageSize = int.MaxValue);

        DataTable GetDevicebyDeviceID();
        void InsertAttendanceDeviceLocation(AttendanceDeviceLocation AttendanceDevicelocation);

        #endregion

        #region AttendanceDevice

        AttendanceDevice GetAttendanceDeviceById(int AttendanceDeviceId, bool IsTrack = false);

        List<AttendanceDevice> GetAllAttendanceDevices(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAttendanceDevice(AttendanceDevice AttendanceDevice);

        void UpdateAttendanceDevice(AttendanceDevice AttendanceDevice);

        void DeleteAttendanceDevice(int AttendanceDeviceId = 0);

        #endregion

        #region  AttendancePeriodWise

        AttendancePeriodWise GetAttendancePeriodWiseById(int AttendancePeriodWiseId, bool IsTrack = false);

        List<AttendancePeriodWise> GetAllAttendancePeriodWise(ref int totalcount, int? AttendanceId = null, int? PeriodId = null, int? AttendanceStatusId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAttendancePeriodWise(AttendancePeriodWise AttendancePeriodWise);

        void UpdateAttendancePeriodWise(AttendancePeriodWise AttendancePeriodWise);

        void DeleteAttendancePeriodWise(int AttendancePeriodWiseId = 0);

        #endregion

        #region ContactCardType

        KSModel.Models.ContactCardType GetContactCardTypeById(int ContactCardTypeId, bool IsTrack = false);


        List<KSModel.Models.ContactCardType> GetAllContactCardTypes(ref int totalcount,
           int PageIndex = 0, int PageSize = int.MaxValue);


        void InsertContactCardType(KSModel.Models.ContactCardType ContactCardType);

        void UpdateContactCardType(KSModel.Models.ContactCardType ContactCardType);

        void DeleteContactCardType(int ContactCardTypeId = 0);


        #endregion

        #region AttendanceCardType

        KSModel.Models.AttendanceCardType GetAttendanceCardTypeById(int AttendanceCardTypeId, bool IsTrack = false);

        List<KSModel.Models.AttendanceCardType> GetAllAttendanceCardTypes(ref int totalcount,
           int PageIndex = 0, int PageSize = int.MaxValue);


        void InsertAttendanceCardType(KSModel.Models.AttendanceCardType AttendanceCardType);


        void UpdateAttendanceCardType(KSModel.Models.AttendanceCardType AttendanceCardType);


        void DeleteAttendanceCardType(int AttendanceCardTypeId = 0);


        #endregion

        #region AttendanceUserCard

        KSModel.Models.AttendanceUserCard GetAttendanceUserCardById(int AttendanceUserCardId, bool IsTrack = false);

        List<KSModel.Models.AttendanceUserCard> GetAllAttendanceUserCards(ref int totalcount,
           int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAttendanceUserCard(KSModel.Models.AttendanceUserCard AttendanceUserCard);

        void UpdateAttendanceUserCard(KSModel.Models.AttendanceUserCard AttendanceUserCard);

        void DeleteAttendanceUserCard(int AttendanceUserCardId = 0);
        #endregion

        #region AttendanceCard

        KSModel.Models.AttendanceCard GetAttendanceCardById(int AttendanceCardId, bool IsTrack = false);

        List<KSModel.Models.AttendanceCard> GetAllAttendanceCards(ref int totalcount,
           int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAttendanceCard(KSModel.Models.AttendanceCard AttendanceCard);

        void UpdateAttendanceCard(KSModel.Models.AttendanceCard AttendanceCard);

        void DeleteAttendanceCard(int AttendanceCardId = 0);
        #endregion

        #region AttendancePunch
        IList<KSModel.Models.Staff> GetAttendancePunchMachineById(int? ExcelPunchId = null);

        IList<KSModel.Models.AttendancePunch> GetAllAttendancePunch(ref int totalcount, DateTime? PunchTime=null,
                                       int? PunchTypeId = null, int? StaffId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAttendancePunch(KSModel.Models.AttendancePunch AttendancePunch);

        void UpdateAttendancePunch(KSModel.Models.AttendancePunch AttendancePunch);

        void DeleteAttendancePunch(int PunchId = 0);
        #endregion

        #region LessonPlan

        KSModel.Models.LessonPlan GetLessonPlanById(int LessonPlanId, bool IsTrack = false);


        List<KSModel.Models.LessonPlan> GetAllLessonPlans(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue);


        void InsertLessonPlan(KSModel.Models.LessonPlan LessonPlan);


        void UpdateLessonPlan(KSModel.Models.LessonPlan LessonPlan);


        void DeleteLessonPlan(int LessonPlanId = 0);
        
        #endregion

    }
}
