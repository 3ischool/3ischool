﻿using KS_SmartChild.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace KS_SmartChild.DAL.ErrorLogModule
{
    public partial class LogService : ILogService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IWebHelper _IWebHelper;
        private readonly IConnectionService _IConnectionService;

        public LogService(IWebHelper IWebHelper, IConnectionService IConnectionService)
        {
            this._IWebHelper = IWebHelper;
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods

        /// <summary>
        /// Determines whether a log level is enabled
        /// </summary>
        /// <param name="level">Log level</param>
        /// <returns>Result</returns>
        public virtual bool IsEnabled(LogLevel level)
        {
            switch (level)
            {
                case LogLevel.Debug:
                    return false;
                default:
                    return true;
            }
        }

        /// <summary>
        /// Deletes a log item
        /// </summary>
        /// <param name="log">Log item</param>
        public virtual void DeleteLog(int ErrorLogId)
        {
            if (ErrorLogId == null)
                throw new ArgumentNullException("ErrorLog");

            var errorlog = (from e in db.ErrorLogs where e.Id == ErrorLogId select e).FirstOrDefault();
            db.Entry(errorlog).State = System.Data.Entity.EntityState.Deleted;
            db.SaveChanges();
        }

        /// <summary>
        /// Deletes a log items
        /// </summary>
        /// <param name="logs">Log items</param>
        public virtual void DeleteLogs(IList<ErrorLog> ErrorLogs)
        {
            if (ErrorLogs == null)
                throw new ArgumentNullException("ErrorLogs");

            foreach (var log in ErrorLogs)
            {
                db.Entry(log).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Clears a log
        /// </summary>
        public virtual void ClearLog()
        {
            if (true)
            {
                //although it's not a stored procedure we use it to ensure that a database supports them
                //we cannot wait until EF team has it implemented - http://data.uservoice.com/forums/72025-entity-framework-feature-suggestions/suggestions/1015357-batch-cud-support

                //do all databases support "Truncate command"?
                string logTableName = "ErrorLog";
                db.Database.ExecuteSqlCommand(String.Format("TRUNCATE TABLE [{0}]", logTableName));
            }

        }

        /// <summary>
        /// Gets all log items
        /// </summary>
        /// <param name="fromUtc">Log item creation from; null to load all records</param>
        /// <param name="toUtc">Log item creation to; null to load all records</param>
        /// <param name="message">Message</param>
        /// <param name="logLevel">Log level; null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Log item items</returns>
        public virtual IPagedList<ErrorLog> GetAllLogs(DateTime? fromUtc = null, DateTime? toUtc = null,
            string message = "", LogLevel? logLevel = null,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = db.ErrorLogs.ToList();
            if (fromUtc.HasValue)
                query = query.Where(l => fromUtc.Value <= l.CreatedOnUtc).ToList();
            if (toUtc.HasValue)
                query = query.Where(l => toUtc.Value >= l.CreatedOnUtc).ToList();
            if (logLevel.HasValue)
            {
                var logLevelId = (int)logLevel.Value;
                query = query.Where(l => logLevelId == l.LogLevelId).ToList();
            }
            if (!String.IsNullOrEmpty(message))
                query = query.Where(l => l.ShortMessage.Contains(message) || l.FullMessage.Contains(message)).ToList();

            query = query.OrderByDescending(l => l.CreatedOnUtc).ToList();

            var log = new PagedList<ErrorLog>(query, pageIndex, pageSize);
            return log;
        }

        /// <summary>
        /// Gets a log item
        /// </summary>
        /// <param name="logId">Log item identifier</param>
        /// <returns>Log item</returns>
        public virtual ErrorLog GetLogById(int ErrorLogId)
        {
            if (ErrorLogId == 0)
                return null;

            var errorLog = (from s in db.ErrorLogs.AsNoTracking() where s.Id == ErrorLogId select s).FirstOrDefault();
            return errorLog;
        }

        /// <summary>
        /// Get log items by identifiers
        /// </summary>
        /// <param name="logIds">Log item identifiers</param>
        /// <returns>Log items</returns>
        public virtual IList<ErrorLog> GetLogByIds(int[] ErrorLogIds)
        {
            if (ErrorLogIds == null || ErrorLogIds.Length == 0)
                return new List<ErrorLog>();

            var query = from l in db.ErrorLogs
                        where ErrorLogIds.Contains(l.Id)
                        select l;
            var logItems = query.ToList();
            //sort by passed identifiers
            var sortedLogItems = new List<ErrorLog>();
            foreach (int id in ErrorLogIds)
            {
                var log = logItems.Find(x => x.Id == id);
                if (log != null)
                    sortedLogItems.Add(log);
            }
            return sortedLogItems;
        }

        /// <summary>
        /// Inserts a log item
        /// </summary>
        /// <param name="logLevel">Log level</param>
        /// <param name="shortMessage">The short message</param>
        /// <param name="fullMessage">The full message</param>
        /// <param name="customer">The customer to associate log record with</param>
        /// <returns>A log item</returns>
        public virtual ErrorLog InsertLog(LogLevel logLevel, string shortMessage, string fullMessage = "", SchoolUser user = null)
        {
            int? userid = null;
            var log = new ErrorLog
            {
                LogLevelId = Convert.ToInt32(logLevel),
                ShortMessage = shortMessage,
                FullMessage = fullMessage,
                IpAddress = _IWebHelper.GetCurrentIpAddress(),
                UserId = user != null && user.UserId != null && user.UserId != 0 ? user.UserId : userid,
                PageUrl = _IWebHelper.GetThisPageUrl(true),
                ReferrerUrl = _IWebHelper.GetUrlReferrer(),
                CreatedOnUtc = DateTime.UtcNow
            };

            db.ErrorLogs.Add(log);
            
            db.SaveChanges();

            return log;
        }

        #endregion
    }
}