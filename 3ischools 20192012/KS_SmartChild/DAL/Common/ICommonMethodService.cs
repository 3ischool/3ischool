﻿using KSModel.Models;
using KS_SmartChild.ViewModel.Common;
using KS_SmartChild.ViewModel.Fee;
using KS_SmartChild.ViewModel.Messenger;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace KS_SmartChild.DAL.Common
{
    public partial interface ICommonMethodService
    {
        #region ConvertDate functions
        string ConvertDate(DateTime Date);

        string FormatDate(string Date);

        string FormatDatehyp(string Date);

        string ConvertDateForKendo(DateTime Date);

        TimeSpan? ConvertStringToTimeSpan(string StringTime);

        DateTime? ConvertStringToPraseDateTime(string date);
        #endregion

        #region TrimFunctions
        string TrimStaffName(Staff item, string Staffname);

        string TrimStudentName(Student item, string StudentName);

        string TrimGuardianName(Guardian item, string GuardianName);
        #endregion

        #region SMSNotiTrigger
        void TiggerNotificationSMS(HWDetail homeworkdetail = null, Student student = null, bool SMS = false,
            bool Notification = false, bool IsNonAPPUser = false, string SelectedAttendanceStatus = "",
            List<Student> Students = null, string TriggerEventName = "", string HWDescription = "",
            string Subject = "", string attendanceStatus = "", string attendanceStatusAbbr = "",
            string ClassName = "", DateTime? AttendanceDate = null,
            string FeePeriodDescription = "", string PaidAmount = "", string PendingAmount = "", string StdClassReceiptNo = "",
            string SchedularDescription = "", string EventTitle = "", string SendSMSMobileOptionId = "", bool IsStaff = false,
            string SubstituteName = "", string Teachername = "", string Period = "", string AdjustmentReason = "",
            DateTime? AdjustmentDate = null, Staff Staff = null, string StaffIds = "", string LeaveApprovalStatus = "",
            DateTime? LeaveFrom = null, DateTime? LeaveTill = null, string Sender = "", string LeaveStatus = "", bool IsUserFullDetail = false,
            string gatePassType = "", string GatepassDynamicText = "", bool IsGuardian = false, string GuradianIds = "", bool SendToGuardian = true,
            string DateSheetName = "", string ExamActivity = "", DateTime? ExamDateFrom = null, DateTime? ExamDateTill = null,
            DateTime? PublishDate = null, DateTime? TimetableFromDate = null, DateTime? TimeTableTillDate = null, string TimetableDays = "",
            string MessageDescription = "", bool IsAdmin = false, string GatePassStatus = "",
            string GatepassApprovedby = "", string GatePassIssuedTo = "", DateTime? GatePassApprovedOn = null, 
            string GatePassType = "");

        string ChooseStudentMobileNo(string SendMessageMobileNoSelection_Id = "", int? studentId = null, string contactinfo = "");

        bool CheckAppUser(int? studentId = null, bool IsNonAPPUser = false, int? UserTypeId = null);

        void CountOfUserForSMsNotification(SMSNotificationModel MessengerModel, ref int NC, ref int SMSC,
                    ref List<int> Id_array, int?[] getNotPresntUsers = null, int?[] getPresntUsers = null,
                    string UserType = "", IEnumerable<Student> GetStudents = null,
                    IEnumerable<Guardian> GetGuardians = null, IEnumerable<Staff> GetStaffs = null, IEnumerable<Staff> GetNonStaffs = null,
                    string SendMessageMobileNoSelection_Id = "");
        void UpdateCreateSendSmsStudentsTextFile(string SchoolDbId,
                                    List<SendSelectedStudentsSMS> ListSendSelectedStudentsSMS, string TextFileName = "");

        #endregion

        #region Navigation
        void UpdateCreateUserRoleActionPermissionTextFile(string SchoolDbId, List<UserRoleActionPermission> ListUserActionPermission);
        #endregion

        #region UserRoles
        string GetUserSuperRole(string userRole, SchoolUser user);
        #endregion

        #region SMSDefaultSettings
        KSModel.Models.SMSDefaultSetting GetSMSDefaultSettingById(int? SMSDefaultSettingsId, bool IsTrack = false);

        List<SMSDefaultSetting> GetAllSMSDefaultSettings(ref int totalcount, string FormKey = "", bool Isfilter = false,
            int PageIndex = 0, int PageSize = int.MaxValue);

        SMSDefaultSetting GetOrSetSMSDefaultSetting(string FormKey = "", bool SMS = false, bool ToNonAppUser = false,
                                    bool Notification = false, string SelectedMobile = "", string StatusSelection = "");

        void InsertSMSDefaultSetting(KSModel.Models.SMSDefaultSetting SMSDefaultSetting);

        void UpdateSMSDefaultSetting(KSModel.Models.SMSDefaultSetting SMSDefaultSetting);

        void DeleteSMSDefaultSetting(int SMSDefaultSettingsId = 0);

        #endregion

        #region FeeModule
        int AutoGenerateReceiptNo(int? RecieptTypeId = null);

        PrintFeeReceiptModel PrintFeeReceipt(string id);
        #endregion

        #region Connection
        DataTable GetSuperDatables(int UserRoleTypeId = 0, int AppFormId = 0, bool IsFindRole = false,
                                        bool IsFindAppForm = false);

        string GetSetCurrentSchoolDataBase(HttpRequestBase Request, HttpSessionStateBase Session, HttpServerUtilityBase Server);
        string GetSetCurrentSchoolDataBaseApp(HttpRequestBase Request, HttpSessionStateBase Session, HttpServerUtilityBase Server, out string Sessionname, out string SchoolId);
        #endregion

        #region Convertfunctions
        String ones(String Number);

        String tens(String Number);

        String ConvertDecimals(String number);

        String ConvertToWords(String numb);

        String ConvertWholeNumber(String Number);

        IEnumerable<DateTime> EachDay(DateTime from, DateTime thru);
        #endregion

        #region ExcelObjects
        ExcelImportObject GetExcelImportObjectById(int ExcelImportObjectId, bool IsTrack = false);

        List<ExcelImportObject> GetAllExcelImportObjects(ref int totalcount, string ExcelImportObject = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExcelImportObject(ExcelImportObject ExcelImportObject);

        void UpdateExcelImportObject(ExcelImportObject ExcelImportObject);

        void DeleteExcelImportObject(int ExcelImportObjectId = 0);
        #endregion

        #region ExcelImportObjectCols
        ExcelImportObjectCol GetExcelImportObjectColById(int ExcelImportObjectColId, bool IsTrack = false);

        List<ExcelImportObjectCol> GetAllExcelImportObjectCols(ref int totalcount, string DBColName = "",
            string ExcelColName = "", int? ExcelImportObjectId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExcelImportObjectCol(ExcelImportObjectCol ExcelImportObjectCol);

        void UpdateExcelImportObjectCol(ExcelImportObjectCol ExcelImportObjectCol);

        void DeleteExcelImportObjectCol(int ExcelImportObjectColId = 0);
        #endregion

        #region SurveillanceAlarm
        List<SurveillanceAlarm> GetAllSurveillanceAlarms(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue);
        void UpdateSurveillanceAlarmsActivatedDate(SurveillanceAlarm SurveillanceAlarm);
        #endregion

        #region SurveillancePunch
        SurviellancePunch GetSurveillancePunchById(int SurveillancePunchId, bool IsTrack = false);

        List<SurviellancePunch> GetAllSurveillancePunch(ref int totalcount,
           int PageIndex = 0, int PageSize = int.MaxValue);


        void InsertSurveillancePunch(SurviellancePunch SurveillancePunch);

        void UpdateSurveillancePunch(SurviellancePunch SurveillancePunch);


        void DeleteSurveillancePunch(int SurveillancePunchId = 0);

        #endregion

        #region User's ProfileImages
        string StudentProfileImage(string SchoolDbId, Student student);

        string StaffImage(string SchoolDbId, Staff staff);

        string GuardianProfileImage(string SchoolDbId, Guardian guardian,int? StudentId = null);
        #endregion

        #region ColorCodes
        IList<ColorCode> GetAllColorCodes(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);
        #endregion
    }
}
