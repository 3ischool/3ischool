﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.DAL.Common
{
    public enum Connectivity
    {
        Login = 1,
        Session = 2,
        Done = 3,
        Error = 4
    }
}