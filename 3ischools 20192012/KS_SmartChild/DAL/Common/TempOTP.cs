﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.DAL.Common
{
    public partial class TempOTP:ITempOTP
    {
          public int count = 0;
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        //private readonly string SqlDataSource;
          private string DataSource
          {
              get
              {
                  var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                  return dtsource;
              }
          }

          private KS_ChildEntities Context;
          public KS_ChildEntities db
          {

              get
              {
                  if (Context == null)
                  {
                      Context = new KS_ChildEntities(DataSource);
                      return Context;
                  }
                  return Context;
              }
          }
          private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;
        public TempOTP(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }
     



        public void InsertTempOtp(temp_otp_detail temp_otp_detail)
        {
            if (temp_otp_detail == null)
                throw new ArgumentNullException("temp_otp_detail");

            db.temp_otp_detail.Add(temp_otp_detail);
            db.SaveChanges();
        }
    }
}