﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.DAL.Common
{
    public class ChangePasswordResult
    {
        public ChangePasswordResult()
        {
            this.Errors = new List<string>();
        }

        public bool Success
        {
            get { return (!this.Errors.Any()); }
        }

        public void AddError(string error)
        {
            this.Errors.Add(error);
        }

        public IList<string> Errors { get; set; }
    }
}