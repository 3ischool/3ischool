﻿using HtmlAgilityPack;
using KS_SmartChild.BAL.Common;
using KS_SmartChild.BAL.Message;
using KS_SmartChild.BAL.Security;
using KS_SmartChild.DAL.AddressModule;
using KS_SmartChild.DAL.CatalogMaster;
using KS_SmartChild.DAL.ContactModule;
using KS_SmartChild.DAL.DocumentModule;
using KS_SmartChild.DAL.FeeModule;
using KS_SmartChild.DAL.GuardianModule;
using KS_SmartChild.DAL.SettingService;
using KS_SmartChild.DAL.StaffModule;
using KS_SmartChild.DAL.StudentModule;
using KS_SmartChild.DAL.TriggerEventModule;
using KS_SmartChild.DAL.UserModule;
using KSModel.Models;
using KS_SmartChild.ViewModel.API;
using KS_SmartChild.ViewModel.Common;
using KS_SmartChild.ViewModel.Fee;
using KS_SmartChild.ViewModel.Messenger;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;

namespace KS_SmartChild.DAL.Common
{
    public partial class CommonMethodService : ICommonMethodService
    {
        #region Fields
        public int count = 0;
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly ITriggerEventService _ITriggerEventService;
        private readonly ISubjectService _ISubjectService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly IUserService _IUserService;
        private readonly IWorkflowMessageService _IWorkflowMessageService;
        private readonly IStudentService _IStudentService;
        private readonly ISMSSender _ISMSSender;
        private readonly IGuardianService _IGuardianService;
        private readonly IContactService _IContactService;
        private readonly IConnectionService _IConnectionService;
        private readonly IUserManagementService _IUserManagementService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IStaffService _IStaffService;
        private readonly IFeeService _IFeeService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly WebHelper _WebHelper;
        private readonly IAddressService _IAddressService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IDocumentService _IDocumentService;
        #endregion

        #region ctor
        public CommonMethodService(IConnectionService IConnectionService,
            ITriggerEventService ITriggerEventService,
            ISubjectService ISubjectService, IAddressMasterService IAddressMasterService,
            IUserService IUserService, IWorkflowMessageService IWorkflowMessageService,
            IStudentService IStudentService, ISMSSender ISMSSender,
            IGuardianService IGuardianService, IContactService IContactService,
            IUserManagementService IUserManagementService,
            ISchoolSettingService ISchoolSettingService, IStaffService IStaffService,
            IFeeService IFeeService, ICustomEncryption ICustomEncryption, WebHelper WebHelper,
            IAddressService IAddressService, ICatalogMasterService ICatalogMasterService,
            IDocumentService IDocumentService)
        {
            //HttpContext context = HttpContext.Current;
            this._IConnectionService = IConnectionService;
            this._ITriggerEventService = ITriggerEventService;
            this._ISubjectService = ISubjectService;
            this._IAddressMasterService = IAddressMasterService;
            this._IUserService = IUserService;
            this._IWorkflowMessageService = IWorkflowMessageService;
            this._IStudentService = IStudentService;
            this._ISMSSender = ISMSSender;
            this._IGuardianService = IGuardianService;
            this._IContactService = IContactService;
            this._IUserManagementService = IUserManagementService;
            this._ISchoolSettingService = ISchoolSettingService;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            this._IStaffService = IStaffService;
            this._IFeeService = IFeeService;
            this._IFeeService = IFeeService;
            this._WebHelper = WebHelper;
            this._IAddressService = IAddressService;
            this._ICustomEncryption = ICustomEncryption;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IDocumentService = IDocumentService;
            //this.db = new KS_ChildEntities(DataSource);
        }
        #endregion

        #region ConvertDate functions
        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }

        public string FormatDate(string Date)
        {
            string sdisplayTime = Date.Trim('"');
            var dd = Date.Split('/');
            sdisplayTime = dd[1] + "/" + dd[0] + "/" + dd[2];
            return sdisplayTime;
        }
        public string FormatDatehyp(string Date)
        {
            string sdisplayTime = Date.Trim('"');
            var dd = Date.Split('-');
            sdisplayTime = dd[1] + "/" + dd[0] + "/" + dd[2];
            return sdisplayTime;
        }

        public TimeSpan? ConvertStringToTimeSpan(string StringTime)
        {
            TimeSpan? DefaultTime = null;
            DefaultTime = TimeSpan.Parse(StringTime);
            return DefaultTime;
        }

        public DateTime? ConvertStringToPraseDateTime(string date)
        {
            DateTime? AttDate = null;
            if (!string.IsNullOrEmpty(date) || !string.IsNullOrWhiteSpace(date))
            {
                AttDate = DateTime.ParseExact(date, "dd/MM/yyyy", null);
                AttDate = AttDate.Value.Date;
            }
            return AttDate;
        }

        public string ConvertDateForKendo(DateTime Date)
        {
            string sdisplayTime = Date.ToString("MM/dd/yyyy");
            var datearry = sdisplayTime.Split('/');
            var returnFromDate = datearry[1] + "/" + datearry[0] + "/" + datearry[2];
            return returnFromDate;
            //return sdisplayTime;
        }
        #endregion

        #region Trim Functions
        public string TrimStaffName(Staff item, string Staffname)
        {
            if (item != null)
            {
                if (!string.IsNullOrEmpty(item.FName))
                    item.FName = item.FName.Trim();

                if (!string.IsNullOrEmpty(item.LName))
                    item.LName = item.LName.Trim();

                Staffname = (item.FName + " " + item.LName).Trim();
            }
            return Staffname;
        }

        public string TrimStudentName(Student item, string StudentName)
        {
            if (item != null)
            {
                if (!string.IsNullOrEmpty(item.FName))
                    item.FName = item.FName.Trim();

                if (!string.IsNullOrEmpty(item.LName))
                    item.LName = item.LName.Trim();

                StudentName = (item.FName + " " + item.LName).Trim();
            }
            return StudentName;
        }

        public string TrimGuardianName(Guardian item, string GuardianName)
        {
            if (item != null)
            {
                if (!string.IsNullOrEmpty(item.FirstName))
                    item.FirstName = item.FirstName.Trim();

                if (!string.IsNullOrEmpty(item.LastName))
                    item.LastName = item.LastName.Trim();

                GuardianName = (item.FirstName + " " + item.LastName).Trim();
            }
            return GuardianName;
        }

        #endregion

        #region SmsNotiTrigger
        public void TiggerNotificationSMS(HWDetail homeworkdetail = null, Student student = null, bool SMS = false,
            bool Notification = false, bool IsNonAPPUser = false, string SelectedAttendanceStatus = "",
            List<Student> Students = null, string TriggerEventName = "", string HWDescription = "",
            string Subject = "", string attendanceStatus = "", string attendanceStatusAbbr = "",
            string ClassName = "", DateTime? AttendanceDate = null,
            string FeePeriodDescription = "", string PaidAmount = "", string PendingAmount = "", string StdClassReceiptNo = "",
            string SchedularDescription = "", string EventTitle = "", string SendSMSMobileOptionId = "", bool IsStaff = false,
            string SubstituteName = "", string Teachername = "", string Period = "", string AdjustmentReason = "",
            DateTime? AdjustmentDate = null, Staff Staff = null, string StaffIds = "", string LeaveApprovalStatus = "",
            DateTime? LeaveFrom = null, DateTime? LeaveTill = null, string Sender = "", string LeaveStatus = "",
            bool IsUserFullDetail = false, string gatePassType = "", string GatepassDynamicText = "", bool IsGuardian = false,
            string GuradianIds = "", bool SendToGuardian = true, string DateSheetName = "", string ExamActivity = "",
            DateTime? ExamDateFrom = null, DateTime? ExamDateTill = null, DateTime? PublishDate = null,
            DateTime? TimetableFromDate = null, DateTime? TimeTableTillDate = null, string TimetableDays = "",
            string MessageDescription = "", bool IsAdmin = false, string GatePassStatus = "",
            string GatepassApprovedby = "", string GatePassIssuedTo = "", DateTime? GatePassApprovedOn = null, string GatePassType = "")
        {
            var trigger = _ITriggerEventService.GetAllTriggerEventList(ref count)
                                    .Where(m => m.TriggerEvent1.ToLower() == TriggerEventName.ToLower()
                                                && m.IsActive == true).FirstOrDefault();
            if (trigger != null)
            {
                if (Notification)
                {
                    #region Notification

                    if (SelectedAttendanceStatus.Contains(attendanceStatusAbbr) || SelectedAttendanceStatus == "")
                    {
                        var triggershooter = trigger.TriggerShooters.Where(m => m.TriggerTemplate != null
                                            && m.TriggerTemplate.IsActive == true && m.TriggerTemplate.TriggerMaster != null
                                            && m.TriggerTemplate.TriggerMaster.IsActive == true
                                            && m.TriggerTemplate.TriggerMaster.TriggerType != null
                                            && m.TriggerTemplate.TriggerMaster.TriggerType.IsActive == true
                                            && m.TriggerTemplate.TriggerMaster.TriggerType.TriggerType1 == "Push Notification"
                                            && m.TriggerTemplate.TriggerMsgTemplate != null
                                            && m.TriggerTemplate.TriggerMsgTemplate.IsActive == true
                                            && m.IsActive == true).ToList();
                        foreach (var triggershoot in triggershooter)
                        {
                            var triggermessage = triggershoot.TriggerTemplate.TriggerMsgTemplate.TriggerMsgTemplate1;
                            var triggermessageTilte = triggershoot.TriggerTemplate.TriggerMsgTemplate.MsgTemplateName;

                            triggermessage = triggermessage.Replace("%HomeWork%", HWDescription);
                            triggermessage = triggermessage.Replace("%Subject%", Subject);
                            triggermessageTilte = triggermessageTilte.Replace("%Subject%", Subject);


                            triggermessageTilte = triggermessageTilte.Replace("%GatepassType%", GatePassType);

                            triggermessage = triggermessage.Replace("%GatepassType%", GatePassType);
                            triggermessage = triggermessage.Replace("%GatePassIssuedTo%", GatePassIssuedTo);
                            triggermessage = triggermessage.Replace("%GatePassStatus%", GatePassStatus);
                            triggermessage = triggermessage.Replace("%GatepassApprovedby%", GatepassApprovedby);

                            if (GatePassApprovedOn != null)
                                triggermessage = triggermessage.Replace("%GatePassApprovedOn%", ConvertDateForKendo((DateTime)GatePassApprovedOn));


                            triggermessage = triggermessage.Replace("%PendingAmount%", PendingAmount);
                            if (student != null && Staff == null)
                            {
                                string Studentname = "";
                                Studentname = TrimStudentName(student, Studentname);

                                if (!IsUserFullDetail)
                                    triggermessage = triggermessage.Replace("%Name%", Studentname);
                                else
                                {
                                    if (!string.IsNullOrEmpty(student.AdmnNo))
                                        Studentname += " (" + student.AdmnNo + ")";

                                    triggermessage = triggermessage.Replace("%Name%", Studentname);
                                }

                                //triggermessageTilte = triggermessageTilte.Replace("%SenderName%", Sender);
                            }

                            triggermessageTilte = triggermessageTilte.Replace("%GatepassType%", gatePassType);
                            triggermessage = triggermessage.Replace("%DynamicText%", GatepassDynamicText);

                            triggermessage = triggermessage.Replace("%Day%", TimetableDays);
                            triggermessage = triggermessage.Replace("%MessageDescription%", MessageDescription);


                            // if (student != null)
                            //    triggermessage = triggermessage.Replace("%Name%", student.LName != null ? student.FName + " " + student.LName : student.FName);

                            triggermessage = triggermessage.Replace("%Status%", attendanceStatus);
                            if (AttendanceDate != null)
                                triggermessage = triggermessage.Replace("%PublishDateTime%", ConvertDateForKendo((DateTime)AttendanceDate));

                            if (LeaveFrom != null)
                                triggermessage = triggermessage.Replace("%LeaveFrom%", ConvertDateForKendo((DateTime)LeaveFrom));

                            if (LeaveTill != null)
                                triggermessage = triggermessage.Replace("%LeaveTill%", ConvertDateForKendo((DateTime)LeaveTill));

                            if (ExamDateFrom != null)
                                triggermessage = triggermessage.Replace("%ExamFromDate%", ConvertDateForKendo((DateTime)ExamDateFrom));

                            if (ExamDateTill != null)
                                triggermessage = triggermessage.Replace("%ExamTillDate%", ConvertDateForKendo((DateTime)ExamDateTill));

                            if (PublishDate != null)
                                triggermessage = triggermessage.Replace("%ExamPublishDate%", ConvertDateForKendo((DateTime)PublishDate));

                            if (TimetableFromDate != null)
                                triggermessage = triggermessage.Replace("%TimeTableFromDate%", ConvertDateForKendo((DateTime)TimetableFromDate));

                            if (TimeTableTillDate != null)
                                triggermessage = triggermessage.Replace("%TimeTableToDate%", ConvertDateForKendo((DateTime)TimeTableTillDate));


                            triggermessage = triggermessage.Replace("%ApprovalStatus%", LeaveApprovalStatus);
                            triggermessageTilte = triggermessageTilte.Replace("%ApprovalStatus%", LeaveApprovalStatus);
                            triggermessage = triggermessage.Replace("%FeePeriod%", FeePeriodDescription);
                            triggermessage = triggermessage.Replace("%Amount%", PaidAmount);

                            triggermessage = triggermessage.Replace("%ReceiptNo%", !string.IsNullOrEmpty(StdClassReceiptNo) ? StdClassReceiptNo : "");
                            if (!string.IsNullOrEmpty(ClassName))
                            {
                                triggermessage = triggermessage.Replace("%Class%", ClassName);
                                triggermessageTilte = triggermessageTilte.Replace("%Class%", ClassName);
                            }

                            triggermessage = triggermessage.Replace("%Description%", SchedularDescription);
                            triggermessageTilte = triggermessageTilte.Replace("%EventTitle%", EventTitle);


                            //for adjustment
                            triggermessage = triggermessage.Replace("%SubstituteName%", SubstituteName);
                            triggermessage = triggermessage.Replace("%Teachername%", Teachername);
                            triggermessage = triggermessage.Replace("%Period%", Period);
                            triggermessage = triggermessage.Replace("%AdjustmentReason%", AdjustmentReason);

                            triggermessage = triggermessage.Replace("%DatesheetName%", DateSheetName);
                            triggermessageTilte = triggermessageTilte.Replace("%DatesheetName%", DateSheetName);
                            triggermessage = triggermessage.Replace("%ExamActivity%", ExamActivity);

                            triggermessage = triggermessage.Replace("%LeaveStatus%", LeaveStatus);

                            triggermessageTilte = triggermessageTilte.Replace("%SenderName%", Sender);

                            if (AdjustmentDate != null)
                                triggermessage = triggermessage.Replace("%AdjustmentDate%", ConvertDateForKendo((DateTime)AdjustmentDate));

                            if (IsStaff)
                            {
                                var contactType = new List<ContactType>();
                                contactType = db.ContactTypes.Where(m => m.IsEmployee != null && (bool)m.IsEmployee).ToList();

                                #region trigger for staff
                                if (Staff != null)
                                {
                                    string Staffname = "";
                                    Staffname = TrimStaffName(Staff, Staffname);

                                    if (!IsUserFullDetail)
                                        triggermessage = triggermessage.Replace("%Name%", Staffname);
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(Staff.EmpCode))
                                            Staffname += " (" + Staff.EmpCode + ")";

                                        triggermessage = triggermessage.Replace("%Name%", Staffname);
                                    }

                                    // if ((bool)Staff.StaffType.IsTeacher)
                                    //   contactType = db.ContactTypes.Where(m => m.IsEmployee!=null && (bool)m.IsEmployee).ToList();
                                    //else
                                    //    contactType = db.ContactTypes.Where(m => m.ContactType1.ToLower() == "non-teaching").FirstOrDefault();
                                }
                                if (StaffIds != "")
                                {
                                    var arrayStaffs = StaffIds.Split(',');
                                    //if (IsAdmin)
                                    //    contactType = _IAddressMasterService.GetAllContactTypes(ref count)
                                    //                    .Where(m => m.ContactType1.ToLower() == "admin").FirstOrDefault();

                                    foreach (var i in arrayStaffs)
                                    {
                                        int Staff_Id = Convert.ToInt32(i);
                                        var getStaff = _IStaffService.GetStaffById(Staff_Id);
                                        if (getStaff != null)
                                        {
                                            //if (!IsAdmin)
                                            //{
                                            //    if ((bool)getStaff.StaffType.IsTeacher)
                                            //        contactType = db.ContactTypes.Where(m => m.ContactType1.ToLower() == "teacher").FirstOrDefault();
                                            //    else
                                            //        contactType = db.ContactTypes.Where(m => m.ContactType1.ToLower() == "non-teaching").FirstOrDefault();
                                            //}

                                            triggermessageTilte = triggermessageTilte.Replace("%SenderName%", Sender);

                                            //send student Notification for HW
                                            var StaffSchooluser = _IUserService.GetAllUsers(ref count)
                                                                    .Where(m => (contactType.Select(n => (int)n.ContactTypeId)).Contains((int)m.UserTypeId)
                                                                     && m.UserContactId == getStaff.StaffId).FirstOrDefault();

                                            if (StaffSchooluser != null)
                                            {
                                                //create collapse Key for notifcitaion
                                                var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                var CounterNotification = 0;
                                                var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                       (currentDate + "_" + CounterNotification.ToString()), null
                                                                                       , "UniqueKey for Notifications on APP").AttributeValue;
                                                var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                {
                                                    CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                    CounterNotification++;
                                                }
                                                else
                                                    CounterNotification = 1;

                                                var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count)
                                                                         .Where(m => (m.UserId == StaffSchooluser.UserId)
                                                                        && m.UserDeviceName == "IOS")
                                                                        .Select(N => N.UserDeviceUniqueId).ToArray();
                                                var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count)
                                                                        .Where(m => m.UserId == StaffSchooluser.UserId
                                                                        && m.UserDeviceName == "Android")
                                                                        .Select(N => N.UserDeviceUniqueId).ToArray();

                                                if (userdeviceIdsArray.Count() > 0)
                                                {
                                                    var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                               (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                    _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray,
                                                                                                    triggermessageTilte,
                                                                                                    triggermessage, "IOS", UpdateCollapseKey);
                                                }
                                                if (userdeviceIdsArrayAndroid.Count() > 0)
                                                {
                                                    var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                               (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                                                    _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid,
                                                                                                    triggermessageTilte,
                                                                                                    triggermessage, "Android", UpdateCollapseKey);
                                                }
                                            }

                                        }
                                    }
                                }
                                #endregion
                            }
                            else if (IsGuardian)
                            {
                                #region trigger for guardian

                                if (GuradianIds != "")
                                {
                                    var arraygaurds = GuradianIds.Split(',');
                                    var contactType = db.ContactTypes.Where(m => m.ContactType1 == "Guardian").FirstOrDefault();
                                    foreach (var i in arraygaurds)
                                    {
                                        int Gaurdian_Id = Convert.ToInt32(i);
                                        var getGaurdian = db.Guardians.Where(x => x.GuardianId == Gaurdian_Id).FirstOrDefault();
                                        if (getGaurdian != null)
                                        {
                                            string guardianname = "";
                                            guardianname = TrimGuardianName(getGaurdian, guardianname);
                                            triggermessage = triggermessage.Replace("%ParentDetail%", guardianname + " "
                                                                                        + getGaurdian.Relation.Relation1);
                                            //send student Notification for HW
                                            var GaurdianSchooluser = db.SchoolUsers
                                                                    .Where(m => m.UserTypeId == contactType.ContactTypeId
                                                                     && m.UserContactId == getGaurdian.GuardianId).FirstOrDefault();

                                            if (GaurdianSchooluser != null)
                                            {

                                                //create collapse Key for notifcitaion
                                                var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                var CounterNotification = 0;
                                                var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                       (currentDate + "_" + CounterNotification.ToString()), null
                                                                                       , "UniqueKey for Notifications on APP").AttributeValue;
                                                var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                {
                                                    CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                    CounterNotification++;
                                                }
                                                else
                                                    CounterNotification = 1;

                                                var userdeviceIdsArray = db.UserDevices
                                                                         .Where(m => (m.UserId == GaurdianSchooluser.UserId)
                                                                        && m.UserDeviceName == "IOS")
                                                                        .Select(N => N.UserDeviceUniqueId).ToArray();
                                                var userdeviceIdsArrayAndroid = db.UserDevices
                                                                                .Where(m => m.UserId == GaurdianSchooluser.UserId
                                                                                && m.UserDeviceName == "Android")
                                                                                .Select(N => N.UserDeviceUniqueId).ToArray();

                                                if (userdeviceIdsArray.Count() > 0)
                                                {
                                                    var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                                (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                    _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray,
                                                                                                    triggermessageTilte,
                                                                                                    triggermessage, "IOS", UpdateCollapseKey);
                                                }
                                                if (userdeviceIdsArrayAndroid.Count() > 0)
                                                {
                                                    var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                               (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                    _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid,
                                                                                                    triggermessageTilte,
                                                                                                    triggermessage, "Android", UpdateCollapseKey);
                                                }
                                            }

                                        }
                                    }
                                }

                                #endregion
                            }
                            else
                            {
                                #region triggers for student
                                var contactType = _IAddressMasterService.GetAllContactTypes(ref count)
                                                  .Where(m => m.ContactType1 == "Student").FirstOrDefault();

                                foreach (var item in Students)
                                {

                                    //send student Notification for HW
                                    var StudentSchooluser = _IUserService.GetAllUsers(ref count)
                                                            .Where(m => m.UserTypeId == contactType.ContactTypeId
                                                             && m.UserContactId == item.StudentId).FirstOrDefault();
                                    if (StudentSchooluser != null)
                                    {

                                        //create collapse Key for notifcitaion
                                        var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                        var CounterNotification = 0;
                                        var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                               (currentDate + "_" + CounterNotification.ToString()), null
                                                                               , "UniqueKey for Notifications on APP").AttributeValue;
                                        var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                        if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                        {
                                            CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                            CounterNotification++;
                                        }
                                        else
                                            CounterNotification = 1;

                                        var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count)
                                                                 .Where(m => (m.UserId == StudentSchooluser.UserId)
                                                                && m.UserDeviceName == "IOS")
                                                                .Select(N => N.UserDeviceUniqueId).ToArray();
                                        var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count)
                                                                .Where(m => m.UserId == StudentSchooluser.UserId
                                                                && m.UserDeviceName == "Android")
                                                                .Select(N => N.UserDeviceUniqueId).ToArray();

                                        if (userdeviceIdsArray.Count() > 0)
                                        {
                                            var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                           (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                            _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray,
                                                                                            triggermessageTilte,
                                                                                            triggermessage, "IOS", UpdateCollapseKey);
                                        }
                                        if (userdeviceIdsArrayAndroid.Count() > 0)
                                        {
                                            var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                           (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                                            _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid,
                                                                                            triggermessageTilte,
                                                                                            triggermessage, "Android", UpdateCollapseKey);
                                        }
                                    }

                                    if (SendToGuardian)
                                    {
                                        var GaurdianRelationTypeIds = _IGuardianService.GetAllRelations(ref count)
                                                                    .Where(x => x.Relation1.ToLower() == "father" || x.Relation1.ToLower() == "mother"
                                                                    || x.Relation1.ToLower() == "guardian")
                                                                    .Select(x => x.RelationId).ToArray();

                                        var getFMGuardianIds = _IGuardianService.GetAllGuardians(ref count, StudentId: item.StudentId)
                                                                        .Where(x => GaurdianRelationTypeIds.Contains((int)x.GuardianTypeId))
                                                                        .Select(x => x.GuardianId).ToArray();

                                        //send student's parent Notification
                                        var StudentGaurdianSchooluser = _IUserService.GetAllUsers(ref count)
                                                                       .Where(m => m.ContactType.ContactType1.ToLower() == "guardian"
                                                                        && getFMGuardianIds.Contains((int)m.UserContactId)).Select(x => x.UserContactId).ToArray();

                                        var getFatherRelation = _IGuardianService.GetAllGuardians(ref count).Where(x => StudentGaurdianSchooluser.Contains(x.GuardianId)
                                                                    && x.Relation.Relation1.ToLower() == "father").FirstOrDefault();

                                        var getMotherrRelation = _IGuardianService.GetAllGuardians(ref count).Where(x => StudentGaurdianSchooluser.Contains(x.GuardianId)
                                                                   && x.Relation.Relation1.ToLower() == "mother").FirstOrDefault();


                                        var getguardianRelation = _IGuardianService.GetAllGuardians(ref count).Where(x => StudentGaurdianSchooluser.Contains(x.GuardianId)
                                                                  && x.Relation.Relation1.ToLower() == "guardian").FirstOrDefault();

                                        bool IsSent = false;
                                        //send msg to father
                                        if (getFatherRelation != null)
                                        {
                                            var UserSchool = _IUserService.GetAllUsers(ref count).Where(x => x.UserContactId == getFatherRelation.GuardianId && x.Status==true
                                                                && x.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();

                                            if (UserSchool != null)
                                            {
                                                IsSent = true;

                                                //create collapse Key for notifcitaion
                                                var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                var CounterNotification = 0;
                                                var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                       (currentDate + "_" + CounterNotification.ToString()), null
                                                                                       , "UniqueKey for Notifications on APP").AttributeValue;
                                                var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                {
                                                    CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                    CounterNotification++;
                                                }
                                                else
                                                    CounterNotification = 1;


                                                var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count)
                                                                       .Where(m => (m.UserId == UserSchool.UserId)
                                                                                   && m.UserDeviceName == "IOS")
                                                                      .Select(N => N.UserDeviceUniqueId).ToArray();
                                                var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count)
                                                               .Where(m => m.UserId == UserSchool.UserId
                                                                       && m.UserDeviceName == "Android")
                                                               .Select(N => N.UserDeviceUniqueId).ToArray();

                                                if (userdeviceIdsArray.Count() > 0)
                                                {
                                                    var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                            (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                    _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray,
                                                                                                    triggermessageTilte,
                                                                                                    triggermessage, "IOS", UpdateCollapseKey);
                                                }
                                                if (userdeviceIdsArrayAndroid.Count() > 0)
                                                {
                                                    var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                            (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                    _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid,
                                                                                                    triggermessageTilte,
                                                                                                    triggermessage, "Android", UpdateCollapseKey);
                                                }
                                            }

                                        }

                                        if (!IsSent)
                                        {
                                            //send msg to mother
                                            if (getMotherrRelation != null)
                                            {
                                                var UserSchool = _IUserService.GetAllUsers(ref count).Where(x => x.UserContactId == getMotherrRelation.GuardianId && x.Status == true
                                                                    && x.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();

                                                if (UserSchool != null)
                                                {
                                                    IsSent = true;

                                                    //create collapse Key for notifcitaion
                                                    var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                    var CounterNotification = 0;
                                                    var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                           (currentDate + "_" + CounterNotification.ToString()), null
                                                                                           , "UniqueKey for Notifications on APP").AttributeValue;
                                                    var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                    if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                    {
                                                        CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                        CounterNotification++;
                                                    }
                                                    else
                                                        CounterNotification = 1;

                                                    var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count)
                                                                           .Where(m => (m.UserId == UserSchool.UserId)
                                                                                       && m.UserDeviceName == "IOS")
                                                                          .Select(N => N.UserDeviceUniqueId).ToArray();
                                                    var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count)
                                                                   .Where(m => m.UserId == UserSchool.UserId
                                                                           && m.UserDeviceName == "Android")
                                                                   .Select(N => N.UserDeviceUniqueId).ToArray();

                                                    if (userdeviceIdsArray.Count() > 0)
                                                    {

                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                                                                                   (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray,
                                                                                                        triggermessageTilte,
                                                                                                        triggermessage, "IOS", UpdateCollapseKey);
                                                    }
                                                    if (userdeviceIdsArrayAndroid.Count() > 0)
                                                    {
                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                                                                                   (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid,
                                                                                                        triggermessageTilte,
                                                                                                        triggermessage, "Android", UpdateCollapseKey);
                                                    }
                                                }
                                            }
                                        }
                                        if (!IsSent)
                                        {
                                            //send msg to guardian
                                            if (getguardianRelation != null)
                                            {
                                                var UserSchool = _IUserService.GetAllUsers(ref count).Where(x => x.UserContactId == getguardianRelation.GuardianId && x.Status == true
                                                                    && x.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();

                                                if (UserSchool != null)
                                                {
                                                    IsSent = true;

                                                    //create collapse Key for notifcitaion
                                                    var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                    var CounterNotification = 0;
                                                    var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                           (currentDate + "_" + CounterNotification.ToString()), null
                                                                                           , "UniqueKey for Notifications on APP").AttributeValue;
                                                    var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                    if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                    {
                                                        CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                        CounterNotification++;
                                                    }
                                                    else
                                                        CounterNotification = 1;

                                                    var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count)
                                                                           .Where(m => (m.UserId == UserSchool.UserId)
                                                                                       && m.UserDeviceName == "IOS")
                                                                          .Select(N => N.UserDeviceUniqueId).ToArray();
                                                    var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count)
                                                                   .Where(m => m.UserId == UserSchool.UserId
                                                                           && m.UserDeviceName == "Android")
                                                                   .Select(N => N.UserDeviceUniqueId).ToArray();

                                                    if (userdeviceIdsArray.Count() > 0)
                                                    {

                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                             (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray,
                                                                                                        triggermessageTilte,
                                                                                                        triggermessage, "IOS", UpdateCollapseKey);
                                                    }
                                                    if (userdeviceIdsArrayAndroid.Count() > 0)
                                                    {
                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                          (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;



                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid,
                                                                                                        triggermessageTilte,
                                                                                                        triggermessage, "Android", UpdateCollapseKey);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
                if (SMS)
                {
                    #region SMS
                    if (SelectedAttendanceStatus.Contains(attendanceStatusAbbr) || SelectedAttendanceStatus == "")
                    {
                        var triggershooterSMS = trigger.TriggerShooters.Where(m => m.TriggerTemplate != null
                                                            && m.TriggerTemplate.IsActive == true
                                                            && m.TriggerTemplate.TriggerMaster != null
                                                            && m.TriggerTemplate.TriggerMaster.IsActive == true
                                                            && m.TriggerTemplate.TriggerMaster.TriggerType != null
                                                            && m.TriggerTemplate.TriggerMaster.TriggerType.IsActive == true
                                                            && m.TriggerTemplate.TriggerMaster.TriggerType.TriggerType1 == "SMS"
                                                            && m.TriggerTemplate.TriggerMsgTemplate != null
                                                            && m.TriggerTemplate.TriggerMsgTemplate.IsActive == true
                                                            && m.IsActive == true).ToList();

                        foreach (var triggershoot in triggershooterSMS)
                        {
                            var triggermessageSMS = triggershoot.TriggerTemplate.TriggerMsgTemplate.TriggerMsgTemplate1;
                            var triggermessageTilteSMS = triggershoot.TriggerTemplate.TriggerMsgTemplate.MsgTemplateName;


                            triggermessageSMS = triggermessageSMS.Replace("%HomeWork%", HWDescription);
                            triggermessageTilteSMS = triggermessageTilteSMS.Replace("%Subject%", Subject);
                            triggermessageSMS = triggermessageSMS.Replace("%Subject%", Subject);
                            triggermessageSMS = triggermessageSMS.Replace("%MessageDescription%", MessageDescription);
                            triggermessageSMS = triggermessageSMS.Replace("%PendingAmount%", PendingAmount);
                            triggermessageTilteSMS = triggermessageTilteSMS.Replace("%GatepassType%", GatePassType);

                            triggermessageSMS = triggermessageSMS.Replace("%GatepassType%", GatePassType);
                            triggermessageSMS = triggermessageSMS.Replace("%GatePassIssuedTo%", GatePassIssuedTo);
                            triggermessageSMS = triggermessageSMS.Replace("%GatePassStatus%", GatePassStatus);
                            triggermessageSMS = triggermessageSMS.Replace("%GatepassApprovedby%", GatepassApprovedby);

                            if (GatePassApprovedOn != null)
                                triggermessageSMS = triggermessageSMS.Replace("%GatePassApprovedOn%", ConvertDateForKendo((DateTime)GatePassApprovedOn));


                            if (student != null && Staff == null)
                            {
                                string Studentname = "";
                                Studentname = TrimStudentName(student, Studentname);

                                if (!IsUserFullDetail)
                                    triggermessageSMS = triggermessageSMS.Replace("%Name%", Studentname);
                                else
                                {
                                    if (!string.IsNullOrEmpty(student.AdmnNo))
                                        Studentname += " (" + student.AdmnNo + ")";

                                    triggermessageSMS = triggermessageSMS.Replace("%Name%", Studentname);
                                }

                                //triggermessageTilte = triggermessageTilte.Replace("%SenderName%", Sender);
                            }

                            triggermessageSMS = triggermessageSMS.Replace("%Status%", attendanceStatus);
                            if (AttendanceDate != null)
                                triggermessageSMS = triggermessageSMS.Replace("%PublishDateTime%", ConvertDateForKendo((DateTime)AttendanceDate));


                            if (LeaveFrom != null)
                                triggermessageSMS = triggermessageSMS.Replace("%LeaveFrom%", ConvertDateForKendo((DateTime)LeaveFrom));

                            if (LeaveTill != null)
                                triggermessageSMS = triggermessageSMS.Replace("%LeaveTill%", ConvertDateForKendo((DateTime)LeaveTill));

                            if (TimetableFromDate != null)
                                triggermessageSMS = triggermessageSMS.Replace("%TimeTableFromDate%", ConvertDateForKendo((DateTime)TimetableFromDate));

                            if (TimeTableTillDate != null)
                                triggermessageSMS = triggermessageSMS.Replace("%TimeTableToDate%", ConvertDateForKendo((DateTime)TimeTableTillDate));


                            triggermessageSMS = triggermessageSMS.Replace("%Day%", TimetableDays);
                            triggermessageSMS = triggermessageSMS.Replace("%LeaveStatus%", LeaveStatus);

                            triggermessageTilteSMS = triggermessageTilteSMS.Replace("%GatepassType%", gatePassType);
                            triggermessageSMS = triggermessageSMS.Replace("%DynamicText%", GatepassDynamicText);

                            triggermessageSMS = triggermessageSMS.Replace("%ApprovalStatus%", LeaveApprovalStatus);
                            triggermessageTilteSMS = triggermessageTilteSMS.Replace("%ApprovalStatus%", LeaveApprovalStatus);

                            triggermessageSMS = triggermessageSMS.Replace("%FeePeriod%", FeePeriodDescription);
                            triggermessageSMS = triggermessageSMS.Replace("%Amount%", PaidAmount);
                            triggermessageSMS = triggermessageSMS.Replace("%ReceiptNo%", !string.IsNullOrEmpty(StdClassReceiptNo) ? StdClassReceiptNo : "");
                            triggermessageSMS = triggermessageSMS.Replace("%Class%", ClassName);
                            triggermessageTilteSMS = triggermessageTilteSMS.Replace("%Class%", ClassName);

                            triggermessageSMS = triggermessageSMS.Replace("%Description%", SchedularDescription);
                            triggermessageTilteSMS = triggermessageTilteSMS.Replace("%EventTitle%", EventTitle);

                            //for adjustment
                            triggermessageSMS = triggermessageSMS.Replace("%SubstituteName%", SubstituteName);
                            triggermessageSMS = triggermessageSMS.Replace("%Teachername%", Teachername);
                            triggermessageSMS = triggermessageSMS.Replace("%Period%", Period);
                            triggermessageSMS = triggermessageSMS.Replace("%AdjustmentReason%", AdjustmentReason);
                            triggermessageTilteSMS = triggermessageTilteSMS.Replace("%SenderName%", Sender);

                            triggermessageSMS = triggermessageSMS.Replace("%DatesheetName%", DateSheetName);
                            triggermessageTilteSMS = triggermessageTilteSMS.Replace("%DatesheetName%", DateSheetName);
                            triggermessageSMS = triggermessageSMS.Replace("%ExamActivity%", ExamActivity);

                            if (ExamDateFrom != null)
                                triggermessageSMS = triggermessageSMS.Replace("%ExamFromDate%", ConvertDateForKendo((DateTime)ExamDateFrom));

                            if (ExamDateTill != null)
                                triggermessageSMS = triggermessageSMS.Replace("%ExamTillDate%", ConvertDateForKendo((DateTime)ExamDateTill));

                            if (PublishDate != null)
                                triggermessageSMS = triggermessageSMS.Replace("%ExamPublishDate%", ConvertDateForKendo((DateTime)PublishDate));

                            if (AdjustmentDate != null)
                                triggermessageSMS = triggermessageSMS.Replace("%AdjustmentDate%", ConvertDateForKendo((DateTime)AdjustmentDate));

                            var errormessage = "";
                            var content = "";
                            var activeuserslist = new List<dynamicusers>();
                            if (IsStaff)
                            {
                                var contactType = new ContactType();
                                #region trigger for staff
                                if (Staff != null)
                                {
                                    string Staffname = "";
                                    Staffname = TrimStaffName(Staff, Staffname);

                                    if (!IsUserFullDetail)
                                        triggermessageSMS = triggermessageSMS.Replace("%Name%", Staffname);
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(Staff.EmpCode))
                                            Staffname += " (" + Staff.EmpCode + ")";

                                        triggermessageSMS = triggermessageSMS.Replace("%Name%", Staffname);
                                    }

                                    if ((bool)Staff.StaffType.IsTeacher)
                                        contactType = db.ContactTypes.Where(m => m.ContactType1.ToLower() == "teacher").FirstOrDefault();
                                    else
                                        contactType = db.ContactTypes.Where(m => m.ContactType1.ToLower() == "non-teaching").FirstOrDefault();
                                }

                                if (IsAdmin)
                                    contactType = _IAddressMasterService.GetAllContactTypes(ref count)
                                                    .Where(m => m.ContactType1 == "Admin").FirstOrDefault();

                                var arrayStaffs = StaffIds.Split(',');
                                foreach (var i in arrayStaffs)
                                {
                                    var dynamicusers = new dynamicusers();
                                    int Staff_Id = Convert.ToInt32(i);
                                    var getStaff = _IStaffService.GetStaffById(Staff_Id);
                                    if (getStaff != null)
                                    {
                                        if (!IsAdmin)
                                        {
                                            if ((bool)getStaff.StaffType.IsTeacher)
                                                contactType = db.ContactTypes.Where(m => m.ContactType1.ToLower() == "teacher").FirstOrDefault();
                                            else
                                                contactType = db.ContactTypes.Where(m => m.ContactType1.ToLower() == "non-teaching").FirstOrDefault();
                                        }

                                        var StaffContactNumber = _IContactService.GetAllContactInfos(ref count)
                                                                  .Where(x => x.ContactId == getStaff.StaffId
                                                                  && x.ContactTypeId == contactType.ContactTypeId
                                                                  && x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                                  && x.IsDefault == true && x.Status == true).FirstOrDefault();

                                        string SendMsgMobileNo = "";
                                        if (StaffContactNumber != null)
                                            SendMsgMobileNo = StaffContactNumber.ContactInfo1;

                                        bool YesAppUser = false;
                                        if (IsNonAPPUser)
                                        {
                                            YesAppUser = CheckAppUser(getStaff.StaffId, IsNonAPPUser, contactType.ContactTypeId);
                                            if (!YesAppUser)
                                            {
                                                if (!string.IsNullOrEmpty(SendMsgMobileNo))
                                                {
                                                    dynamicusers.MobileNo = SendMsgMobileNo;
                                                    activeuserslist.Add(dynamicusers);
                                                    //_ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //for SMS send by choosing option that which person do u send msg.
                                            if (!string.IsNullOrEmpty(SendMsgMobileNo))
                                            {
                                                dynamicusers.MobileNo = SendMsgMobileNo;
                                                activeuserslist.Add(dynamicusers);
                                                // _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                            else if (IsGuardian)
                            {
                                #region trigger for guardian
                                if (GuradianIds != "")
                                {
                                   // var arraygaurds = GuradianIds.Split(',');
                                    var contactType = db.ContactTypes.Where(m => m.ContactType1 == "Guardian").FirstOrDefault();
                                    //foreach (var i in arraygaurds)
                                    //{
                                    var custodyright = student.CustodyRight.CustodyRight1;
                                    var allguardians = _IGuardianService.GetAllGuardians(ref count, StudentId: student.StudentId).ToList();
                                    // Guardian Relation type
                                    var GurdRelationType = _IGuardianService.GetAllRelationTypes(ref count, "Guardian").FirstOrDefault();
                                    // guardian relations
                                    var FatherRelation = _IGuardianService.GetAllRelations(ref count, Relation: "Father", RelationtypeId: GurdRelationType.RelationTypeId).FirstOrDefault();
                                    var MotherRelation = _IGuardianService.GetAllRelations(ref count, Relation: "Mother", RelationtypeId: GurdRelationType.RelationTypeId).FirstOrDefault();
                                    var GuardianRelation = _IGuardianService.GetAllRelations(ref count, Relation: "Guardian", RelationtypeId: GurdRelationType.RelationTypeId).FirstOrDefault();
                                    int Gaurdian_Id = 0;
                                    switch (custodyright)
                                    {
                                        case "All":
                                            Gaurdian_Id = allguardians.FirstOrDefault()!=null? (int)allguardians.FirstOrDefault().StudentId:0;
                                            break;
                                        case "Father":
                                            Gaurdian_Id = allguardians.Where(g => g.GuardianTypeId == FatherRelation.RelationId).FirstOrDefault()!=null? (int)allguardians.Where(g => g.GuardianTypeId == FatherRelation.RelationId).FirstOrDefault().GuardianId:0;
                                            break;
                                        case "Mother":
                                            Gaurdian_Id = allguardians.Where(g => g.GuardianTypeId == MotherRelation.RelationId).FirstOrDefault()!=null?(int)allguardians.Where(g => g.GuardianTypeId == MotherRelation.RelationId).FirstOrDefault().GuardianId : 0;
                                            break;
                                        case "Guardian":
                                            Gaurdian_Id = allguardians.Where(g => g.GuardianTypeId == GuardianRelation.RelationId).FirstOrDefault()!=null?(int)allguardians.Where(g => g.GuardianTypeId == GuardianRelation.RelationId).FirstOrDefault().GuardianId : 0;
                                            break;
                                    }
                                    var dynamicusers = new dynamicusers();
                                        //int Gaurdian_Id = Convert.ToInt32(i);
                                        var getGaurdian = db.Guardians.Where(x => x.GuardianId == Gaurdian_Id).FirstOrDefault();
                                        if (getGaurdian != null)
                                        {
                                            var ContactNumber = _IContactService.GetAllContactInfos(ref count)
                                                                     .Where(x => x.ContactId == getGaurdian.GuardianId
                                                                     && x.ContactTypeId == contactType.ContactTypeId
                                                                     && x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                                     && x.IsDefault == true && x.Status == true).FirstOrDefault();
                                            string SendMsgMobileNo = "";
                                            if (ContactNumber != null)
                                                SendMsgMobileNo = ContactNumber.ContactInfo1;

                                            string guardianname = "";
                                            guardianname = TrimGuardianName(getGaurdian, guardianname);
                                            triggermessageSMS = triggermessageSMS.Replace("%ParentDetail%", guardianname + " "
                                                                                        + getGaurdian.Relation.Relation1);
                                            //send student Notification for HW
                                            var GaurdianSchooluser = db.SchoolUsers
                                                                    .Where(m => m.UserTypeId == contactType.ContactTypeId
                                                                     && m.UserContactId == getGaurdian.GuardianId).FirstOrDefault();

                                            bool YesAppUser = false;
                                            if (IsNonAPPUser)
                                            {
                                                YesAppUser = CheckAppUser(getGaurdian.GuardianId, IsNonAPPUser, contactType.ContactTypeId);
                                                if (!YesAppUser)
                                                {
                                                    if (!string.IsNullOrEmpty(SendMsgMobileNo))
                                                    {

                                                        dynamicusers.MobileNo = SendMsgMobileNo;
                                                        activeuserslist.Add(dynamicusers);
                                                        // _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(SendMsgMobileNo))
                                                {
                                                    dynamicusers.MobileNo = SendMsgMobileNo;
                                                    activeuserslist.Add(dynamicusers);
                                                    //_ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                }

                                            }
                                        }
                                    //}
                                    //foreach (var i in arraygaurds)
                                    //{
                                    //    var dynamicusers = new dynamicusers();
                                    //    int Gaurdian_Id = Convert.ToInt32(i);
                                    //    var getGaurdian = db.Guardians.Where(x => x.GuardianId == Gaurdian_Id).FirstOrDefault();
                                    //    if (getGaurdian != null)
                                    //    {
                                    //        var ContactNumber = _IContactService.GetAllContactInfos(ref count)
                                    //                                 .Where(x => x.ContactId == getGaurdian.GuardianId
                                    //                                 && x.ContactTypeId == contactType.ContactTypeId
                                    //                                 && x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                    //                                 && x.IsDefault == true && x.Status == true).FirstOrDefault();
                                    //        string SendMsgMobileNo = "";
                                    //        if (ContactNumber != null)
                                    //            SendMsgMobileNo = ContactNumber.ContactInfo1;

                                    //        string guardianname = "";
                                    //        guardianname = TrimGuardianName(getGaurdian, guardianname);
                                    //        triggermessageSMS = triggermessageSMS.Replace("%ParentDetail%", guardianname + " "
                                    //                                                    + getGaurdian.Relation.Relation1);
                                    //        //send student Notification for HW
                                    //        var GaurdianSchooluser = db.SchoolUsers
                                    //                                .Where(m => m.UserTypeId == contactType.ContactTypeId
                                    //                                 && m.UserContactId == getGaurdian.GuardianId).FirstOrDefault();

                                    //        bool YesAppUser = false;
                                    //        if (IsNonAPPUser)
                                    //        {
                                    //            YesAppUser = CheckAppUser(getGaurdian.GuardianId, IsNonAPPUser, contactType.ContactTypeId);
                                    //            if (!YesAppUser)
                                    //            {
                                    //                if (!string.IsNullOrEmpty(SendMsgMobileNo))
                                    //                {

                                    //                    dynamicusers.MobileNo = SendMsgMobileNo;
                                    //                    activeuserslist.Add(dynamicusers);
                                    //                    // _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                    //                }
                                    //            }
                                    //        }
                                    //        else
                                    //        {
                                    //            if (!string.IsNullOrEmpty(SendMsgMobileNo))
                                    //            {
                                    //                dynamicusers.MobileNo = SendMsgMobileNo;
                                    //                activeuserslist.Add(dynamicusers);
                                    //                //_ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                    //            }

                                    //        }
                                    //    }
                                    //}
                                }
                                #endregion
                            }
                            else
                            {

                                #region trigger for student
                                var contactType = _IAddressMasterService.GetAllContactTypes(ref count)
                                                        .Where(m => m.ContactType1 == "Student").FirstOrDefault();
                                foreach (var item in Students)
                                {
                                    var dynamicusers = new dynamicusers();
                                    var Student = _IStudentService.GetAllStudents(ref count)
                                                    .Where(m => m.StudentId == item.StudentId).FirstOrDefault();

                                    if (Student != null)
                                    {
                                        bool YesAppUser = false;
                                        if (IsNonAPPUser)
                                        {
                                            YesAppUser = CheckAppUser(Student.StudentId, IsNonAPPUser, contactType.ContactTypeId);
                                            if (!YesAppUser)
                                            {
                                                string SendMsgMobileNo = Student.SMSMobileNo;
                                                //for SMS send by choosing option that which person do u send msg.
                                                SendMsgMobileNo = ChooseStudentMobileNo(SendSMSMobileOptionId, Student.StudentId, SendMsgMobileNo);
                                                if (!string.IsNullOrEmpty(SendMsgMobileNo))
                                                {
                                                    dynamicusers.MobileNo = SendMsgMobileNo;
                                                    activeuserslist.Add(dynamicusers);
                                                    // _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string SendMsgMobileNo = Student.SMSMobileNo;
                                            //for SMS send by choosing option that which person do u send msg.
                                            SendMsgMobileNo = ChooseStudentMobileNo(SendSMSMobileOptionId, Student.StudentId, SendMsgMobileNo);
                                            if (!string.IsNullOrEmpty(SendMsgMobileNo))
                                            {

                                                dynamicusers.MobileNo = SendMsgMobileNo;
                                                activeuserslist.Add(dynamicusers);
                                                //_ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            var MaxNoOfRecipientsinSMS = _ISchoolSettingService.GetorSetSchoolData("MaxNoOfRecipientsinSMS", "2").AttributeValue;
                            var contactinfolist = new List<string>();
                            //send users sms seprated by comma
                            foreach (var userPhone in activeuserslist)
                                contactinfolist.Add(userPhone.MobileNo);

                            if (contactinfolist.Count > 0)
                            {
                                //send multiple messages
                                while (contactinfolist.Any())
                                {
                                    var selectedlist = contactinfolist.Take(Convert.ToInt32(MaxNoOfRecipientsinSMS));
                                    contactinfolist = contactinfolist.Skip(Convert.ToInt32(MaxNoOfRecipientsinSMS)).ToList();
                                    //MaxNoOfRecipientsinSMS
                                    if (selectedlist.Count() == 1)
                                    {
                                        _ISMSSender.SendSMS(String.Join("", selectedlist), triggermessageSMS, false,
                                                                       out errormessage, out content);
                                    }
                                    else
                                    {
                                        _ISMSSender.SendSMS(String.Join(",", selectedlist), triggermessageSMS, false,
                                                                                        out errormessage, out content);
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
        }


        public string ChooseStudentMobileNo(string SendMessageMobileNoSelection_Id = "", int? studentId = null, string contactinfo = "")
        {
            //check selected number from popup to send message
            if (!string.IsNullOrEmpty(SendMessageMobileNoSelection_Id))
            {
                switch (SendMessageMobileNoSelection_Id)
                {
                    //choose student mobile number if have
                    case "SMN":
                        var StudentMobileNo = _IContactService.GetAllContactInfos(ref count).Where(m => m.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                    && m.ContactType.ContactType1.ToLower() == "student"
                                                    && m.ContactId == studentId
                                                    && m.Status == true && m.IsDefault == true).FirstOrDefault();
                        if (StudentMobileNo != null)
                            contactinfo = StudentMobileNo.ContactInfo1;
                        break;


                    //choose student SMS mobile number if have
                    case "SMS":
                        var StudentSMSMobileNo = _IStudentService.GetStudentById((int)studentId);
                        if (StudentSMSMobileNo != null)
                            contactinfo = StudentSMSMobileNo.SMSMobileNo;
                        break;


                    //choose custody guardian mobile number if have
                    case "GMN":
                        int ContactId = 0;
                        var guardianInfo = new Guardian();
                        var StuCustodyRight = _IStudentService.GetStudentById((int)studentId);
                        if (StuCustodyRight != null)
                        {
                            guardianInfo = _IGuardianService.GetAllGuardians(ref count, StudentId: studentId).FirstOrDefault();
                            if (guardianInfo != null)
                                ContactId = guardianInfo.GuardianId;

                            if (StuCustodyRight.CustodyRight != null)
                            {
                                switch (StuCustodyRight.CustodyRight.CustodyRight1.ToLower())
                                {
                                    case "father":
                                        guardianInfo = _IGuardianService.GetAllGuardians(ref count, StudentId: studentId)
                                                        .Where(x => x.Relation.Relation1.ToLower() == "father").FirstOrDefault();
                                        if (guardianInfo != null)
                                            ContactId = guardianInfo.GuardianId;
                                        break;

                                    case "mother":
                                        guardianInfo = _IGuardianService.GetAllGuardians(ref count, StudentId: studentId)
                                                       .Where(x => x.Relation.Relation1.ToLower() == "mother").FirstOrDefault();
                                        if (guardianInfo != null)
                                            ContactId = guardianInfo.GuardianId;
                                        break;

                                    case "guardian":
                                        guardianInfo = _IGuardianService.GetAllGuardians(ref count, StudentId: studentId)
                                                       .Where(x => x.Relation.Relation1.ToLower() == "guardian").FirstOrDefault();
                                        if (guardianInfo != null)
                                            ContactId = guardianInfo.GuardianId;
                                        break;
                                }
                            }

                            if (ContactId > 0)
                            {
                                var getContactInfo = _IContactService.GetAllContactInfos(ref count)
                                                    .Where(x => x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                    && x.ContactType.ContactType1.ToLower() == "guardian"
                                                    && x.ContactId == ContactId
                                                    && x.Status == true && x.IsDefault == true).FirstOrDefault();

                                if (getContactInfo != null)
                                    contactinfo = getContactInfo.ContactInfo1;
                            }
                        }
                        break;
                }
            }
            return contactinfo;
        }

        public bool CheckAppUser(int? userId = null, bool IsNonAPPUser = false, int? UserTypeId = null)
        {
            if (IsNonAPPUser)
            {
                if (userId != null)
                {
                    var SchoolUser = _IUserService.GetAllUsers(ref count, UserTypeId: UserTypeId, UserContactId: userId).FirstOrDefault();
                    if (SchoolUser != null)
                    {
                        var userDevice = _IUserService.GetAllUserDevices(ref count).Where(x => x.UserId == SchoolUser.UserId).ToList();
                        if (userDevice.Count > 0)
                        {
                            IsNonAPPUser = true;
                        }
                    }
                    else
                        IsNonAPPUser = false;
                }
            }
            return IsNonAPPUser;
        }

        public void CountOfUserForSMsNotification(SMSNotificationModel MessengerModel, ref int NC, ref int SMSC,
                    ref List<int> Id_array, int?[] getNotPresntUsers = null, int?[] getPresntUsers = null,
                    string UserType = "", IEnumerable<Student> GetStudents = null,
                    IEnumerable<Guardian> GetGuardians = null, IEnumerable<Staff> GetStaffs = null, IEnumerable<Staff> GetNonStaffs = null,
                    string SendMessageMobileNoSelection_Id = "")
        {
            switch (UserType.ToLower())
            {
                case "student":
                    #region
                    if (MessengerModel.AttendanceStatus == "NotPresent")
                        GetStudents = GetStudents.Where(x => getNotPresntUsers.Contains(x.StudentId));
                    else if (MessengerModel.AttendanceStatus == "Present")
                        GetStudents = GetStudents.Where(x => getPresntUsers.Contains(x.StudentId));

                    if (MessengerModel.RecipientStudentType == "Hostler")
                        GetStudents = GetStudents.Where(m => m.IsHostler == true);
                    else if (MessengerModel.RecipientStudentType == "DayScholar")
                        GetStudents = GetStudents.Where(m => m.IsHostler == false || m.IsHostler == null);

                    foreach (var stu in GetStudents)
                    {
                        string ContactInfo = ChooseStudentMobileNo(SendMessageMobileNoSelection_Id, stu.StudentId);
                        //if SMS tick and NonAppUser tick then Send MSG to non app usrs
                        if (MessengerModel.IsSMS && MessengerModel.IsNonAppUserSMS)
                        {
                            var schooluser = _IUserService.GetAllUsers(ref count, UserTypeId: MessengerModel.ContacttypeId,
                                                     UserContactId: stu.StudentId).FirstOrDefault();

                            if (schooluser == null && !string.IsNullOrEmpty(ContactInfo))
                            {
                                SMSC++;
                                Id_array.Add(stu.StudentId);
                            }

                        }
                        else if (MessengerModel.IsSMS && !MessengerModel.IsNonAppUserSMS)
                        {
                            //if SMS tick and untick Nonapp user then send msges to All users that have contactinfo
                            if (!string.IsNullOrEmpty(ContactInfo))
                            {
                                SMSC++;
                                Id_array.Add(stu.StudentId);
                            }
                        }

                        //if Notifiction tick then send all users notification that has account
                        if (MessengerModel.IsNotification)
                        {
                            var schooluser = _IUserService.GetAllUsers(ref count, UserTypeId: MessengerModel.ContacttypeId,
                                                     UserContactId: stu.StudentId).FirstOrDefault();
                            if (schooluser != null)
                            {
                                NC++;
                                Id_array.Add(stu.StudentId);
                            }
                        }
                    }
                    #endregion
                    break;
                case "guardian":
                    #region
                    if (MessengerModel.AttendanceStatus == "NotPresent")
                        GetGuardians = GetGuardians.Where(x => getNotPresntUsers.Contains(x.StudentId));
                    else if (MessengerModel.AttendanceStatus == "Present")
                        GetGuardians = GetGuardians.Where(x => getPresntUsers.Contains(x.StudentId));

                    if (MessengerModel.RecipientStudentType == "Hostler")
                        GetGuardians = GetGuardians.Where(m => m.Student.IsHostler == true);
                    else if (MessengerModel.RecipientStudentType == "DayScholar")
                        GetGuardians = GetGuardians.Where(m => m.Student.IsHostler == false || m.Student.IsHostler == null);

                    foreach (var gurd in GetGuardians)
                    {
                        var ContactInfo = _IContactService.GetAllContactInfos(ref count, ContactId: gurd.GuardianId,
                                                    Isdefault: true, status: true)
                                                .Where(x => x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                 && x.ContactType.ContactTypeId == MessengerModel.ContacttypeId).FirstOrDefault();
                        string ContactNo = ContactInfo != null ? ContactInfo.ContactInfo1 : "";
                        //if SMS tick and NonAppUser tick then Send MSG to non app usrs
                        if (MessengerModel.IsSMS && MessengerModel.IsNonAppUserSMS)
                        {
                            var schooluser = _IUserService.GetAllUsers(ref count, UserTypeId: MessengerModel.ContacttypeId,
                                                     UserContactId: gurd.GuardianId).FirstOrDefault();
                            if (schooluser == null && !string.IsNullOrEmpty(ContactNo))
                            {
                                SMSC++;
                                Id_array.Add(gurd.GuardianId);
                            }

                        }
                        else if (MessengerModel.IsSMS && !MessengerModel.IsNonAppUserSMS)
                        {
                            //if SMS tick and untick Nonapp user then send msges to All users that have contactinfo

                            if (!string.IsNullOrEmpty(ContactNo))
                            {
                                SMSC++;
                                Id_array.Add(gurd.GuardianId);
                            }
                        }

                        //if Notifiction tick then send all users notification that has account
                        if (MessengerModel.IsNotification)
                        {
                            var schooluser = _IUserService.GetAllUsers(ref count, UserTypeId: MessengerModel.ContacttypeId,
                                                     UserContactId: gurd.GuardianId).FirstOrDefault();
                            if (schooluser != null)
                            {
                                NC++;
                                Id_array.Add(gurd.GuardianId);
                            }
                        }
                    }
                    #endregion
                    break;
                case "teacher":
                    #region
                    if (MessengerModel.AttendanceStatus == "NotPresent")
                        GetStaffs = GetStaffs.Where(x => getNotPresntUsers.Contains(x.StaffId));
                    else if (MessengerModel.AttendanceStatus == "Present")
                        GetStaffs = GetStaffs.Where(x => getPresntUsers.Contains(x.StaffId));

                    if (!string.IsNullOrEmpty(MessengerModel.RecipientStudentType))
                        GetStaffs = GetStaffs.Where(m => m.StaffCategory != null
                                    && m.StaffCategory.CategoryAbbr == MessengerModel.RecipientStudentType);

                    foreach (var staff in GetStaffs)
                    {
                        var ContactInfo = _IContactService.GetAllContactInfos(ref count, ContactId: staff.StaffId,
                                                    Isdefault: true, status: true).Where(x => x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                        && x.ContactType.ContactTypeId == MessengerModel.ContacttypeId).FirstOrDefault();
                        string ContactNo = ContactInfo != null ? ContactInfo.ContactInfo1 : "";
                        //if SMS tick and NonAppUser tick then Send MSG to non app usrs
                        if (MessengerModel.IsSMS && MessengerModel.IsNonAppUserSMS)
                        {
                            var schooluser = _IUserService.GetAllUsers(ref count, UserTypeId: MessengerModel.ContacttypeId,
                                                     UserContactId: staff.StaffId).FirstOrDefault();

                            if (schooluser == null && !string.IsNullOrEmpty(ContactNo))
                            {
                                SMSC++;
                                Id_array.Add(staff.StaffId);
                            }

                        }
                        else if (MessengerModel.IsSMS && !MessengerModel.IsNonAppUserSMS)
                        {
                            //if SMS tick and untick Nonapp user then send msges to All users that have contactinfo
                            if (!string.IsNullOrEmpty(ContactNo))
                            {
                                SMSC++;
                                Id_array.Add(staff.StaffId);
                            }
                        }

                        //if Notifiction tick then send all users notification that has account
                        if (MessengerModel.IsNotification)
                        {
                            var schooluser = _IUserService.GetAllUsers(ref count, UserTypeId: MessengerModel.ContacttypeId,
                                                     UserContactId: staff.StaffId).FirstOrDefault();
                            if (schooluser != null)
                            {
                                NC++;
                                Id_array.Add(staff.StaffId);
                            }
                        }
                    }
                    #endregion
                    break;
                case "non-teaching":
                    #region
                    if (MessengerModel.AttendanceStatus == "NotPresent")
                        GetStaffs = GetStaffs.Where(x => getNotPresntUsers.Contains(x.StaffId));
                    else if (MessengerModel.AttendanceStatus == "Present")
                        GetStaffs = GetStaffs.Where(x => getPresntUsers.Contains(x.StaffId));

                    foreach (var staff in GetStaffs)
                    {
                        var ContactInfo = _IContactService.GetAllContactInfos(ref count, ContactId: staff.StaffId,
                                                    Isdefault: true, status: true).Where(x => x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                        && x.ContactType.ContactTypeId == MessengerModel.ContacttypeId).FirstOrDefault();

                        string ContactNo = ContactInfo != null ? ContactInfo.ContactInfo1 : "";
                        //if SMS tick and NonAppUser tick then Send MSG to non app usrs
                        if (MessengerModel.IsSMS && MessengerModel.IsNonAppUserSMS)
                        {
                            var schooluser = _IUserService.GetAllUsers(ref count, UserTypeId: MessengerModel.ContacttypeId,
                                                     UserContactId: staff.StaffId).FirstOrDefault();

                            if (schooluser == null && !string.IsNullOrEmpty(ContactNo))
                            {
                                SMSC++;
                                Id_array.Add(staff.StaffId);
                            }

                        }
                        else if (MessengerModel.IsSMS && !MessengerModel.IsNonAppUserSMS)
                        {
                            //if SMS tick and untick Nonapp user then send msges to All users that have contactinfo
                            if (!string.IsNullOrEmpty(ContactNo))
                            {
                                SMSC++;
                                Id_array.Add(staff.StaffId);
                            }
                        }

                        //if Notifiction tick then send all users notification that has account
                        if (MessengerModel.IsNotification)
                        {
                            var schooluser = _IUserService.GetAllUsers(ref count, UserTypeId: MessengerModel.ContacttypeId,
                                                     UserContactId: staff.StaffId).FirstOrDefault();
                            if (schooluser != null)
                            {
                                NC++;
                                Id_array.Add(staff.StaffId);
                            }
                        }
                    }
                    #endregion
                    break;
            }

        }

        public void UpdateCreateSendSmsStudentsTextFile(string SchoolDbId,
                                  List<SendSelectedStudentsSMS> ListSendSelectedStudentsSMS, string TextFileName = "")
        {
            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(ListSendSelectedStudentsSMS.ToArray());
            string path = System.Web.HttpContext.Current.Server.MapPath("~/Images/" + SchoolDbId + "/UserDataFile");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);

                var pathString = System.IO.Path.Combine(path, TextFileName);
                if (!System.IO.File.Exists(pathString))
                {
                    using (System.IO.FileStream fs = new FileStream(pathString, FileMode.Create, FileAccess.ReadWrite))
                    {
                        fs.Dispose();
                        fs.Close();
                        //write string to file
                        System.IO.File.WriteAllText(pathString, json);
                    }
                }
                else
                {
                    System.IO.File.WriteAllText(pathString, String.Empty);
                    System.IO.File.WriteAllText(pathString, json);
                }
            }
            else
            {
                var pathString = System.IO.Path.Combine(path, TextFileName);
                if (!System.IO.File.Exists(pathString))
                {
                    using (System.IO.FileStream fs = new FileStream(pathString, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                    {
                        fs.Dispose();
                        fs.Close();
                        //System.IO.File.Create(pathString).Close();
                        //write string to file
                        System.IO.File.WriteAllText(pathString, json);
                    }
                }
                else
                {
                    System.IO.File.WriteAllText(pathString, String.Empty);
                    System.IO.File.WriteAllText(pathString, json);
                }
            }
        }


        #endregion

        #region SMSDefaultSettings
        public KSModel.Models.SMSDefaultSetting GetSMSDefaultSettingById(int? SMSDefaultSettingsId, bool IsTrack = false)
        {
            if (SMSDefaultSettingsId == 0)
                throw new ArgumentNullException("SMSDefaultSettings");

            var SMSDefaultSetting = new SMSDefaultSetting();

            if (IsTrack)
                SMSDefaultSetting = db.SMSDefaultSettings.Where(m => m.SMSDefaultSettingsId == SMSDefaultSettingsId).FirstOrDefault();
            else
                SMSDefaultSetting = db.SMSDefaultSettings.AsNoTracking().Where(m => m.SMSDefaultSettingsId == SMSDefaultSettingsId).FirstOrDefault();

            return SMSDefaultSetting;
        }

        public List<SMSDefaultSetting> GetAllSMSDefaultSettings(ref int totalcount, string FormKey = "", bool Isfilter = false,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SMSDefaultSettings.ToList();

            if (!String.IsNullOrEmpty(FormKey))
            {
                if (Isfilter)
                {
                    query = query.Where(e => e.FormKey.ToLower().Contains(FormKey.ToLower())).ToList();
                }
                else
                {
                    query = query.Where(e => e.FormKey.ToLower() == FormKey.ToLower()).ToList();
                }
            }

            var SMSDefaultSettings = new PagedList<SMSDefaultSetting>(query, PageIndex, PageSize);
            return SMSDefaultSettings;
        }

        public SMSDefaultSetting GetOrSetSMSDefaultSetting(string FormKey = "", bool SMS = false, bool ToNonAppUser = false,
                                   bool Notification = false, string SelectedMobile = "", string StatusSelection = "")
        {
            // Custom Object 
            var SMSDefaultSettingObj = new SMSDefaultSetting();
            var IsKeyExist = db.SMSDefaultSettings.AsNoTracking().Where(g => g.FormKey.ToLower() == FormKey.ToLower()).FirstOrDefault();
            if (IsKeyExist != null)
            {
                SMSDefaultSettingObj.SMSDefaultSettingsId = IsKeyExist.SMSDefaultSettingsId;
                SMSDefaultSettingObj.FormKey = IsKeyExist.FormKey;
                SMSDefaultSettingObj.SMS = IsKeyExist.SMS;
                SMSDefaultSettingObj.ToNonAppUser = IsKeyExist.ToNonAppUser;
                SMSDefaultSettingObj.Notification = IsKeyExist.Notification;
                SMSDefaultSettingObj.SelectedMobile = IsKeyExist.SelectedMobile;
                SMSDefaultSettingObj.StatusSelection = IsKeyExist.StatusSelection;
            }
            else
            {
                SMSDefaultSettingObj = new SMSDefaultSetting();
                SMSDefaultSettingObj.FormKey = FormKey;
                SMSDefaultSettingObj.SMS = SMS;
                SMSDefaultSettingObj.ToNonAppUser = ToNonAppUser;
                SMSDefaultSettingObj.Notification = Notification;
                SMSDefaultSettingObj.SelectedMobile = SelectedMobile;
                SMSDefaultSettingObj.StatusSelection = StatusSelection;

                InsertSMSDefaultSetting(SMSDefaultSettingObj);
            }
            return SMSDefaultSettingObj;
        }

        public void InsertSMSDefaultSetting(KSModel.Models.SMSDefaultSetting SMSDefaultSetting)
        {
            if (SMSDefaultSetting == null)
                throw new ArgumentNullException("SMSDefaultSetting");

            db.SMSDefaultSettings.Add(SMSDefaultSetting);
            db.SaveChanges();
        }

        public void UpdateSMSDefaultSetting(KSModel.Models.SMSDefaultSetting SMSDefaultSetting)
        {
            if (SMSDefaultSetting == null)
                throw new ArgumentNullException("SMSDefaultSetting");

            db.Entry(SMSDefaultSetting).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSMSDefaultSetting(int SMSDefaultSettingsId = 0)
        {
            if (SMSDefaultSettingsId == 0)
                throw new ArgumentNullException("SMSDefaultSettings");

            var SMSDefaultSetting = (from s in db.SMSDefaultSettings
                                     where s.SMSDefaultSettingsId == SMSDefaultSettingsId
                                     select s).FirstOrDefault();
            db.SMSDefaultSettings.Remove(SMSDefaultSetting);
            db.SaveChanges();
        }

        #endregion

        #region Navigation
        public void UpdateCreateUserRoleActionPermissionTextFile(string SchoolDbId, List<UserRoleActionPermission> ListUserActionPermission)
        {
            var List = ListUserActionPermission.Select(x => new
            {
                x.UserRoleActionPermissionId,
                x.UserRoleId,
                x.AppFormActionId,
                x.AppFormId,
                x.IsForm,
                x.IsPermitted
            }).AsQueryable();
            var jsonSerialiser = new JavaScriptSerializer();
            var json = jsonSerialiser.Serialize(List.ToArray());
            string path = System.Web.HttpContext.Current.Server.MapPath("~/Images/" + SchoolDbId + "/UserDataFile");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);

                var pathString = System.IO.Path.Combine(path, "UserRoleActionPermissions.txt");
                if (!System.IO.File.Exists(pathString))
                {
                    using (System.IO.FileStream fs = new FileStream(pathString, FileMode.Create, FileAccess.ReadWrite))
                    {
                        fs.Dispose();
                        fs.Close();
                        //write string to file
                        System.IO.File.WriteAllText(pathString, json);
                    }
                }
                else
                {
                    System.IO.File.WriteAllText(pathString, String.Empty);
                    System.IO.File.WriteAllText(pathString, json);
                }
            }
            else
            {
                var pathString = System.IO.Path.Combine(path, "UserRoleActionPermissions.txt");
                if (!System.IO.File.Exists(pathString))
                {
                    using (System.IO.FileStream fs = new FileStream(pathString, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                    {
                        fs.Dispose();
                        fs.Close();
                        //System.IO.File.Create(pathString).Close();
                        //write string to file
                        System.IO.File.WriteAllText(pathString, json);
                    }
                }
                else
                {
                    System.IO.File.WriteAllText(pathString, String.Empty);
                    System.IO.File.WriteAllText(pathString, json);
                }
            }
        }
        #endregion

        #region UserRole
        public DataTable GetSuperDatables(int UserRoleTypeId = 0, int AppFormId = 0, bool IsFindRole = false,
                                        bool IsFindAppForm = false)
        {
            DataTable dt = new DataTable();
            SqlConnection connection;
            SqlCommand command = new SqlCommand();
            SqlDataAdapter adapter = new SqlDataAdapter();
            string Datasource = Convert.ToString(WebConfigurationManager.AppSettings["DataSource"]);
            string SuperDatabasePassword = Convert.ToString(WebConfigurationManager.AppSettings["SuperDatabasePassword"]);
            string SuperDatabase = Convert.ToString(WebConfigurationManager.AppSettings["SuperDatabase"]);
            string SuperDatabaseUsername = Convert.ToString(WebConfigurationManager.AppSettings["SuperDatabaseUsername"]);
            string DataSource = "";
            string Password = "";
            string Catatlog = "";
            string UserId = "";

            if (!string.IsNullOrEmpty(Datasource) && !string.IsNullOrEmpty(SuperDatabasePassword)
                && !string.IsNullOrEmpty(SuperDatabase) &&
                !string.IsNullOrEmpty(SuperDatabaseUsername))
            {
                DataSource = Datasource;
                Password = SuperDatabasePassword;
                Catatlog = SuperDatabase;
                UserId = SuperDatabaseUsername;
            }


            string sqlConnectionString = @"Data Source=" + DataSource.ToString() + ";Initial Catalog=" + Catatlog.ToString() + ";User ID=" + UserId.ToString() + ";Password=" + Password.ToString() + "";
            connection = new SqlConnection(sqlConnectionString);
            connection.Open();
            if (IsFindRole)
            {
                if (UserRoleTypeId > 0)
                    command = new SqlCommand("Select * from UserRoleType Where UserRoleTypeId=" + UserRoleTypeId, connection);
                else
                    command = new SqlCommand("Select * from UserRoleType", connection);
            }
            else if (IsFindAppForm)
            {
                if (AppFormId > 0)
                    command = new SqlCommand("Select * from AppFormAction Where appFormId=" + AppFormId, connection);
                else
                    command = new SqlCommand("Select * from AppFormAction", connection);
            }
            adapter.SelectCommand = command;
            adapter.Fill(dt);
            connection.Close();
            return dt;
        }

        public string GetUserSuperRole(string userRole, SchoolUser user)
        {
            try
            {
                if (user != null)
                {
                    var Role = _IUserService.GetUserRoleById((int)user.UserRoleId);
                    if (Role != null)
                    {
                        DataTable dt = GetSuperDatables((int)Role.UserRoleTypeId, IsFindRole: true);
                        if (dt.Rows.Count > 0)
                        {
                            if (dt.Rows[0]["UserRoleType"].ToString().ToLower() != "")
                                userRole = dt.Rows[0]["UserRoleType"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return userRole;
        }

        #endregion

        #region FeeModule
        public int AutoGenerateReceiptNo(int? RecieptTypeId = null)
        {
            int ReceiptNo = 0;
            if (RecieptTypeId != null)
            {
                var receipttype = _IFeeService.GetReceiptTypeById((int)RecieptTypeId);
                if (receipttype != null)
                {
                    if (receipttype.ReceiptSeries == null || receipttype.ReceiptSeries == 0)
                    {
                        //Get feereceipt by receiptseries
                        var allrefreceipttypes = _IFeeService.GetAllReceiptTypes(ref count)
                                                  .Where(m => m.ReceiptSeriesRef == receipttype.ReceiptSeriesRef)
                                                  .Select(m => m.ReceiptTypeId).ToList();
                        allrefreceipttypes.Add((int)receipttype.ReceiptSeriesRef);

                        var lastFeereceipt = _IFeeService.GetAllFeeReceipts(ref count)
                                                .Where(m => allrefreceipttypes.Contains((int)m.ReceiptTypeId))
                                                .OrderByDescending(n => n.ReceiptNo).FirstOrDefault();
                        if (lastFeereceipt != null)
                        {
                            ReceiptNo = (int)lastFeereceipt.ReceiptNo + 1;
                        }
                        else
                        {
                            ReceiptNo = (int)_IFeeService.GetReceiptTypeById((int)receipttype.ReceiptSeriesRef).ReceiptSeries;
                        }
                    }
                    else
                    {
                        //get last feereceipt by ref 
                        var firstReceiptType = _IFeeService.GetAllReceiptTypes(ref count)
                                                .Where(m => m.ReceiptSeriesRef == receipttype.ReceiptTypeId)
                                                .Select(m => m.ReceiptTypeId).ToList();
                        firstReceiptType.Add(receipttype.ReceiptTypeId);

                        var lastfeereceipt = _IFeeService.GetAllFeeReceipts(ref count)
                                                .Where(m => firstReceiptType.Contains((int)m.ReceiptTypeId))
                                                .OrderByDescending(n => n.ReceiptNo).FirstOrDefault();
                        if (lastfeereceipt != null)
                        {
                            ReceiptNo = (int)lastfeereceipt.ReceiptNo + 1;
                        }
                        else
                        {
                            ReceiptNo = (int)receipttype.ReceiptSeries;
                        }
                    }
                }
                else
                {
                    var FeeReceiptSeries = _ISchoolSettingService.GetAttributeValue("FeeReceiptSeries");
                    ReceiptNo = Convert.ToInt32(FeeReceiptSeries);
                }
            }
            else
            {
                var FeeReceiptSeries = _ISchoolSettingService.GetAttributeValue("FeeReceiptSeries");
                ReceiptNo = Convert.ToInt32(FeeReceiptSeries);
            }
            return ReceiptNo;
        }

        public PrintFeeReceiptModel PrintFeeReceipt(string id)
        {
            var qrid = _ICustomEncryption.base64d(id);
            int feereceiptid = 0;
            feereceiptid = Convert.ToInt32(qrid);
            var feereceipt = _IFeeService.GetFeeReceiptById(feereceiptid);
            feereceipt.IsPrint = true;
            _IFeeService.UpdateFeeReceipt(feereceipt);
            var model = new PrintFeeReceiptModel();

            var UserTitleOnReceiptPrint = _ISchoolSettingService.GetorSetSchoolData("UserTitleOnReceiptPrint", "Generated By", null).AttributeValue;
            var schoolcopy = _ISchoolSettingService.GetorSetSchoolData("IsPrintSchoolCopy", "1", null).AttributeValue;
            var studentcopy = _ISchoolSettingService.GetorSetSchoolData("IsPrintStudentCopy", "1", null).AttributeValue;
            var showReceiptTypeinPrint = _ISchoolSettingService.GetorSetSchoolData("FeeReceipt_ShowFeeReceiptTypeinPrint", "1").AttributeValue;
            var showCopytext = _ISchoolSettingService.GetorSetSchoolData("FeeReceipt_showCopytext", "1").AttributeValue;
            var printmode = _ISchoolSettingService.GetorSetSchoolData("FeePrintModeLandScapeOrPortrait", "1", null).AttributeValue;
            var remarks = _ISchoolSettingService.GetorSetSchoolData("ShowRemarksInPrintFeeReceipt", "1", null).AttributeValue;
            var ConsolidatedFeeReceiptPrint = _ISchoolSettingService.GetorSetSchoolData("ConsolidatedFeeReceiptPrint", "0", null).AttributeValue;
            var GroupFeeheadinreceipt = _ISchoolSettingService.GetorSetSchoolData("GroupFeeHeadsInReceiptPrint", "1", null).AttributeValue;

            var FeereceiptHalfPageView = _ISchoolSettingService.GetorSetSchoolData("FeereceiptHalfPageView", "0", null).AttributeValue;
            var Feereceipt_ShowRegNo = _ISchoolSettingService.GetorSetSchoolData("Feereceipt_ShowRegNo", "0", null).AttributeValue;

            int isgroup = 1;
            if (GroupFeeheadinreceipt == "1")
                isgroup = 1;
            else
                isgroup = 0;
            model = _IFeeService.ViewOrPrintFeeReceiptBySp(feereceiptid, isgroup);
            model.regno_FeeReceiptPrint = Feereceipt_ShowRegNo == "1" ? true : false;
            model.FeereceiptHalfPageView = FeereceiptHalfPageView == "1" ? true : false;
            model.ConsolidatedFeeReceiptPrint = ConsolidatedFeeReceiptPrint == "1" ? true : false;
            var limiteddetail = _ISchoolSettingService.GetorSetSchoolData("ShowLimitedDetailInFeeReceipt", "1", null).AttributeValue;
            if (limiteddetail == "1")
                model.ShowlimitedDetail = true;
            else
                model.ShowlimitedDetail = false;
            var ShowBalance = _ISchoolSettingService.GetorSetSchoolData("ShowBalanceInFeeReceipt", "1", null).AttributeValue;
            if (ShowBalance == "1")
                model.ShowBalance = true;
            else
                model.ShowBalance = false;

            string receiptTemplate = model.ReceiptTemplate;
            if (remarks == "1")
                model.ShowRemarks = "1";
            else
                model.ShowRemarks = "0";
            model.SchoolLogo = (model.SchoolLogo != null && model.SchoolLogo != null) ? _WebHelper.GetStoreLocation() + "/Images/"
                                + HttpContext.Current.Session["SchoolId"] + "/" + model.SchoolLogo : "";

            string address = "";
            var stuadres = _IAddressService.GetAllAddresss(ref count, contactId: (int)feereceipt.StudentId, contacttypeId: 1).FirstOrDefault();
            if (stuadres != null)
                address = stuadres.Sector_Street + " " + stuadres.AddressCity.City;
            var ShowStudentAddressInLandScapeMode = _ISchoolSettingService.GetorSetSchoolData("ShowStudentAddressInLandScapeMode", "0", null).AttributeValue;
            var Fee_ReceiptHeadstobeshowminFeePrint = _ISchoolSettingService.GetorSetSchoolData("Fee_ReceiptHeadstobeshowminFeePrint", "srno,headname,payableamount,paidamount,balance", null).AttributeValue;

            if (ShowStudentAddressInLandScapeMode == "1")
                model.StudentAddress = address;
            else
                model.StudentAddress = "";


            if (!string.IsNullOrEmpty(model.ReceiptTemplate))
            {
                var doc = new HtmlDocument();
                doc.LoadHtml(model.ReceiptTemplate);
            
                
                var headlistcount = model.PrintFeeReceiptHeadsList.Count;
                if (headlistcount > 9)
                {
                    doc.GetElementbyId("main_div").RemoveClass("main_div_height");
                    doc.GetElementbyId("child_div").RemoveClass("child_div_height");
                    receiptTemplate = doc.DocumentNode.OuterHtml;
                    var counttobeadded = (headlistcount - 9)*5 + 105;
                    var counttobeaddedchild = (headlistcount - 9)*5 + 60;
                    receiptTemplate = receiptTemplate.Replace("@mainheight", "height:" + counttobeadded + "mm");
                    receiptTemplate = receiptTemplate.Replace("@Child_height", "height:" + counttobeaddedchild + "mm");
                }
               
                receiptTemplate = receiptTemplate.Replace("@DisplayPhone", model.DisplayPhone.ToString());
                receiptTemplate = receiptTemplate.Replace("@Schoollogo", model.SchoolLogo.ToString());

                receiptTemplate = receiptTemplate.Replace("@SchoolName", model.SchoolHeader.ToString());
                receiptTemplate = receiptTemplate.Replace("@AffiliationBoard", model.Affiliationboard.ToString());
                receiptTemplate = receiptTemplate.Replace("@AffiliationNo", model.AffiliationNo.ToString());
                receiptTemplate = receiptTemplate.Replace("@DisplayAddress", model.DisplayAddress.ToString());

                receiptTemplate = receiptTemplate.Replace("@ReceiptNo", model.ReceiptNo.ToString());
                receiptTemplate = receiptTemplate.Replace("@AdmissionNo", model.AdmnNo.ToString());
                receiptTemplate = receiptTemplate.Replace("@ReceiptDate", feereceipt.ReceiptDate.Value.ToString("dd/MM/yyyy"));
                receiptTemplate = receiptTemplate.Replace("@Class", model.ClassName.ToString());
                receiptTemplate = receiptTemplate.Replace("@StudentName", model.StudentName);
                receiptTemplate = receiptTemplate.Replace("@RollNo", model.RollNo);
                receiptTemplate = receiptTemplate.Replace("@FatherName", model.FatherName);
                receiptTemplate = receiptTemplate.Replace("@RegNo", model.RegistrationNo);
                receiptTemplate = receiptTemplate.Replace("@Category", model.Category);
                receiptTemplate = receiptTemplate.Replace("@Adress", address);
                receiptTemplate = receiptTemplate.Replace("@TotalAmount", model.TotalPayable);
                receiptTemplate = receiptTemplate.Replace("@ReceivedAmt", model.TotalPaidAmount.ToString());
                receiptTemplate = receiptTemplate.Replace("@BalancedAmt", model.TotalBalance.ToString());
                receiptTemplate = receiptTemplate.Replace("@FeePeriod", model.FeePeriod.ToString());
                receiptTemplate = receiptTemplate.Replace("@Remarks", model.Remarks.ToString());
                var bankname = model.BankBranch != null ? model.BankBranch : "";
                var chequeno = model.ChequeNo != null && model.ChequeNo != "" && model.PaymentMode.Contains("Cheque") ? " (" + model.ChequeNo + ") " + "(" + bankname + ") " : "";
                receiptTemplate = receiptTemplate.Replace("@PaymentMode", model.PaymentMode.ToString() + chequeno);
                string html = "";
                string newHTML = "";
                if (model.PrintFeeReceiptHeadsList.Count > 0)
                {
                    for (int i = 0; i < model.PrintFeeReceiptHeadsList.Count; i++)
                    {
                        html += "<tr>";
                        html += "  <td>" + (i + 1).ToString() + "</td>";
                        html += "<td >" + model.PrintFeeReceiptHeadsList[i].HeadName + "</td>";
                        html += " <td style='text-align: right;'>" + model.PrintFeeReceiptHeadsList[i].PaidAmount.ToString() + "</td>";
                        html += "</tr>";

                        newHTML += "<tr>";
                        if(Fee_ReceiptHeadstobeshowminFeePrint.Contains("srno"))
                        newHTML += "  <td>" + (i + 1).ToString() + "</td>";
                        if (Fee_ReceiptHeadstobeshowminFeePrint.Contains("headname"))
                            newHTML += "<td >" + model.PrintFeeReceiptHeadsList[i].HeadName + "</td>";
                        if (Fee_ReceiptHeadstobeshowminFeePrint.Contains("payableamount"))
                            newHTML += " <td style='text-align: right;'>" + model.PrintFeeReceiptHeadsList[i].PayableAmount.ToString() + "</td>";
                        if (Fee_ReceiptHeadstobeshowminFeePrint.Contains("paidamount"))
                            newHTML += " <td style='text-align: right;'>" + model.PrintFeeReceiptHeadsList[i].PaidAmount.ToString() + "</td>";
                        if (Fee_ReceiptHeadstobeshowminFeePrint.Contains("balance"))
                            newHTML += " <td style='text-align: right;'>" + model.PrintFeeReceiptHeadsList[i].Balance.ToString() + "</td>";
                        newHTML += "</tr>";
                    }

                }
                receiptTemplate = receiptTemplate.Replace("@TotalRowsNew", newHTML.ToString());
                receiptTemplate = receiptTemplate.Replace("@TotalRows", html.ToString());
                
                receiptTemplate = receiptTemplate.Replace("@TotalDeduction", model.TotalDiscount.ToString());
                if (model.regno_FeeReceiptPrint)
                    receiptTemplate = receiptTemplate.Replace("@RegNo", model.RegistrationNo);
                else
                {
                    receiptTemplate = receiptTemplate.Replace("@RegNo", "");
                }
                model.ReceiptTemplate = receiptTemplate;
            }
            model.TotalPaidAmountInWords = ConvertToWords(model.TotalPaidAmount);

            model.UserName = "";


            if (!string.IsNullOrEmpty(UserTitleOnReceiptPrint))
            {
                var getSchoolUser = _IUserService.GetUserById((int)feereceipt.UserId);
                if (getSchoolUser != null)
                {
                    string UserRole = "", StaffName = "";
                    UserRole = GetUserSuperRole(UserRole, getSchoolUser);
                    if (UserRole.ToLower() == "admin" )
                    {
                      if(getSchoolUser.UserContactId==null)
                        {
                            model.UserName =  (UserTitleOnReceiptPrint + " : " + getSchoolUser.UserName) ;
                        }
                        else
                        {
                            var getStaffInfo = _IStaffService.GetStaffById((int)getSchoolUser.UserContactId);
                            StaffName = getStaffInfo != null ? TrimStaffName(getStaffInfo, StaffName) : "";
                            model.UserName = getStaffInfo != null ? (UserTitleOnReceiptPrint + " : " + StaffName) : "";
                        }
                    }
                    else
                    {
                        var getStaffInfo = _IStaffService.GetStaffById((int)getSchoolUser.UserContactId);
                        StaffName = getStaffInfo != null ? TrimStaffName(getStaffInfo, StaffName) : "";
                        model.UserName = getStaffInfo != null ? (UserTitleOnReceiptPrint + " : " + StaffName) : "";
                    }
                }
            }
            return model;
        }
        #endregion

        #region Connection
        public string GetSetCurrentSchoolDataBase(HttpRequestBase Request, HttpSessionStateBase Session, HttpServerUtilityBase Server)
        {
            try
            {
                // get subdomain
                string User = "";
                string Subdomain = Request.Url.AbsolutePath;
                Subdomain = Subdomain.Split('/')[1];
               // Subdomain = "bbisraikot";
                // API url
                string MainUrl = "";
                string suburl = Convert.ToString(WebConfigurationManager.AppSettings["SubUrl"]);
                string synccompany = Convert.ToString(WebConfigurationManager.AppSettings["syncapiname"]);
                string syncsub = Convert.ToString(WebConfigurationManager.AppSettings["syncsub"]);
                string syncext = Convert.ToString(WebConfigurationManager.AppSettings["syncext"]);
                string APIName = Convert.ToString(WebConfigurationManager.AppSettings["APIName"]);

                if (Request.Url.AbsoluteUri.Contains("localhost"))
                {
                    User = Convert.ToString(WebConfigurationManager.AppSettings["User"]);
                    // MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                    //  // User = "stroop";
                    //  // User="atsderabassi";
                    //   User = "Dev-KS_School";
                    //  //User = "3is_demo";
                    //  User = "demo";
                    //  // User = "kschild";
                    //  //User = "bpsambala";
                    //  //User = "hgdemo";
                    //  //User = "dtsk8";
                    //  //User = "";
                    // MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;

                    MainUrl = "http://app.3ischools.com/api" + "/" + suburl;
                   // MainUrl = "http://mobileapi.digitech.co.in" + "/" + suburl;


                }
                else if (Request.Url.AbsoluteUri.Contains("digitech.com") || Request.Url.AbsoluteUri.Contains("172.16.1.2"))
                {
                    User = Subdomain;
                    //MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
                    MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                }
                else if (Request.Url.AbsoluteUri.Contains("digitech.co.in"))
                {
                    User = Subdomain;
                    // MainUrl = "http://api.digitech.co.in" + "/" + suburl;
                    MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;

                }
                else
                {
                    User = Subdomain;
                    //  MainUrl = "http://3ischools.com/api" + "/" + suburl;
                    MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;

                }

                HttpClient client = new HttpClient();
                client.Timeout = new TimeSpan(0, 10, 1);
                MainUrl = MainUrl + "?DBName=" + User; // url with parameter
                client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync(MainUrl).Result;
                var content = response.Content.ReadAsStringAsync().Result;

                var IsDatabase = JsonConvert.DeserializeObject<APIDatabaseModel>(content);
                if (IsDatabase != null && !string.IsNullOrEmpty(IsDatabase.DBName))
                {
                    // change connection string 
                    var cn = _IConnectionService.Connectionstring(IsDatabase.DBName);
                    // check directory exist or not
                    Session.Timeout = 520000;
                    Session["SchoolDB"] = IsDatabase.DBName;
                    Session["SchoolId"] = IsDatabase.DBId.ToString();

                    var directory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId));
                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    var teacherdirectory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId + "/Teacher/Photos"));
                    if (!Directory.Exists(teacherdirectory))
                        Directory.CreateDirectory(teacherdirectory);
                }
                else
                    Connectivity.Error.ToString();

                // current user
                //string user = System.Web.HttpContext.Current.User.Identity.Name;
                //if (string.IsNullOrEmpty(user))
                //    return Connectivity.Done.ToString();
                //else
                return Connectivity.Done.ToString();
            }
            catch (Exception ex)
            {
                // _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Connectivity.Error.ToString();
            }
        }

        public string GetSetCurrentSchoolDataBaseApp(HttpRequestBase Request, HttpSessionStateBase Session, HttpServerUtilityBase Server, out string Sessionname, out string SchoolId)
        {
            Sessionname = "";
            SchoolId = "";
            try
            {
               
                // get subdomain
                string User = "";
                string Subdomain = Request.Url.AbsolutePath;
                Subdomain = Subdomain.Split('/')[1];
                // Subdomain = "bbisraikot";
                // API url
                //string MainUrl = "";
                //string suburl = Convert.ToString(WebConfigurationManager.AppSettings["SubUrl"]);
                //string synccompany = Convert.ToString(WebConfigurationManager.AppSettings["syncapiname"]);
                //string syncsub = Convert.ToString(WebConfigurationManager.AppSettings["syncsub"]);
                //string syncext = Convert.ToString(WebConfigurationManager.AppSettings["syncext"]);
                //string APIName = Convert.ToString(WebConfigurationManager.AppSettings["APIName"]);

                if (Request.Url.AbsoluteUri.Contains("localhost"))
                {
                    User = Convert.ToString(WebConfigurationManager.AppSettings["User"]);
                    // MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                    //  // User = "stroop";
                    //  // User="atsderabassi";
                    //   User = "Dev-KS_School";
                    //  //User = "3is_demo";
                    //  User = "demo";
                    //  // User = "kschild";
                    //  //User = "bpsambala";
                    //  //User = "hgdemo";
                    //  //User = "dtsk8";
                    //  //User = "";
                    // MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;

              //      MainUrl = "http://app.3ischools.com/api" + "/" + suburl;
                    // MainUrl = "http://mobileapi.digitech.co.in" + "/" + suburl;


                }
                else if (Request.Url.AbsoluteUri.Contains("digitech.com") || Request.Url.AbsoluteUri.Contains("172.16.1.2"))
                {
                    User = Subdomain;
                    //MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
             //       MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                }
                else if (Request.Url.AbsoluteUri.Contains("digitech.co.in"))
                {
                    User = Subdomain;
                    // MainUrl = "http://api.digitech.co.in" + "/" + suburl;
             //       MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;

                }
                else
                {
                    User = Subdomain;
                    //  MainUrl = "http://3ischools.com/api" + "/" + suburl;
            //        MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;

                }
                #region get database details from json

                var dynamicnavigationpath = "";

                dynamicnavigationpath = "navigationpath";
                string file = "";
                file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(WebConfigurationManager.AppSettings["Credential"]));
                string Json = System.IO.File.ReadAllText(file);
                System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();

                dynamic stuff1 = Newtonsoft.Json.JsonConvert.DeserializeObject(Json);
                JObject o = stuff1;

                var nameOfProperty = _ICustomEncryption.base64e(User);

                IList<string> keys = o.Properties().Select(p => p.Name).ToList();
                JToken entireJson = JToken.Parse(Json);
                JArray inner = entireJson[nameOfProperty].Value<JArray>();
                var db_id = Convert.ToInt32(inner.First);
                var db_name = Convert.ToInt32(inner.Last);

                var dbid = (db_id - 1987) / 6;
                var dbname = (db_name - 1947) / 3;
                #endregion
                //HttpClient client = new HttpClient();
                //client.Timeout = new TimeSpan(0, 10, 1);
                //MainUrl = MainUrl + "?DBName=" + User; // url with parameter
                //client.DefaultRequestHeaders.Accept.Add(
                //new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //var response = client.GetAsync(MainUrl).Result;
                //var content = response.Content.ReadAsStringAsync().Result;

                //  var IsDatabase = JsonConvert.DeserializeObject<APIDatabaseModel>(content);
                var IsDatabase = new APIDatabaseModel();
                IsDatabase.DBId = dbid;
                if (Request.Url.AbsoluteUri.Contains("localhost") || Request.Url.AbsoluteUri.Contains("172.16.1.2"))
                {
                    IsDatabase.DBName = "ks_" + dbname.ToString().PadLeft(5, '0');
                }
                else if (Request.Url.AbsoluteUri.Contains("digitech.co.in"))
                {
                    IsDatabase.DBName = "tst_" + dbname.ToString().PadLeft(5, '0');
                }
                else
                {
                    IsDatabase.DBName = "3is_" + dbname.ToString().PadLeft(5, '0');
                }
                if (IsDatabase != null && !string.IsNullOrEmpty(IsDatabase.DBName))
                {
                    // change connection string 
                   // var cn = _IConnectionService.Connectionstring(IsDatabase.DBName);
                    // check directory exist or not
                    Session.Timeout = 520000;
                    Session["SchoolDB"] = IsDatabase.DBName;
                    Sessionname = IsDatabase.DBName;
                    Session["SchoolId"] = IsDatabase.DBId.ToString();
                    SchoolId= IsDatabase.DBId.ToString();
                    var directory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId));
                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    var teacherdirectory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId + "/Teacher/Photos"));
                    if (!Directory.Exists(teacherdirectory))
                        Directory.CreateDirectory(teacherdirectory);
                }
                else
                    Connectivity.Error.ToString();

                // current user
                //string user = System.Web.HttpContext.Current.User.Identity.Name;
                //if (string.IsNullOrEmpty(user))
                //    return Connectivity.Done.ToString();
                //else
                return Connectivity.Done.ToString();
            }
            catch (Exception ex)
            {
                // _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Connectivity.Error.ToString();
            }
        }

        #endregion

        #region convert Functions
        public String ones(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = "";
            switch (_Number)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }

        public String tens(String Number)
        {
            int _Number = Convert.ToInt32(Number);
            String name = null;
            switch (_Number)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (_Number > 0)
                    {
                        name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                    }
                    break;
            }
            return name;
        }

        //get the range between two dates
        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        public String ConvertDecimals(String number)
        {
            String cd = "", digit = "", engOne = "";
            for (int i = 0; i < number.Length; i++)
            {
                digit = number[i].ToString();
                if (digit.Equals("0"))
                {
                    engOne = "Zero";
                }
                else
                {
                    engOne = ones(digit);
                }
                cd += " " + engOne;
            }
            return cd;
        }

        public String ConvertToWords(String numb)
        {
            String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
            String endStr = "Only";
            try
            {
                int decimalPlace = numb.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNo = numb.Substring(0, decimalPlace);
                    points = numb.Substring(decimalPlace + 1);
                    if (Convert.ToInt32(points) > 0)
                    {
                        andStr = "and";// just to separate whole numbers from points/cents  
                        endStr = "Paisa " + endStr;//Cents  
                        pointStr = ConvertDecimals(points);
                    }
                }
                val = String.Format("{0} {1}{2} {3}", ConvertWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
            }
            catch { }
            return val;
        }

        public String ConvertWholeNumber(String Number)
        {
            string word = "";
            try
            {
                bool beginsZero = false;//tests for 0XX  
                bool isDone = false;//test if already translated  
                double dblAmt = (Convert.ToDouble(Number));
                //if ((dblAmt > 0) && number.StartsWith("0"))  
                if (dblAmt > 0)
                {//test for zero or digit zero in a nuemric  
                    beginsZero = Number.StartsWith("0");

                    int numDigits = Number.Length;
                    int pos = 0;//store digit grouping  
                    String place = "";//digit grouping name:hundres,thousand,etc...  
                    switch (numDigits)
                    {
                        case 1://ones' range  

                            word = ones(Number);
                            isDone = true;
                            break;
                        case 2://tens' range  
                            word = tens(Number);
                            isDone = true;
                            break;
                        case 3://hundreds' range  
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range  
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range  
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        case 10://Billions's range  
                        case 11:
                        case 12:

                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                        //add extra case options for anything above Billion...  
                        default:
                            isDone = true;
                            break;
                    }
                    if (!isDone)
                    {//if transalation is not done, continue...(Recursion comes in now!!)  
                        if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                        {
                            try
                            {
                                word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                            }
                            catch { }
                        }
                        else
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                        }

                        //check for trailing zeros  
                        //if (beginsZero) word = " and " + word.Trim();  
                    }
                    //ignore digit grouping names  
                    if (word.Trim().Equals(place.Trim())) word = "";
                }
            }
            catch { }
            return word.Trim();
        }

        #endregion

        #region ExcelObjects
        public ExcelImportObject GetExcelImportObjectById(int ExcelImportObjectId, bool IsTrack = false)
        {
            if (ExcelImportObjectId == 0)
                throw new ArgumentNullException("ExcelImportObject");

            var ExcelImportObject = new ExcelImportObject();
            if (IsTrack)
                ExcelImportObject = (from s in db.ExcelImportObjects where s.ExcelImportObjectId == ExcelImportObjectId select s).FirstOrDefault();
            else
                ExcelImportObject = (from s in db.ExcelImportObjects.AsNoTracking() where s.ExcelImportObjectId == ExcelImportObjectId select s).FirstOrDefault();

            return ExcelImportObject;
        }

        public List<ExcelImportObject> GetAllExcelImportObjects(ref int totalcount, string ExcelImportObject = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExcelImportObjects.ToList();
            if (!String.IsNullOrEmpty(ExcelImportObject))
                query = query.Where(e => e.ExcelImportObject1.ToLower().Contains(ExcelImportObject.ToLower())).ToList();

            totalcount = query.Count;
            var ExcelImportObjects = new PagedList<KSModel.Models.ExcelImportObject>(query, PageIndex, PageSize);
            return ExcelImportObjects;
        }

        public void InsertExcelImportObject(ExcelImportObject ExcelImportObject)
        {
            if (ExcelImportObject == null)
                throw new ArgumentNullException("ExcelImportObject");

            db.ExcelImportObjects.Add(ExcelImportObject);
            db.SaveChanges();
        }

        public void UpdateExcelImportObject(ExcelImportObject ExcelImportObject)
        {
            if (ExcelImportObject == null)
                throw new ArgumentNullException("ExcelImportObject");

            db.Entry(ExcelImportObject).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExcelImportObject(int ExcelImportObjectId = 0)
        {
            if (ExcelImportObjectId == 0)
                throw new ArgumentNullException("ExcelImportObject");

            var ExcelImportObject = (from s in db.ExcelImportObjects where s.ExcelImportObjectId == ExcelImportObjectId select s).FirstOrDefault();
            db.ExcelImportObjects.Remove(ExcelImportObject);
            db.SaveChanges();
        }
        #endregion

        #region ExcelImportObjectCols
        public ExcelImportObjectCol GetExcelImportObjectColById(int ExcelImportObjectColId, bool IsTrack = false)
        {
            if (ExcelImportObjectColId == 0)
                throw new ArgumentNullException("ExcelImportObjectCol");

            var ExcelImportObjectCol = new ExcelImportObjectCol();
            if (IsTrack)
                ExcelImportObjectCol = (from s in db.ExcelImportObjectCols where s.ExcelImportObjectColsId == ExcelImportObjectColId select s).FirstOrDefault();
            else
                ExcelImportObjectCol = (from s in db.ExcelImportObjectCols.AsNoTracking() where s.ExcelImportObjectColsId == ExcelImportObjectColId select s).FirstOrDefault();

            return ExcelImportObjectCol;
        }

        public List<ExcelImportObjectCol> GetAllExcelImportObjectCols(ref int totalcount, string DBColName = "",
            string ExcelColName = "", int? ExcelImportObjectId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExcelImportObjectCols.ToList();
            if (!String.IsNullOrEmpty(DBColName))
                query = query.Where(e => e.DBColName.ToLower().Contains(DBColName.ToLower())).ToList();
            if (!String.IsNullOrEmpty(ExcelColName))
                query = query.Where(e => e.ExcelColName.ToLower().Contains(ExcelColName.ToLower())).ToList();
            if (ExcelImportObjectId != null)
                query = query.Where(e => e.ExcelImportObjectId == ExcelImportObjectId).ToList();

            totalcount = query.Count;
            var ExcelImportObjectCol = new PagedList<KSModel.Models.ExcelImportObjectCol>(query, PageIndex, PageSize);
            return ExcelImportObjectCol;
        }

        public void InsertExcelImportObjectCol(ExcelImportObjectCol ExcelImportObjectCol)
        {
            if (ExcelImportObjectCol == null)
                throw new ArgumentNullException("ExcelImportObjectCol");

            db.ExcelImportObjectCols.Add(ExcelImportObjectCol);
            db.SaveChanges();
        }

        public void UpdateExcelImportObjectCol(ExcelImportObjectCol ExcelImportObjectCol)
        {
            if (ExcelImportObjectCol == null)
                throw new ArgumentNullException("ExcelImportObjectCol");

            db.Entry(ExcelImportObjectCol).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteExcelImportObjectCol(int ExcelImportObjectColId = 0)
        {
            if (ExcelImportObjectColId == 0)
                throw new ArgumentNullException("ExcelImportObjectCol");

            var ExcelImportObjectCol = (from s in db.ExcelImportObjectCols where s.ExcelImportObjectColsId == ExcelImportObjectColId select s).FirstOrDefault();
            db.ExcelImportObjectCols.Remove(ExcelImportObjectCol);
            db.SaveChanges();
        }
        #endregion

        #region SurveillanceAlarm
        public List<SurveillanceAlarm> GetAllSurveillanceAlarms(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SurveillanceAlarms.ToList();

            totalcount = query.Count();
            var SurveillanceAlarms = new PagedList<KSModel.Models.SurveillanceAlarm>(query, PageIndex, PageSize);
            return SurveillanceAlarms;
        }
        public void UpdateSurveillanceAlarmsActivatedDate(SurveillanceAlarm SurveillanceAlarm)
        {
            if (SurveillanceAlarm == null)
                throw new ArgumentNullException("SurveillanceAlarm");

            db.Entry(SurveillanceAlarm).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        #endregion

        #region SurveillancePunch
        public SurviellancePunch GetSurveillancePunchById(int SurveillancePunchId, bool IsTrack = false)
        {
            if (SurveillancePunchId == 0)
                throw new ArgumentNullException("SurveillancePunch");

            var SurveillancePunch = new SurviellancePunch();
            if (IsTrack)
                SurveillancePunch = (from s in db.SurviellancePunches where s.SurviellancePunchId == SurveillancePunchId select s).FirstOrDefault();
            else
                SurveillancePunch = (from s in db.SurviellancePunches.AsNoTracking() where s.SurviellancePunchId == SurveillancePunchId select s).FirstOrDefault();

            return SurveillancePunch;
        }

        public List<SurviellancePunch> GetAllSurveillancePunch(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SurviellancePunches.ToList();


            totalcount = query.Count;
            var SurveillancePunch = new PagedList<KSModel.Models.SurviellancePunch>(query, PageIndex, PageSize);
            return SurveillancePunch;
        }

        public void InsertSurveillancePunch(SurviellancePunch SurveillancePunch)
        {
            if (SurveillancePunch == null)
                throw new ArgumentNullException("SurveillancePunch");

            db.SurviellancePunches.Add(SurveillancePunch);
            db.SaveChanges();
        }

        public void UpdateSurveillancePunch(SurviellancePunch SurveillancePunch)
        {
            if (SurveillancePunch == null)
                throw new ArgumentNullException("SurveillancePunch");

            db.Entry(SurveillancePunch).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteSurveillancePunch(int SurveillancePunchId = 0)
        {
            if (SurveillancePunchId == 0)
                throw new ArgumentNullException("SurveillancePunch");

            var SurveillancePunch = (from s in db.SurviellancePunches where s.SurviellancePunchId == SurveillancePunchId select s).FirstOrDefault();
            db.SurviellancePunches.Remove(SurveillancePunch);
            db.SaveChanges();
        }
        #endregion

        #region User's ProfileImage
        public string StudentProfileImage(string SchoolDbId, Student student)
        {
            string defaultimg = "";
            if (student.GenderId != null)
            {
                string UrlImage = "";
                var getGender = _ICatalogMasterService.GetGenderById((int)student.GenderId);
                if (getGender != null)
                {
                    switch (getGender.Gender1.ToLower())
                    {
                        case "male":
                            UrlImage = "/Images/male-avtar.png";
                            break;
                        case "female":
                            UrlImage = "/Images/Female-avtar.png";
                            break;
                        case "transgender":
                            UrlImage = "/Images/male-avtar.png";
                            break;
                    }
                }
                defaultimg = _WebHelper.GetStoreLocation() + UrlImage;
            }
            var doctypeid = _IDocumentService.GetAllDocumentTypes(ref count)
                                            .Where(d => d.DocumentType1 == "Image").FirstOrDefault();
            var image = _IDocumentService.GetAllAttachedDocuments(ref count)
                                            .Where(d => d.DocumentTypeId == doctypeid.DocumentTypeId
                                            && d.StudentId == (int)student.StudentId).FirstOrDefault();

            string UserImage = defaultimg;
            if (image != null)
            {
                if (!string.IsNullOrEmpty(image.Image))
                    UserImage = _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/StudentPhotos/" + image.Image;
            }

            return UserImage;
        }

        public string StaffImage(string SchoolDbId, Staff staff)
        {
            string defaultimg = "";
            if (staff.GenderId != null)
            {
                string UrlImage = "";
                var getGender = db.Genders.Where(x => x.GenderId == staff.GenderId).FirstOrDefault();
                if (getGender != null)
                {
                    switch (getGender.Gender1.ToLower())
                    {
                        case "male":
                            UrlImage = "/Images/male-avtar.png";
                            break;
                        case "female":
                            UrlImage = "/Images/Female-avtar.png";
                            break;
                        case "transgender":
                            UrlImage = "/Images/male-avtar.png";
                            break;
                    }
                }
                defaultimg = _WebHelper.GetStoreLocation() + UrlImage;
            }
            var staffdoctypeid = db.StaffDocumentTypes.Where(d => d.DocumentType == "Image").FirstOrDefault();
            var teacherimage = staff.StaffDocuments.Where(d => d.DocumentTypeId == staffdoctypeid.DocumentTypeId).FirstOrDefault();
            if (teacherimage != null)
            {
                if (!string.IsNullOrEmpty(teacherimage.Image))
                    defaultimg = _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/Teacher/Photos/" + teacherimage.Image;

            }
            return defaultimg;
        }

        public string GuardianProfileImage(string SchoolDbId, Guardian guardian, int? StudentId = null)
        {
            string defaultimg = "";
            if (guardian.GuardianTypeId != null)
            {
                string UrlImage = "";
                var getGender = db.Relations.Where(x => x.RelationId == guardian.GuardianTypeId).FirstOrDefault();
                if (getGender != null)
                {
                    switch (getGender.Relation1.ToLower())
                    {
                        case "father":
                            UrlImage = "/Images/male-avtar.png";
                            break;
                        case "mother":
                            UrlImage = "/Images/Female-avtar.png";
                            break;
                        case "sister":
                            UrlImage = "/Images/Female-avtar.png";
                            break;
                        case "brother":
                            UrlImage = "/Images/male-avtar.png";
                            break;
                        case "guardian":
                            UrlImage = "/Images/default.png";
                            break;
                    }
                }
                defaultimg = _WebHelper.GetStoreLocation() + UrlImage;
            }

            var image = db.AttachedDocuments.Where(d => d.DocumentType.DocumentType1 == "Image"
                                                && d.StudentId == StudentId).FirstOrDefault();
            if (image != null)
            {
                if (!string.IsNullOrEmpty(image.Image))
                    defaultimg = _WebHelper.GetStoreLocation() + "/Images/" + SchoolDbId + "/GuardianPhotos/" + image.Image;
            }
            return defaultimg;
        }
        #endregion

        #region ColorCodes
        public IList<ColorCode> GetAllColorCodes(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ColorCodes.ToList();


            totalcount = query.Count;
            var ColorCodes = new PagedList<KSModel.Models.ColorCode>(query, PageIndex, PageSize);
            return ColorCodes;
        }
        #endregion
    }
}