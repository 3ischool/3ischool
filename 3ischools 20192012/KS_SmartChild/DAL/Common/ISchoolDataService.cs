﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSModel.Models;
using System.Web.Helpers;
using System.Web;
using System.Drawing;
using System.Drawing.Drawing2D;
namespace KS_SmartChild.DAL.Common
{
    public partial interface ISchoolDataService
    {
        List<SchoolData> GetSchoolDataDetail(ref int totalcount,
          int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertSchoolData(SchoolData SchoolData);

        void UpdateSchoolData(SchoolData SchoolData);

        void DeleteSchoolData(int SchoolDataId = 0);

        WebImage ResizeImage(HttpPostedFileBase file, string ImageType = "", int height = 0, int width = 0,string ImageExtension = "");

        //Image ResizeImage(Image image, Size size, bool preserveAspectRatio = true);

    }
}
    