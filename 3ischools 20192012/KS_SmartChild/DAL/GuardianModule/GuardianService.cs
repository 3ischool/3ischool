﻿using KS_SmartChild.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace KS_SmartChild.DAL.GuardianModule
{
    public partial class GuardianService : IGuardianService
    {
        // private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public GuardianService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods

        #region RelationType

        public RelationType GetRelationTypeById(int RelationTypeId)
        {
            if (RelationTypeId == 0)
                throw new ArgumentNullException("RelationType");

            var RelationType = (from s in db.RelationTypes.AsNoTracking() where s.RelationTypeId == RelationTypeId select s).FirstOrDefault();
            return RelationType;
        }

        public List<RelationType> GetAllRelationTypes(ref int totalcount, string RelationType = "", int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.RelationTypes.ToList();
            if (!String.IsNullOrEmpty(RelationType))
                query = query.Where(e => e.RelationType1.ToLower().Contains(RelationType.ToLower())).ToList();

            totalcount = query.Count;
            var RelationTypes = new PagedList<KSModel.Models.RelationType>(query, PageIndex, PageSize);
            return RelationTypes;
        }

        public void InsertRelationType(RelationType RelationType)
        {
            if (RelationType == null)
                throw new ArgumentNullException("RelationType");

            db.RelationTypes.Add(RelationType);
            db.SaveChanges();
        }

        public void UpdateRelationType(RelationType RelationType)
        {
            if (RelationType == null)
                throw new ArgumentNullException("RelationType");

            db.Entry(RelationType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteRelationType(int RelationTypeId = 0)
        {
            if (RelationTypeId == 0)
                throw new ArgumentNullException("RelationType");

            var RelationType = (from s in db.RelationTypes where s.RelationTypeId == RelationTypeId select s).FirstOrDefault();
            db.RelationTypes.Remove(RelationType);
            db.SaveChanges();
        }

        #endregion

        #region Relation

        public Relation GetRelationById(int RelationId)
        {
            if (RelationId == 0)
                throw new ArgumentNullException("RelationType");

            var Relation = (from s in db.Relations.AsNoTracking() where s.RelationId == RelationId select s).FirstOrDefault();
            return Relation;
        }

        public List<Relation> GetAllRelations(ref int totalcount, string Relation = "", int? RelationtypeId = null, int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.Relations.AsQueryable();
            if (!String.IsNullOrEmpty(Relation))
                query = query.Where(e => e.Relation1.ToLower().Contains(Relation.ToLower()));
            if (RelationtypeId != null && RelationtypeId > 0)
                query = query.Where(r => r.RelationTypeId == RelationtypeId);

            totalcount = query.Count();
            var Relations = new PagedList<KSModel.Models.Relation>(query.ToList(), PageIndex, PageSize);
            return Relations;
        }

        public void InsertRelation(Relation Relation)
        {
            if (Relation == null)
                throw new ArgumentNullException("Relation");

            db.Relations.Add(Relation);
            db.SaveChanges();
        }

        public void UpdateRelation(Relation Relation)
        {
            if (Relation == null)
                throw new ArgumentNullException("Relation");

            db.Entry(Relation).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteRelation(int RelationId = 0)
        {
            if (RelationId == 0)
                throw new ArgumentNullException("Relation");

            var Relation = (from s in db.Relations where s.RelationId == RelationId select s).FirstOrDefault();
            db.Relations.Remove(Relation);
            db.SaveChanges();
        }

        #endregion

        #region Guardian

        public Guardian GetGuardianById(int GuardianId,bool IsTrack = false)
        {
            if (GuardianId == 0)
                throw new ArgumentNullException("Guardian");

            var Guardian = new Guardian();
            if(IsTrack)
                Guardian = (from s in db.Guardians.AsNoTracking() where s.GuardianId == GuardianId select s).FirstOrDefault();
            else
                Guardian = (from s in db.Guardians.AsNoTracking() where s.GuardianId == GuardianId select s).FirstOrDefault();

            return Guardian;
        }

        public IList<Guardian> GetAllGuardians(ref int totalcount, string FirstName = "", string LastName = "", string FullName = "",
            int? QualificationId = null, int? OccupationId = null, int? GuardianTypeId = null, int? StudentId = null,
            string Income = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Guardians.AsQueryable();
            if (!String.IsNullOrEmpty(FirstName))
                query = query.Where(e => e.FirstName.ToLower().Contains(FirstName.ToLower()));
            if (!String.IsNullOrEmpty(LastName))
                query = query.Where(e => e.LastName.ToLower().Contains(LastName.ToLower()));
            if (!String.IsNullOrEmpty(FullName))
                query = query.Where(e => (e.FirstName + e.LastName).ToLower().Contains(FullName.ToLower()));
            if (!String.IsNullOrEmpty(Income))
                query = query.Where(e => e.IncomeSlabId == Income);
            if (QualificationId != null && QualificationId > 0)
                query = query.Where(e => e.QualificationId == QualificationId);
            if (OccupationId != null && OccupationId > 0)
                query = query.Where(e => e.OccupationId == OccupationId);
            if (GuardianTypeId != null && GuardianTypeId > 0)
                query = query.Where(e => e.GuardianTypeId == GuardianTypeId);
            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId);

            totalcount = query.Count();
            var Guardians = new PagedList<KSModel.Models.Guardian>(query.ToList(), PageIndex, PageSize);
            return Guardians;
        }

        public void InsertGuardian(Guardian Guardian)
        {
            if (Guardian == null)
                throw new ArgumentNullException("Guardian");

            db.Guardians.Add(Guardian);
            db.SaveChanges();
        }

        public void UpdateGuardian(Guardian Guardian)
        {
            if (Guardian == null)
                throw new ArgumentNullException("Guardian");

            db.Entry(Guardian).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteGuardian(int GuardianId = 0)
        {
            if (GuardianId == 0)
                throw new ArgumentNullException("Guardian");

            var Guardian = (from s in db.Guardians where s.GuardianId == GuardianId select s).FirstOrDefault();
            db.Guardians.Remove(Guardian);
            db.SaveChanges();
        }

        #endregion

        #region Income Slabs

        public IncomeSlab GetIncomeSlabById(int SlabId, bool IsTrack = false)
        {
            if (SlabId == 0)
                throw new ArgumentNullException("SlabId");

            var IncomeSlab = new IncomeSlab();
            if (IsTrack)
                IncomeSlab = (from s in db.IncomeSlabs.AsNoTracking() where s.SlabId == SlabId select s).FirstOrDefault();
            else
                IncomeSlab = (from s in db.IncomeSlabs.AsNoTracking() where s.SlabId == SlabId select s).FirstOrDefault();

            return IncomeSlab;
        }
        public List<IncomeSlab> GetAllIncomeSlabs(ref int totalcount, string SlabName = "", decimal? IncomeFrom = null, decimal? IncomeTo = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.IncomeSlabs.ToList();
            if (!String.IsNullOrEmpty(SlabName))
                query = query.Where(e => e.SlabName.ToLower().Contains(SlabName.ToLower())).ToList();
            if (IncomeFrom != null && IncomeFrom > 0)
                query = query.Where(e => e.IncomeFrom == IncomeFrom).ToList();
            if (IncomeTo != null && IncomeTo > 0)
                query = query.Where(e => e.IncomeTo == IncomeTo).ToList();

            totalcount = query.Count;
            var IncomeSlab = new PagedList<KSModel.Models.IncomeSlab>(query, PageIndex, PageSize);
            return IncomeSlab;
        }

        public void InsertIncomeSlab(IncomeSlab IncomeSlab)
        {
            if (IncomeSlab == null)
                throw new ArgumentNullException("IncomeSlab");

            db.IncomeSlabs.Add(IncomeSlab);
            db.SaveChanges();
        }

        public void UpdateIncomeSlab(IncomeSlab IncomeSlab)
        {
            if (IncomeSlab == null)
                throw new ArgumentNullException("IncomeSlab");

            db.Entry(IncomeSlab).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteIncomeSlab(int SlabId = 0)
        {
            if (SlabId == 0)
                throw new ArgumentNullException("IncomeSlab");

            var Incomeslab = (from s in db.IncomeSlabs where s.SlabId == SlabId select s).FirstOrDefault();
            db.IncomeSlabs.Remove(Incomeslab);
            db.SaveChanges();
        }

        #endregion

        #endregion
    }
}