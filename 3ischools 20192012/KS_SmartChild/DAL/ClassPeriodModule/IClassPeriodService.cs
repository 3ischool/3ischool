﻿using KSModel.Models;
using KS_SmartChild.ViewModel.TimeTable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.ClassPeriodModule
{
    public partial interface IClassPeriodService
    {
        #region Period

        Period GetPeriodById(int PeriodId, bool IsActive = false);

        List<Period> GetAllPeriods(ref int totalcount, string Period = "", int? PeriodIndex = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        //List<Period> GetPeriodsByClass(ref int totalcount, string Period = "", int? PeriodIndex = null,
        //   int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertPeriod(Period Period);

        void UpdatePeriod(Period Period);

        void DeletePeriod(int PeriodId = 0);

        #endregion

        #region ClassPeriod

        ClassPeriod GetClassPeriodById(int ClassPeriodId, bool IsActive = false);

        List<ClassPeriod> GetAllClassPeriods(ref int totalcount, int? PeriodId = null, int? ClassSubjectId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertClassPeriod(ClassPeriod ClassPeriod);

        void UpdateClassPeriod(ClassPeriod ClassPeriod);

        void DeleteClassPeriod(int ClassPeriodId = 0);

        #endregion

        #region Time Table

        TimeTable GetTimeTableById(int TimeTableId, bool IsActive = false);

        List<TimeTable> GetAllTimeTables(ref int totalcount, DateTime? EffectiveFrom = null, DateTime? EffectiveTill = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTimeTable(TimeTable TimeTable);

        void UpdateTimeTabled(TimeTable TimeTable);

        void DeleteTimeTable(int TimeTableId = 0);

        IList<TimeTableGridModel> GetTimeTablegridViewBySP(int Classid, int[] VirtualClassId,int TimeTableId);
        #endregion

        #region ClassPeriodDetail

        ClassPeriodDetail GetClassPeriodDetailById(int ClassPeriodDetailId, bool IsActive = false);

        List<ClassPeriodDetail> GetAllClassPeriodDetails(ref int totalcount, int? ClassPeriodId = null, int? TeacherId = null,
            int? TimeTableId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertClassPeriodDetail(ClassPeriodDetail ClassPeriodDetail);

        void UpdateClassPeriodDetail(ClassPeriodDetail ClassPeriodDetail);

        void DeleteClassPeriodDetail(int ClassPeriodDetailId = 0);

        #endregion

        #region Day Master

        DayMaster GetDayMasterById(int DayMasterId, bool IsActive = false);

        List<DayMaster> GetAllDayMasters(ref int totalcount, string Dayname = null, string Dayabr = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertDayMaster(DayMaster DayMaster);

        void UpdateDayMaster(DayMaster DayMaster);

        void DeleteDayMaster(int DayMasterId = 0);

        #endregion

        #region Class Period Day

        ClassPeriodDay GetClassPeriodDayById(int ClassPeriodDayId, bool IsActive = false);

        List<ClassPeriodDay> GetAllClassPeriodDays(ref int totalcount, int? DayId = null, int? ClassPeriodDetailId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertClassPeriodDay(ClassPeriodDay ClassPeriodDay);

        void UpdateClassPeriodDay(ClassPeriodDay ClassPeriodDay);

        void DeleteClassPeriodDay(int ClassPeriodDayId = 0);

        #endregion

        #region TimeTableAdjustment

        TimeTableAdjustment GetTimeTableAdjustmentById(int TimeTableAdjustmentId, bool IsActive = false);

        List<TimeTableAdjustment> GetAllTimeTableAdjustments(ref int totalcount, int? ClassPeriodDetailId = null, int? ClassPeriodId = null,
            int? TeacherId = null, DateTime? Date = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTimeTableAdjustment(TimeTableAdjustment TimeTableAdjustment);

        void UpdateTimeTableAdjustment(TimeTableAdjustment TimeTableAdjustment);

        void DeleteTimeTableAdjustment(int TimeTableAdjustmentId = 0);

        #endregion


        #region Time Table Period Quota

        TimeTablePeriodQuota GetTimeTablePeriodQuotaById(int TimeTablePeriodQuotaId, bool IsActive = false);

        List<TimeTablePeriodQuota> GetAllTimeTablePeriodQuotas(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTimeTablePeriodQuota(TimeTablePeriodQuota TimeTablePeriodQuota);

        void UpdateTimeTablePeriodQuotad(TimeTablePeriodQuota TimeTablePeriodQuota);

        void DeleteTimeTablePeriodQuota(int TimeTablePeriodQuotaId = 0);

        IList<StaffPeriodDetailModel> GetStaffPeriodDetailBySP();
        #endregion

        #region Time Table Quota Detail

        TimeTableQuotaDetail GetTimeTableQuotaDetailById(int TimeTableQuotaDetailId, bool IsActive = false);

        List<TimeTableQuotaDetail> GetAllTimeTableQuotaDetails(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTimeTableQuotaDetail(TimeTableQuotaDetail TimeTableQuotaDetail);

        void UpdateTimeTableQuotaDetaild(TimeTableQuotaDetail TimeTableQuotaDetail);

        void DeleteTimeTableQuotaDetail(int TimeTableQuotaDetailId = 0);

        #endregion

        #region ClassTest

         KSModel.Models.ClassTest GetClassTestById(int ClassTestId, bool IsActive = false);


        List<KSModel.Models.ClassTest> GetAllClassTests(ref int totalcount, string ClassTest = "",
           int PageIndex = 0, int PageSize = int.MaxValue);


        void InsertClassTest(KSModel.Models.ClassTest ClassTest);


        void UpdateClassTest(KSModel.Models.ClassTest ClassTest);


        void DeleteClassTest(int ClassTestId = 0);
       

        #endregion
    }
}
