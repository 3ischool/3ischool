﻿using KSModel.Models;
using KS_SmartChild.ViewModel.Stock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.StockModule
{
    public partial interface IStockService
    {
        #region"Product"
        List<ProductInfo> GetAllStocksBysP(ref int totalcount, string ProductName = "", int ProductGroupId = 0, 
            int ProductTypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue, int ProductId = 0, int ProcedureType=1);

        List<Product> GetAllProducts(ref int totalcount, string ProductName = "", string ProductAbbr = "", string ProductDescription = "",
                              int? ProductTypeId = 0, int? ProductGroupId = 0, int PageIndex = 0, int PageSize = int.MaxValue,
                               string ProductType = "", string ProductGroup = "", bool Isfilter = false);
        Product GetProductById(int? ProductId, bool IsTrack = false);
        ProductInfo GetProductInfoById(int? ProductId, bool IsTrack = false);
        void InsertProduct(Product ProductM);
        void UpdateProduct(Product ProductM);

        List<ProductGroup> GetAllProductGroup(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);
        List<ProductType> GetAllProductType(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);
        List<Unit> GetAllUnit(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);

        bool GetDeleteStatus(Boolean IsAuthToDelete, string ProductId=null);

        void DeleteProduct(int ProductId);

        string GetGroupName(int ProGroupId);

        string GetProductType(int ProTypeId);
        string GetUnit(int UnitId);

        List<ProductInfo> GetAllRateHistoryBysP(ref int totalcount, int ProductId, int PageIndex = 0, int PageSize = int.MaxValue, int ProcedureType = 2);

        void InserRateHistory(ProductRateHistory PRH);
        void UpdateEffectiveTillDate(int ProductId);

        List<ProductInfo> GetAllProductSpecificationBysP(ref int totalcount, int ProductId, int PageIndex = 0, int PageSize = int.MaxValue, int ProcedureType = 3);

        void InsertOrUpdateProductSpecificationbySp(int ProductId = 0, int DetailId = 0, string AttributeValue = "", int AttributeId = 0);

        ProductDetail GetProductDetailById(int? DetailId, bool IsTrack = false);
        List<ProductAttribute> GetAllAttributes(ref int totalcount,int ProductId=0, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);
        void DeleteProductDetail(int detailId);


        List<ProductInfo> GetAllProductUnitConversionBysP(ref int totalcount, int ProductId, int PageIndex = 0, int PageSize = int.MaxValue, int ProcedureType =4);
        void InsertProductUnitConversion(ProductUnitConversion PUC);
        void DeleteProductUnitConversion(int UnitConversionId);

        void InsertProductImage(ProductImage PI);
        void UpdateProductImage(ProductImage PI);

        ProductImage GetProductImageById(int? ProductId, bool IsTrack = false);

        List<ProductAttributeValue> GetAllAttributeValues(ref int totalcount, int ProductAttributeId = 0, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false);
        bool InsertProductAttributeValue(ProductAttributeValue PAV);

        void InserorUpdateProductSpecification(ProductDetail PD);

        void DeleteProductRateHistory(int HistoryId);

        List<ClassMaster> GetAllClasses(ref int totalcount);
        List<Product> GetAllProducts(ref int totalcount);

        int  InsertorUpdatePackage(StockPackage StockPack);

        void InsertStockPackageClass(StockPackageClass StockCls);

        void InsertorUpdateStockPackageDetail(StockPackageDetail StockDetail);

        List<ProductInfo> GetAllPackagesBysP(ref int totalcount, string PackageName = ""
          , int PageIndex = 0, int PageSize = int.MaxValue, int ProcedureType = 1);

        ProductInfo GetPackageById(int? PackageId, bool IsTrack = false);

        void DeletePackage(int PackageId);

        List<ProductsDetailList> GetProductDetailList(int VoucherId = 0, int ProcedureType = 1);

        List<Inv_Store> GetAllStores(ref int count, int UserId);

        List<AccountTypeMaster> GetAllAccountTypes(ref int count);

        List<AccountListD> GetAllaccounts(ref int count, int AccounTypeId = 0);
        List<KSModel.Models.TaxType> GetAllTaxtypes(ref int count);
        VoucherInfo GetVoucherDetail(int VoucherId = 0,int ProcedureType=2);
        List<KSModel.Models.TrsDoc> GetAllVoucherTypes();
        List<KSModel.Models.StockPackage> GetAllPackages(ref int count);
        List<KSModel.Models.StockPackageClass> GetAllStockPackageClasses(ref int count);
        #endregion
    }
}