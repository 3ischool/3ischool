﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSModel.Models;

namespace KS_SmartChild.DAL.MenuModel
{
    public partial interface INavigationService
    {
        #region NavigationItem

        NavigationItem GetNavigationItemById(int NavigationItemId, bool IsTrack = false);

        List<NavigationItem> GetAllNavigationItems(ref int totalcount, string NavigationItem = "", string QueryString = "", 
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertNavigationItem(NavigationItem NavigationItem);

        void UpdateNavigationItem(NavigationItem NavigationItem);

        void DeleteNavigationItem(int NavigationItemId = 0);

        #endregion

        #region UserRoleNavigationItem

        UserRoleNavigationItem GetUserRoleNavigationItemById(int UserRoleNavigationItemId, bool IsTrack = false);

        List<UserRoleNavigationItem> GetAllUserRoleNavigationItems(ref int totalcount,
            int? UserRoleId = null, int? NavigationItemId = null,bool? IsActive = null, int PageIndex = 0,
            int PageSize = int.MaxValue);

        void InsertUserRoleNavigationItem(UserRoleNavigationItem UserRoleNavigationItem);

        void UpdateUserRoleNavigationItem(UserRoleNavigationItem UserRoleNavigationItem);

        void DeleteUserRoleNavigationItem(int UserRoleNavigationItemId = 0);

        #endregion

        #region UserMobNavigationItem

        UserMobNavigationItem GetUserMobNavigationItemById(int UserMobNavigationItemId, bool IsTrack = false);

        List<UserMobNavigationItem> GetAllUserMobNavigationItems(ref int totalcount, string NavigationItem = "",
            int? UserRoleId = null, int? NavigationItemId = null, int? RefId = null, int? SortingIndex = null,
            int? NavigationLevel = null, bool? IsActive = null, int PageIndex = 0,
            int PageSize = int.MaxValue);

        void InsertUserMobNavigationItem(UserMobNavigationItem UserMobNavigationItem);

        void UpdateUserMobNavigationItem(UserMobNavigationItem UserMobNavigationItem);

        void DeleteUserMobNavigationItem(int UserMobNavigationItemId = 0);

        #endregion

    }
}
