﻿using KS_SmartChild.BAL.Common;
using KS_SmartChild.BAL.SPClasses;
using KS_SmartChild.DAL.CatalogMaster;
using KS_SmartChild.DAL.Common;
using KSModel.Models;
using KS_SmartChild.ViewModel.Fee;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace KS_SmartChild.DAL.FeeModule
{
    public partial class FeeService : IFeeService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;
        //private readonly string SqlDataSource;
        private readonly ICatalogMasterService _ICatalogMasterService;
        public FeeService(IConnectionService IConnectionService, ICatalogMasterService ICatalogMasterService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
            this._ICatalogMasterService = ICatalogMasterService;
        }

        #region Methods

        #region FeeFrequency

        public FeeFrequency GetFeeFrequencyById(int FeeFrequencyId, bool IsTrack = false)
        {
            if (FeeFrequencyId == 0)
                throw new ArgumentNullException("FeeFrequency");

            var FeeFrequency = new FeeFrequency();
            if (IsTrack)
                FeeFrequency = (from s in db.FeeFrequencies where s.FeeFrequencyId == FeeFrequencyId select s).FirstOrDefault();
            else
                FeeFrequency = (from s in db.FeeFrequencies.AsNoTracking() where s.FeeFrequencyId == FeeFrequencyId select s).FirstOrDefault();

            return FeeFrequency;
        }

        public List<FeeFrequency> GetAllFeeFrequencies(ref int totalcount, string FeeFrequency = "", int? NoOfMonths = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeeFrequencies.ToList();
            if (!String.IsNullOrEmpty(FeeFrequency))
                query = query.Where(e => e.FeeFrequency1.ToLower().Contains(FeeFrequency.ToLower())).ToList();
            if (NoOfMonths != null)
                query = query.Where(e => e.NoOfMonths == NoOfMonths).ToList();

            totalcount = query.Count;
            var FeeFrequencies = new PagedList<KSModel.Models.FeeFrequency>(query, PageIndex, PageSize);
            return FeeFrequencies;
        }

        public void InsertFeeFrequency(FeeFrequency FeeFrequency)
        {
            if (FeeFrequency == null)
                throw new ArgumentNullException("FeeFrequency");

            db.FeeFrequencies.Add(FeeFrequency);
            db.SaveChanges();
        }

        public void UpdateFeeFrequency(FeeFrequency FeeFrequency)
        {
            if (FeeFrequency == null)
                throw new ArgumentNullException("FeeFrequency");

            db.Entry(FeeFrequency).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeFrequency(int FeeFrequencyId = 0)
        {
            if (FeeFrequencyId == 0)
                throw new ArgumentNullException("FeeFrequency");

            var FeeFrequency = (from s in db.FeeFrequencies where s.FeeFrequencyId == FeeFrequencyId select s).FirstOrDefault();
            db.FeeFrequencies.Remove(FeeFrequency);
            db.SaveChanges();
        }

        #endregion

        #region FeeType

        public FeeType GetFeeTypeById(int FeeTypeId, bool IsTrack = false)
        {
            if (FeeTypeId == 0)
                throw new ArgumentNullException("FeeType");

            var FeeType = new FeeType();
            if (IsTrack)
                FeeType = (from s in db.FeeTypes where s.FeeTypeId == FeeTypeId select s).FirstOrDefault();
            else
                FeeType = (from s in db.FeeTypes.AsNoTracking() where s.FeeTypeId == FeeTypeId select s).FirstOrDefault();

            return FeeType;
        }

        public List<FeeType> GetAllFeeTypes(ref int totalcount, string FeeType = "", bool? Status = null, int? FeeFrequencyId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeeTypes.ToList();
            if (!String.IsNullOrEmpty(FeeType))
                query = query.Where(e => e.FeeType1.ToLower().Contains(FeeType.ToLower())).ToList();
            if (FeeFrequencyId != null && FeeFrequencyId > 0)
                query = query.Where(e => e.FeeFrequencyId == FeeFrequencyId).ToList();
            if (Status != null)
            {
                if ((bool)Status)
                    query = query.Where(e => e.Status == true).ToList();
                else
                    query = query.Where(e => e.Status == false).ToList();
            }
            else
                query = query.Where(e => e.Status == false || e.Status == true || e.Status == null).ToList();

            totalcount = query.Count;
            var FeeTypes = new PagedList<KSModel.Models.FeeType>(query, PageIndex, PageSize);
            return FeeTypes;
        }

        public List<FeeType> GetFeeTypesByClass(int[] FeeTypeIds)
        {
            var query = db.FeeTypes.ToList();
            query = query.Where(f => FeeTypeIds.Contains(f.FeeTypeId)).ToList();

            var feetypes = query.ToList();
            return feetypes;
        }

        public void InsertFeeType(FeeType FeeType)
        {
            if (FeeType == null)
                throw new ArgumentNullException("FeeType");

            db.FeeTypes.Add(FeeType);
            db.SaveChanges();
        }

        public void UpdateFeeType(FeeType FeeType)
        {
            if (FeeType == null)
                throw new ArgumentNullException("FeeType");

            db.Entry(FeeType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeType(int FeeTypeId = 0)
        {
            if (FeeTypeId == 0)
                throw new ArgumentNullException("FeeType");

            var FeeType = (from s in db.FeeTypes where s.FeeTypeId == FeeTypeId select s).FirstOrDefault();
            db.FeeTypes.Remove(FeeType);
            db.SaveChanges();
        }

        public void DeleteReceipt(int ReceiptId = 0)
        {
            if (ReceiptId == 0)
                throw new ArgumentNullException("FeeReceipt");
            //delete feereceipt concessiondetail

            var FRCD = (from frdc in db.FeeReceiptDetailConcs
                        join frd in db.FeeReceiptDetails
on frdc.FeeReceiptDetailId equals frd.ReceiptDetailId
                        join fr in db.FeeReceipts on frd.ReceiptId equals fr.ReceiptId
                        where fr.ReceiptId == ReceiptId
                        select frdc).ToList();

            if (FRCD != null)
            {
                db.FeeReceiptDetailConcs.RemoveRange(FRCD);
                db.SaveChanges();
            }
            var FRD = (from s in db.FeeReceiptDetails
                       where s.ReceiptId == ReceiptId
                       select s).ToList();

            if (FRD != null)
            {
                db.FeeReceiptDetails.RemoveRange(FRD);
                db.SaveChanges();
            }

            var FPD = (from s in db.FeePrevPendingReceipts
                       where s.ReceiptId == ReceiptId
                       select s).ToList();
            if (FPD != null)
            {
                db.FeePrevPendingReceipts.RemoveRange(FPD);
                db.SaveChanges();
            }

            // Delete Receipt
            var Feereceipt = (from s in db.FeeReceipts
                              where s.ReceiptId == ReceiptId
                              select s).FirstOrDefault();
            if (Feereceipt != null)
            {
                db.FeeReceipts.Remove(Feereceipt);
                db.SaveChanges();
            }

            var feeinstallments = db.FeeInstallments.Where(p => p.ReceiptId == ReceiptId).ToList();
            if (feeinstallments.Count > 0)
            {
                foreach (var item in feeinstallments)
                {
                    db.FeeInstallments.Remove(item);
                    db.SaveChanges();
                }
            }
        }

        #endregion

        #region FeeClassGroup

        public KSModel.Models.FeeClassGroup GetFeeClassGroupById(int FeeClassGroupId, bool IsTrack = false)
        {
            if (FeeClassGroupId == 0)
                throw new ArgumentNullException("FeeClassGroup");

            var FeeClassGroup = new FeeClassGroup();
            if (IsTrack)
                FeeClassGroup = (from s in db.FeeClassGroups where s.FeeClassGroupId == FeeClassGroupId select s).FirstOrDefault();
            else
                FeeClassGroup = (from s in db.FeeClassGroups.AsNoTracking() where s.FeeClassGroupId == FeeClassGroupId select s).FirstOrDefault();

            return FeeClassGroup;
        }

        public List<KSModel.Models.FeeClassGroup> GetAllFeeClassGroups(ref int totalcount, string FeeClassGroup = "", int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.FeeClassGroups.ToList();
            if (!String.IsNullOrEmpty(FeeClassGroup))
                query = query.Where(e => e.FeeClassGroup1.ToLower().Contains(FeeClassGroup.ToLower())).ToList();

            totalcount = query.Count;
            var FeeClassGroups = new PagedList<KSModel.Models.FeeClassGroup>(query, PageIndex, PageSize);
            return FeeClassGroups;
        }

        public void InsertFeeClassGroup(KSModel.Models.FeeClassGroup FeeClassGroup)
        {
            if (FeeClassGroup == null)
                throw new ArgumentNullException("FeeClassGroup");

            db.FeeClassGroups.Add(FeeClassGroup);
            db.SaveChanges();
        }

        public void UpdateFeeClassGroup(KSModel.Models.FeeClassGroup FeeClassGroup)
        {
            if (FeeClassGroup == null)
                throw new ArgumentNullException("FeeClassGroup");

            db.Entry(FeeClassGroup).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeClassGroup(int FeeClassGroupId = 0)
        {
            if (FeeClassGroupId == 0)
                throw new ArgumentNullException("FeeClassGroup");

            var FeeClassGroup = (from s in db.FeeClassGroups where s.FeeClassGroupId == FeeClassGroupId select s).FirstOrDefault();
            db.FeeClassGroups.Remove(FeeClassGroup);
            db.SaveChanges();
        }

        #endregion

        #region FeeClassGroupType

        public KSModel.Models.FeeClassGroupType GetFeeClassGroupTypeById(int FeeClassGroupTypeId, bool IsTrack = false)
        {
            if (FeeClassGroupTypeId == 0)
                throw new ArgumentNullException("FeeClassGroupType");

            var FeeClassGroupType = new FeeClassGroupType();
            if (IsTrack)
                FeeClassGroupType = (from s in db.FeeClassGroupTypes where s.FeeClassGroupTypeId == FeeClassGroupTypeId select s).FirstOrDefault();
            else
                FeeClassGroupType = (from s in db.FeeClassGroupTypes.AsNoTracking() where s.FeeClassGroupTypeId == FeeClassGroupTypeId select s).FirstOrDefault();

            return FeeClassGroupType;
        }

        public List<KSModel.Models.FeeClassGroupType> GetAllFeeClassGroupTypes(ref int totalcount, int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.FeeClassGroupTypes.ToList();

            totalcount = query.Count;
            var FeeClassGroupTypes = new PagedList<KSModel.Models.FeeClassGroupType>(query, PageIndex, PageSize);
            return FeeClassGroupTypes;
        }

        public void InsertFeeClassGroupType(KSModel.Models.FeeClassGroupType FeeClassGroupType)
        {
            if (FeeClassGroupType == null)
                throw new ArgumentNullException("FeeClassGroupType");

            db.FeeClassGroupTypes.Add(FeeClassGroupType);
            db.SaveChanges();
        }

        public void UpdateFeeClassGroupType(KSModel.Models.FeeClassGroupType FeeClassGroupType)
        {
            if (FeeClassGroupType == null)
                throw new ArgumentNullException("FeeClassGroupType");

            db.Entry(FeeClassGroupType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeClassGroupType(int FeeClassGroupTypeId = 0)
        {
            if (FeeClassGroupTypeId == 0)
                throw new ArgumentNullException("FeeClassGroupType");

            var FeeClassGroupType = (from s in db.FeeClassGroupTypes where s.FeeClassGroupTypeId == FeeClassGroupTypeId select s).FirstOrDefault();
            db.FeeClassGroupTypes.Remove(FeeClassGroupType);
            db.SaveChanges();
        }

        #endregion

        #region FeeClassGroupDetail

        public FeeClassGroupDetail GetFeeClassGroupDetailById(int FeeClassGroupDetailId, bool IsTrack = false)
        {
            if (FeeClassGroupDetailId == 0)
                throw new ArgumentNullException("FeeClassGroupDetail");

            var FeeClassGroupDetail = new FeeClassGroupDetail();
            if (IsTrack)
                FeeClassGroupDetail = (from s in db.FeeClassGroupDetails where s.FeeClassGroupDetailId == FeeClassGroupDetailId select s).FirstOrDefault();
            else
                FeeClassGroupDetail = (from s in db.FeeClassGroupDetails.AsNoTracking() where s.FeeClassGroupDetailId == FeeClassGroupDetailId select s).FirstOrDefault();

            return FeeClassGroupDetail;
        }

        public List<FeeClassGroupDetail> GetAllFeeClassGroupDetails(ref int totalcount, int? FeeClassGroupId = null,
            int? StandardId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeeClassGroupDetails.ToList();
            if (FeeClassGroupId != null && FeeClassGroupId > 0)
                query = query.Where(e => e.FeeClassGroupId == FeeClassGroupId).ToList();
            if (StandardId != null && StandardId > 0)
                query = query.Where(e => e.StandardId == StandardId).ToList();

            totalcount = query.Count;
            var FeeClassGroupDetails = new PagedList<KSModel.Models.FeeClassGroupDetail>(query, PageIndex, PageSize);
            return FeeClassGroupDetails;
        }

        public void InsertFeeClassGroupDetail(FeeClassGroupDetail FeeClassGroupDetail)
        {
            if (FeeClassGroupDetail == null)
                throw new ArgumentNullException("FeeClassGroupDetail");

            db.FeeClassGroupDetails.Add(FeeClassGroupDetail);
            db.SaveChanges();
        }

        public void UpdateFeeClassGroupDetail(FeeClassGroupDetail FeeClassGroupDetail)
        {
            if (FeeClassGroupDetail == null)
                throw new ArgumentNullException("FeeClassGroupDetail");

            db.Entry(FeeClassGroupDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeClassGroupDetails(int FeeClassGroupDetailId = 0)
        {
            if (FeeClassGroupDetailId == 0)
                throw new ArgumentNullException("FeeClassGroupDetail");

            var FeeClassGroupDetail = (from s in db.FeeClassGroupDetails where s.FeeClassGroupDetailId == FeeClassGroupDetailId select s).FirstOrDefault();
            db.FeeClassGroupDetails.Remove(FeeClassGroupDetail);
            db.SaveChanges();
        }

        #endregion

        #region FeeHeadType

        public FeeHeadType GetFeeHeadTypeById(int FeeHeadTypeId, bool IsTrack = false)
        {
            if (FeeHeadTypeId == 0)
                throw new ArgumentNullException("FeeHeadType");

            var FeeHeadType = new FeeHeadType();
            if (IsTrack)
                FeeHeadType = (from s in db.FeeHeadTypes where s.FeeHeadTypeId == FeeHeadTypeId select s).FirstOrDefault();
            else
                FeeHeadType = (from s in db.FeeHeadTypes.AsNoTracking() where s.FeeHeadTypeId == FeeHeadTypeId select s).FirstOrDefault();

            return FeeHeadType;
        }

        public List<FeeHeadType> GetAllFeeHeadTypes(ref int totalcount, string FeeHeadType = "", bool IsAddon = false, int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.FeeHeadTypes.ToList();
            if (!String.IsNullOrEmpty(FeeHeadType))
                query = query.Where(e => e.FeeHeadType1.ToLower().Contains(FeeHeadType.ToLower())).ToList();
            if (IsAddon == true)
                query = query.Where(e => e.IsAddon == IsAddon).ToList();

            totalcount = query.Count;
            var FeeHeadTypes = new PagedList<KSModel.Models.FeeHeadType>(query, PageIndex, PageSize);
            return FeeHeadTypes;
        }

        public void InsertFeeHeadType(FeeHeadType FeeHeadType)
        {
            if (FeeHeadType == null)
                throw new ArgumentNullException("FeeHeadType");

            db.FeeHeadTypes.Add(FeeHeadType);
            db.SaveChanges();
        }

        public void UpdateFeeHeadType(FeeHeadType FeeHeadType)
        {
            if (FeeHeadType == null)
                throw new ArgumentNullException("FeeHeadType");

            db.Entry(FeeHeadType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeHeadType(int FeeHeadTypeId = 0)
        {
            if (FeeHeadTypeId == 0)
                throw new ArgumentNullException("FeeHeadType");

            var FeeHeadType = (from s in db.FeeHeadTypes where s.FeeHeadTypeId == FeeHeadTypeId select s).FirstOrDefault();
            db.FeeHeadTypes.Remove(FeeHeadType);
            db.SaveChanges();
        }

        #endregion

        #region FeeHead

        public FeeHead GetFeeHeadById(int FeeHeadId, bool IsTrack = false)
        {
            if (FeeHeadId == 0)
                throw new ArgumentNullException("FeeHead");

            var FeeHead = new FeeHead();
            if (IsTrack)
                FeeHead = (from s in db.FeeHeads where s.FeeHeadId == FeeHeadId select s).FirstOrDefault();
            else
                FeeHead = (from s in db.FeeHeads.AsNoTracking() where s.FeeHeadId == FeeHeadId select s).FirstOrDefault();

            return FeeHead;
        }

        public List<FeeHead> GetAllFeeHeads(ref int totalcount, int? FeeHeadTypeId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeeHeads.ToList();
            if (FeeHeadTypeId != null && FeeHeadTypeId > 0)
                query = query.Where(e => e.FeeHeadTypeId == FeeHeadTypeId).ToList();

            totalcount = query.Count;
            var FeeHeads = new PagedList<KSModel.Models.FeeHead>(query, PageIndex, PageSize);
            return FeeHeads;
        }

        public void InsertFeeHead(FeeHead FeeHead)
        {
            if (FeeHead == null)
                throw new ArgumentNullException("FeeHead");

            db.FeeHeads.Add(FeeHead);
            db.SaveChanges();
        }

        public void UpdateFeeHead(FeeHead FeeHead)
        {
            if (FeeHead == null)
                throw new ArgumentNullException("FeeClassGroup");

            db.Entry(FeeHead).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeHead(int FeeHeadId = 0)
        {
            if (FeeHeadId == 0)
                throw new ArgumentNullException("FeeHead");

            var FeeHead = (from s in db.FeeHeads where s.FeeHeadId == FeeHeadId select s).FirstOrDefault();
            db.FeeHeads.Remove(FeeHead);
            db.SaveChanges();
        }

        #endregion

        #region GroupFeeType

        public GroupFeeType GetGroupFeeTypeById(int GroupFeeTypeId, bool IsTrack = false)
        {
            if (GroupFeeTypeId == 0)
                throw new ArgumentNullException("GroupFeeType");

            var GroupFeeType = new GroupFeeType();
            if (IsTrack)
                GroupFeeType = (from s in db.GroupFeeTypes where s.GroupFeeTypeId == GroupFeeTypeId select s).FirstOrDefault();
            else
                GroupFeeType = (from s in db.GroupFeeTypes.AsNoTracking() where s.GroupFeeTypeId == GroupFeeTypeId select s).FirstOrDefault();

            return GroupFeeType;
        }

        public List<GroupFeeType> GetAllGroupFeeTypes(ref int totalcount, int? FeeTypeId = null, int? FeeClassGroupId = null, bool? TakeAddOns = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.GroupFeeTypes.ToList();
            if (FeeTypeId != null && FeeTypeId > 0)
                query = query.Where(e => e.FeeTypeId == FeeTypeId).ToList();
            if (FeeClassGroupId != null && FeeClassGroupId > 0)
                query = query.Where(e => e.FeeClassGroupId == FeeClassGroupId).ToList();
            if (TakeAddOns != null)
                query = query.Where(g => g.TakeAddOns == TakeAddOns).ToList();

            totalcount = query.Count;
            var GroupFeeTypes = new PagedList<KSModel.Models.GroupFeeType>(query, PageIndex, PageSize);
            return GroupFeeTypes;
        }

        public void InsertGroupFeeType(GroupFeeType GroupFeeType)
        {
            if (GroupFeeType == null)
                throw new ArgumentNullException("GroupFeeType");

            db.GroupFeeTypes.Add(GroupFeeType);
            db.SaveChanges();
        }

        public void UpdateGroupFeeType(GroupFeeType GroupFeeType)
        {
            if (GroupFeeType == null)
                throw new ArgumentNullException("GroupFeeType");

            db.Entry(GroupFeeType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteGroupFeeType(int GroupFeeTypeId = 0)
        {
            if (GroupFeeTypeId == 0)
                throw new ArgumentNullException("GroupFeeType");

            var GroupFeeType = (from s in db.GroupFeeTypes where s.GroupFeeTypeId == GroupFeeTypeId select s).FirstOrDefault();
            db.GroupFeeTypes.Remove(GroupFeeType);
            db.SaveChanges();
        }

        #endregion

        #region GroupFeeHead

        public GroupFeeHead GetGroupFeeHeadById(int GroupFeeHeadId, bool IsTrack = false)
        {
            if (GroupFeeHeadId == 0)
                throw new ArgumentNullException("GroupFeeHead");

            var GroupFeeHead = new GroupFeeHead();
            if (IsTrack)
                GroupFeeHead = (from s in db.GroupFeeHeads where s.GroupFeeHeadId == GroupFeeHeadId select s).FirstOrDefault();
            else
                GroupFeeHead = (from s in db.GroupFeeHeads.AsNoTracking() where s.GroupFeeHeadId == GroupFeeHeadId select s).FirstOrDefault();

            return GroupFeeHead;
        }

        public List<GroupFeeHead> GetAllGroupFeeHeads(ref int totalcount, int? GroupFeeTypeId = null, int? FeeHeadId = null, DateTime? EffectiveDate = null,
            bool? IsCommon = null, bool? IsPending = null, bool? IsLateFee = null, int PageIndex = 0, int PageSize = int.MaxValue, int? ReceiptTypeFeeHeadId = null, int? ReceiptTypeId = null)
        {
            var query = db.GroupFeeHeads.ToList();
            if (FeeHeadId != null && FeeHeadId > 0)
                query = query.Where(e => e.FeeHeadId == FeeHeadId).ToList();
            if (GroupFeeTypeId != null && GroupFeeTypeId > 0)
                query = query.Where(e => e.GroupFeeTypeId == GroupFeeTypeId).ToList();

            if (IsCommon != null)
                query = query.Where(e => e.IsCommon == IsCommon).ToList();

            if (IsPending != null)
                query = query.Where(e => e.IsPending == IsPending).ToList();

            if (IsLateFee != null)
                query = query.Where(e => e.IsLateFee == IsLateFee).ToList();

            if (ReceiptTypeFeeHeadId != null)
                query = query.Where(f => f.ReceiptTypeFeeHeadId == ReceiptTypeFeeHeadId).ToList();

            if (ReceiptTypeId != null)
                query = query.Where(f => f.ReceiptTypeId == ReceiptTypeId).ToList();

            totalcount = query.Count;
            var GroupFeeHeads = new PagedList<KSModel.Models.GroupFeeHead>(query, PageIndex, PageSize);
            return GroupFeeHeads;
        }

        public void InsertGroupFeeHead(GroupFeeHead GroupFeeHead)
        {
            if (GroupFeeHead == null)
                throw new ArgumentNullException("GroupFeeHead");

            db.GroupFeeHeads.Add(GroupFeeHead);
            db.SaveChanges();
        }

        public void UpdateGroupFeeHead(GroupFeeHead GroupFeeHead)
        {
            if (GroupFeeHead == null)
                throw new ArgumentNullException("GroupFeeHead");

            db.Entry(GroupFeeHead).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteGroupFeeHead(int GroupFeeHeadId = 0)
        {
            if (GroupFeeHeadId == 0)
                throw new ArgumentNullException("GroupFeeHead");

            var GroupFeeHead = (from s in db.GroupFeeHeads where s.GroupFeeHeadId == GroupFeeHeadId select s).FirstOrDefault();
            db.GroupFeeHeads.Remove(GroupFeeHead);
            db.SaveChanges();
        }

        #endregion

        #region GroupFeeHeadDetail

        public GroupFeeHeadDetail GetGroupFeeHeadDetailById(int GroupFeeHeadDetailId, bool IsTrack = false)
        {
            if (GroupFeeHeadDetailId == 0)
                throw new ArgumentNullException("GroupFeeHeadDetail");

            var GroupFeeHeadDetail = new GroupFeeHeadDetail();
            if (IsTrack)
                GroupFeeHeadDetail = (from s in db.GroupFeeHeadDetails where s.GroupFeeHeadDetailId == GroupFeeHeadDetailId select s).FirstOrDefault();
            else
                GroupFeeHeadDetail = (from s in db.GroupFeeHeadDetails.AsNoTracking() where s.GroupFeeHeadDetailId == GroupFeeHeadDetailId select s).FirstOrDefault();

            return GroupFeeHeadDetail;
        }

        public List<GroupFeeHeadDetail> GetAllGroupFeeHeadDetails(ref int totalcount, int? GroupFeeHeadId = null, DateTime? EffectiveDate = null,
            decimal? Amount = null, bool? IsDefault = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.GroupFeeHeadDetails.ToList();
            if (GroupFeeHeadId != null && GroupFeeHeadId > 0)
                query = query.Where(e => e.GroupFeeHeadId == GroupFeeHeadId).ToList();
            if (EffectiveDate.HasValue)
                query = query.Where(e => e.EffectiveDate == EffectiveDate).ToList();
            if (Amount != null && Amount > 0)
                query = query.Where(e => e.Amount == Amount).ToList();
            if (IsDefault != null)
                query = query.Where(e => e.IsDefault == IsDefault).ToList();

            totalcount = query.Count;
            var GroupFeeHeadDetails = new PagedList<KSModel.Models.GroupFeeHeadDetail>(query, PageIndex, PageSize);
            return GroupFeeHeadDetails;
        }

        public void InsertGroupFeeHeadDetail(GroupFeeHeadDetail GroupFeeHeadDetail)
        {
            if (GroupFeeHeadDetail == null)
                throw new ArgumentNullException("GroupFeeHeadDetail");

            db.GroupFeeHeadDetails.Add(GroupFeeHeadDetail);
            db.SaveChanges();
        }

        public void UpdateGroupFeeHeadDetail(GroupFeeHeadDetail GroupFeeHeadDetail)
        {
            if (GroupFeeHeadDetail == null)
                throw new ArgumentNullException("GroupFeeHeadDetail");

            db.Entry(GroupFeeHeadDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteGroupFeeHeadDetail(int GroupFeeHeadDetailId = 0)
        {
            if (GroupFeeHeadDetailId == 0)
                throw new ArgumentNullException("GroupFeeHeadDetail");

            var GroupFeeHeadDetail = (from s in db.GroupFeeHeadDetails where s.GroupFeeHeadDetailId == GroupFeeHeadDetailId select s).FirstOrDefault();
            db.GroupFeeHeadDetails.Remove(GroupFeeHeadDetail);
            db.SaveChanges();
        }

        #endregion

        #region FeeReceipt

        public DataTable getfeereciept_storedprocedure(int StudentId)
        {
            // call store procedure for Selected student fee receipts
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_getfeereceiptbystudent", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@student", StudentId);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            return dt;

        }


        public FeeReceipt GetFeeReceiptById(int FeeReceiptId, bool IsTrack = false)
        {
            if (FeeReceiptId == 0)
                throw new ArgumentNullException("FeeReceipt");

            var FeeReceipt = new FeeReceipt();
            if (IsTrack)
                FeeReceipt = (from s in db.FeeReceipts where s.ReceiptId == FeeReceiptId select s).FirstOrDefault();
            else
                FeeReceipt = (from s in db.FeeReceipts.AsNoTracking() where s.ReceiptId == FeeReceiptId select s).FirstOrDefault();

            return FeeReceipt;
        }

        public List<FeeReceipt> GetAllFeeReceipts(ref int totalcount, int? ReceiptNo = null, DateTime? ReceiptFromDate = null,
            DateTime? ReceiptToDate = null, int? StudentId = null, int? FeePeriodId = null, int? PaymentModeId = null,
            decimal? PaidAmountFrom = null, decimal? PaidAmountTo = null, int?[] StudentIds = null, int?[] PaymentModeIds = null,
            int PageIndex = 0, int PageSize = int.MaxValue, int? ReceiptTypeId = null, int? SessionId = null)
        {
            var query = db.FeeReceipts.AsQueryable();
            if (ReceiptTypeId != null && ReceiptTypeId > 0)
                query = query.Where(e => e.ReceiptTypeId == ReceiptTypeId);
            if (ReceiptNo != null && ReceiptNo > 0)
                query = query.Where(e => e.ReceiptNo == ReceiptNo);
            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId);
            if (ReceiptFromDate.HasValue)
                query = query.Where(e => e.ReceiptDate >= ReceiptFromDate);
            if (ReceiptToDate.HasValue)
                query = query.Where(e => e.ReceiptDate <= ReceiptToDate);
            if (FeePeriodId != null && FeePeriodId > 0)
                query = query.Where(e => e.FeePeriodId == FeePeriodId);
            if (PaymentModeId != null && PaymentModeId > 0)
                query = query.Where(e => e.PaymentModeId == PaymentModeId);
            if (PaidAmountFrom != null && PaidAmountFrom > 0)
                query = query.Where(e => e.PaidAmount >= PaidAmountFrom);
            if (PaidAmountTo != null && PaidAmountTo > 0)
                query = query.Where(e => e.PaymentModeId <= PaidAmountTo);
            if (StudentIds != null)
                query = query.Where(f => StudentIds.Contains(f.StudentId));
            if (SessionId != null && SessionId > 0)
                query = query.Where(e => e.SessionId == SessionId);
            if (PaymentModeIds != null && PaymentModeIds.Count()>0)
                query = query.Where(f => PaymentModeIds.Contains(f.PaymentModeId));

          //  query = query.ToList();
            totalcount = query.Count();
            var FeeReceipts = new PagedList<KSModel.Models.FeeReceipt>(query.ToList(), PageIndex, PageSize);
            return FeeReceipts;
        }

        public void InsertFeeReceipt(FeeReceipt FeeReceipt)
        {
            if (FeeReceipt == null)
                throw new ArgumentNullException("FeeReceipt");

            db.FeeReceipts.Add(FeeReceipt);
            db.SaveChanges();
        }

        public void UpdateFeeReceipt(FeeReceipt FeeReceipt)
        {
            if (FeeReceipt == null)
                throw new ArgumentNullException("FeeReceipt");

            db.Entry(FeeReceipt).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeReceipt(int FeeReceiptId = 0)
        {
            if (FeeReceiptId == 0)
                throw new ArgumentNullException("FeeReceipt");

            var FeeReceipt = (from s in db.FeeReceipts where s.ReceiptId == FeeReceiptId select s).FirstOrDefault();
            db.FeeReceipts.Remove(FeeReceipt);
            db.SaveChanges();
        }

        public bool GetFeeReceiptStatus(int GroupFeeheadId = 0, int StudentId = 0, int SessionId = 0)
        {
            var query = db.FeeReceiptDetails.ToList();
            if (GroupFeeheadId > 0)
            {
                query = query.Where(x => x.GroupFeeHeadId == GroupFeeheadId).ToList();
            }
            if (StudentId > 0)
            {
                query = query.Where(x => x.FeeReceipt.StudentId == StudentId).ToList();
            }
            if (SessionId > 0)
            {
                query = query.Where(x => x.FeeReceipt.SessionId == SessionId).ToList();
            }
            if (query.Count > 0)
                return true;
            else
                return false;
        }

        public bool AddonshowStatus(int receiptTypeId = 0)
        {
            bool IsShowaddon = false;
            if (receiptTypeId > 0)
            {
                var receipttype = db.ReceiptTypes.Where(f => f.ReceiptTypeId == receiptTypeId).FirstOrDefault();
                if (receipttype != null)
                {
                    IsShowaddon = Convert.ToBoolean(receipttype.LinkStudentAddon);
                }
            }
            return IsShowaddon;
        }

        public decimal? GetRegistrationfee(int StandardId = 0)
        {
            //var feeclassgroupid = (int)_IFeeService.GetAllFeeClassGroupDetails(ref count).Where(m => m.StandardId == model.StandardId && m.FeeClassGroup.FeeClassGroupTypeId == null).FirstOrDefault().FeeClassGroupId;

            //            var ReceiptTypeFeeHeadId = _IFeeService.GetAllReceiptTypeFeeHeads(ref count).Where(m => m.FeeHead.FeeHeadType.FeeHeadType1.ToLower().Contains("registration") && m.ReceiptType.ReceiptCode == 3 && m.ReceiptType.ReceiptType1.ToLower().Contains("registration")).FirstOrDefault().ReceiptTypeFeeHeadId;
            //      var groupfeeheads= _IFeeService.GetAllGroupFeeHeads(ref count).
              var currentSession = _ICatalogMasterService.GetCurrentSession();
               var query= from t1 in db.GroupFeeHeadDetails
                          join t2 in db.GroupFeeHeads on t1.GroupFeeHeadId equals t2.GroupFeeHeadId
                          join t3 in db.GroupFeeTypes on t2.GroupFeeTypeId equals t3.GroupFeeTypeId
                          join t4 in db.FeeClassGroups on t3.FeeClassGroupId equals t4.FeeClassGroupId
                          join t5 in db.FeeClassGroupDetails on t4.FeeClassGroupId equals t5.FeeClassGroupId
                          join t6 in db.ReceiptTypeFeeHeads on t2.ReceiptTypeFeeHeadId equals t6.ReceiptTypeFeeHeadId
                          join t7 in db.ReceiptTypes on t6.ReceiptTypeId equals t7.ReceiptTypeId
                          join t8 in db.FeeHeads on t6.FeeHeadId equals t8.FeeHeadId
                          join t9 in db.FeeHeadTypes on t8.FeeHeadTypeId equals t9.FeeHeadTypeId

                          where t5.StandardId==StandardId && t7.ReceiptCode==3 
                          && t9.FeeHeadType1.ToLower().Contains("registration") && (t1.EffectiveDate !=null && (  
                          (
			   t1.EffectiveDate <= currentSession.StartDate
               && 
			   ((t1.EndDate!=null && t1.EffectiveDate >= currentSession.StartDate) || t1.EndDate==null)
			   )

               || 
			   (t1.EffectiveDate <= currentSession.StartDate && t1.EndDate==null)

               || 
			   (
               (t1.EffectiveDate <= currentSession.StartDate || t1.EffectiveDate <= currentSession.EndDate)
               && 
			   t1.EndDate==null
			   )

               ||
			    (
				(t1.EffectiveDate <= currentSession.StartDate || t1.EffectiveDate <= currentSession.EndDate)
               && 
			   (
			   t1.EndDate!=null && t1.EndDate >= currentSession.StartDate
			   )
			   ) ))
                          select t1;
               return query.ToList().FirstOrDefault()!=null?query.ToList().FirstOrDefault().NewStdAmount:0;

                   //t1 in db.FeeClassGroupDetails
                   //       join t2 in db.FeeClassGroups on t1.FeeClassGroupId equals t2.FeeClassGroupId 
                   //       join t3 in db.FeeClassGroupTypes  on t2.FeeClassGroupTypeId equals t3.FeeClassGroupTypeId 
                   //       join t4 in db.GroupFeeTypes on t2.FeeClassGroupId equals t4.FeeClassGroupId
                   //       join t5 in 
        }

        #endregion

        #region FeeReceiptDetail

        public FeeReceiptDetail GetFeeReceiptDetailById(int FeeReceiptDetailId, bool IsTrack = false)
        {
            if (FeeReceiptDetailId == 0)
                throw new ArgumentNullException("FeeReceiptDetail");

            var FeeReceiptDetail = new FeeReceiptDetail();
            if (IsTrack)
                FeeReceiptDetail = (from s in db.FeeReceiptDetails where s.ReceiptDetailId == FeeReceiptDetailId select s).FirstOrDefault();
            else
                FeeReceiptDetail = (from s in db.FeeReceiptDetails.AsNoTracking() where s.ReceiptDetailId == FeeReceiptDetailId select s).FirstOrDefault();

            return FeeReceiptDetail;
        }

        public List<FeeReceiptDetail> GetAllFeeReceiptDetails(ref int totalcount, int? ReceiptId = null, int? GroupFeeHeadId = null, int[] groupfeeheadidArray = null,
            decimal? FromAmount = null, decimal? ToAmount = null, int? PrintCount = null, int? ConsessionId = null,
            int PageIndex = 0, int PageSize = int.MaxValue, int? StudentId = null, int? FeePeriodId = null, int? ReceiptTypeId = null, int? SessionId = null)
        {
            var query = db.FeeReceiptDetails.ToList();
            if (ReceiptId != null && ReceiptId > 0)
                query = query.Where(e => e.ReceiptId == ReceiptId).ToList();
            if (GroupFeeHeadId != null && GroupFeeHeadId > 0)
                query = query.Where(e => e.GroupFeeHeadId == GroupFeeHeadId).ToList();
            if (groupfeeheadidArray != null)
                query = query.Where(m => m.GroupFeeHeadId != null && groupfeeheadidArray.Contains((int)m.GroupFeeHeadId)).ToList();
            if (FromAmount != null && FromAmount > 0)
                query = query.Where(e => e.Amount <= FromAmount).ToList();
            if (ToAmount != null && ToAmount > 0)
                query = query.Where(e => e.Amount >= ToAmount).ToList();
            if (PrintCount != null && PrintCount > 0)
                query = query.Where(e => e.PrintCount >= PrintCount).ToList();
            if (ConsessionId != null && ConsessionId > 0)
                query = query.Where(e => e.ConsessionId >= ConsessionId).ToList();
            if (StudentId > 0)
                query = query.Where(e => e.FeeReceipt.StudentId == StudentId).ToList();
            if (FeePeriodId > 0)
                query = query.Where(e => e.PendingFeePeriodId == FeePeriodId).ToList();
            if (ReceiptTypeId > 0)
                query = query.Where(e => e.FeeReceipt.ReceiptTypeId == ReceiptTypeId).ToList();
            if (SessionId > 0)
                query = query.Where(e => e.FeeReceipt.SessionId == SessionId).ToList();
            totalcount = query.Count;
            var FeeReceiptDetails = new PagedList<KSModel.Models.FeeReceiptDetail>(query, PageIndex, PageSize);
            return FeeReceiptDetails;
        }

        public List<FeeReceiptDetail> GetAllFeeReceiptDetailsFeePeriod(ref int totalcount, int? ReceiptId = null, int? GroupFeeHeadId = null,
           decimal? FromAmount = null, decimal? ToAmount = null, int? PrintCount = null, int? ConsessionId = null,
           int PageIndex = 0, int PageSize = int.MaxValue, int? StudentId = null, int? FeePeriodId = null, int? ReceiptTypeId = null, int? SessionId = null)
        {
            var query = db.FeeReceiptDetails.ToList();
            if (ReceiptId != null && ReceiptId > 0)
                query = query.Where(e => e.ReceiptId == ReceiptId).ToList();
            if (GroupFeeHeadId != null && GroupFeeHeadId > 0)
                query = query.Where(e => e.GroupFeeHeadId == GroupFeeHeadId).ToList();
            if (FromAmount != null && FromAmount > 0)
                query = query.Where(e => e.Amount <= FromAmount).ToList();
            if (ToAmount != null && ToAmount > 0)
                query = query.Where(e => e.Amount >= ToAmount).ToList();
            if (PrintCount != null && PrintCount > 0)
                query = query.Where(e => e.PrintCount >= PrintCount).ToList();
            if (ConsessionId != null && ConsessionId > 0)
                query = query.Where(e => e.ConsessionId >= ConsessionId).ToList();
            if (StudentId > 0)
                query = query.Where(e => e.FeeReceipt.StudentId == StudentId).ToList();
            if (FeePeriodId > 0)
                query = query.Where(e => e.FeeReceipt.FeePeriodId == FeePeriodId).ToList();
            if (ReceiptTypeId > 0)
                query = query.Where(e => e.FeeReceipt.ReceiptTypeId == ReceiptTypeId).ToList();
            if (SessionId > 0)
                query = query.Where(e => e.FeeReceipt.SessionId == SessionId).ToList();
            totalcount = query.Count;
            var FeeReceiptDetails = new PagedList<KSModel.Models.FeeReceiptDetail>(query, PageIndex, PageSize);
            return FeeReceiptDetails;
        }

        public void InsertFeeReceiptDetail(FeeReceiptDetail FeeReceiptDetail)
        {
            if (FeeReceiptDetail == null)
                throw new ArgumentNullException("FeeReceiptDetail");

            db.FeeReceiptDetails.Add(FeeReceiptDetail);
            db.SaveChanges();
        }

        public void UpdateFeeReceiptDetail(FeeReceiptDetail FeeReceiptDetail)
        {
            if (FeeReceiptDetail == null)
                throw new ArgumentNullException("FeeReceiptDetail");
            if (FeeReceiptDetail.WaivedOffAmount != null || FeeReceiptDetail.WaivedOffAmount != 0)
            {
                var feedetail = db.FeeReceiptDetails.Where(m => m.ReceiptId == FeeReceiptDetail.ReceiptId && m.GroupFeeHeadId == FeeReceiptDetail.GroupFeeHeadId && m.LateFeePeriodId == FeeReceiptDetail.LateFeePeriodId && m.PendingFeePeriodId == FeeReceiptDetail.PendingFeePeriodId).FirstOrDefault();
                if (feedetail != null)
                {
                    if (feedetail.PaidAmount > 0)
                        feedetail.PaidAmount = feedetail.PaidAmount - FeeReceiptDetail.WaivedOffAmount;
                    feedetail.WaivedOffAmount = FeeReceiptDetail.WaivedOffAmount;
                    db.Entry(feedetail).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            else
            {
                db.Entry(FeeReceiptDetail).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void DeleteFeeReceiptDetail(int FeeReceiptDetailId = 0)
        {
            if (FeeReceiptDetailId == 0)
                throw new ArgumentNullException("FeeReceiptDetail");

            var FeeReceiptDetail = (from s in db.FeeReceiptDetails where s.ReceiptDetailId == FeeReceiptDetailId select s).FirstOrDefault();
            db.FeeReceiptDetails.Remove(FeeReceiptDetail);
            db.SaveChanges();
        }

        public Session GetSessionStartDate()
        {
            var session = db.Sessions.OrderBy(m => m.StartDate).FirstOrDefault();
            return session;
        }
        #endregion

        #region FeePeriod

        public FeePeriod GetFeePeriodById(int FeePeriodId, bool IsTrack = false)
        {
            if (FeePeriodId == 0)
                throw new ArgumentNullException("FeePeriod");

            var FeePeriod = new FeePeriod();
            if (IsTrack)
                FeePeriod = (from s in db.FeePeriods where s.FeePeriodId == FeePeriodId select s).FirstOrDefault();
            else
                FeePeriod = (from s in db.FeePeriods.AsNoTracking() where s.FeePeriodId == FeePeriodId select s).FirstOrDefault();

            return FeePeriod;
        }

        public List<FeePeriod> GetAllFeePeriods(ref int totalcount, int? FeeFrequencyId = null, bool? Status = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeePeriods.ToList();
            if (FeeFrequencyId != null && FeeFrequencyId > 0)
                query = query.Where(e => e.FeeFrequencyId == FeeFrequencyId).ToList();
            if (Status != null)
            {
                if ((bool)Status)
                    query = query.Where(e => e.Status == Status).ToList();
            }

            totalcount = query.Count;
            var FeePeriods = new PagedList<KSModel.Models.FeePeriod>(query, PageIndex, PageSize);
            return FeePeriods;
        }

        public void InsertFeePeriod(FeePeriod FeePeriod)
        {
            if (FeePeriod == null)
                throw new ArgumentNullException("FeePeriod");

            db.FeePeriods.Add(FeePeriod);
            db.SaveChanges();
        }

        public void UpdateFeePeriod(FeePeriod FeePeriod)
        {
            if (FeePeriod == null)
                throw new ArgumentNullException("FeePeriod");

            db.Entry(FeePeriod).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeePeriod(int FeePeriodId = 0)
        {
            if (FeePeriodId == 0)
                throw new ArgumentNullException("FeePeriod");

            var FeePeriod = (from s in db.FeePeriods where s.FeePeriodId == FeePeriodId select s).FirstOrDefault();
            db.FeePeriods.Remove(FeePeriod);
            db.SaveChanges();
        }

        #endregion

        #region FeeReceiptDetailConc

        public FeeReceiptDetailConc GetFeeReceiptDetailConcById(int FeeReceiptDetailConcId, bool IsTrack = false)
        {
            if (FeeReceiptDetailConcId == 0)
                throw new ArgumentNullException("FeeReceiptDetailConc");

            var FeeReceiptDetailConc = new FeeReceiptDetailConc();
            if (IsTrack)
                FeeReceiptDetailConc = (from s in db.FeeReceiptDetailConcs where s.FeeReceiptDetailConcId == FeeReceiptDetailConcId select s).FirstOrDefault();
            else
                FeeReceiptDetailConc = (from s in db.FeeReceiptDetailConcs.AsNoTracking() where s.FeeReceiptDetailConcId == FeeReceiptDetailConcId select s).FirstOrDefault();

            return FeeReceiptDetailConc;
        }

        public List<FeeReceiptDetailConc> GetAllFeeReceiptDetailConcs(ref int totalcount, int? FeeFrequencyId = null, bool? Status = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeeReceiptDetailConcs.ToList();
            
            totalcount = query.Count;
            var FeeReceiptDetailConcs = new PagedList<KSModel.Models.FeeReceiptDetailConc>(query, PageIndex, PageSize);
            return FeeReceiptDetailConcs;
        }

        public void InsertFeeReceiptDetailConc(FeeReceiptDetailConc FeeReceiptDetailConc)
        {
            if (FeeReceiptDetailConc == null)
                throw new ArgumentNullException("FeeReceiptDetailConc");

            db.FeeReceiptDetailConcs.Add(FeeReceiptDetailConc);
            db.SaveChanges();
        }

        public void UpdateFeeReceiptDetailConc(FeeReceiptDetailConc FeeReceiptDetailConc)
        {
            if (FeeReceiptDetailConc == null)
                throw new ArgumentNullException("FeeReceiptDetailConc");

            db.Entry(FeeReceiptDetailConc).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeReceiptDetailConc(int FeeReceiptDetailConcId = 0)
        {
            if (FeeReceiptDetailConcId == 0)
                throw new ArgumentNullException("FeeReceiptDetailConc");

            var FeeReceiptDetailConc = (from s in db.FeeReceiptDetailConcs where s.FeeReceiptDetailConcId == FeeReceiptDetailConcId select s).FirstOrDefault();
            db.FeeReceiptDetailConcs.Remove(FeeReceiptDetailConc);
            db.SaveChanges();
        }

        #endregion

        #region FeePeriodDetail

        public FeePeriodDetail GetFeePeriodDetailById(int FeePeriodDetailId, bool IsTrack = false)
        {
            if (FeePeriodDetailId == 0)
                throw new ArgumentNullException("FeePeriodDetail");

            var FeePeriodDetail = new FeePeriodDetail();
            if (IsTrack)
                FeePeriodDetail = (from s in db.FeePeriodDetails where s.FeePeriodId == FeePeriodDetailId select s).FirstOrDefault();
            else
                FeePeriodDetail = (from s in db.FeePeriodDetails.AsNoTracking() where s.FeePeriodId == FeePeriodDetailId select s).FirstOrDefault();

            return FeePeriodDetail;
        }

        public List<FeePeriodDetail> GetAllFeePeriodDetails(ref int totalcount, int? FeePeriodId = null, DateTime? StartDate = null, DateTime? EndDate = null,
             int PageIndex = 0, int PageSize = int.MaxValue, int? SessionId = null)
        {
            var query = db.FeePeriodDetails.ToList();
            if (StartDate.HasValue)
                query = query.Where(e => e.PeriodStartDate >= StartDate).ToList();
            if (EndDate.HasValue)
                query = query.Where(e => e.PeriodEndDate >= EndDate).ToList();
            if (FeePeriodId != null && FeePeriodId > 0)
                query = query.Where(e => e.FeePeriodId == FeePeriodId).ToList();
            if (SessionId != null && SessionId > 0)
                query = query.Where(e => e.SessionId == SessionId).ToList();
            totalcount = query.Count;
            var FeePeriodDetails = new PagedList<KSModel.Models.FeePeriodDetail>(query, PageIndex, PageSize);
            return FeePeriodDetails;
        }

        public void InsertFeePeriodDetail(FeePeriodDetail FeePeriodDetail)
        {
            if (FeePeriodDetail == null)
                throw new ArgumentNullException("FeePeriodDetail");

            db.FeePeriodDetails.Add(FeePeriodDetail);
            db.SaveChanges();
        }

        public void UpdateFeePeriodDetail(FeePeriodDetail FeePeriodDetail)
        {
            if (FeePeriodDetail == null)
                throw new ArgumentNullException("FeePeriodDetail");

            db.Entry(FeePeriodDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeePeriodDetail(int FeePeriodDetailId = 0)
        {
            if (FeePeriodDetailId == 0)
                throw new ArgumentNullException("FeePeriodDetail");

            var FeePeriodDetail = (from s in db.FeePeriodDetails where s.FeePeriodDetailId == FeePeriodDetailId select s).FirstOrDefault();
            db.FeePeriodDetails.Remove(FeePeriodDetail);
            db.SaveChanges();
        }

        #endregion

        #region PaymentMode

        public PaymentMode GetPaymentModeById(int PaymentModeId, bool IsTrack = false)
        {
            if (PaymentModeId == 0)
                throw new ArgumentNullException("PaymentMode");

            var PaymentMode = new PaymentMode();
            if (IsTrack)
                PaymentMode = (from s in db.PaymentModes where s.PaymentModeId == PaymentModeId select s).FirstOrDefault();
            else
                PaymentMode = (from s in db.PaymentModes.AsNoTracking() where s.PaymentModeId == PaymentModeId select s).FirstOrDefault();

            return PaymentMode;
        }

        public List<PaymentMode> GetAllPaymentModes(ref int totalcount, string PaymentMode = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.PaymentModes.ToList();
            if (!string.IsNullOrEmpty(PaymentMode))
                query = query.Where(e => e.PaymentMode1.ToLower().Contains(PaymentMode.ToLower())).ToList();

            totalcount = query.Count;
            var PaymentModes = new PagedList<KSModel.Models.PaymentMode>(query, PageIndex, PageSize);
            return PaymentModes;
        }

        public void InsertPaymentMode(PaymentMode PaymentMode)
        {
            if (PaymentMode == null)
                throw new ArgumentNullException("PaymentMode");

            db.PaymentModes.Add(PaymentMode);
            db.SaveChanges();
        }

        public void UpdatePaymentMode(PaymentMode PaymentMode)
        {
            if (PaymentMode == null)
                throw new ArgumentNullException("PaymentMode");

            db.Entry(PaymentMode).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeletePaymentMode(int PaymentModeId = 0)
        {
            if (PaymentModeId == 0)
                throw new ArgumentNullException("PaymentMode");

            var PaymentMode = (from s in db.PaymentModes where s.PaymentModeId == PaymentModeId select s).FirstOrDefault();
            db.PaymentModes.Remove(PaymentMode);
            db.SaveChanges();
        }

        #endregion

        #region TrsType

        public TrsType GetTrsTypeById(int TrsTypeId, bool IsTrack = false)
        {
            if (TrsTypeId == 0)
                throw new ArgumentNullException("TrsType");

            var TrsType = new TrsType();
            if (IsTrack)
                TrsType = (from s in db.TrsTypes where s.TrsTypeId == TrsTypeId select s).FirstOrDefault();
            else
                TrsType = (from s in db.TrsTypes.AsNoTracking() where s.TrsTypeId == TrsTypeId select s).FirstOrDefault();

            return TrsType;
        }

        public List<TrsType> GetAllTrsTypes(ref int totalcount, string TrsType = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TrsTypes.ToList();
            if (!String.IsNullOrEmpty(TrsType))
                query = query.Where(e => e.TrsType1.ToLower().Contains(TrsType.ToLower())).ToList();

            totalcount = query.Count;
            var TrsTypes = new PagedList<KSModel.Models.TrsType>(query, PageIndex, PageSize);
            return TrsTypes;
        }

        public void InsertTrsType(TrsType TrsType)
        {
            if (TrsType == null)
                throw new ArgumentNullException("TrsType");

            db.TrsTypes.Add(TrsType);
            db.SaveChanges();
        }

        public void UpdateTrsType(TrsType TrsType)
        {
            if (TrsType == null)
                throw new ArgumentNullException("TrsType");

            db.Entry(TrsType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteTrsType(int TrsTypeId = 0)
        {
            if (TrsTypeId == 0)
                throw new ArgumentNullException("TrsType");

            var TrsType = (from s in db.TrsTypes where s.TrsTypeId == TrsTypeId select s).FirstOrDefault();
            db.TrsTypes.Remove(TrsType);
            db.SaveChanges();
        }

        #endregion

        #region Ledger

        public Ledger GetLedgerById(int EntryId, bool IsTrack = false)
        {
            if (EntryId == 0)
                throw new ArgumentNullException("Ledger");

            var Ledger = new Ledger();
            if (IsTrack)
                Ledger = (from s in db.Ledgers where s.EntryId == EntryId select s).FirstOrDefault();
            else
                Ledger = (from s in db.Ledgers.AsNoTracking() where s.EntryId == EntryId select s).FirstOrDefault();

            return Ledger;
        }

        public List<Ledger> GetAllLedgers(ref int totalcount, DateTime? EntryDate = null, int? AccId = null, int? TrsTypeId = null,
            decimal? Amount = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Ledgers.ToList();
            if (EntryDate.HasValue)
                query = query.Where(e => e.EntryDate == EntryDate).ToList();
            if (AccId != null && AccId > 0)
                query = query.Where(e => e.AccId == AccId).ToList();
            if (TrsTypeId != null && TrsTypeId > 0)
                query = query.Where(e => e.TrsTypeId == TrsTypeId).ToList();
            if (Amount != null && Amount > 0)
                query = query.Where(e => e.Amount == Amount).ToList();

            totalcount = query.Count;
            var Ledgers = new PagedList<KSModel.Models.Ledger>(query, PageIndex, PageSize);
            return Ledgers;
        }

        public void InsertLedger(Ledger Ledger)
        {
            if (Ledger == null)
                throw new ArgumentNullException("Ledger");

            db.Ledgers.Add(Ledger);
            db.SaveChanges();
        }

        public void UpdateLedger(Ledger Ledger)
        {
            if (Ledger == null)
                throw new ArgumentNullException("Ledger");

            db.Entry(Ledger).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteLedger(int EntryId = 0)
        {
            if (EntryId == 0)
                throw new ArgumentNullException("Ledger");

            var Ledger = (from s in db.Ledgers where s.EntryId == EntryId select s).FirstOrDefault();
            db.Ledgers.Remove(Ledger);
            db.SaveChanges();
        }

        #endregion

        #region Fee Defaulter List

        public IList<FeeDefaulterList> GetFeeDefaultersList(ref int count, string arrClassId="", int? ClassId = null,
            int? feeperiodId = null, bool IsStudentlist = false, int ReceiptTypeId = 0, DateTime? SessionStartDate = null, DateTime? SessionEnddate = null, string ShowEWS = "", string IsApplyTptOwnerShip = "1", string IsSeparateTptreceipt = "0", bool IsExportToExcel = false, decimal feeclassGrouptypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            DataTable dt = new DataTable();
            if (arrClassId == "0")
                arrClassId = "";
            //var classid = "";
            //if(arrClassId!=null)
            //    classid= string.Join(",", arrClassId);
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("fee_defaulterlist_Updated", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ClassId", arrClassId);
                    sqlComm.Parameters.AddWithValue("@SelectedFeePeriodId", feeperiodId);
                    sqlComm.Parameters.AddWithValue("@GetStudentsList", IsStudentlist);
                    sqlComm.Parameters.AddWithValue("@ReceiptTypeId", ReceiptTypeId);
                    sqlComm.Parameters.AddWithValue("@SessionStartDate", SessionStartDate);
                    sqlComm.Parameters.AddWithValue("@SessionendDate", SessionEnddate);
                    sqlComm.Parameters.AddWithValue("@ShowEWS", ShowEWS);
                    sqlComm.Parameters.AddWithValue("@IsApplyTptOwnerShip", IsApplyTptOwnerShip);
                    sqlComm.Parameters.AddWithValue("@IsSeparateTptFeeReceipt", IsSeparateTptreceipt);
                    sqlComm.Parameters.AddWithValue("@ExportToExcelAll", IsExportToExcel);
                    sqlComm.Parameters.AddWithValue("@feeclassGrouptypeid", feeclassGrouptypeId);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            IList<FeeDefaulterList> GetAllDefaultersList = new List<FeeDefaulterList>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                FeeDefaulterList Defaulterlist = new FeeDefaulterList();
                Defaulterlist.Class = dt.Rows[i]["Class"].ToString();
                Defaulterlist.Amount = Convert.ToDecimal(dt.Rows[i]["Amount"]);
                if (dt.Rows[i]["ClassId"] != null)
                    Defaulterlist.ClassId = Convert.ToInt32(dt.Rows[i]["ClassId"]);
                if (dt.Rows[i]["StudentName"] != null)
                    Defaulterlist.StudentName = dt.Rows[i]["StudentName"].ToString();
                if (dt.Rows[i]["RollNo"] != null)
                    Defaulterlist.RollNo = dt.Rows[i]["RollNo"].ToString();
                if (dt.Rows[i]["AdmissionNo"] != null)
                    Defaulterlist.AdmissionNo = !string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["AdmissionNo"])) ? dt.Rows[i]["AdmissionNo"].ToString() : "N/A";
                if (dt.Rows[i]["StudentId"] != null && dt.Rows[i]["StudentId"] != "")
                    Defaulterlist.StudentId = Convert.ToInt32(dt.Rows[i]["StudentId"].ToString());

                Defaulterlist.PayableAmount = Convert.ToDecimal(dt.Rows[i]["PayableAmount"]);
                if (dt.Rows[i]["Selectedfeeperiodid"] != null && dt.Rows[i]["Selectedfeeperiodid"] != "")
                    Defaulterlist.CurrentPeriodId = Convert.ToInt32(dt.Rows[i]["Selectedfeeperiodid"].ToString());
                GetAllDefaultersList.Add(Defaulterlist);
            }

            count = GetAllDefaultersList.Count;

            GetAllDefaultersList = new PagedList<FeeDefaulterList>(GetAllDefaultersList, PageIndex, PageSize);
            return GetAllDefaultersList;
        }

        public IList<FeeDefaulterList> GetFeeDefaultersListPreviousSession(ref int count, string arrClassId = "", int? ClassId = null,
           bool IsStudentlist = false, int ReceiptTypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            DataTable dt = new DataTable();

            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("fee_defaulterlist_PreviousSession", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ClassId", arrClassId);
                    sqlComm.Parameters.AddWithValue("@GetStudentsList", IsStudentlist);
                    sqlComm.Parameters.AddWithValue("@ReceiptTypeId", ReceiptTypeId);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            IList<FeeDefaulterList> GetAllDefaultersList = new List<FeeDefaulterList>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                FeeDefaulterList Defaulterlist = new FeeDefaulterList();
                Defaulterlist.Class = dt.Rows[i]["Class"].ToString();
                Defaulterlist.Amount = Convert.ToDecimal(dt.Rows[i]["Amount"]);
                if (dt.Rows[i]["ClassId"] != null)
                    Defaulterlist.ClassId = Convert.ToInt32(dt.Rows[i]["ClassId"]);
                if (dt.Rows[i]["StudentName"] != null)
                    Defaulterlist.StudentName = dt.Rows[i]["StudentName"].ToString();
                if (dt.Rows[i]["RollNo"] != null)
                    Defaulterlist.RollNo = dt.Rows[i]["RollNo"].ToString();
                if (dt.Rows[i]["AdmissionNo"] != null)
                    Defaulterlist.AdmissionNo = !string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["AdmissionNo"])) ? dt.Rows[i]["AdmissionNo"].ToString() : "N/A";
                if (dt.Rows[i]["StudentId"] != null)
                    Defaulterlist.StudentId = Convert.ToInt32(dt.Rows[i]["StudentId"].ToString());

                Defaulterlist.PayableAmount = Convert.ToDecimal(dt.Rows[i]["PayableAmount"]);

                GetAllDefaultersList.Add(Defaulterlist);
            }

            count = GetAllDefaultersList.Count;

            GetAllDefaultersList = new PagedList<FeeDefaulterList>(GetAllDefaultersList, PageIndex, PageSize);
            return GetAllDefaultersList;
        }

        #endregion

        #region ReceiptType

        public ReceiptType GetReceiptTypeById(int ReceiptTypeId, bool IsTrack = false)
        {
            if (ReceiptTypeId == 0)
                throw new ArgumentNullException("ReceiptType");

            var ReceiptType = new ReceiptType();
            if (IsTrack)
                ReceiptType = (from s in db.ReceiptTypes where s.ReceiptTypeId == ReceiptTypeId select s).FirstOrDefault();
            else
                ReceiptType = (from s in db.ReceiptTypes.AsNoTracking() where s.ReceiptTypeId == ReceiptTypeId select s).FirstOrDefault();

            return ReceiptType;
        }

        public List<ReceiptType> GetAllReceiptTypes(ref int totalcount, int? ReceiptTypeId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ReceiptTypes.Where(f => f.IsActive == true).ToList();
            if (ReceiptTypeId != null && ReceiptTypeId > 0)
            {
                query = query.Where(d => d.ReceiptTypeId == ReceiptTypeId).ToList();
            }
            totalcount = query.Count;
            var ReceiptTypes = new PagedList<KSModel.Models.ReceiptType>(query, PageIndex, PageSize);
            return ReceiptTypes;
        }

        public List<ReceiptType> GetAllReceiptTypes_Updated(ref int totalcount, int? ReceiptTypeId = null,
           int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ReceiptTypes.ToList();
            if (ReceiptTypeId != null && ReceiptTypeId > 0)
            {
                query = query.Where(d => d.ReceiptTypeId == ReceiptTypeId).ToList();
            }
            totalcount = query.Count;
            var ReceiptTypes = new PagedList<KSModel.Models.ReceiptType>(query, PageIndex, PageSize);
            return ReceiptTypes;
        }

        public List<ReceiptType> GetAllReceiptTypesWithAddons(ref int totalcount,
           int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ReceiptTypes.Where(f => f.IsActive == true && f.LinkStudentAddon == true).ToList();
            totalcount = query.Count;
            var ReceiptTypes = new PagedList<KSModel.Models.ReceiptType>(query, PageIndex, PageSize);
            return ReceiptTypes;
        }
        public List<ReceiptType> GetAllReceiptTypesgroupFeehead(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue, bool groupfeehead = true)
        {
            var query = db.ReceiptTypes.ToList();
            query = query.Where(m => m.AppearInGroupFeeHead == groupfeehead && m.IsActive == true).ToList();
            totalcount = query.Count;
            var ReceiptTypes = new PagedList<KSModel.Models.ReceiptType>(query, PageIndex, PageSize);
            return ReceiptTypes;
        }
        public void InsertReceiptType(ReceiptType ReceiptType)
        {
            if (ReceiptType == null)
                throw new ArgumentNullException("ReceiptType");

            db.ReceiptTypes.Add(ReceiptType);
            db.SaveChanges();
        }

        public void UpdateReceiptType(ReceiptType ReceiptType)
        {
            if (ReceiptType == null)
                throw new ArgumentNullException("ReceiptType");

            db.Entry(ReceiptType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteReceiptType(int ReceiptTypeId = 0)
        {
            if (ReceiptTypeId == 0)
                throw new ArgumentNullException("ReceiptType");

            var ReceiptType = (from s in db.ReceiptTypes where s.ReceiptTypeId == ReceiptTypeId select s).FirstOrDefault();
            db.ReceiptTypes.Remove(ReceiptType);
            db.SaveChanges();
        }
        public List<FeeHead> GetAllMiscHeads(ref int count)
        {
            var feeHead = db.FeeHeads.Where(f => f.FeeHeadType.FeeHeadType1 == "Miscellaneous" || f.FeeHeadType.FeeHeadType1 == "Misc" || f.FeeHeadType.FeeHeadType1 == "Misc.").ToList();
            return feeHead;
        }

        public void DeleteReceiptTypeGrid(int receiptTypeId)
        {
            if (receiptTypeId == 0)
                throw new ArgumentNullException("Fee");
            // Delete GroupfeeHead of Particular Record
            var GroupFeeHeadId = (from s in db.GroupFeeHeads
                                  where s.ReceiptTypeId == receiptTypeId
                                  select s).ToList();


            if (GroupFeeHeadId.Count > 0)
            {

                int[] Ids = GroupFeeHeadId.Select(f => f.GroupFeeHeadId).ToArray();

                //Delete the GroupfeeHeadDetail.
                var GFHDetailId = (from s in db.GroupFeeHeadDetails
                                   where Ids.Contains((int)s.GroupFeeHeadId)
                                   select s).ToList();
                //Delete the GroupfeeHeadConc.
                if (GFHDetailId.Count > 0)
                {
                    int[] GFHDetailIds = GFHDetailId.Select(f => f.GroupFeeHeadDetailId).ToArray();
                    var GruopFeeHeadConcId = (from s in db.GroupFeeHeadConcs
                                              where GFHDetailIds.Contains((int)s.GroupFeeHeadDetailId)
                                              select s).ToList();
                    if (GruopFeeHeadConcId.Count > 0)
                    {
                        db.GroupFeeHeadConcs.RemoveRange(GruopFeeHeadConcId);
                        db.SaveChanges();
                    }

                    db.GroupFeeHeadDetails.RemoveRange(GFHDetailId);
                    db.SaveChanges();
                }
                db.GroupFeeHeads.RemoveRange(GroupFeeHeadId);
                db.SaveChanges();
            }




            // Delete Consession
            var CId = (from s in db.Consessions
                       where s.ReceiptTypeId == receiptTypeId
                       select s).ToList();
            if (CId.Count > 0)
            {
                db.Consessions.RemoveRange(CId);
                db.SaveChanges();
            }

            // Delete ReceiptTypeFeeHead of Particular record
            var RTFH = (from s in db.ReceiptTypeFeeHeads
                        where s.ReceiptTypeId == receiptTypeId
                        select s).ToList();
            if (RTFH.Count > 0)
            {
                db.ReceiptTypeFeeHeads.RemoveRange(RTFH);
                db.SaveChanges();
            }




            // Delete Receipt Type
            var ReceiptType = (from s in db.ReceiptTypes
                               where s.ReceiptTypeId == receiptTypeId
                               select s).FirstOrDefault();
            if (ReceiptType != null)
            {
                db.ReceiptTypes.Remove(ReceiptType);
                db.SaveChanges();
            }
        }


        #endregion

        #region ReceiptTypeFeeHead

        public ReceiptTypeFeeHead GetReceiptTypeFeeHeadById(int ReceiptTypeFeeHeadId, bool IsTrack = false)
        {
            if (ReceiptTypeFeeHeadId == 0)
                throw new ArgumentNullException("ReceiptTypeFeeHead");

            var ReceiptTypeFeeHead = new ReceiptTypeFeeHead();
            if (IsTrack)
                ReceiptTypeFeeHead = (from s in db.ReceiptTypeFeeHeads where s.ReceiptTypeFeeHeadId == ReceiptTypeFeeHeadId select s).FirstOrDefault();
            else
                ReceiptTypeFeeHead = (from s in db.ReceiptTypeFeeHeads.AsNoTracking() where s.ReceiptTypeFeeHeadId == ReceiptTypeFeeHeadId select s).FirstOrDefault();

            return ReceiptTypeFeeHead;
        }

        public List<ReceiptTypeFeeHead> GetAllReceiptTypeFeeHeads(ref int totalcount, int ReceiptTypeId = 0, int FeeHeadId = 0, bool IsActive = false,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ReceiptTypeFeeHeads.ToList();
            if (ReceiptTypeId > 0)
            {
                query = query.Where(m => m.ReceiptTypeId == ReceiptTypeId).ToList();
            }
            if (FeeHeadId > 0)
            {
                query = query.Where(m => m.FeeHeadId == FeeHeadId).ToList();
            }
            if (IsActive)
            {
                query = query.Where(m => m.IsActive == IsActive).ToList();
            }

            totalcount = query.Count;
            var ReceiptTypeFeeHeads = new PagedList<KSModel.Models.ReceiptTypeFeeHead>(query, PageIndex, PageSize);
            return ReceiptTypeFeeHeads;
        }

        public void InsertReceiptTypeFeeHead(ReceiptTypeFeeHead ReceiptTypeFeeHead)
        {
            if (ReceiptTypeFeeHead == null)
                throw new ArgumentNullException("ReceiptTypeFeeHead");

            db.ReceiptTypeFeeHeads.Add(ReceiptTypeFeeHead);
            db.SaveChanges();
        }

        public void UpdateReceiptTypeFeeHead(ReceiptTypeFeeHead ReceiptTypeFeeHead)
        {
            if (ReceiptTypeFeeHead == null)
                throw new ArgumentNullException("ReceiptTypeFeeHead");

            db.Entry(ReceiptTypeFeeHead).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteReceiptTypeFeeHead(int ReceiptTypeFeeHeadId = 0)
        {
            if (ReceiptTypeFeeHeadId == 0)
                throw new ArgumentNullException("ReceiptTypeFeeHead");

            var ReceiptTypeFeeHead = (from s in db.ReceiptTypeFeeHeads where s.ReceiptTypeFeeHeadId == ReceiptTypeFeeHeadId select s).FirstOrDefault();
            db.ReceiptTypeFeeHeads.Remove(ReceiptTypeFeeHead);
            db.SaveChanges();
        }

        #endregion

        #region FeeCollectionList

        public IList<FeeCollectionListmodel> GetFeeCollectionList(ref int count, int? sessionid = null, int? FeeTypeId = null,
            int? ClassId = null, DateTime? fromdate = null, DateTime? todate = null, string PaymentModeIds = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            DataTable dt = new DataTable();

            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_GetFeeCollectionList", conn))
                {

                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ClassId", ClassId);
                    sqlComm.Parameters.AddWithValue("@FeeTypeId", FeeTypeId);
                    sqlComm.Parameters.AddWithValue("@SessionId", sessionid);
                    sqlComm.Parameters.AddWithValue("@PaymentModeids", PaymentModeIds);
                    if (fromdate.HasValue)
                        sqlComm.Parameters.AddWithValue("@PeriodStartDate", fromdate);
                    if (todate.HasValue)
                        sqlComm.Parameters.AddWithValue("@PeriodEndDate", todate);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            IList<FeeCollectionListmodel> GetAllFeeCollectionList = new List<FeeCollectionListmodel>();
            FeeCollectionListmodel FeeList = new FeeCollectionListmodel();


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                FeeList = new FeeCollectionListmodel();
                FeeList.ReceiptId = dt.Rows[i]["ReceiptId"] != null ? Convert.ToInt32(dt.Rows[i]["ReceiptId"]) : 0;
                FeeList.ReceiptNo = dt.Rows[i]["ReceiptNo"] != null ? Convert.ToInt32(dt.Rows[i]["ReceiptNo"]) : 0;
                FeeList.RollNo = dt.Rows[i]["RollNo"] != null ? Convert.ToString(dt.Rows[i]["RollNo"]) : "";
                FeeList.AdmissionNo = dt.Rows[i]["AdmissionNo"] != null ? dt.Rows[i]["AdmissionNo"].ToString() : "";
                FeeList.StudentName = dt.Rows[i]["StudentName"] != null ? dt.Rows[i]["StudentName"].ToString() : "";
                FeeList.DOJ = dt.Rows[i]["DOJ"] != null ? dt.Rows[i]["DOJ"].ToString() : "";
                FeeList.BankName = dt.Rows[i]["BankName"] != null ? dt.Rows[i]["BankName"].ToString() : "";
                FeeList.PaymentDate = dt.Rows[i]["PaymentDate"] != null ? dt.Rows[i]["PaymentDate"].ToString() : "";
                FeeList.IsHostler = dt.Rows[i]["IsHostler"] is DBNull ? false : Convert.ToBoolean((dt.Rows[i]["IsHostler"]));
                FeeList.RecieptType = dt.Rows[i]["RecieptType"] != null ? dt.Rows[i]["RecieptType"].ToString() : "";
                FeeList.Class = dt.Rows[i]["Class"] != null ? dt.Rows[i]["Class"].ToString() : "";
                var bankname = FeeList.BankName != "" ? " ( " + FeeList.BankName + ")" : "";
                FeeList.PaymentMethod = dt.Rows[i]["PaymentMethod"] != null ? dt.Rows[i]["PaymentMethod"].ToString()+ bankname : "";
                FeeList.ChequeNo = dt.Rows[i]["ChequeNo"] != null ? dt.Rows[i]["ChequeNo"].ToString() : "";
                FeeList.Details = dt.Rows[i]["Details"] != null ? dt.Rows[i]["Details"].ToString() : "";
                if (dt.Rows[i]["Amount"] == null)
                {
                    FeeList.Amount = 0;
                }
                else if (dt.Rows[i]["Amount"].ToString() == "")
                {
                    FeeList.Amount = 0;
                }
                else
                {
                    FeeList.Amount = Convert.ToDecimal(dt.Rows[i]["Amount"].ToString());
                }
                FeeList.IsOld = dt.Rows[i]["IsOld"] is DBNull ? false : Convert.ToBoolean((dt.Rows[i]["IsOld"]));
                GetAllFeeCollectionList.Add(FeeList);
            }

            count = GetAllFeeCollectionList.Count;

            GetAllFeeCollectionList = new PagedList<FeeCollectionListmodel>(GetAllFeeCollectionList, PageIndex, PageSize);
            return GetAllFeeCollectionList;
        }

        #endregion

        #endregion

        #region"Previous Fee Pending"


        public FeePrevPendingDetail GetFeePrevPendingById(int FeePrevPendingId, bool IsTrack = false)
        {
            if (FeePrevPendingId == 0)
                throw new ArgumentNullException("FeePrevPending");

            var FeePrevPending = new FeePrevPendingDetail();
            if (IsTrack)
                FeePrevPending = (from s in db.FeePrevPendingDetails where s.FeePrevPendingDetailId == FeePrevPendingId select s).FirstOrDefault();
            else
                FeePrevPending = (from s in db.FeePrevPendingDetails.AsNoTracking() where s.FeePrevPendingDetailId == FeePrevPendingId select s).FirstOrDefault();

            return FeePrevPending;
        }

        public List<FeePrevPendingDetail> GetAllFeePrevPendings(ref int totalcount, int SessionId = 0, int StudenId = 0, int ReceiptId = 0, int ClassId = 0, int ReceiptTypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeePrevPendingDetails.ToList();
            if (SessionId > 0)
                query = query.Where(m => m.FeePrevPending.SessionId == SessionId).ToList();
            if (StudenId > 0)
                query = query.Where(m => m.FeePrevPending.StudentId == StudenId).ToList();
            if (ReceiptTypeId > 0)
                query = query.Where(m => m.ReceiptTypeId == ReceiptTypeId).ToList();
            if (ClassId > 0)
                query = query.Where(m => m.FeePrevPending.ClassId == ClassId).ToList();

            totalcount = query.Count;
            var FeePrevPendings = new PagedList<KSModel.Models.FeePrevPendingDetail>(query, PageIndex, PageSize);
            return FeePrevPendings;
        }

        public List<FeePrevPendingReceipt> GetAllPreviousFeeReceipts(ref int totalcount, int FeePrevPendingDetailId = 0, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeePrevPendingReceipts.ToList();
            if (FeePrevPendingDetailId > 0)
                query = query.Where(m => m.FeePrevPendingDetailId == FeePrevPendingDetailId).ToList();

            totalcount = query.Count;
            var FeePrevPendingreceipts = new PagedList<KSModel.Models.FeePrevPendingReceipt>(query, PageIndex, PageSize);
            return FeePrevPendingreceipts;
        }

        public void InsertFeePrevPending(FeePrevPending FeePrevPending)
        {
            if (FeePrevPending == null)
                throw new ArgumentNullException("FeePrevPending");

            db.FeePrevPendings.Add(FeePrevPending);
            db.SaveChanges();
        }

        public void InsertFeePrevPendingDetail(FeePrevPendingDetail FeePrevPendingDetail)
        {
            if (FeePrevPendingDetail == null)
                throw new ArgumentNullException("FeePrevPendingDetail");

            db.FeePrevPendingDetails.Add(FeePrevPendingDetail);
            db.SaveChanges();
        }

        public void UpdateFeePrevPending(FeePrevPendingDetail FeePrevPending)
        {
            if (FeePrevPending == null)
                throw new ArgumentNullException("FeePrevPending");

            db.Entry(FeePrevPending).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeePrevPending(int FeePrevPendingId = 0)
        {
            if (FeePrevPendingId == 0)
                throw new ArgumentNullException("FeePrevPending");

            var FeePrevPending = (from s in db.FeePrevPendings where s.FeePrevPendingId == FeePrevPendingId select s).FirstOrDefault();
            db.FeePrevPendings.Remove(FeePrevPending);
            db.SaveChanges();
        }
        public void DeleteFeePrevPendingDetail(int FeePrevPendingDetailId = 0)
        {
            if (FeePrevPendingDetailId == 0)
                throw new ArgumentNullException("FeePrevPending");

            var FeePrevPendingDetail = (from s in db.FeePrevPendingDetails where s.FeePrevPendingDetailId == FeePrevPendingDetailId select s).FirstOrDefault();
            db.FeePrevPendingDetails.Remove(FeePrevPendingDetail);
            db.SaveChanges();
        }

        public void InsertFeePreviousPendingDetail(FeePrevPendingDetail FeePrevDetail)
        {
            if (FeePrevDetail == null)
                throw new ArgumentNullException("FeePrevPendingDetail");

            db.FeePrevPendingDetails.Add(FeePrevDetail);
            db.SaveChanges();
        }
        public void InsertFeePreviousReceipts(FeePrevPendingReceipt FeePrevPendingReceipt)
        {
            if (FeePrevPendingReceipt == null)
                throw new ArgumentNullException("FeePrevPendingReceipt");

            db.FeePrevPendingReceipts.Add(FeePrevPendingReceipt);
            db.SaveChanges();
        }
        #endregion

        #region FeePending

        public FeePending GetFeePendingById(int FeePendingId, bool IsTrack = false)
        {
            if (FeePendingId == 0)
                throw new ArgumentNullException("FeePending");

            var FeePending = new FeePending();
            if (IsTrack)
                FeePending = (from s in db.FeePendings where s.FeePendingId == FeePendingId select s).FirstOrDefault();
            else
                FeePending = (from s in db.FeePendings.AsNoTracking() where s.FeePendingId == FeePendingId select s).FirstOrDefault();

            return FeePending;
        }

        public List<FeePending> GetAllFeePendings(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeePendings.ToList();

            totalcount = query.Count;
            var FeePendings = new PagedList<KSModel.Models.FeePending>(query, PageIndex, PageSize);
            return FeePendings;
        }

        public void InsertFeePending(FeePending FeePending)
        {
            if (FeePending == null)
                throw new ArgumentNullException("FeePending");

            db.FeePendings.Add(FeePending);
            db.SaveChanges();
        }

        public void UpdateFeePending(FeePending FeePending)
        {
            if (FeePending == null)
                throw new ArgumentNullException("FeePending");

            db.Entry(FeePending).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeePending(int FeePendingId = 0)
        {
            if (FeePendingId == 0)
                throw new ArgumentNullException("FeePending");

            var FeePending = (from s in db.FeePendings where s.FeePendingId == FeePendingId select s).FirstOrDefault();
            db.FeePendings.Remove(FeePending);
            db.SaveChanges();
        }

        #endregion

        #region PGRequest

        public PGRequest GetPGRequestById(int PGRequestId, bool IsTrack = false)
        {
            if (PGRequestId == 0)
                throw new ArgumentNullException("PGRequest");

            var PGRequest = new PGRequest();
            if (IsTrack)
                PGRequest = (from s in db.PGRequests where s.PGRequestId == PGRequestId select s).FirstOrDefault();
            else
                PGRequest = (from s in db.PGRequests.AsNoTracking() where s.PGRequestId == PGRequestId select s).FirstOrDefault();

            return PGRequest;
        }

        public List<PGRequest> GetAllPGRequests(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.PGRequests.ToList();

            totalcount = query.Count;
            var PGRequest = new PagedList<KSModel.Models.PGRequest>(query, PageIndex, PageSize);
            return PGRequest;
        }

        public void InsertPGRequest(PGRequest PGRequest)
        {
            if (PGRequest == null)
                throw new ArgumentNullException("PGRequest");

            db.PGRequests.Add(PGRequest);
            db.SaveChanges();
        }

        public void UpdatePGRequest(PGRequest PGRequest)
        {
            if (PGRequest == null)
                throw new ArgumentNullException("PGRequest");

            db.Entry(PGRequest).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeletePGRequest(int PGRequestId = 0)
        {
            if (PGRequestId == 0)
                throw new ArgumentNullException("PGRequest");

            var PGRequest = (from s in db.PGRequests where s.PGRequestId == PGRequestId select s).FirstOrDefault();
            db.PGRequests.Remove(PGRequest);
            db.SaveChanges();
        }

        #endregion

        #region FeeReceiptOnline

        public FeeReceiptOnline GetFeeReceiptOnlineById(int FeeReceiptOnlineId, bool IsTrack = false)
        {
            if (FeeReceiptOnlineId == 0)
                throw new ArgumentNullException("FeeReceiptOnline");

            var FeeReceiptOnline = new FeeReceiptOnline();
            if (IsTrack)
                FeeReceiptOnline = (from s in db.FeeReceiptOnlines where s.FeeReceiptOnlineId == FeeReceiptOnlineId select s).FirstOrDefault();
            else
                FeeReceiptOnline = (from s in db.FeeReceiptOnlines.AsNoTracking() where s.FeeReceiptOnlineId == FeeReceiptOnlineId select s).FirstOrDefault();

            return FeeReceiptOnline;
        }

        public List<FeeReceiptOnline> GetAllFeeReceiptOnlines(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeeReceiptOnlines.ToList();

            totalcount = query.Count;
            var FeeReceiptOnline = new PagedList<KSModel.Models.FeeReceiptOnline>(query, PageIndex, PageSize);
            return FeeReceiptOnline;
        }

        public void InsertFeeReceiptOnline(FeeReceiptOnline FeeReceiptOnline)
        {
            if (FeeReceiptOnline == null)
                throw new ArgumentNullException("FeeReceiptOnline");

            db.FeeReceiptOnlines.Add(FeeReceiptOnline);
            db.SaveChanges();
        }

        public void UpdateFeeReceiptOnline(FeeReceiptOnline FeeReceiptOnline)
        {
            if (FeeReceiptOnline == null)
                throw new ArgumentNullException("FeeReceiptOnline");

            db.Entry(FeeReceiptOnline).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeReceiptOnline(int FeeReceiptOnlineId = 0)
        {
            if (FeeReceiptOnlineId == 0)
                throw new ArgumentNullException("FeeReceiptOnline");

            var FeeReceiptOnline = (from s in db.FeeReceiptOnlines where s.FeeReceiptOnlineId == FeeReceiptOnlineId select s).FirstOrDefault();
            db.FeeReceiptOnlines.Remove(FeeReceiptOnline);
            db.SaveChanges();
        }

        #endregion

        #region PGSetup

        public PGSetup GetPGSetupById(int PGSetupId, bool IsTrack = false)
        {
            if (PGSetupId == 0)
                throw new ArgumentNullException("PGSetup");

            var PGSetup = new PGSetup();
            if (IsTrack)
                PGSetup = (from s in db.PGSetups where s.PGId == PGSetupId select s).FirstOrDefault();
            else
                PGSetup = (from s in db.PGSetups.AsNoTracking() where s.PGId == PGSetupId select s).FirstOrDefault();

            return PGSetup;
        }

        public List<PGSetup> GetAllPGSetups(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.PGSetups.ToList();

            totalcount = query.Count;
            var PGSetups = new PagedList<KSModel.Models.PGSetup>(query, PageIndex, PageSize);
            return PGSetups;
        }

        public void InsertPGSetup(PGSetup PGSetup)
        {
            if (PGSetup == null)
                throw new ArgumentNullException("PGSetup");

            db.PGSetups.Add(PGSetup);
            db.SaveChanges();
        }

        public void UpdatePGSetup(PGSetup PGSetup)
        {
            if (PGSetup == null)
                throw new ArgumentNullException("PGSetup");

            db.Entry(PGSetup).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeletePGSetup(int PGSetupId = 0)
        {
            if (PGSetupId == 0)
                throw new ArgumentNullException("PGSetup");

            var PGSetup = (from s in db.PGSetups where s.PGId == PGSetupId select s).FirstOrDefault();
            db.PGSetups.Remove(PGSetup);
            db.SaveChanges();
        }

        #endregion

        #region FeePendingReceipt

        public FeePendingReceipt GetFeePendingReceiptById(int FeePendingReceiptId, bool IsTrack = false)
        {
            if (FeePendingReceiptId == 0)
                throw new ArgumentNullException("FeePendingReceipt");

            var FeePendingReceipt = new FeePendingReceipt();
            if (IsTrack)
                FeePendingReceipt = (from s in db.FeePendingReceipts where s.FeePendingReceiptId == FeePendingReceiptId select s).FirstOrDefault();
            else
                FeePendingReceipt = (from s in db.FeePendingReceipts.AsNoTracking() where s.FeePendingReceiptId == FeePendingReceiptId select s).FirstOrDefault();

            return FeePendingReceipt;
        }

        public List<FeePendingReceipt> GetAllFeePendingReceipts(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeePendingReceipts.ToList();

            totalcount = query.Count;
            var FeePendingReceipts = new PagedList<KSModel.Models.FeePendingReceipt>(query, PageIndex, PageSize);
            return FeePendingReceipts;
        }

        public void InsertFeePendingReceipt(FeePendingReceipt FeePendingReceipt)
        {
            if (FeePendingReceipt == null)
                throw new ArgumentNullException("FeePendingReceipt");

            db.FeePendingReceipts.Add(FeePendingReceipt);
            db.SaveChanges();
        }

        public void UpdateFeePendingReceipt(FeePendingReceipt FeePendingReceipt)
        {
            if (FeePendingReceipt == null)
                throw new ArgumentNullException("FeePendingReceipt");

            db.Entry(FeePendingReceipt).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeePendingReceipt(int FeePendingReceiptId = 0)
        {
            if (FeePendingReceiptId == 0)
                throw new ArgumentNullException("FeePendingReceipt");

            var FeePendingReceipt = (from s in db.FeePendingReceipts where s.FeePendingReceiptId == FeePendingReceiptId select s).FirstOrDefault();
            if (FeePendingReceipt != null)
            {
                db.FeePendingReceipts.Remove(FeePendingReceipt);
                db.SaveChanges();
            }
        }

        #endregion

        #region"Transport pendancy Report"
        public BoardingPoint GetBoardingPointDetailbyId(int BoadringPointId)
        {
            var query = db.BoardingPoints.Where(f => f.BoardingPointId == BoadringPointId).FirstOrDefault();
            return query;
        }

        public IList<TransportFeeInfolist> GetTransportFeePendancy(ref int count, int RouteId = 0, DateTime? FromDate = null, DateTime? ToDate = null)
        {
            IList<TransportFeeInfolist> list = new List<TransportFeeInfolist>();

            var query = db.BusRoutes.ToList();
            if (RouteId > 0)
            {
                TransportFeeInfolist info = new TransportFeeInfolist();
                decimal Payableamt = 0;
                decimal receivedamt = 0;
                int groupfeeheadid = 0;
                var route = query.Where(f => f.BusRouteId == RouteId).FirstOrDefault();
                if (route != null)
                {
                    info.RouteId = RouteId;
                    info.Route = route.BusRoute1;
                }
                var tptroutes = db.TptFacilities.Where(f => (f.BusRouteId == RouteId || f.DropRouteId == RouteId) && f.VehicleTypeId == null && f.EffectiveDate != null && (((f.EffectiveDate != null && f.EffectiveDate <= FromDate) && (f.EndDate == null || (f.EndDate != null && f.EndDate >= FromDate))) || f.EffectiveDate != null && f.EffectiveDate >= FromDate) && ((f.EndDate != null && f.EndDate <= ToDate) || f.EndDate == null)).ToList();
                decimal tptFeeHeadamt = 0;
                var fromyear = FromDate.Value.Year;
                var toyear = ToDate.Value.Year;
                //string year = fromyear + "-" + toyear;
                // var session = db.Sessions.Where(m => m.Session1 == year).FirstOrDefault();
                var session = db.Sessions.Where(f => f.EndDate >= ToDate).FirstOrDefault();
                int sessionid = 0;
                if (session != null)
                    sessionid = (int)session.SessionId;
                if (tptroutes != null)
                {
                    foreach (var item in tptroutes)
                    {
                        int standardid = 0;
                        var standard = db.StudentClasses.Where(f => f.SessionId == sessionid && f.StudentId == item.StudentId).FirstOrDefault();
                        if (standard != null)
                        {
                            standardid = (int)standard.ClassMaster.StandardId;

                            var feeclassgroup = db.FeeClassGroupDetails.Where(f => f.StandardId == standardid).FirstOrDefault();
                            if (feeclassgroup != null)
                            {
                                var groupfeetype = db.GroupFeeTypes.Where(f => f.FeeClassGroupId == feeclassgroup.FeeClassGroupId).FirstOrDefault();
                                if (groupfeetype != null)
                                {
                                    var groupfeehead = db.GroupFeeHeads.Where(f => f.GroupFeeTypeId == groupfeetype.GroupFeeTypeId && f.FeeHead.FeeHead1.Contains("Transport") && (f.ReceiptTypeFeeHead.ReceiptType.IsForPreviousSession == null || f.ReceiptTypeFeeHead.ReceiptType.IsForPreviousSession == false)).FirstOrDefault();
                                    if (groupfeehead != null)
                                    {
                                        groupfeeheadid = (int)groupfeehead.GroupFeeHeadId;
                                        // var groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(f => ((f.EffectiveDate<=FromDate &&(f.EndDate==null || (f.EndDate!=null && f.EndDate>=FromDate))) || f.EffectiveDate >= FromDate) && ((f.EndDate != null && f.EndDate <= ToDate) || f.EndDate == null)).FirstOrDefault();
                                        var groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(f => f.EffectiveDate >= FromDate && ((f.EndDate != null && f.EndDate <= ToDate) || f.EndDate == null)).FirstOrDefault();
                                        if (groupfeeheaddetail == null)
                                        {
                                            groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(t => t.EffectiveDate <= FromDate &&
                                                    (t.EndDate == null)).FirstOrDefault();
                                            if (groupfeeheaddetail == null)
                                            {
                                                groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(t => (t.EffectiveDate <= FromDate || t.EffectiveDate <= ToDate) &&
                                                   (t.EndDate == null)).FirstOrDefault();
                                            }
                                            if (groupfeeheaddetail == null)
                                            {
                                                groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(t => (t.EffectiveDate <= FromDate || t.EffectiveDate <= ToDate) &&
                                                   (t.EndDate != null && t.EndDate >= FromDate)).FirstOrDefault();
                                            }
                                        }
                                        if (groupfeeheaddetail != null)
                                        {
                                            //decimal fee = feeCalculation((int)groupfeetype.FeeType.FeeFrequencyId, (decimal)groupfeeheaddetail.Amount, FromDate, ToDate,item.EffectiveDate);
                                            //tptFeeHeadamt = fee;
                                            tptFeeHeadamt = (decimal)groupfeeheaddetail.Amount;
                                        }
                                        if (item.BoardingPointId != null)
                                        {
                                            var boardingfee = db.BoardingPointFees.Where(f => f.BoardingPointId == item.BoardingPointId && f.EfectiveDate >= FromDate && (f.EndDate == null || (f.EndDate != null && f.EndDate <= ToDate))).FirstOrDefault();
                                            if (boardingfee == null)
                                            {
                                                boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.BoardingPointId && t.EfectiveDate <= FromDate &&
                                                        (t.EndDate == null)).FirstOrDefault();
                                                if (boardingfee == null)
                                                {
                                                    boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.BoardingPointId && (t.EfectiveDate <= FromDate || t.EfectiveDate <= ToDate) &&
                                                       (t.EndDate == null)).FirstOrDefault();
                                                }
                                                if (boardingfee == null)
                                                {
                                                    boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.BoardingPointId && (t.EfectiveDate <= FromDate || t.EfectiveDate <= ToDate) &&
                                                       (t.EndDate != null && t.EndDate >= FromDate)).FirstOrDefault();
                                                }
                                            }
                                            if (boardingfee != null)
                                            {
                                                if (item.TptTypeDetail.TptName == "Pick Drop")
                                                {
                                                    if (boardingfee.TransportFee_2 == null || boardingfee.TransportFee_2 == 0)
                                                    {
                                                        decimal fee = feeCalculation((int)groupfeetype.FeeType.FeeFrequencyId, (decimal)tptFeeHeadamt, FromDate, ToDate, item.EffectiveDate);
                                                        Payableamt += (decimal)fee;
                                                    }
                                                    else
                                                    {
                                                        decimal fee = feeCalculation((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_2, FromDate, ToDate, item.EffectiveDate);
                                                        Payableamt += fee;
                                                    }
                                                }
                                                else if (item.TptTypeDetail.TptName == "Pick" || item.TptTypeDetail.TptName == "Drop")
                                                {
                                                    if (boardingfee.TransportFee_1 == null || boardingfee.TransportFee_1 == 0)
                                                    {
                                                        decimal fee = feeCalculation((int)groupfeetype.FeeType.FeeFrequencyId, (decimal)tptFeeHeadamt, FromDate, ToDate, item.EffectiveDate);
                                                        Payableamt += (decimal)fee;
                                                    }
                                                    else
                                                    {
                                                        decimal fee = feeCalculation((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_1, FromDate, ToDate, item.EffectiveDate);
                                                        Payableamt += fee;
                                                    }
                                                }
                                            }
                                        }
                                        else if (item.DropPointId != null)
                                        {
                                            var boardingfee = db.BoardingPointFees.Where(f => f.BoardingPointId == item.DropPointId && f.EfectiveDate >= FromDate && (f.EndDate == null || (f.EndDate != null && f.EndDate <= ToDate))).FirstOrDefault();
                                            if (boardingfee == null)
                                            {
                                                boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.DropPointId && t.EfectiveDate <= FromDate &&
                                                        (t.EndDate == null)).FirstOrDefault();
                                                if (boardingfee == null)
                                                {
                                                    boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.DropPointId && (t.EfectiveDate <= FromDate || t.EfectiveDate <= ToDate) &&
                                                       (t.EndDate == null)).FirstOrDefault();
                                                }
                                                if (boardingfee == null)
                                                {
                                                    boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.DropPointId && (t.EfectiveDate <= FromDate || t.EfectiveDate <= ToDate) &&
                                                       (t.EndDate != null && t.EndDate >= FromDate)).FirstOrDefault();
                                                }
                                            }
                                            if (boardingfee != null)
                                            {
                                                if (item.TptTypeDetail.TptName == "Pick Drop")
                                                {
                                                    if (boardingfee.TransportFee_2 == null || boardingfee.TransportFee_2 == 0)
                                                    {
                                                        decimal fee = feeCalculation((int)groupfeetype.FeeType.FeeFrequencyId, (decimal)tptFeeHeadamt, FromDate, ToDate, item.EffectiveDate);
                                                        Payableamt += (decimal)fee;
                                                    }
                                                    else
                                                    {
                                                        decimal fee = feeCalculation((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_2, FromDate, ToDate, item.EffectiveDate);
                                                        Payableamt += fee;
                                                    }
                                                }
                                                else if (item.TptTypeDetail.TptName == "Pick" || item.TptTypeDetail.TptName == "Drop")
                                                {
                                                    if (boardingfee.TransportFee_1 == null || boardingfee.TransportFee_1 == 0)
                                                    {
                                                        decimal fee = feeCalculation((int)groupfeetype.FeeType.FeeFrequencyId, (decimal)tptFeeHeadamt, FromDate, ToDate, item.EffectiveDate);
                                                        Payableamt += (decimal)fee;
                                                    }
                                                    else
                                                    {
                                                        decimal fee = feeCalculation((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_1, FromDate, ToDate, item.EffectiveDate);
                                                        Payableamt += fee;
                                                    }
                                                }
                                            }

                                        }

                                        var feeperiodid = db.FeePeriodDetails.Where(f => f.PeriodStartDate >= FromDate && f.PeriodEndDate <= ToDate && f.FeePeriod.FeeFrequencyId == groupfeetype.FeeType.FeeFrequencyId).OrderBy(f => f.FeePeriodId).ToList();
                                        if (feeperiodid != null && feeperiodid.Count > 0)
                                        {
                                            var Feeprdids = feeperiodid.Select(f => f.FeePeriodId).ToList();
                                            var feeperiods = db.FeePeriods.Where(f => Feeprdids.Contains(f.FeePeriodId)).ToList();
                                            int Firstfeeperiodindex = 1;
                                            int Lastfeeperiodindex = 0;

                                            Firstfeeperiodindex = (int)feeperiodid.FirstOrDefault().FeePeriod.FeePeriodIndex;
                                            Lastfeeperiodindex = (int)feeperiodid.OrderByDescending(f => f.FeePeriodId).FirstOrDefault().FeePeriod.FeePeriodIndex;
                                            for (int i = Firstfeeperiodindex; i <= Lastfeeperiodindex; i++)
                                            {
                                                int studentid = (int)item.StudentId;
                                                var feeperiodbyloop = feeperiods.Where(f => f.FeePeriodIndex == i).FirstOrDefault();
                                                // check if Fee period already exist in fee period detail
                                                var isfeeperiodexist = GetAllFeeReceiptDetailsFeePeriod(ref count, StudentId: studentid, FeePeriodId: feeperiodbyloop.FeePeriodId, ReceiptTypeId: groupfeehead.ReceiptTypeId, SessionId: sessionid);
                                                if (isfeeperiodexist.Count > 0)
                                                {
                                                    var feereceipts = db.FeeReceipts.Where(f => f.SessionId == sessionid && f.StudentId == studentid && f.FeePeriodId == feeperiodbyloop.FeePeriodId).ToList();
                                                    if (feereceipts != null)
                                                    {
                                                        foreach (var fee in feereceipts)
                                                        {
                                                            var feedetail = fee.FeeReceiptDetails.Where(f => f.ReceiptId == fee.ReceiptId);
                                                            var amt = feedetail.Where(f => f.GroupFeeHeadId == groupfeeheadid).Select(f => f.Amount).Sum();
                                                            var waivedamt = feedetail.Where(f => f.GroupFeeHeadId == groupfeeheadid).Select(f => f.WaivedOffAmount).Sum();
                                                            receivedamt += (decimal)amt;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var Feedetail = db.FeeReceiptDetails.Where(f => f.PendingFeePeriodId == feeperiodbyloop.FeePeriodId && f.FeeReceipt.SessionId == sessionid && f.FeeReceipt.StudentId == item.StudentId).FirstOrDefault();
                                                        if (Feedetail != null)
                                                        {
                                                            receivedamt += (decimal)Feedetail.Amount;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    var Feedetail = db.FeeReceiptDetails.Where(f => f.PendingFeePeriodId == feeperiodbyloop.FeePeriodId && f.FeeReceipt.SessionId == sessionid && f.FeeReceipt.StudentId == item.StudentId).FirstOrDefault();
                                                    if (Feedetail != null)
                                                    {
                                                        receivedamt += (decimal)Feedetail.Amount;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (Payableamt > 0)
                {
                    info.Payable = Math.Round((decimal)(Payableamt), 2);
                    info.Received = Math.Round((decimal)(receivedamt), 2);
                    info.Pending = Math.Round((decimal)(info.Payable - info.Received), 2);
                    list.Add(info);
                }
            }
            else
            {
                var busroutes = db.BusRoutes.ToList();
                if (busroutes != null && busroutes.Count > 0)
                {
                    foreach (var busroute in busroutes)
                    {
                        TransportFeeInfolist info = new TransportFeeInfolist();
                        decimal Payableamt = 0;
                        decimal receivedamt = 0;
                        int groupfeeheadid = 0;
                        var route = query.Where(f => f.BusRouteId == busroute.BusRouteId).FirstOrDefault();
                        if (route != null)
                        {
                            info.RouteId = busroute.BusRouteId;
                            info.Route = route.BusRoute1;
                        }
                        var tptroutes = db.TptFacilities.Where(f => (f.BusRouteId == busroute.BusRouteId || f.DropRouteId == busroute.BusRouteId) && f.VehicleTypeId == null && f.EffectiveDate != null && ((f.EffectiveDate <= FromDate && (f.EndDate == null || (f.EndDate != null && f.EndDate >= FromDate))) || f.EffectiveDate >= FromDate) && ((f.EndDate != null && f.EndDate <= ToDate) || f.EndDate == null)).ToList();
                        decimal tptFeeHeadamt = 0;
                        var fromyear = FromDate.Value.Year;
                        var toyear = ToDate.Value.Year;
                        var session = db.Sessions.Where(f => f.EndDate >= ToDate).FirstOrDefault();
                        //if(fromyear==toyear)
                        //{
                        //    toyear = toyear + 1;
                        //}
                        //string year = fromyear + "-" + toyear;

                        //var session = db.Sessions.Where(m => m.Session1 == year).FirstOrDefault();
                        int sessionid = 0;
                        if (session != null)
                            sessionid = (int)session.SessionId;
                        if (tptroutes != null)
                        {
                            foreach (var item in tptroutes)
                            {
                                int standardid = 0;
                                var standard = db.StudentClasses.Where(f => f.SessionId == sessionid && f.StudentId == item.StudentId).FirstOrDefault();
                                if (standard != null)
                                {
                                    standardid = (int)standard.ClassMaster.StandardId;

                                    var feeclassgroup = db.FeeClassGroupDetails.Where(f => f.StandardId == standardid).FirstOrDefault();
                                    if (feeclassgroup != null)
                                    {
                                        var groupfeetype = db.GroupFeeTypes.Where(f => f.FeeClassGroupId == feeclassgroup.FeeClassGroupId).FirstOrDefault();
                                        if (groupfeetype != null)
                                        {
                                            var groupfeehead = db.GroupFeeHeads.Where(f => f.GroupFeeTypeId == groupfeetype.GroupFeeTypeId && f.FeeHead.FeeHead1.Contains("Transport") && (f.ReceiptTypeFeeHead.ReceiptType.IsForPreviousSession == null || f.ReceiptTypeFeeHead.ReceiptType.IsForPreviousSession == false)).FirstOrDefault();
                                            if (groupfeehead != null)
                                            {
                                                groupfeeheadid = (int)groupfeehead.GroupFeeHeadId;
                                                //var groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(f => ((f.EffectiveDate<=FromDate &&(f.EndDate==null || (f.EndDate!=null && f.EndDate>=FromDate))) || f.EffectiveDate >= FromDate) && ((f.EndDate != null && f.EndDate <= ToDate) || f.EndDate == null)).FirstOrDefault();
                                                var groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(f => f.EffectiveDate >= FromDate && ((f.EndDate != null && f.EndDate <= ToDate) || f.EndDate == null)).FirstOrDefault();
                                                if (groupfeeheaddetail == null)
                                                {
                                                    groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(t => t.EffectiveDate <= FromDate &&
                                                            (t.EndDate == null)).FirstOrDefault();
                                                    if (groupfeeheaddetail == null)
                                                    {
                                                        groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(t => (t.EffectiveDate <= FromDate || t.EffectiveDate <= ToDate) &&
                                                           (t.EndDate == null)).FirstOrDefault();
                                                    }
                                                    if (groupfeeheaddetail == null)
                                                    {
                                                        groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(t => (t.EffectiveDate <= FromDate || t.EffectiveDate <= ToDate) &&
                                                           (t.EndDate != null && t.EndDate >= FromDate)).FirstOrDefault();
                                                    }
                                                }
                                                if (groupfeeheaddetail != null)
                                                {
                                                    decimal fee = feeCalculation((int)groupfeetype.FeeType.FeeFrequencyId, (decimal)groupfeeheaddetail.Amount, FromDate, ToDate, item.EffectiveDate);
                                                    tptFeeHeadamt = fee;
                                                    // tptFeeHeadamt = (decimal)groupfeeheaddetail.Amount;
                                                }
                                                if (item.BoardingPointId != null)
                                                {
                                                    var boardingfee = db.BoardingPointFees.Where(f => f.BoardingPointId == item.BoardingPointId && f.EfectiveDate >= FromDate && (f.EndDate == null || (f.EndDate != null && f.EndDate <= ToDate))).FirstOrDefault();
                                                    if (boardingfee == null)
                                                    {
                                                        boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.BoardingPointId && t.EfectiveDate <= FromDate &&
                                                                (t.EndDate == null)).FirstOrDefault();
                                                        if (boardingfee == null)
                                                        {
                                                            boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.BoardingPointId && (t.EfectiveDate <= FromDate || t.EfectiveDate <= ToDate) &&
                                                               (t.EndDate == null)).FirstOrDefault();
                                                        }
                                                        if (boardingfee == null)
                                                        {
                                                            boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.BoardingPointId && (t.EfectiveDate <= FromDate || t.EfectiveDate <= ToDate) &&
                                                               (t.EndDate != null && t.EndDate >= FromDate)).FirstOrDefault();
                                                        }
                                                    }
                                                    if (boardingfee != null)
                                                    {
                                                        if (item.TptTypeDetail.TptName == "Pick Drop")
                                                        {
                                                            if (boardingfee.TransportFee_2 == null || boardingfee.TransportFee_2 == 0)
                                                            {
                                                                Payableamt += (decimal)tptFeeHeadamt;
                                                            }
                                                            else
                                                            {
                                                                decimal fee = feeCalculation((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_2, FromDate, ToDate, item.EffectiveDate);
                                                                Payableamt += fee;
                                                            }
                                                        }
                                                        else if (item.TptTypeDetail.TptName == "Pick" || item.TptTypeDetail.TptName == "Drop")
                                                        {
                                                            if (boardingfee.TransportFee_1 == null || boardingfee.TransportFee_1 == 0)
                                                            {
                                                                Payableamt += (decimal)tptFeeHeadamt;
                                                            }
                                                            else
                                                            {
                                                                decimal fee = feeCalculation((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_1, FromDate, ToDate, item.EffectiveDate);
                                                                Payableamt += fee;
                                                            }
                                                        }
                                                    }
                                                }
                                                else if (item.DropPointId != null)
                                                {
                                                    var boardingfee = db.BoardingPointFees.Where(f => f.BoardingPointId == item.DropPointId && f.EfectiveDate >= FromDate && (f.EndDate == null || (f.EndDate != null && f.EndDate <= ToDate))).FirstOrDefault();
                                                    if (boardingfee == null)
                                                    {
                                                        boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.DropPointId && t.EfectiveDate <= FromDate &&
                                                                (t.EndDate == null)).FirstOrDefault();
                                                        if (boardingfee == null)
                                                        {
                                                            boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.DropPointId && (t.EfectiveDate <= FromDate || t.EfectiveDate <= ToDate) &&
                                                               (t.EndDate == null)).FirstOrDefault();
                                                        }
                                                        if (boardingfee == null)
                                                        {
                                                            boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.DropPointId && (t.EfectiveDate <= FromDate || t.EfectiveDate <= ToDate) &&
                                                               (t.EndDate != null && t.EndDate >= FromDate)).FirstOrDefault();
                                                        }
                                                    }
                                                    if (boardingfee != null)
                                                    {
                                                        if (item.TptTypeDetail.TptName == "Pick Drop")
                                                        {
                                                            if (boardingfee.TransportFee_2 == null || boardingfee.TransportFee_2 == 0)
                                                            {
                                                                Payableamt += (decimal)tptFeeHeadamt;
                                                            }
                                                            else
                                                            {
                                                                decimal fee = feeCalculation((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_2, FromDate, ToDate, item.EffectiveDate);
                                                                Payableamt += fee;
                                                            }
                                                        }
                                                        else if (item.TptTypeDetail.TptName == "Pick" || item.TptTypeDetail.TptName == "Drop")
                                                        {
                                                            if (boardingfee.TransportFee_1 == null || boardingfee.TransportFee_1 == 0)
                                                            {
                                                                Payableamt += (decimal)tptFeeHeadamt;
                                                            }
                                                            else
                                                            {
                                                                decimal fee = feeCalculation((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_1, FromDate, ToDate, item.EffectiveDate);
                                                                Payableamt += fee;
                                                            }
                                                        }
                                                    }

                                                }

                                                var feeperiodid = db.FeePeriodDetails.Where(f => f.PeriodStartDate >= FromDate && f.PeriodEndDate <= ToDate && f.FeePeriod.FeeFrequencyId == groupfeetype.FeeType.FeeFrequencyId && f.SessionId == sessionid).OrderBy(f => f.FeePeriodId).ToList();
                                                if (feeperiodid != null && feeperiodid.Count > 0)
                                                {
                                                    var Feeprdids = feeperiodid.Select(f => f.FeePeriodId).ToList();
                                                    var feeperiods = db.FeePeriods.Where(f => Feeprdids.Contains(f.FeePeriodId)).ToList();
                                                    int Firstfeeperiodindex = 1;
                                                    int Lastfeeperiodindex = 0;

                                                    Firstfeeperiodindex = (int)feeperiodid.FirstOrDefault().FeePeriod.FeePeriodIndex;
                                                    Lastfeeperiodindex = (int)feeperiodid.OrderByDescending(f => f.FeePeriodId).FirstOrDefault().FeePeriod.FeePeriodIndex;
                                                    for (int i = Firstfeeperiodindex; i <= Lastfeeperiodindex; i++)
                                                    {
                                                        int studentid = (int)item.StudentId;
                                                        var feeperiodbyloop = feeperiods.Where(f => f.FeePeriodIndex == i).FirstOrDefault();
                                                        // check if Fee period already exist in fee period detail
                                                        var isfeeperiodexist = GetAllFeeReceiptDetailsFeePeriod(ref count, StudentId: studentid, FeePeriodId: feeperiodbyloop.FeePeriodId, ReceiptTypeId: groupfeehead.ReceiptTypeId, SessionId: sessionid);
                                                        if (isfeeperiodexist.Count > 0)
                                                        {
                                                            var feereceipts = db.FeeReceipts.Where(f => f.SessionId == sessionid && f.StudentId == studentid && f.FeePeriodId == feeperiodbyloop.FeePeriodId).ToList();
                                                            if (feereceipts != null)
                                                            {
                                                                foreach (var fee in feereceipts)
                                                                {
                                                                    var feedetail = fee.FeeReceiptDetails.Where(f => f.ReceiptId == fee.ReceiptId);
                                                                    var amt = feedetail.Where(f => f.GroupFeeHeadId == groupfeeheadid).Select(f => f.Amount).Sum();
                                                                    var waivedamt = feedetail.Where(f => f.GroupFeeHeadId == groupfeeheadid).Select(f => f.WaivedOffAmount).Sum();
                                                                    receivedamt += (decimal)amt;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                var Feedetail = db.FeeReceiptDetails.Where(f => f.PendingFeePeriodId == feeperiodbyloop.FeePeriodId && f.FeeReceipt.SessionId == sessionid && f.FeeReceipt.StudentId == item.StudentId).FirstOrDefault();
                                                                if (Feedetail != null)
                                                                {
                                                                    receivedamt += (decimal)Feedetail.Amount;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            var Feedetail = db.FeeReceiptDetails.Where(f => f.PendingFeePeriodId == feeperiodbyloop.FeePeriodId && f.FeeReceipt.SessionId == sessionid && f.FeeReceipt.StudentId == item.StudentId).FirstOrDefault();
                                                            if (Feedetail != null)
                                                            {
                                                                receivedamt += (decimal)Feedetail.Amount;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (Payableamt > 0)
                        {
                            info.Payable = Math.Round((decimal)(Payableamt), 2);
                            info.Received = Math.Round((decimal)(receivedamt), 2);
                            info.Pending = Math.Round((decimal)(info.Payable - info.Received), 2);
                            list.Add(info);
                        }
                    }
                }
            }


            return list;
        }

        public decimal feeCalculation(int Frequencyid = 0, decimal Fee = 0, DateTime? Fromdate = null, DateTime? Todate = null, DateTime? EffectiveDate = null)
        {
            if (Fromdate >= EffectiveDate || ((Fromdate.Value.Month == EffectiveDate.Value.Month) && (Fromdate.Value.Year == EffectiveDate.Value.Year)))
            {
                var feefrequency = db.FeeFrequencies.Where(f => f.FeeFrequencyId == Frequencyid).FirstOrDefault();
                decimal months = Convert.ToDecimal(feefrequency.NoOfMonths);
                Fee = Convert.ToDecimal(Fee / months);
                decimal totalmnth = 12 * (Fromdate.Value.Year - Todate.Value.Year) + Fromdate.Value.Month - Todate.Value.Month;
                totalmnth = Math.Abs(totalmnth);
                if (totalmnth != 0)
                    Fee = Math.Round((decimal)(Fee * totalmnth), 2);
            }
            else
            {
                Fee = 0;
            }
            return Fee;

        }

        public IList<TransportRoutewiseList> GetRouteWiseFeeDetail(ref int count, int RouteId = 0, int StudentId = 0, DateTime? FromDate = null, DateTime? ToDate = null)
        {
            IList<TransportRoutewiseList> list = new List<TransportRoutewiseList>();
            if (RouteId > 0)
            {
                TransportRoutewiseList routeinfo = new TransportRoutewiseList();
                var query = db.BusRoutes.Where(f => f.BusRouteId == RouteId).FirstOrDefault();
                if (query != null)
                {
                    routeinfo.RouteId = query.BusRouteId.ToString();
                    routeinfo.RouteName = query.BusRoute1;
                }
                var fromyear = FromDate.Value.Year;
                var toyear = ToDate.Value.Year;
                var session = db.Sessions.Where(f => f.EndDate >= ToDate).FirstOrDefault();
                //string year = fromyear + "-" + toyear;
                //var session = db.Sessions.Where(m => m.Session1 == year).FirstOrDefault();
                int sessionid = 0;
                if (session != null)
                    sessionid = (int)session.SessionId;
                var tptroutes = db.TptFacilities.Where(f => (f.BusRouteId == RouteId || f.DropRouteId == RouteId) && f.EffectiveDate != null && f.StudentId == StudentId && f.VehicleTypeId == null && ((f.EffectiveDate <= FromDate && (f.EndDate == null || (f.EndDate != null && f.EndDate >= FromDate))) || f.EffectiveDate >= FromDate) && ((f.EndDate != null && f.EndDate <= ToDate) || f.EndDate == null)).ToList();
                IList<StudentinfoDetail> Stulist = new List<StudentinfoDetail>();
                if (tptroutes != null)
                {
                    foreach (var item in tptroutes)
                    {
                        string classname = "";
                        StudentinfoDetail studetail = new StudentinfoDetail();
                        var standard = db.StudentClasses.Where(f => f.SessionId == sessionid && f.StudentId == item.StudentId).FirstOrDefault();
                        if (standard != null)
                        {
                            classname = standard.ClassMaster.Class;
                            var guardian = db.Guardians.Where(f => f.StudentId == item.StudentId).FirstOrDefault();
                            string midname = "";
                            if (guardian != null)
                            {
                                var gender = item.Student.Gender.Gender1;
                                if (gender == "Male")
                                {
                                    midname = "S/O" + " " + guardian.FirstName + " " + guardian.LastName;
                                }
                                else
                                {
                                    midname = "D/O" + " " + guardian.FirstName + " " + guardian.LastName;
                                }
                            }
                            string StudentName = item.Student.FName + " " + item.Student.LName + " " + midname;
                            studetail.StudentName = StudentName;
                            studetail.Class = classname;
                            var standardid = standard.ClassMaster.StandardId;
                            int groupfeeheadid = 0;
                            var feeclassgroup = db.FeeClassGroupDetails.Where(f => f.StandardId == standardid).FirstOrDefault();
                            if (feeclassgroup != null)
                            {
                                var groupfeetype = db.GroupFeeTypes.Where(f => f.FeeClassGroupId == feeclassgroup.FeeClassGroupId).FirstOrDefault();
                                if (groupfeetype != null)
                                {
                                    var groupfeehead = db.GroupFeeHeads.Where(f => f.GroupFeeTypeId == groupfeetype.GroupFeeTypeId && f.FeeHead.FeeHead1.Contains("Transport") && (f.ReceiptTypeFeeHead.ReceiptType.IsForPreviousSession == null || f.ReceiptTypeFeeHead.ReceiptType.IsForPreviousSession == false)).FirstOrDefault();
                                    if (groupfeehead != null)
                                    {
                                        groupfeeheadid = (int)groupfeehead.GroupFeeHeadId;
                                        decimal totalmnth = 12 * (FromDate.Value.Year - ToDate.Value.Year) + FromDate.Value.Month - ToDate.Value.Month;
                                        totalmnth = Math.Abs(totalmnth);
                                        List<MonthlyFeeList> feelist = new List<MonthlyFeeList>();
                                        var months = new[] { "January", "February", "March", "April", "May", "June", "July",
                        "August", "September", "October", "November", "December" };
                                        for (int i = 0; i <= totalmnth; i++)
                                        {
                                            decimal tptFeeHeadamt = 0;
                                            decimal Payableamt = 0;
                                            decimal receivedamt = 0;
                                            DateTime StartDate = FromDate.Value.AddMonths(i);
                                            int month = (StartDate.Month) - 1;
                                            MonthlyFeeList monthlyfee = new MonthlyFeeList();
                                            if (month <= 11)
                                            {
                                                monthlyfee.FeePeriod = months[month];
                                            }
                                            else
                                            {
                                                monthlyfee.FeePeriod = months[month - 12];
                                            }
                                            DateTime Enddate = StartDate.AddMonths(1).AddDays(-1);
                                            var groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(f => f.EffectiveDate >= StartDate && ((f.EndDate != null && f.EndDate <= Enddate) || f.EndDate == null)).FirstOrDefault();
                                            if (groupfeeheaddetail == null)
                                            {
                                                groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(t => t.EffectiveDate <= StartDate &&
                                                        (t.EndDate == null)).FirstOrDefault();
                                                if (groupfeeheaddetail == null)
                                                {
                                                    groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(t => (t.EffectiveDate <= StartDate || t.EffectiveDate <= Enddate) &&
                                                       (t.EndDate == null)).FirstOrDefault();
                                                }
                                                if (groupfeeheaddetail == null)
                                                {
                                                    groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(t => (t.EffectiveDate <= StartDate || t.EffectiveDate <= Enddate) &&
                                                       (t.EndDate != null && t.EndDate >= StartDate)).FirstOrDefault();
                                                }
                                            }
                                            if (groupfeeheaddetail != null)
                                            {
                                                decimal fee = FeeCalculationMonthwise((int)groupfeetype.FeeType.FeeFrequencyId, (decimal)groupfeeheaddetail.Amount, StartDate, item.EffectiveDate);
                                                tptFeeHeadamt = fee;
                                            }
                                            if (item.BoardingPointId != null)
                                            {
                                                var boardingfee = db.BoardingPointFees.Where(f => f.BoardingPointId == item.BoardingPointId && f.EfectiveDate >= StartDate && (f.EndDate == null || (f.EndDate != null && f.EndDate <= Enddate))).FirstOrDefault();
                                                if (boardingfee == null)
                                                {
                                                    boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.BoardingPointId && t.EfectiveDate <= StartDate &&
                                                            (t.EndDate == null)).FirstOrDefault();
                                                    if (boardingfee == null)
                                                    {
                                                        boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.BoardingPointId && (t.EfectiveDate <= StartDate || t.EfectiveDate <= Enddate) &&
                                                           (t.EndDate == null)).FirstOrDefault();
                                                    }
                                                    if (boardingfee == null)
                                                    {
                                                        boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.BoardingPointId && (t.EfectiveDate <= StartDate || t.EfectiveDate <= Enddate) &&
                                                           (t.EndDate != null && t.EndDate >= StartDate)).FirstOrDefault();
                                                    }
                                                }
                                                if (boardingfee != null)
                                                {
                                                    if (item.TptTypeDetail.TptName == "Pick Drop")
                                                    {
                                                        if (boardingfee.TransportFee_2 == null || boardingfee.TransportFee_2 == 0)
                                                        {
                                                            Payableamt = (decimal)tptFeeHeadamt;
                                                        }
                                                        else
                                                        {
                                                            decimal fee = FeeCalculationMonthwise((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_2, StartDate, item.EffectiveDate);
                                                            Payableamt = fee;
                                                        }
                                                    }
                                                    else if (item.TptTypeDetail.TptName == "Pick" || item.TptTypeDetail.TptName == "Drop")
                                                    {
                                                        if (boardingfee.TransportFee_1 == null || boardingfee.TransportFee_1 == 0)
                                                        {
                                                            Payableamt = (decimal)tptFeeHeadamt;
                                                        }
                                                        else
                                                        {
                                                            decimal fee = FeeCalculationMonthwise((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_1, StartDate, item.EffectiveDate);
                                                            Payableamt = fee;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Payableamt = (decimal)tptFeeHeadamt;
                                                }
                                            }
                                            else if (item.DropPointId != null)
                                            {
                                                var boardingfee = db.BoardingPointFees.Where(f => f.BoardingPointId == item.DropPointId && f.EfectiveDate >= StartDate && (f.EndDate == null || (f.EndDate != null && f.EndDate <= Enddate))).FirstOrDefault();
                                                if (boardingfee == null)
                                                {
                                                    boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.DropPointId && t.EfectiveDate <= StartDate &&
                                                            (t.EndDate == null)).FirstOrDefault();
                                                    if (boardingfee == null)
                                                    {
                                                        boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.DropPointId && (t.EfectiveDate <= StartDate || t.EfectiveDate <= Enddate) &&
                                                           (t.EndDate == null)).FirstOrDefault();
                                                    }
                                                    if (boardingfee == null)
                                                    {
                                                        boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.DropPointId && (t.EfectiveDate <= StartDate || t.EfectiveDate <= Enddate) &&
                                                           (t.EndDate != null && t.EndDate >= StartDate)).FirstOrDefault();
                                                    }
                                                }
                                                if (boardingfee != null)
                                                {
                                                    if (item.TptTypeDetail.TptName == "Pick Drop")
                                                    {
                                                        if (boardingfee.TransportFee_2 == null || boardingfee.TransportFee_2 == 0)
                                                        {
                                                            Payableamt = (decimal)tptFeeHeadamt;
                                                        }
                                                        else
                                                        {
                                                            decimal fee = FeeCalculationMonthwise((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_2, StartDate, item.EffectiveDate);
                                                            Payableamt = fee;
                                                        }
                                                    }
                                                    else if (item.TptTypeDetail.TptName == "Pick" || item.TptTypeDetail.TptName == "Drop")
                                                    {
                                                        if (boardingfee.TransportFee_1 == null || boardingfee.TransportFee_1 == 0)
                                                        {
                                                            Payableamt = (decimal)tptFeeHeadamt;
                                                        }
                                                        else
                                                        {
                                                            decimal fee = FeeCalculationMonthwise((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_1, StartDate, item.EffectiveDate);
                                                            Payableamt = fee;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Payableamt = (decimal)tptFeeHeadamt;
                                                }
                                            }

                                            var feeperiodid = db.FeePeriodDetails.Where(f => f.PeriodStartDate >= StartDate && f.PeriodEndDate <= Enddate && f.FeePeriod.FeeFrequencyId == groupfeetype.FeeType.FeeFrequencyId && f.SessionId == sessionid).FirstOrDefault();
                                            if (feeperiodid != null)
                                            {
                                                var feeperiods = db.FeePeriods.Where(f => f.FeePeriodId == feeperiodid.FeePeriodId).FirstOrDefault();
                                                var isfeeperiodexist = GetAllFeeReceiptDetailsFeePeriod(ref count, StudentId: item.StudentId, FeePeriodId: feeperiods.FeePeriodId, ReceiptTypeId: groupfeehead.ReceiptTypeId, SessionId: sessionid);
                                                if (isfeeperiodexist.Count > 0)
                                                {
                                                    var feereceipts = db.FeeReceipts.Where(f => f.SessionId == sessionid && f.StudentId == item.StudentId && f.FeePeriodId == feeperiods.FeePeriodId).ToList();
                                                    if (feereceipts != null)
                                                    {
                                                        foreach (var fee in feereceipts)
                                                        {
                                                            var feedetail = fee.FeeReceiptDetails.Where(f => f.ReceiptId == fee.ReceiptId);
                                                            var amt = feedetail.Where(f => f.GroupFeeHeadId == groupfeeheadid).Select(f => f.Amount).Sum();
                                                            var waivedamt = feedetail.Where(f => f.GroupFeeHeadId == groupfeeheadid).Select(f => f.WaivedOffAmount).Sum();
                                                            decimal feecal = FeeCalculationMonthwise((int)feeperiods.FeeFrequencyId, (decimal)amt, StartDate, item.EffectiveDate);
                                                            receivedamt += (decimal)feecal;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var Feedetail = db.FeeReceiptDetails.Where(f => f.PendingFeePeriodId == feeperiods.FeePeriodId && f.FeeReceipt.SessionId == sessionid && f.FeeReceipt.StudentId == item.StudentId).FirstOrDefault();
                                                        if (Feedetail != null)
                                                        {
                                                            receivedamt = (decimal)Feedetail.Amount;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    var Feedetail = db.FeeReceiptDetails.Where(f => f.PendingFeePeriodId == feeperiods.FeePeriodId && f.FeeReceipt.SessionId == sessionid && f.FeeReceipt.StudentId == item.StudentId).FirstOrDefault();
                                                    if (Feedetail != null)
                                                    {
                                                        receivedamt = (decimal)Feedetail.Amount;
                                                    }
                                                }
                                            }
                                            monthlyfee.Payable = Math.Round((decimal)Payableamt, 2);
                                            monthlyfee.Received = Math.Round((decimal)receivedamt, 2);
                                            monthlyfee.Pending = Math.Round((decimal)(Payableamt - receivedamt), 2);
                                            if (Payableamt > 0)
                                            {
                                                feelist.Add(monthlyfee);
                                            }
                                        }
                                        studetail.FeeList = feelist;
                                    }
                                }
                            }
                        }
                        Stulist.Add(studetail);
                    }
                }
                routeinfo.StudentDetail = Stulist;
                list.Add(routeinfo);
            }
            return list;
        }

        public decimal FeeCalculationMonthwise(int FrequencyId = 0, decimal Fee = 0, DateTime? Fromdate = null, DateTime? EffectiveDate = null)
        {

            if (Fromdate >= EffectiveDate || ((Fromdate.Value.Month == EffectiveDate.Value.Month) && (Fromdate.Value.Year == EffectiveDate.Value.Year)))
            {
                var frequency = db.FeeFrequencies.Where(m => m.FeeFrequencyId == FrequencyId).FirstOrDefault();
                decimal months = Convert.ToDecimal(frequency.NoOfMonths);
                Fee = Convert.ToDecimal(Fee / months);
            }
            else
            {
                Fee = 0;
            }
            return Fee;
        }
        public IList<TransportRoutewiseList> GetRouteWiseFeeDetailTotal(ref int count, int RouteId = 0, DateTime? FromDate = null, DateTime? ToDate = null)
        {
            IList<TransportRoutewiseList> list = new List<TransportRoutewiseList>();
            if (RouteId > 0)
            {
                TransportRoutewiseList routeinfo = new TransportRoutewiseList();
                var query = db.BusRoutes.Where(f => f.BusRouteId == RouteId).FirstOrDefault();
                if (query != null)
                {
                    routeinfo.RouteId = query.BusRouteId.ToString();
                    routeinfo.RouteName = query.BusRoute1;
                }
                var fromyear = FromDate.Value.Year;
                var toyear = ToDate.Value.Year;
                //string year = fromyear + "-" + toyear;
                //var session = db.Sessions.Where(m => m.Session1 == year).FirstOrDefault();
                var session = db.Sessions.Where(f => f.EndDate >= ToDate).FirstOrDefault();
                int sessionid = 0;
                if (session != null)
                    sessionid = (int)session.SessionId;
                var tptroutes = db.TptFacilities.Where(f => (f.BusRouteId == RouteId || f.DropRouteId == RouteId) && f.EffectiveDate != null && f.VehicleTypeId == null && ((f.EffectiveDate <= FromDate && (f.EndDate == null || (f.EndDate != null && f.EndDate >= FromDate))) || f.EffectiveDate >= FromDate) && ((f.EndDate != null && f.EndDate <= ToDate) || f.EndDate == null)).ToList();
                IList<StudentinfoDetail> Stulist = new List<StudentinfoDetail>();
                if (tptroutes != null)
                {
                    foreach (var item in tptroutes)
                    {
                        string classname = "";
                        StudentinfoDetail studetail = new StudentinfoDetail();
                        var standard = db.StudentClasses.Where(f => f.SessionId == sessionid && f.StudentId == item.StudentId).FirstOrDefault();
                        if (standard != null)
                        {
                            classname = standard.ClassMaster.Class;
                            var guardian = db.Guardians.Where(f => f.StudentId == item.StudentId).FirstOrDefault();
                            string midname = "";
                            if (guardian != null)
                            {
                                var gender = item.Student.Gender.Gender1;
                                if (gender == "Male")
                                {
                                    midname = "S/O" + " " + guardian.FirstName + " " + guardian.LastName;
                                }
                                else
                                {
                                    midname = "D/O" + " " + guardian.FirstName + " " + guardian.LastName;
                                }
                            }
                            string StudentName = item.Student.FName + " " + item.Student.LName + " " + midname;
                            studetail.StudentId = item.StudentId.ToString();
                            studetail.StudentName = StudentName;
                            studetail.Class = classname;
                            var standardid = standard.ClassMaster.StandardId;
                            int groupfeeheadid = 0;
                            var feeclassgroup = db.FeeClassGroupDetails.Where(f => f.StandardId == standardid).FirstOrDefault();
                            if (feeclassgroup != null)
                            {
                                var groupfeetype = db.GroupFeeTypes.Where(f => f.FeeClassGroupId == feeclassgroup.FeeClassGroupId).FirstOrDefault();
                                if (groupfeetype != null)
                                {
                                    var groupfeehead = db.GroupFeeHeads.Where(f => f.GroupFeeTypeId == groupfeetype.GroupFeeTypeId && f.FeeHead.FeeHead1.Contains("Transport") && (f.ReceiptTypeFeeHead.ReceiptType.IsForPreviousSession == null || f.ReceiptTypeFeeHead.ReceiptType.IsForPreviousSession == false)).FirstOrDefault();
                                    if (groupfeehead != null)
                                    {
                                        groupfeeheadid = (int)groupfeehead.GroupFeeHeadId;
                                        decimal totalmnth = 12 * (FromDate.Value.Year - ToDate.Value.Year) + FromDate.Value.Month - ToDate.Value.Month;
                                        totalmnth = Math.Abs(totalmnth);
                                        List<MonthlyFeeList> feelist = new List<MonthlyFeeList>();
                                        var months = new[] { "January", "February", "March", "April", "May", "June", "July",
                        "August", "September", "October", "November", "December" };
                                        decimal tptFeeHeadamt = 0;
                                        decimal Payableamt = 0;
                                        decimal receivedamt = 0;
                                        MonthlyFeeList monthlyfee = new MonthlyFeeList();
                                        for (int i = 0; i <= totalmnth; i++)
                                        {
                                            DateTime StartDate = FromDate.Value.AddMonths(i);
                                            int month = (StartDate.Month) - 1;

                                            if (month <= 11)
                                            {
                                                monthlyfee.FeePeriod = months[month];
                                            }
                                            else
                                            {
                                                monthlyfee.FeePeriod = months[month - 12];
                                            }
                                            DateTime Enddate = StartDate.AddMonths(1).AddDays(-1);
                                            var groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(f => f.EffectiveDate >= StartDate && ((f.EndDate != null && f.EndDate <= Enddate) || f.EndDate == null)).FirstOrDefault();
                                            if (groupfeeheaddetail == null)
                                            {
                                                groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(t => t.EffectiveDate <= StartDate &&
                                                        (t.EndDate == null)).FirstOrDefault();
                                                if (groupfeeheaddetail == null)
                                                {
                                                    groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(t => (t.EffectiveDate <= StartDate || t.EffectiveDate <= Enddate) &&
                                                       (t.EndDate == null)).FirstOrDefault();
                                                }
                                                if (groupfeeheaddetail == null)
                                                {
                                                    groupfeeheaddetail = groupfeehead.GroupFeeHeadDetails.Where(t => (t.EffectiveDate <= StartDate || t.EffectiveDate <= Enddate) &&
                                                       (t.EndDate != null && t.EndDate >= StartDate)).FirstOrDefault();
                                                }
                                            }
                                            if (groupfeeheaddetail != null)
                                            {
                                                decimal fee = FeeCalculationMonthwise((int)groupfeetype.FeeType.FeeFrequencyId, (decimal)groupfeeheaddetail.Amount, StartDate, item.EffectiveDate);
                                                tptFeeHeadamt = fee;
                                            }
                                            if (item.BoardingPointId != null)
                                            {
                                                var boardingfee = db.BoardingPointFees.Where(f => f.BoardingPointId == item.BoardingPointId && f.EfectiveDate >= StartDate && (f.EndDate == null || (f.EndDate != null && f.EndDate <= Enddate))).FirstOrDefault();
                                                if (boardingfee == null)
                                                {
                                                    boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.BoardingPointId && t.EfectiveDate <= StartDate &&
                                                            (t.EndDate == null)).FirstOrDefault();
                                                    if (boardingfee == null)
                                                    {
                                                        boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.BoardingPointId && (t.EfectiveDate <= StartDate || t.EfectiveDate <= Enddate) &&
                                                           (t.EndDate == null)).FirstOrDefault();
                                                    }
                                                    if (boardingfee == null)
                                                    {
                                                        boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.BoardingPointId && (t.EfectiveDate <= StartDate || t.EfectiveDate <= Enddate) &&
                                                           (t.EndDate != null && t.EndDate >= StartDate)).FirstOrDefault();
                                                    }
                                                }
                                                if (boardingfee != null)
                                                {
                                                    if (item.TptTypeDetail.TptName == "Pick Drop")
                                                    {
                                                        if (boardingfee.TransportFee_2 == null || boardingfee.TransportFee_2 == 0)
                                                        {
                                                            Payableamt += (decimal)tptFeeHeadamt;
                                                        }
                                                        else
                                                        {
                                                            decimal fee = FeeCalculationMonthwise((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_2, StartDate, item.EffectiveDate);
                                                            Payableamt += fee;
                                                        }
                                                    }
                                                    else if (item.TptTypeDetail.TptName == "Pick" || item.TptTypeDetail.TptName == "Drop")
                                                    {
                                                        if (boardingfee.TransportFee_1 == null || boardingfee.TransportFee_1 == 0)
                                                        {
                                                            Payableamt += (decimal)tptFeeHeadamt;
                                                        }
                                                        else
                                                        {
                                                            decimal fee = FeeCalculationMonthwise((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_1, StartDate, item.EffectiveDate);
                                                            Payableamt += fee;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Payableamt += (decimal)tptFeeHeadamt;
                                                }
                                            }
                                            else if (item.DropPointId != null)
                                            {
                                                var boardingfee = db.BoardingPointFees.Where(f => f.BoardingPointId == item.DropPointId && f.EfectiveDate >= StartDate && (f.EndDate == null || (f.EndDate != null && f.EndDate <= Enddate))).FirstOrDefault();
                                                if (boardingfee == null)
                                                {
                                                    boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.DropPointId && t.EfectiveDate <= StartDate &&
                                                            (t.EndDate == null)).FirstOrDefault();
                                                    if (boardingfee == null)
                                                    {
                                                        boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.DropPointId && (t.EfectiveDate <= StartDate || t.EfectiveDate <= Enddate) &&
                                                           (t.EndDate == null)).FirstOrDefault();
                                                    }
                                                    if (boardingfee == null)
                                                    {
                                                        boardingfee = db.BoardingPointFees.Where(t => t.BoardingPointId == item.DropPointId && (t.EfectiveDate <= StartDate || t.EfectiveDate <= Enddate) &&
                                                           (t.EndDate != null && t.EndDate >= StartDate)).FirstOrDefault();
                                                    }
                                                }
                                                if (boardingfee != null)
                                                {
                                                    if (item.TptTypeDetail.TptName == "Pick Drop")
                                                    {
                                                        if (boardingfee.TransportFee_2 == null || boardingfee.TransportFee_2 == 0)
                                                        {
                                                            Payableamt += (decimal)tptFeeHeadamt;
                                                        }
                                                        else
                                                        {
                                                            decimal fee = FeeCalculationMonthwise((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_2, StartDate, item.EffectiveDate);
                                                            Payableamt += fee;
                                                        }
                                                    }
                                                    else if (item.TptTypeDetail.TptName == "Pick" || item.TptTypeDetail.TptName == "Drop")
                                                    {
                                                        if (boardingfee.TransportFee_1 == null || boardingfee.TransportFee_1 == 0)
                                                        {
                                                            Payableamt += (decimal)tptFeeHeadamt;
                                                        }
                                                        else
                                                        {
                                                            decimal fee = FeeCalculationMonthwise((int)boardingfee.FeeFrequencyId, (decimal)boardingfee.TransportFee_1, StartDate, item.EffectiveDate);
                                                            Payableamt += fee;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Payableamt += (decimal)tptFeeHeadamt;
                                                }
                                            }
                                            var feeperiodid = db.FeePeriodDetails.Where(f => f.PeriodStartDate >= StartDate && f.PeriodEndDate <= Enddate && f.FeePeriod.FeeFrequencyId == groupfeetype.FeeType.FeeFrequencyId && f.SessionId == sessionid).FirstOrDefault();
                                            if (feeperiodid != null)
                                            {
                                                var feeperiods = db.FeePeriods.Where(f => f.FeePeriodId == feeperiodid.FeePeriodId).FirstOrDefault();
                                                var isfeeperiodexist = GetAllFeeReceiptDetailsFeePeriod(ref count, StudentId: item.StudentId, FeePeriodId: feeperiods.FeePeriodId, ReceiptTypeId: groupfeehead.ReceiptTypeId, SessionId: sessionid);
                                                if (isfeeperiodexist.Count > 0)
                                                {
                                                    var feereceipts = db.FeeReceipts.Where(f => f.SessionId == sessionid && f.StudentId == item.StudentId && f.FeePeriodId == feeperiods.FeePeriodId).ToList();
                                                    if (feereceipts != null)
                                                    {
                                                        foreach (var fee in feereceipts)
                                                        {
                                                            var feedetail = fee.FeeReceiptDetails.Where(f => f.ReceiptId == fee.ReceiptId);
                                                            var amt = feedetail.Where(f => f.GroupFeeHeadId == groupfeeheadid).Select(f => f.Amount).Sum();
                                                            var waivedamt = feedetail.Where(f => f.GroupFeeHeadId == groupfeeheadid).Select(f => f.WaivedOffAmount).Sum();
                                                            decimal feecal = FeeCalculationMonthwise((int)feeperiods.FeeFrequencyId, (decimal)amt, StartDate, item.EffectiveDate);
                                                            receivedamt += (decimal)feecal;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        var Feedetail = db.FeeReceiptDetails.Where(f => f.PendingFeePeriodId == feeperiods.FeePeriodId && f.FeeReceipt.SessionId == sessionid && f.FeeReceipt.StudentId == item.StudentId).FirstOrDefault();
                                                        if (Feedetail != null)
                                                        {
                                                            receivedamt += (decimal)Feedetail.Amount;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    var Feedetail = db.FeeReceiptDetails.Where(f => f.PendingFeePeriodId == feeperiods.FeePeriodId && f.FeeReceipt.SessionId == sessionid && f.FeeReceipt.StudentId == item.StudentId).FirstOrDefault();
                                                    if (Feedetail != null)
                                                    {
                                                        receivedamt += (decimal)Feedetail.Amount;
                                                    }
                                                }
                                            }
                                            monthlyfee.Payable = Math.Round((decimal)Payableamt, 2);
                                            monthlyfee.Received = Math.Round((decimal)receivedamt, 2);
                                            monthlyfee.Pending = Math.Round((decimal)(Payableamt - receivedamt), 2);

                                        }
                                        if (Payableamt > 0)
                                        {
                                            feelist.Add(monthlyfee);
                                        }
                                        studetail.FeeList = feelist;
                                    }
                                }
                            }
                        }
                        Stulist.Add(studetail);
                    }
                }
                routeinfo.StudentDetail = Stulist;
                list.Add(routeinfo);
            }
            return list;
        }

        #endregion

        #region feeinstallments

        public FeeInstallment GetFeeInstallmentById(int FeeInstallmentId, bool IsTrack = false)
        {
            if (FeeInstallmentId == 0)
                throw new ArgumentNullException("FeeInstallment");

            var FeeInstallment = new FeeInstallment();
            if (IsTrack)
                FeeInstallment = (from s in db.FeeInstallments where s.FeeInstallmentId == FeeInstallmentId select s).FirstOrDefault();
            else
                FeeInstallment = (from s in db.FeeInstallments.AsNoTracking() where s.FeeInstallmentId == FeeInstallmentId select s).FirstOrDefault();

            return FeeInstallment;
        }

        public List<FeeInstallment> GetAllFeeInstallments(ref int totalcount, int ReceiptId = 0, int PaidReceiptId = 0, DateTime? InstallmentDate = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeeInstallments.ToList();
            if (ReceiptId > 0)
                query = query.Where(m => m.ReceiptId == PaidReceiptId).ToList();
            if (PaidReceiptId > 0)
                query = query.Where(m => m.PaidReceiptId == PaidReceiptId).ToList();
            if (InstallmentDate.HasValue)
                query = query.Where(m => m.InstallmentDate == InstallmentDate).ToList();

            totalcount = query.Count;
            var FeeInstallments = new PagedList<KSModel.Models.FeeInstallment>(query, PageIndex, PageSize);
            return FeeInstallments;
        }

        public void InsertFeeInstallment(FeeInstallment FeeInstallment)
        {
            if (FeeInstallment == null)
                throw new ArgumentNullException("FeeInstallment");

            db.FeeInstallments.Add(FeeInstallment);
            db.SaveChanges();
        }

        public void UpdateFeeInstallment(FeeInstallment FeeInstallment)
        {
            if (FeeInstallment == null)
                throw new ArgumentNullException("FeeInstallment");

            db.Entry(FeeInstallment).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeInstallment(int FeeInstallmentId = 0)
        {
            if (FeeInstallmentId == 0)
                throw new ArgumentNullException("FeeInstallment");

            var FeeInstallment = (from s in db.FeeInstallments where s.FeeInstallmentId == FeeInstallmentId select s).FirstOrDefault();
            db.FeeInstallments.Remove(FeeInstallment);
            db.SaveChanges();
        }

        #endregion

        #region"Donation"
        public DonationFeeReceipt GetDonationDetailById(int DonationFeeReceiptId = 0)
        {
            DonationFeeReceipt donationreceipt = db.DonationFeeReceipts.Where(f => f.DonationFeeReceiptId == DonationFeeReceiptId).FirstOrDefault();
            return donationreceipt;

        }
        public void InsertDonationFeeReceipt(DonationFeeReceipt DonationFeeReceipt)
        {
            if (DonationFeeReceipt == null)
                throw new ArgumentNullException("DonationFeeReceipt");

            db.DonationFeeReceipts.Add(DonationFeeReceipt);
            db.SaveChanges();
        }

        public void UpdateDonationFeeReceipt(DonationFeeReceipt DonationFeeReceipt)
        {
            if (DonationFeeReceipt == null)
                throw new ArgumentNullException("DonationFeeReceipt");

            db.Entry(DonationFeeReceipt).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }
        public List<DonationModel> GetAllDonationsBySp(ref int count, string DonorName = "", DateTime? DonationFromDate = null, DateTime? DonationToDate = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_GetAllDonations", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@DonorName", DonorName);
                    sqlComm.Parameters.AddWithValue("@Fromdate", DonationFromDate);
                    sqlComm.Parameters.AddWithValue("@Todate", DonationToDate);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<DonationModel> GetAlldonations = new List<DonationModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DonationModel donationList = new DonationModel();
                donationList.DonationFeeReceiptId = (dt.Rows[i]["DonationFeeReceiptId"].ToString());
                donationList.DonorName = Convert.ToString(dt.Rows[i]["DonorName"]);
                donationList.ReceiptNo = Convert.ToInt32(dt.Rows[i]["DonationReceiptNo"]);
                donationList.Amount = Convert.ToDecimal(dt.Rows[i]["Amount"].ToString());
                donationList.DonationDate = Convert.ToDateTime(dt.Rows[i]["Date"]);
                donationList.DonorDescription = Convert.ToString(dt.Rows[i]["DonorDescription"]);
                donationList.Remarks = Convert.ToString(dt.Rows[i]["Remarks"]);
                donationList.PaymentModeId = Convert.ToInt32(dt.Rows[i]["PaymentModeId"]);
                GetAlldonations.Add(donationList);
            }
            count = GetAlldonations.Count;

            GetAlldonations = new PagedList<DonationModel>(GetAlldonations, PageIndex, PageSize);
            return GetAlldonations;
        }

        public void DeleteDonation(int DonationFeeReceiptId = 0)
        {
            if (DonationFeeReceiptId == 0)
                throw new ArgumentNullException("DonationFeeReceipt");

            var donation = (from s in db.DonationFeeReceipts where s.DonationFeeReceiptId == DonationFeeReceiptId select s).FirstOrDefault();
            db.DonationFeeReceipts.Remove(donation);
            db.SaveChanges();
        }

        #endregion

        #region" Delete Fee receipt"
        public void InsertDeletedFeeReceipt(DeletedFeeReceipt DeletedFeeReceipt)
        {
            if (DeletedFeeReceipt == null)
                throw new ArgumentNullException("DeletedFeeReceipt");

            db.DeletedFeeReceipts.Add(DeletedFeeReceipt);
            db.SaveChanges();
        }
        public void InsertDeletedFeeReceiptDetails(DeletedFeeReceiptDetail DeletedFeeReceiptDetail)
        {
            if (DeletedFeeReceiptDetail == null)
                throw new ArgumentNullException("DeletedFeeReceiptDetail");

            db.DeletedFeeReceiptDetails.Add(DeletedFeeReceiptDetail);
            db.SaveChanges();
        }

        #endregion

        #region"Fee class group concession"
        public FeeClassGroupConcessionDetail GetFeeClassGroupConcessionDetailById(int ConcessionDetailid = 0)
        {
            var query = db.FeeClassGroupConcessionDetails.Where(f => f.FeeClassGroupConcessionDetailId == ConcessionDetailid).FirstOrDefault();
            return query;
        }

        public List<FeeClassGroupConcessionModel> GetAllFeeClassGroupConcessionBySp(ref int count, int? FeeClassGroupId = null, int? ConcessionId = null, bool? ApplyToAll = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_GetAllFeeClassGroupConcession", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@FeeClassGroupId", FeeClassGroupId);
                    sqlComm.Parameters.AddWithValue("@ConcessionId", ConcessionId);
                    sqlComm.Parameters.AddWithValue("@ApplyToAll", ApplyToAll);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<FeeClassGroupConcessionModel> GetAllGroupconcessions = new List<FeeClassGroupConcessionModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                FeeClassGroupConcessionModel concessionList = new FeeClassGroupConcessionModel();
                concessionList.FeeClassGroupConcessiondetailId = (dt.Rows[i]["FeeClassGroupConcessionDetailId"].ToString());
                concessionList.FeeClassGroupConcessionId = Convert.ToString(dt.Rows[i]["ClassGroupConcessionId"]);
                concessionList.FeeClassGroupId = Convert.ToInt32(dt.Rows[i]["FeeClassGroupId"]);
                concessionList.Concession = (dt.Rows[i]["Consession"].ToString());
                concessionList.ConcessionId = Convert.ToInt32(dt.Rows[i]["ConcessionId"]);
                concessionList.FeeClassGroup = Convert.ToString(dt.Rows[i]["FeeClassGroup"]);
                concessionList.Amount = Convert.ToDecimal(dt.Rows[i]["Amount"]);
                concessionList.ApplyToAll = Convert.ToBoolean(dt.Rows[i]["ApplyToAll"]);
                concessionList.EffectiveDate = Convert.ToString(dt.Rows[i]["EffectiveFrom"]);
                if (dt.Rows[i]["EffectiveTill"] == null || dt.Rows[i]["EffectiveTill"].ToString() == "")
                {
                    concessionList.EndDate = "";
                }
                else
                {
                    concessionList.EndDate = Convert.ToString(dt.Rows[i]["EffectiveTill"]);
                }
                if (dt.Rows[i]["ApplyToAll"] == null || dt.Rows[i]["ApplyToAll"].ToString() == "false")
                {
                    concessionList.ApplyAll = "False";
                }
                else
                {
                    concessionList.ApplyAll = "True";
                }
                var feeconcessiondetail = db.FeeReceiptDetails.Where(f => f.ConsessionId == concessionList.ConcessionId && f.GroupFeeHeadId == null).ToList();
                var addonconcession = db.StudentFeeAddOns.Where(f => f.ConsessionId == concessionList.ConcessionId).ToList();
                if (feeconcessiondetail.Count > 0)
                {
                    concessionList.IsAuthToEdit = false;
                    concessionList.IsAuthToDelete = false;
                }
                else if (addonconcession.Count > 0)
                {
                    concessionList.IsAuthToEdit = false;
                    concessionList.IsAuthToDelete = false;
                }
                else
                {
                    concessionList.IsAuthToEdit = true;
                    concessionList.IsAuthToDelete = true;
                }
                GetAllGroupconcessions.Add(concessionList);
            }
            count = GetAllGroupconcessions.Count;

            GetAllGroupconcessions = new PagedList<FeeClassGroupConcessionModel>(GetAllGroupconcessions, PageIndex, PageSize);
            return GetAllGroupconcessions;
        }

        public FeeClassGroupConcession GetFeeclassGroupConcessionById(int groupConcessionId = 0)
        {
            var query = db.FeeClassGroupConcessions.Where(f => f.ClassGroupConcessionId == groupConcessionId).FirstOrDefault();
            return query;
        }

        public void UpdateFeeClassGroupConcession(FeeClassGroupConcession ClassGroupConcession)
        {
            if (ClassGroupConcession == null)
                throw new ArgumentNullException("FeeClassGroupConcession");

            db.Entry(ClassGroupConcession).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void UpdateFeeClassGroupConcessionDetail(FeeClassGroupConcessionDetail ClassGroupConcessionDetail)
        {
            if (ClassGroupConcessionDetail == null)
                throw new ArgumentNullException("FeeClassGroupConcessionDetail");

            db.Entry(ClassGroupConcessionDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void InsertFeeClassGroupConcession(FeeClassGroupConcession ClassGroupConcession)
        {
            if (ClassGroupConcession == null)
                throw new ArgumentNullException("ClassGroupConcession");

            db.FeeClassGroupConcessions.Add(ClassGroupConcession);
            db.SaveChanges();
        }
        public void InsertFeeClassGroupConcessionDetail(FeeClassGroupConcessionDetail ClassGroupConcessionDetail)
        {
            if (ClassGroupConcessionDetail == null)
                throw new ArgumentNullException("ClassGroupConcessionDetail");

            db.FeeClassGroupConcessionDetails.Add(ClassGroupConcessionDetail);
            db.SaveChanges();
        }
        public void DeleteFeeClassGroupConcessionDetail(int ConcessionDetailId = 0)
        {
            if (ConcessionDetailId == 0)
                throw new ArgumentNullException("FeeClassGroupConcessionDetail");

            var ConcessionDetail = (from s in db.FeeClassGroupConcessionDetails where s.FeeClassGroupConcessionDetailId == ConcessionDetailId select s).FirstOrDefault();
            db.FeeClassGroupConcessionDetails.Remove(ConcessionDetail);
            db.SaveChanges();
        }
        #endregion

        #region"Fee Calculation With new method"
        public IList<FeeReceiptDetailModel> GetFeeHeadCalculationsBySp(int StudentId = 0, int SessionId = 0, string ReceiptDate = "",
            int FeeTypeId = 0, int FeePeriodId = 0, int ReceiptTypeId = 0, string SessionStartDate = "",
            string SessionEndDate = "", string EWSDetail = "0", string IsApplyTptOwnerShip = "1", string IsSeparateTptreceipt = "0",
            bool OnlyCurrentPeriod = false)
        {
            IList<FeeReceiptDetailModel> FeerReceiptDetailList = new List<FeeReceiptDetailModel>();
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_FeeHeadCalculation", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@StudentId", StudentId);
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@ReceiptDate", ReceiptDate);
                    sqlComm.Parameters.AddWithValue("@FeeTypeId", FeeTypeId);
                    sqlComm.Parameters.AddWithValue("@SelectedFeePeriod", FeePeriodId);
                    sqlComm.Parameters.AddWithValue("@ReceiptTypeId", ReceiptTypeId);
                    sqlComm.Parameters.AddWithValue("@SessionStartDate", SessionStartDate);
                    sqlComm.Parameters.AddWithValue("@SessionendDate", SessionEndDate);
                    sqlComm.Parameters.AddWithValue("@ShowEWS", EWSDetail);
                    sqlComm.Parameters.AddWithValue("@IsApplyTptOwnerShip", IsApplyTptOwnerShip);
                    sqlComm.Parameters.AddWithValue("@IsSeparateTptFeeReceipt", IsSeparateTptreceipt);
                    sqlComm.Parameters.AddWithValue("@onlyCurrentPeriod", OnlyCurrentPeriod);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            if (dt != null)
            {
                object discountsumObject;
                discountsumObject = dt.Compute("Sum(EWSDiscount)", "");
                object concessionsumObject;
                concessionsumObject = dt.Compute("Sum(ConcessionAmount)", "");
                var count = 0;
                var feeperiodLIst = GetAllFeePeriods(ref count);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    FeeReceiptDetailModel feereceiptmodel = new FeeReceiptDetailModel();
                    feereceiptmodel.GroupFeeHeadId = Convert.ToInt32(dt.Rows[i]["HeadId"]);
                    if(dt.Rows[i]["ReceiptId"]!=null && dt.Rows[i]["ReceiptId"]!="")
                    feereceiptmodel.periodwiseReceiptId = Convert.ToInt32(dt.Rows[i]["ReceiptId"]);
                    
                    feereceiptmodel.GroupFeeHeadName = Convert.ToString(dt.Rows[i]["HeadName"]);
                    feereceiptmodel.HeadType = Convert.ToString(dt.Rows[i]["HeadType"]);
                    feereceiptmodel.IsDiscount = Convert.ToBoolean(dt.Rows[i]["IsDiscount"]);
                    feereceiptmodel.FeeperiodId = Convert.ToInt32(dt.Rows[i]["Selectedfeeperiodid"]);
                    feereceiptmodel.Feeperiodindex = feeperiodLIst.Where(m => m.FeePeriodId == feereceiptmodel.FeeperiodId).FirstOrDefault() != null ? (int)feeperiodLIst.Where(m => m.FeePeriodId == feereceiptmodel.FeeperiodId).FirstOrDefault().FeePeriodIndex : 0;
                    
                    feereceiptmodel.LateFeePeriodId = Convert.ToInt32(dt.Rows[i]["LateFeePeriodid"]);
                    feereceiptmodel.PendingFeePeriodId = Convert.ToInt32(dt.Rows[i]["PendingFeePeriodId"]);
                    if (dt.Rows[i]["EWSDiscount"].ToString() == "")
                        feereceiptmodel.EWSDiscount = 0;
                    else
                        feereceiptmodel.EWSDiscount = Convert.ToDecimal(dt.Rows[i]["EWSDiscount"].ToString());
                    if (dt.Rows[i]["ConcessionAmount"].ToString() == "")
                        feereceiptmodel.FeeHeadConcession = 0;
                    else
                        feereceiptmodel.FeeHeadConcession = Convert.ToDecimal(dt.Rows[i]["ConcessionAmount"].ToString());
                    feereceiptmodel.TotalHeadAmount = Convert.ToDecimal(dt.Rows[i]["TotalAmount"].ToString());
                    feereceiptmodel.CalculatedHeadAmount = Convert.ToDecimal(dt.Rows[i]["CalculatedAmount"].ToString());
                    if (EWSDetail.ToString() == "0")
                        feereceiptmodel.ShowEWS = false;
                    else
                    {
                        if (discountsumObject.ToString() == "" || discountsumObject.ToString() == "0" || discountsumObject.ToString() == "0.00")
                        {
                            feereceiptmodel.ShowEWS = false;
                        }
                        else
                        {
                            feereceiptmodel.ShowEWS = true;
                        }
                    }

                    if (concessionsumObject.ToString() == "" || concessionsumObject.ToString() == "0" || concessionsumObject.ToString() == "0.00")
                        feereceiptmodel.ShowHeadConcession = false;
                    else
                        feereceiptmodel.ShowHeadConcession = true;

                    FeerReceiptDetailList.Add(feereceiptmodel);
                }
            }
            return FeerReceiptDetailList.OrderBy(m=>m.Feeperiodindex).ToList();
        }

        public IList<FeeReceiptDetailModel> GetFeeHeadCalculationsDefaulterBySp(int StudentId = 0, int SessionId = 0, string ReceiptDate = "", int FeeTypeId = 0, int FeePeriodId = 0, int ReceiptTypeId = 0, string SessionStartDate = "", string SessionEndDate = "", string EWSDetail = "0", string IsApplyTptOwnerShip = "1", string IsSeparateTptreceipt = "0")
        {
            IList<FeeReceiptDetailModel> FeerReceiptDetailList = new List<FeeReceiptDetailModel>();
            DataTable dt = new DataTable();
            var count = 0;
            var feeperiod = GetAllFeePeriods(ref count);
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_FeeHeadCalculation_Defaulter", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@StudentId", StudentId);
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@ReceiptDate", ReceiptDate);
                    sqlComm.Parameters.AddWithValue("@FeeTypeId", FeeTypeId);
                    sqlComm.Parameters.AddWithValue("@SelectedFeePeriod", FeePeriodId);
                    sqlComm.Parameters.AddWithValue("@ReceiptTypeId", ReceiptTypeId);
                    sqlComm.Parameters.AddWithValue("@SessionStartDate", SessionStartDate);
                    sqlComm.Parameters.AddWithValue("@SessionendDate", SessionEndDate);
                    sqlComm.Parameters.AddWithValue("@ShowEWS", EWSDetail);
                    sqlComm.Parameters.AddWithValue("@IsApplyTptOwnerShip", IsApplyTptOwnerShip);
                    sqlComm.Parameters.AddWithValue("@IsSeparateTptFeeReceipt", IsSeparateTptreceipt);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            if (dt != null)
            {
                object discountsumObject;
                discountsumObject = dt.Compute("Sum(EWSDiscount)", "");
                object concessionsumObject;
                concessionsumObject = dt.Compute("Sum(ConcessionAmount)", "");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    FeeReceiptDetailModel feereceiptmodel = new FeeReceiptDetailModel();
                    feereceiptmodel.GroupFeeHeadId = Convert.ToInt32(dt.Rows[i]["HeadId"]);
                    feereceiptmodel.GroupFeeHeadName = Convert.ToString(dt.Rows[i]["HeadName"]);
                    feereceiptmodel.HeadType = Convert.ToString(dt.Rows[i]["HeadType"]);
                    feereceiptmodel.IsDiscount = Convert.ToBoolean(dt.Rows[i]["IsDiscount"]);
                    feereceiptmodel.LateFeePeriodId = Convert.ToInt32(dt.Rows[i]["LateFeePeriodid"]);
                    feereceiptmodel.PendingFeePeriodId = Convert.ToInt32(dt.Rows[i]["PendingFeePeriodId"]);
                    feereceiptmodel.Feeperiodindex = feeperiod.Where(m => m.FeePeriodId == Convert.ToInt32(dt.Rows[i]["Selectedfeeperiodid"])).FirstOrDefault() != null ? (int)feeperiod.Where(m => m.FeePeriodId == Convert.ToInt32(dt.Rows[i]["Selectedfeeperiodid"])).FirstOrDefault().FeePeriodIndex : 0;
                    if (dt.Rows[i]["EWSDiscount"].ToString() == "")
                        feereceiptmodel.EWSDiscount = 0;
                    else
                        feereceiptmodel.EWSDiscount = Convert.ToDecimal(dt.Rows[i]["EWSDiscount"].ToString());
                    if (dt.Rows[i]["ConcessionAmount"].ToString() == "")
                        feereceiptmodel.FeeHeadConcession = 0;
                    else
                        feereceiptmodel.FeeHeadConcession = Convert.ToDecimal(dt.Rows[i]["ConcessionAmount"].ToString());
                    feereceiptmodel.TotalHeadAmount = Convert.ToDecimal(dt.Rows[i]["TotalAmount"].ToString());
                    feereceiptmodel.CalculatedHeadAmount = Convert.ToDecimal(dt.Rows[i]["CalculatedAmount"].ToString());
                    if (EWSDetail.ToString() == "0")
                        feereceiptmodel.ShowEWS = false;
                    else
                    {
                        if (discountsumObject.ToString() == "" || discountsumObject.ToString() == "0" || discountsumObject.ToString() == "0.00")
                        {
                            feereceiptmodel.ShowEWS = false;
                        }
                        else
                        {
                            feereceiptmodel.ShowEWS = true;
                        }
                    }

                    if (concessionsumObject.ToString() == "" || concessionsumObject.ToString() == "0" || concessionsumObject.ToString() == "0.00")
                        feereceiptmodel.ShowHeadConcession = false;
                    else
                        feereceiptmodel.ShowHeadConcession = true;

                    FeerReceiptDetailList.Add(feereceiptmodel);
                }
            }
            return FeerReceiptDetailList;
        }

        #endregion

        #region "Group fee head Concession"
        public List<GroupFeeHeadConcession> GetGroupFeeheadConList(ref int count, int GroupFeeHeadDetailId = 0, int concessionid = 0, DateTime? StartDate = null, DateTime? EndDate = null)
        {
            List<GroupFeeHeadConcession> GrpConcessionList = new List<GroupFeeHeadConcession>();
            var query = db.GroupFeeHeadConcs.ToList();
            if (GroupFeeHeadDetailId > 0)
                query = query.Where(f => f.GroupFeeHeadDetailId == GroupFeeHeadDetailId).ToList();
            if (concessionid > 0)
                query = query.Where(f => f.ConcessionId == concessionid).ToList();
            if (StartDate != null && EndDate != null)
                query = query.Where(f => f.EffectiveDate >= StartDate && f.EffectiveDate <= EndDate).ToList();

            var AllFeeRecieptDetailsConcs = GetAllFeeReceiptDetailConcs(ref count);
            foreach (var item in query)
            {
                GroupFeeHeadConcession grpconcession = new GroupFeeHeadConcession();
                grpconcession.GroupFeeHeadConcId = item.GroupFeeHeadConcId;
                grpconcession.GroupFeeHeadDetailId = (int)item.GroupFeeHeadDetailId;
                grpconcession.Amount = item.Amount != null ? (decimal)item.Amount : 0;
                grpconcession.IsPercent = item.IsPercent != null ? (bool)item.IsPercent : false;
                grpconcession.ConcessionId = (int)item.ConcessionId;
                var concession = db.Consessions.Where(f => f.ConsessionId == item.ConcessionId).FirstOrDefault();
                if (concession != null)
                    grpconcession.Concession = concession.Consession1;
                else
                    grpconcession.Concession = "";
                DateTime dteffective = Convert.ToDateTime(item.EffectiveDate);
                grpconcession.EffectiveDate = dteffective.ToString("dd/MM/yyyy");
                if (item.EndDate != null)
                {
                    DateTime dtenddate = Convert.ToDateTime(item.EndDate);
                    grpconcession.EndDate = dtenddate.ToString("dd/MM/yyyy");
                    grpconcession.CloseDate = item.EndDate;
                }
                else
                {
                    grpconcession.EndDate = "";
                    grpconcession.CloseDate = null;
                }
                grpconcession.IsAuthToClose = true;
                grpconcession.IsAuthToEdit = true;
                grpconcession.IsAuthToDelete = true;
                if (item.EndDate != null)
                {
                  //  grpconcession.IsAuthToEdit = false;
                  //  grpconcession.IsAuthToDelete = false;
                    grpconcession.IsAuthToClose = false;
                }
                var groupfeeheadDetail = db.GroupFeeHeadDetails.Where(f => f.GroupFeeHeadDetailId == GroupFeeHeadDetailId).FirstOrDefault();
                //var feerecptcount = GetAllFeeReceiptDetails(ref count, GroupFeeHeadId: groupfeeheadDetail.GroupFeeHeadId, ReceiptTypeId: groupfeeheadDetail.GroupFeeHead.ReceiptTypeId);
                var feerecptcount = GetAllFeeReceiptDetails(ref count, GroupFeeHeadId: groupfeeheadDetail.GroupFeeHeadId, ReceiptTypeId: groupfeeheadDetail.GroupFeeHead.ReceiptTypeId).Where(f=>f.ConsessionId!=null && f.ConsessionId==item.ConcessionId);
                if (feerecptcount.Count() > 0)
                {
                    grpconcession.IsAuthToEdit = false;
                    grpconcession.IsAuthToDelete = false;
                }
                var FeeRecieptDetailIds = GetAllFeeReceiptDetails(ref count, GroupFeeHeadId: groupfeeheadDetail.GroupFeeHeadId, ReceiptTypeId: groupfeeheadDetail.GroupFeeHead.ReceiptTypeId).Select(f => f.ReceiptDetailId).ToArray();
                if (FeeRecieptDetailIds != null && FeeRecieptDetailIds.Count() > 0)
                {
                    var feerecptdetlcount = AllFeeRecieptDetailsConcs.Where(f => f.ConcessionId != null && f.ConcessionId == item.ConcessionId && FeeRecieptDetailIds.Contains((int)f.FeeReceiptDetailId)).Count();
                    if (feerecptdetlcount > 0)
                    {
                        grpconcession.IsAuthToEdit = false;
                        grpconcession.IsAuthToDelete = false;
                    }
                }
                GrpConcessionList.Add(grpconcession);
            }
            return GrpConcessionList;
        }

        public void InsertHeadWiseConcession(GroupFeeHeadConc GrpfeeHeadCon)
        {
            if (GrpfeeHeadCon == null)
                throw new ArgumentNullException("GrpfeeHeadCon");

            db.GroupFeeHeadConcs.Add(GrpfeeHeadCon);
            db.SaveChanges();
        }

        public GroupFeeHeadConc GetGroupfeeheadConById(int GroupfeeheadConId = 0)
        {
            var query = db.GroupFeeHeadConcs.Where(f => f.GroupFeeHeadConcId == GroupfeeheadConId).FirstOrDefault();
            return query;
        }
        public void UpdateGroupFeeHeadCon(GroupFeeHeadConc GroupfeeHeadConc)
        {
            if (GroupfeeHeadConc == null)
                throw new ArgumentNullException("GroupfeeHeadConc");

            db.Entry(GroupfeeHeadConc).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }
        public void DeleteGroupFeeHeadConcession(int GroupFeeHeadConId = 0)
        {
            if (GroupFeeHeadConId == 0)
                throw new ArgumentNullException("GroupFeeHeadConc");

            var ConcessionDetail = (from s in db.GroupFeeHeadConcs where s.GroupFeeHeadConcId == GroupFeeHeadConId select s).FirstOrDefault();
            db.GroupFeeHeadConcs.Remove(ConcessionDetail);
            db.SaveChanges();
        }
        #endregion

        #region "View or Print fee Receipt"
        public DataTable PrintFeeReceiptCertificateBySp(ref int totalcount, string ReceiptTypeId = "",
                                                                    int? SessionId = null, int? StudentId = null)
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("Sp_HeadCalculate_Certificate", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ReceiptTypeId", ReceiptTypeId);
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@StudentId", StudentId);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            return dt;
        }

        public PrintFeeReceiptModel ViewOrPrintFeeReceiptBySp(int ReceiptId = 0, int GroupFeeheadsinreceipt = 0)
        {
            PrintFeeReceiptModel model = new PrintFeeReceiptModel();
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_ViewOrPrintFeeReceipt", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ReceiptId", ReceiptId);
                    sqlComm.Parameters.AddWithValue("@IsGroupFeeHeads", GroupFeeheadsinreceipt);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    conn.Close();
                }
            }
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    model.ReceiptNo = Convert.ToString(ds.Tables[0].Rows[0]["ReceiptNo"]);
                    model.ReciptDate = Convert.ToString(ds.Tables[0].Rows[0]["ReceiptDate"]);
                    model.Remarks = Convert.ToString(ds.Tables[0].Rows[0]["Remarks"]);
                    model.FeePeriod = Convert.ToString(ds.Tables[0].Rows[0]["FeePeriod"]);
                    model.FeePeriodText = Convert.ToString(ds.Tables[0].Rows[0]["FeePeriodText"]);
                    model.Category= Convert.ToString(ds.Tables[0].Rows[0]["Category"]);
                    model.ReceiptType = Convert.ToString(ds.Tables[0].Rows[0]["ReceiptType"]);
                    model.PaymentMode = Convert.ToString(ds.Tables[0].Rows[0]["PaymentMode"]);
                    model.Modetype = Convert.ToString(ds.Tables[0].Rows[0]["ModeType"]);
                    if (model.Modetype.Contains("Transfer"))
                    {
                        model.BankBranch = Convert.ToString(ds.Tables[0].Rows[0]["BankName"]);
                        model.TransactionNo = Convert.ToString(ds.Tables[0].Rows[0]["IntrumentNo"]);
                        model.TransactionDate = Convert.ToString(ds.Tables[0].Rows[0]["InstrumentDate"]);
                    }
                    else if (model.Modetype.Contains("Instrument"))
                    {
                        if (model.PaymentMode.Contains("Cheque"))
                        {
                            model.lblChkDDNAme = "Cheque No.";
                            model.lblChkDDDate = "Cheque Date";
                        }
                        else
                        {
                            model.lblChkDDNAme = "DD No.";
                            model.lblChkDDDate = "DD Date";
                        }
                        model.BankBranch = Convert.ToString(ds.Tables[0].Rows[0]["BankName"]);
                        model.ChequeNo = Convert.ToString(ds.Tables[0].Rows[0]["IntrumentNo"]);
                        model.ChequeDate = Convert.ToString(ds.Tables[0].Rows[0]["InstrumentDate"]);
                    }
                    model.SchoolLogo = Convert.ToString(ds.Tables[0].Rows[0]["SchoolLogo"]);
                    model.AdmnNo = Convert.ToString(ds.Tables[0].Rows[0]["Admnno"]);
                    model.StudentName = Convert.ToString(ds.Tables[0].Rows[0]["StudentName"]);
                    model.StudentNameonly = Convert.ToString(ds.Tables[0].Rows[0]["StudentNameonly"]);
                    model.ClassName = Convert.ToString(ds.Tables[0].Rows[0]["Class"]);
                    model.RollNo = Convert.ToString(ds.Tables[0].Rows[0]["RollNo"]);
                    model.CurrencyIcon = Convert.ToString(ds.Tables[0].Rows[0]["CurrencyIcon"]);
                    model.SessionName = Convert.ToString(ds.Tables[0].Rows[0]["Session"]);
                    model.Header1 = Convert.ToString(ds.Tables[0].Rows[0]["ReceiptText1"]);
                    model.Header2 = Convert.ToString(ds.Tables[0].Rows[0]["ReceiptText2"]);
                    model.ReceiptTypeExtraText = Convert.ToString(ds.Tables[0].Rows[0]["ExtraPrinttext"]);
                    model.UserName = Convert.ToString(ds.Tables[0].Rows[0]["UserName"]);
                    model.SchoolHeader = Convert.ToString(ds.Tables[0].Rows[0]["SchoolName"]);
                    model.Affiliationboard = Convert.ToString(ds.Tables[0].Rows[0]["AffiliationBoard"]);
                    model.AffiliationNo = Convert.ToString(ds.Tables[0].Rows[0]["AffiliationNo"]);
                    model.DisplayAddress = Convert.ToString(ds.Tables[0].Rows[0]["DisplayAddress"]);
                    model.DisplayPhone = Convert.ToString(ds.Tables[0].Rows[0]["DisplayPhone"]);
                    model.ReceiptTemplate = Convert.ToString(ds.Tables[0].Rows[0]["ReceiptTemaplate"]);

                    model.RegistrationNo = Convert.ToString(ds.Tables[0].Rows[0]["RegistrationNo"]);
                    if (Convert.ToString(ds.Tables[0].Rows[0]["IsPrintSchoolCopy"]) == "1")
                        model.IsPrintSchoolCopy = true;
                    else
                        model.IsPrintSchoolCopy = false;
                    if (Convert.ToString(ds.Tables[0].Rows[0]["IsPrintStudentCopy"]) == "1")
                        model.IsPrintStudentCopy = true;
                    else
                        model.IsPrintStudentCopy = false;
                    if (Convert.ToString(ds.Tables[0].Rows[0]["FeePrintModeLandScapeOrPortrait"]) == "1")
                        model.PrintLandScapeOrPortrait = true;
                    else
                        model.PrintLandScapeOrPortrait = false;
                    if (Convert.ToString(ds.Tables[0].Rows[0]["FeeReceipt_ShowFeeReceiptTypeinPrint"]) == "1")
                        model.ShowReceiptType = true;
                    else
                        model.ShowReceiptType = false;
                    if (Convert.ToString(ds.Tables[0].Rows[0]["FeeReceipt_showCopytext"]) == "1")
                        model.ShowCopyText = true;
                    else
                        model.ShowCopyText = false;
                    var count = 0;
                    var includeheader = GetAllReceiptTypes(ref count).Where(m => (m.ReceiptType1.ToLower()) == model.ReceiptType.ToLower()).FirstOrDefault().IncludeHeader != null ? GetAllReceiptTypes(ref count).Where(m => (m.ReceiptType1.ToLower()) == model.ReceiptType.ToLower()).FirstOrDefault().IncludeHeader : false;
                    if ((bool)includeheader)
                        model.Isheaderrequired = true;
                    else
                        model.Isheaderrequired = false;

                    model.HeaderHeight = Convert.ToString(ds.Tables[0].Rows[0]["FeeReceiptHeaderHeight"]);

                    if (ds.Tables[0].Rows[0]["CancelReceipt"] != null)
                    {
                        if (ds.Tables[0].Rows[0]["CancelReceipt"].ToString() == "True")
                        {
                            model.IsCancel = true;
                        }
                        else
                        {
                            model.IsCancel = false;
                        }
                    }
                    else
                    {
                        model.IsCancel = false;
                    }


                }
                decimal TotalAmount = 0;
                decimal TotalDeductions = 0;
                decimal TotalPayable = 0;
                decimal TotalPaidAmount = 0;
                decimal Totalbalance = 0;
                decimal TotalExcessAmount = 0;
                long TotalPaidAmountwords = 0;
                if (ds.Tables[1].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {
                        PrintFeeReceiptHeads heads = new PrintFeeReceiptHeads();
                        heads.HeadName = Convert.ToString(ds.Tables[1].Rows[i]["HeadName"]);
                        heads.HeadAmount = Convert.ToString(ds.Tables[1].Rows[i]["HeadAmount"]);
                        heads.Deduction = Convert.ToString(ds.Tables[1].Rows[i]["Deduction"]);
                        heads.PayableAmount = Convert.ToString(ds.Tables[1].Rows[i]["PayableAmount"]);
                        heads.PaidAmount = Convert.ToString(ds.Tables[1].Rows[i]["PaidAmount"]);
                        heads.Balance = Convert.ToString(ds.Tables[1].Rows[i]["Balance"]);
                        heads.ExcessAmount = Convert.ToString(ds.Tables[1].Rows[i]["ExcessAmount"]);
                        model.PrintFeeReceiptHeadsList.Add(heads);
                        TotalAmount += Convert.ToDecimal(heads.HeadAmount);
                        TotalDeductions += Convert.ToDecimal(heads.Deduction);
                        TotalPayable += Convert.ToDecimal(heads.PayableAmount);
                        TotalPaidAmount += Convert.ToDecimal(heads.PaidAmount);
                        Totalbalance += Convert.ToDecimal(heads.Balance);
                        TotalExcessAmount += Convert.ToDecimal(heads.ExcessAmount);
                        //TotalPaidAmountwords += Convert.ToInt64(heads.PaidAmount);
                    }
                }

                model.TotalAmount = Convert.ToString(TotalAmount);
                model.TotalDiscount = Convert.ToString(TotalDeductions);
                model.TotalPayable = Convert.ToString(TotalPayable);
                model.TotalPaidAmount = Convert.ToString(TotalPaidAmount);
                model.TotalBalance = Convert.ToString(Totalbalance);
                model.TotalExcessAmount = Convert.ToString(TotalExcessAmount);
                //model.TotalPaidAmountInWords = ConvertToWords(model.TotalPaidAmount);
            }
            return model;
        }

        //public string ConvertNumbertoWords(long number)
        //{
        //    if (number == 0) return "ZERO";
        //    if (number < 0) return "minus " + ConvertNumbertoWords(Math.Abs(number));
        //    string words = "";
        //    if ((number / 1000000) > 0)
        //    {
        //        words += ConvertNumbertoWords(number / 100000) + " LAKES ";
        //        number %= 1000000;
        //    }
        //    if ((number / 1000) > 0)
        //    {
        //        words += ConvertNumbertoWords(number / 1000) + " THOUSAND ";
        //        number %= 1000;
        //    }
        //    if ((number / 100) > 0)
        //    {
        //        words += ConvertNumbertoWords(number / 100) + " HUNDRED ";
        //        number %= 100;
        //    }
        //    //if ((number / 10) > 0)  
        //    //{  
        //    // words += ConvertNumbertoWords(number / 10) + " RUPEES ";  
        //    // number %= 10;  
        //    //}  
        //    if (number > 0)
        //    {
        //        if (words != "") words += "AND ";
        //        var unitsMap = new[]   
        //{  
        //    "ZERO", "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE", "TEN", "ELEVEN", "TWELVE", "THIRTEEN", "FOURTEEN", "FIFTEEN", "SIXTEEN", "SEVENTEEN", "EIGHTEEN", "NINETEEN"  
        //};
        //        var tensMap = new[]   
        //{  
        //    "ZERO", "TEN", "TWENTY", "THIRTY", "FORTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINETY"  
        //};
        //        if (number < 20) words += unitsMap[number];
        //        else
        //        {
        //            words += tensMap[number / 10];
        //            if ((number % 10) > 0) words += " " + unitsMap[number % 10];
        //        }
        //    }
        //    return words;
        //}  

        #endregion

        #region"Head wise report"
        public DataTable GetHeadWiseConcessionbySp(ref int count, string FromDate = "", string ToDate = "",
            int ReceipttypeId = 0, int SessionId = 0, int ClassId = 0, int FeeTypeId = 0, int StandardId = 0,
            int AllClassesOption = 0, string PaymentModeIds = "")
        {

            if (!string.IsNullOrEmpty(FromDate))
            {
                DateTime dtt = DateTime.Now;
                //try
                //{ dtt = DateTime.ParseExact(FromDate, "dd/MM/yyyy", null); }
                //catch
                // {
                dtt = DateTime.ParseExact(FromDate, "MM/dd/yyyy", null); //}

                FromDate = dtt.ToString("yyyy-MM-dd");
            }
            if (!string.IsNullOrEmpty(ToDate))
            {
                DateTime dtt = DateTime.Now;
                //try
                //{ dtt = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null); }
                //catch
                //{
                dtt = DateTime.ParseExact(ToDate, "MM/dd/yyyy", null); //}

                ToDate = dtt.ToString("yyyy-MM-dd");
            }
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("Sp_HeadwiseCollectionReport_Updated", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@FromDate", FromDate);
                    sqlComm.Parameters.AddWithValue("@ToDate", ToDate);
                    sqlComm.Parameters.AddWithValue("@ReceiptTypeId", ReceipttypeId);
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@ClassId", ClassId);
                    sqlComm.Parameters.AddWithValue("@FeeTypeId", FeeTypeId);
                    sqlComm.Parameters.AddWithValue("@StandardId", StandardId);
                    sqlComm.Parameters.AddWithValue("@AllClassesOption", AllClassesOption);
                    sqlComm.Parameters.AddWithValue("@PaymentModeIds", PaymentModeIds);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            return dt;
        }

        public DataTable GetPrintHeadWiseConcessionbySp(ref int count, string FromDate = "", string ToDate = "",
            int ReceipttypeId = 0, int SessionId = 0, int ClassId = 0, int FeeTypeId = 0,
            int StandardId = 0, int AllClassesOption = 0, string PaymentModeIds = "")
        {

            if (!string.IsNullOrEmpty(FromDate))
            {
                DateTime dtt = DateTime.Now;
                try
                { dtt = DateTime.ParseExact(FromDate, "dd/MM/yyyy", null); }
                catch
                {
                    dtt = DateTime.ParseExact(FromDate, "MM/dd/yyyy", null);
                }

                FromDate = dtt.ToString("yyyy-MM-dd");
            }
            if (!string.IsNullOrEmpty(ToDate))
            {
                DateTime dtt = DateTime.Now;
                try
                { dtt = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null); }
                catch
                {
                    dtt = DateTime.ParseExact(ToDate, "MM/dd/yyyy", null);
                }

                ToDate = dtt.ToString("yyyy-MM-dd");
            }
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("Sp_HeadwiseCollectionReport_Updated", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@FromDate", FromDate);
                    sqlComm.Parameters.AddWithValue("@ToDate", ToDate);
                    sqlComm.Parameters.AddWithValue("@ReceiptTypeId", ReceipttypeId);
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@ClassId", ClassId);
                    sqlComm.Parameters.AddWithValue("@FeeTypeId", FeeTypeId);
                    sqlComm.Parameters.AddWithValue("@StandardId", StandardId);
                    sqlComm.Parameters.AddWithValue("@AllClassesOption", AllClassesOption);
                    sqlComm.Parameters.AddWithValue("@PaymentModeIds", PaymentModeIds);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            return dt;
        }

        #endregion

        #region "receiptList with new method"
        public List<StudentFeeReceiptModel> GetAllFeeReceiptList(ref int count, string Fromdate = "", string ToDate = "", int ReceipttypeId = 0, int SessionId = 0, int ClassId = 0, int StandardId = 0, int AllClassesOption = 0, string Admnno = "", int ReceiptNo = 0, string Deleteoption = "", string DeleteDays = "", string Deletepermission = "", int ShowCancel = 0)
        {
            List<StudentFeeReceiptModel> ReceiptList = new List<StudentFeeReceiptModel>();

            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("[sp_Fee_FeeReceiptList]", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@FromDate", Fromdate);
                    sqlComm.Parameters.AddWithValue("@ToDate", ToDate);
                    sqlComm.Parameters.AddWithValue("@ReceiptTypeId", ReceipttypeId);
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@ClassId", ClassId);
                    sqlComm.Parameters.AddWithValue("@StandardId", StandardId);
                    sqlComm.Parameters.AddWithValue("@AllClassesOption", AllClassesOption);
                    sqlComm.Parameters.AddWithValue("@Admnno", Admnno);
                    sqlComm.Parameters.AddWithValue("@Receiptno", ReceiptNo);
                    sqlComm.Parameters.AddWithValue("@DeleteOption", Deleteoption);
                    sqlComm.Parameters.AddWithValue("@DeleteDays", DeleteDays);
                    sqlComm.Parameters.AddWithValue("@Deletepermission", Deletepermission);
                    sqlComm.Parameters.AddWithValue("@ShowCancelReceipts", ShowCancel);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }

            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    StudentFeeReceiptModel model = new StudentFeeReceiptModel();
                    model.AdmnNo = Convert.ToString(dt.Rows[i]["Admnno"]);
                    if( dt.Rows[i]["ComReceiptId"].ToString()!="" )
                      model.CompareFeeReceiptId = Convert.ToInt32(dt.Rows[i]["ComReceiptId"]);

                    model.RcptNo = Convert.ToInt32(dt.Rows[i]["ReceiptNo"]);
                    model.StudentName = Convert.ToString(dt.Rows[i]["StudentName"]);
                    model.ClassName = Convert.ToString(dt.Rows[i]["Class"]);
                    model.EncFeeReceiptId = Convert.ToString(dt.Rows[i]["ReceiptId"]);
                    model.FatherName = Convert.ToString(dt.Rows[i]["Guardian"]);
                    if (dt.Rows[i]["PaidFee"].ToString() == "")
                    { model.FeePaid = 0; }
                    else
                    {
                        model.FeePaid = Convert.ToDecimal(dt.Rows[i]["PaidFee"].ToString());
                    }
                    if (dt.Rows[i]["PendingFee"].ToString() == "")
                    {
                        model.FeePending = 0;
                    }
                    else
                    {
                        model.FeePending = Convert.ToDecimal(dt.Rows[i]["PendingFee"].ToString());
                    }
                    model.FeePeriod = Convert.ToString(dt.Rows[i]["FeePeriodId"]).TrimStart(',');
                    model.FeeReceiptId = Convert.ToInt32(dt.Rows[i]["ReceiptId"].ToString());
                    model.FeeType = Convert.ToString(dt.Rows[i]["FeeType"]);
                    model.ReceiptType = Convert.ToString(dt.Rows[i]["ReceiptType"]);
                    model.RcptDate = DateTime.ParseExact(dt.Rows[i]["ReceiptDate"].ToString(), "dd/MM/yyyy", null);
                    model.ReciptDate = Convert.ToString(dt.Rows[i]["ReceiptDate"]);
                    model.PaymentMode = Convert.ToString(dt.Rows[i]["PaymentMode"]);
                    model.Session = Convert.ToString(dt.Rows[i]["Session"]);
                    model.StudentFee = Convert.ToString(dt.Rows[i]["StudentFee"]);
                    model.TransportFee = Convert.ToString(dt.Rows[i]["TransportFee"]);
                    if (dt.Rows[i]["IsPrint"].ToString() == "1")
                        model.IsPrinted = true;
                    else
                        model.IsPrinted = false;
                    //if (dt.Rows[i]["IsDelete"].ToString() == "1")
                    //    model.IsAuthToDelete = true;
                    //else
                    //    model.IsAuthToDelete = false;
                    ReceiptList.Add(model);
                }
            }
            return ReceiptList;

        }

        #endregion

        #region"FeeDayCloseReport"
        public List<FeeDayCloseModel> GetFeeDayCloseReport(ref int totalcount, int UserId = 0, int ReceiptTypeId = 0, string FromDate = "", string ToDate = "")
        {
            List<FeeDayCloseModel> feeCloseList = new List<FeeDayCloseModel>();

            if (!string.IsNullOrEmpty(FromDate))
            {
                DateTime dtt = DateTime.Now;
                dtt = DateTime.ParseExact(FromDate, "MM/dd/yyyy", null); //}
                FromDate = dtt.ToString("yyyy-MM-dd");
            }
            if (!string.IsNullOrEmpty(ToDate))
            {
                DateTime dtt = DateTime.Now;
                dtt = DateTime.ParseExact(ToDate, "MM/dd/yyyy", null); //}
                ToDate = dtt.ToString("yyyy-MM-dd");
            }
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_FeeDayCloseReport", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@FromDate", FromDate);
                    sqlComm.Parameters.AddWithValue("@ToDate", ToDate);
                    sqlComm.Parameters.AddWithValue("@ReceiptTypeId", ReceiptTypeId);
                    sqlComm.Parameters.AddWithValue("@UserId", UserId);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            if (dt.Rows.Count > 0)
            {
                DataRow[] drs = dt.Select("UserName is not null or UserName <> ''");
                int length = drs.Length;
                for (int i = 0; i < length; i++)
                {
                    decimal Totalpayable = 0;
                    decimal TotalDposit = 0;
                    var feeclosemodel = new FeeDayCloseModel();
                    feeclosemodel.UserName = drs[i]["UserName"].ToString();
                    DataRow[] drrecept = dt.Select("Userid=" + drs[i]["Userid"].ToString() + " and ReceiptType is not null or ReceiptType <> ''");
                    int rcptlength = drrecept.Length;
                    for (int j = 0; j < rcptlength; j++)
                    {
                        var feereceipttypemodel = new ReceiptTypeList();
                        feereceipttypemodel.ReceiptTypeId = Convert.ToInt32(drrecept[j]["ReceiptTypeid"].ToString());
                        feereceipttypemodel.ReceiptType = drrecept[j]["ReceiptType"].ToString();
                        feereceipttypemodel.BeforeHeadText = "Before (" + FromDate.ToString() + ")";
                        feereceipttypemodel.AfterHeadtext = "During Selected Period";
                        DataRow[] drdata = dt.Select("Userid=" + drs[i]["Userid"].ToString() + " and ReceiptTypeid=" + drrecept[j]["ReceiptTypeid"].ToString() + " and (PaymentModeid is not null OR PaymentModeid <> '' )");
                        int datalength = drdata.Length;
                        for (int k = 0; k < datalength; k++)
                        {
                            var BeforePeriodList = new BeforePeriodList();
                            var DuringPeriodList = new DuringPeriodList();

                            BeforePeriodList.UserId = Convert.ToInt32(drs[i]["Userid"].ToString());
                            BeforePeriodList.ReceiptTypeId = Convert.ToInt32(drrecept[j]["ReceiptTypeid"].ToString());
                            BeforePeriodList.PaymentModeId = Convert.ToInt32(drdata[k]["PaymentModeid"].ToString());
                            BeforePeriodList.Balance = Convert.ToDecimal(drdata[k]["Beforebalance"].ToString());
                            BeforePeriodList.Collected = Convert.ToDecimal(drdata[k]["BeforeCollected"].ToString());
                            BeforePeriodList.Deposited = Convert.ToDecimal(drdata[k]["BeforeDeposited"].ToString());
                            BeforePeriodList.HeadType = "before";
                            BeforePeriodList.PaymentMode = Convert.ToString(drdata[k]["PaymentMode"]);

                            DuringPeriodList.UserId = Convert.ToInt32(drs[i]["Userid"].ToString());
                            DuringPeriodList.ReceiptTypeId = Convert.ToInt32(drrecept[j]["ReceiptTypeid"].ToString());
                            DuringPeriodList.PaymentModeId = Convert.ToInt32(drdata[k]["PaymentModeid"].ToString());
                            DuringPeriodList.Balance = Convert.ToDecimal(drdata[k]["AfterBalance"].ToString());
                            DuringPeriodList.Collected = Convert.ToDecimal(drdata[k]["AfterCollected"].ToString());
                            DuringPeriodList.Deposited = Convert.ToDecimal(drdata[k]["AfterDeposited"].ToString());
                            DuringPeriodList.HeadType = "after";
                            DuringPeriodList.PaymentMode = Convert.ToString(drdata[k]["PaymentMode"]);
                            DuringPeriodList.PrevBalance = BeforePeriodList.Balance;
                            DuringPeriodList.Payable = BeforePeriodList.Balance + DuringPeriodList.Collected;
                            TotalDposit += DuringPeriodList.Deposited;
                            Totalpayable += DuringPeriodList.Payable;
                            if (BeforePeriodList.Collected > 0 || BeforePeriodList.Balance > 0 || BeforePeriodList.Deposited > 0)
                            {
                                feereceipttypemodel.PreviousList.Add(BeforePeriodList);
                            }
                            if (DuringPeriodList.Collected > 0 || DuringPeriodList.Deposited > 0 || DuringPeriodList.Balance > 0)
                            {
                                feereceipttypemodel.CurrentList.Add(DuringPeriodList);
                            }
                        }

                        feeclosemodel.ReceiptTypeList.Add(feereceipttypemodel);
                    }
                    feeclosemodel.TotalDeposit = TotalDposit;
                    feeclosemodel.TotalPayable = Totalpayable;
                    feeCloseList.Add(feeclosemodel);
                }
            }
            return feeCloseList;
        }
        #endregion

        #region"Fee Installment"
        public List<FeeInstallmentList> GetAllFeeInstallments_Updated(ref int count, string Fromdate = "", string ToDate = "", int SessionId = 0, int ClassId = 0, int StandardId = 0, int AllClassesOption = 0, string Admnno = "")
        {
            List<FeeInstallmentList> InstallmentList = new List<FeeInstallmentList>();

            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_AllFeeInstallmentList", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@FromDate", Fromdate);
                    sqlComm.Parameters.AddWithValue("@ToDate", ToDate);
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@ClassId", ClassId);
                    sqlComm.Parameters.AddWithValue("@StandardId", StandardId);
                    sqlComm.Parameters.AddWithValue("@AllClassesOption", AllClassesOption);
                    sqlComm.Parameters.AddWithValue("@Admnno", Admnno);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var model = new FeeInstallmentList();
                    model.Admnno = Convert.ToString(dt.Rows[i]["AdmnNo"]);
                    model.EncInstallmentId = Convert.ToString(dt.Rows[i]["FeeInstallmentId"]);
                    model.InstallmentDate = Convert.ToDateTime(dt.Rows[i]["InstallmentDate"]);
                    model.InstallmentId = Convert.ToInt32(dt.Rows[i]["FeeInstallmentId"]);
                    if (Convert.ToString(dt.Rows[i]["IsFeepaid"]) == "1")
                        model.IsFeePaid = true;
                    else
                        model.IsFeePaid = false;
                    model.Paid = Convert.ToString(dt.Rows[i]["Paid"]);
                    model.PaidReceiptid = Convert.ToInt32(dt.Rows[i]["PaidReceiptId"]);
                    model.Paybale = Convert.ToString(dt.Rows[i]["Payable"]);
                    model.RceiptId = Convert.ToInt32(dt.Rows[i]["ReceiptId"]);
                    model.StudentName = Convert.ToString(dt.Rows[i]["StudentName"]);
                    InstallmentList.Add(model);
                }
            }

            return InstallmentList;
        }

        public FeeInstallment GetFeeInstallmentById(int InstallmentId = 0)
        {
            var query = db.FeeInstallments.Where(f => f.FeeInstallmentId == InstallmentId).FirstOrDefault();
            return query;
        }


        #endregion

        #region"Fee Security Account"
        public List<SecurityManagementList> GetAllFeeSecurityAccount(ref int count, string Fromdate = "", string ToDate = "", int SessionId = 0, int ClassId = 0, int StandardId = 0, int AllClassesOption = 0, string Admnno = "")
        {
            List<SecurityManagementList> SecurityList = new List<SecurityManagementList>();

            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("spGetAllFeeSecurityAccount", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@FromDate", Fromdate);
                    sqlComm.Parameters.AddWithValue("@ToDate", ToDate);
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@ClassId", ClassId);
                    sqlComm.Parameters.AddWithValue("@StandardId", StandardId);
                    sqlComm.Parameters.AddWithValue("@AllClassesOption", AllClassesOption);
                    sqlComm.Parameters.AddWithValue("@Admnno", Admnno);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var model = new SecurityManagementList();
                    model.ReceiptDetailid = Convert.ToInt32(dt.Rows[i]["ReceiptDetailId"]);
                    model.SecurityAccountid = Convert.ToInt32(dt.Rows[i]["SecurityAccountId"]);
                    model.Admnno = Convert.ToString(dt.Rows[i]["AdmnNo"]);
                    model.Class = Convert.ToString(dt.Rows[i]["Class"]);
                    model.StudentName = Convert.ToString(dt.Rows[i]["StudentName"]);
                    model.Due = Convert.ToDecimal(dt.Rows[i]["Amount"]);
                    model.FoefietAmount = Convert.ToDecimal(dt.Rows[i]["ForfeitAmount"]);
                    if (Convert.ToString(dt.Rows[i]["ForfeitDate"]) == "1900-01-01")
                    { model.FoefietDate = ""; }
                    else
                    {
                        model.FoefietDate = Convert.ToString(dt.Rows[i]["ForfeitDate"]);
                    }
                    model.FoefietReason = Convert.ToString(dt.Rows[i]["ForfeitReason"]);
                    model.Pending = Convert.ToDecimal(dt.Rows[i]["Pending"]);
                    model.Received = Convert.ToDecimal(dt.Rows[i]["PaidAmount"]);
                    model.Refund = Convert.ToDecimal(dt.Rows[i]["RefundAmount"]);
                    if (Convert.ToString(dt.Rows[i]["RefundDate"]) == "1900-01-01")
                    {
                        model.RefundDate = "";
                    }
                    else
                    {
                        model.RefundDate = Convert.ToString(dt.Rows[i]["RefundDate"]);
                    }
                    model.Remarks = Convert.ToString(dt.Rows[i]["Remarks"]);
                    model.RollNo = Convert.ToString(dt.Rows[i]["RollNo"]);
                    model.Status = Convert.ToString(dt.Rows[i]["Status"]);
                    model.StudentType = Convert.ToString(dt.Rows[i]["Studenttype"]);
                    if (dt.Rows[i]["Refunded"] != null)
                    {
                        decimal refund = Convert.ToDecimal(dt.Rows[i]["Refunded"]);
                        if (refund > 0)
                            model.IsRefund = false;
                        else
                            model.IsRefund = true;
                    }
                    SecurityList.Add(model);
                }
            }

            return SecurityList;
        }
        public void InsertFeeSecurityAccount(FeeSecurityAccount Security)
        {
            if (Security == null)
                throw new ArgumentNullException("FeeSecurityAccount");

            db.FeeSecurityAccounts.Add(Security);
            db.SaveChanges();
        }
        public FeeSecurityAccount GetSecurityAccountById(int SecurityAccountId = 0)
        {
            var securityaccount = db.FeeSecurityAccounts.Where(f => f.SecurityAccountId == SecurityAccountId).FirstOrDefault();
            return securityaccount;
        }
        public void UpdateFeeSecurityAccount(FeeSecurityAccount Security)
        {
            if (Security == null)
                throw new ArgumentNullException("FeeSecurityAccount");

            db.Entry(Security).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }
        #endregion

        #region Consession

        public Consession GetConsessionById(int ConsessionId, bool IsTrack = false)
        {
            if (ConsessionId == 0)
                throw new ArgumentNullException("Consession");

            var Consession = new Consession();
            if (IsTrack)
                Consession = (from s in db.Consessions where s.ConsessionId == ConsessionId select s).FirstOrDefault();
            else
                Consession = (from s in db.Consessions.AsNoTracking() where s.ConsessionId == ConsessionId select s).FirstOrDefault();

            return Consession;
        }

        public List<Consession> GetAllConsessions(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Consessions.ToList();

            totalcount = query.Count;
            var Consessions = new PagedList<KSModel.Models.Consession>(query, PageIndex, PageSize);
            return Consessions;
        }

        public void InsertConsession(Consession Consession)
        {
            if (Consession == null)
                throw new ArgumentNullException("Consession");

            db.Consessions.Add(Consession);
            db.SaveChanges();
        }

        public void UpdateConsession(Consession Consession)
        {
            if (Consession == null)
                throw new ArgumentNullException("Consession");

            db.Entry(Consession).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteConsession(int ConsessionId = 0)
        {
            if (ConsessionId == 0)
                throw new ArgumentNullException("Consession");

            var Consession = (from s in db.Consessions where s.ConsessionId == ConsessionId select s).FirstOrDefault();
            db.Consessions.Remove(Consession);
            db.SaveChanges();
        }

        #endregion

        #region FeeBulkData

        public FeeBulkData GetFeeBulkDataById(int FeeBulkDataId, bool IsTrack = false)
        {
            if (FeeBulkDataId == 0)
                throw new ArgumentNullException("FeeBulkData");

            var FeeBulkData = new FeeBulkData();
            if (IsTrack)
                FeeBulkData = (from s in db.FeeBulkDatas where s.FeeBulkDataId == FeeBulkDataId select s).FirstOrDefault();
            else
                FeeBulkData = (from s in db.FeeBulkDatas.AsNoTracking() where s.FeeBulkDataId == FeeBulkDataId select s).FirstOrDefault();

            return FeeBulkData;
        }

        public List<FeeBulkData> GetAllFeeBulkDatas(ref int totalcount, int? SessionId = null, string TrsNo = "", DateTime? TrsDate = null, int? StudentId = null,
            int? ReceiptTypeId = null, int? FeePeriodId = null, int? PaymentModeId = null, decimal? FeeAmount = null, decimal? TptAmount = null,
            string Remarks = "", int? UserId = null, string BankName = "", string InstrumentNo = "", DateTime? InstrumentDate = null,
            bool? Status = null, int? ReceiptId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeeBulkDatas.AsEnumerable();
            if (SessionId != null)
                query = query.Where(x => x.SessionId == SessionId);
            if (PaymentModeId != null)
                query = query.Where(x => x.PaymentModeId == PaymentModeId);
            if (ReceiptTypeId != null)
                query = query.Where(x => x.ReceiptTypeId == ReceiptTypeId);
            if (FeePeriodId != null)
                query = query.Where(x => x.FeePeriodId == FeePeriodId);
            if (TrsDate != null)
                query = query.Where(x => x.TrsDate == TrsDate);
            if (StudentId != null)
                query = query.Where(x => x.StudentId == StudentId);
            if (FeeAmount != null)
                query = query.Where(x => x.PaidAmount == FeeAmount);
            if (UserId != null)
                query = query.Where(x => x.UserId == UserId);
            if (ReceiptId != null)
                query = query.Where(x => x.ReceiptId == ReceiptId);
            if (Status != null)
                query = query.Where(x => x.Status == Status);


            totalcount = query.Count();
            var FeeBulkData = new PagedList<KSModel.Models.FeeBulkData>(query, PageIndex, PageSize, totalcount);
            return FeeBulkData;
        }

        public void InsertFeeBulkData(FeeBulkData FeeBulkData)
        {
            if (FeeBulkData == null)
                throw new ArgumentNullException("FeeBulkData");

            db.FeeBulkDatas.Add(FeeBulkData);
            db.SaveChanges();
        }

        public void UpdateFeeBulkData(FeeBulkData FeeBulkData)
        {
            if (FeeBulkData == null)
                throw new ArgumentNullException("FeeBulkData");

            db.Entry(FeeBulkData).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeBulkData(int FeeBulkDataId = 0)
        {
            if (FeeBulkDataId == 0)
                throw new ArgumentNullException("FeeBulkData");

            var FeeBulkData = (from s in db.FeeBulkDatas where s.FeeBulkDataId == FeeBulkDataId select s).FirstOrDefault();
            db.FeeBulkDatas.Remove(FeeBulkData);
            db.SaveChanges();
        }

        #endregion

        #region FeeBulkDataDetail

        public FeeBulkDataDetail GetFeeBulkDataDetailById(int FeeBulkDataDetailId, bool IsTrack = false)
        {
            if (FeeBulkDataDetailId == 0)
                throw new ArgumentNullException("FeeBulkDataDetail");

            var FeeBulkDataDetail = new FeeBulkDataDetail();
            if (IsTrack)
                FeeBulkDataDetail = (from s in db.FeeBulkDataDetails where s.FeeBulkDataDetailId == FeeBulkDataDetailId select s).FirstOrDefault();
            else
                FeeBulkDataDetail = (from s in db.FeeBulkDataDetails.AsNoTracking() where s.FeeBulkDataDetailId == FeeBulkDataDetailId select s).FirstOrDefault();

            return FeeBulkDataDetail;
        }

        public List<FeeBulkDataDetail> GetAllFeeBulkDataDetails(ref int totalcount, int? FeeBulkDataId = null, int? GroupFeeHeadId = null,
            int? ConsessionId = null, int? FeePeriodId = null, int? AddOnHeadId = null, decimal? Amount = null,
            int? PendingFeePeriodId = null, int? LateFeePeriodId = null, decimal? WaivedOffAmount = null, string Remarks = "",
            int? FeeHeadId = null, decimal? PaidAmount = null, decimal? EWSDiscount = null, decimal? FeeHeadConcession = null,
            decimal? ExcessAdjusted = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeeBulkDataDetails.ToList();
            if (FeeBulkDataId != null)
                query = query.Where(x => x.FeeBulkDataId == FeeBulkDataId).ToList();
            if (GroupFeeHeadId != null)
                query = query.Where(x => x.GroupFeeHeadId == GroupFeeHeadId).ToList();
            if (ConsessionId != null)
                query = query.Where(x => x.ConsessionId == ConsessionId).ToList();
            if (FeePeriodId != null)
                query = query.Where(x => x.FeePeriodId == FeePeriodId).ToList();
            if (AddOnHeadId != null)
                query = query.Where(x => x.AddOnHeadId == AddOnHeadId).ToList();
            if (PendingFeePeriodId != null)
                query = query.Where(x => x.PendingFeePeriodId == PendingFeePeriodId).ToList();
            if (LateFeePeriodId != null)
                query = query.Where(x => x.LateFeePeriodId == LateFeePeriodId).ToList();
            if (FeeHeadId != null)
                query = query.Where(x => x.FeeHeadId == FeeHeadId).ToList();

            totalcount = query.Count;
            var FeeBulkDataDetail = new PagedList<KSModel.Models.FeeBulkDataDetail>(query, PageIndex, PageSize);
            return FeeBulkDataDetail;
        }

        public void InsertFeeBulkDataDetail(FeeBulkDataDetail FeeBulkDataDetail)
        {
            if (FeeBulkDataDetail == null)
                throw new ArgumentNullException("FeeBulkDataDetail");

            db.FeeBulkDataDetails.Add(FeeBulkDataDetail);
            db.SaveChanges();
        }

        public void UpdateFeeBulkDataDetail(FeeBulkDataDetail FeeBulkDataDetail)
        {
            if (FeeBulkDataDetail == null)
                throw new ArgumentNullException("FeeBulkDataDetail");

            db.Entry(FeeBulkDataDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeBulkDataDetail(int FeeBulkDataDetailId = 0)
        {
            if (FeeBulkDataDetailId == 0)
                throw new ArgumentNullException("FeeBulkDataDetail");

            var FeeBulkDataDetail = (from s in db.FeeBulkDataDetails where s.FeeBulkDataDetailId == FeeBulkDataDetailId select s).FirstOrDefault();
            db.FeeBulkDataDetails.Remove(FeeBulkDataDetail);
            db.SaveChanges();
        }

        public void DeleteFeeBulkDetailWithFeeBulkId(int FeeBulkDataId = 0)
        {
            if (FeeBulkDataId == 0)
                throw new ArgumentNullException("FeeBulkDataDetail");

            var getFeeBulkDetail = (from s in db.FeeBulkDataDetails where s.FeeBulkDataId == FeeBulkDataId select s).AsEnumerable();
            foreach (var data in getFeeBulkDetail)
            {
                var FeeBulkDataDetail = (from s in db.FeeBulkDataDetails where s.FeeBulkDataDetailId == data.FeeBulkDataDetailId select s).FirstOrDefault();
                db.FeeBulkDataDetails.Remove(FeeBulkDataDetail);
            }
            db.SaveChanges();
        }
        #endregion

        #region FeeReceiptCancelReason

        public FeeReceiptCancelReason GetFeeReceiptCancelReasonById(int FeeReceiptCancelReasonId, bool IsTrack = false)
        {
            if (FeeReceiptCancelReasonId == 0)
                throw new ArgumentNullException("FeeReceiptCancelReason");

            var FeeReceiptCancelReason = new FeeReceiptCancelReason();
            if (IsTrack)
                FeeReceiptCancelReason = (from s in db.FeeReceiptCancelReasons where s.FeeReceiptCancelReasonId == FeeReceiptCancelReasonId select s).FirstOrDefault();
            else
                FeeReceiptCancelReason = (from s in db.FeeReceiptCancelReasons.AsNoTracking() where s.FeeReceiptCancelReasonId == FeeReceiptCancelReasonId select s).FirstOrDefault();

            return FeeReceiptCancelReason;
        }

        public List<FeeReceiptCancelReason> GetAllFeeReceiptCancelReasons(ref int totalcount,int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeeReceiptCancelReasons.AsEnumerable();
            totalcount = query.Count();
            var FeeReceiptCancelReason = new PagedList<KSModel.Models.FeeReceiptCancelReason>(query, PageIndex, PageSize, totalcount);
            return FeeReceiptCancelReason;
        }

        public void InsertFeeReceiptCancelReason(FeeReceiptCancelReason FeeReceiptCancelReason)
        {
            if (FeeReceiptCancelReason == null)
                throw new ArgumentNullException("FeeReceiptCancelReason");

            db.FeeReceiptCancelReasons.Add(FeeReceiptCancelReason);
            db.SaveChanges();
        }

        public void UpdateFeeReceiptCancelReason(FeeReceiptCancelReason FeeReceiptCancelReason)
        {
            if (FeeReceiptCancelReason == null)
                throw new ArgumentNullException("FeeReceiptCancelReason");

            db.Entry(FeeReceiptCancelReason).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteFeeReceiptCancelReason(int FeeReceiptCancelReasonId = 0)
        {
            if (FeeReceiptCancelReasonId == 0)
                throw new ArgumentNullException("FeeReceiptCancelReason");

            var FeeReceiptCancelReason = (from s in db.FeeReceiptCancelReasons where s.FeeReceiptCancelReasonId == FeeReceiptCancelReasonId select s).FirstOrDefault();
            db.FeeReceiptCancelReasons.Remove(FeeReceiptCancelReason);
            db.SaveChanges();
        }

        #endregion

        public List<GroupFeeHeadConc> GetAllGrouptfeeHeadConsessions(ref int totalcount,
           int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.GroupFeeHeadConcs.ToList();


            totalcount = query.Count;
            var GroupFeeHeadConc = new PagedList<KSModel.Models.GroupFeeHeadConc>(query, PageIndex, PageSize);
            return GroupFeeHeadConc;
        }

        public DataTable GetDueReceiptStudentList(int classId = 0)
        {
            // call store procedure for Selected student fee receipts
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_FeeDueStudentList", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ClassId", classId);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            return dt;

        }
    }

}