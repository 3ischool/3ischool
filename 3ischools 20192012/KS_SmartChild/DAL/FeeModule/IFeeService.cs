﻿using KS_SmartChild.BAL.SPClasses;
using KSModel.Models;
using KS_SmartChild.ViewModel.Fee;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.FeeModule
{
    public partial interface IFeeService
    {
        #region FeeFrequency

        FeeFrequency GetFeeFrequencyById(int FeeFrequencyId, bool IsTrack = false);

        List<FeeFrequency> GetAllFeeFrequencies(ref int totalcount, string FeeFrequency = "", int? NoOfMonths = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeeFrequency(FeeFrequency FeeFrequency);

        void UpdateFeeFrequency(FeeFrequency FeeFrequency);

        void DeleteFeeFrequency(int FeeFrequencyId = 0);

        #endregion

        #region FeeType

        FeeType GetFeeTypeById(int FeeTypeId, bool IsTrack = false);

        List<FeeType> GetAllFeeTypes(ref int totalcount, string FeeType = "", bool? Status = null, int? FeeFrequencyId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        List<FeeType> GetFeeTypesByClass(int[] FeeTypeIds);

        void InsertFeeType(FeeType FeeType);

        void UpdateFeeType(FeeType FeeType);

        void DeleteFeeType(int FeeTypeId = 0);

        #endregion

        #region FeeClassGroup

        FeeClassGroup GetFeeClassGroupById(int FeeClassGroupId, bool IsTrack = false);

        List<FeeClassGroup> GetAllFeeClassGroups(ref int totalcount, string FeeClassGroup = "", int PageIndex = 0,
            int PageSize = int.MaxValue);

        void InsertFeeClassGroup(FeeClassGroup FeeClassGroup);

        void UpdateFeeClassGroup(FeeClassGroup FeeClassGroup);

        void DeleteFeeClassGroup(int FeeClassGroupId = 0);

        #endregion

        #region FeeClassGroupType

        FeeClassGroupType GetFeeClassGroupTypeById(int FeeClassGroupTypeId, bool IsTrack = false);

        List<FeeClassGroupType> GetAllFeeClassGroupTypes(ref int totalcount, int PageIndex = 0,
            int PageSize = int.MaxValue);

        void InsertFeeClassGroupType(FeeClassGroupType FeeClassGroupType);

        void UpdateFeeClassGroupType(FeeClassGroupType FeeClassGroupType);

        void DeleteFeeClassGroupType(int FeeClassGroupTypeId = 0);

        #endregion

        #region FeeClassGroupDetail

        FeeClassGroupDetail GetFeeClassGroupDetailById(int FeeClassGroupDetailId, bool IsTrack = false);

        List<FeeClassGroupDetail> GetAllFeeClassGroupDetails(ref int totalcount, int? FeeClassGroupId = null,
            int? StandardId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeeClassGroupDetail(FeeClassGroupDetail FeeClassGroupDetail);

        void UpdateFeeClassGroupDetail(FeeClassGroupDetail FeeClassGroupDetail);

        void DeleteFeeClassGroupDetails(int FeeClassGroupDetailId = 0);

        #endregion

        #region FeeHeadType

        FeeHeadType GetFeeHeadTypeById(int FeeHeadTypeId, bool IsTrack = false);

        List<FeeHeadType> GetAllFeeHeadTypes(ref int totalcount, string FeeHeadType = "", bool IsAddon = false,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeeHeadType(FeeHeadType FeeHeadType);

        void UpdateFeeHeadType(FeeHeadType FeeHeadType);

        void DeleteFeeHeadType(int FeeHeadTypeId = 0);

        void DeleteReceipt(int ReceiptId = 0);

        #endregion

        #region FeeHead

        FeeHead GetFeeHeadById(int FeeHeadId, bool IsTrack = false);

        List<FeeHead> GetAllFeeHeads(ref int totalcount, int? FeeHeadTypeId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeeHead(FeeHead FeeHead);

        void UpdateFeeHead(FeeHead FeeHead);

        void DeleteFeeHead(int FeeHeadId = 0);

        #endregion

        #region GroupFeeType

        GroupFeeType GetGroupFeeTypeById(int GroupFeeTypeId, bool IsTrack = false);

        List<GroupFeeType> GetAllGroupFeeTypes(ref int totalcount, int? FeeTypeId = null, int? FeeClassGroupId = null, bool? TakeAddOns = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertGroupFeeType(GroupFeeType GroupFeeType);

        void UpdateGroupFeeType(GroupFeeType GroupFeeType);

        void DeleteGroupFeeType(int GroupFeeTypeId = 0);

        #endregion

        #region GroupFeeHead

        GroupFeeHead GetGroupFeeHeadById(int GroupFeeHeadId, bool IsTrack = false);

        List<GroupFeeHead> GetAllGroupFeeHeads(ref int totalcount, int? GroupFeeTypeId = null, int? FeeHeadId = null, DateTime? EffectiveDate = null,
            bool? IsCommon = null, bool? IsPending = null, bool? IsLateFee = null, int PageIndex = 0, int PageSize = int.MaxValue, int? ReceiptTypeFeeHeadId = null, int? ReceiptTypeId = null);

        void InsertGroupFeeHead(GroupFeeHead GroupFeeHead);

        void UpdateGroupFeeHead(GroupFeeHead GroupFeeHead);

        void DeleteGroupFeeHead(int GroupFeeHeadId = 0);

        #endregion

        #region GroupFeeHeadDetail

        GroupFeeHeadDetail GetGroupFeeHeadDetailById(int GroupFeeHeadDetailId, bool IsTrack = false);

        List<GroupFeeHeadDetail> GetAllGroupFeeHeadDetails(ref int totalcount, int? GroupFeeHeadId = null, DateTime? EffectiveDate = null,
            decimal? Amount = null, bool? IsDefault = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertGroupFeeHeadDetail(GroupFeeHeadDetail GroupFeeHeadDetail);

        void UpdateGroupFeeHeadDetail(GroupFeeHeadDetail GroupFeeHeadDetail);

        void DeleteGroupFeeHeadDetail(int GroupFeeHeadDetailId = 0);

        #endregion

        #region FeeReceipt
        DataTable getfeereciept_storedprocedure(int StudentId);
        FeeReceipt GetFeeReceiptById(int FeeReceiptId, bool IsTrack = false);

        List<FeeReceipt> GetAllFeeReceipts(ref int totalcount, int? ReceiptNo = null, DateTime? ReceiptFromDate = null,
            DateTime? ReceiptToDate = null, int? StudentId = null, int? FeePeriodId = null, int? PaymentModeId = null,
            decimal? PaidAmountFrom = null, decimal? PaidAmountTo = null, int?[] StudentIds = null, int?[] PaymentModeIds = null,
            int PageIndex = 0, int PageSize = int.MaxValue, int? ReceiptTypeId = null, int? SessionId = null);

        void InsertFeeReceipt(FeeReceipt FeeReceipt);

        void UpdateFeeReceipt(FeeReceipt FeeReceipt);

        void DeleteFeeReceipt(int FeeReceiptId = 0);

        bool GetFeeReceiptStatus(int GroupFeeheadId = 0, int StudentId = 0, int SessionId = 0);

        Session GetSessionStartDate();

        bool AddonshowStatus(int receiptTypeId = 0);
        decimal? GetRegistrationfee(int StandardId = 0);
        #endregion

        #region FeeReceiptDetail

        FeeReceiptDetail GetFeeReceiptDetailById(int FeeReceiptDetailId, bool IsTrack = false);

        List<FeeReceiptDetail> GetAllFeeReceiptDetails(ref int totalcount, int? ReceiptId = null, int? GroupFeeHeadId = null, int[] groupfeeheadidArray = null,
            decimal? FromAmount = null, decimal? ToAmount = null, int? PrintCount = null, int? ConsessionId = null,
            int PageIndex = 0, int PageSize = int.MaxValue, int? StudentId = null, int? FeePeriodId = null, int? ReceiptTypeId = null, int? SessionId = null);

        List<FeeReceiptDetail> GetAllFeeReceiptDetailsFeePeriod(ref int totalcount, int? ReceiptId = null, int? GroupFeeHeadId = null,
           decimal? FromAmount = null, decimal? ToAmount = null, int? PrintCount = null, int? ConsessionId = null,
           int PageIndex = 0, int PageSize = int.MaxValue, int? StudentId = null, int? FeePeriodId = null, int? ReceiptTypeId = null, int? SessionId = null);

        void InsertFeeReceiptDetail(FeeReceiptDetail FeeReceiptDetail);

        void UpdateFeeReceiptDetail(FeeReceiptDetail FeeReceiptDetail);

        void DeleteFeeReceiptDetail(int FeeReceiptDetailId = 0);

        #endregion

        #region FeePeriod

        FeePeriod GetFeePeriodById(int FeePeriodId, bool IsTrack = false);

        List<FeePeriod> GetAllFeePeriods(ref int totalcount, int? FeeFrequencyId = null, bool? Status = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeePeriod(FeePeriod FeePeriod);

        void UpdateFeePeriod(FeePeriod FeePeriod);

        void DeleteFeePeriod(int FeePeriodId = 0);

        #endregion

        #region FeeReceiptDetailConc

        FeeReceiptDetailConc GetFeeReceiptDetailConcById(int FeeReceiptDetailConcId, bool IsTrack = false);

        List<FeeReceiptDetailConc> GetAllFeeReceiptDetailConcs(ref int totalcount, int? FeeFrequencyId = null, bool? Status = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeeReceiptDetailConc(FeeReceiptDetailConc FeeReceiptDetailConc);

        void UpdateFeeReceiptDetailConc(FeeReceiptDetailConc FeeReceiptDetailConc);

        void DeleteFeeReceiptDetailConc(int FeeReceiptDetailConcId = 0);

        #endregion

        #region PGRequest

        PGRequest GetPGRequestById(int FeePeriodId, bool IsTrack = false);

        List<PGRequest> GetAllPGRequests(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertPGRequest(PGRequest PGRequest);

        void UpdatePGRequest(PGRequest PGRequest);

        void DeletePGRequest(int PGRequestId = 0);

        #endregion

        #region FeeReceiptOnline

        FeeReceiptOnline GetFeeReceiptOnlineById(int FeeReceiptOnlineId, bool IsTrack = false);

        List<FeeReceiptOnline> GetAllFeeReceiptOnlines(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeeReceiptOnline(FeeReceiptOnline FeeReceiptOnline);

        void UpdateFeeReceiptOnline(FeeReceiptOnline FeeReceiptOnline);

        void DeleteFeeReceiptOnline(int FeeReceiptOnlineId = 0);

        #endregion

        #region PGSetup

        PGSetup GetPGSetupById(int PGId, bool IsTrack = false);

        List<PGSetup> GetAllPGSetups(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertPGSetup(PGSetup PGSetup);

        void UpdatePGSetup(PGSetup PGSetup);

        void DeletePGSetup(int PGId = 0);

        #endregion

        #region FeePeriodDetail

        FeePeriodDetail GetFeePeriodDetailById(int FeePeriodDetailId, bool IsTrack = false);

        List<FeePeriodDetail> GetAllFeePeriodDetails(ref int totalcount, int? FeePeriodId = null, DateTime? StartDate = null, DateTime? EndDate = null,
            int PageIndex = 0, int PageSize = int.MaxValue, int? SessionId = null);

        void InsertFeePeriodDetail(FeePeriodDetail FeePeriodDetail);

        void UpdateFeePeriodDetail(FeePeriodDetail FeePeriodDetail);

        void DeleteFeePeriodDetail(int FeePeriodDetailId = 0);

        #endregion

        #region PaymentMode

        PaymentMode GetPaymentModeById(int PaymentModeId, bool IsTrack = false);

        List<PaymentMode> GetAllPaymentModes(ref int totalcount, string PaymentMode = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertPaymentMode(PaymentMode PaymentMode);

        void UpdatePaymentMode(PaymentMode PaymentMode);

        void DeletePaymentMode(int PaymentModeId = 0);

        #endregion

        #region TrsType

        TrsType GetTrsTypeById(int TrsTypeId, bool IsTrack = false);

        List<TrsType> GetAllTrsTypes(ref int totalcount, string TrsType = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTrsType(TrsType TrsType);

        void UpdateTrsType(TrsType TrsType);

        void DeleteTrsType(int TrsTypeId = 0);

        #endregion

        #region Ledger

        Ledger GetLedgerById(int EntryId, bool IsTrack = false);

        List<Ledger> GetAllLedgers(ref int totalcount, DateTime? EntryDate = null, int? AccId = null, int? TrsTypeId = null, decimal? Amount = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertLedger(Ledger Ledger);

        void UpdateLedger(Ledger Ledger);

        void DeleteLedger(int EntryId = 0);

        #endregion

        #region FeeDefaulter
        IList<FeeDefaulterList> GetFeeDefaultersList(ref int count, string arrClassId="", int? ClassId = null, int? feeperiodId = null, bool IsStudentlist = false, int ReceiptTypeId = 0, DateTime? SessionStartDate = null, DateTime? SessionEnddate = null, string ShowEWS = "", string IsApplyTptOwnerShip = "1", string IsSeparateTptreceipt = "0", bool IsExportToExcel = false, decimal feeclassGrouptypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue);
        IList<FeeDefaulterList> GetFeeDefaultersListPreviousSession(ref int count, string arrClassId = "", int? ClassId = null, bool IsStudentlist = false, int ReceiptTypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue);

        #endregion

        #region ReceiptType

        ReceiptType GetReceiptTypeById(int ReceiptTypeId, bool IsTrack = false);


        List<ReceiptType> GetAllReceiptTypes(ref int totalcount, int? ReceiptTypeId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);
        List<ReceiptType> GetAllReceiptTypes_Updated(ref int totalcount, int? ReceiptTypeId = null,
           int PageIndex = 0, int PageSize = int.MaxValue);
        List<ReceiptType> GetAllReceiptTypesWithAddons(ref int totalcount,
         int PageIndex = 0, int PageSize = int.MaxValue);

        List<ReceiptType> GetAllReceiptTypesgroupFeehead(ref int totalcount,
          int PageIndex = 0, int PageSize = int.MaxValue, bool groupfeehead = true);


        void InsertReceiptType(ReceiptType ReceiptType);


        void UpdateReceiptType(ReceiptType ReceiptType);

        void DeleteReceiptTypeGrid(int ProductId);

        void DeleteReceiptType(int ReceiptTypeId = 0);

        List<FeeHead> GetAllMiscHeads(ref int count);

        #endregion

        #region ReceiptTypeFeeHead

        ReceiptTypeFeeHead GetReceiptTypeFeeHeadById(int ReceiptTypeFeeHeadId, bool IsTrack = false);


        List<ReceiptTypeFeeHead> GetAllReceiptTypeFeeHeads(ref int totalcount, int ReceiptTypeId = 0, int FeeHeadId = 0, bool IsActive = false,
            int PageIndex = 0, int PageSize = int.MaxValue);


        void InsertReceiptTypeFeeHead(ReceiptTypeFeeHead ReceiptTypeFeeHead);


        void UpdateReceiptTypeFeeHead(ReceiptTypeFeeHead ReceiptTypeFeeHead);


        void DeleteReceiptTypeFeeHead(int ReceiptTypeFeeHeadId = 0);


        #endregion

        #region FeeCollectionList

        IList<FeeCollectionListmodel> GetFeeCollectionList(ref int count, int? sessionid = null, int? FeeTypeId = null,
            int? ClassId = null, DateTime? fromdate = null, DateTime? todate = null, string PaymentModeIds = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        #endregion

        #region PendingPreviousFee

        FeePrevPendingDetail GetFeePrevPendingById(int FeePrevPendingId, bool IsTrack = false);


        List<FeePrevPendingDetail> GetAllFeePrevPendings(ref int totalcount, int SessionId = 0, int StudenId = 0, int ReceiptId = 0, int ClassId = 0, int ReceiptTypeId = 0,
            int PageIndex = 0, int PageSize = int.MaxValue);

        List<FeePrevPendingReceipt> GetAllPreviousFeeReceipts(ref int totalcount, int FeePrevPendingDetailId = 0, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeePrevPending(FeePrevPending FeePrevPending);

        void InsertFeePrevPendingDetail(FeePrevPendingDetail FeePrevPendingDetail);

        void UpdateFeePrevPending(FeePrevPendingDetail FeePrevPending);


        void DeleteFeePrevPending(int FeePrevPendingId = 0);
        void DeleteFeePrevPendingDetail(int FeePrevPendingDetailId = 0);

        void InsertFeePreviousPendingDetail(FeePrevPendingDetail FeePrevDetail);

        BoardingPoint GetBoardingPointDetailbyId(int BoadringPointId);

        void InsertFeePreviousReceipts(FeePrevPendingReceipt FeePrevPendingReceipt);

        #endregion

        #region FeePending

        FeePending GetFeePendingById(int FeePendingId, bool IsTrack = false);

        List<FeePending> GetAllFeePendings(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeePending(FeePending FeePending);

        void UpdateFeePending(FeePending FeePending);

        void DeleteFeePending(int FeePendingId = 0);

        #endregion

        #region FeePendingReceipt

        FeePendingReceipt GetFeePendingReceiptById(int FeePendingReceiptId, bool IsTrack = false);

        List<FeePendingReceipt> GetAllFeePendingReceipts(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeePendingReceipt(FeePendingReceipt FeePendingReceipt);

        void UpdateFeePendingReceipt(FeePendingReceipt FeePendingReceipt);

        void DeleteFeePendingReceipt(int FeePendingReceiptId = 0);

        #endregion

        #region"TransportFeePendancy"
        IList<TransportFeeInfolist> GetTransportFeePendancy(ref int count, int RouteId = 0, DateTime? FromDate = null, DateTime? ToDate = null);
        IList<TransportRoutewiseList> GetRouteWiseFeeDetail(ref int count, int RouteId = 0, int StudentId = 0, DateTime? FromDate = null, DateTime? ToDate = null);
        IList<TransportRoutewiseList> GetRouteWiseFeeDetailTotal(ref int count, int RouteId = 0, DateTime? FromDate = null, DateTime? ToDate = null);
        #endregion


        #region feeinstallments

        FeeInstallment GetFeeInstallmentById(int FeeInstallmentId, bool IsTrack = false);

        List<FeeInstallment> GetAllFeeInstallments(ref int totalcount, int ReceiptId = 0, int PaidReceiptId = 0, DateTime? InstallmentDate = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeeInstallment(FeeInstallment FeeInstallment);

        void UpdateFeeInstallment(FeeInstallment FeeInstallment);

        void DeleteFeeInstallment(int FeeInstallmentId = 0);

        #endregion

        #region"Donation"
        DonationFeeReceipt GetDonationDetailById(int DonationFeeReceiptId = 0);

        void UpdateDonationFeeReceipt(DonationFeeReceipt DonationFeeReceipt);

        void InsertDonationFeeReceipt(DonationFeeReceipt DonationFeeReceipt);

        List<DonationModel> GetAllDonationsBySp(ref int count, string DonorName = "", DateTime? DonationFromDate = null, DateTime? DonationToDate = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void DeleteDonation(int DonationFeeReceiptId = 0);
        #endregion

        #region"Fee Class Group Concession"
        FeeClassGroupConcessionDetail GetFeeClassGroupConcessionDetailById(int ConcessionDetailid = 0);

        List<FeeClassGroupConcessionModel> GetAllFeeClassGroupConcessionBySp(ref int count, int? FeeClassGroupId = null, int? ConcessionId = null, bool? ApplyToAll = null, int PageIndex = 0, int PageSize = int.MaxValue);

        FeeClassGroupConcession GetFeeclassGroupConcessionById(int groupConcessionId = 0);

        void UpdateFeeClassGroupConcession(FeeClassGroupConcession ClassGroupConcession);

        void UpdateFeeClassGroupConcessionDetail(FeeClassGroupConcessionDetail ClassGroupConcessionDetail);

        void InsertFeeClassGroupConcession(FeeClassGroupConcession ClassGroupConcession);

        void InsertFeeClassGroupConcessionDetail(FeeClassGroupConcessionDetail ClassGroupConcessionDetail);

        void DeleteFeeClassGroupConcessionDetail(int ConcessionDetailId = 0);

        #endregion

        #region "Delete Fee receipt"
        void InsertDeletedFeeReceipt(DeletedFeeReceipt DeletedFeeReceipt);
        void InsertDeletedFeeReceiptDetails(DeletedFeeReceiptDetail DeletedFeeReceiptDetail);
        #endregion


        #region"Fee Calculation with New Method"
        IList<FeeReceiptDetailModel> GetFeeHeadCalculationsBySp(int StudentId = 0, int SessionId = 0, string ReceiptDate = "",
                int FeeTypeId = 0, int FeePeriodId = 0, int ReceiptTypeId = 0, string SessionStartDate = "",
            string SessionEndDate = "", string EWSDetail = "0", string IsApplyTptOwnerShip = "1", string IsSeparateTptreceipt = "0",
            bool OnlyCurrentPeriod = false);
        IList<FeeReceiptDetailModel> GetFeeHeadCalculationsDefaulterBySp(int StudentId = 0, int SessionId = 0, string ReceiptDate = "", int FeeTypeId = 0, int FeePeriodId = 0, int ReceiptTypeId = 0, string SessionStartDate = "", string SessionEndDate = "", string EWSDetail = "0", string IsApplyTptOwnerShip = "1", string IsSeparateTptreceipt = "0");

        #endregion

        #region "Group fee head Concession"
        List<GroupFeeHeadConcession> GetGroupFeeheadConList(ref int count, int GroupFeeHeadDetailId = 0, int concessionid = 0, DateTime? StartDate = null, DateTime? EndDate = null);
        void InsertHeadWiseConcession(GroupFeeHeadConc GrpfeeHeadCon);

        GroupFeeHeadConc GetGroupfeeheadConById(int GroupfeeheadConId = 0);

        void UpdateGroupFeeHeadCon(GroupFeeHeadConc GroupfeeHeadConc);
        void DeleteGroupFeeHeadConcession(int GroupFeeHeadConId = 0);
        #endregion

        #region "View or Print fee Receipt"
        DataTable PrintFeeReceiptCertificateBySp(ref int totalcount, string ReceiptTypeId = "",
                                                                    int? SessionId = null, int? StudentId = null);

        PrintFeeReceiptModel ViewOrPrintFeeReceiptBySp(int ReceiptId = 0, int GroupFeeheadsinreceipt = 0);
        #endregion

        #region"Head wise report"
        DataTable GetHeadWiseConcessionbySp(ref int count, string FromDate = "", string ToDate = "", int ReceipttypeId = 0,
            int SessionId = 0, int ClassId = 0, int FeeTypeId = 0, int StandardId = 0, int AllClassesOption = 0, string PaymentModeIds = "");

        DataTable GetPrintHeadWiseConcessionbySp(ref int count, string FromDate = "", string ToDate = "", int ReceipttypeId = 0,
                                                    int SessionId = 0, int ClassId = 0, int FeeTypeId = 0, int StandardId = 0,
                                                    int AllClassesOption = 0, string PaymentModeIds = "");

        #endregion

        #region "receiptList with new method"
        List<StudentFeeReceiptModel> GetAllFeeReceiptList(ref int count, string Fromdate = "", string ToDate = "", int ReceipttypeId = 0, int SessionId = 0, int ClassId = 0, int StandardId = 0, int AllClassesOption = 0, string Admnno = "", int ReceiptNo = 0, string Deleteoption = "", string DeleteDays = "", string Deletepermission = "", int ShowCancel = 0);

        #endregion

        #region"FeeDayCloseReport"
        List<FeeDayCloseModel> GetFeeDayCloseReport(ref int totalcount, int UserId = 0, int ReceiptTypeId = 0, string FromDate = "", string ToDate = "");
        #endregion

        #region"Fee Installment"
        List<FeeInstallmentList> GetAllFeeInstallments_Updated(ref int count, string Fromdate = "", string ToDate = "", int SessionId = 0, int ClassId = 0, int StandardId = 0, int AllClassesOption = 0, string Admnno = "");
        FeeInstallment GetFeeInstallmentById(int InstallmentId = 0);
        #endregion

        #region"Fee Security Account"
        List<SecurityManagementList> GetAllFeeSecurityAccount(ref int count, string Fromdate = "", string ToDate = "", int SessionId = 0, int ClassId = 0, int StandardId = 0, int AllClassesOption = 0, string Admnno = "");
        void InsertFeeSecurityAccount(FeeSecurityAccount Security);
        FeeSecurityAccount GetSecurityAccountById(int SecurityAccountId = 0);
        void UpdateFeeSecurityAccount(FeeSecurityAccount Security);
        #endregion

        #region Consession

        Consession GetConsessionById(int ConsessionId, bool IsTrack = false);

        List<Consession> GetAllConsessions(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertConsession(Consession Consession);

        void UpdateConsession(Consession Consession);

        void DeleteConsession(int ConsessionId = 0);

        List<GroupFeeHeadConc> GetAllGrouptfeeHeadConsessions(ref int totalcount,
          int PageIndex = 0, int PageSize = int.MaxValue);
        #endregion

        #region FeeBulkData

        FeeBulkData GetFeeBulkDataById(int FeeBulkDataId, bool IsTrack = false);

        List<FeeBulkData> GetAllFeeBulkDatas(ref int totalcount, int? SessionId = null, string TrsNo = "", DateTime? TrsDate = null, int? StudentId = null,
            int? ReceiptTypeId = null, int? FeePeriodId = null, int? PaymentModeId = null, decimal? FeeAmount = null, decimal? TptAmount = null,
            string Remarks = "", int? UserId = null, string BankName = "", string InstrumentNo = "", DateTime? InstrumentDate = null,
            bool? Status = null, int? ReceiptId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeeBulkData(FeeBulkData FeeBulkData);

        void UpdateFeeBulkData(FeeBulkData FeeBulkData);

        void DeleteFeeBulkData(int FeeBulkDataId = 0);

        #endregion

        #region FeeBulkDataDetail

        FeeBulkDataDetail GetFeeBulkDataDetailById(int FeeBulkDataDetailId, bool IsTrack = false);

        List<FeeBulkDataDetail> GetAllFeeBulkDataDetails(ref int totalcount, int? FeeBulkDataId = null, int? GroupFeeHeadId = null,
            int? ConsessionId = null, int? FeePeriodId = null, int? AddOnHeadId = null, decimal? Amount = null,
            int? PendingFeePeriodId = null, int? LateFeePeriodId = null, decimal? WaivedOffAmount = null, string Remarks = "",
            int? FeeHeadId = null, decimal? PaidAmount = null, decimal? EWSDiscount = null, decimal? FeeHeadConcession = null,
            decimal? ExcessAdjusted = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeeBulkDataDetail(FeeBulkDataDetail FeeBulkDataDetail);

        void UpdateFeeBulkDataDetail(FeeBulkDataDetail FeeBulkDataDetail);

        void DeleteFeeBulkDataDetail(int FeeBulkDataDetailId = 0);

        void DeleteFeeBulkDetailWithFeeBulkId(int FeeBulkDataId = 0);

        #endregion

        #region FeeReceiptCancelReason

        FeeReceiptCancelReason GetFeeReceiptCancelReasonById(int FeeReceiptCancelReasonId, bool IsTrack = false);

        List<FeeReceiptCancelReason> GetAllFeeReceiptCancelReasons(ref int totalcount,int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertFeeReceiptCancelReason(FeeReceiptCancelReason FeeReceiptCancelReason);

        void UpdateFeeReceiptCancelReason(FeeReceiptCancelReason FeeReceiptCancelReason);

        void DeleteFeeReceiptCancelReason(int FeeReceiptCancelReasonId = 0);

        #endregion
        DataTable GetDueReceiptStudentList(int classId = 0);
    }
}
