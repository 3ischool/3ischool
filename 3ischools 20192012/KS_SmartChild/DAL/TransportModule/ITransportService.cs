﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.TransportModule
{
    public partial interface ITransportService
    {
        #region TptType

        TptType GetTptTypeById(int TptTypeId, bool IsTrack = false);

        List<TptType> GetAllTptTypes(ref int totalcount, string TptType = "", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTptType(TptType TptType);

        void UpdateTptType(TptType TptType);

        void DeleteTptType(int TptTypeId = 0);

        #endregion

        #region TptTypeDetail

        TptTypeDetail GetTptTypeDetailById(int TptTypeDetailId, bool IsTrack = false);

        List<TptTypeDetail> GetAllTptTypeDetails(ref int totalcount, int? TptTypeId = null, string TptName = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTptTypeDetail(TptTypeDetail TptTypeDetail);

        void UpdateTptTypeDetail(TptTypeDetail TptTypeDetail);

        void DeleteTptTypeDetail(int TptTypeDetailId = 0);

        #endregion

        #region TptFacility

        TptFacility GetTptFacilityById(int TptFacilityId, bool IsTrack = false);

        List<TptFacility> GetAllTptFacilitys(ref int totalcount, int? TptTypeDetailId = null, int? BusRoutrId = null,
            int? BoardingPointId = null, int? VehicletypeId = null, string VehicleNo = "", bool? ParkingRequired = null,
            int? StudentId = null, bool? Status = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTptFacility(TptFacility TptFacility);

        void UpdateTptFacility(TptFacility TptFacility);

        void DeleteTptFacility(int TptFacilityId = 0);

        #endregion

        #region OwnershipTpt

        List<TptOwnerShip> GetAllTptOwnerShips(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        #endregion

        #region VehicleType

        VehicleType GetVehicleTypeById(int VehicleTypeId, bool IsTrack = false);

        List<VehicleType> GetAllVehicleTypes(ref int totalcount, string VehicleType = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertVehicleType(VehicleType VehicleType);

        void UpdateVehicleType(VehicleType VehicleType);

        void DeleteVehicleType(int VehicleTypeId = 0);

        #endregion

    }
}
