﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSModel.Models;
using KS_SmartChild.DAL.Security;
using KS_SmartChild.DAL.Common;
using KS_SmartChild.ViewModel.User;
using System.Data;
using KS_SmartChild.ViewModel.Security;

namespace KS_SmartChild.DAL.UserModule
{
    public partial interface IUserService
    {
        #region  SchoolUser

        SchoolUser GetUserById(int UserId, bool IsTrack = false);

        List<SchoolUser> GetAllUsers(ref int totalcount, DateTime? regdDate = null, string username = null,
            int? UserTypeId = null, int? UserContactId = null, bool? status = null, int? userinfoId = null, bool IsDeleted = false,
            int PageIndex = 0, int PageSize = int.MaxValue);

        dynamic GetAllUsersBuUserType(ref int totalcount, int UserTypeId, DateTime? regdDate = null, string username = null,
           int? UserContactId = null, bool? status = null, int? userinfoId = null,
           int PageIndex = 0, int PageSize = int.MaxValue);

        SchoolUser GetUserByEmail(string email);

        UserLoginResults ValidateUserWithLoginCheck(out SchoolUser User, out UserLoginCheck getLoginCheck, out ContactType usertype, string usernameOrEmail = "", string password = "");

        List<UserListModel> GetRegisteredUserBySP(ref int totalcount, string UserName = "", int? UserTypeId = null,
            int? UserRoleId = null, string PhoneNumber = "", bool? Status_Id = null, int? ClassId = null, int? SessionId = null);

        ChangePasswordResult ChangePassword(ChangePasswordRequest request);

        void InsertUser(SchoolUser User);

        void UpdateUser(SchoolUser User);

        void DeleteUser(int UserId = 0);

        #endregion

        #region UserLoginInfo

        UserLoginInfo GetUserLoginInfoById(int UserLoginInfoId, bool IsTrack = false);

        List<UserLoginInfo> GetAllUserLoginInfos(ref int totalcount, int? userId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertUserLoginInfo(UserLoginInfo UserLoginInfo);

        void UpdateUserLoginInfo(UserLoginInfo UserLoginInfo);

        void DeleteUserLoginInfo(int UserLoginInfoId = 0);

        #endregion

        #region UserRole

        UserRole GetUserRoleById(int UserRoleId, bool IsTrack = false);

        List<UserRole> GetAllUserRoles(ref int totalcount, string UserRole = null, bool? IsActive = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertUserRole(UserRole UserRole);

        void UpdateUserRole(UserRole UserRole);

        void DeleteUserRole(int UserRoleId = 0);

        #endregion

        #region UserRoleContactType

        UserRoleContactType GetUserRoleContactTypeById(int UserRoleContactTypeId, bool IsTrack = false);

        List<UserRoleContactType> GetAllUserRoleContactTypes(ref int totalcount, int? UserRoleId = null,
            int? ContactTypeId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertUserRoleContactType(UserRoleContactType UserRoleContactType);

        void UpdateUserRoleContactType(UserRoleContactType UserRoleContactType);

        void DeleteUserRoleContactType(int UserRoleContactTypeId = 0);

        #endregion

        #region UserLoginCheck

        UserLoginCheck GetUserLoginCheckById(int UserLoginCheckId, bool IsTrack = false);

        List<UserLoginCheck> GetAllUserLoginChecks(ref int totalcount, string IP = "",
                     int? UserId = null, DateTime? Datefrom = null, DateTime? DateTill = null, DateTime? TimeFrom = null,
                      DateTime? TimeTill = null, int PageIndex = 0, int PageSize = int.MaxValue);

        List<UserLoginCheckViewModel> GetAllUserLoginCheckBySPs(ref int totalcount, int? UserTypeId = null,
            string UserIds = "", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertUserLoginCheck(UserLoginCheck UserLoginCheck);

        void UpdateUserLoginCheck(UserLoginCheck UserLoginCheck);

        void DeleteUserLoginCheck(int UserLoginCheckId = 0);

        #endregion

        #region UserDevice

        UserDevice GetUserDeviceById(int UserDeviceId, bool IsTrack = false);

        List<UserDevice> GetAllUserDevices(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertUserDevice(UserDevice UserDevice);

        void UpdateUserDevice(UserDevice UserDevice);

        void DeleteUserDevice(int UserDeviceId = 0);

        #endregion

        #region UserPwdHistory

        UserPwdHistory GetUserPwdHistoryById(int UserPwdHistoryId, bool IsTrack = false);

        List<UserPwdHistory> GetAllUserPwdHistorys(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertUserPwdHistory(UserPwdHistory UserPwdHistory);

        void UpdateUserPwdHistory(UserPwdHistory UserPwdHistory);

        void DeleteUserPwdHistory(int UserPwdHistoryId = 0);

        #endregion

    }
}
