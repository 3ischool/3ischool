﻿using KS_SmartChild.DAL.Common;
using KS_SmartChild.DAL.Security;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using KS_SmartChild.DAL.AddressModule;
using KS_SmartChild.DAL.StudentModule;
using KS_SmartChild.DAL.GuardianModule;
using KS_SmartChild.DAL.StaffModule;
using Microsoft.Office;
using System.Data.SqlClient;
using System.Data;
using KS_SmartChild.ViewModel.User;
using System.Net;
using KS_SmartChild.ViewModel.Security;
using KS_SmartChild.DAL.SettingService;

namespace KS_SmartChild.DAL.UserModule
{
    public partial class UserService : IUserService
    {
        public int count;
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }
        private KS_ChildEntities Context;
        public  KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        //private KS_ChildEntities db
        //{
            
        //    get
        //    {
        //        if (db.Entry(DataSource).Entity == null)
        //        return new KS_ChildEntities(DataSource);
        //        else
        //        {
        //            return db;
        //        }
        //    }
        //}
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IEncryptionService _IEncryptionService;
        private readonly IConnectionService _IConnectionService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly IStudentMasterService _IStudentMasterService;
        private readonly IGuardianService _IGuardianService;
        private readonly IStaffService _IStaffService;
        private readonly IStudentService _IStudentService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        //private readonly string SqlDataSource;


        public UserService(IEncryptionService IEncryptionService,
            IConnectionService IConnectionService, IAddressMasterService IAddressMasterService
            , IStudentMasterService IStudentMasterService, IGuardianService IGuardianService, IStaffService IStaffService, IStudentService IStudentService, ISchoolSettingService ISchoolSettingService)
        {
            this._IAddressMasterService = IAddressMasterService;
            this._IStaffService = IStaffService;
            this._IGuardianService = IGuardianService;
            this._IStudentMasterService = IStudentMasterService;
            this._IEncryptionService = IEncryptionService;
            this._IConnectionService = IConnectionService;
            this._IStudentService = IStudentService;
            this._ISchoolSettingService = ISchoolSettingService;
            //HttpContext context = HttpContext.Current;
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }


        #region  Methods

        #region  User

        public KSModel.Models.SchoolUser GetUserById(int UserId, bool IsTrack = false)
        {
            if (UserId == 0)
                throw new ArgumentNullException("User");

            var User = new SchoolUser();
            if (IsTrack)
                User = (from s in db.SchoolUsers where s.UserId == UserId select s).FirstOrDefault();
            else
                User = (from s in db.SchoolUsers.AsNoTracking() where s.UserId == UserId select s).FirstOrDefault();
            return User;
        }

        public List<KSModel.Models.SchoolUser> GetAllUsers(ref int totalcount, DateTime? regdDate = null, string username = null,
            int? UserTypeId = null, int? UserContactId = null, bool? status = null, int? userinfoId = null,bool IsDeleted=false,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SchoolUsers.AsQueryable();
            if (UserTypeId != null && UserTypeId > 0)
                query = query.Where(e => e.UserTypeId == UserTypeId);
            if (UserContactId != null && UserContactId > 0)
                query = query.Where(e => e.UserContactId == UserContactId);
            if (regdDate.HasValue)
                query = query.Where(e => e.RegdDate == regdDate);
            if (!String.IsNullOrEmpty(username))
                query = query.Where(e => e.UserName.ToLower() == username.ToLower());
            if (IsDeleted != true)
                query = query.Where(e => e.IsDeleted !=true );

            query = query.Where(e => e.Status==true);
            if (status != null)
                query = query.Where(e => e.Status == status);

            if (userinfoId != null && userinfoId > 0)
                query = query.Where(e => e.UserContactId == userinfoId);

            
            var Users = new PagedList<KSModel.Models.SchoolUser>(query.ToList(), PageIndex, PageSize);
            totalcount = query.ToList().Count;
            return Users;
        }

         public dynamic GetAllUsersBuUserType(ref int totalcount,int UserTypeId, DateTime? regdDate = null, string username = null,
           int? UserContactId = null, bool? status = null, int? userinfoId = null,
           int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SchoolUsers.AsQueryable();
            if (UserTypeId != null && UserTypeId > 0)
                query = query.Where(e => e.UserTypeId == UserTypeId);
            if (UserContactId != null && UserContactId > 0)
                query = query.Where(e => e.UserContactId == UserContactId);
            if (regdDate.HasValue)
                query = query.Where(e => e.RegdDate == regdDate);
            if (!String.IsNullOrEmpty(username))
                query = query.Where(e => e.UserName.ToLower() == username.ToLower());

            if (status != null)
                query = query.Where(e => e.Status == status);

            if (userinfoId != null && userinfoId > 0)
                query = query.Where(e => e.UserContactId == userinfoId);

            IList<StaffUser> StaffList = new List<StaffUser>();
            IList<StudentUser> StudentList = new List<StudentUser>();
            IList<GuardianUser> GuardianList = new List<GuardianUser>();
            if (query.Count() > 0)
            {
                var ContactType = _IAddressMasterService.GetContactTypeById(UserTypeId);
                if (ContactType.ContactType1.ToLower() == "teacher" || ContactType.ContactType1.ToLower() == "non-teaching")
                { 
                var model = new StaffUser();
                var StaffIds=query.Select(f=>f.UserContactId).ToArray();
                var UserStaff=_IStaffService.GetAllStaffs(ref count).Where(f=>StaffIds.Contains(f.StaffId));
                foreach (var staff in query)
                {
                    model = new StaffUser();
                    //Staff
                    var StaffDetail=UserStaff.Where(f=>f.StaffId==staff.UserContactId).FirstOrDefault();
                    model.UserId =staff.UserId;
                    model.RegdDate =staff.RegdDate ;
                    model.UserName =staff.UserName  ;
                    model.Password =staff.Password;
                    model.Status =staff.Status;
                    model.UserTypeId =staff.UserTypeId;
                    model.UserContactId =staff.UserContactId;
                    model.PwdActivatedOn =staff.PwdActivatedOn;
                    model.PwdActivatedById =staff.PwdActivatedById;
                    model.PwdLastUpatedOn =staff.PwdLastUpatedOn;
                    model.PwdLastUpatedbyId =staff.PwdLastUpatedbyId;
                    model.UserRoleId =staff.UserRoleId;
                    model.DefaultProfileTypeId =staff.DefaultProfileTypeId;
                    model.IsDeleted = staff.IsDeleted;
                    if(StaffDetail!=null){
                    model.StaffId =StaffDetail.StaffId  ;
                    model.FName =StaffDetail.FName;
                    model.LName =StaffDetail.LName  ;
                    model.IsActive =StaffDetail.IsActive;
                    model.StaffTypeId =StaffDetail.StaffTypeId;
                    model.PunchMcId =StaffDetail.PunchMcId;
                    model.EmpCode =StaffDetail.EmpCode  ;
                    model.DesignationId =StaffDetail.DesignationId  ;
                    model.DepartmentId =StaffDetail.DepartmentId;
                    model.AadharCardNo =StaffDetail.AadharCardNo;
                    model.GenderId =StaffDetail.GenderId;
                    model.StaffCategoryId =StaffDetail.StaffCategoryId;
                    model.ContactTypeId =StaffDetail.ContactTypeId;
                    model.DOJ =StaffDetail.DOJ;
                    model.DOL =StaffDetail.DOL;
                    model.DOB =StaffDetail.DOB  ;
                    model.BloodGroupId = StaffDetail.BloodGroupId;
                    }
                    StaffList.Add(model);
                   }
                var StaffUsers = new PagedList<StaffUser>(StaffList.ToList(), PageIndex, PageSize);
                totalcount = query.ToList().Count;
                return StaffUsers;
                }
                if (ContactType.ContactType1.ToLower() == "student")
                {
                    var StudentModel = new StudentUser();
                    var StudentIds = query.Select(f => f.UserContactId).ToArray();
                    var AllStudents = _IStudentService.GetAllStudents(ref count, StudentIdarray: StudentIds);
                        foreach (var student in query)
                        {
                            StudentModel = new StudentUser();
                            var studentDetail = AllStudents.Where(f => f.StudentId == student.UserContactId).FirstOrDefault();
                            StudentModel.UserId = student.UserId;
                            StudentModel.RegdDate = student.RegdDate;
                            StudentModel.UserName = student.UserName;
                            StudentModel.Password = student.Password;
                            StudentModel.Status = student.Status;
                            StudentModel.UserTypeId = student.UserTypeId;
                            StudentModel.UserContactId = student.UserContactId;
                            StudentModel.PwdActivatedOn = student.PwdActivatedOn;
                            StudentModel.PwdActivatedById = student.PwdActivatedById;
                            StudentModel.PwdLastUpatedOn = student.PwdLastUpatedOn;
                            StudentModel.PwdLastUpatedbyId = student.PwdLastUpatedbyId;
                            StudentModel.UserRoleId = student.UserRoleId;
                            StudentModel.DefaultProfileTypeId = student.DefaultProfileTypeId;
                            StudentModel.IsDeleted = student.IsDeleted;
                            if (studentDetail != null)
                            {
                            StudentModel.StudentId = studentDetail.StudentId;
                            StudentModel.FName = studentDetail.FName;
                            StudentModel.LName = studentDetail.LName;
                            StudentModel.DOB = studentDetail.DOB;
                            StudentModel.GenderId=studentDetail.GenderId ;
                            StudentModel.RegNo=studentDetail.RegNo ;
                            StudentModel.RegDate =studentDetail.RegDate  ;
                            StudentModel.AdmnNo =studentDetail.AdmnNo;
                            StudentModel.AdmnDate =studentDetail.AdmnDate;
                            StudentModel.StudentCategoryId =studentDetail.StudentCategoryId;
                            StudentModel.StudentActivityId =studentDetail.StudentActivityId;
                            StudentModel.BloodGroupId =studentDetail.BloodGroupId;
                            StudentModel.NationalityId =studentDetail.NationalityId;
                            StudentModel.CustodyRightId =studentDetail.CustodyRightId;
                            StudentModel.PunchMcId =studentDetail.PunchMcId;
                            StudentModel.Allergies =studentDetail.Allergies;
                            StudentModel.ReligionId =studentDetail.ReligionId;
                            StudentModel.AdmnStandardId =studentDetail.AdmnStandardId;
                            StudentModel.AdmnSessionId =studentDetail.AdmnSessionId;
                            StudentModel.DOJ =studentDetail.DOJ;
                            StudentModel.AdmnStatus =studentDetail.AdmnStatus;
                            StudentModel.AadharCardNo =studentDetail.AadharCardNo;
                            StudentModel.IsOnlineRegistration =studentDetail.IsOnlineRegistration;
                            StudentModel.OnlineRegnEmail =studentDetail.OnlineRegnEmail;
                            StudentModel.IsExisting =studentDetail.IsExisting;
                            StudentModel.AdmissionFeePaid =studentDetail.AdmissionFeePaid;
                            StudentModel.RejectionReason =studentDetail.RejectionReason;
                            StudentModel.IsHostler =studentDetail.IsHostler;
                            StudentModel.MedicalHistory =studentDetail.MedicalHistory;
                            StudentModel.RegularMedicine =studentDetail.RegularMedicine;
                            StudentModel.IsOldStudent =studentDetail.IsOldStudent;
                            StudentModel.SRN =studentDetail.SRN;
                            StudentModel.AadharEnrlNo =studentDetail.AadharEnrlNo;
                            StudentModel.IsStaffChild =studentDetail.IsStaffChild;
                            StudentModel.PortalEnrolNo =studentDetail.PortalEnrolNo;
                            StudentModel.SMSMobileNo =studentDetail.SMSMobileNo;
                            StudentModel.StudentStatusId =studentDetail.StudentStatusId;
                            StudentModel.ApplRegnNo =studentDetail.ApplRegnNo;
                            StudentModel.ParentStaffId =studentDetail.ParentStaffId;
                            StudentModel.RegnFeePayable =studentDetail.RegnFeePayable;
                            StudentModel.RegnFeePaid = studentDetail.RegnFeePaid;
                            }
                            StudentList.Add(StudentModel);
                        }
                        var StudentUsers = new PagedList<StaffUser>(StaffList.ToList(), PageIndex, PageSize);
                        totalcount = query.ToList().Count;
                        return StudentUsers;
                }
                if (ContactType.ContactType1.ToLower() == "guardian")
                {
                    var GuardianModel = new GuardianUser();
                    var GuardianIds = query.Select(f => f.UserContactId).ToArray();
                    var AllGuardians = _IGuardianService.GetAllGuardians(ref count).Where(f => GuardianIds.Contains(f.GuardianId));
                    foreach (var student in query)
                    {
                        GuardianModel = new GuardianUser();
                        var guardianDetail = AllGuardians.Where(f => f.StudentId == student.UserContactId).FirstOrDefault();
                        GuardianModel.UserId = student.UserId;
                        GuardianModel.RegdDate = student.RegdDate;
                        GuardianModel.UserName = student.UserName;
                        GuardianModel.Password = student.Password;
                        GuardianModel.Status = student.Status;
                        GuardianModel.UserTypeId = student.UserTypeId;
                        GuardianModel.UserContactId = student.UserContactId;
                        GuardianModel.PwdActivatedOn = student.PwdActivatedOn;
                        GuardianModel.PwdActivatedById = student.PwdActivatedById;
                        GuardianModel.PwdLastUpatedOn = student.PwdLastUpatedOn;
                        GuardianModel.PwdLastUpatedbyId = student.PwdLastUpatedbyId;
                        GuardianModel.UserRoleId = student.UserRoleId;
                        GuardianModel.DefaultProfileTypeId = student.DefaultProfileTypeId;
                        GuardianModel.IsDeleted = student.IsDeleted;
                        if (guardianDetail != null)
                        {
                        GuardianModel.GuardianId =guardianDetail.GuardianId;
                        GuardianModel.FirstName =guardianDetail.FirstName;
                        GuardianModel.LastName =guardianDetail.LastName;
                        GuardianModel.QualificationId =guardianDetail.QualificationId;
                        GuardianModel.OccupationId =guardianDetail.OccupationId;
                        GuardianModel.GuardianTypeId =guardianDetail.GuardianTypeId;
                        GuardianModel.StudentId =guardianDetail.StudentId;
                        GuardianModel.IncomeSlabId =guardianDetail.IncomeSlabId;
                        GuardianModel.Image =guardianDetail.Image;
                        GuardianModel.AadharNo = guardianDetail.AadharNo;
                        }
                        GuardianList.Add(GuardianModel);
                    }
                    var GuardianUsers = new PagedList<GuardianUser>(GuardianList.ToList(), PageIndex, PageSize);
                    totalcount = query.ToList().Count;
                    return GuardianUsers;
                }
            }
            var Users = new PagedList<KSModel.Models.SchoolUser>(query.ToList(), PageIndex, PageSize);
            totalcount = query.ToList().Count;
            return Users;
        }

        public SchoolUser GetUserByEmail(string email)
        {
            //var query = db.SchoolUsers.Where(u => u.UserName == email).FirstOrDefault();
              var query = db.SchoolUsers.Where(u => u.UserName == email && u.IsDeleted!=true).FirstOrDefault();
            return query;
        }

        public virtual UserLoginResults ValidateUserWithLoginCheck(out SchoolUser User, out UserLoginCheck getLoginCheck, out ContactType usertype, string usernameOrEmail = "", string password = "")
        {
            var AllowLoginUsingOtp = _ISchoolSettingService.GetorSetSchoolData("AllowLoginUsingOtp", "0").AttributeValue;
            //string passord = _IEncryptionService.DecryptText(password);
            User = GetUserByEmail(usernameOrEmail);
            if (User == null)
            {
                var contactid = db.ContactInfoes.Where(m => m.ContactInfo1 == usernameOrEmail).FirstOrDefault() != null ? db.ContactInfoes.Where(m => m.ContactInfo1 == usernameOrEmail).FirstOrDefault().ContactId : 0;
                var contactTypeID = db.ContactInfoes.Where(m => m.ContactInfo1 == usernameOrEmail).FirstOrDefault() != null ? db.ContactInfoes.Where(m => m.ContactInfo1 == usernameOrEmail).FirstOrDefault().ContactTypeId : 0;
                if (contactid > 0 && contactTypeID > 0)
                    User =db.SchoolUsers.Where(u => u.UserContactId == contactid && u.UserTypeId== contactTypeID && u.IsDeleted != true ).FirstOrDefault();
            }

             var user = User;
             //check ths user logg in process
             getLoginCheck = new UserLoginCheck();
             usertype = new ContactType();

            if (user == null)
                return UserLoginResults.CustomerNotExist;
            //only registered can login
            // get user type
            getLoginCheck = GetAllUserLoginChecks(ref count, UserId: user.UserId).FirstOrDefault();
            usertype = _IAddressMasterService.GetContactTypeById((int)user.UserTypeId);
            var PasswordLifeTime = usertype.PwdAgeDays;
            string pwd;
            pwd = _IEncryptionService.EncryptText(password);

            bool isValid = pwd == user.Password;

            UserLoginResults loginresult;
            if (!isValid)
            {

                loginresult = UserLoginResults.WrongPassword;

                if (AllowLoginUsingOtp == "1")
                {
                    switch (loginresult)
                    {
                        case UserLoginResults.WrongPassword:
                            if (password!=null && password!="" && user.LoginOTP == password)
                            {
                                user.LoginOTP = null;
                                UpdateUser(user);
                            }
                            else
                            {
                                return UserLoginResults.WrongPassword;
                            }
                            break;
                    }
                }
                else
                return UserLoginResults.WrongPassword;
            }

            
            bool IsUserRoleActive = (bool)db.UserRoles.Where(p => p.UserRoleId == user.UserRoleId).FirstOrDefault().IsActive;
            if (!IsUserRoleActive)
                return UserLoginResults.InActiveUserRole;

            if (usertype.ContactType1.ToLower() == "student" || usertype.ContactType1.ToLower() == "guardian")
            {
                if (usertype.ContactType1.ToLower() == "student")
                {
                    var studentStatus = _IStudentMasterService.GetAllStudentStatusDetail(ref count, StudentId: user.UserContactId);
                    if (studentStatus.Count > 0)
                    {
                        var status = studentStatus.OrderByDescending(p => p.StatusChangeDate).ThenByDescending(p => p.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus;
                        if (status == "Left")
                            return UserLoginResults.UserLeft;
                        if (status == "Inactive")
                            return UserLoginResults.UserNotActive;
                    }
                }
                //optimization
                else if (usertype.ContactType1.ToLower() == "guardian")
                {
                    var guardian = _IGuardianService.GetGuardianById(GuardianId: (int)user.UserContactId);
                    if (guardian != null)
                    {
                        var studentId = guardian.StudentId;
                        var studentStatus = _IStudentMasterService.GetAllStudentStatusDetail(ref count, StudentId: studentId);
                        if (studentStatus.Count > 0)
                        {
                            var status = studentStatus.OrderByDescending(p => p.StatusChangeDate).ThenByDescending(p => p.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus;
                            if (status == "Left")
                                return UserLoginResults.UserLeft;
                            if (status == "Inactive")
                                return UserLoginResults.UserNotActive;
                        }
                    }
                }

            }
            //optimization
            else if (usertype.ContactType1.ToLower() == "teacher")
            {
                var staff = _IStaffService.GetStaffById(StaffId: (int)user.UserContactId);
                if (staff != null)
                {
                    if (!(bool)staff.IsActive)
                        return UserLoginResults.UserNotActive;
                }
            }

            bool isActive = (bool)user.Status;
            if (!isActive)
                return UserLoginResults.UserNotActive;

         
            // Get the Public IP  Address
            string Publicip = GetIPAddress();
            Console.Write(Publicip);
            if (getLoginCheck != null)
            {
                //    return UserLoginResults.CustomerNotExist;
                if (!string.IsNullOrEmpty(getLoginCheck.IP) || !string.IsNullOrWhiteSpace(getLoginCheck.IP))
                {
                    if (getLoginCheck.IP != Publicip)
                        return UserLoginResults.NotAuthorizedIP;
                }
                if (getLoginCheck.Datefrom != null && getLoginCheck.DateTill != null)
                {
                    if (DateTime.Now.Date >= getLoginCheck.Datefrom && DateTime.Now.Date <= getLoginCheck.DateTill) { }
                    else
                        return UserLoginResults.TryAgainLater;
                }
                if (getLoginCheck.TimeFrom != null && getLoginCheck.TimeTill != null)
                {
                    if (DateTime.Now.TimeOfDay >= getLoginCheck.TimeFrom && DateTime.Now.TimeOfDay <= getLoginCheck.TimeTill) { }
                    else
                        return UserLoginResults.TryAgainLater;
                }
            }
            //optimization
            // UpdateUser(user);

            //check for passwordExpiry
            var userpasworddetail = GetAllUserPwdHistorys(ref count).Where(m => m.UserId == user.UserId).OrderByDescending(m => m.ChangedOn).FirstOrDefault();
            if (userpasworddetail!=null)
            {
                var paswwordAge = DateTime.UtcNow - userpasworddetail.ChangedOn;
                if(paswwordAge.Value.TotalDays> usertype.PwdAgeDays)
                {
                    return UserLoginResults.PasswordExpired;
                }
            }
            else
            {
                var userlogininfo = GetAllUserLoginInfos(ref count).Where(m=>m.UserId==user.UserId).OrderBy(m => m.LoginInfoId).FirstOrDefault();
                if (userlogininfo != null)
                {
                    var paswwordAge = DateTime.UtcNow - userlogininfo.LoginTime;
                    if (paswwordAge.Value.TotalDays > usertype.PwdAgeDays)
                    {
                        return UserLoginResults.PasswordExpired;
                    }
                    //UserPwdHistory userPwdHistory = new UserPwdHistory();
                    //userPwdHistory.
                }
                else
                {

                }

            }

           

            return UserLoginResults.Successful;
        }

        protected string GetIPAddress()
        {
            // Gets the current context
            System.Web.HttpContext context = System.Web.HttpContext.Current;

            // Checks the HTTP_X_FORWARDED_FOR Header (which can be multiple IPs)
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            //If that is not empty
            if (!string.IsNullOrEmpty(ipAddress))
            {
                // Grab the first address
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            // Otherwise use the REMOTE_ADDR Header
            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public virtual ChangePasswordResult ChangePassword(ChangePasswordRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            var result = new ChangePasswordResult();
            if (String.IsNullOrWhiteSpace(request.Email))
            {
                result.AddError("Email Is Not Provided");
                return result;
            }
            if (String.IsNullOrWhiteSpace(request.NewPassword))
            {
                result.AddError("Password Is Not Provided");
                return result;
            }

            var user = GetUserByEmail(request.Email);
            if (user == null)
            {
                result.AddError("Email Not Found");
                return result;
            }

            var requestIsValid = false;
            if (request.ValidateRequest)
            {
                //password
                string oldPwd;
                oldPwd = _IEncryptionService.EncryptText(request.OldPassword);

                bool oldPasswordIsValid = oldPwd == user.Password;
                if (!oldPasswordIsValid)
                    result.AddError("Old Password Doesnt Match");

                if (oldPasswordIsValid)
                    requestIsValid = true;
            }
            else
                requestIsValid = true;

            //at this point request is valid
            if (requestIsValid)
            {
                user.Password = _IEncryptionService.EncryptText(request.NewPassword);
                UpdateUser(user);
            }
            return result;
        }

        public void InsertUser(KSModel.Models.SchoolUser User)
        {
            if (User == null)
                throw new ArgumentNullException("User");

            db.SchoolUsers.Add(User);
            db.SaveChanges();
        }

        public void UpdateUser(KSModel.Models.SchoolUser User)
        {
            if (User == null)
                throw new ArgumentNullException("User");

            db.Entry(User).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteUser(int UserId = 0)
        {
            if (UserId == 0)
                throw new ArgumentNullException("SchoolUser");

            var User = (from s in db.SchoolUsers where s.UserId == UserId select s).FirstOrDefault();
            db.SchoolUsers.Remove(User);
            db.SaveChanges();
        }

        public List<UserListModel> GetRegisteredUserBySP(ref int totalcount, string UserName = "", int? UserTypeId = null,
            int? UserRoleId = null, string PhoneNumber = "", bool? Status_Id = null,int? ClassId = null,int? SessionId = null)
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("GetRegistered_Users", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@UserName", UserName);
                    sqlComm.Parameters.AddWithValue("@ContactTypeId", UserTypeId);
                    sqlComm.Parameters.AddWithValue("@UserRoleId", UserRoleId);
                    sqlComm.Parameters.AddWithValue("@PhoneNumber", PhoneNumber);
                    sqlComm.Parameters.AddWithValue("@ClassId", ClassId);
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@Status", Status_Id);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }

            List<UserListModel> objUserList = new List<UserListModel>();
            if (dt.Rows.Count > 0)
            {
                foreach(DataRow dr in dt.Rows)
                {
                    UserListModel userList = new UserListModel();
                    userList.PhoneNumber = dr["ContactInfo"].ToString();
                    userList.UserId = dr["UserId"].ToString();
                    userList.UserName = dr["UserName"].ToString();
                    userList.RegistrationDate = dr["RegdDate"].ToString();
                    userList.UserRole = dr["UserRole"].ToString();
                    userList.UserType = dr["ContactType"].ToString();
                    userList.UserContactId = Convert.ToInt32(dr["UserContactId"]);
                    userList.Status = dr["Status"].ToString();

                    objUserList.Add(userList);
                }
            }

            return objUserList.ToList();
        }

        #endregion

        #region UserLoginInfo

        public KSModel.Models.UserLoginInfo GetUserLoginInfoById(int UserLoginInfoId, bool IsTrack = false)
        {
            if (UserLoginInfoId == 0)
                throw new ArgumentNullException("UserLoginInfo");

            var UserLoginInfo = new UserLoginInfo();
            if (IsTrack)
                UserLoginInfo = (from s in db.UserLoginInfoes where s.LoginInfoId == UserLoginInfoId select s).FirstOrDefault();
            else
                UserLoginInfo = (from s in db.UserLoginInfoes.AsNoTracking() where s.LoginInfoId == UserLoginInfoId select s).FirstOrDefault();

            return UserLoginInfo;
        }

        public List<KSModel.Models.UserLoginInfo> GetAllUserLoginInfos(ref int totalcount, int? userId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.UserLoginInfoes.ToList();
            if (userId != null && userId > 0)
                query = query.Where(e => e.UserId == userId).ToList();

            totalcount = query.Count;
            var UserLoginInfoes = new PagedList<KSModel.Models.UserLoginInfo>(query, PageIndex, PageSize);
            return UserLoginInfoes;
        }

        public void InsertUserLoginInfo(KSModel.Models.UserLoginInfo UserLoginInfo)
        {
            if (UserLoginInfo == null)
                throw new ArgumentNullException("UserLoginInfo");

            db.UserLoginInfoes.Add(UserLoginInfo);
            db.SaveChanges();
        }

        public void UpdateUserLoginInfo(KSModel.Models.UserLoginInfo UserLoginInfo)
        {
            if (UserLoginInfo == null)
                throw new ArgumentNullException("UserLoginInfo");

            db.Entry(UserLoginInfo).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteUserLoginInfo(int UserLoginInfoId = 0)
        {
            if (UserLoginInfoId == 0)
                throw new ArgumentNullException("UserLoginInfo");

            var UserLoginInfo = (from s in db.UserLoginInfoes where s.LoginInfoId == UserLoginInfoId select s).FirstOrDefault();
            db.UserLoginInfoes.Remove(UserLoginInfo);
            db.SaveChanges();
        }

        #endregion

        #region UserRole

        public UserRole GetUserRoleById(int UserRoleId, bool IsTrack = false)
        {
            if (UserRoleId == 0)
                throw new ArgumentNullException("UserRole");

            var UserRole = new UserRole();
            if (IsTrack)
                UserRole = (from s in db.UserRoles where s.UserRoleId == UserRoleId select s).FirstOrDefault();
            else
                UserRole = (from s in db.UserRoles.AsNoTracking() where s.UserRoleId == UserRoleId select s).FirstOrDefault();

            return UserRole;
        }

        public List<UserRole> GetAllUserRoles(ref int totalcount, string UserRole = null, bool? IsActive = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.UserRoles.ToList();
            if (!string.IsNullOrEmpty(UserRole))
                query = query.Where(e => e.UserRole1.ToLower() == UserRole.ToLower()).ToList();
            if (IsActive != null)
                query = query.Where(e => e.IsActive == IsActive).ToList();

            totalcount = query.Count;
            var UserRoles = new PagedList<KSModel.Models.UserRole>(query, PageIndex, PageSize);
            return UserRoles;
        }

        public void InsertUserRole(UserRole UserRole)
        {
            if (UserRole == null)
                throw new ArgumentNullException("UserRole");

            db.UserRoles.Add(UserRole);
            db.SaveChanges();
        }

        public void UpdateUserRole(UserRole UserRole)
        {
            if (UserRole == null)
                throw new ArgumentNullException("UserRole");

            db.Entry(UserRole).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteUserRole(int UserRoleId = 0)
        {
            if (UserRoleId == 0)
                throw new ArgumentNullException("UserRole");

            var UserRole = (from s in db.UserRoles where s.UserRoleId == UserRoleId select s).FirstOrDefault();
            db.UserRoles.Remove(UserRole);
            db.SaveChanges();
        }

        #endregion

        #region UserRoleContactType

        public UserRoleContactType GetUserRoleContactTypeById(int UserRoleContactTypeId, bool IsTrack = false)
        {
            if (UserRoleContactTypeId == 0)
                throw new ArgumentNullException("UserRoleContactType");

            var UserRoleContactType = new UserRoleContactType();
            if (IsTrack)
                UserRoleContactType = (from s in db.UserRoleContactTypes where s.UserRoleContactTypeId == UserRoleContactTypeId select s).FirstOrDefault();
            else
                UserRoleContactType = (from s in db.UserRoleContactTypes.AsNoTracking() where s.UserRoleContactTypeId == UserRoleContactTypeId select s).FirstOrDefault();

            return UserRoleContactType;
        }

        public List<UserRoleContactType> GetAllUserRoleContactTypes(ref int totalcount, int? UserRoleId = null,
            int? ContactTypeId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.UserRoleContactTypes.ToList();
            if (UserRoleId != null && UserRoleId > 0)
                query = query.Where(e => e.UserRoleId == UserRoleId).ToList();
            if (ContactTypeId != null && ContactTypeId > 0)
                query = query.Where(e => e.ContactTypeId == ContactTypeId).ToList();

            totalcount = query.Count;
            var UserRoleContactTypes = new PagedList<KSModel.Models.UserRoleContactType>(query, PageIndex, PageSize);
            return UserRoleContactTypes;
        }

        public void InsertUserRoleContactType(UserRoleContactType UserRoleContactType)
        {
            if (UserRoleContactType == null)
                throw new ArgumentNullException("UserRoleContactType");

            db.UserRoleContactTypes.Add(UserRoleContactType);
            db.SaveChanges();
        }

        public void UpdateUserRoleContactType(UserRoleContactType UserRoleContactType)
        {
            if (UserRoleContactType == null)
                throw new ArgumentNullException("UserRoleContactType");

            db.Entry(UserRoleContactType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteUserRoleContactType(int UserRoleContactTypeId = 0)
        {
            if (UserRoleContactTypeId == 0)
                throw new ArgumentNullException("UserRoleContactType");

            var UserRoleContactType = (from s in db.UserRoleContactTypes where s.UserRoleContactTypeId == UserRoleContactTypeId select s).FirstOrDefault();
            db.UserRoleContactTypes.Remove(UserRoleContactType);
            db.SaveChanges();
        }

        #endregion

        #region UserLoginCheck

        public UserLoginCheck GetUserLoginCheckById(int UserLoginCheckId, bool IsTrack = false)
        {
            if (UserLoginCheckId == 0)
                throw new ArgumentNullException("UserLoginCheck");

            var UserLoginCheck = new UserLoginCheck();
            if (IsTrack)
                UserLoginCheck = (from s in db.UserLoginChecks where s.UserLoginCheckId == UserLoginCheckId select s).FirstOrDefault();
            else
                UserLoginCheck = (from s in db.UserLoginChecks.AsNoTracking() where s.UserLoginCheckId == UserLoginCheckId select s).FirstOrDefault();

            return UserLoginCheck;
        }

        public List<UserLoginCheck> GetAllUserLoginChecks(ref int totalcount, string IP = "",
            int? UserId = null, DateTime? Datefrom = null, DateTime? DateTill = null, DateTime? TimeFrom = null,
                                        DateTime? TimeTill = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.UserLoginChecks.ToList();

            if (!string.IsNullOrEmpty(IP))
                query = query.Where(e => e.IP == IP).ToList();
            if (UserId != null && UserId > 0)
                query = query.Where(e => e.UserId == UserId).ToList();
            if (Datefrom != null)
                query = query.Where(e => e.Datefrom == Datefrom.Value.Date).ToList();
            if (DateTill != null)
                query = query.Where(e => e.DateTill == DateTill.Value.Date).ToList();
            if (TimeFrom != null)
                query = query.Where(e => e.TimeFrom == TimeFrom.Value.TimeOfDay).ToList();
            if (TimeTill != null)
                query = query.Where(e => e.TimeTill == TimeTill.Value.TimeOfDay).ToList();

            totalcount = query.Count;
            var UserLoginChecks = new PagedList<KSModel.Models.UserLoginCheck>(query, PageIndex, PageSize);
            return UserLoginChecks;
        }


        public List<UserLoginCheckViewModel> GetAllUserLoginCheckBySPs(ref int totalcount, int? UserTypeId = null,
            string UserIds = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            // call store procedure for Advanced serach of books
            System.Data.DataTable dt = new System.Data.DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_getUserLoginChecks", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@UserTypeId", UserTypeId == null ? 0 : UserTypeId);
                    sqlComm.Parameters.AddWithValue("@UserIds", UserIds);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<UserLoginCheckViewModel> GetAllUserLoginList = new List<UserLoginCheckViewModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                UserLoginCheckViewModel checklist = new UserLoginCheckViewModel();
                checklist.UserId = Convert.ToString(dt.Rows[i]["UserId"]);
                checklist.UserContactId = Convert.ToString(dt.Rows[i]["UserContactId"]);
                checklist.UserTypeValue = Convert.ToString(dt.Rows[i]["ContactType"]);
                checklist.UserTypeId = Convert.ToString(dt.Rows[i]["ContactTypeId"]);
                checklist.IP = Convert.ToString(dt.Rows[i]["IP"]);
                checklist.Datefrom = Convert.ToString(dt.Rows[i]["Datefrom"]);
                checklist.DateTill = Convert.ToString(dt.Rows[i]["DateTill"]);
                checklist.TimeFrom = Convert.ToString(dt.Rows[i]["TimeFrom"]);
                checklist.TimeTill = Convert.ToString(dt.Rows[i]["TimeTill"]);
                checklist.UserLoginCheckId = Convert.ToString(dt.Rows[i]["UserLoginCheckId"]);

                GetAllUserLoginList.Add(checklist);
            }

            totalcount = GetAllUserLoginList.Count;

            GetAllUserLoginList = new PagedList<UserLoginCheckViewModel>(GetAllUserLoginList, PageIndex, PageSize);
            return GetAllUserLoginList;
        }

        public void InsertUserLoginCheck(UserLoginCheck UserLoginCheck)
        {
            if (UserLoginCheck == null)
                throw new ArgumentNullException("UserLoginCheck");

            db.UserLoginChecks.Add(UserLoginCheck);
            db.SaveChanges();
        }

        public void UpdateUserLoginCheck(UserLoginCheck UserLoginCheck)
        {
            if (UserLoginCheck == null)
                throw new ArgumentNullException("UserLoginCheck");

            db.Entry(UserLoginCheck).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteUserLoginCheck(int UserLoginCheckId = 0)
        {
            if (UserLoginCheckId == 0)
                throw new ArgumentNullException("UserLoginCheck");

            var UserLoginCheck = (from s in db.UserLoginChecks where s.UserLoginCheckId == UserLoginCheckId select s).FirstOrDefault();
            db.UserLoginChecks.Remove(UserLoginCheck);
            db.SaveChanges();
        }

        #endregion

        #region UserDevice

        public UserDevice GetUserDeviceById(int UserDeviceId, bool IsTrack = false)
        {
            if (UserDeviceId == 0)
                throw new ArgumentNullException("UserDevice");

            var UserDevice = new UserDevice();
            if (IsTrack)
                UserDevice = (from s in db.UserDevices where s.UserDeviceId == UserDeviceId select s).FirstOrDefault();
            else
                UserDevice = (from s in db.UserDevices.AsNoTracking() where s.UserDeviceId == UserDeviceId select s).FirstOrDefault();

            return UserDevice;
        }

        public List<UserDevice> GetAllUserDevices(ref int totalcount,int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.UserDevices.ToList();
            totalcount = query.Count;
            var UserDevices = new PagedList<KSModel.Models.UserDevice>(query, PageIndex, PageSize);
            return UserDevices;
        }


        public void InsertUserDevice(UserDevice UserDevice)
        {
            if (UserDevice == null)
                throw new ArgumentNullException("UserDevice");

            db.UserDevices.Add(UserDevice);
            db.SaveChanges();
        }

        public void UpdateUserDevice(UserDevice UserDevice)
        {
            if (UserDevice == null)
                throw new ArgumentNullException("UserDevice");

            db.Entry(UserDevice).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteUserDevice(int UserDeviceId = 0)
        {
            if (UserDeviceId == 0)
                throw new ArgumentNullException("UserDevice");

            var UserDevice = (from s in db.UserDevices where s.UserDeviceId == UserDeviceId select s).FirstOrDefault();
            db.UserDevices.Remove(UserDevice);
            db.SaveChanges();
        }

        #endregion

        #region UserPwdHistory

        public UserPwdHistory GetUserPwdHistoryById(int UserPwdHistoryId, bool IsTrack = false)
        {
            if (UserPwdHistoryId == 0)
                throw new ArgumentNullException("UserPwdHistory");

            var UserPwdHistory = new UserPwdHistory();
            if (IsTrack)
                UserPwdHistory = (from s in db.UserPwdHistories where s.UserPwdHistoryId == UserPwdHistoryId select s).FirstOrDefault();
            else
                UserPwdHistory = (from s in db.UserPwdHistories.AsNoTracking() where s.UserPwdHistoryId == UserPwdHistoryId select s).FirstOrDefault();

            return UserPwdHistory;
        }

        public List<UserPwdHistory> GetAllUserPwdHistorys(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.UserPwdHistories.ToList();
            totalcount = query.Count;
            var UserPwdHistorys = new PagedList<KSModel.Models.UserPwdHistory>(query, PageIndex, PageSize);
            return UserPwdHistorys;
        }


        public void InsertUserPwdHistory(UserPwdHistory UserPwdHistory)
        {
            if (UserPwdHistory == null)
                throw new ArgumentNullException("UserPwdHistory");

            db.UserPwdHistories.Add(UserPwdHistory);
            db.SaveChanges();
        }

        public void UpdateUserPwdHistory(UserPwdHistory UserPwdHistory)
        {
            if (UserPwdHistory == null)
                throw new ArgumentNullException("UserPwdHistory");

            db.Entry(UserPwdHistory).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteUserPwdHistory(int UserPwdHistoryId = 0)
        {
            if (UserPwdHistoryId == 0)
                throw new ArgumentNullException("UserPwdHistory");

            var UserPwdHistory = (from s in db.UserPwdHistories where s.UserPwdHistoryId == UserPwdHistoryId select s).FirstOrDefault();
            db.UserPwdHistories.Remove(UserPwdHistory);
            db.SaveChanges();
        }

        #endregion

        #endregion


    }
}