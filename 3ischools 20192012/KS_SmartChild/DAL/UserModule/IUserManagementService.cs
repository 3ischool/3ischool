﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.UserModule
{
    public interface IUserManagementService
    {
        #region AppForm

        AppForm GetAppFormById(int AppFormId, bool IsTrack = false);

        List<AppForm> GetAllAppForms(ref int totalcount, string AppForm = "", int? ModuleId = null, int? SortingIndex = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAppForm(AppForm AppForm);

        void UpdateAppForm(AppForm AppForm);

        void DeleteAppForm(int AppFormId = 0);

        #endregion

        #region AppFormException

        AppFormException GetAppFormExceptionById(int AppFormExceptionId, bool IsTrack = false);

        List<AppFormException> GetAllAppFormExceptions(ref int totalcount, string AppFormEvent = "", string AppFormException = "",
            int? AppFormId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAppFormException(AppFormException AppFormException);

        void UpdateAppFormException(AppFormException AppFormException);

        void DeleteAppFormException(int AppFormExceptionId = 0);

        #endregion

        #region AppFormField

        AppFormField GetAppFormFieldById(int AppFormFieldId, bool IsTrack = false);

        List<AppFormField> GetAllAppFormFields(ref int totalcount, string FieldName = "", int? SortingIndex = null,
            int? AppFormId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAppFormField(AppFormField AppFormField);

        void UpdateAppFormField(AppFormField AppFormField);

        void DeleteAppFormField(int AppFormFieldId = 0);

        #endregion

        #region AppFormStep

        AppFormStep GetAppFormStepById(int AppFormStepId, bool IsTrack = false);

        List<AppFormStep> GetAllAppFormSteps(ref int totalcount, int? StepNo = null,
            int? AppFormId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAppFormStep(AppFormStep AppFormStep);

        void UpdateAppFormStep(AppFormStep AppFormStep);

        void DeleteAppFormStep(int AppFormStepId = 0);

        #endregion

        #region AppFormStepDetail

        AppFormStepDetail GetAppFormStepDetailById(int AppFormStepDetailId, bool IsTrack = false);

        List<AppFormStepDetail> GetAllAppFormStepDetails(ref int totalcount, int? NextStepNo = null,
            int? AppFormStepId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAppFormStepDetail(AppFormStepDetail AppFormStepDetail);

        void UpdateAppFormStepDetail(AppFormStepDetail AppFormStepDetail);

        void DeleteAppFormStepDetail(int AppFormStepDetailId = 0);

        #endregion

        #region AppFormInfo

        AppFormInfo GetAppFormInfoById(int AppFormInfoId, bool IsTrack = false);

        List<AppFormInfo> GetAllAppFormInfos(ref int totalcount, int? SortingIndex = null,
            int? AppFormId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAppFormInfo(AppFormInfo AppFormInfo);

        void UpdateAppFormInfo(AppFormInfo AppFormInfo);

        void DeleteAppFormInfo(int AppFormInfoId = 0);

        #endregion

        #region AppFormValidation

        AppFormValidation GetAppFormValidationById(int AppFormValidationId, bool IsTrack = false);

        List<AppFormValidation> GetAllAppFormValidations(ref int totalcount, int? SortingIndex = null, string ValidationName = "",
            int? AppFormId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAppFormValidation(AppFormValidation AppFormValidation);

        void UpdateAppFormValidation(AppFormValidation AppFormValidation);

        void DeleteAppFormValidation(int AppFormValidationId = 0);

        #endregion

        #region AppFormActionType

        AppFormActionType GetAppFormActionTypeById(int AppFormActionTypeId, bool IsTrack = false);

        List<AppFormActionType> GetAllAppFormActionTypes(ref int totalcount, string FormActionType = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAppFormActionType(AppFormActionType AppFormActionType);

        void UpdateAppFormActionType(AppFormActionType AppFormActionType);

        void DeleteAppFormActionType(int AppFormActionTypeId = 0);

        #endregion

        #region AppFormAction

        AppFormAction GetAppFormActionById(int AppFormActionId, bool IsTrack = false);

        List<AppFormAction> GetAllAppFormActions(ref int totalcount, int? FormActionTypeId = null, int? AppFormId = null,
            string ActionName = "", int? SortingIndex = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAppFormAction(AppFormAction AppFormAction);

        void UpdateAppFormAction(AppFormAction AppFormAction);

        void DeleteAppFormAction(int AppFormActionId = 0);

        #endregion

        #region AppFormGrid

        AppFormGrid GetAppFormGridById(int AppFormGridId, bool IsTrack = false);

        List<AppFormGrid> GetAllAppFormGrids(ref int totalcountl, int? AppFormId = null,
            string GridTitle = "", string SubTitlePrefix = "", string SubTitleSuffix = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertAppFormGrid(AppFormGrid AppFormGrid);

        void UpdateAppFormGrid(AppFormGrid AppFormGrid);

        void DeleteAppFormGrid(int AppFormGridId = 0);

        #endregion

        #region UserRoleActionPermission

        UserRoleActionPermission GetUserRoleActionPermissionById(int UserRoleActionPermissionId, bool IsTrack = false);

        IList<UserRoleActionPermission> GetAllUserRoleActionPermissionLessColumns(ref int totalcount, int? UserRoleId = null);

        List<UserRoleActionPermission> GetAllUserRoleActionPermissions(ref int totalcount, int? UserRoleId = null,
            int? AppFormId = null, int? AppFormActionId = null, bool? IsPermitted = null,
            bool? IsForm = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertUserRoleActionPermission(UserRoleActionPermission UserRoleActionPermission);

        void UpdateUserRoleActionPermission(UserRoleActionPermission UserRoleActionPermission);

        void DeleteUserRoleActionPermission(int UserRoleActionPermissionId = 0);

        #endregion

    }
}
