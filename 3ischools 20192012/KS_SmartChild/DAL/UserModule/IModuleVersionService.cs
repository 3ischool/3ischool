﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSModel.Models;

namespace KS_SmartChild.DAL.UserModule
{
    public partial interface IModuleVersionService
    {
        #region  ModuleType

        ModuleType GetModuleTypeById(int ModuleTypeId, bool IsTrack = false);

        List<ModuleType> GetAllModuleTypes(ref int totalcount, string ModuleType = "", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertModuleType(ModuleType ModuleType);

        void UpdateModuleType(ModuleType ModuleType);

        void DeleteModuleType(int ModuleTypeId = 0);

        #endregion

        #region Module

        Module GetModuleById(int ModuleId, bool IsTrack = false);

        List<Module> GetAllModules(ref int totalcount, int? ModuleTypeId = null, string Module = "", bool? status = false,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertModule(Module Module);

        void UpdateModule(Module Module);

        void DeleteModule(int ModuleId = 0);

        #endregion
    }
}
