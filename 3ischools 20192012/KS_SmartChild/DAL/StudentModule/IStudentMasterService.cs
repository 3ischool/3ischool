﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.StudentModule
{
    public partial interface IStudentMasterService
    {
        #region StudentStatu

        StudentStatu GetStudentStatuById(int StudentStatuId, bool IsTrack = false);

        List<StudentStatu> GetAllStudentStatus(ref int totalcount, string Status = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudentStatu(StudentStatu StudentStatu);

        void UpdateStudentStatu(StudentStatu StudentStatu);

        void DeleteStudentStatu(int StudentStatuId = 0);

        #endregion

        #region StudentStatusDetail

        StudentStatusDetail GetStudentStatusDetailById(int StudentStatusDetailId, bool IsTrack = false);


        List<StudentStatusDetail> GetAllStudentStatusDetail(ref int totalcount, int? StudentId = null, DateTime? StatusChangeDate = null, int? StudentStatusId = null, int PageIndex = 0,
           int PageSize = int.MaxValue);


        void InsertStudentStatusDetail(StudentStatusDetail StudentStatusDetail);


        void UpdateStudentStatusDetail(StudentStatusDetail StudentStatusDetail);


        void DeleteStudentStatusDetail(int StudentStatusDetailId = 0);


        #endregion

        #region StudentCategory

        StudentCategory GetStudentCategoryById(int StudentCategoryId, bool IsTrack = false);

        List<StudentCategory> GetAllStudentCategorys(ref int totalcount, string StudentCategory = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudentCategory(StudentCategory StudentCategory);

        void UpdateStudentCategory(StudentCategory StudentCategory);

        void DeleteStudentCategory(int StudentCategoryId = 0);

        #endregion

        #region SiblingInfo

        SiblingInfo GetSiblingInfoById(int SiblingInfoId, bool IsTrack = false);

        List<SiblingInfo> GetAllSiblingInfos(ref int totalcount, int? StudentId = null, int? SiblingId = null, int? RelationId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertSiblingInfo(SiblingInfo SiblingInfo);

        void UpdateSiblingInfo(SiblingInfo SiblingInfo);

        void DeleteSiblingInfo(int SiblingInfoId = 0);

        #endregion

        #region ExtraActivity

        ExtraActivity GetExtraActivityById(int ExtraActivityId, bool IsTrack = false);

        List<ExtraActivity> GetAllExtraActivitys(ref int totalcount, string ExtraActivity = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertExtraActivity(ExtraActivity ExtraActivity);

        void UpdateExtraActivity(ExtraActivity ExtraActivity);

        void DeleteExtraActivity(int ExtraActivityId = 0);

        #endregion

        #region StudentActivity

        StudentActivity GetStudentActivityById(int StudentActivityId, bool IsTrack = false);

        List<StudentActivity> GetAllStudentActivitys(ref int totalcount, int? StudentId = null, int? ExtraActivityId = null,
            DateTime? EffectiveDate = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudentActivity(StudentActivity StudentActivity);

        void UpdateStudentActivity(StudentActivity StudentActivity);

        void DeleteStudentActivity(int StudentActivityId = 0);

        #endregion

        #region studentfullname

        string GetStudentFullName(int studentid);

        #endregion
    }
}
