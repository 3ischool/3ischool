﻿using KSModel.Models;
using KS_SmartChild.ViewModel.Attendence;
using KS_SmartChild.ViewModel.Student;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace KS_SmartChild.DAL.StudentModule
{
    public partial interface IStudentService
    {
        #region Student

        Student GetStudentById(int StudentId, bool IsTrack = false);

        IList<Student> GetAllStudents(ref int totalcount, string Fname = "", string Lname = "", string Fullname = "", DateTime? DOB = null,
            int? GenderId = null, string RegNo = "", DateTime? RegDate = null, string AdmnNo = "", DateTime? AdmnDate = null,
            int? StudentCategoryId = null, int? StudentActivityId = null, int? HouseId = null, int? BloodGroupId = null,
            int? NationalityId = null, int? CustodyId = null, int? PunchMcId = null, int? ReligionId = null,
            string Allergies = "", int? StandardId = null, int? StudentId = null, int?[] StudentIdarray = null, int PageIndex = 0, int PageSize = int.MaxValue);

        IList<Student> GetAllStudentsByIds(ref int totalcount, int[] studentIds = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        IList<Student> GetAllRegisteredStudentBySP(ref int totalcount, int? StudentId = null, int? StandardId = null, string regno = "",
                                string ViewType = "", int? ListType = null, int? OptionId = null, bool? IsViewAll = null, int SessionId = 0
                                , int PageIndex = 0, int PageSize = int.MaxValue);

        DataTable GetStudentsForAttendanceReportMonthWise(ref int totalcount, int? ClassId = null, DateTime? Date = null,
                         int? Month = null, int? CurSessionId = null, string AttendanceStatusIds = "", int? StudentId = null,
                                                        int PageIndex = 0, int PageSize = int.MaxValue);

        List<StudentAttendanceReport> GetStudentsForAttendanceReport(ref int totalcount, int? ClassId = null, DateTime? Date = null,
                       int? Month = null, int? CurSessionId = null, string AttendanceStatusIds = "", int? StudentId = null,
                                                      int PageIndex = 0, int PageSize = int.MaxValue);

        // only registered std
        IList<Student> GetAllRegisteredStudents(int? StudentId = null, int? StandardId = null, string regno = "",
            string ViewType = "", int? ListType = null, int? OptionId = null, bool? IsViewAll = null, int SessionId = 0);

        IList<Student> GetAllAdmitStudents(int? StudentId = null, int? StandardId = null, int? SessionId = null, bool SetAdmnAfterFeePay = false,
            bool record = false);
        IEnumerable<Student> GetAllAdmitStudentsForAssignSections(int? StudentId = null, int? StandardId = null, int? SessionId = null, bool SetAdmnAfterFeePay = false, bool AdmissionFeePayButton = false,
            bool record = false);
        IList<Student> GetAllAdmitStudentsForSections(int? StudentId = null, int? StandardId = null, int? SessionId = null, bool SetAdmnAfterFeePay = false, bool AdmissionFeePayButton = false,
            bool record = false);

        IList<Student> GetAdmittedList(ref int totalcount, string TypeId, int? SessionId = null, int? StandardId = null, int? ClassId = null,
            int? StudentStatusId = null, int? StudentCategoryId = null, DateTime? FromDate = null, DateTime? ToDate = null, bool? IsStaffChild = null, string StudentName = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        IList<StudentGridModel> GetAdmittedListBySP(ref int totalcount, string Type = "", int? SessionId = null, int? StandardId = null,
             int? StudentCategoryId = null, DateTime? FromDate = null, DateTime? ToDate = null, bool? IsStaffChild = null, string StudentName = null,
             bool? IsHostler = null, int PageIndex = 0, int PageSize = int.MaxValue);

        IList<Student> GetStudentList(ref int totalcount, int? TypeId = null, int? SessionId = null, int? StandardId = null, int? ClassId = null,
            int? StudentStatusId = null, int? StudentCategoryId = null, string AdmissionNo = null, int PageIndex = 0, int PageSize = int.MaxValue);
        
        IList<StudentInfoList> GetManageStudentList(ref int totalcount, int? TypeId = null, int? SessionId = null, int? StandardId = null, int[] ClassId = null,
           int? StudentStatusId = null, int? StudentCategoryId = null, string AdmissionNo = null,
            string Hostler = null, string HouseId = null, string AdmissionSessionid = null, string AdvanceSearch = null, int? IsStaffChild = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        IList<StudentGridModel> GetManageStudentListForGirdView(ref int totalcount, int? TypeId = null, int? SessionId = null, int? StandardId = null, int[] ClassId = null,
           int? StudentStatusId = null, int? StudentCategoryId = null, string AdmissionNo = null,
           string Hostler = null, string HouseId = null, string AdvanceSearch = null, int? IsStaffChild = null,
            Session currentsession = null,int? AdmissionId=null,string StudentName=null,
           int PageIndex = 0, int PageSize = int.MaxValue);

        IList<Student> GetStudentsforPromotion(int? StudentId = null, int? PrvSessionId = null, int? CurSessionId = null, int? ClassId = null,
            string Rollno = null, int[] curclassids = null, bool record = false, int nextClssId = 0);

        IList<Student> GetPromotedStudent(int? StudentId = null, int? PrvSessionId = null, int? CurSessionId = null, int? ClassId = null,
           string Rollno = null, int[] curclassids = null, bool record = false, int nextClssId = 0);

        IList<StudentClass> GetStudentsByClass(int? CurSessionId = null, int? ClassId = null, int? OrderId = null, bool record = false);

        DataTable ExportStudentListWithFilter(ref int totalcount, int? TypeId = null, int? SessionId = null,
            int? StandardId = null, string[] ClassId = null,
            int? StudentStatusId = null, int? StudentCategoryId = null, string AdmissionNo = null, string Hostler = null,
            string HouseId = null,int? AdmsnId =null, string AdvanceSearch = null, bool IsColumns = false,
            int PageIndex = 0, int PageSize = int.MaxValue);

        DataTable ExportAdmittedStudentListWithFilter(ref int totalcount, bool? ColumnsOnly = null, string Type = "", int? SessionId = null, int? StandardId = null,
             int? StudentCategoryId = null, DateTime? FromDate = null, DateTime? ToDate = null, bool? IsStaffChild = null,string StudentName=null,
             bool? IsHostler = null, int PageIndex = 0, int PageSize = int.MaxValue);

        DataTable ExportStudentExamCorrection(ref int totalcount, int? StudentId = null, int? SessionId = null, int? ClassId = null
                                                            , int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudent(Student Student);

        void UpdateStudent(Student Student);

        void DeleteStudent(int StudentId = 0);

        int LastStudentId();

        #endregion

        #region PrevSchool

        PrevSchool GetPrevSchoolById(int PrevSchoolId, bool IsTrack = false);

        List<PrevSchool> GetAllPrevSchools(ref int totalcount, string PrevSchoolName = "", int? BoardId = null, int? StandardId = null,
            string Percentage = "", string Grade = "", string Result = "", int? StudentId = null, string Address = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertPrevSchool(PrevSchool PrevSchool);

        void UpdatePrevSchool(PrevSchool PrevSchool);

        void DeletePrevSchool(int PrevSchoolId = 0);

        #endregion

        #region StudentClass

        StudentClass GetStudentClassById(int StudentClassId, bool IsTrack = false);

        IList<StudentClass> GetAllStudentClasss(ref int totalcount, int? SessionId = null, int? ClassId = null, string RollNo = "",
            string BoardRollNo = "", int? HouseId = null, int? StudentId = null, int[] ClassIds = null, int[] StudentIdarray = null,
            int PageIndex = 0, int PageSize = int.MaxValue, bool? Status = false);

        IList<StudentInfoList> GetAllActiveStudentCount(ref int totalcount, int? SessionId = null, int? StudentStatusId = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudentClass(StudentClass StudentClass);

        void UpdateStudentClass(StudentClass StudentClass);

        void DeleteStudentClass(int StudentClassId = 0);

        #endregion

        #region student list by class

        IList<SelectListItem> StudentListbyClassId(int Classid, int? StudentId = null, int? SessionId = null,
            List<int?> Studentidlist = null, bool? AdmnStatus = null, string SortingFor = "", bool? IsHostler = null);
        IList<SelectListItem> StudentListincludingleftbyClassId(int Classid, int? StudentId = null, int? SessionId = null,
            List<int?> Studentidlist = null, bool? AdmnStatus = null, string SortingFor = "");
        IList<SelectListItem> StudentListincludingleftbyStandardId(int StandardId, int? StudentId = null, int? SessionId = null,
            List<int?> Studentidlist = null, bool? AdmnStatus = null, string SortingFor = "");
        IList<SelectListItem> StudentListbyClassIdsibling(int Classid, int? StudentId = null, int? SessionId = null, List<int?> Studentidlist = null, bool? AdmnStatus = null);

        #endregion

        #region StudentClassDetail

        StudentClassDetail GetStudentClassDetailById(int StudentClassDetailId, bool IsTrack = false);

        List<StudentClassDetail> GetStudentClassDetailByStudentClassId(int StudentClassId, int StudentClassDetailsId, bool IsTrack = false);

        List<StudentClassDetail> GetAllStudentClassDetails(ref int totalcount, int? StudentClassId = null, string Height = "",
          int? Weight = null, string Vision_L = "", string Vision_R = "", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudentClassDetail(StudentClassDetail StudentClassDetail);

        void UpdateStudentClassDetail(StudentClassDetail StudentClassDetail);

        void DeleteStudentClassDetail(int StudentClassDetailId = 0);

        #endregion

        #region StudentClassDetail

        StudentExtraDetail GetStudentExtraDetailById(int StudentExtraDetailId, bool IsTrack = false);

        List<StudentExtraDetail> GetAllStudentExtraDetails(ref int totalcount, bool? IsDefault = null, string StudentExtraDetail = "",
           int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudentExtraDetail(StudentExtraDetail StudentExtraDetail);

        void UpdateStudentExtraDetail(StudentExtraDetail StudentExtraDetail);

        void DeleteStudentExtraDetail(int StudentExtraDetailId = 0);

        #endregion

        #region StudentDetailMapping

        StudentDetailMapping GetStudentDetailMappingById(int StudentDetailMappingId, bool IsTrack = false);

        List<StudentDetailMapping> GetAllStudentDetailMappings(ref int totalcount, int? StudentClassDetailsId = null,
            int? StudentExtraDetailId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudentDetailMapping(StudentDetailMapping StudentDetailMapping);

        void UpdateStudentDetailMapping(StudentDetailMapping StudentDetailMapping);

        void DeleteStudentDetailMapping(int StudentDetailMappingId = 0);

        #endregion

        #region"Student ledger"
        IList<LedgerInfo> GetStudentLedgerbySp(ref int count, int UserId = 0, string LedgerType = "", DateTime? FromDate = null, DateTime? ToDate = null, int PageIndex = 0, int PageSize = int.MaxValue);
        #endregion

        #region Student Facility

        // StudentFacility GetStudentFacilityById(int StudentFacilityId, bool IsTrack = false);

        // IList<StudentFacility> GetAllStudentFacilities(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        // void InsertStudentFacility(StudentFacility StudentFacility);

        // void UpdateStudentFacility(StudentFacility StudentFacility);

        // void DeleteStudentFacility(int StudentFacilityId = 0);

        #endregion

        #region Student Facility Details

        StudentFacilityDetail GetStudentFacilityDetailsById(int StudentFacilityDetailsId, bool IsTrack = false);

        IList<StudentFacilityDetail> GetAllStudentFacilityDetails(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudentFacilityDetails(StudentFacilityDetail StudentFacilityDetails);

        void UpdateStudentFacilityDetails(StudentFacilityDetail StudentFacilityDetails);

        void DeleteStudentFacilityDetails(int StudentFacilityDetailsId = 0);

        #endregion

        #region StudentCustodyRight

        StudentCustodyRight GetStudentCustodyRightsById(int StudentCustodyRightsId, bool IsTrack = false);
        

        IList<StudentCustodyRight> GetAllStudentCustodyRights(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);
        

        void InsertStudentCustodyRights(StudentCustodyRight StudentCustodyRights);
        

        void UpdateStudentCustodyRights(StudentCustodyRight StudentCustodyRights);


        void DeleteStudentCustodyRights(int StudentCustodyRightsId = 0);
        



        #endregion

        #region"Section change"
        void InsertChangeSectionDetail(StudentSectionChange SectionChange);

        StudentSectionChange GetSectionChangeDetail(int StudentClassId = 0, int SessionId = 0);

        void DeleteStudentSectionChange(int StudentSectionChangeId = 0);
        #endregion

        #region Hostler's BoardingNumbers

        #region BoardingNos
        BoardingNo GetBoardingNoById(int BoardingNoId, bool IsTrack = false);

        List<BoardingNo> GetAllBoardingNos(ref int totalcount, string BoardingNo = "", bool? IsActive = null, string[] BoradingNos = null,
                                bool IsFiltered = false,int PageIndex = 0, int PageSize = int.MaxValue,bool? IsOrderBy=null);

        void InsertBoardingNo(BoardingNo BoardingNo);

        void UpdateBoardingNo(BoardingNo BoardingNo);

        void DeleteBoardingNo(int BoardingNoId = 0);

        void DeleteBoardingNobyActivityId(int BoardingNoId = 0);

        #endregion

        #region BoardingActivity
        BoardingActivity GetBoardingActivityById(int BoardingActivityId, bool IsTrack = false);

        List<BoardingActivity> GetAllBoardingActivities(ref int totalcount, string BoardingActivity = "", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertBoardingActivity(BoardingActivity BoardingActivity);

        void UpdateBoardingActivity(BoardingActivity BoardingActivity);

        void DeleteBoardingActivity(int BoardingActivityId = 0);

        #endregion

        #region StudentBoardingNo
        StudentBoardingNo GetStudentBoardingNoById(int StudentBoardingNoId, bool IsTrack = false);

        List<StudentBoardingNo> GetAllStudentBoardingNos(ref int totalcount, int? StudentId = null,int? BoardingNoId=null,
                                        int? BoardingActivityId = null, DateTime? ActivityDateTime= null,  int PageIndex = 0, 
                                        int PageSize = int.MaxValue);

        void InsertStudentBoardingNo(StudentBoardingNo BoardingActivity);

        void UpdateStudentBoardingNo(StudentBoardingNo BoardingActivity);

        void DeleteStudentBoardingNo(int StudentBoardingNoId = 0);

        #endregion

        #endregion

        #region Student Absent List
        DataTable GetAllAbsentStudentByDate(ref int totalcount, DateTime? AttendanceDate = null, int? ClassId = null,int? SessionId=null);

        #endregion
    }
}
