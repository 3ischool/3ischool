﻿using KS_SmartChild.BAL.SPClasses;
using KS_SmartChild.DAL.CatalogMaster;
using KS_SmartChild.DAL.Common;
using KSModel.Models;
using KS_SmartChild.ViewModel.Attendence;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace KS_SmartChild.DAL.StudentModule
{
    public partial class StudentAddOnService : IStudentAddOnService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        //private readonly string SqlDataSource;
        public StudentAddOnService(IConnectionService IConnectionService,
            ICatalogMasterService ICatalogMasterService)
        {
            this._IConnectionService = IConnectionService;
            this._ICatalogMasterService =ICatalogMasterService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods

        #region ConsessionType

        public KSModel.Models.ConsessionType GetConsessionTypeById(int ConsessionTypeId, bool IsTrack = false)
        {
            if (ConsessionTypeId == 0)
                throw new ArgumentNullException("ConsessionType");

            var ConsessionType = new ConsessionType();

            if(IsTrack)
                ConsessionType = (from s in db.ConsessionTypes where s.ConsessionTypeId == ConsessionTypeId select s).FirstOrDefault();
            else
                ConsessionType = (from s in db.ConsessionTypes.AsNoTracking() where s.ConsessionTypeId == ConsessionTypeId select s).FirstOrDefault();

            return ConsessionType;
        }

        public List<KSModel.Models.ConsessionType> GetAllConsessionTypes(ref int totalcount, string ConsessionType = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ConsessionTypes.ToList();
            if (!String.IsNullOrEmpty(ConsessionType))
                query = query.Where(e => e.ConsessionType1.ToLower().Contains(ConsessionType.ToLower())).ToList();

            totalcount = query.Count;
            var ConsessionTypes = new PagedList<KSModel.Models.ConsessionType>(query, PageIndex, PageSize);
            return ConsessionTypes;
        }

        public void InsertConsessionType(KSModel.Models.ConsessionType ConsessionType)
        {
            if (ConsessionType == null)
                throw new ArgumentNullException("FeeClassGroup");

            db.ConsessionTypes.Add(ConsessionType);
            db.SaveChanges();
        }

        public void UpdateConsessionType(KSModel.Models.ConsessionType ConsessionType)
        {
            if (ConsessionType == null)
                throw new ArgumentNullException("ConsessionType");

            db.Entry(ConsessionType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteConsessionType(int ConsessionTypeId = 0)
        {
            if (ConsessionTypeId == 0)
                throw new ArgumentNullException("ConsessionType");

            var ConsessionType = (from s in db.ConsessionTypes where s.ConsessionTypeId == ConsessionTypeId select s).FirstOrDefault();
            db.ConsessionTypes.Remove(ConsessionType);
            db.SaveChanges();
        }

        #endregion

        #region ConsessionTag



        public List<KSModel.Models.ConcessionTypeTag> GetAllConcessionTypeTags(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ConcessionTypeTags.ToList();
          
            totalcount = query.Count;
            var ConsessionTypeTags = new PagedList<KSModel.Models.ConcessionTypeTag>(query, PageIndex, PageSize);
            return ConsessionTypeTags;
        }

      

        #endregion

        #region Consession

        public Consession GetConsessionById(int ConsessionId, bool IsTrack = false)
        {
            if (ConsessionId == 0)
                throw new ArgumentNullException("Consession");

            var Consession = new Consession();

            if(IsTrack)
                Consession = (from s in db.Consessions where s.ConsessionId == ConsessionId select s).FirstOrDefault();
            else
                Consession = (from s in db.Consessions.AsNoTracking() where s.ConsessionId == ConsessionId select s).FirstOrDefault();

            return Consession;
        }

        public List<Consession> GetAllConsessions(ref int totalcount, string Consession = "", int? ConsessionTypeId = null,
            decimal? Amount = null, bool? Status = null, int ReceipttypeId = 0, bool GeneralConcession = true, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Consessions.AsNoTracking().ToList();
            if (!String.IsNullOrEmpty(Consession))
                query = query.Where(e => e.Consession1.ToLower().Contains(Consession.ToLower())).ToList();
            if (ConsessionTypeId != null && ConsessionTypeId > 0)
                query = query.Where(e => e.ConsessionTypeId == ConsessionTypeId).ToList();
            if (Amount != null && Amount > 0)
                query = query.Where(e => e.Amount == Amount).ToList();
            if (Status != null)
                query = query.Where(e => e.Status == Status).ToList();
            if (ReceipttypeId > 0)
                query = query.Where(e => e.ReceiptTypeId == ReceipttypeId).ToList();
            if(GeneralConcession==false)
                query=query.Where(e=> e.ConsessionType.ConcessionTypeTag.ConcessionTypeTag1 != "General").ToList();
            else
                query = query.Where(e => e.ConsessionType.ConcessionTypeTag.ConcessionTypeTag1 == "General" || e.ConsessionType.ConcessionTypeTag.ConcessionTypeTag1 == "Other" || e.ConsessionType.ConcessionTypeTag.ConcessionTypeTag1 == "Student Specific").ToList();
            totalcount = query.Count;
            var Consessions = new PagedList<KSModel.Models.Consession>(query, PageIndex, PageSize);
            return Consessions;
        }
        public List<Consession> GetAllConsessionsList(ref int totalcount, string Consession = "", int? ConsessionTypeId = null,
        decimal? Amount = null, bool? Status = null, int ReceipttypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Consessions.AsNoTracking().ToList();
            if (!String.IsNullOrEmpty(Consession))
                query = query.Where(e => e.Consession1.ToLower().Contains(Consession.ToLower())).ToList();
            if (ConsessionTypeId != null && ConsessionTypeId > 0)
                query = query.Where(e => e.ConsessionTypeId == ConsessionTypeId).ToList();
            if (Amount != null && Amount > 0)
                query = query.Where(e => e.Amount == Amount).ToList();
            if (Status != null)
                query = query.Where(e => e.Status == Status).ToList();
            if (ReceipttypeId > 0)
                query = query.Where(e => e.ReceiptTypeId == ReceipttypeId).ToList();
        
            totalcount = query.Count;
            var Consessions = new PagedList<KSModel.Models.Consession>(query, PageIndex, PageSize);
            return Consessions;
        }

        public void InsertConsession(Consession Consession)
        {
            if (Consession == null)
                throw new ArgumentNullException("Consession");

            db.Consessions.Add(Consession);
            db.SaveChanges();
        }

        public void UpdateConsession(Consession Consession)
        {
            if (Consession == null)
                throw new ArgumentNullException("Consession");

            db.Entry(Consession).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteConsession(int ConsessionId = 0)
        {
            if (ConsessionId == 0)
                throw new ArgumentNullException("Consession");

            var Consession = (from s in db.Consessions where s.ConsessionId == ConsessionId select s).FirstOrDefault();
            db.Consessions.Remove(Consession);
            db.SaveChanges();
        }

        public IList<KS_SmartChild.BAL.SPClasses.StudentList> GetConcStudentList(ref int count, string ConsessiontypeTag="",int ConsessionId=0,int SessionId=0, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            DataTable dt = new DataTable();

            if (SessionId == 0)
            {
                SessionId = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.IsDefualt == true).FirstOrDefault().SessionId;
            }
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_AggrGetStdConcList", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ConsessionId", ConsessionId);
                    sqlComm.Parameters.AddWithValue("@ConsessionTypeTag", ConsessiontypeTag);
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            IList<KS_SmartChild.BAL.SPClasses.StudentList> GetAllStudentsList = new List<KS_SmartChild.BAL.SPClasses.StudentList>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                KS_SmartChild.BAL.SPClasses.StudentList StudentList = new KS_SmartChild.BAL.SPClasses.StudentList();
             
                if (dt.Rows[i]["Class"] != null)
                    StudentList.Class = (dt.Rows[i]["Class"]).ToString();
                if (dt.Rows[i]["StudentName"] != null)
                    StudentList.StudentName = dt.Rows[i]["StudentName"].ToString();
                if (dt.Rows[i]["AdmnNo"] != null)
                    StudentList.AdmissionNo = !string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["AdmnNo"])) ? dt.Rows[i]["AdmnNo"].ToString() : "N/A";
                if (dt.Rows[i]["StudentId"] != null)
                    StudentList.StudentId = Convert.ToInt32(dt.Rows[i]["StudentId"].ToString());
                if (dt.Rows[i]["Amount"] != null && dt.Rows[i]["Amount"] != "")
                    StudentList.Amount = Convert.ToDecimal(dt.Rows[i]["Amount"]);
                if (dt.Rows[i]["MonthlyAmount"] != null && dt.Rows[i]["MonthlyAmount"] != "")
                    StudentList.MonthlyAmount = Convert.ToDecimal(dt.Rows[i]["MonthlyAmount"]);


                GetAllStudentsList.Add(StudentList);
            }

            count = GetAllStudentsList.Count;

            GetAllStudentsList = new PagedList<KS_SmartChild.BAL.SPClasses.StudentList>(GetAllStudentsList, PageIndex, PageSize);
            return GetAllStudentsList;
        }


        #endregion

        #region StudentFeeAddonType

        public StudentFeeAddonType GetStudentFeeAddonTypeById(int StudentFeeAddonTypeId, bool IsTrack = false)
        {
            if (StudentFeeAddonTypeId == 0)
                throw new ArgumentNullException("StudentFeeAddonType");

            var StudentFeeAddonType = new StudentFeeAddonType();

            if(IsTrack)
                StudentFeeAddonType = (from s in db.StudentFeeAddonTypes where s.StudentFeeAddonTypeId == StudentFeeAddonTypeId select s).FirstOrDefault();
            else
                StudentFeeAddonType = (from s in db.StudentFeeAddonTypes.AsNoTracking() where s.StudentFeeAddonTypeId == StudentFeeAddonTypeId select s).FirstOrDefault();

            return StudentFeeAddonType;
        }

        public List<StudentFeeAddonType> GetAllStudentFeeAddonTypes(ref int totalcount, string StudentFeeAddonType = "",
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StudentFeeAddonTypes.ToList();
            if (!String.IsNullOrEmpty(StudentFeeAddonType))
                query = query.Where(e => e.StudentFeeAddonType1.ToLower().Contains(StudentFeeAddonType.ToLower())).ToList();

            totalcount = query.Count;
            var StudentFeeAddonTypes = new PagedList<KSModel.Models.StudentFeeAddonType>(query, PageIndex, PageSize);
            return StudentFeeAddonTypes;
        }

        public void InsertStudentFeeAddonType(StudentFeeAddonType StudentFeeAddonType)
        {
            if (StudentFeeAddonType == null)
                throw new ArgumentNullException("StudentFeeAddonType");

            db.StudentFeeAddonTypes.Add(StudentFeeAddonType);
            db.SaveChanges();
        }

        public void UpdateStudentFeeAddonType(StudentFeeAddonType StudentFeeAddonType)
        {
            if (StudentFeeAddonType == null)
                throw new ArgumentNullException("StudentFeeAddonType");

            db.Entry(StudentFeeAddonType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStudentFeeAddonType(int StudentFeeAddonTypeId = 0)
        {
            if (StudentFeeAddonTypeId == 0)
                throw new ArgumentNullException("StudentFeeAddonType");

            var StudentFeeAddonType = (from s in db.StudentFeeAddonTypes where s.StudentFeeAddonTypeId == StudentFeeAddonTypeId select s).FirstOrDefault();
            db.StudentFeeAddonTypes.Remove(StudentFeeAddonType);
            db.SaveChanges();
        }

        #endregion

        #region StudentFeeAddOn

        public StudentFeeAddOn GetStudentFeeAddOnById(int StudentFeeAddOnId, bool IsTrack = false)
        {
            if (StudentFeeAddOnId == 0)
                throw new ArgumentNullException("StudentFeeAddOn");

            var StudentFeeAddOn = new StudentFeeAddOn();

            if(IsTrack)
                StudentFeeAddOn = (from s in db.StudentFeeAddOns where s.StudentFeeAddonId == StudentFeeAddOnId select s).FirstOrDefault();
            else
                StudentFeeAddOn = (from s in db.StudentFeeAddOns.AsNoTracking() where s.StudentFeeAddonId == StudentFeeAddOnId select s).FirstOrDefault();

            return StudentFeeAddOn;
        }

        public List<StudentFeeAddOn> GetAllStudentFeeAddOns(ref int totalcount, string StudentFeeAddOn = "", int? AddOnHeadId = null,
            int? StudentId = null, int? StudentFeeAddOnTypeId = null, int? ConsessionId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StudentFeeAddOns.ToList();
            if (!String.IsNullOrEmpty(StudentFeeAddOn))
                query = query.Where(e => e.StudentFeeAddon1.ToLower().Contains(StudentFeeAddOn.ToLower())).ToList();
            if (AddOnHeadId != null && AddOnHeadId > 0)
                query = query.Where(e => e.AddonHeadId == AddOnHeadId).ToList();
            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();
            if (StudentFeeAddOnTypeId != null && StudentFeeAddOnTypeId > 0)
                query = query.Where(e => e.StudentFeeAddonTypeId == StudentFeeAddOnTypeId).ToList();
            if (ConsessionId != null && ConsessionId > 0)
                query = query.Where(e => e.ConsessionId == ConsessionId).ToList();

            totalcount = query.Count;
            var StudentFeeAddOns = new PagedList<KSModel.Models.StudentFeeAddOn>(query, PageIndex, PageSize);
            return StudentFeeAddOns;
        }

        public void InsertStudentFeeAddOn(StudentFeeAddOn StudentFeeAddOn)
        {
            if (StudentFeeAddOn == null)
                throw new ArgumentNullException("StudentFeeAddOn");

            db.StudentFeeAddOns.Add(StudentFeeAddOn);
            db.SaveChanges();
        }

        public void UpdateStudentFeeAddOn(StudentFeeAddOn StudentFeeAddOn)
        {
            if (StudentFeeAddOn == null)
                throw new ArgumentNullException("StudentFeeAddOn");

            db.Entry(StudentFeeAddOn).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStudentFeeAddOn(int StudentFeeAddOnId = 0)
        {
            if (StudentFeeAddOnId == 0)
                throw new ArgumentNullException("StudentFeeAddOn");

            var StudentFeeAddOn = (from s in db.StudentFeeAddOns where s.StudentFeeAddonId == StudentFeeAddOnId select s).FirstOrDefault();
            db.StudentFeeAddOns.Remove(StudentFeeAddOn);
            db.SaveChanges();
        }

        #endregion

        #region StudentFeeAddOnDetail

        public StudentFeeAddOnDetail GetStudentFeeAddOnDetailById(int StudentFeeAddOnDetailId, bool IsTrack = false)
        {
            if (StudentFeeAddOnDetailId == 0)
                throw new ArgumentNullException("StudentFeeAddOnDetail");

            var StudentFeeAddOnDetail = new StudentFeeAddOnDetail();

            if(IsTrack)
                StudentFeeAddOnDetail = (from s in db.StudentFeeAddOnDetails where s.StudentFeeAddOnDetailId == StudentFeeAddOnDetailId select s).FirstOrDefault();
            else
                StudentFeeAddOnDetail = (from s in db.StudentFeeAddOnDetails.AsNoTracking() where s.StudentFeeAddOnDetailId == StudentFeeAddOnDetailId select s).FirstOrDefault();
            return StudentFeeAddOnDetail;
        }

        public List<StudentFeeAddOnDetail> GetAllStudentFeeAddOnDetails(ref int totalcount, int? StudentFeeAddOnId = null,
            DateTime? EffectiveDate = null, decimal? Amount = null, bool? IsDefualt = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.StudentFeeAddOnDetails.ToList();
            if (StudentFeeAddOnId != null && StudentFeeAddOnId > 0)
                query = query.Where(e => e.StudentFeeAddOnId == StudentFeeAddOnId).ToList();
            if (EffectiveDate.HasValue)
                query = query.Where(e => e.EffectiveDate == EffectiveDate).ToList();
            if (Amount != null && Amount > 0)
                query = query.Where(e => e.Amount == Amount).ToList();
            if (IsDefualt != null)
                query = query.Where(e => e.IsDefualt == IsDefualt).ToList();

            totalcount = query.Count;
            var StudentFeeAddOnDetails = new PagedList<KSModel.Models.StudentFeeAddOnDetail>(query, PageIndex, PageSize);
            return StudentFeeAddOnDetails;
        }

        public void InsertStudentFeeAddOnDetail(StudentFeeAddOnDetail StudentFeeAddOnDetail)
        {
            if (StudentFeeAddOnDetail == null)
                throw new ArgumentNullException("StudentFeeAddOnDetail");

            db.StudentFeeAddOnDetails.Add(StudentFeeAddOnDetail);
            db.SaveChanges();
        }

        public void UpdateStudentFeeAddOnDetail(StudentFeeAddOnDetail StudentFeeAddOnDetail)
        {
            if (StudentFeeAddOnDetail == null)
                throw new ArgumentNullException("StudentFeeAddOnDetail");

            db.Entry(StudentFeeAddOnDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteStudentFeeAddOnDetail(int StudentFeeAddOnDetailId = 0)
        {
            if (StudentFeeAddOnDetailId == 0)
                throw new ArgumentNullException("StudentFeeAddOnDetail");

            var StudentFeeAddOnDetail = (from s in db.StudentFeeAddOnDetails where s.StudentFeeAddOnDetailId == StudentFeeAddOnDetailId select s).FirstOrDefault();
            db.StudentFeeAddOnDetails.Remove(StudentFeeAddOnDetail);
            db.SaveChanges();
        }

        #endregion

        #endregion
        #region"AttendancePunch"
        public IList<AttendanceSerial> GetDeviceTypeList(ref int totalcount)
        {
            IList<AttendanceSerial> attendaceDevicelist = new List<AttendanceSerial>();
            var query = db.AttendanceDevices.Where(e=>e.AttendanceDeviceType.AttendanceDeviceType1=="BioMetric").ToList();
            foreach(var item in query)
            {
                AttendanceSerial adp = new AttendanceSerial();
                adp.AttendanceDeviceId = Convert.ToString(item.AttendanceDeviceId);
                adp.AttendanceDevice = item.AttendanceDeviceType.AttendanceDeviceType1 + "(" + item.SerialNo + ")";
                attendaceDevicelist.Add(adp);
            }

            totalcount = attendaceDevicelist.Count;
            return attendaceDevicelist;
        }
        #endregion
    }
}