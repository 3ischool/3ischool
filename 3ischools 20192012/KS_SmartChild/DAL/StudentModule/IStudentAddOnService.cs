﻿using KSModel.Models;
using KS_SmartChild.ViewModel.Attendence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.StudentModule
{
    public partial interface IStudentAddOnService
    {
        #region ConsessionType

        ConsessionType GetConsessionTypeById(int ConsessionTypeId, bool IsTrack = false);

        List<ConsessionType> GetAllConsessionTypes(ref int totalcount, string ConsessionType = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertConsessionType(ConsessionType ConsessionType);

        void UpdateConsessionType(ConsessionType ConsessionType);

        void DeleteConsessionType(int ConsessionTypeId = 0);

        #endregion

        #region ConsessionTag



        List<KSModel.Models.ConcessionTypeTag> GetAllConcessionTypeTags(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);
        



        #endregion

        #region Consession

        Consession GetConsessionById(int ConsessionId, bool IsTrack = false);

        List<Consession> GetAllConsessions(ref int totalcount, string Consession = "", int? ConsessionTypeId = null,
            decimal? Amount = null, bool? Status = null, int ReceipttypeId = 0, bool GeneralConcession=true, int PageIndex = 0, int PageSize = int.MaxValue);


        List<Consession> GetAllConsessionsList(ref int totalcount, string Consession = "", int? ConsessionTypeId = null,
            decimal? Amount = null, bool? Status = null, int ReceipttypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertConsession(Consession Consession);

        void UpdateConsession(Consession Consession);

        void DeleteConsession(int ConsessionId = 0);
        IList<KS_SmartChild.BAL.SPClasses.StudentList> GetConcStudentList(ref int count, string ConsessiontypeTag = "", int ConsessionId = 0, int SessionId = 0, int PageIndex = 0, int PageSize = int.MaxValue);
        #endregion

        #region StudentFeeAddonType

        StudentFeeAddonType GetStudentFeeAddonTypeById(int StudentFeeAddonTypeId, bool IsTrack = false);

        List<StudentFeeAddonType> GetAllStudentFeeAddonTypes(ref int totalcount, string StudentFeeAddonType = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudentFeeAddonType(StudentFeeAddonType StudentFeeAddonType);

        void UpdateStudentFeeAddonType(StudentFeeAddonType StudentFeeAddonType);

        void DeleteStudentFeeAddonType(int StudentFeeAddonTypeId = 0);

        #endregion

        #region StudentFeeAddOn

        StudentFeeAddOn GetStudentFeeAddOnById(int StudentFeeAddOnId, bool IsTrack = false);

        List<StudentFeeAddOn> GetAllStudentFeeAddOns(ref int totalcount, string StudentFeeAddOn = "", int? AddOnHeadId = null, int? StudentId = null,
            int? StudentFeeAddOnTypeId = null, int? ConsessionId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudentFeeAddOn(StudentFeeAddOn StudentFeeAddOn);

        void UpdateStudentFeeAddOn(StudentFeeAddOn StudentFeeAddOn);

        void DeleteStudentFeeAddOn(int StudentFeeAddOnId = 0);

        #endregion

        #region StudentFeeAddOnDetail

        StudentFeeAddOnDetail GetStudentFeeAddOnDetailById(int StudentFeeAddOnDetailId, bool IsTrack = false);

        List<StudentFeeAddOnDetail> GetAllStudentFeeAddOnDetails(ref int totalcount, int? StudentFeeAddOnId = null, DateTime? EffectiveDate = null,
            decimal? Amount = null, bool? IsDefualt = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertStudentFeeAddOnDetail(StudentFeeAddOnDetail StudentFeeAddOnDetail);

        void UpdateStudentFeeAddOnDetail(StudentFeeAddOnDetail StudentFeeAddOnDetail);

        void DeleteStudentFeeAddOnDetail(int StudentFeeAddOnDetailId = 0);

        #endregion

        #region"AttendancePunch"
        IList<AttendanceSerial> GetDeviceTypeList(ref int totalcount);
        #endregion


    }
}
