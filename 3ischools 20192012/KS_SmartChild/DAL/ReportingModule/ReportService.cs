﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KS_SmartChild.DAL.Common;
using KSModel.Models;
using System.Data;
using System.Data.SqlClient;
using KS_SmartChild.BAL.Reporting;

namespace KS_SmartChild.DAL.ReportingModule
{
    public class ReportService : IReportService
    {
        #region fields

        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        //private readonly string SqlDataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        #endregion

        #region ctor

        public ReportService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #endregion

        #region methods

        #region Report Master

        public RptMaster GetRptMasterById(int RptMasterId, bool IsTrack = false)
        {
            if (RptMasterId == 0)
                throw new ArgumentNullException("RptMaster");

            var RptMaster = new RptMaster();
            if (IsTrack)
                RptMaster = (from s in db.RptMasters where s.RptMasterId == RptMasterId select s).FirstOrDefault();
            else
                RptMaster = (from s in db.RptMasters.AsNoTracking() where s.RptMasterId == RptMasterId select s).FirstOrDefault();

            return RptMaster;
        }

        public List<RptMaster> GetAllRptMasters(ref int totalcount, DateTime? CreationDate = null, string RptMasterName = null,
            string RptSPName = null, string GenericName = null, bool? IsActive = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.RptMasters.ToList();
            if (CreationDate.HasValue)
                query = query.Where(e => e.CreationDate == CreationDate).ToList();
            if (!String.IsNullOrEmpty(RptMasterName))
                query = query.Where(e => e.ReportName.ToLower() == RptMasterName.ToLower()).ToList();
            if (!String.IsNullOrEmpty(GenericName))
                query = query.Where(e => e.GenericName.ToLower() == GenericName.ToLower()).ToList();
            if (!String.IsNullOrEmpty(RptSPName))
                query = query.Where(e => e.StoredProcedureName.ToLower() == RptSPName.ToLower()).ToList();
            if (IsActive != null)
                query = query.Where(e => e.IsActive == IsActive).ToList();

            totalcount = query.Count;
            var RptMasters = new PagedList<KSModel.Models.RptMaster>(query, PageIndex, PageSize);
            return RptMasters;
        }

        public void InsertRptMaster(RptMaster RptMaster)
        {
            if (RptMaster == null)
                throw new ArgumentNullException("RptMaster");

            db.RptMasters.Add(RptMaster);
            db.SaveChanges();
        }

        public void UpdateRptMaster(RptMaster RptMaster)
        {
            if (RptMaster == null)
                throw new ArgumentNullException("RptMaster");

            db.Entry(RptMaster).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteRptMaster(int RptMasterId = 0)
        {
            if (RptMasterId == 0)
                throw new ArgumentNullException("RptMaster");

            var RptMaster = (from s in db.RptMasters where s.RptMasterId == RptMasterId select s).FirstOrDefault();
            db.RptMasters.Remove(RptMaster);
            db.SaveChanges();
        }

        public DataTable GetStandardReportCols(string StoreProcedureName, List<StoreProcedureParameter> paralist)
        {
            DataTable dtcols = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand(StoreProcedureName, conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    foreach (var para in paralist)
                        sqlComm.Parameters.AddWithValue("@" + para.ParaName, para.Value);

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dtcols);
                    conn.Close();
                }
            }

            return dtcols;
        }

        #endregion

        #region Report Parameter

        public RptParameter GetRptParameterById(int RptParameterId, bool IsTrack = false)
        {
            if (RptParameterId == 0)
                throw new ArgumentNullException("RptParameter");

            var RptParameter = new RptParameter();
            if (IsTrack)
                RptParameter = (from s in db.RptParameters where s.RptParameterId == RptParameterId select s).FirstOrDefault();
            else
                RptParameter = (from s in db.RptParameters.AsNoTracking() where s.RptParameterId == RptParameterId select s).FirstOrDefault();

            return RptParameter;
        }

        public List<RptParameter> GetAllRptParameters(ref int totalcount, int? RptMasterId = null, string ParameterName = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.RptParameters.ToList();
            if (RptMasterId > 0)
                query = query.Where(e => e.RptMasterId == RptMasterId).ToList();
            if (!String.IsNullOrEmpty(ParameterName))
                query = query.Where(e => e.RptParameterName.ToLower() == ParameterName.ToLower()).ToList();

            totalcount = query.Count;
            var RptParameters = new PagedList<KSModel.Models.RptParameter>(query, PageIndex, PageSize);
            return RptParameters;
        }

        public void InsertRptParameter(RptParameter RptParameter)
        {
            if (RptParameter == null)
                throw new ArgumentNullException("RptParameter");

            db.RptParameters.Add(RptParameter);
            db.SaveChanges();
        }

        public void UpdateRptParameter(RptParameter RptParameter)
        {
            if (RptParameter == null)
                throw new ArgumentNullException("RptParameter");

            db.Entry(RptParameter).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteRptParameter(int RptParameterId = 0)
        {
            if (RptParameterId == 0)
                throw new ArgumentNullException("RptParameter");

            var RptParameter = (from s in db.RptParameters where s.RptParameterId == RptParameterId select s).FirstOrDefault();
            db.RptParameters.Remove(RptParameter);
            db.SaveChanges();
        }

        #endregion

        #region Report PageNoPosition

        public RptPageNoPosition GetRptPageNoPositionById(int RptPageNoPositionId, bool IsTrack = false)
        {
            if (RptPageNoPositionId == 0)
                throw new ArgumentNullException("RptParameter");

            var RptPageNoPosition = new RptPageNoPosition();
            if (IsTrack)
                RptPageNoPosition = (from s in db.RptPageNoPositions where s.RptPageNoPositionId == RptPageNoPositionId select s).FirstOrDefault();
            else
                RptPageNoPosition = (from s in db.RptPageNoPositions.AsNoTracking() where s.RptPageNoPositionId == RptPageNoPositionId select s).FirstOrDefault();

            return RptPageNoPosition;
        }

        public List<RptPageNoPosition> GetAllRptPageNoPositions(ref int totalcount, string RptPageNoPosition = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.RptPageNoPositions.ToList();
            if (!String.IsNullOrEmpty(RptPageNoPosition))
                query = query.Where(e => e.RptPageNoPosition1.ToLower() == RptPageNoPosition.ToLower()).ToList();

            totalcount = query.Count;
            var RptPageNoPositions = new PagedList<KSModel.Models.RptPageNoPosition>(query, PageIndex, PageSize);
            return RptPageNoPositions;
        }

        public void InsertRptPageNoPosition(RptPageNoPosition RptPageNoPosition)
        {
            if (RptPageNoPosition == null)
                throw new ArgumentNullException("RptPageNoPosition");

            db.RptPageNoPositions.Add(RptPageNoPosition);
            db.SaveChanges();
        }

        public void UpdateRptPageNoPosition(RptPageNoPosition RptPageNoPosition)
        {
            if (RptPageNoPosition == null)
                throw new ArgumentNullException("RptPageNoPosition");

            db.Entry(RptPageNoPosition).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteRptPageNoPosition(int RptPageNoPositionId = 0)
        {
            if (RptPageNoPositionId == 0)
                throw new ArgumentNullException("RptPageNoPosition");

            var RptPageNoPosition = (from s in db.RptPageNoPositions where s.RptPageNoPositionId == RptPageNoPositionId select s).FirstOrDefault();
            db.RptPageNoPositions.Remove(RptPageNoPosition);
            db.SaveChanges();
        }

        #endregion

        #region Report Layout

        public RptLayout GetRptLayoutById(int RptLayoutId, bool IsTrack = false)
        {
            if (RptLayoutId == 0)
                throw new ArgumentNullException("RptLayout");

            var RptLayout = new RptLayout();
            if (IsTrack)
                RptLayout = (from s in db.RptLayouts where s.RptLayoutId == RptLayoutId select s).FirstOrDefault();
            else
                RptLayout = (from s in db.RptLayouts.AsNoTracking() where s.RptLayoutId == RptLayoutId select s).FirstOrDefault();

            return RptLayout;
        }

        public List<RptLayout> GetAllRptLayouts(ref int totalcount, int? RptMasterId = null, bool? IsPageTotal = null, 
            bool? IsTotalOpening = null, bool? IsPageNo = null, int? PageNoPositionId = null, DateTime? CurrentDate = null,
            TimeSpan? CurrentTime = null, bool? IsOrgAddress = null, bool? IsOrgName = null, bool? IsOrgLogo = null, 
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.RptLayouts.ToList();
            if (RptMasterId > 0)
                query = query.Where(e => e.RptMasterId == RptMasterId).ToList();
            if (IsPageTotal != null)
                query = query.Where(e => e.IsPageTotal == IsPageTotal).ToList();
            if (IsTotalOpening != null)
                query = query.Where(e => e.IsTotalOpening == IsTotalOpening).ToList();
            if (IsPageNo != null)
                query = query.Where(e => e.IsPageNo == IsPageNo).ToList();
            if (PageNoPositionId > 0)
                query = query.Where(e => e.PageNoPositionId == PageNoPositionId).ToList();
            if (IsOrgAddress != null)
                query = query.Where(e => e.IsOrgAddress == IsOrgAddress).ToList();
            if (IsOrgName != null)
                query = query.Where(e => e.IsOrgName == IsOrgName).ToList();
            if (IsOrgLogo != null)
                query = query.Where(e => e.IsOrgLogo == IsOrgLogo).ToList();
            if (CurrentDate.HasValue)
                query = query.Where(e => e.CurrentDate == CurrentDate).ToList();
            if (CurrentTime.HasValue)
                query = query.Where(e => e.CurrentTime == CurrentTime).ToList();

            totalcount = query.Count;
            var RptLayouts = new PagedList<KSModel.Models.RptLayout>(query, PageIndex, PageSize);
            return RptLayouts;
        }

        public void InsertRptLayout(RptLayout RptLayout)
        {
            if (RptLayout == null)
                throw new ArgumentNullException("RptLayout");

            db.RptLayouts.Add(RptLayout);
            db.SaveChanges();
        }

        public void UpdateRptLayout(RptLayout RptLayout)
        {
            if (RptLayout == null)
                throw new ArgumentNullException("RptLayout");

            db.Entry(RptLayout).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteRptLayout(int RptLayoutId = 0)
        {
            if (RptLayoutId == 0)
                throw new ArgumentNullException("RptLayout");

            var RptLayout = (from s in db.RptLayouts where s.RptLayoutId == RptLayoutId select s).FirstOrDefault();
            db.RptLayouts.Remove(RptLayout);
            db.SaveChanges();
        }

        #endregion

        #region Report FilterControlType

        public RptFilterControlType GetRptFilterControlTypeById(int RptFilterControlTypeId, bool IsTrack = false)
        {
            if (RptFilterControlTypeId == 0)
                throw new ArgumentNullException("RptFilterControlType");

            var RptFilterControlType = new RptFilterControlType();
            if (IsTrack)
                RptFilterControlType = (from s in db.RptFilterControlTypes where s.RptFilterControlTypeId == RptFilterControlTypeId select s).FirstOrDefault();
            else
                RptFilterControlType = (from s in db.RptFilterControlTypes.AsNoTracking() where s.RptFilterControlTypeId == RptFilterControlTypeId select s).FirstOrDefault();

            return RptFilterControlType;
        }

        public List<RptFilterControlType> GetAllRptFilterControlTypes(ref int totalcount, string FilterControlType = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.RptFilterControlTypes.ToList();
            if (!String.IsNullOrEmpty(FilterControlType))
                query = query.Where(e => e.RptFilterControlType1.ToLower() == FilterControlType.ToLower()).ToList();

            totalcount = query.Count;
            var RptFilterControlTypes = new PagedList<KSModel.Models.RptFilterControlType>(query, PageIndex, PageSize);
            return RptFilterControlTypes;
        }

        public void InsertRptFilterControlType(RptFilterControlType RptFilterControlType)
        {
            if (RptFilterControlType == null)
                throw new ArgumentNullException("RptFilterControlType");

            db.RptFilterControlTypes.Add(RptFilterControlType);
            db.SaveChanges();
        }

        public void UpdateRptFilterControlType(RptFilterControlType RptFilterControlType)
        {
            if (RptFilterControlType == null)
                throw new ArgumentNullException("RptFilterControlType");

            db.Entry(RptFilterControlType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteRptFilterControlType(int RptFilterControlTypeId = 0)
        {
            if (RptFilterControlTypeId == 0)
                throw new ArgumentNullException("RptFilterControlType");

            var RptFilterControlType = (from s in db.RptFilterControlTypes where s.RptFilterControlTypeId == RptFilterControlTypeId select s).FirstOrDefault();
            db.RptFilterControlTypes.Remove(RptFilterControlType);
            db.SaveChanges();
        }

        #endregion

        #region Report Standard Col

        public RptStandardCol GetRptStandardColById(int RptStandardColId, bool IsTrack = false)
        {
            if (RptStandardColId == 0)
                throw new ArgumentNullException("RptStandardCol");

            var RptStandardCol = new RptStandardCol();
            if (IsTrack)
                RptStandardCol = (from s in db.RptStandardCols where s.RptStdColId == RptStandardColId select s).FirstOrDefault();
            else
                RptStandardCol = (from s in db.RptStandardCols.AsNoTracking() where s.RptStdColId == RptStandardColId select s).FirstOrDefault();

            return RptStandardCol;
        }

        public List<RptStandardCol> GetAllRptStandardCols(ref int totalcount, int? RptMasterId = null, string ColName = null, 
            int? ColIndex = null, bool? IsFilter = null, int? FilterControlTypeId = null, bool? IsVisible = null, 
            bool? IsSorted = null, int? SortingIndex = null, bool? IsGroup = null, bool? IsSum = null, int PageIndex = 0,
            int PageSize = int.MaxValue)
        {
            var query = db.RptStandardCols.ToList();
            if (RptMasterId > 0)
                query = query.Where(e => e.RptMasterId == RptMasterId).ToList();
            if (!String.IsNullOrEmpty(ColName))
                query = query.Where(e => e.ColName.ToLower() == ColName.ToLower()).ToList();
            if (ColIndex > 0)
                query = query.Where(e => e.ColIndex == ColIndex).ToList();
            if (IsFilter != null)
                query = query.Where(e => e.IsFilter == IsFilter).ToList();
            if (FilterControlTypeId > 0)
                query = query.Where(e => e.RptFilterControlTypeId == FilterControlTypeId).ToList();
            if (IsVisible != null)
                query = query.Where(e => e.IsVisible == IsVisible).ToList();
            if (IsSorted != null)
                query = query.Where(e => e.IsSorted == IsSorted).ToList();
            if (SortingIndex > 0)
                query = query.Where(e => e.SortingIndex == SortingIndex).ToList();
            if (IsGroup != null)
                query = query.Where(e => e.IsGroup == IsGroup).ToList();
            if (IsSum != null)
                query = query.Where(e => e.IsSum == IsSum).ToList();

            totalcount = query.Count;
            var RptStandardCols = new PagedList<KSModel.Models.RptStandardCol>(query, PageIndex, PageSize);
            return RptStandardCols;
        }

        public void InsertRptStandardCol(RptStandardCol RptStandardCol)
        {
            if (RptStandardCol == null)
                throw new ArgumentNullException("RptStandardCol");

            db.RptStandardCols.Add(RptStandardCol);
            db.SaveChanges();
        }

        public void UpdateRptStandardCol(RptStandardCol RptStandardCol)
        {
            if (RptStandardCol == null)
                throw new ArgumentNullException("RptStandardCol");

            db.Entry(RptStandardCol).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteRptStandardCol(int RptStandardColId = 0)
        {
            if (RptStandardColId == 0)
                throw new ArgumentNullException("RptParameter");

            var RptStandardCol = (from s in db.RptStandardCols where s.RptStdColId == RptStandardColId select s).FirstOrDefault();
            db.RptStandardCols.Remove(RptStandardCol);
            db.SaveChanges();
        }

        #endregion

        #region Report Generic Col

        public RptGenericCol GetRptGenericColById(int RptGenericColId, bool IsTrack = false)
        {
            if (RptGenericColId == 0)
                throw new ArgumentNullException("RptGenericCol");

            var RptGenericCol = new RptGenericCol();
            if (IsTrack)
                RptGenericCol = (from s in db.RptGenericCols where s.RptGenericColId == RptGenericColId select s).FirstOrDefault();
            else
                RptGenericCol = (from s in db.RptGenericCols.AsNoTracking() where s.RptGenericColId == RptGenericColId select s).FirstOrDefault();

            return RptGenericCol;
        }

        public List<RptGenericCol> GetAllRptGenericCols(ref int totalcount, int? RptMasterId = null, string ColName = null,
            int? ColIndex = null, bool? IsFilter = null, bool? IsVisible = null, bool? IsGroup = null, bool? IsSum = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.RptGenericCols.ToList();
            if (RptMasterId > 0)
                query = query.Where(e => e.RptMasterId == RptMasterId).ToList();
            if (!String.IsNullOrEmpty(ColName))
                query = query.Where(e => e.ColName.ToLower() == ColName.ToLower()).ToList();
            if (ColIndex > 0)
                query = query.Where(e => e.ColIndex == ColIndex).ToList();
            if (IsFilter != null)
                query = query.Where(e => e.IsFilter == IsFilter).ToList();
            if (IsVisible != null)
                query = query.Where(e => e.IsVisible == IsVisible).ToList();
            if (IsGroup != null)
                query = query.Where(e => e.IsGroup == IsGroup).ToList();
            if (IsSum != null)
                query = query.Where(e => e.IsSum == IsSum).ToList();

            totalcount = query.Count;
            var RptGenericCols = new PagedList<KSModel.Models.RptGenericCol>(query, PageIndex, PageSize);
            return RptGenericCols;
        }

        public void InsertRptGenericCol(RptGenericCol RptGenericCol)
        {
            if (RptGenericCol == null)
                throw new ArgumentNullException("RptGenericCol");

            db.RptGenericCols.Add(RptGenericCol);
            db.SaveChanges();
        }

        public void UpdateRptGenericCol(RptGenericCol RptGenericCol)
        {
            if (RptGenericCol == null)
                throw new ArgumentNullException("RptGenericCol");

            db.Entry(RptGenericCol).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteRptGenericCol(int RptGenericColId = 0)
        {
            if (RptGenericColId == 0)
                throw new ArgumentNullException("RptGenericCol");

            var RptGenericCol = (from s in db.RptGenericCols where s.RptGenericColId == RptGenericColId select s).FirstOrDefault();
            db.RptGenericCols.Remove(RptGenericCol);
            db.SaveChanges();
        }

        #endregion

        #region Report CustomMaster

        public RptCustomMaster GetRptCustomMasterById(int RptCustomMasterId, bool IsTrack = false)
        {
            if (RptCustomMasterId == 0)
                throw new ArgumentNullException("RptCustomMaster");

            var RptCustomMaster = new RptCustomMaster();
            if (IsTrack)
                RptCustomMaster = (from s in db.RptCustomMasters where s.RptCustomMasterId == RptCustomMasterId select s).FirstOrDefault();
            else
                RptCustomMaster = (from s in db.RptCustomMasters.AsNoTracking() where s.RptCustomMasterId == RptCustomMasterId select s).FirstOrDefault();

            return RptCustomMaster;
        }

        public List<RptCustomMaster> GetAllRptCustomMasters(ref int totalcount, int? RptMasterId = null, int? UserId = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.RptCustomMasters.ToList();
            if (RptMasterId > 0)
                query = query.Where(e => e.RptMasterId == RptMasterId).ToList();
            if (UserId > 0)
                query = query.Where(e => e.UserId == UserId).ToList();

            totalcount = query.Count;
            var RptCustomMasters = new PagedList<KSModel.Models.RptCustomMaster>(query, PageIndex, PageSize);
            return RptCustomMasters;
        }

        public void InsertRptCustomMaster(RptCustomMaster RptCustomMaster)
        {
            if (RptCustomMaster == null)
                throw new ArgumentNullException("RptCustomMaster");

            db.RptCustomMasters.Add(RptCustomMaster);
            db.SaveChanges();
        }

        public void UpdateRptCustomMaster(RptCustomMaster RptCustomMaster)
        {
            if (RptCustomMaster == null)
                throw new ArgumentNullException("RptCustomMaster");

            db.Entry(RptCustomMaster).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteRptCustomMaster(int RptCustomMasterId = 0)
        {
            if (RptCustomMasterId == 0)
                throw new ArgumentNullException("RptCustomMaster");

            var RptCustomMaster = (from s in db.RptCustomMasters where s.RptCustomMasterId == RptCustomMasterId select s).FirstOrDefault();
            db.RptCustomMasters.Remove(RptCustomMaster);
            db.SaveChanges();
        }

        #endregion

        #region Report Custom Col

        public RptCustomCol GetRptCustomColById(int RptCustomColId, bool IsTrack = false)
        {
            if (RptCustomColId == 0)
                throw new ArgumentNullException("RptCustomCol");

            var RptCustomCol = new RptCustomCol();
            if (IsTrack)
                RptCustomCol = (from s in db.RptCustomCols where s.RptCustomColId == RptCustomColId select s).FirstOrDefault();
            else
                RptCustomCol = (from s in db.RptCustomCols.AsNoTracking() where s.RptCustomColId == RptCustomColId select s).FirstOrDefault();

            return RptCustomCol;
        }

        public List<RptCustomCol> GetAllRptCustomCols(ref int totalcount, int? RptCustomMasterId = null, string ColName = null, 
            int? ColIndex = null, bool? IsVisible = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.RptCustomCols.ToList();
            if (RptCustomMasterId > 0)
                query = query.Where(e => e.RptCustomMasterId == RptCustomMasterId).ToList();
            if (!String.IsNullOrEmpty(ColName))
                query = query.Where(e => e.ColName.ToLower() == ColName.ToLower()).ToList();
            if (ColIndex > 0)
                query = query.Where(e => e.ColIndex == ColIndex).ToList();
            if (IsVisible != null)
                query = query.Where(e => e.IsVisible == IsVisible).ToList();


            totalcount = query.Count;
            var RptCustomCols = new PagedList<KSModel.Models.RptCustomCol>(query, PageIndex, PageSize);
            return RptCustomCols;
        }

        public void InsertRptCustomCol(RptCustomCol RptCustomCol)
        {
            if (RptCustomCol == null)
                throw new ArgumentNullException("RptCustomCol");

            db.RptCustomCols.Add(RptCustomCol);
            db.SaveChanges();
        }

        public void UpdateRptCustomCol(RptCustomCol RptCustomCol)
        {
            if (RptCustomCol == null)
                throw new ArgumentNullException("RptCustomCol");

            db.Entry(RptCustomCol).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteRptCustomCol(int RptCustomColId = 0)
        {
            if (RptCustomColId == 0)
                throw new ArgumentNullException("RptCustomCol");

            var RptCustomCol = (from s in db.RptCustomCols where s.RptCustomColId == RptCustomColId select s).FirstOrDefault();
            db.RptCustomCols.Remove(RptCustomCol);
            db.SaveChanges();
        }

        #endregion

        #endregion
        
    }
}