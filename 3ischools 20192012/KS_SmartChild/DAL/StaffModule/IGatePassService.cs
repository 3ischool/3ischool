﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KSModel.Models;

namespace KS_SmartChild.DAL.StaffModule
{
    public partial interface IGatePassService
    {

        #region VisitorCard
        VisitorCard GetVisitorCardById(int VisitorCardId, bool IsTrack = false);

        List<VisitorCard> GetAllVisitorCard(ref int totalcount,bool IsActive=true,
        int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertVisitorCard(VisitorCard VisitorCard);

        void UpdateVisitorCard(VisitorCard VisitorCard);

        void DeleteVisitorCard(int VisitorCardId = 0);

        #endregion

        #region GatePassType

        GatePassType GetGatePassTypeById(int GatePassTypeId, bool IsTrack = false);

        List<GatePassType> GetAllGatePassType(ref int totalcount,
          int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertGatePassType(GatePassType GatePassType);

        void UpdateGatePassType(GatePassType GatePassType);

        void DeleteGatePassType(int GatePassTypeId = 0);

        #endregion

        #region GatePass

        GatePass GetGatePassById(int GatePassId, bool IsTrack = false);

        List<GatePass> GetAllGatePass(ref int totalcount, int? PassTypeId = null, string ContactPersonId = "", 
            int? StudentId = null, int? StaffId = null, DateTime? IssueDate = null, DateTime? ToDate = null,
            TimeSpan? InTime = null, TimeSpan? OutTime = null, int? SessionId = null, int? classId = null,
            int PageIndex = 0, int PageSize = int.MaxValue, string visitor = "");

        void InsertGatePass(GatePass GatePass);

        void UpdateGatePass(GatePass GatePass);

        void DeleteGatePass(int GatePassId = 0);

        #endregion

        #region GatePassDetail

        GatePassDetail GatePassDetailById(int GatePassDetailId, bool IsTrack = false);

        List<GatePassDetail> GetAllGatePassDetail(ref int totalcount,
          int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertGatePassDetail(GatePassDetail GatePassDetail);

        void UpdateGatePassDetail(GatePassDetail GatePassDetail);

        void DeleteGatePassDetail(int GatePassDetailId = 0);

        #endregion

        #region GatePassAuthority

        GatePassAuthority GatePassAuthorityById(int GatePassAuthorityId, bool IsTrack = false);

        List<GatePassAuthority> GetAllGatePassAuthority(ref int totalcount, int AuthorityId = 0, int GatePassTypeId = 0, int ContactTypeId = 0, bool IsActive = false,
          int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertGatePassAuthority(GatePassAuthority GatePassAuthority);

        void UpdateGatePassAuthority(GatePassAuthority GatePassAuthority);

        void DeleteGatePassAuthority(int GatePassAuthorityId = 0);

        #endregion

    }
}
