﻿using KS_SmartChild.BAL.Common;
using KS_SmartChild.BAL.SPClasses;
using KS_SmartChild.DAL.Common;
using KS_SmartChild.DAL.Kendo;
using KSModel.Models;
using KS_SmartChild.ViewModel.Library;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KS_SmartChild.DAL.Kendo;

namespace KS_SmartChild.DAL.LibraryModule
{
    public partial class BookMasterService : IBookMasterService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        //private readonly string SqlDataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;
        private readonly ICustomEncryption _ICustomEncryption;
        public BookMasterService(IConnectionService IConnectionService,
            ICustomEncryption ICustomEncryption)
        {
            //HttpContext context = HttpContext.Current;
            this._IConnectionService = IConnectionService;
            this._ICustomEncryption = ICustomEncryption;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.SqlDataSource = _IConnectionService.SqlConnectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region Methods

        #region BookMaster

        public KSModel.Models.BookMaster GetBookMasterById(int? BookId, bool IsTrack = false)
        {
            if (BookId == 0)
                throw new ArgumentNullException("BookMaster");

            var BookMasters = new BookMaster();

            if (IsTrack)
                BookMasters = db.BookMasters.Where(m => m.BookId == BookId).FirstOrDefault();
            else
                BookMasters = db.BookMasters.AsNoTracking().Where(m => m.BookId == BookId).FirstOrDefault();

            return BookMasters;
        }

        public List<BookMaster> GetAllBookMasters(ref int totalcount, string BookTitle = "", string ISBN = "", string BookAbbr = "",
              int? MaterialTypeId = null, string SubTitle = "", int PageIndex = 0, int PageSize = int.MaxValue,
             string MaterialType = "", bool Isfilter = false)
        {
            var query = db.BookMasters.ToList();

            if (!String.IsNullOrEmpty(BookTitle))
            {
                if (Isfilter)
                {
                    query = query.Where(e => e.BookTitle.ToLower().Contains(BookTitle.ToLower())).ToList();
                }
                else
                {
                    query = query.Where(e => e.BookTitle.ToLower() == BookTitle.ToLower()).ToList();
                }
            }
            if (!String.IsNullOrEmpty(BookAbbr))
                query = query.Where(e => e.BookAbbr.ToLower().Contains(BookAbbr.ToLower())).ToList();
            if (!String.IsNullOrEmpty(ISBN))
                query = query.Where(e => e.ISBN.ToLower() == ISBN.ToLower()).ToList();
            if (MaterialTypeId != null)
                query = query.Where(e => e.MaterialTypeId == MaterialTypeId).ToList();
            if (!String.IsNullOrEmpty(MaterialType))
            {
                var marterialtype = db.BookMaterialTypes.Where(x => x.MaterialType.ToLower().Contains(MaterialType.ToLower())).FirstOrDefault();
                query = query.Where(e => e.MaterialTypeId == marterialtype.MaterialTypeId).ToList();
            }

            var BookMasters = new PagedList<BookMaster>(query, PageIndex, PageSize);
            return BookMasters;
        }


        public List<BookAdditionalInfoViewModel> GetAllBookMastersBySp(ref int totalcount, string BookTitle = "", string ISBN = "", string BookAbbr = "",
                      int MaterialTypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue, int BookId = 0, string UniqueId = "",
            string LUI = "", string AccessionNo = "")
        {
            // call store procedure for Advanced serach of books
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_getBooks", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@BookId", BookId);
                    sqlComm.Parameters.AddWithValue("@BookTitle", BookTitle);
                    sqlComm.Parameters.AddWithValue("@MaterialTypeId", MaterialTypeId);
                    sqlComm.Parameters.AddWithValue("@ISBN", ISBN);
                    sqlComm.Parameters.AddWithValue("@LUI", LUI);
                    //    sqlComm.Parameters.AddWithValue("@UniqueId", UniqueId);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<BookAdditionalInfoViewModel> GetAllBookList = new List<BookAdditionalInfoViewModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                BookAdditionalInfoViewModel booklist = new BookAdditionalInfoViewModel();
                booklist.BookId = _ICustomEncryption.base64e(Convert.ToString(dt.Rows[i]["BookId"]));
                booklist.BookTitle = Convert.ToString(dt.Rows[i]["BookTitle"]);
                booklist.SubTitle = Convert.ToString(dt.Rows[i]["SubTitle"]);
                booklist.ISBN = Convert.ToString(dt.Rows[i]["ISBN"]);
                booklist.MaterialTypeId = _ICustomEncryption.base64e(Convert.ToString(dt.Rows[i]["MaterialTypeId"]));
                booklist.MaterialType = Convert.ToString(dt.Rows[i]["MaterialType"]);
                booklist.Description = Convert.ToString(dt.Rows[i]["Description"]);
                booklist.BooKMarkAutoIncrement = Convert.ToBoolean(dt.Rows[i]["BooKMarkAutoIncrement"]);
                booklist.TotalEditions = Convert.ToString(dt.Rows[i]["TotalEditions"]) == "0" ? "" : Convert.ToString(dt.Rows[i]["TotalEditions"]);
                booklist.TotalCopies = Convert.ToString(dt.Rows[i]["TotalCopies"]) == "0" ? "" : Convert.ToString(dt.Rows[i]["TotalCopies"]);
                booklist.TotalIssuedBooks = Convert.ToString(dt.Rows[i]["TotalIssued"]) == "0" ? "" : Convert.ToString(dt.Rows[i]["TotalIssued"]);
                booklist.AvaiableCopies = Convert.ToString(dt.Rows[i]["AvaiableCopies"]) == "0" ? "" : Convert.ToString(dt.Rows[i]["AvaiableCopies"]);
                booklist.strAccessionNo = Convert.ToString(dt.Rows[i]["AccessionNo"]) == null ? "" : Convert.ToString(dt.Rows[i]["AccessionNo"]);
                booklist.TotalLost = Convert.ToString(dt.Rows[i]["TotalLost"]) == "0" ? "" : Convert.ToString(dt.Rows[i]["TotalLost"]);

                booklist.intTotalCopies = booklist.TotalCopies == "0" || booklist.TotalCopies == "" || booklist.TotalCopies == null ? 0 : Convert.ToInt32(booklist.TotalCopies);
                booklist.intTotalIssuedBooks = booklist.TotalIssuedBooks == "0" || booklist.TotalIssuedBooks == "" || booklist.TotalIssuedBooks == null ? 0 : Convert.ToInt32(booklist.TotalIssuedBooks);
                booklist.intAvaiableCopies = booklist.AvaiableCopies == "0" || booklist.AvaiableCopies == "" || booklist.AvaiableCopies == null ? 0 : Convert.ToInt32(booklist.AvaiableCopies);
                booklist.intIsLost = booklist.TotalLost == "0" || booklist.TotalLost == "" || booklist.TotalLost == null ? 0 : Convert.ToInt32(booklist.TotalLost);

                // bookMasterlist.strAccessionNo = p.strAccessionNo == null ? "" : p.strAccessionNo;
                booklist.IsAuthToDelete = false;

                //has dependencies to another tables
                //    var hasMasterDetails = _IBookMasterService.GetAllBookDetails(ref count, BookId: p.intbookId).Count();
                //   var hasBookAttributeValues = _IBookMasterService.GetAllBookAttributeValues(ref count, BookId: p.intbookId).Count();
                //var hasBookIdentities = _IBookMasterService.GetAllBookIdentities(ref count, BookId: p.intbookId).Count();
                //var hasBookEditions = _IBookMasterService.GetAllBookEditions(ref count, BookId: p.intbookId).Count();

                var hasBookIdentities = dt.Rows[i]["BookIdentityCount"] != null && dt.Rows[i]["BookIdentityCount"] != "" ? Convert.ToInt32(dt.Rows[i]["BookIdentityCount"]) : 0;
                var hasBookEditions = dt.Rows[i]["BookEditionCount"] != null && dt.Rows[i]["BookEditionCount"] != "" ? Convert.ToInt32(dt.Rows[i]["BookEditionCount"]) : 0;

                if (hasBookIdentities > 0 || hasBookEditions > 0)
                {
                    booklist.IsAuthToDelete = true;
                }


                GetAllBookList.Add(booklist);
            }
            totalcount = GetAllBookList.Count;

            GetAllBookList = new PagedList<BookAdditionalInfoViewModel>(GetAllBookList, PageIndex, PageSize);
            return GetAllBookList;
        }

        public List<BookAdditionalInfoViewModel> GetAllBookMastersByLinq(ref int totalcount, string BookTitle = "", string ISBN = "", string BookAbbr = "",
                      int MaterialTypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue, int BookId = 0, string UniqueId = "",
            string LUI = "", string AccessionNo = "")
        {

            var AllBooks = db.BookMasters.AsQueryable();

            if (!string.IsNullOrEmpty(LUI))
            {
                var LUIBooks = GetAllBookIdentities(ref totalcount, LUI: LUI.ToLower());
                var arrBooksId = LUIBooks.Select(f => f.BookId).ToArray();
                if (arrBooksId != null && arrBooksId.Count() > 0)
                    AllBooks = AllBooks.Where(f => arrBooksId.Contains(f.BookId));
            }
            if (BookId != null && BookId > 0)
                AllBooks = AllBooks.Where(f => f.BookId == BookId);
            if (!string.IsNullOrEmpty(BookTitle))
                AllBooks = AllBooks.Where(f =>f.BookTitle.ToLower().Contains(BookTitle));
            if (!string.IsNullOrEmpty(ISBN))
                AllBooks = AllBooks.Where(f => f.ISBN.ToLower().Contains(ISBN.ToLower()));
            if (MaterialTypeId != null && MaterialTypeId > 0)
                AllBooks = AllBooks.Where(f => f.MaterialTypeId == MaterialTypeId);
            if (!string.IsNullOrEmpty(BookAbbr))
                AllBooks = AllBooks.Where(f =>f.BookAbbr.ToLower().Contains(f.BookAbbr.ToLower()));
            if (!string.IsNullOrEmpty(AccessionNo))
                AllBooks = AllBooks.Where(f => f.AccessionNo.ToString().Contains(AccessionNo));


            var bookIDs = AllBooks.Select(f => f.BookId).ToArray();
            var BookEditions = GetAllBookEditions(ref totalcount);
            var BookIdentities = GetAllBookIdentities(ref totalcount);
            var BookTransaction = GetAllBookTransactions(ref totalcount);
            string[] arrLostScrap = new string[2]{"lost","scrap"};
            var BookStatus = GetAllBookStatuss(ref totalcount).Where(f =>arrLostScrap.Contains(f.BookStatus.ToLower()));
            var arrBookStatusCoed = BookStatus.Select(f => f.StatusCode.ToString()).ToArray();
            int lostcode = 0;
            var LostBooks = BookStatus.Where(f => f.BookStatus.ToLower() == "lost").FirstOrDefault();
            lostcode = LostBooks.StatusCode != null ? Convert.ToInt32(LostBooks.StatusCode) : 0;
            var AllBookslist = new PagedList<BookMaster>(AllBooks.ToList(), PageIndex-1, PageSize);

            List<BookAdditionalInfoViewModel> GetAllBookList = new List<BookAdditionalInfoViewModel>();
            var model = new BookAdditionalInfoViewModel();
            foreach (var book in AllBookslist)
            {
                model = new BookAdditionalInfoViewModel();
                model.BookId = _ICustomEncryption.base64e(book.BookId.ToString());
                model.BookTitle = book.BookTitle;
                model.SubTitle = book.SubTitle;
                model.ISBN = book.ISBN;
                model.Description = book.Description;
                model.MaterialTypeId = book.MaterialTypeId.ToString();
                model.MaterialType = book.BookMaterialType.MaterialType;
                model.BooKMarkAutoIncrement = (bool)book.BooKMarkAutoIncrement;
                model.strAccessionNo = book.AccessionNo.ToString();
                model.intEditions = BookEditions.Where(f => f.BookId == book.BookId).Count();

                if (arrBookStatusCoed != null && arrBookStatusCoed.Count() > 0)
                    model.intCopies = BookIdentities.Where(f => f.BookId == book.BookId && !arrBookStatusCoed.Contains(f.BookStatusId.ToString())).Count();
                else
                    model.intCopies = BookIdentities.Where(f => f.BookId == book.BookId).Count();

                model.TotalEditions = model.intEditions == 0 ? "" : model.intEditions.ToString();
                model.TotalCopies = model.intCopies == 0 ? "" : model.intCopies.ToString();
                var arrBookIdentities = BookIdentities.Where(f => f.BookId == book.BookId).Select(f => f.BookIdentityId.ToString()).ToArray();
                if (arrBookIdentities != null && arrBookIdentities.Count() > 0)
                {
                    model.intIssuedBooks = BookTransaction.Where(f => arrBookIdentities.Contains(f.BookIdentityId.ToString()) && f.ActualReturnDate == null && (f.IsLost==null||f.IsLost==false)).Count();
                    model.TotalIssuedBooks = model.intIssuedBooks == 0 ? "" : model.intIssuedBooks.ToString();
                }
                else
                    model.TotalIssuedBooks = "";

                model.intAvaiableCopies = model.intCopies - model.intIssuedBooks;
                model.AvaiableCopies = model.intAvaiableCopies == 0 ? "" : model.intAvaiableCopies.ToString();
                if (lostcode > 0)
                    model.intLost = BookIdentities.Where(f =>f.BookId == book.BookId && f.BookStatusId == lostcode).Count();

                model.IsAuthToDelete = false;
                if (model.intCopies > 0 || model.intEditions > 0)
                    model.IsAuthToDelete = true;

                GetAllBookList.Add(model);
            }

            return GetAllBookList;
        }

        public dynamic GetAllBooksGridData(ref int totalcount, string BookTitle = "", string ISBN = "", string BookAbbr = "",
                      int MaterialTypeId = 0, int PageIndex = 0, int PageSize = int.MaxValue, int BookId = 0, string UniqueId = "",
            string LUI = "", string AccessionNo = "")
        {

            var AllBooks = db.BookMasters.OrderBy(m => m.AccessionNo).AsQueryable();
            if (!string.IsNullOrEmpty(LUI))
            {
                var LUIBooks = GetAllBookIdentities(ref totalcount, LUI: LUI.ToLower());
                var arrBooksId = LUIBooks.Select(f => f.BookId).ToArray();
                if (arrBooksId != null && arrBooksId.Count() > 0)
                    AllBooks = AllBooks.Where(f => arrBooksId.Contains(f.BookId));
                else
                    AllBooks = AllBooks.Where(f => f.AccessionNo.ToString().ToLower()== LUI.ToLower());
            }
            if (BookId != null && BookId > 0)
                AllBooks = AllBooks.Where(f => f.BookId == BookId);
            if (!string.IsNullOrEmpty(BookTitle))
                AllBooks = AllBooks.Where(f => f.BookTitle.ToLower().Contains(BookTitle));
            if (!string.IsNullOrEmpty(ISBN))
                AllBooks = AllBooks.Where(f => f.ISBN.ToLower().Contains(ISBN.ToLower()));
            if (MaterialTypeId != null && MaterialTypeId > 0)
                AllBooks = AllBooks.Where(f => f.MaterialTypeId == MaterialTypeId);
            if (!string.IsNullOrEmpty(BookAbbr))
                AllBooks = AllBooks.Where(f => f.BookAbbr.ToLower().Contains(f.BookAbbr.ToLower()));
            if (!string.IsNullOrEmpty(AccessionNo))
                AllBooks = AllBooks.Where(f => f.AccessionNo!=null && f.AccessionNo.ToString().ToLower()==AccessionNo.ToLower());


            var bookIDs = AllBooks.Select(f => f.BookId).ToArray();
            var BookEditions = GetAllBookEditions(ref totalcount);
            var BookIdentities = GetAllBookIdentities(ref totalcount);
            var BookTransaction = GetAllBookTransactions(ref totalcount);
            string[] arrLostScrap = new string[2] { "lost", "scrap" };
            var BookStatus = GetAllBookStatuss(ref totalcount).Where(f => arrLostScrap.Contains(f.BookStatus.ToLower()));
            var arrBookStatusCoed = BookStatus.Select(f => f.StatusCode.ToString()).ToArray();
            int lostcode = 0;
            var LostBooks = BookStatus.Where(f => f.BookStatus.ToLower() == "lost").FirstOrDefault();
            lostcode = LostBooks.StatusCode != null ? Convert.ToInt32(LostBooks.StatusCode) : 0;
            var AllBookslist = new PagedList<BookMaster>(AllBooks.ToList(), PageIndex - 1, PageSize);

            List<BookAdditionalInfoViewModel> GetAllBookList = new List<BookAdditionalInfoViewModel>();
            var model = new BookAdditionalInfoViewModel();
            foreach (var book in AllBookslist)
            {
                model = new BookAdditionalInfoViewModel();
                model.BookId = _ICustomEncryption.base64e(book.BookId.ToString());
                model.BookTitle = book.BookTitle;
                model.SubTitle = book.SubTitle;
                model.ISBN = book.ISBN;
                model.Description = book.Description;
                model.MaterialTypeId = book.MaterialTypeId.ToString();
                model.MaterialType = book.BookMaterialType!=null?book.BookMaterialType.MaterialType:null;
                model.BooKMarkAutoIncrement = book.BooKMarkAutoIncrement!=null?(bool)book.BooKMarkAutoIncrement:false;
                model.strAccessionNo = book.AccessionNo.ToString();
                model.AccessionNo = (long)book.AccessionNo;
                model.intEditions = BookEditions.Where(f => f.BookId == book.BookId).Count();

                if (arrBookStatusCoed != null && arrBookStatusCoed.Count() > 0)
                    model.intCopies = BookIdentities.Where(f => f.BookId == book.BookId && !arrBookStatusCoed.Contains(f.BookStatusId.ToString())).Count();
                else
                    model.intCopies = BookIdentities.Where(f => f.BookId == book.BookId).Count();

                model.TotalEditions = model.intEditions == 0 ? "" : model.intEditions.ToString();
                model.TotalCopies = model.intCopies == 0 ? "" : model.intCopies.ToString();
                var arrBookIdentities = BookIdentities.Where(f => f.BookId == book.BookId).Select(f => f.BookIdentityId.ToString()).ToArray();
                if (arrBookIdentities != null && arrBookIdentities.Count() > 0)
                {
                    model.intIssuedBooks = BookTransaction.Where(f => arrBookIdentities.Contains(f.BookIdentityId.ToString()) && f.ActualReturnDate == null && (f.IsLost == null || f.IsLost == false)).Count();
                    model.TotalIssuedBooks = model.intIssuedBooks == 0 ? "" : model.intIssuedBooks.ToString();
                    model.IsBookInTransaction = BookTransaction.Where(f=> arrBookIdentities.Contains(f.BookIdentityId.ToString())).Count();
                }
                else
                    model.TotalIssuedBooks = "";

                model.intAvaiableCopies = model.intCopies - model.intIssuedBooks;
                model.AvaiableCopies = model.intAvaiableCopies == 0 ? "" : model.intAvaiableCopies.ToString();
                if (lostcode > 0)
                    model.intLost = BookIdentities.Where(f => f.BookId == book.BookId && f.BookStatusId == lostcode).Count();

                model.IsAuthToDelete = false;
                if (model.intCopies > 0 || model.intEditions > 0)
                    model.IsAuthToDelete = true;

                GetAllBookList.Add(model);
            }


            var totalCopiesWithoutLost = BookIdentities.Where(f => !arrBookStatusCoed.Contains(f.BookStatusId.ToString())).Count();
            var IssuedCopies = BookTransaction.Where(f => f.ActualReturnDate == null && (f.IsLost == null || f.IsLost == false)).Count(); ;
            var availableCopies = totalCopiesWithoutLost - IssuedCopies;
            var LostCopies = BookIdentities.Count() - totalCopiesWithoutLost;

            var gridModel = new DataSourceResult
            {
                Data = GetAllBookList.OrderBy(m => m.AccessionNo),
                Total = AllBooks.Count(),
                Value1 = totalCopiesWithoutLost.ToString(),
                Value2 = IssuedCopies.ToString(),
                Value3 = availableCopies.ToString(),
                Value4 = LostCopies.ToString(),
            };
            return (gridModel);
        }

        public List<BookDefaulterList> SeacrhAllBookMastersBySp(ref int totalcount, string Searchtext = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            // call store procedure for Advanced serach of books
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_SearchAllBooks", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@Searchtext", Searchtext == null ? "" : Searchtext);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<BookDefaulterList> GetAllBookList = new List<BookDefaulterList>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                BookDefaulterList booklist = new BookDefaulterList();
                booklist.BookId = Convert.ToInt32(dt.Rows[i]["BookId"]);
                booklist.BookTitle = Convert.ToString(dt.Rows[i]["BookTitle"]);
                booklist.SubTitle = Convert.ToString(dt.Rows[i]["SubTitle"]);
                booklist.ISBN = Convert.ToString(dt.Rows[i]["ISBN"] == null ? string.Empty : dt.Rows[i]["ISBN"]);
                booklist.Description = Convert.ToString(dt.Rows[i]["Description"] == null ? string.Empty : dt.Rows[i]["Description"]);
                booklist.MaterialTypeId = Convert.ToInt32(dt.Rows[i]["MaterialTypeId"]);
                booklist.MaterialType = Convert.ToString(dt.Rows[i]["MaterialType"]);
                booklist.AvialableCopies = Convert.ToString(dt.Rows[i]["AvaiableCopies"]);
                booklist.AccessionNo = Convert.ToString(dt.Rows[i]["AccessionNo"]);

                GetAllBookList.Add(booklist);
            }
            totalcount = GetAllBookList.Count;

            GetAllBookList = new PagedList<BookDefaulterList>(GetAllBookList, PageIndex, PageSize);
            return GetAllBookList;

        }

        public List<BookDefaulterList> AvancedSearchBookMasters(ref int totalcount, string SubTitle = "", string ISBN = "", string MaterialType = "",
          string AttributeValues = "", string Attribute = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            // call store procedure for Advanced serach of books
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_getAdvancedSearch", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@BookMasterAttributeValue", AttributeValues);
                    sqlComm.Parameters.AddWithValue("@BookAttributeValue", Attribute == null ? string.Empty : Attribute.Trim());
                    sqlComm.Parameters.AddWithValue("@MaterialType", MaterialType);
                    sqlComm.Parameters.AddWithValue("@SubTitle", SubTitle == null ? string.Empty : SubTitle.Trim());
                    sqlComm.Parameters.AddWithValue("@ISBN", ISBN == null ? string.Empty : ISBN.Trim());

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            List<BookDefaulterList> GetAllBookList = new List<BookDefaulterList>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                BookDefaulterList booklist = new BookDefaulterList();
                booklist.BookId = Convert.ToInt32(dt.Rows[i]["BookId"]);
                booklist.BookTitle = Convert.ToString(dt.Rows[i]["BookTitle"]);
                booklist.ISBN = Convert.ToString(dt.Rows[i]["ISBN"] == null ? string.Empty : dt.Rows[i]["ISBN"]);
                booklist.Description = Convert.ToString(dt.Rows[i]["Description"] == null ? string.Empty : dt.Rows[i]["Description"]);
                booklist.MaterialTypeId = Convert.ToInt32(dt.Rows[i]["MaterialTypeId"]);
                booklist.MaterialType = Convert.ToString(dt.Rows[i]["MaterialType"]);
                booklist.AvialableCopies = Convert.ToString(dt.Rows[i]["AvaiableCopies"]);

                GetAllBookList.Add(booklist);
            }
            totalcount = GetAllBookList.Count;

            GetAllBookList = new PagedList<BookDefaulterList>(GetAllBookList, PageIndex, PageSize);
            return GetAllBookList;
        }

        public void InsertBookMaster(KSModel.Models.BookMaster BookMaster)
        {
            if (BookMaster == null)
                throw new ArgumentNullException("BookMaster");


            var BookAbbr = BookMaster.BookAbbr != null ?"'"+ BookMaster.BookAbbr +"'": "Null";
            var SubTitle = BookMaster.SubTitle != null ? "N'" + BookMaster.SubTitle+ "'": "Null";
            var ISBN = BookMaster.ISBN != null ? "'" + BookMaster.ISBN +"'": "Null";
            var Description = BookMaster.Description != null ? "'" + BookMaster.Description +"'" : "Null";
            var AccessionPrefix = BookMaster.AccessionPrefix != null ? "'" + BookMaster.AccessionPrefix + "'": "Null";
            var BooKMarkAutoIncrement=BookMaster.BooKMarkAutoIncrement != null ? 1 : 0;


            var sqlquery = "Insert into BookMaster values(" + "N'" + BookMaster.BookTitle.Replace("'", "''") + "'," + BookAbbr + "," + SubTitle + "," + ISBN + "," + Description + "," + BookMaster.MaterialTypeId + "," + BooKMarkAutoIncrement + "," + BookMaster.AccessionNo + "," + AccessionPrefix + ") SELECT SCOPE_IDENTITY()";
            //  db.BookMasters.Add(BookMaster);

            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand(sqlquery, conn))
                {
                    conn.Open();
              //      sqlComm.ExecuteNonQuery();
                    Int32 newId = Convert.ToInt32(sqlComm.ExecuteScalar());
                    BookMaster.BookId = newId;
                    conn.Close();
                }
            }
            //db.BookMasters.SqlQuery(sqlquery);
            //db.exc();
        }

        public void UpdateBookMaster(KSModel.Models.BookMaster BookMaster)
        {
            if (BookMaster == null)
                throw new ArgumentNullException("BookMaster");

            var BookAbbr = BookMaster.BookAbbr != null ? "'" + BookMaster.BookAbbr + "'": "Null";
            var SubTitle = BookMaster.SubTitle != null ? "N'" + BookMaster.SubTitle +"'" : "Null";
            var ISBN = BookMaster.ISBN != null ? "'" + BookMaster.ISBN+ "'"  : "Null";
            var Description = BookMaster.Description != null ? "'" + BookMaster.Description + "'" : "Null";
            var AccessionPrefix = BookMaster.AccessionPrefix != null ? "'" + BookMaster.AccessionPrefix + "'": "Null";
            var BooKMarkAutoIncrement = BookMaster.BooKMarkAutoIncrement != null ? 1 : 0;
            if (BookMaster.BookTitle.Contains("'"))
            {
                BookMaster.BookTitle = BookMaster.BookTitle.Replace("'", "''");
            }

            var sqlquery = "update BookMaster set  BookTitle="+"N'" + BookMaster.BookTitle + "',BookAbbr=" + BookAbbr + ",SubTitle="+ SubTitle + ",ISBN=" + ISBN + ",Description=" + Description + ",MaterialTypeId=" + BookMaster.MaterialTypeId + ",BooKMarkAutoIncrement=" + BooKMarkAutoIncrement + ",AccessionNo=" + BookMaster.AccessionNo + ",AccessionPrefix=" + AccessionPrefix + " where BookId="+ BookMaster.BookId+"";
            //  db.BookMasters.Add(BookMaster);

            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand(sqlquery, conn))
                {
                    conn.Open();
                    sqlComm.ExecuteNonQuery();
                    conn.Close();
                }
            }
            //db.Entry(BookMaster).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();
        }

        public void DeleteBookMaster(int BookId = 0)
        {
            if (BookId == 0)
                throw new ArgumentNullException("BookMaster");

            var bookMaster = (from s in db.BookMasters
                              where s.BookId == BookId
                              select s).FirstOrDefault();
            db.BookMasters.Remove(bookMaster);
            db.SaveChanges();
        }

        #endregion

        #region BookMaterialType

        public BookMaterialType GetBookMaterialTypeById(int? MaterialTypeId, bool IsTrack = false)
        {
            if (MaterialTypeId == 0)
                throw new ArgumentNullException("BookMaterialType");

            var BookMaterialTypes = new BookMaterialType();

            if (IsTrack)
                BookMaterialTypes = db.BookMaterialTypes.Where(m => m.MaterialTypeId == MaterialTypeId).FirstOrDefault();
            else
                BookMaterialTypes = db.BookMaterialTypes.AsNoTracking().Where(m => m.MaterialTypeId == MaterialTypeId).FirstOrDefault();

            return BookMaterialTypes;
        }

        public List<BookMaterialType> GetAllBookMaterialTypes(ref int totalcount, string MaterialType = "",
                                    int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.BookMaterialTypes.ToList();

            if (!String.IsNullOrEmpty(MaterialType))
            {
                if (Isfilter)
                {
                    query = query.Where(e => e.MaterialType.ToLower().Contains(MaterialType.ToLower())).ToList();
                }
                else
                {
                    query = query.Where(e => e.MaterialType.ToLower() == MaterialType.ToLower()).ToList();
                }
            }
            totalcount = query.Count;

            var BookMaterialType = new PagedList<KSModel.Models.BookMaterialType>(query, PageIndex, PageSize);
            return BookMaterialType;
        }

        public void InsertBookMaterialType(BookMaterialType BookMaterialType)
        {
            if (BookMaterialType == null)
                throw new ArgumentNullException("BookMaterialType");

            db.BookMaterialTypes.Add(BookMaterialType);
            db.SaveChanges();
        }

        public void UpdateBookMaterialType(BookMaterialType BookMaterialType)
        {
            if (BookMaterialType == null)
                throw new ArgumentNullException("BookMaterialType");

            db.Entry(BookMaterialType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookMaterialType(int MaterialTypeId = 0)
        {
            if (MaterialTypeId == 0)
                throw new ArgumentNullException("BookMaterialType");

            var BookMaterialType = (from s in db.BookMaterialTypes
                                    where s.MaterialTypeId == MaterialTypeId
                                    select s).FirstOrDefault();
            db.BookMaterialTypes.Remove(BookMaterialType);
            db.SaveChanges();
        }

        #endregion

        #region BookMasterAttribute

        public KSModel.Models.BookMasterAttribute GetBookMasterAttributeById(int? BookMasterAttributeId, bool IsTrack = false)
        {
            if (BookMasterAttributeId == 0)
                throw new ArgumentNullException("BookMasterAttribute");

            var BookAttributes = new BookMasterAttribute();

            if (IsTrack)
                BookAttributes = db.BookMasterAttributes.Where(m => m.BookMasterAttributeId == BookMasterAttributeId).FirstOrDefault();
            else
                BookAttributes = db.BookMasterAttributes.AsNoTracking().Where(m => m.BookMasterAttributeId == BookMasterAttributeId).FirstOrDefault();

            return BookAttributes;
        }

        public List<KSModel.Models.BookMasterAttribute> GetAllBookMasterAttributes(ref int totalcount
                                    , string MasterAttribute = "", int? AttrMaxLength = null, bool? IsActive = null
                                    , int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.BookMasterAttributes.ToList();

            if (!String.IsNullOrEmpty(MasterAttribute))
            {
                if (Isfilter)
                {
                    query = query.Where(e => e.MasterAttribute.ToLower().Contains(MasterAttribute.ToLower())).ToList();
                }
                else
                {
                    query = query.Where(e => e.MasterAttribute.ToLower() == MasterAttribute.ToLower()).ToList();
                }

            }
            if (AttrMaxLength != null)
                query = query.Where(e => e.AttrMaxLength == AttrMaxLength).ToList();
            if (IsActive != null)
                query = query.Where(e => e.IsActive == IsActive).ToList();

            totalcount = query.Count;

            var BookMasterAttributes = new PagedList<KSModel.Models.BookMasterAttribute>(query, PageIndex, PageSize);
            return BookMasterAttributes;
        }

        public void InsertBookMasterAttribute(KSModel.Models.BookMasterAttribute BookMasterAttribute)
        {
            if (BookMasterAttribute == null)
                throw new ArgumentNullException("InsertBookAttribute");

            db.BookMasterAttributes.Add(BookMasterAttribute);
            db.SaveChanges();
        }

        public void UpdateBookMasterAttribute(KSModel.Models.BookMasterAttribute BookMasterAttribute)
        {
            if (BookMasterAttribute == null)
                throw new ArgumentNullException("BookMasterAttribute");

            db.Entry(BookMasterAttribute).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookMasterAttribute(int BookMasterAttributeId = 0)
        {
            if (BookMasterAttributeId == 0)
                throw new ArgumentNullException("BookMasterAttribute");

            var bookMasterAttribute = (from s in db.BookMasterAttributes
                                       where s.BookMasterAttributeId == BookMasterAttributeId
                                       select s).FirstOrDefault();
            db.BookMasterAttributes.Remove(bookMasterAttribute);
            db.SaveChanges();
        }

        #endregion

        #region BookMasterData

        public KSModel.Models.BookMasterData GetBookDataById(int? BookMasterDataId, bool IsTrack = false)
        {
            if (BookMasterDataId == 0)
                throw new ArgumentNullException("BookMasterData");

            var BookMasterData = new BookMasterData();

            if (IsTrack)
                BookMasterData = db.BookMasterDatas.Where(m => m.BookMasterDataId == BookMasterDataId).FirstOrDefault();
            else
                BookMasterData = db.BookMasterDatas.AsNoTracking().Where(m => m.BookMasterDataId == BookMasterDataId).FirstOrDefault();

            return BookMasterData;
        }

        public List<KSModel.Models.BookMasterData> GetAllBookData(ref int totalcount
                                    , int? BookMasterAttributeId = null, string AttributeValue = ""
                                    , bool? IsActive = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.BookMasterDatas.ToList();

            if (!String.IsNullOrEmpty(AttributeValue))
            {
                if (Isfilter)
                {
                    query = query.Where(e => e.AttributeValue.ToLower().Contains(AttributeValue.ToLower())).ToList();
                }
                else
                {
                    query = query.Where(e => e.AttributeValue.ToLower() == AttributeValue.ToLower()).ToList();
                }
            }
            if (BookMasterAttributeId != null)
                query = query.Where(e => e.BookMasterAttributeId == BookMasterAttributeId).ToList();

            if (IsActive != null)
                query = query.Where(e => e.IsActive == IsActive).ToList();

            totalcount = query.Count;

            var BookMasterDatas = new PagedList<KSModel.Models.BookMasterData>(query, PageIndex, PageSize);
            return BookMasterDatas;
        }

        public void InsertBookData(KSModel.Models.BookMasterData BookMasterData)
        {
            if (BookMasterData == null)
                throw new ArgumentNullException("BookMasterData");

            var IsActive = BookMasterData.IsActive != null ? "'"+BookMasterData.IsActive +"'": "Null";

            var sqlquery = "Insert into BookMasterData values("+BookMasterData.BookMasterAttributeId+","+"N'"+BookMasterData.AttributeValue+"',"+ IsActive + ") SELECT SCOPE_IDENTITY()";
            //  db.BookMasters.Add(BookMaster);
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand(sqlquery, conn))
                {
                    conn.Open();
                    //      sqlComm.ExecuteNonQuery();
                    Int32 newId = Convert.ToInt32(sqlComm.ExecuteScalar());
                    BookMasterData.BookMasterDataId = newId;
                    conn.Close();
                }
            }
            //db.BookMasterDatas.Add(BookMasterData);
            //db.SaveChanges();
        }

        public void UpdateBookData(KSModel.Models.BookMasterData BookMasterData)
        {
            if (BookMasterData == null)
                throw new ArgumentNullException("BookMasterData");

            //db.Entry(BookMasterData).State = System.Data.Entity.EntityState.Modified;
            //db.SaveChanges();
            //BookMasterData.BookMasterAttributeId,
            var IsActive = BookMasterData.IsActive != null ? "'" + BookMasterData.IsActive + "'" : "Null";
            var sqlquery = "update BookMasterData set  AttributeValue=" + "N'" + BookMasterData.AttributeValue + "',BookMasterAttributeId=" + BookMasterData.BookMasterAttributeId + ",IsActive=" + IsActive + " where BookMasterDataId=" + BookMasterData.BookMasterDataId + "";
            //  db.BookMasters.Add(BookMaster);

            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand(sqlquery, conn))
                {
                    conn.Open();
                    sqlComm.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }

        public void DeleteBookData(int BookMasterDataId = 0)
        {
            if (BookMasterDataId == 0)
                throw new ArgumentNullException("BookMasterData");

            var bookMasterData = (from s in db.BookMasterDatas
                                  where s.BookMasterDataId == BookMasterDataId
                                  select s).FirstOrDefault();
            db.BookMasterDatas.Remove(bookMasterData);
            db.SaveChanges();
        }

        #endregion

        #region BookMasterDetail

        public KSModel.Models.BookMasterDetail GetBookDetailById(int? BookMasterDetailId, bool IsTrack = false)
        {
            if (BookMasterDetailId == 0)
                throw new ArgumentNullException("BookMasterDetail");

            var BookMasterDetail = new BookMasterDetail();

            if (IsTrack)
                BookMasterDetail = db.BookMasterDetails.Where(m => m.BookMasterDetailId == BookMasterDetailId).FirstOrDefault();
            else
                BookMasterDetail = db.BookMasterDetails.AsNoTracking().Where(m => m.BookMasterDetailId == BookMasterDetailId).FirstOrDefault();

            return BookMasterDetail;
        }

        public List<KSModel.Models.BookMasterDetail> GetAllBookDetails(ref int totalcount, int? BookMasterDataId = null,
                                int? BookId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.BookMasterDetails.ToList();

            if (BookMasterDataId != null)
                query = query.Where(e => e.BookMasterDataId == BookMasterDataId).ToList();
            if (BookId != null)
                query = query.Where(e => e.BookId == BookId).ToList();

            totalcount = query.Count;

            var BookMasterDetails = new PagedList<KSModel.Models.BookMasterDetail>(query, PageIndex, PageSize);
            return BookMasterDetails;
        }

        public DataSet GetAllBookDetColumns(ref int totalcount, bool ShowCopies = false)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_ExportBookColumnsWithData", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@ShowCopies", ShowCopies);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    conn.Close();
                }
            }
            return ds;
        }

        public void InsertBookDetail(KSModel.Models.BookMasterDetail BookMasterDetail)
        {
            if (BookMasterDetail == null)
                throw new ArgumentNullException("BookMasterDetail");

            db.BookMasterDetails.Add(BookMasterDetail);
            db.SaveChanges();
        }

        public void UpdateBookDetail(KSModel.Models.BookMasterDetail BookMasterDetail)
        {
            if (BookMasterDetail == null)
                throw new ArgumentNullException("BookMasterDetail");

            db.Entry(BookMasterDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookDetailByBookId(int BookId = 0)
        {
            if (BookId == 0)
                throw new ArgumentNullException("BookMasterDetail");

            var bookMasterDetail = (from s in db.BookMasterDetails
                                    where s.BookId == BookId
                                    select s);
            if (bookMasterDetail != null)
            {
                foreach (var detail in bookMasterDetail)
                {
                    var bookDetail = (from t in db.BookMasterDetails
                                      where t.BookId == detail.BookId && t.BookMasterDetailId == detail.BookMasterDetailId
                                      select t).FirstOrDefault();

                    db.BookMasterDetails.Remove(bookDetail);
                   
                }
                db.SaveChanges();
            }
        }

        public void DeleteBookDetail(int BookMasterDetailId = 0)
        {
            if (BookMasterDetailId == 0)
                throw new ArgumentNullException("BookMasterDetail");

            var bookMasterDetail = (from s in db.BookMasterDetails
                                    where s.BookMasterDetailId == BookMasterDetailId
                                    select s).FirstOrDefault();
            db.BookMasterDetails.Remove(bookMasterDetail);
            db.SaveChanges();
        }

        #endregion

        #region BookAttribute

        public KSModel.Models.BookAttribute GetBookAttributeById(int? BookAttributeId, bool IsTrack = false)
        {
            if (BookAttributeId == 0)
                throw new ArgumentNullException("BookAttribute");

            var BookAttribute = new BookAttribute();

            if (IsTrack)
                BookAttribute = db.BookAttributes.Where(m => m.BookAttributeId == BookAttributeId).FirstOrDefault();
            else
                BookAttribute = db.BookAttributes.AsNoTracking().Where(m => m.BookAttributeId == BookAttributeId).FirstOrDefault();

            return BookAttribute;
        }

        public List<KSModel.Models.BookAttribute> GetAllBookAttributes(ref int totalcount, string Attribute = "", int? AttrMaxLength = null
                                , bool? IsActive = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.BookAttributes.ToList();

            if (!String.IsNullOrEmpty(Attribute))
            {
                if (Isfilter)
                {
                    query = query.Where(e => e.Attribute.ToLower().Contains(Attribute.ToLower())).ToList();
                }
                else
                {
                    query = query.Where(e => e.Attribute.ToLower() == Attribute.ToLower()).ToList();
                }
            }
            if (AttrMaxLength != null)
                query = query.Where(e => e.AttrMaxLength == AttrMaxLength).ToList();
            if (IsActive != null)
                query = query.Where(e => e.IsActive == IsActive).ToList();

            totalcount = query.Count;

            var BookAttribute = new PagedList<KSModel.Models.BookAttribute>(query, PageIndex, PageSize);
            return BookAttribute;
        }

        public void InsertBookAttribute(KSModel.Models.BookAttribute BookAttribute)
        {
            if (BookAttribute == null)
                throw new ArgumentNullException("BookAttribute");

            db.BookAttributes.Add(BookAttribute);
            db.SaveChanges();
        }

        public void UpdateBookAttribute(KSModel.Models.BookAttribute BookAttribute)
        {
            if (BookAttribute == null)
                throw new ArgumentNullException("BookAttribute");

            db.Entry(BookAttribute).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookAttribute(int BookAttributeId = 0)
        {
            if (BookAttributeId == 0)
                throw new ArgumentNullException("BookAttribute");

            var bookAttribute = (from s in db.BookAttributes
                                 where s.BookAttributeId == BookAttributeId
                                 select s).FirstOrDefault();
            db.BookAttributes.Remove(bookAttribute);
            db.SaveChanges();
        }

        #endregion

        #region BookAttributeValue

        public KSModel.Models.BookAttributeValue GetBookAttributeValueById(int? BookAttributeValueId, bool IsTrack = false)
        {
            if (BookAttributeValueId == 0)
                throw new ArgumentNullException("BookAttributeValue");

            var BookAttributeValue = new BookAttributeValue();

            if (IsTrack)
                BookAttributeValue = db.BookAttributeValues.Where(m => m.BookAttributeValueId == BookAttributeValueId).FirstOrDefault();
            else
                BookAttributeValue = db.BookAttributeValues.AsNoTracking().Where(m => m.BookAttributeValueId == BookAttributeValueId).FirstOrDefault();

            return BookAttributeValue;
        }

        public List<KSModel.Models.BookAttributeValue> GetAllBookAttributeValues(ref int totalcount, string AttributeValue = "", int? BookAttributeId = null
                                            , int? BookId = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.BookAttributeValues.ToList();

            if (!String.IsNullOrEmpty(AttributeValue))
            {
                if (Isfilter)
                {
                    query = query.Where(e => e.AttributeValue.ToLower().Contains(AttributeValue.ToLower())).ToList();
                }
                else
                {
                    query = query.Where(e => e.AttributeValue.ToLower() == AttributeValue.ToLower()).ToList();
                }
            }
            if (BookAttributeId != null)
                query = query.Where(e => e.BookAttributeId == BookAttributeId).ToList();
            if (BookId != null)
                query = query.Where(e => e.BookId == BookId).ToList();

            totalcount = query.Count;

            var BookAttributeValue = new PagedList<KSModel.Models.BookAttributeValue>(query, PageIndex, PageSize);
            return BookAttributeValue;
        }

        public void InsertBookAttributeValue(KSModel.Models.BookAttributeValue BookAttributeValue)
        {
            if (BookAttributeValue == null)
                throw new ArgumentNullException("BookAttributeValue");

            db.BookAttributeValues.Add(BookAttributeValue);
            db.SaveChanges();
        }

        public void UpdateBookAttributeValue(KSModel.Models.BookAttributeValue BookAttributeValue)
        {
            if (BookAttributeValue == null)
                throw new ArgumentNullException("BookAttributeValue");

            db.Entry(BookAttributeValue).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookAttributeValueByBookId(int BookId = 0)
        {
            if (BookId == 0)
                throw new ArgumentNullException("BookAttributeValue");

            var bookAttributeValue = (from s in db.BookAttributeValues
                                      where s.BookId == BookId
                                      select s).ToList();
            if (bookAttributeValue != null)
            {
                foreach (var AttrVal in bookAttributeValue)
                {
                    var bookAttrVal = (from t in db.BookAttributeValues
                                       where t.BookId == AttrVal.BookId && t.BookAttributeValueId == AttrVal.BookAttributeValueId
                                       select t).FirstOrDefault();

                    db.BookAttributeValues.Remove(bookAttrVal);
                    
                }
                db.SaveChanges();

            }
        }

        public void DeleteBookAttributeValue(int BookAttributeValueId = 0)
        {
            if (BookAttributeValueId == 0)
                throw new ArgumentNullException("BookAttributeValue");

            var bookAttributeValue = (from s in db.BookAttributeValues
                                      where s.BookAttributeValueId == BookAttributeValueId
                                      select s).FirstOrDefault();
            db.BookAttributeValues.Remove(bookAttributeValue);
            db.SaveChanges();
        }
        #endregion

        #region BookEdition

        public KSModel.Models.BookEdition GetBookEditionById(int? BookEditionId, bool IsTrack = false)
        {
            if (BookEditionId == 0)
                throw new ArgumentNullException("BookEdition");

            var BookEdition = new BookEdition();

            if (IsTrack)
                BookEdition = db.BookEditions.Where(m => m.BookEditionId == BookEditionId).FirstOrDefault();
            else
                BookEdition = db.BookEditions.AsNoTracking().Where(m => m.BookEditionId == BookEditionId).FirstOrDefault();

            return BookEdition;
        }

        public List<KSModel.Models.BookEdition> GetAllBookEditions(ref int totalcount, string Edition = "", string Volume = "", int? PageNo = 0, int? BookId = null,
                                    DateTime? PublishingDate = null, int? BookSellerId = null, int PageIndex = 0,
                                    int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.BookEditions.ToList();

            if (!String.IsNullOrEmpty(Edition))
                query = query.Where(e => e.Edition == Edition).ToList();
            if (!String.IsNullOrEmpty(Volume))
                query = query.Where(e => e.Volume == Volume).ToList();
            if (BookId != null)
                query = query.Where(e => e.BookId == BookId).ToList();
            if (PublishingDate != null)
                query = query.Where(e => e.PublishingDate == PublishingDate.Value.Date).ToList();
            if (BookSellerId != null)
                query = query.Where(e => e.BookSellerId == BookSellerId).ToList();
            if (PageNo > 0)
                query = query.Where(e => e.PageNo == PageNo).ToList();

            totalcount = query.Count;

            var BookEditions = new PagedList<KSModel.Models.BookEdition>(query, PageIndex, PageSize);
            return BookEditions;
        }

        public void InsertBookEdition(KSModel.Models.BookEdition BookEdition)
        {
            if (BookEdition == null)
                throw new ArgumentNullException("BookEdition");

            db.BookEditions.Add(BookEdition);
            db.SaveChanges();
        }

        public void UpdateBookEdition(KSModel.Models.BookEdition BookEdition)
        {
            if (BookEdition == null)
                throw new ArgumentNullException("BookEdition");

            db.Entry(BookEdition).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookEdition(int BookEditionId = 0)
        {
            if (BookEditionId == 0)
                throw new ArgumentNullException("BookEdition");

            var bookEdition = (from s in db.BookEditions
                               where s.BookEditionId == BookEditionId
                               select s).FirstOrDefault();
            db.BookEditions.Remove(bookEdition);
            db.SaveChanges();
        }

        #endregion

        #region BookSeller

        public KSModel.Models.BookSeller GetBookSellerById(int? BookSellerId, bool IsTrack = false)
        {
            if (BookSellerId == 0)
                throw new ArgumentNullException("BookSeller");

            var BookSeller = new BookSeller();

            if (IsTrack)
                BookSeller = db.BookSellers.Where(m => m.BookSellerId == BookSellerId).FirstOrDefault();
            else
                BookSeller = db.BookSellers.AsNoTracking().Where(m => m.BookSellerId == BookSellerId).FirstOrDefault();

            return BookSeller;
        }

        public List<KSModel.Models.BookSeller> GetAllBookSellers(ref int totalcount, string BookSellerName = "", int? CityId = null,
                                    int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.BookSellers.ToList();

            if (!String.IsNullOrEmpty(BookSellerName))
            {
                if (Isfilter)
                {
                    query = query.Where(e => e.BookSellerName.ToLower().Contains(BookSellerName.ToLower())).ToList();
                }
                else
                {
                    query = query.Where(e => e.BookSellerName.ToLower() == BookSellerName.ToLower()).ToList();
                }
            }
            if (CityId != null)
                query = query.Where(e => e.CityId == CityId).ToList();

            totalcount = query.Count;

            var BookSellers = new PagedList<KSModel.Models.BookSeller>(query, PageIndex, PageSize);
            return BookSellers;
        }

        public void InsertBookSeller(KSModel.Models.BookSeller BookSeller)
        {
            if (BookSeller == null)
                throw new ArgumentNullException("BookSeller");

            db.BookSellers.Add(BookSeller);
            db.SaveChanges();
        }

        public void UpdateBookSeller(KSModel.Models.BookSeller BookSeller)
        {
            if (BookSeller == null)
                throw new ArgumentNullException("BookSeller");

            db.Entry(BookSeller).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookSeller(int BookSellerId = 0)
        {
            if (BookSellerId == 0)
                throw new ArgumentNullException("BookSeller");

            var bookSeller = (from s in db.BookSellers
                              where s.BookSellerId == BookSellerId
                              select s).FirstOrDefault();
            db.BookSellers.Remove(bookSeller);
            db.SaveChanges();
        }

        #endregion

        #region BookIdentity

        public KSModel.Models.BookIdentity GetBookIdentityById(int? BookIdentityId, bool IsTrack = false)
        {
            if (BookIdentityId == 0)
                throw new ArgumentNullException("BookIdentity");

            var BookIdentity = new BookIdentity();

            if (IsTrack)
                BookIdentity = db.BookIdentities.Where(m => m.BookIdentityId == BookIdentityId).FirstOrDefault();
            else
                BookIdentity = db.BookIdentities.AsNoTracking().Where(m => m.BookIdentityId == BookIdentityId).FirstOrDefault();

            return BookIdentity;
        }

        public List<KSModel.Models.BookIdentity> GetAllBookIdentities(ref int totalcount, int? BookId = null,int BookSellerId=0, string UniqueId = "",
                           DateTime? EntryDate = null, string BarCode = "",string LUI="", int? BookEditionId = null, int? LocationId = null
                           , int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false,bool IsLost=false)
        {
            var query = db.BookIdentities.ToList();
            if (!String.IsNullOrEmpty(UniqueId))
                query = query.Where(e => e.UniqueId == UniqueId).ToList();
            if (BookId != null)
                query = query.Where(e => e.BookId == BookId).ToList();
            if (BookEditionId != null)
                query = query.Where(e => e.BookEditionId == BookEditionId).ToList();
            if (LocationId != null)
                query = query.Where(e => e.LocationId == LocationId).ToList();
            if (BookSellerId > 0)
                query = query.Where(e => e.BookSellerId == BookSellerId).ToList();
             if (!string.IsNullOrEmpty(LUI))
                query = query.Where(e => e.LUI!=null && e.LUI.ToLower()==LUI.ToLower()).ToList();
             if (IsLost == true)
            {
                string[] arrLostScrap = new string[2] { "lost", "scrap" };
               var BookStatus = GetAllBookStatuss(ref totalcount).Where(f => arrLostScrap.Contains(f.BookStatus.ToLower())).Select(f => f.StatusCode.ToString()).ToArray();
               if (BookStatus != null && BookStatus.Count()>0)
                    query = query.Where(f=>BookStatus.ToString().Contains(f.BookStatusId.ToString())).ToList();
             }

            totalcount = query.Count;

            var BookIdentity = new PagedList<KSModel.Models.BookIdentity>(query, PageIndex, PageSize);
            return BookIdentity;
        }

        public List<KSModel.Models.BookIdentity> GetAllBookIdentitiesINTransactions(ref int totalcount, int? BookId = null
                                                         , int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = (from s in db.BookIdentities
                         join a in db.BookTransactions on s.BookIdentityId equals a.BookIdentityId
                         where s.BookId == (int)BookId && a.ActualReturnDate == null
                         select s).Distinct().ToList();

            totalcount = query.Count;

            var BookIdentity = new PagedList<KSModel.Models.BookIdentity>(query, PageIndex, PageSize);
            return BookIdentity;
        }

        public int GetBookUniqueId(int? BookId)
        {
            if (BookId == 0)
                throw new ArgumentNullException("BookIdentity");

            var BookIdentity = new BookIdentity();
            int bookBUid = 0;
            var books = db.BookIdentities.Where(x => x.BookId == BookId).OrderByDescending(x => x.BookIdentityId).FirstOrDefault();
            if (books != null)
            {
                string[] ids = books.UniqueId.Split('-');
                bookBUid = Convert.ToInt32(ids[1]);
            }
            bookBUid++;

            return bookBUid;
        }

        public void InsertBookIdentity(KSModel.Models.BookIdentity BookIdentity)
        {
            if (BookIdentity == null)
                throw new ArgumentNullException("BookIdentity");

            db.BookIdentities.Add(BookIdentity);
            db.SaveChanges();
        }

        public void UpdateBookIdentity(KSModel.Models.BookIdentity BookIdentity)
        {
            if (BookIdentity == null)
                throw new ArgumentNullException("BookIdentity");

            db.Entry(BookIdentity).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookIdentity(int BookIdentityId = 0)
        {
            if (BookIdentityId == 0)
                throw new ArgumentNullException("BookIdentity");

            var bookIdentity = (from s in db.BookIdentities
                                where s.BookIdentityId == BookIdentityId
                                select s).FirstOrDefault();
            db.BookIdentities.Remove(bookIdentity);
            db.SaveChanges();
        }

        #endregion

        #region BookLocationAttribute

        public KSModel.Models.BookLocationAttribute GetBookLocationAttributeById(int? BookLocationAttributeId, bool IsTrack = false)
        {
            if (BookLocationAttributeId == 0)
                throw new ArgumentNullException("BookLocationAttribute");

            var BookLocationAttribute = new BookLocationAttribute();

            if (IsTrack)
                BookLocationAttribute = db.BookLocationAttributes.Where(m => m.BookLocationAttributeId == BookLocationAttributeId).FirstOrDefault();
            else
                BookLocationAttribute = db.BookLocationAttributes.AsNoTracking().Where(m => m.BookLocationAttributeId == BookLocationAttributeId).FirstOrDefault();

            return BookLocationAttribute;
        }

        public List<KSModel.Models.BookLocationAttribute> GetAllBookLocationAttributes(ref int totalcount, int? AttrMaxLength = null, string LocationAttribute = "",
                                     bool? IsActive = null, int? SortingIndex = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.BookLocationAttributes.ToList();
            if (!String.IsNullOrEmpty(LocationAttribute))
            {
                if (Isfilter)
                {
                    query = query.Where(e => e.LocationAttribute.ToLower().Contains(LocationAttribute.ToLower())).ToList();
                }
                else
                {
                    query = query.Where(e => e.LocationAttribute.ToLower() == LocationAttribute.ToLower()).ToList();
                }
            }
            if (AttrMaxLength != null)
                query = query.Where(e => e.AttrMaxLength == AttrMaxLength).ToList();
            if (IsActive != null)
                query = query.Where(e => e.IsActive == IsActive).ToList();
            if (SortingIndex != null)
                query = query.Where(e => e.SortingIndex == SortingIndex).ToList();

            totalcount = query.Count;

            var BookLocationAttribute = new PagedList<KSModel.Models.BookLocationAttribute>(query, PageIndex, PageSize);
            return BookLocationAttribute;
        }

        public void InsertBookLocationAttribute(KSModel.Models.BookLocationAttribute BookLocationAttribute)
        {
            if (BookLocationAttribute == null)
                throw new ArgumentNullException("BookLocationAttribute");

            db.BookLocationAttributes.Add(BookLocationAttribute);
            db.SaveChanges();
        }

        public void UpdateBookLocationAttribute(KSModel.Models.BookLocationAttribute BookLocationAttribute)
        {
            if (BookLocationAttribute == null)
                throw new ArgumentNullException("BookLocationAttribute");

            db.Entry(BookLocationAttribute).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookLocationAttribute(int BookLocationAttributeId = 0)
        {
            if (BookLocationAttributeId == 0)
                throw new ArgumentNullException("BookLocationAttribute");

            var bookLocationAttribute = (from s in db.BookLocationAttributes
                                         where s.BookLocationAttributeId == BookLocationAttributeId
                                         select s).FirstOrDefault();
            db.BookLocationAttributes.Remove(bookLocationAttribute);
            db.SaveChanges();
        }

        #endregion

        #region BookLocationData

        public KSModel.Models.BookLocationData GetBookLocationDataById(int? BookLocationDataId, bool IsTrack = false)
        {
            if (BookLocationDataId == 0)
                throw new ArgumentNullException("BookLocationData");

            var BookLocationData = new BookLocationData();

            if (IsTrack)
                BookLocationData = db.BookLocationDatas.Where(m => m.BookLocationDataId == BookLocationDataId).FirstOrDefault();
            else
                BookLocationData = db.BookLocationDatas.AsNoTracking().Where(m => m.BookLocationDataId == BookLocationDataId).FirstOrDefault();

            return BookLocationData;
        }

        public List<KSModel.Models.BookLocationData> GetAllBookLocationDatas(ref int totalcount, int? BookLocationAttributeId = null, string AttributeValue = "",
                                     bool? IsActive = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.BookLocationDatas.ToList();
            if (!String.IsNullOrEmpty(AttributeValue))
            {
                if (Isfilter)
                {
                    query = query.Where(e => e.AttributeValue.ToLower().Contains(AttributeValue.ToLower())).ToList();
                }
                else
                {
                    query = query.Where(e => e.AttributeValue.ToLower() == AttributeValue.ToLower()).ToList();
                }
            }
            if (BookLocationAttributeId != null)
                query = query.Where(e => e.BookLocationAttributeId == BookLocationAttributeId).ToList();
            if (IsActive != null)
                query = query.Where(e => e.IsActive == IsActive).ToList();

            totalcount = query.Count;

            var BookLocationDatas = new PagedList<KSModel.Models.BookLocationData>(query, PageIndex, PageSize);
            return BookLocationDatas;
        }

        public void InsertBookLocationData(KSModel.Models.BookLocationData BookLocationData)
        {
            if (BookLocationData == null)
                throw new ArgumentNullException("BookLocationData");

            db.BookLocationDatas.Add(BookLocationData);
            db.SaveChanges();
        }

        public void UpdateBookLocationData(KSModel.Models.BookLocationData BookLocationData)
        {
            if (BookLocationData == null)
                throw new ArgumentNullException("BookLocationData");

            db.Entry(BookLocationData).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookLocationData(int BookLocationDataId = 0)
        {
            if (BookLocationDataId == 0)
                throw new ArgumentNullException("BookLocationData");

            var bookLocationData = (from s in db.BookLocationDatas
                                    where s.BookLocationDataId == BookLocationDataId
                                    select s).FirstOrDefault();
            db.BookLocationDatas.Remove(bookLocationData);
            db.SaveChanges();
        }

        #endregion

        #region BookLocationDetail

        public KSModel.Models.BookLocationDetail GetBookLocationDetailById(int? BookLocationDetailId, bool IsTrack = false)
        {
            if (BookLocationDetailId == 0)
                throw new ArgumentNullException("BookLocationDetail");

            var BookLocationDetail = new BookLocationDetail();

            if (IsTrack)
                BookLocationDetail = db.BookLocationDetails.Where(m => m.BookLocationDetailId == BookLocationDetailId).FirstOrDefault();
            else
                BookLocationDetail = db.BookLocationDetails.AsNoTracking().Where(m => m.BookLocationDetailId == BookLocationDetailId).FirstOrDefault();

            return BookLocationDetail;
        }

        public List<KSModel.Models.BookLocationDetail> GetAllBookLocationDetails(ref int totalcount, int? BookLocationDataId = null,
            int? BookIdentityId = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isfilter = false)
        {
            var query = db.BookLocationDetails.ToList();
            if (BookLocationDataId != null)
                query = query.Where(e => e.BookLocationDataId == BookLocationDataId).ToList();
            if (BookIdentityId != null)
                query = query.Where(e => e.BookIdentityId == BookIdentityId).ToList();

            totalcount = query.Count;

            var BookLocationDetail = new PagedList<KSModel.Models.BookLocationDetail>(query, PageIndex, PageSize);
            return BookLocationDetail;
        }

        public void InsertBookLocationDetail(KSModel.Models.BookLocationDetail BookLocationDetail)
        {
            if (BookLocationDetail == null)
                throw new ArgumentNullException("BookLocationDetail");

            db.BookLocationDetails.Add(BookLocationDetail);
            db.SaveChanges();
        }

        public void UpdateBookLocationDetail(KSModel.Models.BookLocationDetail BookLocationDetail)
        {
            if (BookLocationDetail == null)
                throw new ArgumentNullException("BookLocationDetail");

            db.Entry(BookLocationDetail).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookLocationDetail(int BookLocationDetailId = 0)
        {
            if (BookLocationDetailId == 0)
                throw new ArgumentNullException("BookLocationDetail");

            var bookLocationDetail = (from s in db.BookLocationDetails
                                      where s.BookLocationDetailId == BookLocationDetailId
                                      select s).FirstOrDefault();
            db.BookLocationDetails.Remove(bookLocationDetail);
            db.SaveChanges();
        }

        public void DeleteBookLocationDetailByIdentityId(int BookIdentityId = 0)
        {
            if (BookIdentityId == 0)
                throw new ArgumentNullException("BookLocationDetail");

            var bookLoctionDetail = (from s in db.BookLocationDetails
                                     where s.BookIdentityId == BookIdentityId
                                     select s);
            if (bookLoctionDetail != null)
            {
                foreach (var locdetail in bookLoctionDetail)
                {
                    var bookLocDetail = (from t in db.BookLocationDetails
                                         where t.BookIdentityId == locdetail.BookIdentityId
                                         && t.BookLocationDetailId == locdetail.BookLocationDetailId
                                         select t).FirstOrDefault();

                    db.BookLocationDetails.Remove(bookLocDetail);
                }
                db.SaveChanges();
            }
        }

        #endregion

        #region BookTransaction

        public BookTransaction GetBookTransactionById(int? BookTransactionId, bool IsTrack = false)
        {
            if (BookTransactionId == 0)
                throw new ArgumentNullException("BookTransaction");

            var BookTransaction = new BookTransaction();

            if (IsTrack)
                BookTransaction = db.BookTransactions.Where(m => m.BookTransactionId == BookTransactionId).FirstOrDefault();
            else
                BookTransaction = db.BookTransactions.AsNoTracking().Where(m => m.BookTransactionId == BookTransactionId).FirstOrDefault();

            return BookTransaction;
        }

        public List<BookTransaction> GetAllBookTransactions(ref int totalcount, DateTime? TransactionDate = null,
            int? BookMemberTypeId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.BookTransactions.ToList();
            if (BookMemberTypeId != null)
                query = query.Where(e => e.BookMemberTypeId == BookMemberTypeId).ToList();
            if (TransactionDate != null)
                query = query.Where(e => e.TransactionDate == TransactionDate).ToList();

            totalcount = query.Count;

            var BookTransaction = new PagedList<KSModel.Models.BookTransaction>(query, PageIndex, PageSize);
            return BookTransaction;
        }

        public List<BookTransaction> GetAllIssuedBooks(ref int totalcount, int? BookTrsTypeId = null, DateTime? TransactionDate = null,
            int? BookMemberTypeId = null, string SearchText = "", DateTime? ToDateFilter = null, DateTime? FromDateFilter = null, int? DateFilterId = null,
            int? BookId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.BookTransactions.ToList();
            if (BookId != null)
            {
                query = (from s in db.BookTransactions
                         join a in db.BookIdentities on s.BookIdentityId equals a.BookIdentityId
                         join b in db.BookMasters on a.BookId equals b.BookId
                         join c in db.BookMemberTypes on s.BookMemberTypeId equals c.BookMemberTypeId
                         where b.BookId == BookId
                         select s).ToList();
            }
            else
            {
                if (!String.IsNullOrEmpty(SearchText))
                {
                    query = (from s in db.BookTransactions
                             join a in db.BookIdentities on s.BookIdentityId equals a.BookIdentityId
                             join b in db.BookMasters on a.BookId equals b.BookId
                             join c in db.BookMemberTypes on s.BookMemberTypeId equals c.BookMemberTypeId
                             where (b.BookTitle.Contains(SearchText) || b.SubTitle.Contains(SearchText)
                             || b.ISBN.Contains(SearchText) || c.BookMemberType1.Contains(SearchText)
                             || a.UniqueId.Contains(SearchText))
                             select s).ToList();
                }
                else if (ToDateFilter != null || DateFilterId != null || FromDateFilter != null)
                {
                    if (DateFilterId == 1)
                    {
                        query = (from s in db.BookTransactions
                                 join a in db.BookIdentities on s.BookIdentityId equals a.BookIdentityId
                                 join b in db.BookMasters on a.BookId equals b.BookId
                                 join c in db.BookMemberTypes on s.BookMemberTypeId equals c.BookMemberTypeId
                                 where (s.TransactionDate >= FromDateFilter &&
                                 s.TransactionDate <= ToDateFilter)
                                 select s).ToList();
                    }
                    else if (DateFilterId == 2)
                    {
                        query = (from s in db.BookTransactions
                                 join a in db.BookIdentities on s.BookIdentityId equals a.BookIdentityId
                                 join b in db.BookMasters on a.BookId equals b.BookId
                                 join c in db.BookMemberTypes on s.BookMemberTypeId equals c.BookMemberTypeId
                                 where (s.ExpectedReturnDate >= FromDateFilter &&
                                 s.ExpectedReturnDate <= ToDateFilter)
                                 select s).ToList();
                    }
                    else
                    {
                        query = (from s in db.BookTransactions
                                 join a in db.BookIdentities on s.BookIdentityId equals a.BookIdentityId
                                 join b in db.BookMasters on a.BookId equals b.BookId
                                 join c in db.BookMemberTypes on s.BookMemberTypeId equals c.BookMemberTypeId
                                 where (s.ActualReturnDate >= FromDateFilter &&
                                 s.ActualReturnDate <= ToDateFilter)
                                 select s).ToList();
                    }
                }
            }


            totalcount = query.Count;

            var BookTransaction = new PagedList<KSModel.Models.BookTransaction>(query, PageIndex, PageSize);
            return BookTransaction;
        }

        public void InsertBookTransaction(BookTransaction BookTransaction)
        {
            if (BookTransaction == null)
                throw new ArgumentNullException("BookTransaction");

            db.BookTransactions.Add(BookTransaction);
            db.SaveChanges();
        }

        public void UpdateBookTransaction(BookTransaction BookTransaction)
        {
            if (BookTransaction == null)
                throw new ArgumentNullException("BookTransaction");

            db.Entry(BookTransaction).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookTransaction(int BookTransactionId = 0)
        {
            if (BookTransactionId == 0)
                throw new ArgumentNullException("BookTransaction");

            var bookTransaction = (from s in db.BookTransactions
                                   where s.BookTransactionId == BookTransactionId
                                   select s).FirstOrDefault();
            db.BookTransactions.Remove(bookTransaction);
            db.SaveChanges();
        }

        #endregion

        #region BookTransactionType

        public BookTrsType GetBookTrsTypeById(int? BookTrsTypeId, bool IsTrack = false)
        {
            if (BookTrsTypeId == 0)
                throw new ArgumentNullException("BookTrsType");

            var BookTrsType = new BookTrsType();

            if (IsTrack)
                BookTrsType = db.BookTrsTypes.Where(m => m.BookTrsTypeId == BookTrsTypeId).FirstOrDefault();
            else
                BookTrsType = db.BookTrsTypes.AsNoTracking().Where(m => m.BookTrsTypeId == BookTrsTypeId).FirstOrDefault();

            return BookTrsType;
        }

        public List<BookTrsType> GetAllBookTrsTypes(ref int totalcount, string BookTrsType = "",
                                            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.BookTrsTypes.ToList();

            if (!String.IsNullOrEmpty(BookTrsType))
                query = query.Where(e => e.BookTrsType1 == BookTrsType).ToList();

            totalcount = query.Count;

            var BookTrsTypes = new PagedList<KSModel.Models.BookTrsType>(query, PageIndex, PageSize);
            return BookTrsTypes;
        }

        public void InsertBookTrsType(BookTrsType BookTransactionType)
        {
            if (BookTransactionType == null)
                throw new ArgumentNullException("BookTrsType");

            db.BookTrsTypes.Add(BookTransactionType);
            db.SaveChanges();
        }

        public void UpdateBookTrsType(BookTrsType BookTransactionType)
        {
            if (BookTransactionType == null)
                throw new ArgumentNullException("BookTrsType");

            db.Entry(BookTransactionType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookTrsType(int BookTrsTypeId = 0)
        {
            if (BookTrsTypeId == 0)
                throw new ArgumentNullException("BookTransaction");

            var bookTransactiontype = (from s in db.BookTrsTypes
                                       where s.BookTrsTypeId == BookTrsTypeId
                                       select s).FirstOrDefault();
            db.BookTrsTypes.Remove(bookTransactiontype);
            db.SaveChanges();
        }

        #endregion

        #region BookMemberType

        public BookMemberType GetBookMemberTypeById(int? BookMemberTypeId, bool IsTrack = false)
        {
            if (BookMemberTypeId == 0)
                throw new ArgumentNullException("BookMemberType");

            var BookMemberType = new BookMemberType();

            if (IsTrack)
                BookMemberType = db.BookMemberTypes.Where(m => m.BookMemberTypeId == BookMemberTypeId).FirstOrDefault();
            else
                BookMemberType = db.BookMemberTypes.AsNoTracking().Where(m => m.BookMemberTypeId == BookMemberTypeId).FirstOrDefault();

            return BookMemberType;
        }

        public List<BookMemberType> GetAllBookMemberTypes(ref int totalcount, string BookMemberType = "", int? AllowedDays = null,
                                  bool? IsActive = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.BookMemberTypes.ToList();

            if (!String.IsNullOrEmpty(BookMemberType))
                query = query.Where(e => e.BookMemberType1 == BookMemberType).ToList();
            if (IsActive != null)
                query = query.Where(e => e.IsActive == IsActive).ToList();

            totalcount = query.Count;

            var BookMemberTypes = new PagedList<KSModel.Models.BookMemberType>(query, PageIndex, PageSize);
            return BookMemberTypes;
        }

        public void InsertBookMemberType(BookMemberType BookMemberType)
        {
            if (BookMemberType == null)
                throw new ArgumentNullException("BookMemberType");

            db.BookMemberTypes.Add(BookMemberType);
            db.SaveChanges();
        }

        public void UpdateBookMemberType(BookMemberType BookMemberType)
        {
            if (BookMemberType == null)
                throw new ArgumentNullException("BookMemberType");

            db.Entry(BookMemberType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookMemberType(int BookMemberTypeId = 0)
        {
            if (BookMemberTypeId == 0)
                throw new ArgumentNullException("BookMemberType");

            var BookMemberTypes = (from s in db.BookMemberTypes
                                   where s.BookMemberTypeId == BookMemberTypeId
                                   select s).FirstOrDefault();
            db.BookMemberTypes.Remove(BookMemberTypes);
            db.SaveChanges();
        }

        #endregion

        #region BookStatus

        public BookStatu GetBookStatusById(int? BookStatusId, bool IsTrack = false)
        {
            if (BookStatusId == 0)
                throw new ArgumentNullException("BookStatus");

            var BookStatus = new BookStatu();

            if (IsTrack)
                BookStatus = db.BookStatus.Where(m => m.BookStatusId == BookStatusId).FirstOrDefault();
            else
                BookStatus = db.BookStatus.AsNoTracking().Where(m => m.BookStatusId == BookStatusId).FirstOrDefault();

            return BookStatus;
        }

        public List<BookStatu> GetAllBookStatuss(ref int totalcount, string BookStatus = "", int? AllowedDays = null,
                                  int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.BookStatus.ToList();

            if (!String.IsNullOrEmpty(BookStatus))
                query = query.Where(e => e.BookStatus.ToLower() == BookStatus).ToList();

            totalcount = query.Count;

            var BookStatuss = new PagedList<KSModel.Models.BookStatu>(query, PageIndex, PageSize);
            return BookStatuss;
        }

        public void InsertBookStatus(BookStatu BookStatus)
        {
            if (BookStatus == null)
                throw new ArgumentNullException("BookStatus");

            db.BookStatus.Add(BookStatus);
            db.SaveChanges();
        }

        public void UpdateBookStatus(BookStatu BookStatus)
        {
            if (BookStatus == null)
                throw new ArgumentNullException("BookStatus");

            db.Entry(BookStatus).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteBookStatus(int BookStatusId = 0)
        {
            if (BookStatusId == 0)
                throw new ArgumentNullException("BookStatus");

            var BookStatuss = (from s in db.BookStatus
                                   where s.BookStatusId == BookStatusId
                                   select s).FirstOrDefault();
            db.BookStatus.Remove(BookStatuss);
            db.SaveChanges();
        }

        #endregion

        #endregion
    }
}