﻿using KSModel.Models;
using KS_SmartChild.ViewModel.Student;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.DAL.DocumentModule
{
    public partial interface IDocumentService
    {
        #region DocumentType

        DocumentType GetDocumentTypeById(int DocumentTypeId, bool IsTrack = false);

        List<DocumentType> GetAllDocumentTypes(ref int totalcount, string DocumentType = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        List<StudentPendingDocumentReportModel> GetStudentPendingDocumentTypeBySP(ref int totalcount, string DocumentType = "", int? ClassId = null,
                                                         int? SessionId = null, int PageIndex = 0, int PageSize = int.MaxValue);
        void InsertDocumentType(DocumentType DocumentType);

        void UpdateDocumentType(DocumentType DocumentType);

        void DeleteDocumentType(int DocumentTypeId = 0);

        #endregion

        #region AttachedDocument

        AttachedDocument GetAttachedDocumentById(int AttachedDocumentId, bool IsTrack = false);

        List<AttachedDocument> GetAllAttachedDocuments(ref int totalcount, int? DocumentTypeId = null, string Image = "",
            int? StudentId = null, int PageIndex = 0, int PageSize = int.MaxValue);
        string GetImageByStudentId(ref int totalcount,
           int? StudentId = null);
        void InsertAttachedDocument(AttachedDocument AttachedDocument);

        void UpdateAttachedDocument(AttachedDocument AttachedDocument);

        void DeleteAttachedDocument(int AttachedDocumentId = 0);

        #endregion

        #region DocumentGroup
        DocumentGroup GetDocumentGroupById(int DocumentGroupId, bool IsTrack = false);

        List<DocumentGroup> GetAllDocumentGroups(ref int totalcount, string DocumentGroup = "",
                            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertDocumentGroup(DocumentGroup DocumentGroup);

        void UpdateDocumentGroup(DocumentGroup DocumentGroup);

        void DeleteDocumentGroup(int DocumentGroupId = 0);

        #endregion

        #region DocumentDetail
        DocumentDetail GetDocumentDetailById(int DocumentDetailId, bool IsTrack = false);

        List<DocumentDetail> GetAllDocumentDetail(ref int totalcount, int DocumentId = 0, DateTime? IssuedOn = null,
        DateTime? ReminderDate = null, int ReminderType = 0, DateTime? ExpiryDate = null, DateTime? UploadedOn = null, string Remarks = "", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertDocumentDetail(DocumentDetail DocumentDetail);

        void UpdateDocumentDetail(DocumentDetail DocumentDetail);

        void DeleteDocumentDetail(int DocumentDetailId = 0);

        #endregion

        #region DocumentCategory
        DocumentCategory GetDocumentCategoryById(int DocumentCategoryId, bool IsTrack = false);

        IList<DocumentCategory> GetAllDocumentCategory(ref int totalcount, int DocumentCategoryId = 0, bool? IsActive = null, string DocumentCategory = "", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertDocumentCategory(DocumentCategory DocumentCategory);

        void UpdateDocumentCategory(DocumentCategory DocumentCategory);

        void DeleteDocumentCategory(int DocumentCategoryId = 0);

        #endregion

        #region Document
        Document GetDocumentById(int DocumentId, bool IsTrack = false);

        IList<Document> GetAllDocument(ref int totalcount, int DocumentCategoryId = 0, bool? IsActive = null, 
                                        string DocumentTitle = "", string DocumentDescription = "", int PageIndex = 0, 
                                        int PageSize = int.MaxValue);

        void InsertDocument(Document Document);

        void UpdateDocument(Document Document);

        void DeleteDocument(int DocumentId = 0);

        #endregion

        #region DocReminderType
        DocReminderType GetDocReminderTypeById(int ReminderTypeId, bool IsTrack = false);

        List<DocReminderType> GetAllDocReminderTypes(ref int totalcount, string ReminderType = "", bool? IsActive = null,
                                                    int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertDocReminderType(DocReminderType DocReminderType);

        void UpdateDocReminderType(DocReminderType DocReminderType);

        void DeleteDocReminderType(int ReminderTypeId = 0);

        #endregion
    }
}
