﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Attendence
{
    public class ManageAttendenceStatus
    {
        public ManageAttendenceStatus()
        {
            StudentList = new List<StudentList>();
            PeriodList = new List<SelectListItem>();
            HeaderList = new List<SelectListItem>();
            SessionList = new List<SelectListItem>();
            ClassList = new List<SelectListItem>();
            AttendanceStatusList = new List<SelectListItem>();
            studentDailyAttendance = new List<StudentDailyAttendance>();
            StudentAttendanceStatusList = new List<SelectListItem>();
            AvailableSendMessageMobileNoOptions = new List<SelectListItem>();
            MultipleSelectedAttendanceStatus = new List<SelectListItem>();
        }
       

        public int TimetableId { get; set; }

        public int? PeriodId { get; set; }

        public IList<SelectListItem> PeriodList { get; set; }

        public IList<SelectListItem> HeaderList { get; set; }

        public IList<SelectListItem> SessionList { get; set; }

        public int? SessionId { get; set; }

        public int? ClassId { get; set; }

        public string SendMessageMobileNoSelectionId { get; set; }

        public IList<SelectListItem> AvailableSendMessageMobileNoOptions { get; set; }

        public IList<SelectListItem> ClassList { get; set; }

        public bool SMS { get; set; }

        public bool ToNonAppUser { get; set; }

        public bool Notification { get; set; }

        public string SelectedMobile { get; set; }

        public string SelectedItems { get; set; }

        public string[] StatusSelection { get; set; }

        public IList<SelectListItem> MultipleSelectedAttendanceStatus { get; set; }

        public string MobileNoSelection { get; set; }

        public string SMSSelectionStudentMobileNo { get; set; }

        public string DefaultAttendanceStatusId { get; set; }

        public IList<SelectListItem> AttendanceStatusList { get; set; }

        public string AttendanceStatusId { get; set; }

        public IList<SelectListItem> StudentAttendanceStatusList { get; set; }

        public DateTime Date { get; set; }

        public string AttDate { get; set; }

        public string modifiedDate { get; set; }

        public string IntTime { get; set; }

        public string TextForUnMarkedAtt { get; set; }

        public string HasCountAttStatus { get; set; }

        public bool IsEnablePeriodWise { get; set; }

        public string OutTime { get; set; }

        public string StudentAttedanceStatusList { get; set; }

        public string ClassSubjectLabel { get; set; }

        public bool IsLogInByIncharge { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsAllPeriodAttendanceAllowed { get; set; }

        public IList<StudentDailyAttendance> studentDailyAttendance { get; set; }

        public string TimeTableMessage { get; set; }

        public bool IsMultipleClassIncharge { get; set; }

        public IList<StudentList> StudentList { get; set; }

        public bool IsPeriodAttendance { get; set; }

        public string ClassName { get; set; }

        public string SubjectName { get; set; }

        public bool IsAllowCopyDailyAttendanceToAll { get; set; }

        public bool IsCopyDailyAttendanceToAllEnabled { get; set; }

        public bool IsCopyDailyAttendanceToAllChecked { get; set; }

        public DateTime? SessionStartDate { get; set; }

        public DateTime? SessionEndDate { get; set; }

        public bool IsSessionShow { get; set; }

        public bool IsAuthToAdd { get; set; }

    }
    
    public class StudentList
    {
        public string ClassName { get; set; }

        public string SubjectName { get; set; }

        public string AttendanceAbbr { get; set; }

        public int AttendanceId { get; set; }

        public int StudentId { get; set; }

        public string RollNo { get; set; }

        public string StudentName { get; set; }

        public bool IsAuto { get; set; }

        public string AttendanceStatusId { get; set; }

        public string AttendanceStatus { get; set; }

        public bool IsLeave { get; set; }

        public string IntTime { get; set; }

        public string OutTime { get; set; }

        public string ColorCode { get; set; }

        public bool IsInActive { get; set; }

        public bool? hasAttendance { get; set; }
        public IList<SelectListItem> AttendanceStatusList { get; set; }
    }

    public class SMSAbsentStudentReport : StudentList
    {
        public SMSAbsentStudentReport()
        {
            AvialableClassLists = new List<SelectListItem>();
            AvailableStatusList = new List<SelectListItem>();
        }

        public string AttendanceDate { get; set; }

        public int ClassId { get; set; }

        public string selectedClassid { get; set; }

        public string SMSSelectionStudentMobileNo { get; set; }

        public string SendMessageMobileNoSelectionId { get; set; }

        public IList<SelectListItem> AvailableStatusList { get; set; }

        public string AdmnNo { get; set; }

        public int SessionId { get; set; }

        public int ModuleId { get; set; }

        public bool IsAuto { get; set; }

        public bool IsUnMarkedAttendance { get; set; }

        public string AbsentStudentAttedanceList { get; set; }

        public IList<SelectListItem> AvialableClassLists { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }
    }

    public class StudentAttendanceReport
    {
        public StudentAttendanceReport()
        {
            AttendanceStatusList = new List<SelectListItem>();
            ClassList = new List<SelectListItem>();
            AvailableSessions = new List<SelectListItem>();
            studentAttendanceStatus = new List<string>();
            AttendanceTypes = new List<SelectListItem>();
            AttendanceModes = new List<SelectListItem>();
        }

        public int StudentPunchCount { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string ViewAttnStatus { get; set; }

        public int StudentId { get; set; }
        
        public string Student_Id { get; set; }
        
        public string RollNo { get; set; }

        public string StudentName { get; set; }

        public int[] AttendanceStatusIds { get; set; }

        public int AttendanceStatusId { get; set; }

        public string AttendanceStatus { get; set; }

        public string AttendanceAbbr { get; set; }

        public string Date { get; set; }

        public bool ShowDateColumn { get; set; }

        public DateTime?  AttendanceDate { get; set; }

        public string AttendanceInTime { get; set; }

        public string AttendanceOutTime { get; set; }

        public string AttendanceMode { get; set; }

        public string AttendanceIntimeMode { get; set; }

        public string AttendanceInOutMode { get; set; }

        public IList<SelectListItem> AttendanceStatusList { get; set; }

        public IList<SelectListItem> AttendanceTypes { get; set; }
        public string AttendanceTypeId { get; set; }

        public IList<SelectListItem> AttendanceModes { get; set; }
        public string AttendaceModeId { get; set; }
        public int ClassId { get; set; }

        public string Class_Id { get; set; }
        
        public IList<SelectListItem> ClassList { get; set; }

        public string ClassName { get; set; }

        public int SessionId { get; set; }

        public string IsAuto { get; set; }

        public string IsOutAuto { get; set; }

        public bool IsAuthToGenrateReport { get; set; }

        public IList<SelectListItem> AvailableSessions { get; set; }

        public List<string> studentAttendanceStatus { get; set; }

        public string MonthName { get; set; }

        public string TptType { get; set; }

        public string BoardingPoint { get; set; }
    }

    public class StudentDailyAttendance
    {
        public int StudentId { get; set; }

        public string RollNo { get; set; }

        public string StudentName { get; set; }

        public int AttendanceStatusId { get; set; }

        public string ColorCode { get; set; }

        public bool IsLeave { get; set; }

        public string AttendanceAbbr { get; set; }

        public bool IsInActive { get; set; }

        public string StringAttendanceStatusId { get; set; }

        public string IntTime { get; set; }

        public string OutTime { get; set; }

        public IList<SelectListItem> AttendanceStatusAbbrList { get; set; }

        public IList<StudentPeriodWiseAttendance> studentPeriodWiseAttendanceList { get; set; }
    }

    public class StudentSummaryAttendance
    {
        public string ClassId { get; set; }

        public string ClassName { get; set; }

        public string TotalStudents { get; set; }

        public string TotalPresents { get; set; }

        public string TotalAbsents { get; set; }

        public string TotalNotMarked { get; set; }

        public string TotalLeaves { get; set; }

       

    }

    public class StudentPeriodWiseAttendance
    {
        public string AttendanceAbbr { get; set; }

        public string ColorCode { get; set; }

        public bool IsLeave { get; set; }

        public int PeriodAttendanceStatusId { get; set; }

        public int PeriodId { get; set; }

        public IList<SelectListItem> AttendanceStatusPeriodWiseAbbrList { get; set; }
    }

    public class StaffAttendanceListModel
    {
        public int? StaffId { get; set; }

        public int AttendanceId { get; set; }

        public string AttendanceStatusId { get; set; }

        public string AttendanceStatusAbbr { get; set; }

        public string Name { get; set; }

        public string StaffType { get; set; }

        public string EmpCode { get; set; }

        public string IntTime { get; set; }

        public string OutTime { get; set; }

        public bool? IsIntimeInserted { get; set; }

        public bool? IsOuttimeInserted { get; set; }
        public string Date { get; set; }

        public bool IsAuto { get; set; }

        public DateTime AttendanceDate { get; set; }

        public string AttendanceStatus { get; set; }
    }

    public class StaffAttendanceModel
    {
        public StaffAttendanceModel()
        {
            StatusList = new List<SelectListItem>();
            StaffAttendanceStatusList = new List<SelectListItem>();
            StaffAttendanceStatusCloseList = new List<SelectListItem>();
        }
        public int? StatusId { get; set; }

        public IList<SelectListItem> StatusList { get; set; }

        public IList<SelectListItem> StaffAttendanceStatusList { get; set; }

        public IList<SelectListItem> StaffAttendanceStatusCloseList { get; set; }

        public string HasCountAttStatus { get; set; }

        public string StaffAttendanceList { get; set; }

        public string StaffAttendanceStatusId { get; set; }

        public string StaffAttendanceStatusCloseId { get; set; }

        public string IntTime { get; set; }

        public string DefaultAttendanceStatusId { get; set; }

        public string TextForUnMarkedAtt { get; set; }

        public string OutTime { get; set; }

        public string CloseAttendTime { get; set; }

        public bool Notification { get; set; }

        public bool SMS { get; set; }

        public bool NonAppUser { get; set; }

        public string date { get; set; }

        public bool IsAuthToAdd { get; set; }
    }
    //added on 22March
    public class AttendanceCardModel
    {
        public AttendanceCardModel()
        {
            DeviceList = new List<SelectListItem>();
            LocationList = new List<SelectListItem>();
        }
        public string LocationId { get; set; }
        public List<SelectListItem> LocationList { get; set; }
        public string Device { get; set; }
        public string EndDate { get; set; }
        public string EffectiveDate { get; set; }
        public string DeviceId { get; set; }
        public string CalStartDate { get; set; }
        public string CalEndDate { get; set; }
        public List<SelectListItem> DeviceList { get; set; }
        public string EncAttendanceDeviceLocationId { get; set; }
        public bool IsAuthToAdd { get; set; }
        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToExport { get; set; }
        public bool IsAuthToDelete { get; set; }
        public string gridPageSizes { get; set; }
        public string defaultGridPageSize { get; set; }
    }
    public class AttendanceCardGridModel
    {
        public string EncAttendanceDeviceLocationId { get; set; }
        public int AttendanceDeviceLocationId { get; set; }
        public string AttendanceDeviceId { get; set; }
        public string AttendanceLocationId { get; set; }
        public string EffectiveDate { get; set; }
        public string Location { get; set; }
        public string EndDate { get; set; }
        public string Device { get; set; }
        public string AttendanceLocation { get; set; }
        public string MyProperty { get; set; }
        public bool IsAttendanceDeletable { get; set; }
        //public string MyProperty { get; set; }
    }

    public class AttendanceUserCardModel
    {
        public AttendanceUserCardModel()
        {
            AvailableContactTypes = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
            SessionList= new List<SelectListItem>();
            UserList = new List<SelectListItem>();
        }
        public int ContactTypeId { get; set; }
        public IList<SelectListItem> AvailableContactTypes { get; set; }
        public int ClassId { get; set; }
        public IList<SelectListItem> AvailableClasses { get; set; }
        public IList<SelectListItem> UserList { get; set; }
        public string gridPageSizes { get; set; }
        public string defaultGridPageSize { get; set; }
        public string SerialNo { get; set; }
        public IList<SelectListItem> SessionList { get; set; }
        public string OldSerialNo { get; set; }
        public int? SessionId { get; set; }
        public int? NextSessionId { get; set; }
        public int UserId { get; set; }
    }
    public class AttendanceUserCardDeatilsModel
    {
        public AttendanceUserCardDeatilsModel()
        {
            ColumnsNames = new List<string>();
            RowsNames = new List<List<string>>();
        }
        public List<string> ColumnsNames { get; set; }
        public List<List<string>> RowsNames { get; set; }

    }
    public class ImportAttendancePunch
    {
        public ImportAttendancePunch()
        {
            DeviceTypeList = new List<SelectListItem>();
        }
        public IList<SelectListItem> DeviceTypeList { get; set; }
        public string DeviceType { get; set; }
        public string PunchId { get; set; }
        public string AttendanceDeviceId { get; set; }
        public string PunchTime { get; set; }
        public string ContactTypeId { get; set; }
        public string UserId { get; set; }
        public string PunchTypeId { get; set; }

        public bool IsDateEmbaded { get; set; }

        public string SelectedDate { get; set; }
    }


    public class AttendanceSerial
    {
        public string AttendanceDeviceId { get; set; }
        public string AttendanceDevice { get; set; }
    }

    public class StudentAttendanceDetailReport
    {

        public StudentAttendanceDetailReport()
        {
            StudentAttendanceDetailReportList = new List<StudentAttendanceDetailReport>();
        }
        public IList<StudentAttendanceDetailReport> StudentAttendanceDetailReportList { get; set; }

        public int SN { get; set; }

        public int StudentId { get; set; }

        public string Student_Id { get; set; }

        public string RollNo { get; set; }

        public string StudentName { get; set; }

        public string Class { get; set; }

        public string Time { get; set; }

        public string Location { get; set; }

        public string Date { get; set; }

        public string DeviceSerialNo { get; set; }

    }

    public class StudentAttendance
    {
        public StudentAttendance()
        {
            monthlyAttendance = new List<Student.AppStudentAttendanceModel>();
            ClassList = new List<SelectListItem>();
            StudentList = new List<SelectListItem>();
            StaffList = new List<SelectListItem>();
        }

        public string Name { get; set; }
        public string AdmsnNo { get; set; }
        public string gridPageSizes { get; set; }
        public string defaultGridPageSize { get; set; }
        public string Attendance { get; set; }
        public decimal WorkingTotalDays { get; set; }
        public decimal TotalPresentAttendance { get; set; }
        public int TotalAbsent { get; set; }
        public int TotalOnDuty { get; set; }
        public int TotalLeave { get; set; }
        public int TotalNotMarked { get; set; }
        public Int64 TotalWorkDays { get; set; }
       
        public decimal TotalPresents { get; set; }

        public string StudentImage { get; set; }

        public string Class { get; set; }
        public string EmpCode{ get; set; }

        public string RollNo { get; set; }
        public string AdmissionNo { get; set; }
        public int monthscount { get; set; }

        public int absentcount { get; set; }

        public int staffid { get; set; }
        public int classid { get; set; }
        public string studentid { get; set; }
        public string TotalPresentsYear { get; set; }
        public string TotalLeaveYear { get; set; }
        public string TotalOtherYear { get; set; }
        public string TotalAbsentYear { get; set; }
        public string TotalWorkingDaysYear { get; set; }

        public bool IsStudentSelected { get; set; }
        public bool IsAdmin { get; set; }
        public IList<SelectListItem> ClassList { get; set; }
        public IList<SelectListItem> StaffList { get; set; }
        

        public IList<SelectListItem> StudentList { get; set; }
        public IList<Student.AppStudentAttendanceModel> monthlyAttendance { get; set; }
        
    }
}



