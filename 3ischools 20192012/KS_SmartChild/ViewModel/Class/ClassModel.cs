﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Class
{
    public class ClassModel
    {

    }
    public class ClassMasterModel
    {
        public int? ClassId { get; set; }

        public int SectionId { get; set; }

        public int StandardId { get; set; }

        public int? Index { get; set; }

        public string Class { get; set; }

        public string RoomNo { get; set; }

        public string strIndex { get; set; }

        public int CurrentSession { get; set; }

        public IList<SelectListItem> AvailableSessions { get; set; }

        public IList<SelectListItem> AvailableSection { get; set; }

        public IList<SelectListItem> AvailableStandard { get; set; }

        public IList<SelectListItem> AvailableSectionFilter { get; set; }

        public IList<SelectListItem> AvailableStandardFilter { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToDelete { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string EncClassId { get; set; }

        public string Section { get; set; }

        public string Standard { get; set; }

        public int Session { get; set; }

        public bool IsClassNameEditable { get; set; }

        public bool ClassDeletionFlag { get; set; }

        public string StudentCount { get; set; }

        public int? MaleCount { get; set; }

        public int? FemaleCount { get; set; }

        public int? OtherCount { get; set; }

        public bool IsModelError { get; set; }

        public bool IsAllSectionsBound { get; set; }
        public bool IsAuthToView { get; set; }
    }


    public class VirtualClassModel
    {
        public VirtualClassModel()
        {
            ClassList = new List<SelectListItem>();
            SessionList = new List<SelectListItem>();
            VirtualClassList = new List<SelectListItem>();
        }

        public string VirtualClassId { get; set; }

        public string SessionId { get; set; }

        public string VirtualClass { get; set; }

        public string ClassId { get; set; }

        public IList<SelectListItem> ClassList { get; set; }

        public string[] ClassIds { get; set; }

        public IList<SelectListItem> VirtualClassList { get; set; }

        public IList<SelectListItem> SessionList { get; set; }
    }

    public class VirtualClassDataModel
    {
        public int SrNo { get; set; }

        public string StudentClassId { get; set; }

        public string StudentName { get; set; }

        public string Class { get; set; }

        public string RollNo { get; set; }

        public string AdmnNo { get; set; }

        public bool IsSaved { get; set; }
    }

}