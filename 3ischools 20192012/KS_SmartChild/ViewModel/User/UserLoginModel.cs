﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace KS_SmartChild.ViewModel.User
{
    public partial class UserLoginModel
    {
        public UserLoginModel()
        {
        }

        public string UserContactId { get; set; }

        public string UserId { get; set; }

        public string[] UserIds { get; set; }

        public string UserTypeId { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string Status { get; set; }

        public bool setColor { get; set; }

        public bool IsAuthToGridDelete { get; set; }

        public bool IsAuthToGridEdit { get; set; }

        public bool IsAuthToGridView { get; set; }

        public bool IsAuthToGirdAdd { get; set; }

        public bool IsAuthToDelete { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToAdd { get; set; }
    }

    public class UserLoginCheckViewModel : UserLoginModel
    {
        public UserLoginCheckViewModel()
        {
            AaviableUsers = new List<SelectListItem>();
            AaviableUserTypes = new List<SelectListItem>();
        }

        public string UserLoginCheckId { get; set; }

        public string UserTypeValue { get; set; }

        public string UserName { get; set; }

        public string FullName { get; set; }

        public string IP { get; set; }

        public string Datefrom { get; set; }

        public string DateTill { get; set; }

        public string TimeFrom { get; set; }

        public string TimeTill { get; set; }

        public IList<SelectListItem> AaviableUsers { get; set; }

        public IList<SelectListItem> AaviableUserTypes { get; set; }
    }

    public class UserList 
    {
        public string ContactType { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
    }


    public class ManageUser
    {
        public ManageUser()
        {
            UserList = new List<UserList>();
        }
        public IList<UserList> UserList { get; set; }

        public string UserInfo { get; set; }

    }



}