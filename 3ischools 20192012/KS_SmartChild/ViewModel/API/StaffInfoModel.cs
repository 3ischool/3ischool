﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.ViewModel.API
{
    public class StaffInfoModel
    {

        public string PhoneNo { get; set; }

        public string AddressType { get; set; }

        public string PlotNo { get; set; }

        public string SectorOrStreet { get; set; }

        public string Locality { get; set; }

        public string Zip { get; set; }

        public string Department { get; set; }

        public string Designation { get; set; }

        public string Gender { get; set; }

        public string City { get; set; }

        public string Email { get; set; }

        public string Employeecode { get; set; }
    }
    public class AppStaffTimeTable
    {
        public string Period { get; set; }

        public string PeriodTime { get; set; }

        public string Subject { get; set; }

        public string Class { get; set; }

    }

}