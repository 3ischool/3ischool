﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Stock
{
    public class Vouchers
    {
        public string defaultGridPageSize { get; set; }
        public string gridPageSizes { get; set; }
        public bool IsAuthToDelete { get; set; }
        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToAdd { get; set; }
    }

    public class VoucherInfo : Vouchers
    {
        public VoucherInfo()
        {
            StoreList = new List<SelectListItem>();
            VouchertypeList = new List<SelectListItem>();
            AccountTypeList = new List<SelectListItem>();
            AccountList = new List<SelectListItem>();
            PackageList = new List<SelectListItem>();
            TaxTypeList = new List<SelectListItem>();
            ProductGroupList = new List<SelectListItem>();
            ProductTypeList = new List<SelectListItem>();
            ProductList = new List<SelectListItem>();
            //ProductDetail = new List<ProductsDetailList>();
        }
        public string VoucherId { get; set; }
        public string Voucher { get; set; }
        public List<SelectListItem> StoreList { get; set; }
        public string StoreId { get; set; }
        public DateTime VoucherFromdate { get; set; }
        public DateTime VoucherToDate { get; set; }
        public List<SelectListItem> VouchertypeList { get; set; }
        public string VoucherTypeId { get; set; }
        public DateTime VoucherDate { get; set; }
        public List<SelectListItem> AccountTypeList { get; set; }
        public string AccountTypeId { get; set; }
        public List<SelectListItem> AccountList { get; set; }
        public string AccountId { get; set; }
        public string DocumentNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public List<SelectListItem> PackageList { get; set; }
        public List<SelectListItem> TaxTypeList { get; set; }
        public string TaxTypeId { get; set; }
        public string PackageId { get; set; }
        public List<SelectListItem> ProductGroupList { get; set; }
        public List<SelectListItem> ProductTypeList { get; set; }
        public List<SelectListItem> ProductList { get; set; }
        public string Quantity { get; set; }
        public string ProductId { get; set; }
        public string ProductDetail { get; set; }
        //public List<ProductsDetailList> ProductDetail { get; set; }
    }

    public class ProductsDetailList
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string Quantity { get; set; }
        public string Price { get; set; }
        public string GSTRate { get; set; }
        public string CGSTValue { get; set; }
        public string SGSTValue { get; set; }
        public string IGSTValue { get; set; }
        public string CessValue { get; set; }
        public string TotalAmount { get; set; }
        public string TaxableAmount { get; set; }
    }
    public class Inv_Store
    {
        public string StoreId { get; set; }
        public string StoreName { get; set; }
    }
    public class AccountTypeMaster
    {
        public string AccountTypeId { get; set; }
        public string AccountType { get; set; }
    }
    public class AccountListD
    {
        public string AccountId { get; set; }
        public string Account { get; set; }
    }
}