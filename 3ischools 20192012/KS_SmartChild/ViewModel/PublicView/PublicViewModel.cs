﻿using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.PublicViewModel
{
    public class PublicViewModel
    {
        public PublicViewModel()
        {
        }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

    }

    public class StudentAttendanceModel : PublicViewModel
    {
        public StudentAttendanceModel()
        {
            AvaiableClasses = new List<SelectListItem>();
        }

        public string ClassId { get; set; }

        public string Class { get; set; }

        public string Date { get; set; }

        public string StudentName { get; set; }

        public string RollNo { get; set; }

        public string AttendanceAbbr { get; set; }

        public IList<SelectListItem> AvaiableClasses { get; set; }
    }

}