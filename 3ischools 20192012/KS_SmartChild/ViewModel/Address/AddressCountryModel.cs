﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.ViewModel.Address
{
    public class AddressCountryModel
    {
        public int? CountryId { get; set; }

        public string CountryName { get; set; }
    }
}