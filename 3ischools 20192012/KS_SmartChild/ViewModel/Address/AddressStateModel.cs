﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Address
{
    public class AddressStateModel
    {
        public AddressStateModel()
        {
            AvailableCountries = new List<SelectListItem>();
            AddressCountryModel = new AddressCountryModel();
        }

        public int? StateId { get; set; }

        public int? State_CountryId { get; set; }

        public string StateName { get; set; }

        public IList<SelectListItem> AvailableCountries { get; set; }

        public AddressCountryModel AddressCountryModel { get; set; }
    }
}