﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.TimeTable
{
    public class TimeTableModel
    {
        public TimeTableModel()
        {
            TimeTablePeriodsList = new List<TimeTablePeriods>();
            DaysModelList = new List<DaysModel>();
            TimeTableClassesList = new List<TimeTableClasses>();
            TimeTableSubjectsList = new List<TimeTableSubjects>();
            TimeTableTeacherList = new List<TimeTableTeacher>();
            TimeTableGrid = new List<TimeTableGridModel>();
            TimeTableList = new List<TimeTableViewModel>();
            StandardList = new List<SelectListItem>();
            SessionList = new List<SelectListItem>();
            TimeTableGridModellist = new List<TimeTableGridModel>();
        }
        public string PeriodDetail { get; set; }


        public IList<TimeTableGridModel> TimeTableGridModellist { get; set; }
        public bool IsExcel { get; set; }
        [AllowHtml]
        public string  HTMLString { get; set; }
        public int[] TeacherIds { get; set; }
        public int[] ClassIds { get; set; }
        public int[] PeriodIdList { get; set; }
        public int[] DayIds { get; set; }
        public int TimetableIdPrint { get; set; }
        public int SessionId { get; set; }
        
        public int? TimeTableId { get; set; }

        public bool IsAdmin { get; set; }

        public bool SMS { get; set; }

        public bool NonAppUser { get; set; }

        public bool Notification { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public IList<TimeTablePeriods> TimeTablePeriodsList { get; set; }

        public int? PeriodId { get; set; }

        public IList<DaysModel> DaysModelList { get; set; }

        public string DayArray { get; set; }

        public IList<TimeTableClasses> TimeTableClassesList { get; set; }

        public int? ClassId { get; set; }

        public IList<TimeTableSubjects> TimeTableSubjectsList { get; set; }

        public int? SubjectId { get; set; }

        public int? SkillId { get; set; }

        public IList<TimeTableTeacher> TimeTableTeacherList { get; set; }

        public int? TeacherId { get; set; }

        public IList<TimeTableGridModel> TimeTableGrid { get; set; }

        public bool IsConflict { get; set; }

        public bool ConflictProceed { get; set; }

        public IList<TimeTableViewModel> TimeTableList { get; set; }

        public string Day { get; set; }

        public bool IsCopyFrom { get; set; }

        public int? ClassPeriodId { get; set; }

        public int? ClassPeriodDetailId { get; set; }

        public bool IsTimeTablePeriodWsie { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToExtend { get; set; }

        public bool IsAuthToClose { get; set; }

        public bool IsAuthToCopy { get; set; }

        public bool IsAuthToSetTimeTable { get; set; }

        public bool IsAuthToDelete { get; set; }

        public bool IsStudentorGuardian { get; set; }

        public bool IsForAll { get; set; }

        public int[] PeriodIds { get; set; }

        public List<SelectListItem> StandardList { get; set; }

        public List<SelectListItem> SessionList { get; set; }
        
        public bool IsAuthToView { get; set; }

        public bool PrintOnSamePage { get; set; }

        public bool IsAuthToPrint { get; set; }
        public bool IsAuthToDownloadPdf { get; set; }
        public bool IsAuthToExportToExcel { get; set; }
        public int VirtualClassId { get; set; }
    }

    public class TimeTableViewModel
    {
        public int TimeTableId { get; set; }

        public string EffectiveFrom { get; set; }

        public string EffectiveTill { get; set; }

        public DateTime? EffectiveFromDate { get; set; }

        public DateTime? EffectiveTillDate { get; set; }

        public DateTime? ClosingTimeTableDate { get; set; }

        public DateTime? ExtendEffectiveFromDate { get; set; }

        public DateTime? ExtendEffectiveTillDate { get; set; }

        public bool ExtendFromDate { get; set; }

        public bool IsDeleted { get; set; }

        public int? CopyFromTimeTableId { get; set; }

        public string CopyFromClassArray { get; set; }

        public DateTime? CurrentUtcDate { get; set; }
    }

    public class DaysModel
    {
        public int DayId { get; set; }

        public string DayName { get; set; }

        public string DayAbbr { get; set; }
    }

    public class TimeTablePeriods
    {
        public int PriodId { get; set; }

        public string PriodName { get; set; }

        public string PriodIndex { get; set; }

        public string PriodStartTime { get; set; }

        public string PriodEndTime { get; set; }

        public bool IsDisabled { get; set; }
    }

    public class TimeTableClasses
    {
        public int ClassId { get; set; }

        public int VirtualClassId { get; set; }

        public string ClassName { get; set; }

        public string AssignedSubject { get; set; }

        public bool IsDisabled { get; set; }

        public bool? IsMotherTeacher { get; set; }
    }

    public class TimeTableSubjects
    {
        public int SubjectId { get; set; }

        public int SubjectSkillId { get; set; }

        public string SubjectName { get; set; }

        public string AssignedTeacher { get; set; }

        public bool IsSubjectAssignedToPeriod { get; set; }
    }

    public class TimeTableTeacher
    {
        public int TeacherId { get; set; }

        public string TeacherName { get; set; }

        public string TeacherImage { get; set; }
    }

    public class TimeTableGridModel
    {
        public string Day { get; set; }

        public string PeriodDay { get; set; }

        public string Period { get; set; }

        public int? PeriodId { get; set; }

        public string strPeriodId { get; set; }
        public string PeriodTime { get; set; }

        public string Class { get; set; }

        public string strClasses { get; set; }

        public int? ClassId { get; set; }

        public string strClassId { get; set; }
        public string TeacherSubject { get; set; }

        public string TimeTableClassPeriodId { get; set; }

        public string TimeTableClassPeriodDetailId { get; set; }

        public int VirtaulclassId { get; set; }

        public string strVirtaulclassId { get; set; }

        public int ClassPeriodId { get; set; }

        public int ClassPeriodDetailId { get; set; }

        public int DayId { get; set; }

        public string StaffName { get; set; }

        public string SubjectId { get; set; }

        public string SkillId{ get; set; }

    }

    public class TimeTableRecentRecordList
    {
        public string Period { get; set; }

        public string Class { get; set; }

        public string Teacher { get; set; }

        public string Subject { get; set; }
    }

    public class CopyFromClassList
    {
        public int? ClassId { get; set; }
        public int? VirtualClassId { get; set; }
    }

    public class StudentTimeTable
    {

        public int ClassId { get; set; }

        public int TimeTableId { get; set; }

    }

    public class TimeTablePeriodQuotaModel
    {
        public TimeTablePeriodQuotaModel()
        {

            TeachingStaffList = new List<SelectListItem>();
            DepartmentList = new List<SelectListItem>();
            DesignationList = new List<SelectListItem>();
            //   TimeTableGridModellist = new List<TimeTableGridModel>();
        }
        public int? TimeTablePeriodQuotaId { get; set; }

        public string EncTTPQid { get; set; }

        public string TimeTablePeriodQuota { get; set; }

        public int? DailyPeriodQuota { get; set; }

        public int? WeeklyPeriodQuota { get; set; }

        public bool? IsDeleted { get; set; }

        public string gridPageSizes { get; set; }
        public string defaultGridPageSize { get; set; }

        public string EncStaffId { get; set; }

        public string EmpCode { get; set; }
        public string StaffName { get; set; }
        public string DepartMent { get; set; }

        public string DesigNation { get; set; }

        public string DisableError { get; set; }
        public bool? IsDisable { get; set; }
        public bool? IsChecked { get; set; }

        public string AssignTeachersCount { get; set; }
        public List<SelectListItem> TeachingStaffList { get; set; }

        public List<SelectListItem> DepartmentList { get; set; }
        public List<SelectListItem> DesignationList { get; set; }
    }

    public class StaffPeriodDetailModel
    {
       
        public int? TimeTablePeriodQuotaId { get; set; }

        public int? TimeTableId { get; set; }
        public int? StaffId { get; set; }
        public string EncTTPQid { get; set; }
        public string TimeTablePeriodQuota { get; set; }
        public int? DailyPeriodQuota { get; set; }
        public int? WeeklyPeriodQuota { get; set; }
        public string gridPageSizes { get; set; }
        public string defaultGridPageSize { get; set; }
        public string EncStaffId { get; set; }
        public string StaffName { get; set; }
        public string DepartMent { get; set; }
        public string DesigNation { get; set; }

        public int? Monday { get; set; }
        public int? Tuesday { get; set; }
        public int? Wednesday{ get; set; }
        public int? Thursday { get; set; }
        public int? Friday { get; set; }
        public int? Saturday { get; set; }
        

    }
}