﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.ViewModel.TimeTable
{

    public class AdjustmentListReportModel
    {
        public AdjustmentListReportModel()
        {
            List = new List<AdjustmentReportModel>();
        }
        public List<AdjustmentReportModel> List { get; set; }
        public bool IsAuthToExport { get; set; }
        public string ScrollSpeed { get; set; }
    }

    public class AdjustmentReportModel
    {
        public string Class { get; set; }

        public string Period { get; set; }

        public string OnDutyTeacher { get; set; }

        public string AdjustedTeacher { get; set; }
    }
}