﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.ViewModel.TimeTable
{
    public class TeacherTimeTableModel
    {
        public int ClassId { get; set; }

        public string Class { get; set; }

        public string Subject { get; set; }

        public string Period { get; set; }

        public string StudentCount { get; set; }

        public string EncClassId { get; set; }

        public bool IsVirtualClass { get; set; }
    }

    public class StudentClassModel
    {
        public int StudentId { get; set; }

        public string StudentName { get; set; }

        public string Email { get; set; }

        public string PhoneNo { get; set; }

        public string AdmissionNo { get; set; }

        public string EncStudentId { get; set; }

        public string RollNo { get; set; }

        public string StudentStatus { get; set; }
    }

}