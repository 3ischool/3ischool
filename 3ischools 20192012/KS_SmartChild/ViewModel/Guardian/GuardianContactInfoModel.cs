﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.ViewModel.Guardian
{
    public class GuardianContactInfoModel
    {
        public int ContactInfoId { get; set; }

        public string ContactInfo { get; set; }

        public Nullable<int> ContactInfoTypeId { get; set; }

        public Nullable<int> ContactId { get; set; }

        public Nullable<bool> Status { get; set; }

        public Nullable<bool> IsDefault { get; set; }

        public Nullable<int> ContactTypeId { get; set; }
    }
}