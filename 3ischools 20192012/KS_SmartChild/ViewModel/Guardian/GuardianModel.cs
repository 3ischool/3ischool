﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Guardian
{
    public class GuardianModel
    {
        public GuardianModel()
        {
            AvailableGuardianType = new List<SelectListItem>();
            AvailableQualification = new List<SelectListItem>();
            AvailableOccupation = new List<SelectListItem>();
            GuardianAddressModel = new GuardianAddressModel();
            GuardianContactInfoModel = new List<GuardianContactInfoModel>();
            AvailableIncomeSlab = new List<SelectListItem>();
        }

        public int? GuardianId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName { get; set; }

        public int? GuardianTypeId { get; set; }

        public string GuardianType { get; set; }

        public string GuardianAddressType { get; set; }

        public int? QualificationId { get; set; }

        public string Qualification { get; set; }

        public int? OccupationId { get; set; }

        public string Occupation { get; set; }

        public string Income { get; set; }

        public string IncomeSlabId { get; set; }

        public string GuardianAddressShow { get; set; }

        public string GuardianAddressSave { get; set; }

        public string GuardianContactInfoSave { get; set; }

        public string GuardianContactInfoShow { get; set; }

        public IList<SelectListItem> AvailableGuardianType { get; set; }

        public IList<SelectListItem> AvailableQualification { get; set; }

        public IList<SelectListItem> AvailableOccupation { get; set; }

        public GuardianAddressModel GuardianAddressModel { get; set; }

        public IList<GuardianContactInfoModel> GuardianContactInfoModel { get; set; }

        public string EncGuardianId { get; set; }

        public IList<SelectListItem> AvailableIncomeSlab { get; set; }

        public string GuardianImagepath { get; set; }

        public string GuardianDistrict { get; set; }

    }
}