﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.StudentPromotion
{
    public class PromotionModel
    {
        public PromotionModel()
        {
            AvailableClasses = new List<SelectListItem>();
            AvailableStudents = new List<SelectListItem>();
            AvailableNextClasses = new List<SelectListItem>();
            PromotionStudentList = new List<PromotionStudentList>();
            AvailableSession = new List<SelectListItem>();
            AvailableNextSession = new List<SelectListItem>();
        }

        [Required(ErrorMessage="Class required")]
        public int? ClassId { get; set; }

        public int CurSessionId { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }

        public int? StudentId { get; set; }

        public IList<SelectListItem> AvailableStudents { get; set; }

        public string RollNo { get; set; }

        public string RollNoStartFrom { get; set; }

        public string PromotionArray { get; set; }

        public int? NextClassId { get; set; }

        public IList<SelectListItem> AvailableNextClasses { get; set; }

        public IList<PromotionStudentList> PromotionStudentList { get; set; }

        public bool IsAuthToAdd { get; set; }

        public IList<SelectListItem> AvailableSession { get; set; }

        public int? SessionId { get; set; }

        public IList<SelectListItem> AvailableNextSession { get; set; }

        public int? NextSessionId { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public bool IspayAdmnFee { get; set; }

        public bool IsRollBack { get; set; }
    }

    public class PromotionStudentList
    {
        public int StudentId { get; set; }

        public string Admnno { get; set; }

        public string StudentName { get; set; }

        public string FatherName { get; set; }

        public string RollNo { get; set; }

        public bool Feepaid { get; set; }

        public string Gender { get; set; }

        public int? SessionId { get; set; }

        public bool? AllowPayFee { get; set; }

        public string PromotedOn { get; set; }
    }

}