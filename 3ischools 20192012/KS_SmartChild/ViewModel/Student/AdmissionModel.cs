﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Student
{
    public partial class AdmissionModel
    {
        public AdmissionModel()
        {
            AvailableStandard = new List<SelectListItem>();
            AvailableTypesForWaitingList = new List<SelectListItem>();
            AvailableTypesForAdmissionList = new List<SelectListItem>();
            AvailableTypesForRejectedList = new List<SelectListItem>();
            AvailableStudents = new List<SelectListItem>();
            AdmitRegisteredStudents = new List<AdmitRegisteredStudent>();
            AvailableSessions = new List<SelectListItem>();

        }
        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public bool ShowPayAdmissionFee { get; set; }

        public string Regno { get; set; }

        public int? StandardId { get; set; }

        public IList<SelectListItem> AvailableStandard { get; set; }

        public int StudentId { get; set; }

        public IList<SelectListItem> AvailableStudents { get; set; }

        public int IsAdmn { get; set; }

        public int? TypeId { get; set; }

        public string ViewType { get; set; }

        public int? AdmissionTypeId { get; set; }

        public IList<SelectListItem> AvailableTypesForAdmissionList { get; set; }

        public int? WaitingTypeId { get; set; }

        public IList<SelectListItem> AvailableTypesForWaitingList { get; set; }

        public int? RejectedTypeId { get; set; }

        public IList<SelectListItem> AvailableTypesForRejectedList { get; set; }

        public IList<AdmitRegisteredStudent> AdmitRegisteredStudents { get; set; }

        public string Admitarray { get; set; }

        public int AdmnSeriesEditable { get; set; }

        public bool IsAuthToAdd { get; set; }

        public string ViewName { get; set; }

        public string CurrencyIcon { get; set; }

        public string refurl { get; set; }

        public bool FeeAdmissionCheck { get; set; }

        public IList<SelectListItem> AvailableSessions { get; set; }

        public int SessionId { get; set; }

        public bool IsSetAdmissionNoAfterFeePaid { get; set; }

        public bool IsPayAdmnFee { get; set; }

        public bool IsViewrecord { get; set; }
    }

    public class AdmitRegisteredStudent
    {
        public int StudentId { get; set; }

        public string Regno { get; set; }

        public string Admnno { get; set; }

        public string StudentName { get; set; }

        public string FatherName { get; set; }

        public string Standard { get; set; }

        public string Status { get; set; }

        public bool IsCheck { get; set; }

        public bool Feepaid { get; set; }

        public string RejectionReason { get; set; }

        public string Sessionid { get; set; }

        public bool AdmnSeriesEditable { get; set; }

        public bool IsAlreadyExists { get; set; }

        public decimal? PendingRegistrationfee { get; set; }

    }
}