﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Student
{
    public class SiblingInfoModel
    {
        public SiblingInfoModel()
        {
            AvailableRelation = new List<SelectListItem>();
            AvailableSibling = new List<SelectListItem>();
        }

        public int? SiblinginfoId { get; set; }

        public int? SiblingId { get; set; }

        public string EncSiblingId { get; set; }

        public int? SiblingRelationId { get; set; }

        public IList<SelectListItem> AvailableRelation { get; set; }

        public IList<SelectListItem> AvailableSibling { get; set; }

        public int? SiblingClassId { get; set; }

        public string SiblingClass { get; set; }

        public string SiblingName { get; set; }

        public string SiblingRelation { get; set; }

        public string EncSiblinginfoId { get; set; }

        public string SiblingStatus { get; set; }

        public string SiblingSessionId { get; set; }

        public string AppliedConcession { get; set; }

    }

    public class SiblingVerificationModel
    {
        public string StudentId { get; set; }

        public string SibStudentId { get; set; }

        public string SibStudentName { get; set; }

        public string SibStuFatherName { get; set; }

        public string SibStuMotherName { get; set; }

        public string SibStuAddress { get; set; }

        public string SibStuRelation { get; set; }

        public string StudentName { get; set; }


        public string SibParentInfo { get; set; }

    }

    public class StudentParentInfoModel
    {
        public string StudentId { get; set; }

        public string StudentName { get; set; }

        public string StuFatherFName { get; set; }

        public string StuMotherFName { get; set; }

        public string StuFatherLName { get; set; }

        public string StuMotherLName { get; set; }

        public string StuAddress { get; set; }

        public string ParentInfo { get; set; }
    }

}