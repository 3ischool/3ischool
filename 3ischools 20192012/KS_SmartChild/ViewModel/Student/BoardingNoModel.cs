﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Student
{
    public class BoardingNoModel
    {
        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string BoardingNoId { get; set; }

        public string BoardingActivityId { get; set; }

        public string StudentName { get; set; }

        public string BoardingActivityStatus { get; set; }

        public bool IsAuthToGirdAdd { get; set; }

        public bool IsAuthToGridDelete { get; set; }

        public bool IsAuthToDelete { get; set; }

        public bool IsAuthToGridEdit { get; set; }
    }

    public class HostlerBoardingNo : BoardingNoModel
    {
        public HostlerBoardingNo()
        {
            AviableActivties = new List<SelectListItem>();
            AvailableSessions=new List<SelectListItem>();
            AvailableClass = new List<SelectListItem>();
            AvailableStudent=new List<SelectListItem>();
            AvailableBoardingActivity=new List<SelectListItem>();
            AvailableBoardingNos = new List<SelectListItem>();
        }

        public string BoardingNo { get; set; }

        public int intBoardingNo { get; set; }

        public int? intBoardingActivityId { get; set; }

        public int? SessionId { get; set; }

        public int? StudentId { get; set; }

        public string EncStudentId { get; set; }

        public int? ClassId { get; set; }

        public string NoRangeFrom { get; set; }

        public string NoRangeTo { get; set; }

        public bool IsActive { get; set; }

        public string IsActiveStatus { get; set; }

        public string AdmnNo { get; set; }

        public string Class { get; set; }

        public bool IsAssignedNo { get; set; }

        public bool? IsBoardingNoAssigned { get; set; }

        public bool? IsHistory { get; set; }

        public IList<SelectListItem> AviableActivties { get; set; }

        public IList<SelectListItem> AviableFilterActivties { get; set; }

        public IList<SelectListItem> AvailableSessions { get; set; }

        public IList<SelectListItem> AvailableClass { get; set; }

        public IList<SelectListItem> AvailableStudent{ get; set; }

        public IList<SelectListItem> AvailableBoardingActivity{ get; set; }

        public IList<SelectListItem> AvailableBoardingNos { get; set; }
    }

    public class StudentBoardingNoViewModel : HostlerBoardingNo
    {
        public string StudentBoardingNoId { get; set; }

        public string StudentId { get; set; }

       

        public string ActivityDateTime { get; set; }
    }
}