﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Session
{
    public class SessionModel
    {
        public int? SessionId { get; set; }

        public string Session { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string SessionStartDate { get; set; }

        public string SessionEndDate { get; set; }
    }

    public class SessionAdmissionOpeningModel
    {
        public SessionAdmissionOpeningModel()
        {
            SessionAdmissionStandardDetList = new List<SessionAdmissionStandardDetail>();
            AvailableStandards = new List<SelectListItem>();
        }

        public int? SessionId { get; set; }

        public int? SessionAdmnDetailId { get; set; }

        public bool IsAuthToAdd { get; set; }

        public string NewSessionWarning { get; set; }

        public bool IsCreateNewSession { get; set; }

        public DateTime? AdmnOpeningDate { get; set; }

        public DateTime? AdmnClosingDate { get; set; }

        public string AdmnOpenDate { get; set; }

        public string AdmnCloseDate { get; set; }

        public int? StandardId { get; set; }

        public IList<SelectListItem> AvailableStandards { get; set; }

        public int? Seats { get; set; }

        public IList<SessionAdmissionStandardDetail> SessionAdmissionStandardDetList { get; set; }

    }

    public class SessionAdmissionStandardDetail
    {
        public int? SessionAdmnDetId { get; set; }

        public int? StandardId { get; set; }

        public string Standard { get; set; }

        public string OpeningDate { get; set; }

        public string ClosingDate { get; set; }

        public string Seats { get; set; }

        public bool IsEditable { get; set; }

        public bool IsDeleteable { get; set; }

    }


}