﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.ViewModel.Calender
{
    public class WeekForMonth
    {
        public WeekForMonth()
        {
            Week1 = new List<CalenderDay>();
            Week2 = new List<CalenderDay>();
            Week3 = new List<CalenderDay>();
            Week4 = new List<CalenderDay>();
            Week5 = new List<CalenderDay>();
            Week6 = new List<CalenderDay>();
        }
        public List<CalenderDay> Week1 { get; set; }
        public List<CalenderDay> Week2 { get; set; }
        public List<CalenderDay> Week3 { get; set; }
        public List<CalenderDay> Week4 { get; set; }
        public List<CalenderDay> Week5 { get; set; }
        public List<CalenderDay> Week6 { get; set; }
        public string nextMonth { get; set; }
        public string prevMonth { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public bool? IsExamShow { get; set; }
      
    }
    public class CalenderDay
    {
        public CalenderDay()
        {
            Events = new List<EventsCalenderDay>();
        }
        public DateTime Date { get; set; }
        public string dateStr { get; set; }
        public int dtDay { get; set; }
        public int? daycolumn { get; set; }
        public bool IsTodayExam { get; set; }
        public string ExamCount { get; set; }
        public bool IsTodayanc { get; set; }
        public string AncCount { get; set; }
        public bool IsTodayact { get; set; }
        public bool IsTodayResult { get; set; }
        public string ResultCount { get; set; }
        public string ActCount { get; set; }
        public List<EventsCalenderDay> Events { get; set; }
    }
    public class EventsCalenderDay
    {
        public string EventName { get; set; }
        public string startday { get; set; }
        public string endday { get; set; }
    }
    public class EventGrid
    {
        public int? EventId { get; set; }
        public string EventTitle { get; set; }
    }
    public class EventPopUp
    {
        public string EventTitle { get; set; }
        public string EventDesc { get; set; }
        public string EventVenue { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string FromTime { get; set; }
        public string TillTime { get; set; }
        public string Class { get; set; }
        public string EventType { get; set; }
    }
}