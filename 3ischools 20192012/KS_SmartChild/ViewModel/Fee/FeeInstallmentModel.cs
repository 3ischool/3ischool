﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Fee
{
    public class FeeInstallmentModel
    {
        public FeeInstallmentModel()
        {
            AvailableSession = new List<SelectListItem>();
            AvailableStandard = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
        }

        public bool IsAuthToExport { get; set; }
        public IList<SelectListItem> AvailableSession { get; set; }
        public int? SessionId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string AdmissionNo { get; set; }
        public int? StandardId { get; set; }
        public IList<SelectListItem> AvailableStandard { get; set; }
        public bool IsAllClasses { get; set; }
        public IList<SelectListItem> AvailableClasses { get; set; }
        public int? StdClass { get; set; }
        public string defaultGridPageSize { get; set; }
        public string gridPageSizes { get; set; }
        public string CurrencyIcon { get; set; }
    }

    public class FeeInstallmentList
    {
        public string EncInstallmentId { get; set; }
        public int InstallmentId { get; set; }
        public int RceiptId { get; set; }
        public int PaidReceiptid { get; set; }
        public string Admnno { get; set; }
        public string StudentName { get; set; }
        public string Paybale { get; set; }
        public string Paid { get; set; }
        public bool IsFeePaid { get; set; }
        public DateTime InstallmentDate { get; set; }
        public int SessionId { get; set; }
        public string InstallmentDatestring { get; set; }
    }

}