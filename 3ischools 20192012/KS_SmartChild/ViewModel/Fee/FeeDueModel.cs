﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Fee
{
    public class FeeDueModel
    {
        public  FeeDueModel() {

            FeedueStudentList = new DataTable();
            AvailableClasses = new List<SelectListItem>();
            AvailableSendMessageMobileNoOptions = new List<SelectListItem>();
        }
        public DataTable FeedueStudentList { get; set; }
        public string StudentIdList { get; set; }
        public IList<SelectListItem> AvailableClasses { get; set; }
        public int ClassId { get; set; }
        public IList<SelectListItem> AvailableSendMessageMobileNoOptions { get; set; }
        public string SMSSelectionStudentMobileNo { get; set; }
        public string SendMessageMobileNoSelectionId { get; set; }
    }
    public class StudentlistModel
    {
        public int StudentId { get; set; }

        public string StudentPendingFee { get; set; }
    }
}