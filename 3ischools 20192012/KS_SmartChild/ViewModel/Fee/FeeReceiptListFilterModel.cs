﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Fee
{
    public class FeeReceiptListFilterModel
    {
        public FeeReceiptListFilterModel()
        {
            AvailableClasses = new List<SelectListItem>();
            AvailableStudents = new List<SelectListItem>();
            AvailableFeePeriods = new List<SelectListItem>();
            AvailableReceiptTypes = new List<SelectListItem>();
            AvailableSession = new List<SelectListItem>();
            AvailableStandard = new List<SelectListItem>();
            AvailableFeeReceiptCancelReason = new List<SelectListItem>();
        }
        public bool IsExcel { get; set; }
        public int? ReceiptTypeId { get; set; }

        public IList<SelectListItem> AvailableReceiptTypes { get; set; }

        public int? SessionId { get; set; }

        public IList<SelectListItem> AvailableSession { get; set; }

        public int? StandardId { get; set; }

        public IList<SelectListItem> AvailableStandard { get; set; }

        public bool IsAllClasses { get; set; }

        public int? ReceiptNo { get; set; }

        public bool ShowCancelReceipts { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public int? StdClass { get; set; }

        public string AdmissionNo { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }

        public int? Student { get; set; }

        public IList<SelectListItem> AvailableStudents { get; set; }

        public int? FeePeriod { get; set; }

        public IList<SelectListItem> AvailableFeePeriods { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToExport { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string CurrencyIcon { get; set; }

        public string RefUrl { get; set; }

        public bool IsAuthToView { get; set; }

        public bool IsAuthToPrint { get; set; }

        public bool IsAuthToDelete { get; set; }

        public string CancelText { get; set; }

        public bool AllowFeerecieptIssuence { get; set; }

        public IList<SelectListItem> AvailableFeeReceiptCancelReason { get; set; }

        public int? FeeReceiptCancelId { get; set; }
        public decimal ChequeBounceCharges { get; set; }
        public bool IsAllowEditingOfChequeBounceCharges { get; set; }

    }


    public class FeeBulkImportedStudentData
    {

        public FeeBulkImportedStudentData()
        {
            AvailableReceiptTypes = new List<SelectListItem>();
            AvailableSession = new List<SelectListItem>();
            AvailablePaymentModes = new List<SelectListItem>();
            AvaliableStudents = new List<SelectListItem>();
            AvaliableClasses = new List<SelectListItem>();
            AvaliableFeePeriods = new List<SelectListItem>();
        }

        public List<SelectListItem> AvaliableStudents { get; set; }

        public IList<SelectListItem> AvaliableFeePeriods { get; set; }

        public List<SelectListItem> AvaliableClasses { get; set; }

        public string EncFeeBulkDataId { get; set; }

        public string UploadLotNo { get; set; }
        
        public string EncSessionId { get; set; }

        public string EncPaymentModeTypeId { get; set; }

        public string TrsNo { get; set; }

        public string SrNo { get; set; }

        public string TrsDate { get; set; }

        public string ReceiptNo { get; set; }

        public string EncStudentId { get; set; }

        public string EncReceiptTypeId { get; set; }

        public string PaymentMode { get; set; }

        public string PaymentModeType { get; set; }

        public string FeePeriod { get; set; }

        public string ReceiptType { get; set; }

        public string Session { get; set; }

        public string EncFeePeriodId { get; set; }

        public string EncPaymentModeId { get; set; }

        public string FeeAmount { get; set; }

        public bool IsWavedOff { get; set; }

        public string ActualFeeAmount { get; set; }

        public string LateFeeAmount { get; set; }

        public string TptAmount { get; set; }

        public string TotalExcelAmount { get; set; }

        public string EncUserId { get; set; }

        public string InstrumentNo { get; set; }

        public string InstrumentDate { get; set; }

        public string Status { get; set; }

        public string EncReceiptId { get; set; }

        public string BankName { get; set; }

        public string StudentName { get; set; }

        public string Remarks { get; set; }

        public string AdmNo { get; set; }

        public string Class { get; set; }

        public string EncClassId { get; set; }

        public int? ReceiptTypeId { get; set; }

        public int PaymentModeId { get; set; }

        public IList<SelectListItem> AvailablePaymentModes { get; set; }

        public IList<SelectListItem> AvailableReceiptTypes { get; set; }

        public int? SessionId { get; set; }

        public string RefUrl { get; set; }

        public IList<SelectListItem> AvailableSession { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToExport { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string CurrencyIcon { get; set; }

    }
}