﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Fee
{
    public class DonationModel
    {

        public DonationModel()
        {
            PaymentModeList = new List<SelectListItem>();
        }
        public string  DonationFeeReceiptId { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string DonorName { get; set; }

        public int ReceiptNo { get; set; }

        public DateTime? DonationFromDate { get; set; }

        public DateTime? DonationToDate { get; set; }

        public DateTime DonationDate { get; set; }

        public string DonorDescription { get; set; }

        public decimal Amount { get; set; }

        public string  Remarks { get; set; }

        public IList<SelectListItem> PaymentModeList { get; set; }

        public int PaymentModeId { get; set; }

        public string PaymentDetail { get; set; }

        public string PaymentMode { get; set; }

        public bool Isheaderrequired { get; set; }

        public string HeaderHeight { get; set; }
        public string SchoolHeader { get; set; }

        public string SessionName { get; set; }
        public string SchoolLogo { get; set; }

        public string SchoolContactNo { get; set; }
        public string Header1 { get; set; }

        public string Header2 { get; set; }

        public string UserName { get; set; }
        public string CurrencyIcon { get; set; }

        public string Date { get; set; }
    }
}