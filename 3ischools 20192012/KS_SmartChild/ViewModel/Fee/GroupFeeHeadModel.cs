﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAL.BAL.SPClasses;
using KSModel.Models;
using System.Data;

namespace KS_SmartChild.ViewModel.Fee
{
    public class GroupFeeHeadModel
    {
        public GroupFeeHeadModel()
        {
            AvailableGroupFeeType = new List<SelectListItem>();
            AvailableFeeType = new List<SelectListItem>();
            AvailableFeeClassGroup = new List<SelectListItem>();
            AvailableFeeHead = new List<SelectListItem>();
            AvailableReceiptTypes = new List<SelectListItem>();
            AvailableConcessionAmountTypeList = new List<SelectListItem>();
            AvailableConcessionList = new List<SelectListItem>();
        }

        public int? GroupFeeTypeId { get; set; }

        public IList<SelectListItem> AvailableGroupFeeType { get; set; }

        public int? FeeTypeId { get; set; }

        public IList<SelectListItem> AvailableFeeType { get; set; }

        public int? FeeClassGroupId { get; set; }

        public IList<SelectListItem> AvailableFeeClassGroup { get; set; }

        public int? ConcessionAmountType { get; set; }

        public IList<SelectListItem> AvailableConcessionAmountTypeList { get; set; }

        public IList<SelectListItem> AvailableConcessionList { get; set; }

        public bool? TakeAddOns { get; set; }

        public int? FeeHeadId { get; set; }

        public bool IsShowAll { get; set; }

        public int? GroupFeeHeadId { get; set; }

        public int? GroupFeeHeadDetailId { get; set; }

        public DateTime? Date { get; set; }

        public decimal? Amt { get; set; }

        public decimal? NewAmt { get; set; }

        public IList<SelectListItem> AvailableFeeHead { get; set; }

        public string EndDate { get; set; }

        public bool? IsDeleted { get; set; }

        public bool? IsDefault { get; set; }

        public bool IsAuthToAddGroupFeeType { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToDelete { get; set; }

        public bool IsAuthToClose { get; set; }

        public string EncGroupFeeHeadId { get; set; }

        public string EncGroupFeeHeadDetailId { get; set; }

        public bool? IsPending { get; set; }

        public Boolean IsLate { get; set; }

        public string CurrencyIcon { get; set; }

        public string Sessionstartdate { get; set; }

        public IList<SelectListItem> AvailableReceiptTypes { get; set; }

        public int RecieptTypeId { get; set; }

        public Boolean IsDiscount { get; set; }
        public Boolean IsSingleTime { get; set; }

        public bool ShowForAll { get; set; }
        public bool ShowForOne { get; set; }
        public bool ShowForNone { get; set; }
        public decimal EWSDiscount { get; set; }
        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }
    }

    public class GroupfeeTypeModel
    {
        public GroupfeeTypeModel()
        {
            AvailableFeeType = new List<SelectListItem>();
            AvailableFeeClassGroup = new List<SelectListItem>();
            GroupFeeTypeList = new List<GroupfeeTypeModel>();
        }
        public int FeeTypeId { get; set; }

        public int FeeClassGroupId { get; set; }

        public bool TakeAddOns { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToDelete { get; set; }

        public IList<SelectListItem> AvailableFeeType { get; set; }

        public IList<SelectListItem> AvailableFeeClassGroup { get; set; }

        public string FeeClassGroup { get; set; }

        public string FeeType { get; set; }

        public IList<GroupfeeTypeModel> GroupFeeTypeList { get; set; }

        public int? GroupFeeTypeId { get; set; }

        public string EncGroupFeeTypeId { get; set; }

        public bool AllowEditDelete { get; set; }

    }

    public class GroupFeeHeadDetailModel
    {
        public int? GrpFeeTypeId { get; set; }

        public int? FeeHeadId { get; set; }

        public int? GroupFeeHeadId { get; set; }

        public int? GroupFeeHeadDetailId { get; set; }

        public DateTime? Date { get; set; }

        public decimal? Amt { get; set; }

        public decimal? NewAmt { get; set; }

        public string FeeHead { get; set; }

        public DateTime? dtEffectiveDate { get; set; }

        public string EffectiveDate { get; set; }

        public DateTime? ClosingDate { get; set; }

        public string EndDate { get; set; }

        public bool? IsDeleted { get; set; }

        public bool? IsDefault { get; set; }

        public string EncGroupFeeHeadId { get; set; }

        public string EncGroupFeeHeadDetailId { get; set; }

        public string EncFeeHeadId { get; set; }

        public bool IsPending { get; set; }

        public bool IsLateFee { get; set; }

        public bool IsDiscount { get; set; }

        public int RecieptTypeId { get; set; }

        public string RecieptType { get; set; }

        public bool ApplyNewStdFee { get; set; }

        public decimal EWSDiscount { get; set; }

        public bool? IsSingleTime { get; set; }

        public bool? IsConcession { get; set; }

    }

    public class SessionWiseGroupFeeHeadDetailModel
    {
        public SessionWiseGroupFeeHeadDetailModel()
        {
            GroupFeeHeadDetailList = new List<GroupFeeHeadDetailModel>();
        }
        public int SessionId { get; set; }
        public string Session1 { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsDefualt { get; set; }
        public string SessionAbbr { get; set; }
        public string strStartDate { get; set; }
        public string strEndDate { get; set; }
        public IList<GroupFeeHeadDetailModel> GroupFeeHeadDetailList { get; set; }
    }

    public class RecieptTypeFeeHeadModel
    {
        public RecieptTypeFeeHeadModel()
        {
            AvailableRecieptType = new List<SelectListItem>();
            AvailableFeeHead = new List<SelectListItem>();
        }

        public int ReceiptTypeFeeHeadId { get; set; }

        public int ReceiptTypeId { get; set; }

        public int FeeHeadId { get; set; }

        public bool IsActive { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string FeeHead { get; set; }

        public string ReceiptType { get; set; }

        public IList<SelectListItem> AvailableRecieptType { get; set; }

        public IList<SelectListItem> AvailableFeeHead { get; set; }

        public string ErrorList { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToDelete { get; set; }
    }


    public class FeeCollectionList
    {
        public FeeCollectionList()
        {
            AvailableSession = new List<SelectListItem>();
            AvailableFeeType = new List<SelectListItem>();
            AvailableClass = new List<SelectListItem>();
            AvailableReceiptType = new List<SelectListItem>();
            AvailableStandard = new List<SelectListItem>();
            AvailableUsers = new List<SelectListItem>();
            FeeCollectionGridList = new List<FeeCollectionListmodel>();
            FeeReceiptDetails = new List<FeeCollectionFeeReceiptDetailsModel>();
            dt = new DataTable();
            AvailablePaymentMode = new List<SelectListItem>();
        }

        public IList<SelectListItem> AvailableSession { get; set; }
        public IList<SelectListItem> AvailableStandard { get; set; }

        public IList<SelectListItem> AvailableFeeType { get; set; }
        public IList<SelectListItem> AvailablePaymentMode { get; set; }
        public IList<SelectListItem> AvailableClass { get; set; }
        public IList<SelectListItem> AvailableReceiptType { get; set; }
        public IList<SelectListItem> AvailableUsers { get; set; }
        public IList<FeeCollectionListmodel> FeeCollectionGridList { get; set; }

        public int PaymentModeId { get; set; }
        public int?[] PaymentModeIds { get; set; }

        public int SessionId { get; set; }
        public int ReceiptTypeId { get; set; }
        public int UserId { get; set; }
        public int FeeTypeId { get; set; }
        public int ClassId { get; set; }
        public string FromDate { get; set; }
        public int StandardId { get; set; }

        public int SelectedSessionId { get; set; }
        public bool IsAllClasses { get; set; }
        public string ToDate { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public bool Isheaderrequired { get; set; }

        public string HeaderHeight { get; set; }

        public string Header1 { get; set; }

        public string Header2 { get; set; }

        public string SchoolHeader { get; set; }

        public string SchoolLogo { get; set; }

        public string SessionName { get; set; }

        public string PrintDate { get; set; }

        public string PrintTime { get; set; }

        public string DataDate { get; set; }

        public bool IsHostler { get; set; }

        public bool IsAuthToPrint { get; set; }
        public string Fee_ShowDateofJoining_FeeCollectionList { get; set; }
        public IList<FeeCollectionFeeReceiptDetailsModel> FeeReceiptDetails { get; set; }

        public DataTable dt { get; set; }

        public string IsExcel { get; set; }

        [AllowHtml]
        public string HtmlForExcelFeeConcession { get; set; }
    }

    public class FeeCollectionFeeReceiptDetailsModel
    {
        public int ReceiptDetailId { get; set; }
        public Nullable<int> ReceiptId { get; set; }
        public Nullable<int> GroupFeeHeadId { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<int> PrintCount { get; set; }
        public Nullable<int> ConsessionId { get; set; }
        public Nullable<int> AddOnHeadId { get; set; }
        public Nullable<int> PendingFeePeriodId { get; set; }
        public Nullable<int> LateFeePeriodId { get; set; }
        public Nullable<decimal> WaivedOffAmount { get; set; }
        public string FeeHead { get; set; }
        public bool IsSubtractable { get; set; }
        public virtual GroupFeeHead GroupFeeHead { get; set; }

        public int FeeHeadId { get; set; }
    }

    public class GroupFeeHeadConcession
    {
        public int GroupFeeHeadConcId { get; set; }
        public int GroupFeeHeadDetailId { get; set; }
        public int ConcessionId { get; set; }
        public string Concession { get; set; }
        public bool IsPercent { get; set; }
        public decimal Amount { get; set; }
        public string EffectiveDate { get; set; }
        public string EndDate { get; set; }
        public bool IsAuthToDelete { get; set; }
        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToClose { get; set; }
        public DateTime? CloseDate { get; set; }
    }

    public class ConsessionTypeModel
    {
        public ConsessionTypeModel()
        {
            ConsessionTypeTagList = new List<SelectListItem>();
            ConsessionTypeList = new List<ConsessionType>();
            ConsessionTypeTagDetails = new List<string>();
        }
        public IList<SelectListItem> ConsessionTypeTagList { get; set; }
        public string ConsessionType { get; set; }
        public int ConsessionTypeTagId { get; set; }
        public int ConsessionTypeId { get; set; }
        public IList<string> ConsessionTypeTagDetails { get; set; }
        public IList<ConsessionType> ConsessionTypeList { get; set; }
        public bool IsAuthToAdd { get; set; }
        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToDelete { get; set; }
    }

    public class ConsessionModel
    {
        public ConsessionModel()
        {
            AvailableConsessionType = new List<SelectListItem>();
            AvailableReceiptType = new List<SelectListItem>();
            ConsessionList = new List<Consession>();
        }
        public string Consession { get; set; }
        public decimal Amount { get; set; }
        public IList<SelectListItem> AvailableConsessionType { get; set; }
        public int ConsessionTypeId { get; set; }
        public int ConsessionId { get; set; }
        public IList<SelectListItem> AvailableReceiptType { get; set; }
        public int ReceiptTypeId { get; set; }
        public bool IsActive { get; set; }
        public IList<Consession> ConsessionList { get; set; }
        public bool IsAuthToAdd { get; set; }
        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToDelete { get; set; }
        public bool IsAuthToViewStudentDetails { get; set; }
        public bool IsAuthToExport { get; set; }
    }
}