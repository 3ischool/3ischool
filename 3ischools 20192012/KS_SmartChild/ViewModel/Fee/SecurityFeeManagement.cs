﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace KS_SmartChild.ViewModel.Fee
{
    public class SecurityFeeManagement
    {
        public  SecurityFeeManagement()
        {
            AvailableSession = new List<SelectListItem>();
            AvailableStandard = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
        }
        public IList<SelectListItem> AvailableSession { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string AdmissionNo { get; set; }
        public int SessionId { get; set; }
        public string gridPageSizes { get; set; }
        public string defaultGridPageSize { get; set; }
        public string CurrencyIcon { get; set; }
        public bool IsAllClasses { get; set; }
        public int? StdClass { get; set; }
        public int? StandardId { get; set; }
        public IList<SelectListItem> AvailableStandard { get; set; }
        public IList<SelectListItem> AvailableClasses { get; set; }
       
    }

    public class SecurityManagementList
    {
        public int? ReceiptDetailid { get; set; }
        public int? SecurityAccountid { get; set; }
        public string Admnno { get; set; }
        public string Class { get; set; }
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string RollNo { get; set; }
        public string  Status { get; set; }
        public string StudentType { get; set; }
        public decimal Due { get; set; }
        public decimal Received { get; set; }
        public decimal Pending { get; set; }
        public decimal Refund { get; set; }
        public string RefundDate { get; set; }
        public decimal FoefietAmount { get; set; }
        public string  FoefietDate { get; set; }
        public string Remarks { get; set; }
        public string FoefietReason { get; set; }
        public bool IsRefund { get;set;}
        public decimal BalanceAmount { get;set;}
    }
}