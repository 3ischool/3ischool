﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Fee
{
    public class FeeClassGroupModel
    {
        public FeeClassGroupModel()
        {
            AvailableFromStandard = new List<SelectListItem>();

            AvailableToStandard = new List<SelectListItem>();

            FeeClassGroupList = new List<FeeClassGroupList>();
        }
        public string EncFeeClassGroupId { get; set; }

        public int? FeeClassGroupId { get; set; }

        public string FeeClassGroup { get; set; }

        public int? FeeClassGroupDetailId { get; set; }

        public int? StandardId { get; set; }

        public IList<SelectListItem> AvailableFromStandard { get; set; }

        public IList<SelectListItem> AvailableToStandard { get; set; }
        public IList<SelectListItem> AvailableFeeClassGroupType { get; set; }
        public int? FeeClassGroupTypeId { get; set; }
        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToDelete { get; set; }

        public bool IsAuthToEdit { get; set; }

        public int FromStandardIndex { get; set; }

        public int? ToStandardIndex { get; set; }

        public IList<FeeClassGroupList> FeeClassGroupList { get; set; }
        public IList<int> StandardList { get; set; }
    }

    public class FeeClassGroupList
    {
        public string FeeClassGroup { get; set; }
        public string Standard { get; set; }
        public int? FromStandardIndex { get; set; }
        public int? ToStandardIndex { get; set; }
        public int? FeeClassGroupId { get; set; }
        public bool FeeClassGroupAlreadyInUse { get; set; }
        public string EncFeeClassGroupId { get; set; }
        public int? FeeClassGroupTypeId { get; set; }
        public string StandardList { get; set; }
    }
}