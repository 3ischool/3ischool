﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace KS_SmartChild.ViewModel.Fee
{
    public class PreviousPendingFeeModel
    {
        public PreviousPendingFeeModel()
        {
            ClassList = new List<SelectListItem>();
            SessionList = new List<SelectListItem>();
            DataList = new List<PendingDataModel>();
            ReceiptTypeList = new List<SelectListItem>();
            MonthList = new List<SelectListItem>();
        }
        public string ClassId { get; set; }

        public List<SelectListItem> ClassList { get; set; }

        public string SessionId { get; set; }

        public IList<SelectListItem> SessionList { get; set; }

        public List<PendingDataModel> DataList { get; set; }

        public IList<SelectListItem> ReceiptTypeList { get; set; }

        public IList<SelectListItem> MonthList { get; set; }

        public string MonthID { get; set; }

        public string ReceiptTypeId { get; set; }

        public string DateToSave { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool Ispreviouspendingfee { get; set; }

        public bool IsSchoolFee{ get; set; }

        public string PeriodDate { get; set; }
    }

    public class PendingDataModel
    {
        public string FeePrevPendingId { get; set; }

        public string AdmissionNo { get; set; }

        public string StudentId { get; set; }

        public string StudentName { get; set; }

        public string RollNo { get; set; }

        public string Guardian { get; set; }

        public string PendingAmount { get; set; }

        public string PendingTransportAmount { get; set; }

        public string ReceiptId { get; set; }
        public string AsOnDate { get; set; }

        public DateTime? PendingAsonDate { get; set; }
        public string ReceiptDetail { get; set; }
    }

    public class DeserializaitonModel
    {
        public string FeePrevPendingId { get; set; }

        public string StudentId { get; set; }

        public string PendingFeeAmount { get; set; }

        public string ReceiptTypeId { get; set; }

        public string AsOnDate { get; set; }


    }
}