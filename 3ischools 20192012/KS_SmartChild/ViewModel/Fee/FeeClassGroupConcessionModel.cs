﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Fee
{
    public class FeeClassGroupConcessionModel
    {
        public FeeClassGroupConcessionModel()
        {
            FeeClassGroupList = new List<SelectListItem>();
            ConcessionList = new List<SelectListItem>();
        }
        public string FeeClassGroupConcessionId { get; set; }

        public IList<SelectListItem> FeeClassGroupList { get; set; }

        public IList<SelectListItem> ConcessionList { get; set; }

        public string  FeeClassGroupConcessiondetailId { get; set; }

        public int? ConcessionId { get; set; }

        public int? FeeClassGroupId { get; set; }

        public string FeeClassGroup { get; set; }

        public string Concession { get; set; }

        public string EffectiveDate { get; set; }

        public decimal? Amount { get; set; }

        public Boolean ApplyToAll { get; set; }

        public string EndDate { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string ApplyAll { get; set; }

        public Boolean IsEndDate { get; set; }

        public Boolean IsAuthToDelete { get; set; }
        public Boolean IsAuthToEdit { get; set; }
    }
}