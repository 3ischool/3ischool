﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Staff
{
    public class StaffAttachedDocumentModel
    {
        public StaffAttachedDocumentModel()
        {
            AvailableDocumentType = new List<SelectListItem>();
        }

        public int? AttachDocumentId { get; set; }

        public int? AttachDocumentTypeId { get; set; }

        public string Image { get; set; }

        public string Description { get; set; }

        public bool? IsMultipleAllowed { get; set; }

        public string DocumentType { get; set; }

        public IList<SelectListItem> AvailableDocumentType { get; set; }
    }
}