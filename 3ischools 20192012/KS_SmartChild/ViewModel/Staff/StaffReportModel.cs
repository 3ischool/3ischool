﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Staff
{
    public class StaffReportModel
    {
        public StaffReportModel()
        {
            AvailableStandard = new List<SelectListItem>();
            AvailableSubjects = new List<SelectListItem>();
            StaffReportByClassSubjectList = new List<StaffReportByClassSubjectModel>();
        }

        public int ViewBy { get; set; }

        public int? StandardId { get; set; }

        public IList<SelectListItem> AvailableStandard { get; set; }

        public int? SubjectId { get; set; }

        public IList<SelectListItem> AvailableSubjects { get; set; }

        public IList<StaffReportByClassSubjectModel> StaffReportByClassSubjectList { get; set; }

        public string Notification { get; set; }

    }

    public class StaffReportByClassSubjectModel
    {
        public string TeacherName { get; set; }

        public bool TeacherStatus { get; set; }

        public string StandardName { get; set; }

        public string SubjectName { get; set; }

    }

}