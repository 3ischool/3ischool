﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Staff
{
    public class ManageClassIncharge
    {

        public int? ClassTeacherId { get; set; }

        public int? ClassId { get; set; }

        public int? ClassIdh { get; set; }

        public IList<SelectListItem> ClassList { get; set; }

        public int? TeacherId { get; set; }

        public IList<SelectListItem> TeacherList { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public DateTime? EndDate { get; set; }

        public IList<SelectListItem> ClassListForFilter { get; set; }

        public IList<SelectListItem> TeacherListForFilter { get; set; }

        public string CurrentSessionStartDate { get; set; }

        public string CurrentSessionEndDate { get; set; }

        public string TomorrowsDate { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToDelete { get; set; }
    }
    public class InchargeGridModel
    {
        public bool IsInchargeDeletable { get; set; }

        public int? ClassTeacherId { get; set; }

        public int? ClassId { get; set; }

        public int? StandardId { get; set; }

        public string Standard { get; set; }

        public int? TeacherId { get; set; }

        public string Class { get; set; }

        public string Teacher { get; set; }

        public DateTime EffectiveDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string EffDt { get; set; }

        public string EndDt { get; set; }

        public bool IsActive { get; set; }

        public string SectionId { get; set; }

        public string Section { get; set; }
    }
}