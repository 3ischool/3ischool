﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KS_SmartChild.ViewModel.Common;
using System.Web.Mvc;
namespace KS_SmartChild.ViewModel.Staff
{
    public class LeaveApprovalModel
    {
        public LeaveApprovalModel()
        {
            AviableLeaveStatus = new List<SelectListItem>();
            AviableLeaveTypes = new List<SelectListItem>();
            AviableStaffList = new List<SelectListItem>();
            AviableStaffDesignationList = new List<SelectListItem>();
            AviableDepartments = new List<SelectListItem>(); 
            AvailableSendMessageMobileNoOptions = new List<SelectListItem>();
            AviableClassList = new List<SelectListItem>(); 
        }
        public bool IsAdmin { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string LoggedInUser { get; set; }

        public string LeaveApplyFromTime { get; set; }

        public string LeaveApplyToTime { get; set; }

        public bool IsAuthApprove { get; set; }

        public bool IsAuthDisapprove { get; set; }

        public string FromAppiledDate { get; set; }

        public string ToAppiledDate { get; set; }

        public string LeaveTypeId { get; set; }

        public string LeaveStatus { get; set; }

        public string FilterDateBy { get; set; }

        public string StaffId { get; set; }

        public int? ClassId { get; set; }

        public bool SMS { get; set; }

        public bool Notification { get; set; }

        public bool NonAppUser { get; set; }

        public bool StuSMS { get; set; }

        public bool StuNotification { get; set; }

        public bool StuNonAppUser { get; set; }

        public string DesginationId { get; set; }

        public string DepartmentId { get; set; }

        public string SMSSelectionStudentMobileNo { get; set; }

        public IList<SelectListItem> AviableStaffDesignationList { get; set; }

        public IList<SelectListItem> AviableDepartments { get; set; }

        public IList<SelectListItem> AviableStaffList { get; set; }

        public IList<SelectListItem> AviableLeaveTypes { get; set; }

        public IList<SelectListItem> AviableLeaveStatus { get; set; }

        public IList<SelectListItem> AvailableSendMessageMobileNoOptions { get; set; }

        public IList<SelectListItem> AviableClassList { get; set; }
    }

    public class LeaveApprovalGridModel : LeaveModel
    {
        public string Name { get; set; }

        public string Class { get; set; }

        public string RollNo { get; set; }

        public string Designation { get; set; }

        public string Department { get; set; }

        public DateTime? AppliedDate { get; set; }

        public string Empcode { get; set; }

        public string AppliedTime { get; set; }
    }
}