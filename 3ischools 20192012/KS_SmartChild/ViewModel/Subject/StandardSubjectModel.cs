﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Subject
{
    public class StandardSubjectModel
    {
        public StandardSubjectModel()
        {
            AvailableStandard = new List<SelectListItem>();
            AvailableStandardForCopy = new List<SelectListItem>();
            AvailableCoreSubject = new List<SelectListItem>();
            AvailableSubject = new List<SelectListItem>();
        }

        public int? StandardId { get; set; }

        public IList<SelectListItem> AvailableStandard { get; set; }

        // copy from
        public int? CopyStandardId { get; set; }

        public IList<SelectListItem> AvailableStandardForCopy { get; set; }

        public int?[] CoreSubjectId { get; set; }

        public IList<SelectListItem> AvailableCoreSubject { get; set; }

        public int?[] SubjectIds { get; set; }

        public string SubIds { get; set; }

        public IList<SelectListItem> AvailableSubject { get; set; }

        public bool IsAuthToAdd { get; set; }
    }

    public class StudentClassSubjectModel
    {
        public StudentClassSubjectModel()
        {
            AvaiableClassStudents = new List<SelectListItem>();
            AvaiableStuClasSubs = new List<SelectListItem>();
            AvaiableStuSessions = new List<SelectListItem>();
            AvaiableClasses = new List<SelectListItem>();
        }
        
        public string arrayIds { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string StudentClassSubjectsId { get; set; }

        public string SessionId { get; set; }

        public string StudentClassId { get; set; }

        public string ClassId { get; set; }

        public string ClassSubjectId { get; set; }

        public string SubjectId { get; set; }

        public string StudentId { get; set; }

        public IList<SelectListItem> AvaiableClasses { get; set; }

        public IList<SelectListItem> AvaiableClassStudents { get; set; }

        public IList<SelectListItem> AvaiableStuClasSubs { get; set; }

        public IList<SelectListItem> AvaiableStuSessions { get; set; }
    }

    public class StudentClassesModel
    {
        public string SubjectCode { get; set; }

        public string ClassSubjectId { get; set; }

        public string StudentClassId { get; set; }

        public bool Selected { get; set; }
        

    }

}