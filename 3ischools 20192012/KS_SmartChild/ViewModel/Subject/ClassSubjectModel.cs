﻿using KS_SmartChild.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Subject
{
    public class ClassSubjectModel
    {
        public ClassSubjectModel()
        {
            AvailableClasses = new List<SelectListItem>();
            AvailableAllSubjects = new List<SelectListItem>();
            AvailableCoreSubjects = new List<SelectListItem>();
            AvailableOptionalSubject = new List<SelectListItem>();
            AvailableAddtionalSubject = new List<SelectListItem>();
            AvailableAllSkillSubjects = new List<SelectListItem>();
            AvailableSkillSubjects = new List<SelectListItem>();
            ClassTypeList = new List<SelectListItem>();
        }

        public int? ClassTypeId { get; set; }

        public bool IsDynamicNavigationPath { get; set; }

        public IList<SelectListItem> ClassTypeList { get; set; }

        public int? ClassId { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }

        public int?[] AllSubjectIds { get; set; }

        public IList<SelectListItem> AvailableAllSubjects { get; set; }

        public int?[] CoreSubjectIds { get; set; }

        public IList<SelectListItem> AvailableCoreSubjects { get; set; }

        public int? OptionalSubjecttypeId { get; set; }

        public int?[] OptionalSubjectIds { get; set; }

        public IList<SelectListItem> AvailableOptionalSubject { get; set; }

        public int? AddtionaSubjecttypeId { get; set; }

        public int?[] AddtionalSubjectIds { get; set; }

        public IList<SelectListItem> AvailableAddtionalSubject { get; set; }

        public int?[] AllSkillSubjectIds { get; set; }

        public IList<SelectListItem> AvailableAllSkillSubjects { get; set; }

        public int?[] SkillSubjectIds { get; set; }

        public IList<SelectListItem> AvailableSkillSubjects { get; set; }

        public bool IsAuthToAdd { get; set; }
    }

    public class PracticalSubArayModel
    {
        public int ClassSubjectId { get; set; }

        public int SubjectId { get; set; }

        public bool IsPractical { get; set; }
    }
}