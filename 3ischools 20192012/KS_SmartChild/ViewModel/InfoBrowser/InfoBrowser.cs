﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.InfoBrowser
{
    public class BrowserInfo
    {
        public string defaultGridPageSize { get; set; }
        public string gridPageSizes { get; set; }
        public bool IsAuthToDelete { get; set; }
        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToAdd { get; set; }
    }

    public class BrowserInfoDetail : BrowserInfo
    {
        public BrowserInfoDetail()
        {
            BrowserObjectList = new List<SelectListItem>();
        }
        public IList<SelectListItem> BrowserObjectList { get; set; }
        public string BroserObjectId { get; set; }
    }

    public class Browser_Object
    {
        public Browser_Object()
        {
            BrowserObjectCol = new List<BrowserObjectColumn>();
        }
        public int BrowserObjectId { get; set; }
        public string BrowserObject { get; set; }
        public int? MaxViewColCount { get; set; }
        public int? MaxColCount { get; set; }
        public int? MaxFilterColCount { get; set; }
        public string StoredProcedureName { get; set; }
        public List<BrowserObjectColumn> BrowserObjectCol { get; set; }
    }

    public class BrowserObjectColumn
    {
        public BrowserObjectColumn()
        {
            TableCol = new List<TableColumn>();
            BrowserObjectColRef = new List<BrowserObjectColumnRef>();
        }
        public int BrowserObjectColId { get; set; }
        public string BrowserObjectCol { get; set; }
        public bool? CanView { get; set; }
        public bool? CanEdit { get; set; }
        public bool? CanFilter { get; set; }
        public List<BrowserObjectColumnRef> BrowserObjectColRef { get; set; }
        public List<TableColumn> TableCol { get; set; }
    }

    public class TableColumn
    {
        public int TableColId { get; set; }
        public string ActualColName { get; set; }
    }

    public class BrowserObjectColumnRef
    {
        public int BrowserObjectColRefId { get; set; }
        public int? BrowserObjectColId { get; set; }
        public string RefTable { get; set; }
        public string RefTableCol { get; set; }
        public string ControlType { get; set; }
    }

    public class FilterList
    {
        public FilterList()
        {
            FilterData = new List<Filterdata>();
        }
        public string FilterName { get; set; }
        public string FilterType { get; set; }
        public List<Filterdata> FilterData { get; set; }
    }
    public class Filterdata
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }

    public class ColumnList
    {
        public int ColumnId { get; set; }
        public string ColumnName { get; set; }
    }
    


}