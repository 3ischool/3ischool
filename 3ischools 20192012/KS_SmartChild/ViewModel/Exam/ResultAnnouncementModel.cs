﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.ViewModel.Exam
{
    public class ResultAnnouncementModel
    {
        public ResultAnnouncementModel()
        {
            ResultAnnouncementClassesList = new List<ResultAnnouncementClasses>();
        }

        public string DSI { get; set; }

        public int ExDSI { get; set; }

        public string ResultPublishDate { get; set; }

        public string MinResultPublishDate { get; set; }

        public string ResultPublishArray { get; set; }

        public IList<ResultAnnouncementClasses> ResultAnnouncementClassesList { get; set; }

        public bool IsAuthToAnnounce { get; set; }

        public bool AllResultPublished { get; set; }

    }

    public class ResultAnnouncementClasses
    {
        public int ExamResultDateId { get; set; }

        public int StandardId { get; set; }

        public string Standard { get; set; }

        public bool? Selected { get; set; }

        public bool? IsFreez { get; set; }
    }

    public class ResultAnnouncementPendencyModel
    {
        public ResultAnnouncementPendencyModel()
        {
            PendencySubjectList = new List<PendencySubjectListModel>();
        }

        public int ExamDateSheetId { get; set; }

        public int StudentId { get; set; }

        public string Student { get; set; }

        public IList<PendencySubjectListModel> PendencySubjectList { get; set; }
    }

    public class PendencySubjectListModel
    {
        public int ExamDateSheetDetailId { get; set; }

        public string MaxMarks { get; set; } 

        public int SubjectId { get; set; }

        public int StandardId { get; set; }

        public int SkillId { get; set; }

        public string Subject { get; set; }
    }

}