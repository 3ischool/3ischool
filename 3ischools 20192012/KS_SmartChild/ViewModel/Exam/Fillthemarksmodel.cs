﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Exam
{
    public partial class Fillthemarksmodel
    {
        public Fillthemarksmodel()
        {
            AvailableClasses = new List<SelectListItem>();
            ExamResultModelList = new List<ExamResultModel>();
            ExamtermEndSkillListScholastic = new List<ExamTermEndSkillsModel>();
            ExamtermEndSkillListCoScholastic = new List<ExamTermEndSkillsModel>();
            newExamtermEndSkillListScholastic = new List<ExamTermEndSkillsModel>();
        }
        public int ExamDateSheetDetailId { get; set; }

        public int StandardId { get; set; }

        public int SortingIndex { get; set; }

        public int ClassId { get; set; }

        public string ClassName { get; set; }

        public int SubjectId { get; set; }

        public string SubjectName { get; set; }

        public string ExamDate { get; set; }

        public string ErrorMessage { get; set; }

        public int MarksPatternId { get; set; }

        public string MaxMarks { get; set; }

        public string IAMaxMarks { get; set; }

        public string EncExamDateSheetDetailId { get; set; }

        public string EncMarksPatternId { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }

        public IList<ExamResultModel> ExamResultModelList { get; set; }

        public bool? IsClassResultAnnounce { get; set; }

        public bool IsExamDatesheetDetail { get; set; }

        public bool IsAuthToSet { get; set; }

        public bool IsDeltePermission { get;set;}

        public bool IsAllResultPublished { get; set; }

        public IList<ExamTermEndSkillsModel> ExamtermEndSkillListScholastic { get; set; }

        public IList<ExamTermEndSkillsModel> ExamtermEndSkillListCoScholastic { get; set; }

        public IList<ExamTermEndSkillsModel> newExamtermEndSkillListScholastic { get; set; }

        public int ExamDateSheetId { get; set; }

        public int ClassSubjectId { get; set; }

        public bool IsCompleted { get; set; }

        public bool IsExamInternalAssesmentApplicable { get; set; }

        public int SubjectSkillId { get; set; }

        public bool CoScholasticActiveforSchool { get; set; }

        public bool ScholasticActiveforSchool { get; set; }

        public bool OtherDetailsActiveforSchool { get; set; }

        public string ActivityName { get;set;}

        public string DateSheetName { get; set; }

        public string ImportUrl { get; set; }


    }

    public partial class FTMmodel
    {
        public int ExamDateSheetDetailId { get; set; }

        public int ClassId { get; set; }

        public int MarksPatternId { get; set; }

        public string EncExamDateSheetDetailId { get; set; }

        public string EncMarksPatternId { get; set; }

        public int SubjectId { get; set; }

        public int SubjectSkillId { get; set; }
    }

    public partial class ExamResultModel
    {
        public int ExamResultId { get; set; }

        public int ExamDateSheetDetailId { get; set; }

        public int StudentId { get; set; }

        public string ObtMarks { get; set; }
        public string ObtIAMarks { get; set; }

        public string ObtainedMarks { get; set; }
        public decimal? ObtainedIAMarks { get; set; }


        public decimal? MaxMarks { get; set; }

        public string Remarks { get; set; }

        public bool IsAbsent { get; set; }

        public string RollNo { get; set; }

        public string StudentName { get; set; }

        public string EncExamResultId { get; set; }

        public string EncExamDateSheetDetailId { get; set; }

        public bool IsCompleted { get; set; }

        public decimal? IAMaxMarks { get; set; }

        public string Admnno { get; set; }

    }
    public class ExamTermEndSkill
    {


    }
}