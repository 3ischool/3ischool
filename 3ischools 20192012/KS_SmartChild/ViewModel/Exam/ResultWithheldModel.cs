﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Exam
{
    public class ResultWithheldModel
    {
        public ResultWithheldModel()
        {
            AvailableClassList = new List<SelectListItem>();
        }
        public IList<SelectListItem> AvailableClassList { get; set; }

        public int ClassId { get; set; }

        public string StudentName { get; set; }

        public string RollNo { get; set; }

        public string Reason { get; set; }

        public string WithheldDate { get; set; }

        public string ReleasedOnDate { get; set; }

        public int DSI { get; set; }

        public int ExamResultWithHeldId { get; set; }

        public int StudentId { get; set; }

        public string Withheldarray { get; set; }

        public string DatesheetId { get; set; }

        public string SessionStartDate { get; set; }

        public bool StudentInActive { get; set; }
    }
}