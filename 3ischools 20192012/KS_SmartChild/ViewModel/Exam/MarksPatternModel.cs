﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.ViewModel.Exam
{
    public class MarksPatternModel
    {
        public int? MarksPatternId { get; set; }

        public int? GradePatternId { get; set; }

        public decimal? MaxMarks { get; set; }

        public decimal? PassMarks { get; set; }

    }
}