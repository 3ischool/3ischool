﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Exam
{
    public class ViewResultModel
    {
        public ViewResultModel()
        {
            AvailableClasses = new List<SelectListItem>();
            AvailableStudents = new List<SelectListItem>();
            AvailableSubjects = new List<SelectListItem>();
            ViewResultByStudent = new ViewResultByStudentModel();
            ViewResultByClassList = new List<ViewResultByClassModel>();
            ViewResultDetailBySubjectList = new List<ViewReportcardmarks>();
            ExamTermEndSkillsListModel = new List<ExamTermEndSkillsListModel>();
            ViewResultDetailByCoscholastic = new List<ViewReportcardmarks>();
            ViewResultDetailByDiscipline = new List<ViewReportcardmarks>();
            AvailableSession = new List<SelectListItem>();
            AvailableExamTerms = new List<SelectListItem>();
            AvailableClassSubjects = new List<ClassSubjectList>();
        }
        public int? Examterm { get; set; }
        public int TermId { get; set; }
        public IList<SelectListItem> AvailableExamTerms { get; set; }
        public bool IsExporttoExcel { get; set; }
        public bool IsAdmin { get; set; }

        public string DSI { get; set; }

        public int ExDSI { get; set; }

        public int? ClassId { get; set; }

        public bool IsVisibleReportCardDetails { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }
        public IList<ClassSubjectList> AvailableClassSubjects { get; set; }

        public int? StudentId { get; set; }

        public string strStudentId { get; set; }
        public string StudentImage { get; set; }

        public IList<SelectListItem> AvailableStudents { get; set; }

        public int? SessionId { get; set; }

        public IList<SelectListItem> AvailableSession { get; set; }

        public int? SubjectId { get; set; }

        public string ExamActivity { get; set; }

        public IList<SelectListItem> AvailableSubjects { get; set; }

        public ViewResultByStudentModel ViewResultByStudent { get; set; }

        public IList<ViewResultByClassModel> ViewResultByClassList { get; set; }

        public bool MultipleTerm { get; set; }
        public IList<ViewReportcardmarks> ViewResultDetailBySubjectList { get; set; }
        public IList<ViewReportcardmarks> ViewResultDetailByCoscholastic { get; set; }
        public IList<ViewReportcardmarks> ViewResultDetailByDiscipline { get; set; }
        public IList<ExamTermEndSkillsListModel> ExamTermEndSkillsListModel { get; set; }
        public bool IsTermEnd { get; set; }

        public string  UserType { get; set; }

        public bool IsInternalAssessmentAplicable { get; set; }

        public bool IsAuthToExport { get; set; }

        public bool ExamWithheld { get; set; }

        public string WithheldMessage { get; set; }

        public bool IgnoreExamResultofSubjectIfAbsent { get; set; }

        public bool IsExportTopdfAllStudents { get; set; }

    }
    public class ClassSubjectList {

        public string SubjectName { get; set; }
        public int SubjectId { get; set; }
        public int SkillId { get; set; }
        public bool IsInternalAssesmentApplicable { get; set; }
        public int index { get; set; }
    }

    #region By Single Student Model

    public class ViewResultByStudentModel
    {
        public ViewResultByStudentModel()
        {
            ViewResultDetailBySubjectList = new List<ViewResultDetailBySubject>();
        }
        public bool IsResultwithheld { get; set; }
        public string StudentName { get; set; }

        public string RollNo { get; set; }

        public string TotalMaxMarks { get; set; }

        public string TotalPassMarks { get; set; }

        public string TotalObtMarks { get; set; }

        public string FinalResult { get; set; }

        public string Percentage { get; set; }

        public string ExamTermActivity { get; set; }

        public IList<ViewResultDetailBySubject> ViewResultDetailBySubjectList { get; set; }

        public string grade { get; set; }

    }

    public class ViewResultDetailBySubject
    {
        public string SubjectName { get; set; }

        public string MaxMarks { get; set; }

        public string IAMAxMarks { get; set; }

        public string PassMarks { get; set; }
        
        public string ObtainedMarks { get; set; }

        public string Result { get; set; }

        public string InternalAssessment { get; set; }

        public bool IssubjectInternalAssessment { get; set; }

        public string TotalObtainedMarks { get; set; }

        public string subjectpercentage { get; set; }

        public string grade { get; set; }

    }

    #endregion

    #region By Class

    public class ViewResultByClassModel
    {
        public ViewResultByClassModel()
        {
            ViewResultDetailList = new List<ViewResultDetail>();
        }
        public bool IsExamresultWithheld { get; set; }
        public string StudentName { get; set; }

        public int StudentId { get; set; }

        public string RollNo { get; set; }

        public string TotalMaxMarks { get; set; }

        public string TotalPassMarks { get; set; }

        public string TotalObtainedMarks { get; set; }

        public string Result { get; set; }

        public string Remarks { get; set; }
        
        public string Percentage { get; set; }

        public IList<ViewResultDetail> ViewResultDetailList { get; set; }

        public string AdmnNo { get; set; }

        public string Grade { get; set; }

        public string InternalAssessment { get; set; }
    }

    public class ViewResultDetail
    {
        public string SubjectName { get; set; }

        public string ObtainedMarks { get; set; }

        public string InternalAssessment { get; set; }

        public bool IsSubjectInternalAssessment { get; set; }

        public string TotalObtainedMarks { get; set; }

        public string Grade { get; set; }
    }

    #endregion

}