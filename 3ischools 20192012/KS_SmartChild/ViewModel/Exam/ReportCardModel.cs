﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Exam
{
    public class ReportCardModel : ViewResultModel
    {
        public ReportCardModel()
        {
            ViewResultDetailBySubjectList = new List<ViewReportcardmarks>();
            ExamTermEndSkillsListModel = new List<ExamTermEndSkillsListModel>();
            ViewResultDetailByCoscholastic = new List<ViewReportcardmarks>();
            ViewResultDetailByDiscipline = new List<ViewReportcardmarks>();
            AvailableSession = new List<SelectListItem>();
        }

        [AllowHtml]
        public string ReportCardDynamicHtml { get; set; }

        public bool IsPdf { get; set; }
        public bool IsExcel { get; set; }
        public string CurrentSession { get; set; }
        public string Class { get; set; }
        public string Standard { get; set; }
        public string RollNo { get; set; }
        public string StudentName { get; set; }
        public string ParentName { get; set; }
        public string DOB { get; set; }
        public string AdmissionNo { get; set; }
        public string AnotherAddress { get; set; }
        public bool MultipleTerm { get; set; }
        public int? SessionId { get; set; }
        public IList<SelectListItem> AvailableSession { get; set; }
        public IList<ViewReportcardmarks> ViewResultDetailBySubjectList { get; set; }
        public IList<ViewReportcardmarks> ViewResultDetailByCoscholastic { get; set; }
        public IList<ViewReportcardmarks> ViewResultDetailByDiscipline { get; set; }
        public IList<ExamTermEndSkillsListModel> ExamTermEndSkillsListModel { get; set; }
        public string SchoolName { get; set; }
        public string SchoolAddress { get; set; }
        public string BoardLogopath { get; set; }
        public string SchoolLogoPath { get; set; }
        public bool IsAuthToPrint { get; set; }
        public decimal TotalMarksObtained { get; set; }
        public string TotalGrade { get; set; }
        public decimal TotalPercentage { get; set; }
        public bool IsClassWisePrint { get; set; }
        public string House { get; set; }
        public string AadhaarNumber { get; set; }
    }
    public class ReportCardClassWiseModel
    {

        public ReportCardClassWiseModel() 
        {
            ReportCardClassWiseList = new List<ReportCardModel>();
        }
        public IList<ReportCardModel> ReportCardClassWiseList { get; set; }
    }
    public class ViewReportcardmarks : ViewResultDetailBySubject
    {
        public ViewReportcardmarks()
        {
            ExamTermEndSkillsListModel = new List<ExamTermEndSkillsListModel>();
            ExamTermEndSkillsListModelterm1 = new List<ExamTermEndSkillsListModel>();
        }
        public decimal PeriodicTestmarks { get; set; }
        public decimal AnnualTestmarks { get; set; }
        public decimal MarksObtained { get; set; }
        public decimal NoteBook { get; set; }
        public decimal SubjectEnrichment { get; set; }
        public string Grade { get; set; }
        public decimal PeriodicTestmarksterm2 { get; set; }
        public decimal AnnualTestmarksterm2 { get; set; }
        public decimal MarksObtainedterm2 { get; set; }
        public decimal NoteBookterm2 { get; set; }
        public decimal SubjectEnrichmentterm2 { get; set; }
        public string Gradeterm2 { get; set; }
        public bool MultipleTerm { get; set; }
        public IList<ExamTermEndSkillsListModel> ExamTermEndSkillsListModel { get; set; }
        public IList<ExamTermEndSkillsListModel> ExamTermEndSkillsListModelterm1 { get; set; }
      
    }
    public class ExamTermEndSkillsListModel
    {
        public string ExamTermEndSkill { get; set; }
        public int ExamTermEndSkillId { get; set; }
        public string Value { get; set; }
        public string Value2 { get; set; }
    }
    public class PeriodicMarks
    {
        public decimal percentage { get; set; }
        public int resultid { get; set; }
    }

}