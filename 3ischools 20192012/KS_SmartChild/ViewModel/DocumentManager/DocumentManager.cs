﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.DocumentManager
{
    public class DocumentManager
    {
        public DocumentManager()
        {
            AvailableDocCategories = new List<SelectListItem>();
        }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string txtTitle { get; set; }
        public string txtDesc { get; set; }
        public string mdlDocumentId { get; set; }
        public string FileUpload1 { get; set; }
        public string updateFileUpload { get; set; }

        public int id { get; set; }

        public string TitleSearch { get; set; }
        public string DescriptionSearch { get; set; }
        public int GroupIdSearch { get; set; }
        public int hiddenManageGrid { get; set; }

        public string updateIssuedatepicker { get; set; }

        public string updaetexpdatepicker { get; set; }

        public int updatedocumentdetailId { get; set; }

        public string updateImgurl { get; set; }

        public string updateremarks { get; set; }
        public string updatereminderdatepicker { get; set; }

        public int DocumentId { get; set; }

        public List<SelectListItem> AvailableDocCategories { get; set; }
    }


    public class DocmentCategoryModel : DocumentManager
    {
        public int DocumentCategoryId { get; set; }
        public string DocumentCategory { get; set; }
        public string DocumentDescription { get; set; }
        public string documntdscrptn { get; set; }
        public string DocumentTitle { get; set; }
        public int SR { get; set; }
        public string ExpiryDate { get; set; }
    }

    public class DocmentDetailModel : DocmentCategoryModel
    {
        public int DocumentDetailId { get; set; }
        public string DocumentImage { get; set; }
        public string ExpiryDt { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime issuedte { get; set; }
        public DateTime UplodedOn { get; set; }
        public string IssuedDate { get; set; }
        public string Remarkssubstring { get; set; }
        public string Remarks { get; set; }
        public string ReminderDt { get; set; }
        public string UploadedDate { get; set; }
        public string ImagePathSpilt { get; set; }
        public string ReminderType { get; set; }
        public int ReminderTypeId { get; set; }
        public int MangeGridCount { get; set; }
        public string Removefrnt { get; set; }
        public string SchoolId { get; set; }
    }


}