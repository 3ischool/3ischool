﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.GatePass
{
    public class VisitorCardModel
    {
          public VisitorCardModel()
          {
            AvailableList = new List<SelectListItem>();
          }
        public int VisitorCardId { get; set; }

        public string EncVisitorCardId { get; set; }

        public string VisitorCardNo { get; set; }

        public string RFIDCode { get; set; }

        public bool IsIssued { get; set; }

        public string Availability { get; set; }

        public bool IsActive { get; set; }

        public string SerialNo { get; set; }

        public string LastGatePass { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public string AvailableId{ get; set; }

        public IList<SelectListItem> AvailableList { get; set; }
           
    }
}