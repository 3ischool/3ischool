﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.GatePass
{
    public class GatePassModel
    {
        public GatePassModel()
        {
            GatePassTypeList = new List<SelectListItem>();
            ConcernedStaffList = new List<SelectListItem>();
            StudentList = new List<SelectListItem>();
            StaffList = new List<SelectListItem>();
            VehicleTypeList = new List<SelectListItem>();
            ClassList = new List<SelectListItem>();
            CardList = new List<SelectListItem>();
            DetailList = new List<PassDetails>();
            StudentListForFilter = new List<SelectListItem>();
            SessionList = new List<SelectListItem>();
            SessionFilterList = new List<SelectListItem>();
            ClassFilterList = new List<SelectListItem>();
            AvailableSendMessageMobileNoOptions = new List<SelectListItem>();
        }

        public string GatePassTypeId { get; set; }

        public List<SelectListItem> GatePassTypeList { get; set; }

        public string IssueDate { get; set; }

        public string SerialNo { get; set; }

        public string Session { get; set; }

        public string IssueDateToPost { get; set; }

        public List<SelectListItem> ClassFilterList { get; set; }

        public string InTime { get; set; }

        public bool EnableSerialNo { get; set; }

        public string OutTime { get; set; }

        public string ConcernedStaffId { get; set; }

        public string MobileNo { get; set; }

        public string GuardianMobileNo { get; set; }

        public string Designation { get; set; }

        public string Department { get; set; }

        public string RollNo { get; set; }

        public List<SelectListItem> ConcernedStaffList { get; set; }

        public bool StuSMS { get; set; }

        public bool StuNonAppUser { get; set; }

        public bool StuNotification { get; set; }

        public string ImageUrl { get; set; }

        public string SMSSelectionStudentMobileNo { get; set; }

        public List<SelectListItem> AvailableSendMessageMobileNoOptions { get; set; }

        public bool IsAdmin { get; set; }

        public bool SMS { get; set; }

        public bool NonAppUser { get; set; }

        public bool Notification { get; set; }

        public bool GuardSMS { get; set; }

        public bool GuardNonAppUser { get; set; }

        public bool GuardNotification { get; set; }

        public string ParentName { get; set; }

        public string StudentId { get; set; }

        public int? SessionId { get; set; }

        public IList<SelectListItem> SessionFilterList { get; set; }

        public IList<SelectListItem> SessionList { get; set; }

        public List<SelectListItem> StudentList { get; set; }

        public string StaffId { get; set; }

        public List<SelectListItem> StaffList { get; set; }

        public string VehicleTypeId { get; set; }

        public List<SelectListItem> VehicleTypeList { get; set; }

        public string VehicleNo { get; set; }

        public string VisitorIdProof { get; set; }

        public string Purpose { get; set; }

        public string NoOfPersons { get; set; }

        public bool IsIssueDateDisabled { get; set; }

        public string GatePassId { get; set; }

        public string MadeByUserId { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public string ClassId { get; set; }

        public IList<SelectListItem> ClassList { get; set; }

        public IList<SelectListItem> CardList { get; set; }

        public string AdmissionNo { get; set; }

        public string PassDetails { get; set; }

        public List<PassDetails> DetailList { get; set; }

        public string ContactNo { get; set; }

        public string StaffImagepath { get; set; }

        public bool IsVisitorInfoOn { get; set; }

        public bool IsIdProofOn { get; set; }

        public string Schoollogo { get; set; }

        public bool IsShowPic { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToPrint { get; set; }

        public bool IsAuthToDelete { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToReceive { get; set; }

        public bool IsAuthToSet { get; set; }

        public string GatePassType { get; set; }

        public bool IsReturnable { get; set; }

        public bool IsIssueDateVisible { get; set; }

        public List<SelectListItem> StudentListForFilter { get; set; }

        public string SchoolName { get; set; }
        public string DropPoint { get; set; }
        public string BusNo { get; set; }
        public string RouteNo { get; set; }
        public bool IsAuthToUseVisitorCard { get; set; }
        public bool IsShowAddress { get; set; }
        public string PrintSettingLeftSideFooterText { get; set; }
        public string PrintSettingCentreSideFooterText { get; set; }
        public string PrintSettingRightSideFooterText { get; set; }
        public string SchoolAddress { get; set; }
    }

    public class GatePassGridModel : GatePassModel
    {
        public string GatePassType { get; set; }

        public string ConcernedPerson { get; set; }

        public bool IsTimeOutExist { get; set; }

        public bool IsAllGatePassReceived { get; set; }

        public string GatePassStatus { get; set; }

        public string IssuedTo { get; set; }

        public bool IsPrint { get; set; }

        public bool CanDelete { get; set; }

        public string IssuedToPopUp { get; set; }

        public string PurposePopUp { get; set; }

        public string IssueToDate { get; set; }

        public string visitor { get; set; }

        public string Session_Id { get; set; }

        public string ClassId { get; set; }

        public string SessionName { get; set; }

        public bool IsAnyCardAssigned { get; set; }
    }

    public class PassDetails
    {
        public string GatePassDetailId { get; set; }

        public string PersonName { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string CardId { get; set; }

        public string Relation { get; set; }

        public string Image { get; set; }

        public string CardNo { get; set; }

        public string EncCardId { get; set; }

        public bool IsRecieved { get; set; }
    }

    public class GuardinaDetailModel
    {
        public string GurardianId { get; set; }

        public string Name { get; set; }

        public string Relation { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string Image { get; set; }
    }

    public class ReceivedPassModel
    {
        public string GatePassDetailId { get; set; }

        public string GatePassNumber { get; set; }

        public string Received { get; set; }
    }

    public class GatePassAuthModel
    {
        public int Id { get; set; }

        public bool Status { get; set; }
    }

    public class DataList
    {
        public DataList()
        {
            List = new List<SetAuthModel>();
        }
        public string GataPassTypeId { get; set; }

        public List<SetAuthModel> List { get; set; }
    }

    public class SetAuthModel
    {
        public SetAuthModel()
        {
            GatePassAuthList = new List<GatePassAuth>();
        }
        public string AuthType { get; set; }

        public List<GatePassAuth> GatePassAuthList { get; set; }
    }

    public class GatePassAuth
    {
        public int Id { get; set; }

        public int ContactTypeId { get; set; }

        public bool Status { get; set; }

        public string Authority { get; set; }
    }
}