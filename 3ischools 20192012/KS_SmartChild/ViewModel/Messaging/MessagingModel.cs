﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Messaging
{
    public class MessagingModel
    {
        public EmailTemplateModel EmailTemplate { get; set; }
    }

    public class EmailTemplateModel
    {
       public EmailTemplateModel()
        {
            AvailableDelayPeriodList = new List<SelectListItem>();
            AvailableMessageDurationList = new List<SelectListItem>();
            AvailableEmailAccountList = new List<SelectListItem>();
            EmailTemplateList = new List<EmailTemplateModel>();
        }
        public int MsgEmailTemplateId { get; set; }
        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }

        public string BccEmailAddress { get; set; }

        public string CCEmailAddress { get; set; }

        [Required(ErrorMessage = "Subject is Required")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Body is Required")]
        [AllowHtml]
        public string Body { get; set; }

        public bool IsActive { get; set; }

        public bool IsSendImmediately { get; set; }

        public bool IsInAdvance { get; set; }

        public int? DelayBeforeSend { get; set; }

        public int? DelayPeriodId { get; set; }

        public int? MsgDurationId { get; set; }

        public int? MsgFrequencyId { get; set; }

        public int? EmailAccountId { get; set; }

        public IList<SelectListItem> AvailableDelayPeriodList { get; set; }

        public IList<SelectListItem> AvailableMessageDurationList { get; set; }

        public IList<SelectListItem> AvailableEmailAccountList { get; set; }

        public List<EmailTemplateModel> EmailTemplateList { get; set; }

        public string EncMsgEmailTemplateId { get; set; }

        public bool IsAuthToAdd { get; set; }
        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToDelete { get; set; }

    }

    public class SMSTemplateModel
    {
        public SMSTemplateModel()
        {
            AvailableMessageDurationList = new List<SelectListItem>();
            SMSTemplateList = new List<SMSTemplateModel>();
        }
        public int MsgSMSTemplateId { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Body is Required")]
        [AllowHtml]
        public string Body { get; set; }

        public bool IsActive { get; set; }

        public bool IsInAdvance { get; set; }

        public string SendTo { get; set; }

        public int? MsgDurationId { get; set; }

        public int? MsgFrequencyId { get; set; }

        public IList<SelectListItem> AvailableMessageDurationList { get; set; }

        public List<SMSTemplateModel> SMSTemplateList { get; set; }

        public string EncMsgSMSTemplateId { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToDelete { get; set; }


    }
}