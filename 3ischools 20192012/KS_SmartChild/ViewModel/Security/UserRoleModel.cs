﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Security
{
    public class UserRoleModel
    {
        public UserRoleModel()
        {
            AvailableContactType = new List<SelectListItem>();
            UserRoleList = new List<UserRoleList>();
            UserRoleTypeList = new List<SelectListItem>();
            AvailableStaffs = new List<SelectListItem>();
        }

        public int? UserRoleId { get; set; }

        public string UserRole { get; set; }

        public string UserTypeId { get; set; }

        public string UserTypeRoleId { get; set; }

        public bool IsActive { get; set; }

        public int? UserRoleContactTypeId { get; set; }

        public int? ContactTypeId { get; set; }

        public string StaffId { get; set; }

        public IList<SelectListItem> AvailableStaffs { get; set; }

        public IList<SelectListItem> AvailableContactType { get; set; }

        public IList<UserRoleList> UserRoleList { get; set; }

        public string EncUserRoleId { get; set; }

        public int? UserRoleTypeId { get; set; }

        public List<SelectListItem> UserRoleTypeList { get; set; }
    }

    public class UserRoleList : UserRoleModel
    {
        public string Status { get; set; }

        public bool IsDeleted { get; set; }

        public string ContactType { get; set; }

        public string UserContactId { get; set; }

        public string UserRoleType { get; set; }

        public bool IsNavigationPerExist { get; set; }

        public bool IsActionPerExist { get; set; }
    }

    public class UserRoleTypeListModel
    {
        public int? UserRoleTypeId { get; set; }

        public string UserRoleType { get; set; }
    }

    public class ChangeUserRoleModel
    {
        public ChangeUserRoleModel()
        {
            UserTypeList = new List<SelectListItem>();
            UserList = new List<SelectListItem>();
            UserRoleList = new List<SelectListItem>();
            ClassList = new List<SelectListItem>();
        }
        public string UserTypeId { get; set; }

        public List<SelectListItem> UserTypeList { get; set; }

        public string UserId { get; set; }

        public List<SelectListItem> UserList { get; set; }

        public string UserRoleId { get; set; }

        public List<SelectListItem> UserRoleList { get; set; }

        public string Classid { get; set; }

        public List<SelectListItem> ClassList { get; set; }
    }
}