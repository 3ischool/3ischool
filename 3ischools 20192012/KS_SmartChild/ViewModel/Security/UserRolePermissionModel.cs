﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.ViewModel.Security
{
    public class UserRolePermissionModel
    {
        public UserRolePermissionModel()
        {
            UserRoleModulePermissinList = new List<UserRoleModulePermissin>();
        }

        public string UserRoleId { get; set; }

        public string permarray { get; set; }

        public string TypeId { get; set; }

        public string UserRole { get; set; }

        public string UserRoleType { get; set; }

        public string UserRoleTypeId { get; set; }

        public IList<UserRoleModulePermissin> UserRoleModulePermissinList { get; set; }
    }

    public class UserRoleModulePermissin
    {
        public UserRoleModulePermissin()
        {
            UserRoleAppFormsList = new List<UserRoleAppForms>();
        }
        public IList<UserRoleAppForms> UserRoleAppFormsList { get; set; }
    }

    public class UserRoleAppForms
    {
        public UserRoleAppForms()
        {
            UserRoleAppFormActionsList = new List<UserRoleAppFormActions>();
            UserRoleChildAppFormsList = new List<UserRoleChildAppForms>();
        }

        public string AppFormId { get; set; }

        public string AppForm { get; set; }

        public string UserRoleFormPermissionId { get; set; }

        public bool IsPermitted { get; set; }

        public bool IsForm { get; set; }

        public IList<UserRoleAppFormActions> UserRoleAppFormActionsList { get; set; }

        public IList<UserRoleChildAppForms> UserRoleChildAppFormsList { get; set; }
    }

    public class UserRoleChildAppForms
    {
        public UserRoleChildAppForms()
        {
            UserRoleAppFormActionsList = new List<UserRoleAppFormActions>();
            UserRoleChildAppFormsList = new List<UserRoleChildAppForms1>();
        }

        public string AppFormId { get; set; }

        public string AppForm { get; set; }

        public string UserRoleFormPermissionId { get; set; }

        public bool IsPermitted { get; set; }

        public bool IsForm { get; set; }

        public IList<UserRoleAppFormActions> UserRoleAppFormActionsList { get; set; }

        public IList<UserRoleChildAppForms1> UserRoleChildAppFormsList { get; set; }
    }

    public class UserRoleChildAppForms1
    {
        public UserRoleChildAppForms1()
        {
            UserRoleAppFormActionsList = new List<UserRoleAppFormActions>();
        }

        public string AppFormId { get; set; }

        public string AppForm { get; set; }

        public string UserRoleFormPermissionId { get; set; }

        public bool IsPermitted { get; set; }

        public bool IsForm { get; set; }

        public IList<UserRoleAppFormActions> UserRoleAppFormActionsList { get; set; }
    }



    public class UserRoleAppFormActions
    {
        public string AppFormActionId { get; set; }

        public string AppFormAction { get; set; }

        public bool IsPermitted { get; set; }

        public bool IsForm { get; set; }

        public string UserRoleActionPermissionId { get; set; }
    }

    public class UserRoleActionPermissionModel
    {
        public int AppFormId { get; set; }

        public int AppFormActionId { get; set; }

        public int UserRoleActionPermissionId { get; set; }

        public bool IsPermitted { get; set; }

        public bool IsForm { get; set; }
    }

    public class UserPermissionModel
    {
        public UserPermissionModel()
        {
            List = new List<UserNavigationPermissionModel>();
        }
        public int UserRoleId { get; set; }

        public string UserRole { get; set; }

        public string PermArray { get; set; }

        public int UserRoleTypeId { get; set; }

        public bool IsNavigationExist { get; set; }

        public List<UserNavigationPermissionModel> List { get; set; }
    }

    public class UserNavigationPermissionModel
    {
        public UserNavigationPermissionModel()
        {
            navigation = new List<UserParentNavigationPermissionModel>();
        }
        public string user_role_type { get; set; }

        public int user_role_type_id { get; set; }

        public string user_role_place { get; set; }

        public List<UserParentNavigationPermissionModel> navigation { get; set; }
    }

    public class DecPermArrayModel
    {
        public int NavigationId { get; set; }

        public bool IsActive { get; set; }
    }


    public class UserParentNavigationPermissionModel
    {
        public UserParentNavigationPermissionModel()
        {
            Child = new List<UserSecondLevNavigationPermissionModel>();
        }

        public int NavigationPlacementId { get; set; }

        public int NavigationItemId { get; set; }

        public string NavigationItem { get; set; }

        public string ControllerName { get; set; }

        public string ActionName { get; set; }

        public string QueryString { get; set; }

        public string NavItemTypeId { get; set; }

        public string NavItemType { get; set; }

        public bool IsActive { get; set; }

        public List<UserSecondLevNavigationPermissionModel> Child { get; set; }
    }

    public class UserSecondLevNavigationPermissionModel
    {
        public UserSecondLevNavigationPermissionModel()
        {
            Child = new List<UserThirdLevNavigationPermissionModel>();
        }
        public int NavigationPlacementId { get; set; }

        public int NavigationItemId { get; set; }

        public string NavigationItem { get; set; }

        public string ControllerName { get; set; }

        public string ActionName { get; set; }

        public string QueryString { get; set; }

        public string NavItemTypeId { get; set; }

        public string NavItemType { get; set; }

        public bool IsActive { get; set; }

        public List<UserThirdLevNavigationPermissionModel> Child { get; set; }
    }

    public class UserThirdLevNavigationPermissionModel
    {
        public int NavigationPlacementId { get; set; }

        public int NavigationItemId { get; set; }

        public string NavigationItem { get; set; }

        public string ControllerName { get; set; }

        public string ActionName { get; set; }

        public string QueryString { get; set; }

        public string NavItemTypeId { get; set; }

        public string NavItemType { get; set; }

        public bool IsActive { get; set; }
    }
}