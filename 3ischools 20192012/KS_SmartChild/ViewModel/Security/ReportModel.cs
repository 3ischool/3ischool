using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Security
{
    public class ReportModel
    {
        public ReportModel()
        {
            UserTypeList = new List<SelectListItem>();
            AvaiableStatusList = new List<SelectListItem>();
            AvaiableUserRoles = new List<SelectListItem>();
            AvaiableUserClasses = new List<SelectListItem>();
            AvaiableSessions = new List<SelectListItem>();
            CreateUserModel = new CreateUserModel();
        }
        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public string UserTypeId { get; set; }

        public string StatusId { get; set; }

        public string UserRoleId { get; set; }

        public int ClassId { get; set; }

        public int SessionId { get; set; }

        public IList<SelectListItem> AvaiableSessions { get; set; }

        public List<SelectListItem> AvaiableUserClasses { get; set; }

        public List<SelectListItem> AvaiableUserRoles { get; set; }

        public List<SelectListItem> AvaiableStatusList { get; set; }

        public List<SelectListItem> UserTypeList { get; set; }

        public CreateUserModel CreateUserModel { get; set; }
    }


    public class UserListModel
    {

        public string EncUserId { get; set; }
        
        public string UserId { get; set; }

        public int SessionId { get; set; }

        public string UserName { get; set; }

        public bool InActiveUser { get; set; }

        public string UserFLName { get; set; }

        public int UserContactId { get; set; }

        public int ClassId { get; set; }

        public string Class { get; set; }

        public string PhoneNumber { get; set; }

        public int? UserTypeId { get; set; }

        public string UserType { get; set; }

        public string UserRoleId { get; set; }

        public string UserRole { get; set; }

        public string RegistrationDate { get; set; }

        public string Status { get; set; }

        public string FullName { get; set; }
        

        public string User { get; set; }
    }

}