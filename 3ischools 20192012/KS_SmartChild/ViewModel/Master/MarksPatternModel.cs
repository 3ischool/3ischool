﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.ViewModel.Master
{
    public class MarksPatternModel
    {
        public MarksPatternModel()
        {
            AvailableGradePattern = new List<SelectListItem>();
            MarksPatternList = new List<MarksPatternModel>();
        }
        public int? MarksPatternId { get; set; }

        public int? GradePatternId { get; set; }

        public decimal? MaxMarks { get; set; }

        public decimal? PassMarks { get; set; }


        public decimal? IAMaxMarks { get; set; }

        public decimal? IAPassMarks { get; set; }

        public IList<SelectListItem> AvailableGradePattern { get; set; }

        public IList<MarksPatternModel> MarksPatternList { get; set; }
        public string GradePattern { get; set; }
        public bool MarksPatternUsed { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToDelete { get; set; }

        public string EncMarksPatternId { get; set; }

        public bool IsExamInternalAssesmentApplicable { get; set; }

        public string ExamInternalAssessmentText { get; set; }
    }
}