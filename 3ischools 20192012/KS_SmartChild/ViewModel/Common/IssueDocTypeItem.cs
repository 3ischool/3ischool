﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.ViewModel.Common
{
    public class IssueDocTypeItem
    {
        public string IssueDocTypeID { get; set; }
        public string IssueDoctype { get; set; }
        public bool IOO { get; set; }
        public bool MI { get; set; }
        public bool FC { get; set; }
        public bool Selected { get; set; }

    }
}