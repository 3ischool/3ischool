﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.ViewModel.Common
{
    public class LeaveModel
    {
        public int LeaveId { get; set; }

        public string LeaveType { get; set; }

        public DateTime? LeaveFrom { get; set; }

        public DateTime? LeaveTill { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }

        public string ReasonDesc { get; set; }

        public bool IsLeaveEditable { get; set; }

        public bool IsLeavedeleteable { get; set; }

        public string LeaveTypeId { get; set; }
    }
}