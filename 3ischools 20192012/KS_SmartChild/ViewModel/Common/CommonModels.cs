﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.ViewModel.Common
{
    public class CommonModels
    {
        public int Id { get; set; }
        public bool IsDb { get; set; }
        public bool IsActive { get; set; }
        public string htmldata { get; set; }
    }

    public class PracticalSubjectModel
    {
        public int ClassSubjectId { get; set; }
        public int SubjectId { get; set; }
        public string Subject { get; set; }
        public bool IsPractical { get; set; }
    }

    public class SendSelectedStudentsSMS
    {
        public string StudentId { get; set; }
        public string ModuleId { get; set; }
        public string Flags { get; set; }
    }

    public class SurveillanceAlarmModel
    {
        public string StudentImage { get; set; }
        public string StudentName { get; set; }
        public string AdmnNo { get; set; }
        public string ClassName { get; set; }
        public string DeviceLocation { get; set; }
        public string EntryTime { get; set; }
        public int SurveillancePunchId { get; set; }
        public int StudentId { get; set; }
        public int SurveillanceAllarmId { get; set; }
    }
    public class SchoolCredentails
    {
        //public SchoolCredentails() {
        //    Database = new List<databases>();
        //}
        public string objecttype { get; set; }
        public int[] Database { get; set; }
    }
    public class databases
    {
        public string DatabaseId { get; set; }
        public string SchoolDB { get; set; }

    }

}