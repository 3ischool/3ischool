﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.ViewModel.Common
{
    public class LanguageModel
    {
        public int LanguageId { get; set; }
        public string Language { get; set; }
        public string LanguageAbbr {get; set; }
        public int IsActive { get; set; }
    }
}