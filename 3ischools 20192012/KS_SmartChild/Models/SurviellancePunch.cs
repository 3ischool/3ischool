//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SurviellancePunch
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SurviellancePunch()
        {
            this.SurveillanceAlarms = new HashSet<SurveillanceAlarm>();
        }
    
        public int SurviellancePunchId { get; set; }
        public Nullable<int> ContactId { get; set; }
        public Nullable<int> ContactTypeId { get; set; }
        public Nullable<System.DateTime> InTime { get; set; }
        public Nullable<System.DateTime> OutTime { get; set; }
        public Nullable<System.DateTime> ClosedTime { get; set; }
        public Nullable<System.DateTime> AutoCloseTime { get; set; }
        public Nullable<int> AttendanceDeviceId { get; set; }
        public Nullable<int> AttendanceLocationId { get; set; }
        public Nullable<int> ClosedByUserId { get; set; }
        public string Remarks { get; set; }
        public Nullable<byte> IsAlarmActive { get; set; }
    
        public virtual AttendanceDevice AttendanceDevice { get; set; }
        public virtual AttendanceLocation AttendanceLocation { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SurveillanceAlarm> SurveillanceAlarms { get; set; }
    }
}
