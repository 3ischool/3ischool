//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FeeHead
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FeeHead()
        {
            this.GroupFeeHeads = new HashSet<GroupFeeHead>();
            this.IssueDocFeeHeads = new HashSet<IssueDocFeeHead>();
            this.ReceiptTypeFeeHeads = new HashSet<ReceiptTypeFeeHead>();
            this.StudentFacilityDetails = new HashSet<StudentFacilityDetail>();
            this.StudentFeeAddOns = new HashSet<StudentFeeAddOn>();
        }
    
        public int FeeHeadId { get; set; }
        public string FeeHead1 { get; set; }
        public Nullable<int> FeeHeadTypeId { get; set; }
        public Nullable<bool> UseInFeeCertificate { get; set; }
    
        public virtual FeeHeadType FeeHeadType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GroupFeeHead> GroupFeeHeads { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IssueDocFeeHead> IssueDocFeeHeads { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReceiptTypeFeeHead> ReceiptTypeFeeHeads { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentFacilityDetail> StudentFacilityDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StudentFeeAddOn> StudentFeeAddOns { get; set; }
    }
}
