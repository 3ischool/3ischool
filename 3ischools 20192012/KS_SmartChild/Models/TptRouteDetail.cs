//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TptRouteDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TptRouteDetail()
        {
            this.TptRouteAttendants = new HashSet<TptRouteAttendant>();
            this.TptRouteDrivers = new HashSet<TptRouteDriver>();
        }
    
        public int TptRouteDetailId { get; set; }
        public Nullable<int> DriverId { get; set; }
        public Nullable<int> BusId { get; set; }
        public Nullable<System.DateTime> EffectiveDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public int BusRouteId { get; set; }
        public Nullable<int> TptAttendantId { get; set; }
    
        public virtual Bus Bus { get; set; }
        public virtual BusRoute BusRoute { get; set; }
        public virtual TptAttendant TptAttendant { get; set; }
        public virtual TptDriver TptDriver { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TptRouteAttendant> TptRouteAttendants { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TptRouteDriver> TptRouteDrivers { get; set; }
    }
}
