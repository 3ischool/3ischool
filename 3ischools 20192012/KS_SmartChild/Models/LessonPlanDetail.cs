//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class LessonPlanDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LessonPlanDetail()
        {
            this.LessonPlanNotes = new HashSet<LessonPlanNote>();
        }
    
        public int LessonPlanDetailId { get; set; }
        public Nullable<int> LessonPlanId { get; set; }
        public Nullable<int> SyllabusUnitTopicId { get; set; }
        public string SyllabusUnitTopic { get; set; }
        public string TeachingAids { get; set; }
        public string LearningOutcome { get; set; }
        public string TopicDescription { get; set; }
        public Nullable<System.DateTime> CreationDate { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public Nullable<System.DateTime> CheckedOn { get; set; }
        public string Remarks { get; set; }
        public Nullable<int> CheckedByUserId { get; set; }
    
        public virtual LessonPlan LessonPlan { get; set; }
        public virtual SyllabusUnitTopic SyllabusUnitTopic1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LessonPlanNote> LessonPlanNotes { get; set; }
    }
}
