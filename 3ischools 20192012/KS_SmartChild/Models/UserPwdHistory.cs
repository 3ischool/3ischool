//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserPwdHistory
    {
        public int UserPwdHistoryId { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<System.DateTime> ChangedOn { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    
        public virtual SchoolUser SchoolUser { get; set; }
    }
}
