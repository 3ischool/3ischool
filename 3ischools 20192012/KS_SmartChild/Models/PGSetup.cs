//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PGSetup
    {
        public int PGId { get; set; }
        public string PGName { get; set; }
        public string PGCode { get; set; }
        public string RequestParams { get; set; }
        public string ResponseParams { get; set; }
        public string PGModuleURL { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsDefault { get; set; }
        public Nullable<decimal> MinimumTrsAmount { get; set; }
        public string PGImage { get; set; }
    }
}
