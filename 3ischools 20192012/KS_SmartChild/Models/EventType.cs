//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class EventType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EventType()
        {
            this.Events = new HashSet<Event>();
            this.EventCols = new HashSet<EventCol>();
        }
    
        public int EventTypeId { get; set; }
        public string EventType1 { get; set; }
        public string EventColor { get; set; }
        public Nullable<byte> ShowInWebCalendar { get; set; }
        public Nullable<byte> ShowInAppCalendar { get; set; }
        public Nullable<byte> SortingMethod { get; set; }
        public string EventCode { get; set; }
        public string MobColoCode { get; set; }
        public string MobileIcon { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Event> Events { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EventCol> EventCols { get; set; }
    }
}
