//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SurviellanceStandard
    {
        public int SurviellanceStandardId { get; set; }
        public Nullable<int> SurviellanceSetupId { get; set; }
        public Nullable<int> StandardId { get; set; }
    
        public virtual SurviellanceSetup SurviellanceSetup { get; set; }
    }
}
