//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExamSubjectGroupType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ExamSubjectGroupType()
        {
            this.ExamStandardSubjectGroups = new HashSet<ExamStandardSubjectGroup>();
        }
    
        public int ExamSubjectGroupTypeId { get; set; }
        public string ExamSubjectGroupType1 { get; set; }
        public string GroupMark { get; set; }
        public string GroupText { get; set; }
        public Nullable<int> Bestof { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsAverage { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ExamStandardSubjectGroup> ExamStandardSubjectGroups { get; set; }
    }
}
