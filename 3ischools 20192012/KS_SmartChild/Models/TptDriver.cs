//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TptDriver
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TptDriver()
        {
            this.TptRouteDetails = new HashSet<TptRouteDetail>();
            this.TptRouteDrivers = new HashSet<TptRouteDriver>();
        }
    
        public int TptDriverId { get; set; }
        public string DriverName { get; set; }
        public string DriverLicenseNo { get; set; }
        public Nullable<System.DateTime> LicenseExpiryDate { get; set; }
        public string DriverDescription { get; set; }
        public string ContactNo { get; set; }
        public Nullable<System.DateTime> DateOfJoining { get; set; }
        public Nullable<System.DateTime> DateOfLeaving { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TptRouteDetail> TptRouteDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TptRouteDriver> TptRouteDrivers { get; set; }
    }
}
