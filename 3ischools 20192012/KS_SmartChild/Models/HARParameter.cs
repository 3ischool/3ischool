//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HARParameter
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HARParameter()
        {
            this.HARPageColumnDatas = new HashSet<HARPageColumnData>();
            this.HARStudentClasses = new HashSet<HARStudentClass>();
        }
    
        public int HARParameterId { get; set; }
        public string HARParameter1 { get; set; }
        public string HARParameterRemarks { get; set; }
        public Nullable<int> HARComponentId { get; set; }
        public int HARParameterParentId { get; set; }
    
        public virtual HARComponent HARComponent { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HARPageColumnData> HARPageColumnDatas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HARStudentClass> HARStudentClasses { get; set; }
    }
}
