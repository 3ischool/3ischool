//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClassTestMark
    {
        public int ClassTestMarksId { get; set; }
        public Nullable<int> ClassTestId { get; set; }
        public Nullable<int> StudentId { get; set; }
        public Nullable<decimal> Marks { get; set; }
        public Nullable<bool> IsAbsent { get; set; }
        public string Remarks { get; set; }
        public Nullable<int> GradeId { get; set; }
        public Nullable<int> ClassTestNonSubReasonId { get; set; }
    
        public virtual ClassTest ClassTest { get; set; }
        public virtual ClassTestNonSubReason ClassTestNonSubReason { get; set; }
        public virtual Grade Grade { get; set; }
        public virtual Student Student { get; set; }
    }
}
