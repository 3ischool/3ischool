//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TrsDoc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TrsDoc()
        {
            this.InventoryVouchers = new HashSet<InventoryVoucher>();
        }
    
        public int TrsDocId { get; set; }
        public string TrsDoc1 { get; set; }
        public Nullable<int> TrsDocTypeId { get; set; }
        public Nullable<byte> IsFix { get; set; }
        public string TrsDocAbbr { get; set; }
        public Nullable<bool> IsSale { get; set; }
        public Nullable<byte> IsActive { get; set; }
        public Nullable<bool> ShowLogoInPrint { get; set; }
        public Nullable<bool> ShowNameInPrint { get; set; }
        public Nullable<bool> ShowAddressInPrint { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InventoryVoucher> InventoryVouchers { get; set; }
        public virtual TrsDocType TrsDocType { get; set; }
    }
}
