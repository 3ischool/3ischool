//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductReturn
    {
        public int ProductReturnId { get; set; }
        public Nullable<int> ProductTransactionId { get; set; }
        public Nullable<decimal> ReturnedQty { get; set; }
        public Nullable<System.DateTime> ReturnDate { get; set; }
    
        public virtual ProductTransaction ProductTransaction { get; set; }
    }
}
