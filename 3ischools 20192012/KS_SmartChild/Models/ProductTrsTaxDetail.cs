//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProductTrsTaxDetail
    {
        public int ProductTrsTaxDetailId { get; set; }
        public Nullable<int> ProductTransactionId { get; set; }
        public Nullable<decimal> GSTRate { get; set; }
        public Nullable<decimal> CGSTValue { get; set; }
        public Nullable<decimal> SGSTValue { get; set; }
        public Nullable<decimal> IGSTValue { get; set; }
        public Nullable<decimal> CessValue { get; set; }
    
        public virtual ProductTransaction ProductTransaction { get; set; }
    }
}
