//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SportEventTeamPlayer
    {
        public int SportEventTeamPlayerId { get; set; }
        public Nullable<int> SportEventTeamId { get; set; }
        public Nullable<int> SportStudentId { get; set; }
        public Nullable<int> SportGameTitleId { get; set; }
        public string PerformanceRemarks { get; set; }
        public Nullable<int> SortingIndex { get; set; }
        public Nullable<bool> IsExtra { get; set; }
    
        public virtual SportEventTeam SportEventTeam { get; set; }
        public virtual SportGameTitle SportGameTitle { get; set; }
        public virtual SportStudent SportStudent { get; set; }
    }
}
