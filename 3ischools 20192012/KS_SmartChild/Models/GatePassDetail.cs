//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class GatePassDetail
    {
        public int GatePassDetailId { get; set; }
        public Nullable<int> GatePassId { get; set; }
        public string PersonName { get; set; }
        public string RelationWithStudent { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public Nullable<int> VisitorCardId { get; set; }
        public string PersonImage { get; set; }
        public Nullable<System.DateTime> Received { get; set; }
    
        public virtual GatePass GatePass { get; set; }
        public virtual VisitorCard VisitorCard { get; set; }
    }
}
