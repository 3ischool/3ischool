//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KS_SmartChild.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FeeClassGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FeeClassGroup()
        {
            this.FeeClassGroupConcessions = new HashSet<FeeClassGroupConcession>();
            this.FeeClassGroupDetails = new HashSet<FeeClassGroupDetail>();
            this.GroupFeeTypes = new HashSet<GroupFeeType>();
        }
    
        public int FeeClassGroupId { get; set; }
        public string FeeClassGroup1 { get; set; }
        public Nullable<int> FeeClassGroupTypeId { get; set; }
    
        public virtual FeeClassGroupType FeeClassGroupType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FeeClassGroupConcession> FeeClassGroupConcessions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FeeClassGroupDetail> FeeClassGroupDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GroupFeeType> GroupFeeTypes { get; set; }
    }
}
