﻿$(document).ready(function () {

    $('.Amount').keypress(function (event) {
        var $this = $(this);
        if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
           ((event.which < 48 || event.which > 57) &&
           (event.which != 0 && event.which != 8))) {
            event.preventDefault();
        }
        var text = $(this).val();
        if ((event.which == 46) && (text.indexOf('.') == -1)) {
            setTimeout(function () {
                if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                    $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                }
            }, 1);
        }
        if ((text.indexOf('.') != -1) &&
            (text.substring(text.indexOf('.')).length > 2) &&
            (event.which != 0 && event.which != 8) &&
            ($(this)[0].selectionStart >= text.length - 2)) {
            event.preventDefault();
        }
    });

    $('.Amount').bind("paste", function (e) {
        var text = e.originalEvent.clipboardData.getData('Text');
        if ($.isNumeric(text)) {
            if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
                e.preventDefault();
                $(this).val(text.substring(0, text.indexOf('.') + 3));
            }
        }
        else {
            e.preventDefault();
        }
    });

});

// function check emial verified
function isValidEmailAddress(emailAddress) {
    var pattern = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return pattern.test(emailAddress);
};

// function check valid mobile no
function validateMobile(mobileno) {
    if (mobileno.length == 10) {
        if (mobileno.match('[0-9]{10}'))
            return true;
        else
            return false;
    }
    else
        return false;
}


// function check valid aplanumeric values for textbox
function alphaNumericOnly(txtValue) {
    var alphaNumericPattern = /^[a-z0-9]+$/i;
    return alphaNumericPattern.test(txtValue);
}

// function check valid Landline no
function validateWebsite(websiteUrl) {
    var UrlPattern = /^((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z0-9\&\.\/\?\:@\-_=#])*/g;
    return UrlPattern.test(websiteUrl);
}

// function check valid Landline no
function validateMultipleLandline(landlineno) {
    var phoneNumberPattern = /^(\d{*}(,\d{*})*)?$/;
    return phoneNumberPattern.test(landlineno);
}

// function check valid Landline no
function validateLandline(landlineno) {
    var phoneNumberPattern = /^[0-9]\d{2,4}-\d{6,8}$/;
    return phoneNumberPattern.test(landlineno);
}

// function for remove last comma from string
var removeLastChar = function (value, char) {
    var lastChar = value.slice(-1);
    if (lastChar == char) {
        value = value.slice(0, -1);
        return removeLastChar(value, char);
    }
    else
        return value;
}

//Confirmation box with yes/no
function doConfirm(msg, yesFn, noFn) {
    var confirmBox = $("#confirmBox");
    confirmBox.find(".message").text(msg);
    confirmBox.find("span").addClass("btn");
    confirmBox.find(".yes,.no").unbind().click(function () {
        confirmBox.hide();
    });
    confirmBox.find(".yes").click(yesFn);
    confirmBox.find(".no").click(noFn);
    confirmBox.show();
}

function NumericOnlyWithDecimal(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

// THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
function isNumber(evt, element) {

    var charCode = (evt.which) ? evt.which : event.keyCode

    if (
        //(charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
        //(charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
        (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function NumericOnly(e) {
    // with special char and with space
    var inputvalue = e.which;
    if ((inputvalue >= 35 && inputvalue <= 40) || (inputvalue >= 48 && inputvalue <= 57) ||
        inputvalue == 8 || inputvalue == 9 || inputvalue == 32 || (inputvalue >= 96 && inputvalue <= 105)) {
        return;
    }
    else
        e.preventDefault();
}

function CheckNumeric(e) { // with only 0-9
    //if the letter is not digit then don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        e.preventDefault(); //return false;
    }
}

//allow delete key click
function AlphabeticValuesOnly(e) {
    // Allow controls such as backspace, tab etc.
    var arr = [8, 9, 16, 17, 20, 32, 35, 36, 37, 38, 39, 40, 45, 46];

    // Allow letters
    for (var i = 65; i <= 90; i++) {
        arr.push(i);
    }

    // Prevent default if not in array
    if (jQuery.inArray(event.which, arr) === -1) {
        event.preventDefault();
    }
}


//does not allow delete button
function lettersOnly(e) {
    var inputvalue = e.which;
    if ((inputvalue >= 65 && inputvalue <= 90) || (inputvalue >= 35 && inputvalue <= 40) || inputvalue == 8 || inputvalue == 9 || inputvalue == 32) {
        return;
    }
    else {
        e.preventDefault();
    }
}

function display_kendoui_grid_error(e) {
    if (e.errors) {
        if ((typeof e.errors) == 'string') {
            //single error
            //display the message
            alert(e.errors);
        } else {
            //array of errors
            var message = "The following errors have occurred:";
            //create a message containing all errors.
            $.each(e.errors, function (key, value) {
                if (value.errors) {
                    message += "\n";
                    message += value.errors.join("\n");
                }
            });
            //display the message
            alert(message);
        }
        //ignore empty error
    }
    else if (e.errorThrown)
        alert('Error happened');
}

$(document).on('keyup', '.numericonly', function () {
    this.value = this.value.replace(/[^0-9]/g, '');
});