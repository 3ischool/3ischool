﻿using System.Web;
using System.Web.Optimization;

namespace KS_SmartChild
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
           //  css files bundling
            bundles.Add(new StyleBundle("~/bundles/bootstracss").Include(
                      "~/Content/assets/css/bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/bundles/login").Include(
                      "~/Content/assets/css/login.css"));

            bundles.Add(new StyleBundle("~/bundles/themecss").Include(
                      "~/Content/assets/css/maroon-theme.css"));

            bundles.Add(new StyleBundle("~/bundles/stylecss").Include(
                      "~/Content/assets/css/styles.css"));

            bundles.Add(new StyleBundle("~/bundles/iestyle").Include(
                      "~/Content/assets/css/ie10-viewport-bug-workaround.css"));

            bundles.Add(new StyleBundle("~/bundles/select2css").Include(
               "~/Content/select2.min.css"));

            bundles.Add(new StyleBundle("~/bundles/Kendocommon").Include(
                "~/Content/Kendo/kendo.common.min.css"));

            bundles.Add(new StyleBundle("~/bundles/Kendotheme").Include(
                "~/Content/Kendo/kendo.bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/bundles/accordion").Include(
              "~/Content/accordion.min.css"));
            bundles.Add(new StyleBundle("~/bundles/mutliselect").Include(
          "~/Content/assets/css/bootstrap-multiselect.css"));

               bundles.Add(new StyleBundle("~/bundles/content").Include(
          "~/Content/Site.css",
          "~/Content/assets/css/Dashboard.css",
          "~/Content/assets/css/jquery.mczToast.css",
          "~/Content/assets/css/timepicki.css",
          "~/Content/assets/css/yamm.css",
          "~/Content/assets/css/yammdemo.css"
          ));

               bundles.Add(new StyleBundle("~/bundles/timeoutDialog").Include(
          "~/Content/timeout-dialog.css"));
                bundles.Add(new StyleBundle("~/bundles/jqueryUI").Include(
          "~/Content/jquery-ui.css"));
              bundles.Add(new StyleBundle("~/bundles/bootstrapcdn").Include(
          "~/Content/font-awesome.min.css"));
            
              bundles.Add(new StyleBundle("~/bundles/Navigation").Include(
         "~/Content/assets/css/navigation.css"));
              bundles.Add(new StyleBundle("~/bundles/ksdrawer").Include(
          "~/Content/assets/css/ks-drawer.css"));

              bundles.Add(new StyleBundle("~/bundles/jquerydatetimepickercss").Include(
          "~/Content/jquerydatetimepicker/css/jquery.datetimepicker.min.css"));
      


            // js script files bundling

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Content/assets/js/jquery.min.js"));

          

            bundles.Add(new ScriptBundle("~/bundles/bootstrapjs").Include(
                      "~/Content/assets/js/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/iescript").Include(
                     "~/Content/assets/js/ie10-viewport-bug-workaround.js"));

            bundles.Add(new ScriptBundle("~/bundles/select2js").Include(
              "~/Scripts/select2.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/Kendocore").Include(
                      "~/Scripts/Kendo/kendo.core.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Kendoweb").Include(
                      "~/Scripts/Kendo/kendo.web.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Kendogrid").Include(
                      "~/Scripts/Kendo/kendo.grid.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Kendodatepicker").Include(
                      "~/Scripts/Kendo/kendo.datetimepicker.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Kendodata").Include(
                      "~/Scripts/Kendo/kendo.data.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Kendocalender").Include(
                      "~/Scripts/Kendo/kendo.calendar.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Kendoall").Include(
                      "~/Scripts/Kendo/kendo.all.min.js", "~/Scripts/Kendo/jszip.min.js"));

            //bundles.Add(new ScriptBundle("~/bundles/Kendoall").Include(
            //       "~/Scripts/Kendo/jszip.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/multiselectjs").Include(
                    "~/Scripts/bootstrap-multiselect.js"));

             bundles.Add(new ScriptBundle("~/bundles/common").Include(
                    "~/Scripts/common.js"));
             bundles.Add(new ScriptBundle("~/bundles/ksdrawerscript").Include(
                    "~/Content/assets/js/ks-drawer.js"));
             bundles.Add(new ScriptBundle("~/bundles/jquerymczToast").Include(
                    "~/Content/assets/js/jquery.mczToast.js"));
             bundles.Add(new ScriptBundle("~/bundles/timepicki").Include(
                    "~/Content/assets/js/timepicki.js"));
             bundles.Add(new ScriptBundle("~/bundles/webcam").Include(
                    "~/Scripts/webcam.js"));
             bundles.Add(new ScriptBundle("~/bundles/timeoutdialog").Include(
                    "~/Scripts/timeout-dialog.js"));
             bundles.Add(new ScriptBundle("~/bundles/jqueryuiscript").Include(
                    "~/Scripts/jquery-ui.js"));

               bundles.Add(new ScriptBundle("~/bundles/jquerydatetimepickerjs").Include(
                       "~/Content/jquerydatetimepicker/js/jquery.datetimepicker.min.js"));

            //bundles.Add(new ScriptBundle("~/bundles/sweetalert2cdn").Include(
            //        "~/Scripts/sweetalert2cdn"));
            bundles.IgnoreList.Clear();
        }
    }
}
