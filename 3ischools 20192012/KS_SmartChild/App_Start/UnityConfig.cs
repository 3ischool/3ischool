using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.UserModule;
using DAL.DAL.AddressModule;
using DAL.DAL.Authentication;
using System.Web;
using DAL.DAL.Common;
using DAL.DAL.Security;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.CommunicationModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.ContactModule;
using DAL.DAL.DocumentModule;
using DAL.DAL.FeeModule;
using DAL.DAL.GuardianModule;
using DAL.DAL.LateFeeModule;
using DAL.DAL.SMSAPIModule;
using DAL.DAL.StudentModule;
using DAL.DAL.StaffModule;
using DAL.DAL.TransportModule;
using DAL.DAL.SettingService;
using DAL.DAL.ClassPeriodModule;
using DAL.DAL.MessengerModule;
using DAL.DAL.ExaminationModule;
using DAL.DAL.Schedular;
using DAL.DAL.MenuModel;
using DAL.DAL.CertificateModule;
using BAL.BAL.Common;
using BAL.BAL.Security;
using BAL.BAL;
using DAL.DAL.ReportingModule;
using BAL.BAL.Reporting;
using DAL.DAL.MessagingModule;
using DAL.DAL.HomeWorkModule;
using DAL.DAL.LibraryModule;
using DAL.DAL.StockModule;
using DAL.DAL.InfoBrowser;
using BAL.BAL.Message;
using DAL.DAL.PublicViewModule;
using DAL.DAL.TriggerEventModule;
using DAL.DAL.Message;

namespace KS_SmartChild.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            container.RegisterType<HttpContextBase>(new InjectionFactory(_ =>
                new HttpContextWrapper(HttpContext.Current)));

            // TODO: Register your types here

            container.RegisterType<IActivityLogMasterService, ActivityLogMasterService>();
            container.RegisterType<IActivityLogService, ActivityLogService>();

            container.RegisterType<IAddressMasterService, AddressMasterService>();
            container.RegisterType<IAddressService, AddressService>();

            container.RegisterType<IAuthenticationService, FormsAuthenticationService>();

            container.RegisterType<ICatalogMasterService, CatalogMasterService>();
            container.RegisterType<IConnectionService, ConnectionService>();
            container.RegisterType<ISubjectService, SubjectService>();

            container.RegisterType<IDropDownService, DropDownService>();
            container.RegisterType<IWebHelper, WebHelper>();

            container.RegisterType<ICommunicationService, CommunicationService>();

            container.RegisterType<IContactService, ContactService>();

            container.RegisterType<IDocumentService, DocumentService>();

            container.RegisterType<ILogService, LogService>();

            container.RegisterType<IFeeService, FeeService>();

            container.RegisterType<IGuardianService, GuardianService>();

            container.RegisterType<ILateFeeService, LateFeeService>();

            container.RegisterType<IEncryptionService, EncryptionService>();

            container.RegisterType<ISMSAPIService, SMSAPIService>();

            container.RegisterType<IStudentAddOnService, StudentAddOnService>();
            container.RegisterType<IStudentMasterService, StudentMasterService>();
            container.RegisterType<IStudentService, StudentService>();

            container.RegisterType<IStaffService, StaffService>();

            container.RegisterType<IBusService, BusService>();
            container.RegisterType<ITransportService, TransportService>();

            container.RegisterType<IUserService, UserService>();

            container.RegisterType<ISchoolSettingService, SchoolSettingService>();

            container.RegisterType<IClassPeriodService, ClassPeriodService>();

            container.RegisterType<IMessengerService, MessengerService>();

            container.RegisterType<IExamService, ExamService>();

            container.RegisterType<ISchedularService, SchedularService>();

            container.RegisterType<INavigationService, NavigationService>();

            container.RegisterType<ICertificateService, CertificateService>();

            container.RegisterType<ICustomEncryption, CustomEncryption>();

            container.RegisterType<IModuleVersionService, ModuleVersionService>();

            container.RegisterType<IUserManagementService, UserManagementService>();

            container.RegisterType<IUserAuthorizationService, UserAuthorizationService>();

            container.RegisterType<IMainService, MainService>();

            container.RegisterType<IReportService, ReportService>();

            container.RegisterType<IReportingLogicService, ReportingLogicService>();

            container.RegisterType<IMessagingService, MessagingService>();

            container.RegisterType<ISMSSender, SMSSender>();

            container.RegisterType<IHomeWorkService, HomeWorkService>();

            container.RegisterType<IBookMasterService, BookMasterService>();

            container.RegisterType<ITempOTP, TempOTP>();

            container.RegisterType<IExportHelper, ExportHelper>();

            container.RegisterType<IGatePassService, GatePassService>();
            container.RegisterType<IStockService, StockService>();
            container.RegisterType<ISchoolDataService, SchoolDataService>();

            container.RegisterType<IObjectBrowserInfo, ObjectBrowserInfo>();
            container.RegisterType<IWorkflowMessageService, WorkflowMessageService>();
            container.RegisterType<ITokenizer, Tokenizer>();

            container.RegisterType<IPublicViewInformation, PublicViewInformation>();
            container.RegisterType<ITriggerEventService, TriggerEventService>();
            container.RegisterType<ICommonMethodService, CommonMethodService>();
           
        }
    }
}
