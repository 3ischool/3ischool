$(document).ready(function(e) {	
						   
        $('<div id="mczToastNotification"></div><style>#mczToastNotification{ width:442px; height:auto; position:fixed; z-index:999999999999999999;}.mczToastWrap{width:400px; height:auto; float:left;padding:10px; font-size:16px; border-radius:5px; -moz-border-radius:5px; -ms-border-radius:5px; -o-border-radius:5px; -webkit-border-radius:5px; color:#fff; box-shadow: 0 1px 5px rgba(0, 0, 0, 0.15);-moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.15);-webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.15); margin:2px 10px; display:none;}.mcztop{ top:0;}.mczbottom{ bottom:0;}.mczleft{ left:20px;}.mczright{ right:20px;}.mczcenter{ left:50%; margin-left:-221px;}</style>').appendTo('body');
		 
		});
	$(document).on("click",".mczToastClose",function(e){
			
			var ToastId = '#mczToast'+$(this).attr('data-id');
			$(ToastId).slideUp(800,function(){$(ToastId).remove();});
			
			var obj = $('#mczToastNotification');		
			var count = obj.children('div').length;
			 
			if(count==1){ obj.delay(1000).fadeOut(800);}
			});
			
	 	$(document).on("click",".mczToastWrap a",function(e){
			
			var ToastId =  $(this).parent('div').attr('data-id');
			$(ToastId).slideUp(800,function(){$(ToastId).remove();});
			
			var obj = $('#mczToastNotification');		
			var count = obj.children('div').length;
			 
			if(count==1){ obj.delay(1000).fadeOut(800);}
			});	
			
			 
	(function($){
    $.mczToast = function(errorMessage,errorType,autoClose,autoDelay,settings){
		// settings
		var mczToastConfig = {
			'vPos': 'top',
			'hPos': 'center',			
		}; 
		if ( settings ){$.extend(mczToastConfig, settings);}
		if ( errorMessage ){}else{errorMessage='Please enter message';}
		if ( errorType ){}else{errorMessage='You have not defined one of the required parameters (Message , Error Type)'; errorType='error';}
		if ( autoClose ){}else{autoClose=false;}
		if ( autoDelay ){}else{autoDelay=2000;}

		 
		var obj = $('#mczToastNotification');		
		var count = obj.children('div').length;
		
		
		if(count<1){obj.addClass("mcz"+mczToastConfig.vPos+" mcz"+mczToastConfig.hPos); obj.fadeIn(800);}
		
		$('<div  id="mczToast'+count+'"  class="mczToastWrap mcz'+errorType+'"><table cellpadding="0" cellspacing="0" style="width:400px;" border="0"><tr><td valign="middle" align="left" width="380">'+errorMessage+'</td><td valign="middle" align="right" width="20"><i class="mczToastClose icon-remove" style="cursor:pointer;" data-id="'+count+'"></i></td></tr></table></div>').appendTo(obj);	
        $("#mczToast"+count).slideDown(800,function(){
			if(autoClose==true){
				$(this).delay(autoDelay).fadeOut(800);
				}
			});
		return this;
	};
	
	
	$.mczToastDestroy = function(){		 
		
		var obj = $('#mczToastNotification');		
		var count = obj.children('div').length;
		 
		 
		 $('#mczToastNotification div').each(function(index, element) {
			 var index1 = index+1;
            $(this).fadeOut(800,function(){$(this).remove();});
			 
			if(index1==count){
				obj.delay(1000).fadeOut(800);
				}
        	}); 		
		 
		
		
		 
		
		 	
         
		return this;
	};
	
})(jQuery);
