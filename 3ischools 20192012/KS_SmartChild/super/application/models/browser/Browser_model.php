<?php
class Browser_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}

function get_browser_obj_col($browser_obj_id){
	
	$this->db->select('BrowserObjectCol.BrowserObjectColId,BrowserObjectCol.BrowserObjectCol,BrowserObjectCol.CanView,BrowserObjectCol.CanEdit,BrowserObjectCol.CanFilter');
	$this->db->from('BrowserObjectCol');
	$this->db->where('BrowserObjectCol.BrowserObjectId', $browser_obj_id);
	//$this->db->join('TableMaster','TableMaster.TableId = BrowserObjectCol.TableId');
	$result = $this->db->get();
	return $data = $result->result_array();
	}		
	
	function model_get_all_browser_obj(){
		
			$this->db->select('BrowserObjectId,BrowserObject,MaxViewColCount,MaxColCount,MaxFilterColCount,StoredProcedureName');
			$this->db->from('BrowserObject');
			$this->db->where('BrowserObject.IsActicve', 1);
			$result = $this->db->get();
			return $data = $result->result_array();
	}

	function get_browser_obj_col_ref($BrowserObjectColId){
		
			$this->db->select('BrowserObjectColRef.BrowserObjectColRefId,BrowserObjectColRef.RefTable,BrowserObjectColRef.ControlType');
			$this->db->from('BrowserObjectColRef');
			$this->db->where('BrowserObjectColRef.BrowserObjectColId', $BrowserObjectColId);
			//$this->db->join('TableMaster','TableMaster.TableId = BrowserObjectColRef.RefTableId');
			$result = $this->db->get();
			return $data = $result->result_array();
	}
	
	function get_all_browser_obj_col(){
		
			$this->db->select('BrowserObjectCol.BrowserObjectColId,BrowserObjectCol.BrowserObjectCol,BrowserObjectCol.CanView,BrowserObjectCol.CanEdit,BrowserObjectCol.CanFilter,BrowserObject.BrowserObject');
			$this->db->from('BrowserObjectCol');
			$this->db->join('BrowserObject','BrowserObject.BrowserObjectId = BrowserObjectCol.BrowserObjectId');
			$result = $this->db->get();
			return $data = $result->result_array();
	}

	function get_all_browser_object_col_ref(){
		
			$this->db->select('BrowserObjectColRef.BrowserObjectColRefId,BrowserObjectColRef.ControlType,BrowserObjectCol.BrowserObjectCol,BrowserObjectColRef.RefTableCol,BrowserObjectColRef.RefTable');
			$this->db->from('BrowserObjectColRef');
			$this->db->join('BrowserObjectCol','BrowserObjectCol.BrowserObjectColId = BrowserObjectColRef.BrowserObjectColId');
			$result = $this->db->get();
			return $data = $result->result_array();
	}

	function get_table_cols_by_ids($table_col_array){
		
			$this->db->select('ActualColName');
			$this->db->from('TableCol');
			$this->db->where_in('TableColId',$table_col_array);
			$result = $this->db->get();
			return $data = $result->result_array();
	}

}
