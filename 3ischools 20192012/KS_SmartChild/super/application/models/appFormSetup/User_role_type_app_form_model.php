<?php
class User_role_type_app_form_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}


	function get_user_role_type_app_form($user_role_type_id, $is_active=""){
	    
	    $this->db->select('UserRoleTypeAppForm.UserRoleTypeAppFormId, UserRoleTypeAppForm.IsActive, AppForm.AppForm, AppForm.AppFormId, NavigationItem.NavigationItemId, NavigationItem.NavigationItem, NavigationItem.ControllerName, NavigationItem.ActionName, NavigationItem.QueryString, AppForm.AppFormParentId');
	    $this->db->from('UserRoleTypeAppForm');
	    if($is_active != ''){
			$this->db->where('UserRoleTypeAppForm.IsActive',$is_active);
		}
	    $this->db->where('UserRoleTypeAppForm.UserRoleTypeId',$user_role_type_id);
	    $this->db->order_by('AppForm.SortingIndex','asc');
	    $this->db->join('AppForm','AppForm.AppFormId = UserRoleTypeAppForm.AppFormId');
	    $this->db->join('NavigationItem','NavigationItem.NavigationItemId = AppForm.NavigationItemId','left');
	    $this->db->order_by('UserRoleTypeAppForm.SortingIndex','asc');
		$result = $this->db->get();
	    return $result = $result->result_array();
	}
	
	
	function get_user_role_type_app_form_by_parent($user_role_type_id, $parent_Id, $is_active=""){
	    
	    $this->db->select('UserRoleTypeAppForm.UserRoleTypeAppFormId, UserRoleTypeAppForm.IsActive, AppForm.AppForm, AppForm.AppFormId, NavigationItem.NavigationItemId, NavigationItem.NavigationItem, NavigationItem.ControllerName, NavigationItem.ActionName, NavigationItem.QueryString, AppForm.AppFormParentId');
	    $this->db->from('UserRoleTypeAppForm');
	    if($is_active != ''){
			$this->db->where('UserRoleTypeAppForm.IsActive',$is_active);
			
		}
		$this->db->where('AppForm.AppFormParentId',$parent_Id);
	    $this->db->where('UserRoleTypeAppForm.UserRoleTypeId',$user_role_type_id);
	    $this->db->order_by('AppForm.SortingIndex','asc');
	    $this->db->join('AppForm','AppForm.AppFormId = UserRoleTypeAppForm.AppFormId');
	    $this->db->join('NavigationItem','NavigationItem.NavigationItemId = AppForm.NavigationItemId');
	    $this->db->order_by('UserRoleTypeAppForm.SortingIndex','asc');
		$result = $this->db->get();
	    return $result = $result->result_array();
	}
	
	function get_app_forms($app_form_id_rec=""){
		
		$this->db->select('AppForm,AppFormId');	
		$this->db->from('AppForm');	
		if(!empty($app_form_id_rec)){
			$this->db->where_not_in('AppFormId',$app_form_id_rec);	
		}
		$this->db->order_by('AppForm','asc');
		$result = $this->db->get();
	    return $result = $result->result_array();
	}
	
	 
	function get_form_action_and_type_by_app_form($app_form_id){
	    
	    $this->db->select('AppFormAction.AppFormActionId,AppFormAction.ActionName,AppFormAction.ActionInfo,AppFormActionType.FormActionType,AppFormActionType.FormActionTypeId');
	    $this->db->from('AppFormAction');
	    $this->db->where('AppFormAction.AppFormId', $app_form_id);
	    $this->db->join('AppFormActionType','AppFormActionType.FormActionTypeId = AppFormAction.FormActionTypeId');
	    $result = $this->db->get();
	    return $result = $result->result_array();
}
 
}
