<?php
class AppFormTitle_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}


function get_all_appFormTitle($app_form_id,$language_id){
	 
			 $this->db->select('AppFormTitle.AppFormTitleId, AppFormTitle.AppFormTitle, AppForm.AppForm, Language.Language');
			 $this->db->from('AppFormTitle');
			 if($app_form_id != ''){
				$this->db->where('AppForm.AppFormId', $app_form_id);
			 }
			 if($language_id != ''){
				$this->db->where('Language.LanguageId', $language_id);
			 }
			 $this->db->join('AppForm', 'AppForm.AppFormId = AppFormTitle.AppFormId');
			 $this->db->join('Language', 'Language.LanguageId = AppFormTitle.LanguageId');
			 $result = $this->db->get();
			 return $data = $result->result_array();
}

 
 function update_app_form_title($q){
	 
		$q = "update AppFormTitle set ".$q;
		$query = $this->db->query($q);
		$error = $this->db->error();
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = 1;
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to update data</div>';
		}
		return $rData;
 }
 function insert_app_form_title($appFormId,$language_id,$appFormTitle){
	 
		$q = "insert into AppFormTitle(AppFormId,LanguageId,AppFormTitle) values(".$appFormId.",".$language_id.",N'".$appFormTitle."')";
		$query = $this->db->query($q);
		$error = $this->db->error();
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = $this->db->insert_id();
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to insert data</div>';
		}
		
		return $rData;
 }

}
