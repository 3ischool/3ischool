<?php
class Module_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}


function get_all_module($pageSize,$pageNumber){
	    
	    $q = 'WITH CTE AS
    (
      SELECT
        ROW_NUMBER() OVER ( ORDER BY [ModuleId] ) AS RowNum , *
      FROM Module

        
    )

   SELECT
      *
    FROM CTE
    WHERE
      (RowNum > '.$pageSize.' * ('.$pageNumber.' - 1) )
      AND
      (RowNum <= '.$pageSize.' * '.$pageNumber.' )
    Order By RowNum ';
	    
	    
	    $query = $this->db->query($q);
        return $result = $query->result_array();
}


function record_count(){
	
	$this->db->select('ModuleId');
	$result = $this->db->get('Module');
	return $data = $result->num_rows();
	}	

}
