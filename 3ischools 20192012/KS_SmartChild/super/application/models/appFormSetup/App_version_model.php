<?php
class App_version_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}


function get_all_app_version($pageSize,$pageNumber){
	    
	    $q = 'WITH CTE AS
    (
      SELECT
        ROW_NUMBER() OVER ( ORDER BY [VersionId] ) AS RowNum , *
      FROM AppVersion

        
    )

   SELECT
      *
    FROM CTE
    WHERE
      (RowNum > '.$pageSize.' * ('.$pageNumber.' - 1) )
      AND
      (RowNum <= '.$pageSize.' * '.$pageNumber.' )
    Order By RowNum ';
	    
	    
	    $query = $this->db->query($q);
        return $result = $query->result_array();
}


function record_count(){
	
	$this->db->select('VersionId');
	$result = $this->db->get('AppVersion');
	return $data = $result->num_rows();
	}	

}
