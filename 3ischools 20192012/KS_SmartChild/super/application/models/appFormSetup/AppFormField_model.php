<?php
class AppFormField_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}


function get_all_appFormField($pageSize,$pageNumber){
	    
	    $q = 'WITH CTE AS
    (
      SELECT
        ROW_NUMBER() OVER ( ORDER BY [AppFormFieldId] ) AS RowNum , *
      FROM AppFormField

        
    )

   SELECT
      *
    FROM CTE
    WHERE
      (RowNum > '.$pageSize.' * ('.$pageNumber.' - 1) )
      AND
      (RowNum <= '.$pageSize.' * '.$pageNumber.' )
    Order By RowNum ';
	    
	    
	    $query = $this->db->query($q);
        return $result = $query->result_array();
}


function record_count(){
	
	$this->db->select('AppFormFieldId');
	$result = $this->db->get('AppFormField');
	return $data = $result->num_rows();
	}	

}
