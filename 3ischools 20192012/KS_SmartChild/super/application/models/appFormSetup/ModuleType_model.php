<?php
class ModuleType_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}


function get_all_moduleType($pageSize,$pageNumber){
	    
	    $q = 'WITH CTE AS
    (
      SELECT
        ROW_NUMBER() OVER ( ORDER BY [ModuleTypeId] ) AS RowNum , *
      FROM ModuleType

        
    )

   SELECT
      *
    FROM CTE
    WHERE
      (RowNum > '.$pageSize.' * ('.$pageNumber.' - 1) )
      AND
      (RowNum <= '.$pageSize.' * '.$pageNumber.' )
    Order By RowNum ';
	    
	    
	    $query = $this->db->query($q);
        return $result = $query->result_array();
}


function record_count(){
	
	$this->db->select('ModuleTypeId');
	$result = $this->db->get('ModuleType');
	return $data = $result->num_rows();
	}	

}
