<?php
class App_form_action_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}


function get_all_app_form_action(){
	    
	    $this->db->select('AppFormAction.AppFormActionId,AppFormAction.ActionName,AppFormAction.ActionInfo,AppForm.AppForm,AppFormActionType.FormActionType');
	    $this->db->from('AppFormAction');
	    $this->db->join('AppForm','AppForm.AppFormId = AppFormAction.AppFormId');
	    $this->db->join('AppFormActionType','AppFormActionType.FormActionTypeId = AppFormAction.FormActionTypeId');
	    $this->db->order_by('AppFormActionId','desc');
		$result = $this->db->get();
	    return $result = $result->result_array();
}


function record_count(){
	
	$this->db->select('AppFormActionId');
	$result = $this->db->get('AppFormAction');
	return $data = $result->num_rows();
	}	

}
