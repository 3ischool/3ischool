<?php
class App_form_action_type_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}


function get_all_app_form_action_type($pageSize,$pageNumber){
	    
	    $q = 'WITH CTE AS
    (
      SELECT
        ROW_NUMBER() OVER ( ORDER BY [FormActionTypeId] ) AS RowNum , *
      FROM AppFormActionType

        
    )

   SELECT
      *
    FROM CTE
    WHERE
      (RowNum > '.$pageSize.' * ('.$pageNumber.' - 1) )
      AND
      (RowNum <= '.$pageSize.' * '.$pageNumber.' )
    Order By RowNum ';
	    
	    
	    $query = $this->db->query($q);
        return $result = $query->result_array();
}


function record_count(){
	
	$this->db->select('FormActionTypeId');
	$result = $this->db->get('AppFormActionType');
	return $data = $result->num_rows();
	}	

}
