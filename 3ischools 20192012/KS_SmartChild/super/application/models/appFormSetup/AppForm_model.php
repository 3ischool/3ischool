<?php
class AppForm_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}


function get_all_appForm_by_id($parent_id){
	    
	    $this->db->select('AppForm.AppFormId,AppForm.AppForm,Module.Module,NavigationItem.NavigationItem');
	    $this->db->from('AppForm');
	    $this->db->where('AppForm.AppFormParentId',$parent_id);
	    if($parent_id == 0){
			$this->db->or_where('AppForm.AppFormParentId',null);
		}
	    $this->db->join('Module','Module.ModuleId = AppForm.ModuleId');
	    $this->db->join('NavigationItem','NavigationItem.NavigationItemId = AppForm.NavigationItemId','left');
	    $query = $this->db->get();
        return $result = $query->result_array();
}


function record_count(){
	
	$this->db->select('AppFormId');
	$result = $this->db->get('AppForm');
	return $data = $result->num_rows();
	}	

}
