<?php
class AppFormLabel_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}
 

function get_all_records($language_id){
	
	$this->db->select('AppFormLabelId,FormFieldLabel,Language.Language,AppFormField.FieldName');
	$this->db->from('AppFormLabel');
	if($language_id != ''){
		$this->db->where('Language.LanguageId',$language_id);
	}
	$this->db->join('Language','Language.LanguageId=AppFormLabel.LanguageId');
	$this->db->join('AppFormField','AppFormField.AppFormFieldId=AppFormLabel.AppFormFieldId');
	$result = $this->db->get();
	return $data = $result->result_array();
	}	

function update_app_form_label($q){
	 
		$q = "update AppFormLabel set ".$q;
		$query = $this->db->query($q);
		$error = $this->db->error();
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = 1;
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to update data</div>';
		}
		return $rData;
 }
 function insert_app_form_label($appFormFieldId,$languageId,$appFormLabel,$formFieldDescription){
	 
		$q = "insert into AppFormLabel(AppFormFieldId,LanguageId,FormFieldLabel,FormFieldHelpText) values(".$appFormFieldId.",".$languageId.",N'".$appFormLabel."','".$formFieldDescription."')";
		$query = $this->db->query($q);
		$error = $this->db->error();
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = $this->db->insert_id();
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to insert data</div>';
		}
		
		return $rData;
 }

 
	/* getting data against a particular column table */
		function get_label_by_lang($currentLangId){	
		  
			$this->db->from('AppFormLabel');             
            $this->db->select('AppFormLabel.FormFieldLabel,AppFormLabel.FormFieldHelpText,AppFormLabel.AppFormFieldId,AppFormField.FieldName,AppFormField.AppFormId');
            $this->db->where('AppFormLabel.LanguageId',$currentLangId);
            $this->db->join('AppFormField','AppFormField.AppFormFieldId = AppFormLabel.AppFormFieldId');
			$query = $this->db->get();	
            
			return $query->result_array();	
		}
	/* //getting data against a particular column table */	

}
