<?php
class App_version_detail_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}


function get_all_app_version_detail($pageSize,$pageNumber){
	    
	    $q = 'WITH CTE AS
    (
      SELECT
        ROW_NUMBER() OVER ( ORDER BY [VersionDetailId] ) AS RowNum , *
      FROM AppVersionDetail

        
    )

   SELECT
      *
    FROM CTE
    WHERE
      (RowNum > '.$pageSize.' * ('.$pageNumber.' - 1) )
      AND
      (RowNum <= '.$pageSize.' * '.$pageNumber.' )
    Order By RowNum ';
	    
	    
	    $query = $this->db->query($q);
        return $result = $query->result_array();
}


function record_count(){
	
	$this->db->select('VersionDetailId');
	$result = $this->db->get('AppVersionDetail');
	return $data = $result->num_rows();
	}	

}
