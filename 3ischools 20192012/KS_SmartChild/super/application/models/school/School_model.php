<?php
class School_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}


 
function get_all_rec(){
	
	$this->db->select('School.SchoolId,School.SchoolName,School.Domain,School.StudentsStrength,SchoolDB.DBName,SchoolGroup.SchoolGroup,Board.BoardName');
	$this->db->from('School');
	$this->db->join('Board','Board.BoardId = School.BoardId','left');
	$this->db->join('SchoolGroup','SchoolGroup.SchoolGroupId = School.SchoolGroupId','left');
	$this->db->join('SchoolDB','SchoolDB.SchoolDBId = School.SchoolDBId','left');
	$result = $this->db->get();
	return $data = $result->result_array();
	}
	
function get_schools_with_db(){
	
	$this->db->select('School.Domain,SchoolDB.DBName');
	$this->db->from('School');
	$this->db->join('SchoolDB','SchoolDB.SchoolDBId = School.SchoolDBId','left');
	$result = $this->db->get();
	return $data = $result->result_array();
	}		

}
