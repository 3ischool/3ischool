<?php
class Navigation_placement_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}
 
	function get_navigation_lang_nav_place($place_id,$parent_id){
		
			$this->db->select('NavigationPlacement.*,NavigationItem.NavigationItemId,NavigationItem.NavigationItem');
			$this->db->from('NavigationPlacement');
			$this->db->where('NavigationPlacement.NavigationPlaceId', $place_id);
			$this->db->where('NavigationPlacement.RefId', $parent_id);
			$this->db->join('NavigationItem','NavigationItem.NavigationItemId = NavigationPlacement.NavigationItemId');
			$result = $this->db->get();
			return $result->result_array();
	} 
 

}
