<?php
class Language_navigation_item_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}


	function get_all(){
	 
			 $this->db->select('LanguageNavigationItemId, LanguageNavigationItem, NavigationItem.NavigationItem, Language.Language');
			 $this->db->from('LanguageNavigationItem');
			 $this->db->join('NavigationItem', 'NavigationItem.NavigationItemId = LanguageNavigationItem.NavigationItemId');
			 $this->db->join('Language', 'Language.LanguageId = LanguageNavigationItem.LanguageId');
			 $result = $this->db->get();
			 return $data = $result->result_array();
	}

 

}
