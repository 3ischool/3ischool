<?php
class Navigation_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}

	 

	function get_navigation_items($clientId, $refId){		
		$result = $this->db->query('select NavigationItem.NavigationItemId, NavigationItem.NavigationItem, NavigationItem.ControllerName, NavigationItem.ActionName, NavigationItem.QueryString from NavigationItem where NavigationItem.RefId = '.$refId.' order by SortingIndex asc');
		return $result->result_array();
	}
/*and NavigationItem.NavigationItemId in (select UserRoleNavigationItem.NavigationItemId from UserRoleNavigationItem where UserRoleNavigationItem.IsActive = 1 and UserRoleNavigationItem.UserRoleId = (select UserRoleId from ClientUser where ClientUserId = '.$clientId.')) */
	function get_max_sorting_value(){
		
		$this->db->select_max('SortingIndex');
		$this->db->from('NavigationItem');
		$result = $this->db->get();
		return $result->result_array();
	}

	function get_all_user_role_type_nav_place(){
		
		$this->db->select('NavigationPlace.NavigationPlace,UserRoleType.UserRoleType,UserRoleTypeNavigationPlace.AddInPermissionJson,UserRoleTypeNavigationPlace.UserRoleTypeNavigationPlaceId');	
		$this->db->from('UserRoleTypeNavigationPlace');
		$this->db->join('NavigationPlace','NavigationPlace.NavigationPlaceId = UserRoleTypeNavigationPlace.NavigationPlaceId');
		$this->db->join('UserRoleType','UserRoleType.UserRoleTypeId = UserRoleTypeNavigationPlace.UserRoleTypeId');
		$result = $this->db->get();
		return $result->result_array();
	}
	
	function get_user_role_type_nav_place($user_role_type, $place_id){
		
		$this->db->select('UserRoleTypeNavigationPlaceId');	
		$this->db->from('UserRoleTypeNavigationPlace');	
		$this->db->where('UserRoleTypeId',$user_role_type);	
		$this->db->where('NavigationPlaceId',$place_id);	
		$result = $this->db->get();
		return $result->result_array();
	}

}

