<?php
class Navigation_item_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}
 
	function get_all_parent_item(){
	 
			 $this->db->select('NavigationItemId, NavigationItem');
			 $this->db->from('NavigationItem');
			 $result = $this->db->get();
			 return $data = $result->result_array();
	}
	
	function get_navigation_items($navigation_item_id){
		
			$this->db->select('NavigationItemId, NavigationItem');
			$this->db->from('NavigationItem');
			$this->db->where_not_in('NavigationItemId', $navigation_item_id);
			$result = $this->db->get();
			return $data = $result->result_array();
		}
	function get_navigation_placement_by_place($role_type_nav_place_id, $parent_id){
		 
			$this->db->select('NavigationPlacement.NavigationPlacementId,NavigationItem.NavigationItemId,NavigationItem.NavigationItem,NavigationItem.ControllerName,NavigationItem.ActionName,NavigationItem.QueryString,NavigationPlacement.IsActive,NavigationPlacement.RefId');
			$this->db->from('NavigationPlacement');
			$this->db->where('NavigationPlacement.RefId', $parent_id);
			$this->db->where('NavigationPlacement.UserRoleTypeNavigationPlaceId', $role_type_nav_place_id);
			$this->db->order_by('NavigationPlacement.SortingIndex', 'asc');
			$this->db->join('NavigationItem', 'NavigationItem.NavigationItemId = NavigationPlacement.NavigationItemId','left');
			$result = $this->db->get();
			return $data = $result->result_array();
			 
		}
		
		
	function get_navigation_placement_by_place_parent($role_type_nav_place_id, $parent_id){
		 
			$this->db->select('NavigationPlacement.NavigationPlacementId as id,NavigationItem.NavigationItem as name,NavigationItem.ControllerName,NavigationItem.ActionName,NavigationItem.QueryString');
			$this->db->from('NavigationPlacement');
			$this->db->where('NavigationPlacement.RefId', $parent_id);
			$this->db->where('NavigationPlacement.UserRoleTypeNavigationPlaceId', $role_type_nav_place_id);
			$this->db->order_by('NavigationPlacement.SortingIndex', 'asc');
			$this->db->join('NavigationItem', 'NavigationItem.NavigationItemId = NavigationPlacement.NavigationItemId');
			$result = $this->db->get();
			
			return $data = $result->result_array();
		}	
/*
   function add_update_lang_nav_placement($placement_id, $language_id, $data){
	   
			$this->db->select('LangNavPlacementId');
			$this->db->from('LangNavPlacement');
			$this->db->where('NavigationPlacementId', $placement_id);
			$this->db->where('LanguageId', $language_id);
			$result = $this->db->get();
			$retData = $result->result_array();
			if(empty($retData)){
				$q = "insert into LangNavPlacement(NavigationPlacementId,LanguageId,LangNavigationItem) values(".$placement_id.",".$language_id.",N'".$data."')";
				$query = $this->db->query($q);
				$error = $this->db->error();
				if($error['code']==00000 && trim($error['message'])==''){
					$rData = $this->db->insert_id();
				}
				else{
					$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to insert data</div>';
				}
				
			}
			else{
				 
				$q = "update LangNavPlacement set LangNavigationItem = N'".$data."' where NavigationPlacementId = ".$placement_id." AND LanguageId = ".$language_id."";
				$query = $this->db->query($q);
				$error = $this->db->error();
				if($error['code']==00000 && trim($error['message'])==''){
					$rData = 1;
				}
				else{
					$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to update data</div>';
				}
			}
			 
			return $rData;	
	   }
	   */
	   function get_navigation_item_by_id($navigation_item_id){
		
			$this->db->select('NavigationItemId, NavigationItem');
			$this->db->from('NavigationItem');
			$this->db->where('NavigationItemId', $navigation_item_id);
			$result = $this->db->get();
			return $data = $result->result_array();
		}

	  function get_navigation_placement_rec_for_json($role_type_nav_place_id, $parent_id){
		 
			$this->db->select('NavigationPlacement.NavigationPlacementId,NavigationItem.NavigationItemId,NavigationItem.NavigationItem,NavigationItem.ControllerName,NavigationItem.ActionName,NavigationItem.QueryString,NavigationItem.NavigationIcon,NavigationItem.NavItemTypeId,NavItemType.NavItemType');
			$this->db->from('NavigationPlacement');
			$this->db->where('NavigationPlacement.IsActive', 1);
			$this->db->where('NavigationPlacement.RefId', $parent_id);
			$this->db->where('NavigationPlacement.UserRoleTypeNavigationPlaceId', $role_type_nav_place_id);
			$this->db->order_by('NavigationPlacement.SortingIndex', 'asc');
			$this->db->join('NavigationItem', 'NavigationItem.NavigationItemId = NavigationPlacement.NavigationItemId');
			$this->db->join('NavItemType', 'NavItemType.NavItemTypeId = NavigationItem.NavItemTypeId','left');
			$result = $this->db->get();
			return $data = $result->result_array();
			 
		}	
		
	function get_nav_placement($navigation_item_id){
		 
			$this->db->select('NavigationPlacement.UserRoleTypeNavigationPlaceId');
			$this->db->from('NavigationPlacement');
			$this->db->where('NavigationPlacement.NavigationItemId', $navigation_item_id);
			$result = $this->db->get();
			return $data = $result->result_array();
			 
		}	 
	
	function get_role_type_nav_place($role_type_nav_place_id=""){
		 
			$this->db->select('UserRoleTypeNavigationPlace.UserRoleTypeNavigationPlaceId,NavigationPlace.NavigationPlace,UserRoleType.UserRoleType');
			$this->db->from('UserRoleTypeNavigationPlace');
			if($role_type_nav_place_id != ''){
				$this->db->where('UserRoleTypeNavigationPlace.UserRoleTypeNavigationPlaceId',$role_type_nav_place_id);	
			}
			$this->db->join('NavigationPlace','NavigationPlace.NavigationPlaceId = UserRoleTypeNavigationPlace.NavigationPlaceId');
			$this->db->join('UserRoleType','UserRoleType.UserRoleTypeId = UserRoleTypeNavigationPlace.UserRoleTypeId');
			$result = $this->db->get();
			return $data = $result->result_array();
			 
		}	 
		 
	function get_active_permission_user_role_type_nav_place(){
		
			$this->db->select('UserRoleTypeNavigationPlaceId,UserRoleTypeNavigationPlace.NavigationPlaceId,UserRoleTypeNavigationPlace.UserRoleTypeId,NavigationPlace.NavigationPlace,UserRoleType.UserRoleType');
			$this->db->from('UserRoleTypeNavigationPlace');
			$this->db->where('AddInPermissionJson', 1);
			$this->db->join('NavigationPlace','NavigationPlace.NavigationPlaceId = UserRoleTypeNavigationPlace.NavigationPlaceId');
			$this->db->join('UserRoleType','UserRoleType.UserRoleTypeId = UserRoleTypeNavigationPlace.UserRoleTypeId');
			$result = $this->db->get();
			return $result->result_array();
	}

}
