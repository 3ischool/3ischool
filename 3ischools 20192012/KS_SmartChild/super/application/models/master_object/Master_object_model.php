<?php
class Master_object_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}

	 
	function get_master_ids_info(){
		
		$this->db->select('*');	
		$this->db->from('MasterIdsInfo');	
		$this->db->where('MasterIdsInfo.IsActive',1);	
		$this->db->order_by('MasterIdsInfo.SortingIndex','asc');	
		$result = $this->db->get();
		return $result->result_array();
	}

}

