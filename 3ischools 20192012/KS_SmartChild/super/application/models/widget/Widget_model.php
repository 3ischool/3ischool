<?php
class Widget_model extends CI_Model
{
	function __construct() 
	{
		parent::__construct();
	}


 
	function get_all_widget_type_records(){
		
		$this->db->select('Wid_WidgetType.WidgetTypeId,Wid_WidgetType.WidgetType,Wid_WidgetType.IsActive,Wid_HTMLTemplate.HTMLTemplateTitle');
		$this->db->from('Wid_WidgetType');
		$this->db->join('Wid_HTMLTemplate','Wid_HTMLTemplate.HTMLTemplateId = Wid_WidgetType.HTMLTemplateId');
		$result = $this->db->get();
		return $data = $result->result_array();
	}


	function get_all_color_class_records(){
		
		$this->db->select('Wid_ColorClassMaster.ColorClassId,Wid_ColorClassMaster.CSSClass,Wid_WidgetType.WidgetType');
		$this->db->from('Wid_ColorClassMaster');
		$this->db->join('Wid_WidgetType','Wid_WidgetType.WidgetTypeId = Wid_ColorClassMaster.WidgetTypeId');
		$result = $this->db->get();
		return $data = $result->result_array();
	}

	function get_all_widget_tab_records(){
		
		$this->db->select('Wid_Tab.TabId,Wid_Tab.TabTitle,Wid_Tab.IsActive,Wid_Tab.IconImage,Wid_Tab.IconColor,Wid_Area.AreaName');
		$this->db->from('Wid_Tab');
		$this->db->order_by('SortingIndex','asc');
		$this->db->join('Wid_Area','Wid_Area.AreaId = Wid_Tab.AreaId');
		$result = $this->db->get();
		return $data = $result->result_array();
	}

	function get_all_widget_records($widget_id=""){
		
		$this->db->select('Wid_Widget.WidgetId,Wid_Widget.WidgetTitle,Wid_Widget.WidgetIcon,Wid_Widget.InfoText,Wid_Widget.SPName,Wid_Widget.ClickURL,Wid_WidgetType.WidgetType,Wid_ColorClassMaster.CSSClass,Wid_Widget.WidgetTypeId,Wid_Widget.ColorClassId');
		$this->db->from('Wid_Widget');
		if($widget_id != ''){
			$this->db->where('Wid_Widget.WidgetId',$widget_id);	
		}
		$this->db->where('Wid_WidgetType.IsActive',1);	
		$this->db->join('Wid_WidgetType','Wid_WidgetType.WidgetTypeId = Wid_Widget.WidgetTypeId');
		$this->db->join('Wid_ColorClassMaster','Wid_ColorClassMaster.ColorClassId = Wid_Widget.ColorClassId');
		$result = $this->db->get();
		return $data = $result->result_array();
	}

	function get_all_tab_area_records(){
		
		$this->db->select('Wid_Tab.TabId,Wid_Tab.TabTitle,Wid_Area.AreaName');	
		$this->db->from('Wid_Tab');	
		$this->db->join('Wid_Area','Wid_Area.AreaId = Wid_Tab.AreaId');	
		$result = $this->db->get();
		return $data = $result->result_array();
	}

	function get_widget_tab_map($tab_id=""){
		
		$this->db->select('Wid_TabWidget.TabWidgetId,Wid_TabWidget.IsActive,Wid_Tab.TabTitle,Wid_Widget.WidgetTitle');	
		$this->db->from('Wid_TabWidget');	
		if($tab_id != ''){
			$this->db->where('Wid_TabWidget.TabId',$tab_id);
		}
		$this->db->order_by('Wid_TabWidget.SortingIndex','asc');
		$this->db->join('Wid_Tab','Wid_Tab.TabId = Wid_TabWidget.TabId');	
		$this->db->join('Wid_Widget','Wid_Widget.WidgetId = Wid_TabWidget.WidgetId');	
		$result = $this->db->get();
		return $data = $result->result_array();
	}
	
	function get_wid_by_tab_id($tab_id){
		
		$this->db->select('Wid_TabWidget.WidgetId');	
		$this->db->from('Wid_TabWidget');	
		$this->db->where('Wid_TabWidget.TabId',$tab_id);	
		$result = $this->db->get();
		return $data = $result->result_array();
	}

	function get_all_widgets($id){
		
		$this->db->select('WidgetId,WidgetTitle');	
		$this->db->from('Wid_Widget');
		$this->db->where_not_in('WidgetId',$id);
		$result = $this->db->get();
		return $data = $result->result_array();	
	}

	function get_max_sorting($tab_id){
		
		$this->db->select_max('SortingIndex');
		$this->db->from('Wid_TabWidget');
		$this->db->where('TabId',$tab_id);
		$query = $this->db->get();	
		return $query->result_array();	
	} 
 
	function get_widget_tab_map_for_user_type($id=""){
		
		$this->db->select('Wid_TabWidget.TabWidgetId,Wid_TabWidget.IsActive,Wid_Tab.TabTitle,Wid_Widget.WidgetTitle');	
		$this->db->from('Wid_TabWidget');	
		if($id != ''){
			$this->db->where_not_in('Wid_TabWidget.TabWidgetId',$id);
		}
		//$this->db->join('Wid_TabWidget','Wid_TabWidget.TabWidgetId = Wid_TabWidgetUserRoleType.TabWidgetId');	
		$this->db->join('Wid_Tab','Wid_Tab.TabId = Wid_TabWidget.TabId');	
		$this->db->join('Wid_Widget','Wid_Widget.WidgetId = Wid_TabWidget.WidgetId');	
		$result = $this->db->get();
		return $data = $result->result_array();
	}
	
	function get_widget_tab_map_user_role($user_type_id){
		
		$this->db->select('Wid_TabWidgetUserRoleType.TabWidgetUserRoleTypeId,Wid_Tab.TabTitle,Wid_Widget.WidgetTitle');	
		$this->db->from('Wid_TabWidgetUserRoleType');	
		$this->db->where('Wid_TabWidgetUserRoleType.UserRoleTypeId',$user_type_id);
		$this->db->join('Wid_TabWidget','Wid_TabWidget.TabWidgetId = Wid_TabWidgetUserRoleType.TabWidgetId');	
		$this->db->join('Wid_Tab','Wid_Tab.TabId = Wid_TabWidget.TabId');	
		$this->db->join('Wid_Widget','Wid_Widget.WidgetId = Wid_TabWidget.WidgetId');	
		$result = $this->db->get();
		return $data = $result->result_array();
	}
	
	function get_area_by_role_type($user_role_type){
		
		$q = 'Select
				Distinct Wid_Area.AreaId, Wid_Area.AreaName
				from Wid_Area
				Join Wid_Tab On Wid_Area.AreaId = Wid_Tab.AreaId
				Join Wid_TabWidget On Wid_TabWidget.TabId = Wid_Tab.TabId
				Join Wid_TabWidgetUserRoleType On Wid_TabWidgetUserRoleType.TabWidgetId = Wid_TabWidget.TabWidgetId
				Join UserRoleType On UserRoleType.UserRoleTypeId = Wid_TabWidgetUserRoleType.UserRoleTypeId
				where Wid_Tab.IsActive = 1 and Wid_TabWidget.IsActive = 1 and UserRoleType.UserRoleTypeId = '.$user_role_type;
		$result = $this->db->query($q);
		return $data = $result->result_array();
	}
	
	function get_tabs_by_role_type_area_id($user_role_type, $area_id){
		
		$q = 'Select
				Distinct Wid_Tab.TabId, Wid_Tab.TabTitle, Wid_Tab.IconImage , Wid_Tab.IconColor, Wid_Tab.SortingIndex
				from Wid_Tab
				Join Wid_TabWidget On Wid_TabWidget.TabId = Wid_Tab.TabId
				Join Wid_TabWidgetUserRoleType On Wid_TabWidgetUserRoleType.TabWidgetId = Wid_TabWidget.TabWidgetId
				Join UserRoleType On UserRoleType.UserRoleTypeId = Wid_TabWidgetUserRoleType.UserRoleTypeId
				where Wid_Tab.IsActive = 1 and Wid_TabWidget.IsActive = 1 and UserRoleType.UserRoleTypeId = '.$user_role_type.' and Wid_Tab.AreaId = '.$area_id.' order by Wid_Tab.SortingIndex ASC';
				$result = $this->db->query($q);
				return $data = $result->result_array();
	}
	
	function get_widgets_by_role_type_tab_id($user_role_type, $tab_id){
		
		$q = 'Select
				Wid_Widget.WidgetTitle, Wid_Widget.WidgetIcon, Wid_Widget.InfoText, Wid_Widget.SPName, Wid_Widget.ClickURL,
				Wid_ColorClassMaster.CSSClass, Wid_ColorClassMaster.CssCode,
				Wid_WidgetType.WidgetType, Wid_HTMLTemplate.HTMLTemplateTitle, Wid_HTMLTemplate.HTMLTemplate
				from Wid_Widget
				Join Wid_TabWidget On Wid_TabWidget.WidgetId = Wid_Widget.WidgetId
				Join Wid_TabWidgetUserRoleType On Wid_TabWidgetUserRoleType.TabWidgetId = Wid_TabWidget.TabWidgetId
				Join UserRoleType On UserRoleType.UserRoleTypeId = Wid_TabWidgetUserRoleType.UserRoleTypeId
				Join Wid_ColorClassMaster On Wid_ColorClassMaster.ColorClassId = Wid_Widget.ColorClassId
				Join Wid_WidgetType On Wid_WidgetType.WidgetTypeId = Wid_Widget.WidgetTypeId
				Join Wid_HTMLTemplate On Wid_HTMLTemplate.HTMLTemplateId = Wid_WidgetType.HTMLTemplateId
				where Wid_TabWidget.IsActive = 1 and Wid_WidgetType.IsActive = 1 and Wid_HTMLTemplate.IsActive = 1 and UserRoleType.UserRoleTypeId = '.$user_role_type.' and Wid_TabWidget.TabId = '.$tab_id.'
				order by Wid_TabWidget.SortingIndex ASC';
				$result = $this->db->query($q);
				return $data = $result->result_array();
	}
	
	
	function get_active_html_templates(){
		
			$this->db->select('HTMLTemplateId,HTMLTemplateTitle,HTMLTemplate');
			$this->db->from('Wid_HTMLTemplate');
			$this->db->where('IsActive',1);
			$result = $this->db->get();
			return $data = $result->result_array();
	}
	
}
