<?php
Class Common_model extends CI_Model
{
     function __construct(){
        parent::__construct();
    }
   
 
	/* getting all records from a particular table */
		function model_get_all_records($tableName, $id=""){	
			(string)$tableName = $tableName;
			if(!empty($id)){
				$this->db->where_not_in('UserId',$id);
			 }
			$this->db->from($tableName);			
			$query = $this->db->get();
			return $query->result_array();	
		}
	/* getting all records from a particular table order by column */	
		function model_get_all_records_orderby_col($tableName, $order_by_col){	
			(string)$tableName = $tableName;		
			$this->db->order_by($order_by_col);	 
			$result = $this->db->get($tableName);
			return $data = $result->result_array();	
			 
			 
		}
		function getRecordData($table,$column_array=array(),$order_by=array(),$excludeIds=array(),$select_column="",$join_array=array()){	
		    if($select_column != ""){
				$this->db->select($select_column);
			}
			$this->db->from($table);
			if(is_array($join_array)){
				foreach($join_array as $val){
					$this->db->join($val['table1'],$val['table1Val'].'='.$val['table2Val'],$val['joinType']);
				}
			}
			if(is_array($column_array)){
				foreach($column_array as $key=>$val){
					$this->db->where($key,$val);
				}
			}
			if(is_array($excludeIds)){
				if(!empty($excludeIds)){
					$this->db->where_not_in($excludeIds[0],$excludeIds[1]);
				}
			}
			if(is_array($order_by)){
				foreach($order_by as $key=>$val){
					$this->db->order_by($key,$val);
				}
			}
			$query = $this->db->get();	
			return $query->result_array();	
		}
	
	/* getting data against a particular column table */
		function model_get_by_col($tableName,$col,$colValue,$isActive=""){	
		    
			$this->db->from($tableName);
			if($isActive!=''){
				$this->db->where('IsActive',1);	
			}
            $this->db->where($col,$colValue);
			$query = $this->db->get();	
            
			return $query->result_array();	
		}
	/* //getting data against a particular column table */
	
	/* Insert data in table */
	function insert($data, $table){
		$this->db->insert($table,$data);
		$error = $this->db->error();
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = $this->db->insert_id();
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to insert data</div>';
		}
		 
		return $rData;
	}
	/* //Insert data in table */
	
    
    /* update data in table */
	function update($ID, $data, $colum_name, $table){
        (string)$colum_name = $colum_name;
        (int)$ID = $ID;
		$this->db->where($colum_name, $ID);
		$this->db->update($table, $data);
		$error = $this->db->error();
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = 1;
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to update data</div>';
		}
		 
		return $rData;
    }
	/* //update data in table */
	
	 function delete($ID, $colum_name, $table){
        (string)$colum_name = $colum_name;
        (int)$ID = $ID;    
		$this->db->where($colum_name, $ID);
		$this->db->delete($table);
		$error = $this->db->error(); 
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = '<div class="alert alert-success">Record deleted!!</div>';
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] Unable to delete data</div>';
		}
		 
		return $rData;
	}
	
	
	function compare_in_table($tableName,$col,$colValue, $second_col="", $second_col_val=""){	
		
		$this->db->where($col,$colValue);
		if($second_col != '' && $second_col_val != ''){
			$this->db->where($second_col,$second_col_val);
		}
		$this->db->select($col);
		$this->db->from($tableName);
		$query = $this->db->get();	
		
		return $query->result_array();	
	} 
	
	 
	function get_single_col_value($tableName,$col,$col_select,$colValue){	
		
		$this->db->where($col,$colValue);
		$this->db->select($col_select);
		$this->db->from($tableName);
		$query = $this->db->get();	
		
		return $query->result_array();	
	} 
	
	function get_max_sorting($table, $col){
		
		$this->db->select_max($col);
		$this->db->from($table);
		$query = $this->db->get();	
		return $query->result_array();	
	} 
	
	function model_get_validations_of_a_col($col_id){
		$this->db->select('dfb_form_col_validation.ControlValidationValue,dfb_column_validation.*');
		$this->db->join('dfb_column_validation', 'dfb_column_validation.ColumnValidationId = dfb_form_col_validation.ControlValidationId');
		$this->db->from('dfb_form_col_validation');
		$this->db->where('dfb_form_col_validation.FormColumnId',$col_id);		
		$query = $this->db->get();		
		return $query->result_array();
	}
	
		
}	
?>
