<?php
class Master_object extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('master_object/master_object_model');
		$this->load->model('common_model');
		$this->load->helper('common');
		is_logged_in();
		
	}
         
	function master_table_json(){
			
			 $this->load->view('super/master_json');
	}	  
	
	function generate_masters_json(){
		 
						$i = 0;
						$x = 0;
						$data = array();
						$masterIdsData = array();
						$MasterObjectRec = $this->common_model->model_get_all_records('MasterObject'); 
						$MasterIdsInfoRec = $this->master_object_model->get_master_ids_info(); 
						foreach($MasterObjectRec as $object_rec){
							$data[$i]['MasterObjectId'] =  $object_rec['MasterObjectId'];
							$data[$i]['MasterObject'] =  $object_rec['MasterObject'];
							$i++;
						}
						foreach($MasterIdsInfoRec as $master_ids_data_rec){
							$masterIdsData[$x]['TabId'] =  $master_ids_data_rec['TabId'];
							$masterIdsData[$x]['ObjectId'] =  $master_ids_data_rec['ObjectId'];
							$masterIdsData[$x]['TabLabel'] =  $master_ids_data_rec['TabLabel'];
							$masterIdsData[$x]['TabQuery'] =  $master_ids_data_rec['TabQuery'];
							$masterIdsData[$x]['SortingIndex'] =  $master_ids_data_rec['SortingIndex'];
							$x++;
						}
						$json_folder_name = $this->config->item('json_folder_name');
						if(!empty($data)){
							 
							$file_name = 'masterObject.json';
							$file_path = $json_folder_name.'/'.$file_name;
							$return_status = $this->_generate_json($data,$file_name,$file_path);
							if($return_status == 1){
								$this->session->unset_userdata('json_created'); 
								$this->session->set_userdata('json_created', 'JSON created successfuly.'); 
							}else{
								$this->session->unset_userdata('json_created'); 
								$this->session->set_userdata('json_created', 'Something went wrong.'); 	
							}
						} 
						if(!empty($masterIdsData)){
							 
							$file_name2 = 'masterIdsInfo.json';
							$file_path2 = $json_folder_name.'/'.$file_name2;
							$return_status2 = $this->_generate_json($masterIdsData,$file_name2,$file_path2);
							if($return_status2 == 1){
								$this->session->unset_userdata('json_created'); 
								$this->session->set_userdata('json_created', 'JSON created successfuly.'); 
							}else{
								$this->session->unset_userdata('json_created'); 
								$this->session->set_userdata('json_created', 'Something went wrong.'); 	
							}
						}
						redirect('master_object/master_table_json');
						
		}
		
	function _generate_json($rData,$file_name,$file_path){
		
				
			if(!empty($rData)){
				$data = json_encode($rData);
				if(file_exists($file_path)){
					unlink($file_path);					 
				} 
				$myfile = fopen($file_path, "w") or die("Unable to open file!");
				fwrite($myfile, $data);
				fclose($myfile);
				return true;	
				//$this->session->set_userdata('json_msg', $msg); 
			}else{
				return false;	
			}	
	}	
		
	 
}
?>	
