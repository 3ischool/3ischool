<?php
class AppFormSetup extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->load->model('appFormSetup/moduleType_model');
		$this->load->model('appFormSetup/module_model');
		$this->load->model('appFormSetup/appForm_model');
		$this->load->model('appFormSetup/language_model');
		$this->load->model('appFormSetup/appFormField_model');
		$this->load->model('appFormSetup/appFormLabel_model');
		$this->load->model('appFormSetup/app_version_model');
		$this->load->model('appFormSetup/app_version_detail_model');
		$this->load->model('appFormSetup/app_form_action_type_model');
		$this->load->model('appFormSetup/app_form_info_model');
		$this->load->model('appFormSetup/app_form_action_model');
		$this->load->model('appFormSetup/appFormTitle_model');
		$this->load->model('appFormSetup/user_role_type_app_form_model');
		$this->load->library('digipaging');
		$this->load->helper('common');
		is_logged_in();
	}

    function index(){
		
		redirect('appFormSetup/manage_moduleType','refresh');
		}
	
	
	/*
	 * 
	 * Module Type Section Start
	 * 
	 */
	 
	// Manage Module Type
	function manage_moduleType(){
		
		if($this->uri->segment(3)!=''){
			$current_page = $this->uri->segment(3);
		}	
		else{
			$current_page = 1;
		}  
		$config[] = array();
		$config['base_url'] = site_url().'/appFormSetup/manage_moduleType';
		$config['current_page'] = $current_page;
		$total_rows = $this->moduleType_model->record_count();
		$config['total_rows'] = $total_rows;
		$data['total_rows'] = $total_rows;
		$data['links'] = $this->digipaging->create_links($config);
		$page_size = $this->digipaging->page_size();
		$data['moduleType']=$this->moduleType_model->get_all_moduleType($page_size,$current_page);   //Display all the contries
		$this->load->view('super/moduleType/manage_moduleType',$data);	
		}
	
	//Add Module Type
	function add_moduleType(){
		
		$this->form_validation->set_rules('moduleType', 'Module Type', 'trim|required|max_length[20]');
		if($this->form_validation->run()==false){
			$this->load->view('super/moduleType/add_moduleType');
			}
		else{
			$moduleType = $this->input->post('moduleType');
			$data = array(
						'ModuleType' => $moduleType
					);
			$this->common_model->insert($data,'ModuleType');		
			redirect('appFormSetup/manage_moduleType','refresh');
			}	
		}	
		
		//Edit Module Type
		function edit_moduleType(){
		
			$id = base64d($this->uri->segment(3)); 
		    if(!is_numeric($id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="ModuleType";
		    $col="ModuleTypeId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			if(empty($data['result'])){
				echo "OOOPS!!! Error. Please contact administratorrr";
				exit; 
				}
			$this->form_validation->set_rules('moduleType', 'Module Type', 'trim|required|max_length[20]');
			if($this->form_validation->run()==false){
				$this->load->view('super/moduleType/add_moduleType',$data);
				}
			else{
				$moduleType = $this->input->post('moduleType');
				$data = array(
							'ModuleType' => $moduleType
						);
				$this->common_model->update($id,$data,'ModuleTypeId','ModuleType');		
				redirect('appFormSetup/manage_moduleType','refresh');
				}	
		}	
		
		// Delete Module Type
		public function delete_moduleType() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='ModuleTypeId';
		    $table ="ModuleType";
		    $this->common_model->delete($id,$colum_name,$table);
	        redirect('appFormSetup/manage_moduleType','refresh');	
		  }   
	        
		
	   /*
	 * 
	 *Module Type Section End
	 * 
	 */	
		
		
	/*
	 * 
	 * Module Section Start
	 * 
	 */
	 
	// Manage Module 
	function manage_module(){
		
		if($this->uri->segment(3)!=''){
			$current_page = $this->uri->segment(3);
		}	
		else{
			$current_page = 1;
		}	  
		$config[] = array();
		$config['base_url'] = site_url().'/appFormSetup/manage_module';
		$config['current_page'] = $current_page;
		$total_rows = $this->module_model->record_count();
		$config['total_rows'] = $total_rows;
		$data['total_rows'] = $total_rows;
		$data['links'] = $this->digipaging->create_links($config);
		$page_size = $this->digipaging->page_size();
		$data['module']=$this->module_model->get_all_module($page_size,$current_page);   //Display all the contries
		$data['moduleType'] = $this->common_model->model_get_all_records('ModuleType');
		$this->load->view('super/module/manage_module',$data);	
		}
	
	//Add Module 
	function add_module(){
		
		$data['moduleType'] = $this->common_model->model_get_all_records('ModuleType');
		//print_r($data['moduleType']);exit;
		if(empty($data['moduleType'])){
			echo "OOOPS!!! Error. Please contact administrator";
			exit; 
			}
		$this->form_validation->set_rules('module', 'Module', 'trim|required|max_length[20]');	
		$this->form_validation->set_rules('moduleType', 'Module Type', 'trim|required');
		$this->form_validation->set_rules('is_active', 'Status', 'trim|required');
		if($this->form_validation->run()==false){
			$this->load->view('super/module/add_module',$data);
			}
		else{
			$module = $this->input->post('module');
			$moduleType = $this->input->post('moduleType');
			$is_active = $this->input->post('is_active');
			$data = array(
						'Module' => $module,
						'ModuleTypeId' => $moduleType,
						'Status' => $is_active
					);
			$this->common_model->insert($data,'Module');		
			redirect('appFormSetup/manage_module','refresh');
			}	
		}	
		
		//Edit Module 
		function edit_module(){
		
			$id = base64d($this->uri->segment(3)); 
		    if(!is_numeric($id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="Module";
		    $col="ModuleId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			if(empty($data['result'])){
				echo "OOOPS!!! Error. Please contact administratorrr";
				exit; 
				}
			$data['moduleType'] = $this->common_model->model_get_all_records('ModuleType');	
			$this->form_validation->set_rules('module', 'Module', 'trim|required|max_length[20]');	
			$this->form_validation->set_rules('moduleType', 'Module Type', 'trim|required');
			$this->form_validation->set_rules('is_active', 'Status', 'trim|required');
			if($this->form_validation->run()==false){
				$this->load->view('super/module/add_module',$data);
				}
			else{
				$module = $this->input->post('module');
				$moduleType = $this->input->post('moduleType');
				$is_active = $this->input->post('is_active');
				$data = array(
							'Module' => $module,
							'ModuleTypeId' => $moduleType,
							'Status' => $is_active
						);
				$this->common_model->update($id,$data,'ModuleId','Module');		
				redirect('appFormSetup/manage_module','refresh');
				}	
		}	
		
		// Delete Module 
		public function delete_module() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='ModuleId';
		    $table ="Module";
		    $this->common_model->delete($id,$colum_name,$table);
	        redirect('appFormSetup/manage_module','refresh');	
		  }   
	        
		
	   /*
	 * 
	 * Module Section End
	 * 
	 */		
		
	/*
	 * 
	 * App Form Section Start
	 * 
	 */
	 
	// Manage App Form 
	function manage_appForm(){
		
		$data['appForm'] = $this->appForm_model->get_all_appForm_by_id(0);
		$this->load->view('super/appForm/manage_appForm',$data);	
		}
	
	//Add App Form 
	function add_appForm(){
		
		$data['module'] = $this->common_model->model_get_all_records('Module');
		$data['navigation_item'] = $this->common_model->model_get_all_records('NavigationItem');
		$data['app_form_rec'] = $this->common_model->model_get_all_records('AppForm');  
		if(empty($data['module'])){
			echo "OOOPS!!! Error. Please contact administrator";
			exit; 
			}
		$this->form_validation->set_rules('appForm', 'App Form', 'trim|required|max_length[30]');	
		$this->form_validation->set_rules('moduleId', 'Module Id', 'trim|required');
		$this->form_validation->set_rules('navigation_item', 'Navigation Item', 'trim');
		//$this->form_validation->set_rules('appFormTitle', 'App Form Title', 'trim|required|max_length[30]');
		if($this->form_validation->run()==false){
			$this->load->view('super/appForm/add_appForm',$data);
			}
		else{
			$appForm = $this->input->post('appForm');
			$moduleId = $this->input->post('moduleId');
			$navigation_item = $this->input->post('navigation_item');
			$app_form_parent = $this->input->post('app_form_parent');
			//$appFormTitle = $this->input->post('appFormTitle');
			$data = array(
						'AppForm' => $appForm,
						'ModuleId' => $moduleId,
						'NavigationItemId' => $navigation_item,
						'AppFormParentId' => $app_form_parent
					);
			$this->common_model->insert($data,'AppForm');		
			redirect('appFormSetup/manage_appForm','refresh');
			}	
		}	
		
		//Edit App Form 
		function edit_appForm(){
		
			$id = base64d($this->uri->segment(3)); 
		    if(!is_numeric($id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="AppForm";
		    $col="AppFormId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			if(empty($data['result'])){
				echo "OOOPS!!! Error. Please contact administratorrr";
				exit; 
				}
			$data['module'] = $this->common_model->model_get_all_records('Module');	
			$data['navigation_item'] = $this->common_model->model_get_all_records('NavigationItem');
			$data['app_form_rec'] = $this->common_model->model_get_all_records('AppForm');  
			$this->form_validation->set_rules('appForm', 'App Form', 'trim|required|max_length[30]');	
			$this->form_validation->set_rules('moduleId', 'Module Id', 'trim|required');
			$this->form_validation->set_rules('navigation_item', 'Navigation Item', 'trim');
			//$this->form_validation->set_rules('appFormTitle', 'App Form Title', 'trim|required|max_length[30]');
			if($this->form_validation->run()==false){
					$this->load->view('super/appForm/add_appForm',$data);
					}
			else{
				$appForm = $this->input->post('appForm');
				$moduleId = $this->input->post('moduleId');
				$navigation_item = $this->input->post('navigation_item');
				$app_form_parent = $this->input->post('app_form_parent');
				//$appFormTitle = $this->input->post('appFormTitle');
				$data = array(
							'AppForm' => $appForm,
							'ModuleId' => $moduleId,
							'NavigationItemId' => $navigation_item,
							'AppFormParentId' => $app_form_parent
						);
				$this->common_model->update($id,$data,'AppFormId','AppForm');		
				redirect('appFormSetup/manage_appForm','refresh');
				}	
		}	
		
		// Delete App Form 
		public function delete_appForm() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='AppFormId';
		    $table ="AppForm";
		    $AppFormField = $this->common_model->compare_in_table('AppFormField','AppFormId',$id);
		    $AppFormTitle = $this->common_model->compare_in_table('AppFormTitle','AppFormId',$id);
		    if(empty($AppFormField) && empty($AppFormTitle)){
				$this->common_model->delete($id,$colum_name,$table);
			}
			redirect('appFormSetup/manage_appForm','refresh');	
		  }   
	        
		
	   /*
	 * 
	 * App Form Section End
	 * 
	 */		
	
	/*
	 * 
	 * App Form Info Section Start
	 * 
	 */
	 
	// Manage App Form Info
	function manage_app_form_info(){
		
		if($this->uri->segment(3)!=''){
			$current_page = $this->uri->segment(3);
		}	
		else{
			$current_page = 1;
		}	  
		$config[] = array();
		$config['base_url'] = site_url().'/appFormSetup/manage_app_form_info';
		$config['current_page'] = $current_page;
		$total_rows = $this->app_form_info_model->record_count();
		$config['total_rows'] = $total_rows;
		$data['total_rows'] = $total_rows;
		$data['links'] = $this->digipaging->create_links($config);
		$page_size = $this->digipaging->page_size();
		$data['app_form_info']=$this->app_form_info_model->get_all_app_form_info($page_size,$current_page);   //Display all the contries
		$data['app_form'] = $this->common_model->model_get_all_records('AppForm');
		$this->load->view('super/app_form_info/manage_app_form_info',$data);	
		}
	
	//Add App Form Info 
	function add_app_form_info(){
		
		$data['app_form'] = $this->common_model->model_get_all_records_orderby_col('AppForm','AppForm');
		if(empty($data['app_form'])){
			echo "OOOPS!!! Error. Please contact administrator";
			exit; 
			}
		$this->form_validation->set_rules('form_id', 'Form', 'trim|required');	
		$this->form_validation->set_rules('form_use', 'Form Use', 'trim|required');
		$this->form_validation->set_rules('form_note', 'Form Note', 'trim|required');
		if($this->form_validation->run()==false){
			$this->load->view('super/app_form_info/add_app_form_info',$data);
			}
		else{
			$form_id = $this->input->post('form_id');
			$form_use = $this->input->post('form_use');
			$form_note = $this->input->post('form_note');
			$data = array(
							'AppFormId' => $form_id,
							'AppFormUse' => $form_use,
							'AppFormNote' => $form_note
						);
			$this->common_model->insert($data,'AppFormInfo');		
			redirect('appFormSetup/manage_app_form_info','refresh');
			}	
		}	
		
		//Edit App Form Info 
		function edit_app_form_info(){
		
			$id = base64d($this->uri->segment(3)); 
		    if(!is_numeric($id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="AppFormInfo";
		    $col="AppFormInfoId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			if(empty($data['result'])){
				echo "OOOPS!!! Error. Please contact administratorrr";
				exit; 
				}
			$data['app_form'] = $this->common_model->model_get_all_records_orderby_col('AppForm','AppForm');	
			$this->form_validation->set_rules('form_id', 'Form', 'trim|required');	
			$this->form_validation->set_rules('form_use', 'Form Use', 'trim|required');
			$this->form_validation->set_rules('form_note', 'Form Note', 'trim|required');
			if($this->form_validation->run()==false){
				$this->load->view('super/app_form_info/add_app_form_info',$data);
				}
			else{
				$form_id = $this->input->post('form_id');
				$form_use = $this->input->post('form_use');
				$form_note = $this->input->post('form_note');
				$data = array(
								'AppFormId' => $form_id,
								'AppFormUse' => $form_use,
								'AppFormNote' => $form_note
							);
				$this->common_model->update($id,$data,'AppFormInfoId','AppFormInfo');		
				redirect('appFormSetup/manage_app_form_info','refresh');
				}	
		}	
		
		// Delete App Form Info
		public function delete_app_form_info() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='AppFormInfoId';
		    $table ="AppFormInfo";
		    $this->common_model->delete($id,$colum_name,$table);
	        redirect('appFormSetup/manage_app_form_info','refresh');	
		  }   
	        
		
	   /*
	 * 
	 * App Form Info Section End
	 * 
	 */	
	
	
	
	
	 /*
	 * 
	 * Language Section Start
	 * 
	 */
	 
	// Manage Language
	function manage_language(){
		
		if($this->uri->segment(3)!=''){
			$current_page = $this->uri->segment(3);
		}	
		else{
			$current_page = 1;
		}	  
		$config[] = array();
		$config['base_url'] = site_url().'/appFormSetup/manage_language';
		$config['current_page'] = $current_page;
		$total_rows = $this->language_model->record_count();
		$config['total_rows'] = $total_rows;
		$data['total_rows'] = $total_rows;
		$data['links'] = $this->digipaging->create_links($config);
		$page_size = $this->digipaging->page_size();
		$data['language']=$this->language_model->get_all_language($page_size,$current_page);   //Display all the contries
		$this->load->view('super/language/manage_language',$data);	
		}
	
	//Add Language
	function add_language(){
		
		$this->form_validation->set_rules('language', 'Language', 'trim|required|max_length[20]');
		if($this->form_validation->run()==false){
			$this->load->view('super/language/add_language');
			}
		else{
			$language = $this->input->post('language');
			$data = array(
						'Language' => $language
					);
			$this->common_model->insert($data,'Language');		
			redirect('appFormSetup/manage_language','refresh');
			}	
		}	
		
		//Edit Language
		function edit_language(){
		
			$id = base64d($this->uri->segment(3)); 
		    if(!is_numeric($id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="Language";
		    $col="LanguageId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			if(empty($data['result'])){
				echo "OOOPS!!! Error. Please contact administratorrr";
				exit; 
				}
			$this->form_validation->set_rules('language', 'Language', 'trim|required|max_length[20]');
			if($this->form_validation->run()==false){
				$this->load->view('super/language/add_language',$data);
				}
			else{
				$language = $this->input->post('language');
				$data = array(
							'Language' => $language
						);
				$this->common_model->update($id,$data,'LanguageId','Language');		
				redirect('appFormSetup/manage_language','refresh');
				}	
		}	
		
		// Delete Language
		public function delete_language() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='LanguageId';
		    $table ="Language";
		    $this->common_model->delete($id,$colum_name,$table);
	        redirect('appFormSetup/manage_language','refresh');	
		  }   
	        
		
	   /*
	 * 
	 * Language Section End
	 * 
	 */	
		
	/*
	 * 
	 * App Form Field Section Start
	 * 
	 */
	 
	// Manage App Form Field
	function manage_appFormField(){
		
		$data['appFormField'] = $this->common_model->model_get_all_records('AppFormField');
		$data['appForm'] = $this->common_model->model_get_all_records('AppForm');
		$this->load->view('super/appFormField/manage_appFormField',$data);	
		}
	
	//Add App Form Field
	function add_appFormField(){
		
		$data['appForm'] = $this->common_model->model_get_all_records_orderby_col('AppForm','AppForm');
		if(empty($data['appForm'])){
			echo "OOOPS!!! Error. Please contact administrator";
			exit; 
			}
		$this->form_validation->set_rules('fieldName', 'Field Name', 'trim|required|max_length[25]');	
		$this->form_validation->set_rules('appFormId', 'App Form', 'trim|required');
		if($this->form_validation->run()==false){
			$this->load->view('super/appFormField/add_appFormField',$data);
			}
		else{
			$fieldName = $this->input->post('fieldName');
			$appFormId = $this->input->post('appFormId');
			$data = array(
						'FieldName' => $fieldName,
						'AppFormId' => $appFormId
					);
			$this->common_model->insert($data,'AppFormField');		
			redirect('appFormSetup/manage_appFormField','refresh');
			}	
		}	
		
		//Edit App Form Field
		function edit_appFormField(){
		
			$id = base64d($this->uri->segment(3)); 
		    if(!is_numeric($id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="AppFormField";
		    $col="AppFormFieldId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			if(empty($data['result'])){
				echo "OOOPS!!! Error. Please contact administratorrr";
				exit; 
				}
			$data['appForm'] = $this->common_model->model_get_all_records_orderby_col('AppForm','AppForm');	
			$this->form_validation->set_rules('fieldName', 'Field Name', 'trim|required|max_length[25]');	
			$this->form_validation->set_rules('appFormId', 'App Form', 'trim|required');
			if($this->form_validation->run()==false){
						$this->load->view('super/appFormField/add_appFormField',$data);
						}
			else{
				$fieldName = $this->input->post('fieldName');
				$appFormId = $this->input->post('appFormId');
				$data = array(
							'FieldName' => $fieldName,
							'AppFormId' => $appFormId
						);
				$this->common_model->update($id,$data,'AppFormFieldId','AppFormField');		
				redirect('appFormSetup/manage_appFormField','refresh');
				}	
		}	
		
		// Delete App Form Field 
		public function delete_appFormField() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='AppFormFieldId';
		    $table ="AppFormField";
		    $AppFormLabel = $this->common_model->compare_in_table('AppFormLabel','AppFormFieldId',$id);
		    if(empty($AppFormLabel)){
				$this->common_model->delete($id,$colum_name,$table);
			}
	        redirect('appFormSetup/manage_appFormField','refresh');	
		  }   
	        
		
	   /*
	 * 
	 * App Form Field Section End
	 * 
	 */		
				
	/*
	 * 
	 * App Form Label Section Start
	 * 
	 */
	 
	// Manage App Form Label
	function manage_appFormLabel(){
		
		$language_id = $this->session->userdata('languageId');
		$data['language_id'] = $language_id;
		
		$data['language'] = $this->common_model->model_get_all_records('Language');
		$data['appFormLabel'] = $this->appFormLabel_model->get_all_records($language_id);
		$this->load->view('super/appFormLabel/manage_appFormLabel',$data);	
		}
	
	function set_appform_label_filters(){
		
			$languageId = $this->uri->segment(3);
			if($languageId != ''){
					$this->session->unset_userdata('languageId');
					$this->session->set_userdata('languageId', base64d($languageId));
			}else{
				$this->session->unset_userdata('languageId');
				}
			redirect('appFormSetup/manage_appFormLabel','refresh');
	}
	
	//Add App Form Label
	function add_appFormLabel(){
		
		$data['appFormField'] = $this->common_model->model_get_all_records_orderby_col('AppFormField','FieldName');
		$data['language'] = $this->common_model->model_get_all_records('Language');
		if(empty($data['appFormField']) ||  empty($data['language'])){
			echo "OOOPS!!! Error. Please contact administrator";
			exit; 
			}
		$this->form_validation->set_rules('appFormLabel', 'App Form Label', 'trim|required|max_length[100]');	
		$this->form_validation->set_rules('appFormFieldId', 'App Form Field', 'trim|required');
		$this->form_validation->set_rules('languageId', 'Language', 'trim|required');
		$this->form_validation->set_rules('formFieldDescription', 'Form Field Help Text', 'trim|max_length[250]');	
		if($this->form_validation->run()==false){
			$this->load->view('super/appFormLabel/add_appFormLabel',$data);
			}
		else{
			$appFormLabel = $this->input->post('appFormLabel');
			$appFormFieldId = $this->input->post('appFormFieldId');
			$languageId = $this->input->post('languageId');
			$formFieldDescription = $this->input->post('formFieldDescription');
			$data = array(
						'FormFieldLabel' => $appFormLabel,
						'AppFormFieldId' => $appFormFieldId,
						'FormFieldHelpText' => $formFieldDescription,
						'LanguageId' => $languageId
					);
			//$this->common_model->insert($data,'AppFormLabel');	
			$this->appFormLabel_model->insert_app_form_label($appFormFieldId,$languageId,$appFormLabel,$formFieldDescription);		
			redirect('appFormSetup/manage_AppFormLabel','refresh');
			}	
		}	
		
		//Edit App Form Label
		function edit_appFormLabel(){
		
			$id = base64d($this->uri->segment(3)); 
		    if(!is_numeric($id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="AppFormLabel";
		    $col="AppFormLabelId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			if(empty($data['result'])){
				echo "OOOPS!!! Error. Please contact administratorrr";
				exit; 
				}
			$data['appFormField'] = $this->common_model->model_get_all_records_orderby_col('AppFormField','FieldName');
			$data['language'] = $this->common_model->model_get_all_records('Language');
			if(empty($data['appFormField']) ||  empty($data['language'])){
				echo "OOOPS!!! Error. Please contact administrator";
				exit; 
				}	
			$this->form_validation->set_rules('appFormLabel', 'App Form Label', 'trim|required|max_length[100]');	
			$this->form_validation->set_rules('appFormFieldId', 'App Form Field', 'trim|required');
			$this->form_validation->set_rules('languageId', 'Language', 'trim|required');
			$this->form_validation->set_rules('formFieldDescription', 'Form Field Help Text', 'trim|max_length[250]');	
			if($this->form_validation->run()==false){
				$this->load->view('super/appFormLabel/add_appFormLabel',$data);
				}
			else{
				$appFormLabel = $this->input->post('appFormLabel');
				$appFormFieldId = $this->input->post('appFormFieldId');
				$languageId = $this->input->post('languageId');
				$formFieldDescription = $this->input->post('formFieldDescription');
				$data = array(
							'FormFieldLabel' => $appFormLabel,
							'AppFormFieldId' => $appFormFieldId,
							'FormFieldHelpText' => $formFieldDescription,
							'LanguageId' => $languageId
						);
				$q = "FormFieldLabel = N'".$appFormLabel."',AppFormFieldId = ".$appFormFieldId.",LanguageId = ".$languageId.",FormFieldHelpText = '".$formFieldDescription."' where AppFormLabelId = ".$id;
				$rData = $this->appFormLabel_model->update_app_form_label($q);			
				//$this->common_model->update($id,$data,'AppFormLabelId','AppFormLabel');		
				redirect('appFormSetup/manage_AppFormLabel','refresh');
				}	
		}	
		
		// Delete App Form Label 
		public function delete_appFormLabel() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='AppFormLabelId';
		    $table ="AppFormLabel";
		    $this->common_model->delete($id,$colum_name,$table);
	        redirect('appFormSetup/manage_appFormLabel','refresh');	
		  }   
	    
	    function generate_app_form_label_json(){
		
			$lang_enc_id = $this->uri->segment(3);
			$lang_id = base64d($lang_enc_id);
			//$result = $this->common_model->model_get_by_col('AppFormLabel','LanguageId',$lang_id);
			$result = $this->appFormLabel_model->get_label_by_lang($lang_id);
			$x = 0;
			if(!empty($result)){
				foreach($result as $res){
					$data[$x]['FormFieldLabel'] = trim($res['FormFieldLabel']);
					$data[$x]['FormFieldHelpText'] = trim($res['FormFieldHelpText']);
					$data[$x]['AppFormFieldId'] = trim($res['AppFormFieldId']);
					$data[$x]['FieldName'] = trim($res['FieldName']);
					$data[$x]['AppFormId'] = trim($res['AppFormId']);
					
					$x++;
				}
				//$data = json_encode($data);
				//echo "<pre>";
				$language_rec = $this->common_model->model_get_by_col('Language','LanguageId',$lang_id);
				$abbr = strtolower($language_rec[0]['Abbr']);
				$file_name = 'app_form_label_'.$abbr.'.json';
				$json_folder_name = $this->config->item('json_folder_name');
				$file_path = $json_folder_name.'/'.$file_name;
				$return_status = generate_upload_json($data,$file_name,$file_path);
				if ($return_status == 1) {
						echo 'JSON file successfully generated';
				}else{
						echo $return_status;
				}
				
			}else{
				echo 'JSON cannot generated without data';
				}
			
	   }	      
		
	   /*
	 * 
	 * App Form Label Section End
	 * 
	 */		
	
	/*
	 * 
	 * App Form Title Section Start
	 * 
	 */
	 
	// Manage App Form Title
	function manage_appFormTitle(){
		 
		$app_form_id = $this->session->userdata('appFormId');
		$data['app_form_id'] = $app_form_id;
		
		$language_id = $this->session->userdata('languageId');
		$data['language_id'] = $language_id;
		
		$data['appForm'] = $this->common_model->model_get_all_records_orderby_col('AppForm','AppForm');
		$data['language'] = $this->common_model->model_get_all_records('Language');
		$data['appFormTitle'] = $this->appFormTitle_model->get_all_appFormTitle($app_form_id,$language_id);
		$this->load->view('super/appFormTitle/manage_appFormTitle',$data);	
		}
	
	function set_appform_title_filters(){
		
			$appFormId = $this->uri->segment(3);
			$languageId = $this->uri->segment(4);
			if($appFormId != 'unset'){
					$this->session->unset_userdata('appFormId');
					$this->session->set_userdata('appFormId', base64d($appFormId));
			}else{
				$this->session->unset_userdata('appFormId');
				}
			if($languageId != 'unset'){
					$this->session->unset_userdata('languageId');
					$this->session->set_userdata('languageId', base64d($languageId));
			}else{
				$this->session->unset_userdata('languageId');
				}
			redirect('appFormSetup/manage_AppFormTitle','refresh');
	}
	
	//Add App Form Title
	function add_appFormTitle(){
		
		$data['appForm'] = $this->common_model->model_get_all_records_orderby_col('AppForm','AppForm');
		$data['language'] = $this->common_model->model_get_all_records('Language');
		if(empty($data['appForm']) ||  empty($data['language'])){
			echo "OOOPS!!! Error. Please contact administrator";
			exit; 
			}
		$this->form_validation->set_rules('appFormTitle', 'App Form Title', 'trim|required|max_length[100]');	
		$this->form_validation->set_rules('appFormId', 'App Form', 'trim|required');
		$this->form_validation->set_rules('languageId', 'Language', 'trim|required');
		if($this->form_validation->run()==false){
			$this->load->view('super/appFormTitle/add_appFormTitle',$data);
			}
		else{
			$appFormTitle = $this->input->post('appFormTitle');
			$appFormId = $this->input->post('appFormId');
			$languageId = $this->input->post('languageId');
			/*$data = array(
						'AppFormTitle' => $appFormTitle,
						'AppFormId' => $appFormId,
						'LanguageId' => $languageId
					);*/
			$this->appFormTitle_model->insert_app_form_title($appFormId,$languageId,$appFormTitle);		
			//$this->common_model->insert($data,'AppFormTitle');		
			redirect('appFormSetup/manage_AppFormTitle','refresh');
			}	
		}	
		
		//Edit App Form Title
		function edit_appFormTitle(){
		
			$id = base64d($this->uri->segment(3)); 
		    if(!is_numeric($id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="AppFormTitle";
		    $col="AppFormTitleId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			if(empty($data['result'])){
				echo "OOOPS!!! Error. Please contact administratorrr";
				exit; 
				}
			$data['appForm'] = $this->common_model->model_get_all_records_orderby_col('AppForm','AppForm');
			$data['language'] = $this->common_model->model_get_all_records('Language');
			if(empty($data['appForm']) ||  empty($data['language'])){
				echo "OOOPS!!! Error. Please contact administrator";
				exit; 
				}	
			$this->form_validation->set_rules('appFormTitle', 'App Form Title', 'trim|required|max_length[100]');	
			$this->form_validation->set_rules('appFormId', 'App Form', 'trim|required');
			$this->form_validation->set_rules('languageId', 'Language', 'trim|required');
			if($this->form_validation->run()==false){
				$this->load->view('super/appFormTitle/add_appFormTitle',$data);
				}
			else{
				$appFormTitle = $this->input->post('appFormTitle');
				$appFormId = $this->input->post('appFormId');
				$languageId = $this->input->post('languageId');
				/*$data = array(
							'AppFormTitle' => $appFormTitle,
							'AppFormId' => $appFormId,
							'LanguageId' => $languageId
						);*/
				$q = "AppFormTitle = N'".$appFormTitle."',AppFormId = ".$appFormId.",LanguageId = ".$languageId." where AppFormTitleId = ".$id;
				$rData = $this->appFormTitle_model->update_app_form_title($q);			
				//$this->common_model->update($id,$data,'AppFormTitleId','AppFormTitle');		
				redirect('appFormSetup/manage_AppFormTitle','refresh');
				}	
		}	
		
		// Delete App Form Title 
		public function delete_appFormTitle() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='AppFormTitleId';
		    $table ="AppFormTitle";
		    $this->common_model->delete($id,$colum_name,$table);
	        redirect('appFormSetup/manage_appFormTitle','refresh');	
		  }   
	        
		
		
		function generate_app_form_title_json(){
		
			$lang_enc_id = $this->uri->segment(3);
			$lang_id = base64d($lang_enc_id);
			$result = $this->common_model->model_get_by_col('AppFormTitle','LanguageId',$lang_id);
			$x = 0;
			if(!empty($result)){
				foreach($result as $res){
					$data[$x]['AppFormTitleId'] = trim($res['AppFormTitleId']);
					$data[$x]['AppFormTitle'] = trim($res['AppFormTitle']);
					$data[$x]['AppFormId'] = trim($res['AppFormId']);
					$data[$x]['LanguageId'] = trim($res['LanguageId']);
					
					$x++;
				}
				
				//$data = json_encode($data);
				//echo "<pre>";
				$language_rec = $this->common_model->model_get_by_col('Language','LanguageId',$lang_id);
				$abbr = strtolower($language_rec[0]['Abbr']);
				$file_name = 'app_form_title_'.$abbr.'.json';
				$json_folder_name = $this->config->item('json_folder_name');
				$file_path = $json_folder_name.'/'.$file_name;
				$return_status = generate_upload_json($data,$file_name,$file_path);
				if ($return_status == 1) {
						echo 'JSON file successfully generated';
				}else{
						echo $return_status;
				}
				
			}else{
				echo 'JSON cannot generated without data';
				}
			
	}	      
		
	   /*
	 * 
	 * App Form Title Section End
	 * 
	 */		
	
	
	/*
	 * 
	 * App Version Section Start
	 * 
	 */
	 
	// Manage App Version
	function manage_app_version(){
		
		if($this->uri->segment(3)!=''){
			$current_page = $this->uri->segment(3);
		}	
		else{
			$current_page = 1;
		}  
		$config[] = array();
		$config['base_url'] = site_url().'/appFormSetup/manage_app_version';
		$config['current_page'] = $current_page;
		$total_rows = $this->app_version_model->record_count();
		$config['total_rows'] = $total_rows;
		$data['total_rows'] = $total_rows;
		$data['links'] = $this->digipaging->create_links($config);
		$page_size = $this->digipaging->page_size();
		$data['app_version']=$this->app_version_model->get_all_app_version($page_size,$current_page);   //Display all the contries
		$this->load->view('super/app_version/manage_app_version',$data);	
		}
	
	//Add App Version
	function add_app_version(){
		
		$this->form_validation->set_rules('version', 'App Version', 'trim|required|max_length[30]');
		$this->form_validation->set_rules('regd_date', 'Release Date', 'trim|required|max_length[30]');
		if($this->form_validation->run()==false){
			$this->load->view('super/app_version/add_app_version');
			}
		else{
			$version = $this->input->post('version');
			$regd_date = date("Y-m-d",strtotime($this->input->post('regd_date')));
			$data = array(
						'Version' => $version,
						'ReleaseDate' => $regd_date
					);
			$this->common_model->insert($data,'AppVersion');		
			redirect('appFormSetup/manage_app_version','refresh');
			}	
		}	
		
		//Edit App Version
		function edit_app_version(){
		
			$id = base64d($this->uri->segment(3)); 
		    if(!is_numeric($id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="AppVersion";
		    $col="VersionId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			if(empty($data['result'])){
				echo "OOOPS!!! Error. Please contact administratorrr";
				exit; 
				}
			$this->form_validation->set_rules('version', 'App Version', 'trim|required|max_length[30]');
			$this->form_validation->set_rules('regd_date', 'Release Date', 'trim|required|max_length[30]');
			if($this->form_validation->run()==false){
				$this->load->view('super/app_version/add_app_version',$data);
				}
			else{
				$version = $this->input->post('version');
				$regd_date = date("Y-m-d",strtotime($this->input->post('regd_date')));
				$data = array(
							'Version' => $version,
							'ReleaseDate' => $regd_date
						);
				$this->common_model->update($id,$data,'VersionId','AppVersion');		
				redirect('appFormSetup/manage_app_version','refresh');
				}	
		}	
		
		// Delete App Version
		public function delete_app_version() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='VersionId';
		    $table ="AppVersion";
		    $this->common_model->delete($id,$colum_name,$table);
	        redirect('appFormSetup/manage_app_version','refresh');	
		  }   
	        
		
	   /*
	 * 
	 *App Version Section End
	 * 
	 */	
	
	/*
	 * 
	 * App Version Detail Start
	 * 
	 */
	 
	// Manage App Version Detail 
	function manage_app_version_detail(){
		
		if($this->uri->segment(3)!=''){
			$current_page = $this->uri->segment(3);
		}	
		else{
			$current_page = 1;
		}	  
		$config[] = array();
		$config['base_url'] = site_url().'/appFormSetup/manage_app_version_detail';
		$config['current_page'] = $current_page;
		$total_rows = $this->app_version_detail_model->record_count();
		$config['total_rows'] = $total_rows;
		$data['total_rows'] = $total_rows;
		$data['links'] = $this->digipaging->create_links($config);
		$page_size = $this->digipaging->page_size();
		$data['app_version_detail']=$this->app_version_detail_model->get_all_app_version_detail($page_size,$current_page);   //Display all the contries
		$data['app_version'] = $this->common_model->model_get_all_records('AppVersion');
		$this->load->view('super/app_version_detail/manage_app_version_detail',$data);	
		}
	
	//Add App Version Detail 
	function add_app_version_detail(){
		
		$data['app_version'] = $this->common_model->model_get_all_records('AppVersion');
		if(empty($data['app_version'])){
			echo "OOOPS!!! Error. Please contact administrator";
			exit; 
			}
		$this->form_validation->set_rules('version_id', 'Version', 'trim|required');	
		$this->form_validation->set_rules('functionality', 'Functionality Change', 'trim|required');
		$this->form_validation->set_rules('coding', 'Coding Change', 'trim|required');
		if($this->form_validation->run()==false){
			$this->load->view('super/app_version_detail/add_app_version_detail',$data);
			}
		else{
			$version_id = $this->input->post('version_id');
			$functionality = $this->input->post('functionality');
			$coding = $this->input->post('coding');
			$data = array(
							'VersionId' => $version_id,
							'FunctionalityChange' => $functionality,
							'CodingChange' => $coding
						);
			$this->common_model->insert($data,'AppVersionDetail');		
			redirect('appFormSetup/manage_app_version_detail','refresh');
			}	
		}	
		
		//Edit App Version Detail 
		function edit_app_version_detail(){
		
			$id = base64d($this->uri->segment(3)); 
		    if(!is_numeric($id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="AppVersionDetail";
		    $col="VersionDetailId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			if(empty($data['result'])){
				echo "OOOPS!!! Error. Please contact administratorrr";
				exit; 
				}
			$data['app_version'] = $this->common_model->model_get_all_records('AppVersion');	
			$this->form_validation->set_rules('version_id', 'Version', 'trim|required');	
			$this->form_validation->set_rules('functionality', 'Functionality Change', 'trim|required');
			$this->form_validation->set_rules('coding', 'Coding Change', 'trim|required');
			if($this->form_validation->run()==false){
				$this->load->view('super/app_version_detail/add_app_version_detail',$data);
				}
			else{
				$version_id = $this->input->post('version_id');
				$functionality = $this->input->post('functionality');
				$coding = $this->input->post('coding');
				$data = array(
							'VersionId' => $version_id,
							'FunctionalityChange' => $functionality,
							'CodingChange' => $coding
						);
				$this->common_model->update($id,$data,'VersionDetailId','AppVersionDetail');		
				redirect('appFormSetup/manage_app_version_detail','refresh');
				}	
		}	
		
		// Delete App Version Detail 
		public function delete_app_version_detail() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='VersionDetailId';
		    $table ="AppVersionDetail";
		    $this->common_model->delete($id,$colum_name,$table);
	        redirect('appFormSetup/manage_app_version_detail','refresh');	
		  }   
	        
		
	   /*
	 * 
	 * App Version Detail  Section End
	 * 
	 */		
		
	
		
	
	/*
	 * 
	 * App Form Action Type Section Start
	 * 
	 */
	 
	// Manage App Form Action Type
	function manage_app_form_action_type(){
		
		if($this->uri->segment(3)!=''){
			$current_page = $this->uri->segment(3);
		}	
		else{
			$current_page = 1;
		}  
		$config[] = array();
		$config['base_url'] = site_url().'/appFormSetup/manage_app_form_action_type';
		$config['current_page'] = $current_page;
		$total_rows = $this->app_form_action_type_model->record_count();
		$config['total_rows'] = $total_rows;
		$data['total_rows'] = $total_rows;
		$data['links'] = $this->digipaging->create_links($config);
		$page_size = $this->digipaging->page_size();
		$data['result']=$this->app_form_action_type_model->get_all_app_form_action_type($page_size,$current_page);   //Display all the contries
		$this->load->view('super/app_form_action_type/manage_app_form_action_type',$data);	
		}
	
	//Add App Form Action Type
	function add_app_form_action_type(){
		
		$this->form_validation->set_rules('app_form_action_type', 'App Form Action Type', 'trim|required|max_length[20]');
		if($this->form_validation->run()==false){
			$this->load->view('super/app_form_action_type/add_app_form_action_type');
			}
		else{
			$app_form_action_type = $this->input->post('app_form_action_type');
			$data = array(
						'FormActionType' => $app_form_action_type
					);
			$this->common_model->insert($data,'AppFormActionType');		
			redirect('appFormSetup/manage_app_form_action_type','refresh');
			}	
		}	
		
		//Edit App Form Action Type
		function edit_app_form_action_type(){
		
			$id = base64d($this->uri->segment(3)); 
		    if(!is_numeric($id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="AppFormActionType";
		    $col="FormActionTypeId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			if(empty($data['result'])){
				echo "OOOPS!!! Error. Please contact administratorrr";
				exit; 
				}
			$this->form_validation->set_rules('app_form_action_type', 'App Form Action Type', 'trim|required|max_length[20]');
			if($this->form_validation->run()==false){
				$this->load->view('super/app_form_action_type/add_app_form_action_type',$data);
				}
			else{
				$app_form_action_type = $this->input->post('app_form_action_type');
				$data = array(
							'FormActionType' => $app_form_action_type
						);
				$this->common_model->update($id,$data,'FormActionTypeId','AppFormActionType');		
				redirect('appFormSetup/manage_app_form_action_type','refresh');
				}	
		}	
		
		// Delete App Form Action Type
		public function delete_app_form_action_type() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='FormActionTypeId';
		    $table ="AppFormActionType";
		    $this->common_model->delete($id,$colum_name,$table);
	        redirect('appFormSetup/manage_app_form_action_type','refresh');	
		  }   
	        
		
	   /*
	 * 
	 * App Form Action Type Section End
	 * 
	 */	
			
	/*
	 * 
	 * App Form Action Section Start
	 * 
	 */
	 
	// Manage App Form Action
	function manage_app_form_action(){
		
		$unique_action_name = '';
		$app_form_action_id = 0;	
		$data['result'] = array();
		if($this->uri->segment(3)!=''){
			$app_form_action_id = (int)base64d($this->uri->segment(3));
			if(!is_numeric($app_form_action_id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="AppFormAction";
		    $col="AppFormActionId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$app_form_action_id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			if(empty($data['result'])){
				echo "OOOPS!!! Error. Please contact administratorrr";
				exit; 
				}
			if(strtolower(trim($this->input->post('action_name'))) != strtolower($result[0]['ActionName'])){
				$unique_action_name = '|callback__is_unique_action_name';	
			}	
		}else{
			$unique_action_name = '|callback__is_unique_action_name';
			}	
		 	
		$data['app_form'] = $this->common_model->model_get_all_records_orderby_col('AppForm','AppForm');
		$data['app_form_action_type'] = $this->common_model->model_get_all_records_orderby_col('AppFormActionType','FormActionType');
		if(empty($data['app_form'])){
			echo "OOOPS!!! Error. Please contact administrator";
			exit; 
			}
		if(empty($data['app_form_action_type'])){
			echo "OOOPS!!! Error. Please contact administrator";
			exit; 
			}	  
		 
		$data['app_form_action']=$this->app_form_action_model->get_all_app_form_action();   //Display all the contries
		//$data['app_form'] = $this->common_model->model_get_all_records('AppForm');
		//$data['app_form_action_type'] = $this->common_model->model_get_all_records('AppFormActionType');
		$this->form_validation->set_rules('form_id', 'Form', 'trim|required');	
		$this->form_validation->set_rules('form_action_type', 'Form Action Type', 'trim|required');
		$this->form_validation->set_rules('action_name', 'Action Name', 'trim|required|max_length[25]'.$unique_action_name);
		$this->form_validation->set_rules('action_info', 'Action Info', 'trim|required|max_length[250]');
		if($this->form_validation->run()==false){
			$this->load->view('super/app_form_action/manage_app_form_action',$data);	
		}
		else{
			$form_id = $this->input->post('form_id');
			$form_action_type = $this->input->post('form_action_type');
			$action_name = $this->input->post('action_name');
			$action_info = $this->input->post('action_info');
			$data = array(
							'AppFormId' => $form_id,
							'FormActionTypeId' => $form_action_type,
							'ActionName' => $action_name,
							'ActionInfo' => $action_info
						);
			if($app_form_action_id>0){
				$this->common_model->update($app_form_action_id,$data,'AppFormActionId','AppFormAction');	
			}
			else{			
				$this->common_model->insert($data,'AppFormAction');		
			}
			redirect('appFormSetup/manage_app_form_action','refresh');
		}
	}
	
	//Add App Form Info 
	/*function add_app_form_action(){
		
		$data['app_form'] = $this->common_model->model_get_all_records('AppForm');
		$data['app_form_action_type'] = $this->common_model->model_get_all_records('AppFormActionType');
		if(empty($data['app_form'])){
			echo "OOOPS!!! Error. Please contact administrator";
			exit; 
			}
		if(empty($data['app_form_action_type'])){
			echo "OOOPS!!! Error. Please contact administrator";
			exit; 
			}	
		$this->form_validation->set_rules('form_id', 'Form', 'trim|required');	
		$this->form_validation->set_rules('form_action_type', 'Form Action Type', 'trim|required');
		$this->form_validation->set_rules('action_name', 'Action Name', 'trim|required|max_length[25]|callback__is_unique_action_name');
		$this->form_validation->set_rules('action_info', 'Action Info', 'trim|required|max_length[250]');
		if($this->form_validation->run()==false){
			$this->load->view('super/app_form_action/add_app_form_action',$data);
			}
		else{
			$form_id = $this->input->post('form_id');
			$form_action_type = $this->input->post('form_action_type');
			$action_name = $this->input->post('action_name');
			$action_info = $this->input->post('action_info');
			$data = array(
							'AppFormId' => $form_id,
							'FormActionTypeId' => $form_action_type,
							'ActionName' => $action_name,
							'ActionInfo' => $action_info
						);
			$this->common_model->insert($data,'AppFormAction');		
			redirect('appFormSetup/manage_app_form_action','refresh');
			}	
		}	
		
		//Edit App Form Action 
		function edit_app_form_action(){
		
			$id = base64d($this->uri->segment(3)); 
		    if(!is_numeric($id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="AppFormAction";
		    $col="AppFormActionId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			if(empty($data['result'])){
				echo "OOOPS!!! Error. Please contact administratorrr";
				exit; 
				}
			$unique_action_name = '';	
			if(strtolower(trim($this->input->post('action_name'))) != strtolower($result[0]['ActionName'])){
				$unique_action_name = '|callback__is_unique_action_name';	
			}	
			$data['app_form'] = $this->common_model->model_get_all_records('AppForm');	
			$data['app_form_action_type'] = $this->common_model->model_get_all_records('AppFormActionType');
			$this->form_validation->set_rules('form_id', 'Form', 'trim|required');	
			$this->form_validation->set_rules('form_action_type', 'Form Action Type', 'trim|required');
			$this->form_validation->set_rules('action_name', 'Action Name', 'trim|required|max_length[25]'.$unique_action_name);
			$this->form_validation->set_rules('action_info', 'Action Info', 'trim|required|max_length[250]');
			if($this->form_validation->run()==false){
				$this->load->view('super/app_form_action/add_app_form_action',$data);
				}
			else{
				$form_id = $this->input->post('form_id');
				$form_action_type = $this->input->post('form_action_type');
				$action_name = $this->input->post('action_name');
				$action_info = $this->input->post('action_info');
				$data = array(
								'AppFormId' => $form_id,
								'FormActionTypeId' => $form_action_type,
								'ActionName' => $action_name,
								'ActionInfo' => $action_info
							);
				$this->common_model->update($id,$data,'AppFormActionId','AppFormAction');		
				redirect('appFormSetup/manage_app_form_action','refresh');
				}	
		}	
		*/
		function _is_unique_action_name($str){
			
				$action_name = $str;
				if(trim($str) == ''){
					$this->form_validation->set_message('_is_unique_action_name','Action Name required');	
					return false;
				}
				else{
					$form_id = $this->input->post('form_id');
					$rData = $this->common_model->compare_in_table('AppFormAction','AppFormId',$form_id, 'ActionName', $action_name);
					if(empty($rData)){
						return true;
					}
					else{
						$this->form_validation->set_message('_is_unique_action_name','Action Name should be unique by App Form');
						return false;
						}
				}
		}
		
		// Delete App Form Action
		public function delete_app_form_action() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='AppFormActionId';
		    $table ="AppFormAction";
		    $this->common_model->delete($id,$colum_name,$table);
	        redirect('appFormSetup/manage_app_form_action','refresh');	
		  }   
	        
		
	   /*
	 * 
	 * App Form Action Section End
	 * 
	 */	
	
	
	function update_app_form_title(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id>0){
				$title = $this->input->post('titleval');
				$q = "AppFormTitle = N'".$title."' where AppFormTitleId = ".$id;
				$rData = $this->appFormTitle_model->update_app_form_title($q);		
				echo $rData;
			}
	}
	function update_app_form_label(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id>0){
				$title = $this->input->post('titleval');
				$q = "FormFieldLabel = N'".$title."' where AppFormLabelId = ".$id;
				$rData = $this->appFormLabel_model->update_app_form_label($q);		
				echo $rData;
			}
	}
	
	//	User role type app form
	function manage_user_role_type_app_form(){
		
		$data['user_role_type_app_form_detail'] = array();
		$data['app_form'] = array();
		$data['user_role_type_id'] = '';
		if($this->uri->segment(3) != ''){
			$user_role_type_id_enc = $this->uri->segment(3);
			$user_role_type_id = base64d($user_role_type_id_enc);
			$data['user_role_type_id'] = $user_role_type_id_enc; 
			$user_role_type_app_form_detail = $this->user_role_type_app_form_model->get_user_role_type_app_form($user_role_type_id);
			//print_r($user_role_type_app_form_detail);exit;
			$data['user_role_type_app_form_detail'] = $user_role_type_app_form_detail;
			$sd_count = count($data['user_role_type_app_form_detail']);
			for($i = 0;$i<$sd_count;$i++){
				$app_form_id_rec[$i] =  $user_role_type_app_form_detail[$i]['AppFormId'];
			}
			if(!empty($app_form_id_rec)){
				$data['app_form'] = $this->user_role_type_app_form_model->get_app_forms($app_form_id_rec);
			}
			else{
				$data['app_form'] = $this->user_role_type_app_form_model->get_app_forms();	
			}
		}
		$data['user_role_type'] = $this->common_model->model_get_all_records_orderby_col('UserRoleType','UserRoleType');
		$this->form_validation->set_rules('user_role_type','User Role Type','trim|required');
		$this->form_validation->set_rules('app_form[]','App Form','trim|required');
		if($this->form_validation->run() == false){
			$this->load->view('super/user_role_type_app_form/user_role_type_app_form',$data);
		}
		else{
			$user_role_type = base64d($this->input->post('user_role_type'));	
			$app_form = $this->input->post('app_form');	
			$y = count($app_form);
			$this->db->trans_start();
			for($x = 0;$x<$y;$x++){
				$max_sorting = $this->common_model->get_max_sorting('UserRoleTypeAppForm','SortingIndex');
				if(!empty($max_sorting)){
					$max_sorting1 = $max_sorting[0]['SortingIndex'] + 1;
				}
				else{
					$max_sorting1 = 1;	
				}
				$data = array(
						'UserRoleTypeId' => $user_role_type,
						'AppFormId' => base64d($app_form[$x]),
						'SortingIndex' => $max_sorting1,
						'IsActive' => 1
						);
				$rData = $this->common_model->insert($data,'UserRoleTypeAppForm');
			}
			$this->db->trans_complete();
			redirect('appFormSetup/manage_user_role_type_app_form/'.$user_role_type_id_enc);		
		}
	}
	
	function update_user_role_type_app_form_status(){
		
			$id = base64d($this->uri->segment(3));
			$is_active = $this->uri->segment(4);
			if(is_numeric($id)){
				if($is_active == 1){
					  $status = 0;
				}else{
					$status = 1;
				}
				
				$data = array('IsActive' => $status );		
				
				$result = $this->common_model->update($id, $data, 'UserRoleTypeAppFormId', 'UserRoleTypeAppForm');	
				if($result){
					$this->session->set_userdata('formComplete', 'ToastMessage("User role type app form Status has been updated.","success",true,"2000");');	
				
					echo json_encode(array("success" => true));
				}else{
					echo json_encode(array("success" => false));
				}
			}else{
				echo json_encode(array("success" => false));
			}
		} 
		
	function delete_user_role_type_app_form(){
		 
				$user_role_type_id_enc = $this->uri->segment(3);
				$id = base64d($this->uri->segment(4));
				$this->common_model->delete($id,'UserRoleTypeAppFormId','UserRoleTypeAppForm');
				$this->session->set_userdata('formComplete', 'ToastMessage("User role type app form has been deleted.","success",true,"2000");');	
				redirect('appFormSetup/manage_user_role_type_app_form/'.$user_role_type_id_enc);
		}

	public function update_sorting()
	{
		
		$table= $this->uri->segment(3);
		$colum_name= $this->uri->segment(4);
		
		$id = $_POST['ids'];
		//print_r($id);
		$count = count($id);
		for ($s = 0; $s < $count; $s++){
			$y = $s+1;
			$data = array('SortingIndex'=>$y);
			
			$result = $this->common_model->update($id[$s], $data, $colum_name, $table);   //query to Update data 			
			echo json_encode(array("success" => $result)); 
		}
		
	}  	
	
	function generate_user_role_type_app_form_json(){
			
			$data = array();
			$result = $this->common_model->model_get_all_records('UserRoleType');
			if(!empty($result)){
				$x = 0;
				foreach($result as $result){
					
						$data[$x]['UserRoleTypeId'] = $result['UserRoleTypeId'];
						$data[$x]['UserRoleType'] = $result['UserRoleType'];
						$appData = $this->_get_app_form_data($result['UserRoleTypeId'],0,0);						
						$data[$x]['AppFormData'] = $appData;
						$x++;
				}
				//echo "<pre>";
				//print_r($data); exit; 
				$file_name = 'user_role_type_app_form.json';
				$json_folder_name = $this->config->item('json_folder_name');
				$file_path = $json_folder_name.'/'.$file_name;
				$return_status = generate_upload_json($data,$file_name,$file_path);
				if ($return_status == 1) {
						echo 'JSON file successfully generated';
				}else{
						echo $return_status;
				}
				
			}else{
				echo 'JSON cannot generated without data';
				}
			
	   }
	   
	   
	   function _get_app_form_data($UserRoleTypeId,$parent_Id,$AppFormLevel){
		   $y = 0;	
		   $result2 = $this->user_role_type_app_form_model->get_user_role_type_app_form_by_parent($UserRoleTypeId, $parent_Id, 1);
						$appData = array();
						foreach($result2 as $result2){
							$appFormTitle_with_tilt_sign = $result2['AppForm'];
							$appFormTitle = '';
							if($appFormTitle_with_tilt_sign!=''){
								$appFormTitle_with_tilt_sign = explode('~',$appFormTitle_with_tilt_sign);
								$appFormTitle = $appFormTitle_with_tilt_sign[0];
							}
							$appData[$y]['AppFormId'] = $result2['AppFormId'];
							$appData[$y]['AppForm'] = $appFormTitle;
							$appData[$y]['AppFormParentId'] = $result2['AppFormParentId'];
							$appData[$y]['NavigationItemId'] = $result2['NavigationItemId'];
							$appData[$y]['NavigationItem'] = $result2['NavigationItem'];
							$appData[$y]['ControllerName'] = $result2['ControllerName'];
							$appData[$y]['ActionName'] = $result2['ActionName'];
							$appData[$y]['QueryString'] = $result2['QueryString'];
							
							$result3 = $this->user_role_type_app_form_model->get_form_action_and_type_by_app_form($result2['AppFormId']);
							$z = 0;
							$actionData = array();
							foreach($result3 as $result3){
								$actionData[$z]['AppFormActionId'] = $result3['AppFormActionId'];
								$actionData[$z]['ActionName'] = $result3['ActionName'];
								$actionData[$z]['FormActionTypeId'] = $result3['FormActionTypeId'];
								$actionData[$z]['FormActionType'] = $result3['FormActionType'];
								$z++;
							}
							$appData[$y]['Action'] = $actionData;
							$AppFormLevelNext = $AppFormLevel+1;
							$test = $this->_get_app_form_data($UserRoleTypeId,$result2['AppFormId'],$AppFormLevelNext);
							if($AppFormLevel==0){
								$appData[$y]['ChildAppForm'] = $test;
							}else{
								$appData[$y]['ChildAppForm'.$AppFormLevel] = $test;
							}
							$y++;
						}
		   return $appData;
	   }	 
		
}
?>	
