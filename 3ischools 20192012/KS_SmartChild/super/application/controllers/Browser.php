<?php
class Browser extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('common_model'); 
		$this->load->model('browser/browser_model'); 
		$this->load->helper('cookie');
	}
	
	function generate_json(){
		
		$data = array();
		$x = 0;
		$BrowserObject = $this->browser_model->model_get_all_browser_obj();
		foreach($BrowserObject as $rec){
			$data[$x]['BrowserObjectId'] = $rec['BrowserObjectId'];
			$data[$x]['BrowserObject'] = $rec['BrowserObject'];
			$data[$x]['MaxViewColCount'] = $rec['MaxViewColCount'];
			$data[$x]['MaxColCount'] = $rec['MaxColCount'];
			$data[$x]['MaxFilterColCount'] = $rec['MaxFilterColCount'];
			$data[$x]['StoredProcedureName'] = $rec['StoredProcedureName'];
			$boData = array();
			$BrowserObjectCol = $this->browser_model->get_browser_obj_col($rec['BrowserObjectId']);
			$y = 0;			 
			foreach($BrowserObjectCol as $col){
				$boData[$y]['BrowserObjectColId'] = $col['BrowserObjectColId'];	
				$boData[$y]['BrowserObjectCol'] = $col['BrowserObjectCol'];	
				$boData[$y]['CanView'] = $col['CanView'];	
				$boData[$y]['CanEdit'] = $col['CanEdit'];	
				$boData[$y]['CanFilter'] = $col['CanFilter'];	
				/*$TableCol = array();
				$table_col = array();
				$table_col1 = explode(',',$col['TableColId']);
				$table_col_count = count($table_col1);
				$TableColRec = array();
				for($i=0;$i<$table_col_count;$i++){
					$TableColRec = $this->common_model->model_get_by_col('TableCol','TableColId',$table_col1[$i]);
					$TableCol[$i]['TableColId'] = $TableColRec[0]['TableColId'];
					$TableCol[$i]['ActualColName'] = $TableColRec[0]['ActualColName'];
				}*/
				$BrowserObjectColRef = $this->browser_model->get_browser_obj_col_ref($col['BrowserObjectColId']);
				$z = 0;
				$BrowserObjectColRefRec = array();
				foreach($BrowserObjectColRef as $ref_rec){
					$BrowserObjectColRefRec[$z]['BrowserObjectColRefId'] = $ref_rec['BrowserObjectColRefId'];
					$BrowserObjectColRefRec[$z]['RefTable'] = $ref_rec['RefTable'];
					$BrowserObjectColRefRec[$z]['ControlType'] = $ref_rec['ControlType'];
					$z++;
				}
				
				$boData[$y]['BrowserObjectColRef'] = $BrowserObjectColRefRec;	
				//$boData[$y]['TableCol'] = $TableCol;
					
				$y++;
			}
			
			$data[$x]['BrowserObjectCol'] = $boData;
			$x++;
		}
		echo json_encode($data);	
	}
     
    /********************************* Browser Object Start  *********************************/ 
    function manage_browser_object(){
		
			$data['browser_obj_rec'] = $this->common_model->model_get_all_records('BrowserObject');
			$this->load->view('super/browser/browser_object/manage_browser_object',$data);
	}
    
    function add_browser_object(){
			$this->form_validation->set_rules('browser_object','Browser Object','trim|required|max_length[50]|is_unique[BrowserObject.BrowserObject]');
			$this->form_validation->set_rules('max_view_col_count','Max View Col Count','trim|required|numeric');
			$this->form_validation->set_rules('max_col_count','Max Col Count','trim|required|numeric');
			$this->form_validation->set_rules('max_filter_col_count','Max Filter Col Count','trim|required|numeric');
			if($this->form_validation->run() == false){
				$this->load->view('super/browser/browser_object/add_browser_object');
			}else{
				$browser_object = $this->input->post('browser_object');
				$max_view_col_count = $this->input->post('max_view_col_count');
				$max_col_count = $this->input->post('max_col_count');
				$max_filter_col_count = $this->input->post('max_filter_col_count');
				$data = array(
						'BrowserObject' => $browser_object,
						'MaxViewColCount' => $max_view_col_count,
						'MaxColCount' => $max_col_count,
						'MaxFilterColCount' => $max_filter_col_count,
						'IsActicve' => 1
						);
				$rData = $this->common_model->insert($data,'BrowserObject');	
				if($rData == 1){
					$this->session->set_userdata('msg','Browser Object added successfully.');	
				}else{
					$this->session->set_userdata('error',$rData);	
				}	
				if(isset($_POST['publish'])){
					redirect('browser/add_browser_object');	
				}else{
					redirect('browser/manage_browser_object');		
				}
			}
	}
	
    function edit_browser_object(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
				echo 'Please Contact Administrator.';
				exit;
			}
			$edit_rec = $this->common_model->model_get_by_col('BrowserObject','BrowserObjectId',$id);
			$data['edit_rec'] = $edit_rec;
			if(strtolower($edit_rec[0]['BrowserObject']) != strtolower($this->input->post('browser_object'))){
				$is_unique = '|is_unique[BrowserObject.BrowserObject]';
			}else{
				$is_unique = '';
				}
			$this->form_validation->set_rules('browser_object','Browser Object','trim|required|max_length[50]'.$is_unique);
			$this->form_validation->set_rules('max_view_col_count','Max View Col Count','trim|required|numeric');
			$this->form_validation->set_rules('max_col_count','Max Col Count','trim|required|numeric');
			$this->form_validation->set_rules('max_filter_col_count','Max Filter Col Count','trim|required|numeric');
			if($this->form_validation->run() == false){
				$this->load->view('super/browser/browser_object/add_browser_object',$data);
			}else{
				$browser_object = $this->input->post('browser_object');
				$max_view_col_count = $this->input->post('max_view_col_count');
				$max_col_count = $this->input->post('max_col_count');
				$max_filter_col_count = $this->input->post('max_filter_col_count');
				$data = array(
						'BrowserObject' => $browser_object,
						'MaxViewColCount' => $max_view_col_count,
						'MaxColCount' => $max_col_count,
						'MaxFilterColCount' => $max_filter_col_count
						);
				$rData = $this->common_model->update($id,$data,'BrowserObjectId','BrowserObject');	
				if($rData == 1){
					$this->session->set_userdata('msg','Browser Object updated successfully.');	
				}else{
					$this->session->set_userdata('error',$rData);	
				}	
				redirect('browser/manage_browser_object');		
				
			}
	}
	
	function delete_browser_object(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
				echo 'Please Contact Administrator.';
				exit;
			}
			$this->common_model->delete($id,'BrowserObjectId','BrowserObject');
			$this->session->set_userdata('msg','Browser Object deleted successfully.');	
			redirect('browser/manage_browser_object');	
	}
	
	function update_browser_object_status(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
				echo 'Please Contact Administrator.';
				exit;
			}
			$status = $this->uri->segment(4);
			if($status == 1){
				$is_active = 0;
			}else{
				$is_active = 1;	
			}
			$data = array('IsActicve' => $is_active);
			$this->common_model->update($id,$data,'BrowserObjectId','BrowserObject');
			$this->session->set_userdata('msg','Browser Object status updated successfully.');	
			redirect('browser/manage_browser_object');	
	}
    /********************************* Browser Object End  *********************************/ 
     
    /********************************* Browser Object Col Start  *********************************/ 
    function manage_browser_object_col(){
			$data['browser_obj_col_rec'] = $this->browser_model->get_all_browser_obj_col();
			$this->load->view('super/browser/browser_object_col/manage_browser_object_col',$data);
	}
    
    function add_browser_object_col(){
			
			$data['browser_obj_rec'] = $this->common_model->model_get_all_records('BrowserObject');
			$this->form_validation->set_rules('browser_object_col','Browser Object Column','trim|required|max_length[50]|is_unique[BrowserObjectCol.BrowserObjectCol]');
			$this->form_validation->set_rules('browser_object','Browser Object','trim|required|numeric');
			$this->form_validation->set_rules('can_view','Can View','trim|numeric');
			$this->form_validation->set_rules('can_edit','Can Edit','trim|numeric');
			$this->form_validation->set_rules('can_filter','Can Filter','trim|numeric');
			if($this->form_validation->run() == false){
				$this->load->view('super/browser/browser_object_col/add_browser_object_col',$data);
			}else{
				$browser_object_col = $this->input->post('browser_object_col');
				$browser_object = $this->input->post('browser_object');
				$can_view = $this->input->post('can_view');
				$can_edit = $this->input->post('can_edit');
				$can_filter = $this->input->post('can_filter');
				$data = array(
						'BrowserObjectCol' => $browser_object_col,
						'BrowserObjectId' => $browser_object,
						'CanView' => $can_view,
						'CanEdit' => $can_edit,
						'CanFilter' => $can_filter
						);
				$rData = $this->common_model->insert($data,'BrowserObjectCol');	
				if($rData == 1){
					$this->session->set_userdata('msg','Browser Object Column added successfully.');	
				}else{
					$this->session->set_userdata('error',$rData);	
				}	
				if(isset($_POST['publish'])){
					redirect('browser/add_browser_object_col');	
				}else{
					redirect('browser/manage_browser_object_col');		
				}
			}
	}
	
    function edit_browser_object_col(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
				echo 'Please Contact Administrator.';
				exit;
			}
			$edit_rec = $this->common_model->model_get_by_col('BrowserObjectCol','BrowserObjectColId',$id);
			$data['edit_rec'] = $edit_rec;
			if(strtolower($edit_rec[0]['BrowserObjectCol']) != strtolower($this->input->post('browser_object_col'))){
				$is_unique = '|is_unique[BrowserObjectCol.BrowserObjectCol]';
			}else{
				$is_unique = '';
				}
			$data['browser_obj_rec'] = $this->common_model->model_get_all_records('BrowserObject');
			$this->form_validation->set_rules('browser_object_col','Browser Object Column','trim|required|max_length[50]'.$is_unique);
			$this->form_validation->set_rules('browser_object','Browser Object','trim|required|numeric');
			$this->form_validation->set_rules('can_view','Can View','trim|numeric');
			$this->form_validation->set_rules('can_edit','Can Edit','trim|numeric');
			$this->form_validation->set_rules('can_filter','Can Filter','trim|numeric');
			if($this->form_validation->run() == false){
				$this->load->view('super/browser/browser_object_col/add_browser_object_col',$data);
			}else{
				$browser_object_col = $this->input->post('browser_object_col');
				$browser_object = $this->input->post('browser_object');
				$can_view = $this->input->post('can_view');
				$can_edit = $this->input->post('can_edit');
				$can_filter = $this->input->post('can_filter');
				$data = array(
						'BrowserObjectCol' => $browser_object_col,
						'BrowserObjectId' => $browser_object,
						'CanView' => $can_view,
						'CanEdit' => $can_edit,
						'CanFilter' => $can_filter
						);
				$rData = $this->common_model->update($id,$data,'BrowserObjectColId','BrowserObjectCol');	
				if($rData == 1){
					$this->session->set_userdata('msg','Browser Object Column updated successfully.');	
				}else{
					$this->session->set_userdata('error',$rData);	
				}	
				redirect('browser/manage_browser_object_col');		
				
			}
	}
	
	function delete_browser_object_col(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
				echo 'Please Contact Administrator.';
				exit;
			}
			$this->common_model->delete($id,'BrowserObjectColId','BrowserObjectCol');
			$this->session->set_userdata('msg','Browser Object Column deleted successfully.');	
			redirect('browser/manage_browser_object_col');	
	}
	
	 
    /********************************* Browser Object Col End  *********************************/ 
     
	/********************************* Browser Object Col Ref Start  *********************************/ 
    function manage_browser_object_col_ref(){
		
			$data['browser_obj_col_ref_rec'] = $this->browser_model->get_all_browser_object_col_ref();
			$this->load->view('super/browser/browser_object_col_ref/manage_browser_object_col_ref',$data);
	}
    
    function add_browser_object_col_ref(){
		
			$data['browser_obj_col_rec'] = $this->common_model->model_get_all_records('BrowserObjectCol');
			$data['table_rec'] = $this->common_model->model_get_all_records('TableMaster');
			$this->form_validation->set_rules('browser_object_col','Browser Object Col','trim|required|numeric');
			$this->form_validation->set_rules('ref_table','Ref Table','trim|required|max_length[50]');
			$this->form_validation->set_rules('RefTableCol','Ref Table','trim|required|max_length[100]');
			$this->form_validation->set_rules('control_type','Control Type','trim|required|max_length[50]');
			if($this->form_validation->run() == false){
				$this->load->view('super/browser/browser_object_col_ref/add_browser_object_col_ref',$data);
			}else{
				$browser_object_col = $this->input->post('browser_object_col');
				$ref_table = $this->input->post('ref_table');
				$control_type = $this->input->post('control_type');
				$RefTableCol = $this->input->post('RefTableCol');
				$data = array(
						'BrowserObjectColId' => $browser_object_col,
						'RefTable' => $ref_table,
						'ControlType' => $control_type,
						'RefTableCol' => $RefTableCol
						);
				$rData = $this->common_model->insert($data,'BrowserObjectColRef');	
				if($rData == 1){
					$this->session->set_userdata('msg','Browser Object Col Ref added successfully.');	
				}else{
					$this->session->set_userdata('error',$rData);	
				}	
				if(isset($_POST['publish'])){
					redirect('browser/add_browser_object_col_ref');	
				}else{
					redirect('browser/manage_browser_object_col_ref');		
				}
			}
	}
	
    function edit_browser_object_col_ref(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
				echo 'Please Contact Administrator.';
				exit;
			}
			$edit_rec = $this->common_model->model_get_by_col('BrowserObjectColRef','BrowserObjectColRefId',$id);
			$data['edit_rec'] = $edit_rec;
			$data['browser_obj_col_rec'] = $this->common_model->model_get_all_records('BrowserObjectCol');
			$data['table_rec'] = $this->common_model->model_get_all_records('TableMaster');
			$this->form_validation->set_rules('browser_object_col','Browser Object Col','trim|required|numeric');
			$this->form_validation->set_rules('ref_table','Ref Table','trim|required|max_length[50]');
			$this->form_validation->set_rules('RefTableCol','Ref Table','trim|required|max_length[100]');
			$this->form_validation->set_rules('control_type','Control Type','trim|required|max_length[50]');
			if($this->form_validation->run() == false){
				$this->load->view('super/browser/browser_object_col_ref/add_browser_object_col_ref',$data);
			}else{
				$browser_object_col = $this->input->post('browser_object_col');
				$ref_table = $this->input->post('ref_table');
				$control_type = $this->input->post('control_type');
				$RefTableCol = $this->input->post('RefTableCol');
				$data = array(
						'BrowserObjectColId' => $browser_object_col,
						'RefTable' => $ref_table,
						'ControlType' => $control_type,
						'RefTableCol' => $RefTableCol
						);
				$rData = $this->common_model->update($id,$data,'BrowserObjectColRefId','BrowserObjectColRef');	
				if($rData == 1){
					$this->session->set_userdata('msg','Browser Object Col Ref updated successfully.');	
				}else{
					$this->session->set_userdata('error',$rData);	
				}	
				redirect('browser/manage_browser_object_col_ref');		
				
			}
	}
	
	function delete_browser_object_col_ref(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
				echo 'Please Contact Administrator.';
				exit;
			}
			$this->common_model->delete($id,'BrowserObjectColRefId','BrowserObjectColRef');
			$this->session->set_userdata('msg','Browser Object Col Ref deleted successfully.');	
			redirect('browser/manage_browser_object_col_ref');	
	}
	
    /********************************* Browser Object Col Ref End  *********************************/ 
     	
}
?>	
