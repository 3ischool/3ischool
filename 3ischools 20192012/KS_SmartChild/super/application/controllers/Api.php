<?php
class Api extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		 
		$this->load->model('common_model'); 
		 
		 
	}
	
	 
	function _access_key(){
		$AccessKey = 'GtB123u-nhyuj-6yt5ad-poklj5y-uyt1';
		return $AccessKey;
	}
	
	function school_list_for_rfid_app(){  
		 
	    $submitted_key = '';
	    if(isset($_POST['auth_key'])){
			$submitted_key = $_POST['auth_key'];
		}
		$stat = "error";
		$message = "authentication error";
		$data = array();
		if($submitted_key==$this->_access_key()){
			 $this->load->model('school/school_model');
			 $result = $this->school_model->get_schools_with_db();
			 if(empty($result)){
				 $stat = 'success';
				 $message = 'No record found';
				 $data = array();
			 }
			 else{
				 $stat = 'success';
				 $message = 'success';	
				 $data =  $result;
			 }
		}
		$returnData = array();
		$returnData['status'] = $stat;
		$returnData['message'] = $message;
		$returnData['data'] = $data;
		header('Content-Type: application/json');
	    echo json_encode($returnData);
	}
	
	function get_navigation_item(){
		 
		$submitted_key = $_POST['auth_key'];
		$stat = "error";
		$message = "authentication error";
		$data = array('error'=>'');
		if($submitted_key==$this->_access_key()){
			 $result = $this->common_model->model_get_all_records('NavigationItem');
			 if(empty($result)){
				 $stat = 'success';
				 $message = 'No record found';
				 $data = array();
			 }
			 else{
				 $stat = 'success';
				 $message = 'success';	
				 $x = 0;
				 foreach($result as $rec){
					$data[$x]['NavigationItemId'] = $rec['NavigationItemId'];
					$data[$x]['NavigationItem'] = $rec['NavigationItem'];
					$data[$x]['ControllerName'] = $rec['ControllerName'];
					$data[$x]['ActionName'] = $rec['ActionName'];
					$data[$x]['QueryString'] = $rec['QueryString'];
					$data[$x]['NavigationIcon'] = $rec['NavigationIcon'];
					$data[$x]['NavItemTypeId'] = $rec['NavItemTypeId'];
					$x++;
				 }
			 }
		}
		$returnData = array();
		$returnData['status'] = $stat;
		$returnData['message'] = $message;
		$returnData['data'] = $data;
		header('Content-Type: application/json');
	    echo json_encode($returnData);
		
		 
	}
	
	
	function show_all_schools(){
		$this->load->model('school/school_model');
		$result = $this->school_model->get_schools_with_db();
		
		$t = array();
		$i = 0;
		foreach($result as $result){
			$t[$i]['s'] = base64_encode($result['Domain']);
			$t[$i]['d'] = base64_encode($result['DBName']);
			 
			$i++;
		}
		header('Content-Type: application/json');
	    echo json_encode($t);
	} 
	
	function get_supper_data(){
		$stat = "error";
		$message = "authentication error";
		$response = array();
		$submitted_key = $_POST['auth_key'];
		if($submitted_key==$this->_access_key()){
			$table = ""; 
			$column_array = array(); 
			$order_by = array(); 
			$excludeIds = array();
			$select_column = "";
			$join_array = array();
			
			$table = (string)$this->input->post('table');		
			$column_array = $this->input->post('column_array');	
			$order_by = $this->input->post('order_by');
			$excludeIds = $this->input->post('exclude_ids');
			$select_column = $this->input->post('select_column');
			$join_array = $this->input->post('join_array');
			
			if($table != ""){				
				$response = $this->common_model->getRecordData($table,$column_array,$order_by,$excludeIds,$select_column,$join_array);
				$stat = "success";
				$message = "";
			}else{
				$message = "Table name is required.";
			}
		}
		$returnData = array();
		$returnData['status'] = $stat;
		$returnData['message'] = $message;
		$returnData['data'] = $response;
		header('Content-Type: application/json');
	    echo json_encode($returnData);
	}
	    
		 
}
?>	
