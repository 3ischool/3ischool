<?php
class Support_user extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->load->model('supportUser/support_user_model');
		is_logged_in();
	}
	
	 /*
	  * 
	  *  Change Password
	  * 
	  */	
		
	function change_password(){
		
		$support_user_id = base64d($this->input->cookie('s_ui'));
		$this->form_validation->set_rules('old_password','Old Password','trim|required|callback__compare_old_password');
		$this->form_validation->set_rules('new_password','New Password','trim|required|min_length[8]');
		$this->form_validation->set_rules('confirm_password','Confirm Password','trim|required|callback__compare_confirm_password');
		if($this->form_validation->run() == false){
			$this->load->view('super/change_password');
		 }else{
			   $new_password = $this->input->post('new_password');
			   $data = array(
							'Password' => md5($new_password)
							);	
			   $this->common_model->update($support_user_id,$data,'Id','SuperUser');		
			   redirect('support_user/change_password','refresh');				
			 }
		}	
	
	function _compare_old_password($str){
		
		 if($str != ''){
			$support_user_id = base64d($this->input->cookie('s_ui'));
			$user = $this->common_model->model_get_by_col('SuperUser','Id',$support_user_id);     
			$user_pass = $user[0]['Password'];
			$old_pass = md5($str);
			if($user_pass != $old_pass){
				$this->form_validation->set_message('_compare_old_password','Invalid Password');
				return false;
				}else{
					return true;
					}
		  }
		}
		
	function _compare_confirm_password($str){
		
		$new_pass = $this->input->post('new_password');
		if($str != '' && $new_pass != ''){
			$new_pass = md5($new_pass);
			$confirm_pass = md5($str);
			if($new_pass != $confirm_pass){
				$this->form_validation->set_message('_compare_confirm_password','Invalid Confirm Password');
				return false;
				}else{
					return true;
					}
		  }			
		}	
		
		
}
?>	
