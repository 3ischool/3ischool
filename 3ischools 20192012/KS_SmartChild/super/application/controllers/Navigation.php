<?php
class Navigation extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->load->model('navigation/navigation_model');
		$this->load->model('navigation/navigation_item_model');
		$this->load->model('navigation/language_navigation_item_model');
		$this->load->model('navigation/navigation_placement_model');
		$this->load->library('digipaging');
		$this->load->helper('imageupload');
		$this->load->helper('common');
		is_logged_in();
		
	}
 
 function test(){
	$test = $this->common_model->model_get_all_records('UserRoleType');
	echo json_encode($test,true); 
	}
	/*
	 * 
	 * Navigation Item Section Start
	 * 
	 */
	 
	// Manage Navigation Item
	function manage_navigation_item(){
		
		$data['navigation_item_rec'] = $this->common_model->model_get_all_records('NavigationItem');
		$this->load->view('super/navigation_item/manage_navigation_item',$data);	
	}
	
	//Add Navigation Item
	function add_navigation_item(){
		
		$data['navigation_item_rec'] = $this->common_model->model_get_all_records('NavItemType');
		$this->form_validation->set_rules('navigation_item', 'Navigation Item', 'trim|required|max_length[50]');	
		$this->form_validation->set_rules('controller_name', 'Controller Name', 'trim|max_length[50]');
		$this->form_validation->set_rules('action_name', 'Action Name', 'trim|max_length[50]');
		$this->form_validation->set_rules('query_string', 'Query String', 'trim|max_length[250]');	
		$this->form_validation->set_rules('nav_item_type', 'Nav Item Type', 'trim|required');	
		if($this->form_validation->run()==false){
			$this->load->view('super/navigation_item/add_navigation_item',$data);
			}
		else{
			$navigation_item = $this->input->post('navigation_item');
			$controller_name = $this->input->post('controller_name');
			$action_name = $this->input->post('action_name');
			$query_string = $this->input->post('query_string');
			$nav_item_type = $this->input->post('nav_item_type');
			$image_name = $_FILES['userfile']['name'];
			if($image_name != ''){
				$path = './uploads/navigation_item_icon/';	//Image upload path
				$size = 2048;	//Image size
				$width = '';	//Image width
				$height = '';	//Image height
				
				$image = image_upload($path, $size,$width,$height,$width,$height);	//Image upload function
				//print_r($image);
				$err = ""; 
				if(!empty($image)){
					//foreach($image['entity'] as $files){
						if($image['entity']['status']=="error"){
							echo $err =  $err."Error : ".$image['entity']['message']."<br>";
						}
						 
					//}
				}
			}	
			$data = array(
						'NavigationItem' => $navigation_item,
						'ControllerName' => $controller_name,
						'ActionName' => $action_name,
						'QueryString' => $query_string,
						'NavItemTypeId' => $nav_item_type,
						'NavigationIcon' => $image_name
						 
					);
			$this->common_model->insert($data,'NavigationItem');	
			 
			if(isset($_POST['continue'])){
				redirect('navigation/add_navigation_item','refresh');
			}
			else{
				redirect('navigation/manage_navigation_item','refresh');
			}	
		}
	}	
		
		//Edit Navigation Item
		function edit_navigation_item(){
		
			$id = base64d($this->uri->segment(3)); 
		    if(!is_numeric($id)){
				echo "OOOPS!!! Error. Please contact administrator";
				exit;   
			   } 
		    $tableName="NavigationItem";
		    $col="NavigationItemId";
		    $result = $this->common_model->model_get_by_col($tableName,$col,$id);             
            if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		    }
		    $data['result'] = $result;   
			 
			$data['navigation_item_rec'] = $this->common_model->model_get_all_records('NavItemType');
			$this->form_validation->set_rules('navigation_item', 'Navigation Item', 'trim|required|max_length[50]');	
			$this->form_validation->set_rules('controller_name', 'Controller Name', 'trim|max_length[50]');
			$this->form_validation->set_rules('action_name', 'Action Name', 'trim|max_length[50]');
			$this->form_validation->set_rules('query_string', 'Query String', 'trim|max_length[250]');	
			$this->form_validation->set_rules('nav_item_type', 'Nav Item Type', 'trim|required');
			if($this->form_validation->run()==false){
				$this->load->view('super/navigation_item/add_navigation_item',$data);
				}
			else{
				$navigation_item = $this->input->post('navigation_item');
				$controller_name = $this->input->post('controller_name');
				$action_name = $this->input->post('action_name');
				$query_string = $this->input->post('query_string');
				$nav_item_type = $this->input->post('nav_item_type');
				$image_name = $_FILES['userfile']['name'];
				if($image_name != ''){
					$path = './uploads/navigation_item_icon/';	//Image upload path
					$size = 2048;	//Image size
					$width = '';	//Image width
					$height = '';	//Image height
					
					$image = image_upload($path, $size,$width,$height,$width,$height);	//Image upload function
					//print_r($image);
					$err = ""; 
					if(!empty($image)){
						//foreach($image['entity'] as $files){
							if($image['entity']['status']=="error"){
								echo $err =  $err."Error : ".$image['entity']['message']."<br>";
							}
							 
						//}
					}
					unlink_files('./uploads/navigation_item_icon/',$result[0]['NavigationIcon']);
				}
				else{
					$image_name = $result[0]['NavigationIcon'];
				}
				$data = array(
							'NavigationItem' => $navigation_item,
							'ControllerName' => $controller_name,
							'ActionName' => $action_name,
							'QueryString' => $query_string,
							'NavItemTypeId' => $nav_item_type,
							'NavigationIcon' => $image_name
						);
				$this->common_model->update($id,$data,'NavigationItemId','NavigationItem');		
				
				// Generate JSON
				$NavigationPlacement = $this->navigation_item_model->get_nav_placement($id);
				//$navigation_item_rec = $this->common_model->model_get_all_records('NavigationPlacement');
				//print_r($navigation_item_rec);exit;  
				if(!empty($NavigationPlacement)){
					foreach($NavigationPlacement as $placement_rec){
						$place_id = $placement_rec['UserRoleTypeNavigationPlaceId'];
						$rData = $this->_generate_json_file($place_id);
						if($rData == 1){
								$this->session->set_userdata('json','JSON generated successfully');
						}else{
							$this->session->set_userdata('json',$rData);
							}
					}
				} 
				// End JSON
				// Redirect
				redirect('navigation/manage_navigation_item','refresh');
				}	
		}	
		
		// Delete Navigation Item
		public function delete_navigation_item() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='NavigationItemId';
		    $table ="NavigationItem";
		    $NavigationPlacementItem = $this->common_model->compare_in_table('NavigationPlacement','NavigationItemId',$id);
		    if(empty($NavigationPlacementItem)){
				$result = $this->common_model->model_get_by_col($table,$colum_name,$id);      
				unlink_files('./uploads/navigation_item_icon/',$result[0]['NavigationIcon']);
				$this->common_model->delete($id,$colum_name,$table);
			}
	        redirect('navigation/manage_navigation_item','refresh');	
		  }   
	        
		
	   /*
	 * 
	 * Navigation Item Section End
	 * 
	 */		
			
	 
	 
		
		 
		
		 
	
	function main_navigation($clientId=4, $refId=0, $location=0){
			
					//$result = $this->navigation_model->get_navigation_items($clientId, $refId);
					$result = $this->language_navigation_item_model->get_all();
					print_r($result);
					
				}
		
		
	/*
	 * 
	 * Navigation Placement Start
	 * 
	 */	
	 
	function navigation_placement(){
		 //echo base64e(1);exit;
		$data['navigation_item_rec'] = array();
		$data['navigation_placement_rec'] = array();
		$data['role_type_nav_place_id'] = '';
		 
		if($this->uri->segment(3) != ''){
			$navigation_item_rec = array();
			$data['navigation_lang_item_rec'] = '';
			$userRoleTypeNavPlaceId_enc = $this->uri->segment(3);
			//$user_role_type_id_enc = $this->uri->segment(4);
			$userRoleTypeNavPlaceId = base64d($userRoleTypeNavPlaceId_enc);	
			//$user_role_type_id = base64d($user_role_type_id_enc);	
			//$data['place_id'] = $place_id;
			//$data['user_role_type_id'] = $user_role_type_id;
			//$role_type_nav_place = $this->navigation_item_model->get_role_type_nav_place($place_id,$user_role_type_id);
			//$role_type_nav_place_id = $role_type_nav_place[0]['UserRoleTypeNavigationPlaceId'];
			$data['role_type_nav_place_id'] = $userRoleTypeNavPlaceId;
			$navigation_item_placement_rec = $this->common_model->model_get_by_col('NavigationPlacement','UserRoleTypeNavigationPlaceId',$userRoleTypeNavPlaceId);
			//print_r($navigation_item_placement_rec);
			if(!empty($navigation_item_placement_rec)){
				$i = 0;
				$navigation_item_id = array();
				foreach($navigation_item_placement_rec as $rec){
					
					$navigation_item_id[$i] = $rec['NavigationItemId'];
					$i++;
					}
				$navigation_item_rec = $this->navigation_item_model->get_navigation_items($navigation_item_id);
			}else{
				$navigation_item_rec = $this->common_model->model_get_all_records('NavigationItem');
			}
			$data['navigation_item_rec'] = $navigation_item_rec;
			//print_r($data['navigation_item_rec']);exit;
			$navigation_placement_rec = $this->navigation_item_model->get_navigation_placement_by_place($userRoleTypeNavPlaceId,0);
			$data['navigation_placement_rec'] = $navigation_placement_rec;
			 
		}
		$data['user_role_type_nav_place_rec'] = $this->navigation_item_model->get_role_type_nav_place();
		//$data['navigation_places'] = $this->common_model->model_get_all_records('NavigationPlace');
		//$data['user_role_type_rec'] = $this->common_model->model_get_all_records('UserRoleType');
		//$this->form_validation->set_rules('place', 'Place', 'trim|required');	
		$this->form_validation->set_rules('user_role_type_nav_place', 'User Role Type Nav Place', 'trim|required');	
		$this->form_validation->set_rules('navigation_items[]', 'Navigation Items', 'trim|required');	
		if($this->form_validation->run()==false){	
			//$data['navigation_items'] = $this->common_model->model_get_all_records('NavigationItem');
			$this->load->view('super/navigation_item_places/manage_navigation_item_places', $data);	
		}
		else{
			$user_role_type_nav_place = base64d($this->input->post('user_role_type_nav_place'));
			$navigation_items[] = $this->input->post('navigation_items');
			foreach($navigation_items as $result){
					$result_count = count($result);
					for($x = 0; $x<$result_count; $x++){
						$navigation_item_id = $result[$x];
						$data = array(
								'UserRoleTypeNavigationPlaceId' => $user_role_type_nav_place,
								'NavigationItemId' => $navigation_item_id,
								'RefId' => 0,
								'IsActive' =>1
						);
						$rData = $this->common_model->insert($data,'NavigationPlacement');		
						 	 
					}
				
				
			}
			// Generate JSON
				//$rec = $this->navigation_item_model->get_role_type_nav_place($place_id,$user_role_type_id);
		    $rData = $this->_generate_json_file($user_role_type_nav_place);
			if($rData == 1){
				$this->session->set_userdata('json','JSON generated successfully');
			}else{
				$this->session->set_userdata('json',$rData);
				}
				
				// End JSON 
			redirect('navigation/navigation_placement/'.$userRoleTypeNavPlaceId_enc,'refresh');
		}
	} 
	
 
	
	function update_nav_sorting(){
		if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['PostData'])){
			$postData = $_POST['PostData'];
			$postData = rtrim($postData,'|');
			$explodeRows = explode('|',$postData);			 
			for($x=0; $x<count($explodeRows); $x++){
				$explodeValues = explode('-',$explodeRows[$x]);
				$RecordId = $explodeValues[0];
				$RefId = $explodeValues[1];
				$SortingIndex = $explodeValues[2];
				$data = array(
						'RefId' => $RefId,
						'SortingIndex' => $SortingIndex
					);
				$this->common_model->update($RecordId, $data, 'NavigationPlacementId', 'NavigationPlacement');
				
			}
			// Generate JSON
				$UserRoleTypeNavigationPlace = $this->common_model->model_get_all_records('UserRoleTypeNavigationPlace');  
				if(!empty($UserRoleTypeNavigationPlace)){
					foreach($UserRoleTypeNavigationPlace as $rec){
						$role_nav_place_id = $rec['UserRoleTypeNavigationPlaceId'];
						$rData = $this->_generate_json_file($role_nav_place_id);
						if($rData == 1){
								$this->session->set_userdata('json','JSON generated successfully');
						}else{
							$this->session->set_userdata('json',$rData);
							}
					}
				} 
			// End JSON
				
			echo 1;
		}else{
			echo 0;
		}
	}
	
	 
	public function delete_navigation_placement_item() 
		{
			$role_type_nav_place_id = $this->uri->segment(3);
			$id = base64d($this->uri->segment(4));
		    $colum_name='NavigationPlacementId';
		    $table ="NavigationPlacement";
		    $this->common_model->delete($id,$colum_name,$table);
		    $rData = $this->_generate_json_file(base64d($role_type_nav_place_id));
			if($rData == 1){
				$this->session->set_userdata('json','JSON generated successfully');
			}else{
				$this->session->set_userdata('json',$rData);
				}
	        redirect('navigation/navigation_placement/'.$role_type_nav_place_id,'refresh');	
		  }
	
	public function delete_selected_navigation_placements(){
			$role_type_nav_place_id = $this->uri->segment(3);
			$ids = $this->input->post('delrec');
			$x = count($ids);
			for($i=0;$i<$x;$i++){
				$id = base64d($ids[$i]);
				$this->common_model->delete($id,'NavigationPlacementId','NavigationPlacement');
				 
			}
			$rData = $this->_generate_json_file(base64d($role_type_nav_place_id));
			if($rData == 1){
				$this->session->set_userdata('json','JSON generated successfully');
			}else{
				$this->session->set_userdata('json',$rData);
				}
			redirect('navigation/navigation_placement/'.$role_type_nav_place_id,'refresh');
		}
		  
	 
	function navigation_items_by_places(){
		
			$place_id = base64d($this->uri->segment(3));	
			$navigation_placement = array();
			$navigation_placement_rec = $this->navigation_item_model->get_navigation_placement_by_place($place_id, 1);
			
			foreach($navigation_placement_rec as $parent){
				$navigation_placement_child_rec = $this->navigation_item_model->get_navigation_placement_by_place($place_id
				, 2, $parent['NavigationItemId']);
				foreach($navigation_placement_child_rec as $child_rec){
						$child_record[] = array(
											'name' => $child_rec['NavigationItem'],
											'id' => $child_rec['NavigationPlacementId']
										  );
					}
					
				$navigation_placement[] = array(
										'name' => $parent['NavigationItem'],
										'id' => $parent['NavigationPlacementId'],
										'children' => $child_record
										);
			}
			
			
		echo json_encode($navigation_placement);
	} 
	/*
	 * 
	 * Navigation Placement End
	 * 
	 */	
		
		
	
	
	 
	
	public function is_active_navigation_placement_item() 
		{
			$role_type_nav_place_id = $this->uri->segment(3);
			$id = base64d($this->uri->segment(4));
			$status = $this->uri->segment(5);
			if($status == 1){
					$is_active = 0;
			}else{
					$is_active = 1;
			}
			$data = array('IsActive' => $is_active);
			$this->common_model->update($id,$data,'NavigationPlacementId','NavigationPlacement');	
			$rData = $this->_generate_json_file(base64d($role_type_nav_place_id));
			if($rData == 1){
				$this->session->set_userdata('json','JSON generated successfully');
			}else{
				$this->session->set_userdata('json',$rData);
				}
	        redirect('navigation/navigation_placement/'.$role_type_nav_place_id,'refresh');	
	}    
		  
	function generate_json(){
		
			$role_type_nav_place_id_enc = $this->uri->segment(3);
			$role_type_nav_place_id = base64d($role_type_nav_place_id_enc);
			$rData = $this->_generate_json_file($role_type_nav_place_id);
			if($rData == 1){
					echo 'JSON Generater Successfully';
			}else{
				echo $rData;
				}
	}	  
	
	function _generate_json_file($role_type_nav_place_id){
		
						$navigation_placement_parent_rec = $this->navigation_item_model->get_navigation_placement_rec_for_json($role_type_nav_place_id,0);
						$data = array();
						$x = 0;
						//print_r($navigation_placement_parent_rec);
						foreach($navigation_placement_parent_rec as $rec){
								
								if($rec['NavigationIcon'] == ''){
									$nav_icon = '';
								}
								else{
									$nav_icon = base_url('uploads/navigation_item_icon/'.$rec['NavigationIcon']);
								}
								$data[$x]['NavigationPlacementId'] = $rec['NavigationPlacementId'];
								$data[$x]['NavigationItemId'] = $rec['NavigationItemId'];
								$data[$x]['NavigationItem'] = $rec['NavigationItem'];
								$data[$x]['ControllerName'] = $rec['ControllerName'];
								$data[$x]['ActionName'] = $rec['ActionName'];
								$data[$x]['QueryString'] = $rec['QueryString'];
								$data[$x]['NavigationIcon'] = $nav_icon;
								$data[$x]['NavItemTypeId'] = $rec['NavItemTypeId'];
								$data[$x]['NavItemType'] = $rec['NavItemType'];
								// Child
								$navigation_placement_child_rec = $this->navigation_item_model->get_navigation_placement_rec_for_json($role_type_nav_place_id,$rec['NavigationPlacementId']);
								$child_array = array();	
								$i = 0;	
								foreach($navigation_placement_child_rec as $child_rec){	
									if($child_rec['NavigationIcon'] == ''){
										$child_nav_icon = '';
									}
									else{
										$child_nav_icon = base_url('uploads/navigation_item_icon/'.$child_rec['NavigationIcon']);
									}	
									$child_array[$i]['NavigationPlacementId'] = $child_rec['NavigationPlacementId'];
									$child_array[$i]['NavigationItemId'] = $child_rec['NavigationItemId'];
									$child_array[$i]['NavigationItem'] = $child_rec['NavigationItem'];
									$child_array[$i]['ControllerName'] = $child_rec['ControllerName'];
									$child_array[$i]['ActionName'] = $child_rec['ActionName'];
									$child_array[$i]['QueryString'] = $child_rec['QueryString'];
									$child_array[$i]['NavigationIcon'] = $child_nav_icon;
									$child_array[$i]['NavItemTypeId'] = $child_rec['NavItemTypeId'];
									$child_array[$i]['NavItemType'] = $child_rec['NavItemType'];
									
									// third level child
									$navigation_placement_third_level_child_rec = $this->navigation_item_model->get_navigation_placement_rec_for_json($role_type_nav_place_id,$child_rec['NavigationPlacementId']);
									//print_r($navigation_placement_third_level_child_rec);
									$third_level_child_array = array();	
									$y = 0;	
									foreach($navigation_placement_third_level_child_rec as $third_level_child_rec){	
										if($third_level_child_rec['NavigationIcon'] == ''){
											$third_level_child_nav_icon = '';
										}
										else{
											$third_level_child_nav_icon = base_url('uploads/navigation_item_icon/'.$third_level_child_rec['NavigationIcon']);
										}	
										$third_level_child_array[$y]['NavigationPlacementId'] = $third_level_child_rec['NavigationPlacementId'];
										$third_level_child_array[$y]['NavigationItemId'] = $third_level_child_rec['NavigationItemId'];
										$third_level_child_array[$y]['NavigationItem'] = $third_level_child_rec['NavigationItem'];
										$third_level_child_array[$y]['ControllerName'] = $third_level_child_rec['ControllerName'];
										$third_level_child_array[$y]['ActionName'] = $third_level_child_rec['ActionName'];
										$third_level_child_array[$y]['QueryString'] = $third_level_child_rec['QueryString'];
										$third_level_child_array[$y]['NavigationIcon'] = $third_level_child_nav_icon;
										$third_level_child_array[$y]['NavItemTypeId'] = $third_level_child_rec['NavItemTypeId'];
										$third_level_child_array[$y]['NavItemType'] = $third_level_child_rec['NavItemType'];
										// third level child
										
										$y++;		
									}
									//print_r($third_level_child_array);
									$child_array[$i]['child'] = $third_level_child_array;
									$i++;		
								}
								$data[$x]['child'] = $child_array;
								$x++;
										
						}
						//print_r($data);exit;
						if(!empty($data)){
							//$data = json_encode($data);
							//echo "<pre>";
							$user_role_nav_place_rec = $this->navigation_item_model->get_role_type_nav_place($role_type_nav_place_id);
							//$user_role_nav_place_rec = $this->common_model->model_get_by_col('UserRoleTypeNavigationPlace','UserRoleTypeNavigationPlaceId',$role_type_nav_place_id);
							//$place_id = $user_role_nav_place_rec[0]['NavigationPlaceId'];
							//$role_type_id = $user_role_nav_place_rec[0]['UserRoleTypeId'];
							//$place_rec = $this->common_model->model_get_by_col('NavigationPlace','NavigationPlaceId',$place_id);
							//$role_type_rec = $this->common_model->model_get_by_col('UserRoleType','UserRoleTypeId',$role_type_id);
							$place = strtolower($user_role_nav_place_rec[0]['NavigationPlace']);
							$role = strtolower($user_role_nav_place_rec[0]['UserRoleType']);
							$file_name = $role.'_'.$place.'_nav.json';
							$json_folder_name = $this->config->item('json_folder_name');
							$file_path = $json_folder_name.'/'.$file_name;
							$return_status = generate_upload_json($data,$file_name,$file_path);
							return $return_status;
						}else{
							$return_status = 'No data found';
							return $return_status;
							}
		}
		
	/*
	 * 
	 * User Role Type Nav Place Section Start
	 * 
	 */
	 
	// Manage User Role Type Nav Place
	function manage_user_role_type_nav_place(){
		
		$data['user_role_type_nav_place_rec'] = $this->navigation_model->get_all_user_role_type_nav_place();
		$this->load->view('super/user_role_type_nav_place/manage_user_role_type_nav_place',$data);	
	}	
	
	//Add User Role Type Nav Place
	function add_user_role_type_nav_place(){
		
		$data['place_rec'] = $this->common_model->model_get_all_records('NavigationPlace');
		$data['user_role_type_rec'] = $this->common_model->model_get_all_records('UserRoleType');
		$this->form_validation->set_rules('place', 'Place', 'trim|required');	
		$this->form_validation->set_rules('user_role_type', 'User Role Type', 'trim|required|callback__is_unique_role_place');
		$this->form_validation->set_rules('json_permission', 'json Permission', 'trim|required');
		if($this->form_validation->run()==false){
			$this->load->view('super/user_role_type_nav_place/add_user_role_type_nav_place',$data);
			}
		else{
			$place = $this->input->post('place');
			$user_role_type = $this->input->post('user_role_type');
			$json_permission = $this->input->post('json_permission'); 
			$data = array(
						'NavigationPlaceId' => $place,
						'UserRoleTypeId' => $user_role_type,
						'AddInPermissionJson' => $json_permission
					);
			$this->common_model->insert($data,'UserRoleTypeNavigationPlace');	
			 
			if(isset($_POST['continue'])){
				redirect('navigation/add_user_role_type_nav_place','refresh');
			}
			else{
				redirect('navigation/manage_user_role_type_nav_place','refresh');
			}	
		}
	}
	
	function _is_unique_role_place($str){
		
			if($str == ''){
				$this->form_validation->set_message('_is_unique_role_place','User Role Type Required');	
				return false;
			}else{
				$user_role_type = $str;
				$place_id = $this->input->post('place');
				$result = $this->navigation_model->get_user_role_type_nav_place($user_role_type, $place_id);
				if(empty($result)){
					return true;
				}else{
					$this->form_validation->set_message('_is_unique_role_place','User Role Type & Place combination should be unique');	
					return false;
				}	
			}
	}
	
	//Edit User Role Type Nav Place
	function edit_user_role_type_nav_place(){
		
		$id = base64d($this->uri->segment(3)); 
		if(!is_numeric($id)){
			echo "OOOPS!!! Error. Please contact administrator";
			exit;   
		} 
		$result = $this->common_model->model_get_by_col('UserRoleTypeNavigationPlace','UserRoleTypeNavigationPlaceId',$id);             
        if(empty($result)){
			echo "OOOPS!!! Error. Please contact administratorr";
			exit;   
		}
		$data['result'] = $result;
		$data['place_rec'] = $this->common_model->model_get_all_records('NavigationPlace');
		$data['user_role_type_rec'] = $this->common_model->model_get_all_records('UserRoleType');
		$place = $this->input->post('place');
		$user_role_type = $this->input->post('user_role_type');
		if($result[0]['NavigationPlaceId'] == $place && $result[0]['UserRoleTypeId'] == $user_role_type){
			$is_unique = '';	
		}else{
			$is_unique = '|callback__is_unique_role_place';
			}
		$this->form_validation->set_rules('place', 'Place', 'trim|required');	
		$this->form_validation->set_rules('user_role_type', 'User Role Type', 'trim|required'.$is_unique);
		//$this->form_validation->set_rules('json_permission', 'json Permission', 'trim|required');
		if($this->form_validation->run()==false){
			$this->load->view('super/user_role_type_nav_place/add_user_role_type_nav_place',$data);
			}
		else{
			$json_permission = $this->input->post('json_permission'); 
			$data = array(
						'NavigationPlaceId' => $place,
						'UserRoleTypeId' => $user_role_type,
						'AddInPermissionJson' => $json_permission
					);
			$this->common_model->update($id,$data,'UserRoleTypeNavigationPlaceId','UserRoleTypeNavigationPlace');		 
			redirect('navigation/manage_user_role_type_nav_place','refresh');
			
		}
	}
	
	public function delete_user_role_type_nav_place() 
		{
			$id = base64d($this->uri->segment(3));
		    $colum_name='UserRoleTypeNavigationPlaceId';
		    $table ="UserRoleTypeNavigationPlace";
		    $NavigationPlacement = $this->common_model->compare_in_table('NavigationPlacement','UserRoleTypeNavigationPlaceId',$id);
		    if(empty($NavigationPlacement)){
				$this->common_model->delete($id,$colum_name,$table);
			}
			redirect('navigation/manage_user_role_type_nav_place','refresh');	
		  }   
	
	function generate_permission_json(){
		
					$userRoleTypeNavPlace = $this->navigation_item_model->get_active_permission_user_role_type_nav_place();
					$json_data = array();
					$j = 0;
					foreach($userRoleTypeNavPlace as $userRoleTypeNavPlace){	
						$role_type_nav_place_id = $userRoleTypeNavPlace['UserRoleTypeNavigationPlaceId'];
						$navigation_placement_parent_rec = $this->navigation_item_model->get_navigation_placement_rec_for_json($role_type_nav_place_id,0);
						//if(!empty($navigation_placement_parent_rec)){
							$json_data[$j]['user_role_place'] = $userRoleTypeNavPlace['NavigationPlace'];
							$json_data[$j]['user_role_type'] = $userRoleTypeNavPlace['UserRoleType'];
							$json_data[$j]['user_role_type_id'] = $userRoleTypeNavPlace['UserRoleTypeId'];
							$json_data[$j]['navigation'] = '';
							$data = array();
							$x = 0;
							foreach($navigation_placement_parent_rec as $rec){
									
									if($rec['NavigationIcon'] == ''){
										$nav_icon = '';
									}
									else{
										$nav_icon = base_url('uploads/navigation_item_icon/'.$rec['NavigationIcon']);
									}
									$data[$x]['NavigationPlacementId'] = $rec['NavigationPlacementId'];
									$data[$x]['NavigationItemId'] = $rec['NavigationItemId'];
									$data[$x]['NavigationItem'] = $rec['NavigationItem'];
									$data[$x]['ControllerName'] = $rec['ControllerName'];
									$data[$x]['ActionName'] = $rec['ActionName'];
									$data[$x]['QueryString'] = $rec['QueryString'];
									$data[$x]['NavigationIcon'] = $nav_icon;
									$data[$x]['NavItemTypeId'] = $rec['NavItemTypeId'];
									$data[$x]['NavItemType'] = $rec['NavItemType'];
									// Child
									$navigation_placement_child_rec = $this->navigation_item_model->get_navigation_placement_rec_for_json($role_type_nav_place_id,$rec['NavigationPlacementId']);
									$child_array = array();	
									$i = 0;	
									foreach($navigation_placement_child_rec as $child_rec){	
										if($child_rec['NavigationIcon'] == ''){
											$child_nav_icon = '';
										}
										else{
											$child_nav_icon = base_url('uploads/navigation_item_icon/'.$child_rec['NavigationIcon']);
										}	
										$child_array[$i]['NavigationPlacementId'] = $child_rec['NavigationPlacementId'];
										$child_array[$i]['NavigationItemId'] = $child_rec['NavigationItemId'];
										$child_array[$i]['NavigationItem'] = $child_rec['NavigationItem'];
										$child_array[$i]['ControllerName'] = $child_rec['ControllerName'];
										$child_array[$i]['ActionName'] = $child_rec['ActionName'];
										$child_array[$i]['QueryString'] = $child_rec['QueryString'];
										$child_array[$i]['NavigationIcon'] = $child_nav_icon;
										$child_array[$i]['NavItemTypeId'] = $child_rec['NavItemTypeId'];
										$child_array[$i]['NavItemType'] = $child_rec['NavItemType'];
										
										// third level child
										$navigation_placement_third_level_child_rec = $this->navigation_item_model->get_navigation_placement_rec_for_json($role_type_nav_place_id,$child_rec['NavigationPlacementId']);
										//print_r($navigation_placement_third_level_child_rec);
										$third_level_child_array = array();	
										$y = 0;	
										foreach($navigation_placement_third_level_child_rec as $third_level_child_rec){	
											if($third_level_child_rec['NavigationIcon'] == ''){
												$third_level_child_nav_icon = '';
											}
											else{
												$third_level_child_nav_icon = base_url('uploads/navigation_item_icon/'.$third_level_child_rec['NavigationIcon']);
											}	
											$third_level_child_array[$y]['NavigationPlacementId'] = $third_level_child_rec['NavigationPlacementId'];
											$third_level_child_array[$y]['NavigationItemId'] = $third_level_child_rec['NavigationItemId'];
											$third_level_child_array[$y]['NavigationItem'] = $third_level_child_rec['NavigationItem'];
											$third_level_child_array[$y]['ControllerName'] = $third_level_child_rec['ControllerName'];
											$third_level_child_array[$y]['ActionName'] = $third_level_child_rec['ActionName'];
											$third_level_child_array[$y]['QueryString'] = $third_level_child_rec['QueryString'];
											$third_level_child_array[$y]['NavigationIcon'] = $third_level_child_nav_icon;
											$third_level_child_array[$y]['NavItemTypeId'] = $third_level_child_rec['NavItemTypeId'];
											$third_level_child_array[$y]['NavItemType'] = $third_level_child_rec['NavItemType'];
											// third level child
											
											$y++;		
										}
										//print_r($third_level_child_array);
										$child_array[$i]['child'] = $third_level_child_array;
										$i++;		
									}
									$data[$x]['child'] = $child_array;
									$x++;
											
							}
							$json_data[$j]['navigation'] = $data;
							 
							
						$j++;	 
						//}
					}
					if(!empty($json_data)){
								
								//echo '<pre>';
								//print_r($json_data);
								$json_data1 = json_encode($json_data);
								$file_name = 'permission.json';
								$file_path = 'navigation/'.$file_name;
								if(file_exists($file_path)){
										 
										unlink($file_path);
										$myfile = fopen($file_path, "w") or die("Unable to open file!");
										fwrite($myfile, $json_data1);
										fclose($myfile);
										
								}else{
										$myfile = fopen($file_path, "w") or die("Unable to open file!");
										fwrite($myfile, $json_data1);
										fclose($myfile);
										
								}
								// connect and login to FTP server
									$ftp_server = $this->config->item('ftp_server');
									$ftp_uname = $this->config->item('ftp_uname');
									$ftp_password = $this->config->item('ftp_password');
									$ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
									$login = ftp_login($ftp_conn, $ftp_uname, $ftp_password);
									$file = $file_path;
									if (file_exists($file_name)) { // file_name is server file path
										ftp_delete($ftp_conn, $file_name);
										//echo 'File Deleted';exit;	
									}
								// upload file
									if(ftp_put($ftp_conn, $file_name, $file, FTP_ASCII))
									  {
									  //echo "Successfully uploaded $file.";
									  }
									else
									  {
									  echo "Error uploading $file.";
									  }
									
								// close connection
									ftp_close($ftp_conn);
								
							}
					//exit;
					$this->session->set_userdata('json_perm','JSON successfully generated');
					redirect('navigation/manage_user_role_type_nav_place');
	}
	
	/*
	 * 
	 * User Role Type Nav Place Section End
	 * 
	 */		  	
}
?>	
