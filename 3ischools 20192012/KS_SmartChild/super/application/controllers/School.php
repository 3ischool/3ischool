<?php
class School extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->load->model('school/school_model');
		$this->load->library('digipaging');
		is_logged_in();
	}

    function index(){
		//admin@gmail.com
		//echo md5('12345678');exit;
		redirect('school/manage_school_db');
	}
	
	/****************************** Start School Board Section **********************************/
	function manage_board(){
		
			$data['board'] = $this->common_model->model_get_all_records('Board');
			$this->load->view('super/school/board/manage_board',$data);
	} 
	
	function add_board(){
		
			$this->form_validation->set_rules('board','School Board','trim|required|is_unique[Board.BoardName]|max_length[50]');
			$this->form_validation->set_rules('description','Description','trim|max_length[100]');
			if($this->form_validation->run() == false){
				$this->load->view('super/school/board/add_board');
			}
			else{
				$board = $this->input->post('board');
				$description = $this->input->post('description');
				$data = array(
						'BoardName' => $board,
						'BoardDescription' => $description
						);
				$this->common_model->insert($data,'Board');	
				if(isset($_POST['publish'])){
						redirect('school/add_board');
				}
				else{
						$this->session->set_userdata('msg','School Board inserted successfully.');
						redirect('school/manage_board');
				}	
			}
	}
	
	function edit_board(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
					echo 'Please Contact Administrator';
					exit;
			}
			$edit_rec = $this->common_model->model_get_by_col('Board','BoardId',$id);
			$data['edit_rec'] = $edit_rec;
			$is_unique = '';
			if($this->input->post('board') != $edit_rec[0]['BoardName']){
				$is_unique = '|is_unique[Board.BoardName]';
			}
			$this->form_validation->set_rules('board','School Board','trim|required|max_length[50]'.$is_unique);
			$this->form_validation->set_rules('description','Description','trim|max_length[100]');
			if($this->form_validation->run() == false){
				$this->load->view('super/school/board/add_board', $data);
			}
			else{
				$board = $this->input->post('board');
				$description = $this->input->post('description');
				$data = array(
						'BoardName' => $board,
						'BoardDescription' => $description
						);
				$this->common_model->update($id,$data,'BoardId','Board');	
				$this->session->set_userdata('msg','School Board Updated successfully.');
				redirect('school/manage_board');
					
			}
	}
	
	function delete_board(){
		
			$id = base64d($this->uri->segment(3));
			if($id == ''){
					echo 'Please Contact Administrator';
					exit;
			}
			$rData = $this->common_model->compare_in_table('School','BoardId',$id);
			if(empty($rData)){
				$this->common_model->delete($id,'BoardId','Board');
				$this->session->set_userdata('msg','School Board deleted Successfully');
			}
			redirect('school/manage_board');
	}
	/****************************** End School Board Section **********************************/
	/****************************** Start School Group Section **********************************/
	function manage_school_group(){
		
			$data['school_group'] = $this->common_model->model_get_all_records('SchoolGroup');
			$this->load->view('super/school/school_group/manage_school_group',$data);
	} 
	
	function add_school_group(){
		
			$this->form_validation->set_rules('school_group','School Group','trim|required|is_unique[SchoolGroup.SchoolGroup]|max_length[50]');
			if($this->form_validation->run() == false){
				$this->load->view('super/school/school_group/add_school_group');
			}
			else{
				$school_group = $this->input->post('school_group');
				$data = array(
						'SchoolGroup' => $school_group 
						);
				$this->common_model->insert($data,'SchoolGroup');	
				if(isset($_POST['publish'])){
						redirect('school/add_school_group');
				}
				else{
						$this->session->set_userdata('msg','School Group inserted successfully.');
						redirect('school/manage_school_group');
				}	
			}
	}
	
	function edit_school_group(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
					echo 'Please Contact Administrator';
					exit;
			}
			$edit_rec = $this->common_model->model_get_by_col('SchoolGroup','SchoolGroupId',$id);
			$data['edit_rec'] = $edit_rec;
			$is_unique = '';
			if($this->input->post('school_group') != $edit_rec[0]['SchoolGroup']){
				$is_unique = '|is_unique[SchoolGroup.SchoolGroup]';
			}
			$this->form_validation->set_rules('school_group','School Group','trim|required|max_length[50]'.$is_unique);
			if($this->form_validation->run() == false){
				$this->load->view('super/school/school_group/add_school_group', $data);
			}
			else{
				$school_group = $this->input->post('school_group');
				$data = array(
						'SchoolGroup' => $school_group 
						);
				$this->common_model->update($id,$data,'SchoolGroupId','SchoolGroup');	
				$this->session->set_userdata('msg','School Group Updated successfully.');
				redirect('school/manage_school_group');
					
			}
	}
	
	function delete_school_group(){
		
			$id = base64d($this->uri->segment(3));
			if($id == ''){
					echo 'Please Contact Administrator';
					exit;
			}
			$rData = $this->common_model->compare_in_table('School','SchoolGroupId',$id);
			if(empty($rData)){
				$this->common_model->delete($id,'SchoolGroupId','SchoolGroup');
				$this->session->set_userdata('msg','School Group deleted Successfully');
			}
			redirect('school/manage_school_group');
	}
	/****************************** End School Group Section **********************************/
	/****************************** Start School DB Section **********************************/
	function manage_school_db(){
		
			$data['school_db'] = $this->common_model->model_get_all_records('SchoolDB');
			$this->load->view('super/school/school_db/manage_school_db', $data);
	} 
	
	function add_school_db(){
		
			$this->form_validation->set_rules('db_name','School DBName','trim|required|max_length[150]');
			$this->form_validation->set_rules('effective_date','Effective Date','trim|required');
			if($this->form_validation->run() == false){
				$this->load->view('super/school/school_db/add_school_db');
			}
			else{
				$db_name = $this->input->post('db_name');
				$effective_date = $this->input->post('effective_date');
				$is_default = $this->input->post('is_default');
				if($is_default == 1){
						$is_default = 1;
				}else{
						$is_default = 0;
					}
				$data = array(
						'DBName' => $db_name, 
						'EffectiveDate' => $effective_date, 
						'IsDefault' => $is_default 
						);
				$this->common_model->insert($data,'SchoolDB');	
				if(isset($_POST['publish'])){
						redirect('school/add_school_db');
				}
				else{
						$this->session->set_userdata('msg','School Group inserted successfully.');
						redirect('school/manage_school_db');
				}	
			}
	}
	
	function edit_school_db(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
					echo 'Please Contact Administrator';
					exit;
			}
			$edit_rec = $this->common_model->model_get_by_col('SchoolDB','SchoolDBId',$id);
			$data['edit_rec'] = $edit_rec;
			$this->form_validation->set_rules('db_name','School DBName','trim|required|max_length[150]');
			$this->form_validation->set_rules('effective_date','Effective Date','trim|required');
			if($this->form_validation->run() == false){
				$this->load->view('super/school/school_db/add_school_db',$data);
			}
			else{
				$db_name = $this->input->post('db_name');
				$effective_date = $this->input->post('effective_date');
				$is_default = $this->input->post('is_default');
				if($is_default == 1){
						$is_default = 1;
				}else{
						$is_default = 0;
					}
				$data = array(
						'DBName' => $db_name, 
						'EffectiveDate' => $effective_date, 
						'IsDefault' => $is_default 
						);
				$this->common_model->update($id,$data,'SchoolDBId','SchoolDB');	
				$this->session->set_userdata('msg','School DB Updated successfully.');
				redirect('school/manage_school_db');
					
			}
	}
	
	function delete_school_db(){
		
			$id = base64d($this->uri->segment(3));
			if($id == ''){
					echo 'Please Contact Administrator';
					exit;
			}
			$rData = $this->common_model->compare_in_table('School','SchoolDBId',$id);
			if(empty($rData)){
				$this->common_model->delete($id,'SchoolDBId','SchoolDB');
				$this->session->set_userdata('msg','School DB deleted Successfully');
			}
			redirect('school/manage_school_db');
	}
	/****************************** End School DB Section **********************************/
	
	/****************************** Start School Section **********************************/
	function manage_school(){
		
			$data['school'] = $this->school_model->get_all_rec();
			$this->load->view('super/school/school/manage_school', $data);
	} 
	
	function add_school(){
		
			$data['board_rec'] = $this->common_model->model_get_all_records('Board');
			$data['school_group_rec'] = $this->common_model->model_get_all_records('SchoolGroup');
			$data['school_db_rec'] = $this->common_model->model_get_all_records('SchoolDB');
			$this->form_validation->set_rules('school_name','School Name','trim|required|max_length[150]|is_unique[School.SchoolName]');
			$this->form_validation->set_rules('school_board','School Board','trim');
			$this->form_validation->set_rules('school_group','School Group','trim');
			$this->form_validation->set_rules('domain','Domain','trim|max_length[150]');
			$this->form_validation->set_rules('student_strength','Student Strength','trim|required|integer');
			$this->form_validation->set_rules('school_db','School DB','trim');
			if($this->form_validation->run() == false){
				$this->load->view('super/school/school/add_school',$data);
			}
			else{
				$school_name = $this->input->post('school_name');
				$school_board = $this->input->post('school_board');
				$school_group = $this->input->post('school_group');
				$domain = $this->input->post('domain');
				$student_strength = $this->input->post('student_strength');
				$school_db = $this->input->post('school_db');
				 
				$data = array(
						'SchoolName' => $school_name, 
						'BoardId' => $school_board, 
						'SchoolGroupId' => $school_group, 
						'Domain' => $domain, 
						'StudentsStrength' => $student_strength, 
						'SchoolDBId' => $school_db
						);
				//print_r($data);exit;		
				$this->common_model->insert($data,'School');	
				if(isset($_POST['publish'])){
						redirect('school/add_school');
				}
				else{
						$this->session->set_userdata('msg','School inserted successfully.');
						redirect('school/manage_school');
				}	
			}
	}
	
	function edit_school(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
					echo 'Please Contact Administrator';
					exit;
			}
			$edit_rec = $this->common_model->model_get_by_col('School','SchoolId',$id);
			$data['edit_rec'] = $edit_rec;
			$is_unique = '';
			if($this->input->post('school_name') != $edit_rec[0]['SchoolName']){
					$is_unique = '|is_unique[School.SchoolName]';
			}
			$data['board_rec'] = $this->common_model->model_get_all_records('Board');
			$data['school_group_rec'] = $this->common_model->model_get_all_records('SchoolGroup');
			$data['school_db_rec'] = $this->common_model->model_get_all_records('SchoolDB');
			$this->form_validation->set_rules('school_name','School Name','trim|required|max_length[150]'.$is_unique);
			$this->form_validation->set_rules('school_board','School Board','trim');
			$this->form_validation->set_rules('school_group','School Group','trim');
			$this->form_validation->set_rules('domain','Domain','trim|max_length[150]');
			$this->form_validation->set_rules('student_strength','Student Strength','trim|required|integer');
			$this->form_validation->set_rules('school_db','School DB','trim');
			if($this->form_validation->run() == false){
				$this->load->view('super/school/school/add_school',$data);
			}
			else{
				$school_name = $this->input->post('school_name');
				$school_board = $this->input->post('school_board');
				$school_group = $this->input->post('school_group');
				$domain = $this->input->post('domain');
				$student_strength = $this->input->post('student_strength');
				$school_db = $this->input->post('school_db');
				 
				$data = array(
						'SchoolName' => $school_name, 
						'BoardId' => $school_board, 
						'SchoolGroupId' => $school_group, 
						'Domain' => $domain, 
						'StudentsStrength' => $student_strength, 
						'SchoolDBId' => $school_db
						);
				$this->common_model->update($id,$data,'SchoolId','School');	
				$this->session->set_userdata('msg','School Updated successfully.');
				redirect('school/manage_school');
					
			}
	}
	
	function delete_school(){
		
			$id = base64d($this->uri->segment(3));
			if($id == ''){
					echo 'Please Contact Administrator';
					exit;
			}
			$this->common_model->delete($id,'SchoolId','School');
			$this->session->set_userdata('msg','School deleted Successfully');
			redirect('school/manage_school');
	}
	/****************************** End School DB Section **********************************/
	
	 
	 
		
}
?>	
