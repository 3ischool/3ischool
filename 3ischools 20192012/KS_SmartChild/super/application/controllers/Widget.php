<?php
class Widget extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');
		$this->load->model('widget/widget_model');
		$this->load->library('digipaging');
		$this->load->helper('imageupload');
		$this->load->helper('common');
		is_logged_in();
	}

    function index(){
		//admin@gmail.com
		//echo md5('12345678');exit;
		redirect('widget/manage_html_template');
	}
	
	/****************************** Start HTML Template Section **********************************/
	function manage_html_template(){
		
			$data['html_template_rec'] = $this->common_model->model_get_all_records('Wid_HTMLTemplate');
			$this->load->view('super/widget/html_template/manage_html_template',$data);
	} 
	
	function add_html_template(){
		
			$this->form_validation->set_rules('title','Title','trim|required|is_unique[Wid_HTMLTemplate.HTMLTemplateTitle]|max_length[50]');
			$this->form_validation->set_rules('html_template','HTML Template','trim|required|max_length[500]');
			if($this->form_validation->run() == false){
				$this->load->view('super/widget/html_template/add_html_template');
			}
			else{
				$title = $this->input->post('title');
				$html_template = $this->input->post('html_template');
				$data = array(
						'HTMLTemplateTitle' => $title,
						'HTMLTemplate' => $html_template,
						'IsActive' => 1
						);
				$rData = $this->common_model->insert($data,'Wid_HTMLTemplate');	
				if(is_numeric($rData)){
					$this->session->set_userdata('msg','HTML Template added successfully.');
				}else{
					$this->session->set_userdata('msg',$rData);
				}
				if(isset($_POST['publish'])){
						redirect('widget/add_html_template');
				}
				else{
						$this->session->set_userdata('msg','HTML Template added successfully.');
						redirect('widget/manage_html_template');
				}	
			}
	}
	
	function edit_html_template(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
					echo 'Please Contact Administrator';
					exit;
			}
			$edit_rec = $this->common_model->model_get_by_col('Wid_HTMLTemplate','HTMLTemplateId',$id);
			//print_r($edit_rec);exit;
			$data['edit_rec'] = $edit_rec;
			$is_unique = '';
			if($this->input->post('title') != $edit_rec[0]['HTMLTemplateTitle']){
				$is_unique = '|is_unique[Wid_HTMLTemplate.HTMLTemplateTitle]';
			}
			$this->form_validation->set_rules('title','Title','trim|required|max_length[50]'.$is_unique);
			$this->form_validation->set_rules('html_template','HTML Template','trim|max_length[50]');
			if($this->form_validation->run() == false){
				$this->load->view('super/widget/html_template/add_html_template', $data);
			}
			else{
				$title = $this->input->post('title');
				$html_template = $this->input->post('html_template');
				$data = array(
						'HTMLTemplateTitle' => $title,
						'HTMLTemplate' => $html_template
						);
				$this->common_model->update($id,$data,'HTMLTemplateId','Wid_HTMLTemplate');	
				$this->session->set_userdata('msg','Html Template Updated successfully.');
				redirect('widget/manage_html_template');
					
			}
	}
	
	function delete_html_template(){
		
			$id = base64d($this->uri->segment(3));
			if($id == ''){
					echo 'Please Contact Administrator';
					exit;
			}
			$rData = $this->common_model->compare_in_table('Wid_WidgetType','HTMLTemplateId',$id);
			if(empty($rData)){
				$this->common_model->delete($id,'HTMLTemplateId','Wid_HTMLTemplate');
				$this->session->set_userdata('msg','Html Template deleted Successfully');
			}
			redirect('widget/manage_html_template');
	}
	/****************************** End HTML Template Section **********************************/
	 
	/****************************** Start Widget Type Section **********************************/
	function manage_widget_type(){
		
			$data['widget_type_rec'] = $this->widget_model->get_all_widget_type_records();
			$this->load->view('super/widget/widget_type/manage_widget_type',$data);
	} 
	
	function add_widget_type(){
		
			$data['template_rec'] = $this->common_model->model_get_all_records('Wid_HTMLTemplate');
			//print_r($data['template_rec']);exit;	
			$this->form_validation->set_rules('widget_type','Widget Type','trim|required|is_unique[Wid_WidgetType.WidgetType]|max_length[50]');
			$this->form_validation->set_rules('html_template','HTML Template','trim|required|max_length[50]');
			if($this->form_validation->run() == false){
				$this->load->view('super/widget/widget_type/add_widget_type',$data);
			}
			else{
				$widget_type = $this->input->post('widget_type');
				$html_template = $this->input->post('html_template');
				$data = array(
						'WidgetType' => $widget_type,
						'HTMLTemplateId' => $html_template,
						'IsActive' => 1
						);
				$rData = $this->common_model->insert($data,'Wid_WidgetType');	
				if(is_numeric($rData)){
					$this->session->set_userdata('msg','Widget Type added successfully.');
				}else{
					$this->session->set_userdata('msg',$rData);
				}
				if(isset($_POST['publish'])){
						redirect('widget/add_widget_type');
				}
				else{
						redirect('widget/manage_widget_type');
				}	
			}
	}
	
	function edit_widget_type(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
					echo 'Please Contact Administrator';
					exit;
			}
			$edit_rec = $this->common_model->model_get_by_col('Wid_WidgetType','WidgetTypeId',$id);
			//print_r($edit_rec);exit;
			$data['edit_rec'] = $edit_rec;
			$is_unique = '';
			if($this->input->post('widget_type') != $edit_rec[0]['WidgetType']){
				$is_unique = '|is_unique[Wid_WidgetType.WidgetType]';
			}
			$data['template_rec'] = $this->common_model->model_get_all_records('Wid_HTMLTemplate');
			//print_r($data['template_rec']);exit;	
			$this->form_validation->set_rules('widget_type','Widget Type','trim|required|max_length[50]'.$is_unique);
			$this->form_validation->set_rules('html_template','HTML Template','trim|required|max_length[50]');
			if($this->form_validation->run() == false){
				$this->load->view('super/widget/widget_type/add_widget_type',$data);
			}
			else{
				$widget_type = $this->input->post('widget_type');
				$html_template = $this->input->post('html_template');
				$data = array(
						'WidgetType' => $widget_type,
						'HTMLTemplateId' => $html_template,
						'IsActive' => 1
						);
				$this->common_model->update($id,$data,'WidgetTypeId','Wid_WidgetType');	
				$this->session->set_userdata('msg','Widget Type Updated successfully.');
				redirect('widget/manage_widget_type');
					
			}
	}
	
	function delete_widget_type(){
		
			$id = base64d($this->uri->segment(3));
			if($id == ''){
					echo 'Please Contact Administrator';
					exit;
			}
			$rData = $this->common_model->compare_in_table('Wid_ColorClassMaster','WidgetTypeId',$id);
			if(empty($rData)){
				$this->common_model->delete($id,'WidgetTypeId','Wid_WidgetType');
				$this->session->set_userdata('msg','Widget Type deleted Successfully');
			}
			redirect('widget/manage_widget_type');
	}
	/****************************** End Widget Type Section **********************************/
	 
	/****************************** Start Color Class Master Section **********************************/
	function manage_color_class_master(){
		
			$data['color_class_master_rec'] = $this->widget_model->get_all_color_class_records();
			$this->load->view('super/widget/color_class_master/manage_color_class_master',$data);
	} 
	
	function add_color_class_master(){
		
			$data['widget_type_rec'] = $this->common_model->model_get_all_records('Wid_WidgetType');
			//print_r($data['template_rec']);exit;	
			$this->form_validation->set_rules('css_class','Css Class','trim|required|is_unique[Wid_ColorClassMaster.CSSClass]|max_length[50]');
			$this->form_validation->set_rules('widget_type','Widget Type','trim|required');
			$this->form_validation->set_rules('css_code','Css Code','trim|required');
			if($this->form_validation->run() == false){
				$this->load->view('super/widget/color_class_master/add_color_class_master',$data);
			}
			else{
				$widget_type = $this->input->post('widget_type');
				$css_class = $this->input->post('css_class');
				$css_code = $this->input->post('css_code');
				$data = array(
						'WidgetTypeId' => $widget_type,
						'CSSClass' => $css_class,
						'CssCode' => $css_code
						);
				$rData = $this->common_model->insert($data,'Wid_ColorClassMaster');	
				if(is_numeric($rData)){
					$this->session->set_userdata('msg','Color Class added successfully.');
				}else{
					$this->session->set_userdata('msg',$rData);
				}
				if(isset($_POST['publish'])){
						redirect('widget/add_color_class_master');
				}
				else{
						redirect('widget/manage_color_class_master');
				}	
			}
	}
	
	function add_color_class_popup(){
		
			$widget_type_id = (int)$this->uri->segment(3);
			$data['widget_type_rec'] = $this->common_model->model_get_all_records('Wid_WidgetType');
			//print_r($data['template_rec']);exit;	
			$this->form_validation->set_rules('css_class','Css Class','trim|required|is_unique[Wid_ColorClassMaster.CSSClass]|max_length[50]');
			$this->form_validation->set_rules('css_code','Css Code','trim|required');
			if($this->form_validation->run() == false){
				$this->load->view('super/widget/color_class_master/add_color_class_popup',$data);
			}
			else{
				$css_class = $this->input->post('css_class');
				$css_code = $this->input->post('css_code');
				$data = array(
						'WidgetTypeId' => $widget_type_id,
						'CSSClass' => $css_class,
						'CssCode' => $css_code
						);
				$rData = $this->common_model->insert($data,'Wid_ColorClassMaster');	
				redirect('widget/add_color_class_popup/'.base64e($widget_type_id).'/'.$rData);
				
			}
	}
	
	function edit_color_class_master(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
					echo 'Please Contact Administrator';
					exit;
			}
			$edit_rec = $this->common_model->model_get_by_col('Wid_ColorClassMaster','ColorClassId',$id);
			//print_r($edit_rec);exit;
			$data['edit_rec'] = $edit_rec;
			$is_unique = '';
			if($this->input->post('css_class') != $edit_rec[0]['CSSClass']){
				$is_unique = '|is_unique[Wid_ColorClassMaster.CSSClass]';
			}
			$data['widget_type_rec'] = $this->common_model->model_get_all_records('Wid_WidgetType');
			//print_r($data['template_rec']);exit;	
			$this->form_validation->set_rules('widget_type','Widget Type','trim|required|max_length[50]');
			$this->form_validation->set_rules('css_class','Css Class','trim|required|max_length[50]'.$is_unique);
			$this->form_validation->set_rules('css_code','Css Code','trim|required');
			if($this->form_validation->run() == false){
				$this->load->view('super/widget/color_class_master/add_color_class_master',$data);
			}
			else{
				$widget_type = $this->input->post('widget_type');
				$css_class = $this->input->post('css_class');
				$css_code = $this->input->post('css_code');
				$data = array(
						'WidgetTypeId' => $widget_type,
						'CSSClass' => $css_class,
						'CssCode' => $css_code
						);
				$this->common_model->update($id,$data,'ColorClassId','Wid_ColorClassMaster');	
				$this->session->set_userdata('msg','Color Class Updated successfully.');
				redirect('widget/manage_color_class_master');
					
			}
	}
	
	function delete_color_class_master(){
		
			$id = base64d($this->uri->segment(3));
			if($id == ''){
					echo 'Please Contact Administrator';
					exit;
			}
			$rData = $this->common_model->compare_in_table('Wid_Widget','ColorClassId',$id);
			if(empty($rData)){
				$this->common_model->delete($id,'ColorClassId','Wid_ColorClassMaster');
				$this->session->set_userdata('msg','Color Class deleted Successfully');
			}
			redirect('widget/manage_color_class_master');
	}
	/****************************** End Color Class Master Section **********************************/
	
	/****************************** Start Widget Area Section **********************************/
	function manage_widget_area(){
		
			$data['widget_area_rec'] = $this->common_model->model_get_all_records('Wid_Area');
			$this->load->view('super/widget/widget_area/manage_widget_area',$data);
	} 
	
	function add_widget_area(){
		
			$this->form_validation->set_rules('widget_area','Widget Area','trim|required|is_unique[Wid_Area.AreaName]|max_length[50]');
			if($this->form_validation->run() == false){
				$this->load->view('super/widget/widget_area/add_widget_area');
			}
			else{
				$widget_area = $this->input->post('widget_area');
				$data = array(
						'AreaName' => $widget_area
						);
				$rData = $this->common_model->insert($data,'Wid_Area');	
				if(is_numeric($rData)){
					$this->session->set_userdata('msg','Widget Area added successfully.');
				}else{
					$this->session->set_userdata('msg',$rData);
				}
				// generate JSON
				$this->_generate_json(); 
				if(isset($_POST['publish'])){
						redirect('widget/add_widget_area');
				}
				else{
						redirect('widget/manage_widget_area');
				}	
			}
	}
	
	function edit_widget_area(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
					echo 'Please Contact Administrator';
					exit;
			}
			$edit_rec = $this->common_model->model_get_by_col('Wid_Area','AreaId',$id);
			//print_r($edit_rec);exit;
			$data['edit_rec'] = $edit_rec;
			$is_unique = '';
			if(strtolower($this->input->post('widget_area')) != strtolower($edit_rec[0]['AreaName'])){
				$is_unique = '|is_unique[Wid_Area.AreaName]';
			}
			$this->form_validation->set_rules('widget_area','Widget Area','trim|required|max_length[50]'.$is_unique);
			if($this->form_validation->run() == false){
				$this->load->view('super/widget/widget_area/add_widget_area',$data);
			}
			else{
				$widget_area = $this->input->post('widget_area');
				$data = array(
						'AreaName' => $widget_area
						);
				$this->common_model->update($id,$data,'AreaId','Wid_Area');	
				$this->session->set_userdata('msg','Widget Area Updated successfully.');
				// generate JSON
				$this->_generate_json(); 
				redirect('widget/manage_widget_area');
					
			}
	}
	
	function delete_widget_area(){
		
			$id = base64d($this->uri->segment(3));
			if($id == ''){
					echo 'Please Contact Administrator';
					exit;
			}
			$rData = $this->common_model->compare_in_table('Wid_Tab','AreaId',$id);
			if(empty($rData)){
				$this->common_model->delete($id,'AreaId','Wid_Area');
				$this->session->set_userdata('msg','Widget Area deleted Successfully');
				// generate JSON
				$this->_generate_json(); 
			}
			redirect('widget/manage_widget_area');
	}
	/****************************** End Widget Area Section **********************************/
	 
	/****************************** Start Widget Tab Section **********************************/
	function manage_widget_tab(){
		
			$data['widget_tab_rec'] = $this->widget_model->get_all_widget_tab_records();
			$this->load->view('super/widget/widget_tab/manage_widget_tab',$data);
	} 
	
	function add_widget_tab(){
		
			$data['widget_area_rec'] = $this->common_model->model_get_all_records('Wid_Area');
			$this->form_validation->set_rules('title','Title','trim|required|is_unique[Wid_Tab.TabTitle]|max_length[50]');
			$this->form_validation->set_rules('area_id','Area','trim|required');
			$this->form_validation->set_rules('icon_color','Icon Color','trim|required|min_length[4]|max_length[7]');
			$this->form_validation->set_rules('userfile','Icon Image','trim|callback__image_required');
			if($this->form_validation->run() == false){
				$this->load->view('super/widget/widget_tab/add_widget_tab',$data);
			}
			else{
				$sorting = $this->common_model->get_max_sorting('Wid_Tab','SortingIndex');
				if(!empty($sorting)){
					$sorting_index = $sorting[0]['SortingIndex'] + 1;
				}
				else{
					$sorting_index = 1;	
				}
				$area_id = $this->input->post('area_id');
				$title = $this->input->post('title');
				$icon_color = $this->input->post('icon_color');
				$image_name = $_FILES['userfile']['name'];
				//echo $image_name;exit;
				if($image_name != ''){
					 
					$path = './uploads/tab_icon/';	//Image upload path
					$size = 200;	//Image size
					$width = '';	//Image width
					$height = '';	//Image height
					
					$image = image_upload($path, $size,$width,$height,$width,$height);	//Image upload function
					//print_r($image);
					$err = ""; 
					if(!empty($image)){
						//foreach($image['entity'] as $files){
							if($image['entity']['status']=="error"){
								echo $err =  $err."Error : ".$image['entity']['message']."<br>";
							}
							 
						//}
					}
				}	
				$data = array(
						'AreaId' => $area_id,
						'TabTitle' => $title,
						'IsActive' => 1,
						'IconColor' => $icon_color,
						'IconImage' => $image_name,
						'SortingIndex' => $sorting_index
						);
				$rData = $this->common_model->insert($data,'Wid_Tab');	
				if(is_numeric($rData)){
					$this->session->set_userdata('msg','Widget Tab added successfully.');
				}else{
					$this->session->set_userdata('msg',$rData);
				}
				// generate JSON
				$this->_generate_json(); 
				if(isset($_POST['publish'])){
						redirect('widget/add_widget_tab');
				}
				else{
						redirect('widget/manage_widget_tab');
				}	
			}
	}
	
	function edit_widget_tab(){
		
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
					echo 'Please Contact Administrator';
					exit;
			}
			$edit_rec = $this->common_model->model_get_by_col('Wid_Tab','TabId',$id);
			//print_r($edit_rec);exit;
			$data['edit_rec'] = $edit_rec;
			$is_unique = '';
			if(strtolower($this->input->post('title')) != strtolower($edit_rec[0]['TabTitle'])){
				$is_unique = '|is_unique[Wid_Tab.TabTitle]';
			}
			$data['widget_area_rec'] = $this->common_model->model_get_all_records('Wid_Area');
			$this->form_validation->set_rules('title','Title','trim|required|max_length[50]'.$is_unique);
			$this->form_validation->set_rules('area_id','Area','trim|required');
			$this->form_validation->set_rules('icon_color','Icon Color','trim|required|min_length[4]|max_length[7]');
			if($this->form_validation->run() == false){
				$this->load->view('super/widget/widget_tab/add_widget_tab',$data);
			}
			else{
				$area_id = $this->input->post('area_id');
				$title = $this->input->post('title');
				$icon_color = $this->input->post('icon_color');
				
				$image_name = $_FILES['userfile']['name'];
				if($image_name != ''){
					 
					unlink_files('./uploads/tab_icon/',$edit_rec[0]['IconImage']);
					$path = './uploads/tab_icon/';	//Image upload path
					$size = 200;	//Image size
					$width = '';	//Image width
					$height = '';	//Image height
					
					$image = image_upload($path, $size,$width,$height,$width,$height);	//Image upload function
					//print_r($image);
					$err = ""; 
					if(!empty($image)){
						//foreach($image['entity'] as $files){
							if($image['entity']['status']=="error"){
								echo $err =  $err."Error : ".$image['entity']['message']."<br>";
							}
							 
						//}
					}
				}
				else{
					$image_name = $edit_rec[0]['IconImage'];	
				}	
				$data = array(
						'AreaId' => $area_id,
						'TabTitle' => $title,
						'IconColor' => $icon_color,
						'IconImage' => $image_name
						);
				$this->common_model->update($id,$data,'TabId','Wid_Tab');	
				$this->session->set_userdata('msg','Widget Tab Updated successfully.');
				// generate JSON
				$this->_generate_json(); 
				redirect('widget/manage_widget_tab');
					
			}
	}
	
	function delete_widget_tab(){
		
			$id = base64d($this->uri->segment(3));
			if($id == ''){
					echo 'Please Contact Administrator';
					exit;
			}
			$rData = $this->common_model->compare_in_table('Wid_TabWidget','TabId',$id);
			if(empty($rData)){
				$tab_rec = $this->common_model->model_get_by_col('Wid_Tab','TabId',$id);
				$this->common_model->delete($id,'TabId','Wid_Tab');
				if($tab_rec[0]['IconImage'] != ''){
					unlink_files('./uploads/tab_icon/',$tab_rec[0]['IconImage']);
				}
				$this->session->set_userdata('msg','Widget Tab deleted Successfully');
				// generate JSON
				$this->_generate_json(); 
			}
			redirect('widget/manage_widget_tab');
	}
	/****************************** End Color Class Master Section **********************************/
	
	
	/****************************** Start Widget Section **********************************/
	function manage_widget(){
		
			$data['widget_rec'] = $this->widget_model->get_all_widget_records();
			$this->load->view('super/widget/widget/manage_widget',$data);
	} 
	
	function add_widget(){
		
			$data['color_classes'] = '';
			$widget_type_id = (int)$this->input->post('widget_type_id');
			if($widget_type_id>0){
				$data['color_classes'] = $this->common_model->model_get_by_col('Wid_ColorClassMaster','WidgetTypeId',$widget_type_id);	
			}
			$data['widget_type_rec'] = $this->common_model->model_get_all_records('Wid_WidgetType');
			$this->form_validation->set_rules('title','Title','trim|required|is_unique[Wid_Widget.WidgetTitle]|max_length[50]');
			$this->form_validation->set_rules('widget_type_id','Widget Type','trim|required');
			$this->form_validation->set_rules('info_text','Info Text','trim|required|max_length[150]');
			$this->form_validation->set_rules('sp_name','SP Name','trim|required|max_length[50]');
			$this->form_validation->set_rules('click_url','Click Url','trim|max_length[250]');
			$this->form_validation->set_rules('color_class_id','Color Class','trim|required');
			$this->form_validation->set_rules('userfile','Widget Icon','trim|callback__image_required');
			if($this->form_validation->run() == false){
				$this->load->view('super/widget/widget/add_widget',$data);
			}
			else{
				$title = $this->input->post('title');
				$widget_type_id = $this->input->post('widget_type_id');
				$info_text = $this->input->post('info_text');
				$sp_name = $this->input->post('sp_name');
				$click_url = $this->input->post('click_url');
				$color_class_id = $this->input->post('color_class_id');
				$image_name = $_FILES['userfile']['name'];
				if($image_name != ''){
					 
					$path = './uploads/widget_icon/';	//Image upload path
					$size = 200;	//Image size
					$width = '';	//Image width
					$height = '';	//Image height
					
					$image = image_upload($path, $size,$width,$height,$width,$height);	//Image upload function
					//print_r($image);
					$err = ""; 
					if(!empty($image)){
						//foreach($image['entity'] as $files){
							if($image['entity']['status']=="error"){
								echo $err =  $err."Error : ".$image['entity']['message']."<br>";
							}
							 
						//}
					}
				}	
				$data = array(
						'WidgetTitle' => $title,
						'WidgetTypeId' => $widget_type_id,
						'InfoText' => $info_text,
						'SPName' => $sp_name,
						'ClickURL' => $click_url,
						'ColorClassId' => $color_class_id,
						'WidgetIcon' => $image_name
						);
				$rData = $this->common_model->insert($data,'Wid_Widget');	
				if(is_numeric($rData)){
					$this->session->set_userdata('msg','Widget added successfully.');
				}else{
					$this->session->set_userdata('msg',$rData);
				}
				// generate JSON
				//$this->_generate_json(); 
				if(isset($_POST['publish'])){
						redirect('widget/add_widget');
				}
				else{
						redirect('widget/manage_widget');
				}	
			}
	}
	
	function _image_required(){
		
			$image_name = $_FILES['userfile']['name'];
			if($image_name == ''){
				$this->form_validation->set_message('_image_required','Icon Image is required');
				return false;
			}
			else{
				return true;	
			}
	}
	
	function edit_widget(){
			$err = ""; 
			$id = (int)base64d($this->uri->segment(3));
			if($id <= 0){
					echo 'Please Contact Administrator';
					exit;
			}
			$edit_rec = $this->common_model->model_get_by_col('Wid_Widget','WidgetId',$id);
			//print_r($edit_rec);exit;
			$data['edit_rec'] = $edit_rec;
			$data['color_classes'] = '';
			$widget_type_id = $edit_rec[0]['WidgetTypeId'];
			if($widget_type_id>0){
				$data['color_classes'] = $this->common_model->model_get_by_col('Wid_ColorClassMaster','WidgetTypeId',$widget_type_id);	
			}
			if(strtolower($this->input->post('title')) == strtolower($edit_rec[0]['WidgetTitle'])){
				$is_unique = '';	
			}
			else{
				$is_unique = '|is_unique[Wid_Widget.WidgetTitle]';	
			}
			$data['widget_type_rec'] = $this->common_model->model_get_all_records('Wid_WidgetType');
			$this->form_validation->set_rules('title','Title','trim|required|max_length[50]'.$is_unique);
			$this->form_validation->set_rules('widget_type_id','Widget Type','trim|required');
			$this->form_validation->set_rules('info_text','Info Text','trim|required|max_length[150]');
			$this->form_validation->set_rules('sp_name','SP Name','trim|required|max_length[50]');
			$this->form_validation->set_rules('click_url','Click Url','trim|max_length[250]');
			$this->form_validation->set_rules('color_class_id','Color Class','trim|required');
			if($this->form_validation->run() == false){
				$this->load->view('super/widget/widget/add_widget',$data);
			}
			else{
				$title = $this->input->post('title');
				$widget_type_id = $this->input->post('widget_type_id');
				$info_text = $this->input->post('info_text');
				$sp_name = $this->input->post('sp_name');
				$click_url = $this->input->post('click_url');
				$color_class_id = $this->input->post('color_class_id');
				
				$image_name = $_FILES['userfile']['name'];
				if($image_name != ''){
					 
					unlink_files('./uploads/widget_icon/',$edit_rec[0]['WidgetIcon']);
					$path = './uploads/widget_icon/';	//Image upload path
					$size = 200;	//Image size
					$width = '';	//Image width
					$height = '';	//Image height
					
					$image = image_upload($path, $size,$width,$height,$width,$height);	//Image upload function
					//print_r($image);exit;
					
					if(!empty($image)){
						//foreach($image['entity'] as $files){
							if($image['entity']['status']=="error"){
								 $err =  $err."Error : ".$image['entity']['message']."<br>";
							}
							 
						//}
					}
				}
				else{
					$image_name = $edit_rec[0]['WidgetIcon'];	
				}	
				$data = array(
						'WidgetTitle' => $title,
						'WidgetTypeId' => $widget_type_id,
						'InfoText' => $info_text,
						'SPName' => $sp_name,
						'ClickURL' => $click_url,
						'ColorClassId' => $color_class_id,
						'WidgetIcon' => $image_name
						);
				$rData = $this->common_model->update($id,$data,'WidgetId','Wid_Widget');	
				if(is_numeric($rData)){
					if($err != ''){
						$this->session->set_userdata('msg',$err);
					}
					else{
						$this->session->set_userdata('msg','Widget updated successfully.');
						}
					// generate JSON
					$this->_generate_json(); 
				}else{
					$this->session->set_userdata('msg',$rData);
				}
				redirect('widget/manage_widget');
				
			}
	}
	
	function delete_widget(){
		
			$id = base64d($this->uri->segment(3));
			if($id == ''){
					echo 'Please Contact Administrator';
					exit;
			}
			$rData = $this->common_model->compare_in_table('Wid_TabWidget','WidgetId',$id);
			if(empty($rData)){
				$widget_rec = $this->common_model->model_get_by_col('Wid_Widget','WidgetId',$id);
				$this->common_model->delete($id,'WidgetId','Wid_Widget');
				if($widget_rec[0]['WidgetIcon'] != ''){
					unlink_files('./uploads/widget_icon/',$widget_rec[0]['WidgetIcon']);
					// generate JSON
					//$this->_generate_json(); 
				}
				$this->session->set_userdata('msg','Widget deleted Successfully');
			}
			redirect('widget/manage_widget');
	}
	/****************************** End Widget Section **********************************/
	 
	 
	function getColorClasses(){
		
			$id = (int)$this->uri->segment(3);
			$color_classes = $this->common_model->model_get_by_col('Wid_ColorClassMaster','WidgetTypeId',$id);
			$data = '<option value="">Select Color Class</option>';
			foreach($color_classes as $rec){
				$data .= '<option value="'.$rec['ColorClassId'].'">'.$rec['CSSClass'].'</option>';	
			}
			$rData['color_classes'] = $data;
			echo json_encode($rData);
	} 
	 
	function reloadColorClasses(){
		
			$id = (int)$this->uri->segment(3);
			$color_id = (int)$this->uri->segment(4);
			$color_classes = $this->common_model->model_get_by_col('Wid_ColorClassMaster','WidgetTypeId',$id);
			$data = '<option value="">Select Color Class</option>';
			foreach($color_classes as $rec){
				$selected = '';
				if($rec['ColorClassId'] == $color_id){
					$selected = 'selected=selected';
				}
				$data .= '<option '.$selected.' value="'.$rec['ColorClassId'].'">'.$rec['CSSClass'].'</option>';	
			}
			$rData['color_classes'] = $data;
			echo json_encode($rData);
	} 
	 
	 function update_status(){
		 
		$id = (int)base64d($this->uri->segment(3));	
		if($id <= 0){
			echo 'Please Contact Administrator';
			exit;
		}
		$is_active = $this->uri->segment(4);	
		$table = $this->uri->segment(5);	
		$col = $this->uri->segment(6);	
		$redirect = $this->uri->segment(7);	
		if($is_active == 1){
			$is_active = 0;	
		}
		else{
			$is_active = 1;	
		}
		$data = array('IsActive' => $is_active);
		$rData = $this->common_model->update($id,$data,$col,$table);	
		if(is_numeric($rData)){
			$this->session->set_userdata('msg','Status Updated successfully.');
		}
		else{
			$this->session->set_userdata('msg',$rData);
		}
		// generate JSON
		$this->_generate_json(); 
		redirect('widget/'.$redirect);
	}
	
	public function update_sorting()
	{
		
		$table= $this->uri->segment(3);
		$colum_name= $this->uri->segment(4);
		
		$id = $_POST['ids'];
		//print_r($id);
		$count = count($id);
		for ($s = 0; $s < $count; $s++){
			$y = $s+1;
			$data = array('SortingIndex'=>$y);
			
			$result = $this->common_model->update($id[$s], $data, $colum_name, $table);   //query to Update data 			
			echo json_encode(array("success" => $result)); 
		}
		
	} 
	 
	/****************************** Widget tab map ******************************/ 
	function manage_widget_tab_map(){
		
		$data['widget_tab_map_rec'] = array();
		$data['widget_rec'] = array();
		$tab_id_enc = $this->uri->segment(3);
		$tab_id = (int)base64d($tab_id_enc);
		if($tab_id>0){
			$wid_tab_map_by_tab_id = $this->widget_model->get_wid_by_tab_id($tab_id);	
			if(!empty($wid_tab_map_by_tab_id)){
				$x = count($wid_tab_map_by_tab_id);
				for($i=0;$i<$x;$i++){
					$id[$i] = $wid_tab_map_by_tab_id[$i]['WidgetId'];	
				}
				$data['widget_rec'] = $this->widget_model->get_all_widgets($id);
			}
			else{
				$data['widget_rec'] = $this->common_model->model_get_all_records('Wid_Widget');
				}
			$data['widget_tab_map_rec'] = $this->widget_model->get_widget_tab_map($tab_id);
		}
		$data['tab_area_rec'] = $this->widget_model->get_all_tab_area_records();
		$this->form_validation->set_rules('tab_id','Tab','trim|required');
		$this->form_validation->set_rules('widget_id[]','Widget','trim|required');
		if($this->form_validation->run() == false){
			$this->load->view('super/widget/widget_tab_map/manage_widget_tab_map',$data);	
		}
		else{
			$tab_id = base64d($this->input->post('tab_id')); 
			$widget_id = $this->input->post('widget_id'); 
			$c = count($widget_id);
			for($y=0;$y<$c;$y++){
				$sorting_rec = $this->widget_model->get_max_sorting($tab_id);
				if(!empty($sorting_rec)){
					$sorting_index = $sorting_rec[0]['SortingIndex'] + 1;	
				}
				else{
					$sorting_index = 1;
					}
				$data = array(
						'TabId' => $tab_id,	
						'WidgetId' => $widget_id[$y],	
						'IsActive' => 1,
						'SortingIndex' => $sorting_index	
						);
				$rData = $this->common_model->insert($data,'Wid_TabWidget');	
			}	
			if(is_numeric($rData)){
				$this->session->set_userdata('msg','data inserted successfully');
			}
			else{
				$this->session->set_userdata('msg',$rData);
			}
			// generate JSON
			$this->_generate_json(); 
			redirect('widget/manage_widget_tab_map/'.$tab_id_enc);
		}
	}
	
	function update_tab_wid_map_status(){
		 
		$id = (int)base64d($this->uri->segment(3));	
		if($id <= 0){
			echo 'Please Contact Administrator';
			exit;
		}
		$is_active = $this->uri->segment(4);	
		$tab_id = $this->uri->segment(5);	
		if($is_active == 1){
			$is_active = 0;	
		}
		else{
			$is_active = 1;	
		}
		$data = array('IsActive' => $is_active);
		$rData = $this->common_model->update($id,$data,'TabWidgetId','Wid_TabWidget');	
		if(is_numeric($rData)){
			$this->session->set_userdata('msg','Status Updated successfully.');
		}
		else{
			$this->session->set_userdata('msg',$rData);
		}
		// generate JSON
		$this->_generate_json(); 
		redirect('widget/manage_widget_tab_map/'.$tab_id);
	}
	
	function delete_widget_tab_map(){
		
			$id = base64d($this->uri->segment(3));
			$tab_id = $this->uri->segment(4);
			if($id == ''){
					echo 'Please Contact Administrator';
					exit;
			}
			$rData = $this->common_model->compare_in_table('Wid_TabWidgetUserRoleType','TabWidgetId',$id);
			if(empty($rData)){
				$this->common_model->delete($id,'TabWidgetId','Wid_TabWidget');
				$this->session->set_userdata('msg','Widget Tab mapping deleted Successfully');
				// generate JSON
				$this->_generate_json(); 
			}
			redirect('widget/manage_widget_tab_map/'.$tab_id);
	}
	
	
	/****************************** Widget tab user permission ******************************/ 
	function manage_widget_tab_user_role(){
		
		$data['widget_tab_user_role_rec'] = array();
		$data['widget_tab_map_rec'] = array();
		$user_type_id_enc = $this->uri->segment(3);
		$user_type_id = (int)base64d($user_type_id_enc);
		if($user_type_id>0){
			$wid_tab_map_by_user_type_id = $this->common_model->model_get_by_col('Wid_TabWidgetUserRoleType','UserRoleTypeId',$user_type_id);	
			//print_r($wid_tab_map_by_user_type_id);exit;
			if(!empty($wid_tab_map_by_user_type_id)){
				$x = count($wid_tab_map_by_user_type_id);
				for($i=0;$i<$x;$i++){
					$id[$i] = $wid_tab_map_by_user_type_id[$i]['TabWidgetId'];	
				}
				
				$data['widget_tab_map_rec'] = $this->widget_model->get_widget_tab_map_for_user_type($id);
			//print_r($data['widget_tab_map_rec']);exit;
			}
			else{
				$data['widget_tab_map_rec'] = $this->widget_model->get_widget_tab_map_for_user_type();
				//print_r(count($data['widget_tab_map_rec']));exit;
				}
			$data['widget_tab_user_role_rec'] = $this->widget_model->get_widget_tab_map_user_role($user_type_id);
		}
		$data['user_role_rec'] = $this->common_model->model_get_all_records('UserRoleType');
		$this->form_validation->set_rules('user_role','User Role','trim|required');
		$this->form_validation->set_rules('wid_tab_map_id[]','Widget Tab','trim|required');
		if($this->form_validation->run() == false){
			$this->load->view('super/widget/tab_widget_user_role_type/manage_tab_widget_user_role_type',$data);	
		}
		else{
			$user_role = base64d($this->input->post('user_role')); 
			$wid_tab_map_id = $this->input->post('wid_tab_map_id'); 
			//print_r($wid_tab_map_id);exit;
			$c = count($wid_tab_map_id);
			for($y=0;$y<$c;$y++){
				$data = array(
							'UserRoleTypeId' => $user_role,	
							'TabWidgetId' => $wid_tab_map_id[$y]
							);
				$rData = $this->common_model->insert($data,'Wid_TabWidgetUserRoleType');
			}	
			if(is_numeric($rData)){
				$this->session->set_userdata('msg','data inserted successfully');
			}
			else{
				$this->session->set_userdata('msg',$rData);
			}
			 //generate JSON
			 $this->_generate_json(); 
			redirect('widget/manage_widget_tab_user_role/'.$user_type_id_enc);
		}
	}
	
	function delete_widget_tab_user_role(){
		
			$id = base64d($this->uri->segment(3));
			$user_role_id = $this->uri->segment(4);
			if($id == ''){
					echo 'Please Contact Administrator';
					exit;
			}
			$this->common_model->delete($id,'TabWidgetUserRoleTypeId','Wid_TabWidgetUserRoleType');
			$this->session->set_userdata('msg','Tab Widget User role type mapping deleted Successfully');
			//generate JSON
			$this->_generate_json();
			redirect('widget/manage_widget_tab_user_role/'.$user_role_id);
	}
	
	/*********************** Generate JSON for widgets *****************************/
	function _generate_json(){
			// get user role types
			$user_role_type_rec = $this->common_model->model_get_all_records('UserRoleType');	
			$i = 0;
			$isActive = 1;
			$rData = array();
			foreach($user_role_type_rec as $role_type_rec){
					$rData[$i]['UserRoleTypeId'] = $role_type_rec['UserRoleTypeId'];
					$rData[$i]['UserRoleType'] = $role_type_rec['UserRoleType'];
					// get area by user role types id
					$wid_area_rec = $this->widget_model->get_area_by_role_type($role_type_rec['UserRoleTypeId']);
					$x = 0;
					$areaData = array();
					foreach($wid_area_rec as $wid_area){
						$areaData[$x]['AreaId'] = $wid_area['AreaId']; 	
						$areaData[$x]['AreaName'] = $wid_area['AreaName']; 	
						// get tabs by user role types and area id
						$wid_tab_rec = $this->widget_model->get_tabs_by_role_type_area_id($role_type_rec['UserRoleTypeId'],$wid_area['AreaId']);
						$y = 0;
						$tabData = array();
						foreach($wid_tab_rec as $wid_tab){
							$tabData[$y]['TabId'] = $wid_tab['TabId']; 	
							$tabData[$y]['TabTitle'] = $wid_tab['TabTitle']; 	
							$tabData[$y]['IconImage'] = $wid_tab['IconImage']; 	
							$tabData[$y]['IconColor'] = $wid_tab['IconColor']; 	
							$tabData[$y]['SortingIndex'] = $wid_tab['SortingIndex']; 	
							// get widgets by user role types and tab id
							$widget_rec = $this->widget_model->get_widgets_by_role_type_tab_id($role_type_rec['UserRoleTypeId'],$wid_tab['TabId']);
							$z = 0;
							$widgetData = array();
							foreach($widget_rec as $widget){
								$widget_icon = '';
								if(trim($widget['WidgetIcon']) != ''){
									$widget_icon = base_url().'uploads/widget_icon/'.$widget['WidgetIcon'];	
								}
								$widgetData[$z]['WidgetTitle'] = $widget['WidgetTitle']; 	
								$widgetData[$z]['WidgetIcon'] = $widget_icon; 	
								$widgetData[$z]['InfoText'] = $widget['InfoText']; 	
								$widgetData[$z]['SPName'] = $widget['SPName']; 	
								$widgetData[$z]['ClickURL'] = $widget['ClickURL']; 	
								$widgetData[$z]['CSSClass'] = $widget['CSSClass']; 	
								$widgetData[$z]['CssCode'] = $widget['CssCode']; 	
								$widgetData[$z]['WidgetType'] = $widget['WidgetType']; 	
								$widgetData[$z]['HTMLTemplateTitle'] = $widget['HTMLTemplateTitle']; 	
								$widgetData[$z]['HTMLTemplate'] = $widget['HTMLTemplate']; 	
								 
							}
							$tabData[$z]['Widget'] = $widgetData;
							$z++;
						}
						$areaData[$y]['Tab'] = $tabData;
						$y++;
					}
					$rData[$i]['Area'] = $areaData;
					$i++;
					
			}
			//echo '<pre>';
			//print_r($rData);exit;
					
			/*********** JSON ****************/
			$json_folder_name = $this->config->item('json_folder_name');
			$file_name = 'widget.json';
			$file_path = $json_folder_name.'/'.$file_name;
			$status_msg = generate_upload_json($rData,$file_name,$file_path);
			$this->session->set_userdata('json_msg', $status_msg); 
			 
	}
	function _generate_json_backup(){
			// get area
			$wid_area = $this->common_model->model_get_all_records('Wid_Area');	
			$i = 0;
			$isActive = 1;
			$rData = array();
			foreach($wid_area as $area_rec){
				$rData[$i]['AreaId'] = $area_rec['AreaId'];
				$rData[$i]['AreaName'] = $area_rec['AreaName'];
				// get tabs by area id
				$wid_tab = $this->common_model->model_get_by_col('Wid_Tab','AreaId',$area_rec['AreaId'],$isActive);
				$x = 0;
				$tabData = array();
				foreach($wid_tab as $tab_rec){
					$tabData[$x]['TabId'] = $tab_rec['TabId']; 	
					$tabData[$x]['TabTitle'] = $tab_rec['TabTitle']; 	
					$tabData[$x]['IconImage'] = $tab_rec['IconImage']; 	
					$tabData[$x]['IconColor'] = $tab_rec['IconColor']; 	
					$tabData[$x]['SortingIndex'] = $tab_rec['SortingIndex']; 	
					$tabData[$x]['IsActive'] = $tab_rec['IsActive']; 	
					// get tab widgets maping by tab id
					$tab_wid_map = $this->common_model->model_get_by_col('Wid_TabWidget','TabId',$tab_rec['TabId'],$isActive);
					$y = 0;
					$widgetData = array();
					foreach($tab_wid_map as $tab_wid_map_rec){
						// get Tab Widget UserRoleType by mapping ids
						$wid_permissions = $this->common_model->model_get_by_col('Wid_TabWidgetUserRoleType','TabWidgetId',$tab_wid_map_rec['TabWidgetId']);
						$u = 0;
						$userData = array();
						foreach($wid_permissions as $permissions_rec){
							// get permissions by Tab Widget UserRoleType ids
							$user_role_type = $this->common_model->model_get_by_col('UserRoleType','UserRoleTypeId',$permissions_rec['UserRoleTypeId']);	
							$userData[$u]['UserRoleTypeId'] = $user_role_type[0]['UserRoleTypeId'];	
							$userData[$u]['UserRoleType'] = $user_role_type[0]['UserRoleType'];	
							$u++;
						
						}	
						// get widgets by widget id
						$widget_rec = $this->widget_model->get_all_widget_records($tab_wid_map_rec['WidgetId']);	
						if(!empty($widget_rec)){
							$widgetData[$y]['WidgetId'] = $widget_rec[0]['WidgetId']; 		
							$widgetData[$y]['WidgetTypeId'] = $widget_rec[0]['WidgetTypeId']; 		
							$widgetData[$y]['WidgetTitle'] = $widget_rec[0]['WidgetTitle']; 		
							$widgetData[$y]['WidgetIcon'] = $widget_rec[0]['WidgetIcon']; 		
							$widgetData[$y]['InfoText'] = $widget_rec[0]['InfoText']; 		
							$widgetData[$y]['SPName'] = $widget_rec[0]['SPName']; 		
							$widgetData[$y]['ColorClassId'] = $widget_rec[0]['ColorClassId']; 	
							$widgetData[$y]['CSSClass'] = $widget_rec[0]['CSSClass']; 	
							$widgetData[$y]['Permissions'] = $userData; 	
							
							$y++;
						}
					}
					
				$tabData[$x]['Widget'] = $widgetData;
				$x++;	
				}
				
				$rData[$i]['Tabs'] = $tabData;
				$i++;
			}
			
			/*********** JSON ****************/
			$json_folder_name = $this->config->item('json_folder_name');
			$file_name = 'widget.json';
			$file_path = $json_folder_name.'/'.$file_name;
			$status_msg = generate_upload_json($rData,$file_name,$file_path);
			$this->session->set_userdata('json_msg', $status_msg); 
			 
	}
		
	function generate_html_template_json(){
		
			$rData = array();
			$html_template_rec = $this->widget_model->get_active_html_templates();
			$x = 0;
			foreach($html_template_rec as $rec){
				$rData[$x]['HTMLTemplateId'] = $rec['HTMLTemplateId']; 	
				$rData[$x]['HTMLTemplateTitle'] = $rec['HTMLTemplateTitle']; 	
				$rData[$x]['HTMLTemplate'] = $rec['HTMLTemplate']; 	
				$x++;
			}
			$json_folder_name = $this->config->item('json_folder_name');
			$file_name = 'html_template.json';
			$file_path = $json_folder_name.'/'.$file_name;
			echo generate_upload_json($rData,$file_name,$file_path);
	}	
	function generate_color_class_master_json(){
		
			$rData = array();
			$color_class_master_rec = $this->common_model->model_get_all_records('Wid_ColorClassMaster');
			$x = 0;
			foreach($color_class_master_rec as $rec){
				$rData[$x]['ColorClassId'] = $rec['ColorClassId']; 	
				$rData[$x]['CSSClass'] = $rec['CSSClass']; 	
				$rData[$x]['WidgetTypeId'] = $rec['WidgetTypeId']; 	
				$rData[$x]['CssCode'] = $rec['CssCode']; 	
				$x++;
			}
			$json_folder_name = $this->config->item('json_folder_name');
			$file_name = 'color_class_master.json';
			$file_path = $json_folder_name.'/'.$file_name;
			echo generate_upload_json($rData,$file_name,$file_path);
	}	
	/*
	function _generate_upload_json($rData,$file_name,$file_path){
		
			if(!empty($rData)){
				$data = json_encode($rData);
				if(file_exists($file_path)){
					unlink($file_path);
					$myfile = fopen($file_path, "w") or die("Unable to open file!");
					fwrite($myfile, $data);
					fclose($myfile);
				}
				else{
					$myfile = fopen($file_path, "w") or die("Unable to open file!");
					fwrite($myfile, $data);
					fclose($myfile);
				}
				// connect and login to FTP server
				$status = check_valid_ftp_connection($file_path,$file_name);
				
				return $status;	
				//$this->session->set_userdata('json_msg', $msg); 
			}
	}
	*/	
}
?>	
