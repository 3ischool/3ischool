<?php
class Common extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		 
		$this->load->helper('cookie');
	}

    function set_paging(){
			$cookieVal = (int)$this->uri->segment(3);
			if($cookieVal>=1){
				$cookie= array(
					  'name'   => 'digipagingpagesize',
					  'value'  => $cookieVal,
					   'expire' => '86500',
				  );
				  set_cookie($cookie);
				  echo 1;
			}else{
					echo 0;
			}
			 
	} 
	
	
	
	function form_builder_setup(){
			$this->load->view('super/formbuildersetup');
	}
		
}
?>	
