<?php
class User extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('supportUser/support_user_model');
		 
	}
 
	function index(){
		$this->login();
		}
	 
	function login(){
		is_already_logged_in();
		$data['error'] = ""; 
		 
		$this->form_validation->set_rules('usr', 'Email Id', 'trim|required|valid_email|max_length[255]');
		$this->form_validation->set_rules('pwd', 'Password', 'trim|required|min_length[8]|callback__validate_login_credentials');
		 
		if($this->form_validation->run()==false){
			$this->load->view('super/login',$data);
		}
		else{
			 
			redirect('user/dashboard','refresh');
			}	
		} 
	
	function _validate_login_credentials($str){
			if($str!=''){
				$pwd = md5($str);
			}else{
					 $this->form_validation->set_message('_validate_login_credentials', 'Password field is required');
                        return FALSE;
			}
			$usr = $this->input->post('usr');			
			$result = $this->support_user_model->validate_login($usr,$pwd);
			
			if (empty($result))
                {
                        $this->form_validation->set_message('_validate_login_credentials', 'Invalid login details');
                        return FALSE;
                }
                else
                {
						$domain_valid = '';
						$cookie_secure = false;
						
						$cookie = array(
						'name'   => 's_ui',
						'value'  => base64e($result[0]['Id']),
						'expire' => '2595000', //one month expiry
						'domain' => $domain_valid,			 
						'secure' => $cookie_secure
						);
						$this->input->set_cookie($cookie);
						
						$cookie = array(
						'name'   => 's_ue',
						'value'  => base64e($result[0]['UserEmail']),
						'expire' => '2595000', //one month expiry
						'domain' => $domain_valid,			 
						'secure' => $cookie_secure
						);
						$this->input->set_cookie($cookie);
						
						$cookie = array(
						'name'   => 's_un',
						'value'  => base64e($result[0]['UserName']),
						'expire' => '2595000', //one month expiry
						'domain' => $domain_valid,			 
						'secure' => $cookie_secure
						);
						$this->input->set_cookie($cookie);
						
						 
						
                        return TRUE;
                }
			
			
	}
	
	function dashboard(){
		is_logged_in();
		$this->load->view('super/dashboard');
		}
		
	function logout(){
		delete_cookie('s_ui');
		delete_cookie('s_ue');
		delete_cookie('s_un');
		redirect('user/login','refresh');
		}		
}
?>	
