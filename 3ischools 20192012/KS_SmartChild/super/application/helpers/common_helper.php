<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function validate_gstin($gstin,$stateCode){
	$errorCount  = 0;
	$errorMessage  = '';
	$responseType = "";
	$validGSTIN = "";
	$pan = "";
	$returnResponse = array();
	$gstin = trim($gstin);	
	$gstin = str_replace(" ","",$gstin);
	$gstin = str_replace(".","",$gstin);
	$gstin = strtoupper($gstin);
	$gstinLen = strlen($gstin);
	if($gstinLen==15){
		$StateCodeEntered = substr($gstin,0,2);
		$AlphaBlock1 = substr($gstin,2,5);
		$NumericBlock2 = substr($gstin,7,4);
		$AlphaBlock3 = substr($gstin,11,1);
		$AlpaNumricBlock4 = substr($gstin,11,3);		
		if(!is_numeric($StateCodeEntered)){
			$errorCount++;
			$errorMessage .= "char[1-2] Invalid state code<br>"; 	
		}
		if($stateCode!=''){
			if($stateCode!=$StateCodeEntered){
			  $errorCount++;
			  $errorMessage .= "char[1-2] State code doesn't match with state selected in address<br>"; 				
			}
		}
		if(!ctype_alpha($AlphaBlock1)){
			$errorCount++;
			$errorMessage .= "char[3-7] should be alphabet only<br>"; 
		}	
		if(!is_numeric($NumericBlock2)){
			$errorCount++;
			$errorMessage .= "char[8-11] should be numeric only<br>"; 
		}
		if(!ctype_alpha($AlphaBlock3)){
			$errorCount++;
			$errorMessage .= "char[12] should be alphabet only<br>";
		}	
		if(!ctype_alnum($AlpaNumricBlock4)){
			$errorCount++;
			$errorMessage .= "char[13-15] should be alpha-numeric only<br>";
		}
		
		if($errorCount==0){
			$ci =& get_instance();
			$ci->load->model('common_model');
			$Check_if_exist = $ci->common_model->model_get_by_col('GSPCredentials','GSTIN',$gstin);
			if(!empty($Check_if_exist)){
				$responseType = "error";
				$errorMessage = "GSTIN - ".$gstin." already exist with other account";
			}else{
				$responseType = "success"; 	
				$pan = 	substr($gstin,2,10);
				$validGSTIN = $gstin;	
			}
					
		}
		else{
			$responseType = "error";
			 
		}
		
	}else{
		$responseType = "error";
		$errorMessage  = "Exact Length of GSTIN should be 15";
	}	
	$returnResponse['status'] = $responseType;
	$returnResponse['message'] = $errorMessage;	
	$returnResponse['GSTIN'] = $validGSTIN;
	$returnResponse['pan'] = $pan;
	return json_encode($returnResponse);
}
 

function return_credentials($db_prefix, $dbuser_prefix, $no){
	 
		$number = str_pad($no, 5, '0', STR_PAD_LEFT);
		$dbname = $db_prefix.''.$number;
		$dbuser = $dbuser_prefix.''.$number;
		$length = 12;
		$alphabets = range('A','Z');
		$numbers = range('0','9');
		$additional_characters = array('_','&','@','#','$','%','*');
		$final_array = array_merge($alphabets,$numbers,$additional_characters);
		$password = '';
	  
		while($length--) {
		  $key = array_rand($final_array);
		  $password .= $final_array[$key];
		}
	    $return_array = array(
						'dbname' => $dbname,
						'dbuser' => $dbuser,
						'password' => $password
						);
		return $return_array;
}

function check_valid_ftp_connection($file_path,$file_name){
		 
		$ci =& get_instance();
		$ci->load->library('ftp');
		 
		$destination_path = $ci->config->item('ftp_uploaded_path');
		$json_folder_name = $ci->config->item('json_folder_name');
		
		$config['hostname'] = $ci->config->item('ftp_server');
		$config['username'] = $ci->config->item('ftp_uname');
		$config['password'] = $ci->config->item('ftp_password');
		$config['port'] = $ci->config->item('ftp_port');
		$config['debug'] = false;

		$ci->ftp->connect($config);

		$fileUploadStatus = $ci->ftp->upload($file_path, $file_name, 'ascii');
		$fileUploadStatus1 = (int)$fileUploadStatus;
		
		$ci->ftp->close();
		
		if($fileUploadStatus1==1){
			$returnData = 1;
		}else{
			$returnData = 'Error in FTP';
		}
		return $returnData;
		
}

function generate_upload_json($rData,$file_name,$file_path){
		  
			if(!empty($rData)){
				$data = json_encode($rData);
				if(file_exists($file_path)){
					unlink($file_path);					 
				} 
				$myfile = fopen($file_path, "w") or die("Unable to open file!");
				fwrite($myfile, $data);
				fclose($myfile);
				// connect and login to FTP server
				$status = check_valid_ftp_connection($file_path,$file_name);
				
				return $status;	
				//$this->session->set_userdata('json_msg', $msg); 
			}
	}

?>
