<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * image_upload()
 *
 * @author Sachin Garg <sachin.garg@digitechsoft.com>
 * @owner Digitech Software Solutions Pvt Ltd
 * @param string $target_path -- Required
 * @param int $max_allowed_size -- Required
 * @param int $max_width -- optional
 * @param int $max_height -- optional
 * @param int $min_width -- optional
 * @param int $min_height -- optional 
 * @return array
 *
 */

function image_upload($target_path,$max_allowed_size,$max_width,$max_height,$min_width,$min_height){
 
	$CI =& get_instance();
	$response = array(
		'upload_status' => "",
		'upload_message' => "",
		'entity' => array() 
	); //set an empty erray
	$image_names = array(); //set an empty erray
	if(empty($target_path)){ // Validation for Target Path 
		$response['upload_status'] = "error";
		$response['upload_message'] = "target path not defined";
	}	 
	else if(empty($max_allowed_size)){ // Validation for Max allowed Size 
		$response['upload_status'] = "error";
		$response['upload_message'] = "max allowed size not defined";
	}
	else if(!is_numeric($max_allowed_size)){ // Validation for Max allowed Size
		$response['upload_status'] = "error";
		$response['upload_message'] = "max allowed size is not valid";
	}	
	else{
		if (!is_dir($target_path)) { // creating the target path directory if not exist
			mkdir($target_path, 0777, TRUE);
		}
		/* Image uploading config setup */
		$config['upload_path'] = $target_path;   
		$config['allowed_types'] = 'jpg|png|gif|jpeg';  
        $config['max_size'] = $max_allowed_size;
		
		if(!empty($max_width)){
			$config['max_width'] = $max_width;
		}
		if(!empty($max_height)){
			$config['max_height'] = $max_height;
		}
		if(!empty($min_width)){
			$config['min_width'] = $min_width;
		}
		if(!empty($min_height)){
			$config['min_height'] = $min_height;
		}
		/* //Image uploading config setup */
		
		/* loading file upload library */		
        $CI->load->library('upload', $config);
		/* //loading file upload library */
		
		$filecount = count($_FILES['userfile']['size']); // Total Uploaded file count 
		$value = $_FILES;
		 
		$error = 0;	
		 $fileStatus = array();
		 //for ($s = 0; $s <= $filecount - 1; $s++)
		 //{
		 	$_FILES['userfile']['name'] = $value['userfile']['name'];
            $_FILES['userfile']['type'] = $value['userfile']['type'];
            $_FILES['userfile']['tmp_name'] = $value['userfile']['tmp_name'];
            $_FILES['userfile']['error'] = $value['userfile']['error'];
            $_FILES['userfile']['size'] = $value['userfile']['size'];
			
			if (!$CI->upload->do_upload('userfile')) {
				$error++;
				 	
				$fileStatus['status'] = "error";
				$fileStatus['message'] = $_FILES['userfile']['name'].' - '.strip_tags($CI->upload->display_errors());		 
			}
			else{
				$data = $CI->upload->data();	
				$fileStatus['status'] = "success";
				$fileStatus['message'] = $data['file_name'];
				$image_resize = image_resize($target_path,$data['file_name'],'backend_thumb','150','150',false);			 
						
			}							
		//}
		
		if($error==0){
			$response['upload_status'] = "success";
			$response['upload_message'] = "All files uploaded successfully";
		}else{
			$response['upload_status'] = "error";
			$response['upload_message'] = $error." file(s) having error while uploading";
		}
		$response['entity'] = $fileStatus;
	}
return $response;	
}



function image_resize($source_path,$image_name,$output_folder_name,$width,$height,$maintain_ration){
 
	$CI =& get_instance();
	$response = array(); //set an empty erray
	$image_names = array(); //set an empty erray
	$outputDirectory = $source_path.'/'.$output_folder_name;
	$source_image = $source_path.'/'.$image_name;
	if (file_exists($source_image) && is_file($source_image)){
		if (!is_dir($outputDirectory)) { // creating the target path directory if not exist
				mkdir($outputDirectory, 0777, TRUE);
		}
		 
		$config_size = array(
			'image_library' => 'gd2',
			'source_image' => $source_image,
			'new_image' => $outputDirectory.'/'.$image_name,
			'create_thumb' => true,
			'maintain_ratio' => $maintain_ration,
			'thumb_marker' => '',
			'width' =>$width,
			'height' => $height
		);
		
		$CI->load->library('image_lib');
		$CI->image_lib->initialize($config_size);
		
		if (!$CI->image_lib->resize()){
			$response['status'] = "error";
			$response['message'] = strip_tags($CI->image_lib->display_errors());
		}
		else{
			$response['status'] = "Success";
			$response['message'] = "Image Resize Done";
		}
		
		$CI->image_lib->clear();
	}else{
		$response['status'] = "error";
		$response['message'] = "given image does not exist";
	}	 
	return $response;	
}


function unlink_files($source_directory,$file_name){
	$directories = glob($source_directory . '/*' , GLOB_ONLYDIR);
	$full_size_file = $source_directory.'/'.$file_name;
	$err = 0;
	if(file_exists($full_size_file) && is_file($full_size_file)){
				@unlink($full_size_file);
	}
	else{
		$err++;
	}
	
	if(!empty($directories)){
		for($x=0; $x<count($directories); $x++){
			$filename = $directories[$x].'/'.$file_name; 
			if(file_exists($filename) && is_file($filename)){
				@unlink($filename);
			}else{
				$err++;
			}
		}
	}
	$message = "All files deleted successfully";
	if($err>=1){
		$message = "Error Deleting all files";
	}
	return $message;	
}


function get_image($source_path,$image_name,$output_folder_name,$width,$height,$maintain_ration){
	$CI =& get_instance();
	$CI->load->helper('url');
	$outputDirectory = $source_path.'/'.$output_folder_name;
	$output_image = $source_path.'/'.$output_folder_name.'/'.$image_name;
	$source_image = $source_path.'/'.$image_name;
	if (file_exists($output_image) && is_file($output_image)){
		
	}else{
		$image_resize = image_resize($source_path,$image_name,$output_folder_name,$width,$height,$maintain_ration);
	}
	$output_image = base_url($output_image);
	return $output_image;
}

/* End of file imageupload_helper.php */
/* Location: ./application/helpers/imageupload_helper.php */
?>
