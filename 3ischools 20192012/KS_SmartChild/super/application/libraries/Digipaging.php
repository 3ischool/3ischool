<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Digipaging {

        protected $CI;

        // We'll use a constructor, as you can't directly call a function
        // from a property definition.
        public function __construct()
        {
                // Assign the CodeIgniter super-object
                $this->CI =& get_instance();
                $this->CI->load->helper('cookie');
        }
        
        function page_size(){
				$pageSizeCookie = get_cookie('digipagingpagesize');
				if($pageSizeCookie!='' && is_numeric($pageSizeCookie)){
					$per_page =$pageSizeCookie; 
				}else{
					$per_page = 20;
				}
				return $per_page;
		}
		
		function selected_page_size($val){
			$per_page = $this->page_size();	
				if($per_page==$val){
					$returnValue = 'selected="selected"';
				}else{
					$returnValue = '';
					}
					
					return $returnValue;
		}

        public function create_links($config)
        {
				$base_url = ''; 
				
				 
				$per_page = $this->page_size();
				 			
				$total_rows = ''; 
				$current_page = '';
                if(array_key_exists('base_url', $config)){
					$base_url = trim($config['base_url']);
				}
                 
                
                 
                                 
                if(array_key_exists('total_rows', $config)){
					$total_rows = trim($config['total_rows']);
				}
				
                if(array_key_exists('current_page', $config)){
					$current_page = trim($config['current_page']);
					$current_page = (int)$current_page;
				}
				
				if($base_url!='' && $per_page!='' && $total_rows!='' && $current_page!=''){
						$totalPages = ceil($total_rows/$per_page); 
						$prev = $current_page-1;
						if($prev<=0){
							$prevPage = "javascript:void(0)";		
						}else{
							$prevPage = $base_url."/".$prev;
						}
						$next = $current_page+1;
						if($next>$totalPages){
								$nextPage = "javascript:void(0)";
						}
						else{
							$nextPage = $base_url."/".$next;
						}
						$site_url = $this->CI->config->item('base_url')."index.php/";
						$html = '<input id="digipagingbaseurl" type="hidden" value="'.$base_url.'"><div class="pagination pull-right" style="margin:10px 0px;">
											<span class="pull-left" style="line-height:35px;">Showing page <b>'.$current_page.'</b> out of <i id="digipagingtotal">'.$totalPages.'</i> &nbsp;&nbsp;&nbsp;</span>
                                                    <ul class="pull-left">
                                                        <li class="pull-left"><a href="'.$prevPage.'">&lt;&lt;</a></li>
                                                        <li class="pull-left">
                                                        
                                                        <input value="'.$current_page.'" type="text" id="digipagingbox"  onkeypress="handle(event)" style="border-radius:0; text-align:center; border-color:#ddd; border-left:0; width:60px;"></li>                                                         
                                                        <li class="pull-left"><a href="'.$nextPage.'">&gt;&gt;</a></li>
                                                    </ul>
                                                    <span class="pull-left" style="line-height:35px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Page size&nbsp;</span>
                                                    <span class="pull-left" style="line-height:35px;">
														<select onchange="change_page_size(this.value)" style="border-radius:0; text-align:center; border-color:#ddd; width:60px;">
															<option value="'.$site_url.'common/set_paging/2" '.$this->selected_page_size(2).'>2</option>
															<option value="'.$site_url.'common/set_paging/10" '.$this->selected_page_size(10).'>10</option>
															<option value="'.$site_url.'common/set_paging/20" '.$this->selected_page_size(20).'>20</option>
															<option value="'.$site_url.'common/set_paging/50" '.$this->selected_page_size(50).'>50</option>
															<option value="'.$site_url.'common/set_paging/100" '.$this->selected_page_size(100).'>100</option>
															<option value="'.$site_url.'common/set_paging/500" '.$this->selected_page_size(500).'>500</option>
															<option value="'.$site_url.'common/set_paging/1000" '.$this->selected_page_size(1000).'>1000</option>
														</select>
                                                    </span>
                                                </div>
                                                
                                    <div class="clearfix"></div>
                                    <script>
										function handle(e){
											if(e.keyCode === 13){
												e.preventDefault();  
												var pageNo = parseInt(document.getElementById("digipagingbox").value);
												var totalPages = parseInt($.trim($("#digipagingtotal").text()));
												if(pageNo<=totalPages){
													window.location.href = document.getElementById("digipagingbaseurl").value+"/"+pageNo;
												}
												else{
													alert("Entered page no. doesnot exist");
												}   
												
											}
										}
										
										function change_page_size(val){
											$.get(val, function(data, status){
												if(data==1){
													window.location.href = document.getElementById("digipagingbaseurl").value;
												}
											});	
										}
									</script>
                                    ';
						
						
						$returnData = $html;
				}else{
						$returnData = 'Please pass all required params for activate paging[base_url,per_page,current_page,total_rows]';
				}
               return $returnData;  
                
        }

         

} ?>
