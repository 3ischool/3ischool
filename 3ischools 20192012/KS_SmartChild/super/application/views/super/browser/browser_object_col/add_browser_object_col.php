<?php $this->load->view('super/assets/header');?>
 <?php
 $table_col_id_rec = array();
 if($this->uri->segment(3) != ''){
	 $browser_object_col = $edit_rec[0]['BrowserObjectCol'];
	 $browser_object_id = $edit_rec[0]['BrowserObjectId'];
	 $can_view = $edit_rec[0]['CanView'];
	 $can_edit = $edit_rec[0]['CanEdit'];
	 $can_filter = $edit_rec[0]['CanFilter'];
	 $button_val = 'Update';
	  
 }else{
	 $browser_object_col = set_value('browser_object_col');
	 $browser_object_id = set_value('browser_object');
	 $can_view = set_value('can_view');
	 $can_edit = set_value('can_edit');
	 $can_filter = set_value('can_filter');
	 $button_val = 'Save & Continue';
	  
 }
 ?>
 <style>
 .multiselect-clear-filter{ display:none; }
 </style>
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Browser Object Column";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                   
                  <div class="nav-collapse collapse">
                    <ul class="nav">
                       
                      </ul>                    
                  </div><!--/.nav-collapse -->
                   <a href="<?php echo site_url('browser/manage_browser_object_col');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					<?php
						   $msg = $this->session->userdata('msg');
						   if($msg != ''){
								$this->session->unset_userdata('msg');
								$class = 'success';
						   }else{
							   $msg = $this->session->unset_userdata('error');
								$this->session->unset_userdata('error');
								$class = 'danger';
							}
						 if($msg != ''){	
						   ?> 
							<div class="alert alert-<?php echo $class; ?> alert-dismissable">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <?php echo $msg; ?>
							</div>  
						   <?php	   
							}
						   ?>   
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid">
						
						  <div class="span6">
                              <label>Browser Object Column</label>
                               <input name="browser_object_col" placeholder="" class="span12" type="text" value="<?php echo $browser_object_col; ?>">
							   <?php  echo form_error('browser_object_col','<span class="label label-important">','</span>');?>	
                          </div> 	
						    
						  <div class="span6">
                              <label>Browser Object</label>
                              <select class="span12 SmartSearch	" name="browser_object">
								  <option value="">Select Browser Object</option>
                              <?php foreach($browser_obj_rec as $browser_obj){ ?>
								  <option <?php if($browser_obj['BrowserObjectId'] == $browser_object_id){ ?>selected=selected<?php } ?> value="<?php echo $browser_obj['BrowserObjectId']; ?>"><?php echo $browser_obj['BrowserObject']; ?></option>
							  <?php } ?>	  
							  </select>
                              <?php  echo form_error('browser_object','<span class="label label-important">','</span>');?>	
                          </div> 	
						   
                        </div>
                         
                        <div class="row-fluid">
						  <div class="span4">
							   <label></label>
                               <input <?php if($can_view == 1){ ?>checked=checked<?php } ?> type="checkbox" name="can_view" value="1">&nbsp;&nbsp;Can View
                               <?php  echo form_error('can_view','<span class="label label-important">','</span>');?>	
                          </div> 
                         </div> 	
                         
                         <div class="row-fluid">
						  <div class="span4">
                               <input <?php if($can_edit == 1){ ?>checked=checked<?php } ?> type="checkbox" name="can_edit" value="1">&nbsp;&nbsp;Can Edit
                               <?php  echo form_error('can_edit','<span class="label label-important">','</span>');?>	
                          </div> 
                         </div> 	
                         <div class="row-fluid">
						  <div class="span4">
                               <input <?php if($can_filter == 1){ ?>checked=checked<?php } ?> type="checkbox" name="can_filter" value="1">&nbsp;&nbsp;Can Filter
                               <?php  echo form_error('can_filter','<span class="label label-important">','</span>');?>	
                          </div> 	
						 </div>
                      
                        <div class="row-fluid top10">
                         <div class="span6">
                              <input type="submit" class="btn" name="publish" value="<?php echo $button_val;?>">
                              <?php if($this->uri->segment(3) == ''){ ?>                     
                              <input type="submit" class="btn" name="publish1" value="Save & Exit">                
                              <?php } ?>     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
      <script>
      $('.table_col_multiselect').multiselect({
			 buttonWidth : "100%",
			 maxHeight : 200,
			 nonSelectedText : "Select Table Columns",
			 enableFiltering: true,
			 searchText : true,
			 enableCaseInsensitiveFiltering: true
		});	
      </script>       
      <?php $this->load->view('super/assets/footer');?>
 
