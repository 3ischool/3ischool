<?php $this->load->view('super/assets/header');?>
  <style>
  .iconClass{
		text-align:center !important;
	}
  </style>      
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Browser Object Column";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	 
        	<div class="btn-group pull-left left15">
                                         
                                    </div>
                                	 <div class="btn-group pull-right left10">
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('browser/add_browser_object_col'); ?>">
                                        	Add
                                        </a>
                                        
                                    </div>                               
                                     
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                        <?php
						   $msg = $this->session->userdata('msg');
						   if($msg != ''){
								$this->session->unset_userdata('msg');
								$class = 'success';
						   }else{
							   $msg = $this->session->unset_userdata('error');
								$this->session->unset_userdata('error');
								$class = 'danger';
							}
						 if($msg != ''){	
						   ?> 
							<div class="alert alert-<?php echo $class; ?> alert-dismissable">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <?php echo $msg; ?>
							</div>  
						   <?php	   
							}
						   ?>         
                        <div class="row-fluid ">
							<div class="row-fluid">
                                	<table id="myTable" class="table table-bordered" style="border-top:0px;">
                                      <thead>	
                                    	<tr>
                                        	<th>Browser Object Column</th>
                                        	<th>Browser Object</th>
                                        	<th>View</th>
                                        	<th>Edit</th>
                                        	<th>Filter</th>
                                        	<th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>      
										<?php foreach($browser_obj_col_rec as $browser_obj_col){ ?>
                                        <tr>
                                        	<td><?php echo $browser_obj_col['BrowserObjectCol']; ?></td>
                                        	<td><?php echo $browser_obj_col['BrowserObject']; ?></td>
                                        	 
                                        	<?php 
                                        	$ok_sign = '<i class="icon icon-ok icon-x" style="color:green;"></i>';
                                        	$remove_sign = '<i class="icon icon-remove icon-x" style="color:red"></i>';
                                        	if($browser_obj_col['CanView'] == 1){
												$CanView11 = 'Yes';
												$CanView = $ok_sign;
											}else{
												$CanView = $remove_sign;
											}
                                        	 
                                        	if($browser_obj_col['CanEdit'] == 1){
												$CanEdit = $ok_sign;
											}else{
												$CanEdit = $remove_sign;
											}
                                        	 
                                        	if($browser_obj_col['CanFilter'] == 1){
												$CanFilter = $ok_sign;
											}else{
												$CanFilter = $remove_sign;
											}
                                        	?>
                                        	<td class="iconClass"><?php echo $CanView; ?></td>
                                        	<td class="iconClass"><?php echo $CanEdit; ?></td>
                                        	<td class="iconClass"><?php echo $CanFilter; ?></td>
                                        	<td>
                                            <a href="<?php echo site_url('browser/edit_browser_object_col/'.base64e($browser_obj_col['BrowserObjectColId'])); ?>" class="btn btn-xs btn-default" title="Edit"> <i class="icon icon-pencil"></i></a>
											<a href="<?php echo site_url('browser/delete_browser_object_col/'.base64e($browser_obj_col['BrowserObjectColId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											</td>
                                        </tr>
                                       <?php } ?>       	
                                      </tbody> 
                                    </table>

                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>

            </div>
        </div>
        <!-- //Dash Content -->
             
       <?php $this->load->view('super/assets/footer');?>
