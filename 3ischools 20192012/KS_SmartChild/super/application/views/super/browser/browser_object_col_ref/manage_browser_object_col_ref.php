<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Browser Object Column Refrence";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	 
        	<div class="btn-group pull-left left15">
                                         
                                    </div>
                                	 <div class="btn-group pull-right left10">
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('browser/add_browser_object_col_ref'); ?>">
                                        	Add
                                        </a>
                                        
                                    </div>                               
                                     
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                        <?php
						   $msg = $this->session->userdata('msg');
						   if($msg != ''){
								$this->session->unset_userdata('msg');
								$class = 'success';
						   }else{
							   $msg = $this->session->unset_userdata('error');
								$this->session->unset_userdata('error');
								$class = 'danger';
							}
						 if($msg != ''){	
						   ?> 
							<div class="alert alert-<?php echo $class; ?> alert-dismissable">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <?php echo $msg; ?>
							</div>  
						   <?php	   
							}
						   ?>         
                        <div class="row-fluid ">
							<div class="row-fluid">
                                	<table id="myTable" class="table table-bordered" style="border-top:0px;">
                                      <thead>	
                                    	<tr>
                                        	<th>Browser Object Column</th>
                                        	<th>Refrence Table</th>
                                        	<th>Control Type</th>
                                        	<th>Ref Table Col</th>
                                        	<th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>      
										<?php foreach($browser_obj_col_ref_rec as $browser_obj_col_ref){ ?>
                                        <tr>
                                        	<td><?php echo $browser_obj_col_ref['BrowserObjectCol']; ?></td>
                                        	<td><?php echo $browser_obj_col_ref['RefTable']; ?></td>
                                        	<td><?php echo $browser_obj_col_ref['ControlType']; ?></td>
                                        	<td><?php echo $browser_obj_col_ref['RefTableCol']; ?></td>
                                        	<td>
                                            <a href="<?php echo site_url('browser/edit_browser_object_col_ref/'.base64e($browser_obj_col_ref['BrowserObjectColRefId'])); ?>" class="btn btn-xs btn-default" title="Edit"> <i class="icon icon-pencil"></i></a>
											<a href="<?php echo site_url('browser/delete_browser_object_col_ref/'.base64e($browser_obj_col_ref['BrowserObjectColRefId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											</td>
                                        </tr>
                                       <?php } ?>       	
                                      </tbody> 
                                    </table>

                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>

            </div>
        </div>
        <!-- //Dash Content -->
             
       <?php $this->load->view('super/assets/footer');?>
