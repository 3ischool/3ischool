<?php $this->load->view('super/assets/header');?>
 <?php
 if($this->uri->segment(3) != ''){
	 $browser_object_col_id = $edit_rec[0]['BrowserObjectColId'];
	 $ref_table = $edit_rec[0]['RefTable'];
	 $control_type = $edit_rec[0]['ControlType'];
	 $RefTableCol = $edit_rec[0]['RefTableCol'];
	 $button_val = 'Update';
 }else{
	 $browser_object_col_id = set_value('browser_object_col');
	 $ref_table = set_value('ref_table');
	 $control_type = set_value('control_type');
	 $RefTableCol = set_value('RefTableCol');
	 $button_val = 'Save & Continue';
}
 ?>
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Browser Object Column Refrence";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                   
                  <div class="nav-collapse collapse">
                    <ul class="nav">
                       
                      </ul>                    
                  </div><!--/.nav-collapse -->
                   <a href="<?php echo site_url('browser/manage_browser_object_col_ref');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					<?php
						   $msg = $this->session->userdata('msg');
						   if($msg != ''){
								$this->session->unset_userdata('msg');
								$class = 'success';
						   }else{
							   $msg = $this->session->unset_userdata('error');
								$this->session->unset_userdata('error');
								$class = 'danger';
							}
						 if($msg != ''){	
						   ?> 
							<div class="alert alert-<?php echo $class; ?> alert-dismissable">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <?php echo $msg; ?>
							</div>  
						   <?php	   
							}
						   ?>   
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid">
						
						  <div class="span6">
                              <label>Browser Object Column</label>
                              <select name="browser_object_col" class="span12 SmartSearch">
								  <option value="">Select Object Column</option>
								  <?php foreach($browser_obj_col_rec as $browser_obj_col){ ?>
									  <option <?php if($browser_object_col_id == $browser_obj_col['BrowserObjectColId']){ ?>selected=selected<?php } ?> value="<?php echo $browser_obj_col['BrowserObjectColId']; ?>"><?php echo $browser_obj_col['BrowserObjectCol']; ?></option>
								  <?php } ?>	  	
                              </select>
                              <?php  echo form_error('browser_object_col','<span class="label label-important">','</span>');?>	
                          </div> 	
						    
						  <div class="span6">
                              <label>Ref Table</label>
                              <input name="ref_table" placeholder="" class="span12" type="text" value="<?php echo $ref_table; ?>">
							   <?php  echo form_error('ref_table','<span class="label label-important">','</span>');?>	
                          </div> 		
						   
                        </div>
                        <div class="row-fluid" style="margin-top:10px;">
						
						  <div class="span6">
                              <label>Control Type</label>
                               <input name="control_type" placeholder="" class="span12" type="text" value="<?php echo $control_type; ?>">
							   <?php  echo form_error('control_type','<span class="label label-important">','</span>');?>	
                          </div> 	
						  <div class="span6">
                              <label>Ref Table Col</label>
                               <input name="RefTableCol" placeholder="" class="span12" type="text" value="<?php echo $RefTableCol; ?>">
							   <?php  echo form_error('RefTableCol','<span class="label label-important">','</span>');?>	
                          </div> 	
						   
                        </div>
                       
                      
                        <div class="row-fluid top10">
                         <div class="span6">
                              <input type="submit" class="btn" name="publish" value="<?php echo $button_val;?>">  
                               <?php if($this->uri->segment(3) == ''){ ?>                     
                              <input type="submit" class="btn" name="publish1" value="Save & Exit">          
                              <?php } ?>           
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
