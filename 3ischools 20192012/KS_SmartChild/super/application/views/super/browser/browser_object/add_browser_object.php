<?php $this->load->view('super/assets/header');?>
 <?php
 if($this->uri->segment(3) != ''){
	 $browser_object = $edit_rec[0]['BrowserObject'];
	 $max_view_col_count = $edit_rec[0]['MaxViewColCount'];
	 $max_col_count = $edit_rec[0]['MaxColCount'];
	 $max_filter_col_count = $edit_rec[0]['MaxFilterColCount'];
	 $button_val = 'Update';
 }else{
	 $browser_object = set_value('browser_object');
	 $max_view_col_count = set_value('max_view_col_count');
	 $max_col_count = set_value('max_col_count');
	 $max_filter_col_count = set_value('max_filter_col_count');
	 $button_val = 'Save & Continue';
}
 ?>
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Browser Object";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                   
                  <div class="nav-collapse collapse">
                    <ul class="nav">
                       
                      </ul>                    
                  </div><!--/.nav-collapse -->
                   <a href="<?php echo site_url('browser/manage_browser_object');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					<?php
						   $msg = $this->session->userdata('msg');
						   if($msg != ''){
								$this->session->unset_userdata('msg');
								$class = 'success';
						   }else{
							   $msg = $this->session->unset_userdata('error');
								$this->session->unset_userdata('error');
								$class = 'danger';
							}
						 if($msg != ''){	
						   ?> 
							<div class="alert alert-<?php echo $class; ?> alert-dismissable">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <?php echo $msg; ?>
							</div>  
						   <?php	   
							}
						   ?>   
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid">
						
						  <div class="span6">
                              <label>Browser Object</label>
                               <input name="browser_object" placeholder="" class="span12" type="text" value="<?php echo $browser_object; ?>">
							   <?php  echo form_error('browser_object','<span class="label label-important">','</span>');?>	
                          </div> 	
						    
						  <div class="span6">
                              <label>Max View Column Count</label>
                               <input name="max_view_col_count" placeholder="" class="span12" type="text" value="<?php echo $max_view_col_count; ?>">
							   <?php  echo form_error('max_view_col_count','<span class="label label-important">','</span>');?>	
                          </div> 	
						   
                        </div>
                        <div class="row-fluid">
						
						  <div class="span6">
                              <label>Max Column Count</label>
                               <input name="max_col_count" placeholder="" class="span12" type="text" value="<?php echo $max_col_count; ?>">
							   <?php  echo form_error('max_col_count','<span class="label label-important">','</span>');?>	
                          </div> 	
						    
						  <div class="span6">
                              <label>Max Filter Column Count</label>
                               <input name="max_filter_col_count" placeholder="" class="span12" type="text" value="<?php echo $max_filter_col_count; ?>">
							   <?php  echo form_error('max_filter_col_count','<span class="label label-important">','</span>');?>	
                          </div> 	
						   
                        </div>
                       
                      
                        <div class="row-fluid top10">
                         <div class="span6">
                              <input type="submit" class="btn" name="publish" value="<?php echo $button_val;?>">  
                               <?php if($this->uri->segment(3) == ''){ ?>                     
                              <input type="submit" class="btn" name="publish1" value="Save & Exit">          
                              <?php } ?>           
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
