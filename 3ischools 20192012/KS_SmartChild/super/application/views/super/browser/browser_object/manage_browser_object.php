<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Browser Object";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	 
        	<div class="btn-group pull-left left15">
                                         
                                    </div>
                                	 <div class="btn-group pull-right left10">
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('browser/add_browser_object'); ?>">
                                        	Add
                                        </a>
                                        
                                    </div>                               
                                     
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                        <?php
						   $msg = $this->session->userdata('msg');
						   if($msg != ''){
								$this->session->unset_userdata('msg');
								$class = 'success';
						   }else{
							   $msg = $this->session->unset_userdata('error');
								$this->session->unset_userdata('error');
								$class = 'danger';
							}
						 if($msg != ''){	
						   ?> 
							<div class="alert alert-<?php echo $class; ?> alert-dismissable">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <?php echo $msg; ?>
							</div>  
						   <?php	   
							}
						   ?>         
                        <div class="row-fluid ">
							<div class="row-fluid">
                                	<table id="myTable" class="table table-bordered" style="border-top:0px;">
                                      <thead>	
                                    	<tr>
                                        	<th>Browser Object</th>
                                        	<th>Max View Column Count</th>
                                        	<th>Max Column Count</th>
                                        	<th>Max Filter Column Count</th>
                                        	<th>Status</th>
                                        	<th>Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>      
										<?php foreach($browser_obj_rec as $browser_obj){ ?>
                                        <tr>
                                        	<td><?php echo $browser_obj['BrowserObject']; ?></td>
                                        	<td><?php echo $browser_obj['MaxViewColCount']; ?></td>
                                        	<td><?php echo $browser_obj['MaxColCount']; ?></td>
                                        	<td><?php echo $browser_obj['MaxFilterColCount']; ?></td>
                                        	<?php 
                                        	if($browser_obj['IsActicve'] == 1){
												$is_active = 'Active';
											}else{
												$is_active = 'Inactive';	
											}
                                        	?>
                                        	<td><a href="<?php echo site_url('browser/update_browser_object_status/'.base64e($browser_obj['BrowserObjectId']).'/'.$browser_obj['IsActicve']); ?>"><?php echo $is_active; ?></a></td>
                                        	<td>
                                            <a href="<?php echo site_url('browser/edit_browser_object/'.base64e($browser_obj['BrowserObjectId'])); ?>" class="btn btn-xs btn-default" title="Edit"> <i class="icon icon-pencil"></i></a>
											<a href="<?php echo site_url('browser/delete_browser_object/'.base64e($browser_obj['BrowserObjectId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											</td>
                                        </tr>
                                       <?php } ?>       	
                                      </tbody> 
                                    </table>

                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>

            </div>
        </div>
        <!-- //Dash Content -->
             
       <?php $this->load->view('super/assets/footer');?>
