<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Navigation Item";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	 
        	<div class="btn-group pull-left left15">
                                        
                                    </div>
                                	 <div class="btn-group pull-right left10">
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('navigation/add_navigation_item'); ?>">
                                        	Add
                                        </a>
                                        
                                    </div>                               
                                    
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                    
                                  
                        <div class="row-fluid ">
                         <?php
                   $msg = $this->session->userdata('json');
                   if($msg != ''){
					$this->session->unset_userdata('json');
                   ?> 
					<div class="alert alert-success alert-dismissable">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <?php echo $msg; ?>
					</div>  
				   <?php	   
					}
                   ?>
                                    <div class="row-fluid">
                                	 
                                    <table id="myTable" class="table table-bordered" style="border-top:0px;">
                                      <thead>	
                                    	<tr>
                                        	<th>Navigation Item</th>
                                        	<th>Url</th>
                                        	<th>Icon</th>
                                        	 
                                        	<th>Action</th>
                                        </tr>
                                       </thead>
                                       <tbody>     
                                        <?php foreach($navigation_item_rec as $rec){ ?>
                                        <tr>
											<td><?php echo $rec['NavigationItem']; ?></td>
											<?php
												$controller = '';
												$action = '';
												$query = '';
												if($rec['ControllerName'] != ''){
													$controller = $rec['ControllerName'];
												}
												if($rec['ActionName'] != ''){
													$action = '/'.$rec['ActionName'];
												}
												if($rec['QueryString'] != ''){
													$query = '/'.$rec['QueryString'];
												}
											?>
											<td><?php echo $controller.$action.$query; ?></td>
											
											<td>
												<?php 
												if($rec['NavigationIcon'] != ''){
												echo '<img src="'.base_url('uploads/navigation_item_icon/'.$rec['NavigationIcon']).'">';	
												} 
												
												?>
											</td>
                                        	 
											<td>
                                            <a href="<?php echo site_url('navigation/edit_navigation_item/'.base64e($rec['NavigationItemId'])); ?>" class="btn btn-xs btn-default" title="Edit"> <i class="icon icon-pencil"></i></a>
											<?php 
											$NavigationPlacementItem = $this->common_model->compare_in_table('NavigationPlacement','NavigationItemId',$rec['NavigationItemId']);
											 
											if(empty($NavigationPlacementItem)){
											 ?>
											<a href="<?php echo site_url('navigation/delete_navigation_item/'.base64e($rec['NavigationItemId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											<?php } ?>
											</td>
                                        </tr>
                                       <?php } ?>
                                       </tbody> 
                                    </table>
                                    
                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>
            	
                
            </div>
        </div>
        <!-- //Dash Content -->
             
       <?php $this->load->view('super/assets/footer');?>
