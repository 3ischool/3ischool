<?php $this->load->view('super/assets/header');?>
 <?php 
 $function_name = $this->uri->segment(2);
 if($function_name == 'add_navigation_item'){
	 $navigation_item = set_value('navigation_item');
	 $controller_name = set_value('controller_name');
	 $action_name = set_value('action_name');
	 $query_string = set_value('query_string');
	 $nav_item_type_id = set_value('nav_item_type');
	 $nav_item_icon = ''; 
	 $button_val = 'Add';
	} 
 else{
	 $navigation_item = $result[0]['NavigationItem'];
	 $controller_name = $result[0]['ControllerName'];
	 $action_name = $result[0]['ActionName'];
	 $query_string = $result[0]['QueryString'];
	 $nav_item_type_id = $result[0]['NavItemTypeId'];
	 $nav_item_icon = $result[0]['NavigationIcon'];
	 $button_val = 'Update';
	} 	
 ?>       
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Navigation Item";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                   
                  <div class="nav-collapse collapse">
                    <ul class="nav">
                         
                      </ul>                    
                  </div><!--/.nav-collapse -->
                   <a href="<?php echo site_url('navigation/manage_navigation_item');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open_multipart(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
						
						  <div class="span4">
                              <label>Navigation Item</label>
                               <input name="navigation_item" placeholder="" class="span12" type="text" value="<?php echo $navigation_item; ?>">
							   <?php  echo form_error('navigation_item','<span class="label label-important">','</span>');?>	
                          </div> 	
						  
						  <div class="span4">
                              <label>Controller Name</label>
                               <input name="controller_name" placeholder="" class="span12" type="text" value="<?php echo $controller_name; ?>">
							   <?php  echo form_error('controller_name','<span class="label label-important">','</span>');?>	
                          </div> 
                          
                          <div class="span4">
                              <label>Action Name</label>
                               <input name="action_name" placeholder="" class="span12" type="text" value="<?php echo $action_name; ?>">
							   <?php  echo form_error('action_name','<span class="label label-important">','</span>');?>	
                          </div> 
                          
                          
                          
                        </div>  	
						
						<div class="row-fluid top10">
                        
							
							<div class="span4">
                              <label>Query String</label>
                               <input name="query_string" placeholder="" class="span12" type="text" value="<?php echo $query_string; ?>">
							   <?php  echo form_error('query_string','<span class="label label-important">','</span>');?>	
                            </div> 
							
							<div class="span4">
                              <label>Navigation Item Type</label>
                              <select name="nav_item_type">
								<option value="">Select</option>
								<?php foreach($navigation_item_rec as $item_rec){ ?>
								<option <?php if($nav_item_type_id == $item_rec['NavItemTypeId']){ ?>selected=selected<?php } ?> value="<?php echo $item_rec['NavItemTypeId']; ?>"><?php echo $item_rec['NavItemType']; ?></option>
								<?php } ?>
                              </select>
                              <?php  echo form_error('nav_item_type','<span class="label label-important">','</span>');?>	
                          </div> 
                          <div class="span4">
								<label>Nav Item Icon</label>
								<input type="file" class="span12" name="userfile" id="userfile">
								<?php
								if($nav_item_icon != ''){
								?>
								<img src="<?php echo base_url('uploads/navigation_item_icon/'.$nav_item_icon); ?>">
								<?php	
									}
								?>
								<?php  echo form_error('userfile','<span class="label label-danger">','</span>');?>
						  </div>
                        </div>
                        
                        <div class="row-fluid top10">
                         <div class="span6">
							 <?php if($function_name == 'add_navigation_item'){ ?>                
                              <input type="submit" name="continue" class="btn" value="Save & Continue">         
                              <?php } ?>   
                              <input type="submit" class="btn" value="<?php echo $button_val; ?>">     
                                       
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
