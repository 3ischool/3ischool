<?php $this->load->view('super/assets/header');?>
 <?php 
 $function_name = $this->uri->segment(2);
 if($function_name == 'add_appFormField'){
	 $fieldName = set_value('fieldName');
	 $appFormId = set_value('appFormId');
	 $button_val = 'Add';
	} 
 else{
	 $fieldName = $result[0]['FieldName'];
	 $appFormId = $result[0]['AppFormId'];
	 $button_val = 'Update';
	} 	
 ?>       
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "App Form Field";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                   
                  <div class="nav-collapse collapse">
                    <ul class="nav">
                       
                      </ul>                    
                  </div><!--/.nav-collapse -->
                   <a href="<?php echo site_url('appFormSetup/manage_appFormField');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
						
						  <div class="span6">
                              <label>Field Name</label>
                               <input id="fieldName" name="fieldName" placeholder="" class="span12" type="text" value="<?php echo $fieldName; ?>">
							   <?php  echo form_error('fieldName','<span class="label label-important">','</span>');?>	
                          </div> 	
						  
						  <div class="span6">
                              <label>App Form</label>   
                              <select class="span12 SmartSearch" name="appFormId">
                              <option value="">Select</option>
                              <?php 
                              foreach($appForm as $appForm){
                              ?>
                              <option <?php if($appFormId == $appForm['AppFormId']){ ?>selected=selected <?php } ?> value="<?php echo $appForm['AppFormId']; ?>"><?php echo $appForm['AppForm']; ?></option>
                              <?php } ?>
                              </select>    
                              <?php  echo form_error('appFormId','<span class="label label-important">','</span>');?>                  
                          </div>  	
                         
                        </div>
                       
                        <div class="row-fluid top10">
                         <div class="span6">
                              <input type="submit" class="btn" value="<?php echo $button_val; ?>">                     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
