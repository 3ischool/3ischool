  
        <!-- Coding //Part Starts Here -->
    </div>
  </div>  
  <!-- //Dashboard Container -->  

  <script>
  $( function() {
    $( ".datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
    $('#DrawerBtnGST').click(function(){
		$('#LeftPanelGst').toggleClass('leftPanelGSTPad_Open');
	});
	
	$('#containerMain').click(function(){
			if($('#LeftPanelGst').hasClass('leftPanelGSTPad_Open')){
				 $('#DrawerBtnGST').click();
			}
	});
	
	$('.GSTPadNav').click(function(){
			if($('#LeftPanelGst').hasClass('leftPanelGSTPad_Open')){
				 $('#DrawerBtnGST').click();
			}
	});
	
	$(".SmartSearch").select2();
	
  } );
  
  </script>
   <script>
	
	$(document).on('submit','#formID', function(e){
		e.preventDefault();
		var formAction = $(this).attr("action");
		
		$.ajax({
				url:formAction,
				type:'POST',
				data:$(this).serialize(),
				success:function(result){
					//alert(result);
						alert('Sorting updated successfully');
					}

		});
		
	});
	   
 	$(document).ready(function(){
		
		//DataTable 
		$('#myTable').DataTable({
			 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			 "bSort" : false
			});
		 
		 var fixHelperModified = function(e, tr) {
			var $originals = tr.children();
			var $helper = tr.clone();
			$helper.children().each(function(index)
			{
			  $(this).width($originals.eq(index).width())
			});
			return $helper;
		};

		
		$("#sortable").sortable({
			helper: fixHelperModified,
			update: function () {
						$('#submitID').trigger('click');
					}  
		}).disableSelection();		
		 
	});
	
	 $(document).on("click",".popupclass",function(e){
			 e.preventDefault();
			 var uurl = $(this).attr('href');
			 $('#popupFrame').attr('src',uurl);
			 $('#myModal').modal('show');
		 });
	function hidePopupManual(){
		$('#myModal').modal('hide');
		}	 

	function hidePopupManualRefresh(){
		$('#myModal').modal('hide');
		window.location.href = window.location.href;
	}	 
	
	</script>   
		
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   
  <div class="modal-body" style="padding:0;">
    <iframe id="popupFrame" src="" frameborder="0" style="width:100%;height:400px; float:left"></iframe>
    <div class="clearfix"></div>
  </div>
   
</div>		
  </body>
</html>
 
