  
        <!-- Coding //Part Starts Here -->
    </div>
  </div>  
  <!-- //Dashboard Container -->  
  
  <script src="<?php echo base_url('assets/js/base.js');?>"></script>    
  <script src="<?php echo base_url('assets/js/jquery.nicescroll.min.js');?>"></script>
  <!--<script type="text/javascript" src="<?php echo base_url('assets/js/mczToastNotifications/jquery.mczToast.js');?>"></script> -->  
  <script src="<?php echo base_url('assets/js/jquery.mspConnect.js');?>"></script>
  <script src="<?php echo base_url('assets/js/common_ajax.js');?>"></script> 
   <script src="<?php echo base_url('assets/js/tinymce/tinymce.min.js'); ?>"></script>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
			tinymce.init({ 
						selector:'.textarea',
						plugins : 'advlist autolink link image lists charmap print preview'
					 });
  </script>
  <script>
  $( function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
  } );
  </script>
   <script>
 	$(document).ready(function(){
		
		$( ".country" ).change(function() {
		    var id = $(this).val();	
		    $.ajax({
				type   : 'POST',
				url : '<?php echo site_url('address/getStates/'); ?>'+id,
				success : function(data){
							var json = $.parseJSON(data);
					        $(".state").html(json.states);
					        $(".city").html(json.cities);
							}
				 });
		  });
		 
		$( ".state" ).change(function() {
		    var id = $(this).val();	
		    $.ajax({
				type   : 'POST',
				url : '<?php echo site_url('address/getCity/'); ?>'+id,
				success : function(data){
							var json = $.parseJSON(data);
					        $(".city").html(json.cities);
							}
				 });
		  });  
		  
		$( ".subscription" ).change(function() {
		    var id = $(this).val();	
		    //alert(id);
		    $.ajax({
				type   : 'POST',
				url : '<?php echo site_url('client/getSubscriptionDetails/'); ?>'+id,
				success : function(data){
					//alert(data);
							var json = $.parseJSON(data);
							$(".rate").val(json.rate);
							}
				 });
		  });  
		  
		  
	});
	</script>   
			
  </body>
</html>
 
