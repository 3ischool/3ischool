<?php if(!isset($pageTitle)){
	$pageTitle = 'KSSuper';
	}?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $pageTitle;?></title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
     
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo base_url('assets/css/base.css');?>" rel="stylesheet">     
    <link href="<?php echo base_url('assets/css/base-fluid.css');?>" rel="stylesheet">     
    <link href="<?php echo base_url('assets/css/custom.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/blue_grey.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/custom-fluid.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/font-awesome/css/font-awesome-ie7.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/js/mczToastNotifications/jquery.mczToast.css');?>" rel="stylesheet"> 
	 
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="<?php echo base_url('assets/js/html5shiv.js');?>"></script>
    <![endif]-->
   
   
    
      
	  <link rel="stylesheet" href="<?php echo base_url('assets/js/jquery/jquery-ui.css');?>">   
	  <script src="<?php echo base_url('assets/js/jquery/jquery-1.12.4.js');?>"></script>
	  <script src="<?php echo base_url('assets/js/jquery/jquery-ui.js');?>"></script>
	  <script src="<?php echo base_url('assets/js/base.js');?>"></script> 
	  <script type="text/javascript" src="<?php echo base_url('assets/js/mczToastNotifications/jquery.mczToast.js');?>"></script>
	  	
	  
  <link rel="stylesheet" href="<?php echo base_url('assets/js/select2/select2.min.css');?>" />
  <script src="<?php echo base_url('assets/js/select2/select2.min.js');?>"></script> 
  
  <!-- Sweet Alert -->
  <script src="<?php echo base_url('assets/sweetalert/sweetalert.min.js');?>"></script>
  <link rel="stylesheet" href="<?php echo base_url('assets/sweetalert/sweetalert.css');?>">
  <!--.......................-->
   <!-- Data Table -->
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
   <!--.......................-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datetimepicker/jquery.datetimepicker.min.css');?>"/>
<script src="<?php echo base_url('assets/datetimepicker/jquery.datetimepicker.min.js');?>"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/gst.css');?>"/>

<script src="<?php echo base_url('assets/js/tree.jquery.js');?>"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/qtree.css');?>"/>

<!-- Multiple select-->
<link href="<?php echo base_url('assets/css/bootstrap-multiselect.css'); ?>" rel="stylesheet">
<script src="<?php echo base_url('assets/js/bootstrap-multiselect.js'); ?>"></script>

<style>
.ui-sortable-handle{cursor: grabbing;}
</style>
  </head>
  <body> 
  <div id="ajax_loader" class="LoadingDiv">
	<img src="<?php echo base_url('assets/img/loader.gif');?>">
  </div>		
  <!-- Header -->
  <div class="navbar navbar-fixed-top GSTPadNav">
  <div class="navbar-inner">
    <div class="container-fluid">
 
      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
 
      <!-- Be sure to leave the brand out there if you want it shown -->
      <a class="brand" href="http://3ibox.com">
		<img src="<?php echo base_url('assets/img/GSTPad.png');?>" class="gstLogo">
      </a>
 
      <!-- Everything you want hidden at 940px or less, place within here -->
      <div class="nav-collapse collapse">
			<ul class="nav">
                      <li><a href="<?php echo site_url('common/form_builder_setup');?>">FB Setup</a></li>                      
                      <li><a href="<?php echo site_url('common/view_master');?>">Masters</a></li>                      
                        
                       
                      
                    </ul>
                    <ul class="nav pull-right">
						 
						<li><a>Welcome! <?php echo base64d($this->input->cookie('s_un')); ?></a></li>
						<li class="">
						<a href="<?php echo site_url('user/logout');?>">Logout</a>
                      </li>
                      <li class="">
						<a href="<?php echo site_url('support_user/change_password');?>">Change Password</a>
                      </li>
                    </ul>
      </div>
 
    </div>
  </div>
</div>
  
   
  
  <!-- Left Panel -->  
  <div class="leftPanel leftPanelGSTPad" id="LeftPanelGst" >
	  <div id="DrawerBtnGST">
		<div class="btnbar"></div>
		<div class="btnbar"></div>
		<div class="btnbar"></div>
	  </div>
	  <div class="GSTSideBarLogo"><img src="<?php echo base_url('assets/img/GSTPad.png');?>"></div>
  	<div class="leftInner"> 
    	 
        <div id="leftScrollingLinks">
            <div class="accordion" id="accordion2">
               <!--
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" href=""  rel="navigation">
                            <div class="projects">&nbsp;</div><span class="pull-left"></span>
                        </a>
                    </div>                     
                </div> -->
                <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#a1">
                            <div class="service">&nbsp;</div><span class="pull-left">Master</span>
                        </a>
                    </div>
                    <div id="a1" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <ul>
                               <li>
								  <a href="<?php echo site_url('school/manage_board'); ?>" rel="navigation">School Board</a>
							   </li>
                               <li>
								  <a href="<?php echo site_url('school/manage_school_group'); ?>" rel="navigation">School Group</a>
							   </li>
                               <li>
								  <a href="<?php echo site_url('school/manage_school_db'); ?>" rel="navigation">School DB</a>
							   </li>
                               <li>
								  <a href="<?php echo site_url('school/manage_school'); ?>" rel="navigation">School</a>
							   </li>
                          
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
                 <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#a2">
                            <div class="role">&nbsp;</div><span class="pull-left">Navigation Master</span>
                        </a>
                    </div>
                    <div id="a2" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <ul>
								<li>
									 <a href="<?php echo site_url('navigation/manage_navigation_item'); ?>" rel="navigation">Navigation Item</a>
								 </li>
								 <li>
									 <a href="<?php echo site_url('navigation/navigation_placement'); ?>" rel="navigation">Navigation Item Places</a>
								 </li>
								 <li>
									 <a href="<?php echo site_url('navigation/manage_user_role_type_nav_place'); ?>" rel="navigation">User Role Type Nav Places</a>
								 </li>
								 <li>
									 <a href="<?php echo site_url('navigation/mobile_nav_items_sorting'); ?>" rel="navigation">Mobile Nav Dashboard Items</a>
								 </li>
								
								  
								  
							</ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                   </div> 
                 
                
                 <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#a3">
                            <div class="role">&nbsp;</div><span class="pull-left">Widget</span>
                        </a>
                    </div>
                    <div id="a3" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <ul>
								 
								<li>
									 <a href="<?php echo site_url('widget/manage_widget_tab_map'); ?>" rel="navigation">Assign Widgets to Tab</a>
								 </li>
								<li>
									 <a href="<?php echo site_url('widget/manage_widget'); ?>" rel="navigation">Widget</a>
								 </li> 
								<li>
									 <a href="<?php echo site_url('widget/manage_widget_tab'); ?>" rel="navigation">Tab</a>
								 </li> 
								<li>
									 <a href="<?php echo site_url('widget/manage_widget_area'); ?>" rel="navigation">Area</a>
								 </li> 
								<li>
									 <a href="<?php echo site_url('widget/manage_widget_type'); ?>" rel="navigation">Widget Type</a>
								 </li> 
								<li>
									 <a href="<?php echo site_url('widget/manage_widget_tab_user_role'); ?>" rel="navigation">Assign Permission to Widgets</a>
								 </li>
								 <li>
									 <a href="<?php echo site_url('widget/manage_color_class_master'); ?>" rel="navigation">Color Class Master</a>
								 </li>
								 <li>
									 <a href="<?php echo site_url('widget/manage_html_template'); ?>" rel="navigation">Html Template</a>
								 </li> 
								  
							</ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                   </div> 
                 
                
                 <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#a4">
                            <div class="role">&nbsp;</div><span class="pull-left">App Form</span>
                        </a>
                    </div>
                    <div id="a4" class="accordion-body collapse">
                        <div class="accordion-inner">
                        <ul>
						 <li>
                             <a href="<?php echo site_url('appFormSetup/manage_user_role_type_app_form'); ?>" rel="navigation">User Role Type App Form map</a>
                         </li>
						 <li>
                             <a href="<?php echo site_url('appFormSetup/manage_appForm'); ?>" rel="navigation">App Form</a>
                         </li>
                         <li>
                             <a href="<?php echo site_url('appFormSetup/manage_app_form_action'); ?>" rel="navigation">App Form Action</a>
                         </li>
                         <li>
                             <a href="<?php echo site_url('appFormSetup/manage_app_form_action_type'); ?>" rel="navigation">App Form Action Type</a>
                         </li>
                         <li>
                             <a href="<?php echo site_url('appFormSetup/manage_appFormField'); ?>" rel="navigation">App Form Field</a>
                         </li>
                         <li>
                             <a href="<?php echo site_url('appFormSetup/manage_appFormLabel'); ?>" rel="navigation">App Form Label</a>
                         </li>
                         <li>
                             <a href="<?php echo site_url('appFormSetup/manage_appFormTitle'); ?>" rel="navigation">App Form Title</a>
                         </li> 
                         <li>
                             <a href="<?php echo site_url('appFormSetup/manage_app_form_info'); ?>" rel="navigation">App Form Info</a>
                         </li> 
                         </ul>
                         <div class="clearfix"></div>
                        </div>
                    </div>
                   </div> 
                 
                 <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#a5">
                            <div class="role">&nbsp;</div><span class="pull-left">Browser</span>
                        </a>
                    </div>
                    <div id="a5" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <ul>
                               <li>
								  <a href="<?php echo site_url('browser/manage_browser_object'); ?>" rel="navigation">Browser Object</a>
							   </li>
                               <li>
								  <a href="<?php echo site_url('browser/manage_browser_object_col'); ?>" rel="navigation">Browser Object Column</a>
							   </li>
                               <li>
								  <a href="<?php echo site_url('browser/manage_browser_object_col_ref'); ?>" rel="navigation">Browser Object Column Ref</a>
							   </li>
                               
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
                 <div class="accordion-group">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#a6">
                            <div class="role">&nbsp;</div><span class="pull-left">Master Tables</span>
                        </a>
                    </div>
                    <div id="a6" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <ul>
                               <li>
								  <a href="<?php echo site_url('master_table/master_table_json'); ?>" rel="navigation">Master Table JSON</a>
							   </li>
                               
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
                 
                    
                   
                </div>
                
            </div>
        </div>
                 
    </div>
  </div>    
  <!-- //Left Panel -->  
  
  <!-- Dashboard Container -->
  <div class="containerWrap">
  	<div id="containerMain">
    	<!-- Coding Part Starts Here -->
        
    	
