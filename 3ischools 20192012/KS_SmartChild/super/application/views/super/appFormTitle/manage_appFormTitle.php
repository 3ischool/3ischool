<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "App Form Title";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	   
                                	 <div class="btn-group pull-right left10">
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('appFormSetup/add_appFormTitle'); ?>">
                                        	Add
                                        </a>
                                        
                                    </div>                               
                                     
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                    
                                  
                        <div class="row-fluid ">
                         
                                    <div class="row-fluid">
										
									 <!-- Filter -->		
                                	
									 <!-- App Form -->	 
                                	 <select id="appForm" class="SmartSearch" onchange="filterByAppFormLang();">
									 <option value="">Select AppForm</option>	 
									 <?php foreach($appForm as $appForm){ ?>	
										 <option <?php if($app_form_id == $appForm['AppFormId']){ ?>selected=selected<?php } ?> value="<?php echo base64e($appForm['AppFormId']); ?>"><?php echo $appForm['AppForm']; ?></option>
									 <?php } ?>	  
                                	 </select>
                                	 <!-- Language -->
                                	 <select id="language" class="SmartSearch" onchange="filterByAppFormLang();">
									  <option value="">Select Language</option>	 
                                	  <?php foreach($language as $language){ ?>	
										 <option <?php if($language_id == $language['LanguageId']){ ?>selected=selected<?php } ?> value="<?php echo base64e($language['LanguageId']); ?>"><?php echo $language['Language']; ?></option>
									 <?php } ?>	 
                                	 </select>
                                	 <?php if($language_id != ''){ ?>
											<a onclick="generateJSON('<?php echo base64e($language_id); ?>');" class="btn btn-xs btn-success">Generate JSON</a>
									 <?php } ?>	 
                                	 </div>
                                	 <!-- /Filter -->
                                	 
                                	 
                                    <table id="myTable" class="table table-bordered" style="border-top:0px;">
                                      <thead>	
                                    	<tr>
                                        	<th>App Form Title</th>
                                        	<th>App Form </th>
                                        	<th>Language</th>
                                        	<th>Action</th>
                                        </tr>
                                       </thead>
                                       <tbody>     
                                        <?php foreach($appFormTitle as $appFormTitle){ ?>
                                        <tr>
                                        	<td><input onblur="updateTitle(this.value,'<?php echo $appFormTitle['AppFormTitleId']; ?>');" id="title<?php echo $appFormTitle['AppFormTitleId']; ?>" value="<?php echo $appFormTitle['AppFormTitle']; ?>" data-url="<?php echo site_url('appFormSetup/update_app_form_title/'.base64e($appFormTitle['AppFormTitleId'])); ?>">
                                        	<i class="icon-check" id="afrm<?php echo $appFormTitle['AppFormTitleId'];?>" style="color:#4CAF50; display:none; font-size:17px;"></i>
													
                                        	</td>
                                        	<td><?php echo $appFormTitle['AppForm']; ?></td>
											<td><?php echo $appFormTitle['Language']; ?></td>
											<td>
                                            <a href="<?php echo site_url('appFormSetup/edit_appFormTitle/'.base64e($appFormTitle['AppFormTitleId'])); ?>" class="btn btn-xs btn-default" title="Edit"> <i class="icon icon-pencil"></i></a>
											<a href="<?php echo site_url('appFormSetup/delete_appFormTitle/'.base64e($appFormTitle['AppFormTitleId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											</td>
                                        </tr>
                                       <?php } ?>
                                       </tbody> 
                                    </table>
                                    
                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>
            	
                
            </div>
        </div>
        <!-- //Dash Content -->
        <script>
        function filterByAppFormLang(){
				
				var appForm_id = $('#appForm option:selected').val();
				var language_id = $('#language option:selected').val();
				if(appForm_id == ''){
					appForm_id = 'unset';
					}
				if(language_id == ''){
					language_id = 'unset';
					}
				var urll = '<?php echo site_url('appFormSetup/set_appform_title_filters'); ?>/'+appForm_id+'/'+language_id; 
				window.location.href = urll;
		}
		
		function generateJSON(lang_id){
				
				var obj = $(this);
				var urll = '<?php echo site_url('appFormSetup/generate_app_form_title_json'); ?>/'+lang_id;
				$.post( urll, function(data){
						alert(data);
					
				});
		}
		
		function updateTitle(title,id){
				 
				 var successID = '#afrm'+id;
				 $.post( $('#title'+id).attr('data-url'), {titleval:title},function(data){
					if(Math.floor(data) == data && $.isNumeric(data)){
						$(successID).show(800).delay(3000).hide(800);
					}else{
						alert(data);
						}
				});
			}
		
        </script>     
       <?php $this->load->view('super/assets/footer');?>
