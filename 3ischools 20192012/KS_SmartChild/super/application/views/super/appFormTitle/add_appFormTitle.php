<?php $this->load->view('super/assets/header');?>
 <?php 
 $function_name = $this->uri->segment(2);
 if($function_name == 'add_appFormTitle'){
	 $appFormTitle = set_value('appFormTitle');
	 $appFormId = set_value('appFormId');
	 $languageId = set_value('languageId');
	 $button_val = 'Add';
	} 
 else{
	 $appFormTitle = $result[0]['AppFormTitle'];
	 $appFormId = $result[0]['AppFormId'];
	 $languageId = $result[0]['LanguageId'];
	 $button_val = 'Update';
	} 	
 ?>       
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "App Form Title";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                   
                  <div class="nav-collapse collapse">
                    <ul class="nav">
                         
                      </ul>                    
                  </div><!--/.nav-collapse -->
                   <a href="<?php echo site_url('appFormSetup/manage_appFormTitle');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
						
						  <div class="span6">
                              <label>Form Field Title</label>
                               <input id="appFormTitle" name="appFormTitle" placeholder="" class="span12" type="text" value="<?php echo $appFormTitle; ?>">
							   <?php  echo form_error('appFormTitle','<span class="label label-important">','</span>');?>	
                          </div> 	
						  
						  <div class="span6">
                              <label>App Form</label>   
                              <select class="span12 SmartSearch" name="appFormId">
                              <option value="">Select</option>
                              <?php 
                              foreach($appForm as $appForm){
                              ?>
                              <option <?php if($appFormId == $appForm['AppFormId']){ ?>selected=selected <?php } ?> value="<?php echo $appForm['AppFormId']; ?>"><?php echo $appForm['AppForm']; ?></option>
                              <?php } ?>
                              </select>    
                              <?php  echo form_error('appFormId','<span class="label label-important">','</span>');?>                  
                          </div>  	
                         
                        </div>
                      <div class="row-fluid top10"> 
					 
                      <div class="span6">
                              <label>Language</label>   
                              <select class="span12" name="languageId">
                              <option value="">Select</option>
                              <?php 
                              foreach($language as $language){
                              ?>
                              <option <?php if($languageId == $language['LanguageId']){ ?>selected=selected <?php } ?> value="<?php echo $language['LanguageId']; ?>"><?php echo $language['Language']; ?></option>
                              <?php } ?>
                              </select>    
                              <?php  echo form_error('languageId','<span class="label label-important">','</span>');?>                  
                          </div>  	
                       </div>
                      
                        <div class="row-fluid top10">
                         <div class="span6">
                              <input type="submit" class="btn" value="<?php echo $button_val; ?>">                     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
