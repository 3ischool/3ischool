<?php $this->load->view('super/assets/header');?>
   
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Change Password";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                   
                 
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
                          <div class="span6">
                              <label>Old Password</label>
								<input id="old_password" name="old_password" placeholder="" class="span12" type="password" value="">
							    <?php  echo form_error('old_password','<span class="label label-important">','</span>');?>
						  </div> 
						  </div> 
                          <div class="row-fluid top10">
                          <div class="span6">
                              <label>New Password</label>
                               <input id="new_password" name="new_password" placeholder="" class="span12" type="password" value="">
							   <?php  echo form_error('new_password','<span class="label label-important">','</span>');?>	
                          </div> 
                        </div>
                        <div class="row-fluid top10">
                          <div class="span6">
                              <label>Confirm Password</label>
                              <input id="confirm_password" name="confirm_password" placeholder="" class="span12" type="password" value="">
                              <?php  echo form_error('confirm_password','<span class="label label-important">','</span>');?>
                          </div> 
                         </div>  
                        <div class="row-fluid top10">
                         
                          <div class="span6">
                              <input type="submit" class="btn" value="Submit">                     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
