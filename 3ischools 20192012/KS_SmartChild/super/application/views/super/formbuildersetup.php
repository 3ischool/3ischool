<?php $this->load->view('super/assets/header');?>
       
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Form Builder Setup";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        
        <div class="clearfix"></div>
        
        
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid">
            	<div class="row-fluid">
					<div class="span12">
						<iframe src="<?php echo base_url('form-setup');?>" style="width:100%; height:3500px; float:left" frameborder="0" scrolling="no"></iframe>
					</div>            
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
