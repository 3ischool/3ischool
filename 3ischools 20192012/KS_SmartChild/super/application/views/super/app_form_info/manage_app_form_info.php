<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "App Form Info";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	 
                                	 <div class="btn-group pull-right left10">
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('appFormSetup/add_app_form_info'); ?>">
                                        	Add
                                        </a>
                                        
                                    </div>                               
                                    
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                    
                                  
                        <div class="row-fluid ">
                        
                        	<div class="span12 dashBoxes" style="overflow:hidden;">
                                	<div class="dashBoxTitle">
                                	<span class="title">Manage </span>
                                      <span class="pull-right">Total records : <?php echo $total_rows;?> &nbsp;&nbsp;&nbsp;</span>                         
                                                        
                                </div>
                                
                                    <div class="row-fluid">
                                	 
                                    <table class="table table-bordered" id="myTable1" style="border-top:0px;">
										<thead>
                                    	<tr>
                                        	<th>Form</th>
                                        	<th>Form Use</th>
                                        	<th>Form Note</th>
                                        	<th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>    
                                        <?php foreach($app_form_info as $form_info){ ?>
                                        <tr>
                                        	
                                        	<td><?php foreach($app_form as $form){ 
												if($form['AppFormId'] == $form_info['AppFormId']) echo $form['AppForm']; 
												}?>
												</td>
											<td><?php echo substr($form_info['AppFormUse'],0,25);if(strlen($form_info['AppFormUse']) > 25){echo '...';};  ?></td>	
											<td><?php echo substr($form_info['AppFormNote'],0,25);if(strlen($form_info['AppFormNote']) > 25){echo '...';};  ?></td>
                                           <td>
                                            <a href="<?php echo site_url('appFormSetup/edit_app_form_info/'.base64e($form_info['AppFormInfoId'])); ?>" class="btn btn-xs btn-default" title="Edit"> <i class="icon icon-pencil"></i></a>
											<a href="<?php echo site_url('appFormSetup/delete_app_form_info/'.base64e($form_info['AppFormInfoId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											</td>
                                        </tr>
                                       <?php } ?>
                                       </tbody> 
                                    </table>
                                  <div class="boxGraphFooter" style="border-radius:0; margin:0; padding:0 15px;">
                                		 
                                            		<?php
													  echo $links;
													  ?>
                                            
                                        
                                        
                                	</div>  
                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>
            	
                
            </div>
        </div>
        <!-- //Dash Content -->
             
       <?php $this->load->view('super/assets/footer');?>
