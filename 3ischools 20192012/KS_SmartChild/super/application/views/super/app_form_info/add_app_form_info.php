<?php $this->load->view('super/assets/header');?>
 <?php 
 $function_name = $this->uri->segment(2);
 if($function_name == 'add_app_form_info'){
	 $form_id = set_value('form_id');
	 $form_use = set_value('form_use');
	 $form_note = set_value('form_note');
	 $button_val = 'Add';
	} 
 else{
	 $form_id = $result[0]['AppFormId'];
	 $form_use = $result[0]['AppFormUse'];
	 $form_note = $result[0]['AppFormNote'];
	 $button_val = 'Update';
	} 	
 ?>       
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "App Form Info";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                   
                  <div class="nav-collapse collapse">
                    <ul class="nav">
                      <li><a href="#">Advanced Segments</a></li>
                      <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Email <b class="icon-caret-down"></b></a>
                        <ul class="dropdown-menu">
                          <li><a href="#">Action</a></li>
                          <li><a href="#">Another action</a></li>
                          
                        </ul>
                      </li>
                      <li><a href="#">Export</a></li>    
                      </ul>                    
                  </div><!--/.nav-collapse -->
                   <a href="<?php echo site_url('appFormSetup/manage_app_form_info');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">  
							 <div class="span6">
                              <label>Form</label>  
                              <select class="span12 SmartSearch" name="form_id">
                              <option value="">Select</option>
                              <?php  
                              foreach($app_form as $form){
                              ?>
                              <option <?php if($form_id == $form['AppFormId']){ ?>selected=selected <?php } ?> value="<?php echo $form['AppFormId']; ?>"><?php echo $form['AppForm']; ?></option>
                              <?php } ?>
                              </select>    
                              <?php  echo form_error('form_id','<span class="label label-important">','</span>');?>                  
                          </div>  	
                         
                       </div>
						              
                        <div class="row-fluid top10">
						
                              <label>Form Use</label>
                              <textarea name="form_use" class="span12 textarea" rows="4" cols="50"><?php echo $form_use;?></textarea>
						      <?php  echo form_error('form_use','<span class="label label-important">','</span>');?>	
                        </div> 
                        <div class="row-fluid top10">
                              <label>Form Note</label>
                              <textarea name="form_note" class="span12 textarea" rows="4" cols="50"><?php echo $form_note;?></textarea>
						      <?php  echo form_error('form_note','<span class="label label-important">','</span>');?>	
                         </div> 	
						
                        <div class="row-fluid top10">
                         
                          <div class="span6">
                              <input type="submit" class="btn" value="<?php echo $button_val; ?>">                     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
