<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>GSTPad</title>
	<meta name="description" content="TraKomo" />
	<meta name="keywords" content="TraKomo" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
     
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo base_url();?>assets/css/base.css" rel="stylesheet">     
    <link href="<?php echo base_url();?>assets/css/base-fluid.css" rel="stylesheet">     
    <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/green.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/custom-fluid.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome-ie7.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/js/mczToastNotifications/jquery.mczToast.css" rel="stylesheet">
	 
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.js"></script>
    <![endif]-->
    <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/js/common_ajax.js"></script>
    <script src="<?php echo base_url();?>assets/js/base.js"></script>    
	<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/mczToastNotifications/jquery.mczToast.js"></script>
    
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    
    	 
  </head>
  <body> 
   <div class="container">

      <form method="post" action="<?php echo site_url('user/login');?>" class="form-signin">
        <h2 class="form-signin-heading">Please sign in</h2>
        <?php  echo form_error('usr','<span class="label label-important">','</span>');?>
        <input type="text" class="input-block-level" placeholder="Email address" name="usr" value="<?php echo set_value('usr');?>">
        <?php  echo form_error('pwd','<span class="label label-important">','</span>');?>
        <input type="password" class="input-block-level" placeholder="Password" name="pwd">
        
        <input class="btn btn-large btn-primary" type="submit" value="Sign in"> 
         
      </form>

    </div>
  </body>
</html>
 
