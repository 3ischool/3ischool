<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Assign Tab Widgets Permission";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);
    	?>
        <?php
        $user_role_id = set_value('user_role');
        $wid_tab_map_id = set_value('wid_tab_map_id');
        if($this->uri->segment(3) != ''){
			$user_role_id	= $this->uri->segment(3);
		}
        ?>
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	 
        	<div class="btn-group pull-left left15">
                                        
                                    </div>
                                	 <div class="btn-group pull-right left10">
                                        
                                    </div>                               
                                    <div class="btn-group pull-right left10">
                                         
                                    </div>
                                    
                                    <div class="btn-group pull-right">
                                        
                                    </div>
                                    
                                    
                                    
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                    
                        <?php
						   $msg = $this->session->userdata('msg');
						   if($msg != ''){
							$this->session->unset_userdata('msg');
						   ?> 
							<div class="alert alert-success alert-dismissable">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <?php echo $msg; ?>
							</div>  
						   <?php	   
							}
						   ?>  
			 
            	<div class="row-fluid">
				 <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                         
					 <div class="row-fluid top10">		
                         <div class="span2">
                              <label>User Role Type</label>
                               <select id="user_role" name="user_role" class="span12" onchange="reloadGrid();">
									<option value="">Select User Role </option>
									<?php foreach($user_role_rec as $user_role_rec){ ?>
									 <option <?php if(base64d($user_role_id) == $user_role_rec['UserRoleTypeId']){ ?>selected=selected<?php } ?> value="<?php echo base64e($user_role_rec['UserRoleTypeId']); ?>"><?php echo $user_role_rec['UserRoleType']; ?></option>
									<?php } ?>  
                               </select>
							   <?php  echo form_error('user_role','<span class="label label-important">','</span>');?>	
                          </div> 
                       	<?php if(!empty($widget_tab_map_rec)){ ?>
                         <div class="span3">
                              <label>Tab Widget</label>
                               <select id="widget_id" name="wid_tab_map_id[]" class="span12 tab_widget_multiselect" multiple>
									<?php foreach($widget_tab_map_rec as $widget_rec){ ?>
									 <option <?php if($wid_tab_map_id == $widget_rec['TabWidgetId']){ ?>selected=selected<?php } ?> value="<?php echo $widget_rec['TabWidgetId']; ?>"><?php echo $widget_rec['TabTitle'].' - '.$widget_rec['WidgetTitle']; ?></option>
									<?php } ?> 
                               </select>
							   <?php  echo form_error('wid_tab_map_id[]','<span class="label label-important">','</span>');?>	
                          </div> 
						 
                          <div class="span2">
							  <label>&nbsp;</label>
                              <input type="submit" name="publish" class="btn" value="Save">                     
                          </div>  
                           <?php } ?>	
                        </div> 
                       </div> 
                       </form> 
                    </div>  
				  </div>  
			    
			  <!-- Table -->  
						 
                        <div class="row-fluid">
									
                        	<div class="span12 dashBoxes" style="overflow:hidden;">
                                	 
                                
                                    <div class="row-fluid">
                                	 
                                   <form id="formID" method="post" action="<?php echo site_url('widget/update_sorting/Wid_TabWidget/TabWidgetId'); ?>">
									<table class="table table-bordered" id="myTable" style="border-top:0px;">
									 <thead>	 
                                    	<tr>
											<th>Tab</th> 
											<th>Widget</th> 
                                        	<th>Action</th>
                                        </tr>
                                       </thead>
                                       <tbody> 
                                        <?php foreach($widget_tab_user_role_rec as $rec){ ?>
                                        <tr>
                                        	<td><?php echo $rec['TabTitle']; ?></td>
                                        	<td><?php echo $rec['WidgetTitle']; ?></td>
                                        	<td>
                                            <a href="<?php echo site_url('widget/delete_widget_tab_user_role/'.base64e($rec['TabWidgetUserRoleTypeId']).'/'.$user_role_id); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											</td>
                                        </tr>
                                       <?php } ?>
                                       </tbody> 
                                      </form> 
                                    </table>
                                    
                                	</div>  
                                
                                </div>
                                 
							</div> 
							               
                        </div>
                        
                    </div>
                </div>
            	
            </div>
        </div>
        <!-- //Dash Content -->
        <script>
         function reloadGrid(){
			 
				var userRole = $('#user_role :selected').val();
				var formAction = '<?php echo site_url('widget/manage_widget_tab_user_role/'); ?>'+userRole;
				window.location.href = formAction;
				
		 }
		 $('.tab_widget_multiselect').multiselect({
			 buttonWidth : "80%",
			 //maxHeight : 600,
			 nonSelectedText : "Select Tab - Widget"
		});
		
        </script>     
       <?php $this->load->view('super/assets/footer');?>
