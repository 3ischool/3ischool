<?php $this->load->view('super/assets/header_popup');?>
  <?php
      $widget_type_id = set_value('widget_type');
	  $css_class = set_value('css_class');
	  $css_code = set_value('css_code');
   ?>
    	
<style>
	.customDivParent{ float:left; width:100%}
.customdiv{ width:40%; float:left; padding-left:30px;}
.customdiv1{ width:40%; float:right;  padding-right:30px;}
</style>
        <!-- Dash Content -->
        <div class="dashContent">
			<h5 class="text-center text-info">Add Color Class</h5>
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="customDivParent">
						
						  <div class="customdiv">
                              <label>Css Class</label>
                               <input id="css_class" name="css_class" placeholder="" class="span12" type="text" value="<?php echo $css_class; ?>">
							   <?php  echo form_error('css_class','<span class="label label-important">','</span>');?>	
                          </div> 
                           <div class="customdiv1">	
							   <label>Css Code</label>
                               <textarea id="css_code" name="css_code" class="span12" type="text" ><?php echo $css_code; ?></textarea>
							   <?php  echo form_error('css_code','<span class="label label-important">','</span>');?>	
						   </div> 	
                        </div> 	
                        <div class="customDivParent">
                          <div class="customdiv">
							  <label>&nbsp;</label>
                              <input type="submit" name="publish2" class="btn" value="Save"> 
                              <a href="javascript:void(0)" onclick="parent.hidePopupManual()" class="btn btn-default">Close</a>
							</div>  
                        </div> 
                     </div> 	
					  
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
        <?php 
        $widget_type_id = (int)base64d($this->uri->segment(3));
        $color_class_id = (int)$this->uri->segment(4);
        if($color_class_id>0){ ?>
			<script>
			window.onload= function(){
				parent.reload_color_class(<?php echo $widget_type_id;?>,<?php echo $color_class_id;?>);
			}	
			</script>
		<?php } ?>	     
      <?php $this->load->view('super/assets/footer_popup');?>
 
