<?php $this->load->view('super/assets/header');?>
  <?php
  if($this->uri->segment(2) == 'edit_color_class_master'){
	  $widget_type_id = $edit_rec[0]['WidgetTypeId'];
	  $css_class = $edit_rec[0]['CSSClass'];
	  $css_code = $edit_rec[0]['CssCode'];
  }else{
	  $widget_type_id = set_value('widget_type');
	  $css_class = set_value('css_class');
	  $css_code = set_value('css_code');
	  }
  ?>
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Color Class Master";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                    
                   <a href="<?php echo site_url('widget/manage_color_class_master');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
						
						  <div class="span6">
                              <label>Css Class</label>
                               <input id="css_class" name="css_class" placeholder="" class="span12" type="text" value="<?php echo $css_class; ?>">
							   <?php  echo form_error('css_class','<span class="label label-important">','</span>');?>	
                          </div> 
                          <div class="span6">
                              <label>Widget Type</label>
                               <select id="widget_type" name="widget_type" class="span12" >
									<option value="">Select Widget Type</option>
									<?php foreach($widget_type_rec as $rec){ ?>
										<option <?php if($rec['WidgetTypeId'] == $widget_type_id){?>selected=selected<?php } ?> value="<?php echo $rec['WidgetTypeId']; ?>"><?php echo $rec['WidgetType']; ?></option>
									<?php } ?>	
                               </select>
							   <?php  echo form_error('widget_type','<span class="label label-important">','</span>');?>	
                          </div> 	
                     </div> 	
					 <div class="row-fluid top10">		
                           <div class="span6">
                              <label>Css Code</label>
                               <textarea id="css_code" name="css_code" class="span12" type="text" ><?php echo $css_code; ?></textarea>
							   <?php  echo form_error('css_code','<span class="label label-important">','</span>');?>	
                          </div> 
                          <div class="span6">
							  <label>&nbsp;</label>
                              <input type="submit" name="publish" class="btn" value="Save & Continue">                     
                              <input type="submit" name="publish2" class="btn" value="Save & Exit">                     
                          </div>   
                        </div>
                          
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
