<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Assign Widgets to Tab";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);
    	?>
        <?php
        $tab_id = set_value('tab_id');
        $widget_id = set_value('widget_id');
        if($this->uri->segment(3) != ''){
			$tab_id	= $this->uri->segment(3);
		}
        ?>
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	 
        	<div class="btn-group pull-left left15">
                                        
                                    </div>
                                	 <div class="btn-group pull-right left10">
                                        
                                    </div>                               
                                    <div class="btn-group pull-right left10">
                                         
                                    </div>
                                    
                                    <div class="btn-group pull-right">
                                        
                                    </div>
                                    
                                    
                                    
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                    
                        <?php
						   $msg = $this->session->userdata('msg');
						   if($msg != ''){
							$this->session->unset_userdata('msg');
						   ?> 
							<div class="alert alert-success alert-dismissable">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <?php echo $msg; ?>
							</div>  
						   <?php	   
							}
						   ?>  
			 
            	<div class="row-fluid">
				 <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                         
					 <div class="row-fluid top10">		
                         <div class="span2">
                              <label>Tab</label>
                               <select id="tab_id" name="tab_id" class="span12" onchange="reloadGrid();">
									<option value="">Select Tab</option>
									<?php foreach($tab_area_rec as $tab_area_rec){ ?>
									 <option <?php if(base64d($tab_id) == $tab_area_rec['TabId']){ ?>selected=selected<?php } ?> value="<?php echo base64e($tab_area_rec['TabId']); ?>"><?php echo $tab_area_rec['TabTitle']; ?></option>
									<?php } ?>  
                               </select>
							   <?php  echo form_error('tab_id','<span class="label label-important">','</span>');?>	
                          </div> 
                       	<?php if(!empty($widget_rec)){ ?>
                         <div class="span3">
                              <label>Widget</label>
                               <select id="widget_id" name="widget_id[]" class="span12 widget_multiselect" multiple>
									<?php foreach($widget_rec as $widget_rec){ ?>
									 <option <?php if($widget_id == $widget_rec['WidgetId']){ ?>selected=selected<?php } ?> value="<?php echo $widget_rec['WidgetId']; ?>"><?php echo $widget_rec['WidgetTitle']; ?></option>
									<?php } ?> 
                               </select>
							   <?php  echo form_error('widget_id[]','<span class="label label-important">','</span>');?>	
                          </div> 
						 
                          <div class="span2">
							  <label>&nbsp;</label>
                              <input type="submit" name="publish" class="btn" value="Save">                     
                          </div>  
                           <?php } ?>	
                        </div> 
                       </div> 
                       </form> 
                    </div>  
				  </div>  
			    
			  <!-- Table -->  
						 
                        <div class="row-fluid">
									
                        	<div class="span12 dashBoxes" style="overflow:hidden;">
                                	 
                                
                                    <div class="row-fluid">
                                	 
                                   <form id="formID" method="post" action="<?php echo site_url('widget/update_sorting/Wid_TabWidget/TabWidgetId'); ?>">
									<table class="table table-bordered" id="myTable sort" style="border-top:0px;">
									 <thead>	 
                                    	<tr>
											<th style="display:none;"></th>
                                        	<th>Widget</th> 
                                        	<th>Status</th> 
                                        	<th>Action</th>
                                        </tr>
                                       </thead>
                                       <tbody id="sortable"> 
                                        <?php foreach($widget_tab_map_rec as $rec){ ?>
                                        <tr class="ui-sortable">
                                        	<td style="display:none;" class="ui-sortable-handle"><input type="hidden" name="ids[]" value="<?php echo $rec['TabWidgetId']; ?>" /></td>
											<td class="ui-sortable-handle"><?php echo $rec['WidgetTitle']; ?></td>
                                        	<?php
                                        	 if($rec['IsActive'] == 1){
												$is_active = 'Active';
											 }
											 else{
												$is_active = 'Inactive';	
											 }
                                        	 ?>
                                        	<td class="ui-sortable-handle"><a href="<?php echo site_url('widget/update_tab_wid_map_status/'.base64e($rec['TabWidgetId']).'/'.$rec['IsActive'].'/'.$tab_id); ?>" ><?php echo $is_active; ?></a></td> 
                                        	<td class="ui-sortable-handle">
                                             
											<?php
											$rData = $this->common_model->compare_in_table('Wid_TabWidgetUserRoleType','TabWidgetId',$rec['TabWidgetId']);
											if(empty($rData)){
											?>
											<a href="<?php echo site_url('widget/delete_widget_tab_map/'.base64e($rec['TabWidgetId']).'/'.$tab_id); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											<?php } ?>
											</td>
                                        </tr>
                                       <?php } ?>
                                       </tbody> 
                                      </form> 
                                    </table>
                                    <input type="submit" id="submitID" style=" display:none" />
                                     
                                	</div>  
                                
                                </div>
                                 
							</div> 
							                      
                        </div>
                        
                    </div>
                </div>
            	
            </div>
        </div>
        <!-- //Dash Content -->
        <script>
         function reloadGrid(){
			 
				var tabId = $('#tab_id :selected').val();
				var formAction = '<?php echo site_url('widget/manage_widget_tab_map/'); ?>'+tabId;
				window.location.href = formAction;
				
		 }
		 $('.widget_multiselect').multiselect({
			 buttonWidth : "80%",
			 //maxHeight : 200,
			 nonSelectedText : "Select Widget"
		});
        </script>     
       <?php $this->load->view('super/assets/footer');?>
