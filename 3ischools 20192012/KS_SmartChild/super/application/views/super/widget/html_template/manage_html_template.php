<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "HTML Template";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	 
        	<div class="btn-group pull-left left15">
                                        
                                    </div>
                                	 <div class="btn-group pull-right left10">
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('widget/add_html_template'); ?>">
                                        	Add
                                        </a>
                                        
                                    </div>                               
<!--
                                    <div class="btn-group pull-right left10">
                                         <a class="btn" href="javascript:void(0);" onclick="generateJSON();">
                                        	Generate JSON
                                        </a>
                                        
                                    </div>
-->
                                    
                                    <div class="btn-group pull-right">
                                        
                                    </div>
                                    
                                    
                                    
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                    
                        <?php
						   $msg = $this->session->userdata('msg');
						   if($msg != ''){
							$this->session->unset_userdata('msg');
						   ?> 
							<div class="alert alert-success alert-dismissable">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <?php echo $msg; ?>
							</div>  
						   <?php	   
							}
						   ?>          
                        <div class="row-fluid ">
                        
                        	<div class="span12 dashBoxes" style="overflow:hidden;">
                                	 
                                
                                    <div class="row-fluid">
                                	 
                                    <table class="table table-bordered" id="myTable" style="border-top:0px;">
									   <thead>	 
                                    	<tr>
                                        	<th>Title</th> 
                                        	<th>Status</th> 
                                        	<th>Action</th>
                                        </tr>
                                       </thead>
                                       <tbody> 
                                        <?php foreach($html_template_rec as $rec){ ?>
                                        <tr>
                                        	<td><?php echo $rec['HTMLTemplateTitle']; ?></td>
                                        	 <?php
                                        	 if($rec['IsActive'] == 1){
												$is_active = 'Active';
											 }
											 else{
												$is_active = 'Inactive';	
											 }
                                        	 ?>
                                        	<td><a href="<?php echo site_url('widget/update_status/'.base64e($rec['HTMLTemplateId']).'/'.$rec['IsActive'].'/Wid_HTMLTemplate/HTMLTemplateId/manage_html_template'); ?>" ><?php echo $is_active; ?></a></td> 
                                        	<td>
                                            <a href="<?php echo site_url('widget/edit_html_template/'.base64e($rec['HTMLTemplateId'])); ?>" class="btn btn-xs btn-default" title="Edit"> <i class="icon icon-pencil"></i></a>
											<?php
											$rData = $this->common_model->compare_in_table('Wid_WidgetType','HTMLTemplateId',$rec['HTMLTemplateId']);
											if(empty($rData)){
											?>
											<a href="<?php echo site_url('widget/delete_html_template/'.base64e($rec['HTMLTemplateId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											<?php } ?>
											</td>
                                        </tr>
                                       <?php } ?>
                                       </tbody> 
                                    </table>
                                    <div class="boxGraphFooter" style="border-radius:0; margin:0; padding:0 15px;">
                                		 
                                            		<?php
													 // echo $links;
													  ?>
                                            
                                        
                                        
                                	</div>  
                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>
            	
                
            </div>
        </div>
        <!-- //Dash Content -->
        <script>
			function generateJSON(){
					
					var formAction = '<?php echo site_url('widget/generate_html_template_json'); ?>';
					$.ajax({
						type   : 'POST',
						url : formAction,
						success : function(result){
							if(result == 1){
										alert('JSON generated successfully');
									}
									else{
										alert(result);	
									}
							}
					 });
			}
        </script>     
       <?php $this->load->view('super/assets/footer');?>
