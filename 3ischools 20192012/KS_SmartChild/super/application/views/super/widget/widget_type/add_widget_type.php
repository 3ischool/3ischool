<?php $this->load->view('super/assets/header');?>
  <?php
  if($this->uri->segment(2) == 'edit_widget_type'){
	  $widget_type = $edit_rec[0]['WidgetType'];
	  $template_id = $edit_rec[0]['HTMLTemplateId'];
  }else{
	  $widget_type = set_value('widget_type');
	  $template_id = set_value('html_template');
	  }
  ?>
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Widget Type";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                    
                   <a href="<?php echo site_url('widget/manage_widget_type');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
						
						  <div class="span6">
                              <label>Widget Type</label>
                               <input id="widget_type" name="widget_type" placeholder="" class="span12" type="text" value="<?php echo $widget_type; ?>">
							   <?php  echo form_error('widget_type','<span class="label label-important">','</span>');?>	
                          </div> 	
                     </div> 	
					 <div class="row-fluid top10">		
                         <div class="span6">
                              <label>Html Template</label>
                               <select id="html_template" name="html_template" class="span12" >
									<option value="">Select Template</option>
									<?php foreach($template_rec as $rec){ ?>
										<option <?php if($rec['HTMLTemplateId'] == $template_id){?>selected=selected<?php } ?> value="<?php echo $rec['HTMLTemplateId']; ?>"><?php echo $rec['HTMLTemplateTitle']; ?></option>
									<?php } ?>	
                               </select>
							   <?php  echo form_error('html_template','<span class="label label-important">','</span>');?>	
                          </div> 
                           
                        </div>
                         
                        <div class="row-fluid top10">
                          <div class="span6">
                              <input type="submit" name="publish" class="btn" value="Save & Continue">                     
                              <input type="submit" name="publish2" class="btn" value="Save & Exit">                     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
