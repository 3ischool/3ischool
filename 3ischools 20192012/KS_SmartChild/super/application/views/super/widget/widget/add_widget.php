<?php $this->load->view('super/assets/header');?>
  <?php
  if($this->uri->segment(2) == 'edit_widget'){
	  $title = $edit_rec[0]['WidgetTitle'];
	  $widget_type_id = $edit_rec[0]['WidgetTypeId'];
	  $info_text = $edit_rec[0]['InfoText'];
	  $sp_name = $edit_rec[0]['SPName'];
	  $click_url = $edit_rec[0]['ClickURL'];
	  $widget_icon = $edit_rec[0]['WidgetIcon'];
	  $color_class_id = $edit_rec[0]['ColorClassId'];
  }else{
	  $title = set_value('title');
	  $widget_type_id = set_value('widget_type_id');
	  $info_text = set_value('info_text');
	  $sp_name = set_value('sp_name');
	  $click_url = set_value('click_url');
	  $widget_icon = set_value('userfile');
	  $color_class_id = set_value('color_class_id');
	  }
	if($widget_type_id != ''){
	  ?>
	  <style>
		  .widget_type{display:block !important}
	  </style>
	  <?php	   
	  }
	    
  ?>
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Widget";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                    
                   <a href="<?php echo site_url('widget/manage_widget');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					 
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open_multipart(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
						
						  <div class="span4">
                              <label>Widget Title</label>
                               <input id="title" name="title" placeholder="" class="span12" type="text" value="<?php echo $title; ?>">
							   <?php  echo form_error('title','<span class="label label-important">','</span>');?>	
                          </div> 	
						  <div class="span4">
                              <label>Widget Type</label>
                               <select id="widget_type_id" name="widget_type_id" class="span12" onchange="loadColorClass();">
									<option value="">Select Area</option>
									<?php foreach($widget_type_rec as $rec){ ?>
										<option <?php if($rec['WidgetTypeId'] == $widget_type_id){?>selected=selected<?php } ?> value="<?php echo $rec['WidgetTypeId']; ?>"><?php echo $rec['WidgetType']; ?></option>
									<?php } ?>	
                               </select>
							   <?php  echo form_error('widget_type_id','<span class="label label-important">','</span>');?>	
                          </div> 
                           <div class="span4 widget_type" id="widget_type" style="display:none">
                              <label><a class="popupclass" id="color_class_popup" href="<?php echo site_url('widget/add_color_class_popup'); ?>"><i class="icon icon-plus"></i> Color Class</a></label>
                               <select id="color_class_id" name="color_class_id" class="span12">
									 <option value="">Select Color</option>
									 <?php foreach($color_classes as $color_classes){ ?>
										<option <?php if($color_class_id == $color_classes['ColorClassId']){ ?>selected=selected<?php } ?> value="<?php echo $color_classes['ColorClassId']; ?>"><?php echo $color_classes['CSSClass']; ?></option>
									 <?php } ?>	 
                               </select>
							   <?php  echo form_error('color_class_id','<span class="label label-important">','</span>');?>	
                          </div> 
                          	
                        </div>
                        
                        <div class="row-fluid top10">
						
						  <div class="span4">
                              <label>Info Text</label>
                               <input id="info_text" name="info_text" placeholder="" class="span12" type="text" value="<?php echo $info_text; ?>">
							   <?php  echo form_error('info_text','<span class="label label-important">','</span>');?>	
                          </div> 	
						  <div class="span4">
                              <label>SP Name</label>
                               <input id="sp_name" name="sp_name" placeholder="" class="span12" type="text" value="<?php echo $sp_name; ?>">
							   <?php  echo form_error('sp_name','<span class="label label-important">','</span>');?>	
                          </div> 
						  <div class="span4">
                              <label>Click Url</label>
                               <input id="click_url" name="click_url" placeholder="" class="span12" type="text" value="<?php echo $click_url; ?>">
							   <?php  echo form_error('click_url','<span class="label label-important">','</span>');?>	
                          </div> 
						  
                        </div> 
                        <div class="row-fluid top10">
						   <div class="span4">
                              <label>Widget Icon</label>
                               <input type="file" class="span12" name="userfile" id="userfile">
                               <?php if($widget_icon != ''){ ?>
                               <img height="30" width="30" src="<?php echo base_url('uploads/widget_icon/'.$widget_icon); ?>">
                               <?php } ?>
							   <?php  echo form_error('userfile','<span class="label label-important">','</span>');?>	
                          </div> 	
						    
                        </div> 
                        <div class="row-fluid top10">  	
                          <div class="span6">
							  <?php if($this->uri->segment(2) != 'edit_widget'){ ?>
                              <input type="submit" name="publish" class="btn" value="Save & Continue">  
                              <?php } ?>                   
                              <input type="submit" name="publish2" class="btn" value="Save & Exit">                     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
        <script>
        function loadColorClass(){
				var widgetTypeId = $('#widget_type_id :selected').val();
				if(widgetTypeId != ''){
					$.ajax({
						type   : 'POST',
						url : '<?php echo site_url('widget/getColorClasses/'); ?>'+widgetTypeId,
						success : function(data){
							//alert(data);
									var json = $.parseJSON(data);
									$("#color_class_id").html(json.color_classes);
									var color_class_popup = '<?php echo site_url('widget/add_color_class_popup/'); ?>'+widgetTypeId;
									$('#color_class_popup').attr('href', color_class_popup);
									$('#widget_type').show();
									}
					});
					
				}
				else{
					$('#widget_type').hide();	
				}
		}
		
		function reload_color_class(widget_type_id,colorClassId){
			
				$.ajax({
						type   : 'POST',
						url : '<?php echo site_url('widget/reloadColorClasses/'); ?>'+widget_type_id+'/'+colorClassId,
						success : function(data){
							//alert(data);
									var json = $.parseJSON(data);
									$("#color_class_id").html(json.color_classes);
									$('#myModal').modal('hide');
									$('#widget_type').show();
								}
					});
		}
		
        </script>     
      <?php $this->load->view('super/assets/footer');?>
 
