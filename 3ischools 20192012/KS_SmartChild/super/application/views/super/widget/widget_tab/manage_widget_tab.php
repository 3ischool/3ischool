<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Widget Tab";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	 
        	<div class="btn-group pull-left left15">
                                        
                                    </div>
                                	 <div class="btn-group pull-right left10">
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('widget/add_widget_tab'); ?>">
                                        	Add
                                        </a>
                                        
                                    </div>                               
                                    <div class="btn-group pull-right left10">
                                         
                                    </div>
                                    
                                    <div class="btn-group pull-right">
                                        
                                    </div>
                                    
                                    
                                    
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                    
                        <?php
						   $msg = $this->session->userdata('msg');
						   if($msg != ''){
							$this->session->unset_userdata('msg');
						   ?> 
							<div class="alert alert-success alert-dismissable">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <?php echo $msg; ?>
							</div>  
						   <?php	   
							}
						   ?>          
                        <div class="row-fluid ">
                        
                        	<div class="span12 dashBoxes" style="overflow:hidden;">
                                	 
                                
                                    <div class="row-fluid">
                                	 
                                   <form id="formID" method="post" action="<?php echo site_url('widget/update_sorting/Wid_Tab/TabId'); ?>">
									<table class="table table-bordered" id="myTable sort" style="border-top:0px;">
									 <thead>	 
                                    	<tr>
											<th style="display:none;"></th>
                                        	<th>Tab Title</th> 
                                        	<th>Area</th> 
                                        	<th>Icon Image</th> 
                                        	<th>Icon Color</th> 
                                        	<th>Status</th> 
                                        	<th>Action</th>
                                        </tr>
                                       </thead>
                                       <tbody id="sortable"> 
                                        <?php foreach($widget_tab_rec as $rec){ ?>
                                        <tr class="ui-sortable">
                                        	<td style="display:none;" class="ui-sortable-handle"><input type="hidden" name="ids[]" value="<?php echo $rec['TabId']; ?>" /></td>
											<td class="ui-sortable-handle"><?php echo $rec['TabTitle']; ?></td>
                                        	<td class="ui-sortable-handle"><?php echo $rec['AreaName']; ?></td>
                                        	<td class="ui-sortable-handle"><?php if($rec['IconImage'] != ''){ ?><img width="50" height="50" src="<?php echo base_url('uploads/tab_icon/'.$rec['IconImage']) ?>"><?php } ?></td>
                                        	<td class="ui-sortable-handle"><?php echo $rec['IconColor']; ?></td>
                                        	<?php
                                        	 if($rec['IsActive'] == 1){
												$is_active = 'Active';
											 }
											 else{
												$is_active = 'Inactive';	
											 }
                                        	 ?>
                                        	<td class="ui-sortable-handle"><a href="<?php echo site_url('widget/update_status/'.base64e($rec['TabId']).'/'.$rec['IsActive'].'/Wid_Tab/TabId/manage_widget_tab'); ?>" ><?php echo $is_active; ?></a></td> 
                                        	<td class="ui-sortable-handle">
                                            <a href="<?php echo site_url('widget/edit_widget_tab/'.base64e($rec['TabId'])); ?>" class="btn btn-xs btn-default" title="Edit"> <i class="icon icon-pencil"></i></a>
											<?php
											$rData = $this->common_model->compare_in_table('Wid_TabWidget','TabId',$rec['TabId']);
											if(empty($rData)){
											?>
											<a href="<?php echo site_url('widget/delete_widget_tab/'.base64e($rec['TabId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											<?php } ?>
											</td>
                                        </tr>
                                       <?php } ?>
                                       </tbody> 
                                      </form> 
                                    </table>
                                    <input type="submit" id="submitID" style=" display:none" />
                                     
                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>
            	
                
            </div>
        </div>
        <!-- //Dash Content -->
        <script>
         /* $( function() {
			$( "#sortable" ).sortable(function(){
				
					alert(1);
			});
			$( "#sortable" ).disableSelection();
		  } );
		  
		  function updateSorting(){
			  
			alert(1);  
		  }
		  */
        </script>     
       <?php $this->load->view('super/assets/footer');?>
