<?php $this->load->view('super/assets/header');?>
  <?php
  if($this->uri->segment(2) == 'edit_widget_tab'){
	  $title = $edit_rec[0]['TabTitle'];
	  $area_id = $edit_rec[0]['AreaId'];
	  $icon_image = $edit_rec[0]['IconImage'];
	  $icon_color = $edit_rec[0]['IconColor'];
  }else{
	  $title = set_value('title');
	  $area_id = set_value('area_id');
	  $icon_image = set_value('icon_image');
	  $icon_color = set_value('icon_color');
	  }
  ?>
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Widget Tab";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                    
                   <a href="<?php echo site_url('widget/manage_widget_tab');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open_multipart(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
						
						  <div class="span6">
                              <label>Tab Title</label>
                               <input id="title" name="title" placeholder="" class="span12" type="text" value="<?php echo $title; ?>">
							   <?php  echo form_error('title','<span class="label label-important">','</span>');?>	
                          </div> 
                          
                          <div class="span6">
                              <label>Area</label>
                               <select id="area_id" name="area_id" class="span12" >
									<option value="">Select Area</option>
									<?php foreach($widget_area_rec as $rec){ ?>
										<option <?php if($rec['AreaId'] == $area_id){?>selected=selected<?php } ?> value="<?php echo $rec['AreaId']; ?>"><?php echo $rec['AreaName']; ?></option>
									<?php } ?>	
                               </select>
							   <?php  echo form_error('area_id','<span class="label label-important">','</span>');?>	
                          </div> 
                           	
                     </div> 	
					 <div class="row-fluid top10">		
						 
                          <div class="span6">
                              <label>Icon Image</label>
                               <input type="file" class="span12" name="userfile" id="userfile">
                               <?php if($icon_image != ''){ ?>
                               <img height="30" width="30" src="<?php echo base_url('uploads/tab_icon/'.$icon_image); ?>">
                               <?php } ?>
							   <?php  echo form_error('userfile','<span class="label label-important">','</span>');?>	
                          </div> 	
                           <div class="span6">
                              <label>Icon Color</label>
                               <input id="icon_color" name="icon_color" placeholder="" class="span12" type="text" value="<?php echo $icon_color; ?>">
							   <?php  echo form_error('icon_color','<span class="label label-important">','</span>');?>	
                          </div> 
                      </div>
                         
                      <div class="row-fluid top10">
                          <div class="span6">
                              <input type="submit" name="publish" class="btn" value="Save & Continue">                     
                              <input type="submit" name="publish2" class="btn" value="Save & Exit">                     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
