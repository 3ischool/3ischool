<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "User Role Type Navigation Place";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	 
        	<div class="btn-group pull-left left15">
                                        
                                    </div>
                                	 <div class="btn-group pull-right left10">
                                        <a style="margin-right:4px" class="btn dropdown-toggle" href="<?php echo site_url('navigation/add_user_role_type_nav_place'); ?>">
                                        	Add
                                        </a>
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('navigation/generate_permission_json'); ?>">
                                        	Generate Permission JSON
                                        </a>
                                        
                                    </div>                               
                                    
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                    
                                  
                        <div class="row-fluid ">
                   <?php
                   $msg = $this->session->userdata('json_perm');
                   if($msg != ''){
					$this->session->unset_userdata('json_perm');
                   ?> 
					<div class="alert alert-success alert-dismissable">
					  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					  <?php echo $msg; ?>
					</div>  
				   <?php	   
					}
                   ?>
                                    <div class="row-fluid">
                                	 
                                    <table id="myTable" class="table table-bordered" style="border-top:0px;">
                                      <thead>	
                                    	<tr>
                                        	<th>Navigation Place</th>
                                        	<th>User Role Type</th>
                                        	<th>JSON Permission</th>
                                        	<th>Action</th>
                                        </tr>
                                       </thead>
                                       <tbody>     
                                        <?php foreach($user_role_type_nav_place_rec as $rec){ ?>
                                        <tr>
											<td><?php echo $rec['NavigationPlace']; ?></td>
											<td><?php echo $rec['UserRoleType']; ?></td>
											<?php
											if($rec['AddInPermissionJson'] == 1){
												$permission = 'Yes';	
											}else{
												$permission = 'No';
												}
											?>
											<td><?php echo $permission; ?></td>
											 
											<td>
                                            <a href="<?php echo site_url('navigation/edit_user_role_type_nav_place/'.base64e($rec['UserRoleTypeNavigationPlaceId'])); ?>" class="btn btn-xs btn-default" title="Edit"> <i class="icon icon-pencil"></i></a>
											<?php 
											$nav_placement = $this->common_model->compare_in_table('NavigationPlacement','UserRoleTypeNavigationPlaceId',$rec['UserRoleTypeNavigationPlaceId']);
											if(empty($nav_placement)){
											?> 
											<a href="<?php echo site_url('navigation/delete_user_role_type_nav_place/'.base64e($rec['UserRoleTypeNavigationPlaceId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											<?php } ?> 
											</td>
                                        </tr>
                                       <?php } ?>
                                       </tbody> 
                                    </table>
                                    
                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>
            	
                
            </div>
        </div>
        <!-- //Dash Content -->
             
       <?php $this->load->view('super/assets/footer');?>
