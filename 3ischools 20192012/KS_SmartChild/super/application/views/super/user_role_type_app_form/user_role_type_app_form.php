<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "User Role Type App Form";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData); 
	 
		// $sequence_id = set_value('sequence');
		 $app_form_id = set_value('app_form');
		 $url = site_url('appFormSetup/manage_user_role_type_app_form/'.$user_role_type_id);
		 //~ if($user_role_type_id != ''){ 
			 //~ $user_role_type_rec = $this->common_model->model_get_by_col('UserRoleType','UserRoleTypeId',base64d($user_role_type_id)); 
		 //~ }
?>
   <style>
.ui-sortable-handle{cursor: grabbing;}
</style>
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	  
			<div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent" style="min-height:500px;">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
					   
                    <!-- form start -->
					<form action="<?php echo $url; ?>" method="post">
						<div class="row-fluid">
							
							<div class="span3">
								<label>User Role Type</label>	
                                <select name="user_role_type" id="user_role_type" onchange="loadTable(this.value);" class="span12">
									<option value="">Select User Role Type</option>
									<?php foreach($user_role_type as $user_role_type){ ?>
									<option <?php if($user_role_type_id == base64e($user_role_type['UserRoleTypeId'])){ ?>selected=selected<?php } ?> value="<?php echo base64e($user_role_type['UserRoleTypeId']); ?>"><?php echo $user_role_type['UserRoleType']; ?></option>
									<?php } ?>
                                </select>
                              <?php  echo form_error('sequence','<span class="label label-important">','</span>');?>
							</div> 
							
							<div class="span2">
								<label>App Form</label>	
                                <select name="app_form[]" id="app_form" class="span12 store_multiselect" multiple>
									<?php foreach($app_form as $app_form){ ?>
									<option <?php if($app_form_id == $app_form['AppFormId']){ ?>selected=selected<?php } ?> value="<?php echo base64e($app_form['AppFormId']); ?>"><?php echo $app_form['AppForm']; ?></option>
									<?php } ?>
                                </select>
                              <?php  echo form_error('app_form[]','<span class="label label-important">','</span>');?>
							</div> 
						 
							<div class="span2">
								<label>&nbsp;</label>
								<input type="submit" class="btn btn-info" value="Submit">
							</div>
							<div class="span2">
								<label>&nbsp;</label>
								<a onclick="generateJSON();" class="btn btn-success">Generate JSON</a>
							</div>
<!--
	href="<?php echo site_url('appFormSetup/generate_user_role_type_app_form_json'); ?>" 						
-->
						</div>	
						
						<div class="clearfix"></div>
                       </form> 
                     <!-- form end --> 
                   	
                       <div class="row-fluid" id="allDataDiv">
							<div class="span12 dashBoxes" style="overflow:hidden;" id="allDataTable">
<!--
                                	<div class="dashBoxTitle">
                                	<span class="title"> <?php if($user_role_type_id != ''){ echo $user_role_type_rec[0]['UserRoleType']; ?> - Role Type <?php } ?></span>
                                	 <span class="pull-right"></span>                         
                                </div>
-->
								<div class="row-fluid" >
									<form id="formID" method="post" action="<?php echo site_url('appFormSetup/update_sorting/UserRoleTypeAppForm/UserRoleTypeAppFormId'); ?>">
									  <table class="table table-bordered table-condensed sort" id="myTable" style="border-top:0px;">
										<thead>
                                    	<tr>
											<th style="display:none;"></th>
                                        	<th>App Form</th>
                                        	<th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody id="sortable"> 
                                        <?php foreach($user_role_type_app_form_detail as $detail){ ?>
                                        <tr>
											<td style="display:none;" class="ui-sortable-handle"><input type="hidden" name="ids[]" value="<?php echo $detail['UserRoleTypeAppFormId']; ?>" /></td>
											<td class="ui-sortable-handle"><?php echo $detail['AppForm']; ?></td>
                                        	<?php
                                        	if($detail['IsActive'] == 1){
													$is_active = "Active";
												}else{
													$is_active = "Inactive";
												} 
                                        	?>
                                        	<td class="ui-sortable-handle"><a href="javascript:void(0)" onClick="updateUserTypeAppFormStatus('<?php echo base64e($detail['UserRoleTypeAppFormId']); ?>','<?php echo $detail['IsActive']; ?>')" ><?php echo $is_active; ?></a></td>
                                        	</td>
                                        	<td class="ui-sortable-handle">
												<a href="<?php echo site_url('appFormSetup/delete_user_role_type_app_form/'.$user_role_type_id.'/'.base64e($detail['UserRoleTypeAppFormId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											 
                                           </td>
											
                                        </tr>
                                       <?php }?>
                                       </tbody>  
                                    </table>
                                    <input type="submit" id="submitID" style=" display:none" />
                                     
                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>
            	
                
            </div>
        </div>
        <!-- //Dash Content -->
         <script>
			
		   function loadTable(){
			    
				var userRoleTypeId = $("#user_role_type option:selected").val();
				var redirectUrl	= "<?php echo site_url('appFormSetup/manage_user_role_type_app_form');?>/"+userRoleTypeId;				 
				window.location.href = redirectUrl; 
		   }	
			 
		   function delete_sequentialDetail(sequentialDetailId){
				var result = confirm("Are you sure want to delete this?");
				if(result){
					$('#ajax_loader').show();
					$.ajax({
						type   : 'POST',
						url : '<?php echo site_url('appFormSetup/delete_sequence_detail/')?>'+sequentialDetailId,  
						success:function(data) {
							var result = $.parseJSON(data);
							if(result['success'] == true){
								var redirectUrl	= "<?php echo current_url();?>";
								$( "#allDataDiv" ).load( redirectUrl+" #allDataTable", function() {
									
								});
								
							}
						}			
					});
					
					$('#ajax_loader').hide();
					$.get("<?php echo site_url('common/get_session');?>", function( session ){ eval(session); });
					
				}else{
					
					return false;
				}
			}
			
	 
			
		function reloadStore(storeId){
			
			$('#ajax_loader').show();
			$.ajax({
					type   : 'POST',
					url : '<?php echo site_url('ppc/reload_store/')?>'+storeId,  
					success:function(data1) {
						//alert(data1);
						var result = $.parseJSON(data1);
						 //$('.btn-group').addClass('open');
						 $('button .multiselect-selected-text').attr('title',result['title']);
						 $('button .multiselect-selected-text').text(result['title']);
						 $('.multiselect-container').append(result['data']);
						 $('.store_multiselect').append(result['select']);
						 //$('select[multiple]').multiselect('reload');
						 var brands = $('#multiselect1 option:selected');
						var selection = [];
						selection.push(brand);
						
					}			
			});
			$('#ajax_loader').hide();
			$('#myModal').modal('hide');	
		}	
		
		 
		function updateShortingIndex(event,id){
				
				var x = event.which || event.keyCode; 
				if(x == 13){
					var sequentialDetailId = id;
					var shortingIndex = $.trim($('#sd'+sequentialDetailId).val());
					if(shortingIndex != ''){
						$.ajax({
								type   : 'POST',
								url : '<?php echo site_url('ppc/update_sorting_index/')?>'+sequentialDetailId+'/'+shortingIndex,  
								success:function(data) {
									  if(data == 1){
										var redirectUrl	= "<?php echo current_url();?>";
										$( "#allDataDiv" ).load( redirectUrl+" #allDataTable", function() {
										}); 
										alert('Sorting Index has been updated successfully');	
									  }
									  else{
										alert(data);  
									  }
								}			
						});
					}
					else{
						alert('Sorting Index required');	
					}
				}
		}
		
		function updateUserTypeAppFormStatus(userRoleTypeAppFormId,status){
			
				$('#ajax_loader').show();
				$.ajax({
					type   : 'POST',
					url : '<?php echo site_url('appFormSetup/update_user_role_type_app_form_status/')?>'+userRoleTypeAppFormId+'/'+status,  
					success:function(data) {
						var result = $.parseJSON(data);
						if(result['success'] == true){
							var redirectUrl	= "<?php echo current_url();?>";
							$( "#allDataDiv" ).load( redirectUrl+" #allDataTable", function() {
								$('#ajax_loader').hide();
							});
						}
					}			
				});
				
		    }	
		
		$('.store_multiselect').multiselect({
			 buttonWidth : "100%",
			 maxHeight : 200,
			 nonSelectedText : "Select App Form"
		});
		
		function generateJSON(){
				 
				var urll = '<?php echo site_url('appFormSetup/generate_user_role_type_app_form_json'); ?>';
				$.post( urll, function(data){
						alert(data);
					
				});
		}
		 
       </script>      
       <?php $this->load->view('super/assets/footer');?>
