<?php $this->load->view('super/assets/header');?>
       
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Generate Masters Json";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);
    	//$UURRL = str_replace('super/','',base_url());
    	?>
    	 
        
        <div class="clearfix"></div>
        
        
      
        <!-- Dash Content -->
        <div class="dashContent">
			 
        	<div class="container-fluid">
				<div class="row-fluid">
					<div class="span3">
						<a class="btn btn-sx btn-primary" href="<?php echo site_url('master_object/generate_masters_json'); ?>">Generate JSON</a>
					</div>
					 
				</div>
				 
            </div>
        </div>
        <!-- //Dash Content -->
        <?php
				$sessionData = $this->session->userdata('json_created');
				if(trim($sessionData) != ''){
				?>
					<script>
						alert('<?php echo  $sessionData; ?>');
					</script>
				<?php		
				}
        ?>     
      <?php $this->load->view('super/assets/footer');?>
 
