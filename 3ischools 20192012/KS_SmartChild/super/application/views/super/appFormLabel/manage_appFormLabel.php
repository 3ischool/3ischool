<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "App Form Label";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	 
                                	 <div class="btn-group pull-right left10">
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('appFormSetup/add_appFormLabel'); ?>">
                                        	Add
                                        </a>
                                        
                                    </div>                               
                                     
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                    
                                  
                        <div class="row-fluid ">
                         
                                    <div class="row-fluid">
                                	<!-- Filter -->		
                                	 <div>
									 <!-- Language -->
                                	 <select id="language" onchange="filterByLang();">
									  <option value="">Select Language</option>	 
                                	  <?php foreach($language as $language){ ?>	
										 <option <?php if($language_id == $language['LanguageId']){ ?>selected=selected<?php } ?> value="<?php echo base64e($language['LanguageId']); ?>"><?php echo $language['Language']; ?></option>
									 <?php } ?>	 
                                	 </select>
                                	 <?php if($language_id != ''){ ?>
											<a onclick="generateJSON('<?php echo base64e($language_id); ?>');" class="btn btn-xs btn-success">Generate JSON</a>
									 <?php } ?>	 
                                	 </div>
                                	 <!-- /Filter --> 
                                    <table id="myTable" class="table table-bordered" style="border-top:0px;">
                                      <thead>	
                                    	<tr>
                                        	<th>Form Field Label</th>
                                        	<th>App Form Field</th>
                                        	<th>Language</th>
                                        	<th>Action</th>
                                        </tr>
                                       </thead>
                                       <tbody>     
                                        <?php foreach($appFormLabel as $appFormLabel){ ?>
                                        <tr>
                                        	<td><input id="title<?php echo $appFormLabel['AppFormLabelId'];?>" onblur="updateLabel(this.value,'<?php echo $appFormLabel['AppFormLabelId']; ?>')" value="<?php echo $appFormLabel['FormFieldLabel']; ?>" data-url="<?php echo site_url('appFormSetup/update_app_form_label/'.base64e($appFormLabel['AppFormLabelId'])); ?>">
                                        	<i class="icon-check" id="afrm<?php echo $appFormLabel['AppFormLabelId'];?>" style="color:#4CAF50; display:none; font-size:17px;"></i>
                                        	</td>
                                        	<td><?php echo $appFormLabel['FieldName'];?></td>
											<td><?php echo $appFormLabel['Language'];?></td>
											<td>
                                            <a href="<?php echo site_url('appFormSetup/edit_appFormLabel/'.base64e($appFormLabel['AppFormLabelId'])); ?>" class="btn btn-xs btn-default" title="Edit"> <i class="icon icon-pencil"></i></a>
											<a href="<?php echo site_url('appFormSetup/delete_appFormLabel/'.base64e($appFormLabel['AppFormLabelId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											</td>
                                        </tr>
                                       <?php } ?>
                                       </tbody> 
                                    </table>
                                    
                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>
            	
                
            </div>
        </div>
        <!-- //Dash Content -->
        <script>
			
        function filterByLang(){
				
				var language_id = $('#language option:selected').val();
				var urll = '<?php echo site_url('appFormSetup/set_appform_label_filters'); ?>/'+language_id; 
				window.location.href = urll;
		}
		
		function generateJSON(lang_id){
				
				var obj = $(this);
				var urll = '<?php echo site_url('appFormSetup/generate_app_form_label_json'); ?>/'+lang_id;
				$.post( urll, function(data){
						alert(data);
					
				});
		}
		
		function updateLabel(title,id){
				 
				 var successID = '#afrm'+id;
				 $.post( $('#title'+id).attr('data-url'), {titleval:title},function(data){
					if(Math.floor(data) == data && $.isNumeric(data)){
						$(successID).show(800).delay(3000).hide(800);
					}else{
						alert(data);
						}
				});
			}
        </script>          
       <?php $this->load->view('super/assets/footer');?>
