<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "App Form Action Type";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	  
                                	 <div class="btn-group pull-right left10">
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('appFormSetup/add_app_form_action_type'); ?>">
                                        	Add
                                        </a>
                                        
                                    </div>                               
                                      
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                    
                                  
                        <div class="row-fluid ">
                        
                        	<div class="span12 dashBoxes" style="overflow:hidden;">
                                	<div class="dashBoxTitle">
                                	<span class="title">Manage </span>
                                     <span class="pull-right">Total records : <?php echo $total_rows;?> &nbsp;&nbsp;&nbsp;</span>                         
                                                         
                                </div>
                                
                                    <div class="row-fluid">
                                	 
                                    <table class="table table-bordered" id="myTable1" style="border-top:0px;">
                                    	<thead> 
                                    	<tr>
                                        	<th>Form Action Type</th>
                                        	<th>Action</th>
                                            </tr>
                                        </thead>    
                                        <tbody>
                                        <?php foreach($result as $res){ ?>
                                        <tr>
                                        	<td><?php echo $res['FormActionType']; ?></td>
                                           <td>
                                            <a href="<?php echo site_url('appFormSetup/edit_app_form_action_type/'.base64e($res['FormActionTypeId'])); ?>" class="btn btn-xs btn-default" title="Edit"> <i class="icon icon-pencil"></i></a>
											<?php 
											$record = $this->common_model->compare_in_table('AppFormAction','FormActionTypeId',$res['FormActionTypeId']); 
											if(empty($record)){
											?>
											<a href="<?php echo site_url('appFormSetup/delete_app_form_action_type/'.base64e($res['FormActionTypeId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											 <?php } ?>
											</td>
                                        </tr>
                                       <?php } ?>
                                       </tbody> 
                                    </table>
                                    <div class="boxGraphFooter" style="border-radius:0; margin:0; padding:0 15px;">
                                		 
                                            		<?php
													  echo $links;
													  ?>
                                             
                                	</div>   
                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>
            	
                
            </div>
        </div>
        <!-- //Dash Content -->
             
       <?php $this->load->view('super/assets/footer');?>
