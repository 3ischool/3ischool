<?php $this->load->view('super/assets/header');?>
 <?php
 $function_name = $this->uri->segment(2);
 if($function_name == 'add_app_form_action_type'){
	 $app_form_action_type = set_value('app_form_action_type');
	 $button_val = 'Add';
	} 
else{
	 $app_form_action_type = $result[0]['FormActionType'];
	 $button_val = 'Update';
	} 	
 ?>       
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "App Form Action Type";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                   
                   <a href="<?php echo site_url('appFormSetup/manage_app_form_action_type');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
						
						  <div class="span6">
                              <label>App Form Action Type</label>
                               <input  id="app_form_action_type" name="app_form_action_type" placeholder="" class="span12" type="text" value="<?php echo $app_form_action_type; ?>">
							   <?php  echo form_error('app_form_action_type','<span class="label label-important">','</span>');?>	
                          </div> 	
							
                         
                        </div>
                      
                        <div class="row-fluid top10">
                          <div class="span6">
                              <input type="submit" class="btn" value="<?php echo $button_val; ?>">                     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
