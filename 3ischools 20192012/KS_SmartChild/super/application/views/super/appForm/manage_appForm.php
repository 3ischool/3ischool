<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "App Form";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	 
        	<div class="btn-group pull-left left15">
                                         
                                    </div>
                                	 <div class="btn-group pull-right left10">
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('appFormSetup/add_appForm'); ?>">
                                        	Add
                                        </a>
                                        
                                    </div>                               
                                     
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                    
                                  
                        <div class="row-fluid ">
                        
                        	 
                                    <div class="row-fluid">
                                	 
 
                                    <table id="myTable" class="table table-bordered" style="border-top:0px;">
 
                                      <thead>	
                                    	<tr>
                                        	<th>App Form</th>
                                        	<th>Module Type</th>
                                        	<th>Navigation Item</th>
                                        	<th>Action</th>
                                            </tr>
                                      </thead>
                                      <tbody>      
                                      <?php foreach($appForm as $appForm){ 
												// Parent
												$parent['AppForm'] = $appForm['AppForm'];
												$parent['Module'] = $appForm['Module'];
												$parent['NavigationItem'] = $appForm['NavigationItem'];
												$parent['AppFormId'] = $appForm['AppFormId'];
												$parent['IsChild'] = 0;
												$this->load->view('super/appForm/appForm_row',$parent); 
												// Child
												$childData = $this->appForm_model->get_all_appForm_by_id($appForm['AppFormId']);
												foreach($childData as $childData1){ 
													$child['AppForm'] = $childData1['AppForm'];
													$child['Module'] = $childData1['Module'];
													$child['NavigationItem'] = $childData1['NavigationItem'];
													$child['AppFormId'] = $childData1['AppFormId'];
													$child['IsChild'] = 1;
													$this->load->view('super/appForm/appForm_row',$child); 
												
													// thirdLevelChild
													$data = $this->appForm_model->get_all_appForm_by_id($childData1['AppFormId']);
													foreach($data as $data2){ 
														$thirdLevelChild['AppForm'] = $data2['AppForm'];
														$thirdLevelChild['Module'] = $data2['Module'];
														$thirdLevelChild['NavigationItem'] = $data2['NavigationItem'];
														$thirdLevelChild['AppFormId'] = $data2['AppFormId'];
														$thirdLevelChild['IsChild'] = 2;
														$this->load->view('super/appForm/appForm_row',$thirdLevelChild); 
													
													 }
												}
											  } ?>
                                       </tbody> 
                                    </table>

                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>
            	
                
            </div>
        </div>
        <!-- //Dash Content -->
         
       <?php $this->load->view('super/assets/footer');?>
