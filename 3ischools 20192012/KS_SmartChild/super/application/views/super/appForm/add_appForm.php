<?php $this->load->view('super/assets/header');?>
 <?php 
 $function_name = $this->uri->segment(2);
 if($function_name == 'add_appForm'){
	 $appForm = set_value('appForm');
	 $moduleId = set_value('moduleId');
	 $navigation_item_id = set_value('navigation_item');
	 $app_form_id = set_value('app_form_parent');
	 $button_val = 'Add';
	} 
 else{
	 $appForm = $result[0]['AppForm'];
	 $moduleId = $result[0]['ModuleId'];
	 $navigation_item_id = $result[0]['NavigationItemId'];
	 $app_form_id = $result[0]['AppFormParentId'];
	 $button_val = 'Update';
	} 	
 ?>       
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "App Form";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                   
                  <div class="nav-collapse collapse">
                    <ul class="nav">
                       
                      </ul>                    
                  </div><!--/.nav-collapse -->
                   <a href="<?php echo site_url('appFormSetup/manage_appForm');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
						
						  <div class="span6">
                              <label>App Form</label>
                               <input id="appForm" name="appForm" placeholder="" class="span12" type="text" value="<?php echo $appForm; ?>">
							   <?php  echo form_error('appForm','<span class="label label-important">','</span>');?>	
                          </div> 	
						  
						  <div class="span6">
                              <label>Module</label>   
                              <select class="span12 SmartSearch" name="moduleId">
                              <option value="">Select</option>
                              <?php 
                              foreach($module as $module){
                              ?>
                              <option <?php if($moduleId == $module['ModuleId']){ ?>selected=selected <?php } ?> value="<?php echo $module['ModuleId']; ?>"><?php echo $module['Module']; ?></option>
                              <?php } ?>
                              </select>    
                              <?php  echo form_error('moduleId','<span class="label label-important">','</span>');?>                  
                          </div>  	
                         
                        </div>
                        <div class="row-fluid top10">
						 
						  <div class="span6">
                              <label>Navigation Item</label>   
                              <select class="span12 SmartSearch" name="navigation_item">
                              <option value="">Select Navigation Item</option>
                              <?php 
                              foreach($navigation_item as $navigation_item1){
                              ?>
                              <option <?php if($navigation_item_id == $navigation_item1['NavigationItemId']){ ?>selected=selected <?php } ?> value="<?php echo $navigation_item1['NavigationItemId']; ?>"><?php echo $navigation_item1['NavigationItem'].'('.$navigation_item1['ControllerName'].'/'.$navigation_item1['ActionName'].')'; ?></option>
                              <?php } ?>
                              </select>    
                              <?php  echo form_error('navigation_item','<span class="label label-important">','</span>');?>                  
                          </div>  	
                          
						  <div class="span6">
                              <label>App Form Parent</label>   
                              <select class="span12 SmartSearch" name="app_form_parent">
                              <option value="0">Parent</option>
                              <?php 
                              foreach($app_form_rec as $app_form_rec1){
                              ?>
                              <option <?php if($app_form_id == $app_form_rec1['AppFormId']){ ?>selected=selected <?php } ?> value="<?php echo $app_form_rec1['AppFormId']; ?>"><?php echo $app_form_rec1['AppForm']; ?></option>
                              <?php } ?>
                              </select>    
                              <?php  echo form_error('app_form_parent','<span class="label label-important">','</span>');?>                  
                          </div>  	
                         
                        </div>
                       
                      
                        <div class="row-fluid top10">
                         <div class="span6">
                              <input type="submit" class="btn" value="<?php echo $button_val;?>">                     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
