<?php $this->load->view('super/assets/header');?>
 <?php 
 $function_name = $this->uri->segment(2);
 if($function_name == 'add_app_form_action'){
	 $form_id = set_value('form_id');
	 $formActionType = set_value('form_action_type');
	 $action_name = set_value('action_name');
	 $action_info = set_value('action_info');
	 $button_val = 'Add';
	} 
 else{
	 $form_id = $result[0]['AppFormId'];
	 $formActionType = $result[0]['FormActionTypeId'];
	 $action_name = $result[0]['ActionName'];
	 $action_info = $result[0]['ActionInfo'];
	 $button_val = 'Update';
	} 	
	
 ?>       
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "App Form Action";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                 
                   <a href="<?php echo site_url('appFormSetup/manage_app_form_action');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					 <!--<div class="span12 dashBoxes">-->
                  <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">  
							
							<div class="span6">
                              <label>Form</label>  
                              <select class="span12" name="form_id">
                              <option value="">Select</option>
                              <?php  
                              foreach($app_form as $form){
                              ?>
                              <option <?php if($form_id == $form['AppFormId']){ ?>selected=selected <?php } ?> value="<?php echo $form['AppFormId']; ?>"><?php echo $form['AppForm']; ?></option>
                              <?php } ?>
                              </select>    
                              <?php  echo form_error('form_id','<span class="label label-important">','</span>');?>                  
                          </div>  
                          <div class="span6">
                              <label>Form Action Type</label>  
                              <select class="span12" name="form_action_type">
                              <option value="">Select</option>
                              <?php  
                              foreach($app_form_action_type as $form_action_type){
                              ?>
                              <option <?php if($formActionType == $form_action_type['FormActionTypeId']){ ?>selected=selected <?php } ?> value="<?php echo $form_action_type['FormActionTypeId']; ?>"><?php echo $form_action_type['FormActionType']; ?></option>
                              <?php } ?>
                              </select>    
                              <?php  echo form_error('form_action_type','<span class="label label-important">','</span>');?>                  
                          </div>  		
                         
                       </div>
						              
                        <div class="row-fluid top10">
						
						   <div class="span6">
                              <label>Action Name</label>
                              <input name="action_name" class="span12" type="text" value="<?php echo $action_name;?>" >
						      <?php  echo form_error('action_name','<span class="label label-important">','</span>');?>	
                          </div> 
                        </div> 	
						<div class="row-fluid top10">
						 <label>Action Info</label>
                              <textarea name="action_info" class="span12 textarea" style="width : 100%"><?php echo $action_info;?></textarea>
						      <?php  echo form_error('action_info','<span class="label label-important">','</span>');?>	
                         
                        </div>
                      
                        <div class="row-fluid top10">
                         
                          <div class="span6">
                              <input type="submit" class="btn" value="<?php echo $button_val; ?>">                     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
