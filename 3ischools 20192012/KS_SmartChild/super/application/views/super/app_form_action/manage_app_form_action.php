<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "App Form Action";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
      <?php 
 if(empty($result)){
	 $form_id = set_value('form_id');
	 $formActionType = set_value('form_action_type');
	 $action_name = set_value('action_name');
	 $action_info = set_value('action_info');
	 $button_val = 'Save';
	} 
 else{
	 $form_id = $result[0]['AppFormId'];
	 $formActionType = $result[0]['FormActionTypeId'];
	 $action_name = $result[0]['ActionName'];
	 $action_info = $result[0]['ActionInfo'];
	 $button_val = 'Update';
	} 	
	
 ?>         
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	  
<!--
                                	 <div class="btn-group pull-right left10">
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('appFormSetup/add_app_form_action'); ?>">
                                        	Add
                                        </a>
                                        
                                    </div>                               
-->
                                    
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
			   <div class="row-fluid ">	
				<div class="span12 well">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">  
							
							<div class="span4">
                              <label>Form</label>  
                              <select class="span12 SmartSearch" name="form_id" id="form_id" onchange="filterGridByForm11();">
                              <option value="">Select</option>
                              <?php  
                              foreach($app_form as $form){
                              ?>
                              <option <?php if($form_id == $form['AppFormId']){ ?>selected=selected <?php } ?> value="<?php echo $form['AppFormId']; ?>"><?php echo $form['AppForm']; ?></option>
                              <?php } ?>
                              </select>    
                              <?php  echo form_error('form_id','<span class="label label-important">','</span>');?>                  
                          </div>  
                          <div class="span4">
                              <label>Form Action Type</label>  
                              <select class="span12 SmartSearch" name="form_action_type">
                              <option value="">Select</option>
                              <?php  
                              foreach($app_form_action_type as $form_action_type){
                              ?>
                              <option <?php if($formActionType == $form_action_type['FormActionTypeId']){ ?>selected=selected <?php } ?> value="<?php echo $form_action_type['FormActionTypeId']; ?>"><?php echo $form_action_type['FormActionType']; ?></option>
                              <?php } ?>
                              </select>    
                              <?php  echo form_error('form_action_type','<span class="label label-important">','</span>');?>                  
                          </div>  
                          <div class="span4">
                              <label>Action Name</label>
                              <input name="action_name" class="span12" type="text" value="<?php echo $action_name;?>" >
						      <?php  echo form_error('action_name','<span class="label label-important">','</span>');?>	
                          </div> 		
                         
                       </div>
						 
						<div class="row-fluid top10">
						  <div class="span4">	
							 <label>Action Info</label>
								  <textarea name="action_info" class="span12 textarea" style="width : 100%"><?php echo $action_info;?></textarea>
								  <?php  echo form_error('action_info','<span class="label label-important">','</span>');?>	
							 
                          </div>
						  <div class="span4">
							  <label>&nbsp;</label>
                              <input type="submit" class="btn" value="<?php echo $button_val; ?>">    
                             <?php if(!empty($result)){ ?>
                              <a class="btn btn-danger" href="<?php echo site_url('appFormSetup/manage_app_form_action'); ?>">Cancel</a>   
                              <?php } ?>              
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div> 
                   </div>  
            	<div class="row-fluid">
					
                	<div class="span12">
                            
                        <div class="row-fluid ">
                         
                                    <div class="row-fluid">
                                	 
                                    <table class="table table-bordered" id="myTable" style="border-top:0px;">
                                    	<thead>
                                    	<tr>
                                        	<th>Form</th>
                                        	<th>Action Type</th>
                                        	<th>Action Name</th>
                                        	<th>Action Info</th>
                                        	<th>Action</th>
                                            </tr>
                                        </thead>    
                                        <tbody>
                                        <?php foreach($app_form_action as $form_action){ ?>
                                        <tr>
                                        	
                                        	<td><?php echo $form_action['AppForm']; ?></td>
											<td><?php echo $form_action['FormActionType']; ?></td>
											<td><?php echo $form_action['ActionName'];  ?></td>	
											<td><?php echo substr($form_action['ActionInfo'],0,25);if(strlen($form_action['ActionInfo']) > 25){echo '...';};  ?></td>
                                           <td>
                                            <a href="<?php echo site_url('appFormSetup/manage_app_form_action/'.base64e($form_action['AppFormActionId'])); ?>" class="btn btn-xs btn-default" title="Edit"> <i class="icon icon-pencil"></i></a>
											<a href="<?php echo site_url('appFormSetup/delete_app_form_action/'.base64e($form_action['AppFormActionId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											</td>
                                        </tr>
                                       <?php } ?>
                                      </tbody>  
                                    </table>
                                  
                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>
            	
                
            </div>
        </div>
        <!-- //Dash Content -->
       <script>
       function filterGridByForm(){
		   
			var formId = $('#form_id :selected').val();
			var urll = '<?php echo site_url('appFormSetup/manage_app_form_action'); ?>';
			window.location.href = urll;
	   }
       </script>      
       <?php $this->load->view('super/assets/footer');?>
