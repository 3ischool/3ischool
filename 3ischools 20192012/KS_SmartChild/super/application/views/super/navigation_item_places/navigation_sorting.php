<?php $parent_nav = $this->navigation_item_model->get_navigation_placement_by_place_parent($role_type_nav_place_id,0);
		$json_data = array();
		$ii = 0;
		
			
		foreach($parent_nav as $parent_nav){
			
				$controller = '';
				$action = '';
				$query = '';
				if($parent_nav['ControllerName'] != ''){
					$controller = $parent_nav['ControllerName'];
				}
				if($parent_nav['ActionName'] != ''){
					$action = '/'.$parent_nav['ActionName'];
				}
				
				$url = $controller.$action;
				if($url != ''){
					$url = '-'.$url;	
				}
				$json_data[$ii]['name'] = $parent_nav['name'].$url;
				$json_data[$ii]['id'] = $parent_nav['id'];
				$childArray = array();
				$childNav = $this->navigation_item_model->get_navigation_placement_by_place_parent($role_type_nav_place_id,$parent_nav['id']);
				$yy = 0;
				foreach($childNav as $childNav){
					$controller = '';
					$action = '';
					if($childNav['ControllerName'] != ''){
						$controller = $childNav['ControllerName'];
					}
					if($childNav['ActionName'] != ''){
						$action = '/'.$childNav['ActionName'];
					}
					 
					$url = $controller.$action;
					if($url != ''){
						$url = '-'.$url;	
					}
					$childArray[$yy]['name'] = $childNav['name'].$url;
					$childArray[$yy]['id'] = $childNav['id'];
					$SubChildArray = array();
					$SubChildNav = $this->navigation_item_model->get_navigation_placement_by_place_parent($role_type_nav_place_id,$childNav['id']);
					$zz = 0;
					foreach($SubChildNav as $SubChildNav){
						$SubChildArray[$zz]['name'] = $SubChildNav['name'];
						$SubChildArray[$zz]['id'] = $SubChildNav['id'];
						$zz++;
					}
					$childArray[$yy]['children'] = $SubChildArray;
					$yy++;
				}
				$json_data[$ii]['children'] = $childArray;
				$ii++;
		}
		 
		 
		$jData = json_encode($json_data); ?>

<script src="<?php echo base_url();?>assets/jqtree/tree.jquery.js"></script>
					<link rel="stylesheet" href="<?php echo base_url();?>assets/jqtree/jqtree.css">
                  
							<a href="javascript:post_data()" class="btn btn-success pull-right">Save Sorting</a>
							<div id="tree1"></div>

           <script>
var data = <?php echo $jData;?>;
$('#tree1').tree({data: data, dragAndDrop: true,selectable: false,saveState: true});
function post_data(){
	$('#ajax_loader').show();
	var gDtata = $('#tree1').tree('getTree');
	var dataArray = gDtata.children;
	var dataArrayLen = dataArray.length;
	var PostData = '';
	 for($x=0; $x<dataArrayLen; $x++){
		 var sortIndexParent = $x+1;
		 var childDataArray = dataArray[$x].children;
		 var parentName = dataArray[$x].name;
		 var parentId = dataArray[$x].id;
		 var childDataArrayLen = childDataArray.length;
		 PostData = PostData+parentId+'-0-'+sortIndexParent+'|';
		 for($y=0; $y<childDataArrayLen; $y++){
			  var sortIndexChild = $y+1;
			 var ChildName  = childDataArray[$y].name;
			 var ChildId  = childDataArray[$y].id;
			 var SubChildDataArray = childDataArray[$y].children;
			 var SubChildDataArrayLen = SubChildDataArray.length;
			 PostData = PostData+ChildId+'-'+parentId+'-'+sortIndexChild+'|'; 
			 
			 for($z=0; $z<SubChildDataArrayLen; $z++){
				var sortIndexSubChild = $z+1;
				var SubChildName  = SubChildDataArray[$z].name;
				var SubChildId  = SubChildDataArray[$z].id;
				PostData = PostData+SubChildId+'-'+ChildId+'-'+sortIndexSubChild+'|'; 
			} 
		 } 
		 
	 }
	 $.post( "<?php echo site_url('navigation/update_nav_sorting');?>", { PostData: PostData}, function( data ) {
		  debugger; 
		  var r = parseInt(data);
		 
		  if(r==1){
			window.location.href = window.location.href;
		  }else{
			alert('OOOPS! Unable to perform the task.');  
			}
	 });
	}
</script>

 
