<?php $this->load->view('super/assets/header');?>
  <?php 
  $user_role_type_nav_place_uri_id = '';
  if($this->uri->segment(3) != ''){
	$user_role_type_nav_place_uri_id = $this->uri->segment(3); 
  }
   
  ?>
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "Navigation Item Places";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                   
                  <div class="nav-collapse collapse">
                    <ul class="nav">
                         
                      </ul>                    
                  </div><!--/.nav-collapse -->
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
						<div class="row-fluid">
					  <?php
					   $msg = $this->session->userdata('json');
					   if($msg != ''){
						$this->session->unset_userdata('json');
					   ?> 
						<div class="alert alert-success alert-dismissable">
						  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						  <?php echo $msg; ?>
						</div>  
					   <?php	   
						}
					   ?>
					<form method="post" action="<?php echo site_url('navigation/navigation_placement/'.$user_role_type_nav_place_uri_id); ?>">  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
						
						 <div class="span2">
								  <label>User Role Type Nav Place</label>   
								  <select name="user_role_type_nav_place" id="user_role_type_nav_place" class="span12" onchange="get_navigation_items()">
								  <option value="">Select Role Type</option>
								  <?php foreach($user_role_type_nav_place_rec as $user_role_type_nav_place){ ?> 
								  <option <?php if($role_type_nav_place_id == $user_role_type_nav_place['UserRoleTypeNavigationPlaceId']){ ?>selected=selected<?php } ?> value="<?php echo base64e($user_role_type_nav_place['UserRoleTypeNavigationPlaceId']); ?>"><?php echo $user_role_type_nav_place['UserRoleType'].'/'.$user_role_type_nav_place['NavigationPlace']; ?></option>
								  <?php } ?>
								  </select>    
								  <?php  echo form_error('user_role_type_nav_place','<span class="label label-important">','</span>');?>  
								             
						   </div>  	
						 <?php if(!empty($navigation_item_rec)){ ?>  
						 <div class="span3">
								  <label>Navigation Items</label>   
								  <select multiple name="navigation_items[]" id="navigation_items" class="span12">
								  <?php foreach($navigation_item_rec as $navigation_item_rec){ ?> 
									  <option value="<?php echo $navigation_item_rec['NavigationItemId']; ?>"><?php echo $navigation_item_rec['NavigationItem']; ?></option>
								  <?php } ?>	  
								  </select>    
								  <?php  echo form_error('navigation_items','<span class="label label-important">','</span>');?>                  
						   </div>  	
						   
                         <div class="span4">
							 <label></label><br>   
                              <input type="submit" class="btn" value="Add">                     
                          </div> 
                          <?php }
                          if($this->uri->segment(3) != ''){
						  ?> 
                           <div class="span4">
							 <label></label><br>  
							<a onclick="generateJSON('<?php echo $user_role_type_nav_place_uri_id; ?>')" href="#" class="btn btn-xs btn-default">Generate JSON</a>
                           </div>     
                           <?php } ?>          	
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   </div> 
                   
                    </div><!-- Table -->
                   
                </div>             
            </div>
        </div>
        
        <!-- //Dash Content -->
        <div class="container">
			<div class="row-fluid">
                   
                   <div class="span8">         
                         
                                	<form method="post" action="<?php echo site_url('navigation/delete_selected_navigation_placements/'.$user_role_type_nav_place_uri_id); ?>"  onsubmit="return confirm('Please confirm to delete multiple records');">
                                	  
                                    <table id="myTable" class="table table-bordered" style="border-top:0px;">
										
									<thead>	
                                    	<tr>
                                        	<th>Navigation Item</th> 
                                        	<th>Url</th> 
                                        	<th>Action <input type="submit" name="delete_selected" value="Delete Selected" class="btn btn-xs btn-danger">
                                        	 </th> 
												
                                        </tr>
                                       </thead>
                                       <tbody>     
                                       <?php  foreach($navigation_placement_rec as $placement_rec){
										  $navigation_placement_child_rec = $this->navigation_item_model->get_navigation_placement_by_place($role_type_nav_place_id,$placement_rec['NavigationPlacementId']);
										  $show_del = 1; 
										  if(!empty($navigation_placement_child_rec)){
											$show_del = 0;   
											 }
										   
                                        $pData['placement_rec'] = $placement_rec;
                                        $pData['user_role_type_nav_place_uri_id'] = $user_role_type_nav_place_uri_id;
                                        $pData['Child_Title_prefix'] = '';
                                        $pData['Title_start_tag'] = '<b>';
                                        $pData['Title_end_tag'] = '</b>';
                                        $pData['show_delete_btn'] = $show_del;
                                        $this->load->view('super/navigation_item_places/row_partial',$pData);
                                        
										if(!empty($navigation_placement_child_rec)){
											foreach($navigation_placement_child_rec as $child_rec){
                                        
												// Child  
												$navigation_third_level_child_rec = $this->navigation_item_model->get_navigation_placement_by_place($role_type_nav_place_id,$child_rec['NavigationPlacementId']);
												$show_child_del = 1; 
												if(!empty($navigation_third_level_child_rec)){
													$show_child_del = 0;   
												}
												$pData['placement_rec'] = $child_rec;
												$pData['user_role_type_nav_place_uri_id'] = $user_role_type_nav_place_uri_id;
												$pData['Child_Title_prefix'] = '-';
												$pData['Title_start_tag'] = '<span style="margin-left:20px;">';
												$pData['Title_end_tag'] = '</span>';
												$pData['show_delete_btn'] = $show_child_del;
												$this->load->view('super/navigation_item_places/row_partial',$pData); 
												
												if(!empty($navigation_third_level_child_rec)){
													foreach($navigation_third_level_child_rec as $third_level_child_rec){
												
														// third level Child  
														 
														$cData['placement_rec'] = $third_level_child_rec;
														$cData['user_role_type_nav_place_uri_id'] = $user_role_type_nav_place_uri_id;
														$cData['Child_Title_prefix'] = '-';
														$cData['Title_start_tag'] = '<span style="margin-left:40px;">';
														$cData['Title_end_tag'] = '</span>';
														$cData['show_delete_btn'] = 1;
														$this->load->view('super/navigation_item_places/row_partial',$cData); 
													 
													}
												 }
											}
                                         }
                                       } ?>
                                       </tbody> 
                                      
                                    </table>
                                    </form> 
                                	                   
                        </div>

						<?php $data['role_type_nav_place_id'] = $role_type_nav_place_id; ?>
                        <div class="span4"><div style="border:1px solid #ccc; background:#f6f6f6; padding:0 0px 10px 10px;"><?php $this->load->view('super/navigation_item_places/navigation_sorting', $role_type_nav_place_id);?></div></div>


                        </div>
        </div>
        
    <script>
		
		 
		function get_navigation_items(){
					 
					var user_role_type_nav_place_id = $('#user_role_type_nav_place option:selected').val();
					if(user_role_type_nav_place_id != ''){
						var redirectUrl	= "<?php echo site_url('navigation/navigation_placement');?>/"+user_role_type_nav_place_id;				 
						window.location.href = redirectUrl;
					}
				
		}
		 
		function generateJSON(role_type_nav_place_id_enc){
				
				var obj = $(this);
				var urll = '<?php echo site_url('navigation/generate_json'); ?>/'+role_type_nav_place_id_enc;
				$.post( urll, function(data){
						alert(data);
					
				});
		}
		
    </script>         
      <?php $this->load->view('super/assets/footer');?>
 
