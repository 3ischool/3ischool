<?php $this->load->view('super/assets/header');?>
  <?php
  if($this->uri->segment(2) == 'edit_school_db'){
	  $db_name = $edit_rec[0]['DBName'];
	  $effective_date = $edit_rec[0]['EffectiveDate'];
	  $is_default = $edit_rec[0]['IsDefault'];
  }else{
	  $db_name = set_value('db_name');
	  $effective_date = set_value('effective_date');
	  $is_default = set_value('is_default');
	 }
  ?>
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "School DB";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                    
                   <a href="<?php echo site_url('school/manage_school_db');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
						  <div class="span6">
                              <label>School DBName</label>
                               <input id="db_name" name="db_name" placeholder="" class="span12" type="text" value="<?php echo $db_name; ?>">
							   <?php  echo form_error('db_name','<span class="label label-important">','</span>');?>	
                          </div> 	
                        </div> 	
                        <div class="row-fluid top10">
						  <div class="span6">
                              <label>Effective Date</label>
                               <input id="effective_date" class="datepicker" name="effective_date" placeholder="" class="span12" type="text" value="<?php echo $effective_date; ?>">
							   <?php  echo form_error('effective_date','<span class="label label-important">','</span>');?>	
                          </div> 	
                        </div> 	
                        <div class="row-fluid top10">
						  <div class="span6">
                              <input id="is_default" name="is_default" type="checkbox" value="1" <?php if($is_default == 1){?>checked=checked<?php } ?>>
							   <label>IsDefault</label>
                               <?php  echo form_error('is_default','<span class="label label-important">','</span>');?>	
                          </div> 	
                        </div> 	
					 
                        <div class="row-fluid top10">
                          <div class="span6">
                              <input type="submit" name="publish" class="btn" value="Save & Continue">                     
                              <input type="submit" name="publish2" class="btn" value="Save & Exit">                     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
