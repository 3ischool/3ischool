<?php $this->load->view('super/assets/header');?>
  <?php
  if($this->uri->segment(2) == 'edit_school'){
	  $school_name = $edit_rec[0]['SchoolName'];
	  $school_board_id = $edit_rec[0]['BoardId'];
	  $school_group_id = $edit_rec[0]['SchoolGroupId'];
	  $domain = $edit_rec[0]['Domain'];
	  $student_strength = $edit_rec[0]['StudentsStrength'];
	  $school_db_id = $edit_rec[0]['SchoolDBId'];
  }else{
	  $school_name = set_value('school_name');
	  $school_board_id = set_value('school_board');
	  $school_group_id = set_value('school_group');
	  $domain = set_value('domain');
	  $student_strength = set_value('student_strength');
	  $school_db_id = set_value('school_db');
	  }
  ?>
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "School";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                    
                   <a href="<?php echo site_url('school/manage_school');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
						
						  <div class="span6">
                              <label>School Name</label>
                               <input id="school_name" name="school_name" placeholder="" class="span12" type="text" value="<?php echo $school_name; ?>">
							   <?php  echo form_error('school_name','<span class="label label-important">','</span>');?>	
                          </div> 	
                     
                         <div class="span6">
                              <label>Board</label>
                              <select name="school_board">
								<option value="">Select School</option>
								<?php foreach($board_rec as $board_rec){ ?>
									<option <?php if($school_board_id == $board_rec['BoardId']){ ?>selected=selected<?php } ?> value="<?php echo $board_rec['BoardId']; ?>"><?php echo $board_rec['BoardName']; ?></option>
								<?php } ?>	
                              </select>  
							   <?php  echo form_error('school_board','<span class="label label-important">','</span>');?>	
                          </div> 
                           
                        </div>
                        <div class="row-fluid top10">
						
						  <div class="span6">
                              <label>School Group</label>
                              <select name="school_group">
								<option value="">Select School</option>
								<?php foreach($school_group_rec as $school_group_rec){ ?>
									<option <?php if($school_group_id == $school_group_rec['SchoolGroupId']){ ?>selected=selected<?php } ?> value="<?php echo $school_group_rec['SchoolGroupId']; ?>"><?php echo $school_group_rec['SchoolGroup']; ?></option>
								<?php } ?>	
                              </select>  
							   <?php  echo form_error('school_group','<span class="label label-important">','</span>');?>	
                          </div> 
						  <div class="span6">
                            <label>Domain</label>
                               <input id="domain" name="domain" placeholder="" class="span12" type="text" value="<?php echo $domain; ?>">
							   <?php  echo form_error('domain','<span class="label label-important">','</span>');?>	
                          </div> 	
                      
                        </div>
                        
                        <div class="row-fluid top10">
						
						  <div class="span6">
                              <label>Students Strength</label>
                               <input id="student_strength" name="student_strength" placeholder="" class="span12" type="text" value="<?php echo $student_strength; ?>">
							   <?php  echo form_error('student_strength','<span class="label label-important">','</span>');?>	
                          </div> 	
                     
                         <div class="span6">
                              <label>School DB</label>
                              <select name="school_db">
								<option value="">Select School</option>
								<?php foreach($school_db_rec as $school_db_rec){ ?>
									<option <?php if($school_db_id == $school_db_rec['SchoolDBId']){ ?>selected=selected<?php } ?> value="<?php echo $school_db_rec['SchoolDBId']; ?>"><?php echo $school_db_rec['DBName']; ?></option>
								<?php } ?>	
                              </select>  
							   <?php  echo form_error('school_db','<span class="label label-important">','</span>');?>	
                          </div> 
                           
                        </div>
                         
                        <div class="row-fluid top10">
                          <div class="span6">
                              <input type="submit" name="publish" class="btn" value="Save & Continue">                     
                              <input type="submit" name="publish2" class="btn" value="Save & Exit">                     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
