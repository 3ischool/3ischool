<?php $this->load->view('super/assets/header');?>
        
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "School Board";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav" style="padding-top:10px; padding-bottom:10px;">
        	 
        	<div class="btn-group pull-left left15">
                                        
                                    </div>
                                	 <div class="btn-group pull-right left10">
                                        <a class="btn dropdown-toggle" href="<?php echo site_url('school/add_board'); ?>">
                                        	Add
                                        </a>
                                        
                                    </div>                               
                                    <div class="btn-group pull-right left10">
                                         
                                    </div>
                                    
                                    <div class="btn-group pull-right">
                                        
                                    </div>
                                    
                                    
                                    
                                    <div class="clearfix"></div>
        </div>
        <!-- //Page Inner Navigation -->
        
        <!-- Dash Content -->
        
        <div class="dashContent">
        	<div class="container-fluid">
            	<div class="row-fluid">
                	<div class="span12">
                    
                        <?php
						   $msg = $this->session->userdata('msg');
						   if($msg != ''){
							$this->session->unset_userdata('msg');
						   ?> 
							<div class="alert alert-success alert-dismissable">
							  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							  <?php echo $msg; ?>
							</div>  
						   <?php	   
							}
						   ?>          
                        <div class="row-fluid ">
                        
                        	<div class="span12 dashBoxes" style="overflow:hidden;">
                                	 
                                    <div class="row-fluid">
                                	 
                                    <table class="table table-bordered" id="myTable" style="border-top:0px;">
										<thead> 
                                    	<tr>
                                        	<th>Board Name</th> 
                                        	<th>Board Description</th> 
                                        	<th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($board as $rec){ ?>
                                        <tr>
                                        	<td><?php echo $rec['BoardName']; ?></td> 
                                        	<td><?php echo $rec['BoardDescription']; ?></td> 
											<td>
                                            <a href="<?php echo site_url('school/edit_board/'.base64e($rec['BoardId'])); ?>" class="btn btn-xs btn-default" title="Edit"> <i class="icon icon-pencil"></i></a>
											<?php
											$rData = $this->common_model->compare_in_table('School','BoardId',$rec['BoardId']);
											if(empty($rData)){
											?>
											<a href="<?php echo site_url('school/delete_board/'.base64e($rec['BoardId'])); ?>" onClick = "return confirm('Are you sure you want to delete this?');" class="btn btn-xs btn-default" title="Delete"><i class="icon icon-trash"></i></a>
											<?php } ?>
											</td>
                                        </tr>
                                       <?php } ?>
                                       </tbody> 
                                    </table>
                                    <div class="boxGraphFooter" style="border-radius:0; margin:0; padding:0 15px;">
                                		 
                                            		<?php
													 // echo $links;
													  ?>
                                            
                                        
                                        
                                	</div>  
                                	</div>  
                                
	 
                                </div>
                                     
                                 
                                
							</div>                          
                        </div>
                        
                        
                       
                    </div>
                </div>
            	
                
            </div>
        </div>
        <!-- //Dash Content -->
             
       <?php $this->load->view('super/assets/footer');?>
