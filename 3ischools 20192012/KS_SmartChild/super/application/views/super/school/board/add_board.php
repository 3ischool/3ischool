<?php $this->load->view('super/assets/header');?>
  <?php
  if($this->uri->segment(2) == 'edit_board'){
	  $board = $edit_rec[0]['BoardName'];
	  $description = $edit_rec[0]['BoardDescription'];
  }else{
	  $board = set_value('board');
	  $description = set_value('description');
	  }
  ?>
    	<!-- Page Title -->
    	<?php
			$pageTitleData['pageTitle'] = "School Board";
			$pageTitleData['pageSubTitle'] = "";
    	$this->load->view('super/assets/pageTitle',$pageTitleData);?>
        
        <!-- //Page Title -->
        <div class="clearfix"></div>
        
        <!-- Page Inner Navigation -->
        <div class="pageInnerNav">
        	<div class="navbar">
              <div class="navbar-inner">
                <div class="container">
                  <button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar" type="button" style="margin-bottom:5px;">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                    
                   <a href="<?php echo site_url('school/manage_board');?>" class="btn btn-default pull-right">Back</a>            
                   
                </div>
              </div>
    </div>
        </div>
        <!-- //Page Inner Navigation -->
      
        <!-- Dash Content -->
        <div class="dashContent">
			
        	<div class="container-fluid customForm">
            	<div class="row-fluid">
					
                    <!--<div class="span12 dashBoxes">-->
                    <div class="span12">
					<?php echo form_open(); ?>  	
                    <div class="Dashtext">                       
                        <div class="row-fluid top10">
						
						  <div class="span6">
                              <label>Board Name</label>
                               <input id="board" name="board" placeholder="" class="span12" type="text" value="<?php echo $board; ?>">
							   <?php  echo form_error('board','<span class="label label-important">','</span>');?>	
                          </div> 	
                     </div> 	
					 <div class="row-fluid top10">		
                         <div class="span6">
                              <label>Board Description</label>
                               <textarea id="description" name="description" class="span12" ><?php echo $description; ?></textarea>
							   <?php  echo form_error('description','<span class="label label-important">','</span>');?>	
                          </div> 
                           
                        </div>
                         
                        <div class="row-fluid top10">
                          <div class="span6">
                              <input type="submit" name="publish" class="btn" value="Save & Continue">                     
                              <input type="submit" name="publish2" class="btn" value="Save & Exit">                     
                          </div>  
                        </div> 
                       </div> 
                       </form> 
                    </div>  
                   
                </div>             
            </div>
        </div>
        <!-- //Dash Content -->
             
      <?php $this->load->view('super/assets/footer');?>
 
