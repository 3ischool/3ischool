<?php
Class Dfbsetup_model extends CI_Model
{
	 
     function __construct(){
        parent::__construct();
        
    }
	
	/* getting all records from a particular table */
		function model_get_all_records($tableName){	
			(string)$tableName = $tableName;	 
			$this->db->from($tableName);			
			$query = $this->db->get();            
			return $query->result_array();	
		}
	/* //getting all records from a particular table */
	
	
	/* getting data against a particular column table */
		function model_get_by_col($tableName,$col,$colValue){	
		  
			$this->db->from($tableName);
            $this->db->where($col,$colValue);
			$query = $this->db->get();	
            
			return $query->result_array();	
		}
	/* //getting data against a particular column table */
	
	/* Insert data in table */
	function insert($data, $table){
		$this->db->insert($table,$data);
		$error = $this->db->error();
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = $this->db->insert_id();
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to insert data</div>';
		}
		 
		return $rData;
	}
	/* //Insert data in table */
	
    
    /* update data in table */
	function update($ID, $data, $colum_name, $table){
        (string)$colum_name = $colum_name;
        (int)$ID = $ID;
		$this->db->where($colum_name, $ID);
		$this->db->update($table, $data);
		$error = $this->db->error();
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = '<div class="alert alert-success">Record Updated!!!</div>';
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to update data</div>';
		}
		 
		return $rData;
    }
	/* //update data in table */
	
	 function delete($ID, $colum_name, $table){
        (string)$colum_name = $colum_name;
        (int)$ID = $ID;    
		$this->db->where($colum_name, $ID);
		$this->db->delete($table);
		$error = $this->db->error(); 
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = '<div class="alert alert-success">Record deleted!!</div>';
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] Unable to delete data</div>';
		}
		 
		return $rData;
	}
	
	/* get assigned HTML Controls of a column */
	function get_html_control_by_col_type($type){	 
		$this->db->select('dfb_html_control.*');
		$this->db->join('dfb_col_type_control', 'dfb_html_control.HTMLControlId = dfb_col_type_control.HTMLControlId');
		$this->db->from('dfb_html_control');
		$this->db->where('dfb_col_type_control.ColTypeId',$type);	
		
		$query = $this->db->get();		
		return $query->result_array();	
	}
	/* //get assigned HTML Controls of a column */
	
	function model_get_validations_of_a_col($col_id){
		$this->db->select('dfb_form_col_validation.ControlValidationValue,dfb_column_validation.*');
		$this->db->join('dfb_column_validation', 'dfb_column_validation.ColumnValidationId = dfb_form_col_validation.ControlValidationId');
		$this->db->from('dfb_form_col_validation');
		$this->db->where('dfb_form_col_validation.FormColumnId',$col_id);		
		$query = $this->db->get();		
		return $query->result_array();
	}
	
	/* get assigned HTML Controls of a column */
	function get_validations_by_html_control($id){	 
		$this->db->select('dfb_control_validation.*,dfb_column_validation.*');
		$this->db->join('dfb_column_validation', 'dfb_column_validation.ColumnValidationId = dfb_control_validation.ColumnValidationId');
		$this->db->from('dfb_control_validation');
		$this->db->where('dfb_control_validation.HTMLControlId',$id);
		
		$query = $this->db->get(); 
					
		return $query->result_array();	
	}
	/* //get assigned HTML Controls of a column */
    
    
    
	/* getting table properties MSSQL */	
	function mssql_table_structure($tableName){
		
		$childDB = $this->load->database('child', TRUE);
		$query = $childDB->query("SELECT  C.NAME AS [COLUMNNAME], P.NAME AS [DATATYPE],C.is_identity 
FROM SYS.OBJECTS AS T
JOIN SYS.COLUMNS AS C ON T.OBJECT_ID=C.OBJECT_ID  
JOIN SYS.TYPES AS P ON C.SYSTEM_TYPE_ID=P.SYSTEM_TYPE_ID
WHERE T.NAME='".$tableName."'");

	$data = $query->result_array();
	$manuplatedData1 = array();	
	foreach($data as $data3){
		$manuplatedData1[$data3['COLUMNNAME']] = array(
			'COLUMNNAME' => $data3['COLUMNNAME'],			 
			'DATATYPE' => $data3['DATATYPE'],
			'is_identity' => $data3['is_identity']
		);
	}	
	 
	$query1 = $childDB->query("SELECT COL_NAME(fc.parent_object_id, fc.parent_column_id) AS COLUMNNAME,
    OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName,
    COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS ReferenceColumnName
FROM sys.foreign_keys AS f
INNER JOIN sys.foreign_key_columns AS fc
ON f.OBJECT_ID = fc.constraint_object_id
where  OBJECT_NAME(f.parent_object_id) = N'".$tableName."'");
	
	$data1 = $query1->result_array();
	$manuplatedData = array();	
	foreach($data1 as $data2){
		$manuplatedData[$data2['COLUMNNAME']] = array(
			'ReferenceTableName' => $data2['ReferenceTableName'],			 
			'ReferenceColumnName' => $data2['ReferenceColumnName']
		);
	}	
		 
	 

	 $merged_data =array_merge_recursive($manuplatedData,$manuplatedData1);
	 $this->load->database('default', TRUE);
	 return $merged_data;
	}
	/* //getting table properties MSSQL */	
	
	
	
	function get_col_validation($col_id,$val_id){
		$this->db->from('dfb_form_col_validation');
            $this->db->where('FormColumnId',$col_id);
			$this->db->where('ControlValidationId',$val_id);
			$query = $this->db->get();           
			return $query->result_array(); 
	} 
	
	
	/* getting column of table with position index asc */
		function get_sorted_cols($id){	
		  
			$this->db->from('dfb_form_column');
            $this->db->where('FormTableId',$id);
			$this->db->order_by('PositionIndex','asc');
			$query = $this->db->get();	
            
			return $query->result_array();	
		}
	/* //getting column of table with position index asc */
	
	
	function check_if_col_exist($formId,$colName){
			$this->db->from('dfb_form_column');
            $this->db->where('FormTableId',$formId);
            $this->db->where('FormColumn',$colName);			 
			$query = $this->db->get();	
            
			return $query->result_array();
	}
		
}	
?>
