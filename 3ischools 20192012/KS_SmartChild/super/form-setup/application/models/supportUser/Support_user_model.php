<?php
class Support_user_model extends CI_Model
{
function __construct() 
{
parent::__construct();

}


function get_all_support_user($pageSize,$pageNumber,$user_name,$supportUserType,$status){
	    
	  $query_string = "";
	    if($user_name != ''){
			 $query_string .= ' UserName LIKE "%'.$user_name.'%" AND ';
		}
		if($supportUserType != ''){
			 $query_string .= ' UserTypeId = '.$supportUserType.' AND ';
		}	
	    if($status != ''){
			 $query_string .= ' IsActive = '.$status.' AND ';
		}  
	    
	    $q = 'WITH CTE AS
    (
      SELECT
        ROW_NUMBER() OVER ( ORDER BY [UserId] ) AS RowNum , *
      FROM SupportUser

        
    )

   SELECT
      *
    FROM CTE
    WHERE
    '.$query_string.'
      (RowNum > '.$pageSize.' * ('.$pageNumber.' - 1) )
      AND
      (RowNum <= '.$pageSize.' * '.$pageNumber.' )
    Order By RowNum ';
	    
	    
	    $query = $this->db->query($q);
        return $result = $query->result_array();
}

function record_count($user_name,$supportUserType,$status){
	
	if($user_name != ''){
		$this->db->like('UserName',$user_name);
		}
	if($supportUserType != ''){
		$this->db->where('UserTypeId',$supportUserType);
		}
	if($status != ''){
		$this->db->where('IsActive',$status);
		}	
	$this->db->select('UserId');
	$result = $this->db->get('SupportUser');
	return $data = $result->num_rows();
	}
	
	function validate_login($usr,$pass){
		$this->db->limit(1);
		$this->db->where('UserEmail',$usr);
		$this->db->where('Password',$pass);
		$result = $this->db->get('SuperUser');
		return $data = $result->result_array();
	}
	
	function check_valid_cookie($id,$eml){
		$this->db->limit(1);
		$this->db->where('UserEmail',$eml);
		$this->db->where('Id',$id);
		$this->db->select('Id');
		$result = $this->db->get('SuperUser');
		return $data = $result->result_array();
	}		

}
