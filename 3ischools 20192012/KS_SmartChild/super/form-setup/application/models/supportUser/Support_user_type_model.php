<?php
class Support_user_type_model extends CI_Model
{
function __construct() 
{
parent::__construct();

}


function get_all_support_user_type($pageSize,$pageNumber){
	    
	    $q = 'WITH CTE AS
    (
      SELECT
        ROW_NUMBER() OVER ( ORDER BY [UserTypeId] ) AS RowNum , *
      FROM SupportUserType

        
    )

   SELECT
      *
    FROM CTE
    WHERE
      (RowNum > '.$pageSize.' * ('.$pageNumber.' - 1) )
      AND
      (RowNum <= '.$pageSize.' * '.$pageNumber.' )
    Order By RowNum ';
	    
	    
	    $query = $this->db->query($q);
        return $result = $query->result_array();
}

function record_count(){
	$this->db->select('UserTypeId');
	$result = $this->db->get('SupportUserType');
	return $data = $result->num_rows();
	}	

}
