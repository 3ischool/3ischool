<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dfbsetup extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('dfbsetup_model');	
		$this->load->helper('digi_superuser_helper');		 
		 
		is_logged_in(); 
	}

	
	function index(){
		$this->manage_table_setup();
	}
	
	
	function manage_table_setup(){
		$data['fb_table_data'] = $this->dfbsetup_model->model_get_all_records('dfb_form_table'); //fetching form builder all tables
		$childDB = $this->load->database('child', TRUE);		
		$data['DB_tables'] = $childDB->list_tables(); // fetching all tables in database
		$this->load->database('default', TRUE); 
		$this->load->view('dfbsetup/manage_table_setup',$data);	
	} 
    
    function change_status(){
        $stat = $this->uri->segment(3);
        $id = $this->uri->segment(4);
        $this->session->set_flashdata('message', '<div class="alert alert-danger">Unable to update data</div>');
        if(is_numeric($stat) && is_numeric($id)){
            $data = array('IsActive' => $stat);
            $update_status = $this->dfbsetup_model->update($id, $data, 'FormTableId', 'dfb_form_table'); 
            $this->session->set_flashdata('message', $update_status);
            
        }
        redirect('dfbsetup/manage_table_setup','refresh');
    }
	
	function create_table_master(){
		$tableName = $this->uri->segment(3);
		$childDB = $this->load->database('child', TRUE);
		 if($childDB->table_exists($tableName)==1){ // Check if table exist
			 $this->load->database('default', TRUE);
		 	$check_in_form_table = $this->dfbsetup_model->model_get_by_col("dfb_form_table","FormTable",$tableName); // check if table exist in dfb_form_table
              
			if(empty($check_in_form_table)){
				$colmns = $this->dfbsetup_model->mssql_table_structure($tableName);
				
                    $tabledata = array(
                    'FormTable'  => $tableName,                     //post variable data into array
                    'FormTitle'  => $tableName,
                    'IsActive'  => 1,

                    );
                    
                $last_insert_id = $this->dfbsetup_model->insert($tabledata,'dfb_form_table');
                
                
                
               /* if(is_numeric($last_insert_id)){
                    $colIndex = 0;
                    foreach($colmns as $colmnsData){
                        $colIndex++;
                        $COLUMNNAME = "";
                        $is_identity = "";
                        $ReferenceTableName = "";
                        $ReferenceColumnName = "";
                        $IsForeign = 0;
                        if (array_key_exists("ReferenceColumnName",$colmnsData)){
                            $ReferenceColumnName = $colmnsData['ReferenceColumnName'];
                            $IsForeign = 1;
                        }
                        if (array_key_exists("ReferenceTableName",$colmnsData)){
                            $ReferenceTableName = $colmnsData['ReferenceTableName'];
                            $IsForeign = 1;
                        }
                        if (array_key_exists("is_identity",$colmnsData)){
                            $is_identity = $colmnsData['is_identity'];
                        }
                        if (array_key_exists("COLUMNNAME",$colmnsData)){
                            $COLUMNNAME = $colmnsData['COLUMNNAME'];
                        }

                        if($COLUMNNAME!='' && ($is_identity==0 || $is_identity==1)){
                            $show_in_form = 1;
							$show_in_grid = 1;
							if($is_identity==1){
								$show_in_form = 0;
								$show_in_grid = 0;
							} 
                            $coldata = array(
                            'FormColumn'  => $COLUMNNAME,                     //post variable data into array
                            'ColLabel'  => $COLUMNNAME,
                            'IsUnique'  => 0,
                            
                            'ColWidth'  => 1,                     //post variable data into array
                            'ShowInForm'  => $show_in_form,
                            'ShowInGrid'  => $show_in_grid,
                            
                            'PositionIndex'  => $colIndex,                     //post variable data into array
                            'FormTableId'  => $last_insert_id,
                            'IsPrimary'  => $is_identity,   
                            
                            'IsForeign' => $IsForeign,
                            'ForeignPrimaryCol' =>$ReferenceColumnName,
                            'ForeignTableName' => $ReferenceTableName     
                                 

                            );
							 
                            //$last_insert_col_id = $this->dfbsetup_model->insert($coldata,'dfb_form_column'); 
                            
                        }	

                     }
                    $this->session->set_flashdata('message', '<div class="alert alert-success">Congratulation!!! Your form is ready to use</div>');
                    
                }else{
                    
                    $this->session->set_flashdata('message', $last_insert_id);
                } */
				$this->session->set_flashdata('message', '<div class="alert alert-success">Congratulation!!! Your form is ready to use</div>');
				 
			 }else{
			     $this->session->set_flashdata('message', '<div class="alert alert-danger">Form for selected table is already exist</div>');
			 } 
		}else{
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Selected table does not exist in database</div>');		
		}
		
	 
	 redirect('dfbsetup/manage_table_setup','refresh');
		 
	}
    
    function col_setup(){
		$tableName = $this->uri->segment(3); //Table Name
		$data['tableName'] = $tableName;
        $id = $this->uri->segment(4); //Table ID in dfb_form_table
        $data['error'] = ''; //Initiated empty error array
        $data['col'] = array(); // Initiated empty col array
        $childDB = $this->load->database('child', TRUE);
		 if($childDB->table_exists($tableName)==1 && is_numeric($id)){ // Check if table exist in database
			  
			 $this->load->database('default', TRUE);
		 	$check_in_form_table = $this->dfbsetup_model->model_get_by_col("dfb_form_table","FormTable",$tableName); // check if table exist in dfb_form_table
              
			if(!empty($check_in_form_table)){
				$colmns = $this->dfbsetup_model->mssql_table_structure($tableName); //Get all current columns of a table form database
				 
                    $colIndex = 0;					
                    foreach($colmns as $colmnsData){
                        $colIndex++;
                        $COLUMNNAME = "";
                        $is_identity = "";
                        $ReferenceTableName = "";
                        $ReferenceColumnName = "";
                        $IsForeign = 0;
                        if (array_key_exists("ReferenceColumnName",$colmnsData)){
                            $ReferenceColumnName = $colmnsData['ReferenceColumnName'];
                            $IsForeign = 1;
                        }
                        if (array_key_exists("ReferenceTableName",$colmnsData)){
                            $ReferenceTableName = $colmnsData['ReferenceTableName'];
                            $IsForeign = 1;
                        }
                        if (array_key_exists("is_identity",$colmnsData)){
                            $is_identity = $colmnsData['is_identity'];
                        }
                        if (array_key_exists("COLUMNNAME",$colmnsData)){
                            $COLUMNNAME = $colmnsData['COLUMNNAME'];
                        }
						 
                        if($COLUMNNAME!='' && ($is_identity==0 || $is_identity==1)){
                            $check_if_col_exist = $this->dfbsetup_model->check_if_col_exist($id,$COLUMNNAME);
							 
							$show_in_form = 1;
							$show_in_grid = 1;
							if($is_identity==1){
								$show_in_form = 0;
								$show_in_grid = 0;
							}
							 
                            if(empty($check_if_col_exist)){
                                $coldata = array(
                                    'FormColumn'  => $COLUMNNAME,                     //post variable data into array
                                    'ColLabel'  => $COLUMNNAME,
                                    'IsUnique'  => 0,
                                    'ColWidth'  => 1,                     //post variable data into array
                                    'ShowInForm'  => $show_in_form,
                                    'ShowInGrid'  => $show_in_grid,
                                    'PositionIndex'  => $colIndex,                     //post variable data into array
                                    'FormTableId'  => $id,
                                    'IsPrimary'  => $is_identity,
                                    'IsForeign' => $IsForeign,
                                    'ForeignPrimaryCol' =>$ReferenceColumnName,
                                    'ForeignTableName' => $ReferenceTableName
                                );

                               $last_insert_col_id = $this->dfbsetup_model->insert($coldata,'dfb_form_column');
								
                            } 
                        }	

                     }  
					 
					 
                $colmnsfrmDB = $this->dfbsetup_model->get_sorted_cols($id);
				
				$data['htmlControl1'] = $this->dfbsetup_model->get_html_control_by_col_type(1);
				$data['htmlControl2'] = $this->dfbsetup_model->get_html_control_by_col_type(2);
				$data['htmlControl3'] = $this->dfbsetup_model->get_html_control_by_col_type(3);
				$data['table_result'] = $check_in_form_table;
				 				 
				$data['col'] = $colmnsfrmDB;
				 
			 }else{
			     $data['error'] =  'Kindly set master for selected form first'; 
			 } 
		}else{
			$data['error'] =  'Selected table does not exist in database'; 
		}
		
	 
		
	 $this->load->view('dfbsetup/col_setup',$data);
	  
		 
	}
	
	
	
	
	function update_col(){		 
		$col_id = $this->uri->segment(3); 
		
		if(!is_numeric($col_id)){
		 
			echo "Error!!!"; exit;
		}	
		$ifk = $this->input->post('ifn');
		$ipk = $this->input->post('ipk');
		$prevHTMLControl = $this->input->post('phc');
		
		
		$this->form_validation->set_rules('fl', 'Label', 'trim|required|alpha_numeric_spaces');
		if($ipk==0){
			$this->form_validation->set_rules('fhc', 'HTML Control', 'trim|required|integer');	
		}	 
		$this->form_validation->set_rules('fiu', 'Unique', 'trim|integer');
		$this->form_validation->set_rules('fsf', 'Show in form', 'trim|integer');
		$this->form_validation->set_rules('fsg', 'Show in grid', 'trim|integer');
		$this->form_validation->set_rules('fs', 'Sorting', 'trim|required|integer');
		$this->form_validation->set_rules('colSize', 'Col Size', 'trim|integer');	
		if($ifk==1){	
			$this->form_validation->set_rules('frc', 'Relation Title', 'trim|required|alpha_numeric');
		}
		
		if ($this->form_validation->run() == FALSE) {
			 
			$errors = validation_errors();
			$errors = str_replace("<p>","<li>",$errors);
			$errors = str_replace("</p>","</li>",$errors);
			$errors = '<div class="alert alert-danger"><ul>'.$errors.'</ul></div>';
			echo $errors;
		}else{
		 	$fl = $this->input->post('fl');
			$fhc = $this->input->post('fhc');						
			$fs = $this->input->post('fs');			
			$fiu = $this->input->post('fiu');			
			$fsf = $this->input->post('fsf');			
			$fsg = $this->input->post('fsg');		
			$frc = $this->input->post('frc');
			$frc = $this->input->post('frc');
			$colSize = $this->input->post('colSize');
			
			
			if($prevHTMLControl!=$fhc){
				$this->dfbsetup_model->delete($col_id, 'FormColumnId', 'dfb_form_col_validation');
			}
			
			$update_data = array(
				'ColLabel' => $fl,
				'IsUnique' => $fiu,
				'ShowInForm' => $fsf,
				'ShowInGrid' => $fsg,
				'HTMLControlId' => $fhc,
				'PositionIndex' => $fs,
				'ForeignTitleCol' => $frc,
				'ColWidth' => $colSize
			);			 
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Validation Added Successfully</div>');
			$rData = $this->dfbsetup_model->update($col_id, $update_data, 'FormColumnId', 'dfb_form_column'); 
			
			echo $rData;
		}
		 
		 
		
		 
	}
	
	
	function add_validations(){
		$col_id = $this->uri->segment(3);
		if(is_numeric($col_id)){
			 $colmnsData = $this->dfbsetup_model->model_get_by_col('dfb_form_column','FormColumnId',$col_id);
			 if(!empty($colmnsData)){
			 	if($colmnsData[0]['HTMLControlId']>=1){
			 		$data['availableValidation'] = $this->dfbsetup_model->get_validations_by_html_control($colmnsData[0]['HTMLControlId']);
					$data['addedValidation'] = $this->dfbsetup_model->model_get_validations_of_a_col($col_id);
					$this->load->view('dfbsetup/add_validations',$data);
				}else{
					echo "Please set HTML Control First!!!";
				}
			 }else{
			 	echo "Column not found in DB!!!";
			 }
		}else{
			echo "Column ID is incorrect!!!";
		}		
	}
	
	function add_validations_ajax(){		 
		$col_id = $this->uri->segment(3); 
		
		if(!is_numeric($col_id)){		 
			echo "Error!!!"; exit;
		} 
		 
		$IsValueHidden = $this->input->post('IsValue');
		 
		$this->form_validation->set_rules('validationId', 'Validation', 'trim|required|integer|callback__check_if_validation_exist_in_col['.$col_id.']');
		if($IsValueHidden==1){
			$this->form_validation->set_rules('setIsValue', 'Value', 'trim|required|integer');	
		}	 
		 
		if ($this->form_validation->run() == FALSE) {
			 
			$errors = validation_errors();
			$errors = str_replace("<p>","<li>",$errors);
			$errors = str_replace("</p>","</li>",$errors);
			$errors = '<div class="alert alert-danger"><ul>'.$errors.'</ul></div>';
			echo $errors;
		}else{
		 	$validationId = $this->input->post('validationId');			 					
			$setIsValue = $this->input->post('setIsValue'); 
			
			$insert_data = array(
				'FormColumnId' => $col_id,
				'ControlValidationId' => $validationId,
				'ControlValidationValue' => $setIsValue 
			);			 
			$this->session->set_flashdata('message', '<div class="alert alert-success">Validation Added Successfully</div>');
			$rData = $this->dfbsetup_model->insert($insert_data, 'dfb_form_col_validation');			
			echo $rData;
		}
		 
		 
		
		 
	}
	
	
	
	function _check_if_validation_exist_in_col($str,$col_id)
        {                
			 
			        
			$result = $this->dfbsetup_model->get_col_validation($col_id,$str); 
			          
			 
			
			 
				if (!empty($result))
                {
                        $this->form_validation->set_message('_check_if_validation_exist_in_col', 'validation already exist in the column');
                        return FALSE;
                }
                else
                {
                        return TRUE;
                }
        }
		
		
		
		function delete_col_validation(){
			(int)$col_id = trim($this->uri->segment(3));
			(int)$validation_ID = trim($this->uri->segment(4));
			if(is_numeric($col_id) && is_numeric($validation_ID)){
				$result = $this->dfbsetup_model->get_col_validation($col_id,$validation_ID);
				
				if(!empty($result)){
					
					(int)$col_id = $col_id;
					(int)$validation_ID = $validation_ID;    
					$this->db->where('FormColumnId', $col_id);
					$this->db->where('ControlValidationId', $validation_ID);
					$this->db->delete('dfb_form_col_validation');
					$error = $this->db->error(); 
					if($error['code']==00000 && trim($error['message'])==''){
						$rData = '<div class="alert alert-success">Record deleted!!</div>';
					}
					else{
						$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] Unable to delete data</div>';
					}
					$this->session->set_flashdata('message', $rData);			
				}else{
					$this->session->set_flashdata('message', '<div class="alert alert-error">Data doesn\'t exist/already deleted!!!</div>');
				}			
				
			}else{
				$this->session->set_flashdata('message', '<div class="alert alert-danger">Unable to delete data</div>');
			} 
			redirect('dfbsetup/add_validations/'.$col_id,'refresh');
		}
	
	
	function removeColumns(){
		if ($_SERVER['REQUEST_METHOD'] == 'POST'){
			$tableID = $this->input->post('tableName');
			$cols = $this->input->post('cols');
			$cols = explode(',',$cols);
			for($x=0; $x<count($cols);$x++){
				$col_id = $cols[$x];
				(int)$col_id = $col_id;
				$this->dfbsetup_model->delete($col_id, 'FormColumnId', 'dfb_form_col_validation');
				$this->dfbsetup_model->delete($col_id, 'FormColumnId', 'dfb_form_column');				 
			}
			echo 1;
		}else{
			echo "2";
		}
	}
	 
	
	
	 
	function edit_table_title(){		 
		$table_id = $this->uri->segment(3); 
		
		if(!is_numeric($table_id)){
		 
			echo "Error!!!"; exit;
		}	
		 
		$this->form_validation->set_rules('FormTitle', 'Title', 'trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('ColCount', 'Column Count', 'trim|required|numeric');
		 
		
		if ($this->form_validation->run() == FALSE) {
			 
			$errors = validation_errors();
			$errors = str_replace("<p>","<li>",$errors);
			$errors = str_replace("</p>","</li>",$errors);
			$errors = '<div class="alert alert-danger"><ul>'.$errors.'</ul></div>';
			echo $errors;
		}else{
		 	$FormTitle = $this->input->post('FormTitle');
			$ColCount = $this->input->post('ColCount');
			 
			
			 
			
			$update_data = array(
				'FormTitle' => $FormTitle,
				'ColCount' => $ColCount
			);			 
			
			$rData = $this->dfbsetup_model->update($table_id, $update_data, 'FormTableId', 'dfb_form_table'); 
			
			echo $rData;
		}
		 
		 
		
		 
	}
	 
	 
	
	

	
	
	
	 
}
