<?php $this->load->view('dfbsetup/header');?>

<!-- Page Heading -->
    <div class="row top-space-10 page-heading">
    	<div class="col-xs-6">
        	<h1>Manage Tables</h2>
        </div>
         
    </div>
    <!-- //Page Heading -->

<div id="formError"><?php echo $this->session->flashdata('message'); ?> </div>
<!-- filter -->
<div class="row top-space-10">
    	<div class="col-md-12" id="mainfrm">
       <form method="post" class="form-inline" action="<?php echo site_url('dfbsetup/create_table_master/');?>" id="masterForm" onsubmit="return submitData()">      
           
          <div class="form-group">
          
          <?php
              $compareArray = array();
              $d = 0;
              foreach($fb_table_data as $fb_table_data1){
                $compareArray[$d] = $fb_table_data1['FormTable'];
                  $d++;
              }
              
              $compareArray[$d] = 'dfb_column_validation';
              $d++;
              
              $compareArray[$d] = 'dfb_control_validation';
              $d++;
              
              $compareArray[$d] = 'dfb_form_col_validation';
              $d++;
              
              $compareArray[$d] = 'dfb_form_column';
              $d++;
              
              $compareArray[$d] = 'dfb_form_table';
              $d++;
              
              $compareArray[$d] = 'dfb_html_control';
              $d++;
              
              $compareArray[$d] = 'sysdiagrams';
              $d++;
               
              $tableCount = count($DB_tables); ?>
            	<select class="form-control" id="tbls">
                	<option value="0">Select Table</option>
                    <?php for($x=0; $x<$tableCount; $x++){
					$tableName = $DB_tables[$x];
					if(!in_array($tableName,$compareArray)){ // skipping tables exist in DFB_FormTable
					?>
					<option><?php echo $tableName;?></option>
					<?php 
					}}?>	
                </select>            
          </div>
          <div class="form-group">
            	<input type="submit" value="Add Master" class="btn btn-success" id="saveBtn" />
                 
             
          </div>          
        </form>
        </div>
    </div>

<!-- //filter -->

 
<div class="row top-space-20">
    	<div class="col-md-12">
         <table id="mainGrid" class="table table-condensed">
         	<thead>
            	<tr>
                	<th>
                    	Master
                    </th>
                    <th>
                    	URL
                    </th>
                    <th>
                    	Status
                    </th>
                    <th>
                    	Action
                    </th>
                </tr>
            </thead>
            <tbody>
            	<?php foreach($fb_table_data as $fb_table_data2){?>
            	<tr>
                	<td><?php echo $fb_table_data2['FormTitle'];?></td>
                	<td><a href="http://digitech.com/anuj/GSTChild/index.php/master/form/<?php echo  base64e($fb_table_data2['FormTable']);?>" target="_blank"><?php echo base64e($fb_table_data2['FormTable']);?></a></td>
                    <td><?php if($fb_table_data2['IsActive']==1 || $fb_table_data2['IsActive']==true){
    echo "Active";
    $link = '<a href="'.site_url('dfbsetup/change_status/0/'.$fb_table_data2['FormTableId']).'" class="btn btn-xs btn-danger"><i class="fa fa-star-o"></i></a>';
}else{ echo "inactive";
     $link = '<a href="'.site_url('dfbsetup/change_status/1/'.$fb_table_data2['FormTableId']).'" class="btn btn-xs btn-success"><i class="fa fa-star"></i></a>';
     }?></td>   
                    <td>
                    	<a href="<?php echo site_url('dfbsetup/col_setup/'.$fb_table_data2['FormTable'].'/'.$fb_table_data2['FormTableId']);?>" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>
                    	
                    	<?php echo $link;?>
                    	
                    </td>
                </tr>
                <?php } ?>
            </tbody>
         </table>
        
        	
</div>
</div>
<!-- //Data Container -->
<script type="text/javascript">
function submitData(val){
	    $('#form_loader').show();
		var selectedTable = $.trim($('#tbls').val());
		
		if(selectedTable!=0 && selectedTable!==''){
			uurl = "<?php echo site_url('dfbsetup/create_table_master/');?>"+selectedTable;
			window.location.href = uurl;
		}else{
		$('#formError').html('<div class="alert alert-danger">Please select a table</div>');
		$('#form_loader').hide();
		}
		
		return false;
		 
		 
	}
</script>


 
 
<script>
    
	
	$(document).on('click','.edit-master-record',function(e){
		e.preventDefault();
		$('#form_loader').show();
		var record = $(this);
		var idd = record.attr('data-id');
		var urll = "<?php echo site_url();?>/form/get_edit_data/<?php echo $tableName;?>/"+idd;
		
		var form_urll = "<?php echo site_url();?>/form/edit_master/<?php echo $tableName;?>/"+idd;
		
		
		 
		$.get( urll, function( data ) {
			$('#masterForm').attr('action' , form_urll); 
		  $('#mainfrm').slideDown(); 
		  $('#saveBtn').val('update record');
		  $('#form_loader').hide();
		  var obj = jQuery.parseJSON(data);
		   
		   <?php foreach($table_structure as $table_structure_jquery){?>
		  if ($("#get_<?php echo $table_structure_jquery['ColName'];?>").length === 0){
				 
		  }else{
		  	 var inputType = $("#get_<?php echo $table_structure_jquery['ColName'];?>").attr('type');
			 var fieldValue = obj[0].<?php echo $table_structure_jquery['ColName'];?>;
			 if(inputType=="checkbox"){
			 	if(fieldValue==1){
					$("#get_<?php echo $table_structure_jquery['ColName'];?>").prop("checked" , "checked");
				}else{
					$("#get_<?php echo $table_structure_jquery['ColName'];?>").removeAttr("checked");
				}
			 }
			 else{
		  	 	$("#get_<?php echo $table_structure_jquery['ColName'];?>").val(fieldValue);
			 }
		  }
		  <?php } ?>
		   
 
		});
		 
		 
	});  
	 	
		
		function openModel(formUrl){
		$('#masterForm').attr('action' , formUrl);
		document.getElementById("masterForm").reset();
		  $('#saveBtn').val('add record');
			$('#mainfrm').slideDown();
		}
		
		function clodeModel(){
			$('#masterForm').attr('action' , '');
		  $('#saveBtn').val('not set');
			$('#mainfrm').slideUp();
		}
		
		
		$(document).ready(function(){
		$('#mainGrid').DataTable();	
		});
</script>

<?php $this->load->view('dfbsetup/footer');?>
