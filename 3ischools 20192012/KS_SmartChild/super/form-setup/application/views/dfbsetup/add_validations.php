<html>
<head> 
<title>Test</title>
<link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
 <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script> 
 <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <div id="formError" style="margin:3px 0px;"><?php echo $this->session->flashdata('message'); ?></div>
    <div class="row">
        <div class="col-md-12" style="margin-top:10px;">
        <?php foreach($addedValidation as $addedValidation){?>
            <a href="<?php echo site_url('dfbsetup/delete_col_validation/'.$this->uri->segment(3).'/'.$addedValidation['ColumnValidationId']);?>" class="label label-info" onClick="return confirm('Are you sure?');"><?php echo $addedValidation['ColumnValidation'];?><?php if($addedValidation['IsValue']==1){ echo "[".$addedValidation['ControlValidationValue']."]";}?> <i class="fa fa-close"></i></a>
          <?php } ?>  
        </div>
    </div>
    
<form action="<?php echo site_url('dfbsetup/add_validations_ajax/'.$this->uri->segment(3));?>" method="post" id="form" onSubmit="return submitData()">
<div class="row" style="margin-top:10px;">
	<div class="col-xs-6"><select onChange="checkIsValue()" name="validationId" class="form-control">
	<option value="" data-id="select">Select</option>
    <?php foreach($availableValidation as $availableValidation){?>
    <option value="<?php echo $availableValidation['ColumnValidationId'];?>" data-id="<?php echo $availableValidation['IsValue'];?>"><?php echo $availableValidation['ColumnValidation'];?></option>
    <?php } ?>
</select></div>
<div class="isValueDiv col-xs-6" style="display:none">
<input type="text" name="setIsValue" class="form-control" /></div>
<input type="hidden" name="IsValue" class="isValueHidden" />
<input type="submit" value="Save" class="btn btn-success" />
</form>
</div>
</div>



<script>
	function checkIsValue(){
		var IsValue = $("select>option:selected").attr('data-id');
		$('.isValueHidden').val(IsValue);
		if(IsValue==1){
			$('.isValueDiv').show();	
		}else{
			$('.isValueDiv').hide();	
		}
	} 
</script>

<script type="text/javascript">
function submitData(){
	    //$('#form_loader').show();
		var form=$('#form');
		 
		$.post( form.attr("action"),form.serialize(), function( data ) {
		 if(Math.floor(data) == data && $.isNumeric(data)){
		 	window.location.href = '<?php echo current_url();?>';
		 }else{			   
		  $('#formError').html(data);
		  }
		});
		 
		
		return false;
	}
	
 
</script>
</body>
</html>
