<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Html</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">     
	<link href="<?php echo base_url('assets/css/styles.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/maroon-theme.css');?>" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>     
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>   
    
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
     
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
     <style>
     .modal-backdrop{ background:none}
     </style>
     
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.13/cr-1.3.2/r-2.1.1/datatables.min.css"/> 
	 <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.13/cr-1.3.2/r-2.1.1/datatables.min.js"></script>
	 
  </head>

  <body style="border:0; overflow-x:hidden">
   
 
