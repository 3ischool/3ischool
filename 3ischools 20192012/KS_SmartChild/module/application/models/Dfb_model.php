<?php
Class Dfb_model extends CI_Model
{
     function __construct(){
        parent::__construct();
    }
	
	/* getting all records from a particular table */
		function model_get_all_records($tableName){	
			$tableName = (string)$tableName;	 
			$this->db->from($tableName);			
			$query = $this->db->get();            
			return $query->result_array();	
		}
	/* //getting all records from a particular table */
	
	
	/* getting data against a particular column table */
		function model_get_by_col($tableName,$col,$colValue){	
		  
			$this->db->from($tableName);
            $this->db->where($col,$colValue);
			$query = $this->db->get();	
            
			return $query->result_array();	
		}
	/* //getting data against a particular column table */
	
	/* Insert data in table */
	function insert($data, $table){
		$this->db->insert($table,$data);
		$error = $this->db->error();
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = $this->db->insert_id();
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to insert data</div>';
		}
		 
		return $rData;
	}
	/* //Insert data in table */
	
    
    /* update data in table */
	function update($ID, $data, $colum_name, $table){
        $colum_name = (string)$colum_name;
        $ID = (int)$ID;
		$this->db->where($colum_name, $ID);
		$this->db->update($table, $data);
		$error = $this->db->error();
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = 1;
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to update data</div>';
		}
		 
		return $rData;
    }
	/* //update data in table */
	
	 function delete($ID, $colum_name, $table){
        $colum_name = (string)$colum_name;
        $ID = (int)$ID;    
		$this->db->where($colum_name, $ID);
		$this->db->delete($table);
		$error = $this->db->error(); 
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = '<div class="alert alert-success">Record deleted!!</div>';
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] Unable to delete data</div>';
		}
		 
		return $rData;
	}
	
	 
	
	function model_get_validations_of_a_col($col_id){
		$this->db->select('dfb_form_col_validation.ControlValidationValue,dfb_column_validation.*');
		$this->db->join('dfb_column_validation', 'dfb_column_validation.ColumnValidationId = dfb_form_col_validation.ControlValidationId');
		$this->db->from('dfb_form_col_validation');
		$this->db->where('dfb_form_col_validation.FormColumnId',$col_id);		
		$query = $this->db->get();		
		return $query->result_array();
	}
	
	 
    
    
	 
	
	
	
	function get_col_validation($col_id,$val_id){
		$this->db->from('dfb_form_col_validation');
            $this->db->where('FormColumnId',$col_id);
			$this->db->where('ControlValidationId',$val_id);
			$query = $this->db->get();           
			return $query->result_array(); 
	} 
	
	
	function get_primary_key_with_tableID($tableID){
		$this->db->from('dfb_form_column');
            $this->db->where('FormTableId',$tableID);
			$this->db->where('IsPrimary',1);
			$query = $this->db->get();           
			return $query->result_array(); 
	} 
	
	
	
	function get_all_with_ref($tableName,$refs){  
		 $tableName = (string)$tableName; 
		  
		 
		$selectRefCol = " ";
		if(!empty($refs)){
		
		foreach($refs as $ref){
			$refTable = (string)$ref['RefTable'];
			$refTitile = (string)$ref['RefTitle'];
			$refPK = (string)$ref['RefPK'];
			$currentTablePK = (string)$ref['currentTablePK'];
			 $this->db->join($refTable, $tableName.".".$currentTablePK." = ".$refTable.".".$refPK);
			 $selectRefCol = $selectRefCol.",".$refTable.".".$refTitile;
		}
		}	       
         
	 
		$this->db->select($tableName.'.*'.$selectRefCol);
		$this->db->from($tableName);
		
		
		 
			
		$query = $this->db->get();
		 
		//print_r($this->db->error()); exit; 
		return $query->result_array();
    }
	
	
	/* getting column of table with position index asc */
		function get_sorted_cols($id){	
		  
			$this->db->from('dfb_form_column');
            $this->db->where('FormTableId',$id);
			$this->db->order_by('PositionIndex','asc');
			$query = $this->db->get();	
            
			return $query->result_array();	
		}
	/* //getting column of table with position index asc */
	
		
}	
?>
