<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('dfb_model');		 
		$this->load->library('pagination'); 
	}
	
	function _validate_login($tableName){		
		$arrayData = $this->_validate_permissions($tableName);
		  
		if(!empty($arrayData)){	 
			  
			if($arrayData['status']=="success" && $arrayData['code']==200){
				return $arrayData['permissions'];
			}else if($arrayData['status']=="failed" && $arrayData['code']!=200){
				 echo "<h1>HTTP Error Code: ".$arrayData['code']."</h1>";
				 exit; 
			}else{
				 
				redirect('form/access_denied/', 'refresh');	
				exit;
			}
		}else{
				echo "API Not Responding!!!";
		}
	}
	
	function access_denied(){
		echo "<h1>Access Denied!!!</h1>";
		}
	 
	public function forms()
	{
		$tableName = trim(base64d(trim($this->uri->segment(3))));
		$tableName = (string)$tableName;
		$data['perms'] = $this->_validate_login($tableName);
		
		if(!ctype_alnum($tableName)){
			echo "Master source not defined!!! Please contact administrator";
            exit;
		}
		 
        if($tableName==''){
            echo "Master source not defined!!! Please contact administrator";
            exit;
        }
		$tblData = $this->dfb_model->model_get_by_col('dfb_form_table','FormTable',$tableName);
		
		if(empty($tblData)){		
			echo "404! Page not found";
			exit;
		}
		if($tblData[0]['IsActive']==0){
		 	echo "Form is inactive";
			exit;
		}
		$tableID = $tblData[0]['FormTableId'];
		$data['FormTitle'] = $tblData[0]['FormTitle'];
		$data['ColCount'] = $tblData[0]['ColCount'];
		$data['table_structure'] =  $this->dfb_model->get_sorted_cols($tableID);
		$data['tableName'] = $tableName;		
		    
		 			
		$this->load->view('dfb/manage_masters',$data);
	}
	
	 
	
	 
	 
	
	function add_master(){
		$tableName = trim(base64d(trim($this->uri->segment(3))));
		$tableName = (string)$tableName;
		$perms = $this->_validate_login($tableName);
		if (in_array("Add Record", $perms)){}else{
			echo "Access Denied!!! Please contact administrator";
            exit;
			}
			  
        if($tableName==''){
            echo "Master source not defined!!! Please contact administrator";
            exit;
        }
		$tblData = $this->dfb_model->model_get_by_col('dfb_form_table','FormTable',$tableName);
		if(empty($tblData)){		
			echo "404! Page not found";
			exit;
		}
		if($tblData[0]['IsActive']==0){
		 	echo "Form is inactive";
			exit;
		}
		$tableID = $tblData[0]['FormTableId'];
		 
		$table_structure =  $this->dfb_model->get_sorted_cols($tableID); 
              
        
        if(!empty($table_structure)){ 
		 
		 
		foreach($table_structure as $table_validation){
			$col_id = $table_validation['FormColumnId'];
			$col_name = $table_validation['FormColumn'];
			$col_label = $table_validation['ColLabel'];
			 
			if($table_validation['ShowInForm']==1){
			$validationString = "trim";
			 if(trim($table_validation['IsUnique'])==1){
				$validationString = $validationString."|is_unique[".$tableName.".".$col_name."]";
			}
			 
			 
			$addedValidation = $this->dfb_model->model_get_validations_of_a_col($col_id);
			if(!empty($addedValidation)){
				
				foreach($addedValidation as $addedValidation){
					$val = '';
					if($addedValidation['IsValue']==1){
						$val = "[".$addedValidation['ControlValidationValue']."]";
				    }
					$validationString = $validationString."|".$addedValidation['ColumnValidation'].$val;
				}
						 
				
			}
        	 
			 $this->form_validation->set_rules($col_name, $col_label, $validationString);
			 }
			  
			 
		}
		 
		 
		        
		if ($this->form_validation->run() == FALSE) {
			 
			$errors = validation_errors();
			$errors = str_replace("<p>","<li>",$errors);
			$errors = str_replace("</p>","</li>",$errors);
			$errors = '<div class="alert alert-danger"><ul>'.$errors.'</ul></div>';
			echo $errors;
		}
		else { 
			
			$dataArray = array();
			foreach($table_structure as $table_data){
        	 
			$col_name = $table_data['FormColumn'];
			 
			if($table_data['ShowInForm']==1){
			 	$dataArray[$col_name] = trim($this->input->post($col_name)) ;
			 }
			}
			 
			$insert_status = $this->dfb_model->insert($dataArray,$tableName);
			
			if(is_numeric($insert_status)){ 
				$this->session->set_flashdata('msg', '<div class="alert alert-success">Record added!!</div>');	
			}
			echo $insert_status; 	 
			 
			
		}
		}else{
			echo "Master source not defined!!! Please contact administrator";
            exit;
		}
	}
	
	
	 
	function edit_master(){
		$tableName = trim(base64d(trim($this->uri->segment(3))));
		$tableName = (string)$tableName;
		
		$perms = $this->_validate_login($tableName);
		if (in_array("Edit Record", $perms)){}else{
			echo "Access Denied!!! Please contact administrator";
            exit;
			}
		
		$recordID = trim(base64d(trim($this->uri->segment(4))));
		$recordID = (int)$recordID;
		
        if($tableName==''){
            echo "Master source not defined!!! Please contact administrator";
            exit;
        }
		$tblData = $this->dfb_model->model_get_by_col('dfb_form_table','FormTable',$tableName);
		if(empty($tblData)){		
			echo "404! Page not found";
			exit;
		}
		if($tblData[0]['IsActive']==0){
		 	echo "Form is inactive";
			exit;
		}
		$tableID = $tblData[0]['FormTableId'];
		 
		$table_structure =  $this->dfb_model->get_sorted_cols($tableID);  
		$pk_data =  $this->dfb_model->get_primary_key_with_tableID($tableID);
		 
		$primaryKey = "na";
		$getById = array();
		if(!empty($pk_data)){
			$primaryKey = $pk_data[0]['FormColumn'];
			$getById = $this->dfb_model->model_get_by_col($tableName,$primaryKey,$recordID);
		}
		 
        
		
		  
        
        if(!empty($table_structure) && !empty($getById)){ 
		 
		 foreach($table_structure as $table_validation){
			$col_id = $table_validation['FormColumnId'];
			$col_name = $table_validation['FormColumn'];
			$col_label = $table_validation['ColLabel'];
			 
			if($table_validation['ShowInForm']==1){
			 $validationString = "trim";
			 if(trim($table_validation['IsUnique'])==1){
				if($this->input->post($col_name) == $getById[0][$col_name]){
					 
				}else{
					$validationString = $validationString."|is_unique[".$tableName.".".$col_name."]"; 
				}
			}
			
			
			 
			 
			$addedValidation = $this->dfb_model->model_get_validations_of_a_col($col_id);
			if(!empty($addedValidation)){
				
				foreach($addedValidation as $addedValidation){
					$val = '';
					if($addedValidation['IsValue']==1){
						$val = "[".$addedValidation['ControlValidationValue']."]";
				    }
					$validationString = $validationString."|".$addedValidation['ColumnValidation'].$val;
				}
						 
				
			}
			
			 
        	 
			 $this->form_validation->set_rules($col_name, $col_label, $validationString);
			 }
			  
			 
		}
		 
		 
		        
		if ($this->form_validation->run() == FALSE) {
			 
			$errors = validation_errors();
			$errors = str_replace("<p>","<li>",$errors);
			$errors = str_replace("</p>","</li>",$errors);
			$errors = '<div class="alert alert-danger"><ul>'.$errors.'</ul></div>';
			echo $errors;
		}
		else { 
			
			$dataArray = array();
			foreach($table_structure as $table_data){
        	 
			$col_name = $table_data['FormColumn'];
			 
			if($table_data['ShowInForm']==1){
			 	$dataArray[$col_name] = trim($this->input->post($col_name)) ;
			 }
			}
			 
			$update_status = $this->dfb_model->update($recordID, $dataArray, $primaryKey, $tableName);
			 if($update_status==1){
			 $this->session->set_flashdata('msg', '<div class="alert alert-success">Record updated!!</div>');
			 }
			echo $update_status; 	 
			 
			
		}
		}else{
			echo "Master source not defined!!! Please contact administrator";
            exit;
		}
	} 
	
	function delete_master(){
		$tableName = trim(base64d(trim($this->uri->segment(3))));
		$tableName = (string)$tableName;
		$perms = $this->_validate_login($tableName);
		if (in_array("Delete Record", $perms)){}else{
			echo "Access Denied!!! Please contact administrator";
            exit;
			}
		$recordID = trim(base64d(trim($this->uri->segment(4))));
		$recordID = (int)$recordID;
         
        if($tableName==''){
            echo "Master source not defined!!! Please contact administrator";
            exit;
        }
		$tblData = $this->dfb_model->model_get_by_col('dfb_form_table','FormTable',$tableName);
		if(empty($tblData)){		
			echo "404! Page not found";
			exit;
		}
		if($tblData[0]['IsActive']==0){
		 	echo "Form is inactive";
			exit;
		}
		$tableID = $tblData[0]['FormTableId'];
		 
		$table_structure =  $this->dfb_model->get_primary_key_with_tableID($tableID);
		 
		$primaryKey = "na";
		$getById = array();
		if(!empty($table_structure)){
			$primaryKey = $table_structure[0]['FormColumn'];
			$getById = $this->dfb_model->model_get_by_col($tableName,$primaryKey,$recordID);
		}
		
		if(empty($getById)){
			echo "record does not exist or is deleted";
			exit;
		}
		
		$delete_status = $this->dfb_model->delete($recordID,$primaryKey,$tableName);
		$this->session->set_flashdata('msg', $delete_status);
		redirect('form/forms/'.base64e($tableName), 'refresh');
		
	}
	
	
	
	function get_edit_data(){
		$tableName = trim(base64d(trim($this->uri->segment(3))));
		$tableName = (string)$tableName;
		
		$recordID = trim(base64d(trim($this->uri->segment(4))));
		$recordID = (int)$recordID;
        if($tableName==''){
            echo "Master source not defined!!! Please contact administrator";
            exit;
        }
		$tblData = $this->dfb_model->model_get_by_col('dfb_form_table','FormTable',$tableName);
		if(empty($tblData)){		
			echo "404! Page not found";
			exit;
		}
		if($tblData[0]['IsActive']==0){
		 	echo "Form is inactive";
			exit;
		}
		
		 
		
		$tableID = $tblData[0]['FormTableId'];
		 
		$table_structure =  $this->dfb_model->get_primary_key_with_tableID($tableID);
		 
		$primaryKey = "na";
		$getById = array();
		if(!empty($table_structure)){
			$primaryKey = $table_structure[0]['FormColumn'];
			$getById = $this->dfb_model->model_get_by_col($tableName,$primaryKey,$recordID);
		}
		 
        
		echo json_encode($getById);
	}
	
	
	 
	function update_description(){
		$desc = "test Desc";
		$tableName = $this->uri->segment(3);
		$colName = $this->uri->segment(4);
		$this->form_validation->set_rules('fl', 'Label', 'trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('fhc', 'HTML Control', 'trim|required|alpha_numeric');
		$this->form_validation->set_rules('fv', 'Validation', 'trim|required');
		$this->form_validation->set_rules('fiu', 'Unique', 'trim|numeric');
		$this->form_validation->set_rules('fsf', 'Show in form', 'trim|numeric');
		$this->form_validation->set_rules('fsg', 'Show in grid', 'trim|numeric');
		$this->form_validation->set_rules('fs', 'Sorting', 'trim|required|numeric');
		if ($this->form_validation->run() == FALSE) {
			 
			$errors = validation_errors();
			$errors = str_replace("<p>","<li>",$errors);
			$errors = str_replace("</p>","</li>",$errors);
			$errors = '<div class="alert alert-danger"><ul>'.$errors.'</ul></div>';
			echo $errors;
		}else{
		 	$fl = $this->input->post('fl');
			$fhc = $this->input->post('fhc');
			$fv = $this->input->post('fv');			
			$fs = $this->input->post('fs');
			
			$fiu = $this->input->post('fiu');
			if($fiu==1){
				$fiu = "is_unique[".$tableName.".".$colName."]";
			}
			else{
				$fiu = "na";
			}
			
			$fsf = $this->input->post('fsf');
			if($fsf==1){
				$fsf = "show";
			}
			else{
				$fsf = "hide";
			}
			
			$fsg = $this->input->post('fsg');
			if($fsg==1){
				$fsg = "show";
			}
			else{
				$fsg = "hide";
			}
			
			
			$desc = $fl.":".$fhc.":".$fv.":".$fiu.":".$fsf.":".$fsg.":".$fs;
			
			 
			$test = set_col_desc($desc,$tableName,$colName);
			if($test=="00000" || $test==00000){
				$rData = '<div class="alert alert-success">Updated!!</div>';
			}
			else{
				$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] Unable to update data</div>';
			}
			
			echo $rData;
		}
		 
		 
		
		 
	}
	
	
	function validate_time(){
		if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
			$tm = $this->input->post('tm');
			$tm = explode(":",$tm);
				if(count($tm)==2){
					$p1 = $tm[0];
					$p2 = $tm[1];
					if(($p1>=0 && $p1<=23) && ($p2>=0 && $p2<=59)){
							echo 1;
					}else{
							echo 2;
					}
				}else{
						echo 2;
				}
			}else{
				echo 2;
				}
				exit;
		}
		
		
		
		function _generate_token(){
			$TokenDefaultValue = "KSP";
			$TokenDateValue = date('d')+date('m'); //Adding current date and month
			$TokenSeprator = "#";
			$TokenTimeValue = date('H')+date('i'); //Adding current hour and minutes
			$FullToken = $TokenDefaultValue.$TokenDateValue.$TokenSeprator.$TokenTimeValue;
			return _encryption_for_dotnet_api($FullToken);
		}
		
		function _curl_request_handler($curlUrl,$PostArray){
			// Get cURL resource
			$curl = curl_init();
			// Set some options - we are passing in a useragent too here
			curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $curlUrl,
				CURLOPT_USERAGENT => 'KonnectSmart PHP App',
				CURLOPT_POST => 1,
				CURLOPT_POSTFIELDS => $PostArray,
			));
			// Send the request & save response to $resp
			$resp = curl_exec($curl);
			// Close request to clear up some resources
			$http_status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			curl_close($curl);
			$http_status_code = (int)$http_status_code;
			  $resp1 = array();
			  $resp = json_decode($resp, true);
			 if($http_status_code!=200){
				$resp['status'] = 'failed';
				$resp['message'] = 'Error';				
				$resp['code'] = $http_status_code;
			}else{
				
				$resp['code'] = $http_status_code;
				}
			return $resp;
		}
		
		function _validate_permissions($tableName){
			 
			$PostToken = $this->_generate_token();  
			$curlUrl = "http://172.16.1.2/KSChild/API/CheckUserAuth";
			$tableName = $tableName;
			$AuthKey = $this->input->cookie('_ASPXAUTH', TRUE);  
			$PostArray = array(
				'Token' => $PostToken,
				'TableName' => $tableName,
				'AuthKey' => $AuthKey,
			);
			 
			$result = $this->_curl_request_handler($curlUrl,$PostArray);
			 
			return $result;
		}
		
		
		
	
	 
}
