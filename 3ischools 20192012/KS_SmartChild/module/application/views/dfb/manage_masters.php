<?php $this->load->view('dfb/header'); 
$actionColumn = 0;
if (in_array("Edit Record", $perms) || in_array("Delete Record", $perms)){
$actionColumn = 1;	
}

?>
<!-- Page Heading -->
    <div class="row top-space-10 page-heading">
    	<div class="col-xs-6">
        	<h1>Manage <?php echo $FormTitle;?></h2>
        </div>
       <?php if (in_array("Add Record", $perms)){?>
        <div class="col-xs-6 text-right">
        	<a  href="javascript:void(0)" onclick="openModel('<?php echo site_url('form/add_master/'.base64e($tableName).'/');?>')"  class="btn btn-xs btn-default">Add New</a> 
        </div>
        <?php } ?>
    </div>
    <!-- //Page Heading -->

<div id="formError"><?php echo $this->session->flashdata('msg'); ?> </div>
<!-- filter -->
<div class="well well-sm top-space-10" id="mainfrm" style="display:none;">
<div class="row">
    	 
       <form method="post" action="<?php echo site_url('form/add_master/'.base64e($tableName).'/');?>" id="masterForm" onsubmit="return submitData()">      
          <?php 
		   
		  if(!isset($ColCount) && $ColCount==''){
		  	 $ColCount = 3;	
		  }
		  $colSize = (int)(12/$ColCount);
	$htmlForm = "";
	$scriptArray = array();
	$sa =0;
	 //print_r($table_structure); exit;	 
	foreach($table_structure as $table_structure_form){
	$scriptArray[$sa] = $table_structure_form['FormColumn'];
	$sa++;
		 
		 $isIdentity = $table_structure_form['IsPrimary'];
		 $ColName = $table_structure_form['FormColumn'];
		 $HTMLControlId = $table_structure_form['HTMLControlId'];
		 $show_in_form = $table_structure_form['ShowInForm'];
		 $fieldLabel = $table_structure_form['ColLabel'];		 
		 $ReferenceTableName = $table_structure_form['ForeignTableName'];
		 $ReferencePK = $table_structure_form['ForeignPrimaryCol'];
		 $ReferenceTitle = $table_structure_form['ForeignTitleCol'];	 
		 $html_control = '';
		 $colWidth = $table_structure_form['ColWidth'];
		 $htmlControlData = $this->dfb_model->model_get_by_col('dfb_html_control','HTMLControlId',$HTMLControlId);
		 if(!empty($htmlControlData)){
		 	$html_control = $htmlControlData[0]['HTMLControl'];
		 }
		  
		 if($ColName != "" && $html_control != "" && $show_in_form == 1 && $fieldLabel != ""){
		 
		 if($isIdentity!=1 ){
		  $colSizeUpdated = $colSize*$colWidth;
		 $htmlForm = $htmlForm.'<div class="col-sm-'.$colSizeUpdated.'">';
		 
         $htmlForm = $htmlForm.'<label>'.$fieldLabel.'</label><br />';
		 
		 if($html_control=="textbox"){ 
		 $htmlForm = $htmlForm.'<input type="text" class="form-control frmdta" name="'.$ColName.'" id="get_'.$ColName.'" placeholder="'.$fieldLabel.'">';
          }else if($html_control=="checkbox"){ 
		  $val = 'true';
		 $htmlForm = $htmlForm.'<input type="checkbox" class="frmdta" name="'.$ColName.'" id="get_'.$ColName.'" value="'.$val.'">';
          }else if($html_control=="textarea"){ 
          $htmlForm = $htmlForm.'<textarea class="form-control frmdta" name="'.$ColName.'" id="get_'.$ColName.'" placeholder="'.$fieldLabel.'"></textarea>';
         
		 }
		 else if($html_control=="date"){ 
          $htmlForm = $htmlForm.'<input type="text" class="form-control frmdta datepicker" readonly="readonly" name="'.$ColName.'" id="get_'.$ColName.'" placeholder="'.$fieldLabel.'">';
         
		 }
		 else if($html_control=="time"){ 
          $htmlForm = $htmlForm.'<input type="text" class="form-control frmdta timepicker" name="'.$ColName.'" id="get_'.$ColName.'" placeholder="'.$fieldLabel.'">';
         
		 }
		 else if($html_control=="selectbox"){
			if($ReferenceTableName!='' && $ReferencePK!=''){
			$ReferenceTitleColumnName = $ReferenceTitle;
			$fk_feild_data = $this->dfb_model->get_all_with_ref($ReferenceTableName,array());
			 
            $htmlForm = $htmlForm.'<select class="form-control frmdta" name="'.$ColName.'"  id="get_'.$ColName.'">
            	<option value="">Select '.$fieldLabel.'</option>';
				
                 foreach($fk_feild_data as $fk_feild_data){ 
                $htmlForm = $htmlForm.'<option value="'.$fk_feild_data[$ReferencePK].'">'.$fk_feild_data[$ReferenceTitleColumnName].'</option>';
               } 
             $htmlForm = $htmlForm.'</select>';
            }} 
          $htmlForm = $htmlForm.'</div>';
		  
		    
          }}}
		 
		  echo $htmlForm; 
		  
		   ?> 
           
          	<div class="col-sm-3">
				<label style="color:#fff">S</label><br>
            	<input type="submit" value="add record" class="btn btn-success" id="saveBtn" />
                <a href="javascript:clodeModel()" class="btn btn-default">close</a>
            </div>
                  
        </form>
         
    </div>
</div>
<!-- //filter -->
 
 
<div class="row top-space-20">
    	<div class="col-md-12">
       <?php 
    
	   $i=0;
	   $html ="";
	   $primaryKey = '';
	   $colFieldLabelArray = array();
	   $colFieldArray = array();
	   $colRelationArray = array();
	   
	    //print_r($table_structure); exit;
	    
	foreach($table_structure as $table_structure_grid){
		 
			 
			 $ColName = $table_structure_grid['FormColumn'];		  
			 $show_in_grid = $table_structure_grid['ShowInGrid'];
			 $fieldLabel = $table_structure_grid['ColLabel'];		 
			 $ReferenceTableName = $table_structure_grid['ForeignTableName'];
			 $ReferenceTablePK = $table_structure_grid['ForeignPrimaryCol'];	
			 $ReferenceTableTitle = $table_structure_grid['ForeignTitleCol'];
			 $IsForeign = $table_structure_grid['IsForeign'];
			 $isPrimary = $table_structure_grid['IsPrimary'];	 
			 $HTMLControlId = $table_structure_grid['HTMLControlId'];
			 $htmlControlData = $this->dfb_model->model_get_by_col('dfb_html_control','HTMLControlId',$HTMLControlId);
			 $html_control =  "";
			 if(!empty($htmlControlData)){
				$html_control = $htmlControlData[0]['HTMLControl'];
			 } 
			 
			 if($isPrimary==1){
				$primaryKey = $ColName;
				}
			 
			 
			 
		 	 if($fieldLabel!='' && $ColName!=='' && $show_in_grid==1 && $HTMLControlId!=''){
				$colFieldLabelArray[$i] = $fieldLabel;
				
				if($IsForeign==1){
					$colFieldArray[$i] = $ReferenceTableTitle.'#'.$html_control;
					$colRelationArray[$i]['RefTable'] = $ReferenceTableName;
					$colRelationArray[$i]['RefPK'] = $ReferenceTablePK;
					$colRelationArray[$i]['RefTitle'] = $ReferenceTableTitle;
					$colRelationArray[$i]['currentTablePK'] = $ColName;
				}else{
					$colFieldArray[$i] = $ColName.'#'.$html_control;
				}
				
				 
				$i++;
			}
		   
		}
		 
		if(!empty($colFieldLabelArray)){
		 	 
		$result = $this->dfb_model->get_all_with_ref($tableName,$colRelationArray);
		if(!empty($result)){
		$html = $html.'<table class="table table-bordered table-striped" id="mainGrid">';
		
		$html = $html.'<thead><tr>';
		for($y=0; $y<count($colFieldLabelArray); $y++){
			$html = $html.'<td>'.$colFieldLabelArray[$y].'</td>';
		}
		if($actionColumn==1){
			$html = $html.'<td class="text-right no-sort" width="50">Action</td>';
		}
		$html = $html.'</thead></tr>';
		$html = $html.'<tbody>';
		foreach($result as $result){
		
		$html = $html.'<tr>'; 
		for($z=0; $z<count($colFieldArray); $z++){
			$fieldNames = explode('#',$colFieldArray[$z]); 
			
			$val = $result[$fieldNames[0]];
			$fType = '';
			if(count($fieldNames)==2){
				$fType = $fieldNames[1];
			}
			
			if(strtolower($fType)=='time'){
					$val = date("Y-m-d")." ".$val;
					$val = date('h:i a',strtotime($val));
			}
			else if(strtolower($fType)=='checkbox'){
					if($val == 1){
					$val = "True";	
					}else{
						$val = "False";
						}
			}
			
			 
			 
			$html = $html.'<td>'.$val.'</td>';
		}
        $extraBtn =""; 
        if($actionColumn==1){    
			$html = $html.'<td class="text-center">'.$extraBtn.'';
			if (in_array("Edit Record", $perms)){
				$html = $html.'<a href="javascript:void()" data-id="'.base64e($result[$primaryKey]).'" class="edit-master-record btn btn-default btn-xs"><i class="fa fa-pencil"></i></a> |';
			}
			if (in_array("Delete Record", $perms)){
				$html = $html.'<a href="'.site_url('form/delete_master/'.base64e($tableName).'/'.base64e($result[$primaryKey])).'" class="btn btn-default btn-xs" onclick="return confirm(\'Are you sure?? The data will not be recovered after deletion\')"><i class="fa fa-trash"></i></a>';
			}
			$html = $html.'</td>';
		}	
		$html = $html.'</tr>';
		}
		$html = $html.'</tbody>';
		$html = $html.'</table>';
		}else{
		echo '<div class="alert alert-danger">No record found...</div>';
		}
		}else{
			echo '<div class="alert alert-danger">Problem in form setup!!! Please contact administrator.</div>';
		} 
		 if (in_array("View Record", $perms)){
			 echo  $html; 
			 }
		  

?>
        
        	
</div>
</div>
<!-- //Data Container -->
<script type="text/javascript">
function submitData(){
	    $('#form_loader').show();
		var form=$('#masterForm');
		$.post( form.attr("action"),form.serialize(), function( data ) {
		  if(Math.floor(data) == data && $.isNumeric(data)){
		  	window.location.href = "<?php echo current_url();?>";
		  }
		  else{
		  	$('#formError').html(data);
		  	docHeight();
			
		  }
		  $('#form_loader').hide();
		});
		 
		
		return false;
	}
</script>


 
 
<script>
    
	
	$(document).on('click','.edit-master-record',function(e){
		e.preventDefault();
		$('#form_loader').show();
		var record = $(this);
		var idd = record.attr('data-id');
		var urll = "<?php echo site_url();?>/form/get_edit_data/<?php echo base64e($tableName);?>/"+idd;
		
		var form_urll = "<?php echo site_url();?>/form/edit_master/<?php echo base64e($tableName);?>/"+idd;
		
		
		 
		$.get( urll, function( data ) {
			$('#masterForm').attr('action' , form_urll); 
		  $('#mainfrm').slideDown(); 
		  $('#saveBtn').val('update record');
		  $('#form_loader').hide();
		  var obj = jQuery.parseJSON(data);
		   
		   <?php for($s=0; $s<count($scriptArray); $s++){
		   	$scriptColName = $scriptArray[$s];
		   ?>
		   
		  if ($("#get_<?php echo $scriptColName;?>").length === 0){
				 
		  }else{
		  	 var inputType = $("#get_<?php echo $scriptColName;?>").attr('type');
			 var fieldValue = obj[0].<?php echo $scriptColName;?>;
			 if(inputType=="checkbox"){
			 	if(fieldValue==1){
					$("#get_<?php echo $scriptColName;?>").prop("checked" , "checked");
				}else{
					$("#get_<?php echo $scriptColName;?>").removeAttr("checked");
				}
			 }
			 else{
		  	 	$("#get_<?php echo $scriptColName;?>").val(fieldValue);
			 }
		  }
		  <?php } ?>
		   docHeight();
			
		}).done(function() {
		$('.timepicker').each(function(){
			var val = $(this).val();
			val = val.substr(0, 5);
			$(this).val(val);
			});
	  });
		 
		 
	});  
	 	
		
		function openModel(formUrl){
		$('#masterForm').attr('action' , formUrl);
		document.getElementById("masterForm").reset();
		  $('#saveBtn').val('add record');
			$('#mainfrm').slideDown("slow",function(){
				docHeight();
				});
			
		}
		
		function clodeModel(){
			$('#masterForm').attr('action' , '');
		  $('#saveBtn').val('not set');
			$('#mainfrm').slideUp("slow",function(){
				docHeight();
				});
		}
</script>

<?php $this->load->view('dfb/footer');?>
