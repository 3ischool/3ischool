</div><!-- /.container -->


<div id="form_loader" style="width:100%; height:100%; background:rgba(255,255,255,0.5); top:0; left:0; position:fixed; color:#FFFFFF; font-size:22px; text-align:center; display:none"> 

	&nbsp;&nbsp;&nbsp;
</div>    
    
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
   
  $('#mainGrid').DataTable();
  $('select').select2();
  
 
})
</script> 
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
   
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.min.js"></script>

  <script>
  $( function(){
   
    docHeight();
     
    $(".timepicker").mask('00:00'); 
    $( ".datepicker" ).datepicker();
	 
	$( ".timepicker" ).blur(function(){
		var obj = $(this);
			var timec = $.trim(obj.val()); 
			if(timec!=''){
		 $.post("<?php echo site_url('form/validate_time');?>",{tm:timec}, function( data ) {
			 var data1 = parseInt(data);
			  
			if(data1=='1'){
				obj.css('border-color' , '#000'); 
			}else{
				obj.css('border-color' , 'red');	
				obj.val('');
				}
			});
		}

        //if (isValid==1) {
          // $(this).css('border-color' , '#ccc');  
        //} else {
		//	$(this).css('border-color' , 'red');	
         //   $(this).val('');
        //}		 
		});
  });
  
	$(document).on("change","#mainGrid_length select",function(){
		
	docHeight();
	})
   
     function docHeight(){
		 var heightN = $('#top-main-cont').height();
		  heightN = parseInt(heightN)+50;
		  parent.MasterFrameHeight(heightN);
		 }
 
  </script>    
  </body>
</html>
