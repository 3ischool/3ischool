<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Html</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">     
	<link href="<?php echo base_url('assets/css/styles.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/maroon-theme.css');?>" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>     
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    
     
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datatables.min.css');?>"/> 
	 <script type="text/javascript" src="<?php echo base_url('assets/js/datatables.min.js');?>"></script>

    <style>
		.no-sort::after { display: none!important; }

.no-sort { pointer-events: none!important; cursor: default!important; }
    </style>
     
    
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
     
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
     <link href="<?php echo base_url('assets/select2/css/select2.min.css');?>" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url('assets/select2/js/select2.min.js');?>"></script> 
  </head>

  <body style="border-top:0">
  	  	
     <div class="container-fluid" id="top-main-cont" style="padding:0">
