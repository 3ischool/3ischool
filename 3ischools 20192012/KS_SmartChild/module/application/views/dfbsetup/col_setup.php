<?php include('header.php');
 $tableID=$this->uri->segment(4);
 $form_builder_error = '';
?>
<div id="form_builder_error">

</div>
<style>
.desc-update .pull-left{ margin-right:15px;}
.desc-update .pull-left input[type=text], .desc-update .pull-left select{ height:30px;}
</style>
<!-- Page Heading -->
    <div class="row top-space-10 page-heading">
    	<div class="col-md-4">
        	<h1>Column Setup <span style="font-size:12px">[<?php echo $table_result[0]['FormTable'];?>]</span></h1>
        </div>
        <div class="col-md-8 text-right">
        	  
             
            	Master Name&nbsp;:&nbsp;<input type="text" id="masterNname" value="<?php echo $table_result[0]['FormTitle'];?>" style="width:200px; text-align:center;" />&nbsp;
                Columns Count&nbsp;:&nbsp;
                <select id="colCount" style="width:50px;">
                	<option <?php if($table_result[0]['ColCount']==1){?> selected="selected"<?php } ?>>1</option>
                    <option <?php if($table_result[0]['ColCount']==2){?> selected="selected"<?php } ?>>2</option>
                    <option <?php if($table_result[0]['ColCount']==3){?> selected="selected"<?php } ?>>3</option>
                    <option <?php if($table_result[0]['ColCount']==4){?> selected="selected"<?php } ?>>4</option>
                    <option <?php if($table_result[0]['ColCount']==6){?> selected="selected"<?php } ?>>6</option>
                </select>
                <a href="javascript:void" onclick="saveMasterName()" class="btn btn-default btn-xs"><i class="fa fa-save"></i></a>
                <a href="<?php echo site_url('form/forms/'.base64e($this->uri->segment(3)));?>" target="_blank" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a> 
            
        </div> 
    </div>
    <!-- //Page Heading -->

<?php if($error!=''){
    ?>
    <div class="alert alert-danger"><?php echo $error;?></div>
    <?php 
}?>
  
  <?php  
 if(!empty($col)){?>
 
 
 
 <?php 
 
 foreach($col as $col){
 $colId = $col['FormColumnId'];
     
$colName = $col['FormColumn'];     
  
 $colLabel = $col['ColLabel'];
 $htmlControl = $col['HTMLControlId'];
 $validation = "";
 $is_uniq = $col['IsUnique'];
 $show_in_form = $col['ShowInForm'];
 $show_in_grid = $col['ShowInGrid'];
 $sortIndex = $col['PositionIndex'];
     
 $IsPrimary = $col['IsPrimary'];
 $IsForeign = $col['IsForeign'];
 
 $ForeignPrimaryCol = $col['ForeignPrimaryCol'];
 $ForeignTitleCol = $col['ForeignTitleCol'];
 $ForeignTableName = $col['ForeignTableName']; 
 $colSize = $col['ColWidth'];      
  
 $validateCol = $this->db->field_exists($colName, $tableName);
 ?>
 <?php if($validateCol==1 || $validateCol==true){?>
 <div id="masterForm_<?php echo $colId;?>_formError"></div>
 <h4><?php echo $colName;?></h4>
 
<div class="well well-sm desc-update">
<form method="post" id="masterForm_<?php echo $colId;?>" action="<?php echo site_url('dfbsetup/update_col/'.$colId);?>" onsubmit="return submitData('#masterForm_<?php echo $colId;?>')">
<input type="hidden" name="ipk" value="<?php echo $IsPrimary;?>" />
 <input type="hidden" name="ifn" value="<?php echo $IsForeign;?>" />
 <input type="hidden" name="phc" id="phc" value="<?php echo $htmlControl;?>" />
	<div class="pull-left">
    	<label>Label</label><br />
        <input type="text" style="width:120px;" name="fl" value="<?php echo $colLabel;?>" />
    </div>
    <?php
	$htmlData = $htmlControl3;
	if($IsPrimary==1){
	$htmlData = $htmlControl1;
	}
	if($IsForeign==1){
	$htmlData = $htmlControl2;
	}
	 if(!empty($htmlData)){
	?>
    <div class="pull-left" >
    	<label>HTML Control</label><br />
        	<select  name="fhc" onchange="htmlControlChange(this.value,'<?php echo $htmlControl;?>')">
            <?php foreach($htmlData as $NEWhtmlControl1){?>
            	<option <?php if($htmlControl==$NEWhtmlControl1['HTMLControlId']){?> selected="selected"<?php }?> value="<?php echo $NEWhtmlControl1['HTMLControlId'];?>"><?php echo $NEWhtmlControl1['HTMLControl'];?></option>                
               <?php } ?> 
            </select>
    </div>
    <?php }?>
     
    
   <?php if($IsPrimary==0 && $IsForeign==0){?> <div class="pull-left" title="is unique">
    <label>IU</label> <br />
    <input type="checkbox" <?php if($is_uniq==1){?> checked="checked"<?php } ?> value="1"  name="fiu" />
    </div>
    <?php }?>
  <?php if($IsPrimary==0){?>  <div class="pull-left" title="show in form" >
    <label>SF</label><br /> 
    <input type="checkbox" <?php if($show_in_form==1){?> checked="checked"<?php } ?> value="1" name="fsf" />
    </div>
    <?php }?>
    
    <div class="pull-left" title="show in grid">
    <label>SG</label><br /> 
    <input type="checkbox" <?php if($show_in_grid==1){?> checked="checked"<?php } ?> value="1"  name="fsg" />
    </div>
    
    <div class="pull-left">
    <label>Sorting</label><br />
        <input type="text" style="width:50px" value="<?php echo $sortIndex;?>"  name="fs" />
    </div>
    <?php if($IsForeign==1){?>
    <div class="pull-left">
    <label>Relation Table</label><br />
        <?php echo $ForeignTableName;?>
        
    </div>
    
    <div class="pull-left">
    <label>Relation PK</label><br />
        <?php echo $ForeignPrimaryCol;?>
    </div>
    <div class="pull-left">
    <label>Relation Title</label><br />
        <?php $fCols = $this->db->list_fields($ForeignTableName);?>
        <select  name="frc">
            <?php for($x=0; $x<count($fCols); $x++){?>
            	<option <?php if($ForeignTitleCol==$fCols[$x]){?> selected="selected"<?php }?>><?php echo $fCols[$x];?></option>                
               <?php } ?> 
            </select>
    </div>
    <?php } ?>
    
     <?php if($IsPrimary==0){?>  <div class="pull-left" title="show in form" >
    <label>Col Size</label><br /> 
    	<select name="colSize">
                	<option <?php if($colSize==1){?> selected="selected"<?php } ?> value="1">Single</option>
                    <option <?php if($colSize==2){?> selected="selected"<?php } ?> value="2">Double</option>
                    <option <?php if($colSize==3){?> selected="selected"<?php } ?> value="3">Tripple</option> 
                </select>
    </div>
    <?php }?>
    <div class="pull-left">
    <label>&nbsp;</label><br /> 
    <input type="submit" value="save" />
    <?php if($IsPrimary==0){?>
    <a href="javascript:void" onclick="openValidations('Set validations for <?php echo $colName;?>','<?php echo site_url('dfbsetup/add_validations/'.$colId);?>')">Validations</a>
    <?php } ?>
    </div>
     
</form>
<div class="clearfix"></div>
	
</div>
<?php }else{
 
$form_builder_error = $form_builder_error.$colId.',';
}?>

<?php }} 
 
?>
<!-- //Data Container -->
<script type="text/javascript">
function submitData(formID){
	    $('#form_loader').show();
		var form=$(formID);
		 
		$.post( form.attr("action"),form.serialize(), function( data ) {	
		  if(data.includes("alert alert-success")){
		 	 window.location.href = "<?php echo current_url();?>";	
		  }	else{   
			  $(formID+'_formError').html(data);
			  $('#form_loader').hide();
		  }
		});
		 
		
		return false;
	}
	function saveMasterName(){
		$('#form_loader').show();
		 
		var val = $('#masterNname').val();
		var colCount = $('#colCount').val();
		 
		$.post( "<?php echo site_url('dfbsetup/edit_table_title/'.$this->uri->segment(4));?>",{FormTitle : val, ColCount : colCount}, function( data ) {
		  
		  $('#form_builder_error').html(data);
		  
		  $('#form_loader').hide();
		});
	}
 function openValidations(title,uurl){
 	$('#myModalLabel').text(title);
				$('#modelFrame').attr('src' , uurl);
				$('#myModal').modal({
				  keyboard: false,
				  backdrop: 'static'
				})	
 }
 
 function htmlControlChange(selectedID,prevID){
  
 	if(prevID!=''){
	if(selectedID==prevID){
	
	}else{
		alert('changing html control will drop all prev validations');
	} }
 }
 
 <?php $form_builder_error = rtrim($form_builder_error,',');
 if($form_builder_error!=''){
 ?>
 
 function removeColumns(){
 	$('#form-builder-loader').show();
 	$.post( "<?php echo site_url('dfbsetup/removeColumns');?>", { tableName: "<?php echo $tableID;?>", cols: "<?php echo $form_builder_error;?>" }, function( data ) {
  		if(data==1){
			alert('Process completed successfully!!! Now you can continue with your work :)');
		}else{
			alert('Unable to complete the process!!! Please contact administrator :(');
		}
		$('#form-builder-loader').hide();
	});
 }
 <?php 
 }else{?>
 function removeColumns(){}
 <?php }?>
 
 
 
 
 $(document).ready(function(){
 removeColumns();
 });
</script>
<div id="form-builder-loader" style="width:100%; height:100%; position:fixed; top:0; left:0; z-index:9999999; background:rgba(0,0,0,0.5); color:#FFFFFF; font-size:22px; text-align:center; display:none"><br />
<br />
<br />
<br />
<br />

	We are removing extra columns from the table. Please be patient and do not reload/refresh/close the page. The process will take some time.
</div> 
 
 

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Set Validations For</h4>
      </div>
      <div class="modal-body">
       	<iframe id="modelFrame" src="" scrolling="auto" frameborder="0" style="width:100%; height:350px; float:left">
        </iframe>  
        <div class="clearfix"></div>
      </div>
       
    </div>
  </div>
</div>

<?php include('footer.php');?>
