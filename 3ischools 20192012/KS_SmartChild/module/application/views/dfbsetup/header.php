<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Html</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">     
	<link href="<?php echo base_url('assets/css/styles.css');?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/css/maroon-theme.css');?>" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>     
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>   
    
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
     
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
     <style>
     .modal-backdrop{ background:none}
     </style>
     
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.13/cr-1.3.2/r-2.1.1/datatables.min.css"/> 
	 <script type="text/javascript" src="https://cdn.datatables.net/v/bs/dt-1.10.13/cr-1.3.2/r-2.1.1/datatables.min.js"></script>
	<link href="<?php echo base_url('assets/select2/css/select2.min.css');?>" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url('assets/select2/js/select2.min.js');?>"></script> 
  </head>

  <body style="border:0; overflow-x:hidden">
  <nav class="navbar navbar-inverse" style="border-radius:0">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Digitech</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
       	<li><a href="<?php echo site_url('dfbsetup/manage_table_setup');?>">Manage Tables</a></li>  
        
        
        
        
      </ul>
       
      <ul class="nav navbar-nav navbar-right">
         
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> <span class="caret"></span></a>
          <ul class="dropdown-menu">             
            <li><a href="#">Settings</a></li>
            <li><a href="#">Logout</a></li> 
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
     <div class="container">
