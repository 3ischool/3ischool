<?php
Class Common_model extends CI_Model
{
     function __construct(){
        parent::__construct();
    }
	
	function insert($data, $table){
		$this->db->insert($table,$data);
		$error = $this->db->error();
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = '<div class="alert alert-success">Record added!!</div>';
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to insert data</div>';
		}
		 
		return $rData;
	}
	
	function update($ID, $data, $colum_name, $table){
        (string)$colum_name = $colum_name;
        (int)$ID = $ID;
		$this->db->where($colum_name, $ID);
		$this->db->update($table, $data);
		$error = $this->db->error();
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = '<div class="alert alert-success">Record updated!!</div>';
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] unable to update data</div>';
		}
		 
		return $rData;
	}
	
	function delete($ID, $colum_name, $table){
        (string)$colum_name = $colum_name;
        (int)$ID = $ID;    
		$this->db->where($colum_name, $ID);
		$this->db->delete($table);
		$error = $this->db->error(); 
		if($error['code']==00000 && trim($error['message'])==''){
			$rData = '<div class="alert alert-success">Record deleted!!</div>';
		}
		else{
			$rData = '<div class="alert alert-danger">[Error:'.$error['code'].'] Unable to delete data</div>';
		}
		 
		return $rData;
	}
    
    function get_total_for_paging($primaryKeyField,$titleField,$searchValue,$tableName){
        (string)$titleField = $titleField;
        (string)$searchValue = $searchValue;
         
        
        if($titleField!="" && $searchValue!=''){
		$this->db->like($titleField,$searchValue);
		}
        
        $this->db->select($primaryKeyField);
		$this->db->from($tableName);
		$query = $this->db->get();
        return $query->num_rows();
    }
    
    function get_all_with_paging($limit,$offset,$titleField,$searchValue,$tableName){
        (string)$titleField = $titleField;
        (string)$searchValue = $searchValue;
        
        $this->db->select('*'); 

		if($titleField!="" && $searchValue!=''){
		$this->db->like($titleField,$searchValue);
		}

		$this->db->limit($limit,$offset);
		$query = $this->db->get($tableName);
		return $query->result_array();
    }
    
    function get_by_id($primaryKeyField,$id,$tableName){
        (int)$id = $id;
        $this->db->select('*'); 

		 
		$this->db->where($primaryKeyField,$id);
		 
		$query = $this->db->get($tableName);
		return $query->result_array();
    }
		
}	
?>
