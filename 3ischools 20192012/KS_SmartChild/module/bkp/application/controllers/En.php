<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class En extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');		 
		$this->load->library('pagination');
	}
	
	
	
	function _user_id(){
			$usrid =  $this->input->cookie('ui',true);
			$usrid =  $this->encryption->decrypt($usrid);
			return $usrid;
	}
	
	function _user_type(){
			$usrid =  $this->input->cookie('ut',true);
			$usrid =  $this->encryption->decrypt($usrid);
			return $usrid;
	}
	
	function _date_time(){
			return date("Y-m-d H:i:s");
	}
	
	function _user_ip(){
			return $this->input->ip_address();
	}
	
	function _user_agent(){
		$this->load->library('user_agent');
		if ($this->agent->is_browser())
		{
				$agent = $this->agent->browser().' '.$this->agent->version();
		}
		elseif ($this->agent->is_robot())
		{
				$agent = $this->agent->robot();
		}
		elseif ($this->agent->is_mobile())
		{
				$agent = $this->agent->mobile();
		}
		else
		{
				$agent = 'Unidentified User Agent';
		}
		$val = $this->agent->platform()."/".$agent;
		
		return $val;
		}
	 
	 
	 
	 
	 
	 
	
	function _is_localhost() {
    $whitelist = array( '127.0.0.1', '::1' );
    if( in_array( $_SERVER['REMOTE_ADDR'], $whitelist) ){
        return true;
		}
		else{
		return false;
		}
	}
    
    function _bootstrap_paging($paging_base_url,$total_rows,$limit,$paging_uri_segment){
        $config = array();
        $config['base_url'] = $paging_base_url;
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $limit;
		$config["uri_segment"] = $paging_uri_segment;
		
		 //config for bootstrap pagination class integration
        $config['full_tag_open'] = '<ul class="pagination" style="margin:0; padding:0">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        return $config;
    }
	
	function _paging_limit(){
        return 100;
    }
	
	function nav(){
		$tableDataArray = json_decode(file_get_contents('tables.json'),true);
		foreach($tableDataArray as $tableDataArray){
			echo '<a href="http://digitech.com/KsChild/Master/All?TableName='.$tableDataArray['tableName'].'" target="_blank">'.$tableDataArray['tableName'].'</a>';
			echo "<br>";
		} 
		//print_r($tableDataArray);
	}
    
    function _database_array($tableName){
        $tableDataArray = json_decode(file_get_contents('tables.json'),true);
		
		 
        
        
        if (array_key_exists($tableName, $tableDataArray)) {
            $returnData = $tableDataArray[$tableName];
        }
        else{
            $returnData = array();            
        }
        
        return $returnData;
        
    }
	
	public function masters()
	{
		 		 
		
		$searchValue = $this->session->userdata('searchValue');		
		$data['searchValue'] = $searchValue;
		
        $tableName = trim($this->uri->segment(3));
        if($tableName==''){
            echo "Master source not defined!!! Please contact administrator";
            exit;
        }
        
        $tableData = $this->_database_array($tableName);
        if(empty($tableData)){
            echo "Source Not Found!!!";
            exit;
        }        
        
        $masterName = "";
        $primaryKeyField = "";
        $titleField = "";
        $titleFieldLabel = "";
        $titleFieldValidationAdd = "";
        $titleFieldValidationEdit = "";
        
        if (array_key_exists('masterName', $tableData)) {
            $masterName = $tableData['masterName'];
        }
        if (array_key_exists('primaryKeyField', $tableData)) {
            $primaryKeyField = $tableData['primaryKeyField'];
        }
        if (array_key_exists('titleField', $tableData)) {
            $titleField = $tableData['titleField'];
        }
        if (array_key_exists('titleFieldLabel', $tableData)) {
            $titleFieldLabel = $tableData['titleFieldLabel'];
        }
        if (array_key_exists('titleFieldValidationAdd', $tableData)) {
            $titleFieldValidationAdd = $tableData['titleFieldValidationAdd'];
        }
        if (array_key_exists('titleFieldValidationEdit', $tableData)) {
            $titleFieldValidationEdit = $tableData['titleFieldValidationEdit'];
        }
        
        if($masterName!='' && $primaryKeyField != "" && $titleField != "" && $titleFieldLabel != "" && $titleFieldValidationAdd != "" && $titleFieldValidationEdit != ""){
            
        }else{
            echo "Required params missing in data source";
            exit;
        }
        
         
        $data['masterName'] = $masterName;
        $data['primaryKeyField'] = $primaryKeyField;
        $data['titleField'] = $titleField;
        $data['titleFieldLabel'] = $titleFieldLabel;
        $data['tableName'] = $tableName;
         
        
		
		if ($this->uri->segment(4) !== false){
			$offset= $this->uri->segment(4);	//Set offset for paging
		}
		else{
				$offset=0;	//Set offset for paging
		}
        
        $paging_base_url = site_url('/en/masters/'.$tableName.'/');
        $total_rows = $this->common_model->get_total_for_paging($primaryKeyField,$titleField,$searchValue,$tableName); 
        $paging_uri_segment = 4;		
		$limit=$this->_paging_limit();	//Set totle number of records per page
		
        $config = $this->_bootstrap_paging($paging_base_url,$total_rows,$limit,$paging_uri_segment);
        
       
		
		$this->pagination->initialize($config);
		$data['no_of_records']  = $total_rows;	//count total number of category
		$data['result'] = $this->common_model->get_all_with_paging($limit,$offset,$titleField,$searchValue,$tableName);		//Get all categories from category table
		
		$data['paging'] = $this->pagination->create_links();
		
		
		
				
		$this->load->view('manage_masters',$data);
	}
	
	function master_filter(){
        $tableName = trim($this->uri->segment(3));
        if($tableName==''){
            echo "Master source not defined!!! Please contact administrator";
            exit;
        }
		
        $this->session->unset_userdata('searchValue');
		$this->session->set_userdata('searchValue',$this->input->post('searchValue'));
        redirect('en/masters/'.$tableName, 'refresh');
		
		
	}
	
	function master_reset(){
		
		$tableName = trim($this->uri->segment(3));
        if($tableName==''){
            echo "Master source not defined!!! Please contact administrator";
            exit;
        }
		
        $this->session->unset_userdata('searchValue');
		 
        redirect('en/masters/'.$tableName, 'refresh');
		
		
	}
	 
	
	function add_master(){
		
        $tableName = trim($this->uri->segment(3));
        if($tableName==''){
            echo "Master source not defined!!! Please contact administrator";
            exit;
        }
        
        $tableData = $this->_database_array($tableName);
        if(empty($tableData)){
            echo "Source Not Found!!!";
            exit;
        }        
        
        $masterName = "";
        $primaryKeyField = "";
        $titleField = "";
        $titleFieldLabel = "";
        $titleFieldValidationAdd = "";
        $titleFieldValidationEdit = "";
        
        if (array_key_exists('masterName', $tableData)) {
            $masterName = $tableData['masterName'];
        }
        if (array_key_exists('primaryKeyField', $tableData)) {
            $primaryKeyField = $tableData['primaryKeyField'];
        }
        if (array_key_exists('titleField', $tableData)) {
            $titleField = $tableData['titleField'];
        }
        if (array_key_exists('titleFieldLabel', $tableData)) {
            $titleFieldLabel = $tableData['titleFieldLabel'];
        }
        if (array_key_exists('titleFieldValidationAdd', $tableData)) {
            $titleFieldValidationAdd = $tableData['titleFieldValidationAdd'];
        }
        if (array_key_exists('titleFieldValidationEdit', $tableData)) {
            $titleFieldValidationEdit = $tableData['titleFieldValidationEdit'];
        }
        
        if($masterName!='' && $primaryKeyField != "" && $titleField != "" && $titleFieldLabel != "" && $titleFieldValidationAdd != "" && $titleFieldValidationEdit != ""){
            
        }else{
            echo "Required params missing in data source";
            exit;
        }
        
         
        $data['masterName'] = $masterName;
        $data['primaryKeyField'] = $primaryKeyField;
        $data['titleField'] = $titleField;
        $data['titleFieldLabel'] = $titleFieldLabel;
        $data['tableName'] = $tableName;
        
        $this->form_validation->set_rules('fieldvalue', $titleFieldLabel, $titleFieldValidationAdd);		
		        
		if ($this->form_validation->run() == FALSE) {
			 
			$this->load->view('add_master',$data);
		}
		else { 
			
			$dataArray = array(
				$titleField => $this->input->post('fieldvalue') 
			);
			 
			$insert_status = $this->common_model->insert($dataArray,$tableName);
			$this->session->set_flashdata('msg', $insert_status);
			redirect('en/add_master/'.$tableName, 'refresh'); 	 
			 
			
		}
	}
	 
	function edit_master(){
		
        $tableName = trim($this->uri->segment(3));
        if($tableName==''){
            echo "Master source not defined!!! Please contact administrator";
            exit;
        }
        
        $tableData = $this->_database_array($tableName);
        if(empty($tableData)){
            echo "Source Not Found!!!";
            exit;
        }        
        
        $masterName = "";
        $primaryKeyField = "";
        $titleField = "";
        $titleFieldLabel = "";
        $titleFieldValidationAdd = "";
        $titleFieldValidationEdit = "";
        
         if (array_key_exists('masterName', $tableData)) {
            $masterName = $tableData['masterName'];
        }
        if (array_key_exists('primaryKeyField', $tableData)) {
            $primaryKeyField = $tableData['primaryKeyField'];
        }
        if (array_key_exists('titleField', $tableData)) {
            $titleField = $tableData['titleField'];
        }
        if (array_key_exists('titleFieldLabel', $tableData)) {
            $titleFieldLabel = $tableData['titleFieldLabel'];
        }
        if (array_key_exists('titleFieldValidationAdd', $tableData)) {
            $titleFieldValidationAdd = $tableData['titleFieldValidationAdd'];
        }
        if (array_key_exists('titleFieldValidationEdit', $tableData)) {
            $titleFieldValidationEdit = $tableData['titleFieldValidationEdit'];
        }
        
        if($masterName!='' && $primaryKeyField != "" && $titleField != "" && $titleFieldLabel != "" && $titleFieldValidationAdd != "" && $titleFieldValidationEdit != ""){
            
        }else{
            echo "Required params missing in data source";
            exit;
        }
        
         
        $data['masterName'] = $masterName;
        $data['primaryKeyField'] = $primaryKeyField;
        $data['titleField'] = $titleField;
        $data['titleFieldLabel'] = $titleFieldLabel;
        $data['tableName'] = $tableName;
        
        		
		
        $id = $this->uri->segment(4);
		 
		if(!is_numeric($id)){
			echo "error";
			exit;	
		}
		$result = $this->common_model->get_by_id($primaryKeyField,$id,$tableName);
		if(empty($result)){
			echo "record does not exist or is deleted";
			exit;
		}
		$data['result'] = $result; 
        
		if($this->input->post('fieldvalue')==$result[0][$titleField]){
		    $this->form_validation->set_rules('fieldvalue', $titleFieldLabel, $titleFieldValidationEdit);		
		}else{
			$this->form_validation->set_rules('fieldvalue', $titleFieldLabel, $titleFieldValidationAdd);
		} 
        
		if ($this->form_validation->run() == FALSE) {
			 
			$this->load->view('add_master',$data);
		}
		else { 
			
			$dataArray = array(
				$titleField => $this->input->post('fieldvalue') 
			);
			
			$updateStatus = $this->common_model->update($id,$dataArray,$primaryKeyField,$tableName);
			$this->session->set_flashdata('msg', $updateStatus);
			redirect('en/edit_master/'.$tableName.'/'.$id, 'refresh'); 	 
			 
			
		}
	} 
	
	function delete_master(){
		 
		
		$tableName = trim($this->uri->segment(3));
        if($tableName==''){
            echo "Master source not defined!!! Please contact administrator";
            exit;
        }
        
        $tableData = $this->_database_array($tableName);
        if(empty($tableData)){
            echo "Source Not Found!!!";
            exit;
        }        
        
        $masterName = "";
        $primaryKeyField = "";
        $titleField = "";
        $titleFieldLabel = "";
        $titleFieldValidationAdd = "";
        $titleFieldValidationEdit = "";
        
         if (array_key_exists('masterName', $tableData)) {
            $masterName = $tableData['masterName'];
        }
        if (array_key_exists('primaryKeyField', $tableData)) {
            $primaryKeyField = $tableData['primaryKeyField'];
        }
        if (array_key_exists('titleField', $tableData)) {
            $titleField = $tableData['titleField'];
        }
        if (array_key_exists('titleFieldLabel', $tableData)) {
            $titleFieldLabel = $tableData['titleFieldLabel'];
        }
        if (array_key_exists('titleFieldValidationAdd', $tableData)) {
            $titleFieldValidationAdd = $tableData['titleFieldValidationAdd'];
        }
        if (array_key_exists('titleFieldValidationEdit', $tableData)) {
            $titleFieldValidationEdit = $tableData['titleFieldValidationEdit'];
        }
        
        if($masterName!='' && $primaryKeyField != "" && $titleField != "" && $titleFieldLabel != "" && $titleFieldValidationAdd != "" && $titleFieldValidationEdit != ""){
            
        }else{
            echo "Required params missing in data source";
            exit;
        }
        
         
        
        
        		
		
        $id = $this->uri->segment(4);
		 
		if(!is_numeric($id)){
			echo "error";
			exit;	
		}
		$result = $this->common_model->get_by_id($primaryKeyField,$id,$tableName);
		if(empty($result)){
			echo "record does not exist or is deleted";
			exit;
		}
		
		$delete_status = $this->common_model->delete($id,$primaryKeyField,$tableName);
		$this->session->set_flashdata('msg', $delete_status);
		redirect('en/masters/'.$tableName, 'refresh');
		
	}
	
	 
}
