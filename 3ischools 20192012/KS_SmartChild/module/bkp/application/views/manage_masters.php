<?php include('header.php');
 
?>
<!-- Page Heading -->
    <div class="row top-space-10 page-heading">
    	<div class="col-xs-6">
        	<h1>Add <?php echo $masterName;?></h2>
        </div>
        <div class="col-xs-6 text-right">
        	<a  href="javascript:void(0)" onclick="openModel('Add New Record','<?php echo site_url('en/add_master/'.$tableName);?>')"  class="btn btn-xs btn-default">Add New</a> 
        </div>
    </div>
    <!-- //Page Heading -->
<?php echo $this->session->flashdata('msg'); ?> 

<!-- filter -->
<div class="row top-space-10">
    	<div class="col-md-12">
       
        </div>
    </div>

<!-- //filter -->

 
<div class="row top-space-20">
    	<div class="col-md-12">
        	<table class="table table-bordered table-striped">
        <tr>
            <td> 
             <form class="form-inline" method="post" action="<?php echo site_url('en/master_filter/'.$tableName);?>">
          <div class="form-group">
            
            <input type="text" class="form-control" name="searchValue" placeholder="<?php echo $titleFieldLabel;?>" value="<?php echo $searchValue;?>"  data-toggle="tooltip" data-placement="top" title="Type and press enter to search data" style="height:28px; border-radius:0;">
          </div> 
          <a href="<?php echo site_url('en/master_reset/'.$tableName);?>" class="btn btn-sm btn-default">Reset</a> 
          
           
        </form>	 
            </td>
            <td class="text-right">Total Entries -
                <?php echo $no_of_records;?>
            </td>
        </tr>
        <tr>
            <th>
                <?php echo $titleFieldLabel;?>
            </th>

            <th class="text-right">
                Action
            </th>
        </tr>
        <?php foreach($result as $result){?>
        <tr>
            <td>
                <?php echo $result[$titleField];?>
            </td>

            <td class="text-right">

                <a href="javascript:void(0)" onclick="openModel('Edit Record','<?php echo site_url('en/edit_master/'.$tableName.'/'.$result[$primaryKeyField]);?>')" class="btn btn-xs btn-warning"><i class="fa fa-pencil"></i></a>
                <a href="<?php echo site_url('en/delete_master/'.$tableName.'/'.$result[$primaryKeyField]);?>" onclick="return confirm('are you sure?');" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>

            </td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="5">
                <?php print_r($paging);?>
            </td>
        </tr>
    </table>
</div>
</div>
<!-- //Data Container -->



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <a href="<?php echo current_url();?>" class="close">&times;</a>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">
                <iframe id="modelFrame" frameborder="0" scrolling="auto" src="" style="width:100%; height:200px; float:left"></iframe>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="<?php echo base_url('admin-assets/css/jquery-ui.css');?>">
<script src="<?php echo base_url('admin-assets/js/jquery-ui.js');?>"></script>
<script>
    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true
        });

        <?php if($svendor>=1){?>
        $.get("<?php echo site_url('user/balance_amt/'.$svendor);?>", function(data) {
            $("#balAmt").text(data);

        });
        <?php }?>


    });

</script>
<script>
    function openModel(title, uurl) {
        $('#myModalLabel').text(title);
        $('#modelFrame').attr('src', uurl);
        $('#myModal').modal({
            keyboard: false,
            backdrop: 'static'
        })
    }

</script>
<?php include('footer.php');?>
