<?php include('header.php');
$fieldvalue = set_value('fieldvalue');
 

if(!empty($result)){
$fieldvalue = $result[0][$titleField];
 
}

?>

<!-- Form Container -->

<form method="post">
    <?php echo $this->session->flashdata('msg'); ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label><?php echo $titleFieldLabel;?></label>
                <input type="text" class="form-control" name="fieldvalue" value="<?php echo $fieldvalue;?>">
                <?php echo form_error('fieldvalue','<span class="label label-danger">','</span>');?>
            </div>

        </div>
    </div>
    <div class="row">

        <div class="col-xs-12">

            <input type="submit" class="btn btn-success" value="Save">
        </div>
    </div>
</form>


<?php include('footer.php');?>
