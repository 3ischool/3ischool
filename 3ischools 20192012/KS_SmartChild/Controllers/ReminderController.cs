﻿using BAL.BAL.Common;
using BAL.BAL.Message;
using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ContactModule;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.FeeModule;
using DAL.DAL.GuardianModule;
using DAL.DAL.Kendo;
using DAL.DAL.LateFeeModule;
using DAL.DAL.SettingService;
using DAL.DAL.StaffModule;
using DAL.DAL.StudentModule;
using DAL.DAL.TransportModule;
using DAL.DAL.TriggerEventModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Fee;

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Net.Http;
using System.Text;
using System.Net;
using System.IO;
using System.Reflection;
using ViewModel.ViewModel.Common;
using DAL.DAL.Message;
using ViewModel.ViewModel.Security;

namespace KS_SmartChild.Controllers
{
    public class ReminderController : Controller
    {

        #region fields

        public int count = 0;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ILogService _Logger;
        private readonly IWebHelper _WebHelper;
        private readonly IFeeService _IFeeService;
        private readonly ILateFeeService _ILateFeeService;

        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStudentService _IStudentService;
        private readonly IStudentAddOnService _IStudentAddOnService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IGuardianService _IGuardianService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly IContactService _IContactService;
        private readonly IStudentMasterService _IStudentMasterService;
        private readonly IBusService _IBusService;
        private readonly ITransportService _ITransportService;
        private readonly ISchoolDataService _ISchoolDataService;
        private readonly IExportHelper _IExportHelper;
        private readonly IAddressService _IAddressService;
        private readonly ITriggerEventService _ITriggerEventService;
        private readonly IWorkflowMessageService _IWorkflowMessageService;
        private readonly ISMSSender _ISMSSender;
        private readonly ICommonMethodService _ICommonMethodService;
        private readonly IStaffService _IStaffService;

        private KS_ChildEntities db;
        private string SqlDataSource;


        #endregion

        #region ctor

        public ReminderController(IUserService IUserService,
            IConnectionService IConnectionService,
            IDropDownService IDropDownService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            ILogService Logger, ISchoolDataService ISchoolDataService,
            IWebHelper WebHelper,
            IFeeService IFeeService,
            ILateFeeService ILateFeeService,

            ICatalogMasterService ICatalogMasterService,
            IStudentService IStudentService,
            IStudentAddOnService IStudentAddOnService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption,
            IGuardianService IGuardianService,
            IAddressMasterService IAddressMasterService,
            IContactService IContactService,
            IStudentMasterService IStudentMasterService,
            IBusService IBusService,
            ITransportService ITransportService, IExportHelper IExportHelper, IAddressService IAddressService,
            ITriggerEventService ITriggerEventService,
            IWorkflowMessageService IWorkflowMessageService,
             ISMSSender ISMSSender, ICommonMethodService ICommonMethodService,
            IStaffService IStaffService)
        {
            this._IUserService = IUserService;

            this._IConnectionService = IConnectionService;
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._Logger = Logger;
            this._WebHelper = WebHelper;
            this._IFeeService = IFeeService;

            this._ICatalogMasterService = ICatalogMasterService;
            this._IStudentService = IStudentService;
            this._IStudentAddOnService = IStudentAddOnService;
            this._ILateFeeService = ILateFeeService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._IGuardianService = IGuardianService;
            this._IAddressMasterService = IAddressMasterService;
            this._IContactService = IContactService;
            this._IStudentMasterService = IStudentMasterService;
            this._IBusService = IBusService;
            this._ITransportService = ITransportService;
            this._ISchoolDataService = ISchoolDataService;
            this._IExportHelper = IExportHelper;
            this._IAddressService = IAddressService;
            this._ITriggerEventService = ITriggerEventService;
            this._IWorkflowMessageService = IWorkflowMessageService;
            this._ISMSSender = ISMSSender;
            this._ICommonMethodService = ICommonMethodService;
            this._IStaffService = IStaffService;
        }

        #endregion

        #region utility
        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }
        public string GetSetCurrentSchoolDataBase(HttpRequestBase Request, HttpServerUtilityBase Server)
        {
            try
            {




                // get subdomain
                string User = "";
                string Subdomain = Request.Url.AbsolutePath;
                Subdomain = Subdomain.Split('/')[1];
                // API url
                //string MainUrl = "";
                //string suburl = Convert.ToString(WebConfigurationManager.AppSettings["SubUrl"]);
                //string synccompany = Convert.ToString(WebConfigurationManager.AppSettings["syncapiname"]);
                //string syncsub = Convert.ToString(WebConfigurationManager.AppSettings["syncsub"]);
                //string syncext = Convert.ToString(WebConfigurationManager.AppSettings["syncext"]);
                //string APIName = Convert.ToString(WebConfigurationManager.AppSettings["APIName"]);

                if (Request.Url.AbsoluteUri.Contains("localhost") || Request.Url.AbsoluteUri.Contains("172.16.1.2"))
                {
                    User = Convert.ToString(WebConfigurationManager.AppSettings["User"]);
                    // MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                    //  // User = "stroop";
                    //  // User="atsderabassi";
                    //   User = "Dev-KS_School";
                    //  //User = "3is_demo";
                    //  User = "demo";
                    //  // User = "kschild";
                    //  //User = "bpsambala";
                    //  //User = "hgdemo";
                    //  //User = "dtsk8";
                    //  //User = "";
                    // MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
                    // MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;

                    // MainUrl = "http://app.3ischools.com/api_v1-11" + "/" + suburl;
                    //MainUrl = "http://mobileapi.digitech.co.in" + "/" + suburl;

                }
                else if (Request.Url.AbsoluteUri.Contains("digitech.com"))
                {
                    User = Subdomain;
                    //MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
                    //MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                }
                else if (Request.Url.AbsoluteUri.Contains("digitech.co.in"))
                {
                    User = Subdomain;
                    // MainUrl = "http://api.digitech.co.in" + "/" + suburl;
                    //MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;

                }
                else
                {
                    User = Subdomain;
                    //  MainUrl = "http://3ischools.com/api" + "/" + suburl;
                    //MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;
                   // User = "ks001";
                }

                #region get database details from json

                var dynamicnavigationpath = "";

                dynamicnavigationpath = "navigationpath";
                string file = "";
                file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(WebConfigurationManager.AppSettings["Credential"]));
                string Json = System.IO.File.ReadAllText(file);
                System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();

                dynamic stuff1 = Newtonsoft.Json.JsonConvert.DeserializeObject(Json);
                JObject o = stuff1;

                var nameOfProperty = _ICustomEncryption.base64e(User);

                IList<string> keys = o.Properties().Select(p => p.Name).ToList();
                JToken entireJson = JToken.Parse(Json);
                JArray inner = entireJson[nameOfProperty].Value<JArray>();
                var db_id = Convert.ToInt32(inner.First);
                var db_name = Convert.ToInt32(inner.Last);

                var dbid = (db_id - 1987) / 6;
                var dbname = (db_name - 1947) / 3;
                #endregion
                //HttpClient client = new HttpClient();
                //client.Timeout = new TimeSpan(0, 10, 1);
                //MainUrl = MainUrl + "?DBName=" + User; // url with parameter
                //client.DefaultRequestHeaders.Accept.Add(
                //new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //var response = client.GetAsync(MainUrl).Result;
                //var content = response.Content.ReadAsStringAsync().Result;

                var IsDatabase = new APIDatabaseModel();
                IsDatabase.DBId = dbid;
                if (Request.Url.AbsoluteUri.Contains("localhost") || Request.Url.AbsoluteUri.Contains("172.16.1.2"))
                {
                    IsDatabase.DBName = "ks_" + dbname.ToString().PadLeft(5, '0');
                }
                else if (Request.Url.AbsoluteUri.Contains("digitech.co.in"))
                {
                    IsDatabase.DBName = "tst_" + dbname.ToString().PadLeft(5, '0');
                }
                else
                {
                    IsDatabase.DBName = "3is_" + dbname.ToString().PadLeft(5, '0');
                }
                if (IsDatabase != null && !string.IsNullOrEmpty(IsDatabase.DBName))
                {
                    // change connection string 
                    //var cn = _IConnectionService.Connectionstring(IsDatabase.DBName);
                    // check directory exist or not


                    //Session.Timeout = 60;
                    //                Session["SchoolDB"] = IsDatabase.DBName;
                    //                Session["SchoolId"] = IsDatabase.DBId.ToString();

                    //var directory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId));
                    //if (!Directory.Exists(directory))
                    //    Directory.CreateDirectory(directory);

                    //var teacherdirectory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId + "/Teacher/Photos"));
                    //if (!Directory.Exists(teacherdirectory))
                    //    Directory.CreateDirectory(teacherdirectory);
                }
                return IsDatabase.DBName;

                //else
                //    Connectivity.Error.ToString();

                // current user
                //string user = System.Web.HttpContext.Current.User.Identity.Name;
                //if (string.IsNullOrEmpty(user))
                //    return Connectivity.Login.ToString();
                //else
                //    return Connectivity.Done.ToString();















                //// get subdomain
                //string User = "";
                //string Subdomain = Request.Url.AbsolutePath;
                //Subdomain = Subdomain.Split('/')[1];
                //// API url
                //string MainUrl = "";
                //string suburl = Convert.ToString(WebConfigurationManager.AppSettings["SubUrl"]);
                //string synccompany = Convert.ToString(WebConfigurationManager.AppSettings["syncapiname"]);
                //string syncsub = Convert.ToString(WebConfigurationManager.AppSettings["syncsub"]);
                //string syncext = Convert.ToString(WebConfigurationManager.AppSettings["syncext"]);
                //string APIName = Convert.ToString(WebConfigurationManager.AppSettings["APIName"]);

                //if (Request.Url.AbsoluteUri.Contains("localhost"))
                //{
                //    User = Convert.ToString(WebConfigurationManager.AppSettings["User"]);
                //    // MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                //    //  // User = "stroop";
                //    //  // User="atsderabassi";
                //    //   User = "Dev-KS_School";
                //    //  //User = "3is_demo";
                //    //  User = "demo";
                //    //  // User = "kschild";
                //    //  //User = "bpsambala";
                //    //  //User = "hgdemo";
                //    //  //User = "dtsk8";
                //    //  //User = "";
                //  // MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
                //   MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;

                // // MainUrl = "http://app.3ischools.com/api_v1-10" + "/" + suburl;
                //   //MainUrl = "http://mobileapi.digitech.co.in" + "/" + suburl;

                //}
                //else if (Request.Url.AbsoluteUri.Contains("digitech.com") || Request.Url.AbsoluteUri.Contains("172.16.1.2"))
                //{
                //    User = Subdomain;
                //    //MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
                //    MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                //}
                //else if (Request.Url.AbsoluteUri.Contains("digitech.co.in"))
                //{
                //    User = Subdomain;
                //    // MainUrl = "http://api.digitech.co.in" + "/" + suburl;
                //    MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;

                //}
                //else
                //{
                //    User = Subdomain;
                //    //  MainUrl = "http://3ischools.com/api" + "/" + suburl;
                //    MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;

                //}

                //HttpClient client = new HttpClient();
                //client.Timeout = new TimeSpan(0, 10, 1);
                //MainUrl = MainUrl + "?DBName=" + User; // url with parameter
                //client.DefaultRequestHeaders.Accept.Add(
                //new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //var response = client.GetAsync(MainUrl).Result;
                //var content = response.Content.ReadAsStringAsync().Result;

                //var IsDatabase = JsonConvert.DeserializeObject<APIDatabaseModel>(content);
                //if (IsDatabase != null && !string.IsNullOrEmpty(IsDatabase.DBName))
                //{
                //    // change connection string 
                //    var cn = _IConnectionService.Connectionstring(IsDatabase.DBName);
                //    // check directory exist or not
                //    Session.Timeout = 60;
                //    Session["SchoolDB"] = IsDatabase.DBName;
                //    Session["SchoolId"] = IsDatabase.DBId.ToString();

                //    var directory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId));
                //    if (!Directory.Exists(directory))
                //        Directory.CreateDirectory(directory);

                //    var teacherdirectory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId + "/Teacher/Photos"));
                //    if (!Directory.Exists(teacherdirectory))
                //        Directory.CreateDirectory(teacherdirectory);
                //}
                //else
                //    Connectivity.Error.ToString();

                //// current user
                //string user = System.Web.HttpContext.Current.User.Identity.Name;
                //if (string.IsNullOrEmpty(user))
                //    return Connectivity.Login.ToString();
                //else
                //    return Connectivity.Done.ToString();
            }
            catch (Exception ex)
            {
                //InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Connectivity.Error.ToString();
            }
        }


        #endregion

        public string GetReturnToken(string content)
        {
            //var content="{'message':'396545725932353536303334','type':'success'}";
            if (content != "")
            {
                dynamic stuff1 = JsonConvert.DeserializeObject(content);
                JObject o = stuff1;

                var nameOfProperty = "message";

                IList<string> keys = o.Properties().Select(p => p.Name).ToList();
                JToken entireJson = JToken.Parse(content);
                string token = entireJson[nameOfProperty].Value<string>();
                return token;
            }
            else
            {
                return "";
            }
        }
        //send reminders
        public void SendSmSReminders()
        {
            var dbname = GetSetCurrentSchoolDataBase(Request, Server);
            var datasource = Connectionstring(dbname);
            db = new KS_ChildEntities(datasource);
            var messageQueuetypeId = 0;
            var queueType = db.MessageQueueTypes.Where(m => m.QueueType.ToLower().Contains("sms")).FirstOrDefault();
            if (queueType != null)
                messageQueuetypeId = queueType.QueueTypeId;

            var AutoFeeReminderSMS = GetorSetSchoolData("AutoFeeReminderOnetimeShootCount", "50", null).AttributeValue;

            var messageQueueList = GetAllMessageQueue(ref count).Where(m => m.MessageQueueType.QueueTypeId == messageQueuetypeId && m.SentTime == null).Take(Convert.ToInt32(AutoFeeReminderSMS));

            foreach (var item in messageQueueList)
            {
                var errormessage = "";
                var content = "";
                SendSMS(String.Join(",", item.Recipient), item.Message, false, out errormessage, out content);
                
                item.ReturnToken = GetReturnToken(content);
                item.SentTime = DateTime.Now;

                UpdateMessageQueue(item);

            }

        }
        public void SurveillanceAlarmNotification()
        {
            var dbname = GetSetCurrentSchoolDataBase(Request, Server);
            var datasource = Connectionstring(dbname);
            db = new KS_ChildEntities(datasource);
            var surveillancealarms = db.SurveillanceAlarms.Where(m => m.AlarmActivatedAt == null  && m.SurviellancePunch.OutTime == null && (m.AlarmTime >= DateTime.Now.AddMinutes(-1) && m.AlarmTime <= DateTime.Now.AddMinutes(1)));
            var currentDate = DateTime.Now.Date.ToString("yyMMdd");
            var CounterNotification = 0;
            var staff = db.Staffs;

                var model = new SurveillanceAlarmModel();
                var modelList = new List<SurveillanceAlarmModel>();
                foreach (var item in surveillancealarms)
                {

                    var student = db.Students.Where(m=>m.StudentId==(int)item.SurviellancePunch.ContactId).FirstOrDefault();
                    model = new SurveillanceAlarmModel();
                    var contacttype = db.ContactTypes.Where(m=>m.ContactTypeId==(int)item.SurviellancePunch.ContactTypeId).FirstOrDefault()!=null? db.ContactTypes.Where(m => m.ContactTypeId == (int)item.SurviellancePunch.ContactTypeId).FirstOrDefault().ContactType1.ToLower():"";
                    if (contacttype == "student")
                        model.StudentName = student.FName + " " + student.FName != null ? student.FName : "";
                    model.SurveillancePunchId = (int)item.SurviellancePunchId;
                    model.SurveillanceAllarmId = item.SurveillanceAlarmId;
                    model.StudentId = (int)item.SurviellancePunch.ContactId;
                    model.AdmnNo = student.AdmnNo != null ? student.AdmnNo : "";
                var today = DateTime.UtcNow;
                var currentsession = db.Sessions.Where(p => p.StartDate.Value.Date <= today.Date && p.EndDate.Value.Date >= today.Date).FirstOrDefault();
                var studentClass = db.StudentClasses.Where(m => m.StudentId == student.StudentId && m.SessionId == currentsession.SessionId).FirstOrDefault();
                    model.ClassName = studentClass != null ? studentClass.ClassMaster.Class : "";
                    model.DeviceLocation = item.SurviellancePunch.AttendanceLocation.AttendanceLocation1;
                    model.EntryTime = ConvertDate((DateTime)item.SurviellancePunch.InTime);

                    var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                    var StudentImagepath = "";
                    var doctypeid = db.DocumentTypes.Where(m=>m.DocumentType1=="Image").FirstOrDefault();
                    var image = db.AttachedDocuments.Where(m=>m.DocumentTypeId==doctypeid.DocumentTypeId && m.StudentId==student.StudentId
                        ).FirstOrDefault();

                    if (image != null)
                    {
                        //model.StudentImageId = image.DocumentId;

                        StudentImagepath = _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/StudentPhotos/" + image.Image;
                    }
                    else
                        StudentImagepath = "";
                    model.StudentImage = StudentImagepath;
                    modelList.Add(model);

                    var messageSMS = "Surveillance alert : " + model.StudentName + " , " + model.AdmnNo + " , " + model.DeviceLocation + " , " + model.EntryTime;
                    var errormessage = "";
                    var content = "";
                    _ISMSSender.SendSMS(item.StaffPhone, messageSMS, false, out errormessage, out content);

                var staffcontacttypeid = staff.Where(m => m.StaffId == item.StaffId).FirstOrDefault() != null ? staff.Where(m => m.StaffId == item.StaffId).FirstOrDefault().ContactTypeId : 0;
                //notification
                var isschooluser = db.SchoolUsers
                                               .Where(m => m.UserTypeId == staffcontacttypeid
                                                   && m.UserContactId == item.StaffId);
                var userdeviceIdsArray = db.UserDevices
                                                               .Where(m => m.UserId == isschooluser.FirstOrDefault().UserId
                                                                && m.UserDeviceName == "IOS")
                                                                .Select(N => N.UserDeviceUniqueId).ToArray();
                    var userdeviceIdsArrayAndroid = db.UserDevices
                                                    .Where(m => m.UserId == isschooluser.FirstOrDefault().UserId
                                                    && m.UserDeviceName == "Android")
                                                    .Select(N => N.UserDeviceUniqueId).ToArray();
                    if (userdeviceIdsArray.Count() > 0)
                    {
                        //var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                        //                                        (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;
                    var UpdateCollapseKey = GetorSetSchoolData("CollapseKeyForNotification",
                                                       (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                    _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, "",
                                                                            messageSMS, "IOS",
                                                                            UpdateCollapseKey);
                    }
                    if (userdeviceIdsArrayAndroid.Count() > 0)
                    {
                        //var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                        //                                        (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;
                    var UpdateCollapseKey = GetorSetSchoolData("CollapseKeyForNotification",
                                                      (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                    _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, "",
                                                                        messageSMS, "Android",
                                                                        UpdateCollapseKey);
                    }

            }
                if (surveillancealarms.Count() > 0)
                {
                    foreach (var item in surveillancealarms)
                    {
                        item.AlarmActivatedAt = DateTime.Now;
                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
               // return Json(new { status = "success", data = modelList });
            
           


            //return Json(new { status = "failed", data = "Error record can't be deleted." });



        }
    
        public bool SendSMS(string mobileNumber, string message, bool IsOTP, out string Errormessage, out string content)
        {

            message = HttpUtility.UrlEncode(message);
            //  string ApiString = "http://bulksms.bagful.net/api/sendhttp.php?authkey=%pass%&message=%msg%";
            var count = 0;
            var MainUrl = "";
            var SMSAPI = GetAllSMSAPIs(ref count).Where(m => m.IsActive == true).FirstOrDefault();
            if (SMSAPI != null)
            {
                MainUrl = SMSAPI.APIFullUrlString;
                var mobilenoslist = mobileNumber.Split(',').ToList<string>();
                var mobilenolistwithcountrycode = new List<string>();
                foreach (var item in mobilenoslist)
                {
                    if ((bool)SMSAPI.AddCountryCode)
                    {
                        mobilenolistwithcountrycode.Add(SMSAPI.CountryCode + item);
                    }
                    else
                    {
                        mobilenolistwithcountrycode.Add(item);
                    }
                }
                string messagepattern = @"%message%";
                string mobilepattern = @"%mobileno%";
                string replacemessage = message;
                string replacemobiles = String.Join(",", mobilenolistwithcountrycode);
                string result = Regex.Replace(MainUrl, messagepattern, replacemessage);
                MainUrl = Regex.Replace(result, mobilepattern, replacemobiles);
            }

            if (MainUrl != "")
            {
                try
                {
                    HttpClient client = new HttpClient();
                    client.Timeout = new TimeSpan(0, 10, 1);
                    //MainUrl = MainUrl; // url with parameter
                    client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync(MainUrl).Result;
                    if (SMSAPI.IsAPIWebHookApplicable != null && (bool)SMSAPI.IsAPIWebHookApplicable)
                    {
                        content = response.Content.ReadAsStringAsync().Result;
                    }
                    else
                    {
                        content = "";
                    }
                    Errormessage = "";
                    return true;
                }
                catch (SystemException ex)
                {
                    content = "";
                    Errormessage = "";
                    return false;
                }
            }
            else
            {
                if (IsOTP)
                {
                    //Your authentication key
                    string authKey = Convert.ToString(WebConfigurationManager.AppSettings["OTPAuthKey"]); //"100067AUMuvZMzUX56752b81";
                    //Multiple mobiles numbers separated by comma
                    //string mobileNumber = "9999999";
                    //Sender ID,While using route4 sender id should be 6 characters long.
                    string senderId = Convert.ToString(WebConfigurationManager.AppSettings["OTPSenderId"]); //"KSmart";
                    //Your message to send, Add URL encoding here.
                    message = HttpUtility.UrlEncode(message);
                    //  string ApiString = "http://bulksms.bagful.net/api/sendhttp.php?authkey=%pass%&message=%msg%";
                    string route = Convert.ToString(WebConfigurationManager.AppSettings["OTPRoute"]); //"4";
                    //Prepare you post parameters
                    StringBuilder sbPostData = new StringBuilder();
                    sbPostData.AppendFormat("authkey={0}", authKey);
                    sbPostData.AppendFormat("&mobiles={0}", "91" + mobileNumber);
                    sbPostData.AppendFormat("&message={0}", message);
                    sbPostData.AppendFormat("&sender={0}", senderId);
                    sbPostData.AppendFormat("&route={0}", route);
                    sbPostData.AppendFormat("&country={0}", "91");

                    try
                    {
                        //Call Send SMS API
                        string sendSMSUri = Convert.ToString(WebConfigurationManager.AppSettings["OTPSendUrl"]); //"http://bulksms.bagful.net/api/sendhttp.php";
                        //Create HTTPWebrequest
                        HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                        //Prepare and Add URL Encoded data
                        UTF8Encoding encoding = new UTF8Encoding();
                        byte[] data = encoding.GetBytes(sbPostData.ToString());
                        //Specify post method
                        httpWReq.Method = "POST";
                        httpWReq.ContentType = "application/x-www-form-urlencoded";
                        httpWReq.ContentLength = data.Length;
                        using (Stream stream = httpWReq.GetRequestStream())
                        {
                            stream.Write(data, 0, data.Length);
                        }
                        //Get the response
                        HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                        StreamReader reader = new StreamReader(response.GetResponseStream());
                        string responseString = reader.ReadToEnd();

                        //Close the response
                        reader.Close();
                        response.Close();
                        Errormessage = "";
                        content = "";
                        return true;
                    }
                    catch (SystemException ex)
                    {
                        Errormessage = "";
                        content = "";
                        return false;
                    }
                }
                else
                {
                    Errormessage = "Please Implement SMS Api";
                    content = "";
                    return false;
                }
            }



        }
        public List<KSModel.Models.SMSAPI> GetAllSMSAPIs(ref int totalcount, string userid = null, string senderid = null,
                    int? schoolId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.SMSAPIs.ToList();
            if (!String.IsNullOrEmpty(userid))
                query = query.Where(e => e.APIUserId.ToLower().Contains(userid.ToLower())).ToList();
            if (!String.IsNullOrEmpty(senderid))
                query = query.Where(e => e.APISenderId.ToLower().Contains(senderid.ToLower())).ToList();
            if (schoolId != null && schoolId > 0)
                //query = query.Where(e => e.SchoolId == schoolId).ToList();

                totalcount = query.Count;
           // var SMSAPIs = new PagedList<KSModel.Models.SMSAPI>(query, PageIndex, PageSize);
          //  return SMSAPIs;
            //  var SMSAPIs = new PagedList<KSModel.Models.SMSAPI>(query, PageIndex, PageSize);
            return query.ToList();
        }


        public List<UserDevice> GetAllUserDevices(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var dbname = GetSetCurrentSchoolDataBase(Request, Server);
            var datasource = Connectionstring(dbname);
            db = new KS_ChildEntities(datasource);
            var query = db.UserDevices.ToList();
            totalcount = query.Count;
          //  var UserDevices = new PagedList<KSModel.Models.UserDevice>(query, PageIndex, PageSize);
          //  return UserDevices;
            // var UserDevices = new PagedList<KSModel.Models.UserDevice>(query, PageIndex, PageSize);
            return query.ToList();
        }

        public void Notification(SchoolUser isschooluser, string Message)
        {
            var currentDate = DateTime.Now.Date.ToString("yyMMdd");
            var CounterNotification = 0;
            var userdeviceIdsArray = GetAllUserDevices(ref count)
                                                                       .Where(m => m.UserId == isschooluser.UserId
                                                                        && m.UserDeviceName == "IOS")
                                                                        .Select(N => N.UserDeviceUniqueId).ToArray();
            var userdeviceIdsArrayAndroid = GetAllUserDevices(ref count)
                                            .Where(m => m.UserId == isschooluser.UserId
                                            && m.UserDeviceName == "Android")
                                            .Select(N => N.UserDeviceUniqueId).ToArray();
            if (userdeviceIdsArray.Count() > 0)
            {
                var UpdateCollapseKey = GetorSetSchoolData("CollapseKeyForNotification",
                                                        (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, "",
                                                                   Message, "IOS",
                                                                    UpdateCollapseKey);
            }
            if (userdeviceIdsArrayAndroid.Count() > 0)
            {
                var UpdateCollapseKey = GetorSetSchoolData("CollapseKeyForNotification",
                                                        (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, "",
                                                               Message, "Android",
                                                                UpdateCollapseKey);
            }

        }


        public void SendNotificationReminders()
        {
            var dbname = GetSetCurrentSchoolDataBase(Request, Server);
            var datasource = Connectionstring(dbname);
            db = new KS_ChildEntities(datasource);
            var messageQueuetypeId = db.MessageQueueTypes.Where(m => m.QueueType.ToLower().Contains("notification")).FirstOrDefault() != null ? db.MessageQueueTypes.Where(m => m.QueueType.ToLower().Contains("notification")).FirstOrDefault().QueueTypeId : 0;
            var AutoFeeReminderSMS = GetorSetSchoolData("AutoFeeReminderOnetimeShootCount", "50", null).AttributeValue;

            var messageQueueList = GetAllMessageQueue(ref count).Where(m => m.MessageQueueType.QueueTypeId == messageQueuetypeId && m.SentTime == null).Take(Convert.ToInt32(AutoFeeReminderSMS));

            foreach (var item in messageQueueList)
            {
                if (item.StudentId != null) { 
                var studentid = Convert.ToInt32(item.StudentId);

                var isschooluser = db.SchoolUsers.Where(m => m.UserContactId == studentid && m.ContactType.ContactType1.ToLower() == "student");

                Notification(isschooluser.FirstOrDefault(), item.Message);
                //get guardians
                var FatherguardiantypeId = GetAllRelations(ref count).Where(m => m.Relation1.ToLower() == "father").FirstOrDefault() != null ? GetAllRelations(ref count).Where(m => m.Relation1.ToLower() == "father").FirstOrDefault().RelationId : 0;
                var MotherGuardianTypeId = GetAllRelations(ref count).Where(m => m.Relation1.ToLower() == "mother").FirstOrDefault() != null ? GetAllRelations(ref count).Where(m => m.Relation1.ToLower() == "mother").FirstOrDefault().RelationId : 0;
                var GuardianTypeId = GetAllRelations(ref count).Where(m => m.Relation1.ToLower() == "guardian").FirstOrDefault() != null ? GetAllRelations(ref count).Where(m => m.Relation1.ToLower() == "mother").FirstOrDefault().RelationId : 0;

                var guardians = db.Guardians.Where(m => m.StudentId == item.StudentId && (m.GuardianTypeId == FatherguardiantypeId ||
                        m.GuardianTypeId == MotherGuardianTypeId || m.GuardianTypeId == GuardianTypeId));

                foreach (var itemgaurdian in guardians)
                {
                    var grdianid = Convert.ToInt32(itemgaurdian.GuardianId);
                    isschooluser = db.SchoolUsers.Where(m => m.UserContactId == grdianid && m.ContactType.ContactType1.ToLower() == "guardian");
                    if (isschooluser.FirstOrDefault() != null)
                        Notification(isschooluser.FirstOrDefault(), item.Message);
                }
                var messagequeue = db.MessageQueues.Where(m => m.MessageQueueId == item.MessageQueueId).FirstOrDefault();
                if (messagequeue != null)
                {
                    messagequeue.SentTime = DateTime.Now;

                    UpdateMessageQueue(messagequeue);
                }
            }
                if (item.StaffId != null)
                {
                    var StaffID = Convert.ToInt32(item.StaffId);

                    var isschooluser = db.SchoolUsers.Where(m => m.UserContactId == StaffID && ( m.ContactType.ContactType1.ToLower() == "teacher" || m.ContactType.ContactType1.ToLower() == "non-teaching"));

                    Notification(isschooluser.FirstOrDefault(), item.Message);
                   
                    var messagequeue = db.MessageQueues.Where(m => m.MessageQueueId == item.MessageQueueId).FirstOrDefault();
                    if (messagequeue != null)
                    {
                        messagequeue.SentTime = DateTime.Now;

                        UpdateMessageQueue(messagequeue);
                    }
                }

            }

        }

        public List<Relation> GetAllRelations(ref int totalcount, string Relation = "", int? RelationtypeId = null, int PageIndex = 0,
                   int PageSize = int.MaxValue)
        {
            var query = db.Relations.AsQueryable();
            if (!String.IsNullOrEmpty(Relation))
                query = query.Where(e => e.Relation1.ToLower().Contains(Relation.ToLower()));
            if (RelationtypeId != null && RelationtypeId > 0)
                query = query.Where(r => r.RelationTypeId == RelationtypeId);

            //            totalcount = query.Count();
            //  var Relations = new PagedList<KSModel.Models.Relation>(query.ToList(), PageIndex, PageSize);
            return query.ToList();
        }
        // GET: Reminder
        public void AddReminderQueue()
        {

            var dbname = GetSetCurrentSchoolDataBase(Request, Server);
            var datasource = Connectionstring(dbname);
            db = new KS_ChildEntities(datasource);
            SqlDataSource = SqlConnectionstring(dbname);


            var AutoFeeReminderSMS = GetorSetSchoolData("AutoFeeReminderSMS", "0", null).AttributeValue;
            var AutoFeeReminderNotification = GetorSetSchoolData("AutoFeeReminderNotification", "0", null).AttributeValue;
            var AutoFeeReminderSMSNonAppUser = GetorSetSchoolData("AutoFeeReminderSMSNonAppUser", "0", null).AttributeValue;
            var remindertypes = db.ReminderTypes.Where(m => m.IsActive!=null && m.IsActive==true).ToList();
            foreach (var item in remindertypes)
            {
                var FeeReminderslist = db.ReminderSetups.Where(m=>m.ReminderTypeId==item.ReminderTypeId);
                foreach (var feeReminderslist in FeeReminderslist.ToList())
                {
                    if (feeReminderslist.IsDaily!=null && feeReminderslist.IsDaily==true)
                    {
                        MethodInfo addMethod = this.GetType().GetMethod(feeReminderslist.ReminderType.ReminderMethod.ToString());
                        //    object result = addMethod.Invoke(this, new object[] { x, y });
                        object result = addMethod.Invoke(this, new object[] { feeReminderslist });

                        //typeof(MyType).GetMethod("add").Invoke(null, new[] { arg1, arg2 })
                        //FeeReminder(feeReminderslist);
                    }
                    else if(feeReminderslist.ReminderDate == DateTime.Now.Date)
                    {
                        // FeeReminder(feeReminderslist);
                        MethodInfo addMethod = this.GetType().GetMethod(feeReminderslist.ReminderType.ReminderMethod.ToString());
                        //    object result = addMethod.Invoke(this, new object[] { x, y });
                        object result = addMethod.Invoke(this, new object[] { feeReminderslist });
                    }
                }
            }
            #region fee
            //var FeeReminderSetup = db.ReminderSetups.ToList();
            //FeeReminderSetup = FeeReminderSetup.Where(m => m.ReminderDate == DateTime.Now.Date).ToList();
            //if (FeeReminderSetup.Count() > 0)
            //{
            //    foreach (var itemFeeReminderSetup in FeeReminderSetup)
            //    {
            //        FeeReceiptModel model = new FeeReceiptModel();
            //        var EWSDetail = GetorSetSchoolData("ShowEWSDiscount", "0", null).AttributeValue;
            //        var Istptownership = GetorSetSchoolData("ApplyTptOwnerShipInReceipt", "1", null).AttributeValue;
            //        var IsSeparateTpt = GetorSetSchoolData("SeparateTptFeeReceipt", "0", null).AttributeValue;

            //        var recepttype = GetAllReceiptTypes(ref count).Where(e => e.IsSchoolFee == true && e.IsForPreviousSession == false).FirstOrDefault();

            //        if (recepttype != null)
            //        {
            //            model.RecieptTypeId = recepttype.ReceiptTypeId;
            //        }

            //        int?[] groupfeetypeids;
            //        groupfeetypeids = GetAllGroupFeeHeads(ref count, ReceiptTypeId: model.RecieptTypeId).Select(f => f.GroupFeeTypeId).Distinct().ToArray();
            //        if (groupfeetypeids.Count() > 0)
            //        {
            //            int?[] FeeclassgrpIds = GetAllGroupFeeTypes(ref count).Where(f => groupfeetypeids.Contains(f.GroupFeeTypeId)).Select(f => f.FeeClassGroupId).Distinct().ToArray();
            //            if (FeeclassgrpIds.Count() > 0)
            //            {
            //                int?[] standardIds = GetAllFeeClassGroupDetails(ref count).Where(f => FeeclassgrpIds.Contains(f.FeeClassGroupId)).Select(f => f.StandardId).Distinct().ToArray();
            //                if (standardIds.Count() > 0)
            //                {
            //                    var classles = GetAllClassMasters(ref count).Where(f => standardIds.Contains(f.StandardId)).ToList();
            //                    if (classles.Count > 0)
            //                    {

            //                        //loop over class list
            //                        foreach (var itemclass in classles)
            //                        {
            //                            //get students list
            //                            int student_id = 0;
            //                            bool admnstat = true;
            //                            var Session = GetCurrentSession();

            //                            var studentStatusDetail = GetAllStudentStatusDetail(ref count);

            //                            var istransport = false;
            //                            if (recepttype != null)
            //                                if (recepttype.IsTransportFee != null)
            //                                    istransport = (bool)recepttype.IsTransportFee;
            //                            List<int?> studentidshavingtransportfacility = null;
            //                            if (istransport)
            //                            {
            //                                studentidshavingtransportfacility = GetAllTptFacilitys(ref count).Where(m => m.BoardingPoint != null || m.DropPointId != null).Select(m => m.StudentId).ToList();
            //                            }
            //                            IList<SelectListItem> studentlist = new List<SelectListItem>();
            //                            if (recepttype.ReceiptCode == 3)
            //                            {
            //                                studentlist = StudentListincludingleftbyStandardId(itemclass.ClassId, student_id, Session.SessionId, Studentidlist: studentidshavingtransportfacility, AdmnStatus: admnstat);

            //                            }
            //                            else
            //                            {

            //                                studentlist = StudentListincludingleftbyClassId(itemclass.ClassId, student_id, Session.SessionId, Studentidlist: studentidshavingtransportfacility, AdmnStatus: admnstat);
            //                            }
            //                            //Loop over StudentList
            //                            foreach (var itemstudent in studentlist)
            //                            {
            //                                if (string.IsNullOrEmpty(itemstudent.Value))
            //                                {

            //                                    continue;
            //                                }
            //                                var isschoolsuser = false;

            //                                var intstudentvalue = Convert.ToInt32(itemstudent.Value);
            //                                isschoolsuser = db.SchoolUsers.Where(m => m.UserContactId == intstudentvalue && m.ContactType.ContactType1.ToLower() == "student").Count() > 0 ? true : false;






            //                                //get feetype list
            //                                IList<FeeType> feetypelist = new List<FeeType>();

            //                                // parameters
            //                                Feetypebyclassandstudent(itemclass.ClassId, intstudentvalue, feetypelist);
            //                                var FeeTypeId = 0;
            //                                if (feetypelist.FirstOrDefault() != null)
            //                                    FeeTypeId = feetypelist.FirstOrDefault().FeeTypeId;
            //                                else
            //                                    continue;


            //                                //get feeperiod list
            //                                var feereceipts = GetAllFeeReceipts(ref count, StudentId: intstudentvalue, ReceiptTypeId: model.RecieptTypeId, SessionId: Session.SessionId).Where(f => f.IsCancelled == false || f.IsCancelled == null).ToList();
            //                                feereceipts = feereceipts.Where(f => f.ReceiptDate >= Session.StartDate && f.ReceiptDate <= Session.EndDate && f.FeePeriodId != null && (f.IsCancelled == false || f.IsCancelled == null)).ToList();
            //                                var lastperiodindexid = feereceipts.Count > 0 ? feereceipts.OrderByDescending(n => n.ReceiptId).FirstOrDefault() != null ? feereceipts.OrderByDescending(n => n.ReceiptId).FirstOrDefault().FeePeriod.FeePeriodIndex : 0 : 0;

            //                                IList<SelectListItem> feeperiodlist = new List<SelectListItem>();
            //                                // feeperiodlist.Add(new SelectListItem { Selected = false, Text = "--Select--", Value = "" });
            //                                // get Frequency by Fee Type
            //                                int Feeperiodidval = 0;
            //                                var FeeTypeFq = GetFeeTypeById(FeeTypeId);
            //                                if (FeeTypeFq != null)
            //                                {
            //                                    if (FeeTypeFq.FeeFrequency != null)
            //                                    {
            //                                        var FeePeriods = FeeTypeFq.FeeFrequency.FeePeriods.Where(m => m.FeePeriodIndex > lastperiodindexid).OrderBy(f => f.FeePeriodIndex).ToList();
            //                                        foreach (var feeprd in FeePeriods)
            //                                        {
            //                                            //if (!feeperiodids.Contains(feeprd.FeePeriodId))
            //                                            //{
            //                                            if (feeprd.FeePeriodDetails.Where(m => m.PeriodStartDate <= DateTime.Now && m.PeriodEndDate >= DateTime.Now).Count() > 0)
            //                                            {

            //                                                var feeperiodid = feeprd.FeePeriodId;



            //                                                var FeeReceiptDetailList = GetFeeHeadCalculationsBySp(StudentId: intstudentvalue,
            //                                                                                                              SessionId: Session.SessionId, ReceiptDate: DateTime.Now.Date.ToString("yyyy-MM-dd"),
            //                                                                                                              FeeTypeId: FeeTypeId,
            //                                                                                                              FeePeriodId: feeperiodid,
            //                                                                                                              ReceiptTypeId: model.RecieptTypeId,
            //                                                                                                              SessionStartDate: Session.StartDate.Value.ToString("yyyy-MM-dd"),
            //                                                                                                              SessionEndDate: Session.EndDate.Value.ToString("yyyy-MM-dd"),
            //                                                                                                              EWSDetail: EWSDetail,
            //                                                                                                              IsApplyTptOwnerShip: Istptownership,
            //                                                                                                              IsSeparateTptreceipt: IsSeparateTpt).ToList();


            //                                                var totalAmount = FeeReceiptDetailList.Where(f => f.GroupFeeHeadId != null
            //                                                                                && (f.HeadType == "gh" ||
            //                                                                                f.HeadType == "ao" || f.HeadType == "fp"))
            //                                                                                .Select(f => f.CalculatedHeadAmount).Sum();
            //                                                var concessionAmount = FeeReceiptDetailList.Where(f => f.GroupFeeHeadId != null
            //                                                                        && f.HeadType == "cn").Select(f => f.CalculatedHeadAmount).Sum();
            //                                                var PayableAmount = totalAmount - concessionAmount;

            //                                                var messagetext = itemFeeReminderSetup.ReminderTemplate.ReminderText.Replace("%FeeAmount%", Convert.ToString(PayableAmount));


            //                                                var schoolSettingSMSMobileNo = GetorSetSchoolData("AutoFeeReminderSmsMobileNoSelection", "SMS",
            //                                                                                   Desc: "SMS : Student SMS Mobile No , SMN: Student mobile no. , GMN : Guardian Mobile No")
            //                                                                                  .AttributeValue;

            //                                                string ContactNumber = ChooseStudentMobileNo(schoolSettingSMSMobileNo, intstudentvalue);

            //                                                if (AutoFeeReminderNotification == "1" && isschoolsuser && PayableAmount > 0)
            //                                                {
            //                                                    var MessageQueuemodel = new MessageQueue();
            //                                                    MessageQueuemodel.Message = messagetext;
            //                                                    // MessageQueuemodel.Recipient = ContactNumber;
            //                                                    MessageQueuemodel.CreatationTime = DateTime.Now;
            //                                                    MessageQueuemodel.StudentId = intstudentvalue;
            //                                                    var queueTypeid = 0;
            //                                                    var queueType = db.MessageQueueTypes.Where(m => m.QueueType.ToLower().Contains("notification")).FirstOrDefault();
            //                                                    if (queueType != null)
            //                                                        queueTypeid = queueType.QueueTypeId;

            //                                                    MessageQueuemodel.MessageQueueTypeId = queueTypeid;

            //                                                    InsertMessageQueue(MessageQueuemodel);
            //                                                }

            //                                                if (AutoFeeReminderSMS == "1" && PayableAmount > 0)
            //                                                {

            //                                                    if (AutoFeeReminderSMSNonAppUser == "1")
            //                                                    {
            //                                                        if (!isschoolsuser)
            //                                                        {
            //                                                            var MessageQueuemodel = new MessageQueue();
            //                                                            MessageQueuemodel.Message = messagetext;
            //                                                            MessageQueuemodel.Recipient = ContactNumber;
            //                                                            MessageQueuemodel.CreatationTime = DateTime.Now;
            //                                                            MessageQueuemodel.StudentId = intstudentvalue;
            //                                                            var queueTypeid = 0;
            //                                                            var queueType = db.MessageQueueTypes.Where(m => m.QueueType.ToLower().Contains("sms")).FirstOrDefault();
            //                                                            if (queueType != null)
            //                                                                queueTypeid = queueType.QueueTypeId;

            //                                                            MessageQueuemodel.MessageQueueTypeId = queueTypeid;
            //                                                            InsertMessageQueue(MessageQueuemodel);
            //                                                        }
            //                                                    }
            //                                                    else
            //                                                    {

            //                                                        var MessageQueuemodel = new MessageQueue();
            //                                                        MessageQueuemodel.Message = messagetext;
            //                                                        MessageQueuemodel.Recipient = ContactNumber;
            //                                                        MessageQueuemodel.CreatationTime = DateTime.Now;
            //                                                        MessageQueuemodel.StudentId = intstudentvalue;
            //                                                        var queueTypeid = 0;
            //                                                        var queueType = db.MessageQueueTypes.Where(m => m.QueueType.ToLower().Contains("sms")).FirstOrDefault();
            //                                                        if (queueType != null)
            //                                                            queueTypeid = queueType.QueueTypeId;

            //                                                        MessageQueuemodel.MessageQueueTypeId = queueTypeid;

            //                                                        InsertMessageQueue(MessageQueuemodel);
            //                                                    }
            //                                                }


            //                                            }
            //                                            //}
            //                                        }
            //                                    }
            //                                }
            //                            }
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            #endregion
        }
        public string TrimStudentNameWithSpace(string Name, Student student)
        {
            if (!string.IsNullOrEmpty(student.LName))
                student.LName = student.LName.Trim();
            if (!string.IsNullOrEmpty(student.FName))
                student.FName = student.FName.Trim();

            Name = student.FName + " " + student.LName;
            return Name;
        }
        public string TrimStaffNameWithSpace(string Name, Staff student)
        {
            if (!string.IsNullOrEmpty(student.LName))
                student.LName = student.LName.Trim();
            if (!string.IsNullOrEmpty(student.FName))
                student.FName = student.FName.Trim();

            Name = student.FName + " " + student.LName;
            return Name;
        }

        public void BirthDayWishes(ReminderSetup itemFeeReminderSetup)
        {
            var date211 = DateTime.Now.Day;
            int currentDate = Convert.ToInt32(DateTime.Now.Date.ToString("dd") );
            var monthofyear = DateTime.Now.Date.Month;

            var activestatus = db.StudentStatus.Where(m => m.StudentStatus.ToLower().Contains("active")).FirstOrDefault();
            var getStudentlist = db.Students.Where(x =>x.DOB.Value.Day == currentDate && x.DOB.Value.Month == monthofyear && x.StudentStatusId== activestatus.StudentStatusId);
            foreach (var getStudentInfo in getStudentlist.ToList())
            {
                string StuName = "";
                StuName = TrimStudentNameWithSpace(StuName, getStudentInfo);

                int date = getStudentInfo.DOB != null ? Convert.ToInt32(getStudentInfo.DOB.Value.ToString("dd")) : 0;
                int month = getStudentInfo.DOB != null ? (int)getStudentInfo.DOB.Value.Month : 0;

                var MessageQueuemodel = new MessageQueue();
                MessageQueuemodel.Message = itemFeeReminderSetup.ReminderTemplate.ReminderText.Replace("%Name%", StuName);
                // MessageQueuemodel.Recipient = ContactNumber;
                MessageQueuemodel.CreatationTime = DateTime.Now;
                MessageQueuemodel.StudentId = getStudentInfo.StudentId;
                var queueTypeid = 0;
                var queueType = db.MessageQueueTypes.Where(m => m.QueueType.ToLower().Contains("notification")).FirstOrDefault();
                if (queueType != null)
                    queueTypeid = queueType.QueueTypeId;

                MessageQueuemodel.MessageQueueTypeId = queueTypeid;

                var schooluser = db.SchoolUsers.Where(m => m.UserContactId == getStudentInfo.StudentId && m.ContactType.ContactType1.ToLower().Contains("student")).FirstOrDefault();
                if(schooluser!=null)
                InsertMessageQueue(MessageQueuemodel);
                var schoolSettingSMSMobileNo = GetorSetSchoolData("AutoBirthDayReminderSmsMobileNoSelection", "SMS",
                                                                                           Desc: "SMS : Student SMS Mobile No , SMN: Student mobile no. , GMN : Guardian Mobile No")
                                                                                          .AttributeValue;

                string ContactNumber = ChooseStudentMobileNo(schoolSettingSMSMobileNo, getStudentInfo.StudentId);

                MessageQueuemodel = new MessageQueue();
                MessageQueuemodel.Message = itemFeeReminderSetup.ReminderTemplate.ReminderText.Replace("%Name%", StuName);
                MessageQueuemodel.Recipient = ContactNumber;
                MessageQueuemodel.CreatationTime = DateTime.Now;
                MessageQueuemodel.StudentId = getStudentInfo.StudentId;
                queueTypeid = 0;
                queueType = db.MessageQueueTypes.Where(m => m.QueueType.ToLower().Contains("sms")).FirstOrDefault();
                if (queueType != null)
                    queueTypeid = queueType.QueueTypeId;

                MessageQueuemodel.MessageQueueTypeId = queueTypeid;
                InsertMessageQueue(MessageQueuemodel);
            }

            //staff
            var StaffBirthDaySmS = GetorSetSchoolData("StaffBirthDaySmS","0",Desc: "StaffBirthDaySmS").AttributeValue;
            var StaffBirthDayNotification = GetorSetSchoolData("StaffBirthDayNotification", "0", Desc: "StaffBirthDayNotification").AttributeValue;

            var getStafflist = db.Staffs.Where(x => x.DOB.Value.Day == currentDate && x.DOB.Value.Month == monthofyear && x.IsActive==true);
            foreach (var getStaffInfo in getStafflist.ToList())
            {
                string StuName = "";
                StuName = TrimStaffNameWithSpace(StuName, getStaffInfo);

                int date = getStaffInfo.DOB != null ? Convert.ToInt32(getStaffInfo.DOB.Value.ToString("dd")) : 0;
                int month = getStaffInfo.DOB != null ? (int)getStaffInfo.DOB.Value.Month : 0;

                var MessageQueuemodel = new MessageQueue();
                MessageQueuemodel.Message = itemFeeReminderSetup.ReminderTemplate.ReminderText.Replace("%Name%", StuName);
                // MessageQueuemodel.Recipient = ContactNumber;
                MessageQueuemodel.CreatationTime = DateTime.Now;
                MessageQueuemodel.StaffId = getStaffInfo.StaffId;
                var queueTypeid = 0;
                var queueType = db.MessageQueueTypes.Where(m => m.QueueType.ToLower().Contains("notification")).FirstOrDefault();
                if (queueType != null)
                    queueTypeid = queueType.QueueTypeId;

                MessageQueuemodel.MessageQueueTypeId = queueTypeid;

                var schooluser = db.SchoolUsers.Where(m => m.UserContactId == getStaffInfo.StaffId && (m.ContactType.ContactType1.ToLower().Contains("teacher") || m.ContactType.ContactType1.ToLower().Contains("non-teaching"))).FirstOrDefault();
                if (schooluser != null && StaffBirthDayNotification=="1")
                    InsertMessageQueue(MessageQueuemodel);
               

                string ContactNumber = ChooseStaffMobileNo(getStaffInfo.StaffId);

                MessageQueuemodel = new MessageQueue();
                MessageQueuemodel.Message = itemFeeReminderSetup.ReminderTemplate.ReminderText.Replace("%Name%", StuName);
                MessageQueuemodel.Recipient = ContactNumber;
                MessageQueuemodel.CreatationTime = DateTime.Now;
                MessageQueuemodel.StaffId = getStaffInfo.StaffId;
                queueTypeid = 0;
                queueType = db.MessageQueueTypes.Where(m => m.QueueType.ToLower().Contains("sms")).FirstOrDefault();
                if (queueType != null)
                    queueTypeid = queueType.QueueTypeId;

                MessageQueuemodel.MessageQueueTypeId = queueTypeid;
                if(StaffBirthDaySmS=="1")
                InsertMessageQueue(MessageQueuemodel);
            }
        }
        public void FeeReminder(ReminderSetup itemFeeReminderSetup) {

            //foreach (var itemFeeReminderSetup in FeeReminderSetup)
            //{
            var receiptcode = _IFeeService.GetAllReceiptTypeCodes(ref count).Where(m => m.ReceiptTypeCode1 == "3").FirstOrDefault() != null ? _IFeeService.GetAllReceiptTypeCodes(ref count).Where(m => m.ReceiptTypeCode1 == "3").FirstOrDefault().ReceiptTypeCodeId : 0;

            var AutoFeeReminderSMS = GetorSetSchoolData("AutoFeeReminderSMS", "0", null).AttributeValue;
            var AutoFeeReminderNotification = GetorSetSchoolData("AutoFeeReminderNotification", "0", null).AttributeValue;
            var AutoFeeReminderSMSNonAppUser = GetorSetSchoolData("AutoFeeReminderSMSNonAppUser", "0", null).AttributeValue;

            FeeReceiptModel model = new FeeReceiptModel();
                var EWSDetail = GetorSetSchoolData("ShowEWSDiscount", "0", null).AttributeValue;
                var Istptownership = GetorSetSchoolData("ApplyTptOwnerShipInReceipt", "1", null).AttributeValue;
                var IsSeparateTpt = GetorSetSchoolData("SeparateTptFeeReceipt", "0", null).AttributeValue;

                var recepttype = GetAllReceiptTypes(ref count).Where(e => e.IsSchoolFee == true && e.IsForPreviousSession == false).FirstOrDefault();

                if (recepttype != null)
                {
                    model.RecieptTypeId = recepttype.ReceiptTypeId;
                }

                int?[] groupfeetypeids;
                groupfeetypeids = GetAllGroupFeeHeads(ref count, ReceiptTypeId: model.RecieptTypeId).Select(f => f.GroupFeeTypeId).Distinct().ToArray();
                if (groupfeetypeids.Count() > 0)
                {
                    int?[] FeeclassgrpIds = GetAllGroupFeeTypes(ref count).Where(f => groupfeetypeids.Contains(f.GroupFeeTypeId)).Select(f => f.FeeClassGroupId).Distinct().ToArray();
                    if (FeeclassgrpIds.Count() > 0)
                    {
                        int?[] standardIds = GetAllFeeClassGroupDetails(ref count).Where(f => FeeclassgrpIds.Contains(f.FeeClassGroupId)).Select(f => f.StandardId).Distinct().ToArray();
                        if (standardIds.Count() > 0)
                        {
                            var classles = GetAllClassMasters(ref count).Where(f => standardIds.Contains(f.StandardId)).ToList();
                            if (classles.Count > 0)
                            {

                                //loop over class list
                                foreach (var itemclass in classles)
                                {
                                    //get students list
                                    int student_id = 0;
                                    bool admnstat = true;
                                    var Session = GetCurrentSession();

                                    var studentStatusDetail = GetAllStudentStatusDetail(ref count);

                                    var istransport = false;
                                    if (recepttype != null)
                                        if (recepttype.IsTransportFee != null)
                                            istransport = (bool)recepttype.IsTransportFee;
                                    List<int?> studentidshavingtransportfacility = null;
                                    if (istransport)
                                    {
                                        studentidshavingtransportfacility = GetAllTptFacilitys(ref count).Where(m => m.BoardingPoint != null || m.DropPointId != null).Select(m => m.StudentId).ToList();
                                    }
                                    IList<SelectListItem> studentlist = new List<SelectListItem>();
                                    if (recepttype.ReceiptCode == receiptcode)
                                    {
                                        studentlist = StudentListincludingleftbyStandardId(itemclass.ClassId, student_id, Session.SessionId, Studentidlist: studentidshavingtransportfacility, AdmnStatus: admnstat);

                                    }
                                    else
                                    {

                                        studentlist = StudentListincludingleftbyClassId(itemclass.ClassId, student_id, Session.SessionId, Studentidlist: studentidshavingtransportfacility, AdmnStatus: admnstat);
                                    }
                                    //Loop over StudentList
                                    foreach (var itemstudent in studentlist)
                                    {
                                        if (string.IsNullOrEmpty(itemstudent.Value))
                                        {

                                            continue;
                                        }
                                        var isschoolsuser = false;

                                        var intstudentvalue = Convert.ToInt32(itemstudent.Value);
                                        isschoolsuser = db.SchoolUsers.Where(m => m.UserContactId == intstudentvalue && m.ContactType.ContactType1.ToLower() == "student").Count() > 0 ? true : false;






                                        //get feetype list
                                        IList<FeeType> feetypelist = new List<FeeType>();

                                        // parameters
                                        Feetypebyclassandstudent(itemclass.ClassId, intstudentvalue, feetypelist);
                                        var FeeTypeId = 0;
                                        if (feetypelist.FirstOrDefault() != null)
                                            FeeTypeId = feetypelist.FirstOrDefault().FeeTypeId;
                                        else
                                            continue;


                                        //get feeperiod list
                                        var feereceipts = GetAllFeeReceipts(ref count, StudentId: intstudentvalue, ReceiptTypeId: model.RecieptTypeId, SessionId: Session.SessionId).Where(f => f.IsCancelled == false || f.IsCancelled == null).ToList();
                                        feereceipts = feereceipts.Where(f => f.ReceiptDate >= Session.StartDate && f.ReceiptDate <= Session.EndDate && f.FeePeriodId != null && (f.IsCancelled == false || f.IsCancelled == null)).ToList();
                                        var lastperiodindexid = feereceipts.Count > 0 ? feereceipts.OrderByDescending(n => n.ReceiptId).FirstOrDefault() != null ? feereceipts.OrderByDescending(n => n.ReceiptId).FirstOrDefault().FeePeriod.FeePeriodIndex : 0 : 0;

                                        IList<SelectListItem> feeperiodlist = new List<SelectListItem>();
                                        // feeperiodlist.Add(new SelectListItem { Selected = false, Text = "--Select--", Value = "" });
                                        // get Frequency by Fee Type
                                        int Feeperiodidval = 0;
                                        var FeeTypeFq = GetFeeTypeById(FeeTypeId);
                                        if (FeeTypeFq != null)
                                        {
                                            if (FeeTypeFq.FeeFrequency != null)
                                            {
                                                var FeePeriods = FeeTypeFq.FeeFrequency.FeePeriods.Where(m => m.FeePeriodIndex > lastperiodindexid).OrderBy(f => f.FeePeriodIndex).ToList();
                                                foreach (var feeprd in FeePeriods)
                                                {
                                                    //if (!feeperiodids.Contains(feeprd.FeePeriodId))
                                                    //{
                                                    if (feeprd.FeePeriodDetails.Where(m => m.PeriodStartDate <= DateTime.Now && m.PeriodEndDate >= DateTime.Now).Count() > 0)
                                                    {

                                                        var feeperiodid = feeprd.FeePeriodId;



                                                        var FeeReceiptDetailList = GetFeeHeadCalculationsBySp(StudentId: intstudentvalue,
                                                                                                                      SessionId: Session.SessionId, ReceiptDate: DateTime.Now.Date.ToString("yyyy-MM-dd"),
                                                                                                                      FeeTypeId: FeeTypeId,
                                                                                                                      FeePeriodId: feeperiodid,
                                                                                                                      ReceiptTypeId: model.RecieptTypeId,
                                                                                                                      SessionStartDate: Session.StartDate.Value.ToString("yyyy-MM-dd"),
                                                                                                                      SessionEndDate: Session.EndDate.Value.ToString("yyyy-MM-dd"),
                                                                                                                      EWSDetail: EWSDetail,
                                                                                                                      IsApplyTptOwnerShip: Istptownership,
                                                                                                                      IsSeparateTptreceipt: IsSeparateTpt).ToList();


                                                        var totalAmount = FeeReceiptDetailList.Where(f => f.GroupFeeHeadId != null
                                                                                        && (f.HeadType == "gh" ||
                                                                                        f.HeadType == "ao" || f.HeadType == "fp"))
                                                                                        .Select(f => f.CalculatedHeadAmount).Sum();
                                                        var concessionAmount = FeeReceiptDetailList.Where(f => f.GroupFeeHeadId != null
                                                                                && f.HeadType == "cn").Select(f => f.CalculatedHeadAmount).Sum();
                                                        var PayableAmount = totalAmount - concessionAmount;

                                                        var messagetext = itemFeeReminderSetup.ReminderTemplate.ReminderText.Replace("%FeeAmount%", Convert.ToString(PayableAmount));


                                                        var schoolSettingSMSMobileNo = GetorSetSchoolData("AutoFeeReminderSmsMobileNoSelection", "SMS",
                                                                                           Desc: "SMS : Student SMS Mobile No , SMN: Student mobile no. , GMN : Guardian Mobile No")
                                                                                          .AttributeValue;

                                                        string ContactNumber = ChooseStudentMobileNo(schoolSettingSMSMobileNo, intstudentvalue);

                                                        if (AutoFeeReminderNotification == "1" && isschoolsuser && PayableAmount > 0)
                                                        {
                                                            var MessageQueuemodel = new MessageQueue();
                                                            MessageQueuemodel.Message = messagetext;
                                                            // MessageQueuemodel.Recipient = ContactNumber;
                                                            MessageQueuemodel.CreatationTime = DateTime.Now;
                                                            MessageQueuemodel.StudentId = intstudentvalue;
                                                            var queueTypeid = 0;
                                                            var queueType = db.MessageQueueTypes.Where(m => m.QueueType.ToLower().Contains("notification")).FirstOrDefault();
                                                            if (queueType != null)
                                                                queueTypeid = queueType.QueueTypeId;

                                                            MessageQueuemodel.MessageQueueTypeId = queueTypeid;

                                                            InsertMessageQueue(MessageQueuemodel);
                                                        }

                                                        if (AutoFeeReminderSMS == "1" && PayableAmount > 0)
                                                        {

                                                            if (AutoFeeReminderSMSNonAppUser == "1")
                                                            {
                                                                if (!isschoolsuser)
                                                                {
                                                                    var MessageQueuemodel = new MessageQueue();
                                                                    MessageQueuemodel.Message = messagetext;
                                                                    MessageQueuemodel.Recipient = ContactNumber;
                                                                    MessageQueuemodel.CreatationTime = DateTime.Now;
                                                                    MessageQueuemodel.StudentId = intstudentvalue;
                                                                    var queueTypeid = 0;
                                                                    var queueType = db.MessageQueueTypes.Where(m => m.QueueType.ToLower().Contains("sms")).FirstOrDefault();
                                                                    if (queueType != null)
                                                                        queueTypeid = queueType.QueueTypeId;

                                                                    MessageQueuemodel.MessageQueueTypeId = queueTypeid;
                                                                    InsertMessageQueue(MessageQueuemodel);
                                                                }
                                                            }
                                                            else
                                                            {

                                                                var MessageQueuemodel = new MessageQueue();
                                                                MessageQueuemodel.Message = messagetext;
                                                                MessageQueuemodel.Recipient = ContactNumber;
                                                                MessageQueuemodel.CreatationTime = DateTime.Now;
                                                                MessageQueuemodel.StudentId = intstudentvalue;
                                                                var queueTypeid = 0;
                                                                var queueType = db.MessageQueueTypes.Where(m => m.QueueType.ToLower().Contains("sms")).FirstOrDefault();
                                                                if (queueType != null)
                                                                    queueTypeid = queueType.QueueTypeId;

                                                                MessageQueuemodel.MessageQueueTypeId = queueTypeid;

                                                                InsertMessageQueue(MessageQueuemodel);
                                                            }
                                                        }


                                                    }
                                                    //}
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            //}
        }
        public List<KSModel.Models.ContactInfo> GetAllContactInfos(ref int totalcount, string ContactInfo = null, int? ContactInfoTypeId = null, int? ContactId = null,
                   int? ContactTypeId = null, bool? status = false, bool? Isdefault = false, int PageIndex = 0, int PageSize = int.MaxValue)
        {


            var query = db.ContactInfoes.ToList();
            if (!string.IsNullOrEmpty(ContactInfo))
                query = query.Where(c => c.ContactInfo1 == ContactInfo).ToList();
            if (ContactInfoTypeId != null && ContactInfoTypeId > 0)
                query = query.Where(e => e.ContactInfoTypeId == ContactInfoTypeId).ToList();
            if (ContactId != null && ContactId > 0)
                query = query.Where(e => e.ContactId == ContactId).ToList();
            if (ContactTypeId != null && ContactTypeId > 0)
                query = query.Where(e => e.ContactTypeId == ContactTypeId).ToList();
            if (status == true)
                query = query.Where(e => e.Status == status).ToList();
            if (Isdefault == true)
                query = query.Where(e => e.IsDefault == Isdefault).ToList();

            totalcount = query.Count;
          //  var ContactInfoes = new PagedList<KSModel.Models.ContactInfo>(query, PageIndex, PageSize);
         //   return ContactInfoes;
            // var ContactInfoes = new PagedList<KSModel.Models.ContactInfo>(query, PageIndex, PageSize);
            return query.ToList();
        }
        public List<Guardian> GetAllGuardians(ref int totalcount, string FirstName = "", string LastName = "", string FullName = "",
                    int? QualificationId = null, int? OccupationId = null, int? GuardianTypeId = null, int? StudentId = null,
                    string Income = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Guardians.ToList();
            if (!String.IsNullOrEmpty(FirstName))
                query = query.Where(e => e.FirstName.ToLower().Contains(FirstName.ToLower())).ToList();
            if (!String.IsNullOrEmpty(LastName))
                query = query.Where(e => e.LastName.ToLower().Contains(LastName.ToLower())).ToList();
            if (!String.IsNullOrEmpty(FullName))
                query = query.Where(e => (e.FirstName + e.LastName).ToLower().Contains(FullName.ToLower())).ToList();
            if (!String.IsNullOrEmpty(Income))
                query = query.Where(e => e.IncomeSlabId == Income).ToList();
            if (QualificationId != null && QualificationId > 0)
                query = query.Where(e => e.QualificationId == QualificationId).ToList();
            if (OccupationId != null && OccupationId > 0)
                query = query.Where(e => e.OccupationId == OccupationId).ToList();
            if (GuardianTypeId != null && GuardianTypeId > 0)
                query = query.Where(e => e.GuardianTypeId == GuardianTypeId).ToList();
            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();

            totalcount = query.Count;
          //  var Guardians = new PagedList<KSModel.Models.Guardian>(query, PageIndex, PageSize);
           // return Guardians;
            //  var Guardians = new PagedList<KSModel.Models.Guardian>(query, PageIndex, PageSize);
            return query.ToList();
        }

        public string ChooseStudentMobileNo(string SendMessageMobileNoSelection_Id = "", int? studentId = null, string contactinfo = "")
        {
            //check selected number from popup to send message
            if (!string.IsNullOrEmpty(SendMessageMobileNoSelection_Id))
            {
                switch (SendMessageMobileNoSelection_Id)
                {
                    //choose student mobile number if have
                    case "SMN":
                        var StudentMobileNo = GetAllContactInfos(ref count).Where(m => m.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                    && m.ContactType.ContactType1.ToLower() == "student"
                                                    && m.ContactId == studentId
                                                    && m.Status == true && m.IsDefault == true).FirstOrDefault();
                        if (StudentMobileNo != null)
                            contactinfo = StudentMobileNo.ContactInfo1;
                        break;


                    //choose student SMS mobile number if have
                    case "SMS":
                        var StudentSMSMobileNo = GetStudentById((int)studentId);
                        if (StudentSMSMobileNo != null)
                            contactinfo = StudentSMSMobileNo.SMSMobileNo;
                        break;


                    //choose custody guardian mobile number if have
                    case "GMN":
                        int ContactId = 0;
                        var guardianInfo = new Guardian();
                        var StuCustodyRight = GetStudentById((int)studentId);
                        if (StuCustodyRight != null)
                        {
                            guardianInfo = GetAllGuardians(ref count, StudentId: studentId).FirstOrDefault();
                            if (guardianInfo != null)
                                ContactId = guardianInfo.GuardianId;

                            if (StuCustodyRight.CustodyRight != null)
                            {
                                switch (StuCustodyRight.CustodyRight.CustodyRight1.ToLower())
                                {
                                    case "father":
                                        guardianInfo = GetAllGuardians(ref count, StudentId: studentId)
                                                        .Where(x => x.Relation.Relation1.ToLower() == "father").FirstOrDefault();
                                        if (guardianInfo != null)
                                            ContactId = guardianInfo.GuardianId;
                                        break;

                                    case "mother":
                                        guardianInfo = GetAllGuardians(ref count, StudentId: studentId)
                                                       .Where(x => x.Relation.Relation1.ToLower() == "mother").FirstOrDefault();
                                        if (guardianInfo != null)
                                            ContactId = guardianInfo.GuardianId;
                                        break;

                                    case "guardian":
                                        guardianInfo = GetAllGuardians(ref count, StudentId: studentId)
                                                       .Where(x => x.Relation.Relation1.ToLower() == "guardian").FirstOrDefault();
                                        if (guardianInfo != null)
                                            ContactId = guardianInfo.GuardianId;
                                        break;
                                }
                            }

                            if (ContactId > 0)
                            {
                                var getContactInfo = GetAllContactInfos(ref count)
                                                    .Where(x => x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                    && x.ContactType.ContactType1.ToLower() == "guardian"
                                                    && x.ContactId == ContactId
                                                    && x.Status == true && x.IsDefault == true).FirstOrDefault();

                                if (getContactInfo != null)
                                    contactinfo = getContactInfo.ContactInfo1;
                            }
                        }
                        break;
                }
            }
            return contactinfo;
        }
        public string ChooseStaffMobileNo( int? studentId = null, string contactinfo = "")
        {
            //check selected number from popup to send message
            
                        var StudentMobileNo = GetAllContactInfos(ref count).Where(m => m.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                    && (m.ContactType.ContactType1.ToLower() == "teacher" || m.ContactType.ContactType1.ToLower() == "non-teaching")
                                                    && m.ContactId == studentId
                                                    && m.Status == true && m.IsDefault == true).FirstOrDefault();
                        if (StudentMobileNo != null)
                            contactinfo = StudentMobileNo.ContactInfo1;
                      


                   


                    
               
            
            return contactinfo;
        }

        private void Feetypebyclassandstudent(int ClassId, int StudentId, IList<FeeType> feetypelist)
        {

            int? FeeClassGrouptTypeId = null;
            var admnstandardId = 0;
            if (StudentId > 0)
            {
                var student = GetStudentById(StudentId: StudentId);
                admnstandardId = (int)student.AdmnStandardId;
                bool? IsHostler = student.IsHostler != null ? student.IsHostler : false;

                if ((bool)IsHostler)
                {
                    FeeClassGrouptTypeId = GetAllFeeClassGroupTypes(ref count).Where(m => m.ClassGroupType.ToLower() == "hostler").FirstOrDefault() != null ? (int)GetAllFeeClassGroupTypes(ref count).Where(m => m.ClassGroupType.ToLower() == "hostler").FirstOrDefault().FeeClassGroupTypeId : FeeClassGrouptTypeId;
                }
            }
            // get class standard
            if (ClassId > 0)
            {
                var classmaster = GetClassMasterById(ClassId);
                if (classmaster != null)
                {
                    var standard = classmaster.StandardId;
                    // get fee class group by standard

                    var feeclassgroupdet = new List<FeeClassGroupDetail>();
                    if (FeeClassGrouptTypeId > 0)
                        feeclassgroupdet = GetAllFeeClassGroupDetails(ref count, StandardId: standard).Where(m => m.FeeClassGroup.FeeClassGroupTypeId == FeeClassGrouptTypeId).ToList();
                    else
                        feeclassgroupdet = GetAllFeeClassGroupDetails(ref count, StandardId: standard).ToList();

                    if (feeclassgroupdet.Count() > 0)
                    {

                        foreach (var item1 in feeclassgroupdet)
                        {


                            // get group fee type by Fee Class Group
                            var groupfeetype = GetAllGroupFeeTypes(ref count, FeeClassGroupId: item1.FeeClassGroupId).ToList();
                            // fee type ids array
                            var feetypeids = groupfeetype.Select(g => (int)g.FeeTypeId).ToArray();
                            // get fee types
                            var feetypes = GetFeeTypesByClass(feetypeids);
                            // loop on it
                            foreach (var item in feetypes)
                            {
                                FeeType FeeType = new FeeType();
                                FeeType.FeeType1 = item.FeeType1;
                                FeeType.FeeTypeId = item.FeeTypeId;
                                feetypelist.Add(FeeType);
                            }
                        }
                    }
                }
            }
            else
            {
                var standard = admnstandardId;
                // get fee class group by standard
                var feeclassgroupdet = GetAllFeeClassGroupDetails(ref count, StandardId: standard).Where(m => m.FeeClassGroup.FeeClassGroupTypeId == FeeClassGrouptTypeId).FirstOrDefault();
                if (feeclassgroupdet != null)
                {
                    // get group fee type by Fee Class Group
                    var groupfeetype = GetAllGroupFeeTypes(ref count, FeeClassGroupId: feeclassgroupdet.FeeClassGroupId).ToList();
                    // fee type ids array
                    var feetypeids = groupfeetype.Select(g => (int)g.FeeTypeId).ToArray();
                    // get fee types
                    var feetypes = GetFeeTypesByClass(feetypeids);
                    // loop on it
                    foreach (var item in feetypes)
                    {
                        FeeType FeeType = new FeeType();
                        FeeType.FeeType1 = item.FeeType1;
                        FeeType.FeeTypeId = item.FeeTypeId;
                        feetypelist.Add(FeeType);
                    }
                }
            }
        }


        #region services
        //public virtual ErrorLog InsertLog(LogLevel logLevel, string shortMessage, string fullMessage = "", SchoolUser user = null)
        //{
        //    int? userid = null;
        //    var log = new ErrorLog
        //    {
        //        LogLevelId = Convert.ToInt32(logLevel),
        //        ShortMessage = shortMessage,
        //        FullMessage = fullMessage,
        //        IpAddress = _IWebHelper.GetCurrentIpAddress(),
        //        UserId = user != null && user.UserId != null && user.UserId != 0 ? user.UserId : userid,
        //        PageUrl = _IWebHelper.GetThisPageUrl(true),
        //        ReferrerUrl = _IWebHelper.GetUrlReferrer(),
        //        CreatedOnUtc = DateTime.UtcNow
        //    };

        //    db.ErrorLogs.Add(log);

        //    db.SaveChanges();

        //    return log;
        //}
        public string Connectionstring(string database, string password = "")
        {
            string DataSource = Convert.ToString(WebConfigurationManager.AppSettings["DataSource"]);
            string Password = Convert.ToString(WebConfigurationManager.AppSettings["Password"]);

            // check database
            string userid = database;
            if (database == "Dev-KS_School" || database == "3is_demo" || string.IsNullOrEmpty(database) || database == "g1" || database == "VPSLehra")
                userid = "sa";
            else if (database == "KsmartChild" || database == "3ischool_Uk")
            {
                userid = "sa";
                Password = "sa@123";
            }
            else if (database.Contains("3is"))
                userid = "3isu_" + database.Split('_')[1];
            else if (database.Contains("ks"))
            {
                userid = "ksu_" + database.Split('_')[1];
                Password = "sa@321";
            }
            else
                userid = "tstu_" + database.Split('_')[1];

            //Build an SQL connection string
            SqlConnectionStringBuilder sqlString = new SqlConnectionStringBuilder()
            {
                DataSource = DataSource, // Server name
                InitialCatalog = database,  //Database
                UserID = userid,         //Username
                Password = Password,  //Password
                MultipleActiveResultSets = true,
            };
            EntityConnectionStringBuilder entitystring = new EntityConnectionStringBuilder
            {
                Provider = "System.Data.SqlClient",
                Metadata = "res://*/Models.KSModel.csdl|res://*/Models.KSModel.ssdl|res://*/Models.KSModel.msl",
                ProviderConnectionString = sqlString.ToString()
            };

            return entitystring.ToString();
        }

        public string SqlConnectionstring(string database, string password = "")
        {
            string DataSource = Convert.ToString(WebConfigurationManager.AppSettings["DataSource"]);
            string Password = Convert.ToString(WebConfigurationManager.AppSettings["Password"]);

            // check database
            // check database
            string userid = database;
            if (database == "Dev-KS_School" || database == "3is_demo" || string.IsNullOrEmpty(database) || database == "g1" || database == "VPSLehra")
                userid = "sa";
            else if (database == "KsmartChild" || database == "3ischool_Uk")
            {
                userid = "sa";
                Password = "sa@123";
            }
            else if (database.Contains("3is"))
                userid = "3isu_" + database.Split('_')[1];
            else if (database.Contains("ks"))
            {
                userid = "ksu_" + database.Split('_')[1];
                Password = "sa@321";
            }
            else
            {
                if (database.Split('_').Count() == 2)
                    userid = "tstu_" + database.Split('_')[1];
            }

            //Build an SQL connection string
            SqlConnectionStringBuilder sqlString = new SqlConnectionStringBuilder()
            {
                DataSource = DataSource, // Server name
                InitialCatalog = database,  //Database
                UserID = userid,         //Username
                Password = Password,  //Password
                MultipleActiveResultSets = true,
            };


            return sqlString.ToString();
        }

        public FeeType GetFeeTypeById(int FeeTypeId, bool IsTrack = false)
        {
            if (FeeTypeId == 0)
                throw new ArgumentNullException("FeeType");

            var FeeType = new FeeType();
            if (IsTrack)
                FeeType = (from s in db.FeeTypes where s.FeeTypeId == FeeTypeId select s).FirstOrDefault();
            else
                FeeType = (from s in db.FeeTypes.AsNoTracking() where s.FeeTypeId == FeeTypeId select s).FirstOrDefault();

            return FeeType;
        }
        public Session GetCurrentSession(int? DaysBeforeAdmnOpening = null)
        {
            Session crntsession = new Session();
            //DateTime? CurrentSessionFromDate;
            //DateTime? CurrentSessionToDate;
            int count = 0;
            // today date
            var today = DateTime.UtcNow;
            //if (today.Month > 3) // means new session start
            //{
            //    CurrentSessionFromDate = Convert.ToDateTime("01-April-" + today.Year);
            //    CurrentSessionToDate = Convert.ToDateTime("31-March-" + (today.Year + 1));
            //    crntsession = GetAllSessions(ref count, StartDate: CurrentSessionFromDate, Enddate: CurrentSessionToDate).FirstOrDefault();
            //}
            //else // prev session
            //{
            //    CurrentSessionFromDate = Convert.ToDateTime("01-April-" + (today.Year - 1));
            //    CurrentSessionToDate = Convert.ToDateTime("31-March-" + today.Year);
            crntsession = GetAllSessions(ref count).Where(p => p.StartDate.Value.Date <= today.Date && p.EndDate.Value.Date >= today.Date).FirstOrDefault();
            //}

            if (DaysBeforeAdmnOpening > 0)
            {
                var currentdate = DateTime.UtcNow.Date;
                var daysdiff = crntsession.EndDate.Value.Date - currentdate;
                if (daysdiff.Days > DaysBeforeAdmnOpening)
                    return crntsession;
                else
                {
                    // get next session
                    var allsessions = GetAllSessions(ref count);
                    var nextsession = allsessions.Where(s => s.SessionId > crntsession.SessionId).OrderBy(s => s.SessionId).FirstOrDefault();
                    return nextsession;
                }
            }

            return crntsession;
        }
        public List<KSModel.Models.Session> GetAllSessions(ref int totalcount, string Session = "", DateTime? StartDate = null,
                   DateTime? Enddate = null, int PageIndex = 0, int PageSize = int.MaxValue, bool Isdeafult = false, int SessionId = 0)
        {
            var query = db.Sessions.ToList();
            if (Isdeafult == true)
            {
                query = query.Where(m => m.IsDefualt == true).ToList();
            }
            if (SessionId > 0)
            {
                query = query.Where(m => m.SessionId == SessionId).ToList();
            }
            if (!String.IsNullOrEmpty(Session))
                query = query.Where(e => e.Session1.ToLower() == Session.ToLower()).ToList();
            if (StartDate.HasValue)
                query = query.Where(s => s.StartDate <= StartDate).ToList();
            if (Enddate.HasValue)
                query = query.Where(s => s.EndDate >= Enddate).ToList();

            totalcount = query.Count;
           // var Sessions = new PagedList<KSModel.Models.Session>(query, PageIndex, PageSize);
            //return Sessions;
            //   var Sessions = new PagedList<KSModel.Models.Session>(query, PageIndex, PageSize);
            return query.ToList();
        }

        public List<KSModel.Models.ReminderSetup> GetAllFeeReminderSetup(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ReminderSetups;


            totalcount = query.Count();
            // var FeeReminderSetup = new PagedList<KSModel.Models.FeeReminderSetup>(query, PageIndex, PageSize);
            return query.ToList();
        }
        public List<KSModel.Models.MessageQueue> GetAllMessageQueue(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.MessageQueues;


            totalcount = query.Count();
           // var MessageQueue = new PagedList<KSModel.Models.MessageQueue>(query, PageIndex, PageSize);
            //  var MessageQueue = new PagedList<KSModel.Models.MessageQueue>(query, PageIndex, PageSize);
            return query.ToList();
        }
        public void UpdateMessageQueue(KSModel.Models.MessageQueue MessageQueue)
        {
            if (MessageQueue == null)
                throw new ArgumentNullException("MessageQueue");

            db.Entry(MessageQueue).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }
        public void InsertMessageQueue(KSModel.Models.MessageQueue MessageQueue)
        {
            if (MessageQueue == null)
                throw new ArgumentNullException("MessageQueue");

            db.MessageQueues.Add(MessageQueue);
            db.SaveChanges();
        }
        public List<KSModel.Models.ClassMaster> GetAllClassMasters(ref int totalcount, int? StandardId = null,
                   int? SectionId = null, string ClassName = "", decimal? StandardIndex = null, int PageIndex = 0, int PageSize = int.MaxValue, IEnumerable<Sort> sort = null)
        {
            var query = db.ClassMasters.ToList().AsQueryable().ToList();
            if (StandardId != null && StandardId > 0)
                query = query.Where(e => e.StandardId == StandardId).ToList();
            if (SectionId != null && SectionId > 0)
                query = query.Where(e => e.SectionId == SectionId).ToList();
            if (ClassName != "" && ClassName != null)
                query = query.Where(e => e.Class == ClassName).ToList();
            if (StandardIndex != null && StandardIndex > 0)
                query = query.Where(e => e.StandardMaster.StandardIndex == StandardIndex).ToList();
            query = query.OrderBy(c => c.StandardMaster.StandardIndex).ToList();

            totalcount = query.Count;
            //var ClassMasters = new PagedList<KSModel.Models.ClassMaster>(query, PageIndex, PageSize);
            return query.ToList();
        }
        public List<FeeClassGroupDetail> GetAllFeeClassGroupDetails(ref int totalcount, int? FeeClassGroupId = null,
                   int? StandardId = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.FeeClassGroupDetails.ToList();
            if (FeeClassGroupId != null && FeeClassGroupId > 0)
                query = query.Where(e => e.FeeClassGroupId == FeeClassGroupId).ToList();
            if (StandardId != null && StandardId > 0)
                query = query.Where(e => e.StandardId == StandardId).ToList();

            totalcount = query.Count;
           // var FeeClassGroupDetails = new PagedList<KSModel.Models.FeeClassGroupDetail>(query, PageIndex, PageSize);
           // return FeeClassGroupDetails;
            // var FeeClassGroupDetails = new PagedList<KSModel.Models.FeeClassGroupDetail>(query, PageIndex, PageSize);
            return query.ToList();
        }


        public List<GroupFeeType> GetAllGroupFeeTypes(ref int totalcount, int? FeeTypeId = null, int? FeeClassGroupId = null, bool? TakeAddOns = null,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.GroupFeeTypes.ToList();
            if (FeeTypeId != null && FeeTypeId > 0)
                query = query.Where(e => e.FeeTypeId == FeeTypeId).ToList();
            if (FeeClassGroupId != null && FeeClassGroupId > 0)
                query = query.Where(e => e.FeeClassGroupId == FeeClassGroupId).ToList();
            if (TakeAddOns != null)
                query = query.Where(g => g.TakeAddOns == TakeAddOns).ToList();

            totalcount = query.Count;
            //var GroupFeeTypes = new PagedList<KSModel.Models.GroupFeeType>(query, PageIndex, PageSize);
            return query.ToList();
        }
        public KSModel.Models.ClassMaster GetClassMasterById(int ClassMasterId, bool istrack = false)
        {
            if (ClassMasterId == 0)
                throw new ArgumentNullException("ClassMaster");
            var ClassMaster = new ClassMaster();
            if (istrack)
                ClassMaster = (from s in db.ClassMasters where s.ClassId == ClassMasterId select s).FirstOrDefault();
            else
                ClassMaster = (from s in db.ClassMasters.AsNoTracking() where s.ClassId == ClassMasterId select s).FirstOrDefault();
            return ClassMaster;
        }

        public List<GroupFeeHead> GetAllGroupFeeHeads(ref int totalcount, int? GroupFeeTypeId = null, int? FeeHeadId = null, DateTime? EffectiveDate = null,
                    bool? IsCommon = null, bool? IsPending = null, bool? IsLateFee = null, int PageIndex = 0, int PageSize = int.MaxValue, int? ReceiptTypeFeeHeadId = null, int? ReceiptTypeId = null)
        {
            var query = db.GroupFeeHeads.ToList();
            if (FeeHeadId != null && FeeHeadId > 0)
                query = query.Where(e => e.FeeHeadId == FeeHeadId).ToList();
            if (GroupFeeTypeId != null && GroupFeeTypeId > 0)
                query = query.Where(e => e.GroupFeeTypeId == GroupFeeTypeId).ToList();

            if (IsCommon != null)
                query = query.Where(e => e.IsCommon == IsCommon).ToList();

            if (IsPending != null)
                query = query.Where(e => e.IsPending == IsPending).ToList();

            if (IsLateFee != null)
                query = query.Where(e => e.IsLateFee == IsLateFee).ToList();

            if (ReceiptTypeFeeHeadId != null)
                query = query.Where(f => f.ReceiptTypeFeeHeadId == ReceiptTypeFeeHeadId).ToList();

            if (ReceiptTypeId != null)
                query = query.Where(f => f.ReceiptTypeId == ReceiptTypeId).ToList();

            totalcount = query.Count;
         //   var GroupFeeHeads = new PagedList<KSModel.Models.GroupFeeHead>(query, PageIndex, PageSize);
          //  return GroupFeeHeads;
            //  var GroupFeeHeads = new PagedList<KSModel.Models.GroupFeeHead>(query, PageIndex, PageSize);
            return query.ToList();
        }
        public List<TptFacility> GetAllTptFacilitys(ref int totalcount, int? TptTypeDetailId = null, int? BusRoutrId = null,
               int? BoardingPointId = null, int? VehicletypeId = null, string VehicleNo = "", bool? ParkingRequired = null,
               int? StudentId = null, bool? Status = null, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.TptFacilities.ToList();
            if (TptTypeDetailId != null && TptTypeDetailId > 0)
                query = query.Where(e => e.TptTypeDetailId == TptTypeDetailId).ToList();
            if (BusRoutrId != null && BusRoutrId > 0)
                query = query.Where(e => e.BusRouteId == BusRoutrId).ToList();
            if (BoardingPointId != null && BoardingPointId > 0)
                query = query.Where(e => e.BoardingPointId == BoardingPointId).ToList();
            if (VehicletypeId != null && VehicletypeId > 0)
                query = query.Where(e => e.VehicleTypeId == VehicletypeId).ToList();
            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();
            if (!String.IsNullOrEmpty(VehicleNo))
                query = query.Where(e => e.VehicleNo.ToLower() == VehicleNo.ToLower()).ToList();
            if (ParkingRequired != null)
                query = query.Where(e => e.ParkingRequired == ParkingRequired).ToList();
            if (Status != null)
                query = query.Where(e => e.Status == Status).ToList();
            // order by date
            query = query.OrderByDescending(t => t.EffectiveDate).OrderByDescending(t => t.TptFacilityId).ToList();

            totalcount = query.Count;
           // var TptFacilities = new PagedList<KSModel.Models.TptFacility>(query, PageIndex, PageSize);
           // return TptFacilities;
            //  var TptFacilities = new PagedList<KSModel.Models.TptFacility>(query, PageIndex, PageSize);
            return query.ToList();
        }
        public List<StudentStatusDetail> GetAllStudentStatusDetail(ref int totalcount, int? StudentId = null, DateTime? StatusChangeDate = null, int? StudentStatusId = null, int PageIndex = 0,
                   int PageSize = int.MaxValue)
        {
            var query = db.StudentStatusDetails.ToList();

            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();

            if (StatusChangeDate.HasValue)
                query = query.Where(e => e.StatusChangeDate == StatusChangeDate).ToList();

            if (StudentStatusId != null && StudentStatusId > 0)
                query = query.Where(e => e.StudentStatusId == StudentStatusId).ToList();

            totalcount = query.Count;
           // var StudentStatusDetail = new PagedList<KSModel.Models.StudentStatusDetail>(query, PageIndex, PageSize);
           // return StudentStatusDetail;
            // var StudentStatusDetail = new PagedList<KSModel.Models.StudentStatusDetail>(query, PageIndex, PageSize);
            return query.ToList();
        }
        public List<FeeReceipt> GetAllFeeReceipts(ref int totalcount, int? ReceiptNo = null, DateTime? ReceiptFromDate = null,
                   DateTime? ReceiptToDate = null, int? StudentId = null, int? FeePeriodId = null, int? PaymentModeId = null,
                   decimal? PaidAmountFrom = null, decimal? PaidAmountTo = null, int?[] StudentIds = null,
                   int PageIndex = 0, int PageSize = int.MaxValue, int? ReceiptTypeId = null, int? SessionId = null)
        {
            var query = db.FeeReceipts.ToList();
            if (ReceiptTypeId != null && ReceiptTypeId > 0)
                query = query.Where(e => e.ReceiptTypeId == ReceiptTypeId).ToList();
            if (ReceiptNo != null && ReceiptNo > 0)
                query = query.Where(e => e.ReceiptNo == ReceiptNo).ToList();
            if (StudentId != null && StudentId > 0)
                query = query.Where(e => e.StudentId == StudentId).ToList();
            if (ReceiptFromDate.HasValue)
                query = query.Where(e => e.ReceiptDate >= ReceiptFromDate).ToList();
            if (ReceiptToDate.HasValue)
                query = query.Where(e => e.ReceiptDate <= ReceiptToDate).ToList();
            if (FeePeriodId != null && FeePeriodId > 0)
                query = query.Where(e => e.FeePeriodId == FeePeriodId).ToList();
            if (PaymentModeId != null && PaymentModeId > 0)
                query = query.Where(e => e.PaymentModeId == PaymentModeId).ToList();
            if (PaidAmountFrom != null && PaidAmountFrom > 0)
                query = query.Where(e => e.PaidAmount >= PaidAmountFrom).ToList();
            if (PaidAmountTo != null && PaidAmountTo > 0)
                query = query.Where(e => e.PaymentModeId <= PaidAmountTo).ToList();
            if (StudentIds != null)
                query = query.Where(f => StudentIds.Contains(f.StudentId)).ToList();
            if (SessionId != null && SessionId > 0)
                query = query.Where(e => e.SessionId == SessionId).ToList();


            totalcount = query.Count;
          //  var FeeReceipts = new PagedList<KSModel.Models.FeeReceipt>(query, PageIndex, PageSize);
           // return FeeReceipts;
            // var FeeReceipts = new PagedList<KSModel.Models.FeeReceipt>(query, PageIndex, PageSize);
            return query.ToList();
        }

        public IList<SelectListItem> StudentListincludingleftbyClassId(int Classid, int? StudentId = null, int? SessionId = null,
                  List<int?> Studentidlist = null, bool? AdmnStatus = null, string SortingFor = "")
        {
            if (Classid == 0)
                throw new ArgumentNullException("Class");

            IList<SelectListItem> Students = new List<SelectListItem>();
            Students.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });

            int count = 0;
            // get student list by class id and sesison
            var session = GetCurrentSession();
            if (SessionId == null)
                SessionId = session.SessionId;
            var query = db.StudentClasses.ToList();
            if (AdmnStatus == null || AdmnStatus == false)
                query = query.Where(c => c.RollNo != null).ToList();
            query = query.Where(b => b.ClassId == Classid).ToList();
            if (SessionId != null && SessionId > 0)
                query = query.Where(b => b.SessionId == SessionId).ToList();
            if (StudentId != null && StudentId > 0)
            {
                if (AdmnStatus == null || AdmnStatus == false)
                    query = query.Where(b => b.StudentId != StudentId).ToList();
                else
                    query = query.Where(b => b.StudentId == StudentId).ToList();
            }

            if (Studentidlist != null)
                query = query.Where(b => Studentidlist.Contains(b.StudentId)).ToList();

            if (query.Count > 0)
            {
                var students = query.OrderBy(s => s.Student.FName).ToList().OrderBy(m => m.Student.FName).OrderBy(m => m.Student.LName);

                if (!string.IsNullOrEmpty(SortingFor))
                {
                    if (SortingFor == "R")//rollno
                        students = students.OrderBy(c => c.RollNo, new SemiNumericComparer());
                    else if (SortingFor == "N")
                        students = students.OrderBy(c => c.Student.FName, new SemiNumericComparer());
                }
                foreach (var student in students)
                {
                    //if (student.Student.StudentStatusDetails.Count > 0)
                    //{
                    //    var IsStudentActive = student.Student.StudentStatusDetails.OrderByDescending(p => p.StatusChangeDate).ThenByDescending(p => p.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus;
                    //    if (IsStudentActive == "Left")
                    //        continue;
                    //}
                    var resultwithheld = GetAllExamResultWithhelds(ref count).Where(m => m.StudentId == student.StudentId && m.ReleasedOn == null);
                    if (resultwithheld.Count() > 0)
                        continue;
                    if (StudentId != null && StudentId > 0)
                    {
                        if (student.StudentId == StudentId)
                        {
                            Students.Add(new SelectListItem { Selected = true, Text = student.Student.FName + " " + student.Student.LName + "- (R.No.-" + student.RollNo + ")", Value = student.StudentId.ToString() });
                        }
                        else
                        {
                            Students.Add(new SelectListItem { Selected = false, Text = student.Student.FName + " " + student.Student.LName + "- (R.No.-" + student.RollNo + ")", Value = student.StudentId.ToString() });
                        }
                    }
                    else
                    {
                        Students.Add(new SelectListItem { Selected = false, Text = student.Student.FName + " " + student.Student.LName + "- (R.No.-" + student.RollNo + ")", Value = student.StudentId.ToString() });
                    }
                }
            }

            return Students;
        }
        public List<KSModel.Models.ExamResultWithheld> GetAllExamResultWithhelds(ref int totalcount,
            int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ExamResultWithhelds.ToList();


            totalcount = query.Count;
          //  var ExamResultWithhelds = new PagedList<KSModel.Models.ExamResultWithheld>(query, PageIndex, PageSize);
         //   return ExamResultWithhelds;
            // var ExamResultWithhelds = new PagedList<KSModel.Models.ExamResultWithheld>(query, PageIndex, PageSize);
            return query.ToList();
        }
        public IList<SelectListItem> StudentListincludingleftbyStandardId(int StandardId, int? StudentId = null, int? SessionId = null,
                   List<int?> Studentidlist = null, bool? AdmnStatus = null, string SortingFor = "")
        {
            if (StandardId == 0)
                throw new ArgumentNullException("Class");

            IList<SelectListItem> Students = new List<SelectListItem>();
            Students.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });

            int count = 0;
            // get student list by class id and sesison
            var session = GetCurrentSession();
            if (SessionId == null)
                SessionId = session.SessionId;
            var query = db.Students.ToList();
            //if (AdmnStatus == null || AdmnStatus == false)
            //    query = query.Where(c => c.RollNo != null).ToList();
            query = query.Where(b => b.AdmnStandardId == StandardId).ToList();
            if (SessionId != null && SessionId > 0)
                query = query.Where(b => b.AdmnSessionId == SessionId).ToList();
            if (StudentId != null && StudentId > 0)
            {
                if (AdmnStatus == null || AdmnStatus == false)
                    query = query.Where(b => b.StudentId != StudentId).ToList();
                else
                    query = query.Where(b => b.StudentId == StudentId).ToList();
            }

            if (Studentidlist != null)
                query = query.Where(b => Studentidlist.Contains(b.StudentId)).ToList();

            if (query.Count > 0)
            {
                var students = query.OrderBy(s => s.FName).ToList().OrderBy(m => m.FName).OrderBy(m => m.LName);

                if (!string.IsNullOrEmpty(SortingFor))
                {
                    //if (SortingFor == "R")//rollno
                    //    students = students.OrderBy(c => c.RollNo, new SemiNumericComparer());
                    //else 
                    if (SortingFor == "N")
                        students = students.OrderBy(c => c.FName, new SemiNumericComparer());
                }
                foreach (var student in students)
                {
                    //if (student.Student.StudentStatusDetails.Count > 0)
                    //{
                    //    var IsStudentActive = student.Student.StudentStatusDetails.OrderByDescending(p => p.StatusChangeDate).ThenByDescending(p => p.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus;
                    //    if (IsStudentActive == "Left")
                    //        continue;
                    //}
                    var resultwithheld = GetAllExamResultWithhelds(ref count).Where(m => m.StudentId == student.StudentId && m.ReleasedOn == null);
                    if (resultwithheld.Count() > 0)
                        continue;
                    if (StudentId != null && StudentId > 0)
                    {
                        if (student.StudentId == StudentId)
                        {
                            Students.Add(new SelectListItem { Selected = true, Text = student.FName + " " + student.LName, Value = student.StudentId.ToString() });
                        }
                        else
                        {
                            Students.Add(new SelectListItem { Selected = false, Text = student.FName + " " + student.LName, Value = student.StudentId.ToString() });
                        }
                    }
                    else
                    {
                        Students.Add(new SelectListItem { Selected = false, Text = student.FName + " " + student.LName, Value = student.StudentId.ToString() });
                    }
                }
            }

            return Students;
        }
        public Student GetStudentById(int StudentId, bool IsTrack = false)
        {
            if (StudentId == 0)
                throw new ArgumentNullException("Student");

            var student = new Student();
            if (IsTrack)
                student = (from s in db.Students where s.StudentId == StudentId select s).FirstOrDefault();
            else
                student = (from s in db.Students.AsNoTracking() where s.StudentId == StudentId select s).FirstOrDefault();
            return student;
        }

        public IList<FeeReceiptDetailModel> GetFeeHeadCalculationsBySp(int StudentId = 0, int SessionId = 0, string ReceiptDate = "",
                   int FeeTypeId = 0, int FeePeriodId = 0, int ReceiptTypeId = 0, string SessionStartDate = "",
                   string SessionEndDate = "", string EWSDetail = "0", string IsApplyTptOwnerShip = "1", string IsSeparateTptreceipt = "0",
                   bool OnlyCurrentPeriod = false)
        {
            IList<FeeReceiptDetailModel> FeerReceiptDetailList = new List<FeeReceiptDetailModel>();
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(SqlDataSource))
            {
                using (SqlCommand sqlComm = new SqlCommand("sp_FeeHeadCalculation", conn))
                {
                    conn.Open();
                    sqlComm.CommandTimeout = 999;
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@StudentId", StudentId);
                    sqlComm.Parameters.AddWithValue("@SessionId", SessionId);
                    sqlComm.Parameters.AddWithValue("@ReceiptDate", ReceiptDate);
                    sqlComm.Parameters.AddWithValue("@FeeTypeId", FeeTypeId);
                    sqlComm.Parameters.AddWithValue("@SelectedFeePeriod", FeePeriodId);
                    sqlComm.Parameters.AddWithValue("@ReceiptTypeId", ReceiptTypeId);
                    sqlComm.Parameters.AddWithValue("@SessionStartDate", SessionStartDate);
                    sqlComm.Parameters.AddWithValue("@SessionendDate", SessionEndDate);
                    sqlComm.Parameters.AddWithValue("@ShowEWS", EWSDetail);
                    sqlComm.Parameters.AddWithValue("@IsApplyTptOwnerShip", IsApplyTptOwnerShip);
                    sqlComm.Parameters.AddWithValue("@IsSeparateTptFeeReceipt", IsSeparateTptreceipt);
                    sqlComm.Parameters.AddWithValue("@onlyCurrentPeriod", OnlyCurrentPeriod);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = sqlComm;
                    da.Fill(dt);
                    conn.Close();
                }
            }
            if (dt != null)
            {
                object discountsumObject;
                discountsumObject = dt.Compute("Sum(EWSDiscount)", "");
                object concessionsumObject;
                concessionsumObject = dt.Compute("Sum(ConcessionAmount)", "");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    FeeReceiptDetailModel feereceiptmodel = new FeeReceiptDetailModel();
                    feereceiptmodel.GroupFeeHeadId = Convert.ToInt32(dt.Rows[i]["HeadId"]);
                    feereceiptmodel.GroupFeeHeadName = Convert.ToString(dt.Rows[i]["HeadName"]);
                    feereceiptmodel.HeadType = Convert.ToString(dt.Rows[i]["HeadType"]);
                    feereceiptmodel.IsDiscount = Convert.ToBoolean(dt.Rows[i]["IsDiscount"]);
                    feereceiptmodel.FeeperiodId = Convert.ToInt32(dt.Rows[i]["Selectedfeeperiodid"]);

                    feereceiptmodel.LateFeePeriodId = Convert.ToInt32(dt.Rows[i]["LateFeePeriodid"]);
                    feereceiptmodel.PendingFeePeriodId = Convert.ToInt32(dt.Rows[i]["PendingFeePeriodId"]);
                    if (dt.Rows[i]["EWSDiscount"].ToString() == "")
                        feereceiptmodel.EWSDiscount = 0;
                    else
                        feereceiptmodel.EWSDiscount = Convert.ToDecimal(dt.Rows[i]["EWSDiscount"].ToString());
                    if (dt.Rows[i]["ConcessionAmount"].ToString() == "")
                        feereceiptmodel.FeeHeadConcession = 0;
                    else
                        feereceiptmodel.FeeHeadConcession = Convert.ToDecimal(dt.Rows[i]["ConcessionAmount"].ToString());
                    feereceiptmodel.TotalHeadAmount = Convert.ToDecimal(dt.Rows[i]["TotalAmount"].ToString());
                    feereceiptmodel.CalculatedHeadAmount = Convert.ToDecimal(dt.Rows[i]["CalculatedAmount"].ToString());
                    if (EWSDetail.ToString() == "0")
                        feereceiptmodel.ShowEWS = false;
                    else
                    {
                        if (discountsumObject.ToString() == "" || discountsumObject.ToString() == "0" || discountsumObject.ToString() == "0.00")
                        {
                            feereceiptmodel.ShowEWS = false;
                        }
                        else
                        {
                            feereceiptmodel.ShowEWS = true;
                        }
                    }

                    if (concessionsumObject.ToString() == "" || concessionsumObject.ToString() == "0" || concessionsumObject.ToString() == "0.00")
                        feereceiptmodel.ShowHeadConcession = false;
                    else
                        feereceiptmodel.ShowHeadConcession = true;

                    FeerReceiptDetailList.Add(feereceiptmodel);
                }
            }
            return FeerReceiptDetailList;
        }
        public List<KSModel.Models.FeeClassGroupType> GetAllFeeClassGroupTypes(ref int totalcount, int PageIndex = 0,
                    int PageSize = int.MaxValue)
        {
            var query = db.FeeClassGroupTypes.ToList();

            totalcount = query.Count;
            //var FeeClassGroupTypes = new PagedList<KSModel.Models.FeeClassGroupType>(query, PageIndex, PageSize);
            return query.ToList();
        }
        public List<FeeType> GetFeeTypesByClass(int[] FeeTypeIds)
        {
            var query = db.FeeTypes.ToList();
            query = query.Where(f => FeeTypeIds.Contains(f.FeeTypeId)).ToList();

            var feetypes = query.ToList();
            return feetypes;
        }
        public List<ReceiptType> GetAllReceiptTypes(ref int totalcount, int? ReceiptTypeId = null,
          int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ReceiptTypes.Where(f => f.IsActive == true).ToList();
            if (ReceiptTypeId != null && ReceiptTypeId > 0)
            {
                query = query.Where(d => d.ReceiptTypeId == ReceiptTypeId).ToList();
            }
            totalcount = query.Count;
        //    var ReceiptTypes = new PagedList<KSModel.Models.ReceiptType>(query, PageIndex, PageSize);
         //   return ReceiptTypes;
            // var ReceiptTypes = new PagedList<KSModel.Models.ReceiptType>(query, PageIndex, PageSize);
            return query.ToList();
        }


        public SchoolSetting GetorSetSchoolData(string AttrName, string AttrValue, int? AppFormId = null, string Desc = "")
        {
            // Custom Object 
            SchoolSetting GlobalSettingDataObj = new SchoolSetting();

            string Description = "";
            if (!string.IsNullOrEmpty(Desc))
                Description = Desc;
            else
                Description = AttrName;

            var IsAttrExist = db.SchoolSettings.Where(g => g.AttributeName.ToLower() == AttrName.ToLower());
            if (IsAttrExist.Count() > 0) // if exist return
            {
                GlobalSettingDataObj.AttributeName = IsAttrExist.FirstOrDefault().AttributeName;
                GlobalSettingDataObj.AttributeValue = IsAttrExist.FirstOrDefault().AttributeValue;
            }
            else // Firstly Insert , then return
            {
                SchoolSetting GlobalSettingObj = new SchoolSetting();
                GlobalSettingObj.AttributeName = AttrName;
                GlobalSettingObj.AttributeValue = AttrValue;
                GlobalSettingObj.Description = Description;
                GlobalSettingObj.AppFormId = AppFormId;

                db.SchoolSettings.Add(GlobalSettingObj);
                db.SaveChanges();

                GlobalSettingDataObj.AttributeName = GlobalSettingObj.AttributeName;
                GlobalSettingDataObj.AttributeValue = GlobalSettingObj.AttributeValue;
            }

            return GlobalSettingDataObj;
        }
        #endregion

    }
}