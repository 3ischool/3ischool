﻿using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.ClassPeriodModule;
using DAL.DAL.CertificateModule;
using DAL.DAL.Common;
using DAL.DAL.ContactModule;
using DAL.DAL.DocumentModule;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.GuardianModule;
using DAL.DAL.Kendo;
using DAL.DAL.SettingService;
using DAL.DAL.StudentModule;
using DAL.DAL.TransportModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Address;
using ViewModel.ViewModel.Guardian;
using ViewModel.ViewModel.Student;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using DAL.DAL.FeeModule;
using ViewModel.ViewModel.Common;
using BAL.BAL.Security;
using ViewModel.ViewModel.Fee;
using BAL.BAL.Common;
using ViewModel.ViewModel.Session;
using ViewModel.ViewModel.Transport;
using ViewModel.ViewModel.IssueDocument;
using System.Data;
using System.Globalization;


namespace KS_SmartChild.Controllers
{
    public class StudentReportsController : Controller
    {
        #region fields

        public int count = 0;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly IAddressService _IAddressService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly ILogService _Logger;
        private readonly IStudentService _IStudentService;
        private readonly IGuardianService _IGuardianService;
        private readonly IContactService _IContactService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStudentMasterService _IStudentMasterService;
        private readonly ITransportService _ITransportService;
        private readonly IDocumentService _IDocumentService;
        private readonly IBusService _IBusService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IWebHelper _WebHelper;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly ISubjectService _ISubjectService;
        private readonly IClassPeriodService _IClassPeriodService;
        private readonly ICertificateService _ICertificateService;
        private readonly IStudentAddOnService _IStudentAddOnService;
        private readonly IFeeService _IFeeService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly ISchoolDataService _ISchoolDataService;
        private readonly IExportHelper _IExportHelper;
        private readonly IUserManagementService _IUserManagementService;
        #endregion

        #region ctor

        public StudentReportsController(IDropDownService IDropDownService,
            IActivityLogService IActivityLogService,
            IActivityLogMasterService IActivityLogMasterService,
            ILogService Logger,
            IAddressService IAddressService,
            IAddressMasterService IAddressMasterService,
            IStudentService IStudentService,
            IGuardianService IGuardianService,
            IContactService IContactService,
            ICatalogMasterService ICatalogMasterService,
            IStudentMasterService IStudentMasterService,
            ITransportService ITransportService,
            IDocumentService IDocumentService,
            IBusService IBusService,
            ISchoolSettingService ISchoolSettingService,
            IWebHelper WebHelper,
            IUserService IUserService,
            IConnectionService IConnectionService,
            ISubjectService ISubjectService,
            IClassPeriodService IClassPeriodService,
            ICertificateService ICertificateService,
            IStudentAddOnService IStudentAddOnService,
            IFeeService IFeeService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption, ISchoolDataService ISchoolDataService,
            IExportHelper IExportHelper,
            IUserManagementService IUserManagementService)
        {
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._IAddressService = IAddressService;
            this._IAddressMasterService = IAddressMasterService;
            this._Logger = Logger;
            this._IStudentService = IStudentService;
            this._IGuardianService = IGuardianService;
            this._IContactService = IContactService;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IStudentMasterService = IStudentMasterService;
            this._ITransportService = ITransportService;
            this._IDocumentService = IDocumentService;
            this._IBusService = IBusService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._WebHelper = WebHelper;
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._ISubjectService = ISubjectService;
            this._IClassPeriodService = IClassPeriodService;
            this._ICertificateService = ICertificateService;
            this._IStudentAddOnService = IStudentAddOnService;
            this._IFeeService = IFeeService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._ISchoolDataService = ISchoolDataService;
            this._IExportHelper = IExportHelper;
            this._IUserManagementService = IUserManagementService;

        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }
        public DateTime FormatDate(string Date)
        {
            string sdisplayTime = Date.Trim('"');
            var dd = Date.Split('/');
            sdisplayTime = dd[1] + "-" + dd[0] + "-" + dd[2];
            return Convert.ToDateTime(sdisplayTime);
        }
        //Convert date
        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }

        #endregion

        #region Methods
        // GET: StudentReports
        public ActionResult PendingDocumentReport()
        {
            SchoolUser user = new SchoolUser();

            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Student", "Create", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            var model = new StudentPendingDocumentReportModel();
            model.AvailableClasses = _IDropDownService.ClassListContainsAll();
            var currentsession = _ICatalogMasterService.GetCurrentSession();
            model.AvailableSessions = _IDropDownService.SessionList();
            model.SessionId = currentsession.SessionId;
            model.AvailableMandatoryDocumentTypes = _IDropDownService.MandatoryDocumentTypeList();
            model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
            model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");

            return View(model);
        }

        public ActionResult PrintPendingDocumentReport(StudentPendingDocumentReportModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            // get all list
            var session = _ICatalogMasterService.GetCurrentSession();
            int SessionIdk = 0;
            if (model.SessionId == 0 || model.SessionId == null || model.SessionId.ToString() == "")
            {
                SessionIdk = session.SessionId;
            }
            else
            {
                SessionIdk = model.SessionId;
            }

            string DocumentType = "";
            if (model.DocumentTypeId > 0)
            {
                var getDocumentType = _IDocumentService.GetDocumentTypeById(model.DocumentTypeId);
                DocumentType = getDocumentType != null ? getDocumentType.DocumentType1 : "";
            }

            var StudentsList = _IDocumentService.GetStudentPendingDocumentTypeBySP(ref count,
                                   SessionId: (int)SessionIdk, ClassId: (int)model.ClassId, DocumentType: DocumentType).ToList();

            var StudentPendingList = new List<StudentPendingDocumentReportModel>();
            foreach (var stu in StudentsList)
            {
                var StudentPendingGridModel = new StudentPendingDocumentReportModel();
                StudentPendingGridModel.StudentName = (!string.IsNullOrEmpty(stu.StudentName)) ? stu.StudentName : "";
                StudentPendingGridModel.Class = (!string.IsNullOrEmpty(stu.Class)) ? stu.Class : "";
                StudentPendingGridModel.ParentName = (!string.IsNullOrEmpty(stu.ParentName)) ? stu.ParentName : "";
                StudentPendingGridModel.PendingDocuments = (!string.IsNullOrEmpty(stu.PendingDocuments)) ? stu.PendingDocuments : "";

                StudentPendingList.Add(StudentPendingGridModel);
            }

            PrintPendingDocumentReportModel PrintPendingDocumentReportModel = new PrintPendingDocumentReportModel();
            PrintPendingDocumentReportModel.PendingDocumentReportList = StudentPendingList;
            return View(PrintPendingDocumentReportModel);
        }

        public DataTable GetAllKendoGridInformation(string SessionId, string ClassId, string DocumentTypeId)
        {
            DataTable dt = new DataTable();
            try
            {
                var Session_Id = 0;
                var Class_Id = 0;
                var DocumentType_Id = 0;
                if (SessionId != null && SessionId != "")
                    Session_Id = Convert.ToInt32(SessionId);
                if (ClassId != null && ClassId != "")
                    Class_Id = Convert.ToInt32(ClassId);
                if (DocumentTypeId != null && DocumentTypeId != "")
                    DocumentType_Id = Convert.ToInt32(DocumentTypeId);

                string DocumentType ="";
                if(DocumentType_Id > 0)
                {
                    var getDocumentType = _IDocumentService.GetDocumentTypeById(DocumentType_Id);
                    DocumentType = getDocumentType != null ? getDocumentType.DocumentType1 : "";
                }

                // get all list
                var session = _ICatalogMasterService.GetCurrentSession();
                int SessionIdk = 0;
                if (Session_Id == 0 || Session_Id == null || Session_Id.ToString() == "")
                {
                    SessionIdk = session.SessionId;
                }
                else
                {
                    SessionIdk = Session_Id;
                }
                var StudentsList = _IDocumentService.GetStudentPendingDocumentTypeBySP(ref count,
                                  SessionId: (int)SessionIdk, ClassId: (int)Class_Id, DocumentType: DocumentType).ToList();

                var StudentPendingList = new List<StudentPendingDocumentReportModel>();
                foreach (var stu in StudentsList)
                {
                    var StudentPendingGridModel = new StudentPendingDocumentReportModel();
                    StudentPendingGridModel.StudentName = (!string.IsNullOrEmpty(stu.StudentName)) ? stu.StudentName : "";
                    StudentPendingGridModel.Class = (!string.IsNullOrEmpty(stu.Class)) ? stu.Class : "";
                    StudentPendingGridModel.AdmnNo = (!string.IsNullOrEmpty(stu.AdmnNo)) ? stu.AdmnNo : "";
                    StudentPendingGridModel.ParentName = (!string.IsNullOrEmpty(stu.ParentName)) ? stu.ParentName : "";
                    StudentPendingGridModel.PendingDocuments = (!string.IsNullOrEmpty(stu.PendingDocuments)) ? stu.PendingDocuments : "";

                    StudentPendingList.Add(StudentPendingGridModel);
                }

                // Add columns in your DataTable
                dt.Columns.Add("Student Name");
                dt.Columns.Add("Class");
                dt.Columns.Add("Admn No");
                dt.Columns.Add("Parent Name");
                dt.Columns.Add("Pending Documents");

                DataRow row;
                if (StudentPendingList.Count() > 0)
                {
                    foreach (var data in StudentPendingList)
                    {
                        row = dt.NewRow();
                        row[0] = data.StudentName == null ? "" : data.StudentName.ToString();

                        row[1] = data.Class == null ? "" : data.Class.ToString();
                        row[2] = data.AdmnNo == null ? "" : data.AdmnNo.ToString();
                        row[3] = data.ParentName == null ? "" : data.ParentName.ToString();
                        row[4] = data.PendingDocuments == null ? "" : data.PendingDocuments.ToString();

                        dt.Rows.Add(row);
                        dt.AcceptChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
            }
            return dt;
        }

        public ActionResult ExporttoExcelPendingDocumentReport(string SessionId, string ClassId, string DocumentTypeId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                string title = "Pending Document Report";
                string Subtitle = "";

                var getForm = _IUserManagementService.GetAllAppForms(ref count, AppForm: "Pending Document Report").FirstOrDefault();
                if (getForm != null)
                {
                    var getFormGirdinfo = _IUserManagementService.GetAllAppFormGrids(ref count, AppFormId: getForm.AppFormId);
                    if (getFormGirdinfo.Count > 0)
                    {
                        title = getFormGirdinfo.ElementAt(0).GridTitle;
                        Subtitle = getFormGirdinfo.ElementAt(0).SubTitlePrefix + " " + getFormGirdinfo.ElementAt(0).SubTitleSuffix;
                    }
                    else
                    {
                        title = ViewBag.Title;
                        Subtitle = "";
                    }
                }
                var IsExcel = "1";
                DataTable dt = new DataTable();
                dt = GetAllKendoGridInformation(SessionId, ClassId, DocumentTypeId);
                if (dt.Rows.Count > 0)
                {
                    if (IsExcel == "1")
                    {
                        _IExportHelper.ExportToExcel(dt, Title: title, Subtitle: Subtitle,HeadingName:"List of Pending Documents");
                    }
                    else if (IsExcel == "2")
                    {
                        _IExportHelper.ExportToPrint(dt, Title: title, Subtitle: Subtitle);
                    }
                    else
                    {
                        _IExportHelper.ExportToPDF(dt, Title: title, Subtitle: Subtitle);
                    }
                }
            }
            catch (Exception ex)
            { }
            return null;
        }

        [HttpPost]
        public ActionResult PendingDocumentReportList(DataSourceRequest command, StudentPendingDocumentReportModel model, IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Student", "Admission", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            string DocumentType = "";
            if (model.DocumentTypeId > 0)
            {
                var getDocumentType = _IDocumentService.GetDocumentTypeById(model.DocumentTypeId);
                DocumentType = getDocumentType != null ? getDocumentType.DocumentType1 : "";
            }


            // get all list
            var session = _ICatalogMasterService.GetCurrentSession();
            int SessionIdk = 0;
            if (model.SessionId == 0 || model.SessionId == null || model.SessionId.ToString() == "")
                SessionIdk = session.SessionId;
            else
                SessionIdk = model.SessionId;

            var StudentsList = _IDocumentService.GetStudentPendingDocumentTypeBySP(ref count,
                                 SessionId: (int)SessionIdk, ClassId: (int)model.ClassId, DocumentType: DocumentType).ToList();
            
            var StudentPendingList = new List<StudentPendingDocumentReportModel>();
            foreach (var stu in StudentsList)
            {
                var StudentPendingGridModel = new StudentPendingDocumentReportModel();
                StudentPendingGridModel.StudentName = (!string.IsNullOrEmpty(stu.StudentName)) ? stu.StudentName : "";
                StudentPendingGridModel.AdmnNo = (!string.IsNullOrEmpty(stu.AdmnNo)) ? stu.AdmnNo : "";
                StudentPendingGridModel.Class = (!string.IsNullOrEmpty(stu.Class)) ? stu.Class : "";
                StudentPendingGridModel.ParentName = (!string.IsNullOrEmpty(stu.ParentName)) ? stu.ParentName : "";
                StudentPendingGridModel.PendingDocuments = (!string.IsNullOrEmpty(stu.PendingDocuments)) ? stu.PendingDocuments : "";

                StudentPendingList.Add(StudentPendingGridModel);
            }

            #region
            //get Student LIst from Session
            //if class is all
            //var StudentsList = _IStudentService.GetAllStudentClasss(ref count, SessionId: SessionIdk).ToList();

            //if (model.ClassId > 0)
            //{
            //    StudentsList = _IStudentService.GetAllStudentClasss(ref count, SessionId: SessionIdk, ClassId: model.ClassId).ToList();
            //}
            //var getAllMandatoryAttachedDocuments = _IDocumentService.GetAllDocumentTypes(ref count).Where(m => m.IsMandatory == true);
            //var StudentPendingList = new List<StudentPendingDocumentReportModel>();
            //foreach (var x in StudentsList)
            //{
            //    var StudentPendingGridModel = new StudentPendingDocumentReportModel();

            //    var AttachedDocumentsList = _IDocumentService.GetAllAttachedDocuments(ref count).Where(m => m.StudentId == x.StudentId).ToList();

            //    StudentPendingGridModel.StudentName = x.Student.LName != null ? x.Student.FName + " " + x.Student.LName : x.Student.FName;
            //    StudentPendingGridModel.Class = x.ClassMaster != null ? x.ClassMaster.Class : "";

            //    var guardianDetail = _IGuardianService.GetAllGuardians(ref count, StudentId: x.StudentId).FirstOrDefault();
            //    var contacttype = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1 == "Guardian").FirstOrDefault();

            //    if (guardianDetail != null)
            //    {
            //        var guardianMobileNoRecord = _IContactService.GetAllContactInfos(ref count, ContactId: guardianDetail.GuardianId, ContactTypeId: contacttype.ContactTypeId).Where(M => M.ContactInfoType.ContactInfoType1 == "Mobile").FirstOrDefault();
            //        var guardianMobileNo = "";
            //        if (guardianMobileNoRecord != null)
            //        {
            //            guardianMobileNo = guardianMobileNoRecord.ContactInfo1;
            //        }
            //        var guardianName = guardianDetail.LastName != null ? guardianDetail.FirstName + " " + guardianDetail.LastName : guardianDetail.FirstName;
            //        StudentPendingGridModel.ParentName = guardianMobileNo != "" ? guardianName + "/" + guardianMobileNo : guardianName;
            //    }
            //    var documnetsnotuploaded = getAllMandatoryAttachedDocuments.Where(m => !AttachedDocumentsList.Select(n => n.DocumentTypeId).Contains(m.DocumentTypeId)).ToList();
            //    StringBuilder PendingDocuments = new StringBuilder();
            //    var i = 0;
            //    foreach (var item in documnetsnotuploaded)
            //    {
            //        if (i > 0)
            //            PendingDocuments.Append(" , ");
            //        PendingDocuments.Append(item.DocumentType1);
            //        i++;
            //    }
            //    StudentPendingGridModel.PendingDocuments = PendingDocuments.ToString();
            //    if (model.DocumentTypeId > 0)
            //    {
            //        if (documnetsnotuploaded.Where(m => m.DocumentTypeId == model.DocumentTypeId).ToList().Count() > 0)
            //            StudentPendingList.Add(StudentPendingGridModel);
            //    }
            //    else
            //        StudentPendingList.Add(StudentPendingGridModel);
            //}
            #endregion

            var stds = StudentPendingList.AsQueryable().Sort(sort);
            var gridModel = new DataSourceResult
            {
                Data = stds.PagedForCommand(command).ToList(),
                Total = stds.Count()
            };

            return Json(gridModel);
        }

        //public ActionResult StudentAddressReports()
        //{
        //    SchoolUser user = new SchoolUser();
        //    // current user
        //    if (HttpContext.Session["SchoolDB"] != null)
        //    {
        //        // re-assign session
        //        var sessiondb = HttpContext.Session["SchoolDB"];
        //        HttpContext.Session["SchoolDB"] = sessiondb;

        //        user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
        //        if (user == null)
        //            return RedirectToAction("Login", "Home");
        //        SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
        //        //// check authorization
        //        //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Student", "Create", "View");
        //        //if (!isauth)
        //        //    return RedirectToAction("AccessDenied", "Error");
        //    }
        //    else
        //    {
        //        // get action name
        //        var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
        //        return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
        //    }

        //    var model = new StudentAddressReport();

        //    var allclasss = _ICatalogMasterService.GetAllClassMasters(ref count).ToList();

        //    if (allclasss.Count > 0)
        //    {
        //        model.ClassList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
        //        foreach (var item in allclasss)
        //            model.ClassList.Add(new SelectListItem { Selected = false, Value = item.ClassId.ToString(), Text = item.Class });

        //    }

        //    model.ReqAddList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
        //    model.ReqAddList.Add(new SelectListItem { Selected = false, Value = "1", Text = "Student" });
        //    model.ReqAddList.Add(new SelectListItem { Selected = false, Value = "2", Text = "Guardian" });


        //    model.StutusList = _IDropDownService.StudentStatusList();
        //    model.AddressTypeList = _IDropDownService.AddressTypeList();

        //    model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
        //    model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");

        //    return View(model);
        //}

        [HttpPost]
        public ActionResult GenerateAddressReport(DataSourceRequest command, StudentListModel model, IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);

                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Student", "List", "View");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");

            }
            // var currentsession = _ICatalogMasterService.GetCurrentSession();

            int[] Classid = model.classfilter!=null?model.classfilter:null;
            //if (!string.IsNullOrEmpty(model.classfilter))
            //    Classid = model.classfilter;

            int? Statusid = null;
            if (!string.IsNullOrEmpty(model.statusfilter))
                Statusid = Convert.ToInt32(model.statusfilter);

 if (model.AdmissionId == null)
  model.AdmissionId=1;

            int ttlcount = 0;
            var students = _IStudentService.GetManageStudentList(ref ttlcount,
                SessionId: model.SessionId,
                StudentStatusId: Statusid,
                ClassId: Classid,
                Hostler: model.hostlercheck,
            AdmissionSessionid: model.AdmissionId.ToString());

            //for new

            var alladdress = _IAddressService.GetAllAddresss(ref count).ToList();

            var studentlist = new List<StudentAddressData>();
            if (students.Count > 0)
            {
                foreach (var item in students)
                {
                    var student = _IStudentService.GetStudentById(StudentId: item.StudentId);
                    var data = new StudentAddressData();
                    if (model.ReqAddId == "1")
                    {
                        if (model.AddressTypeId == "1")
                        {
                            var Isstudentaddressexist = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                            if (Isstudentaddressexist == null && model.IsPickGuardinaAddress == true)
                            {
                                if (student.Guardians.FirstOrDefault() != null && alladdress.Where(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault() != null)
                                {
                                    var Address = alladdress.Where(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                    data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                    data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                }
                                else
                                    continue;
                            }
                            else if (Isstudentaddressexist == null && model.IsPickGuardinaAddress == false)
                                continue;

                            if (model.AddressWithOutStudent == true)
                            {
                                if (student.CustodyRight != null && (student.CustodyRight.CustodyRight1.ToLower() == "all" || student.CustodyRight.CustodyRight1.ToLower() == "guardian" || student.CustodyRight.CustodyRight1.ToLower() == "father"))
                                {
                                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "father"))
                                        data.StudentName = student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault().FirstName + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault().LastName;
                                    else if (student.Guardians.FirstOrDefault() != null)
                                        data.StudentName = student.Guardians.FirstOrDefault().FirstName + " " + student.Guardians.FirstOrDefault().LastName;
                                    else
                                        continue;
                                }
                                else
                                {
                                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "mother"))
                                        data.StudentName = student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault().FirstName + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault().LastName;
                                    else if (student.Guardians.FirstOrDefault() != null)
                                        data.StudentName = student.Guardians.FirstOrDefault().FirstName + " " + student.Guardians.FirstOrDefault().LastName;
                                    else
                                        continue;
                                }
                            }
                            else
                                data.StudentName = _IStudentMasterService.GetStudentFullName(studentid: student.StudentId);

                            if (alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student"))
                            {
                                var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                            }
                        }
                        else
                        {

                            var Isstudentaddressexist = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                            if (Isstudentaddressexist == null && model.IsPickGuardinaAddress == true)
                            {
                                if (student.Guardians.FirstOrDefault() != null && alladdress.Where(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault() != null)
                                {
                                    var Address = alladdress.Where(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                    data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                    data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                }
                                else
                                    continue;
                            }
                            else if (Isstudentaddressexist == null && model.IsPickGuardinaAddress == false)
                                continue;

                            if (model.AddressWithOutStudent == true)
                            {
                                if (student.CustodyRight != null && (student.CustodyRight.CustodyRight1.ToLower() == "all" || student.CustodyRight.CustodyRight1.ToLower() == "guardian" || student.CustodyRight.CustodyRight1.ToLower() == "father"))
                                {
                                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "father"))
                                        data.StudentName = student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault().FirstName + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault().LastName;
                                    else if (student.Guardians.FirstOrDefault() != null)
                                        data.StudentName = student.Guardians.FirstOrDefault().FirstName + " " + student.Guardians.FirstOrDefault().LastName;
                                    else
                                        continue;
                                }
                                else
                                {
                                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "mother"))
                                        data.StudentName = student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault().FirstName + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault().LastName;
                                    else if (student.Guardians.FirstOrDefault() != null)
                                        data.StudentName = student.Guardians.FirstOrDefault().FirstName + " " + student.Guardians.FirstOrDefault().LastName;
                                    else
                                        continue;
                                }
                            }
                            else
                                data.StudentName = _IStudentMasterService.GetStudentFullName(studentid: student.StudentId);

                            if (alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student"))
                            {
                                var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                            }
                        }
                    }
                    else
                    {
                        if (model.AddressTypeId == "1")
                        {
                            if (model.AddressWithOutStudent == true)
                            {
                                if (student.CustodyRight != null && (student.CustodyRight.CustodyRight1.ToLower() == "all" || student.CustodyRight.CustodyRight1.ToLower() == "guardian" || student.CustodyRight.CustodyRight1.ToLower() == "father"))
                                {
                                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "father"))
                                    {
                                        data.StudentName = student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault().FirstName + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault().LastName;
                                        if (student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "father").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "father").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                    else
                                    {
                                        if (student.Guardians.FirstOrDefault() != null)
                                        {
                                            data.StudentName = student.Guardians.FirstOrDefault().FirstName + " " + student.Guardians.FirstOrDefault().LastName;
                                            if (student.Guardians.FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else
                                                continue;
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                }
                                else
                                {
                                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "mother"))
                                    {
                                        data.StudentName = student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault().FirstName + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault().LastName;
                                        if (student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "mother").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "mother").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                    else
                                    {
                                        if (student.Guardians.FirstOrDefault() != null)
                                        {
                                            data.StudentName = student.Guardians.FirstOrDefault().FirstName + " " + student.Guardians.FirstOrDefault().LastName;
                                            if (student.Guardians.FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else
                                                continue;
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                }
                            }
                            else
                            {
                                if (student.CustodyRight != null && (student.CustodyRight.CustodyRight1.ToLower() == "all" || student.CustodyRight.CustodyRight1.ToLower() == "guardian" || student.CustodyRight.CustodyRight1.ToLower() == "father"))
                                {
                                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "father"))
                                    {
                                        data.StudentName = _IStudentMasterService.GetStudentFullName(studentid: student.StudentId);
                                        if (student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "father").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "father").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                    else
                                    {
                                        if (student.Guardians.FirstOrDefault() != null)
                                        {
                                            data.StudentName = _IStudentMasterService.GetStudentFullName(studentid: student.StudentId);
                                            if (student.Guardians.FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else
                                                continue;
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                }
                                else
                                {
                                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "mother"))
                                    {
                                        data.StudentName = _IStudentMasterService.GetStudentFullName(studentid: student.StudentId);
                                        if (student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "mother").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "mother").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                    else
                                    {
                                        if (student.Guardians.FirstOrDefault() != null)
                                        {
                                            data.StudentName = _IStudentMasterService.GetStudentFullName(studentid: student.StudentId);
                                            if (student.Guardians.FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else
                                                continue;
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "permanent" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (model.AddressWithOutStudent == true)
                            {
                                if (student.CustodyRight != null && (student.CustodyRight.CustodyRight1.ToLower() == "all" || student.CustodyRight.CustodyRight1.ToLower() == "guardian" || student.CustodyRight.CustodyRight1.ToLower() == "father"))
                                {
                                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "father"))
                                    {
                                        data.StudentName = student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault().FirstName + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault().LastName;
                                        if (student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "father").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "father").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                    else
                                    {
                                        if (student.Guardians.FirstOrDefault() != null)
                                        {
                                            data.StudentName = student.Guardians.FirstOrDefault().FirstName + " " + student.Guardians.FirstOrDefault().LastName;
                                            if (student.Guardians.FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else
                                                continue;
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                }
                                else
                                {
                                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "mother"))
                                    {
                                        data.StudentName = student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault().FirstName + " " + student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault().LastName;
                                        if (student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "mother").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "mother").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                    else
                                    {
                                        if (student.Guardians.FirstOrDefault() != null)
                                        {
                                            data.StudentName = student.Guardians.FirstOrDefault().FirstName + " " + student.Guardians.FirstOrDefault().LastName;
                                            if (student.Guardians.FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else
                                                continue;
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                }
                            }
                            else
                            {
                                if (student.CustodyRight != null && (student.CustodyRight.CustodyRight1.ToLower() == "all" || student.CustodyRight.CustodyRight1.ToLower() == "guardian" || student.CustodyRight.CustodyRight1.ToLower() == "father"))
                                {
                                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "father"))
                                    {
                                        data.StudentName = _IStudentMasterService.GetStudentFullName(studentid: student.StudentId);
                                        if (student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "father").FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "father").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "father").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                    else
                                    {
                                        if (student.Guardians.FirstOrDefault() != null)
                                        {
                                            data.StudentName = _IStudentMasterService.GetStudentFullName(studentid: student.StudentId);
                                            if (student.Guardians.FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else
                                                continue;
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                }
                                else
                                {
                                    if (student.Guardians.Any(p => p.Relation.Relation1.ToLower() == "mother"))
                                    {
                                        data.StudentName = _IStudentMasterService.GetStudentFullName(studentid: student.StudentId);
                                        if (student.Guardians.Where(p => p.Relation.Relation1.ToLower() == "mother").FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "mother").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.Guardians.Where(m => m.Relation.Relation1.ToLower() == "mother").FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                    else
                                    {
                                        if (student.Guardians.FirstOrDefault() != null)
                                        {
                                            data.StudentName = _IStudentMasterService.GetStudentFullName(studentid: student.StudentId);
                                            if (student.Guardians.FirstOrDefault() != null && alladdress.Any(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.Guardians.FirstOrDefault().GuardianId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student"))
                                            {
                                                var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                                data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                                data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                            }
                                            else
                                                continue;
                                        }
                                        else if (model.IsPickStudentAddress == true && alladdress.Any(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student"))
                                        {
                                            var Address = alladdress.Where(p => p.ContactId == student.StudentId && p.AddressType.AddressType1.ToLower() == "correspondence" && p.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                            data.Address = ((Address.PlotNo != null && Address.PlotNo != "") ? Address.PlotNo : "") + " " + ((Address.Sector_Street != null && Address.Sector_Street != "") ? Address.Sector_Street : "") + " " + ((Address.Locality != null && Address.Locality != "") ? Address.Locality : "") + " " + ((Address.Landmark != null && Address.Landmark != "") ? Address.Landmark : "");
                                            data.City = ((Address.AddressCity.City != null && Address.AddressCity.City != "") ? Address.AddressCity.City : "") + ((Address.AddressCity.AddressState.State != null && Address.AddressCity.AddressState.State != "") ? " (" + Address.AddressCity.AddressState.State + ")" : "") + ((Address.Zip != null && Address.Zip != "") ? " - " + Address.Zip : "");
                                        }
                                        else
                                            continue;
                                    }
                                }
                            }
                        }
                    }
                    studentlist.Add(data);
                }
            }
            model.AddressList = studentlist;
            return View(model);
        }
        #endregion

        #region ExamCorrectionForm
        public StudentExamCorrectionModel PrepareExamCorrectionReport(StudentExamCorrectionModel model)
        {
            model.AvailableSessions = _IDropDownService.SessionList();

            var currentSession = _ICatalogMasterService.GetCurrentSession();
            DateTime startdate = currentSession.EndDate.Value.AddDays(1);
            currentSession = _ICatalogMasterService.GetAllSessions(ref count).Where(x => x.IsDefualt == true).FirstOrDefault();
            model.SessionId = currentSession.SessionId;

            model.AvailableClasses = _IDropDownService.ClassList();

            model.includeSubjectCode = false;
            var ShowCode = _ISchoolSettingService.GetorSetSchoolData("ShowCodeInExamForm", "false").AttributeValue;
            if (ShowCode.ToLower() == "true")
                model.includeSubjectCode = true;

            //model.SchoolLogo = _WebHelper.GetStoreLocation() + "Images/" + Convert.ToString(HttpContext.Session["SchoolId"]) + "/logo.png";
            //model.SchoolHeader = _ISchoolSettingService.GetAttributeValue("SchoolName");
            //model.SchoolAddress = _ISchoolSettingService.GetAttributeValue("SchoolAddress");

            return model;
        }

        public ActionResult StudentListbyClass(string Class, string Id, bool CurrentSession, string Session_id = "")
        {
            SchoolUser user = new SchoolUser();

            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            int student_id = 0;
            if (!string.IsNullOrEmpty(Id))
                int.TryParse(_ICustomEncryption.base64d(Id), out student_id);

            if (string.IsNullOrEmpty(Class))
                return Json(new
                {
                    status = "failed",
                    msg = "Class should not be empty"
                });



            int classid = Convert.ToInt32(Class);
            // current session
            var Session = new Session();
            if (!string.IsNullOrEmpty(Session_id))
                Session = _ICatalogMasterService.GetSessionById(Convert.ToInt32(Session_id));
            else
                Session = _ICatalogMasterService.GetCurrentSession();

            var studentlist = _IStudentService.StudentListbyClassId(classid, student_id, Session.SessionId, AdmnStatus: true);
            var studentStatusDetail = _IStudentMasterService.GetAllStudentStatusDetail(ref count);

            IList<SelectListItem> StudentDDL = new List<SelectListItem>();
            foreach (var item in studentlist)
            {
                if (string.IsNullOrEmpty(item.Value))
                {
                    StudentDDL.Add(item);
                    continue;
                }

                var IsStudentActive = studentStatusDetail.Where(p => p.StudentId == Convert.ToInt32(item.Value)).OrderByDescending(p => p.StatusChangeDate).ThenByDescending(p => p.StudentStatusDetailId).FirstOrDefault();
                if (IsStudentActive != null)
                {
                    if (IsStudentActive.StudentStatu.StudentStatus == "Left")
                        continue;
                    else
                        StudentDDL.Add(item);
                }
            }

            return Json(new
            {
                status = "success",
                data = StudentDDL
            });

        }

        public ActionResult GenerateExamCorrectionReport()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            var model = new StudentExamCorrectionModel();
            model = PrepareExamCorrectionReport(model);

            //authentication
            model.IsAuthToPrint = _IUserAuthorizationService.IsUserAuthorize(user, "StudentReports", "GenerateExamCorrectionReport", "Print");

            return View(model);
        }

        public ActionResult Export()
        {
            return Json(new
            {
                status = "success",
                data = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportData(string StudentId = "", string Session_Id = "", string ClassId = "", string ShowSubCode = "")
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                string Title = "";
                string Subtitle = "";
                string SchoolLogo = "";

                int Sessionid = 0;
                if (!string.IsNullOrEmpty(Session_Id))
                    Sessionid = Convert.ToInt32(Session_Id);

                int Student_Id = 0;
                if (!string.IsNullOrEmpty(StudentId))
                    Student_Id = Convert.ToInt32(StudentId);

                int Class_Id = 0;
                if (!string.IsNullOrEmpty(ClassId))
                    Class_Id = Convert.ToInt32(ClassId);

                bool ShowSubjectCode = false;
                if (!string.IsNullOrEmpty(ShowSubCode))
                {
                    if (ShowSubCode.ToLower() == "true")
                        ShowSubjectCode = true;
                }

                var currentSEssion = _ICatalogMasterService.GetSessionById(Sessionid);
                var StudentClassInfo = _IStudentService.GetAllStudentClasss(ref count, SessionId: Sessionid,
                                                                                   StudentId: Student_Id).FirstOrDefault();


                string ClassName = "";
                if (Class_Id > 0)
                {
                    ClassName = _ICatalogMasterService.GetClassMasterById(Class_Id).Class;
                }
                if (StudentClassInfo != null)
                {
                    Subtitle = "CLASS " + ClassName + " EXAMINATION FORM " + currentSEssion.Session1;
                }

                var SchoolInfo = _ISchoolDataService.GetSchoolDataDetail(ref count).FirstOrDefault();
                if (SchoolInfo != null)
                {
                    Title = SchoolInfo.SchoolName;
                    SchoolLogo = _WebHelper.GetStoreLocation() + "Images/" + Convert.ToString(HttpContext.Session["SchoolId"])
                                                                + "/" + SchoolInfo.Logo;
                }

                DataTable dt = new DataTable();
                dt = _IStudentService.ExportStudentExamCorrection(ref count, SessionId: Sessionid, ClassId: Class_Id,
                                                                                                    StudentId: Student_Id);

                // Get current culture.
                CultureInfo currCulture = CultureInfo.CurrentCulture;
                string htmlPrint = "";
                if (dt.Rows.Count > 0)
                {
                    htmlPrint += "<html><head><style>body{ font-family:arial; font-size:14px;}";
                    htmlPrint += " th,td {padding: 3px;}";
                    htmlPrint += " .candidate_details td{ padding-bottom:15px;}";
                    htmlPrint += " .candidate_details tr td:first-child{font-weight: bold}";
                    htmlPrint += " .printonSame{ width:100%; page-break-after: always; page-break-inside: avoid;}";
                    htmlPrint += "</style></head>";
                    htmlPrint += "<body>";

                    DataView dvFirtSection = dt.DefaultView;
                    DataTable dtFirtSection = dvFirtSection.ToTable("tbl", false, "StudentName", "FatherName"
                                                                    , "MotherName", "DOB"
                                                                    , "AadharCardNo", "Category");
                    dtFirtSection.Columns["StudentName"].ColumnName = "Candidate Name";
                    dtFirtSection.Columns["FatherName"].ColumnName = "Father's Name";
                    dtFirtSection.Columns["MotherName"].ColumnName = "Mother's Name";
                    dtFirtSection.Columns["DOB"].ColumnName = "Date Of Birth";
                    dtFirtSection.Columns["AadharCardNo"].ColumnName = "Aadhaar No.";
                    dtFirtSection.AcceptChanges();

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (i > 0)
                            htmlPrint += "<table style='width:100%;' class='printonSame'>";
                        else
                            htmlPrint += "<table style='width:100%;'>";

                        htmlPrint += "<tr>";
                        htmlPrint += "<td style='line-height: 34px;text-align:center;' colspan='2'>";
                        htmlPrint += "<img src=" + SchoolLogo + " style='width: 80px; float:left;'>";
                        htmlPrint += "<b style='font-size: 20px;'><u>" + Title.ToUpper(currCulture) + "<br>" + Subtitle.ToUpper(currCulture) + "</u></b>";
                        htmlPrint += "<div style='clear:both'></div></td>";
                        htmlPrint += "</tr>";
                        htmlPrint += "<tr>";
                        htmlPrint += "<td colspan='2'>";
                        htmlPrint += "<table class='candidate_details' border='1' cellspacing='0' style='width:100%;'>";

                        for (var k = 0; k < dtFirtSection.Columns.Count; k++)
                        {
                            htmlPrint += "<tr>";
                            htmlPrint += "<td width='170'>" + dtFirtSection.Columns[k].ColumnName + "</td>";
                            foreach (DataRow row in dtFirtSection.Rows)
                            {
                                string Data = "";
                                if (!DBNull.Value.Equals(dtFirtSection.Rows[i][k]))
                                {
                                    if (dtFirtSection.Columns[k].ColumnName.Contains("Date"))
                                        Data = Convert.ToDateTime(dtFirtSection.Rows[i][k]).Date.ToString("dd/MM/yyyy");
                                    else
                                        Data = dtFirtSection.Rows[i][k].ToString();
                                }
                                htmlPrint += "<td width='170'>" + Data + "</td>";
                                break;
                            }

                            if (dtFirtSection.Columns[k].ColumnName.ToLower().Contains("birth"))
                                htmlPrint += "<td width='130'>Correct Date</td>";
                            else if (dtFirtSection.Columns[k].ColumnName.ToLower().Contains("no."))
                                htmlPrint += "<td width='130'>Correct No.</td>";
                            else if (dtFirtSection.Columns[k].ColumnName.ToLower().Contains("category"))
                                htmlPrint += "<td width='130'>Correct Cat.</td>";
                            else
                                htmlPrint += "<td width='130'>Correct Name</td>";

                            htmlPrint += "<td></td>";
                            htmlPrint += "</tr>";
                        }


                        htmlPrint += "</table>";
                        htmlPrint += "</td></tr>";
                        htmlPrint += "<tr><td>";
                        htmlPrint += "<table style='width:100%;' >";
                        DataView dvSecondSection = dt.DefaultView;
                        DataTable dtSecondSection = dvSecondSection.ToTable("tbl2", false, "House", "AdmnNo", "DOJ");
                        dtSecondSection.Columns["AdmnNo"].ColumnName = "Admn. No.";
                        dtSecondSection.Columns["DOJ"].ColumnName = "Date Of Joining";
                        dtSecondSection.AcceptChanges();

                        htmlPrint += "<tr>";
                        for (var j = 0; j < dtSecondSection.Columns.Count; j++)
                        {
                            htmlPrint += "<td width='33.3%'><b>" + dtSecondSection.Columns[j].ColumnName + ":- </b>";
                            foreach (DataRow row in dtSecondSection.Rows)
                            {

                                var Data = "";
                                if (!DBNull.Value.Equals(dtSecondSection.Rows[i][j]))
                                {
                                    if (dtSecondSection.Columns[j].ColumnName.Contains("Date"))
                                        Data = Convert.ToDateTime(dtSecondSection.Rows[i][j]).Date.ToString("dd/MM/yyyy");
                                    else
                                        Data = dtSecondSection.Rows[i][j].ToString();
                                }

                                htmlPrint += Data + " <br>";
                                break;
                            }
                            htmlPrint += "</td>";
                        }
                        htmlPrint += "</tr>";
                        htmlPrint += "</table>";
                        htmlPrint += "</td></tr>";
                        htmlPrint += "<tr><td>";
                        htmlPrint += "<table style='width:100%;'>";
                        htmlPrint += "<tr>";

                        int countSubs = 0;
                        int counter = 0;
                        if (!DBNull.Value.Equals(dt.Rows[i]["Subjects"]))
                        {
                            var arrySubjects = dt.Rows[i]["Subjects"].ToString().Split(',');
                            int rowDivided = arrySubjects.Count() / 8;
                            rowDivided = arrySubjects.Count() - (rowDivided * 8);
                            foreach (var sub in arrySubjects)
                            {
                                countSubs++;
                                counter++;
                                if (counter == 1)
                                {
                                    htmlPrint += "<td><table style='width:100%'>";
                                    htmlPrint += "<tr>";
                                    htmlPrint += "<td><b><u>Subjects</u></td>";

                                    if (ShowSubjectCode)
                                        htmlPrint += "<td colspan='2'><b><u>Code</u></td>";
                                    else
                                        htmlPrint += "<td colspan='2'><b><u>Abbr</u></td>";

                                    htmlPrint += "</tr>";
                                }

                                string subjectname = sub.Split('/')[0];
                                var SubjectshortForm = sub.Split('/')[1].Split('|');
                                string subjectabbr = SubjectshortForm[0];
                                string subjectcode = SubjectshortForm[1];

                                htmlPrint += "<tr>";
                                htmlPrint += "<td><b>" + countSubs.ToString() + ". " + subjectname + "</b></td>";

                                if (ShowSubjectCode)
                                    htmlPrint += "<td colspan='2'><b>" + subjectcode + "</b> </td>";
                                else
                                    htmlPrint += "<td colspan='2'><b>" + subjectabbr + "</b> </td>";

                                htmlPrint += "</tr>";

                                if (counter == 8 || countSubs == arrySubjects.Count())
                                {
                                    if (countSubs == arrySubjects.Count())
                                    {
                                        for (var f = 0; f < (8 - counter); f++)
                                        {
                                            htmlPrint += "<tr><td colspan='3'>&nbsp;&nbsp;&nbsp;</td></tr>";
                                        }
                                    }
                                    htmlPrint += "</table></td>";
                                    counter = 0;
                                }
                            }
                        }
                        else
                        {
                            var subjects = _ISubjectService.GetAllClassSubjects(ref count, ClassId: Class_Id, IsActive: true);
                            countSubs = 0;
                            counter = 0;
                            foreach (var subj in subjects)
                            {
                                countSubs++;
                                counter++;
                                if (counter == 1)
                                {
                                    htmlPrint += "<td><table style='width:100%'>";
                                    htmlPrint += "<tr>";
                                    htmlPrint += "<td><b><u>Subjects</u><br></td>";
                                    if (ShowSubjectCode)
                                        htmlPrint += "<td colspan='2'><b><u>Code</u><br></td>";
                                    else
                                        htmlPrint += "<td colspan='2'><b><u>Abbr</u><br></td>";
                                    htmlPrint += "</tr>";
                                }

                                htmlPrint += "<tr>";
                                if (subj.SkillId == null)
                                    htmlPrint += "<td><b style='line-height: 24px;'>" + countSubs.ToString() + ". " + subj.Subject.Subject1 + "</b></td>";
                                else
                                {
                                    var SubjectSkill = _ISubjectService.GetAllSubjectSkills(ref count, SubjectId: subj.SubjectId
                                                                                        , SkillId: subj.SkillId).FirstOrDefault();
                                    if (SubjectSkill != null)
                                    {
                                        htmlPrint += "<td><b>" + countSubs.ToString() + ". " + SubjectSkill.SubjectSkill1 + "</b></td>";
                                    }
                                }
                                if (ShowSubjectCode)
                                    htmlPrint += "<td colspan='2'><b>" + subj.Subject.SubjectCode + "</b> </td>";
                                else
                                    htmlPrint += "<td colspan='2'><b>" + subj.Subject.SubjectCode1 + "</b> </td>";
                                htmlPrint += "</tr>";

                                if (counter == 8 || countSubs == subjects.Count())
                                {
                                    if (countSubs == subjects.Count())
                                    {
                                        for (var f = 0; f < (8 - counter); f++)
                                        {
                                            htmlPrint += "<tr><td colspan='3'>&nbsp;&nbsp;&nbsp;</td></tr>";
                                        }
                                    }
                                    htmlPrint += "</table></td>";
                                    counter = 0;
                                }
                            }
                        }
                        htmlPrint += "</tr>";
                        htmlPrint += "</table>";
                        htmlPrint += "</td></tr>";
                        htmlPrint += "<tr><td>";
                        htmlPrint += "<table style='width:100%;'>";
                        htmlPrint += "<tr>";
                        htmlPrint += " <td colspan='3' style='text-align: center;'><b><u>Undertaking</u></b> </td>";
                        htmlPrint += "</tr>";
                        htmlPrint += " <tr>";
                        htmlPrint += "<td colspan='3' style='text-align: justify; line-height: 26px;'>";
                        htmlPrint += " <p>";

                        string UnderTakingText = "";
                        var getExamCorrectionFormText = _ICatalogMasterService.GetAllCommonTexts(ref count, FormName: "ExamCorrectionForm").FirstOrDefault();
                        if (getExamCorrectionFormText != null)
                        {
                            UnderTakingText = (!string.IsNullOrEmpty(getExamCorrectionFormText.DescriptionText)) ? getExamCorrectionFormText.DescriptionText.Trim() : "";
                            if (UnderTakingText.Contains("%Class%"))
                            {
                                UnderTakingText = UnderTakingText.Replace("%Class%", ClassName);
                            }

                        }
                        htmlPrint += UnderTakingText;
                        //htmlPrint += " I have thoroughly checked all the details as above filled in for registration of ";
                        //htmlPrint += "Class " + ClassName + " examination in respected of my ward. I have also filled details required in English language. I ";
                        //htmlPrint += "would not request for any change in the details filled in the Examination form of my ward such as Date of ";
                        //htmlPrint += "Birth/ Candidate’s Name/ Mother’s Name/ Father’s Name etc. If any mistake in students credential is ";
                        //htmlPrint += "detected before/after declaration of result, I will solely be held responsible for the same.";
                        htmlPrint += "</p></td></tr>";
                        htmlPrint += "</table>";
                        htmlPrint += "</td></tr>";
                        htmlPrint += "<tr>";
                        htmlPrint += " <td>";
                        htmlPrint += " <table style='width:100%;'>";
                        htmlPrint += "<tr>";
                        htmlPrint += "<td style='width: 50%'><p>Signature of Father</p> </td>";
                        htmlPrint += "<td style='width: 50%'><p>Signature of Mother</p> </td>";
                        htmlPrint += "</tr>";
                        htmlPrint += "<tr>";
                        htmlPrint += "<td style='width: 50%'><p>Signature of class teacher	</p> </td>";
                        htmlPrint += "<td style='width: 50%'><p>Signature of Student</p> </td>";
                        htmlPrint += "</tr>";
                        htmlPrint += "<tr>";
                        htmlPrint += "<td style='width: 50%'><p>Date:-    </p> </td>";
                        htmlPrint += "<td style='width: 50%'><p> Signature of School Headmaster</p> </td>";
                        htmlPrint += "</tr>";
                        htmlPrint += "</table>";
                        htmlPrint += "</td>";
                        htmlPrint += "</tr>";
                        htmlPrint += "</table>";

                    }
                    htmlPrint += "</body>";
                    htmlPrint += "</html>";
                    _IExportHelper.ExportToPrint(html: htmlPrint, footer: "No Date");
                    return null;
                }
                else
                {
                    TempData["errorExamMsges"] = "No Data Found";
                    return RedirectToAction("GenerateExamCorrectionReport");
                }
            }
            catch (Exception ex)
            {
                TempData["errorExamMsges"] = ex.Message;
                return RedirectToAction("GenerateExamCorrectionReport");
            }

        }
        #endregion
    }
}