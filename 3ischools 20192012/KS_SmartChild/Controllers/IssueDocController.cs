﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.SettingService;
using DAL.DAL.StudentModule;
using DAL.DAL.TransportModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Transport;
using ViewModel.ViewModel.IssueDocument;
using DAL.DAL.CertificateModule;
using System.Text;
using BAL.BAL.Common;
using System.Xml.Linq;
using DAL.DAL.GuardianModule;
using DAL.DAL.FeeModule;
using ViewModel.ViewModel.Fee;
using System.Data;
using DAL.DAL.Kendo;

namespace KS_SmartChild.Controllers
{
    public class IssueDocController : Controller
    {
        #region fields

        public int count = 0;
        private readonly IDropDownService _IDropDownService;
        private readonly ILogService _Logger;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStudentService _IStudentService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IWebHelper _WebHelper;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly ICertificateService _ICertificateService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly ISchoolDataService _ISchoolDataService;
        private readonly IGuardianService _IGuardianService;
        private readonly IFeeService _IFeeService;
        private readonly IStudentMasterService _IStudentMasterService;
        private readonly ISubjectService _ISubjectService;
        #endregion

        #region ctor

        public IssueDocController(IDropDownService IDropDownService,
            ILogService Logger, IFeeService IFeeService, IStudentMasterService IStudentMasterService,
            ICatalogMasterService ICatalogMasterService,
            IStudentService IStudentService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            IUserService IUserService, ISchoolDataService ISchoolDataService,
            IConnectionService IConnectionService,
            IWebHelper WebHelper, IGuardianService IGuardianService,
            ISchoolSettingService ISchoolSettingService,
            ICertificateService ICertificateService,
            ICustomEncryption ICustomEncryption,
            ISubjectService ISubjectService)
        {
            this._IDropDownService = IDropDownService;
            this._Logger = Logger;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IStudentService = IStudentService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._IActivityLogService = IActivityLogService;
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._WebHelper = WebHelper;
            this._ISchoolSettingService = ISchoolSettingService;
            this._ICertificateService = ICertificateService;
            this._ICustomEncryption = ICustomEncryption;
            this._ISchoolDataService = ISchoolDataService;
            this._IGuardianService = IGuardianService;
            this._IFeeService = IFeeService;
            this._IStudentMasterService = IStudentMasterService;
            this._ISubjectService = ISubjectService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }

        public string ConvertTime(TimeSpan time)
        {
            TimeSpan timespan = new TimeSpan(time.Hours, time.Minutes, time.Seconds);
            DateTime datetime = DateTime.Today.Add(timespan);
            string sdisplayTime = datetime.ToString("hh:mm tt");
            return sdisplayTime;
        }

        #endregion

        #region Methods

        #region IssueDoc
        public ActionResult GetInformationbyAdmnNo(string AdmnNo, string Session_id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                //int SessionidK = 0;
                var session = new Session();
                if (Session_id == "" || Session_id == null)
                    session = _ICatalogMasterService.GetCurrentSession();
                else
                    session = _ICatalogMasterService.GetSessionById(Convert.ToInt32(Session_id));
                var students = _IStudentService.GetAllStudents(ref count);
                var student = students.Where(s => (s.AdmnNo != null && s.AdmnNo == AdmnNo) || (s.RegNo != null && s.RegNo == AdmnNo)).FirstOrDefault();
                if (student != null)
                {
                    string Fathername = "";
                    var guardians = _IGuardianService.GetAllGuardians(ref count, StudentId: student.StudentId).Where(m => m.Relation.Relation1 == "Father").FirstOrDefault();
                    if (guardians != null)
                    {
                        if (student.Gender.Gender1 == "Female")
                        {
                            Fathername = "D/O - " + guardians.FirstName + " " + guardians.LastName;
                        }
                        else
                        {
                            Fathername = "S/O - " + guardians.FirstName + " " + guardians.LastName;
                        }
                    }
                    var StudentClass = student.StudentClasses.Where(s => s.SessionId == session.SessionId).FirstOrDefault();
                    //var feereceipt = _IFeeService.GetAllFeeReceipts(ref count, StudentId: student.StudentId, SessionId: SessionidK).OrderByDescending(f => f.ReceiptId).FirstOrDefault();
                    //string lastdate = "";
                    //if (feereceipt != null)
                    //    lastdate = feereceipt.ReceiptDate.Value.ToString("MM/dd/yyyy");

                    if (StudentClass != null)
                    {
                        return Json(new
                        {
                            status = "success",
                            StudentVal = student.StudentId,
                            ClassVal = StudentClass.ClassId,
                            FatherName = Fathername,
                            //LastReceiptdate = lastdate
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            msg = "Student class not found in this session"
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        msg = "Invalid Reg./Admn. No."
                    });
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                });
            }
        }

        public ActionResult StudentListbyClass(string Class, string Id, bool CurrentSession, string Session_id = "")
        {
            SchoolUser user = new SchoolUser();

            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            int student_id = 0;
            if (!string.IsNullOrEmpty(Id))
                int.TryParse(_ICustomEncryption.base64d(Id), out student_id);

            if (string.IsNullOrEmpty(Class))
                return Json(new
                {
                    status = "failed",
                    msg = "Class should not be empty"
                });

            int classid = Convert.ToInt32(Class);
            // current session
            var Session = new Session();
            if (!string.IsNullOrEmpty(Session_id))
                Session = _ICatalogMasterService.GetSessionById(Convert.ToInt32(Session_id));
            else
                Session = _ICatalogMasterService.GetCurrentSession();

            var studentlist = _IStudentService.StudentListbyClassId(classid, student_id, Session.SessionId, AdmnStatus: true);
            var studentStatusDetail = _IStudentMasterService.GetAllStudentStatusDetail(ref count);

            IList<SelectListItem> StudentDDL = new List<SelectListItem>();
            foreach (var item in studentlist)
            {
                if (string.IsNullOrEmpty(item.Value))
                {
                    StudentDDL.Add(item);
                    continue;
                }

                if (studentStatusDetail.Where(p => p.StudentId == Convert.ToInt32(item.Value)).ToList().Count() > 0)
                {
                    var IsStudentActive = studentStatusDetail.Where(p => p.StudentId == Convert.ToInt32(item.Value)).OrderByDescending(p => p.StatusChangeDate).ThenByDescending(p => p.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus;
                    if (IsStudentActive == "Left")
                        continue;
                    else
                        StudentDDL.Add(item);
                }
            }

            return Json(new
            {
                status = "success",
                data = StudentDDL
            });

        }

        public ActionResult GetStudentAdmnoByStudent(string Student)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                var StudentId = Convert.ToInt32(Student);
                var student = _IStudentService.GetStudentById(StudentId);
                if (student != null)
                {
                    // Fee Type
                    string FeeType = "";
                    // current session
                    var session = _ICatalogMasterService.GetCurrentSession();
                    // get student class
                    var studentclass = _IStudentService.GetAllStudentClasss(ref count, SessionId: session.SessionId, StudentId: student.StudentId).FirstOrDefault();
                    string Fathername = "";
                    if (studentclass != null)
                    {

                        var guardians = _IGuardianService.GetAllGuardians(ref count, StudentId: student.StudentId).Where(m => m.Relation.Relation1 == "Father").FirstOrDefault();
                        if (guardians != null)
                        {
                            if (student.Gender.Gender1 == "Female")
                            {
                                Fathername = "D/O - " + guardians.FirstName + " " + guardians.LastName;
                            }
                            else
                            {
                                Fathername = "S/O - " + guardians.FirstName + " " + guardians.LastName;
                            }
                        }
                        // get class standard
                        var standard = studentclass.ClassMaster.StandardId;
                        // get fee class group by standard
                        var feeclassgroupdet = _IFeeService.GetAllFeeClassGroupDetails(ref count, StandardId: standard).FirstOrDefault();
                        if (feeclassgroupdet != null)
                        {
                            // get group fee type by Fee Class Group
                            var groupfeetype = _IFeeService.GetAllGroupFeeTypes(ref count, FeeClassGroupId: feeclassgroupdet.FeeClassGroupId).ToList();
                            // fee type ids array
                            var feetypeids = groupfeetype.Select(g => (int)g.FeeTypeId).ToArray();
                            // get fee types
                            var feetype = _IFeeService.GetFeeTypesByClass(feetypeids).FirstOrDefault();
                            if (feetype != null)
                                FeeType = feetype.FeeType1;
                        }
                    }

                    // chcek student Current Session fee Receipts
                    IList<StudentFeeReceiptModel> StudentFeeReceiptList = new List<StudentFeeReceiptModel>();
                    IList<string> feeperiodlist = new List<string>();
                    IList<int> feeperiodids = new List<int>();
                    string LastReceiptdate = "";
                    var FeeReceipts = _IFeeService.GetAllFeeReceipts(ref count, StudentId: StudentId, SessionId: session.SessionId).ToList();
                    if (FeeReceipts.Count > 0)
                    {
                        foreach (var feercpt in FeeReceipts)
                        {
                            feeperiodlist = new List<string>();
                            feeperiodids = new List<int>();

                            StudentFeeReceiptModel StudentFeeReceiptModel = new StudentFeeReceiptModel();
                            StudentFeeReceiptModel.ReceiptNo = feercpt.ReceiptNo.ToString();
                            StudentFeeReceiptModel.ReceiptType = feercpt.ReceiptType.ReceiptType1.ToString();
                            StudentFeeReceiptModel.ReciptDate = feercpt.ReceiptDate.Value.ToString("dd/MM/yyyy");
                            // Fee Receipt Detail
                            // Calculate Amount
                            decimal ttlamt = 0;
                            decimal ttlconsession = 0;
                            int rowcount = 0;

                            var FeeReciptDetail = feercpt.FeeReceiptDetails.ToList();
                            foreach (var feerecptdet in FeeReciptDetail)
                            {
                                if (rowcount == 0)
                                {
                                    if (feerecptdet.FeeReceipt.FeePeriod != null)
                                        StudentFeeReceiptModel.FeeType = feerecptdet.FeeReceipt != null ? feerecptdet.FeeReceipt.FeePeriod.FeeFrequency.FeeTypes.FirstOrDefault().FeeType1 : FeeType;
                                    else
                                        StudentFeeReceiptModel.FeeType = "";
                                }
                                rowcount++;
                                if (feerecptdet.GroupFeeHeadId != null || feerecptdet.AddOnHeadId != null || feerecptdet.PendingFeePeriodId != null || feerecptdet.LateFeePeriodId != null || feerecptdet.FeeHeadId != null)
                                {
                                    ttlamt += Math.Round((decimal)feerecptdet.Amount, 2);

                                }
                                else
                                    ttlconsession += Math.Round((decimal)feerecptdet.Amount, 2);

                                if (feerecptdet.GroupFeeHead != null)
                                {
                                    // if (feerecptdet.GroupFeeHead.FeeHead.FeeHead1.Contains("Late"))
                                    // {
                                    if (feerecptdet.LateFeePeriodId > 0 && !feeperiodids.Contains((int)feerecptdet.LateFeePeriodId))
                                    {
                                        var lateFeePeriod = _IFeeService.GetFeePeriodById((int)feerecptdet.LateFeePeriodId);
                                        feeperiodids.Add((int)feerecptdet.LateFeePeriodId);
                                        feeperiodlist.Add(lateFeePeriod.FeePeriodDescription);
                                    }
                                    // }
                                    // else if (feerecptdet.GroupFeeHead.FeeHead.FeeHead1.Contains("Pending"))
                                    // {
                                    if (feerecptdet.PendingFeePeriodId > 0 && !feeperiodids.Contains((int)feerecptdet.PendingFeePeriodId))
                                    {
                                        var pendingFeePeriod = _IFeeService.GetFeePeriodById((int)feerecptdet.PendingFeePeriodId);
                                        feeperiodids.Add((int)feerecptdet.PendingFeePeriodId);
                                        feeperiodlist.Add(pendingFeePeriod.FeePeriodDescription);
                                    }

                                    //}
                                }
                            }
                            if (feercpt.FeePeriodId != null)
                            {
                                if (!feeperiodids.Contains((int)feercpt.FeePeriodId))
                                {
                                    feeperiodids.Add(feercpt.FeePeriod != null ? (int)feercpt.FeePeriodId : 0);
                                    feeperiodlist.Add(feercpt.FeePeriod != null ? feercpt.FeePeriod.FeePeriodDescription : "");
                                }
                            }
                            StudentFeeReceiptModel.PayableFee = (ttlamt - ttlconsession).ToString();
                            StudentFeeReceiptModel.PaidFee = feercpt.PaidAmount.ToString();
                            StudentFeeReceiptModel.FeePeriod = String.Join(",", feeperiodlist);
                            StudentFeeReceiptModel.EncFeeReceiptId = _ICustomEncryption.base64e(feercpt.ReceiptId.ToString());
                            StudentFeeReceiptList.Add(StudentFeeReceiptModel);

                        }

                        //var feereceiptdate = FeeReceipts.OrderByDescending(p => p.ReceiptId).FirstOrDefault();
                        //if (feereceiptdate != null)
                        //    LastReceiptdate = feereceiptdate.ReceiptDate.Value.ToString("MM/dd/yyyy");
                    }

                    return Json(new
                    {
                        status = "success",
                        AdmnNo = student.AdmnNo,
                        FeeReceiptList = StudentFeeReceiptList,
                        FatherName = Fathername,
                        //LastReceiptdate = LastReceiptdate
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        msg = "No Student found"
                    });
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                });
            }
        }

        public ActionResult getIssuedDocs(int IssueDocTemplid = 0)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                string HasTemplate = "";
                if (IssueDocTemplid > 0)
                {
                    //getDoc Template from IssueDocType
                    var hasAnyTemplate = _ICertificateService.GetAllIssueDoc(ref count, DocTemplateId: IssueDocTemplid).ToList();
                    if (hasAnyTemplate.Count > 0)
                    {
                        HasTemplate = hasAnyTemplate.Count.ToString();
                    }
                }
                return Json(new { HasTemplate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllColsValuesOfTemplate(int IssueDocTypeId = 0, string AnnualSelection = "",
            int Session = 0, string FromDate = "", string ToDate = "", int ReceiptTypeId = 0, string StudentId = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                var paramColsHasNotValueList = new List<IssueDocColsValue>();
                if (IssueDocTypeId > 0)
                {

                    ////penidng

                    var getReceiptType = new ReceiptType();

                    int? SessionIdK = null;
                    if (Session > 0)
                        SessionIdK = Session;

                    int? Student_Id = null;
                    if (!string.IsNullOrEmpty(StudentId))
                        Student_Id = Convert.ToInt32(StudentId);


                    var getTokens = _ICertificateService.GetAllIssueDocTypeCol(ref count, IssueDocTypeId: (int)IssueDocTypeId);
                    var TokensShownInForm = getTokens.Where(f => f.ShowAlwaysInForm == true);

                    var getAllColumnsValues = _ICertificateService.GetAllIssueDocTokensListbySp(ref count, SessionId: SessionIdK, StudentId: Student_Id);

                    if (getAllColumnsValues.Rows.Count > 0)
                    {
                        for (int row = 0; row < getAllColumnsValues.Rows.Count; row++)
                        {
                            foreach (DataColumn dc in getAllColumnsValues.Columns)
                            {
                                if (DBNull.Value.Equals(getAllColumnsValues.Rows[row][dc.ColumnName.ToString()])
                                    || getAllColumnsValues.Rows[row][dc.ColumnName.ToString()] == ""
                                    || getAllColumnsValues.Rows[row][dc.ColumnName.ToString()] == "N/A")
                                {
                                    var Column_ID = getTokens.Where(f => (!string.IsNullOrEmpty(f.ColName)) && f.ColName.ToLower() == dc.ColumnName.ToString().ToLower()).FirstOrDefault();

                                    //var Column_ID = _ICertificateService.GetAllIssueDocTypeCol(ref count, ColName: dc.ColumnName.ToString()
                                    //                                                            , IssueDocTypeId: IssueDocTypeId).FirstOrDefault();

                                    if (Column_ID != null)
                                    {
                                        var alreadyExists = paramColsHasNotValueList.Where(x => x.ColsName.ToLower() == dc.ColumnName.ToString().ToLower())
                                                                                    .FirstOrDefault();
                                        if (alreadyExists == null)
                                        {
                                            paramColsHasNotValueList.Add(new IssueDocColsValue
                                            {
                                                ColsName = dc.ColumnName.ToString(),
                                                ColsId = Column_ID.IssueDocTypeColsId.ToString(),
                                                ColsValue = "N/A"
                                            });
                                        }
                                    }
                                }
                                else if (TokensShownInForm.Where(f => f.ColName.ToLower() == dc.ColumnName.ToString().ToLower()).Count() > 0)
                                {
                                    var Column_ID = getTokens.Where(f => (!string.IsNullOrEmpty(f.ColName)) && f.ColName.ToLower() == dc.ColumnName.ToString().ToLower()).FirstOrDefault();
                                    var alreadyExists = paramColsHasNotValueList.Where(x => x.ColsName.ToLower() == dc.ColumnName.ToString().ToLower())
                                                                                   .FirstOrDefault();
                                    if (alreadyExists == null)
                                    {
                                        paramColsHasNotValueList.Add(new IssueDocColsValue
                                        {
                                            ColsName = dc.ColumnName.ToString(),
                                            ColsId = Column_ID.IssueDocTypeColsId.ToString(),
                                            ColsValue = (getAllColumnsValues.Rows[row][dc.ColumnName.ToString()]).ToString()
                                        });
                                    }
                                }

                            }
                        }
                    }
                    #region
                    //receipt type
                    //if (ReceiptTypeId > 0)
                    //    getReceiptType = _IFeeService.GetReceiptTypeById(ReceiptTypeId);


                    //getDoc Template from IssueDocType
                    //var CheckFeeCertificate = _ICertificateService.GetIssueDocTypeById(IssueDocTypeId);
                    //if (CheckFeeCertificate != null)
                    //{
                    //    if (CheckFeeCertificate.IsFeeCertificate == null)
                    //        isFlagFeeCertificate = false;
                    //    else
                    //    {
                    //        if ((bool)CheckFeeCertificate.IsFeeCertificate)
                    //            isFlagFeeCertificate = true;
                    //    }
                    //}

                    //var getDocTemplate = _ICertificateService.GetAllIssueDocTemplates(ref count, IssueDocTypeId: IssueDocTypeId).Select(x => x.IssueDocTemplateId).ToArray();
                    //var tempDetail = _ICertificateService.GetAllIssueDocTemplateDetails(ref count);
                    //tempDetail = tempDetail.Where(x => getDocTemplate.Contains((int)x.IssueDocTemplateId) && x.EndDate == null).ToList();
                    //if (tempDetail.Count > 0)
                    //{
                    //    if (!string.IsNullOrEmpty(tempDetail.FirstOrDefault().TemplateText))
                    //    {
                    //        var vAL = tempDetail.FirstOrDefault().TemplateText.Split('%');
                    //        for (var i = 0; i <= vAL.Count() - 1; i++)
                    //        {
                    //            if (i % 2 != 0)
                    //            {
                    //                var Cols = vAL[i].ToString();
                    //                var getColValues = _ICertificateService.GetAllIssueDocTypeCol(ref count);
                    //                getColValues = getColValues.Where(x => x.ColName.ToLower() == Cols.ToString().ToLower()).ToList();
                    //                if (getColValues.FirstOrDefault() != null)
                    //                {
                    //                    string ColVal = "";
                    //                    if (isFlagFeeCertificate)
                    //                    {
                    //                        // calculate the token values and fee calculation of student  in a given session
                    //                        var session = _ICatalogMasterService.GetSessionById(SessionId: Session).Session1;

                    //                        if (!String.IsNullOrEmpty(AnnualSelection))
                    //                        {
                    //                            if (AnnualSelection.ToLower() == "annual")
                    //                            {
                    //                                if (getColValues.FirstOrDefault().ColName.Contains("Session"))
                    //                                {
                    //                                    ColVal = session == null ? "" : session;
                    //                                }
                    //                            }
                    //                            else
                    //                            {
                    //                                if (getColValues.FirstOrDefault().ColName.Contains("Session"))
                    //                                {
                    //                                    ColVal = FromDate + "-" + ToDate;
                    //                                }
                    //                            }

                    //                            if (getColValues.FirstOrDefault().ColName.Contains("ReceiptType"))
                    //                            {
                    //                                ColVal = getReceiptType == null ? "" : getReceiptType.ReceiptType1 + "-" + getReceiptType.ReceiptTypeId;
                    //                            }
                    //                            //if (getColValues.FirstOrDefault().ColName.Contains("DaughterOf"))
                    //                            //{
                    //                            //    ColVal = "D/o";
                    //                            //}
                    //                            //else if (getColValues.FirstOrDefault().ColName.Contains("SonOf"))
                    //                            //{
                    //                            //    ColVal = "S/o";
                    //                            //}
                    //                        }
                    //                    }

                    //                    if (string.IsNullOrEmpty(getColValues.FirstOrDefault().TableColName) ||
                    //                           string.IsNullOrEmpty(getColValues.FirstOrDefault().TableName) ||
                    //                           (bool)getColValues.FirstOrDefault().IsTC)
                    //                    {
                    //                        var alreadyExists = paramColsHasNotValueList.Where(x => x.ColsName.ToLower() == Cols.ToLower()).FirstOrDefault();
                    //                        if (alreadyExists == null)
                    //                        {
                    //                            paramColsHasNotValueList.Add(new IssueDocColsValue
                    //                            {
                    //                                ColsName = Cols,
                    //                                ColsId = getColValues.FirstOrDefault().IssueDocTypeColsId.ToString(),
                    //                                ColsValue = ColVal
                    //                            });
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    #endregion
                }
                return Json(new { paramColsHasNotValueList, status = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SetSessionDates(int sessionId = 0)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                var sessionStartDate = "";
                var sessionEndDate = "";
                if (sessionId > 0)
                {
                    var session = _ICatalogMasterService.GetSessionById(SessionId: sessionId);
                    if (session != null)
                    {
                        sessionStartDate = session.StartDate.Value.Date.ToString("dd/MM/yyyy");
                        sessionEndDate = session.EndDate.Value.Date.ToString("dd/MM/yyyy");
                    }
                }
                return Json(new { sessionStartDate, sessionEndDate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetGroupFeeHeads(int ReceiptType_Id = 0)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                var listHeads = new List<SelectListItem>();
                //if (!string.IsNullOrEmpty(Session))
                //    int.TryParse(_ICustomEncryption.base64d(Session), out Session_Id);

                if (ReceiptType_Id > 0)
                {
                    var FeeHeadIds = _IFeeService.GetAllGroupFeeHeads(ref count)
                                                .Where(x => x.ReceiptTypeId == ReceiptType_Id).Select(x => x.FeeHeadId).ToArray();

                    var getGroupFeeHeadDetails = _IFeeService.GetAllFeeHeads(ref count)
                                                .Where(x => FeeHeadIds.Contains(x.FeeHeadId)).ToList();

                    foreach (var head in getGroupFeeHeadDetails)
                    {
                        listHeads.Add(new SelectListItem
                        {
                            Text = head.FeeHead1,
                            Value = head.FeeHeadId.ToString(),
                            Selected = false
                        });
                    }
                }

                return Json(new { listHeads }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region IssueDocType
        public IssueDocTypeModel PrepareIssueDocTypeModel(IssueDocTypeModel model)
        {
            //paging
            model.gridPageSizes = _ISchoolSettingService.GetorSetSchoolData("gridPageSizes", "20,50,100,250").AttributeValue;
            model.defaultGridPageSize = _ISchoolSettingService.GetorSetSchoolData("defaultGridPageSize", "20").AttributeValue;


            var getAllDoctypes = _ICertificateService.GetAllIssueDocType(ref count);
            model.AvailableFilterIssueDocType.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var docId in getAllDoctypes)
            {
                model.AvailableFilterIssueDocType.Add(new SelectListItem
                {
                    Selected = false,
                    Text = docId.IssueDocType1,
                    Value = docId.IssueDocTypeId.ToString(),
                });
            }
           

            return model;
        }

        public string CheckIssueDocTypeError(IssueDocTypeModel model)
        {
            StringBuilder errormsg = new StringBuilder();

            if (string.IsNullOrEmpty(model.IssueDocType) || string.IsNullOrWhiteSpace(model.IssueDocType))
                errormsg.Append("Issue Document Type required ε");
            else // check unique
            {
                // decrypt values
                int IssueDocTypeIdK = 0;
                if (!string.IsNullOrEmpty(model.EncIssueDocTypeId))
                    int.TryParse(_ICustomEncryption.base64d(model.EncIssueDocTypeId), out IssueDocTypeIdK);

                var IsIssueDocTypeExist = _ICertificateService.GetAllIssueDocType(ref count, IssueDocType: model.IssueDocType
                                                                                            , IsLikeFilter: false);
                IsIssueDocTypeExist = IsIssueDocTypeExist.Where(m => m.IssueDocTypeId != IssueDocTypeIdK).ToList();
                if (IsIssueDocTypeExist.Count > 0)
                    errormsg.Append("Issue Document Type already exist ε");
            }
            return errormsg.ToString();
        }

        public ActionResult ManageIssueDocType()
        {
            SchoolUser user = new SchoolUser();
            var model = new IssueDocTypeModel();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                model = PrepareIssueDocTypeModel(model);


                //authentication
                //model.IsAuthToGirdAdd = _IUserAuthorizationService.IsUserAuthorize(user, "BookMaster", "ManageBookAttribute", "Add Record");
                //model.IsAuthToGridDelete = _IUserAuthorizationService.IsUserAuthorize(user, "BookMaster", "ManageBookAttribute", "Delete Record");
                //model.IsAuthToGridEdit = _IUserAuthorizationService.IsUserAuthorize(user, "BookMaster", "ManageBookAttribute", "Edit Record");

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return View(model);
            }
        }

        public ActionResult GetAllIssueDocumentTypes(DataSourceRequest command, IssueDocTypeModel model
                                                                              , IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                var DocTypeList = _ICertificateService.GetAllIssueDocType(ref count, IssueDocType: model.IssueDocType, IsLikeFilter: true);
                var DocTypeListCopy = DocTypeList;
                var gridModel = new DataSourceResult
                {
                    Data = DocTypeList.Select(p =>
                    {
                        var Doctypes = new IssueDocTypeModel();
                        Doctypes.EncIssueDocTypeId = _ICustomEncryption.base64e(p.IssueDocTypeId.ToString());
                        Doctypes.IssueDocTypeId = p.IssueDocTypeId;
                        Doctypes.IssueDocType = p.IssueDocType1;
                        Doctypes.IssueOnceOnly = p.IssueOnceOnly != null ? (bool)p.IssueOnceOnly : false;
                        Doctypes.OnceInAYear = p.OnceInAYear != null ? (bool)p.OnceInAYear : false;
                        Doctypes.IsFeeCertificate = p.IsFeeCertificate != null ? (bool)p.IsFeeCertificate : false;
                        Doctypes.CertificateFee = p.CertificateFee != null ? Convert.ToString(p.CertificateFee) : "";
                        Doctypes.SeriesRef = "";

                        if (p.SeriesStart != null)
                        {
                            Doctypes.SeriesRef = "Self";
                            Doctypes.SeriesStart = p.SeriesStart.ToString();
                        }
                        else if(p.SeriesRef!=null)
                        {
                            Doctypes.strSeriesRefId = p.SeriesRef.ToString();
                            var DocType = DocTypeList.Where(f => f.IssueDocTypeId == p.SeriesRef).FirstOrDefault();
                            Doctypes.SeriesStart = "";
                            if (DocType != null)
                            {
                                Doctypes.SeriesRef = DocType.IssueDocType1;
                            }
                        }

                        var hasDocTemplate = p.IssueDocTemplates.Count();
                        var hasIssueDoc = p.IssueDocs.Count();
                        var hasDocTypeCols = p.IssueDocTypeCols.Count();


                        Doctypes.IsAuthToDelete = false;
                        if (hasDocTemplate > 0 || hasIssueDoc > 0 || hasDocTypeCols > 0)
                            Doctypes.IsAuthToDelete = true;


                        Doctypes.IsExistTokens = false;
                        if (hasDocTypeCols > 0)
                            Doctypes.IsExistTokens = true;

                        return Doctypes;

                    }).AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = DocTypeList.Count()
                };

                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult ManageTokensList(string DocTypeId = "")
        {
            SchoolUser user = new SchoolUser();
            var model = new IssueDocTypeModel();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                int IssueDocTypeIdK = 0;
                if (!string.IsNullOrEmpty(DocTypeId))
                    int.TryParse(_ICustomEncryption.base64d(DocTypeId), out IssueDocTypeIdK);

                var currentSession = _ICatalogMasterService.GetCurrentSession();
                var dtTokens = _ICertificateService.GetAllIssueDocTokensListbySp(ref count,
                                                        SessionId: (int)currentSession.SessionId);
                model.IsCertificateIssued = false;
                
                if (IssueDocTypeIdK > 0)
                {
                    model.IssueDocTypeId = IssueDocTypeIdK;
                    var AllTokens = _ICertificateService.GetAllIssueDocTypeCol(ref count, IssueDocTypeId: IssueDocTypeIdK);
                    var gettokens = AllTokens.Select(x => x.ColName).Distinct().ToList();
                    DataTable dtExistingTokens = dtTokens;
                    foreach (var i in gettokens)
                    {
                        dtExistingTokens.Columns.Remove(i);
                        dtExistingTokens.AcceptChanges();
                    }

                    int LeftcolumnCount = 0;
                    int RightcolumnCount = 0;
                    foreach (var i in gettokens)
                    {
                        LeftcolumnCount++;
                        model.LeftTokenLists.Add(new SelectListItem
                        {
                            Text = i,
                            Value = LeftcolumnCount.ToString(),
                            Selected = false
                        });
                    }

                    foreach (DataColumn i in dtExistingTokens.Columns)
                    {
                        RightcolumnCount++;
                        model.RightTokenLists.Add(new SelectListItem
                        {
                            Text = i.ColumnName,
                            Value = RightcolumnCount.ToString(),
                            Selected = false
                        });
                    }
                    var AllIssueDocTpeColIds = AllTokens.Select(f => f.IssueDocTypeColsId).Distinct().ToArray();
                    if (AllIssueDocTpeColIds != null && AllIssueDocTpeColIds.Count() > 0)
                    {
                        var IssueDocDetails = _ICertificateService.GetAllIssueDocDetail(ref count).Where(f => AllIssueDocTpeColIds.Contains((int)f.IssueDocTypeColId)).ToList();
                        if (IssueDocDetails.Count > 0)
                        {
                            model.IsCertificateIssued = true;
                        }
                    }

                    var EditableTokens = AllTokens.Where(f => f.ShowAlwaysInForm == true);
                    model.EditableTokenLists.Add(new SelectListItem {Selected=true, Text = "--Select Token--", Value = "" });
                    model.SaveTokenLists.Add(new SelectListItem {Selected=true, Text = "--Select Token--", Value = "" });
                    foreach (var item in AllTokens)
                    {
                        model.SaveTokenLists.Add(new SelectListItem { Text = item.ColName, Value = item.IssueDocTypeColsId.ToString() });
                    }
                    foreach (var item in EditableTokens)
                        model.EditableTokenLists.Add(new SelectListItem { Text = item.ColName, Value = item.IssueDocTypeColsId.ToString() });
                }
                else
                {
                    int columnCount = 0;
                    foreach (DataColumn dc in dtTokens.Columns)
                    {
                        columnCount++;
                        model.RightTokenLists.Add(new SelectListItem
                        {
                            Text = dc.ColumnName,
                            Value = columnCount.ToString(),
                            Selected = false
                        });
                    }
                }

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return View(model);
            }
        }

        public ActionResult IsAlwaysShownInForm(int tokenid)
        {
            try
            {
                if (tokenid > 0)
                {
                    var IssueDocTypeCol = _ICertificateService.GetAllIssueDocTypeCol(ref count).Where(f => f.IssueDocTypeColsId == tokenid).FirstOrDefault();
                    if (IssueDocTypeCol != null)
                    {
                        var IsChecked= false;
                        if(IssueDocTypeCol.ShowAlwaysInForm!=null && IssueDocTypeCol.ShowAlwaysInForm==true)
                            IsChecked = true;
                        return Json(new
                        {
                            status = "success",
                            Checked = IsChecked
                        });
                    }
                }
                return Json(new
                {
                    status = "failed",
                    data = "Invalid Token"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult GetEditableIssueDocColList(int issudoctypeid)
        {
            try
            {
                if (issudoctypeid > 0)
                {
                    var AllIssueDocTypeCols = _ICertificateService.GetAllIssueDocTypeCol(ref count).Where(f=>f.IssueDocTypeId== issudoctypeid && f.ShowAlwaysInForm==true).ToList();
                    IList<SelectListItem> list = new List<SelectListItem>();
                    foreach (var item in AllIssueDocTypeCols)
                    {
                        list.Add(new SelectListItem { Value = item.IssueDocTypeColsId.ToString(), Text = item.ColName });
                    }
                    return Json(new
                    {
                        status = "success",
                        List = list
                    },JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "error",
                    data = "Invalid Token"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }
        public ActionResult UpdateEditableToken(int tokenid,bool IsShown)
        {
            try
            {
                if (tokenid > 0)
                {
                    var IssueDocTypeCol = _ICertificateService.GetIssueDocTypeColById(tokenid);
                    if (IssueDocTypeCol != null)
                    {
                        IssueDocTypeCol.ShowAlwaysInForm = IsShown;
                        _ICertificateService.UpdateIssueDocTypeCol(IssueDocTypeCol);

                        return Json(new
                        {
                            status = "success",
                            data = "Update SuccessFully"
                        });
                    }
                }
                return Json(new
                {
                    status = "error",
                    data = "Invalid Token"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }
        public ActionResult InsertUpdateIssueDocType(IssueDocTypeModel model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                string errormsg = CheckIssueDocTypeError(model);
                if (string.IsNullOrEmpty(errormsg))
                {
                    // decrypt values
                    int IssueDocTypeIdK = 0;
                    if (!string.IsNullOrEmpty(model.EncIssueDocTypeId))
                        int.TryParse(_ICustomEncryption.base64d(model.EncIssueDocTypeId), out IssueDocTypeIdK);

                    KSModel.Models.IssueDocType IssueDocTypeData = new KSModel.Models.IssueDocType();
                    if (IssueDocTypeIdK > 0)
                    {
                        //update module
                        IssueDocTypeData = _ICertificateService.GetIssueDocTypeById(IssueDocTypeIdK, true);
                        if (IssueDocTypeData != null)
                        {
                            IssueDocTypeData.IssueDocType1 = model.IssueDocType;
                            IssueDocTypeData.IssueOnceOnly = model.IssueOnceOnly;
                            IssueDocTypeData.OnceInAYear = model.OnceInAYear;
                            IssueDocTypeData.IsFeeCertificate = model.IsFeeCertificate;
                            IssueDocTypeData.CertificateFee = Convert.ToDecimal(model.CertificateFee);
                            if(model.intSeriesStart>0)
                            {
                                IssueDocTypeData.SeriesStart = model.intSeriesStart;
                                IssueDocTypeData.SeriesRef = null;
                            }
                            else if(model.SeriesRefId !=null  && model.SeriesRefId > 0)
                            {
                                IssueDocTypeData.SeriesRef = model.SeriesRefId;
                            }
                            _ICertificateService.UpdateIssueDocType(IssueDocTypeData);

                            message = "Update successfully";
                        }
                    }
                    else
                    {
                        // Insert new module
                        IssueDocTypeData = new KSModel.Models.IssueDocType();
                        IssueDocTypeData.IssueDocType1 = model.IssueDocType;
                        IssueDocTypeData.IssueOnceOnly = model.IssueOnceOnly;
                        IssueDocTypeData.OnceInAYear = model.OnceInAYear;
                        IssueDocTypeData.IsFeeCertificate = model.IsFeeCertificate;
                        IssueDocTypeData.CertificateFee = Convert.ToDecimal(model.CertificateFee);
                        if (model.intSeriesStart > 0)
                        {
                            IssueDocTypeData.SeriesStart = model.intSeriesStart;
                            IssueDocTypeData.SeriesRef = null;
                        }
                        else if (model.SeriesRefId != null && model.SeriesRefId > 0)
                        {
                            IssueDocTypeData.SeriesRef = model.SeriesRefId;
                        }
                        _ICertificateService.InsertIssueDocType(IssueDocTypeData);

                        message = "Added successfully";
                    }
                }
                else
                    return Json(new { status = "failed", data = errormsg });

                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }
        public ActionResult GetSeriesRefList(int editIssueDocTypeId)
        {
            try
            {
                var getAllDoctypes = _ICertificateService.GetAllIssueDocType(ref count).Where(f => f.SeriesStart != null);
                if (editIssueDocTypeId > 0) {
                    getAllDoctypes = getAllDoctypes.Where(f => f.IssueDocTypeId != editIssueDocTypeId);
                }
                IList<SelectListItem>SeriesRefList=new List<SelectListItem>();
                SeriesRefList.Add(new SelectListItem { Text = "Self", Value = "", Selected = true });
                foreach (var item in getAllDoctypes)
                    SeriesRefList.Add(new SelectListItem { Text = item.IssueDocType1, Value = item.IssueDocTypeId.ToString(), Selected = false });
                return Json(new
                {
                    status = "success",
                    List = SeriesRefList
                },JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
            
        }
        public ActionResult DeleteIssueDocType(string IssueDocTypeId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                // decrypt values
                int IssueDocTypeIdK = 0;
                if (!string.IsNullOrEmpty(IssueDocTypeId))
                    int.TryParse(_ICustomEncryption.base64d(IssueDocTypeId), out IssueDocTypeIdK);

                if (IssueDocTypeIdK > 0)
                {
                    _ICertificateService.DeleteIssueDocType(IssueDocTypeIdK);
                    message = "Deleted successfully";
                }
                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult SaveIssueDocTokens(IssueDocTypeModel model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int IssueDocTypeIdK = 0;
                if (!string.IsNullOrEmpty(model.EncIssueDocTypeId))
                    int.TryParse(_ICustomEncryption.base64d(model.EncIssueDocTypeId), out IssueDocTypeIdK);

                KSModel.Models.IssueDocTypeCol IssueDocTypeColData = new KSModel.Models.IssueDocTypeCol();
                if (IssueDocTypeIdK > 0)
                {
                    if (!string.IsNullOrEmpty(model.LeftTokenVal))
                    {
                        var issueDOcColExists = _ICertificateService.GetAllIssueDocTypeCol(ref count, IssueDocTypeId: IssueDocTypeIdK);

                        //insert records
                        var arryVals = model.LeftTokenVal.TrimEnd(',').Split(',');
                        foreach (var tokenCol in arryVals)
                        {
                            var isExistsToken = _ICertificateService.GetAllIssueDocTypeCol(ref count, ColName: tokenCol, IssueDocTypeId: IssueDocTypeIdK)
                                                                .Select(x => x.IssueDocTypeColsId).FirstOrDefault();
                            if (isExistsToken == 0)
                            {
                                IssueDocTypeColData.ColName = tokenCol;
                                IssueDocTypeColData.IssueDocTypeId = IssueDocTypeIdK;
                                IssueDocTypeColData.IsTC = false;
                                IssueDocTypeColData.IsFormField = false;

                                _ICertificateService.InsertIssueDocTypeCol(IssueDocTypeColData);
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(model.RightTokenVal))
                    {
                        var issueDOcColExists = _ICertificateService.GetAllIssueDocTypeCol(ref count, IssueDocTypeId: IssueDocTypeIdK);

                        //insert records
                        var arryVals = model.RightTokenVal.TrimEnd(',').Split(',');
                        foreach (var tokenCol in arryVals)
                        {
                            var isExistsToken = _ICertificateService.GetAllIssueDocTypeCol(ref count, ColName: tokenCol, IssueDocTypeId: IssueDocTypeIdK)
                                                                .Select(x => x.IssueDocTypeColsId).FirstOrDefault();

                            if (isExistsToken != null && isExistsToken != 0)
                            {
                                var IsExistInIssueDocDetail = _ICertificateService.GetAllIssueDocDetail(ref count, IssueDocTypeColId: isExistsToken).ToList();
                                if (IsExistInIssueDocDetail.Count == 0)
                                {
                                    _ICertificateService.DeleteIssueDocTypeCol(isExistsToken);
                                }
                            }
                        }
                    }
                }
                TempData["success_DocMsges"] = "Tokens Saved Successfully";
                return RedirectToAction("ManageTokensList", new { DocTypeId = _ICustomEncryption.base64e(IssueDocTypeIdK.ToString()) });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }
        #endregion

        #region DcoumentTemplate

        public IssueDocTemplateModel prepareissuedoctemplatemodel(IssueDocTemplateModel model)
        {
            model.AvailableIssueDocType.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            //drop down Issue doc type
            var issuedoctype = _ICertificateService.GetAllIssueDocType(ref count);
            foreach (var docId in issuedoctype)
            {
                if (model.IssueDocTypeId > 0)
                {
                    int issuedoctypeid = 0;
                    if (int.TryParse(docId.IssueDocTypeId.ToString(), out issuedoctypeid))
                    {
                        if (issuedoctypeid == model.IssueDocTypeId)
                            model.AvailableIssueDocType.Add(new SelectListItem { Selected = true, Text = docId.IssueDocType1, Value = docId.IssueDocTypeId.ToString() });
                    }
                }
                else
                {
                    var getDocTemplate = _ICertificateService.GetAllIssueDocTemplates(ref count, IssueDocTypeId: docId.IssueDocTypeId).Select(x => x.IssueDocTemplateId).ToArray();
                    if (getDocTemplate.Count() > 0)
                    {
                        var getDocumentdetail = _ICertificateService.GetAllIssueDocTemplateDetails(ref count);
                        var documentTmepIds = getDocumentdetail.Where(x => getDocTemplate.Contains((int)x.IssueDocTemplateId)
                                                                                                            && x.EndDate == null);
                        if (documentTmepIds.Count() > 0)
                        {
                            //nothing
                        }
                        else
                        {
                            documentTmepIds = getDocumentdetail.Where(x => getDocTemplate.Contains((int)x.IssueDocTemplateId)).OrderByDescending(x => x.EndDate).Take(1);
                            if (documentTmepIds.Count() > 0)
                            {
                                model.AvailableIssueDocType.Add(new SelectListItem
                                {
                                    Selected = false,
                                    Text = docId.IssueDocType1,
                                    Value = docId.IssueDocTypeId.ToString(),
                                });
                            }
                        }
                    }
                    else
                    {
                        model.AvailableIssueDocType.Add(new SelectListItem
                        {
                            Selected = false,
                            Text = docId.IssueDocType1,
                            Value = docId.IssueDocTypeId.ToString(),
                        });
                    }

                    //documentTmepIds = documentTmepIds.Where(x => x.EndDate != null);
                    ////has doc assigned any html template
                    //if (documentTmepIds.Count() > 0)
                    //{
                    //    model.AvailableIssueDocType.Add(new SelectListItem
                    //    {
                    //        Selected = false,
                    //        Text = docId.IssueDocType1,
                    //        Value = docId.IssueDocTypeId.ToString(),
                    //    });
                    //}
                }
            }
            return model;
        }

        public ActionResult DocumentTemplate()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new IssueDocTemplateModel();
                model = prepareissuedoctemplatemodel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public IssueDocTemplateModel checkissuedoctemplateerrors(IssueDocTemplateModel model)
        {
            if (string.IsNullOrEmpty(model.TemplateTitle))
                ModelState.AddModelError("TemplateTitle", "Template title required");
            if (string.IsNullOrEmpty(model.TemplateText))
                ModelState.AddModelError("TemplateText", "Template text required");
            if (!string.IsNullOrEmpty(model.EffDate))
            {
                model.EffectiveDate = DateTime.ParseExact(model.EffDate, "dd/MM/yyyy", null);
            }

            if (!model.EffectiveDate.HasValue)
                ModelState.AddModelError("EffDate", "Effective from required");
            else
            {

                if (model.IssueDocTemplateId == null)
                {
                    if (model.EffectiveDate.Value.Date < DateTime.UtcNow.Date)
                        ModelState.AddModelError("EffDate", "Invalid Effective from");
                }
            }
            // check if Issue doc Already exist
            if (model.IssueDocTypeId > 0)
            {
                var issuedoctemps = _ICertificateService.GetAllIssueDocTemplates(ref count, model.IssueDocTypeId);
                if (model.IssueDocTemplateId > 0)
                    issuedoctemps = issuedoctemps.Where(i => i.IssueDocTemplateId != model.IssueDocTemplateId).ToList();

                var issuedoctempids = issuedoctemps.Select(i => i.IssueDocTemplateId).ToArray();
                // get issue doc template detail
                var issuedoctemdet = _ICertificateService.GetAllIssueDocTemplateDetails(ref count);
                issuedoctemdet = issuedoctemdet.Where(i => issuedoctempids.Contains((int)i.IssueDocTemplateId) && i.EndDate == null).ToList();
                issuedoctempids = issuedoctemdet.Select(i => (int)i.IssueDocTemplateId).Distinct().ToArray();
                var issuedocteamplate = issuedoctemps.Where(i => issuedoctempids.Contains(i.IssueDocTemplateId)).ToList();
                if (issuedocteamplate.Count > 0)
                    ModelState.AddModelError("IssueDocTypeId", "Document type already exist");
            }

            return model;
        }

        [HttpPost]
        public ActionResult DocumentTemplate(IssueDocTemplateModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                string message = "";
                model = checkissuedoctemplateerrors(model);

                if (ModelState.IsValid)
                {
                    // Activity log type add , edit
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    // stdclass Id                    
                    var IssueDocTemplatetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "IssueDocTemplate").FirstOrDefault();
                    var IssueDocTemplateDetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "IssueDocTemplateDetail").FirstOrDefault();

                    IssueDocTemplate IssueDocTemplate = new IssueDocTemplate();
                    IssueDocTemplateDetail IssueDocTemplateDetail = new IssueDocTemplateDetail();
                    // update
                    if (model.IssueDocTemplateId > 0)
                    {

                        IssueDocTemplate = _ICertificateService.GetIssueDocTemplateById((int)model.IssueDocTemplateId, true);
                        if (IssueDocTemplate != null)
                        {
                            IssueDocTemplate.TemplateTitle = model.TemplateTitle;
                            IssueDocTemplate.TemplateSubTitle = model.TemplateSubTitle;
                            _ICertificateService.UpdateIssueDocTemplate(IssueDocTemplate);
                            if (IssueDocTemplatetable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, IssueDocTemplatetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(IssueDocTemplate.IssueDocTemplateId, IssueDocTemplatetable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update IssueDocTemplate with IssueDocTemplateId:{0} and IssueDocTypeId:{1}", IssueDocTemplate.IssueDocTemplateId, IssueDocTemplate.IssueDocTypeId), Request.Browser.Browser);
                            }
                        }

                        var IssueDocTemplateDetails = _ICertificateService.GetAllIssueDocTemplateDetails(ref count, (int)model.IssueDocTemplateId);
                        IssueDocTemplateDetail = IssueDocTemplateDetails.Where(i => i.EndDate == null).FirstOrDefault();
                        if (IssueDocTemplateDetail != null)
                        {
                            IssueDocTemplateDetail.TemplateText = model.TemplateText;
                            _ICertificateService.UpdateIssueDocTemplateDetail(IssueDocTemplateDetail);
                            if (IssueDocTemplateDetailtable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, IssueDocTemplateDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(IssueDocTemplateDetail.IssueDocTemplateDetailId, IssueDocTemplateDetailtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update IssueDocTemplateDetail with IssueDocTemplateDetailId:{0} and IssueDocTemplateId:{1}", IssueDocTemplateDetail.IssueDocTemplateDetailId, IssueDocTemplateDetail.IssueDocTemplateId), Request.Browser.Browser);
                            }
                        }

                        message = "Document Template updated successfully";
                    }
                    else
                    {
                        IssueDocTemplate = new IssueDocTemplate();
                        IssueDocTemplate.IssueDocTypeId = model.IssueDocTypeId;
                        IssueDocTemplate.TemplateTitle = model.TemplateTitle;
                        IssueDocTemplate.TemplateSubTitle = model.TemplateSubTitle;
                        _ICertificateService.InsertIssueDocTemplate(IssueDocTemplate);
                        if (IssueDocTemplatetable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, IssueDocTemplatetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(IssueDocTemplate.IssueDocTemplateId, IssueDocTemplatetable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add IssueDocTemplate with IssueDocTemplateId:{0} and IssueDocTypeId:{1}", IssueDocTemplate.IssueDocTemplateId, IssueDocTemplate.IssueDocTypeId), Request.Browser.Browser);
                        }

                        IssueDocTemplateDetail = new IssueDocTemplateDetail();
                        IssueDocTemplateDetail.IssueDocTemplateId = IssueDocTemplate.IssueDocTemplateId;
                        IssueDocTemplateDetail.TemplateText = model.TemplateText;
                        IssueDocTemplateDetail.EffectiveDate = model.EffectiveDate;
                        _ICertificateService.InsertIssueDocTemplateDetail(IssueDocTemplateDetail);
                        if (IssueDocTemplateDetailtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, IssueDocTemplateDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(IssueDocTemplateDetail.IssueDocTemplateDetailId, IssueDocTemplateDetailtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add IssueDocTemplateDetail with IssueDocTemplateDetailId:{0} and IssueDocTemplateId:{1}", IssueDocTemplateDetail.IssueDocTemplateDetailId, IssueDocTemplateDetail.IssueDocTemplateId), Request.Browser.Browser);
                        }

                        message = "Document Template saved successfully";
                    }

                    TempData["SuccessNotification"] = message;
                    return RedirectToAction("DocumentTemplate");
                }

                model = prepareissuedoctemplatemodel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult GetAllDocTemplates(DataSourceRequest command, IssueDocTemplateModel model, string DocTypeStatus = "",
                               string DateFiltering = "", string FromDate = "", string ToDate = "", IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                var IssueDocTemplateIds = _ICertificateService.GetAllIssueDocTemplates(ref count,
                                                             IssueDocTypeId: Convert.ToInt32(model.IssueDocTypeId))
                                                             .Select(x => x.IssueDocTemplateId).ToArray();

                //bydefualt active dos showing n grid
                var doctemplatedetail = _ICertificateService.GetAllIssueDocTemplateDetails(ref count)
                                                .Where(x => IssueDocTemplateIds.Contains((int)x.IssueDocTemplateId)
                                                                                        && x.EndDate == null).ToList();

                switch (DocTypeStatus.ToLower())
                {
                    case "all":
                        doctemplatedetail = _ICertificateService.GetAllIssueDocTemplateDetails(ref count)
                                                .Where(x => IssueDocTemplateIds.Contains((int)x.IssueDocTemplateId)).ToList();
                        break;
                    case "closed":
                        doctemplatedetail = _ICertificateService.GetAllIssueDocTemplateDetails(ref count)
                                                .Where(x => IssueDocTemplateIds.Contains((int)x.IssueDocTemplateId)
                                                                                        && x.EndDate != null).ToList();
                        break;
                }

                DateTime? FrmDate = null;
                if (!string.IsNullOrEmpty(FromDate) || !string.IsNullOrWhiteSpace(FromDate))
                {
                    FrmDate = DateTime.ParseExact(FromDate, "dd/MM/yyyy", null);
                    FrmDate = FrmDate.Value.Date;
                }
                DateTime? toDate = null;
                if (!string.IsNullOrEmpty(ToDate) || !string.IsNullOrWhiteSpace(ToDate))
                {
                    toDate = DateTime.ParseExact(ToDate, "dd/MM/yyyy", null);
                    toDate = toDate.Value.Date;
                }

                //date filter according to selection
                switch (DateFiltering.ToLower())
                {
                    case "effdate":
                        if (FrmDate != null)
                            doctemplatedetail = doctemplatedetail.Where(x => x.EffectiveDate >= FrmDate).ToList();
                        if (toDate != null)
                            doctemplatedetail = doctemplatedetail.Where(x => x.EffectiveDate <= toDate).ToList();
                        break;
                    case "closeddate":
                        if (FrmDate != null)
                            doctemplatedetail = doctemplatedetail.Where(s => s.EndDate != null)
                                                                .Where(x => x.EndDate >= FrmDate).ToList();
                        if (toDate != null)
                            doctemplatedetail = doctemplatedetail.Where(s => s.EndDate != null)
                                                            .Where(x => x.EndDate <= toDate).ToList();
                        break;
                }
                var gridModel = new DataSourceResult
                {
                    Data = doctemplatedetail.Select(p =>
                    {
                        var IssueDocTempList = new IssueDocTemplateList();
                        IssueDocTempList.IssueDocTemplateId = p.IssueDocTemplateId;
                        IssueDocTempList.EncIssueDocTemplateId = _ICustomEncryption.base64e(p.IssueDocTemplateId.ToString());
                        IssueDocTempList.IsDeleted = true;

                        if (p.IssueDocTemplate != null)
                        {
                            IssueDocTempList.IssueDocTypeId = p.IssueDocTemplate.IssueDocType != null ? p.IssueDocTemplate.IssueDocTypeId : null;
                            IssueDocTempList.IssueDocType = p.IssueDocTemplate.IssueDocType != null ? p.IssueDocTemplate.IssueDocType.IssueDocType1 : "";
                            IssueDocTempList.TemplateTitle = p.IssueDocTemplate.TemplateTitle != null ? p.IssueDocTemplate.TemplateTitle : "";
                            IssueDocTempList.TemplateSubTitle = p.IssueDocTemplate.TemplateSubTitle != null ? p.IssueDocTemplate.TemplateSubTitle : "";

                            if (p.IssueDocTemplate.IssueDocTemplateHTMLs.Count > 0)
                                IssueDocTempList.IsDeleted = false;
                        }
                        //getDoc Template from IssueDocType
                        var hasAnyTemplate = _ICertificateService.GetAllIssueDoc(ref count, DocTemplateId: p.IssueDocTemplateId).ToList();
                        if (hasAnyTemplate.Count > 0)
                        {
                            IssueDocTempList.hasAnyTemplate = true;
                        }


                        IssueDocTempList.IsAssignHtml = false;
                        var assignHtmlTemplate = _ICertificateService.GetAllIssueDocTemplateHTMLs(ref count, IssueDocTemplateId: p.IssueDocTemplateId).ToList();
                        if (assignHtmlTemplate.Count > 0)
                        {
                            IssueDocTempList.IsAssignHtml = true;
                        }

                        IssueDocTempList.IssueDocHTMLTemplateId = 0;
                        var issuedochtmltemplate = _ICertificateService.GetAllIssueDocTemplateHTMLs(ref count, IssueDocTemplateId: p.IssueDocTemplateId,
                                                                                                    EndDate: null).ToList();
                        if (issuedochtmltemplate != null)
                        {
                            var active_issuedochtmltemplate = (from idht in issuedochtmltemplate where idht.EndDate == null select idht).FirstOrDefault();
                            if (active_issuedochtmltemplate != null)
                            {
                                var issuedochtmltemplateid = active_issuedochtmltemplate.IssueDocHTMLTemplateId;
                                if (issuedochtmltemplateid != null)
                                {
                                    IssueDocTempList.IssueDocHTMLTemplateId = issuedochtmltemplateid;
                                    IssueDocTempList.EncIssueDocHTMLTemplateId = _ICustomEncryption.base64e(issuedochtmltemplateid.ToString());
                                    IssueDocTempList.HtmlTitle = _ICertificateService.GetIssueDocHTMLTemplateById((int)issuedochtmltemplateid).HTMLTitle;
                                }
                            }
                        }

                        IssueDocTempList.EffDate = ConvertDate(p.EffectiveDate.Value);
                        IssueDocTempList.DTEndDate = p.EndDate != null ? p.EndDate : null;

                        return IssueDocTempList;
                    }).AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = doctemplatedetail.Count()
                };

                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult CheckIssueDocTypeUniqueness(int IDTID, int? IDTempID = null) // Issue doc type Id
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                IList<SelectListItem> issuedoctypecolslist = new List<SelectListItem>();
                // check if Issue doc Already exist
                string createdDate = "";
                if (IDTID > 0)
                {
                    var issuedoctemps = _ICertificateService.GetAllIssueDocTemplates(ref count, IDTID);
                    if (IDTempID > 0)
                        issuedoctemps = issuedoctemps.Where(i => i.IssueDocTemplateId == IDTempID).ToList();

                    var issuedoctempids = issuedoctemps.Select(i => i.IssueDocTemplateId).ToArray();
                    // get issue doc template detail
                    var issuedoctemdet = _ICertificateService.GetAllIssueDocTemplateDetails(ref count);

                    issuedoctemdet = issuedoctemdet.Where(i => issuedoctempids.Contains((int)i.IssueDocTemplateId) && i.EndDate == null).ToList();

                    //if doc temp closed and create new then i will be created after closing end date
                    var closeddocTemp = _ICertificateService.GetAllIssueDocTemplateDetails(ref count).Where(i => issuedoctempids.Contains((int)i.IssueDocTemplateId) && i.EndDate == null).FirstOrDefault();
                    if (closeddocTemp == null)
                    {
                        closeddocTemp = _ICertificateService.GetAllIssueDocTemplateDetails(ref count).Where(i => issuedoctempids.Contains((int)i.IssueDocTemplateId) && i.EndDate != null).OrderByDescending(x => x.EndDate).Take(1).FirstOrDefault();
                    }
                    if (closeddocTemp != null)
                    {
                        if (closeddocTemp.EndDate != null)
                        {
                            if (closeddocTemp.EndDate.Value.Date <= DateTime.Today.Date)
                                createdDate = DateTime.Today.Date.ToString("dd/MM/yyyy");
                            else
                                createdDate = closeddocTemp.EndDate.Value.AddDays(1).Date.ToString("dd/MM/yyyy");
                        }
                        else
                            createdDate = closeddocTemp.EffectiveDate.Value.Date.ToString("dd/MM/yyyy");

                        //createdDate = closeddocTemp.EndDate == null ? closeddocTemp.EffectiveDate.Value.Date.ToString("dd/MM/yyyy") : closeddocTemp.EndDate.Value.AddDays(1).Date.ToString("dd/MM/yyyy");
                    }

                    if (IDTempID == null)
                    {
                        issuedoctempids = issuedoctemdet.Select(i => (int)i.IssueDocTemplateId).Distinct().ToArray();
                        var issuedocteamplate = issuedoctemps.Where(i => issuedoctempids.Contains(i.IssueDocTemplateId)).ToList();
                        if (issuedocteamplate.Count > 0)
                            return Json(new
                            {
                                status = "failed",
                                data = "Document Type already exist"
                            });

                    }

                    // get issue doc type 
                    var issuedoctype = _ICertificateService.GetIssueDocTypeById(IDTID);
                    if (issuedoctype != null)
                    {
                        bool? IsTC = false;
                        var issuedoctypecols = new List<IssueDocTypeCol>();
                        issuedoctypecols = _ICertificateService.GetAllIssueDocTypeCol(ref count, IssueDocTypeId: IDTID).ToList();

                        var ListTokens = new List<IssueDocTypeCol>();

                        foreach (var token in issuedoctypecols)
                        {
                            //    if (issuedoctype.IsFeeCertificate != null)
                            //    {
                            //        if ((bool)issuedoctype.IsFeeCertificate)//tax certificate
                            //        {
                            //            //for tax certificate
                            //            ListTokens = issuedoctypecols.Where(x => (x.IsTC == false && x.IssueDocTypeId != null)
                            //                                            || (x.IsTC == false && x.IssueDocTypeId == null)).ToList();
                            //            break;
                            //        }
                            //        else
                            //        {
                            //            if (issuedoctype.IssueDocType1.ToLower().Contains("transfer"))
                            //            {
                            //                //for transfer certificate
                            //                ListTokens = issuedoctypecols.Where(x => x.IsTC == true).ToList();
                            //                break;
                            //            }
                            //            else
                            //            {
                            //                //for other certificate
                            //                ListTokens = issuedoctypecols.Where(x => x.IsTC == false && x.IssueDocTypeId == null).ToList();
                            //                break;
                            //            }
                            //        }
                            //    }
                            //    else
                            //    {
                            //        if (issuedoctype.IssueDocType1.ToLower().Contains("transfer"))
                            //        {
                            //            //for transfer certificate
                            //            ListTokens = issuedoctypecols.Where(x => x.IsTC == true).ToList();
                            //            break;
                            //        }
                            //        else
                            //        {
                            //            //for other certificate
                            //            ListTokens = issuedoctypecols.Where(x => x.IsTC == false && x.IssueDocTypeId == null).ToList();
                            //            break;
                            //        }
                            //    }
                            //}

                            //foreach (var tokens in ListTokens)
                            // {
                            issuedoctypecolslist.Add(new SelectListItem
                            {
                                Selected = false,
                                Text = token.ColName,
                                Value = token.IssueDocTypeColsId.ToString()
                            });
                            // }
                        }
                    }
                }

                return Json(new
                {
                    status = "success",
                    data = issuedoctypecolslist,
                    createdDate
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult GetDocTypeTemplates(string issuedocid = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            var IsuueDoc_id = 0;
            string PayableAmt = "";
            try
            {
                var paramColsHasNotValueList = new List<SelectListItem>();
                var CrtSeries = 0;
                if (!string.IsNullOrEmpty(issuedocid))
                {

                    if (issuedocid.Contains("filter_"))
                    {
                        var id = issuedocid.Split('_');
                        IsuueDoc_id = Convert.ToInt32(id[1]);
                    }
                    else
                        IsuueDoc_id = Convert.ToInt32(issuedocid);

                   
                    var getIssueDocInfo = _ICertificateService.GetIssueDocTypeById(IsuueDoc_id);
                    if (getIssueDocInfo != null)
                    {
                        PayableAmt = getIssueDocInfo.CertificateFee.Value.ToString("0.00");

                        var AllIssueDocTypes = _ICertificateService.GetAllIssueDocType(ref count);
                        var AllIssueDocs = _ICertificateService.GetAllIssueDoc(ref count);
                        if (getIssueDocInfo.SeriesStart != null)
                        {
                            var GetAllIssueDOcTypeRefIds = AllIssueDocTypes.Where(f => f.SeriesRef != null
                              && f.SeriesRef == getIssueDocInfo.IssueDocTypeId).Select(f => f.IssueDocTypeId).Distinct().ToList();
                            GetAllIssueDOcTypeRefIds.Add(getIssueDocInfo.IssueDocTypeId);
                            var MaxIssueDocNo = AllIssueDocs.Where(f => f.DocNo != null && f.IssueDocTypeId != null && GetAllIssueDOcTypeRefIds.Contains((int)f.IssueDocTypeId)).OrderByDescending(f => f.DocNo).FirstOrDefault();
                            if (MaxIssueDocNo != null)
                            {
                                //CrtSeries = (int)MaxIssueDocNo.DocNo;
                                if(MaxIssueDocNo.DocNo< getIssueDocInfo.SeriesStart)
                                {
                                    CrtSeries = (int)getIssueDocInfo.SeriesStart;
                                }
                                else
                                {
                                    CrtSeries = (int)MaxIssueDocNo.DocNo + 1;
                                }
                            }
                            else
                            {
                                CrtSeries = (int)getIssueDocInfo.SeriesStart;
                            }
                        }
                        else if (getIssueDocInfo.SeriesRef != null)
                        {
                            var SeriesRefIssueDocTypeId = AllIssueDocTypes.Where(f => f.IssueDocTypeId == getIssueDocInfo.SeriesRef).FirstOrDefault();
                            if (SeriesRefIssueDocTypeId != null && SeriesRefIssueDocTypeId.SeriesStart!=null)
                            {
                                var GetAllIssueDOcTypeRefIds = AllIssueDocTypes.Where(f =>
                                   f.IssueDocTypeId == getIssueDocInfo.SeriesRef || f.SeriesRef == getIssueDocInfo.SeriesRef).Select(f => f.IssueDocTypeId).Distinct().ToArray();
                                if (GetAllIssueDOcTypeRefIds != null && GetAllIssueDOcTypeRefIds.Count() > 0)
                                {
                                    var MaxIssueDoc = AllIssueDocs.Where(f => f.DocNo != null && f.IssueDocTypeId != null && GetAllIssueDOcTypeRefIds.Contains((int)f.IssueDocTypeId)).OrderByDescending(f => f.DocNo).FirstOrDefault();
                                    if (MaxIssueDoc != null)
                                    {
                                        if (MaxIssueDoc.DocNo < SeriesRefIssueDocTypeId.SeriesStart)
                                        {
                                            CrtSeries = (int)SeriesRefIssueDocTypeId.SeriesStart;
                                        }
                                        else
                                        {
                                            CrtSeries = (int)MaxIssueDoc.DocNo + 1;
                                        }
                                    }
                                }
                                else
                                {
                                    CrtSeries = (int)SeriesRefIssueDocTypeId.SeriesStart;
                                }
                            }
                        }
                    }
                    // get issue doc detail 
                    //var issuedocdetail = _ICertificateService.GetAllIssueDocDetail(ref count, IsuueDoc_id);
                    //var issuedoccolids = issuedocdetail.Select(i => i.IssueDocTypeColId).ToArray();

                    //// get issuedoc cols
                    //var issdoccols = _ICertificateService.GetAllIssueDocTypeCol(ref count);
                    //issdoccols = issdoccols.Where(i => issuedoccolids.Contains(i.IssueDocTypeColsId)).ToList();
                    //// loop on 
                    //foreach (var colHasNoValue in issdoccols)
                    //{
                    //    if (string.IsNullOrEmpty(colHasNoValue.TableColName) || string.IsNullOrEmpty(colHasNoValue.TableName))
                    //    {
                    //        paramColsHasNotValueList.Add(new SelectListItem
                    //        {
                    //            Text = colHasNoValue.ColName,
                    //            Value=_ICustomEncryption.ToString()
                    //        });
                    //    }
                    //}


                    //var getDocTemplate = _ICertificateService.GetAllIssueDocTemplates(ref count, IssueDocTypeId: IsuueDoc_id);
                    //if (getDocTemplate.Count > 0)
                    //{
                    //    foreach (var temp in getDocTemplate)
                    //    {
                    //        var tempDetail = _ICertificateService.GetAllIssueDocTemplateDetails(ref count,
                    //                                    IssueDocTemplateId: temp.IssueDocTemplateId).Where(x => x.EndDate == null).FirstOrDefault();
                    //        if (tempDetail != null)
                    //        {
                    //            paramDocTemplatesList.Add(new SelectListItem
                    //            {
                    //                Text = temp.TemplateTitle,
                    //                Value = _ICustomEncryption.base64e(temp.IssueDocTemplateId.ToString())
                    //            });
                    //        }
                    //    }
                    //}
                 

                }
                return Json(new { paramColsHasNotValueList, PayableAmt, CrtSeries }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult List()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new IssueDocTemplateModel();
                // get list of Document Templates
                var doctemplates = _ICertificateService.GetAllIssueDocTemplates(ref count);
                // loop
                foreach (var item in doctemplates)
                {
                    //get do ctemplate detail
                    var IssueDocTempList = new IssueDocTemplateList();
                    var doctemplatedetail = _ICertificateService.GetAllIssueDocTemplateDetails(ref count, item.IssueDocTemplateId);
                    foreach (var itemdet in doctemplatedetail)
                    {
                        IssueDocTempList = new IssueDocTemplateList();
                        IssueDocTempList.IssueDocTypeId = item.IssueDocType.IssueDocTypeId;
                        IssueDocTempList.IssueDocType = item.IssueDocType.IssueDocType1;
                        IssueDocTempList.TemplateTitle = item.TemplateTitle;

                        //getDoc Template from IssueDocType
                        var hasAnyTemplate = _ICertificateService.GetAllIssueDoc(ref count, DocTemplateId: itemdet.IssueDocTemplateId).ToList();
                        if (hasAnyTemplate.Count > 0)
                        {
                            IssueDocTempList.hasAnyTemplate = true;
                        }

                        var issuedochtmltemplate = _ICertificateService.GetAllIssueDocTemplateHTMLs(ref count, IssueDocTemplateId: item.IssueDocTemplateId, EndDate: null).ToList();
                        if (issuedochtmltemplate != null)
                        {
                            var active_issuedochtmltemplate = (from idht in issuedochtmltemplate where idht.EndDate == null select idht).FirstOrDefault();
                            if (active_issuedochtmltemplate != null)
                            {
                                var issuedochtmltemplateid = active_issuedochtmltemplate.IssueDocHTMLTemplateId;

                                if (issuedochtmltemplateid != null)
                                {
                                    IssueDocTempList.IssueDocHTMLTemplateId = issuedochtmltemplateid;
                                    IssueDocTempList.EncIssueDocHTMLTemplateId = _ICustomEncryption.base64e(issuedochtmltemplateid.ToString());
                                    IssueDocTempList.HtmlTitle = _ICertificateService.GetIssueDocHTMLTemplateById((int)issuedochtmltemplateid).HTMLTitle;
                                }
                            }
                        }
                        if (item.TemplateSubTitle != null)
                            IssueDocTempList.TemplateSubTitle = item.TemplateSubTitle;
                        IssueDocTempList.EffDate = ConvertDate(itemdet.EffectiveDate.Value);
                        IssueDocTempList.EndDate = itemdet.EndDate.HasValue ? ConvertDate(itemdet.EndDate.Value) : "";
                        IssueDocTempList.IssueDocTemplateId = item.IssueDocTemplateId;
                        IssueDocTempList.EncIssueDocTemplateId = _ICustomEncryption.base64e(item.IssueDocTemplateId.ToString());
                        IssueDocTempList.IsDeleted = true;
                        if (item.IssueDocTemplateHTMLs.Count > 0)
                            IssueDocTempList.IsDeleted = false;

                        model.IssueDocTemplateList.Add(IssueDocTempList);
                    }
                }

                //check is there doc template avaiable for create new 
                var getAllDoctypes = _ICertificateService.GetAllIssueDocType(ref count);
                foreach (var docId in getAllDoctypes)
                {
                    var getDocTemplate = _ICertificateService.GetAllIssueDocTemplates(ref count, IssueDocTypeId: docId.IssueDocTypeId).Select(x => x.IssueDocTemplateId).ToArray();
                    if (getDocTemplate.Count() > 0)
                    {
                        var getDocumentdetail = _ICertificateService.GetAllIssueDocTemplateDetails(ref count);
                        var documentTmepIds = getDocumentdetail.Where(x => getDocTemplate.Contains((int)x.IssueDocTemplateId)
                                                                                                            && x.EndDate == null);
                        if (documentTmepIds.Count() > 0)
                        {
                            //nothing
                        }
                        else
                        {
                            documentTmepIds = getDocumentdetail.Where(x => getDocTemplate.Contains((int)x.IssueDocTemplateId)).OrderByDescending(x => x.EndDate).Take(1);
                            if (documentTmepIds.Count() > 0)
                            {
                                model.AvailableIssueDocType.Add(new SelectListItem
                                {
                                    Selected = false,
                                    Text = docId.IssueDocType1,
                                    Value = docId.IssueDocTypeId.ToString(),
                                });
                            }
                        }
                    }
                    else
                    {
                        model.AvailableIssueDocType.Add(new SelectListItem
                        {
                            Selected = false,
                            Text = docId.IssueDocType1,
                            Value = docId.IssueDocTypeId.ToString(),
                        });
                    }
                }

                model.AvailableFilterIssueDocType.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
                foreach (var docId in getAllDoctypes)
                {
                    model.AvailableFilterIssueDocType.Add(new SelectListItem
                    {
                        Selected = false,
                        Text = docId.IssueDocType1,
                        Value = docId.IssueDocTypeId.ToString(),
                    });
                }

                // html template list
                model.AvailableIssueDocHTMLTemplate.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
                var issuedochtmltemplates = _ICertificateService.GetAllIssueDocHTMLTemplates(ref count);
                foreach (var item in issuedochtmltemplates)
                    model.AvailableIssueDocHTMLTemplate.Add(new SelectListItem { Selected = false, Text = item.HTMLTitle, Value = item.IssueDocHTMLTemplateId.ToString() });


                //paging
                model.gridPageSizes = _ISchoolSettingService.GetorSetSchoolData("gridPageSizes", "20,50,100,250").AttributeValue;
                model.defaultGridPageSize = _ISchoolSettingService.GetorSetSchoolData("defaultGridPageSize", "20").AttributeValue;

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult Edit(string id = "", string CreateNew = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // check id exist
                if (!string.IsNullOrEmpty(id))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(id);
                    int issuedoctemplateid = 0;
                    if (int.TryParse(qsid, out issuedoctemplateid))
                    {
                        var model = new IssueDocTemplateModel();
                        // get list of Document Templates
                        var issuedoctemplate = _ICertificateService.GetIssueDocTemplateById(issuedoctemplateid);
                        if (issuedoctemplate != null)
                        {
                            var IssueDocTemplateDetails = _ICertificateService.GetAllIssueDocTemplateDetails(ref count, (int)issuedoctemplate.IssueDocTemplateId);
                            var IssueDocTemplateDetail = IssueDocTemplateDetails.Where(i => i.EndDate == null).FirstOrDefault();
                            if (IssueDocTemplateDetail == null)
                                IssueDocTemplateDetail = IssueDocTemplateDetails.Where(i => i.EndDate != null).FirstOrDefault();

                            model.IssueDocTypeId = (int)issuedoctemplate.IssueDocTypeId;
                            model.IssueDocTemplateId = issuedoctemplate.IssueDocTemplateId;

                            model.EffDate = ConvertDate(IssueDocTemplateDetail.EffectiveDate.Value);
                            if (IssueDocTemplateDetail.EndDate != null)
                            {
                                if (IssueDocTemplateDetail.EndDate.Value.Date >= DateTime.Today.Date
                                        || IssueDocTemplateDetail.EndDate.Value.Date <= DateTime.Today.Date)
                                {
                                    model.EffDate = ConvertDate(DateTime.Today.Date);
                                }
                            }

                            model.TemplateTitle = issuedoctemplate.TemplateTitle;
                            if (issuedoctemplate.TemplateSubTitle != null)
                                model.TemplateSubTitle = issuedoctemplate.TemplateSubTitle;
                            model.TemplateText = IssueDocTemplateDetail.TemplateText;
                        }

                        model = prepareissuedoctemplatemodel(model);

                        if (!string.IsNullOrEmpty(CreateNew))
                            model.IssueDocTemplateId = 0;

                        return View(model);
                    }
                }

                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult CloseIssueDocTemplate(int IDTID, DateTime EndDate)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                if (EndDate < DateTime.UtcNow.Date)
                {
                    return Json(new
                    {
                        status = "failed",
                        data = "Invalid end date"
                    });
                }

                // Activity log type Update
                var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                var IssueDocTemplateDetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "IssueDocTemplateDetail").FirstOrDefault();
                var issuedoctemplate = _ICertificateService.GetIssueDocTemplateById(IDTID);
                if (issuedoctemplate != null)
                {
                    var IssueDocTemplateDetails = _ICertificateService.GetAllIssueDocTemplateDetails(ref count, (int)issuedoctemplate.IssueDocTemplateId);
                    var IssueDocTemplateDetail = IssueDocTemplateDetails.Where(i => i.EndDate == null).FirstOrDefault();
                    if (IssueDocTemplateDetail != null)
                    {
                        IssueDocTemplateDetail.EndDate = EndDate;
                        _ICertificateService.UpdateIssueDocTemplateDetail(IssueDocTemplateDetail);
                        // table log saved or not
                        if (IssueDocTemplateDetailtable != null)
                        {
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, IssueDocTemplateDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(IssueDocTemplateDetail.IssueDocTemplateDetailId, IssueDocTemplateDetailtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update IssueDocTemplateDetail with IssueDocTemplateDetailId:{0} and IssueDocTemplateId:{1}", IssueDocTemplateDetail.IssueDocTemplateDetailId, IssueDocTemplateDetail.IssueDocTemplateId), Request.Browser.Browser);
                        }
                    }
                }

                return Json(new
                {
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }

        public ActionResult Delete(int id = 0)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // get list of Document Templates
                var issuedoctemplate = _ICertificateService.GetIssueDocTemplateById(id);
                if (issuedoctemplate != null)
                {
                    var logtypeDel = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
                    var IssueDocTemplatetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "IssueDocTemplate").FirstOrDefault();
                    var IssueDocTemplateDetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "IssueDocTemplateDetail").FirstOrDefault();


                    var IssueDocTemplateDetails = _ICertificateService.GetAllIssueDocTemplateDetails(ref count, (int)issuedoctemplate.IssueDocTemplateId);
                    if (issuedoctemplate.IssueDocTemplateHTMLs.Count > 0)
                    {
                        return Json(new
                        {
                            status = "failed",
                            data = "You can't delete this template"
                        });
                    }
                    else
                    {
                        foreach (var item in IssueDocTemplateDetails)
                        {
                            _ICertificateService.DeleteIssueDocTemplateDetail(item.IssueDocTemplateDetailId);
                            if (IssueDocTemplateDetailtable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, IssueDocTemplateDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(item.IssueDocTemplateDetailId, IssueDocTemplateDetailtable.TableId, logtypeDel.ActivityLogTypeId, user, String.Format("Delete IssueDocTemplateDetail with IssueDocTemplateDetailId:{0} and IssueDocTemplateId:{1}", item.IssueDocTemplateDetailId, item.IssueDocTemplateId), Request.Browser.Browser);
                            }
                        }

                        _ICertificateService.DeleteIssueDocTemplate(issuedoctemplate.IssueDocTemplateId);
                        if (IssueDocTemplatetable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, IssueDocTemplatetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(issuedoctemplate.IssueDocTemplateId, IssueDocTemplatetable.TableId, logtypeDel.ActivityLogTypeId, user, String.Format("Delete IssueDocTemplate with IssueDocTemplateId:{0} and IssueDocTypeId:{1}", issuedoctemplate.IssueDocTemplateId, issuedoctemplate.IssueDocTypeId), Request.Browser.Browser);
                        }
                    }
                }

                return Json(new
                {
                    status = "success",
                    data = "Deleted successfully"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        #endregion

        #region AssignHtmlTemplate
        public ActionResult HTMLTemplate(string Id, string HtmlTemplateId = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new IssueDocTemplateModel();
                model.AvailableIssueDocHTMLTemplate.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
                var issuedochtmltemplates = _ICertificateService.GetAllIssueDocHTMLTemplates(ref count);

                // decrypt values
                int IssueDocTempIdK = 0;
                if (!string.IsNullOrEmpty(Id))
                    int.TryParse(_ICustomEncryption.base64d(Id), out IssueDocTempIdK);

                int HtmlTemplateIdK = 0;
                if (!string.IsNullOrEmpty(HtmlTemplateId))
                {
                    if (HtmlTemplateId != "0")
                        HtmlTemplateIdK = Convert.ToInt32(HtmlTemplateId);
                }

                // check id exist
                if (IssueDocTempIdK > 0)
                {
                    var isAleadryAssigned = _ICertificateService.GetAllIssueDocTemplateHTMLs(ref count,
                                                IssueDocTemplateId: IssueDocTempIdK, EndDate: null).FirstOrDefault();
                    if (isAleadryAssigned != null)
                    {
                        foreach (var item in issuedochtmltemplates)
                        {
                            if (HtmlTemplateIdK == item.IssueDocHTMLTemplateId)
                            {
                                model.IssueDocHTMLTemplateId = HtmlTemplateIdK;
                                model.EffDate = isAleadryAssigned.EffectiveDate.Value.Date.ToString("dd/MM/yyyy");
                            }

                            model.AvailableIssueDocHTMLTemplate.Add(new SelectListItem
                            {
                                Selected = false,
                                Text = item.HTMLTitle,
                                Value = item.IssueDocHTMLTemplateId.ToString()
                            });
                        }
                    }
                    else
                    {
                        foreach (var item in issuedochtmltemplates)
                        {
                            model.AvailableIssueDocHTMLTemplate.Add(new SelectListItem
                            {
                                Selected = false,
                                Text = item.HTMLTitle,
                                Value = item.IssueDocHTMLTemplateId.ToString()
                            });
                        }
                    }
                }

                // check id exist
                //if (!string.IsNullOrEmpty(id))
                //{
                //    // decrypt Id
                //    var qsid = _ICustomEncryption.base64d(id);
                //    int issuedoctemplateid = 0;
                //    if (int.TryParse(qsid, out issuedoctemplateid))
                //    {
                //        var Issue_doc_Template_Id = Convert.ToInt32(issuedoctemplateid);
                //        //var Issue_doc_html_Template_Id = Convert.ToInt32(IssueDocHTMLTemplateId);

                //        model.IssueDocTemplateId = Issue_doc_Template_Id;
                //        //model.IssueDocHTMLTemplateId = Issue_doc_html_Template_Id;

                //        // html template list


                //        return View(model);
                //    }
                //}

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult GetFullIssueDocHTMLTemplate(string IDOCTID, int IDOCHTEMPID = 0) // Issue doc Template Id and Issue Doc HTML Template ID
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // check same htmltemplate already mapped with Document template
                // chek if already mapping exist
                string alreadymapped = "";
                // decrypt values
                int IDOCTIDK = 0;
                int? EncIDOCTIDK = null;
                if (!string.IsNullOrEmpty(IDOCTID))
                {
                    int.TryParse(_ICustomEncryption.base64d(IDOCTID), out IDOCTIDK);
                    EncIDOCTIDK = IDOCTIDK;
                }

                if (IDOCHTEMPID > 0)
                {
                    var dochtmltemplates = _ICertificateService.GetAllIssueDocTemplateHTMLs(ref count, IssueDocTemplateId: EncIDOCTIDK,
                                                                                    IssueDocHTMLTemplateId: IDOCHTEMPID);
                    dochtmltemplates = dochtmltemplates.Where(i => i.EndDate == null).ToList();
                    if (dochtmltemplates.Count > 0)
                        alreadymapped = "Same document already mapped";
                }

                var CerificateWidthHeight = _ISchoolSettingService.GetorSetSchoolData("CertificateWidthHeight", "100x100").AttributeValue;
                string fulldoc = "";
                // get html temlate by id
                var issuedochtmltemplate = _ICertificateService.GetIssueDocHTMLTemplateById(IDOCHTEMPID);
                if (issuedochtmltemplate != null)
                {
                    XElement div = XElement.Parse(issuedochtmltemplate.HTMLTemplateText.Replace("<br>", "<br/>"));
                    if (CerificateWidthHeight != null)
                    {
                        var MeaurementCetificate = CerificateWidthHeight.Split('x');
                        div.SetAttributeValue("Width", MeaurementCetificate[0]);
                        div.SetAttributeValue("Height", MeaurementCetificate[1]);
                    }
                    fulldoc += div;
                }
                // get issue doc template 
                var issuedoctemplate = _ICertificateService.GetIssueDocTemplateById((int)EncIDOCTIDK);
                if (issuedoctemplate != null)
                {
                    // get template text 
                    var alldoctemplates = _ICertificateService.GetAllIssueDocTemplateDetails(ref count, issuedoctemplate.IssueDocTemplateId).FirstOrDefault();
                    //var doctemplatetext = alldoctemplates.Where(i => i.EndDate == null).FirstOrDefault();
                    if (alldoctemplates != null)
                        fulldoc = fulldoc.Replace("%DocumentText%", alldoctemplates.TemplateText);
                }

                var schoolinfo = _ISchoolDataService.GetSchoolDataDetail(ref count).FirstOrDefault();
                // replace parameters
                // logo
                //var logo = _WebHelper.GetStoreLocation() + "/Content/assets/img/logo.png";
                var logo = (schoolinfo != null && schoolinfo.Logo != null) ? schoolinfo.Logo : "";
                var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                string path = _WebHelper.GetStoreLocation() + "/Images/" + SchoolDbId;
                fulldoc = fulldoc.Replace("%Logo%", path + "/" + logo);
                // School Name
                //string SchoolName = "Digitech KSmart School";
                string SchoolName = (schoolinfo != null && schoolinfo.SchoolName != null) ? schoolinfo.SchoolName : "";
                string SchoolAddress = (schoolinfo != null && schoolinfo.SchoolAddress != null) ? schoolinfo.SchoolAddress : "";
                string SchoolEmail = (schoolinfo != null && schoolinfo.SchoolEmail != null) ? schoolinfo.SchoolEmail : "";
                string SchoolWebsite = (schoolinfo != null && schoolinfo.DisplayWebsite != null) ? schoolinfo.DisplayWebsite : "";
                string SchoolPhone= (schoolinfo != null && schoolinfo.DisplayPhone != null) ? schoolinfo.DisplayPhone : "";
                fulldoc = fulldoc.Replace("%SchoolName%", SchoolName);
                fulldoc = fulldoc.Replace("%SchoolAddress%", SchoolAddress);
                fulldoc = fulldoc.Replace("%SchoolEmail%", SchoolEmail);
                fulldoc = fulldoc.Replace("%SchoolWebsite%", SchoolWebsite);
                fulldoc = fulldoc.Replace("%SchoolPhone%", SchoolPhone);

                // Document Name
                string DocumentName = issuedoctemplate.TemplateTitle;
                fulldoc = fulldoc.Replace("%DocumentName%", DocumentName);

                if (!string.IsNullOrEmpty(issuedoctemplate.TemplateSubTitle))
                    fulldoc = fulldoc.Replace("%DocumentSubTitle%", "<br/>" + issuedoctemplate.TemplateSubTitle);
                else
                    fulldoc = fulldoc.Replace("%DocumentSubTitle%", "");

                fulldoc = fulldoc.Replace("%DocumentType%", issuedoctemplate.IssueDocType.IssueDocType1);

                // Ref no
                string RefNo = "1002101";
                fulldoc = fulldoc.Replace("%RefNo%", RefNo);
                string BookNo = "1002101";
                fulldoc = fulldoc.Replace("%BookNo%", BookNo);
                string AdmissionNo = "1002101";
                fulldoc = fulldoc.Replace("%AdmissionNo%", AdmissionNo);
                // Current Date
                string Date = ConvertDate(DateTime.UtcNow);
                fulldoc = fulldoc.Replace("%Date%", Date);

                if (fulldoc != "")
                {
                    return Json(new
                    {
                        status = "success",
                        data = fulldoc,
                        msg = alreadymapped
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        data = "No Document found"
                    });
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult InsertDocHTMLTemplateMapping(IssueDocHTMLTemplateMapping model) // Issue doc Template Id and Issue Doc HTML Template ID
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // check validation
                var validationstring = new StringBuilder();
                if (model.IssueDocHTMLTemplateId == null || model.IssueDocHTMLTemplateId == 0)
                    validationstring.Append("HTML Template requiredε");
                if (!model.EffectiveDate.HasValue)
                    validationstring.Append("Effective from requiredε");
                else
                {
                    if (model.EffectiveDate.Value.Date < DateTime.UtcNow.Date)
                        validationstring.Append("Effective from should not be less than from current dateε");
                }
                if (string.IsNullOrEmpty(validationstring.ToString()))
                {
                    // Activity log type add , edit
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    // stdclass Id                    
                    var IssueDocHTMLTemplatetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "IssueDocHTMLTemplate").FirstOrDefault();


                    // decrypt values
                    int IssueDocTempIdK = 0;
                    if (!string.IsNullOrEmpty(model.IssueDocTempId))
                        int.TryParse(_ICustomEncryption.base64d(model.IssueDocTempId), out IssueDocTempIdK);

                    IssueDocTemplateHTML IssueDocTemplateHTML = new IssueDocTemplateHTML();
                    // chek if already mapping exist
                    var dochtmltemplates = _ICertificateService.GetAllIssueDocTemplateHTMLs(ref count, IssueDocTemplateId: IssueDocTempIdK);
                    IssueDocTemplateHTML = dochtmltemplates.Where(i => i.EndDate == null).FirstOrDefault();
                    // update
                    if (IssueDocTemplateHTML != null)
                    {
                        if (IssueDocTemplateHTML.IssueDocHTMLTemplateId != model.IssueDocHTMLTemplateId)
                        {
                            IssueDocTemplateHTML.EndDate = DateTime.UtcNow;
                            _ICertificateService.UpdateIssueDocTemplateHTML(IssueDocTemplateHTML);
                            if (IssueDocHTMLTemplatetable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, IssueDocHTMLTemplatetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(IssueDocTemplateHTML.IssueDocTemplateHTMLId, IssueDocHTMLTemplatetable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update IssueDocTemplateHTML with IssueDocTemplateHTMLId:{0} and IssueDocHTMLTemplateId:{1}", IssueDocTemplateHTML.IssueDocTemplateHTMLId, IssueDocTemplateHTML.IssueDocHTMLTemplateId), Request.Browser.Browser);
                            }

                            IssueDocTemplateHTML = new IssueDocTemplateHTML();
                            IssueDocTemplateHTML.EffectiveDate = model.EffectiveDate;
                            IssueDocTemplateHTML.IssueDocHTMLTemplateId = model.IssueDocHTMLTemplateId;
                            IssueDocTemplateHTML.IssueDocTemplateId = IssueDocTempIdK;
                            _ICertificateService.InsertIssueDocTemplateHTML(IssueDocTemplateHTML);
                            if (IssueDocHTMLTemplatetable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, IssueDocHTMLTemplatetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(IssueDocTemplateHTML.IssueDocTemplateHTMLId, IssueDocHTMLTemplatetable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add IssueDocTemplateHTML with IssueDocTemplateHTMLId:{0} and IssueDocHTMLTemplateId:{1}", IssueDocTemplateHTML.IssueDocTemplateHTMLId, IssueDocTemplateHTML.IssueDocHTMLTemplateId), Request.Browser.Browser);
                            }
                        }
                    }
                    else
                    {
                        IssueDocTemplateHTML = new IssueDocTemplateHTML();
                        IssueDocTemplateHTML.EffectiveDate = model.EffectiveDate;
                        IssueDocTemplateHTML.IssueDocHTMLTemplateId = model.IssueDocHTMLTemplateId;
                        IssueDocTemplateHTML.IssueDocTemplateId = IssueDocTempIdK;
                        _ICertificateService.InsertIssueDocTemplateHTML(IssueDocTemplateHTML);
                        if (IssueDocHTMLTemplatetable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, IssueDocHTMLTemplatetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(IssueDocTemplateHTML.IssueDocTemplateHTMLId, IssueDocHTMLTemplatetable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add IssueDocTemplateHTML with IssueDocTemplateHTMLId:{0} and IssueDocHTMLTemplateId:{1}", IssueDocTemplateHTML.IssueDocTemplateHTMLId, IssueDocTemplateHTML.IssueDocHTMLTemplateId), Request.Browser.Browser);
                        }
                    }

                    return Json(new
                    {
                        status = "success",
                        msg = "Document Mapped Successfully"
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        data = validationstring.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        #endregion

        #region PrintCertificate
        public ActionResult PrintCertificate(string IssueDocId = "", string StudentId = "", string hasHeader = "", string DetailType = ""
                                       , string IsFeeCertificate = "", string Period = "", string Session = "", string ClassId = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                string fulldoc = "";
                // get student by id
                int Student_id = 0;
                int IssueDocId_id = 0;
                int Session_Id = 0;
                int ReceiptType_Id = 0;
                DateTime? startDate = null;
                DateTime? EndDate = null;
                int Class_id = 0;
                if (!string.IsNullOrEmpty(StudentId))
                {
                    int.TryParse(_ICustomEncryption.base64d(StudentId), out Student_id);
                    int.TryParse(_ICustomEncryption.base64d(IssueDocId), out IssueDocId_id);

                    if (!string.IsNullOrEmpty(Session))
                        int.TryParse(_ICustomEncryption.base64d(Session), out Session_Id);


                    if (!string.IsNullOrEmpty(ClassId))
                        int.TryParse(_ICustomEncryption.base64d(ClassId), out Class_id);


                    var student = _IStudentService.GetStudentById(Student_id);
                    // check same htmltemplate already mapped with Document template
                    // chek if already mapping exist
                    // get issue doc
                    var issuedoc = _ICertificateService.GetIssueDocById(IssueDocId_id, true);
                    if (issuedoc != null)
                    {
                        // detail
                        var issuedoctemplatesdetails = _ICertificateService.GetAllIssueDocTemplateDetails(ref count);

                        var issuedoctemplatedetail = issuedoctemplatesdetails.Where(i => i.IssueDocTemplateId == issuedoc.DocTemplateId && i.EndDate == null).FirstOrDefault();
                        if (issuedoctemplatedetail == null)
                        {
                            issuedoctemplatedetail = issuedoctemplatesdetails.Where(i => i.IssueDocTemplateId == issuedoc.DocTemplateId && i.EndDate != null)
                                                                                .OrderByDescending(x => x.EffectiveDate).FirstOrDefault();
                        }

                        if (issuedoctemplatedetail != null)
                        {
                            var issuedoctemplate = issuedoctemplatedetail.IssueDocTemplate;

                            var dochtmltemplates = _ICertificateService.GetAllIssueDocTemplateHTMLs(ref count, IssueDocTemplateId: issuedoctemplate.IssueDocTemplateId);
                            var dochtmltemplate = dochtmltemplates.Where(i => i.EndDate == null).FirstOrDefault();
                            if (dochtmltemplate != null)
                            {
                                var CerificateWidthHeight = _ISchoolSettingService.GetorSetSchoolData("CertificateWidthHeight", "100x100").AttributeValue;

                                // get html temlate by id
                                var issuedochtmltemplate = _ICertificateService.GetIssueDocHTMLTemplateById((int)dochtmltemplate.IssueDocHTMLTemplateId);
                                if (issuedochtmltemplate != null)
                                {
                                    XElement div = XElement.Parse(issuedochtmltemplate.HTMLTemplateText.Replace("<br>", "<br/>"));
                                    if (CerificateWidthHeight != null)
                                    {
                                        var MeaurementCetificate = CerificateWidthHeight.Split('x');
                                        div.SetAttributeValue("Width", MeaurementCetificate[0]);
                                        div.SetAttributeValue("Height", MeaurementCetificate[1]);
                                    }
                                    fulldoc = div.ToString();

                                    #region feeCertificate charges dynamically
                                    ///fee certificate + add all types of charges///
                                    if (IsFeeCertificate != null)
                                    {
                                        if (IsFeeCertificate.ToLower() == "true")
                                        {
                                            if (issuedoc.IssueDocDetails.Count() > 0)
                                            {
                                                var SessionDate = issuedoc.IssueDocDetails.Where(x => x.IssueDocTypeCol.ColName.ToLower() == "session").FirstOrDefault();
                                                if (SessionDate != null)
                                                {
                                                    if (SessionDate.ColValue.Contains('/'))
                                                    {
                                                        Period = SessionDate.ColValue;
                                                        var Date = SessionDate.ColValue.Split('-');
                                                        if (!string.IsNullOrEmpty(Date[0]))
                                                        {
                                                            startDate = Convert.ToDateTime(Date[0]);
                                                            startDate = startDate.Value.Date;
                                                        }
                                                        if (!string.IsNullOrEmpty(Date[1]))
                                                        {
                                                            EndDate = Convert.ToDateTime(Date[1]);
                                                            EndDate = EndDate.Value.Date;
                                                        }
                                                    }
                                                }
                                                var ReceiptTypeID = issuedoc.IssueDocDetails.Where(x => x.IssueDocTypeCol.ColName.ToLower() == "receipttype").FirstOrDefault();
                                                if (ReceiptTypeID != null)
                                                {
                                                    if (!string.IsNullOrEmpty(ReceiptTypeID.ColValue))
                                                    {
                                                        var id = ReceiptTypeID.ColValue.Split('-');
                                                        if (!string.IsNullOrEmpty(id[1]))
                                                            ReceiptType_Id = Convert.ToInt32(id[1]);
                                                    }
                                                }
                                            }
                                            var FeeReceiptsDetail = new List<FeeReceiptDetail>();
                                            var FeeDetail = new DataTable();
                                            var FeeHeadIds = _ICertificateService.GetAllIssueDocFeeHeads(ref count, IssueDocId: issuedoc.IssueDocId)
                                                                                                          .Select(x => x.FeeHeadId).Distinct().ToArray();

                                            if (string.IsNullOrEmpty(Period))
                                            {
                                                //annual charges
                                                var Standard1s = _ICatalogMasterService.GetClassMasterById(Class_id);
                                                if (Standard1s != null)
                                                {
                                                    var FeeClassGroupIds = _IFeeService.GetAllFeeClassGroupDetails(ref count, StandardId: (int)Standard1s.StandardId)
                                                                                                            .Select(x => x.FeeClassGroupId).ToArray();
                                                    var FeeTypeIds = _IFeeService.GetAllGroupFeeTypes(ref count).Where(x => FeeClassGroupIds.Contains(x.FeeClassGroupId))
                                                                                                .Select(x => x.FeeTypeId).ToArray();
                                                    var FeeFrequencyIds = _IFeeService.GetAllFeeTypes(ref count).Where(x => FeeTypeIds.Contains(x.FeeTypeId)).Select(x => x.FeeFrequencyId).ToArray();
                                                    var FeePeriodIds = _IFeeService.GetAllFeePeriods(ref count).Where(x => FeeFrequencyIds.Contains(x.FeeFrequencyId))
                                                                                                .Select(x => x.FeePeriodId).ToArray();

                                                    var getSession = _ICatalogMasterService.GetSessionById(Session_Id);
                                                    var PeriodIds = _IFeeService.GetAllFeePeriodDetails(ref count).Where(x => FeePeriodIds.Contains((int)x.FeePeriodId)
                                                                                    && x.SessionId == Session_Id && x.PeriodStartDate >= getSession.StartDate.Value.Date
                                                                                        && x.PeriodEndDate <= getSession.EndDate.Value.Date).Select(x => x.FeePeriodId).Distinct().ToArray();

                                                    //get all receipts
                                                    if (ReceiptType_Id > 0)
                                                    {
                                                        string ReceiptTypeId = ReceiptType_Id.ToString();
                                                        FeeDetail = _IFeeService.PrintFeeReceiptCertificateBySp(ref count, ReceiptTypeId: ReceiptTypeId,
                                                                                    SessionId: Session_Id, StudentId: (int)Student_id);

                                                        //var AnnuallyFeeRecipts = _IFeeService.GetAllFeeReceipts(ref count, StudentId: (int)Student_id,
                                                        //                           SessionId: Session_Id,
                                                        //                           ReceiptTypeId: ReceiptType_Id).Where(x => PeriodIds.Contains(x.FeePeriodId))
                                                        //                           .Select(x => x.ReceiptId).ToArray();

                                                        //FeeReceiptsDetail = _IFeeService.GetAllFeeReceiptDetails(ref count)
                                                        //                            .Where(x => AnnuallyFeeRecipts.Contains((int)x.ReceiptId)
                                                        //                            && x.GroupFeeHeadId != null && x.GroupFeeHead.FeeHead.UseInFeeCertificate == true).ToList();
                                                    }
                                                    else
                                                    {
                                                        //if receipt type is selected all
                                                        //get receipts which are for tax certificate only
                                                        // var feeHeadIds = _IFeeService.GetAllFeeHeads(ref count).Where(x => x.UseInFeeCertificate == true).Select(x => x.FeeHeadId).ToArray();
                                                        //var ReceiptTypeIds = _IFeeService.GetAllReceiptTypeFeeHeads(ref count, IsActive: true).Where(x => FeeHeadIds.Contains((int)x.FeeHeadId))
                                                        //                                                .Select(x => x.ReceiptTypeId).Distinct().ToArray();

                                                        var ReceiptTypeIds = _IFeeService.GetAllReceiptTypeFeeHeads(ref count, IsActive: true)
                                                                                                .Where(x => FeeHeadIds.Contains((int)x.FeeHeadId))
                                                                                                .Select(x => x.ReceiptTypeId).Distinct().ToArray();
                                                        string ReceiptTyIds = "";
                                                        if (ReceiptTypeIds != null)
                                                        {
                                                            foreach (var ids in ReceiptTypeIds)
                                                            {
                                                                ReceiptTyIds += ids + ",";
                                                            }
                                                            ReceiptTyIds = ReceiptTyIds.Trim(',');

                                                            //get receipts which are for tax certificate only
                                                            FeeDetail = _IFeeService.PrintFeeReceiptCertificateBySp(ref count, ReceiptTypeId: ReceiptTyIds,
                                                                                                             SessionId: Session_Id, StudentId: (int)Student_id);

                                                        }

                                                        //var AnnuallyFeeRecipts = _IFeeService.GetAllFeeReceipts(ref count, StudentId: (int)Student_id,
                                                        //                            SessionId: Session_Id)
                                                        //                            .Where(x => PeriodIds.Contains(x.FeePeriodId))
                                                        //                            .Select(x => x.ReceiptId).ToArray();

                                                        //FeeReceiptsDetail = _IFeeService.GetAllFeeReceiptDetails(ref count)
                                                        //                            .Where(x => AnnuallyFeeRecipts.Contains((int)x.ReceiptId)
                                                        //                            && x.GroupFeeHeadId != null && x.GroupFeeHead.FeeHead.UseInFeeCertificate == true).ToList();
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //periodic charges 
                                                var Standards = _ICatalogMasterService.GetClassMasterById(Class_id);
                                                if (Standards != null)
                                                {
                                                    var FeeClassGroupIds = _IFeeService.GetAllFeeClassGroupDetails(ref count, StandardId: (int)Standards.StandardId)
                                                                                                            .Select(x => x.FeeClassGroupId).ToArray();
                                                    var FeeTypeIds = _IFeeService.GetAllGroupFeeTypes(ref count).Where(x => FeeClassGroupIds.Contains(x.FeeClassGroupId))
                                                                                                .Select(x => x.FeeTypeId).ToArray();
                                                    var FeeFrequencyIds = _IFeeService.GetAllFeeTypes(ref count).Where(x => FeeTypeIds.Contains(x.FeeTypeId)).Select(x => x.FeeFrequencyId).ToArray();
                                                    var FeePeriodIds = _IFeeService.GetAllFeePeriods(ref count).Where(x => FeeFrequencyIds.Contains(x.FeeFrequencyId))
                                                                                                .Select(x => x.FeePeriodId).ToArray();
                                                    var PeriodIds = _IFeeService.GetAllFeePeriodDetails(ref count).Where(x => FeePeriodIds.Contains((int)x.FeePeriodId)
                                                                                    && x.SessionId == Session_Id && x.PeriodStartDate >= startDate.Value.Date
                                                                                        && x.PeriodEndDate <= EndDate.Value.Date).Select(x => x.FeePeriodId).Distinct().ToArray();


                                                    if (ReceiptType_Id > 0)
                                                    {
                                                        string ReceiptTypeId = ReceiptType_Id.ToString();
                                                        FeeDetail = _IFeeService.PrintFeeReceiptCertificateBySp(ref count, ReceiptTypeId: ReceiptTypeId,
                                                                                    SessionId: Session_Id, StudentId: (int)Student_id);


                                                        //var AnnuallyFeeRecipts = _IFeeService.GetAllFeeReceipts(ref count, StudentId: (int)Student_id,
                                                        //                           SessionId: Session_Id, ReceiptTypeId: ReceiptType_Id)
                                                        //                           .Where(x => PeriodIds.Contains(x.FeePeriodId))
                                                        //                           .Select(x => x.ReceiptId).ToArray();

                                                        //FeeReceiptsDetail = _IFeeService.GetAllFeeReceiptDetails(ref count)
                                                        //                            .Where(x => AnnuallyFeeRecipts.Contains((int)x.ReceiptId)
                                                        //                                && x.GroupFeeHeadId != null)
                                                        //                            .ToList();
                                                    }
                                                    else
                                                    {
                                                        //if receipt type is selected all
                                                        //get receipts which are for tax certificate only
                                                        //var ReceiptTypeIds = _IFeeService.GetAllReceiptTypeFeeHeads(ref count, IsActive: true)
                                                        //                            .Where(x => FeeHeadIds.Contains((int)x.FeeHeadId))
                                                        //                            .Select(x => x.ReceiptTypeId).Distinct().ToArray();

                                                        //var AnnuallyFeeRecipts = _IFeeService.GetAllFeeReceipts(ref count, StudentId: (int)Student_id,
                                                        //                            SessionId: Session_Id).Where(x => ReceiptTypeIds.Contains(x.ReceiptTypeId)
                                                        //                            && PeriodIds.Contains(x.FeePeriodId))
                                                        //                            .Select(x => x.ReceiptId).ToArray();

                                                        //FeeReceiptsDetail = _IFeeService.GetAllFeeReceiptDetails(ref count)
                                                        //                            .Where(x => AnnuallyFeeRecipts.Contains((int)x.ReceiptId)
                                                        //                                && x.GroupFeeHeadId != null).ToList();


                                                        var ReceiptTypeIds = _IFeeService.GetAllReceiptTypeFeeHeads(ref count, IsActive: true)
                                                                                       .Where(x => FeeHeadIds.Contains((int)x.FeeHeadId))
                                                                                       .Select(x => x.ReceiptTypeId).Distinct().ToArray();
                                                        string ReceiptTyIds = "";
                                                        if (ReceiptTypeIds != null)
                                                        {
                                                            foreach (var ids in ReceiptTypeIds)
                                                            {
                                                                ReceiptTyIds += ids + ",";
                                                            }
                                                            ReceiptTyIds = ReceiptTyIds.Trim(',');

                                                            //get receipts which are for tax certificate only
                                                            FeeDetail = _IFeeService.PrintFeeReceiptCertificateBySp(ref count, ReceiptTypeId: ReceiptTyIds,
                                                                                                             SessionId: Session_Id, StudentId: (int)Student_id);

                                                        }
                                                    }
                                                }
                                            }
                                            #region oldCode
                                            //if (FeeReceiptsDetail.Count() > 0)
                                            //{
                                            //    decimal totalCharges = 0;
                                            //    var html = "<div id='GrpFeeHeadCal' style='text-align:center;'>";
                                            //    html += "<table style='width:100%;border-style:double;'><thead>";
                                            //    var heads = FeeReceiptsDetail.Select(x => x.GroupFeeHead.FeeHead).Distinct().ToList();
                                            //    var Colspan = 0;
                                            //    if (DetailType.ToLower().Contains("without"))
                                            //    {
                                            #region WithoutDetails
                                            //        html += "<tr>";
                                            //        foreach (var feeHead in heads)
                                            //        {
                                            //            Colspan++;
                                            //            html += "<th style='font-weight:bolder'>" + feeHead.FeeHead1 + "</th>";
                                            //        }

                                            //        html += "</tr></thead><tbody style='text-align: center;'><tr>";
                                            //        //for body of heads
                                            //        foreach (var Head in heads)
                                            //        {
                                            //            decimal HeadTotals = 0;
                                            //            foreach (var feeHead in FeeReceiptsDetail)
                                            //            {
                                            //                var GroupFeeHead = _IFeeService.GetAllGroupFeeHeads(ref count)
                                            //                                    .Where(x => x.GroupFeeHeadId == feeHead.GroupFeeHeadId
                                            //                                        && FeeHeadIds.Contains(x.FeeHeadId)).FirstOrDefault();
                                            //                if (Head.FeeHeadId == GroupFeeHead.FeeHeadId)
                                            //                {
                                            //                    // get fee receipt detail
                                            //                    if (feeHead.GroupFeeHeadId != null && feeHead.ConsessionId != null)
                                            //                    {

                                            //                    }
                                            //                    else
                                            //                    {
                                            //                        ///HeadTotals = 0;
                                            //                        //var totalfeeamt = FeeReceiptsDetail.Where(f => ((f.GroupFeeHeadId != null && f.FeeHeadId == null && f.ConsessionId == null))
                                            //                        //                               || f.PendingFeePeriodId != null || f.LateFeePeriodId != null
                                            //                        //                               || f.AddOnHeadId != null || (f.FeeHeadId != null && f.GroupFeeHeadId == null)
                                            //                        //                               || (f.FeeHeadId != null && f.GroupFeeHeadId != null))
                                            //                        //                               .Where(x => x.ReceiptId == (int)feeHead.ReceiptId
                                            //                        //                                       && x.GroupFeeHeadId == feeHead.GroupFeeHeadId)
                                            //                        //                                   .Select(f => f.PaidAmount).Sum();

                                            //                        //var totalfeeconsession = FeeReceiptsDetail.Where(f => f.ConsessionId != null
                                            //                        //                                && f.GroupFeeHeadId == null && f.ReceiptId == feeHead.ReceiptId && f.GroupFeeHeadId == feeHead.GroupFeeHeadId).Select(f => f.PaidAmount).Sum();
                                            //                        //var totalEWSDiscount = FeeReceiptsDetail.Where(f => f.GroupFeeHeadId != null
                                            //                        //                                && f.ConsessionId != null && f.ReceiptId == feeHead.ReceiptId && f.GroupFeeHeadId == feeHead.GroupFeeHeadId).Select(f => f.PaidAmount).Sum();
                                            //                        //var totalwaveoff = FeeReceiptsDetail.Where(f => f.WaivedOffAmount != null && f.ReceiptId == feeHead.ReceiptId && f.GroupFeeHeadId == feeHead.GroupFeeHeadId)
                                            //                        //                                .Select(f => f.WaivedOffAmount).Sum();


                                            //                        var totalfeeamt = FeeReceiptsDetail.Where(f => ((f.GroupFeeHeadId != null 
                                            //                                                            && f.FeeHeadId == null
                                            //                                                            && f.ConsessionId == null)) || f.PendingFeePeriodId != null
                                            //                                                            || f.LateFeePeriodId != null || f.AddOnHeadId != null
                                            //                                                            || (f.FeeHeadId != null && f.GroupFeeHeadId == null)
                                            //                                                            || (f.FeeHeadId != null && f.GroupFeeHeadId != null))
                                            //                                                    .Select(f => f.Amount).Sum();

                                            //                        var totalfeeconsession = FeeReceiptsDetail.Where(f => f.ConsessionId != null
                                            //                                                 && f.GroupFeeHeadId == null).Select(f => f.Amount).Sum();
                                            //                        var totalEWSDiscount = FeeReceiptsDetail.Where(f => f.GroupFeeHeadId != null 
                                            //                                                 && f.ConsessionId != null).Select(f => f.Amount).Sum();
                                            //                        var totalwaveoff = FeeReceiptsDetail.Where(f => f.WaivedOffAmount != null)
                                            //                                                 .Select(f => f.WaivedOffAmount).Sum();


                                            //                        if (totalEWSDiscount > 0)
                                            //                            HeadTotals += Convert.ToDecimal(totalfeeamt - totalEWSDiscount);
                                            //                        else if (totalfeeconsession > 0)
                                            //                            HeadTotals += Convert.ToDecimal(totalfeeamt);
                                            //                        else if (totalwaveoff > 0)
                                            //                            HeadTotals += Convert.ToDecimal(totalfeeamt - totalwaveoff);
                                            //                        else
                                            //                            HeadTotals += Convert.ToDecimal(totalfeeamt);
                                            //                    }
                                            //                }
                                            //            }
                                            //            html += "<td>" + HeadTotals + "</td>";
                                            //            totalCharges += HeadTotals;

                                            //        }
                                            //        html += "</tr>";
                                            #endregion
                                            //    }
                                            //    else
                                            //    {
                                            #region WithDetails
                                            //        Colspan = 0;
                                            //        if (_IFeeService.GetAllFeeReceipts(ref count, StudentId: Student_id).ToList().Count > 0)
                                            //        {
                                            //            var StudentReceipts = new List<FeeReceipt>();
                                            //            if (ReceiptType_Id > 0)
                                            //            {
                                            //                StudentReceipts = _IFeeService.GetAllFeeReceipts(ref count, StudentId: Student_id,
                                            //                    SessionId: Session_Id, ReceiptTypeId: ReceiptType_Id).ToList();
                                            //            }
                                            //            else
                                            //                StudentReceipts = _IFeeService.GetAllFeeReceipts(ref count, StudentId: Student_id,
                                            //                                    SessionId: Session_Id).ToList();

                                            //            html += "<tr><th>Receipt No</th><th>Receipt Type</th>";
                                            //            foreach (var receipts in heads)
                                            //            {
                                            //                Colspan++;
                                            //                html += "<th>" + receipts.FeeHead1 + "</th>";
                                            //            }
                                            //            Colspan = Colspan + 3;
                                            //            html += "<th>Total</th></tr>";
                                            //            html += "</thead><tbody style='text-align: center;'>";
                                            //            var headIds = FeeReceiptsDetail.Select(x => x.GroupFeeHead).Select(x => x.FeeHeadId).Distinct().ToArray();
                                            //            var studentReceipts = StudentReceipts.Select(x => x.ReceiptId).Distinct().ToArray();
                                            //            decimal MainTotals = 0;
                                            //            var GrouepHeads = FeeReceiptsDetail.Select(x => x.GroupFeeHead).Distinct().ToList();
                                            //            foreach (var r in StudentReceipts)
                                            //            {
                                            //                html += "<tr>";
                                            //                html += "<td>" + r.ReceiptNo + "</td>";
                                            //                var receipttype = _IFeeService.GetReceiptTypeById((int)r.ReceiptTypeId).ReceiptType1;
                                            //                html += "<td>" + receipttype.ToString() + "</td>";
                                            //                var getSameFeeHeadDetail = FeeReceiptsDetail.Where(x => x.ReceiptId == r.ReceiptId).ToList();
                                            //                decimal HeadTotals = 0;
                                            //                decimal ReceiptChrgeTotals = 0;

                                            //                foreach (var head in GrouepHeads)
                                            //                {
                                            //                    if (getSameFeeHeadDetail.Where(x => x.GroupFeeHeadId == head.GroupFeeHeadId).ToList().Count > 0)
                                            //                    {
                                            //                        foreach (var receipt in getSameFeeHeadDetail.Where(x => x.GroupFeeHeadId == head.GroupFeeHeadId).ToList())
                                            //                        {
                                            //                            if (head.FeeHeadId == receipt.GroupFeeHead.FeeHeadId)
                                            //                            {
                                            //                                HeadTotals = 0;
                                            //                                // get fee receipt detail
                                            //                                if (receipt.GroupFeeHeadId != null && receipt.ConsessionId != null)
                                            //                                {

                                            //                                }
                                            //                                else
                                            //                                {
                                            //                                    HeadTotals = 0;
                                            //                                    //var totalfeeamt = FeeReceiptsDetail.Where(f => ((f.GroupFeeHeadId != null && f.FeeHeadId == null && f.ConsessionId == null))
                                            //                                    //                        || f.PendingFeePeriodId != null || f.LateFeePeriodId != null
                                            //                                    //                        || f.AddOnHeadId != null || (f.FeeHeadId != null && f.GroupFeeHeadId == null)
                                            //                                    //                        || (f.FeeHeadId != null && f.GroupFeeHeadId != null))
                                            //                                    //                        .Where(x => x.ReceiptId == (int)receipt.ReceiptId
                                            //                                    //                                && x.GroupFeeHeadId == receipt.GroupFeeHeadId)
                                            //                                    //                            .Select(f => f.PaidAmount).Sum();

                                            //                                    //var totalfeeconsession = FeeReceiptsDetail.Where(f => f.ConsessionId != null
                                            //                                    //                                && f.GroupFeeHeadId == null && f.ReceiptId == receipt.ReceiptId && f.GroupFeeHeadId == receipt.GroupFeeHeadId).Select(f => f.PaidAmount).Sum();
                                            //                                    //var totalEWSDiscount = FeeReceiptsDetail.Where(f => f.GroupFeeHeadId != null
                                            //                                    //                                && f.ConsessionId != null && f.ReceiptId == receipt.ReceiptId && f.GroupFeeHeadId == receipt.GroupFeeHeadId).Select(f => f.PaidAmount).Sum();
                                            //                                    //var totalwaveoff = FeeReceiptsDetail.Where(f => f.WaivedOffAmount != null && f.ReceiptId == receipt.ReceiptId && f.GroupFeeHeadId == receipt.GroupFeeHeadId)
                                            //                                    //                                .Select(f => f.WaivedOffAmount).Sum();

                                            //                                    var totalfeeamt = FeeReceiptsDetail.Where(f => ((f.GroupFeeHeadId != null
                                            //                                                               && f.FeeHeadId == null
                                            //                                                               && f.ConsessionId == null)) || f.PendingFeePeriodId != null
                                            //                                                               || f.LateFeePeriodId != null || f.AddOnHeadId != null
                                            //                                                               || (f.FeeHeadId != null && f.GroupFeeHeadId == null)
                                            //                                                               || (f.FeeHeadId != null && f.GroupFeeHeadId != null))
                                            //                                                                 .Select(f => f.Amount).Sum();

                                            //                                    var totalfeeconsession = FeeReceiptsDetail.Where(f => f.ConsessionId != null
                                            //                                                             && f.GroupFeeHeadId == null).Select(f => f.Amount).Sum();
                                            //                                    var totalEWSDiscount = FeeReceiptsDetail.Where(f => f.GroupFeeHeadId != null
                                            //                                                             && f.ConsessionId != null).Select(f => f.Amount).Sum();
                                            //                                    var totalwaveoff = FeeReceiptsDetail.Where(f => f.WaivedOffAmount != null)
                                            //                                                             .Select(f => f.WaivedOffAmount).Sum();

                                            //                                    if (totalEWSDiscount > 0)
                                            //                                        HeadTotals += Convert.ToDecimal(totalfeeamt - totalEWSDiscount);
                                            //                                    else if (totalfeeconsession > 0)
                                            //                                        HeadTotals += Convert.ToDecimal(totalfeeamt);
                                            //                                    else if (totalwaveoff > 0)
                                            //                                        HeadTotals += Convert.ToDecimal(totalfeeamt - totalwaveoff);
                                            //                                    else
                                            //                                        HeadTotals += Convert.ToDecimal(totalfeeamt);

                                            //                                    if (receipt.GroupFeeHead.GroupFeeHeadId != null)
                                            //                                    {
                                            //                                        html += "<td>" + HeadTotals + "</td>";
                                            //                                        ReceiptChrgeTotals += HeadTotals;
                                            //                                    }
                                            //                                }
                                            //                            }
                                            //                            else
                                            //                                html += "<td></td>";
                                            //                        }
                                            //                    }
                                            //                    else
                                            //                        html += "<td></td>";
                                            //                }
                                            //                html += "<td>" + ReceiptChrgeTotals + "</td>";
                                            //                totalCharges += ReceiptChrgeTotals;
                                            //                html += "</tr>";
                                            //            }
                                            //        }
                                            #endregion
                                            //    }
                                            //    html += "<tr><td colspan='" + Colspan + "'><hr/></td></tr>";
                                            //    html += "<tr><td style='font-weight: bold;text-align: right;' colspan='" + (Colspan - 1)
                                            //                + "'> Total Charges = " + totalCharges + "</td></tr>";
                                            //    html += "</tbody></table></div>";
                                            //    XElement x1 = XElement.Parse(html.Replace("&", "&amp;"));
                                            //    div.Descendants("table").FirstOrDefault().AddAfterSelf(x1);
                                            //    fulldoc = div.ToString();
                                            //}
                                            //else
                                            //    return Content("No Fee Charges Data Found.");
                                            #endregion

                                            #region new code
                                            if (FeeDetail.Rows.Count > 0)
                                            {
                                                var html = "<div id='GrpFeeHeadCal' style='text-align:center;'>";
                                                html += "<table style='width:100%;border-style:double;'><thead><tr>";
                                                var Colspan = 0;
                                                decimal totalCharges = 0;
                                                if (DetailType.ToLower().Contains("without"))
                                                {
                                                    #region Without Detail
                                                    for (int i = 2; i <= (FeeDetail.Columns.Count - 1); i++)
                                                    {
                                                        Colspan++;
                                                        html += "<th>" + FeeDetail.Columns[i].ToString() + "</th>";
                                                    }
                                                    html += "</tr></thead><tbody style='text-align: center;'>";

                                                    for (int i = 0; i <= FeeDetail.Rows.Count - 1; i++)
                                                    {
                                                        html += "<tr>";
                                                        for (int j = 2; j <= (FeeDetail.Columns.Count - 1); j++)
                                                        {
                                                            html += "<td>" + FeeDetail.Rows[i][j].ToString() + "</td>";
                                                            totalCharges += Convert.ToDecimal(FeeDetail.Rows[i][j]);
                                                        }
                                                        html += "</tr>";
                                                    }
                                                    html += "<tr><td colspan='" + Colspan + "'><hr/></td></tr>";
                                                    html += "<tr><td style='font-weight: bold;text-align: right;' colspan='" + (Colspan - 1)
                                                                + "'> Total Charges = " + totalCharges + "</td></tr>";
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region With Detail
                                                    Colspan = 0;
                                                    totalCharges = 0;
                                                    decimal perHeadtotalCharges = 0;
                                                    foreach (DataColumn dc in FeeDetail.Columns)
                                                    {
                                                        Colspan++;
                                                        html += "<th>" + dc.ToString() + "</th>";
                                                    }
                                                    Colspan = Colspan + 1;
                                                    html += "<th>Total</th>";
                                                    html += "</tr></thead><tbody style='text-align: center;'>";
                                                    for (int i = 0; i <= FeeDetail.Rows.Count - 1; i++)
                                                    {
                                                        html += "<tr>";
                                                        perHeadtotalCharges = 0;
                                                        for (int j = 0; j <= (FeeDetail.Columns.Count - 1); j++)
                                                        {
                                                            html += "<td>" + FeeDetail.Rows[i][j].ToString() + "</td>";
                                                        }
                                                        for (int j = 2; j <= (FeeDetail.Columns.Count - 1); j++)
                                                        {
                                                            perHeadtotalCharges += Convert.ToDecimal(FeeDetail.Rows[i][j]);
                                                        }

                                                        html += "<td>" + perHeadtotalCharges.ToString() + "</td>";
                                                        totalCharges += perHeadtotalCharges;
                                                        html += "</tr>";
                                                    }

                                                    html += "<tr><td colspan='" + Colspan + "'><hr/></td></tr>";
                                                    html += "<tr><td style='font-weight: bold;text-align: right;' colspan='" + (Colspan)
                                                                + "'> Total Charges = " + totalCharges + "</td></tr>";
                                                    #endregion
                                                }


                                                html += "</tbody></table></div>";
                                                XElement x1 = XElement.Parse(html.Replace("&", "&amp;"));
                                                div.Descendants("table").FirstOrDefault().AddAfterSelf(x1);
                                                fulldoc = div.ToString();
                                            }
                                            else
                                                return Content("No Fee Charges Data Found.");

                                            #endregion
                                        }
                                    }
                                    #endregion


                                    #region Hide/show header
                                    if (hasHeader != "true")
                                    {
                                        //remove header from print and added empty space in it
                                        //var rows = div.Elements("table");
                                        //if (issuedoc.IssueDocType.IssueDocType1.ToLower().Contains("transfer"))
                                        var rows = div.Descendants("tr");
                                        foreach (var row in rows)
                                        {
                                            if (row.Attribute("class").Value == "FirstTableRow")
                                            {
                                                row.Remove();
                                            }
                                            //else
                                            //{
                                            //    if (row.Attribute("class").Value == "FirstRowheader")
                                            //    {
                                            //        row.SetAttributeValue("height", "100px");
                                            //        foreach (var col in row.Elements("td"))
                                            //        {
                                            //            col.SetValue(" ");
                                            //        }
                                            //    }
                                            //}
                                            fulldoc = div.ToString();
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        var schoolinfo = _ISchoolDataService.GetSchoolDataDetail(ref count).FirstOrDefault();
                                        var logo = (schoolinfo != null && schoolinfo.Logo != null) ? schoolinfo.Logo : "";
                                        // replace parameters
                                        // logo
                                        var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                                        string path = _WebHelper.GetStoreLocation() + "/Images/" + SchoolDbId;
                                        fulldoc = fulldoc.Replace("%Logo%", path + "/" + logo);
                                        // School Name
                                        string SchoolName = (schoolinfo != null && schoolinfo.SchoolName != null) ? schoolinfo.SchoolName : "";
                                        string SchoolAddress = (schoolinfo != null && schoolinfo.SchoolAddress != null) ? schoolinfo.SchoolAddress : "";
                                        string SchoolEmail = (schoolinfo != null && schoolinfo.SchoolEmail != null) ? schoolinfo.SchoolEmail : "";
                                        string SchoolWebsite = (schoolinfo != null && schoolinfo.DisplayWebsite != null) ? schoolinfo.DisplayWebsite : "";
                                        fulldoc = fulldoc.Replace("%SchoolName%", SchoolName);
                                        fulldoc = fulldoc.Replace("%SchoolAddress%", SchoolAddress);
                                        fulldoc = fulldoc.Replace("%SchoolEmail%", SchoolEmail);
                                        fulldoc = fulldoc.Replace("%SchoolWebsite%", SchoolWebsite);
                                    }
                                    #endregion


                                    fulldoc = fulldoc.Replace("%DocumentText%", issuedoctemplatedetail.TemplateText);

                                    // Document Name
                                    string DocumentName = issuedoctemplatedetail.IssueDocTemplate.TemplateTitle;
                                    fulldoc = fulldoc.Replace("%DocumentName%", DocumentName);

                                    //subtitle
                                    if (!string.IsNullOrEmpty(issuedoctemplatedetail.IssueDocTemplate.TemplateSubTitle))
                                        fulldoc = fulldoc.Replace("%DocumentSubTitle%", "<br/>" + issuedoctemplatedetail.IssueDocTemplate.TemplateSubTitle == null ? "" : issuedoctemplatedetail.IssueDocTemplate.TemplateSubTitle);
                                    else
                                        fulldoc = fulldoc.Replace("%DocumentSubTitle%", "");

                                    if (!(bool)issuedoc.IsPrinted)
                                        fulldoc = fulldoc.Replace("%DocumentType%", issuedoctemplate.IssueDocType.IssueDocType1);
                                    else
                                        fulldoc = fulldoc.Replace("%DocumentType%", "Duplicate " + issuedoctemplate.IssueDocType.IssueDocType1);


                                    // Ref no
                                    string RefNo = issuedoc.DocNo.ToString();
                                    fulldoc = fulldoc.Replace("%RefNo%", RefNo);
                                    string BookNo = issuedoc.DocNo.ToString();
                                    fulldoc = fulldoc.Replace("%BookNo%", BookNo);
                                    string AdmissionNo = issuedoc.Student.AdmnNo;
                                    fulldoc = fulldoc.Replace("%AdmissionNo%", AdmissionNo);

                                    // Current Date
                                    DateTime dtIssue = Convert.ToDateTime(issuedoc.IssueDate);
                                    string DateIssue = ConvertDate(dtIssue);
                                    fulldoc = fulldoc.Replace("%Date%", DateIssue);

                                    // get issue doc detail 
                                    var issuedocdetail = _ICertificateService.GetAllIssueDocDetail(ref count, issuedoc.IssueDocId);
                                    var issuedoccolids = issuedocdetail.Select(i => i.IssueDocTypeColId).ToArray();
                                    // get issuedoc cols
                                    var issdoccols = _ICertificateService.GetAllIssueDocTypeCol(ref count);
                                    issdoccols = issdoccols.Where(i => issuedoccolids.Contains(i.IssueDocTypeColsId)).ToList();
                                    // loop on 
                                    var IsSubjectAssigned = false;
                                    foreach (var item in issuedocdetail)
                                    {
                                        var value = "";
                                        var genderOf = "";

                                        // get issue doc col
                                        var doccol = issdoccols.Where(i => i.IssueDocTypeColsId == item.IssueDocTypeColId).FirstOrDefault();
                                        if (doccol.ColName.ToLower() == "receipttype")
                                        {
                                            if (ReceiptType_Id > 0)
                                            {
                                                var getReceiptType = _IFeeService.GetReceiptTypeById(ReceiptType_Id);
                                                value = getReceiptType.ReceiptType1;
                                            }
                                            else
                                                value = "All";
                                        }
                                        else
                                        {
                                            if (doccol.ColName.ToLower() == "dob" || doccol.ColName.ToLower() == "admndate")
                                                value = ConvertDate(Convert.ToDateTime(item.ColValue));
                                            else if (doccol.ColName.ToLower() == "subjects")
                                            {
                                                if (Class_id > 0 && Student_id > 0)
                                                {
                                                    var StudentClass = _IStudentService.GetAllStudentClasss(ref count).Where(f => f.StudentId == Student_id && f.ClassId == Class_id).FirstOrDefault();
                                                    if (StudentClass != null)
                                                    {
                                                        var studentSUbjects = _ISubjectService.GetAllStudentClassSubjects(ref count).Where(f => f.StudentClassId == StudentClass.StudentClassId).Select(f => f.ClassSubjectId).ToArray();
                                                        if (studentSUbjects != null && studentSUbjects.Count() > 0)
                                                        {
                                                            var ClassSubjects = _ISubjectService.GetAllClassSubjects(ref count).Where(f => studentSUbjects.Contains(f.ClassSubjectId) && f.IsActive == true).ToList();
                                                            foreach (var subject in ClassSubjects)
                                                            {
                                                                IsSubjectAssigned = true;
                                                                if (subject.SkillId != null && subject.SkillId > 0)
                                                                {
                                                                    var skillSubject = _ISubjectService.GetAllSubjectSkills(ref count).Where(f => f.SubjectId == subject.SubjectId && f.SkillId == subject.SkillId).FirstOrDefault();
                                                                    if (value != "" && value != null)
                                                                    {
                                                                        value += "," + skillSubject.SubjectSkill1;
                                                                    }
                                                                    else
                                                                    {
                                                                        value = skillSubject.SubjectSkill1;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (value != "" && value != null)
                                                                    {
                                                                        value += "," + subject.Subject.Subject1;
                                                                    }
                                                                    else
                                                                    {
                                                                        value = subject.Subject.Subject1;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                                value = item.ColValue;
                                        }

                                        if (doccol.ColName.Contains("GuardianName"))
                                        {
                                            string guaridanName = "";
                                            var getStudentGender = new Gender();

                                            var getStudentInfo = _IStudentService.GetStudentById((int)Student_id);
                                            if (getStudentInfo != null)
                                            {
                                                getStudentGender = _ICatalogMasterService.GetGenderById((int)getStudentInfo.GenderId);
                                            }

                                            switch (getStudentGender.Gender1.ToLower())
                                            {
                                                case "female":
                                                    genderOf = "D/o";
                                                    break;
                                                case "male":
                                                    genderOf = "S/o";
                                                    break;
                                                case "transgender":
                                                    genderOf = "S/o";
                                                    break;
                                            }

                                            var relations = _IGuardianService.GetAllRelations(ref count).Select(x => x.RelationId).ToArray();
                                            var guardiandetail = _IGuardianService.GetAllGuardians(ref count, StudentId: Student_id).Where(x => relations.Contains((int)x.GuardianTypeId)).ToList();
                                            if (guardiandetail.Count() > 0)
                                            {
                                                guaridanName = genderOf + " ";
                                                foreach (var i in guardiandetail)
                                                {
                                                    if (i.Relation.Relation1.ToLower() != "father" ||
                                                        i.Relation.Relation1.ToLower() != "mother")
                                                    {
                                                        if (i.Relation.Relation1.ToLower() == "father")
                                                        {
                                                            guaridanName += "Sh. " + (i.FirstName + " " + i.LastName).Trim();
                                                        }
                                                        if (i.Relation.Relation1.ToLower() == "mother")
                                                        {
                                                            guaridanName += " & " + "Smt. " + (i.FirstName + " " + i.LastName).Trim();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (i.Relation.Relation1.ToLower() == "guardian")
                                                        {
                                                            guaridanName += "Guardian: " + (i.FirstName + " " + i.LastName).Trim();
                                                        }
                                                    }
                                                }
                                            }
                                            value = guaridanName;
                                        }

                                        if (doccol.ColName.Contains("Subjects"))
                                        {
                                            var a = value;
                                        }

                                        fulldoc = fulldoc.Replace("%" + doccol.ColName + "%", value);
                                    }
                                    // update Issue doc
                                    var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                                    // get table record from table master                 
                                    var IssueDoctable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "IssueDoc").FirstOrDefault();


                                    if (ReceiptType_Id > 0)
                                    {
                                        var getReceiptType = _IFeeService.GetReceiptTypeById(ReceiptType_Id);
                                        fulldoc = fulldoc.Replace("%ReceiptType%", getReceiptType.ReceiptType1);
                                    }

                                    if (issuedoc.IsPrinted == null || issuedoc.IsPrinted == false)
                                    {
                                        issuedoc.IsPrinted = true;
                                        _ICertificateService.UpdateIssueDoc(issuedoc);
                                        if (IssueDoctable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, IssueDoctable.TableId,
                                                                                                        DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(issuedoc.IssueDocId, IssueDoctable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update IssueDoc with IssueDocId:{0},IssueDocType:{1}", issuedoc.IssueDocId, issuedoc.IssueDocType), Request.Browser.Browser);
                                        }
                                    }

                                }
                            }
                        }
                    }
                }

                if (fulldoc != "")
                    return Content(fulldoc);
                else
                    return Content("No Document Find");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Content(ex.Message);
            }
        }
        #endregion

        #endregion
    }
}