﻿using BAL.BAL.Common;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.Kendo;
using DAL.DAL.MenuModel;
using DAL.DAL.SettingService;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.App;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ViewModel.ViewModel.Security;
using System.Web.Configuration;


namespace KS_SmartChild.Controllers
{
    public class AppController : Controller
    {
        #region fields

        public int count = 0;
        private readonly IDropDownService _IDropDownService;
        private readonly ILogService _Logger;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly IUserService _IUserService;
        private readonly IUserManagementService _IUserManagementService;
        private readonly IWebHelper _WebHelper;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly INavigationService _INavigationService;
        private readonly IModuleVersionService _IModuleVersionService;
        private readonly ICustomEncryption _ICustomEncryption;
        #endregion

        #region ctor

        public AppController(IDropDownService IDropDownService,
            ILogService Logger,
            ICatalogMasterService ICatalogMasterService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            IUserService IUserService,
            IUserManagementService IUserManagementService,
            IConnectionService IConnectionService,
            IWebHelper WebHelper,
            ISchoolSettingService ISchoolSettingService,
            INavigationService INavigationService,
            IModuleVersionService IModuleVersionService,
            ICustomEncryption ICustomEncryption)
        {
            this._IDropDownService = IDropDownService;
            this._Logger = Logger;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._IActivityLogService = IActivityLogService;
            this._IUserService = IUserService;
            this._IUserManagementService = IUserManagementService;
            this._WebHelper = WebHelper;
            this._ISchoolSettingService = ISchoolSettingService;
            this._INavigationService = INavigationService;
            this._IModuleVersionService = IModuleVersionService;
            this._ICustomEncryption = ICustomEncryption;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }

        public string ConvertTime(TimeSpan time)
        {
            TimeSpan timespan = new TimeSpan(time.Hours, time.Minutes, time.Seconds);
            DateTime datetime = DateTime.Today.Add(timespan);
            string sdisplayTime = datetime.ToString("hh:mm tt");
            return sdisplayTime;
        }

        #endregion

        #region Methods

        #region App Form with App form Action detail

        // Add App Form
        public AppFormModel prepareappformmodel(AppFormModel model, SchoolUser user)
        {
            // get user role id
            // get user role 
            int userroleId = 0;
            var userrolecontacttype = user.ContactType.UserRoleContactTypes.ToList();
            var userrole = userrolecontacttype.FirstOrDefault();
            if (userrole != null)
                userroleId = userrole.UserRole.UserRoleId;

            // get next display order
            if (model.AppFormDisplayOrder == null)
            {
                var allappform = _IUserManagementService.GetAllAppForms(ref count);
                var sortingindex = allappform.OrderByDescending(a => a.SortingIndex).Select(a => a.SortingIndex).FirstOrDefault();
                if (sortingindex != null)
                    model.AppFormDisplayOrder = sortingindex.Value + 1;
                else
                    model.AppFormDisplayOrder = 1;
            }

            // get module list
            model.AvailableModule = _IDropDownService.ModuleList();

            // get all navigation items and ids array
            var navitems = _INavigationService.GetAllUserRoleNavigationItems(ref count, UserRoleId: userroleId).ToList();
            var dynamicnavigationpath = "";
            var Is_Lite = _ISchoolSettingService.GetorSetSchoolData("Is_Lite", "0").AttributeValue == "1" ? true : false;
            if (Is_Lite)
                dynamicnavigationpath = "litenavigationpath";
            else
                dynamicnavigationpath = "navigationpath";

            var CustomNavigation = _ISchoolSettingService.GetorSetSchoolData("CustomNavigation", "0").AttributeValue == "1" ? true : false;
            string file = "";
            var SchoolDbId = Convert.ToString(System.Web.HttpContext.Current.Session["SchoolId"]);
            //get the Json filepath  
            if (CustomNavigation)
            {
                file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + "/" + SchoolDbId + "/" + Convert.ToString(WebConfigurationManager.AppSettings["leftjson"]));
            }
            else
            {
                file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(WebConfigurationManager.AppSettings["leftjson"]));
            }
            //string file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(WebConfigurationManager.AppSettings["leftjson"]));
            //deserialize JSON from file  
            string Json = System.IO.File.ReadAllText(file).Replace("#", "");
            System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();
            var list = ser.Deserialize<List<UserNavigationPermissionModel>>(Json);
            var navlists = new List<UserParentNavigationPermissionModel>();

            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    foreach (var itm in item.navigation)
                    {
                        if (itm.Child.Count > 0)
                        {
                            foreach (var m in itm.Child)
                            {
                                if (m.Child.Count > 0)
                                {
                                    foreach (var n in m.Child)
                                        navlists.Add(new UserParentNavigationPermissionModel { NavigationItemId = n.NavigationItemId, ControllerName = n.ControllerName, ActionName = n.ActionName, IsActive = n.IsActive, NavigationItem = n.NavigationItem });
                                }
                                else
                                    navlists.Add(new UserParentNavigationPermissionModel { NavigationItemId = m.NavigationItemId, ControllerName = m.ControllerName, ActionName = m.ActionName, IsActive = m.IsActive, NavigationItem = m.NavigationItem });
                            }
                        }
                        else
                            navlists.Add(new UserParentNavigationPermissionModel { NavigationItemId = itm.NavigationItemId, ControllerName = itm.ControllerName, ActionName = itm.ActionName, IsActive = itm.IsActive, NavigationItem = itm.NavigationItem });
                    }
                }
            }

            // navigation item drop down
            model.AvailableNavigationItem.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            foreach (var nav in navlists)
                model.AvailableNavigationItem.Add(new SelectListItem { Selected = false, Text = nav.NavigationItem, Value = nav.NavigationItemId.ToString() });

            // app form action type
            model.AvailableFormActionType = _IDropDownService.AppFormActionTypeList();

            // grid page sizes and default grid page size
            model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
            model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");

            return model;
        }

        public ActionResult AppForm()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new AppFormModel();
                model = prepareappformmodel(model, user);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public AppFormModel checkappformmodelerror(AppFormModel model)
        {
            if (string.IsNullOrEmpty(model.AppForm))
                ModelState.AddModelError("AppForm", "App form required");
            else
            {
                // check unique
                var appformexist = _IUserManagementService.GetAllAppForms(ref count);
                appformexist = appformexist.Where(a => a.AppForm1.ToLower() == model.AppForm.ToLower() && a.AppFormId != model.AppFormId).ToList();
                if (appformexist.Count > 0)
                    ModelState.AddModelError("AppForm", "App form already exist");
            }

            if (model.Module == null)
                ModelState.AddModelError("Module", "Module required");
            if (model.AppFormDisplayOrder == null)
                ModelState.AddModelError("AppFormDisplayOrder", "Display Order required");
            else
            {
                // check unique
                var appformexist = _IUserManagementService.GetAllAppForms(ref count, SortingIndex: model.AppFormDisplayOrder);
                appformexist = appformexist.Where(a => a.AppFormId != model.AppFormId).ToList();
                if (appformexist.Count > 0)
                    ModelState.AddModelError("AppFormDisplayOrder", "Display Order already exist");
            }
            if (model.NavigationItem == null)
                ModelState.AddModelError("NavigationItem", "Navigation Item required");
            else
            {
                // check unique
                var appformexist = _IUserManagementService.GetAllAppForms(ref count);
                appformexist = appformexist.Where(a => a.NavigationItemId == model.NavigationItem && a.AppFormId != model.AppFormId).ToList();
                if (appformexist.Count > 0)
                    ModelState.AddModelError("NavigationItem", "Navigation Item already mapped");
            }

            return model;
        }

        [HttpPost]
        public ActionResult AppForm(AppFormModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                model = checkappformmodelerror(model);
                if (ModelState.IsValid)
                {
                    string message = "";
                    // Activity log type add , edit
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    // stdclass Id                    
                    var AppFormtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AppForm").FirstOrDefault();

                    var AppForm = new AppForm();
                    if (model.AppFormId > 0) // edit mode
                    {
                        // get app form by id
                        AppForm = _IUserManagementService.GetAppFormById((int)model.AppFormId, true);
                        if (AppForm != null)
                        {
                            AppForm.AppForm1 = model.AppForm;
                            AppForm.ModuleId = model.Module;
                            AppForm.NavigationItemId = model.NavigationItem;
                            AppForm.SortingIndex = model.AppFormDisplayOrder;
                            _IUserManagementService.UpdateAppForm(AppForm);
                            if (AppFormtable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AppFormtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(AppForm.AppFormId, AppFormtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update AppForm with AppFormId:{0} and AppForm:{1}", AppForm.AppFormId, AppForm.AppForm1), Request.Browser.Browser);
                            }

                            message = "App from updated successfully";
                        }
                    }
                    else
                    {
                        AppForm.AppForm1 = model.AppForm;
                        AppForm.ModuleId = model.Module;
                        AppForm.NavigationItemId = model.NavigationItem;
                        AppForm.SortingIndex = model.AppFormDisplayOrder;
                        _IUserManagementService.InsertAppForm(AppForm);
                        if (AppFormtable != null)
                        {
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AppFormtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(AppForm.AppFormId, AppFormtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add AppForm with AppFormId:{0} and AppForm:{1}", AppForm.AppFormId, AppForm.AppForm1), Request.Browser.Browser);
                        }

                        message = "App from added successfully";
                    }

                    TempData["SuccessNotification"] = message;
                    return RedirectToAction("EditAppForm", new { Id = _ICustomEncryption.base64e(AppForm.AppFormId.ToString()) });
                }

                if (model.AppFormId > 0)
                {
                    TempData["AppForm_ViewData"] = model;
                    return RedirectToAction("EditAppForm", new { Id = _ICustomEncryption.base64e(model.AppFormId.ToString()) });
                }
                else
                {
                    model = prepareappformmodel(model, user);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }


        [HttpPost]
        public ActionResult DeleteAppForm(string id)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // Activity log type add , edit
                var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
                // stdclass Id                    
                var AppFormtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AppForm").FirstOrDefault();
                // decrypt Id
                var qsid = _ICustomEncryption.base64d(id);
                int Appformid = 0;
                if (int.TryParse(qsid, out Appformid))
                {
                    var AppForm = new AppForm();
                    if (Appformid > 0) // edit mode
                    {
                        // get app form by id
                        AppForm = _IUserManagementService.GetAppFormById(Appformid);
                        if (AppForm != null)
                        {
                            _IUserManagementService.DeleteAppForm(Appformid);
                            if (AppFormtable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AppFormtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(AppForm.AppFormId, AppFormtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Delete AppFormA with AppFormActionId:{0} and AppForm:{1}", AppForm.AppFormId, AppForm.AppForm1), Request.Browser.Browser);
                            }
                        }
                    }

                    return Json(new
                    {
                        status = "success",
                        data = "Deleted successfully"
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        data = "Error"
                    });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }






        // Edit App Form
        public ActionResult EditAppForm(string id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // check id exist
                if (!string.IsNullOrEmpty(id))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(id);
                    int Appformid = 0;
                    if (int.TryParse(qsid, out Appformid))
                    {
                        var model = new AppFormModel();
                        var AppForm = _IUserManagementService.GetAppFormById(Appformid);
                        if (AppForm != null)
                        {
                            if (TempData["AppForm_ViewData"] != null)
                                model = (AppFormModel)TempData["AppForm_ViewData"];
                            else
                            {
                                model.AppFormId = AppForm.AppFormId;
                                model.AppForm = AppForm.AppForm1;
                                model.Module = AppForm.ModuleId;
                                model.NavigationItem = AppForm.NavigationItemId;
                                model.AppFormDisplayOrder = AppForm.SortingIndex;
                            }

                            // get app form action list
                            var appformactionlist = _IUserManagementService.GetAllAppFormActions(ref count, AppFormId: AppForm.AppFormId);
                            foreach (var item in appformactionlist)
                            {
                                AppFormActionList AppFormActionList = new AppFormActionList();
                                AppFormActionList.AppForm = AppForm.AppForm1;
                                AppFormActionList.AppFormAction = item.ActionName;
                                AppFormActionList.AppFormActionId = item.AppFormActionId;
                                AppFormActionList.AppFormActionInfo = item.ActionInfo;
                                AppFormActionList.AppFormActionType = item.AppFormActionType.FormActionType;
                                AppFormActionList.AppFormActionTypeId = (int)item.FormActionTypeId;
                                AppFormActionList.DisplayOrder = (int)item.SortingIndex;
                                AppFormActionList.IsDeleted = true;
                                //if (item.UserRoleActionPermissions.Count > 0)
                                    AppFormActionList.IsDeleted = false;

                                model.AppFormActionList.Add(AppFormActionList);
                            }

                            model = prepareappformmodel(model, user);
                            return View(model);
                        }
                        else
                        {
                            TempData["ErrorNotification"] = "App form not found";
                            return RedirectToAction("AppForm", "App");
                        }
                    }
                }

                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        // Add and Update App Form Actions
        public string checkappformactionmodelerror(AppFormModel model)
        {
            StringBuilder validations = new StringBuilder();

            if (string.IsNullOrEmpty(model.AppFormAction))
                validations.Append("Action name required ε");
            else
            {
                // check unique
                var appformactionexist = _IUserManagementService.GetAllAppFormActions(ref count, AppFormId: model.AppFormId);
                appformactionexist = appformactionexist.Where(a => a.ActionName.ToLower() == model.AppFormAction.ToLower() && a.AppFormActionId != model.AppFormActionId).ToList();
                if (appformactionexist.Count > 0)
                    validations.Append("Action name already exist ε");
            }

            if (string.IsNullOrEmpty(model.AppFormActionInfo))
                validations.Append("Action info required ε");

            if (model.FormActionType == null)
                validations.Append("Action type required ε");
            else
            {
                var appformactionexist = _IUserManagementService.GetAllAppFormActions(ref count, AppFormId: model.AppFormId, FormActionTypeId: model.FormActionType);
                appformactionexist = appformactionexist.Where(a => a.AppFormActionId != model.AppFormActionId).ToList();
                if (appformactionexist.Count > 0)
                    validations.Append("Action type already exist ε");
            }

            if (model.AppFormActionDisplayOrder == null)
                validations.Append("Display Order required ε");
            else
            {
                // check unique
                var appformactionexist = _IUserManagementService.GetAllAppFormActions(ref count, AppFormId: model.AppFormId, SortingIndex: model.AppFormActionDisplayOrder);
                appformactionexist = appformactionexist.Where(a => a.AppFormActionId != model.AppFormActionId).ToList();
                if (appformactionexist.Count > 0)
                    validations.Append("Display Order already exist ε");
            }


            return validations.ToString();
        }

        [HttpPost]
        public ActionResult AppFormAction(AppFormModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                string IsValid = checkappformactionmodelerror(model);
                if (string.IsNullOrEmpty(IsValid))
                {
                    string message = "";
                    // Activity log type add , edit
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    // stdclass Id                    
                    var AppFormActiontable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AppFormAction").FirstOrDefault();

                    var AppFormAction = new AppFormAction();
                    if (model.AppFormActionId > 0) // edit mode
                    {
                        // get app form by id
                        AppFormAction = _IUserManagementService.GetAppFormActionById((int)model.AppFormActionId, true);
                        if (AppFormAction != null)
                        {
                            AppFormAction.ActionName = model.AppFormAction;
                            AppFormAction.ActionInfo = model.AppFormActionInfo;
                            AppFormAction.SortingIndex = model.AppFormActionDisplayOrder;
                            AppFormAction.FormActionTypeId = model.FormActionType;
                            _IUserManagementService.UpdateAppFormAction(AppFormAction);
                            if (AppFormActiontable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AppFormActiontable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(AppFormAction.AppFormActionId, AppFormActiontable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update AppFormAction with AppFormActionId:{0} and ActionName:{1}", AppFormAction.AppFormActionId, AppFormAction.ActionName), Request.Browser.Browser);
                            }

                            message = "App from action updated successfully";
                        }
                    }
                    else
                    {
                        AppFormAction.AppFormId = model.AppFormId;
                        AppFormAction.ActionName = model.AppFormAction;
                        AppFormAction.ActionInfo = model.AppFormActionInfo;
                        AppFormAction.SortingIndex = model.AppFormActionDisplayOrder;
                        AppFormAction.FormActionTypeId = model.FormActionType;
                        _IUserManagementService.InsertAppFormAction(AppFormAction);
                        if (AppFormActiontable != null)
                        {
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AppFormActiontable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(AppFormAction.AppFormActionId, AppFormActiontable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Add AppFormAction with AppFormActionId:{0} and ActionName:{1}", AppFormAction.AppFormActionId, AppFormAction.ActionName), Request.Browser.Browser);
                        }

                        message = "App from action added successfully";
                    }

                    return Json(new
                    {
                        status = "success",
                        data = message
                    });
                }

                return Json(new
                {
                    status = "failed",
                    data = IsValid
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        // delete app from action
        [HttpPost]
        public ActionResult DeleteAppFormAction(int id)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // Activity log type add , edit
                var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
                // stdclass Id                    
                var AppFormActiontable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AppFormAction").FirstOrDefault();

                var AppFormAction = new AppFormAction();
                if (id > 0) // edit mode
                {
                    // get app form by id
                    AppFormAction = _IUserManagementService.GetAppFormActionById(id);
                    if (AppFormAction != null)
                    {
                        //if (AppFormAction.UserRoleActionPermissions.Count > 0)
                        //{
                        //    return Json(new
                        //    {
                        //        status = "success",
                        //        data = "You can't delete this App from action"
                        //    });
                        //}

                        _IUserManagementService.DeleteAppFormAction(AppFormAction.AppFormActionId);
                        if (AppFormActiontable != null)
                        {
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AppFormActiontable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(AppFormAction.AppFormActionId, AppFormActiontable.TableId, logtype.ActivityLogTypeId, user, String.Format("Delete AppFormAction with AppFormActionId:{0} and ActionName:{1}", AppFormAction.AppFormActionId, AppFormAction.ActionName), Request.Browser.Browser);
                        }
                    }
                }

                return Json(new
                {
                    status = "success",
                    data = "Deleted successfully"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        // List/Grid View
        public ActionResult AppFormList()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new AppFormModel();
                model = prepareappformmodel(model, user);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult AppFormSelectList(DataSourceRequest command, AppFormModel model, IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            var dynamicnavigationpath = "";
            var Is_Lite = _ISchoolSettingService.GetorSetSchoolData("Is_Lite", "0").AttributeValue == "1" ? true : false;
            if (Is_Lite)
                dynamicnavigationpath = "litenavigationpath";
            else
                dynamicnavigationpath = "navigationpath";

            var CustomNavigation = _ISchoolSettingService.GetorSetSchoolData("CustomNavigation", "0").AttributeValue == "1" ? true : false;
            string file = "";
            var SchoolDbId = Convert.ToString(System.Web.HttpContext.Current.Session["SchoolId"]);
            //get the Json filepath  
            if (CustomNavigation)
            {
                file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + "/" + SchoolDbId + "/" + Convert.ToString(WebConfigurationManager.AppSettings["leftjson"]));
            }
            else
            {
                file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(WebConfigurationManager.AppSettings["leftjson"]));
            }
            //string file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(WebConfigurationManager.AppSettings["leftjson"]));
            //deserialize JSON from file  
            string Jsonres = System.IO.File.ReadAllText(file).Replace("#", "");
            System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();
            var list = ser.Deserialize<List<UserNavigationPermissionModel>>(Jsonres);
            var navlists = new List<UserParentNavigationPermissionModel>();

            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    foreach (var itm in item.navigation)
                    {
                        if (itm.Child.Count > 0)
                        {
                            foreach (var m in itm.Child)
                            {
                                if (m.Child.Count > 0)
                                {
                                    foreach (var n in m.Child)
                                        navlists.Add(new UserParentNavigationPermissionModel { NavigationItemId = n.NavigationItemId, ControllerName = n.ControllerName, ActionName = n.ActionName, IsActive = n.IsActive, NavigationItem = n.NavigationItem });
                                }
                                else
                                    navlists.Add(new UserParentNavigationPermissionModel { NavigationItemId = m.NavigationItemId, ControllerName = m.ControllerName, ActionName = m.ActionName, IsActive = m.IsActive, NavigationItem = m.NavigationItem });
                            }
                        }
                        else
                            navlists.Add(new UserParentNavigationPermissionModel { NavigationItemId = itm.NavigationItemId, ControllerName = itm.ControllerName, ActionName = itm.ActionName, IsActive = itm.IsActive, NavigationItem = itm.NavigationItem });
                    }
                }
            }

            // navigation item drop down
            foreach (var nav in navlists)
                model.AvailableNavigationItem.Add(new SelectListItem { Selected = false, Text = nav.NavigationItem, Value = nav.NavigationItemId.ToString() });



            var allappforms = _IUserManagementService.GetAllAppForms(ref count, ModuleId: model.Module);
            if (!string.IsNullOrEmpty(model.AppForm))
                allappforms = allappforms.Where(a => a.AppForm1.ToLower().Contains(model.AppForm.ToLower())).ToList();
            allappforms = allappforms.OrderBy(a => a.SortingIndex).ToList();

            var appforms = allappforms.Select(x =>
                {
                    var AppFormList = new AppFormList();
                    AppFormList.AppForm = x.AppForm1;
                    AppFormList.EncAppFormId = _ICustomEncryption.base64e(x.AppFormId.ToString());
                    AppFormList.Module = x.Module.Module1;
                    AppFormList.DisplayOrder = x.SortingIndex.ToString();
                    // find navigation item
                    var navitem = model.AvailableNavigationItem.Where(p => p.Value == x.NavigationItemId.ToString()).FirstOrDefault();
                    AppFormList.NavigationItem = navitem != null ? navitem.Text : "NA";
                    AppFormList.IsDeleted = true;
                   // if (x.UserRoleActionPermissions.Count > 0)
                        //AppFormList.IsDeleted = false;

                    return AppFormList;
                }).AsQueryable().Sort(sort);
            var gridModel = new DataSourceResult
          {
              Data = appforms.PagedForCommand(command).ToList(),
              Total = appforms.Count()
          };
            return Json(gridModel);
        }

        #endregion

        #endregion

        #region Ajax

        #region App Form with App form Action detail



        #endregion

        #endregion

    }
}