﻿using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ContactModule;
using DAL.DAL.DocumentModule;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.SettingService;
using DAL.DAL.StudentModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Student;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.Controllers
{
    public class RegistrationController : Controller
    {
        #region fields

        public int count = 0;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly IAddressService _IAddressService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly ILogService _Logger;
        private readonly IStudentService _IStudentService;
        private readonly IContactService _IContactService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStudentMasterService _IStudentMasterService;
        private readonly IDocumentService _IDocumentService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IWebHelper _WebHelper;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly ISubjectService _ISubjectService;
        private readonly IStudentAddOnService _IStudentAddOnService;

        #endregion

        #region ctor

        public RegistrationController(IDropDownService IDropDownService,
            IActivityLogService IActivityLogService,
            IActivityLogMasterService IActivityLogMasterService,
            ILogService Logger,
            IAddressService IAddressService,
            IAddressMasterService IAddressMasterService,
            IStudentService IStudentService,
            IContactService IContactService,
            ICatalogMasterService ICatalogMasterService,
            IStudentMasterService IStudentMasterService,
            IDocumentService IDocumentService,
            ISchoolSettingService ISchoolSettingService,
            IWebHelper WebHelper,
            IUserService IUserService,
            IConnectionService IConnectionService,
            ISubjectService ISubjectService,
            IStudentAddOnService IStudentAddOnService)
        {
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._IAddressService = IAddressService;
            this._IAddressMasterService = IAddressMasterService;
            this._Logger = Logger;
            this._IStudentService = IStudentService;
            this._IContactService = IContactService;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IStudentMasterService = IStudentMasterService;
            this._IDocumentService = IDocumentService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._WebHelper = WebHelper;
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._ISubjectService = ISubjectService;
            this._IStudentAddOnService = IStudentAddOnService;
        }

        #endregion

        #region utility

        //Convert date
        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }

        #endregion


        public StudentModel preparemodel(StudentModel model)
        {
            string session = string.Empty;
            int currentyear = DateTime.UtcNow.Year;
            int nextyear = currentyear + 1;
            session = currentyear + "-" + nextyear;

            model.IsRegistrationOpen = true;
            var Crntsession = _ICatalogMasterService.GetAllSessions(ref count, session).FirstOrDefault();
            // standard
            var sessionadmnstandards = _ICatalogMasterService.GetAllSessionAdmissionDetails(ref count);
            if (Crntsession != null)
                sessionadmnstandards = sessionadmnstandards.Where(s => s.SessionId == Crntsession.SessionId).ToList();
            sessionadmnstandards = sessionadmnstandards.Where(s => s.AdmnOpenDate <= DateTime.UtcNow.Date && s.AdmnClosingDate >= DateTime.UtcNow.Date).ToList();
            // standard ids
            var standardids = sessionadmnstandards.Select(s => s.StandardId).Distinct().ToArray();
            // get all standards 
            var AllStandards = _ICatalogMasterService.GetAllStandardMasters(ref count);
            AllStandards = AllStandards.Where(s => standardids.Contains(s.StandardId)).ToList();
            if (AllStandards.Count == 0)
                model.IsRegistrationOpen = false;
            else
            {
                model.AvailableStandards.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
                foreach (var item in AllStandards)
                    model.AvailableStandards.Add(new SelectListItem { Selected = true, Text = item.Standard, Value = item.StandardId.ToString() });
            }

            // gender list
            model.AvailableGender = _IDropDownService.GenderList();
            // nationality
            model.AvailableNationality = _IDropDownService.NationalityList();
            // student category
            model.AvailableStudentCategory = _IDropDownService.StudentCategoryList();
            // prv school standard
            model.PreviousSchoolModel.AvailablePrvSchoolStandards = _IDropDownService.StandardList();
            // address type
            model.AddressModel.AvailableAddresstype = _IDropDownService.AddressTypeList();
            // cities
            model.AddressModel.AvailableCities = _IDropDownService.AddressCityList();
            // states
            model.AddressModel.AddressCityModel.AvailableStates = _IDropDownService.AddressStateList();
            // countries
            model.AddressModel.AddressCityModel.AddressStateModel.AvailableCountries = _IDropDownService.AddressCountryList();
            // Religion
            model.AvailableReligions = _IDropDownService.ReligionList();
            // board
            model.PreviousSchoolModel.AvailablePrvSchoolBoards = _IDropDownService.BoardList();
            // Blood Group
            model.AvailableBloodGroup = _IDropDownService.BloodGroupList();
            // Document Type
            model.AttachedDocumentModel.AvailableDocumentType = _IDropDownService.DocumentTypeList();

            return model;
        }

        // GET: Registration
        public ActionResult Index()
        {
            try
            {
                StudentModel model = new StudentModel();
                model = preparemodel(model);
                model.NextStudentId = _IStudentService.LastStudentId(); // get next student Id
                return View(model);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Registration");
            }
        }

        public StudentModel CheckStudentInfoError(StudentModel model)
        {
            if (model.DOB == null)
                ModelState.AddModelError("DateofBirth", "DOB required");

            if (model.DOB != null)
            {
                if (model.DOB > DateTime.UtcNow)
                {
                    ModelState.AddModelError("DateofBirth", "DOB invalid");
                }
            }

            // check if Aadhar card mandatory valid
            var IsAadharValid = _ISchoolSettingService.GetAttributeValue("IsAadharCardMandatory");
            if (IsAadharValid != null && !string.IsNullOrEmpty(IsAadharValid) && IsAadharValid == "1")
            {
                if (string.IsNullOrEmpty(model.AadharCard))
                    ModelState.AddModelError("AadharCard", "AadharCard required");
                else
                {
                    // check if alphabet
                    int aadharno = 0;
                    if (!int.TryParse(model.AadharCard, out aadharno))
                        ModelState.AddModelError("AadharCard", "AadharCard invalid");
                    if (!(model.AadharCard.Length == 16))
                        ModelState.AddModelError("AadharCard", "AadharCard invalid");
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(model.AadharCard))
                {
                    // check if alphabet
                    int aadharno = 0;
                    if (!int.TryParse(model.AadharCard, out aadharno))
                        ModelState.AddModelError("AadharCard", "AadharCard invalid");
                    if (!(model.AadharCard.Length == 16))
                        ModelState.AddModelError("AadharCard", "AadharCard invalid");
                }
            }


            if (!string.IsNullOrEmpty(model.Email))
            {
                string emailRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                         @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                Regex re = new Regex(emailRegex);
                if (!re.IsMatch(model.Email))
                    ModelState.AddModelError("Email", "Email is not valid");

                // check unique
                var contatinfotype = _IContactService.GetAllContactInfoTypes(ref count, "Email").FirstOrDefault(); // contactinfotype
                var allcontactinfos = _IContactService.GetAllContactInfos(ref count, ContactInfoTypeId: contatinfotype.ContactInfoTypeId).ToList();
                if (allcontactinfos.Count > 0)
                {
                    allcontactinfos = allcontactinfos.Where(c => c.ContactInfo1.ToLower() == model.Email.ToLower()).ToList();
                    if (model.StudentId > 0)
                        allcontactinfos = allcontactinfos.Where(c => c.ContactId != model.StudentId).ToList();

                    if (allcontactinfos.Count > 0)
                        ModelState.AddModelError("Email", "Email already exist");
                }
            }
            return model;
        }

        [HttpPost]
        public ActionResult Index(StudentModel model)
        {
            int? nullablevariable = null;
            //decript Student Id
            model = CheckStudentInfoError(model);
            if (ModelState.IsValid)
            {
                try
                {
                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault(); // activity log add
                    var doctypeid = _IDocumentService.GetAllDocumentTypes(ref count, "Image").FirstOrDefault(); // document type Id
                    var contatinfotype = _IContactService.GetAllContactInfoTypes(ref count, "Email").FirstOrDefault(); // contactinfotype
                    var contacttype = _IAddressMasterService.GetAllContactTypes(ref count, "Student").FirstOrDefault(); // contact type

                    #region Save new Student Registration

                    Student student = new Student();
                    string session = string.Empty;
                    int currentyear = DateTime.UtcNow.Year;
                    int nextyear = currentyear + 1;
                    session = currentyear + "-" + nextyear;
                    var Crntsession = _ICatalogMasterService.GetAllSessions(ref count, session).FirstOrDefault();

                    student.AdmnSessionId = Crntsession.SessionId;
                    student.AdmnStandardId = model.StandardId;
                    student.FName = model.FName;
                    student.LName = model.LName;
                    student.DOB = model.DOB;
                    student.AadharCardNo = model.AadharCard;
                    student.GenderId = model.GenderId;

                    // get next registration no
                    student.RegNo = model.RegNo;


                    student.RegDate = DateTime.UtcNow;
                    student.StudentCategoryId = model.StudentCategoryId;
                    //var stdstatus = _IStudentMasterService.GetAllStudentStatus(ref count, "Active").FirstOrDefault();
                    //student.StudentStatusId = stdstatus != null ? stdstatus.StudentStatusId : nullablevariable;
                    student.NationalityId = model.NationalityId;
                    student.ReligionId = model.ReligionId;

                    _IStudentService.InsertStudent(student);

                    #endregion

                    #region student image
                    if (!string.IsNullOrEmpty(model.StudentImagepath))
                    {
                        AttachedDocument AttachedDocument = new AttachedDocument();
                        var filename = SaveStudentImage(model);
                        // get doc type
                        AttachedDocument.DocumentTypeId = doctypeid != null ? doctypeid.DocumentTypeId : nullablevariable;
                        AttachedDocument.StudentId = student.StudentId;
                        AttachedDocument.Image = filename;
                        _IDocumentService.InsertAttachedDocument(AttachedDocument);
                    }
                    #endregion

                    #region student email contact info
                    if (!string.IsNullOrEmpty(model.Email))
                    {
                        ContactInfo stdinfo = new ContactInfo();
                        stdinfo.ContactId = student.StudentId;
                        stdinfo.ContactInfo1 = model.Email;
                        stdinfo.ContactInfoTypeId = contatinfotype != null ? contatinfotype.ContactInfoTypeId : nullablevariable;
                        stdinfo.ContactTypeId = contacttype != null ? contacttype.ContactTypeId : nullablevariable;
                        stdinfo.IsDefault = true;
                        stdinfo.Status = true;
                        _IContactService.InsertContactInfo(stdinfo);
                    }
                    #endregion


                    return RedirectToAction("Edit", new { Id = student.StudentId });
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Registration");
                }
            }

            // change value from DOB , DOJ to string
            if (model.DOB.HasValue)
                model.DateofBirth = ConvertDate(model.DOB.Value);
            // registration series edit able or not
            model = preparemodel(model);
            return View(model);
        }

        // save std image
        public string SaveStudentImage(StudentModel model)
        {
            string newpath = "";
            string filename = "";
            string newpathstring = string.Empty;
            string path = model.StudentImagepath;

            // alter path 
            var pathary = path.Split('\\');
            if (pathary.Length > 1)
            {
                // file name
                var file = path.Split('\\').Last();
                var filenameext = Path.GetExtension(file).ToLower();
                pathary = pathary.Take(pathary.Count() - 1).ToArray();
                newpath = String.Join("\\", pathary);

                // new path with image ename
                newpathstring = newpath + "\\" + model.StudentId + "_image" + filenameext;
                filename = model.StudentId + "_image" + filenameext;

                // rename image
                if (System.IO.File.Exists(newpathstring))
                    System.IO.File.Delete(newpathstring);
                System.IO.File.Move(model.StudentImagepath, newpathstring);
            }
            else
            {
                pathary = path.Split('/');
                if (pathary.Length > 1)
                    filename = pathary[pathary.Length - 1];
            }
            return filename;
        }

        // error page
        public ActionResult Error()
        {
            return View();
        }

    }
}