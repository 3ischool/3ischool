﻿using BAL.BAL.Common;
using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.CertificateModule;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.MessagingModule;
using DAL.DAL.SettingService;
using DAL.DAL.StudentModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.Controllers
{
    public class MessagingController : Controller
    {
        #region fields

        public int count = 0;
        static Regex ValidEmailRegex = CreateValidEmailRegex();
        static Regex ValidPhoneRegex = CreateValidPhoneNoRegex();
        private readonly IDropDownService _IDropDownService;
        private readonly ILogService _Logger;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStudentService _IStudentService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IWebHelper _WebHelper;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly ICertificateService _ICertificateService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IMessagingService _IMessagingService;

        private readonly IUserAuthorizationService _IUserAuthorizationService;
        #endregion

        #region ctor

        public MessagingController(IDropDownService IDropDownService,
            ILogService Logger,
            ICatalogMasterService ICatalogMasterService,
            IStudentService IStudentService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            IUserService IUserService,
            IConnectionService IConnectionService,
            IWebHelper WebHelper,
            ISchoolSettingService ISchoolSettingService,
            ICertificateService ICertificateService,
            ICustomEncryption ICustomEncryption,
            IMessagingService IMessagingService, IUserAuthorizationService IUserAuthorizationService)
        {
            this._IDropDownService = IDropDownService;
            this._Logger = Logger;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IStudentService = IStudentService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._IActivityLogService = IActivityLogService;
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._WebHelper = WebHelper;
            this._ISchoolSettingService = ISchoolSettingService;
            this._ICertificateService = ICertificateService;
            this._ICustomEncryption = ICustomEncryption;
            this._IMessagingService = IMessagingService;
            this._IUserAuthorizationService = IUserAuthorizationService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }

        public string ConvertTime(TimeSpan time)
        {
            TimeSpan timespan = new TimeSpan(time.Hours, time.Minutes, time.Seconds);
            DateTime datetime = DateTime.Today.Add(timespan);
            string sdisplayTime = datetime.ToString("hh:mm tt");
            return sdisplayTime;
        }

        #endregion

        #region Methods

        #region Email Template

        #region Create

        public EmailTemplateModel prepareEmailTemplateModel(EmailTemplateModel model)
        {
            model.AvailableDelayPeriodList = _IDropDownService.DelayPeriodList();
            model.AvailableMessageDurationList = _IDropDownService.MessageDurationList();
            model.AvailableEmailAccountList = _IDropDownService.EmailAccountList();
           
            return model;
        }

        // GET: Messaging
        public ActionResult EmailTemplate()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new EmailTemplateModel();
                model = prepareEmailTemplateModel(model);
                model.IsActive = true;
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        //validate email address
        private static Regex CreateValidEmailRegex()
        {
            string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            return new Regex(validEmailPattern, RegexOptions.IgnoreCase);
        }
        //Post: Messaging
        [HttpPost]
        public ActionResult EmailTemplate(EmailTemplateModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.BccEmailAddress != null)
                    {
                        //validate Bcc Email Addresses
                        var bccEmailAddresslist = model.BccEmailAddress.Split(',').ToList();
                        foreach (var email in bccEmailAddresslist)
                        {
                            bool isValid = ValidEmailRegex.IsMatch(email.Trim());
                            if (!isValid)
                            {
                                ModelState.AddModelError("BccEmailAddress", "Please Correct Email Address :" + email);
                                model = prepareEmailTemplateModel(model);
                                return View(model);
                            }
                        }
                    }
                    if (model.CCEmailAddress != null)
                    {
                        //validate cc Email Addresses
                        var ccEmailAddresslist = model.CCEmailAddress.Split(',').ToList();
                        foreach (var email in ccEmailAddresslist)
                        {
                            bool isValid = ValidEmailRegex.IsMatch(email.Trim());
                            if (!isValid)
                            {
                                ModelState.AddModelError("CCEmailAddress", "Please Correct Email Address :" + email);
                                model = prepareEmailTemplateModel(model);
                                return View(model);
                            }
                        }
                    }
                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    MsgEmailTemplate EmailTemplate = new MsgEmailTemplate();
                    EmailTemplate.Name = model.Name;
                    EmailTemplate.BccEmailAddress = model.BccEmailAddress;
                    EmailTemplate.CCEmailAddress = model.CCEmailAddress;
                    EmailTemplate.Subject = model.Subject;
                    EmailTemplate.Body = model.Body;
                    EmailTemplate.IsActive = model.IsActive;
                   
                    EmailTemplate.EmailAccountId = model.EmailAccountId;
                   

                    if (model.IsSendImmediately == true)
                        EmailTemplate.IsSendImmediately = model.IsSendImmediately;
                    else if (model.IsInAdvance && !model.IsSendImmediately)
                    {
                        EmailTemplate.MsgDurationId = model.MsgDurationId;
                        EmailTemplate.MsgFrequencyId = model.MsgFrequencyId;
                        EmailTemplate.IsSendImmediately = false;
                    }
                    else if (!model.IsInAdvance && !model.IsSendImmediately )
                    {
                        EmailTemplate.IsSendImmediately = false;
                        EmailTemplate.DelayBeforeSend = model.DelayBeforeSend;
                        EmailTemplate.DelayPeriodId = model.DelayPeriodId;
                    }
                    else
                    {
                        TempData["ErrorNotification"] = "Error in Entries";
                        model = prepareEmailTemplateModel(model);
                        return View(model);
                    }
                    _IMessagingService.InsertMsgEmailTemplate(EmailTemplate);
                    // TimeTable log Table
                    var MsgEmailTemplatetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "MsgEmailTemplate").FirstOrDefault();
                    // TimeTable log Table
                    if (MsgEmailTemplatetable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, MsgEmailTemplatetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog(EmailTemplate.MsgEmailTemplateId, MsgEmailTemplatetable.TableId, logtype.ActivityLogTypeId, user, String.Format("Edit MsgEmailTemplate with MsgEmailTemplateId:{0} and Name:{1}", EmailTemplate.MsgEmailTemplateId, EmailTemplate.Name), Request.Browser.Browser);
                    }
                    TempData["SuccessNotification"] = "EmailTemplate saved successfully";
                    return RedirectToAction("EmailTemplate");
                }
                else
                {
                    model = prepareEmailTemplateModel(model);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #region View List

        public EmailTemplateModel prepareViewEmailTemplateModel(EmailTemplateModel model)
        {

            var allEmailTemplate = _IMessagingService.GetAllMsgEmailTemplateList(ref count, Name: model.Name, Subject: model.Subject,IsActive:model.IsActive).ToList();
            EmailTemplateModel EmailTemplateModel = new EmailTemplateModel();
            foreach (var aet in allEmailTemplate)
            {
                EmailTemplateModel = new EmailTemplateModel();
                EmailTemplateModel.Name = aet.Name;
                EmailTemplateModel.Subject = aet.Subject;
                EmailTemplateModel.IsActive = (bool)aet.IsActive;
                EmailTemplateModel.IsSendImmediately = (bool)aet.IsSendImmediately;
                EmailTemplateModel.EncMsgEmailTemplateId = _ICustomEncryption.base64e(aet.MsgEmailTemplateId.ToString());
                model.EmailTemplateList.Add(EmailTemplateModel);
            }

            return model;
        }

        // GET: Messaging
        public ActionResult ViewEmailTemplateList(EmailTemplateModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
             var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Messaging", "ViewEmailTemplateList", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                //var EmailTemplateModel = new EmailTemplateModel();
                model = prepareViewEmailTemplateModel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Messaging", "ViewEmailTemplateList", "Add Record");
                model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Messaging", "ViewEmailTemplateList", "Edit Record");
                model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Messaging", "ViewEmailTemplateList", "Delete Record");
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #region Edit Email Template

        public ActionResult EditEmailTemplate(string id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                int msgEmailTemplateId = 0;
                // check id exist
                if (!string.IsNullOrEmpty(id))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(id);

                    if (int.TryParse(qsid, out msgEmailTemplateId))
                    {

                    }
                }
                var msgEmailTemplate = _IMessagingService.GetMsgEmailTemplateById(msgEmailTemplateId);
                var model = new EmailTemplateModel();
                model.EncMsgEmailTemplateId = _ICustomEncryption.base64e(msgEmailTemplate.MsgEmailTemplateId.ToString());
                model.Name = msgEmailTemplate.Name;
                model.Subject = msgEmailTemplate.Subject;
                model.BccEmailAddress = msgEmailTemplate.BccEmailAddress;
                model.BccEmailAddress = msgEmailTemplate.BccEmailAddress;
                model.Subject = msgEmailTemplate.Subject;
                model.Body = msgEmailTemplate.Body;
                if (msgEmailTemplate.IsActive != null)
                    model.IsActive = (bool)msgEmailTemplate.IsActive;
                if (msgEmailTemplate.IsSendImmediately != null)
                    model.IsSendImmediately = (bool)msgEmailTemplate.IsSendImmediately;
                if (msgEmailTemplate.DelayBeforeSend != null) {
                    model.DelayBeforeSend = (int)msgEmailTemplate.DelayBeforeSend;
                    model.IsSendImmediately = false;
                }
                if (msgEmailTemplate.DelayPeriodId != null) {
                    model.DelayPeriodId = (int)msgEmailTemplate.DelayPeriodId;
                    model.IsSendImmediately = false;
                }
                if (msgEmailTemplate.MsgDurationId != null) {
                    model.MsgDurationId = (int)msgEmailTemplate.MsgDurationId;
                    model.IsInAdvance = true;
                }
                if (msgEmailTemplate.MsgFrequencyId != null) {
                    model.MsgFrequencyId = (int)msgEmailTemplate.MsgFrequencyId;
                    model.IsInAdvance = true;
                }
                if (msgEmailTemplate.EmailAccountId != null)
                    model.EmailAccountId = (int)msgEmailTemplate.EmailAccountId;
                model = prepareEmailTemplateModel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        [HttpPost]
        public ActionResult EditEmailTemplate(EmailTemplateModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                int msgEmailTemplateId = 0;
                // check id exist
                if (!string.IsNullOrEmpty(model.EncMsgEmailTemplateId))
                {
                    if (model.CCEmailAddress != null)
                    {

                        //validate cc Email Addresses
                        var ccEmailAddresslist = model.CCEmailAddress.Split(',').ToList();
                        foreach (var email in ccEmailAddresslist)
                        {
                            var mail = email.Trim();
                            bool isValid = ValidEmailRegex.IsMatch(email);
                            if (!isValid)
                            {
                                var emaillength = mail.Length;
                                if (emaillength > 10)
                                {
                                    ModelState.AddModelError("CCEmailAddress", "Please Correct Email Address");
                                }
                                else
                                    ModelState.AddModelError("CCEmailAddress", "Please Correct Email Address :" + mail);
                                model = prepareEmailTemplateModel(model);
                                return View(model);
                            }
                        }
                    }

                    if (model.BccEmailAddress != null)
                    {
                        //validate Bcc Email Addresses
                        var bccEmailAddresslist = model.BccEmailAddress.Split(',').ToList();
                        foreach (var email in bccEmailAddresslist)
                        {
                            var mail = email.Trim();
                            bool isValid = ValidEmailRegex.IsMatch(mail);
                            if (!isValid)
                            {
                                var emaillength=mail.Length;
                                if (emaillength > 10)
                                {
                                    ModelState.AddModelError("BccEmailAddress", "Please Correct Email Address");
                                }else
                                ModelState.AddModelError("BccEmailAddress", "Please Correct Email Address :" + mail);
                                model = prepareEmailTemplateModel(model);
                                return View(model);
                            }
                        }
                    }
                   
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(model.EncMsgEmailTemplateId);

                    if (int.TryParse(qsid, out msgEmailTemplateId))
                    {
                        model.MsgEmailTemplateId = msgEmailTemplateId;
                    }
                    if (model.MsgEmailTemplateId > 0)
                    {
                        // Activity log type add
                        var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();

                        var msgEmailTemplate = _IMessagingService.GetMsgEmailTemplateById(model.MsgEmailTemplateId);
                        msgEmailTemplate.Name = model.Name;
                        msgEmailTemplate.BccEmailAddress = model.BccEmailAddress;
                        msgEmailTemplate.CCEmailAddress = model.CCEmailAddress;
                        msgEmailTemplate.Subject = model.Subject;
                        msgEmailTemplate.Body = model.Body;
                        msgEmailTemplate.IsActive = model.IsActive;
                       
                        msgEmailTemplate.EmailAccountId = model.EmailAccountId;

                        if (model.IsSendImmediately == true){
                            msgEmailTemplate.IsSendImmediately = model.IsSendImmediately;
                         msgEmailTemplate.MsgDurationId = null;
                         msgEmailTemplate.MsgFrequencyId = null;
                         msgEmailTemplate.DelayBeforeSend = null;
                         msgEmailTemplate.DelayPeriodId = null;
                        }
                        else if (model.IsInAdvance && !model.IsSendImmediately)
                        {
                            msgEmailTemplate.MsgDurationId = model.MsgDurationId;
                            msgEmailTemplate.MsgFrequencyId = model.MsgFrequencyId;
                            msgEmailTemplate.IsSendImmediately = false;
                            msgEmailTemplate.DelayBeforeSend = null;
                            msgEmailTemplate.DelayPeriodId = null;
                        }
                        else if (!model.IsInAdvance && !model.IsSendImmediately)
                        {
                            msgEmailTemplate.IsSendImmediately = false;
                            msgEmailTemplate.DelayBeforeSend = model.DelayBeforeSend;
                            msgEmailTemplate.DelayPeriodId = model.DelayPeriodId;
                            msgEmailTemplate.MsgDurationId = null;
                            msgEmailTemplate.MsgFrequencyId = null;
                        }
                        else
                        {
                            TempData["ErrorNotification"] = "Error in Entries";
                            model = prepareEmailTemplateModel(model);
                            return View(model);
                        }
                        _IMessagingService.UpdateMsgEmailTemplate(msgEmailTemplate);
                        // TimeTable log Table
                        var MsgEmailTemplatetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "MsgEmailTemplate").FirstOrDefault();
                        // TimeTable log Table
                        if (MsgEmailTemplatetable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, MsgEmailTemplatetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(msgEmailTemplate.MsgEmailTemplateId, MsgEmailTemplatetable.TableId, logtype.ActivityLogTypeId, user, String.Format("Edit MsgEmailTemplate with MsgEmailTemplateId:{0} and Name:{1}", msgEmailTemplate.MsgEmailTemplateId, msgEmailTemplate.Name), Request.Browser.Browser);
                        }
                    }
                }
                TempData["SuccessNotification"] = "EmailTemplate has been updated successfully";
                model = prepareEmailTemplateModel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #endregion

        #region Sms Template

        #region Create

        public SMSTemplateModel prepareSMSTemplateModel(SMSTemplateModel model)
        {
            model.AvailableMessageDurationList = _IDropDownService.MessageDurationList();
           
            return model;
        }

        // GET: Messaging
        public ActionResult SMSTemplate()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new SMSTemplateModel();
                model = prepareSMSTemplateModel(model);
                model.IsActive = true;
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        //validate SMS address
        private static Regex CreateValidPhoneNoRegex()
        {
            string validPhoneNoPattern = @"^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$";

            return new Regex(validPhoneNoPattern, RegexOptions.IgnoreCase);
        }
        //Post: Messaging
        [HttpPost]
        public ActionResult SMSTemplate(SMSTemplateModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                if (model.IsInAdvance)
                {
                    if (model.MsgDurationId == 0)
                    {
                        ModelState.AddModelError("MsgDurationId", "Msg Duration is required");
                        //model = prepareSMSTemplateModel(model);
                        //return View(model);
                    }
                }
                if (ModelState.IsValid)
                {
                    
                    //if (model.SendTo != null)
                    //{
                    //    //validate Mobile No
                    //    var MobileNolist = model.SendTo.Split(',').ToList();
                    //    foreach (var mobileNo in MobileNolist)
                    //    {
                    //        var mobile_No = mobileNo.Trim();
                    //        bool isValid = ValidPhoneRegex.IsMatch(mobile_No);
                    //        if (!isValid)
                    //        {
                    //            ModelState.AddModelError("SendTo", "Please Correct Mobile No :" + mobileNo);
                    //            model = prepareSMSTemplateModel(model);
                    //            return View(model);
                    //        }
                    //    }
                    //}
                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    MsgSMSTemplate SMSTemplate = new MsgSMSTemplate();
                    SMSTemplate.Name = model.Name;
                    SMSTemplate.Body = model.Body;
                    SMSTemplate.IsActive = model.IsActive;
                    if (model.IsInAdvance) {
                    SMSTemplate.MsgDurationId = model.MsgDurationId;
                    SMSTemplate.MsgFrequencyId = model.MsgFrequencyId;
                    }
                    else
                    {
                        SMSTemplate.MsgDurationId = null;
                        SMSTemplate.MsgFrequencyId = null;
                    }
                    //SMSTemplate.SendTo = model.SendTo;
                    _IMessagingService.InsertMsgSMSTemplate(SMSTemplate);
                    // TimeTable log Table
                    var MsgSMSTemplatetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "MsgSMSTemplate").FirstOrDefault();
                    // TimeTable log Table
                    if (MsgSMSTemplatetable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, MsgSMSTemplatetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog(SMSTemplate.MsgSMSTemplateId, MsgSMSTemplatetable.TableId, logtype.ActivityLogTypeId, user, String.Format("Edit MsgSMSTemplate with MsgSMSTemplateId:{0} and Name:{1}", SMSTemplate.MsgSMSTemplateId, SMSTemplate.Name), Request.Browser.Browser);
                    }
                    TempData["SuccessNotification"] = "SMSTemplate added successfully";
                    return RedirectToAction("SMSTemplate");
                }
                else
                {
                    model = prepareSMSTemplateModel(model);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #region View List

        public SMSTemplateModel prepareViewSMSTemplateModel(SMSTemplateModel model)
        {

            var allSMSTemplate = _IMessagingService.GetAllMsgSMSTemplateList(ref count).ToList();
            SMSTemplateModel SMSTemplateModel = new SMSTemplateModel();
            foreach (var ast in allSMSTemplate)
            {
                SMSTemplateModel = new SMSTemplateModel();
                SMSTemplateModel.Name = ast.Name;
                if (ast.MsgDurationId != null) {
                    SMSTemplateModel.MsgDurationId = (int)ast.MsgDurationId;
                    model.IsInAdvance = true;
                }
                if (ast.IsActive != null) 
                SMSTemplateModel.IsActive = (bool)ast.IsActive;
                if (ast.MsgFrequencyId != null)
                {
                    SMSTemplateModel.MsgFrequencyId = (int)ast.MsgFrequencyId;
                    model.IsInAdvance = true;
                }
                SMSTemplateModel.EncMsgSMSTemplateId = _ICustomEncryption.base64e(ast.MsgSMSTemplateId.ToString());
                model.SMSTemplateList.Add(SMSTemplateModel);
            }

            return model;
        }

        // GET: Messaging
        public ActionResult ViewSMSTemplateList()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Messaging", "ViewSMSTemplateList", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new SMSTemplateModel();
                model = prepareViewSMSTemplateModel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Messaging", "ViewSMSTemplateList", "Add Record");
                model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Messaging", "ViewSMSTemplateList", "Edit Record");
                model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Messaging", "ViewSMSTemplateList", "Delete Record");
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #region Edit SMS Template

        public ActionResult EditSMSTemplate(string id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                int msgSMSTemplateId = 0;
                // check id exist
                if (!string.IsNullOrEmpty(id))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(id);

                    if (int.TryParse(qsid, out msgSMSTemplateId))
                    {

                    }
                }
                var msgSMSTemplate = _IMessagingService.GetMsgSMSTemplateById(msgSMSTemplateId);
                var model = new SMSTemplateModel();
                model.EncMsgSMSTemplateId = _ICustomEncryption.base64e(msgSMSTemplate.MsgSMSTemplateId.ToString());
                model.Name = msgSMSTemplate.Name;
                model.Body = msgSMSTemplate.Body;
                //model.SendTo = msgSMSTemplate.SendTo;
                if (msgSMSTemplate.IsActive != null)
                    model.IsActive = (bool)msgSMSTemplate.IsActive;
                if (msgSMSTemplate.MsgDurationId != null) {
                    model.MsgDurationId = (int)msgSMSTemplate.MsgDurationId;
                    model.IsInAdvance = true;
                }
                if (msgSMSTemplate.MsgFrequencyId != null) {
                    model.MsgFrequencyId = (int)msgSMSTemplate.MsgFrequencyId;
                    model.IsInAdvance = true;
                }
                model = prepareSMSTemplateModel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        [HttpPost]
        public ActionResult EditSMSTemplate(SMSTemplateModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                if (ModelState.IsValid)
                {
                    int msgSMSTemplateId = 0;
                    //if (model.SendTo != null)
                    //{
                    //    //validate Mobile No
                    //    var MobileNolist = model.SendTo.Split(',').ToList();
                    //    foreach (var mobileNo in MobileNolist)
                    //    {
                    //        var mobile_No = mobileNo.Trim();
                    //        bool isValid = ValidPhoneRegex.IsMatch(mobile_No);
                    //        if (!isValid)
                    //        {
                    //            ModelState.AddModelError("SendTo", "Please Correct Mobile No :" + mobileNo);
                    //            model = prepareSMSTemplateModel(model);
                    //            return View(model);
                    //        }
                    //    }
                    //}
                    // check id exist
                    if (!string.IsNullOrEmpty(model.EncMsgSMSTemplateId))
                    {
                        // decrypt Id
                        var qsid = _ICustomEncryption.base64d(model.EncMsgSMSTemplateId);

                        if (int.TryParse(qsid, out msgSMSTemplateId))
                        {
                            model.MsgSMSTemplateId = msgSMSTemplateId;
                        }
                        if (model.MsgSMSTemplateId > 0)
                        {
                            // Activity log type add
                            var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();

                            var msgSMSTemplate = _IMessagingService.GetMsgSMSTemplateById(model.MsgSMSTemplateId);
                            msgSMSTemplate.Name = model.Name;
                            msgSMSTemplate.Body = model.Body;
                            msgSMSTemplate.IsActive = model.IsActive;
                            if (model.IsInAdvance)
                            {
                                msgSMSTemplate.MsgDurationId = model.MsgDurationId;
                                msgSMSTemplate.MsgFrequencyId = model.MsgFrequencyId;
                            }
                            else
                            {
                                msgSMSTemplate.MsgDurationId = null;
                                msgSMSTemplate.MsgFrequencyId = null;
                            }
                            //msgSMSTemplate.SendTo = model.SendTo;
                            _IMessagingService.UpdateMsgSMSTemplate(msgSMSTemplate);
                            // TimeTable log Table
                            var MsgSMSTemplatetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "MsgSMSTemplate").FirstOrDefault();
                            // TimeTable log Table
                            if (MsgSMSTemplatetable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, MsgSMSTemplatetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(model.MsgSMSTemplateId, MsgSMSTemplatetable.TableId, logtype.ActivityLogTypeId, user, String.Format("Edit MsgSMSTemplate with MsgSMSTemplateId:{0} and Name:{1}", model.MsgSMSTemplateId, model.Name), Request.Browser.Browser);
                            }
                        }
                    }
                    TempData["SuccessNotification"] = "SMSTemplate has been updated successfully";
                }
                model = prepareSMSTemplateModel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #endregion

        #endregion

        #region Ajax Methods

        public ActionResult GetMsgTokenList()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var Emailtokenslist = _IMessagingService.GetAllEmailMsgTokenList(ref count, IsEmail: true);
                if (Emailtokenslist.Count > 0)
                {
                    var tokenslist = (from etl in Emailtokenslist select etl.TokenName).ToList();
                    return Json(new
                    {
                        status = "success",
                        data = tokenslist
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        data = "No Tokens Available"
                    });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion
    }
}