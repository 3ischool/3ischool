﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Windows;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAL.BAL.Common;
using ViewModel.ViewModel.Library;
using System.Data;
using System.Text;
using DAL.DAL.LibraryModule;
using DAL.DAL.UserModule;
using DAL.DAL.Common;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.SettingService;
using DAL.DAL.CatalogMaster;
using BAL.BAL.Security;
using KSModel.Models;
using DAL.DAL.DocumentModule;
using ViewModel.ViewModel.DocumentManager;
using DAL.DAL.Kendo;
using ViewModel.ViewModel.Student;
using DAL.DAL.StudentModule;
using DAL.DAL.AddressModule;
using DAL.DAL.ContactModule;
using Newtonsoft.Json;
using DAL.DAL.FeeModule;
using System.Web.Configuration;
using ViewModel.ViewModel.Fee;
using System.Web.Script.Serialization;
using System.Data.Entity.Core;
using DAL.DAL.GuardianModule;

namespace KS_SmartChild.Controllers
{
    [Authorize]
    public class PaymentController : Controller
    {
        #region fields
        public int count = 0;
        private readonly KS_ChildEntities db;
        private readonly string DataSource;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ILogService _Logger;
        private readonly IWebHelper _WebHelper;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IExportHelper _IExportHelper;
        private readonly IDocumentService _IDocumentService;
        private readonly ICommonMethodService _ICommonMethodService;
        private readonly IStudentService _IStudentService;
        private readonly IAddressService _IAddressService;
        private readonly IContactService _IContactService;
        private readonly ISchoolDataService _ISchoolDataService;
        private readonly IFeeService _IFeeService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly IGuardianService _IGuardianService;
        #endregion

        #region ctor

        public PaymentController(
            IConnectionService IConnectionService,
            IDropDownService IDropDownService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            ILogService Logger, IUserService IUserService,
            IWebHelper WebHelper, ICatalogMasterService ICatalogMasterService,
            ISchoolSettingService ISchoolSettingService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption,
            IExportHelper IExportHelper,
            IDocumentService IDocumentService,
            ICommonMethodService ICommonMethodService,
            IStudentService IStudentService,
            IAddressService IAddressService, IContactService IContactService,
            ISchoolDataService ISchoolDataService, IFeeService IFeeService,
            IAddressMasterService IAddressMasterService,
            IGuardianService IGuardianService)
        {
           // HttpContext context = System.Web.HttpContext.Current;
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._Logger = Logger;
            this._WebHelper = WebHelper;
            this._ISchoolSettingService = ISchoolSettingService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IExportHelper = IExportHelper;
            this._IDocumentService = IDocumentService;
            this._ICommonMethodService = ICommonMethodService;
            this._IStudentService = IStudentService;
            this._IAddressService = IAddressService;
            this._IContactService = IContactService;
            this._ISchoolDataService = ISchoolDataService;
            this._IFeeService = IFeeService;
            this._IAddressMasterService = IAddressMasterService;
            this._IGuardianService = IGuardianService;
           // this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
           // this.db = new KS_ChildEntities(DataSource);
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        #endregion

        [AllowAnonymous]
        public ActionResult Initiate(string rst="")
        {
            SchoolUser user = new SchoolUser();
            _ICommonMethodService.GetSetCurrentSchoolDataBase(Request, Session, Server);
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;
            }
            try
            {
                //var pgrequestid = _ICustomEncryption.Decrypt(Convert.ToString(rst));
                var PGREQUESTID = 0;
                int.TryParse(_ICustomEncryption.Decrypt(Convert.ToString(rst)),out PGREQUESTID);

               var request= _IFeeService.GetPGRequestById(PGREQUESTID);
                System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();

                StudentPaymentPGReuqest ArrayItems = js.Deserialize<StudentPaymentPGReuqest>(request.PGRequest1);
                StudentProfileModel studentProfileModel = new StudentProfileModel();
                studentProfileModel.PaymentRequestId = request.PGRequestId;
                studentProfileModel.TotalPayAmount = ArrayItems.Amount;
                studentProfileModel.UserType = ArrayItems.UserType;
                studentProfileModel.LoggedInUserId = ArrayItems.LoggedInUserId;


                studentProfileModel.EncStudentId = ArrayItems.EncStudentId;
                studentProfileModel.SessionId = Convert.ToInt32(ArrayItems.SessionId);
                studentProfileModel.FeePeriodId = ArrayItems.FeePeriodId;
                studentProfileModel.ReceiptTypeId = ArrayItems.ReceiptTypeId;
                studentProfileModel.UserType = ArrayItems.UserType;
                studentProfileModel.FeeType = ArrayItems.FeeType;

        string EncFeeType_Id = "";
                if (!string.IsNullOrEmpty(ArrayItems.FeeType))
                {
                    //var feetype = db.FeeTypes.Where(x=>x.FeeType1 == FeeType).FirstOrDefault();
                    var feetype = _IFeeService.GetAllFeeTypes(ref count, ArrayItems.FeeType).FirstOrDefault();
                    EncFeeType_Id = feetype != null ? feetype.FeeTypeId.ToString() : "";
                }
                studentProfileModel.EncFeeTypeId = EncFeeType_Id;

                return View(studentProfileModel);
            }
            catch (EntityException ex)
            {
                //reconnect database 
                _ICommonMethodService.GetSetCurrentSchoolDataBase(Request, Session, Server);
                return Redirect(this.Request.RawUrl);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }


       // public ActionResult Initiate(string EncStudentId = "", string PayableFee = "", string SessionId = "", string FeePeriodId = "",
       //string ReceiptTypeId = "", string LoggedInUserId = "", string UserType = "", string FeeType = "")
       // {
       //     SchoolUser user = new SchoolUser();
       //     _ICommonMethodService.GetSetCurrentSchoolDataBase(Request, Session, Server);
       //     if (HttpContext.Session["SchoolDB"] != null)
       //     {
       //         // re-assign session
       //         var sessiondb = HttpContext.Session["SchoolDB"];
       //         HttpContext.Session["SchoolDB"] = sessiondb;
       //     }
       //     try
       //     {
       //         var model = new StudentProfileModel();
       //         var SchoolDb = Convert.ToString(HttpContext.Session["SchoolDB"]);

       //         int Session_Id = 0;
       //         if (!string.IsNullOrEmpty(SessionId))
       //             Session_Id = Convert.ToInt32(SessionId);

       //         int FeePeriod_Id = 0;
       //         if (!string.IsNullOrEmpty(FeePeriodId))
       //             int.TryParse(_ICustomEncryption.base64d(FeePeriodId), out FeePeriod_Id);

       //         int Student_id = 0;
       //         if (!string.IsNullOrEmpty(EncStudentId))
       //             int.TryParse(_ICustomEncryption.base64d(EncStudentId), out Student_id);

       //         string EncFeeType_Id = "";
       //         if (!string.IsNullOrEmpty(FeeType))
       //         {
       //             //var feetype = db.FeeTypes.Where(x=>x.FeeType1 == FeeType).FirstOrDefault();
       //             var feetype = _IFeeService.GetAllFeeTypes(ref count, FeeType).FirstOrDefault();
       //             EncFeeType_Id = feetype != null ? feetype.FeeTypeId.ToString() : "";
       //         }

       //         string CustomerName = "";
       //         string BillingAddress = "";
       //         string MobileNo = "";
       //         var classname = "";
       //         if (Student_id > 0)
       //         {
       //             var SchoolDataInfo = _ISchoolDataService.GetSchoolDataDetail(ref count).FirstOrDefault();
       //             var StuInfo = _IStudentService.GetStudentById(Student_id);
       //             if (StuInfo != null)
       //             {
       //                 classname = _IStudentService.GetAllStudentClasss(ref count).Where(m => m.StudentId == StuInfo.StudentId && m.SessionId == Session_Id).FirstOrDefault() != null ? _IStudentService.GetAllStudentClasss(ref count).Where(m => m.StudentId == StuInfo.StudentId && m.SessionId == Session_Id).FirstOrDefault().ClassMaster.Class : "";
       //                 CustomerName = _ICommonMethodService.TrimStudentName(StuInfo, CustomerName);
       //                 var StuAddress = _IAddressService.GetAllAddresss(ref count, contactId: StuInfo.StudentId)
       //                                     .Where(x => x.ContactType.ContactType1.ToLower() == "student"
       //                                         && x.AddressType.AddressType1.ToLower() == "permanent").FirstOrDefault();
       //                 if (StuAddress == null)
       //                 {
       //                     StuAddress = _IAddressService.GetAllAddresss(ref count, contactId: StuInfo.StudentId)
       //                                        .Where(x => x.ContactType.ContactType1.ToLower() == "student"
       //                                            && x.AddressType.AddressType1.ToLower() == "correspondence").FirstOrDefault();
       //                 }
       //                 if (StuAddress == null)
       //                 {
       //                     if (SchoolDataInfo != null)
       //                     {
       //                         StuAddress = _IAddressService.GetAllAddresss(ref count)
       //                                             .Where(x => x.CityId == (int)SchoolDataInfo.CityId).Distinct()
       //                                             .FirstOrDefault();
       //                     }
       //                 }
       //                 BillingAddress = StuAddress != null ? StuAddress.AddressCity.City : "";
       //                 var ContactInfo = _IContactService.GetAllContactInfos(ref count)
       //                                     .Where(x => x.ContactId == Student_id &&
       //                                         x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
       //                                         && x.ContactType.ContactType1.ToLower() == "student"
       //                                         && x.IsDefault == true && x.Status == true).FirstOrDefault();
       //                 if (ContactInfo == null)
       //                 {
       //                     var getgaurdians = _IGuardianService.GetAllGuardians(ref count)
       //                                         .Where(x => x.StudentId == Student_id).FirstOrDefault();
       //                     if (getgaurdians != null)
       //                     {
       //                         ContactInfo = _IContactService.GetAllContactInfos(ref count)
       //                                         .Where(x => x.ContactId == getgaurdians.GuardianId &&
       //                                             x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
       //                                             && x.ContactType.ContactType1.ToLower() == "guardian"
       //                                             && x.IsDefault == true && x.Status == true).FirstOrDefault();
       //                     }
       //                 }
       //                 MobileNo = ContactInfo != null ? ContactInfo.ContactInfo1 : "";
       //             }

       //             string Email = "";
       //             if (SchoolDataInfo != null)
       //                 Email = SchoolDataInfo.SchoolEmail;


       //             var feeperiod = "";
       //             if (FeePeriod_Id > 0)
       //             {

       //                 feeperiod = _IFeeService.GetAllFeePeriods(ref count).Where(m => m.FeePeriodId == FeePeriod_Id) != null ? _IFeeService.GetAllFeePeriods(ref count).Where(m => m.FeePeriodId == FeePeriod_Id).FirstOrDefault().FeePeriodDescription : "";

       //             }
       //             List<StudentPaymentPGReuqest> StuPay = new List<StudentPaymentPGReuqest>();
       //             StuPay.Add(new StudentPaymentPGReuqest
       //             {
       //                 Amount = PayableFee,
       //                 CustomerName = CustomerName,
       //                 CustomerEmail = Email,
       //                 CustomerBillingAddress = BillingAddress,
       //                 CustomerAccount = MobileNo,
       //                 CustomerMobile = MobileNo,
       //                 Admnno = StuInfo.AdmnNo,
       //                 Class = classname,
       //                 FeePeriod = feeperiod
       //             });
       //             model.ErrorMessage = "";

       //             if (string.IsNullOrEmpty(PayableFee) || string.IsNullOrEmpty(CustomerName) || string.IsNullOrEmpty(Email)
       //                 || string.IsNullOrEmpty(BillingAddress) || string.IsNullOrEmpty(MobileNo))
       //             {
       //                 model.ErrorMessage = "Please Fill required values first:\n";
       //                 if (PayableFee == "")
       //                     model.ErrorMessage += "Payable Fee is not found \n";
       //                 if (CustomerName == "")
       //                     model.ErrorMessage += "Student Name is not found \n";
       //                 if (Email == "")
       //                     model.ErrorMessage += "School Email is not found \n";
       //                 if (BillingAddress == "")
       //                     model.ErrorMessage += "Student/School Address City is not found \n";
       //                 if (MobileNo == "")
       //                     model.ErrorMessage += "Student/Parent mobile number is not found \n";
       //             }
       //             if (model.ErrorMessage == "")
       //             {
       //                 if (StuPay.Count > 0)
       //                 {
       //                     int Request_Id = 0;
       //                     var json = JsonConvert.SerializeObject(StuPay);
       //                     PGRequest objmodel = new PGRequest();
       //                     objmodel.PGRequest1 = json.TrimStart('[').TrimEnd(']');
       //                     objmodel.RequestDatetime = DateTime.Now;
       //                     objmodel.StudentId = Student_id;
       //                     objmodel.FeePeriodId = FeePeriod_Id;
       //                     _IFeeService.InsertPGRequest(objmodel);

       //                     Request_Id = objmodel.PGRequestId;
       //                     model.PaymentRequestId = Request_Id;
       //                     model.TotalPayAmount = PayableFee;
       //                     model.UserType = UserType;
       //                     model.LoggedInUserId = LoggedInUserId;
       //                     model.EncFeeTypeId = EncFeeType_Id;
       //                 }

       //             }
       //         }
       //         return View(model);
       //     }
       //     catch (EntityException ex)
       //     {
       //         //reconnect database 
       //         _ICommonMethodService.GetSetCurrentSchoolDataBase(Request, Session, Server);
       //         return Redirect(this.Request.RawUrl);
       //     }
       //     catch (Exception ex)
       //     {
       //         _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
       //         return Json(new
       //         {
       //             status = "failed",
       //             data = ex.Message
       //         });
       //     }
       // }



        [AllowAnonymous]
        public ActionResult GetAllGateWay()
        {
            SchoolUser user = new SchoolUser();
            _ICommonMethodService.GetSetCurrentSchoolDataBase(Request, Session, Server);
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;
            }
            try
            {
                var DefaultImage = _ISchoolSettingService.GetorSetSchoolData("GateWayDefaultImage", "DefaultGatewayIcon.png", null,
                                                    "Default Image name for all gateways if there is image is not avaiable for anyone.")
                                                    .AttributeValue;

                var GateWaysList = new List<PaymentOnlineGateWay>();
                var getPaymentGatways = _IFeeService.GetAllPGSetups(ref count)
                                        .Where(x => x.IsActive == true).ToList();
                foreach (var pG in getPaymentGatways)
                {
                    string ImageUrl = "";
                    if (!string.IsNullOrEmpty(pG.PGImage))
                        ImageUrl = _WebHelper.GetStoreLocation() + "/PGImages/" + pG.PGImage;
                    else
                        ImageUrl = _WebHelper.GetStoreLocation() + "/PGImages/" + DefaultImage;

                    GateWaysList.Add(new PaymentOnlineGateWay()
                    {
                        IsDefault = (bool)pG.IsDefault,
                        PaymentGatewayId = _ICustomEncryption.base64e(pG.PGId.ToString()),
                        PaymentGatewayName = pG.PGName,
                        PaymentGatewayCode = pG.PGCode,
                        MinimumTransferAmt = (decimal)pG.MinimumTrsAmount,
                        PGImage = ImageUrl
                    });
                }
                return Json(new { status = "success", GateWaysList }, JsonRequestBehavior.AllowGet);
            }
            catch (EntityException ex)
            {
                //reconnect database 
                _ICommonMethodService.GetSetCurrentSchoolDataBase(Request, Session, Server);
                return Redirect(this.Request.RawUrl);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [AllowAnonymous]
        public ActionResult PaymentOnline(StudentProfileModel model)
        {
            SchoolUser user = new SchoolUser();
            _ICommonMethodService.GetSetCurrentSchoolDataBase(Request, Session, Server);
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;
            }
            try
            {
                int FeeReceipt_id = 0;
                if (!string.IsNullOrEmpty(model.EncFeeReceiptId))
                    int.TryParse(_ICustomEncryption.base64d(model.EncFeeReceiptId), out FeeReceipt_id);


                int Student_id = 0;
                if (!string.IsNullOrEmpty(model.EncStudentId))
                    int.TryParse(_ICustomEncryption.base64d(model.EncStudentId), out Student_id);

                int PgSetup_id = 0;
                if (!string.IsNullOrEmpty(model.PaymentGId))
                    int.TryParse(_ICustomEncryption.base64d(model.PaymentGId), out PgSetup_id);

                int PaymentRequest_Id = 0;
                if (model.PaymentRequestId != null)
                    PaymentRequest_Id = model.PaymentRequestId;
                
                string RedirectUrl = "";
                if (PgSetup_id > 0)
                {
                    var getRequest = _IFeeService.GetPGRequestById(PaymentRequest_Id);
                    var getPayMentGateway = _IFeeService.GetPGSetupById((int)PgSetup_id);
                    if (getPayMentGateway != null)
                    {
                        string Domain = Request.Url.AbsolutePath.Split('/')[1];
                        if (Domain.ToLower() == "payment")
                            Domain = Convert.ToString(WebConfigurationManager.AppSettings["User"]);

                        var PGModuleURL = getPayMentGateway.PGModuleURL.Replace("%domain%", Domain)
                                          .Replace("%request_id%", _ICustomEncryption.Encrypt_new(PaymentRequest_Id.ToString()));


                        var ReturnUrl = _WebHelper.GetStoreLocation() + "Payment/Complete?Id=" + model.EncStudentId
                                            + "&PgRequestId="
                                            + _ICustomEncryption.base64e(PaymentRequest_Id.ToString())
                                            + "&SessionId=" + model.SessionId
                                            + "&SelectedFeePeriodId=" + model.FeePeriodId
                                            + "&SelectedReceiptTypeId=" + model.ReceiptTypeId
                                            + "&SelectedFeeTypeId=" + model.EncFeeTypeId
                                            + "&PaymentGatewaySelectionId=" + model.PaymentGId
                                            + "&LoggedInUserId=" + model.LoggedInUserId
                                            + "&UserType=" + model.UserType;

                        if (getRequest != null && !string.IsNullOrEmpty(model.EncStudentId) && (PaymentRequest_Id>0)
                            && (model.SessionId>0) && !string.IsNullOrEmpty(model.FeePeriodId) && !string.IsNullOrEmpty(model.ReceiptTypeId)
                            && !string.IsNullOrEmpty(model.EncFeeTypeId) && !string.IsNullOrEmpty(model.PaymentGId) && !string.IsNullOrEmpty(model.LoggedInUserId)
                            && !string.IsNullOrEmpty(model.UserType))
                        {
                            getRequest.ResponseRedirectURL = ReturnUrl;
                            _IFeeService.UpdatePGRequest(getRequest);

                            RedirectUrl = PGModuleURL;
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "Insufficient",
                                data ="Insufficient Information. Please Contact Administrator for further details"
                            });
                        }
                    }
                    return Json(new
                    {
                        status = "success",
                        RedirectUrl
                    });
                }
                return Json(new
                {
                    status = "failed",
                });
            }
            catch (EntityException ex)
            {
                //reconnect database 
                _ICommonMethodService.GetSetCurrentSchoolDataBase(Request, Session, Server);
                return Redirect(this.Request.RawUrl);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        [AllowAnonymous]
        public ActionResult Complete(string Id = "", string PaymentGatewaySelectionId = "",
            string SessionId = "", string SelectedFeePeriodId = "", 
            string SelectedReceiptTypeId = "", string SelectedFeeTypeId = "", string PgRequestId = "",
            string LoggedInUserId = "", string UserType = "")
        {
            SchoolUser user = new SchoolUser();
            _ICommonMethodService.GetSetCurrentSchoolDataBase(Request, Session, Server);
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;
            }
            try
            {
                var EWSDetail = _ISchoolSettingService.GetorSetSchoolData("ShowEWSDiscount", "0", null).AttributeValue;
                var Istptownership = _ISchoolSettingService.GetorSetSchoolData("ApplyTptOwnerShipInReceipt", "1", null).AttributeValue;
                var IsSeparateTpt = _ISchoolSettingService.GetorSetSchoolData("SeparateTptFeeReceipt", "0", null).AttributeValue;

                var model = new StudentProfileModel();
                int Student_id = 0;
                if (!string.IsNullOrEmpty(Id))
                    int.TryParse(_ICustomEncryption.base64d(Id), out Student_id);

                int PgSetup_id = 0;
                if (!string.IsNullOrEmpty(PaymentGatewaySelectionId))
                    int.TryParse(_ICustomEncryption.base64d(PaymentGatewaySelectionId), out PgSetup_id);

                int Session_Id = 0;
                if (!string.IsNullOrEmpty(SessionId))
                    Session_Id = Convert.ToInt32(SessionId);

                int SelectedFeePeriod_Id = 0;
                if (!string.IsNullOrEmpty(SelectedFeePeriodId))
                    int.TryParse(_ICustomEncryption.base64d(SelectedFeePeriodId), out SelectedFeePeriod_Id);

                int Contacttype_Id = 0;
                if (!string.IsNullOrEmpty(UserType))
                {
                    var getUserType = _IAddressMasterService.GetAllContactTypes(ref count, UserType).FirstOrDefault();
                    Contacttype_Id = getUserType != null ? getUserType.ContactTypeId : 0;
                }

                int LoggedInUser_Id = 0;
                if (!string.IsNullOrEmpty(LoggedInUserId))
                    int.TryParse(_ICustomEncryption.base64d(LoggedInUserId), out LoggedInUser_Id);

                int SelectedReceiptType_Id = 0;
                if (!string.IsNullOrEmpty(SelectedReceiptTypeId))
                    SelectedReceiptType_Id = Convert.ToInt32(SelectedReceiptTypeId);

                int SelectedFeeType_Id = 0;
                if (!string.IsNullOrEmpty(SelectedFeeTypeId))
                    SelectedFeeType_Id = Convert.ToInt32(SelectedFeeTypeId);

                int PaymentRequest_Id = 0;
                if (!string.IsNullOrEmpty(PgRequestId))
                    int.TryParse(_ICustomEncryption.base64d(PgRequestId), out PaymentRequest_Id);

                #region After Payment
                if (PaymentRequest_Id > 0 && !string.IsNullOrEmpty(SelectedFeePeriodId) && !string.IsNullOrEmpty(SelectedReceiptTypeId)
                    && !string.IsNullOrEmpty(SelectedFeeTypeId))
                {
                    int FeePeriod_id = 0;
                    int FeeType_id = SelectedFeeType_Id;
                    int.TryParse(_ICustomEncryption.base64d(SelectedFeePeriodId), out FeePeriod_id);
                    int Select_ReceiptTypeId = Convert.ToInt32(SelectedReceiptTypeId);

                    Session_Id = Convert.ToInt32(SessionId);
                    var getsession = _ICatalogMasterService.GetSessionById(Session_Id);
                    var getResponsePayment = _IFeeService.GetPGRequestById(PaymentRequest_Id);
                    if (getResponsePayment != null)
                    {
                        var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                        model.IsPaymentSucceed = (bool)getResponsePayment.ResponseStatus;

                        //str is JSON string.
                        JavaScriptSerializer ser = new JavaScriptSerializer();
                        if (getResponsePayment.CustomSuccessResponse != "[]")
                        {
                            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(getResponsePayment.CustomSuccessResponse);
                            if ((bool)getResponsePayment.ResponseStatus)
                            {
                                //if succeed 
                                //get all details of fee receipt
                                var FeeReceiptDetailList = _IFeeService.GetFeeHeadCalculationsBySp(StudentId: Student_id,
                                                                SessionId: Session_Id, ReceiptDate: DateTime.Now.Date.ToString("yyyy-MM-dd"),
                                                                FeeTypeId: FeeType_id,
                                                                FeePeriodId: FeePeriod_id,
                                                                ReceiptTypeId: Select_ReceiptTypeId,
                                                                SessionStartDate: getsession.StartDate.Value.ToString("yyyy-MM-dd"),
                                                                SessionEndDate: getsession.EndDate.Value.ToString("yyyy-MM-dd"),
                                                                EWSDetail: EWSDetail,
                                                                IsApplyTptOwnerShip: Istptownership,
                                                                IsSeparateTptreceipt: IsSeparateTpt).ToList();

                                #region insert into feereceipt
                                var totalAmount = FeeReceiptDetailList.Where(f => f.GroupFeeHeadId != null
                                                                && (f.HeadType == "gh" ||
                                                                f.HeadType == "ao" || f.HeadType == "fp"))
                                                                .Select(f => f.CalculatedHeadAmount).Sum();
                                var concessionAmount = FeeReceiptDetailList.Where(f => f.GroupFeeHeadId != null
                                                        && f.HeadType == "cn").Select(f => f.CalculatedHeadAmount).Sum();
                                var PayableAmount = totalAmount - concessionAmount;

                                string fee_Period = "";
                                FeeReceipt FeeReceipt = new FeeReceipt();
                                if (FeePeriod_id != null)
                                {
                                    FeeReceipt.FeePeriodId = FeePeriod_id;
                                    var getFeePeriod = _IFeeService.GetFeePeriodById(FeePeriod_id);
                                    fee_Period = getFeePeriod != null ? getFeePeriod.FeePeriodDescription : "";
                                }
                                else
                                    FeeReceipt.FeePeriodId = null;
                                FeeReceipt.PaidAmount = PayableAmount;

                                var getMode = _IFeeService.GetAllPaymentModes(ref count)
                                                .Where(x => x.PaymentModeType.PaymentModeType1.ToLower() == "online transfer").FirstOrDefault();
                                FeeReceipt.PaymentModeId = getMode != null ? getMode.PaymentModeId : 0;
                                FeeReceipt.ReceiptDate = getResponsePayment.ResponseDatetime.Value.Date;
                                FeeReceipt.ReceiptNo = _ICommonMethodService.AutoGenerateReceiptNo((int)Select_ReceiptTypeId);
                                FeeReceipt.PendingAmount = 0;

                                string CLass = "";
                                var studentclass = _IStudentService.GetAllStudentClasss(ref count, SessionId: Session_Id,
                                                        StudentId: Student_id).OrderByDescending(x => x.StudentClassId).FirstOrDefault();
                                if (studentclass != null)
                                    CLass = studentclass.ClassMaster != null ? studentclass.ClassMaster.Class : "";

                                FeeReceipt.StdClass = CLass;
                                FeeReceipt.StudentId = Student_id;
                                FeeReceipt.ReceiptTypeId = Select_ReceiptTypeId;
                                FeeReceipt.SessionId = Session_Id;
                                FeeReceipt.Remarks = "";
                                FeeReceipt.PaymentDate = getResponsePayment.ResponseDatetime.Value.Date;

                                // get payment mode entity
                                var paymentmode = _IFeeService.GetPaymentModeById((int)FeeReceipt.PaymentModeId);
                                if (paymentmode != null)
                                {
                                    if (paymentmode.PaymentModeType.PaymentModeType1.ToLower() == "online transfer")
                                    {
                                        string bankname = "", TransId = "";
                                        if (dict.Count > 0)
                                        {
                                            foreach (var k in dict)
                                            {
                                                if (k.Key == "bank_transaction_id")
                                                    TransId = k.Value;

                                                if (k.Key == "bank_name")
                                                    bankname = k.Value;
                                            }
                                        }
                                        FeeReceipt.BankName = bankname;
                                        FeeReceipt.InstrumentNo = TransId;//transactonid
                                        FeeReceipt.InstrumentDate = getResponsePayment.ResponseDatetime.Value.Date;

                                        //fill model values 
                                        model.TransactionId = TransId;
                                        model.FeePeriod = fee_Period;
                                        model.TotalPayAmount = PayableAmount.ToString();


                                    }
                                }
                                FeeReceipt.UserId = LoggedInUser_Id;
                                FeeReceipt.ContactTypeId = Contacttype_Id;
                                FeeReceipt.GeneratedOn = DateTime.UtcNow;
                                _IFeeService.InsertFeeReceipt(FeeReceipt);

                                #region Feetrigger
                                var SMSDefaultSetting = _ICommonMethodService.GetOrSetSMSDefaultSetting("Fee", true, false, true, "SMN", "");
                                var feeperiod = _IFeeService.GetFeePeriodById((int)FeePeriod_id);
                                var students = _IStudentService.GetAllStudents(ref count).Where(x => x.StudentId == FeeReceipt.StudentId).ToList();

                                _ICommonMethodService.TiggerNotificationSMS(TriggerEventName: "FeeReceipt", FeePeriodDescription: feeperiod.FeePeriodDescription,
                                    PaidAmount: Convert.ToString(FeeReceipt.PaidAmount), StdClassReceiptNo:
                                    FeeReceipt.StdClass, Students: students, SendSMSMobileOptionId: SMSDefaultSetting.SelectedMobile,
                                    IsNonAPPUser: false, SMS: true, Notification: true);
                                #endregion

                                model.EncFeeReceipt = _ICustomEncryption.base64e(FeeReceipt.ReceiptId.ToString());
                                #endregion

                                #region insert feereceipt detail
                                // FeeReceipt table
                                var FeeReceiptDettable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "FeeReceiptDetail").FirstOrDefault();
                                foreach (var item in FeeReceiptDetailList)
                                {
                                    FeeReceiptDetail FeeReceiptDetail = new FeeReceiptDetail();
                                    FeeReceiptDetail.Amount = item.TotalHeadAmount;
                                    if (item.HeadType.ToLower() == "cn")
                                    {
                                        FeeReceiptDetail.ConsessionId = item.GroupFeeHeadId;
                                        //var fee
                                    }
                                    else if (item.HeadType.ToLower() == "ao")
                                    {
                                        FeeReceiptDetail.AddOnHeadId = item.GroupFeeHeadId;
                                    }
                                    else if (item.HeadType.ToLower() == "misc")
                                    {
                                        FeeReceiptDetail.FeeHeadId = item.GroupFeeHeadId;
                                    }
                                    else if (item.HeadType.ToString() == "dc")
                                    {
                                        FeeReceiptDetail.GroupFeeHeadId = item.GroupFeeHeadId;
                                        FeeReceiptDetail.ConsessionId = item.GroupFeeHeadId;
                                    }
                                    else if (item.HeadType.ToString() == "dcAmt")
                                    {
                                        FeeReceiptDetail.GroupFeeHeadId = item.GroupFeeHeadId;
                                        FeeReceiptDetail.FeeHeadId = item.GroupFeeHeadId;
                                    }
                                    else if (item.GroupFeeHeadId > 0)
                                    {
                                        FeeReceiptDetail.GroupFeeHeadId = item.GroupFeeHeadId;
                                    }
                                    FeeReceiptDetail.PrintCount = 0;
                                    FeeReceiptDetail.ReceiptId = FeeReceipt.ReceiptId;
                                    var headwise = _ISchoolSettingService.GetorSetSchoolData("HeadWisePaidAmount", "0", null).AttributeValue;
                                    bool IsHeadWise = false;
                                    if (headwise.ToString() == "1")
                                        IsHeadWise = true;
                                    // pending fee period 
                                    if (item.PendingFeePeriodId > 0)
                                    {
                                        FeeReceiptDetail.PendingFeePeriodId = item.PendingFeePeriodId;
                                    }
                                    // late fee period
                                    if (item.LateFeePeriodId > 0)
                                    {
                                        FeeReceiptDetail.GroupFeeHeadId = null;
                                        FeeReceiptDetail.LateFeePeriodId = item.LateFeePeriodId;
                                    }
                                    FeeReceiptDetail.FeePeriodId = item.FeeperiodId;

                                    if (item.TotalHeadAmount > 0)
                                    {
                                        FeeReceiptDetail.WaivedOffAmount = 0;
                                        FeeReceiptDetail.EWSDiscount = item.EWSDiscount;
                                        if (item.HeadType.ToLower() == "misc")
                                        {
                                            FeeReceiptDetail.PaidAmount = item.CalculatedHeadAmount;
                                        }
                                        else
                                        {
                                            FeeReceiptDetail.PaidAmount = item.CalculatedHeadAmount;
                                        }
                                        FeeReceiptDetail.FeeHeadConcession = item.FeeHeadConcession;
                                        if (item.HeadType.ToLower() == "fp")
                                        {
                                            var feependingreceipt = new FeePendingReceipt();
                                            feependingreceipt.FeePendingId = item.PendingFeePeriodId;
                                            feependingreceipt.PaidAmount = item.CalculatedHeadAmount;
                                            feependingreceipt.ReceiptId = FeeReceipt.ReceiptId;
                                            _IFeeService.InsertFeePendingReceipt(feependingreceipt);
                                        }
                                        _IFeeService.InsertFeeReceiptDetail(FeeReceiptDetail);
                                        if (FeeReceiptDettable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, FeeReceiptDettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(FeeReceiptDetail.ReceiptDetailId, FeeReceiptDettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add FeeReceiptDetail with ReceiptDetailId:{0} and ReceiptId:{1}", FeeReceiptDetail.ReceiptDetailId, FeeReceiptDetail.ReceiptId), Request.Browser.Browser);
                                        }
                                    }
                                }
                                #endregion

                                //thn delete from pgrequest and save it in fee online receipt.
                                FeeReceiptOnline onlinePayment = new FeeReceiptOnline();
                                onlinePayment.ReceiptId = FeeReceipt.ReceiptId;
                                onlinePayment.PGResponse = getResponsePayment.PGResponse;
                                onlinePayment.ReponseDatetime = getResponsePayment.ResponseDatetime;
                                onlinePayment.PGSetupId = PgSetup_id;
                                _IFeeService.InsertFeeReceiptOnline(onlinePayment);

                                _IFeeService.DeletePGRequest(PaymentRequest_Id);
                            }
                        }
                    }
                }
                #endregion
                return View(model);
            }
            catch (EntityException ex)
            {
                //reconnect database 
                _ICommonMethodService.GetSetCurrentSchoolDataBase(Request, Session, Server);
                return Redirect(this.Request.RawUrl);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        [AllowAnonymous]
        public ActionResult PrintFeeReceipt(string id)
        {
            SchoolUser user = new SchoolUser();
            _ICommonMethodService.GetSetCurrentSchoolDataBase(Request, Session, Server);
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;
                }
            
                // check id exist
                if (!string.IsNullOrEmpty(id))
                {
                    // decrypt Id
                    var model = _ICommonMethodService.PrintFeeReceipt(id);
                    return View(model);
                }
                return RedirectToAction("Page404", "Error");
            }
            catch (EntityException ex)
            {
                //reconnect database 
                _ICommonMethodService.GetSetCurrentSchoolDataBase(Request, Session, Server);
                return Redirect(this.Request.RawUrl);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
    }
}
