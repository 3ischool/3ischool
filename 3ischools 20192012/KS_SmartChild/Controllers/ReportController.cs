﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using BAL.BAL.Common;
using BAL.BAL.Reporting;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.ReportingModule;
using DAL.DAL.SettingService;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Reporting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.Controllers
{
    public class ReportController : Controller
    {
        #region fields

        public int count;
        private readonly IConnectionService _IConnectionService;
        private readonly IUserService _IUserService;
        private readonly ILogService _Logger;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IWebHelper _WebHelper;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IReportService _IReportService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly IDropDownService _IDropDownService;
        private readonly IReportingLogicService _IReportingLogicService;
        
        #endregion

        #region ctor

        public ReportController(IConnectionService IConnectionService,
            ILogService Logger,
            IUserService IUserService,
            ICatalogMasterService ICatalogMasterService,
            IWebHelper WebHelper,
            ISchoolSettingService ISchoolSettingService,
            IReportService IReportService,
            ICustomEncryption ICustomEncryption,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            IDropDownService IDropDownService,
            IReportingLogicService IReportingLogicService)
        {
            count = 0;
            this._IConnectionService = IConnectionService;
            this._IUserService = IUserService;
            this._Logger = Logger;
            this._ICatalogMasterService = ICatalogMasterService;
            this._WebHelper = WebHelper;
            this._ISchoolSettingService = ISchoolSettingService;
            this._IReportService = IReportService;
            this._ICustomEncryption = ICustomEncryption;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._IActivityLogService = IActivityLogService;
            this._IDropDownService = IDropDownService;
            this._IReportingLogicService = IReportingLogicService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }

        public string ConvertTime(TimeSpan time)
        {
            TimeSpan timespan = new TimeSpan(time.Hours, time.Minutes, time.Seconds);
            DateTime datetime = DateTime.Today.Add(timespan);
            string sdisplayTime = datetime.ToString("hh:mm tt");
            return sdisplayTime;
        }

        #endregion

        #region methods

        // Standard Report
        //List
        public List<StandardReportModel> preparerptlistmodel(List<StandardReportModel> model)
        {
            // get all report master
            var reports = _IReportService.GetAllRptMasters(ref count);
            foreach (var item in reports)
            {
                StandardReportModel StandardReportModel = new StandardReportModel();
                StandardReportModel.EncRptMasterId = _ICustomEncryption.base64e(item.RptMasterId.ToString());
                StandardReportModel.CreateDate = item.CreationDate.HasValue ? ConvertDate(item.CreationDate.Value) : "NA";
                StandardReportModel.Status = item.IsActive != null ? item.IsActive == true ? "Yes" : "No" : "No";
                StandardReportModel.RptGenericName = item.GenericName != null ? item.GenericName : "NA";
                StandardReportModel.RptMasterName = item.ReportName;
                StandardReportModel.IsStdViewReport = false;
                StandardReportModel.IsGenViewReport = false;
                if(item.RptGenericCols.Count > 0 )
                    StandardReportModel.IsGenViewReport = true;
                if (item.RptStandardCols.Count > 0)
                    StandardReportModel.IsStdViewReport = true;

                model.Add(StandardReportModel);
            }

            return model;
        }

        public ActionResult ReportList()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new List<StandardReportModel>();
                model = preparerptlistmodel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        // Add Standard Report
        public ActionResult SetStandardReports()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new StandardReportModel();
                model = prepareupdatestdrptmodel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public StandardReportModel checkstdreporterror(StandardReportModel model)
        {
            if (string.IsNullOrEmpty(model.RptMasterName))
                ModelState.AddModelError("RptMasterName", "Report name required");
            if (string.IsNullOrEmpty(model.RptStoredProcedureName))
                ModelState.AddModelError("RptStoredProcedureName", "Stored procedure name required");

            return model;
        }

        [HttpPost]
        public ActionResult SetStandardReports(StandardReportModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                model = checkstdreporterror(model);
                if (ModelState.IsValid)
                {
                    string message = "";
                    // Activity log type add , edit
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    // stdclass Id                    
                    var RptMastertable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "RptMaster").FirstOrDefault();
                    var RptLayouttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "RptLayout").FirstOrDefault();
                    var RptStandardColtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "RptStandardCol").FirstOrDefault();
                    var RptParametertable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "RptParameter").FirstOrDefault();

                    var RptMaster = new RptMaster();
                    var RptLayout = new RptLayout();
                    var RptStandardCol = new RptStandardCol();
                    var RptParameter = new RptParameter();

                    //desearlize array
                    // Filter Array
                    System.Web.Script.Serialization.JavaScriptSerializer Fltrjs = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<StandardReportParametersModel> RptFltrArray = Fltrjs.Deserialize<List<StandardReportParametersModel>>(model.RptFltrArray);
                    
                    // Filter Array
                    System.Web.Script.Serialization.JavaScriptSerializer Coljs = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<StandardReportColsModel> RptColArray = Coljs.Deserialize<List<StandardReportColsModel>>(model.RptColArray);

                    if (model.RptMasterId > 0) // edit mode
                    {
                        // get app form by id
                        RptMaster = _IReportService.GetRptMasterById((int)model.RptMasterId, true);
                        if (RptMaster != null)
                        {
                            RptMaster.ReportName = model.RptMasterName;
                            RptMaster.StoredProcedureName = model.RptStoredProcedureName;
                            _IReportService.UpdateRptMaster(RptMaster);
                            if (RptMastertable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptMastertable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(RptMaster.RptMasterId, RptMastertable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update RptMaster with RptMasterId:{0} and ReportName:{1}", RptMaster.RptMasterId, RptMaster.ReportName), Request.Browser.Browser);
                            }

                            // update layout
                            if (model.StandardReportLayout.ReportLayoutId > 0 || RptMaster.RptLayouts.Count > 0)
                            {
                                RptLayout = _IReportService.GetRptLayoutById((int)model.StandardReportLayout.ReportLayoutId);
                                if (RptLayout == null)
                                    RptLayout = RptMaster.RptLayouts.FirstOrDefault();

                                if (RptLayout != null)
                                {
                                    RptLayout.PageNoPositionId = model.StandardReportLayout.PageNoPostionId;
                                    RptLayout.IsOrgAddress = model.StandardReportLayout.IsOrgAddress;
                                    RptLayout.IsOrgLogo = model.StandardReportLayout.IsOrgLogo;
                                    RptLayout.IsOrgName = model.StandardReportLayout.IsOrgName;
                                    RptLayout.IsPageNo = model.StandardReportLayout.IsPageNo;
                                    RptLayout.IsPageTotal = model.StandardReportLayout.IsPageTotal;
                                    RptLayout.IsTotalOpening = model.StandardReportLayout.IsTotalOpening;
                                    _IReportService.UpdateRptLayout(RptLayout);
                                    if (RptMastertable != null)
                                    {
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptLayouttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(RptLayout.RptLayoutId, RptLayouttable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update RptLayout with RptLayoutId:{0} and RptMasterId:{1}", RptLayout.RptLayoutId, RptLayout.RptMasterId), Request.Browser.Browser);
                                    }
                                }
                            }
                            else
                            {
                                RptLayout.PageNoPositionId = model.StandardReportLayout.PageNoPostionId;
                                RptLayout.IsOrgAddress = model.StandardReportLayout.IsOrgAddress;
                                RptLayout.IsOrgLogo = model.StandardReportLayout.IsOrgLogo;
                                RptLayout.IsOrgName = model.StandardReportLayout.IsOrgName;
                                RptLayout.IsPageNo = model.StandardReportLayout.IsPageNo;
                                RptLayout.IsPageTotal = model.StandardReportLayout.IsPageTotal;
                                RptLayout.IsTotalOpening = model.StandardReportLayout.IsTotalOpening;
                                RptLayout.CurrentDate = DateTime.UtcNow;
                                RptLayout.CurrentTime = DateTime.UtcNow.TimeOfDay;
                                RptLayout.RptMasterId = RptMaster.RptMasterId;
                                _IReportService.InsertRptLayout(RptLayout);
                                if (RptMastertable != null)
                                {
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptLayouttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(RptLayout.RptLayoutId, RptLayouttable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add RptLayout with RptLayoutId:{0} and RptMasterId:{1}", RptLayout.RptLayoutId, RptLayout.RptMasterId), Request.Browser.Browser);
                                }
                            }                           


                            message = "Standard Report updated successfully";
                        }
                    }
                    else
                    {
                        RptMaster.ReportName = model.RptMasterName;
                        RptMaster.StoredProcedureName = model.RptStoredProcedureName;
                        _IReportService.InsertRptMaster(RptMaster);
                        if (RptMastertable != null)
                        {
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptMastertable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(RptMaster.RptMasterId, RptMastertable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add RptMaster with RptMasterId:{0} and ReportName:{1}", RptMaster.RptMasterId, RptMaster.ReportName), Request.Browser.Browser);
                        }

                        RptLayout.PageNoPositionId = model.StandardReportLayout.PageNoPostionId;
                        RptLayout.IsOrgAddress = model.StandardReportLayout.IsOrgAddress;
                        RptLayout.IsOrgLogo = model.StandardReportLayout.IsOrgLogo;
                        RptLayout.IsOrgName = model.StandardReportLayout.IsOrgName;
                        RptLayout.IsPageNo = model.StandardReportLayout.IsPageNo;
                        RptLayout.IsPageTotal = model.StandardReportLayout.IsPageTotal;
                        RptLayout.IsTotalOpening = model.StandardReportLayout.IsTotalOpening;
                        RptLayout.CurrentDate = DateTime.UtcNow;
                        RptLayout.CurrentTime = DateTime.UtcNow.TimeOfDay;
                        RptLayout.RptMasterId = RptMaster.RptMasterId;
                        _IReportService.InsertRptLayout(RptLayout);
                        if (RptMastertable != null)
                        {
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptLayouttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(RptLayout.RptLayoutId, RptLayouttable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add RptLayout with RptLayoutId:{0} and RptMasterId:{1}", RptLayout.RptLayoutId, RptLayout.RptMasterId), Request.Browser.Browser);
                        }

                        message = "Standard Report added successfully";
                    }

                    // Report Filter 
                    foreach (var item in RptFltrArray)
                    {
                        if (item.RptStandardColId > 0)
                        {
                            RptStandardCol = _IReportService.GetRptStandardColById((int)item.RptStandardColId, true);
                            RptStandardCol.ColLabel = item.FilterLabel;
                            RptStandardCol.ColIndex = item.FilterIndex;
                            RptStandardCol.IsFilter = item.IsFilter;
                            RptStandardCol.RptFilterControlTypeId = item.FilerControlTypeId;
                            _IReportService.UpdateRptStandardCol(RptStandardCol);
                            if (RptStandardColtable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptStandardColtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(RptStandardCol.RptStdColId, RptStandardColtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update RptStandardCol with RptStdColId:{0} and ColName:{1}", RptStandardCol.RptStdColId, RptStandardCol.ColName), Request.Browser.Browser);
                            }
                        }
                        else
                        {
                            RptParameter = new RptParameter();
                            RptParameter.RptParameterName = item.FilterName;
                            RptParameter.RptMasterId = RptMaster.RptMasterId;
                            _IReportService.InsertRptParameter(RptParameter);
                            if (RptParametertable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptParametertable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(RptParameter.RptParameterId, RptParametertable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add RptParameter with RptParameterId:{0} and RptParameterName:{1}", RptParameter.RptParameterId, RptParameter.RptParameterName), Request.Browser.Browser);
                            }

                            RptStandardCol = new RptStandardCol();
                            RptStandardCol.RptMasterId = RptMaster.RptMasterId;
                            RptStandardCol.ColName = item.FilterName;
                            RptStandardCol.ColLabel = item.FilterLabel;
                            RptStandardCol.ColIndex = item.FilterIndex;
                            RptStandardCol.IsFilter = item.IsFilter;
                            RptStandardCol.RptFilterControlTypeId = item.FilerControlTypeId;
                            _IReportService.InsertRptStandardCol(RptStandardCol);
                            if (RptStandardColtable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptStandardColtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(RptStandardCol.RptStdColId, RptStandardColtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add RptStandardCol with RptStdColId:{0} and ColName:{1}", RptStandardCol.RptStdColId, RptStandardCol.ColName), Request.Browser.Browser);
                            }
                        }
                    }

                    // Report Cols
                    foreach (var item in RptColArray)
                    {
                        if (item.RptStandardColId > 0)
                        {
                            RptStandardCol = _IReportService.GetRptStandardColById((int)item.RptStandardColId, true);
                            RptStandardCol.ColLabel = item.ColLabel;
                            RptStandardCol.ColIndex = item.ColIndex;
                            RptStandardCol.ColWidth = item.ColWidth;
                            RptStandardCol.IsVisible = item.IsVisible;
                            RptStandardCol.IsSorted = item.IsSorted;
                            RptStandardCol.SortingMethod = item.SortingMethod;
                            RptStandardCol.SortingIndex = item.SortingIndex;
                            RptStandardCol.IsGroup = item.IsGroup;
                            RptStandardCol.GroupIndex = item.GroupIndex;
                            RptStandardCol.IsSum = item.IsSum;
                            RptStandardCol.RefTableName = item.TableName;
                            RptStandardCol.ActualColName = item.ColumnName;
                            _IReportService.UpdateRptStandardCol(RptStandardCol);
                            if (RptStandardColtable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptStandardColtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(RptStandardCol.RptStdColId, RptStandardColtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update RptStandardCol with RptStdColId:{0} and ColName:{1}", RptStandardCol.RptStdColId, RptStandardCol.ColName), Request.Browser.Browser);
                            }
                        }
                        else
                        {
                            RptStandardCol = new RptStandardCol();
                            RptStandardCol.RptMasterId = RptMaster.RptMasterId;
                            RptStandardCol.ColName = item.ColName;
                            RptStandardCol.ColLabel = item.ColLabel;
                            RptStandardCol.ColIndex = item.ColIndex;
                            RptStandardCol.ColWidth = item.ColWidth;
                            RptStandardCol.IsVisible = item.IsVisible;
                            RptStandardCol.IsSorted = item.IsSorted;
                            RptStandardCol.SortingMethod = item.SortingMethod;
                            RptStandardCol.SortingIndex = item.SortingIndex;
                            RptStandardCol.IsGroup = item.IsGroup;
                            RptStandardCol.GroupIndex = item.GroupIndex;
                            RptStandardCol.IsSum = item.IsSum;
                            RptStandardCol.RefTableName = item.TableName;
                            RptStandardCol.ActualColName = item.ColumnName;
                            _IReportService.InsertRptStandardCol(RptStandardCol);
                            if (RptStandardColtable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptStandardColtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(RptStandardCol.RptStdColId, RptStandardColtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add RptStandardCol with RptStdColId:{0} and ColName:{1}", RptStandardCol.RptStdColId, RptStandardCol.ColName), Request.Browser.Browser);
                            }
                        }
                    }

                    TempData["SuccessNotification"] = message;
                    return RedirectToAction("EditStandardReports", new { Id = _ICustomEncryption.base64e(RptMaster.RptMasterId.ToString()) });
                }

                if (model.RptMasterId > 0)
                {
                    TempData["RptMaster_ViewData"] = model;
                    return RedirectToAction("EditStandardReports", new { Id = _ICustomEncryption.base64e(model.RptMasterId.ToString()) });
                }
                else
                {
                    model = prepareupdatestdrptmodel(model);
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        // Edit Standard Report
        public StandardReportModel prepareupdatestdrptmodel(StandardReportModel model)
        {
            // get report cols
            model.StandardReportLayout.AvailablePositionTypes = _IDropDownService.PagePositionList();
            return model;
        }

        public ActionResult EditStandardReports(string id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // check id exist
                if (!string.IsNullOrEmpty(id))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(id);
                    int rptmasterid = 0;
                    if (int.TryParse(qsid, out rptmasterid))
                    {
                        var model = new StandardReportModel();
                        var ReportMaster = _IReportService.GetRptMasterById(rptmasterid);
                        if (ReportMaster != null)
                        {
                            if (TempData["RptMaster_ViewData"] != null)
                                model = (StandardReportModel)TempData["RptMaster_ViewData"];
                            else
                            {
                                // map model
                                model.RptMasterId = ReportMaster.RptMasterId;
                                model.RptMasterName = ReportMaster.ReportName;
                                model.RptStoredProcedureName = ReportMaster.StoredProcedureName;

                                // report layout
                                var reportlayout = ReportMaster.RptLayouts.FirstOrDefault();
                                if (reportlayout != null)
                                {
                                    model.StandardReportLayout.ReportLayoutId = reportlayout.RptLayoutId;
                                    model.StandardReportLayout.PageNoPostionId = reportlayout.PageNoPositionId;
                                    model.StandardReportLayout.IsOrgAddress = reportlayout.IsOrgAddress != null ? (bool)reportlayout.IsOrgAddress : false;
                                    model.StandardReportLayout.IsOrgLogo = reportlayout.IsOrgLogo != null ? (bool)reportlayout.IsOrgLogo : false;
                                    model.StandardReportLayout.IsOrgName = reportlayout.IsOrgName != null ? (bool)reportlayout.IsOrgName : false;
                                    model.StandardReportLayout.IsPageNo = reportlayout.IsPageNo != null ? (bool)reportlayout.IsPageNo : false;
                                    model.StandardReportLayout.IsPageTotal = reportlayout.IsPageTotal != null ? (bool)reportlayout.IsPageTotal : false;
                                    model.StandardReportLayout.IsTotalOpening = reportlayout.IsTotalOpening != null ? (bool)reportlayout.IsTotalOpening : false;
                                }

                                // get report parameters
                                var reportparameters = _IReportService.GetAllRptParameters(ref count, ReportMaster.RptMasterId);
                                var parametersname = reportparameters.Select(r => r.RptParameterName).ToArray();

                                // check if cols not saved
                                var standardrptcols = _IReportService.GetAllRptStandardCols(ref count, ReportMaster.RptMasterId);
                                if (standardrptcols.Count > 0)
                                {
                                    foreach (var item in standardrptcols)
                                    {
                                        if (parametersname.Contains(item.ColName))
                                        {
                                            StandardReportParametersModel StandardReportParametersModel = new StandardReportParametersModel();
                                            StandardReportParametersModel.RptStandardColId = item.RptStdColId;
                                            StandardReportParametersModel.FilterName = item.ColName;
                                            StandardReportParametersModel.FilterLabel = item.ColLabel;
                                            StandardReportParametersModel.FilterIndex = item.ColIndex;
                                            StandardReportParametersModel.IsFilter = item.IsFilter != null ? (bool)item.IsFilter : false;
                                            StandardReportParametersModel.FilerControlTypeId = item.RptFilterControlTypeId;
                                            if (item.RptFilterControlTypeId > 0)
                                            {
                                                foreach (var selectlistitem in _IDropDownService.FilterControlList())
                                                {
                                                    if (!string.IsNullOrEmpty(selectlistitem.Value))
                                                    {
                                                        if (item.RptFilterControlTypeId == Convert.ToInt32(selectlistitem.Value))
                                                            StandardReportParametersModel.AvailableFilerControlType.Add(new SelectListItem { Selected = true, Text = selectlistitem.Text, Value = selectlistitem.Value });
                                                        else
                                                            StandardReportParametersModel.AvailableFilerControlType.Add(new SelectListItem { Selected = false, Text = selectlistitem.Text, Value = selectlistitem.Value });
                                                    }
                                                    else
                                                        StandardReportParametersModel.AvailableFilerControlType.Add(new SelectListItem { Selected = false, Text = selectlistitem.Text, Value = selectlistitem.Value });
                                                }
                                            }
                                            else
                                                StandardReportParametersModel.AvailableFilerControlType = _IDropDownService.FilterControlList();

                                            model.StandardReportParametersList.Add(StandardReportParametersModel);
                                        }
                                        else
                                        {
                                            StandardReportColsModel StandardReportColsModel = new StandardReportColsModel();
                                            StandardReportColsModel.RptStandardColId = item.RptStdColId;
                                            StandardReportColsModel.ColName = item.ColName;
                                            StandardReportColsModel.ColLabel = item.ColLabel;
                                            StandardReportColsModel.ColIndex = item.ColIndex;
                                            StandardReportColsModel.ColWidth = item.ColWidth;
                                            StandardReportColsModel.GroupIndex = item.GroupIndex;
                                            StandardReportColsModel.IsGroup = item.IsGroup != null ? (bool)item.IsGroup : false;
                                            StandardReportColsModel.IsSorted = item.IsSorted != null ? (bool)item.IsSorted : false;
                                            StandardReportColsModel.IsSum = item.IsSum != null ? (bool)item.IsSum : false;
                                            StandardReportColsModel.IsVisible = item.IsVisible != null ? (bool)item.IsVisible : false;
                                            StandardReportColsModel.SortingIndex = item.SortingIndex;
                                            StandardReportColsModel.SortingMethod = item.SortingMethod;
                                            if (!string.IsNullOrEmpty(item.SortingMethod))
                                            {
                                                foreach (var selectlistitem in _IDropDownService.SortingMethodList())
                                                {
                                                    if (item.SortingMethod == selectlistitem.Value)
                                                        StandardReportColsModel.AvailableSortingMethod.Add(new SelectListItem { Selected = true, Text = selectlistitem.Text, Value = selectlistitem.Value });
                                                    else
                                                        StandardReportColsModel.AvailableSortingMethod.Add(new SelectListItem { Selected = false, Text = selectlistitem.Text, Value = selectlistitem.Value });
                                                }
                                            }
                                            else
                                                StandardReportColsModel.AvailableSortingMethod = _IDropDownService.SortingMethodList();

                                            // table name
                                            StandardReportColsModel.TableName = item.RefTableName;
                                            if (!string.IsNullOrEmpty(item.RefTableName))
                                            {
                                                foreach (var selectlistitem in _IDropDownService.TableMasterNameList())
                                                {
                                                    if (item.RefTableName == selectlistitem.Value)
                                                        StandardReportColsModel.AvailableTables.Add(new SelectListItem { Selected = true, Text = selectlistitem.Text, Value = selectlistitem.Value });
                                                    else
                                                        StandardReportColsModel.AvailableTables.Add(new SelectListItem { Selected = false, Text = selectlistitem.Text, Value = selectlistitem.Value });
                                                }
                                            }
                                            else
                                                StandardReportColsModel.AvailableTables = _IDropDownService.TableMasterNameList();

                                            // cols
                                            // get table master by name
                                            StandardReportColsModel.ColumnName = item.ActualColName;
                                            var tablemaster = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: item.RefTableName).FirstOrDefault();
                                            if (tablemaster != null && !string.IsNullOrEmpty(item.RefTableName))
                                            {
                                                var tablecol = _IActivityLogMasterService.GetAllTableCols(ref count, TableMasterId: tablemaster.TableId).ToList();
                                                if (tablecol.Count > 0 && !string.IsNullOrEmpty(item.ActualColName))
                                                {
                                                    foreach (var selectlistitem in tablecol)
                                                    {
                                                        if (item.ActualColName == selectlistitem.ActualColName)
                                                            StandardReportColsModel.AvailableTableCols.Add(new SelectListItem { Selected = true, Text = selectlistitem.ActualColName, Value = selectlistitem.ActualColName });
                                                        else
                                                            StandardReportColsModel.AvailableTableCols.Add(new SelectListItem { Selected = false, Text = selectlistitem.ActualColName, Value = selectlistitem.ActualColName });
                                                    }
                                                }
                                                else
                                                    StandardReportColsModel.AvailableTableCols.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
                                            }
                                            else
                                                StandardReportColsModel.AvailableTableCols.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });

                                            model.StandardReportColsList.Add(StandardReportColsModel);

                                        }
                                    }
                                }
                                else
                                {
                                   // report filters
                                    // report cols
                                    List<StoreProcedureParameter> ParaList = new List<StoreProcedureParameter>();
                                    if (ReportMaster.ReportName.Contains("Admission"))
                                        ParaList = _IReportingLogicService.GetStoredProcedureParameterList(SPName: "Admission", ReportMasterId: ReportMaster.RptMasterId.ToString(), IsStdrpt: true, IsCol: true, IsPara: true);
                                    else if (ReportMaster.ReportName.Contains("Fee"))
                                        ParaList = _IReportingLogicService.GetStoredProcedureParameterList(SPName: "Fee", ReportMasterId: ReportMaster.RptMasterId.ToString(), IsStdrpt: true, IsCol: true, IsPara: true);

                                    var stdrptfilters = _IReportService.GetStandardReportCols(ReportMaster.StoredProcedureName, ParaList);
                                    for (int j = 0; j < stdrptfilters.Rows.Count; j++)
                                    {
                                        StandardReportParametersModel StandardReportParametersModel = new StandardReportParametersModel();
                                        StandardReportParametersModel.RptStandardColId = 0;
                                        StandardReportParametersModel.FilterName = Convert.ToString(stdrptfilters.Rows[j].ItemArray[0]).Replace("@", "");
                                        StandardReportParametersModel.FilterLabel = StandardReportParametersModel.FilterName;
                                        StandardReportParametersModel.FilterIndex = null;
                                        StandardReportParametersModel.IsFilter = false;
                                        StandardReportParametersModel.FilerControlTypeId = 0;
                                        StandardReportParametersModel.AvailableFilerControlType = _IDropDownService.FilterControlList();

                                        model.StandardReportParametersList.Add(StandardReportParametersModel);
                                    }

                                    if (ReportMaster.ReportName.Contains("Admission"))
                                        ParaList = _IReportingLogicService.GetStoredProcedureParameterList(SPName: "Admission", ReportMasterId: ReportMaster.RptMasterId.ToString(), IsStdrpt: true, IsCol: true, IsPara: false);
                                    else if (ReportMaster.ReportName.Contains("Fee"))
                                        ParaList = _IReportingLogicService.GetStoredProcedureParameterList(SPName: "Fee", ReportMasterId: ReportMaster.RptMasterId.ToString(), IsStdrpt: true, IsCol: true, IsPara: false);

                                    // report cols
                                    var stdrptcols = _IReportService.GetStandardReportCols(ReportMaster.StoredProcedureName, ParaList);
                                    for (int i = 0; i < stdrptcols.Rows.Count; i++)
                                    {
                                        if (i == 0) // id
                                            continue;

                                        StandardReportColsModel StandardReportColsModel = new StandardReportColsModel();
                                        StandardReportColsModel.ColName = Convert.ToString(stdrptcols.Rows[i].ItemArray[0]);
                                        StandardReportColsModel.ColLabel = StandardReportColsModel.ColName;
                                        StandardReportColsModel.ColIndex = null;
                                        StandardReportColsModel.ColWidth = null;
                                        StandardReportColsModel.GroupIndex = null;
                                        StandardReportColsModel.IsGroup = false;
                                        StandardReportColsModel.IsSorted = false;
                                        StandardReportColsModel.IsSum = false;
                                        StandardReportColsModel.IsVisible = false;
                                        StandardReportColsModel.RptStandardColId = 0;
                                        StandardReportColsModel.SortingIndex = null;
                                        StandardReportColsModel.SortingMethod = "1";
                                        StandardReportColsModel.AvailableSortingMethod = _IDropDownService.SortingMethodList();
                                        StandardReportColsModel.AvailableTables = _IDropDownService.TableMasterNameList();
                                        StandardReportColsModel.AvailableTableCols.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });

                                        model.StandardReportColsList.Add(StandardReportColsModel);
                                    }
                                }
                            }

                            model = prepareupdatestdrptmodel(model);
                            return View(model);
                        }
                    }
                }

                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult ViewStandardReport(string id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // check id exist
                if (!string.IsNullOrEmpty(id))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(id);
                    int rptmasterid = 0;
                    if (int.TryParse(qsid, out rptmasterid))
                    {
                        var model = new ViewReportModel();
                        model.ReportMasterId = id;

                        var ReportMaster = _IReportService.GetRptMasterById(rptmasterid);
                        if (ReportMaster != null)
                        {
                            var reportfiltercols = ReportMaster.RptStandardCols.Where(r => r.IsFilter == true).ToList();
                            if (reportfiltercols.Count > 0)
                            {
                                foreach (var item in reportfiltercols)
                                {
                                    var ReportControl = new ReportControlsModel();
                                    ReportControl.Label = item.ColLabel;
                                    ReportControl.ControlType = item.RptFilterControlType.RptFilterControlType1;
                                    ReportControl.ControlTypeDisplayName = item.RptFilterControlType.ControlDisplayName;
                                    if (item.RptFilterControlType.RptFilterControlType1 == "select")
                                    {
                                        switch (item.ColLabel)
                                        {
                                            case "Session":
                                                ReportControl.selectlist = _IDropDownService.SessionList();
                                                break;
                                            case "Standard":
                                                ReportControl.selectlist = _IDropDownService.StandardList();
                                                break;
                                            case "Status":
                                                ReportControl.selectlist = _IDropDownService.StudentStatusList();
                                                break;
                                            case "Type":
                                                ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = "Admitted", Value = "1" });
                                                ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = "Registered", Value = "2" });
                                                break;
                                        }
                                    }

                                    model.ReportControlsList.Add(ReportControl);
                                }
                            }

                            return View(model);
                        }
                    }
                }

                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost, ActionName("ViewStandardReport")]
        [FormValueRequired("submit")]
        public ActionResult ViewStandardReport(ViewReportModel model,FormCollection form)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                if (!string.IsNullOrEmpty(model.ReportMasterId))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(model.ReportMasterId);
                    int rptmasterid = 0;
                    if (int.TryParse(qsid, out rptmasterid))
                    {
                        var ReportMaster = _IReportService.GetRptMasterById(rptmasterid);
                        if (ReportMaster != null)
                        {
                            var reportfiltercols = ReportMaster.RptStandardCols.Where(r => r.IsFilter == true).ToList();

                            var paralist = new List<StoreProcedureParameter>();
                            if (ReportMaster.ReportName.Contains("Admission"))
                                paralist = _IReportingLogicService.GetStoredProcedureParameterList("Admission", ReportMaster.RptMasterId.ToString(), true, null, null, false, false);
                            if (ReportMaster.ReportName.Contains("Fee"))
                                paralist = _IReportingLogicService.GetStoredProcedureParameterList("Fee", ReportMaster.RptMasterId.ToString(), true, null, null, false, false);

                            foreach (var item in reportfiltercols)
                            {
                                // check filter
                                if (form[item.ColLabel] != null)
                                {
                                    int intvalue = 0;
                                    string strvalue = "";
                                    DateTime? date;
                                    TimeSpan? time;

                                    if (item.RptFilterControlType.ControlDisplayName.ToLower().Contains("drop down"))
                                    {
                                        int.TryParse(form[item.ColLabel], out intvalue);
                                        if (item.ColLabel == "Session" && intvalue == 0)
                                            intvalue = _ICatalogMasterService.GetCurrentSession().SessionId;

                                        paralist.Where(p => p.ParaName.ToLower() == item.ColLabel.ToLower()).Select(p => { p.Value = intvalue; return p; }).ToList();
                                    }
                                    else if (item.RptFilterControlType.ControlDisplayName.ToLower().Contains("text box"))
                                    {
                                        strvalue = Convert.ToString(form[item.ColLabel]);
                                        paralist.Where(p => p.ParaName.ToLower() == item.ColLabel.ToLower()).Select(p => { p.Value = strvalue; return p; }).ToList();
                                    }
                                    else if (item.RptFilterControlType.ControlDisplayName.ToLower().Contains("date"))
                                    {
                                        var strarray = Convert.ToString(form[item.ColLabel]).Split('/');
                                        date = Convert.ToDateTime(strarray[1] + "/" + strarray[0] + "/" + strarray[2]);
                                        paralist.Where(p => p.ParaName.ToLower() == item.ColLabel.ToLower()).Select(p => { p.Value = date; return p; }).ToList();
                                    }
                                    else if (item.RptFilterControlType.ControlDisplayName.ToLower().Contains("time"))
                                    {
                                        date = DateTime.ParseExact(form[item.ColLabel], "h:mm tt", CultureInfo.InvariantCulture);
                                        time = date.Value.TimeOfDay;
                                        paralist.Where(p => p.ParaName.ToLower() == item.ColLabel.ToLower()).Select(p => { p.Value = time; return p; }).ToList();
                                    }
                                }
                            }

                            // un filtered columns
                             var reportgridcols = ReportMaster.RptStandardCols.Where(r => r.IsFilter == null || r.IsFilter == false).ToList();

                            // call procedure
                            var griddata = _IReportService.GetStandardReportCols(ReportMaster.StoredProcedureName, paralist);
                            // get columns
                            for (int i = 0; i < griddata.Columns.Count; i++)
                            {
                                // check column is visible
                                var Gridcol = reportgridcols.Where(r => r.ColName == griddata.Columns[i].ColumnName).FirstOrDefault();

                                var ViewReportColumn = new ViewReportColumns();
                                ViewReportColumn.ColumName = griddata.Columns[i].ColumnName;
                                ViewReportColumn.Ishidden = true;
                                if (Gridcol != null)
                                {
                                    ViewReportColumn.ColumName = Gridcol.ColLabel;
                                    if((bool)Gridcol.IsVisible)
                                        ViewReportColumn.Ishidden = false;
                                    else
                                        ViewReportColumn.Ishidden = true;

                                    if ((bool)Gridcol.IsGroup)
                                        ViewReportColumn.IsGroupable = true;
                                    else
                                        ViewReportColumn.IsGroupable = false;

                                    if ((bool)Gridcol.IsSorted)
                                        ViewReportColumn.IsSortable = true;
                                    else
                                        ViewReportColumn.IsSortable = false;
                                }

                                model.ColumnList.Add(ViewReportColumn);
                            }
                            // get rows
                            for (int i = 0; i < griddata.Rows.Count; i++)
                            {
                                var row = new ViewReportRows();
                                for (int j = 0; j < griddata.Columns.Count; j++)
                                    row.ColValue.Add(griddata.Rows[i][j]);

                                model.RowList.Add(row);
                            }


                            // filters
                            foreach (var item in reportfiltercols)
                            {
                                var ReportControl = new ReportControlsModel();
                                ReportControl.Label = item.ColLabel;
                                ReportControl.ControlType = item.RptFilterControlType.RptFilterControlType1;
                                ReportControl.ControlTypeDisplayName = item.RptFilterControlType.ControlDisplayName;

                                int intvalue = 0;

                                if (item.RptFilterControlType.RptFilterControlType1 == "select")
                                {
                                    if (form[item.ColLabel] != null)
                                    {
                                        int.TryParse(form[item.ColLabel], out intvalue);
                                    }

                                    switch (item.ColLabel)
                                    {
                                        case "Session":
                                            foreach (var listitem in _IDropDownService.SessionList())
                                            {
                                                if (!string.IsNullOrEmpty(listitem.Value))
                                                {
                                                    if (Convert.ToInt32(listitem.Value) == intvalue)
                                                        ReportControl.selectlist.Add(new SelectListItem { Selected = true, Text = listitem.Text, Value = listitem.Value });
                                                    else
                                                        ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = listitem.Text, Value = listitem.Value });
                                                }
                                                else
                                                    ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = listitem.Text, Value = listitem.Value });
                                            }

                                            break;
                                        case "Standard":
                                            foreach (var listitem in _IDropDownService.StandardList())
                                            {
                                                if (!string.IsNullOrEmpty(listitem.Value))
                                                {
                                                    if (Convert.ToInt32(listitem.Value) == intvalue)
                                                        ReportControl.selectlist.Add(new SelectListItem { Selected = true, Text = listitem.Text, Value = listitem.Value });
                                                    else
                                                        ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = listitem.Text, Value = listitem.Value });
                                                }
                                                else
                                                    ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = listitem.Text, Value = listitem.Value });
                                            }

                                            break;
                                        case "Status":
                                            foreach (var listitem in _IDropDownService.StudentStatusList())
                                            {
                                                if (!string.IsNullOrEmpty(listitem.Value))
                                                {
                                                    if (Convert.ToInt32(listitem.Value) == intvalue)
                                                        ReportControl.selectlist.Add(new SelectListItem { Selected = true, Text = listitem.Text, Value = listitem.Value });
                                                    else
                                                        ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = listitem.Text, Value = listitem.Value });
                                                }
                                                else
                                                    ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = listitem.Text, Value = listitem.Value });
                                            }

                                            break;
                                        case "Type":
                                            if (intvalue == 1)
                                                ReportControl.selectlist.Add(new SelectListItem { Selected = true, Text = "Admitted", Value = "1" });
                                            else
                                                ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = "Admitted", Value = "1" });

                                            if (intvalue == 2)
                                                ReportControl.selectlist.Add(new SelectListItem { Selected = true, Text = "Registered", Value = "2" });
                                            else
                                                ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = "Registered", Value = "2" });

                                            break;
                                    }
                                }

                                model.ReportControlsList.Add(ReportControl);
                            }

                            // grid page settings
                            model.FileName = System.String.Format("KonnectSmart_{0}_{1}.xlsx", ReportMaster.ReportName, user.UserId);
                            model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("RptGridPageSizes");
                            model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("RptdefaultGridPageSize");

                            return View(model);
                        }
                    }
                }

                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        // Generic Reports
        // List
        public ActionResult GenericReportList()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new List<StandardReportModel>();
                model = preparerptlistmodel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public GenericReportModel checkgenericreporterror(GenericReportModel model)
        {
            if (string.IsNullOrEmpty(model.RptGenericName))
                ModelState.AddModelError("RptGenericName", "Name required");

            return model;
        }

        [HttpPost]
        public ActionResult SetGenericReports(GenericReportModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                model = checkgenericreporterror(model);
                if (ModelState.IsValid)
                {
                    string message = "";
                    // Activity log type add , edit
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    // stdclass Id                    
                    var RptMastertable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "RptMaster").FirstOrDefault();
                    var RptGenericColtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "RptGenericCol").FirstOrDefault();

                    var RptMaster = new RptMaster();
                    var RptGenericCol = new RptGenericCol();

                    //desearlize array
                    // Filter Array
                    System.Web.Script.Serialization.JavaScriptSerializer Fltrjs = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<GenericReportParametersModel> RptFltrArray = Fltrjs.Deserialize<List<GenericReportParametersModel>>(model.RptFltrArray);

                    // Filter Array
                    System.Web.Script.Serialization.JavaScriptSerializer Coljs = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<GenericReportColsModel> RptColArray = Coljs.Deserialize<List<GenericReportColsModel>>(model.RptColArray);

                    if (model.RptMasterId > 0) // edit mode
                    {
                        // get app form by id
                        RptMaster = _IReportService.GetRptMasterById((int)model.RptMasterId, true);
                        if (RptMaster != null)
                        {
                            RptMaster.GenericName = model.RptGenericName;
                            RptMaster.CreationDate = DateTime.UtcNow;
                            RptMaster.IsActive = model.IsActive;
                            RptMaster.ReportHeader = model.RptHeaderName;
                            _IReportService.UpdateRptMaster(RptMaster);
                            if (RptMastertable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptMastertable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(RptMaster.RptMasterId, RptMastertable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update RptMaster with RptMasterId:{0} and ReportName:{1}", RptMaster.RptMasterId, RptMaster.ReportName), Request.Browser.Browser);
                            }

                            message = "Generic Report updated successfully";
                        }
                    }

                    // Report Filter 
                    foreach (var item in RptFltrArray)
                    {
                        if (item.RptGenericColId > 0)
                        {
                            RptGenericCol = _IReportService.GetRptGenericColById((int)item.RptGenericColId, true);
                            RptGenericCol.ColLabel = item.FilterLabel;
                            RptGenericCol.ColIndex = item.FilterIndex;
                            RptGenericCol.IsFilter = item.IsFilter;
                            _IReportService.UpdateRptGenericCol(RptGenericCol);
                            if (RptGenericColtable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptGenericColtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(RptGenericCol.RptGenericColId, RptGenericColtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update RptGenericCol with RptGenericColId:{0} and ColName:{1}", RptGenericCol.RptGenericColId, RptGenericCol.ColName), Request.Browser.Browser);
                            }
                        }
                        else
                        {
                            RptGenericCol = new RptGenericCol();
                            RptGenericCol.ColName = item.FilterName;
                            RptGenericCol.ColLabel = item.FilterLabel;
                            RptGenericCol.ColIndex = item.FilterIndex;
                            RptGenericCol.IsFilter = item.IsFilter;
                            _IReportService.InsertRptGenericCol(RptGenericCol);
                            if (RptGenericColtable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptGenericColtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(RptGenericCol.RptGenericColId, RptGenericColtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add RptGenericCol with RptGenericColId:{0} and ColName:{1}", RptGenericCol.RptGenericColId, RptGenericCol.ColName), Request.Browser.Browser);
                            }
                        }
                    }

                    // Report Cols
                    foreach (var item in RptColArray)
                    {
                        if (item.RptGenericColId > 0)
                        {
                            RptGenericCol = _IReportService.GetRptGenericColById((int)item.RptGenericColId, true);
                            RptGenericCol.ColLabel = item.ColLabel;
                            RptGenericCol.ColIndex = item.ColIndex;
                            RptGenericCol.ColWidth = item.ColWidth;
                            RptGenericCol.IsVisible = item.IsVisible;
                            RptGenericCol.IsGroup = item.IsGroup;
                            RptGenericCol.GroupIndex = item.GroupIndex;
                            RptGenericCol.IsSum = item.IsSum;
                            _IReportService.UpdateRptGenericCol(RptGenericCol);
                            if (RptGenericColtable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptGenericColtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(RptGenericCol.RptGenericColId, RptGenericColtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update RptGenericCol with RptGenericColId:{0} and ColName:{1}", RptGenericCol.RptGenericColId, RptGenericCol.ColName), Request.Browser.Browser);
                            }
                        }
                        else
                        {
                            RptGenericCol = new RptGenericCol();
                            RptGenericCol.ColName = item.ColName;
                            RptGenericCol.ColLabel = item.ColLabel;
                            RptGenericCol.ColIndex = item.ColIndex;
                            RptGenericCol.ColWidth = item.ColWidth;
                            RptGenericCol.IsVisible = item.IsVisible;
                            RptGenericCol.IsGroup = item.IsGroup;
                            RptGenericCol.GroupIndex = item.GroupIndex;
                            RptGenericCol.IsSum = item.IsSum;
                            RptGenericCol.RptMasterId = RptMaster.RptMasterId;
                            _IReportService.InsertRptGenericCol(RptGenericCol);
                            if (RptGenericColtable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, RptGenericColtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(RptGenericCol.RptGenericColId, RptGenericColtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add RptGenericCol with RptGenericColId:{0} and ColName:{1}", RptGenericCol.RptGenericColId, RptGenericCol.ColName), Request.Browser.Browser);
                            }
                        }
                    }

                    TempData["SuccessNotification"] = message;
                    return RedirectToAction("EditGenericReports", new { Id = _ICustomEncryption.base64e(RptMaster.RptMasterId.ToString()) });
                }

                if (model.RptMasterId > 0)
                {
                    TempData["RptGenMaster_ViewData"] = model;
                    return RedirectToAction("EditGenericReports", new { Id = _ICustomEncryption.base64e(model.RptMasterId.ToString()) });
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult EditGenericReports(string id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // check id exist
                if (!string.IsNullOrEmpty(id))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(id);
                    int rptmasterid = 0;
                    if (int.TryParse(qsid, out rptmasterid))
                    {
                        var model = new GenericReportModel();
                        var ReportMaster = _IReportService.GetRptMasterById(rptmasterid);
                        if (ReportMaster != null)
                        {
                            if (TempData["RptGenMaster_ViewData"] != null)
                                model = (GenericReportModel)TempData["RptGenMaster_ViewData"];
                            else
                            {
                                // map model
                                model.RptMasterId = ReportMaster.RptMasterId;
                                model.RptMasterName = ReportMaster.ReportName;
                                model.RptGenericName = ReportMaster.GenericName;
                                model.RptHeaderName = ReportMaster.ReportHeader;
                                model.IsActive = ReportMaster.IsActive != null ? (bool)ReportMaster.IsActive : false;
                                model.CreateDate = ReportMaster.CreationDate.HasValue ? ConvertDate(ReportMaster.CreationDate.Value) : "";

                                // get report parameters
                                var reportparameters = _IReportService.GetAllRptParameters(ref count, ReportMaster.RptMasterId);
                                var parametersname = reportparameters.Select(r => r.RptParameterName).ToArray();

                                // check if cols not saved
                                var genericrptcols = _IReportService.GetAllRptGenericCols(ref count, ReportMaster.RptMasterId);
                                if (genericrptcols.Count > 0)
                                {
                                    foreach (var item in genericrptcols)
                                    {
                                        if (parametersname.Contains(item.ColName))
                                        {
                                            GenericReportParametersModel GenericReportParametersModel = new GenericReportParametersModel();
                                            GenericReportParametersModel.RptGenericColId = item.RptGenericColId;
                                            GenericReportParametersModel.FilterName = item.ColName;
                                            GenericReportParametersModel.FilterLabel = item.ColLabel;
                                            GenericReportParametersModel.FilterIndex = item.ColIndex;
                                            GenericReportParametersModel.IsFilter = item.IsFilter != null ? (bool)item.IsFilter : false;

                                            model.GenericReportParametersList.Add(GenericReportParametersModel);
                                        }
                                        else
                                        {
                                            GenericReportColsModel GenericReportColsModel = new GenericReportColsModel();
                                            GenericReportColsModel.RptGenericColId = item.RptGenericColId;
                                            GenericReportColsModel.ColName = item.ColName;
                                            GenericReportColsModel.ColLabel = item.ColLabel;
                                            GenericReportColsModel.ColIndex = item.ColIndex;
                                            GenericReportColsModel.ColWidth = item.ColWidth;
                                            GenericReportColsModel.GroupIndex = item.GroupIndex;
                                            GenericReportColsModel.IsGroup = item.IsGroup != null ? (bool)item.IsGroup : false;
                                            GenericReportColsModel.IsSum = item.IsSum != null ? (bool)item.IsSum : false;
                                            GenericReportColsModel.IsVisible = item.IsVisible != null ? (bool)item.IsVisible : false;

                                            model.GenericReportColsList.Add(GenericReportColsModel);
                                        }
                                    }
                                }
                                else
                                {
                                    // report filters
                                    // report cols
                                    List<StoreProcedureParameter> ParaList = new List<StoreProcedureParameter>();
                                    if (ReportMaster.ReportName.Contains("Admission"))
                                        ParaList = _IReportingLogicService.GetStoredProcedureParameterList(SPName: "Admission", ReportMasterId: ReportMaster.RptMasterId.ToString(), IsGenrpt: true, IsCol: true, IsPara: true);
                                    else if (ReportMaster.ReportName.Contains("Fee"))
                                        ParaList = _IReportingLogicService.GetStoredProcedureParameterList(SPName: "Fee", ReportMasterId: ReportMaster.RptMasterId.ToString(), IsGenrpt: true, IsCol: true, IsPara: true);

                                    var stdrptfilters = _IReportService.GetStandardReportCols(ReportMaster.StoredProcedureName, ParaList);
                                    for (int j = 0; j < stdrptfilters.Rows.Count; j++)
                                    {
                                        GenericReportParametersModel GenericReportParametersModel = new GenericReportParametersModel();
                                        GenericReportParametersModel.RptGenericColId = 0;
                                        GenericReportParametersModel.FilterName = Convert.ToString(stdrptfilters.Rows[j].ItemArray[0]).Replace("@", "");
                                        GenericReportParametersModel.FilterLabel = GenericReportParametersModel.FilterName;
                                        GenericReportParametersModel.FilterIndex = null;
                                        GenericReportParametersModel.IsFilter = false;

                                        model.GenericReportParametersList.Add(GenericReportParametersModel);
                                    }

                                    if (ReportMaster.ReportName.Contains("Admission"))
                                        ParaList = _IReportingLogicService.GetStoredProcedureParameterList(SPName: "Admission", ReportMasterId: ReportMaster.RptMasterId.ToString(), IsGenrpt: true, IsCol: true, IsPara: false);
                                    else if (ReportMaster.ReportName.Contains("Fee"))
                                        ParaList = _IReportingLogicService.GetStoredProcedureParameterList(SPName: "Fee", ReportMasterId: ReportMaster.RptMasterId.ToString(), IsGenrpt: true, IsCol: true, IsPara: false);

                                    // report cols
                                    var stdrptcols = _IReportService.GetStandardReportCols(ReportMaster.StoredProcedureName, ParaList);
                                    for (int i = 0; i < stdrptcols.Rows.Count; i++)
                                    {
                                        if (i == 0) // id
                                            continue;

                                        GenericReportColsModel GenericReportColsModel = new GenericReportColsModel();
                                        GenericReportColsModel.ColName = Convert.ToString(stdrptcols.Rows[i].ItemArray[0]);
                                        GenericReportColsModel.ColLabel = GenericReportColsModel.ColName;
                                        GenericReportColsModel.ColIndex = null;
                                        GenericReportColsModel.ColWidth = null;
                                        GenericReportColsModel.GroupIndex = null;
                                        GenericReportColsModel.IsGroup = false;
                                        GenericReportColsModel.IsSum = false;
                                        GenericReportColsModel.IsVisible = false;
                                        GenericReportColsModel.RptGenericColId = 0;

                                        model.GenericReportColsList.Add(GenericReportColsModel);
                                    }
                                }
                            }

                            return View(model);
                        }
                    }
                }

                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult ViewGenericReport(string id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // check id exist
                if (!string.IsNullOrEmpty(id))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(id);
                    int rptmasterid = 0;
                    if (int.TryParse(qsid, out rptmasterid))
                    {
                        var model = new ViewReportModel();
                        model.ReportMasterId = id;

                        var ReportMaster = _IReportService.GetRptMasterById(rptmasterid);
                        if (ReportMaster != null)
                        {
                            var reportfiltercols = ReportMaster.RptGenericCols.Where(r => r.IsFilter == true).ToList();
                            if (reportfiltercols.Count > 0)
                            {
                                foreach (var item in reportfiltercols)
                                {
                                    var ReportControl = new ReportControlsModel();
                                    ReportControl.Label = item.ColLabel;
                                    // get filter control type
                                    var filtercontroltype = _IReportService.GetAllRptStandardCols(ref count, RptMasterId: ReportMaster.RptMasterId, ColName: item.ColName).FirstOrDefault();
                                    if (filtercontroltype != null)
                                    {
                                        ReportControl.ControlType = filtercontroltype.RptFilterControlType.RptFilterControlType1;
                                        ReportControl.ControlTypeDisplayName = filtercontroltype.RptFilterControlType.ControlDisplayName;
                                        if (filtercontroltype.RptFilterControlType.RptFilterControlType1 == "select")
                                        {
                                            switch (item.ColLabel)
                                            {
                                                case "Session":
                                                    ReportControl.selectlist = _IDropDownService.SessionList();
                                                    break;
                                                case "Standard":
                                                    ReportControl.selectlist = _IDropDownService.StandardList();
                                                    break;
                                                case "Status":
                                                    ReportControl.selectlist = _IDropDownService.StudentStatusList();
                                                    break;
                                                case "Type":
                                                    ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = "Admitted", Value = "1" });
                                                    ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = "Registered", Value = "2" });
                                                    break;
                                            }
                                        }
                                    }

                                    model.ReportControlsList.Add(ReportControl);
                                }
                            }

                            return View(model);
                        }
                    }
                }

                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost, ActionName("ViewGenericReport")]
        [FormValueRequired("submit")]
        public ActionResult ViewGenericReport(ViewReportModel model, FormCollection form)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                if (!string.IsNullOrEmpty(model.ReportMasterId))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(model.ReportMasterId);
                    int rptmasterid = 0;
                    if (int.TryParse(qsid, out rptmasterid))
                    {
                        var ReportMaster = _IReportService.GetRptMasterById(rptmasterid);
                        if (ReportMaster != null)
                        {
                            var reportfiltercols = ReportMaster.RptGenericCols.Where(r => r.IsFilter == true).ToList();

                            var paralist = new List<StoreProcedureParameter>();
                            if (ReportMaster.ReportName.Contains("Admission"))
                                paralist = _IReportingLogicService.GetStoredProcedureParameterList("Admission", ReportMaster.RptMasterId.ToString(), null, true, null, false, false);
                            if (ReportMaster.ReportName.Contains("Fee"))
                                paralist = _IReportingLogicService.GetStoredProcedureParameterList("Fee", ReportMaster.RptMasterId.ToString(), null, true, null, false, false);

                            foreach (var item in reportfiltercols)
                            {
                                // get filter control type
                                var filtercontroltype = _IReportService.GetAllRptStandardCols(ref count, RptMasterId: ReportMaster.RptMasterId, ColName: item.ColName).FirstOrDefault();
                                if (filtercontroltype != null)
                                {
                                    // check filter
                                    if (form[item.ColLabel] != null)
                                    {
                                        int intvalue = 0;
                                        string strvalue = "";
                                        DateTime? date;
                                        TimeSpan? time;

                                        if (filtercontroltype.RptFilterControlType.ControlDisplayName.ToLower().Contains("drop down"))
                                        {
                                            int.TryParse(form[item.ColLabel], out intvalue);
                                            if (item.ColLabel == "Session" && intvalue == 0)
                                                intvalue = _ICatalogMasterService.GetCurrentSession().SessionId;

                                            paralist.Where(p => p.ParaName.ToLower() == item.ColLabel.ToLower()).Select(p => { p.Value = intvalue; return p; }).ToList();
                                        }
                                        else if (filtercontroltype.RptFilterControlType.ControlDisplayName.ToLower().Contains("text box"))
                                        {
                                            strvalue = Convert.ToString(form[item.ColLabel]);
                                            paralist.Where(p => p.ParaName.ToLower() == item.ColLabel.ToLower()).Select(p => { p.Value = strvalue; return p; }).ToList();
                                        }
                                        else if (filtercontroltype.RptFilterControlType.ControlDisplayName.ToLower().Contains("date"))
                                        {
                                            var strarray = Convert.ToString(form[item.ColLabel]).Split('/');
                                            date = Convert.ToDateTime(strarray[1] + "/" + strarray[0] + "/" + strarray[2]);
                                            paralist.Where(p => p.ParaName.ToLower() == item.ColLabel.ToLower()).Select(p => { p.Value = date; return p; }).ToList();
                                        }
                                        else if (filtercontroltype.RptFilterControlType.ControlDisplayName.ToLower().Contains("time"))
                                        {
                                            date = DateTime.ParseExact(form[item.ColLabel], "h:mm tt", CultureInfo.InvariantCulture);
                                            time = date.Value.TimeOfDay;
                                            paralist.Where(p => p.ParaName.ToLower() == item.ColLabel.ToLower()).Select(p => { p.Value = time; return p; }).ToList();
                                        }
                                    }
                                }
                            }

                            // un filtered columns
                            var reportgridcols = ReportMaster.RptGenericCols.Where(r => r.IsFilter == null || r.IsFilter == false).ToList();

                            // call procedure
                            var griddata = _IReportService.GetStandardReportCols(ReportMaster.StoredProcedureName, paralist);
                            // get columns
                            for (int i = 0; i < griddata.Columns.Count; i++)
                            {
                                // get std col for sorting
                                var stdcol = _IReportService.GetAllRptStandardCols(ref count, RptMasterId: rptmasterid, ColName: griddata.Columns[i].ColumnName).FirstOrDefault();
                                // check column is visible
                                var Gridcol = reportgridcols.Where(r => r.ColName == griddata.Columns[i].ColumnName).FirstOrDefault();

                                var ViewReportColumn = new ViewReportColumns();
                                ViewReportColumn.ColumName = griddata.Columns[i].ColumnName;
                                ViewReportColumn.Ishidden = true;
                                if (Gridcol != null)
                                {
                                    ViewReportColumn.ColumName = Gridcol.ColLabel;
                                    if ((bool)Gridcol.IsVisible)
                                        ViewReportColumn.Ishidden = false;
                                    else
                                        ViewReportColumn.Ishidden = true;

                                    if ((bool)Gridcol.IsGroup)
                                        ViewReportColumn.IsGroupable = true;
                                    else
                                        ViewReportColumn.IsGroupable = false;

                                    ViewReportColumn.IsSortable = false;
                                    if (stdcol != null)
                                    {
                                        if ((bool)stdcol.IsSorted)
                                            ViewReportColumn.IsSortable = true;
                                    }
                                }

                                model.ColumnList.Add(ViewReportColumn);
                            }
                            // get rows
                            for (int i = 0; i < griddata.Rows.Count; i++)
                            {
                                var row = new ViewReportRows();
                                for (int j = 0; j < griddata.Columns.Count; j++)
                                    row.ColValue.Add(griddata.Rows[i][j]);

                                model.RowList.Add(row);
                            }


                            // filters
                            foreach (var item in reportfiltercols)
                            {
                                // get filter control type
                                var filtercontroltype = _IReportService.GetAllRptStandardCols(ref count, RptMasterId: ReportMaster.RptMasterId, ColName: item.ColName).FirstOrDefault();
                                if (filtercontroltype != null)
                                {
                                    var ReportControl = new ReportControlsModel();
                                    ReportControl.Label = item.ColLabel;
                                    ReportControl.ControlType = filtercontroltype.RptFilterControlType.RptFilterControlType1;
                                    ReportControl.ControlTypeDisplayName = filtercontroltype.RptFilterControlType.ControlDisplayName;

                                    int intvalue = 0;

                                    if (filtercontroltype.RptFilterControlType.RptFilterControlType1 == "select")
                                    {
                                        if (form[item.ColLabel] != null)
                                        {
                                            int.TryParse(form[item.ColLabel], out intvalue);
                                        }

                                        switch (item.ColLabel)
                                        {
                                            case "Session":
                                                foreach (var listitem in _IDropDownService.SessionList())
                                                {
                                                    if (!string.IsNullOrEmpty(listitem.Value))
                                                    {
                                                        if (Convert.ToInt32(listitem.Value) == intvalue)
                                                            ReportControl.selectlist.Add(new SelectListItem { Selected = true, Text = listitem.Text, Value = listitem.Value });
                                                        else
                                                            ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = listitem.Text, Value = listitem.Value });
                                                    }
                                                    else
                                                        ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = listitem.Text, Value = listitem.Value });
                                                }

                                                break;
                                            case "Standard":
                                                foreach (var listitem in _IDropDownService.StandardList())
                                                {
                                                    if (!string.IsNullOrEmpty(listitem.Value))
                                                    {
                                                        if (Convert.ToInt32(listitem.Value) == intvalue)
                                                            ReportControl.selectlist.Add(new SelectListItem { Selected = true, Text = listitem.Text, Value = listitem.Value });
                                                        else
                                                            ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = listitem.Text, Value = listitem.Value });
                                                    }
                                                    else
                                                        ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = listitem.Text, Value = listitem.Value });
                                                }

                                                break;
                                            case "Status":
                                                foreach (var listitem in _IDropDownService.StudentStatusList())
                                                {
                                                    if (!string.IsNullOrEmpty(listitem.Value))
                                                    {
                                                        if (Convert.ToInt32(listitem.Value) == intvalue)
                                                            ReportControl.selectlist.Add(new SelectListItem { Selected = true, Text = listitem.Text, Value = listitem.Value });
                                                        else
                                                            ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = listitem.Text, Value = listitem.Value });
                                                    }
                                                    else
                                                        ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = listitem.Text, Value = listitem.Value });
                                                }

                                                break;
                                            case "Type":
                                                if (intvalue == 1)
                                                    ReportControl.selectlist.Add(new SelectListItem { Selected = true, Text = "Admitted", Value = "1" });
                                                else
                                                    ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = "Admitted", Value = "1" });

                                                if (intvalue == 2)
                                                    ReportControl.selectlist.Add(new SelectListItem { Selected = true, Text = "Registered", Value = "2" });
                                                else
                                                    ReportControl.selectlist.Add(new SelectListItem { Selected = false, Text = "Registered", Value = "2" });

                                                break;
                                        }
                                    }

                                    model.ReportControlsList.Add(ReportControl);
                                }
                            }

                            // grid page settings
                            model.FileName = System.String.Format("KonnectSmart_{0}_{1}.xlsx", ReportMaster.ReportName, user.UserId);
                            model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("RptGridPageSizes");
                            model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("RptdefaultGridPageSize");

                            return View(model);
                        }
                    }
                }

                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #region Ajax Methods

        public ActionResult GetTableCols(string Table)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                return Json(new
                {
                    status = "failed",
                    data = "Session Expire"
                });
            }

            try
            {
                IList<SelectListItem> tablecollist = new List<SelectListItem>();
                // get table master by name
                var tablemaster = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: Table).FirstOrDefault();
                if (tablemaster != null)
                {
                    var tablecol = _IActivityLogMasterService.GetAllTableCols(ref count, TableMasterId: tablemaster.TableId).ToList();
                    if (tablecol.Count > 0)
                    {
                        tablecollist.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
                        foreach (var selectlistitem in tablecol)
                            tablecollist.Add(new SelectListItem { Selected = true, Text = selectlistitem.ActualColName, Value = selectlistitem.ActualColName });

                        return Json(new
                        {
                            status = "success",
                            data = tablecollist
                        });
                    }
                }

                return Json(new
                {
                    status = "failed",
                    data = "No data found"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        #endregion

    }
}