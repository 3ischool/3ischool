﻿using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.Controllers
{
    public class AddressController : Controller
    {
        private int count = 0;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly ILogService _Logger;
        private readonly IWebHelper _WebHelper;

        public AddressController(IAddressMasterService IAddressMasterService,
             IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            IUserService IUserService,
            IConnectionService IConnectionService,
            ILogService Logger,
            IWebHelper WebHelper)
        {
            this._IAddressMasterService = IAddressMasterService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._IActivityLogService = IActivityLogService;
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._Logger = Logger;
            this._WebHelper = WebHelper;
        }

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        #endregion

        #region Save address via ajax call 

        // country
        public ActionResult SaveAddressCountry(string data)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            if (string.IsNullOrEmpty(data))
                return Json(new
                {
                    status = "failed",
                    msg = "Country should not be empty"
                });

            // check uniqueness for country
            var uniquecountry = _IAddressMasterService.GetAllAddressCountries(ref count, data);
            if (uniquecountry.Count > 0)
            {
                return Json(new
                {
                    status = "failed",
                    msg = "Country already exist"
                });
            }

            AddressCountry country = new AddressCountry();
            country.Country = data;
            _IAddressMasterService.InsertAddressCountry(country);

            var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
            // table Id
            var countrytable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AddressCountry").FirstOrDefault();
            if (countrytable != null)
            {
                // table log saved or not
                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, countrytable.TableId, DateTime.UtcNow, true).LastOrDefault();
                // save activity log
                if (tabledetail != null)
                    _IActivityLogService.SaveActivityLog(country.CountryId, countrytable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add new AddressCountry with CountryId:{0} and Country:{1}", country.CountryId, country.Country), Request.Browser.Browser);
            }

            return Json(new
            {
                status = "success",
                CountryId = country.CountryId
            });
        }

        // State
        public ActionResult SaveAddressState(string data)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            if (string.IsNullOrEmpty(data))
                return Json(new
                {
                    status = "failed",
                    msg = "State should not be empty"
                });

            var stateary = data.Split('ε');
            // check uniqueness for State
            var uniquestates = _IAddressMasterService.GetAllAddressStates(ref count, Convert.ToInt32(stateary[0]), stateary[1]);
            if (uniquestates.Count > 0)
            {
                return Json(new
                {
                    status = "failed",
                    msg = "State already exist"
                });
            }

            AddressState state = new AddressState();
            state.CountryId = Convert.ToInt32(stateary[0]);
            state.State = stateary[1];
            _IAddressMasterService.InsertAddressState(state);

            var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
            // table Id
            var statetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AddressState").FirstOrDefault();
            if (statetable != null)
            {
                // table log saved or not
                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, statetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                // save activity log
                if (tabledetail != null)
                    _IActivityLogService.SaveActivityLog(state.StateId, statetable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add new AddressState with StateId:{0} and State:{1}", state.StateId, state.State), Request.Browser.Browser);
            }

            return Json(new
            {
                status = "success",
                StateId = state.StateId
            });
        }

        // City
        public ActionResult SaveAddressCity(string data)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            if (string.IsNullOrEmpty(data))
                return Json(new
                {
                    status = "failed",
                    msg = "City should not be empty"
                });

            var cityary = data.Split('ε');
            // check uniqueness for State
            var uniquecities = _IAddressMasterService.GetAllAddressCitys(ref count, Convert.ToInt32(cityary[0]), cityary[1]);
            if (uniquecities.Count > 0)
            {
                return Json(new
                {
                    status = "failed",
                    msg = "City already exist"
                });
            }

            AddressCity city = new AddressCity();
            city.StateId = Convert.ToInt32(cityary[0]);
            city.City = cityary[1];
            _IAddressMasterService.InsertAddressCity(city);

            var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
            // table Id
            var citytable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AddressCity").FirstOrDefault();
            if (citytable != null)
            {
                // table log saved or not
                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, citytable.TableId, DateTime.UtcNow, true).LastOrDefault();
                // save activity log
                if (tabledetail != null)
                    _IActivityLogService.SaveActivityLog(city.CityId, citytable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add new AddressCity with CityId:{0} and City:{1}", city.CityId, city.City), Request.Browser.Browser);
            }

            return Json(new
            {
                status = "success",
                CityId = city.CityId
            });
        }

        #endregion

    }
}