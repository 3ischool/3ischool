﻿using BAL.BAL.Common;
using BAL.BAL.Message;
using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ContactModule;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.ExaminationModule;
using DAL.DAL.GuardianModule;
using DAL.DAL.Kendo;
using DAL.DAL.Schedular;
using DAL.DAL.SettingService;
using DAL.DAL.StaffModule;
using DAL.DAL.StudentModule;
using DAL.DAL.TriggerEventModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Schedular;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DAL.DAL.Message;

namespace KS_SmartChild.Controllers
{
    public class SchedularController : Controller
    {
        // GET: Schedular

        #region fields
        public readonly int maxFileSize = 1024;
        public int count = 0;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ILogService _Logger;
        private readonly IWebHelper _WebHelper;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly IStaffService _IStaffService;
        private readonly IStudentService _IStudentService;
        private readonly IGuardianService _IGuardianService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IContactService _IContactService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly ISchedularService _ISchedularService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IExamService _IExamService;
        private readonly ITriggerEventService _ITriggerEventService;
        private readonly IWorkflowMessageService _IWorkflowMessageService;
        private readonly ISMSSender _ISMSSender;
        private readonly IStudentMasterService _IStudentMasterService;
        private readonly ICommonMethodService _ICommonMethodService;

        #endregion

        #region ctor

        public SchedularController(IDropDownService IDropDownService,
            IActivityLogService IActivityLogService,
            IActivityLogMasterService IActivityLogMasterService,
            ILogService Logger,
            IWebHelper WebHelper,
            IUserService IUserService,
            IConnectionService IConnectionService,
            IAddressMasterService IAddressMasterService,
            IStaffService IStaffService,
            IStudentService IStudentService,
            IGuardianService IGuardianService,
            ICatalogMasterService ICatalogMasterService,
            IContactService IContactService,
            ISchoolSettingService ISchoolSettingService,
            ISchedularService ISchedularService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption,
            IExamService IExamService,
            ITriggerEventService ITriggerEventService,
            IWorkflowMessageService IWorkflowMessageService,
            ISMSSender ISMSSender, IStudentMasterService IStudentMasterService,
            ICommonMethodService ICommonMethodService
            )
        {
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._Logger = Logger;
            this._WebHelper = WebHelper;
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._IAddressMasterService = IAddressMasterService;
            this._IStaffService = IStaffService;
            this._IStudentService = IStudentService;
            this._IGuardianService = IGuardianService;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IContactService = IContactService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._ISchedularService = ISchedularService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._IExamService = IExamService;
            this._ITriggerEventService = ITriggerEventService;
            this._IWorkflowMessageService = IWorkflowMessageService;
            this._ISMSSender = ISMSSender;
            this._IStudentMasterService = IStudentMasterService;
            this._ICommonMethodService = ICommonMethodService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }

        public string ConvertTime(TimeSpan time, bool Is24HourFormat = false)
        {
            TimeSpan timespan = new TimeSpan(time.Hours, time.Minutes, time.Seconds);
            DateTime datetime = DateTime.Today.Add(timespan);
            string sdisplayTime = "";
            if (Is24HourFormat)
                sdisplayTime = datetime.ToString("HH:mm");
            else
                sdisplayTime = datetime.ToString("hh:mm tt");

            return sdisplayTime;
        }
        public string ConvertDateForKendo(DateTime Date)
        {
            string sdisplayTime = Date.ToString("MM/dd/yyyy");
            return sdisplayTime;
        }
        #endregion

        #region Methods

        #region Events

        public SchedularModel PrepareModel(SchedularModel model)
        {
            IList<SelectListItem> Dropdownlist = new List<SelectListItem>();
            IList<SelectListItem> EventTypeDropdownlist = new List<SelectListItem>();

            // create drop down for MsgPermissions
            model.AvailableEventTypeList = _IDropDownService.EventTypeList();
            //set datapickers limit
            var currentSession = _ICatalogMasterService.GetCurrentSession();
            model.SessionEndDate = (DateTime)currentSession.EndDate;
            model.SessionStartDate = (DateTime)currentSession.StartDate;
            //check if back dates allowed for entering the schedular activities
            model.IsBackDateAllowed = false;
            var settingvalue = _ISchoolSettingService.GetorSetSchoolData("AllowBackDateEntryOfEvents", "0").AttributeValue;
            if (settingvalue == "1")
                model.IsBackDateAllowed = true;
            // create drop down for MsgPermissions
            Dropdownlist = _IDropDownService.ClassList();
            Dropdownlist.RemoveAt(0);
            Dropdownlist.Insert(0, new SelectListItem { Selected = false, Text = "All Classes", Value = "1.1" });
            model.AvailableClassList = Dropdownlist;

            var contactTypes = _IAddressMasterService.GetAllContactTypes(ref count).Where(x => x.MsgGroupId!=null && x.MsgGroup.MsgGroup1.ToLower() != "admin");
            model.AvaiableContactTypes.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            model.AvaiableContactTypes.Add(new SelectListItem { Text = "All", Value = "All", Selected = false });
            foreach (var types in contactTypes)
            {
                model.AvaiableContactTypes.Add(new SelectListItem
                {
                    Text = types.ContactType1,
                    Value = types.ContactTypeId.ToString(),
                    Selected = false
                });
            }
            return model;
        }

        public ActionResult List(string u)
        {
            SchoolUser user = new SchoolUser();

            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "List", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");

            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                if (!string.IsNullOrEmpty(u))
                {
                    SchedularModel model = new SchedularModel();
                    PrepareModel(model);
                    model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "List", "Add Record");
                    model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "List", "Edit Record");
                    model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "List", "Delete Record");
                    model.IsAuthToViewImages = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "List", "Update Record");
                    model.IsAuthToDownloadDoc = true;

                    //kendo grid paging sizes
                    model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
                    model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");
                    var id = "";
                    id = _ICustomEncryption.base64d(u);
                    var EventByCode = _ISchedularService.GetAllEventTypeList(ref count, EventCode: id).FirstOrDefault();

                    if (EventByCode!=null){
                        model.EventTypeId = EventByCode.EventTypeId;
                        model.EventType = EventByCode.EventType1;
                    }
                    else
                    {
                        return RedirectToAction("Page404", "Error");
                    }
                    model.EventCode = u;
                    return View(model);
                }


                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult Event(string Id)
        {
            SchoolUser user = new SchoolUser();

            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Event", "View");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");

            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                if (!string.IsNullOrEmpty(Id))
                {
                    SchedularModel model = new SchedularModel();
                    PrepareModel(model);
                    // model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Event", "Add Record");
                    // model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Event", "Edit Record");
                    // model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Event", "Delete Record");
                    // model.IsAuthToViewImages = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Event", "View");
                    var id = "";
                    id = _ICustomEncryption.base64d(Id);

                    var EventByCode = _ISchedularService.GetAllEventTypeList(ref count, EventCode: id).FirstOrDefault();
                    if (EventByCode != null)
                    {
                        model.EventTypeId = EventByCode.EventTypeId;
                        model.EventTitle = EventByCode.EventType1;
                    }
                    else
                    {
                        return RedirectToAction("Page404", "Error");
                    }
                    model.EventCode = Id;
                    //if (Eventtypeid != null)
                    //{
                    //    model.EventTypeId = (int)Eventtypeid;
                    //    var EventType= _ISchedularService.GetEventTypeById((int)Eventtypeid);
                    //    if (EventType != null)
                    //        model.EventTitle = EventType.EventType1;
                    //}
                    model.AvailableSendMessageMobileNoOptions.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
                    model.AvailableSendMessageMobileNoOptions.Add(new SelectListItem { Text = "Student SMS Mobile No.", Value = "SMS", Selected = false });
                    model.AvailableSendMessageMobileNoOptions.Add(new SelectListItem { Text = "Student Mobile No.", Value = "SMN", Selected = false });
                    model.AvailableSendMessageMobileNoOptions.Add(new SelectListItem { Text = "Guardian Mobile No.", Value = "GMN", Selected = false });
                    model.SMSSelectionStudentMobileNo = _ISchoolSettingService.GetorSetSchoolData("EventSmsMobileNoSelectionForStudent", "SMN", null,
                                                "SMS : Student SMS Mobile No , SMN: Student mobile no. , GMN : Guardian Mobile No")
                                                .AttributeValue;

                    var StuDefaultSettings = _ICommonMethodService.GetOrSetSMSDefaultSetting("EventStudent", true, false, true, "SMN", "");
                    model.StuSMS = (bool)StuDefaultSettings.SMS;
                    model.StuNotification = (bool)StuDefaultSettings.Notification;
                    model.StuNonAppUser = (bool)StuDefaultSettings.ToNonAppUser;

                    var StaffDefaultSettings = _ICommonMethodService.GetOrSetSMSDefaultSetting("EventStaff", true, false, true, "StaffMobileNumber", "");
                    model.SMS = (bool)StaffDefaultSettings.SMS;
                    model.Notification = (bool)StaffDefaultSettings.Notification;
                    model.NonAppUser = (bool)StaffDefaultSettings.ToNonAppUser;

                    var GuardDefaultSettings = _ICommonMethodService.GetOrSetSMSDefaultSetting("EventGuardian", true, false, true, "GuardianMobileNumber", "");
                    model.GuardSMS = (bool)GuardDefaultSettings.SMS;
                    model.GuardNotification = (bool)GuardDefaultSettings.Notification;
                    model.GuardNonAppUser = (bool)GuardDefaultSettings.ToNonAppUser;

                    if (user.ContactType.ContactType1.ToLower() == "admin")
                    {
                        string Role = "";
                        Role = _ICommonMethodService.GetUserSuperRole(Role, user);
                        if (Role.ToLower() == "admin")
                        {
                            model.IsAdmin = true;
                        }
                    }

                    return View(model);
                }


                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult EditEvent(string i, string si,string Id)
        {
            SchoolUser user = new SchoolUser();

            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);


            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                var event_id = 0;
                var qsid = "0";
                var id = "";
                id = _ICustomEncryption.base64d(Id);

                if (!string.IsNullOrEmpty(i))
                {
                    int.TryParse(_ICustomEncryption.base64d(i), out event_id);
                }
                // check id exist
                if (!string.IsNullOrEmpty(si))
                {
                    // decrypt Id
                    qsid = _ICustomEncryption.base64d(si);
                }
                int Eventtypeid = 0;
                if (int.TryParse(qsid, out Eventtypeid))
                {
                    var eventdetails = _ISchedularService.GetAllEventDetailList(ref count, EventId: event_id);
                    var isallschool = false;
                    if (eventdetails.Count == 0)
                        isallschool = (bool)_ISchedularService.GetEventById(EventId: event_id).IsAllSchool;

                    SchedularModel model = new SchedularModel();
                    model.EventCode = Id;
                    PrepareModel(model);
                    //model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Event", "Add Record");
                    // model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Event", "Edit Record");
                    if (si != null)
                        model.EventTypeId = (int)Eventtypeid;
                    var CurrentEvent = _ISchedularService.GetEventById(event_id);
                    model.EncEventId = i;
                    model.EventTypeId = (int)CurrentEvent.EventTypeId;
                    model.EventTitle = CurrentEvent.EventTitle;
                    if (CurrentEvent.StartDate.HasValue)
                        model.StartDate = ConvertDate((DateTime)CurrentEvent.StartDate);
                    if (CurrentEvent.StartTime.HasValue)
                        model.StartTime = ConvertTime(CurrentEvent.StartTime.Value, false);
                    if (CurrentEvent.EndDate.HasValue)
                        model.EndDate = ConvertDate((DateTime)CurrentEvent.EndDate);
                    if (CurrentEvent.EndTime.HasValue)
                        model.EndTime = ConvertTime(CurrentEvent.EndTime.Value, false);
                    //model.ClassId = Convert.ToString(CurrentEvent.ClassId);
                    model.Description = CurrentEvent.Description;
                    model.Venue = CurrentEvent.Venue;
                    model.IsActive = (bool)CurrentEvent.IsActive;


                    var Eventype = _ISchedularService.GetEventTypeById((int)CurrentEvent.EventTypeId).EventType1;
                    model.EventHeader = Eventype;
                    if (Eventype.ToLower() == "circular")
                    {
                        if (isallschool)
                            model.AllowedTo = "All";
                        else
                            model.AllowedTo = CurrentEvent.AddressedTo.ToString();

                        var document = CurrentEvent.EventCircularDocs.Where(x => x.EventId == event_id).FirstOrDefault();
                        if (document != null)
                        {
                            model.CircularDocCaption = document.DocumentCaption;
                            model.CircularDocFileName = document.FileName;
                        }
                    }

                    //set datapickers limit
                    var currentSession = _ICatalogMasterService.GetCurrentSession();
                    model.SessionEndDate = (DateTime)currentSession.EndDate;

                    // create drop down for MsgPermissions
                    List<SelectListItem> Dropdownlist = new List<SelectListItem>();
                    var classlist = _ICatalogMasterService.GetAllClassMasters(ref count);
                    if (eventdetails.Count > 0)
                    {
                        var classidlist = eventdetails.Select(x => (int)x.ClassId);

                        foreach (var cil in eventdetails)
                        {
                            if (cil.ClassId == null)
                                Dropdownlist.Add(new SelectListItem { Selected = true, Text = "All Classes", Value = "1.1", Disabled = false });
                        }
                        if (Dropdownlist.Count == 0)
                            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "All Classes", Value = "1.1", Disabled = false });

                        foreach (var item in classlist)
                        {
                            if (eventdetails.Where(m => m.ClassId == item.ClassId).Count() > 0)
                                Dropdownlist.Add(new SelectListItem { Selected = true, Text = item.Class, Value = item.ClassId.ToString(), Disabled = false });
                            else if (eventdetails.Where(m => m.ClassId == null).Count() > 0)
                                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Class, Value = item.ClassId.ToString(), Disabled = true });
                            else
                                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Class, Value = item.ClassId.ToString(), Disabled = false });
                        }
                    }
                    else if (isallschool)
                    {
                        Dropdownlist.Add(new SelectListItem { Selected = true, Text = "All Classes", Value = "1.1", Disabled = false });
                        foreach (var item in classlist)
                            Dropdownlist.Add(new SelectListItem { Selected = true, Text = item.Class, Value = item.ClassId.ToString(), Disabled = false });
                    }
                    else
                    {
                        Dropdownlist.Add(new SelectListItem { Selected = false, Text = "All Classes", Value = "1.1", Disabled = false });
                        foreach (var item in classlist)
                            Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Class, Value = item.ClassId.ToString(), Disabled = false });
                    }
                    model.AvailableClassList = Dropdownlist;
                    return View(model);
                }

                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult SelectSchedularList(DataSourceRequest command, SchedularModel SchedularModel, bool Inactive = false, IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            int ttlcount = 0;
            var eventslist = _ISchedularService.GetAllEventList(ref ttlcount, EventTypeId: SchedularModel.EventTypeId,
                                     EventTitle: SchedularModel.EventTitle, Description: SchedularModel.Description,
                                     StartDate: SchedularModel.FilterStartDate, EndDate: SchedularModel.FilterEndDate)
                                .OrderByDescending(p => p.StartDate).ThenByDescending(p => p.EndDate).ToList();


            var evntList = eventslist.AsQueryable().Sort(sort).PagedForCommand(command).ToList().Select(x =>
                {
                    var SchedularModellist = new SchedularModel();
                    StringBuilder classnamelist = new StringBuilder();
                    //SchedularModellist.EventId = x.EventId;
                    SchedularModellist.IsStartDatePasses = true;
                    SchedularModellist.EncEventId = _ICustomEncryption.base64e(x.EventId.ToString());
                    if (x.EventTitle.Length > 20)
                    {
                        var concatinatedEventTitle = x.EventTitle.Substring(0, 20);
                        SchedularModellist.EventTitle = concatinatedEventTitle + "...";
                        SchedularModellist.fullEventTitle = x.EventTitle;
                    }
                    else
                        SchedularModellist.EventTitle = x.EventTitle;
                    var eventdetails = _ISchedularService.GetAllEventDetailList(ref count, EventId: x.EventId);
                    foreach (var ed in eventdetails)
                    {
                        if (classnamelist.Length > 0)
                            classnamelist.Append(" , ");

                        if (ed.ClassId != null)
                            classnamelist.Append(_ICatalogMasterService.GetClassMasterById((int)ed.ClassId).Class);
                        else
                            classnamelist.Append("All Classes");
                    }
                    var classnames = classnamelist.ToString();
                    if (classnames.Length > 12)
                    {
                        var concatinatedClassName = classnames.Substring(0, 12);
                        SchedularModellist.ClassName = concatinatedClassName + "...";
                        SchedularModellist.fullClassName = classnames;
                    }
                    else
                        SchedularModellist.ClassName = classnames;
                    //if (x.ClassId != null && x.ClassId != 0)
                    //    SchedularModellist.ClassName = _ICatalogMasterService.GetClassMasterById((int)x.ClassId).Class;
                    if (x.IsAllSchool == true)
                        SchedularModellist.ClassName = "All Classes";
                    if (x.Description.Length > 40)
                    {
                        var concatinateddescription = x.Description.Substring(0, 40);
                        SchedularModellist.Description = concatinateddescription + "...";
                        SchedularModellist.fullDescription = x.Description;
                    }
                    else
                        SchedularModellist.Description = x.Description;
                    if (x.StartDate.HasValue)
                    {
                        SchedularModellist.StartDate = ConvertDate(x.StartDate.Value);
                        if (x.StartDate.Value.Date < DateTime.Now.Date)
                        {
                            SchedularModellist.IsStartDatePasses = false;
                        }
                    }
                    if (x.EndDate.HasValue)
                    {
                        SchedularModellist.EndDate = ConvertDate(x.EndDate.Value);
                    }
                    if (x.StartTime.HasValue)
                    {
                        var curutctime = new DateTime(x.StartTime.Value.Ticks);
                        DateTime locdttime = curutctime.ToLocalTime();
                        string localtime = locdttime.TimeOfDay.ToString();
                        SchedularModellist.StartTime = ConvertTime(locdttime.TimeOfDay);

                    }
                    if (x.EndTime != null)
                    {
                        var curutctime = new DateTime(x.EndTime.Value.Ticks);
                        DateTime locdttime = curutctime.ToLocalTime();
                        string localtime = locdttime.TimeOfDay.ToString();
                        SchedularModellist.EndTime = ConvertTime(locdttime.TimeOfDay);
                    }

                    var activeinactive = (bool)(x.IsActive);
                    if (activeinactive)
                        SchedularModellist.isActivekendo = "Active";
                    else
                        SchedularModellist.isActivekendo = "InActive";

                    if (SchedularModel.EventTypeId != null)
                    {
                        SchedularModellist.EventTypeId = SchedularModel.EventTypeId;
                        SchedularModellist.EncEventTypeId = _ICustomEncryption.base64e(SchedularModel.EventTypeId.ToString());
                    }
                    SchedularModellist.EventCode = _ICustomEncryption.base64e(x.EventType.EventCode.ToString());
                    SchedularModellist.HasCircularDocs = false;
                    var documents = _ISchedularService.GetAllEventCircularDocs(ref count, EventId: x.EventId).ToList();
                    if (documents.Count() > 0)
                        SchedularModellist.HasCircularDocs = true;

                    SchedularModellist.AllowedTo = "";
                    if (x.EventType.EventType1.ToLower() == "circular")
                    {
                        if (x.AddressedTo != null)
                        {
                            var contactType = _IAddressMasterService.GetContactTypeById((int)x.AddressedTo);
                            SchedularModellist.AllowedTo = contactType != null ? contactType.ContactType1 : "";
                        }
                        else
                        {
                            if ((bool)x.IsAllSchool)
                                SchedularModellist.AllowedTo = "All";
                        }
                    }

                    return SchedularModellist;
                });
            var gridModel = new DataSourceResult
            {
                Data = evntList.ToList(),
                Total = evntList.Count()
            };
            return Json(gridModel);
        }

        public string checkvalidationerrorAddUpdateEvent(SchedularModel model, out List<EventDetailModel> deserializedclassList)
        {
            StringBuilder validation = new StringBuilder();
            List<EventDetailModel> deserializedclass = new List<EventDetailModel>();
            // check id exist
            if (!string.IsNullOrEmpty(model.EncEventId))
            {
                int event_id = 0;
                int.TryParse(_ICustomEncryption.base64d(model.EncEventId), out event_id);
                model.EventId = event_id;
            }
            var eventtype = "";
            if (model.EventTypeId > 0)
                eventtype = _ISchedularService.GetEventTypeById(model.EventTypeId).EventType1;

            var mandatoryfields = _ISchedularService.GetAllEventColsList(ref count, EventTypeId: model.EventTypeId, IsMandatory: true).ToList();
            foreach (var mf in mandatoryfields)
            {
                switch (mf.ColName)
                {
                    case "EventTitle":
                        if (string.IsNullOrEmpty(model.EventTitle))
                            validation.Append("Title is required ε");
                        break;
                    case "ClassId":
                        deserializedclass = (List<EventDetailModel>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.ClassId, typeof(List<EventDetailModel>));
                        if (deserializedclass.Count == 0)
                            validation.Append("Class is required ε");
                        break;
                    case "StartDate":
                        if (string.IsNullOrEmpty(model.StartDate))
                        {
                            if (mandatoryfields.Where(m => m.ColName == "EndDate").ToList().Count == 0)
                                validation.Append("Date is requiredε");
                            else
                                validation.Append("Start Date is required ε");
                        }
                        break;
                    case "StartTime":
                        if (string.IsNullOrEmpty(model.StartTime))
                            validation.Append("Start Time is required ε");

                        break;
                    case "Description":
                        if (string.IsNullOrEmpty(model.Description))
                            validation.Append("Description is required ε");
                        break;
                    case "Venue":
                        if (string.IsNullOrEmpty(model.Venue))
                            validation.Append("Venue is requiredε");
                        break;
                    case "EndDate":
                        if (string.IsNullOrEmpty(model.EndDate))
                            validation.Append("End Date is required ε");

                        break;
                    case "EndTime":
                        if (string.IsNullOrEmpty(model.EndTime))
                            validation.Append("End Time is required ε");
                        break;
                    case "AllowedTo":
                        if (string.IsNullOrEmpty(model.Description))
                            validation.Append("Addressed To is required ε");
                        break;
                    case "CircularDate":
                        if (string.IsNullOrEmpty(model.StartDate))
                            validation.Append("Circular Date is required ε");
                        break;
                }
            }

            var currentSession = _ICatalogMasterService.GetCurrentSession();

            if (!string.IsNullOrEmpty(model.StartTime) && (!string.IsNullOrEmpty(model.StartDate)))
            {
                DateTime StartTime = DateTime.ParseExact(model.StartTime, "hh:mm tt", CultureInfo.InvariantCulture);
                if (Convert.ToDateTime(model.StartDate).Date == DateTime.Now.Date)
                {
                    if (StartTime.TimeOfDay < DateTime.Now.TimeOfDay)
                        validation.Append("Start Time can't be less than current timeε");
                }
            }

            if ((!string.IsNullOrEmpty(model.EndDate)) && (!string.IsNullOrEmpty(model.StartDate)))
                if (Convert.ToDateTime(model.EndDate) < Convert.ToDateTime(model.StartDate))
                    validation.Append("End Date Can't be less than Start Dateε");

            if ((!string.IsNullOrEmpty(model.EndDate)))
            {
                if (Convert.ToDateTime(model.EndDate) > ((DateTime)currentSession.EndDate))
                    validation.Append("EndDate can't be greater than Session End Dateε");
            }

            if ((!string.IsNullOrEmpty(model.StartDate)))
            {
                if (Convert.ToDateTime(model.StartDate) > ((DateTime)currentSession.EndDate))
                    validation.Append("StartDate can't be greater than Session End Dateε");
            }

            //get event type
            if (model.EventTypeId > 0)
            {
                if (eventtype == "Holiday")
                {
                    //check if any exam exists on expected holiday date
                    var AlreadyhaveExam = _IExamService.GetAllExamDateSheetDetails(ref count, ExamDate: Convert.ToDateTime(model.StartDate));
                    if (AlreadyhaveExam.Count > 0)
                        validation.Append("Already have exam scheduled on this dateε");

                    //check if hliday already exists on same date
                    if (eventtype == "Holiday")
                    {
                        var ExistingHolidays = _ISchedularService.GetAllEventList(ref count, EventTypeId: model.EventTypeId).ToList();
                        ExistingHolidays = ExistingHolidays.Where(m => m.EventId != model.EventId && m.StartDate == Convert.ToDateTime(model.StartDate)).ToList();
                        if (ExistingHolidays.Count > 0)
                            validation.Append("Holiday date colliding with Existing Holiday dateε");
                    }
                }
                if (eventtype == "Vacation")
                {
                    var datesheetslist = _IExamService.GetAllExamDateSheets(ref count);
                    if (datesheetslist.Count > 0)
                        foreach (var dsl in datesheetslist)
                        {
                            if ((dsl.FromDate >= Convert.ToDateTime(model.StartDate).Date && dsl.FromDate <= Convert.ToDateTime(model.EndDate).Date) ||
                                (dsl.TillDate >= Convert.ToDateTime(model.StartDate).Date && dsl.TillDate <= Convert.ToDateTime(model.EndDate).Date) ||
                                (Convert.ToDateTime(model.StartDate).Date >= dsl.FromDate && Convert.ToDateTime(model.StartDate).Date <= dsl.TillDate) ||
                                (Convert.ToDateTime(model.EndDate).Date >= dsl.FromDate && Convert.ToDateTime(model.EndDate).Date <= dsl.TillDate))
                            {
                                validation.Append("Vacation dates colliding with Exam date sheet datesε");
                                break;
                            }
                        }
                    //check if vacation start and end date are same
                    if ((!string.IsNullOrEmpty(model.EndDate)) && (!string.IsNullOrEmpty(model.StartDate)))
                    {
                        if (Convert.ToDateTime(model.StartDate).Date == Convert.ToDateTime(model.EndDate).Date)
                            validation.Append("Vacation Start date and End Date Can't be sameε");
                    }

                    //check if vacation already exists
                    var Existingvacations = _ISchedularService.GetAllEventList(ref count, EventTypeId: model.EventTypeId);
                    if (Existingvacations.Count > 0 && model.EventId > 0)
                        Existingvacations = Existingvacations.Where(m => m.EventId != model.EventId).ToList();
                    foreach (var ev in Existingvacations)
                    {

                        if ((ev.StartDate >= Convert.ToDateTime(model.StartDate).Date && ev.StartDate <= Convert.ToDateTime(model.EndDate).Date) ||
                                (ev.EndDate >= Convert.ToDateTime(model.StartDate).Date && ev.EndDate <= Convert.ToDateTime(model.EndDate).Date) ||
                                (Convert.ToDateTime(model.StartDate).Date >= ev.StartDate && Convert.ToDateTime(model.StartDate).Date <= ev.EndDate) ||
                                (Convert.ToDateTime(model.EndDate).Date >= ev.StartDate && Convert.ToDateTime(model.EndDate).Date <= ev.EndDate))
                        {
                            validation.Append("Vacation dates colliding with Existing Vacation datesε");
                            break;
                        }

                    }
                }
            }

            deserializedclassList = deserializedclass;
            return validation.ToString();
        }

        [HttpPost]
        public ActionResult InsertEventTriggerDefualtValues(SchedularModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {

                //insert SMs for Student settings
                var SMSDefaultSetting = _ICommonMethodService.GetOrSetSMSDefaultSetting("EventStudent", true, false, true, "SMN", "");
                if (SMSDefaultSetting.SMSDefaultSettingsId != null)
                {
                    SMSDefaultSetting.FormKey = "EventStudent";
                    SMSDefaultSetting.Notification = (bool)model.StuNotification;
                    SMSDefaultSetting.SMS = (bool)model.StuSMS;
                    SMSDefaultSetting.ToNonAppUser = (bool)model.StuNonAppUser;
                    SMSDefaultSetting.SelectedMobile = model.SMSSelectionStudentMobileNo;

                    _ICommonMethodService.UpdateSMSDefaultSetting(SMSDefaultSetting);
                }

                //insert  SMs Staff settings
                var SMSDefaultSettingstu = _ICommonMethodService.GetOrSetSMSDefaultSetting("EventStaff", true, false, true, "StaffMobileNumber", "");
                if (SMSDefaultSettingstu.SMSDefaultSettingsId != null)
                {
                    SMSDefaultSettingstu.FormKey = "EventStaff";
                    SMSDefaultSettingstu.Notification = (bool)model.Notification;
                    SMSDefaultSettingstu.SMS = (bool)model.SMS;
                    SMSDefaultSettingstu.ToNonAppUser = (bool)model.NonAppUser;

                    _ICommonMethodService.UpdateSMSDefaultSetting(SMSDefaultSettingstu);
                }

                //insert  SMs Guardian settings
                var SMSDefaultSettingGuardian = _ICommonMethodService.GetOrSetSMSDefaultSetting("EventGuardian", true, false, true, "GuardianMobileNumber", "");
                if (SMSDefaultSettingGuardian.SMSDefaultSettingsId != null)
                {
                    SMSDefaultSettingGuardian.FormKey = "EventGuardian";
                    SMSDefaultSettingGuardian.Notification = (bool)model.GuardNotification;
                    SMSDefaultSettingGuardian.SMS = (bool)model.GuardSMS;
                    SMSDefaultSettingGuardian.ToNonAppUser = (bool)model.GuardNonAppUser;

                    _ICommonMethodService.UpdateSMSDefaultSetting(SMSDefaultSettingGuardian);
                }
                return Json(new
                {
                    status = "Success",
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = "Error happened"
                });
            }
        }

        public ActionResult AddUpdateEvent(SchedularModel SchedularModel)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            List<EventDetailModel> deserializedclassList;
            string validation = checkvalidationerrorAddUpdateEvent(SchedularModel, out deserializedclassList);
            StringBuilder classvalidation = new StringBuilder();
            try
            {
                var currentSession = _ICatalogMasterService.GetCurrentSession();
                if (!string.IsNullOrEmpty(SchedularModel.EncEventId))
                {
                    int Eventid = 0;
                    if (int.TryParse(_ICustomEncryption.base64d(SchedularModel.EncEventId), out Eventid))
                        SchedularModel.EventId = Eventid;
                }
                var StuDefaultSettings = _ICommonMethodService.GetOrSetSMSDefaultSetting("EventStudent", true, false, true, "SMN", "");
                var StaffDefaultSettings = _ICommonMethodService.GetOrSetSMSDefaultSetting("EventStaff", true, false, true, "StaffMobileNumber", "");
                var GuardDefaultSettings = _ICommonMethodService.GetOrSetSMSDefaultSetting("EventGuardian", true, false, true, "GuardianMobileNumber", "");

                if (string.IsNullOrEmpty(validation))
                {
                    // convert times
                    DateTime StartTime = DateTime.UtcNow;
                    DateTime EndTime = DateTime.UtcNow;

                    if (SchedularModel.StartTime != null)
                        StartTime = DateTime.ParseExact(SchedularModel.StartTime, "hh:mm tt", CultureInfo.InvariantCulture);
                    if (SchedularModel.EndTime != null)
                        EndTime = DateTime.ParseExact(SchedularModel.EndTime, "hh:mm tt", CultureInfo.InvariantCulture);

                    Event events = new Event();
                    EventDetail eventDetails = new EventDetail();
                    var eventtype = _ISchedularService.GetEventTypeById(SchedularModel.EventTypeId).EventType1;
                    var contactType = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1 == "Student").FirstOrDefault();
                    //Update event
                    if (SchedularModel.EventId != null && SchedularModel.EventId != 0)
                    {
                        // check authorization
                        //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Event", "Edit Record");
                        //if (!isauth)
                        //    return RedirectToAction("AccessDenied", "Error");

                        var eventlist = _ISchedularService.GetEventById(SchedularModel.EventId, true);
                        //delete previous detail
                        var eventdetaillist = _ISchedularService.GetAllEventDetailList(ref count, EventId: SchedularModel.EventId);
                        foreach (var edl in eventdetaillist)
                            _ISchedularService.DeleteEventDetail(edl.EventDetailId);

                        foreach (var id in deserializedclassList)
                        {
                            eventDetails = new EventDetail();
                            if (id.ClassId != "1.1" && id.ClassId != null)
                            {
                                eventDetails.ClassId = Convert.ToInt32(id.ClassId);
                                eventDetails.EventId = SchedularModel.EventId;
                                eventlist.IsAllSchool = false;
                                //eventlist.PostedOn = DateTime.Now;
                                //eventlist.PostedBy = user.UserId;
                                _ISchedularService.InsertEventDetail(eventDetails);
                            }
                            if (id.ClassId == "1.1")
                            {
                                eventlist.IsAllSchool = true;
                                break;
                            }
                        }

                        if (SchedularModel.EndDate != null)
                            eventlist.EndDate = Convert.ToDateTime(SchedularModel.EndDate);
                        if (SchedularModel.StartDate != null)
                            eventlist.StartDate = Convert.ToDateTime(SchedularModel.StartDate);
                        if (SchedularModel.StartTime != null)
                            eventlist.StartTime = StartTime.TimeOfDay;
                        if (SchedularModel.EndTime != null)
                            eventlist.EndTime = EndTime.TimeOfDay;

                        if (!string.IsNullOrEmpty(SchedularModel.AllowedTo))
                        {
                            if (SchedularModel.AllowedTo.ToLower() != "all")
                                eventlist.AddressedTo = Convert.ToInt32(SchedularModel.AllowedTo);
                            else
                                eventlist.IsAllSchool = true;
                        }
                        eventlist.EventTitle = SchedularModel.EventTitle;
                        eventlist.Description = SchedularModel.Description;
                        eventlist.Venue = SchedularModel.Venue;
                        eventlist.EventTypeId = SchedularModel.EventTypeId;
                        eventlist.IsActive = SchedularModel.IsActive;
                        eventlist.PostedOn = DateTime.Now;
                        eventlist.PostedBy = user.UserId;
                        _ISchedularService.UpdateEvent(eventlist);


                        if (eventtype.ToLower() == "circular")
                        {
                            var getcircularEventDoc = _ISchedularService.GetAllEventCircularDocs(ref count, EventId: eventlist.EventId).FirstOrDefault();
                            if (getcircularEventDoc != null)
                            {
                                getcircularEventDoc.DocumentCaption = (!string.IsNullOrEmpty(SchedularModel.CircularDocCaption)) ? SchedularModel.CircularDocCaption.Trim() : "";
                                getcircularEventDoc.EventId = eventlist.EventId;
                                getcircularEventDoc.FileName = SchedularModel.CircularDocFileName;
                                _ISchedularService.UpdateEventCircularDoc(getcircularEventDoc);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(SchedularModel.CircularDocFileName))
                                {
                                    EventCircularDoc circularEventDoc = new EventCircularDoc();
                                    circularEventDoc.DocumentCaption = (!string.IsNullOrEmpty(SchedularModel.CircularDocCaption)) ? SchedularModel.CircularDocCaption.Trim() : "";
                                    circularEventDoc.EventId = eventlist.EventId;
                                    circularEventDoc.FileName = SchedularModel.CircularDocFileName;
                                    _ISchedularService.InsertEventCircularDoc(circularEventDoc);



                                    ////////////trigger/////////
                                    var triggermessage = "";
                                    var triggermessageTilte = "";
                                    var triggermessageSMS = "";
                                    var triggermessageTilteSMS = "";
                                    var triggershooter = new TriggerShooter();
                                    var triggershooterSMS = new TriggerShooter();
                                    var trigger = _ITriggerEventService.GetAllTriggerEventList(ref count).Where(m => m.TriggerEvent1 == "Events" && m.IsActive == true).FirstOrDefault();
                                    if (trigger != null)
                                    {
                                        triggershooter = trigger.TriggerShooters.Where(m => m.TriggerTemplate != null
                                            && m.TriggerTemplate.IsActive == true && m.TriggerTemplate.TriggerMaster != null
                                            && m.TriggerTemplate.TriggerMaster.IsActive == true
                                            && m.TriggerTemplate.TriggerMaster.TriggerType != null
                                            && m.TriggerTemplate.TriggerMaster.TriggerType.IsActive == true &&
                                           m.TriggerTemplate.TriggerMaster.TriggerType.TriggerType1 == "Push Notification"
                                           && m.TriggerTemplate.TriggerMsgTemplate != null
                                           && m.TriggerTemplate.TriggerMsgTemplate.IsActive == true
                                           && m.IsActive == true).FirstOrDefault();
                                        if (triggershooter != null)
                                        {
                                            triggermessage = triggershooter.TriggerTemplate.TriggerMsgTemplate.TriggerMsgTemplate1;
                                            triggermessageTilte = triggershooter.TriggerTemplate.TriggerMsgTemplate.MsgTemplateName;

                                            triggermessage = triggermessage.Replace("%Description%", SchedularModel.Description);
                                            triggermessageTilte = triggermessageTilte.Replace("%EventTitle%", SchedularModel.EventTitle);
                                        }

                                        triggershooterSMS = trigger.TriggerShooters.Where(m => m.TriggerTemplate != null
                                            && m.TriggerTemplate.IsActive == true && m.TriggerTemplate.TriggerMaster != null
                                            && m.TriggerTemplate.TriggerMaster.IsActive == true
                                            && m.TriggerTemplate.TriggerMaster.TriggerType != null
                                            && m.TriggerTemplate.TriggerMaster.TriggerType.IsActive == true &&
                                            m.TriggerTemplate.TriggerMaster.TriggerType.TriggerType1 == "SMS"
                                            && m.TriggerTemplate.TriggerMsgTemplate != null
                                            && m.TriggerTemplate.TriggerMsgTemplate.IsActive == true
                                            && m.IsActive == true).FirstOrDefault();
                                        if (triggershooterSMS != null)
                                        {
                                            triggermessageSMS = triggershooterSMS.TriggerTemplate.TriggerMsgTemplate.TriggerMsgTemplate1;
                                            triggermessageTilteSMS = triggershooterSMS.TriggerTemplate.TriggerMsgTemplate.MsgTemplateName;

                                            triggermessageSMS = triggermessageSMS.Replace("%Description%", SchedularModel.Description);
                                            triggermessageTilteSMS = triggermessageTilteSMS.Replace("%EventTitle%", SchedularModel.EventTitle);
                                        }
                                    }

                                    ///////////trigger//////////
                                    foreach (var id in deserializedclassList)
                                    {
                                        if (id.ClassId != "1.1" && id.ClassId != null)
                                        {
                                            //eventDetails.ClassId = Convert.ToInt32(id.ClassId);
                                            //eventDetails.EventId = events.EventId;
                                            //_ISchedularService.InsertEventDetail(eventDetails);

                                            var Students = _IStudentService.GetAllStudentClasss(ref count).Where(m => m.ClassId == Convert.ToInt32(id.ClassId) && m.SessionId == currentSession.SessionId);
                                            ////////trigger///////////
                                            if (trigger != null && triggershooter != null)
                                            {
                                                if ((bool)StuDefaultSettings.Notification)
                                                {
                                                    foreach (var item in Students)
                                                    {
                                                        var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.UserTypeId == contactType.ContactTypeId && m.UserContactId == item.StudentId).FirstOrDefault();
                                                        if (schooluser != null)
                                                        {
                                                            //create collapse Key for notifcitaion
                                                            var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                            var CounterNotification = 0;
                                                            var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                                   (currentDate + "_" + CounterNotification.ToString()), null
                                                                                                   , "UniqueKey for Notifications on APP").AttributeValue;
                                                            var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                            if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                            {
                                                                CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                                CounterNotification++;
                                                            }
                                                            else
                                                                CounterNotification = 1;


                                                            var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                                                            var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();
                                                            if (userdeviceIdsArray.Count() > 0)
                                                            {
                                                                var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                            (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                                _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray,
                                                                                                triggermessageTilte, triggermessage, "IOS",
                                                                                                UpdateCollapseKey);
                                                            }
                                                            if (userdeviceIdsArrayAndroid.Count() > 0)
                                                            {

                                                                var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                            (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;
                                                                _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, 
                                                                                                triggermessage, "Android", UpdateCollapseKey);
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            if (trigger != null && triggershooterSMS != null)
                                            {
                                                if ((bool)StuDefaultSettings.SMS)
                                                {
                                                    foreach (var item in Students)
                                                    {
                                                        var errormessage = "";
                                                        var content = "";
                                                        var Student = _IStudentService.GetAllStudents(ref count).Where(m => m.StudentId == item.StudentId).FirstOrDefault();
                                                        string SendMsgMobileNo = _ICommonMethodService.ChooseStudentMobileNo(StuDefaultSettings.SelectedMobile, item.StudentId);
                                                        if (Student != null && !string.IsNullOrEmpty(SendMsgMobileNo))
                                                        {
                                                            if ((bool)StuDefaultSettings.ToNonAppUser)
                                                            {
                                                                bool YesAppUser = _ICommonMethodService.CheckAppUser(item.StudentId, (bool)StuDefaultSettings.ToNonAppUser, contactType.ContactTypeId);
                                                                if (!YesAppUser)
                                                                {
                                                                    if (!string.IsNullOrEmpty(SendMsgMobileNo))
                                                                    {
                                                                        _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                //MaxNoOfRecipientsinSMS
                                                                _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                            }
                                                        }

                                                    }
                                                }

                                            }

                                            ///////////trigger/////////

                                        }
                                        if (id.ClassId == "1.1")
                                        {
                                            var Students = _IStudentService.GetAllStudentClasss(ref count).Where(m => m.SessionId == currentSession.SessionId);
                                            ////////trigger///////////
                                            if (trigger != null && triggershooter != null)
                                            {
                                                if ((bool)StuDefaultSettings.Notification)
                                                {
                                                    foreach (var item in Students)
                                                    {
                                                        var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.UserTypeId == contactType.ContactTypeId && m.UserContactId == item.StudentId).FirstOrDefault();
                                                        if (schooluser != null)
                                                        {
                                                            //create collapse Key for notifcitaion
                                                            var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                            var CounterNotification = 0;
                                                            var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                                   (currentDate + "_" + CounterNotification.ToString()), null
                                                                                                   , "UniqueKey for Notifications on APP").AttributeValue;
                                                            var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                            if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                            {
                                                                CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                                CounterNotification++;
                                                            }
                                                            else
                                                                CounterNotification = 1;


                                                            var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                                                            var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();
                                                            if (userdeviceIdsArray.Count() > 0)
                                                            {
                                                                var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                                          (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                                _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, triggermessageTilte, triggermessage, "IOS", UpdateCollapseKey);
                                                            }
                                                            if (userdeviceIdsArrayAndroid.Count() > 0)
                                                            {
                                                                var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                                         (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                                _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, triggermessage, "Android", UpdateCollapseKey);
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            if (trigger != null && triggershooterSMS != null)
                                            {
                                                if ((bool)StuDefaultSettings.SMS)
                                                {
                                                    foreach (var item in Students)
                                                    {
                                                        var errormessage = "";
                                                        var content = "";
                                                        var Student = _IStudentService.GetAllStudents(ref count).Where(m => m.StudentId == item.StudentId).FirstOrDefault();

                                                        string SendMsgMobileNo = _ICommonMethodService.ChooseStudentMobileNo(StuDefaultSettings.SelectedMobile, item.StudentId);
                                                        if (Student != null && !string.IsNullOrEmpty(SendMsgMobileNo))
                                                        {
                                                            if ((bool)StuDefaultSettings.ToNonAppUser)
                                                            {
                                                                bool YesAppUser = _ICommonMethodService.CheckAppUser(item.StudentId, (bool)StuDefaultSettings.ToNonAppUser, contactType.ContactTypeId);
                                                                if (!YesAppUser)
                                                                {
                                                                    if (!string.IsNullOrEmpty(SendMsgMobileNo))
                                                                    {
                                                                        _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                //MaxNoOfRecipientsinSMS
                                                                _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                            }

                                                        }
                                                    }
                                                }

                                            }
                                            break;
                                        }

                                    }
                                }
                            }
                        }
                        return Json(new
                        {
                            status = "success",
                            msg = eventtype + " has been Updated",
                        });
                    }
                    //Add event 
                    else
                    {
                        // check authorization
                        //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Event", "Add Record");
                        //if (!isauth)
                        //    return RedirectToAction("AccessDenied", "Error");

                        events.IsAllSchool = false;
                        foreach (var id in deserializedclassList)
                        {
                            if (id.ClassId == "1.1" && id.ClassId != null)
                            {
                                events.IsAllSchool = true;
                                break;
                            }
                        }

                        if (SchedularModel.EndDate != null)
                            events.EndDate = Convert.ToDateTime(SchedularModel.EndDate);
                        if (SchedularModel.StartDate != null)
                            events.StartDate = Convert.ToDateTime(SchedularModel.StartDate);
                        if (SchedularModel.StartTime != null)
                            events.StartTime = StartTime.TimeOfDay;
                        if (SchedularModel.EndTime != null)
                            events.EndTime = EndTime.TimeOfDay;

                        events.EventTitle = SchedularModel.EventTitle;
                        events.Description = SchedularModel.Description;
                        events.Venue = SchedularModel.Venue;
                        events.EventTypeId = SchedularModel.EventTypeId;
                        events.PostedOn = DateTime.Now;
                        events.PostedBy = user.UserId;

                        if (!string.IsNullOrEmpty(SchedularModel.AllowedTo))
                        {
                            if (SchedularModel.AllowedTo.ToLower() != "all")
                                events.AddressedTo = Convert.ToInt32(SchedularModel.AllowedTo);
                            else
                                events.IsAllSchool = true;
                        }

                        events.IsActive = true;
                        events.IsDeleted = false;
                        _ISchedularService.InsertEvent(events);

                        //insert documents for circular event if any
                        if (eventtype.ToLower() == "circular")
                        {
                            if (!string.IsNullOrEmpty(SchedularModel.CircularDocFileName))
                            {
                                EventCircularDoc circularEventDoc = new EventCircularDoc();
                                circularEventDoc.DocumentCaption = SchedularModel.CircularDocCaption.Trim();
                                circularEventDoc.EventId = events.EventId;
                                circularEventDoc.FileName = SchedularModel.CircularDocFileName;
                                _ISchedularService.InsertEventCircularDoc(circularEventDoc);
                            }
                        }



                        ////////////trigger/////////
                        var triggermessage = "";
                        var triggermessageTilte = "";
                        var triggermessageSMS = "";
                        var triggermessageTilteSMS = "";
                        var triggershooter = new TriggerShooter();
                        var triggershooterSMS = new TriggerShooter();
                        var trigger = _ITriggerEventService.GetAllTriggerEventList(ref count).Where(m => m.TriggerEvent1 == "Events" && m.IsActive == true).FirstOrDefault();
                        if (trigger != null)
                        {
                            triggershooter = trigger.TriggerShooters.Where(m => m.TriggerTemplate != null && m.TriggerTemplate.IsActive == true && m.TriggerTemplate.TriggerMaster != null && m.TriggerTemplate.TriggerMaster.IsActive == true && m.TriggerTemplate.TriggerMaster.TriggerType != null && m.TriggerTemplate.TriggerMaster.TriggerType.IsActive == true &&
                               m.TriggerTemplate.TriggerMaster.TriggerType.TriggerType1 == "Push Notification" && m.TriggerTemplate.TriggerMsgTemplate != null && m.TriggerTemplate.TriggerMsgTemplate.IsActive == true && m.IsActive == true).FirstOrDefault();
                            if (triggershooter != null)
                            {
                                triggermessage = triggershooter.TriggerTemplate.TriggerMsgTemplate.TriggerMsgTemplate1;
                                triggermessageTilte = triggershooter.TriggerTemplate.TriggerMsgTemplate.MsgTemplateName;

                                triggermessage = triggermessage.Replace("%Description%", SchedularModel.Description);
                                triggermessageTilte = triggermessageTilte.Replace("%EventTitle%", SchedularModel.EventTitle);
                            }

                            triggershooterSMS = trigger.TriggerShooters.Where(m => m.TriggerTemplate != null && m.TriggerTemplate.IsActive == true && m.TriggerTemplate.TriggerMaster != null && m.TriggerTemplate.TriggerMaster.IsActive == true && m.TriggerTemplate.TriggerMaster.TriggerType != null && m.TriggerTemplate.TriggerMaster.TriggerType.IsActive == true &&
                            m.TriggerTemplate.TriggerMaster.TriggerType.TriggerType1 == "SMS" && m.TriggerTemplate.TriggerMsgTemplate != null && m.TriggerTemplate.TriggerMsgTemplate.IsActive == true && m.IsActive == true).FirstOrDefault();
                            if (triggershooterSMS != null)
                            {
                                triggermessageSMS = triggershooterSMS.TriggerTemplate.TriggerMsgTemplate.TriggerMsgTemplate1;
                                triggermessageTilteSMS = triggershooterSMS.TriggerTemplate.TriggerMsgTemplate.MsgTemplateName;

                                triggermessageSMS = triggermessageSMS.Replace("%Description%", SchedularModel.Description);
                                triggermessageTilteSMS = triggermessageTilteSMS.Replace("%EventTitle%", SchedularModel.EventTitle);
                            }
                        }

                        ///////////trigger//////////

                        if (deserializedclassList.Count > 0)
                        {
                            #region ForClassSelection
                            foreach (var id in deserializedclassList)
                            {
                                if (id.ClassId != "1.1" && id.ClassId != null)
                                {
                                    eventDetails.ClassId = Convert.ToInt32(id.ClassId);
                                    eventDetails.EventId = events.EventId;
                                    _ISchedularService.InsertEventDetail(eventDetails);

                                    var Students = _IStudentService.GetAllStudentClasss(ref count).Where(m => m.ClassId == Convert.ToInt32(id.ClassId) && m.SessionId == currentSession.SessionId);
                                    ////////trigger///////////
                                    if (trigger != null && triggershooter != null)
                                    {
                                        if ((bool)StuDefaultSettings.Notification)
                                        {
                                            foreach (var item in Students)
                                            {
                                                var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.UserTypeId == contactType.ContactTypeId && m.UserContactId == item.StudentId).FirstOrDefault();
                                                if (schooluser != null)
                                                {

                                                    //create collapse Key for notifcitaion
                                                    var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                    var CounterNotification = 0;
                                                    var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                           (currentDate + "_" + CounterNotification.ToString()), null
                                                                                           , "UniqueKey for Notifications on APP").AttributeValue;
                                                    var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                    if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                    {
                                                        CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                        CounterNotification++;
                                                    }
                                                    else
                                                        CounterNotification = 1;


                                                    var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                                                    var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();
                                                    if (userdeviceIdsArray.Count() > 0)
                                                    {

                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                                   (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, triggermessageTilte, triggermessage, "IOS", UpdateCollapseKey);
                                                    }
                                                    if (userdeviceIdsArrayAndroid.Count() > 0)
                                                    {

                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                                   (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;



                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, triggermessage, "Android", UpdateCollapseKey);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (trigger != null && triggershooterSMS != null)
                                    {
                                        if ((bool)StuDefaultSettings.SMS)
                                        {
                                            foreach (var item in Students)
                                            {
                                                var errormessage = "";
                                                var content = "";
                                                var Student = _IStudentService.GetAllStudents(ref count).Where(m => m.StudentId == item.StudentId).FirstOrDefault();
                                                string SendMsgMobileNo = _ICommonMethodService.ChooseStudentMobileNo(StuDefaultSettings.SelectedMobile, item.StudentId);
                                                if (Student != null && !string.IsNullOrEmpty(SendMsgMobileNo))
                                                {
                                                    if ((bool)StuDefaultSettings.ToNonAppUser)
                                                    {
                                                        bool YesAppUser = _ICommonMethodService.CheckAppUser(item.StudentId, (bool)StuDefaultSettings.ToNonAppUser, contactType.ContactTypeId);
                                                        if (!YesAppUser)
                                                        {
                                                            if (!string.IsNullOrEmpty(SendMsgMobileNo))
                                                            {
                                                                _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //MaxNoOfRecipientsinSMS
                                                        _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                    }

                                                }
                                            }
                                        }
                                    }

                                    ///////////trigger/////////

                                }
                                if (id.ClassId == "1.1")
                                {
                                    var Students = _IStudentService.GetAllStudentClasss(ref count).Where(m => m.SessionId == currentSession.SessionId);
                                    ////////trigger///////////
                                    if (trigger != null && triggershooter != null)
                                    {
                                         if ((bool)StuDefaultSettings.Notification)
                                        {
                                            foreach (var item in Students)
                                            {
                                                var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.UserTypeId == contactType.ContactTypeId && m.UserContactId == item.StudentId).FirstOrDefault();
                                                if (schooluser != null)
                                                {
                                                    //create collapse Key for notifcitaion
                                                    var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                    var CounterNotification = 0;
                                                    var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                           (currentDate + "_" + CounterNotification.ToString()), null
                                                                                           , "UniqueKey for Notifications on APP").AttributeValue;
                                                    var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                    if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                    {
                                                        CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                        CounterNotification++;
                                                    }
                                                    else
                                                        CounterNotification = 1;

                                                    var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                                                    var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();
                                                    if (userdeviceIdsArray.Count() > 0)
                                                    {
                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                            (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, triggermessageTilte, triggermessage, "IOS", UpdateCollapseKey);
                                                    }
                                                    if (userdeviceIdsArrayAndroid.Count() > 0)
                                                    {

                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                            (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, triggermessage, "Android", UpdateCollapseKey);
                                                    }
                                                }

                                            }
                                        }
                                    }

                                    if (trigger != null && triggershooterSMS != null)
                                    {
                                        if ((bool)StuDefaultSettings.SMS)
                                        {
                                            foreach (var item in Students)
                                            {
                                                var errormessage = "";
                                                var content = "";
                                                var Student = _IStudentService.GetAllStudents(ref count).Where(m => m.StudentId == item.StudentId).FirstOrDefault();
                                                string SendMsgMobileNo = _ICommonMethodService.ChooseStudentMobileNo(StuDefaultSettings.SelectedMobile, item.StudentId);
                                                if (Student != null && !string.IsNullOrEmpty(SendMsgMobileNo))
                                                {
                                                    if ((bool)StuDefaultSettings.ToNonAppUser)
                                                    {
                                                        bool YesAppUser = _ICommonMethodService.CheckAppUser(item.StudentId, (bool)StuDefaultSettings.ToNonAppUser, contactType.ContactTypeId);
                                                        if (!YesAppUser)
                                                        {
                                                            if (!string.IsNullOrEmpty(SendMsgMobileNo))
                                                            {
                                                                _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //MaxNoOfRecipientsinSMS
                                                        _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    break;
                                }

                            }
                            #endregion
                        }
                        else
                        {
                            #region in circular
                            if (eventtype.ToLower() == "circular")
                            {
                                var StudentStatusLeft = _IStudentMasterService.GetAllStudentStatus(ref count).Where(m => m.StudentStatus.ToLower() == "left").FirstOrDefault();

                                var GetStudents = new List<Student>();
                                var currentsession = _ICatalogMasterService.GetCurrentSession();
                                var studentIds = _IStudentService.GetAllStudents(ref count)
                                                  .Where(m => m.StudentStatusId != StudentStatusLeft.StudentStatusId)
                                                  .Select(x => x.StudentId).ToArray();
                                GetStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: currentsession.SessionId)
                                                    .Where(x => studentIds.Contains((int)x.StudentId)).Select(x => x.Student).ToList();


                                var Guardians = _IGuardianService.GetAllGuardians(ref count).Where(m => studentIds.Contains((int)m.StudentId)).ToList();
                                var Teachers = _IStaffService.GetAllStaffs(ref count);


                                if ((bool)events.IsAllSchool)
                                {
                                    #region in all case
                                    if (trigger != null)
                                    {

                                        //all students nto left
                                        foreach (var item in GetStudents)
                                        {
                                            var errormessage = "";
                                            var content = "";
                                            string ContactNo = _ICommonMethodService.ChooseStudentMobileNo(StuDefaultSettings.SelectedMobile, item.StudentId);
                                            if (triggershooterSMS != null && (bool)StuDefaultSettings.SMS)
                                            {
                                                if (!string.IsNullOrEmpty(ContactNo))
                                                {
                                                    //MaxNoOfRecipientsinSMS
                                                    _ISMSSender.SendSMS(ContactNo, triggermessageSMS, false, out errormessage, out content);
                                                }
                                            }

                                            if (triggershooter != null && (bool)StuDefaultSettings.Notification)
                                            {
                                                var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.ContactType.ContactType1.ToLower() == "student"
                                                                        && m.UserContactId == item.StudentId).FirstOrDefault();
                                                if (schooluser != null)
                                                {
                                                    //create collapse Key for notifcitaion
                                                    var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                    var CounterNotification = 0;
                                                    var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                           (currentDate + "_" + CounterNotification.ToString()), null
                                                                                           , "UniqueKey for Notifications on APP").AttributeValue;
                                                    var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                    if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                    {
                                                        CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                        CounterNotification++;
                                                    }
                                                    else
                                                        CounterNotification = 1;


                                                    var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                                                    var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();
                                                    if (userdeviceIdsArray.Count() > 0)
                                                    {
                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                            (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, triggermessageTilte, triggermessage, "IOS", UpdateCollapseKey);
                                                    }
                                                    if (userdeviceIdsArrayAndroid.Count() > 0)
                                                    {
                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                            (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, triggermessage, "Android", UpdateCollapseKey);
                                                    }
                                                }
                                            }
                                        }

                                        //for all teachers
                                        foreach (var item in Teachers.Where(x => x.StaffType.IsTeacher == true).ToList())
                                        {
                                            var errormessage = "";
                                            var content = "";
                                            var contactInfo = _IContactService.GetAllContactInfos(ref count, ContactId: item.StaffId)
                                                                .Where(x => x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                                && x.ContactType.ContactType1.ToLower() == "teacher"
                                                                && x.IsDefault == true && x.Status == true).FirstOrDefault();
                                            if (triggershooterSMS != null && (bool)StaffDefaultSettings.SMS)
                                            {
                                                if (contactInfo != null)
                                                {
                                                    //MaxNoOfRecipientsinSMS
                                                    _ISMSSender.SendSMS(contactInfo.ContactInfo1, triggermessageSMS, false, out errormessage, out content);
                                                }
                                            }

                                            if (triggershooter != null && (bool)StaffDefaultSettings.Notification)
                                            {
                                                var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.ContactType.ContactType1.ToLower() == "teacher"
                                                                        && m.UserContactId == item.StaffId).FirstOrDefault();
                                                if (schooluser != null)
                                                {
                                                    //create collapse Key for notifcitaion
                                                    var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                    var CounterNotification = 0;
                                                    var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                           (currentDate + "_" + CounterNotification.ToString()), null
                                                                                           , "UniqueKey for Notifications on APP").AttributeValue;
                                                    var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                    if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                    {
                                                        CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                        CounterNotification++;
                                                    }
                                                    else
                                                        CounterNotification = 1;

                                                    var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                                                    var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();
                                                    if (userdeviceIdsArray.Count() > 0)
                                                    {
                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                            (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, triggermessageTilte, triggermessage, "IOS", UpdateCollapseKey);

                                                    }
                                                    if (userdeviceIdsArrayAndroid.Count() > 0)
                                                    {
                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                            (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;
                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, triggermessage, "Android", UpdateCollapseKey);
                                                    }
                                                }
                                            }
                                        }

                                        //for non teachers
                                        foreach (var item in Teachers.Where(x => x.StaffType.IsTeacher == false).ToList())
                                        {
                                            var errormessage = "";
                                            var content = "";
                                            var contactInfo = _IContactService.GetAllContactInfos(ref count, ContactId: item.StaffId)
                                                                .Where(x => x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                                && x.ContactType.ContactType1.ToLower() == "teacher"
                                                                && x.IsDefault == true && x.Status == true).FirstOrDefault();
                                            if (triggershooterSMS != null && (bool)StaffDefaultSettings.SMS)
                                            {
                                                if (contactInfo != null)
                                                {
                                                    //MaxNoOfRecipientsinSMS
                                                    _ISMSSender.SendSMS(contactInfo.ContactInfo1, triggermessageSMS, false, out errormessage, out content);
                                                }
                                            }

                                            if (triggershooter != null && (bool)StaffDefaultSettings.Notification)
                                            {
                                                var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.ContactType.ContactType1.ToLower() == "teacher"
                                                                        && m.UserContactId == item.StaffId).FirstOrDefault();
                                                if (schooluser != null)
                                                {

                                                    //create collapse Key for notifcitaion
                                                    var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                    var CounterNotification = 0;
                                                    var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                           (currentDate + "_" + CounterNotification.ToString()), null
                                                                                           , "UniqueKey for Notifications on APP").AttributeValue;
                                                    var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                    if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                    {
                                                        CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                        CounterNotification++;
                                                    }
                                                    else
                                                        CounterNotification = 1;

                                                    var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                                                    var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();
                                                    if (userdeviceIdsArray.Count() > 0)
                                                    {
                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                            (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, triggermessageTilte, triggermessage, "IOS", UpdateCollapseKey);
                                                    }
                                                    if (userdeviceIdsArrayAndroid.Count() > 0)
                                                    {
                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                            (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, triggermessage, "Android", UpdateCollapseKey);
                                                    }
                                                }
                                            }
                                        }

                                        //for guardians
                                        foreach (var item in Guardians)
                                        {
                                            var errormessage = "";
                                            var content = "";
                                            var contactInfo = _IContactService.GetAllContactInfos(ref count, ContactId: item.GuardianId)
                                                                .Where(x => x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                                && x.ContactType.ContactType1.ToLower() == "guardian"
                                                                && x.IsDefault == true && x.Status == true).FirstOrDefault();

                                            if (triggershooterSMS != null && (bool)GuardDefaultSettings.SMS)
                                            {
                                                if (contactInfo != null)
                                                {
                                                    //MaxNoOfRecipientsinSMS
                                                    _ISMSSender.SendSMS(contactInfo.ContactInfo1, triggermessageSMS, false, out errormessage, out content);
                                                }
                                            }

                                            if (triggershooter != null && (bool)StaffDefaultSettings.Notification)
                                            {
                                                var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.ContactType.ContactType1.ToLower() == "guardian"
                                                                        && m.UserContactId == item.GuardianId).FirstOrDefault();
                                                if (schooluser != null)
                                                {

                                                    //create collapse Key for notifcitaion
                                                    var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                    var CounterNotification = 0;
                                                    var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                           (currentDate + "_" + CounterNotification.ToString()), null
                                                                                           , "UniqueKey for Notifications on APP").AttributeValue;
                                                    var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                    if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                    {
                                                        CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                        CounterNotification++;
                                                    }
                                                    else
                                                        CounterNotification = 1;


                                                    var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                                                    var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();
                                                    if (userdeviceIdsArray.Count() > 0)
                                                    {
                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                           (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, triggermessageTilte, triggermessage, "IOS", UpdateCollapseKey);
                                                    }
                                                    if (userdeviceIdsArrayAndroid.Count() > 0)
                                                    {
                                                        var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                           (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                                                        _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, triggermessage, "Android", UpdateCollapseKey);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                else
                                {
                                    var UsercontactType = _IAddressMasterService.GetAllContactTypes(ref count).Where(x => x.ContactTypeId == events.AddressedTo).FirstOrDefault();
                                    switch (UsercontactType.ContactType1.ToLower())
                                    {
                                        case "student":
                                            #region students
                                            //all students not left
                                            foreach (var item in GetStudents)
                                            {
                                                var errormessage = "";
                                                var content = "";
                                                string ContactNo = _ICommonMethodService.ChooseStudentMobileNo(StuDefaultSettings.SelectedMobile, item.StudentId);
                                                if (triggershooterSMS != null && (bool)StuDefaultSettings.SMS)
                                                {
                                                    if (!string.IsNullOrEmpty(ContactNo))
                                                    {
                                                        //MaxNoOfRecipientsinSMS
                                                        _ISMSSender.SendSMS(ContactNo, triggermessageSMS, false, out errormessage, out content);
                                                    }
                                                }

                                                if (triggershooter != null && (bool)StuDefaultSettings.Notification)
                                                {
                                                    var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.UserTypeId == UsercontactType.ContactTypeId
                                                                            && m.UserContactId == item.StudentId).FirstOrDefault();
                                                    if (schooluser != null)
                                                    {
                                                        //create collapse Key for notifcitaion
                                                        var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                        var CounterNotification = 0;
                                                        var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                               (currentDate + "_" + CounterNotification.ToString()), null
                                                                                               , "UniqueKey for Notifications on APP").AttributeValue;
                                                        var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                        if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                        {
                                                            CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                            CounterNotification++;
                                                        }
                                                        else
                                                            CounterNotification = 1;

                                                        var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                                                        var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();
                                                        if (userdeviceIdsArray.Count() > 0)
                                                        {
                                                            var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                           (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                            _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, triggermessageTilte, triggermessage, "IOS", UpdateCollapseKey);
                                                        }
                                                        if (userdeviceIdsArrayAndroid.Count() > 0)
                                                        {
                                                            var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                           (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                            _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, triggermessage, "Android", UpdateCollapseKey);
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;

                                        case "teacher":
                                            #region//for all teachers
                                            foreach (var item in Teachers.Where(x => x.StaffType.IsTeacher == true).ToList())
                                            {
                                                var errormessage = "";
                                                var content = "";
                                                var contactInfo = _IContactService.GetAllContactInfos(ref count, ContactId: item.StaffId)
                                                                    .Where(x => x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                                    && x.ContactType.ContactType1.ToLower() == "teacher"
                                                                    && x.IsDefault == true && x.Status == true).FirstOrDefault();

                                                string SMsMobileno = contactInfo != null ? contactInfo.ContactInfo1 : "";
                                                if (triggershooterSMS != null && (bool)StaffDefaultSettings.SMS)
                                                {
                                                    if (!string.IsNullOrEmpty(SMsMobileno))
                                                    {
                                                        //MaxNoOfRecipientsinSMS
                                                        _ISMSSender.SendSMS(SMsMobileno, triggermessageSMS, false, out errormessage, out content);
                                                    }
                                                }

                                                if (triggershooter != null && (bool)StaffDefaultSettings.Notification)
                                                {
                                                    var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.ContactType.ContactType1.ToLower() == "teacher"
                                                                            && m.UserContactId == item.StaffId).FirstOrDefault();
                                                    if (schooluser != null)
                                                    {
                                                        //create collapse Key for notifcitaion
                                                        var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                        var CounterNotification = 0;
                                                        var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                               (currentDate + "_" + CounterNotification.ToString()), null
                                                                                               , "UniqueKey for Notifications on APP").AttributeValue;
                                                        var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                        if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                        {
                                                            CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                            CounterNotification++;
                                                        }
                                                        else
                                                            CounterNotification = 1;

                                                        var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                                                        var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();
                                                        if (userdeviceIdsArray.Count() > 0)
                                                        {
                                                            var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                           (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                            _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, triggermessageTilte, triggermessage, "IOS", UpdateCollapseKey);
                                                        }
                                                        if (userdeviceIdsArrayAndroid.Count() > 0)
                                                        {
                                                            var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                              (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                                                            _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, triggermessage, "Android", UpdateCollapseKey);
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;


                                        case "non-teaching":
                                            #region //for non teachers
                                            foreach (var item in Teachers.Where(x => x.StaffType.IsTeacher == false).ToList())
                                            {
                                                var errormessage = "";
                                                var content = "";
                                                var contactInfo = _IContactService.GetAllContactInfos(ref count, ContactId: item.StaffId)
                                                                    .Where(x => x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                                    && x.ContactType.ContactType1.ToLower() == "teacher"
                                                                    && x.IsDefault == true && x.Status == true).FirstOrDefault();
                                                 string SMsMobileno = contactInfo != null ? contactInfo.ContactInfo1 : "";
                                                if (triggershooterSMS != null && (bool)StaffDefaultSettings.SMS)
                                                {
                                                    if (!string.IsNullOrEmpty(SMsMobileno))
                                                    {
                                                        //MaxNoOfRecipientsinSMS
                                                        _ISMSSender.SendSMS(SMsMobileno, triggermessageSMS, false, out errormessage, out content);
                                                    }
                                                }

                                                if (triggershooter != null && (bool)StaffDefaultSettings.Notification)
                                                {
                                                    var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.ContactType.ContactType1.ToLower() == "teacher"
                                                                            && m.UserContactId == item.StaffId).FirstOrDefault();
                                                    if (schooluser != null)
                                                    {
                                                        //create collapse Key for notifcitaion
                                                        var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                        var CounterNotification = 0;
                                                        var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                               (currentDate + "_" + CounterNotification.ToString()), null
                                                                                               , "UniqueKey for Notifications on APP").AttributeValue;
                                                        var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                        if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                        {
                                                            CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                            CounterNotification++;
                                                        }
                                                        else
                                                            CounterNotification = 1;

                                                        var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                                                        var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();
                                                        if (userdeviceIdsArray.Count() > 0)
                                                        {
                                                            var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                           (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                            _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, triggermessageTilte, triggermessage, "IOS", UpdateCollapseKey);
                                                        }
                                                        if (userdeviceIdsArrayAndroid.Count() > 0)
                                                        {
                                                            var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                           (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                                                            _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, triggermessage, "Android", UpdateCollapseKey);
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;


                                        case "guardian":
                                            #region//for guardians
                                            foreach (var item in Guardians)
                                            {
                                                var errormessage = "";
                                                var content = "";
                                                var contactInfo = _IContactService.GetAllContactInfos(ref count, ContactId: item.GuardianId)
                                                                    .Where(x => x.ContactInfoType.ContactInfoType1.ToLower() == "mobile"
                                                                    && x.ContactType.ContactType1.ToLower() == "guardian"
                                                                    && x.IsDefault == true && x.Status == true).FirstOrDefault();
                                                string SMsMobileno = contactInfo != null ? contactInfo.ContactInfo1 : "";
                                                if (triggershooterSMS != null && (bool)GuardDefaultSettings.SMS)
                                                {
                                                    if (!string.IsNullOrEmpty(SMsMobileno))
                                                    {
                                                        //MaxNoOfRecipientsinSMS
                                                        _ISMSSender.SendSMS(SMsMobileno, triggermessageSMS, false, out errormessage, out content);
                                                    }
                                                }

                                                if (triggershooter != null && (bool)StaffDefaultSettings.Notification)
                                                {
                                                    var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.ContactType.ContactType1.ToLower() == "guardian"
                                                                            && m.UserContactId == item.GuardianId).FirstOrDefault();
                                                    if (schooluser != null)
                                                    {
                                                        //create collapse Key for notifcitaion
                                                        var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                        var CounterNotification = 0;
                                                        var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                               (currentDate + "_" + CounterNotification.ToString()), null
                                                                                               , "UniqueKey for Notifications on APP").AttributeValue;
                                                        var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                        if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                        {
                                                            CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                            CounterNotification++;
                                                        }
                                                        else
                                                            CounterNotification = 1;

                                                        var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                                                        var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();
                                                        if (userdeviceIdsArray.Count() > 0)
                                                        {
                                                            var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                            (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                            _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, triggermessageTilte, triggermessage, "IOS", UpdateCollapseKey);
                                                        }
                                                        if (userdeviceIdsArrayAndroid.Count() > 0)
                                                        {
                                                            var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                           (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                            _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, triggermessage, "Android", UpdateCollapseKey);
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                var Students = _IStudentService.GetAllStudentClasss(ref count).Where(m => m.SessionId == currentSession.SessionId);
                                ////////trigger///////////
                                if (trigger != null && triggershooter != null)
                                {
                                    if ((bool)StuDefaultSettings.Notification)
                                    {
                                        foreach (var item in Students)
                                        {
                                            var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.UserTypeId == contactType.ContactTypeId && m.UserContactId == item.StudentId).FirstOrDefault();
                                            if (schooluser != null)
                                            {
                                                //create collapse Key for notifcitaion
                                                var currentDate = DateTime.Now.Date.ToString("yyMMdd");
                                                var CounterNotification = 0;
                                                var getCollapseKeyNotification = _ISchoolSettingService.GetorSetSchoolData("CollapseKeyForNotification",
                                                                                       (currentDate + "_" + CounterNotification.ToString()), null
                                                                                       , "UniqueKey for Notifications on APP").AttributeValue;
                                                var OnlyAttributeSavedDate = getCollapseKeyNotification.Split('_');
                                                if (currentDate == OnlyAttributeSavedDate[0].ToString())
                                                {
                                                    CounterNotification = Convert.ToInt32(OnlyAttributeSavedDate[1]);
                                                    CounterNotification++;
                                                }
                                                else
                                                    CounterNotification = 1;

                                                var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                                                var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();
                                                if (userdeviceIdsArray.Count() > 0)
                                                {
                                                    var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                        (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;


                                                    _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArray, triggermessageTilte, triggermessage, "IOS", UpdateCollapseKey);
                                                }
                                                if (userdeviceIdsArrayAndroid.Count() > 0)
                                                {

                                                    var UpdateCollapseKey = _ISchoolSettingService.GetSetUpdateSchoolData("CollapseKeyForNotification",
                                                                        (currentDate + "_" + CounterNotification.ToString()), null, "UniqueKey for Notifications on APP").AttributeValue;

                                                    _IWorkflowMessageService.SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, triggermessage, "Android", UpdateCollapseKey);
                                                }
                                            }

                                        }
                                    }
                                }

                                if (trigger != null && triggershooterSMS != null)
                                {
                                    if ((bool)StuDefaultSettings.SMS)
                                    {
                                        foreach (var item in Students)
                                        {
                                            var errormessage = "";
                                            var content = "";
                                            var Student = _IStudentService.GetAllStudents(ref count).Where(m => m.StudentId == item.StudentId).FirstOrDefault();
                                            string SendMsgMobileNo = _ICommonMethodService.ChooseStudentMobileNo(StuDefaultSettings.SelectedMobile, item.StudentId);
                                            if (Student != null && !string.IsNullOrEmpty(SendMsgMobileNo))
                                            {
                                                if ((bool)StuDefaultSettings.ToNonAppUser)
                                                {
                                                    bool YesAppUser = _ICommonMethodService.CheckAppUser(item.StudentId, (bool)StuDefaultSettings.ToNonAppUser, contactType.ContactTypeId);
                                                    if (!YesAppUser)
                                                    {
                                                        if (!string.IsNullOrEmpty(SendMsgMobileNo))
                                                        {
                                                            _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //MaxNoOfRecipientsinSMS
                                                    _ISMSSender.SendSMS(SendMsgMobileNo, triggermessageSMS, false, out errormessage, out content);
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }
                            #endregion

                        }



                        return Json(new
                        {
                            status = "success",
                            msg = eventtype + " has been Added",
                        });
                    }


                }

                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = validation
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
            }
            return RedirectToAction("General", "Error");
        }

        public ActionResult DeleteSchedularEvent(string i, string si)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Event", "Delete Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            int event_id = 0;
            // check id exist
            if (!string.IsNullOrEmpty(i))
                int.TryParse(_ICustomEncryption.base64d(i), out event_id);
            var eventname = "";
            if (i != null)
            {
                var eventtobedeleted = _ISchedularService.GetEventById((int)event_id, true);

                eventtobedeleted.IsDeleted = true;
                _ISchedularService.UpdateEvent(eventtobedeleted);
                eventname = eventtobedeleted.EventType.EventType1;
            }
            return Json(new
            {
                status = "success",
                msg = eventname + " Deleted Sucessfully"
            });
        }

        //public ActionResult Edit(string EventId)
        //{
        //    SchoolUser user = new SchoolUser();
        //    // current user
        //    if (HttpContext.Session["SchoolDB"] != null)
        //    {
        //        // re-assign session
        //        var sessiondb = HttpContext.Session["SchoolDB"];
        //        HttpContext.Session["SchoolDB"] = sessiondb;

        //        user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
        //        if (user == null)
        //            return RedirectToAction("Login", "Home");
        //        SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
        //        // check authorization
        //        var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Events", "Edit Record");
        //        if (!isauth)
        //            return RedirectToAction("AccessDenied", "Error");
        //    }
        //    else
        //    {
        //        // get action name
        //        var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
        //        return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
        //    }
        //    try
        //    {
        //        // check id exist
        //        if (!string.IsNullOrEmpty(EventId))
        //        {
        //            // decrypt Id
        //            var qsid = _ICustomEncryption.base64d(EventId);
        //            int event_id = 0;
        //            if (int.TryParse(qsid, out event_id))
        //            {
        //                if (event_id != 0)
        //                {
        //                    var events = _ISchedularService.GetEventById(event_id, true);
        //                    var eventdetails = _ISchedularService.GetAllEventDetailList(ref count, EventId: event_id);

        //                    SchedularModel model = new SchedularModel();
        //                    SchedularImagesModel imageslistmodel = new SchedularImagesModel();
        //                    model.EncEventId = EventId;
        //                    model.EventTitle = events.EventTitle;
        //                    //if (events.ClassId == null)
        //                    //    model.ClassId = Convert.ToString(1.1);
        //                    //else
        //                    //    model.ClassId = Convert.ToString(events.ClassId);
        //                    if (events.StartDate != null)
        //                        model.StartDate = (events.StartDate.Value);
        //                    if (events.EndDate != null)
        //                        model.EndDate = (events.EndDate.Value);
        //                    if (events.StartTime != null)
        //                        model.StartTime = ConvertTime(events.StartTime.Value);
        //                    if (events.EndTime != null)
        //                        model.EndTime = ConvertTime(events.EndTime.Value);
        //                    model.Description = events.Description;
        //                    model.IsActive = (bool)events.IsActive;
        //                    model.Venue = events.Venue;
        //                    // create drop down for class
        //                    List<SelectListItem> Dropdownlist = new List<SelectListItem>();

        //                    var classlist = _ICatalogMasterService.GetAllClassMasters(ref count);
        //                    if (eventdetails.Count > 0)
        //                    {
        //                        var classidlist = eventdetails.Select(x => (int)x.ClassId);

        //                        foreach (var cil in eventdetails)
        //                        {
        //                            if (cil.ClassId == null)
        //                                Dropdownlist.Add(new SelectListItem { Selected = true, Text = "All Classes", Value = "1.1", Disabled = true });
        //                            else
        //                                Dropdownlist.Add(new SelectListItem { Selected = false, Text = "All Classes", Value = "1.1", Disabled = false });
        //                            foreach (var item in classlist)
        //                                if (cil.ClassId == item.ClassId)
        //                                    Dropdownlist.Add(new SelectListItem { Selected = true, Text = item.Class, Value = item.ClassId.ToString(), Disabled = true });
        //                                else
        //                                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Class, Value = item.ClassId.ToString(), Disabled = false });
        //                        }
        //                    }
        //                    else
        //                    {
        //                                Dropdownlist.Add(new SelectListItem { Selected = false, Text = "All Classes", Value = "1.1", Disabled = false });
        //                            foreach (var item in classlist)
        //                                    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Class, Value = item.ClassId.ToString(), Disabled = false });
        //                    }
        //                    model.AvailableClassList = Dropdownlist;

        //                    return Json(new
        //                    {
        //                        status = "success",
        //                        //msg = "Schedular has been Added",
        //                        model = model
        //                    });
        //                }
        //                else
        //                {
        //                    return Json(new
        //                    {
        //                        status = "failed",
        //                        msg = "NotValid",
        //                        data = "Please Select an Event"
        //                    });
        //                }
        //            }
        //        }
        //        return RedirectToAction("Page404", "Error");
        //    }
        //    catch (Exception ex)
        //    {
        //        _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
        //        return RedirectToAction("General", "Error");
        //    }
        //}

        #endregion

        #region Event Images

        public ActionResult Images(string i, string si,string Id)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Images", "View");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // check id exist
                if (!string.IsNullOrEmpty(i) && !string.IsNullOrEmpty(si))
                {
                    // decrypt Id
                    var id = "";
                    id = _ICustomEncryption.base64d(Id);

                    var qsid = _ICustomEncryption.base64d(i);
                    var qsvid = _ICustomEncryption.base64d(si);
                    int eventid = 0;
                    int Eventtypeid = 0;
                    if (int.TryParse(qsid, out eventid) && int.TryParse(qsvid, out Eventtypeid))
                    {
                        SchedularImagesModel imagesmodel = new SchedularImagesModel();
                        SchedularImagesModel imageslistmodel = new SchedularImagesModel();
                        imagesmodel.EncEventTypeid = i;
                        imagesmodel.EventType = (int)Eventtypeid;
                        imagesmodel.EncEventid = _ICustomEncryption.base64e(Eventtypeid.ToString());
                        var allimagelist = _ISchedularService.GetAllEventImageList(ref count, EventId: eventid).ToList();
                        var schoolid = HttpContext.Session["SchoolId"];
                        imagesmodel.EventCode = Id;
                        foreach (var ail in allimagelist)
                        {
                            imageslistmodel = new SchedularImagesModel();
                            imageslistmodel.ImageCaption = ail.ImageCaption;
                            imageslistmodel.EventImageid = ail.EventImageId;
                            //imageslistmodel.EventImageid = ail.EventImageId;
                            imageslistmodel.EncEventImageid = _ICustomEncryption.base64e(ail.EventImageId.ToString());
                            imageslistmodel.ImageName = _WebHelper.GetStoreLocation() + "/Images/" + schoolid + "/Events/Activity/" + eventid + "/" + ail.ImageName;
                            imagesmodel.imagelist.Add(imageslistmodel);
                        }
                        imagesmodel.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Images", "Add Record");
                        imagesmodel.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Images", "Delete Record");
                        return View(imagesmodel);
                    }
                }

                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult DeleteSchedularEventImages(string id)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Images", "Delete Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // check id exist
                if (!string.IsNullOrEmpty(id))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(id);
                    int eventimage_id = 0;
                    if (int.TryParse(qsid, out eventimage_id))
                    {

                        if (id != null)
                        {
                            var schoolid = HttpContext.Session["SchoolId"];
                            var eventid = _ISchedularService.GetEventImageById((int)eventimage_id, true).EventId;

                            var imagename = _ISchedularService.GetEventImageById((int)eventimage_id, true).ImageName;
                            var imagepath = Path.Combine(Server.MapPath("~\\Images\\" + schoolid + "\\Events\\Activity\\" + eventid + "\\" + imagename));
                            //var imagepath = _WebHelper.GetStoreLocation() + "Images/" + schoolid + "/Events/Activity/" + eventid + "/" + imagename;

                            System.IO.File.Delete(imagepath);
                            _ISchedularService.DeleteEventImage((int)eventimage_id);
                            //id = eventid;
                        }
                        return Json(new
                        {
                            status = "success",
                            msg = "Image has been deleted",
                        });
                    }
                }

                return RedirectToAction("Page404", "Error");

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #region EventCircular
        [HttpPost]
        public ActionResult UploadCircularDocs()
        {
            if (Request.Files.Count > 0)
            {
                string path = String.Empty;
                string fileName = "";
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //file
                        HttpPostedFileBase file = files[i];
                        string unique_no = Request.Form["uniqueno"];
                        string uniqueimage = Request.Form["uniqueimage"];


                        //check that the file is of the permitted file type
                        string fileExtension = Path.GetExtension(file.FileName).ToLower();
                        if (fileExtension == ".png" || fileExtension == ".jpg" || fileExtension == ".jpeg")
                            fileName = "CirImage_" + DateTime.Now.ToString("yyyy-MM-dd HHmmtt").Replace(" ", "-") + Path.GetExtension(file.FileName).ToLower();
                        else if (fileExtension == ".pdf")
                            fileName = "CircularPdfDocument_" + DateTime.Now.ToString("yyyy-MM-dd HHmmtt").Replace(" ", "-") + Path.GetExtension(file.FileName).ToLower();
                        else if (fileExtension == ".doc" || fileExtension == ".docx")
                            fileName = "CircularDocument_" + DateTime.Now.ToString("yyyy-MM-dd HHmmtt").Replace(" ", "-") + Path.GetExtension(file.FileName).ToLower();

                        // check directory exist or not
                        var schoolid = HttpContext.Session["SchoolId"];

                        var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Events/Activity/" + unique_no));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Events/Activity/" + unique_no), fileName);

                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);

                        file.SaveAs(path);
                    }

                    // Returns message that successfully uploaded  
                    return Json(new
                    {
                        status = "success",
                        fileName = fileName,
                        msg = "File Uploaded Successfully!"
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        status = "error",
                        msg = "Error occurred. Error details: " + ex.Message
                    });
                }
            }
            else
            {
                return Json(new
                {
                    status = "error",
                    msg = "No files selected."
                });
            }
        }

        public ActionResult DownloadDocument(string i, string si)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Images", "Delete Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                var schoolid = HttpContext.Session["SchoolId"];

                // decrypt values
                int EventId = 0;
                if (!string.IsNullOrEmpty(i))
                    int.TryParse(_ICustomEncryption.base64d(i), out EventId);

                // decrypt values
                int EventTypeId = 0;
                if (!string.IsNullOrEmpty(si))
                    int.TryParse(_ICustomEncryption.base64d(si), out EventTypeId);

                string DocumentPath = "";
                if (EventId > 0)
                {
                    var CircularDocs = _ISchedularService.GetAllEventCircularDocs(ref count, EventId: EventId).FirstOrDefault();
                    if (CircularDocs != null)
                    {
                        if (!string.IsNullOrEmpty(CircularDocs.FileName))
                        {
                            var fileExtension = CircularDocs.FileName.Split('.')[1];
                            DocumentPath = "~/Images/" + schoolid + "/Events/Activity/" + EventTypeId + "/" + CircularDocs.FileName;
                            var path = Path.Combine(Server.MapPath(DocumentPath));

                            FileInfo file = new FileInfo(path);
                            if (!System.IO.File.Exists(path))
                            {
                                TempData["errorEventMsg"] = "File does not exists";
                                return RedirectToAction("Event");
                            }

                            string name = Path.GetFileName(DocumentPath);
                            string ext = Path.GetExtension(DocumentPath); //get file extension
                            string type = "";
                            // set known types based on file extension
                            if (ext != null)
                            {
                                switch (ext.ToLower())
                                {
                                    case ".GIF":
                                        type = "image/GIF";
                                        break;

                                    case ".jpeg":
                                        type = "image/jpeg";
                                        break;

                                    case ".jpg":
                                        type = "image/jpg";
                                        break;

                                    case ".png":
                                        type = "image/png";
                                        break;

                                    case ".pdf":
                                        type = "application/pdf";
                                        break;

                                    case ".doc":
                                    case ".rtf":
                                        type = "application/msword";
                                        break;
                                    case ".docx":
                                        type = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                                        break;
                                }
                            }
                            Response.Clear();           // Already have this
                            Response.ClearContent();    // Add this line
                            Response.ClearHeaders();
                            Response.ContentType = type;
                            Response.AddHeader("content-disposition", "attachment; filename=" + CircularDocs.FileName.Split('.')[0].Replace("_", "") + ext);
                            Response.AddHeader("Content-Length", file.Length.ToString());

                            Response.TransmitFile(DocumentPath);
                            //Response.BinaryWrite(Response.TransmitFile(DocumentPath).ToArray());
                            Response.End(); //give POP to user for file downlaod

                            return null;
                        }
                    }
                }
                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #endregion

        #region Ajax methods

        //get fields to be shown
        public ActionResult Getfieldstobeshown(int EventTypeId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                if (EventTypeId != null)
                {
                    var EventCols = _ISchedularService.GetAllEventColsList(ref count, EventTypeId: EventTypeId).ToList();
                    var enabledfieldsnames = (from ec in EventCols select ec.ColName).ToList();
                    IList<SelectListItem> hiddencolumnlist = new List<SelectListItem>();
                    foreach (var efn in enabledfieldsnames)
                        hiddencolumnlist.Add(new SelectListItem { Selected = false, Text = efn });

                    return Json(new
                    {
                        status = "success",
                        hiddencolumnlist = hiddencolumnlist
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        data = "Please Select an Event"
                    });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult SaveImages(SchedularImagesModel SchedularImagesModel)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Schedular", "Images", "Add Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // check id exist
                if (!string.IsNullOrEmpty(SchedularImagesModel.EncEventTypeid))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(SchedularImagesModel.EncEventTypeid);
                    int eventtype_id = 0;
                    if (int.TryParse(qsid, out eventtype_id))
                    {
                        SchedularImagesModel.EventTypeid = eventtype_id;
                    }
                }
                IList<SchedularImagesModel> imageslist = new List<SchedularImagesModel>();
                var schoolid = HttpContext.Session["SchoolId"];
                if (SchedularImagesModel.EventImagepath.Contains("ε"))
                {
                    var ImagePathSplit = SchedularImagesModel.EventImagepath.Split('ε');
                    foreach (var imgpth in ImagePathSplit)
                    {
                        var SIM = new SchedularImagesModel();
                        int id = SchedularImagesModel.EventTypeid;
                        EventImage imagesmodel = new EventImage();
                        imagesmodel.ImageCaption = SchedularImagesModel.ImageCaption;
                        imagesmodel.ImageName = imgpth.Split('\\').Last();
                        imagesmodel.EventId = SchedularImagesModel.EventTypeid;
                        _ISchedularService.InsertEventImage(imagesmodel);

                        SIM.ImageCaption = SchedularImagesModel.ImageCaption;
                        SIM.EventImageid = imagesmodel.EventImageId;
                        SIM.EncEventImageid = _ICustomEncryption.base64e(imagesmodel.EventImageId.ToString());
                        SIM.ImageName = _WebHelper.GetStoreLocation() + "/Images/" + schoolid + "/Events/Activity/" + SchedularImagesModel.EventTypeid + "/" + imagesmodel.ImageName;
                        imageslist.Add(SIM);
                    }
                }
                else
                {
                    int id = SchedularImagesModel.EventTypeid;
                    EventImage imagesmodel = new EventImage();
                    imagesmodel.ImageCaption = SchedularImagesModel.ImageCaption;
                    imagesmodel.ImageName = SchedularImagesModel.EventImagepath.Split('\\').Last();
                    imagesmodel.EventId = SchedularImagesModel.EventTypeid;
                    _ISchedularService.InsertEventImage(imagesmodel);

                    SchedularImagesModel.ImageCaption = SchedularImagesModel.ImageCaption;
                    SchedularImagesModel.EventImageid = imagesmodel.EventImageId;
                    SchedularImagesModel.EncEventImageid = _ICustomEncryption.base64e(imagesmodel.EventImageId.ToString());
                    SchedularImagesModel.ImageName = _WebHelper.GetStoreLocation() + "/Images/" + schoolid + "/Events/Activity/" + SchedularImagesModel.EventTypeid + "/" + imagesmodel.ImageName;
                    imageslist.Add(SchedularImagesModel);
                }
                return Json(new
                {
                    status = "success",
                    imageslist = imageslist,
                    msg = "Image Uploaded Successfully!"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult UploadFile()
        {
            if (Request.Files.Count > 0)
            {
                string path = String.Empty;
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //file
                        HttpPostedFileBase file = files[i];
                        // get Filename
                        string fileName = "";
                        string unique_no = Request.Form["uniqueno"];
                        int uniqueno = 0;
                        string ImgCount = Request.Form["ImgCount"];
                        // check id exist
                        if (!string.IsNullOrEmpty(unique_no))
                        {
                            // decrypt Id
                            var qsid = _ICustomEncryption.base64d(unique_no);

                            if (int.TryParse(qsid, out uniqueno))
                            {

                            }
                        }
                        int intImgCount=0;
                        if (!string.IsNullOrEmpty(ImgCount))
                            intImgCount = Convert.ToInt32(ImgCount);

                        string uniqueimage = Request.Form["uniqueimage"];
                        var allimagelist = _ISchedularService.GetAllEventImageList(ref count).ToList();
                        var noofimages = (from ail in allimagelist where ail.EventId == Convert.ToInt32(uniqueno) select ail).ToList();

                        int imagecounter = 0;
                       
                        if (noofimages.Count == 0)
                        {
                            imagecounter = 1;
                            if (intImgCount > 0)
                            {
                                imagecounter += intImgCount;
                                fileName = "Image" + uniqueno + "_00" + imagecounter + Path.GetExtension(file.FileName).ToLower();
                            }
                            else
                            {
                                fileName = "Image" + uniqueno + "_001" + Path.GetExtension(file.FileName).ToLower();
                            }
                        }
                        else
                        {
                            if (noofimages.Count < 9)
                            {
                                imagecounter = noofimages.Count;

                                if (intImgCount > 0)
                                {
                                    imagecounter += intImgCount;
                                }

                                imagecounter++;
                                fileName = "Image" + uniqueno + "_00" + imagecounter + Path.GetExtension(file.FileName).ToLower();
                            }
                            else if (noofimages.Count < 99)
                            {
                                imagecounter = noofimages.Count;
                                if (intImgCount > 0)
                                {
                                    imagecounter += intImgCount;
                                }
                                imagecounter++;
                                fileName = "Image" + uniqueno + "_0" + imagecounter + Path.GetExtension(file.FileName).ToLower();
                            }
                            else if (noofimages.Count >= 99)
                            {
                                imagecounter = noofimages.Count;
                                if (intImgCount > 0)
                                {
                                    imagecounter += intImgCount;
                                }
                                imagecounter++;
                                fileName = "Image" + uniqueno + "_" + imagecounter + Path.GetExtension(file.FileName).ToLower();
                            }
                        }

                        //Is the file too big to upload?
                        int fileSize = file.ContentLength;
                        if (fileSize > (maxFileSize * 1024))
                        {
                            return Json(new
                            {
                                status = "error",
                                msg = "Filesize of image is too large. Maximum file size permitted is " + maxFileSize + "KB"
                            });
                        }
                        //check that the file is of the permitted file type
                        string fileExtension = Path.GetExtension(fileName).ToLower();

                        string[] acceptedFileTypes = new string[3];
                        acceptedFileTypes[0] = ".jpg";
                        acceptedFileTypes[1] = ".jpeg";
                        acceptedFileTypes[2] = ".png";

                        bool acceptFile = false;
                        //should we accept the file?
                        for (int j = 0; j < acceptedFileTypes.Count(); j++)
                        {
                            if (fileExtension == acceptedFileTypes[j])
                            {
                                //accept the file, yay!
                                acceptFile = true;
                                break;
                            }

                            if (!acceptFile)
                                return Json(new
                                {
                                    status = "error",
                                    msg = "The file you are trying to upload is not a permitted file type!"
                                });
                        }
                        // check directory exist or not
                        var schoolid = HttpContext.Session["SchoolId"];

                        var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Events/Activity/" + uniqueno));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Events/Activity/" + uniqueno), fileName);

                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);

                        file.SaveAs(path);
                    }

                    // Returns message that successfully uploaded  
                    return Json(new
                    {
                        status = "success",
                        path = path,
                        msg = "File Uploaded Successfully!"
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        status = "error",
                        msg = "Error occurred. Error details: " + ex.Message
                    });
                }
            }
            else
            {
                return Json(new
                {
                    status = "error",
                    msg = "No files selected."
                });
            }
        }

        public ActionResult UpdateEventImageCaption(string EditableEventImageId, string CaptionValue)
        {
            try
            {
                int EventImageId = 0;
                if (!string.IsNullOrEmpty(EditableEventImageId))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(EditableEventImageId);
                    if (int.TryParse(qsid, out EventImageId))
                    {
                    }
                    if (string.IsNullOrEmpty(CaptionValue))
                    {
                        return Json(new
                        {
                            status = "error",
                            msg = "Invalid Image Caption"
                        });
                    }
                    var EventImage = _ISchedularService.GetEventImageById(EventImageId, true);
                    if (EventImage != null)
                    {
                        EventImage.ImageCaption = CaptionValue;
                        _ISchedularService.UpdateEventImage(EventImage);
                        return Json(new
                        {
                            status = "success",
                            msg = "Image Caption Updated Successfully"
                        });
                    }
                    return Json(new
                    {
                        status = "error",
                        msg = "Invalid Image Caption"
                    });
                }
                return Json(new
                {
                    status = "error",
                    msg = "Invalid Image Caption"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return RedirectToAction("General", "Error");
            }

        }

        public ActionResult ImageList(string EncEventID)
        {
            try
            {
                if (!string.IsNullOrEmpty(EncEventID))
                {
                    var qsid = _ICustomEncryption.base64d(EncEventID);
                    int eventid = 0;
                    if (int.TryParse(qsid, out eventid))
                    {
                        
                    }

                    var allimagelist = _ISchedularService.GetAllEventImageList(ref count, EventId: eventid).ToList();
                    var schoolid = HttpContext.Session["SchoolId"];
                    var imageslistmodel = new SchedularImagesModel();
                    SchedularImagesModel imagesmodel = new SchedularImagesModel();
                    foreach (var ail in allimagelist)
                    {
                        imageslistmodel = new SchedularImagesModel();
                        imageslistmodel.ImageCaption = ail.ImageCaption;
                        imageslistmodel.EventImageid = ail.EventImageId;
                        //imageslistmodel.EventImageid = ail.EventImageId;
                        imageslistmodel.EncEventImageid = _ICustomEncryption.base64e(ail.EventImageId.ToString());
                        imageslistmodel.ImageName = _WebHelper.GetStoreLocation() + "/Images/" + schoolid + "/Events/Activity/" + eventid + "/" + ail.ImageName;
                        imagesmodel.imagelist.Add(imageslistmodel);
                    }
                    return Json(new {status="success", imagelist = imagesmodel.imagelist }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { status = "error", msg="Invalid Event" }, JsonRequestBehavior.AllowGet);
            }
                 catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return RedirectToAction("General", "Error");
            }
        }
        #endregion

    }
}