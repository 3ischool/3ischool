﻿using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ContactModule;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.FeeModule;
using DAL.DAL.Kendo;
using DAL.DAL.SettingService;
using DAL.DAL.StudentModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Student;
using ViewModel.ViewModel.StudentPromotion;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.Controllers
{
    public class PromotionController : Controller
    {
        // GET: Promotion

        #region fields

        public int count = 0;
        private readonly IDropDownService _IDropDownService;
        private readonly ILogService _Logger;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStudentService _IStudentService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IWebHelper _WebHelper;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly ISubjectService _ISubjectService;
        private readonly IFeeService _IFeeService;
        private readonly IContactService _IContactService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly ISchoolDataService _ISchoolDataService;



        #endregion

        #region ctor

        public PromotionController(IDropDownService IDropDownService,
            ILogService Logger,
            ICatalogMasterService ICatalogMasterService,
            IStudentService IStudentService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            IUserService IUserService, ISchoolDataService ISchoolDataService,
            IConnectionService IConnectionService,
            IWebHelper WebHelper,
            IUserAuthorizationService IUserAuthorizationService,
            ISchoolSettingService ISchoolSettingService,
            ISubjectService ISubjectService,
            IFeeService IFeeService,
            IContactService IContactService,
            IAddressMasterService IAddressMasterService)
        {
            this._IDropDownService = IDropDownService;
            this._Logger = Logger;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IStudentService = IStudentService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._IActivityLogService = IActivityLogService;
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._WebHelper = WebHelper;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._ISubjectService = ISubjectService;
            this._IFeeService = IFeeService;
            this._IContactService = IContactService;
            this._IAddressMasterService = IAddressMasterService;
            this._ISchoolDataService = ISchoolDataService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }
        //Convert date
        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }
        #endregion

        #region Promotion

        public IList<ClassMaster> GetNextClasses(int ClassId)
        {
            IList<ClassMaster> classes = new List<ClassMaster>();
            var prvclass = _ICatalogMasterService.GetClassMasterById(ClassId);
            if (prvclass != null)
            {
                var prvstandardId = prvclass.StandardId;
                var prvstandard = _ICatalogMasterService.GetStandardMasterById((int)prvstandardId);
                if (prvstandard != null)
                {
                    var prevstandardClass = _ICatalogMasterService.GetStandardMasterById((int)prvstandardId);

                    if (prevstandardClass != null)
                    {
                        var claslist = _ICatalogMasterService.GetAllClassMasters(ref count, prevstandardClass.StandardId);
                        foreach (var clsitem in claslist)
                        {
                            classes.Add(new ClassMaster { ClassId = clsitem.ClassId, Class = clsitem.Class });
                        }
                    }
                    var stdindex = prvstandard.NextStandardIndex;
                    var nextstandard = _ICatalogMasterService.GetAllStandardMasters(ref count, standardindex: stdindex).ToList();
                    if (nextstandard != null && nextstandard.Count > 0)
                    {
                        foreach (var item in nextstandard)
                        {
                            var nextstandardClass = _ICatalogMasterService.GetStandardMasterById(item.StandardId);

                            if (nextstandardClass != null)
                            {
                                var claslist = _ICatalogMasterService.GetAllClassMasters(ref count, nextstandardClass.StandardId);
                                foreach (var clsitem in claslist)
                                {
                                    classes.Add(new ClassMaster { ClassId = clsitem.ClassId, Class = clsitem.Class });
                                }
                            }

                        }
                    }
                }
            }

            return classes;
        }

        public PromotionModel preparemodel(PromotionModel model)
        {
            // prv session
            model.AvailableSession = _IDropDownService.GetSessionListStartWithcurrent();


            model.SessionId = _ICatalogMasterService.GetCurrentSession().SessionId;
            var session = _ICatalogMasterService.GetCurrentSession();
            if (session != null)
            {
                DateTime startdate = session.EndDate.Value.AddDays(1);
                session = _ICatalogMasterService.GetAllSessions(ref count).Where(f => f.StartDate == startdate).FirstOrDefault();
                model.NextSessionId = null;
                if (session != null)
                {
                    model.NextSessionId = session.SessionId;
                    model.AvailableNextSession = _IDropDownService.GetSessionListStartWithcurrent();
                }
                else
                    model.AvailableNextSession.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            }

            model.CurSessionId = _ICatalogMasterService.GetCurrentSession().SessionId;
            // classes
            // var getprvsessiion = _ICatalogMasterService.GetPreviousSession();
            var prvsessionId = model.CurSessionId; // previous year

            var prvsessionStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: prvsessionId).ToList();


            var currentsessionStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: model.NextSessionId).Select(p => p.StudentId).ToList();
            var classs = _ICatalogMasterService.GetAllClassMasters(ref count).Where(m => m.StandardMaster.NextStandardIndex!=null && m.StandardMaster.NextStandardIndex > 0);
            var maxStandardIndex = _ICatalogMasterService.GetAllStandardMasters(ref count).OrderByDescending(p => p.StandardIndex).Select(p => p.StandardIndex).FirstOrDefault();
            var notPromotedStudents = prvsessionStudents.Where(p => !currentsessionStudents.Contains(p.StudentId)).ToList();
            model.AvailableClasses.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
            foreach (var item in classs)
            {
                var notPromotedStudentsForClass = notPromotedStudents.Where(p => p.ClassId == item.ClassId).ToList();
                if (notPromotedStudentsForClass.Count > 0)
                {
                    var students = _IStudentService.GetStudentsforPromotion(PrvSessionId: model.CurSessionId,
                       CurSessionId: model.NextSessionId,
                       ClassId: item.ClassId,
                       nextClssId: 0
                       );
                    if (students.Count > 0)
                    {
                        if (notPromotedStudentsForClass.Count > 0 && item.StandardMaster.StandardIndex < maxStandardIndex)
                            model.AvailableClasses.Add(new SelectListItem { Selected = false, Value = item.ClassId.ToString(), Text = item.Class });
                    }
                }
            }

            //model.AvailableClasses = _IDropDownService.ClassList();
            // Students
            model.AvailableStudents.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });

            // class
            model.AvailableNextClasses.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            if (model.ClassId > 0)
            {
                // class drop down
                var classes = GetNextClasses((int)model.ClassId);
                foreach (var stdclass in classes)
                {
                    //var currentsessionclassStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: model.CurSessionId, ClassId: stdclass.ClassId).Select(p => p.StudentId).Count();
                    var currentsessionclassMaleStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: model.CurSessionId, ClassId: stdclass.ClassId).Where(s => s.Student.StudentStatusDetails.Any(d => d.StudentStatu.StudentStatus != "Left") && s.Student.Gender.Gender1.ToLower() == "male").Select(p => p.StudentId).Count();
                    var currentsessionclassFemaleStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: model.CurSessionId, ClassId: stdclass.ClassId).Where(s => s.Student.StudentStatusDetails.Any(d => d.StudentStatu.StudentStatus != "Left") && s.Student.Gender.Gender1.ToLower() == "female").Select(p => p.StudentId).Count();

                    model.AvailableNextClasses.Add(new SelectListItem { Selected = true, Text = stdclass.Class + "(M-" + currentsessionclassMaleStudents + " , F-" + currentsessionclassFemaleStudents + " )", Value = stdclass.ClassId.ToString() });
                }
            }
            else
            {
                var nxtsessionStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: model.NextSessionId).ToList();
                foreach (var item in classs)
                {
                    var PromotedStudentsForClass = nxtsessionStudents.Where(p => p.ClassId == item.ClassId).ToList();
                    if (PromotedStudentsForClass.Count > 0)
                    {
                        model.AvailableNextClasses.Add(new SelectListItem { Selected = false, Value = item.ClassId.ToString(), Text = item.Class });
                    }
                }
            }

            return model;
        }

        public bool Isadmissionfeesubmitted(int studentid, int standardId, int SessionId = 0)
        {
            //var session = _ICatalogMasterService.GetCurrentSession();
            //if (SessionId != 0)
            //    session = _ICatalogMasterService.GetSessionById(SessionId: SessionId);

            // get fee class group by standard
            //var feeclassgroupdet = _IFeeService.GetAllFeeClassGroupDetails(ref count, StandardId: standardId).FirstOrDefault();
            //if (feeclassgroupdet != null)
            //{
            //    IList<int> feeperiodids = new List<int>();
            //    // get group fee type by Fee Class Group
            //    var groupfeetype = _IFeeService.GetAllGroupFeeTypes(ref count, FeeClassGroupId: feeclassgroupdet.FeeClassGroupId).ToList();
            //    // fee type ids array
            //    var feetypeids = groupfeetype.Select(g => (int)g.FeeTypeId).ToArray();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
            //    // get fee types
            //    var feetypes = _IFeeService.GetFeeTypesByClass(feetypeids);
            //    var feetypeid = feetypes.Where(m => m.FeeFrequency != null).FirstOrDefault() != null ? feetypes.Where(m => m.FeeFrequency != null).FirstOrDefault().FeeTypeId : 0;

            // fee receipt by student and session
            var feereceipts = _IFeeService.GetAllFeeReceipts(ref count, StudentId: studentid, SessionId: SessionId).ToList();
            // check session date
            feereceipts = feereceipts.Where(f => f.FeePeriodId != null).ToList();
            // receipt ids
            var feereceiptids = feereceipts.Select(f => f.ReceiptId).ToArray();
            // get fee period ids
            //foreach (var feercpt in feereceipts)
            //    feeperiodids.Add((int)feercpt.FeePeriodId);

            // get pending  fee period ids from detail table
            var feereceiptdetail = _IFeeService.GetAllFeeReceiptDetails(ref count).Where(f => feereceiptids.Contains((int)f.ReceiptId)).ToList();
            //feereceiptdetail = feereceiptdetail.Where(f => feereceiptids.Contains((int)f.ReceiptId) ).ToList();
            if (feereceiptdetail.Count > 0)
            {
                return true;
            }
            else return false;
            //}
            //else return false;
        }

        public ActionResult Promotion()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "Promotion", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new PromotionModel();
                model = preparemodel(model);
                model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
                model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");

                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "Promotion", "Edit Record");
                model.IspayAdmnFee = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "Promotion", "Pay Fee");
                model.IsRollBack = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "Promotion", "Update Record");

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult GetStudentListFromSession(DataSourceRequest command, PromotionModel model
                                           , IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            try
            {

                int ClassIdk = 0, SessionIdk = 0, NextSessionIdk = 0, NextClassIdk = 0;
                if (model.SessionId != null)
                    SessionIdk = Convert.ToInt32(model.SessionId);
                if (model.ClassId != null)
                    ClassIdk = Convert.ToInt32(model.ClassId);
                if (model.NextSessionId != null)
                    NextSessionIdk = Convert.ToInt32(model.NextSessionId);
                if (model.NextClassId != null)
                    NextClassIdk = Convert.ToInt32(model.NextClassId);

                var students = _IStudentService.GetStudentsforPromotion(PrvSessionId: SessionIdk,
                     CurSessionId: NextSessionIdk,
                     ClassId: ClassIdk,
                     nextClssId: NextClassIdk
                     );

                var schoolsetting = _ISchoolSettingService.GetorSetSchoolData("PayadmissionFeeOnPromote", "False", null).AttributeValue;
                var Data = students.PagedForCommand(command).Select(p =>
                {
                    var promotemodel = new PromotionStudentList();
                    promotemodel.StudentId = p.StudentId;
                    string grnder = "";
                    string btw = "";
                    if (p.Gender.Gender1 == "Male")
                    {
                        grnder = "M";
                        btw = "S/O-";
                    }
                    else if (p.Gender.Gender1 == "Female")
                    {
                        grnder = "F";
                        btw = "D/O-";
                    }
                    else
                    {
                        grnder = "T";
                        btw = "S/O-";
                    }
                    var guardian = p.Guardians.Where(f => f.StudentId == p.StudentId && f.GuardianTypeId == 1).FirstOrDefault();
                    if (guardian == null)
                        guardian = p.Guardians.Where(f => f.StudentId == p.StudentId).FirstOrDefault();
                    if (guardian != null)
                        promotemodel.FatherName = btw + guardian.FirstName + " " + guardian.LastName;
                    var stdcls = p.StudentClasses.Where(f => f.StudentId == p.StudentId && f.SessionId == SessionIdk).FirstOrDefault();
                    promotemodel.StudentName = p.FName + " " + p.LName + promotemodel.FatherName + "-(R.No.-" + stdcls.RollNo + ")/" + grnder;
                    promotemodel.Admnno = p.AdmnNo;
                    //var feereceipts = _IFeeService.GetAllFeeReceipts(ref count, StudentId: p.StudentId, SessionId: SessionIdk).ToList();
                    //feereceipts = feereceipts.Where(f => f.FeePeriodId != null).ToList();
                    //if(feereceipts.Count>0)
                    //{ promotemodel.Feepaid = true; }
                    //else
                    //{
                    //    promotemodel.Feepaid = false;
                    //}
                    if (schoolsetting != null)
                    {
                        if (schoolsetting.ToLower() == "false")
                        {
                            promotemodel.AllowPayFee = false;
                        }
                        else
                        {
                            promotemodel.AllowPayFee = true;
                            promotemodel.AllowPayFee = promotemodel.Feepaid;
                        }
                    }
                    promotemodel.SessionId = SessionIdk;
                    return promotemodel;
                }).AsQueryable().Sort(sort);
                var gridModel = new DataSourceResult
                {
                    Data = Data.ToList(),
                    Total = students.Count()
                };
                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }


        public ActionResult GetStudentListToSession(DataSourceRequest command, PromotionModel model
                                          , IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            try
            {

                int ClassIdk = 0, SessionIdk = 0, NextSessionIdk = 0, NextClassIdk = 0;
                if (model.SessionId != null)
                    SessionIdk = Convert.ToInt32(model.SessionId);
                if (model.ClassId != null)
                    ClassIdk = Convert.ToInt32(model.ClassId);
                if (model.NextSessionId != null)
                    NextSessionIdk = Convert.ToInt32(model.NextSessionId);
                if (model.NextClassId != null)
                    NextClassIdk = Convert.ToInt32(model.NextClassId);

                var students = _IStudentService.GetPromotedStudent(PrvSessionId: SessionIdk,
                     CurSessionId: NextSessionIdk,
                     ClassId: ClassIdk,
                     nextClssId: NextClassIdk
                     );
                var schoolsetting = _ISchoolSettingService.GetorSetSchoolData("RollBackAfterDays", "2", null).AttributeValue;
                var Data = students.PagedForCommand(command).Select(p =>
                {
                    var promotemodel = new PromotionStudentList();
                    promotemodel.StudentId = p.StudentId;
                    string grnder = "";
                    string btw = "";
                    if (p.Gender.Gender1 == "Male")
                    {
                        grnder = "M";
                        btw = "S/O-";
                    }
                    else if (p.Gender.Gender1 == "Female")
                    {
                        grnder = "F";
                        btw = "D/O-";
                    }
                    else
                    {
                        grnder = "T";
                        btw = "S/O-";
                    }
                    var guardian = p.Guardians.Where(f => f.StudentId == p.StudentId && f.GuardianTypeId == 1).FirstOrDefault();
                    if (guardian == null)
                        guardian = p.Guardians.Where(f => f.StudentId == p.StudentId).FirstOrDefault();
                    if (guardian != null)
                        promotemodel.FatherName = btw + guardian.FirstName + " " + guardian.LastName;
                    var stdcls = p.StudentClasses.Where(f => f.StudentId == p.StudentId && f.SessionId == NextSessionIdk).FirstOrDefault();
                    promotemodel.StudentName = p.FName + " " + p.LName + promotemodel.FatherName + "-(R.No.-" + stdcls.RollNo + ")/" + grnder;
                    promotemodel.Admnno = p.AdmnNo;
                    promotemodel.PromotedOn = ConvertDate(Convert.ToDateTime(stdcls.PromotedOn));
                    if (promotemodel.PromotedOn == "01/01/0001")
                    {
                        promotemodel.AllowPayFee = false;
                        promotemodel.PromotedOn = "";
                    }
                    else
                    {
                        int days = Convert.ToInt32(schoolsetting) * -1;
                        DateTime dt = DateTime.UtcNow.AddDays(days);
                        if (stdcls.PromotedOn >= dt)
                        {
                            promotemodel.AllowPayFee = true;
                        }
                        else
                        {
                            promotemodel.AllowPayFee = false;
                        }
                    }

                    var feereceipts = _IFeeService.GetAllFeeReceipts(ref count, StudentId: p.StudentId, SessionId: NextSessionIdk).ToList();
                    feereceipts = feereceipts.Where(f => f.FeePeriodId != null).ToList();
                    if (feereceipts.Count > 0)
                    { promotemodel.Feepaid = true; }
                    else
                    {
                        promotemodel.Feepaid = false;
                    }


                    promotemodel.Gender = p.Gender.Gender1;
                    promotemodel.SessionId = NextSessionIdk;
                    return promotemodel;
                }).AsQueryable().Sort(sort);
                var gridModel = new DataSourceResult
                {
                    Data = Data.ToList(),
                    Total = students.Count()
                };
                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }
        [HttpPost]
        public ActionResult SaveStudentForPromotion(string PreviousSession, string PreviousClass, string NextSession, string NextClass, string StudentIds)
        {
            SchoolUser user = new SchoolUser();

            try
            {
                int prvSessionidk = 0, prvClassidk = 0, nxtSessionidk = 0, nxtClassidk = 0;
                if (!string.IsNullOrEmpty(PreviousSession))
                    prvSessionidk = Convert.ToInt32(PreviousSession);
                if (!string.IsNullOrEmpty(PreviousClass))
                    prvClassidk = Convert.ToInt32(PreviousClass);
                if (!string.IsNullOrEmpty(NextSession))
                    nxtSessionidk = Convert.ToInt32(NextSession);
                if (!string.IsNullOrEmpty(NextClass))
                    nxtClassidk = Convert.ToInt32(NextClass);

                var previousclassdet = _ICatalogMasterService.GetClassMasterById(prvClassidk);
                var previoussessiondet = _ICatalogMasterService.GetSessionById(prvSessionidk);
                if (nxtClassidk > 0)
                {
                    var nextclassdet = _ICatalogMasterService.GetClassMasterById(nxtClassidk);
                    var nextsessiondet = _ICatalogMasterService.GetSessionById(nxtSessionidk);
                    if (!string.IsNullOrEmpty(StudentIds))
                    {
                        if (prvSessionidk == nxtSessionidk)
                        {
                            dynamic stuff = JsonConvert.DeserializeObject(StudentIds);
                            foreach (var item in stuff)
                            {
                                var studentclass = _IStudentService.GetAllStudentClasss(ref count, SessionId: prvSessionidk, StudentId: (int)item.id).FirstOrDefault();
                                StudentSectionChange Sectionchange = new StudentSectionChange();
                                Sectionchange.ClassId = studentclass.ClassId;
                                if (studentclass.SectionEffectiveDate == null)
                                {
                                    var session = _ICatalogMasterService.GetSessionById((int)studentclass.SessionId);
                                    if (session != null)
                                        Sectionchange.EffectiveDate = session.StartDate;
                                }
                                else
                                {
                                    Sectionchange.EffectiveDate = studentclass.SectionEffectiveDate;
                                }
                                Sectionchange.EndDate = DateTime.UtcNow;
                                Sectionchange.HouseId = studentclass.HouseId;
                                Sectionchange.RollNo = studentclass.RollNo;
                                Sectionchange.SessionId = studentclass.SessionId;
                                Sectionchange.StudentClassId = studentclass.StudentClassId;
                                _IStudentService.InsertChangeSectionDetail(Sectionchange);

                                studentclass.ClassId = nxtClassidk;
                                studentclass.SectionEffectiveDate = DateTime.UtcNow;
                                studentclass.RollNo = "";
                                studentclass.HouseId = studentclass.HouseId;
                                _IStudentService.UpdateStudentClass(studentclass);
                            }

                        }
                        else
                        {
                            dynamic stuff = JsonConvert.DeserializeObject(StudentIds);
                            foreach (var item in stuff)
                            {
                                var studentclass = _IStudentService.GetAllStudentClasss(ref count, SessionId: prvSessionidk, StudentId: (int)item.id).FirstOrDefault();

                                StudentClass stdcls = new StudentClass();
                                stdcls.StudentId = item.id;
                                stdcls.ClassId = nxtClassidk;
                                stdcls.SessionId = nxtSessionidk;
                                stdcls.RollNo = "";
                                stdcls.HouseId = studentclass != null ? studentclass.HouseId : null;
                                stdcls.PromotedOn = DateTime.UtcNow;
                                _IStudentService.InsertStudentClass(stdcls);
                            }
                        }
                    }
                    return Json(new
                    {
                        status = "success",
                        msg = "Promotion from " + previousclassdet.Class + " to section  "
                                                + nextclassdet.Class + " from session " + previoussessiondet.Session1 + " to session "
                                                + nextsessiondet.Session1 + " has been done successfully"
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = "error",
                        msg = "Please select To Class"
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        
        [HttpPost]
        public ActionResult RollbackPromotedStudents(string NextSession, string NextClass, string StudentIds)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Student", "AssignRollNoSection", "Add Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                int nxtSessionidk = 0, nxtClassidk = 0;

                if (!string.IsNullOrEmpty(NextSession))
                    nxtSessionidk = Convert.ToInt32(NextSession);
                if (!string.IsNullOrEmpty(NextClass))
                    nxtClassidk = Convert.ToInt32(NextClass);
                int count = 0;
                if (!string.IsNullOrEmpty(StudentIds))
                {
                    dynamic stuff = JsonConvert.DeserializeObject(StudentIds);
                    foreach (var item in stuff)
                    {
                        StudentClass stdcls = new StudentClass();
                        stdcls = _IStudentService.GetAllStudentClasss(ref count, SessionId: nxtSessionidk, ClassId: nxtClassidk, StudentId: (int)item.id).FirstOrDefault();
                        if (stdcls != null)
                        {
                            var studentsectionchange = _IStudentService.GetSectionChangeDetail(StudentClassId: (int)stdcls.StudentClassId, SessionId: nxtSessionidk);
                            if (studentsectionchange == null)
                            {
                                if (stdcls != null)
                                {
                                    //check if studentclasssubject assigned
                                    var studentclassSubjects = _ISubjectService.GetAllStudentClassSubjects(ref count).Where(m => m.StudentClassId == stdcls.StudentClassId);
                                    var virtualclassstudentids = _ICatalogMasterService.GetAllVirtualClassStudents(ref count).Where(m => m.StudentClassId == stdcls.StudentClassId);
                                    var errormsg = "";
                                    var join = "";
                                    if (studentclassSubjects.Count() > 0){
                                        errormsg = "Subjects ";
                                         join = " and ";
                                    }
                                    if (studentclassSubjects.Count() > 0)
                                        errormsg +=join+ "Virtual class assigned to student";

                                    if (errormsg!="")
                                        return Json(new { status = "error", msg = errormsg }, JsonRequestBehavior.AllowGet);

                                    _IStudentService.DeleteStudentClass(stdcls.StudentClassId);
                                }
                            }
                            else
                            {
                                stdcls.ClassId = studentsectionchange.ClassId;
                                stdcls.RollNo = "";
                                stdcls.SectionEffectiveDate = DateTime.UtcNow;
                                _IStudentService.UpdateStudentClass(stdcls);

                                int StudentSectionChnageId = studentsectionchange.StudentSectionChangeId;
                                _IStudentService.DeleteStudentSectionChange(StudentSectionChnageId);

                            }
                        }
                    }
                }
                return Json(new { status = "success", msg = "Promotion Students are rollbacked successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult LoadNextClasses(string Sessionid, string ClassId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                int Sessionidk = 0, Classidk = 0;
                if (!string.IsNullOrEmpty(Sessionid))
                    Sessionidk = Convert.ToInt32(Sessionid);
                if (!string.IsNullOrEmpty(ClassId))
                    Classidk = Convert.ToInt32(ClassId);
                
                List<ClassMaster> NextClasses = new List<ClassMaster>();
                if (Classidk > 0)
                {
                    IList<ClassMaster> nextclasslist = GetNextClasses(Classidk);
                    foreach (var item in nextclasslist)
                    {
                        NextClasses.Add(new ClassMaster { Class = item.Class, ClassId = item.ClassId });
                    }
                }
                return Json(new { status = "success", NextClasses }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult GetClassacctoSession(string Sessionid)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                int Sessionidk = 0;
                if (!string.IsNullOrEmpty(Sessionid))
                    Sessionidk = Convert.ToInt32(Sessionid);

                List<ClassMaster> NextClasses = new List<ClassMaster>();
                List<SelectListItem> NextSessions = new List<SelectListItem>();

                //as discussed with harish sir
                //get next session studentclasses exclude those students from selected one and get distinct class list

                //get next session
                if(_ICatalogMasterService.GetAllSessions(ref count).Where(m => m.SessionId == Sessionidk).FirstOrDefault() != null)
                {
                    var nextsessiondate = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.SessionId == Sessionidk).FirstOrDefault().EndDate.Value.AddDays(2);

                    var nextsession = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.StartDate < nextsessiondate && m.EndDate > nextsessiondate).FirstOrDefault();

                    if (nextsession != null)
                    {
                        var studentClass = _IStudentService.GetAllStudentClasss(ref count).Where(m => m.SessionId == nextsession.SessionId).Select(m=>m.StudentId);

                       var classs = _IStudentService.GetAllStudentClasss(ref count, SessionId: Sessionidk).Where(m=> !studentClass.Contains(m.StudentId)).Select(m=>m.ClassMaster).Distinct();

                        foreach (var item in classs)
                        {
                          
                                NextClasses.Add(new ClassMaster { Class = item.Class, ClassId = item.ClassId });
                            
                        }

                    }
                }
               



                //var getprvsessiion = _ICatalogMasterService.GetPreviousSession();
                //var prvsessionId = getprvsessiion == null ? 0 : getprvsessiion.SessionId;
                //// previous year
                //                                                                          //var prvsessionStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: prvsessionId).ToList();
                //                                                                          //var currentsessionStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: Sessionidk).Select(p => p.StudentId).ToList();
                //                                                                          //var notPromotedStudents = prvsessionStudents.Where(p => !currentsessionStudents.Contains(p.StudentId)).ToList();

                //var prvsessionStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: prvsessionId).Select(p => p.StudentId).ToList();
                //var currentsessionStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: Sessionidk).ToList();
                //var notPromotedStudents = currentsessionStudents.Where(p => !prvsessionStudents.Contains(p.StudentId)).ToList();

                //List<ClassMaster> NextClasses = new List<ClassMaster>();
                //List<SelectListItem> NextSessions = new List<SelectListItem>();

                //var classs = _ICatalogMasterService.GetAllClassMasters(ref count);
                //var maxStandardIndex = _ICatalogMasterService.GetAllStandardMasters(ref count).OrderByDescending(p => p.StandardIndex).Select(p => p.StandardIndex).FirstOrDefault();

                //NextClasses.Add(new ClassMaster { ClassId = 0, Class = "--Select--" });
                //foreach (var item in classs)
                //{
                //    if (notPromotedStudents.Count > 0)
                //    {
                //        var notPromotedStudentsForClass = notPromotedStudents.Where(p => p.ClassId == item.ClassId).ToList();
                //        if (notPromotedStudentsForClass.Count > 0 && item.StandardMaster.StandardIndex < maxStandardIndex)
                //            NextClasses.Add(new ClassMaster { Class = item.Class, ClassId = item.ClassId });
                //    }
                //    else
                //    {
                //        NextClasses.Add(new ClassMaster { Class = item.Class, ClassId = item.ClassId });
                //    }
                //}

                NextSessions.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = false });
                if (Sessionidk > 0)
                {
                    var getNextSessions = _ICatalogMasterService.GetSessionById(Sessionidk);
                    foreach (var session in _ICatalogMasterService.GetAllSessions(ref count))
                    {
                        if (session.StartDate > getNextSessions.StartDate || session.EndDate > getNextSessions.EndDate)
                        {
                            NextSessions.Add(new SelectListItem
                            {
                                Value = session.SessionId.ToString(),
                                Text = session.Session1,
                                Selected = false
                            });

                        }
                    }
                }

                return Json(new { status = "success", NextClasses, NextSessions }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult GetClassesacctoSession(string Sessionid)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                int Sessionidk = 0;
                if (!string.IsNullOrEmpty(Sessionid))
                    Sessionidk = Convert.ToInt32(Sessionid);
                var currentsessionStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: Sessionidk).ToList();
                List<ClassMaster> NextClasses = new List<ClassMaster>();
                var classs = _ICatalogMasterService.GetAllClassMasters(ref count);
                var maxStandardIndex = _ICatalogMasterService.GetAllStandardMasters(ref count).OrderByDescending(p => p.StandardIndex).Select(p => p.StandardIndex).FirstOrDefault();
                NextClasses.Add(new ClassMaster { ClassId = 0, Class = "--Select--" });
                foreach (var item in classs)
                {
                    var notPromotedStudentsForClass = currentsessionStudents.Where(p => p.ClassId == item.ClassId).ToList();
                    if (notPromotedStudentsForClass.Count > 0)
                        NextClasses.Add(new ClassMaster { Class = item.Class, ClassId = item.ClassId });
                }
                return Json(new { status = "success", NextClasses }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost, ActionName("Promotion")]
        [FormValueRequired("Save")]
        public ActionResult Promotion(PromotionModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "Promotion", "Add Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                if (!string.IsNullOrEmpty(model.PromotionArray))
                {
                    string Notoficationmsg = string.Empty;
                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();

                    System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<PromotionStudentList> PromotionArray = js.Deserialize<List<PromotionStudentList>>(model.PromotionArray);
                    if (PromotionArray.Count > 0)
                    {
                        int rowno = 0;
                        foreach (var promote in PromotionArray)
                        {
                            var student = _IStudentService.GetStudentById(promote.StudentId);
                            if (student != null)
                            {
                                StudentClass stdclass = new StudentClass();
                                stdclass.SessionId = _ICatalogMasterService.GetCurrentSession().SessionId;
                                stdclass.ClassId = Convert.ToInt32(model.NextClassId);
                                stdclass.StudentId = student.StudentId;
                                _IStudentService.InsertStudentClass(stdclass);

                                // stdclass Id
                                var stdclasstable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StudentClass").FirstOrDefault();
                                if (stdclasstable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, stdclasstable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(stdclass.StudentClassId, stdclasstable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add StudentClass with StudentClassId:{0} and Student:{1}", stdclass.StudentClassId, stdclass.StudentId), Request.Browser.Browser);
                                }
                                // for roll no
                                rowno++;
                            }
                        }
                    }
                    var prvsessionId = _ICatalogMasterService.GetPreviousSession().SessionId; // previous year
                    var prvsession = _ICatalogMasterService.GetSessionById(prvsessionId);
                    var currentsession = _ICatalogMasterService.GetSessionById(model.CurSessionId);
                    var prvclass = _ICatalogMasterService.GetClassMasterById(ClassMasterId: (int)model.ClassId);
                    var nextClass = _ICatalogMasterService.GetClassMasterById(ClassMasterId: (int)model.NextClassId);
                    TempData["SuccessNotification"] = "Promotion from " + prvclass.Class + " to section  " + nextClass.Class + " from session " + prvsession.Session1 + " to session " + currentsession.Session1 + " has been done successfully";
                    return RedirectToAction("Promotion");
                }

                model = preparemodel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost, ActionName("Promotion")]
        [FormValueRequired("Search")]
        public ActionResult Promotion(PromotionModel model, FormCollection Form)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                if (ModelState.IsValid)
                {
                    var prvsessionId = _ICatalogMasterService.GetPreviousSession().SessionId; // previous year
                    var cursessionId = _ICatalogMasterService.GetCurrentSession().SessionId; // current year
                    int[] nextclassIds = null;
                    // next clases
                    if (model.ClassId != null && model.ClassId > 0)
                    {
                        var classes = GetNextClasses((int)model.ClassId);
                        nextclassIds = classes.Select(c => c.ClassId).ToArray();
                    }

                    var students = _IStudentService.GetStudentsforPromotion(PrvSessionId: prvsessionId,
                        CurSessionId: cursessionId,
                        ClassId: model.ClassId,
                        curclassids: nextclassIds
                        );

                    foreach (var student in students)
                    {
                        PromotionStudentList PromotionStudent = new PromotionStudentList();
                        PromotionStudent.Admnno = student.AdmnNo;
                        PromotionStudent.StudentName = student.FName + " " + student.LName;
                        PromotionStudent.FatherName = student.Guardians.Where(g => g.Relation.Relation1.ToLower() == "father").Select(g => g.FirstName + " " + g.LastName).FirstOrDefault();
                        PromotionStudent.StudentId = student.StudentId;
                        PromotionStudent.Gender = student.GenderId > 0 ? student.Gender.Gender1 : "";
                        PromotionStudent.RollNo = student.StudentClasses.Where(c => c.ClassId == model.ClassId && c.SessionId == prvsessionId).Select(c => c.RollNo).FirstOrDefault();
                        PromotionStudent.Feepaid = Isadmissionfeesubmitted(student.StudentId, student.StandardMaster.StandardId);
                        model.PromotionStudentList.Add(PromotionStudent);
                    }

                    if (students.Count == 0)
                        TempData["ErrorNotification"] = "Record not found";
                }

                model = preparemodel(model);
                //check authorization to add
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "Promotion", "Add Record");
                // set student names list
                foreach (var std in model.PromotionStudentList)
                    model.AvailableStudents.Add(new SelectListItem { Selected = true, Text = std.StudentName, Value = std.StudentId.ToString() });

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #region Assign RollNo

        public AssignRollnoModel prepareassginrollnomodel(AssignRollnoModel model)
        {
            // classes
            // get all student classes whose rollno null
            var cursessionId = _ICatalogMasterService.GetCurrentSession().SessionId;
            if (model.CurSessionId != null)
                cursessionId = (int)model.CurSessionId;
            // current year
            var allstudentclasses = _IStudentService.GetAllStudentClasss(ref count, cursessionId);

            model.AvailableSessions = _IDropDownService.SessionList();
            model.CurSessionId = cursessionId;
            // var classids
            var classids = allstudentclasses.Select(s => s.ClassId).Distinct().ToArray();
            // all class master
            var classes = _ICatalogMasterService.GetAllClassMasters(ref count);
            classes = classes.Where(c => classids.Contains(c.ClassId)).ToList();
            // ddl
            model.AvailableClasses.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            foreach (var clasmaster in classes)
                model.AvailableClasses.Add(new SelectListItem { Selected = true, Text = clasmaster.Class, Value = clasmaster.ClassId.ToString() });

            // setting
            model.IsSeprateTable = false;
            var Gridsetting = _ISchoolSettingService.GetAttributeValue("RollNoSeprateGrid");
            if (!string.IsNullOrEmpty(Gridsetting))
            {
                if (Gridsetting == "1")
                    model.IsSeprateTable = true;
            }

            var unassigned = 0;
            var noofrecord = 0;
            if (model.ClassId > 0)
            {
                var studentclasses = _IStudentService.GetStudentsByClass(CurSessionId: cursessionId, ClassId: model.ClassId);
                foreach (var studentclass in studentclasses)
                {
                    noofrecord++;
                    AdmitStudents PromotionStudent = new AdmitStudents();
                    PromotionStudent.Admnno = studentclass.Student.AdmnNo;
                    PromotionStudent.StudentName = studentclass.Student.FName + " " + studentclass.Student.LName;
                    PromotionStudent.FatherName = studentclass.Student.Guardians.Where(g => g.Relation.Relation1.ToLower() == "father").Select(g => g.FirstName + " " + g.LastName).FirstOrDefault();
                    PromotionStudent.StudentId = studentclass.Student.StudentId;
                    PromotionStudent.StudentClassId = studentclass.StudentClassId;
                    PromotionStudent.RollNo = studentclass.RollNo != null ? studentclass.RollNo : "";
                    PromotionStudent.Session = studentclass.Session.Session1;
                    PromotionStudent.Standard = studentclass.Student.StandardMaster.Standard;
                    PromotionStudent.FName = studentclass.Student.FName;
                    PromotionStudent.LName = studentclass.Student.LName;
                    PromotionStudent.AdhaarNo = studentclass.Student.AadharCardNo;
                    PromotionStudent.DOB = ConvertDate(studentclass.Student.DOB.Value);
                    var Stdcontacttype = _IAddressMasterService.GetAllContactTypes(ref count, "Student").FirstOrDefault(); // Student contact type
                    var contatinfotype = _IContactService.GetAllContactInfoTypes(ref count, "Email").FirstOrDefault(); // info type 
                    var stdcontactinfo = _IContactService.GetAllContactInfos(ref count,
                                    ContactInfoTypeId: contatinfotype.ContactInfoTypeId,
                                    ContactId: studentclass.Student.StudentId,
                                    ContactTypeId: Stdcontacttype.ContactTypeId).FirstOrDefault();
                    var contatinfotypemobile = _IContactService.GetAllContactInfoTypes(ref count, "Mobile").FirstOrDefault(); // info type 
                    var stdcontactinfomobile = _IContactService.GetAllContactInfos(ref count,
                                    ContactInfoTypeId: contatinfotypemobile.ContactInfoTypeId,
                                    ContactId: studentclass.Student.StudentId,
                                    ContactTypeId: Stdcontacttype.ContactTypeId).FirstOrDefault();
                    if (stdcontactinfo != null)
                        PromotionStudent.Email = stdcontactinfo.ContactInfo1;
                    if (stdcontactinfomobile != null)
                        PromotionStudent.MobileNo = stdcontactinfomobile.ContactInfo1;
                    PromotionStudent.Class = studentclass.ClassMaster.Class;
                    var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);

                    var schoolinfo = _ISchoolDataService.GetSchoolDataDetail(ref count).FirstOrDefault();
                    PromotionStudent.SchoolName = (schoolinfo != null && schoolinfo.SchoolName != null) ? schoolinfo.SchoolName : "";
                    PromotionStudent.SchoolLogoPath = (schoolinfo != null && schoolinfo.Logo != null) ? schoolinfo.Logo : "";
                    PromotionStudent.SchoolAddress = (schoolinfo != null && schoolinfo.SchoolAddress != null) ? schoolinfo.SchoolAddress : "";

                    //PromotionStudent.SchoolName = _ISchoolSettingService.GetSchoolSettingByAttribute("SchoolName").AttributeValue;
                    //PromotionStudent.SchoolLogoPath = _WebHelper.GetStoreLocation() + "/Images/" + SchoolDbId + "/logo.png";

                    // check student status
                    var studentstatus = studentclass.Student.StudentStatusDetails.OrderByDescending(d => d.StatusChangeDate).FirstOrDefault();
                    if (studentstatus != null)
                    {
                        PromotionStudent.StudentStatus = studentstatus.StudentStatu.StudentStatus;
                        PromotionStudent.Color = "";
                        if (studentstatus.StudentStatu.StudentStatus == "Left")
                            PromotionStudent.Color = "red";
                    }


                    PromotionStudent.Status = "Assigned";
                    if (string.IsNullOrEmpty(studentclass.RollNo))
                        PromotionStudent.Status = "UnAssigned";

                    PromotionStudent.Gender = studentclass.Student.GenderId > 0 ? studentclass.Student.Gender.Gender1 : "";
                    model.AdmitStudentslist.Add(PromotionStudent);

                    if (PromotionStudent.Status == "UnAssigned")
                        unassigned++;
                }
                TempData["ErrorNotification"] = "";
                if (studentclasses.Count == 0 && model.ClassId > 0)
                    TempData["ErrorNotification"] = "Record not found";
                else if (unassigned == 0 && noofrecord > 0)
                    TempData["ErrorNotification"] = "No Student with unassigned Roll no";
                else
                {
                    int rollno = 0;
                    var studentlist = _IStudentService.GetAllStudentClasss(ref count, ClassId: model.ClassId, SessionId: cursessionId);
                    if (studentlist.Count > 0)
                    {
                        var RollNo = studentlist.Where(s => s.RollNo != null).OrderByDescending(s => s.RollNo).Select(s => s.RollNo).FirstOrDefault();
                        if (!string.IsNullOrEmpty(RollNo))
                        {
                            rollno = Convert.ToInt32(studentlist.Where(s => s.RollNo != null).OrderByDescending(s => s.RollNo).Select(s => s.RollNo).FirstOrDefault());
                        }
                        model.RollNoStartFrom = (rollno != null ? rollno + 1 : rollno).ToString();
                    }
                }
            }

            return model;
        }

        public ActionResult AssignRollNo()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "AssignRollNo", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new AssignRollnoModel();
                model = prepareassginrollnomodel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "AssignRollNo", "Update Record");
                model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "AssignRollNo", "Edit Record");
                model.IsAuthToExport = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "AssignRollNo", "Export");
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost, ActionName("AssignRollNo")]
        [FormValueRequired("Save")]
        public ActionResult AssignRollNo(AssignRollnoModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "AssignRollNo", "Add Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                if (!string.IsNullOrEmpty(model.AssignedArray))
                {
                    string Notoficationmsg = string.Empty;
                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();

                    System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<AdmitStudents> AssignedArray = js.Deserialize<List<AdmitStudents>>(model.AssignedArray);
                    if (AssignedArray.Count > 0)
                    {
                        int rowno = 0;
                        foreach (var item in AssignedArray)
                        {
                            var studentclass = _IStudentService.GetStudentClassById(item.StudentClassId);
                            if (studentclass != null)
                            {
                                if (!string.IsNullOrEmpty(studentclass.RollNo))
                                    continue;

                                studentclass.RollNo = Convert.ToString(Convert.ToInt32(model.RollNoStartFrom) + rowno);
                                _IStudentService.UpdateStudentClass(studentclass);

                                // stdclass Id
                                var stdclasstable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StudentClass").FirstOrDefault();
                                if (stdclasstable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, stdclasstable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(studentclass.StudentClassId, stdclasstable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update StudentClass with StudentClassId:{0} and StudentId:{1}", studentclass.StudentClassId, studentclass.StudentId), Request.Browser.Browser);
                                }
                                // for roll no
                                rowno++;
                            }
                        }
                    }
                    TempData["SuccessNotification"] = "Roll No assigned successfully";
                    model = prepareassginrollnomodel(model);

                    model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "AssignRollNo", "Update Record");
                    model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "AssignRollNo", "Edit Record");
                    model.IsAuthToExport = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "AssignRollNo", "Export");
                    return View(model);
                }

                model = prepareassginrollnomodel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost, ActionName("AssignRollNo")]
        [FormValueRequired("Search")]
        public ActionResult AssignRollNo(AssignRollnoModel model, FormCollection Form)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {


                model = prepareassginrollnomodel(model);
                //check authorization to add
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "AssignRollNo", "Update Record");
                model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "AssignRollNo", "Edit Record");
                model.IsAuthToExport = _IUserAuthorizationService.IsUserAuthorize(user, "Promotion", "AssignRollNo", "Export");
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        // reset class rollno
        public ActionResult ResetClassRollNo(int classid)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                // cehck if class exist
                var isclass = _ICatalogMasterService.GetClassMasterById(classid);
                if (isclass != null)
                {
                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();

                    var sessionid = _ICatalogMasterService.GetCurrentSession().SessionId; // current year
                    var studentclass = _IStudentService.GetAllStudentClasss(ref count, SessionId: sessionid, ClassId: classid);
                    studentclass = studentclass.Where(s => s.RollNo != null).ToList();
                    // student ids
                    var studentids = studentclass.Select(s => s.StudentId).ToArray();

                    // check student id use in attendance
                    var currentsession = _ICatalogMasterService.GetCurrentSession();
                    // get current session attendance
                    //var attendance = _ISubjectService.GetAllAttendances(ref count);
                    //attendance = attendance.Where(a => studentids.Contains(a.StudentId) && a.AttendanceDate >= currentsession.StartDate && a.AttendanceDate <= currentsession.EndDate).ToList();
                    //if (attendance.Count > 0)
                    //{
                    //    return Json(new
                    //    {
                    //        status = "failed",
                    //        msg = "You can't reset Student Rollno."
                    //    });
                    //}

                    foreach (var item in studentclass)
                    {
                        item.RollNo = null;
                        _IStudentService.UpdateStudentClass(item);
                        // stdclass Id
                        var stdclasstable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StudentClass").FirstOrDefault();
                        if (stdclasstable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, stdclasstable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(item.StudentClassId, stdclasstable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update StudentClass with StudentClassId:{0} and StudentId:{1}", item.StudentClassId, item.StudentId), Request.Browser.Browser);
                        }
                    }

                    return Json(new
                    {
                        status = "success"
                    });
                }
                else
                    return Json(new
                    {
                        status = "failed",
                        msg = "Class not found"
                    });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "success",
                    data = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult UpdateRolNo(string[] studentclassid)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                for (int i = 0; i < studentclassid.Length; i++)
                {
                    var array = studentclassid[i].Split('_');
                    var rlno = array[0].Trim();
                    var stdntclsid = array[1].Trim();
                    int RollNo = Convert.ToInt32(rlno);
                    int StudentClasid = Convert.ToInt32(stdntclsid);
                    if (StudentClasid>0)
                    {
                        var getStudentClass = _IStudentService.GetStudentClassById(StudentClasid);
                        if (getStudentClass !=null)
                        {
                            if (RollNo == 0)
                            {
                                getStudentClass.RollNo = "";
                            }
                            getStudentClass.RollNo = rlno;
                            getStudentClass.StudentClassId = StudentClasid;
                            _IStudentService.UpdateStudentClass(getStudentClass);
                        }
                    }
                   
                }
                return Json(new
                {
                    status = "success",
                    msg = "Updated Successfully",
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
            return null;
        }

        #endregion


    }
}