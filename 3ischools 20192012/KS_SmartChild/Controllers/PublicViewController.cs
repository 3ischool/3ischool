﻿using BAL.BAL.Common;
using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.Kendo;
using DAL.DAL.LibraryModule;
using DAL.DAL.PublicViewModule;
using DAL.DAL.SettingService;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Library;
using ViewModel.ViewModel.PublicViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using ViewModel.ViewModel.Security;

namespace KS_SmartChild.Controllers
{
    [Authorize]
    public class PublicViewController : Controller
    {

        #region fields

        public int count = 0;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ILogService _Logger;
        private readonly IWebHelper _WebHelper;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IPublicViewInformation _IPublicViewInformation;

        #endregion

        #region ctor

        public PublicViewController(
            IConnectionService IConnectionService, IBookMasterService IBookMasterService,
            IDropDownService IDropDownService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            ILogService Logger, IUserService IUserService,
            IWebHelper WebHelper, ICatalogMasterService ICatalogMasterService,
            ISchoolSettingService ISchoolSettingService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption, IPublicViewInformation IPublicViewInformation)
        {
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._Logger = Logger;
            this._WebHelper = WebHelper;
            this._ISchoolSettingService = ISchoolSettingService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IPublicViewInformation = IPublicViewInformation;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        #endregion

        #region StudentAttendace
        [AllowAnonymous]
        public ActionResult DailyStudentAttendance()
        {
            SchoolUser user = new SchoolUser();

            GetSetCurrentSchoolDataBase(Request, Session, Server);
            string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
            StudentAttendanceModel model = new StudentAttendanceModel();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                }

                //paging
                model.gridPageSizes = _ISchoolSettingService.GetorSetSchoolData("gridPageSizes", "20,50,100,250").AttributeValue;
                model.defaultGridPageSize = _ISchoolSettingService.GetorSetSchoolData("defaultGridPageSize", "20").AttributeValue;

                var classes = _ICatalogMasterService.GetAllClassMasters(ref count);
                model.AvaiableClasses.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
                foreach (var clss in classes)
                {
                    model.AvaiableClasses.Add(new SelectListItem
                    {
                        Text = clss.Class,
                        Value = _ICustomEncryption.base64e(clss.ClassId.ToString()),
                        Selected = false
                    });
                }

                return View(model);
            }
            catch (Exception ex)
            {
                //_Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return View(model);
            }
        }

        [AllowAnonymous]
        public string GetSetCurrentSchoolDataBase(HttpRequestBase Request, HttpSessionStateBase Session, HttpServerUtilityBase Server)
        {
            try
            {
                // get subdomain
                string User = "";
                string Subdomain = Request.Url.AbsolutePath;
                Subdomain = Subdomain.Split('/')[1];
                // API url
                string MainUrl = "";
                string suburl = Convert.ToString(WebConfigurationManager.AppSettings["SubUrl"]);
                string synccompany = Convert.ToString(WebConfigurationManager.AppSettings["syncapiname"]);
                string syncsub = Convert.ToString(WebConfigurationManager.AppSettings["syncsub"]);
                string syncext = Convert.ToString(WebConfigurationManager.AppSettings["syncext"]);
                string APIName = Convert.ToString(WebConfigurationManager.AppSettings["APIName"]);

                if (Request.Url.AbsoluteUri.Contains("localhost"))
                {
                    User = Convert.ToString(WebConfigurationManager.AppSettings["User"]);
                    // MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                    //  // User = "stroop";
                    //  // User="atsderabassi";
                    //   User = "Dev-KS_School";
                    //  //User = "3is_demo";
                    //  User = "demo";
                    //  // User = "kschild";
                    //  //User = "bpsambala";
                    //  //User = "hgdemo";
                    //  //User = "dtsk8";
                    //  //User = "";
                    MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;

                    //MainUrl = "http://app.3ischools.com/api" + "/" + suburl;
                    // MainUrl = "http://mobileapi.digitech.co.in" + "/" + suburl;


                }
                else if (Request.Url.AbsoluteUri.Contains("digitech.com") || Request.Url.AbsoluteUri.Contains("172.16.1.2"))
                {
                    User = Subdomain;
                    //MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
                    MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                }
                else if (Request.Url.AbsoluteUri.Contains("digitech.co.in"))
                {
                    User = Subdomain;
                    // MainUrl = "http://api.digitech.co.in" + "/" + suburl;
                    MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;

                }
                else
                {
                    User = Subdomain;
                    //  MainUrl = "http://3ischools.com/api" + "/" + suburl;
                    MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;

                }

                HttpClient client = new HttpClient();
                client.Timeout = new TimeSpan(0, 10, 1);
                MainUrl = MainUrl + "?DBName=" + User; // url with parameter
                client.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetAsync(MainUrl).Result;
                var content = response.Content.ReadAsStringAsync().Result;

                var IsDatabase = JsonConvert.DeserializeObject<APIDatabaseModel>(content);
                if (IsDatabase != null && !string.IsNullOrEmpty(IsDatabase.DBName))
                {
                    // change connection string 
                    var cn = _IConnectionService.Connectionstring(IsDatabase.DBName);
                    // check directory exist or not
                    Session.Timeout = 520000;
                    Session["SchoolDB"] = IsDatabase.DBName;
                    Session["SchoolId"] = IsDatabase.DBId.ToString();

                    var directory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId));
                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    var teacherdirectory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId + "/Teacher/Photos"));
                    if (!Directory.Exists(teacherdirectory))
                        Directory.CreateDirectory(teacherdirectory);
                }
                else
                    Connectivity.Error.ToString();

                // current user
                //string user = System.Web.HttpContext.Current.User.Identity.Name;
                //if (string.IsNullOrEmpty(user))
                //    return Connectivity.Done.ToString();
                //else
                return Connectivity.Done.ToString();
            }
            catch (Exception ex)
            {
                // _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Connectivity.Error.ToString();
            }
        }

        [AllowAnonymous]
        public ActionResult GetNotPresentStudents(StudentAttendanceModel model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                }

                // decrypt values
                int? ClassIdK = null;
                int Class_Id = 0;
                if (!string.IsNullOrEmpty(model.ClassId))
                {
                    int.TryParse(_ICustomEncryption.base64d(model.ClassId), out Class_Id);
                    if (Class_Id > 0)
                        ClassIdK = Class_Id;
                }

                DateTime? SelectedDate = null;
                if (!string.IsNullOrEmpty(model.Date) || !string.IsNullOrWhiteSpace(model.Date))
                {
                    SelectedDate = DateTime.ParseExact(model.Date, "dd/MM/yyyy", null);
                    SelectedDate = SelectedDate.Value.Date;
                }

                int Session_Id = 0;
                var currentSession = _ICatalogMasterService.GetCurrentSession();
                var session = _ICatalogMasterService.GetAllSessions(ref count).Where(x => DateTime.Now.Date >= x.StartDate
                                                                            && DateTime.Now.Date <= x.EndDate).FirstOrDefault();
                if (session != null)
                {
                    Session_Id = session.SessionId;
                }


                var StudentNotPresent = new List<StudentAttendanceModel>();

                var GetNotPresentStudentList = _IPublicViewInformation.GetAllStudentAttendanceBySp(ref count, ClassId: ClassIdK,
                                                         SessionId: Session_Id, SelectedDate: SelectedDate);
                if (GetNotPresentStudentList.Count > 0)
                {
                    foreach (var Stus in GetNotPresentStudentList)
                    {
                        StudentNotPresent.Add(new StudentAttendanceModel
                        {
                            ClassId = _ICustomEncryption.base64e(Stus.ClassId),
                            Class = Stus.Class,
                            StudentName = Stus.StudentName,
                            RollNo = Stus.RollNo,
                            AttendanceAbbr = Stus.AttendanceAbbr
                        });
                    }
                }

                return Json(new { StudentNotPresent }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //_Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}