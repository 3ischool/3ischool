﻿using BAL.BAL.Common;
using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ContactModule;
using DAL.DAL.DocumentModule;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.ClassPeriodModule;
using DAL.DAL.Kendo;
using DAL.DAL.SettingService;
using DAL.DAL.StaffModule;
using DAL.DAL.UserModule;
using DAL.DAL.StudentModule;
using DAL.DAL.HomeWorkModule;
using KSModel.Models;
using ViewModel.ViewModel.Address;
using ViewModel.ViewModel.Attendence;
using ViewModel.ViewModel.ContactInfo;
using ViewModel.ViewModel.Staff;
using ViewModel.ViewModel.Student;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Reflection;
using DAL.DAL.Schedular;
using System.Data.Entity;
using ViewModel.ViewModel.GatePass;
using DAL.DAL.TransportModule;

namespace KS_SmartChild.Controllers
{
    public class StaffController : Controller
    {
        #region fields

        public int count = 0;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ILogService _Logger;
        private readonly IWebHelper _WebHelper;
        private readonly IStaffService _IStaffService;
        private readonly IAddressService _IAddressService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly IDocumentService _IDocumentService;
        private readonly IContactService _IContactService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly ISubjectService _ISubjectService;
        private readonly IStudentService _IStudentService;
        private readonly ISchoolDataService _ISchoolDataService;
        public readonly IExportHelper _IExportHelper;
        public readonly IClassPeriodService _IClassPeriodService;
        public readonly ISchedularService _ISchedularService;
        public readonly ICommonMethodService _ICommonMethodService;
        public readonly IGatePassService _IGatePassService;
        public readonly ITransportService _ITransportService;
        public readonly IHomeWorkService _IHomeWorkService;
        #endregion

        #region ctor

        public StaffController(IUserService IUserService,
            IConnectionService IConnectionService,
            IDropDownService IDropDownService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            ILogService Logger,
            IWebHelper WebHelper,
            IStaffService IStaffService,
            IAddressService _IAddressService,
            IAddressMasterService _IAddressMasterService,
            IDocumentService _IDocumentService,
            IContactService _IContactService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption,
            ICatalogMasterService ICatalogMasterService,
            ISchoolSettingService ISchoolSettingService,
            ISubjectService ISubjectService,
            IStudentService IStudentService,
            ISchoolDataService ISchoolDataService, IExportHelper IExportHelper, IClassPeriodService IClassPeriodService,
            ISchedularService ISchedularService,
            ICommonMethodService ICommonMethodService, IGatePassService IGatePassService,
            ITransportService ITransportService,
            IHomeWorkService IHomeWorkService)
        {
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._Logger = Logger;
            this._WebHelper = WebHelper;
            this._IStaffService = IStaffService;
            this._IAddressService = _IAddressService;
            this._IAddressMasterService = _IAddressMasterService;
            this._IDocumentService = _IDocumentService;
            this._IContactService = _IContactService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._ICatalogMasterService = ICatalogMasterService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._ISubjectService = ISubjectService;
            this._IStudentService = IStudentService;
            this._ISchoolDataService = ISchoolDataService;
            this._IExportHelper = IExportHelper;
            this._IClassPeriodService = IClassPeriodService;
            this._ISchedularService = ISchedularService;
            this._ICommonMethodService = ICommonMethodService;
            this._IGatePassService = IGatePassService;
            this._ITransportService = ITransportService;
            this._IHomeWorkService = IHomeWorkService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }
        public DateTime FormatDate(string Date)
        {
            string sdisplayTime = Date.Trim('"');
            var dd = Date.Split('/');
            sdisplayTime = dd[1] + "-" + dd[0] + "-" + dd[2];
            return Convert.ToDateTime(sdisplayTime);
        }
        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }
        public string Convertmonthyeardate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("MMMM yyyy");
            return sdisplayTime;
        }
        public string ConvertDateForKendo(DateTime Date)
        {
            string sdisplayTime = Date.ToString("MM/dd/yyyy");
            return sdisplayTime;
        }
        #endregion

        #region staff

        //check errors for staffmodel
        public StaffModel checkStaffModelError(StaffModel model)
        {
            if (model.StaffTypeId == null)
                ModelState.AddModelError("StaffTypeId", "Staff is required.");
            else
            {
                // check if staff  type is Teacher then Staff Category should be required
                // get staff type by id
                var staffttype = _IStaffService.GetStaffTypeById((int)model.StaffTypeId);
                if (staffttype != null)
                {
                    if (staffttype.StaffType1 == "Teacher")
                    {
                        if (model.StaffCategoryId == null)
                            ModelState.AddModelError("StaffCategoryId", "Staff Category required.");
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(model.FirstName))
                ModelState.AddModelError("FirstName", "FirstName is required.");
            if (string.IsNullOrWhiteSpace(model.Employeecode))
                ModelState.AddModelError("Employeecode", "Empcode is required.");
            if (model.DepartmentId == null)
                ModelState.AddModelError("DepartmentId", "Department is required.");
            if (model.GenderId == null)
                ModelState.AddModelError("GenderId", "Gender is required.");
            if (model.DesignationId == null)
                ModelState.AddModelError("DesignationId", "Designation is required.");
            if (!string.IsNullOrEmpty(model.PunchMcId))
            {
                int machineID = Convert.ToInt32(model.PunchMcId);
                //Unique PunchMcId
                var allstaffs = _IStaffService.GetAllStaffs(ref count);
                var ceritificates = allstaffs.Where(c => c.PunchMcId == machineID && c.StaffId != model.StaffId).ToList();
                if (ceritificates.Count > 0)
                    ModelState.AddModelError("PunchMcId", "Punch Machine Id already exists.");
            }

            if (!string.IsNullOrWhiteSpace(model.Empcode))
            {
                if (model.Empcode.Length > 10)
                    ModelState.AddModelError("Employeecode", "More than 10 chac. not valid");
            }
            var StaffEmpCodePrefix = _ISchoolSettingService.GetAttributeValue("StaffEmpCodePrefix");
            var StaffEmpCodeSplitter = _ISchoolSettingService.GetAttributeValue("StaffEmpCodeSplitter");
            var StaffEmpCodeEnable = _ISchoolSettingService.GetAttributeValue("StaffEmpCodeEnable");
            var StaffEmpCodeSeries = _ISchoolSettingService.GetAttributeValue("StaffEmpCodeSeries");
            var IsEmpCodeExist = _IStaffService.GetAllStaffs(ref count, EmpCode: model.Empcode).FirstOrDefault();
            if (IsEmpCodeExist != null)
            {
                if (IsEmpCodeExist.StaffId != model.StaffId)
                    ModelState.AddModelError("Employeecode", "Emp Code already exist for other employee.");
            }
            else
            {
                var splittedstaffEmpCode = new string(model.Empcode.ToCharArray().Where(c => char.IsPunctuation(c) || char.IsLetter(c) || char.IsSymbol(c)).ToArray());
                var splittedstaffEmpCodeSeries = new string(model.Employeecode.ToCharArray().Where(c => char.IsDigit(c)).ToArray());
                if (!string.IsNullOrEmpty(StaffEmpCodePrefix) && !string.IsNullOrWhiteSpace(StaffEmpCodePrefix) && !string.IsNullOrEmpty(StaffEmpCodeSeries) && !string.IsNullOrWhiteSpace(StaffEmpCodeSeries))
                {
                    //if (splittedstaffEmpCode != StaffEmpCodePrefix + StaffEmpCodeSplitter)
                    //    ModelState.AddModelError("Employeecode", "Prefix or Splitter format is not valid.");
                    if (string.IsNullOrEmpty(splittedstaffEmpCodeSeries) || string.IsNullOrWhiteSpace(splittedstaffEmpCodeSeries) || Convert.ToInt32(splittedstaffEmpCodeSeries) < Convert.ToInt32(StaffEmpCodeSeries))
                        ModelState.AddModelError("Employeecode", "Emp Code series is invalid");
                }
            }
            // check if Aadhar card mandatory valid
            var IsAadharValid = _ISchoolSettingService.GetAttributeValue("IsAadharCardMandatory");
            if (!string.IsNullOrWhiteSpace(IsAadharValid) && !string.IsNullOrEmpty(IsAadharValid) && IsAadharValid == "1")
            {
                if (string.IsNullOrEmpty(model.AadharCard))
                    ModelState.AddModelError("AadharCard", "AadharCard required");
                else
                    goto AadharValidate;
            }

        AadharValidate:
            {
                if (!string.IsNullOrEmpty(model.AadharCard))
                {
                    // check if alphabet
                    var IsAadharContainsOnlyAlpha = model.AadharCard.All(char.IsDigit);
                    // unique
                    var isaadharexist = _IStaffService.GetAllStaffs(ref count);
                    isaadharexist = isaadharexist.Where(s => s.AadharCardNo == model.AadharCard && s.StaffId != model.StaffId).ToList();

                    if (string.IsNullOrWhiteSpace(model.AadharCard.Trim('0')))
                        ModelState.AddModelError("AadharCard", "AadharCard invalid");
                    else if (!IsAadharContainsOnlyAlpha)
                        ModelState.AddModelError("AadharCard", "AadharCard invalid");
                    else if (!(model.AadharCard.Length == 12))
                        ModelState.AddModelError("AadharCard", "AadharCard invalid");
                    else if (isaadharexist.Count > 0)
                        ModelState.AddModelError("AadharCard", "AadharCard already exist");
                }
            }
            return model;
        }

        //prepare model for staff
        public StaffModel prepareStaffModel(StaffModel model)
        {
            var StaffEmpCodePrefix = _ISchoolSettingService.GetAttributeValue("StaffEmpCodePrefix");
            var StaffEmpCodeSplitter = _ISchoolSettingService.GetAttributeValue("StaffEmpCodeSplitter");
            var StaffEmpCodeSeries = _ISchoolSettingService.GetAttributeValue("StaffEmpCodeSeries");
            var StaffEmpCodeEnable = _ISchoolSettingService.GetAttributeValue("StaffEmpCodeEnable");
            model.EmpCodePrefixWithSplitter = StaffEmpCodePrefix + StaffEmpCodeSplitter;
            if (string.IsNullOrEmpty(model.Employeecode) || string.IsNullOrWhiteSpace(model.Employeecode))
            {
                var staff = _IStaffService.GetAllStaffs(ref count).OrderByDescending(p => p.StaffId).FirstOrDefault();
                if (!string.IsNullOrEmpty(StaffEmpCodePrefix) && !string.IsNullOrWhiteSpace(StaffEmpCodePrefix) && !string.IsNullOrEmpty(StaffEmpCodeSeries) && !string.IsNullOrWhiteSpace(StaffEmpCodeSeries))
                {
                    if (staff != null)
                    {
                        var splittedstaffEmpCode = new string(staff.EmpCode.ToCharArray().Where(c => char.IsDigit(c)).ToArray());
                        var empCodeSeries = Convert.ToInt32(splittedstaffEmpCode);
                        var empCodeSuffix = empCodeSeries + 1;
                        //var newEmpCode = StaffEmpCodePrefix + StaffEmpCodeSplitter + empCodeSuffix;
                        model.Empcode = StaffEmpCodePrefix + StaffEmpCodeSplitter + empCodeSuffix.ToString();
                        model.Employeecode = empCodeSuffix.ToString();
                        model.EmpCodeSufix = empCodeSuffix.ToString();
                    }
                    else
                    {
                        model.Employeecode = StaffEmpCodePrefix + StaffEmpCodeSplitter + StaffEmpCodeSeries;
                        model.EmpCodeSufix = StaffEmpCodeSeries;
                    }
                }
                if (!string.IsNullOrEmpty(StaffEmpCodeEnable) && !string.IsNullOrWhiteSpace(StaffEmpCodeEnable) && StaffEmpCodeEnable == "1")
                    TempData["EmpCodeEnable"] = true;
                else
                    TempData["EmpCodeEnable"] = false;
            }
            else
            {
                if (!string.IsNullOrEmpty(StaffEmpCodeEnable) && !string.IsNullOrWhiteSpace(StaffEmpCodeEnable) && StaffEmpCodeEnable == "1")
                    TempData["EmpCodeEnable"] = true;
                else
                    TempData["EmpCodeEnable"] = false;
            }
            var staffTypeList = new List<SelectListItem>();
            var allStaffTypes = _IStaffService.GetAllStaffTypes(ref count);
            staffTypeList.Add(new SelectListItem { Selected = false, Text = "--select--", Value = "" });
            foreach (var item in allStaffTypes)
                staffTypeList.Add(new SelectListItem { Selected = false, Text = item.StaffType1, Value = item.StaffTypeId.ToString() });

            var IsAadharValid = _ISchoolSettingService.GetAttributeValue("IsAadharCardMandatory");
            if (!string.IsNullOrWhiteSpace(IsAadharValid) && !string.IsNullOrEmpty(IsAadharValid) && IsAadharValid == "1")
                TempData["IsAadharMendatory"] = IsAadharValid;
            //department dropdown
            model.DepartmentsList = _IDropDownService.DepartmentList();
            //designation dropdown
            int stafftypeid = 0;
            if (!string.IsNullOrEmpty(model.StaffTypeId.ToString()))
            {
                stafftypeid = Convert.ToInt32(model.StaffTypeId);
            }
            model.DesignationsList = _IDropDownService.DesignationList(stafftypeid);
            var stfaddress = new List<Address>();
            var addressIds = new List<int>();
            if (model.StaffId > 0)
            {
                var staff = _IStaffService.GetStaffById((int)model.StaffId);
                var Stfcontacttype = _IAddressMasterService.GetAllContactTypes(ref count, contactType: _IStaffService.GetStaffTypeById((int)staff.StaffTypeId).StaffType1).SingleOrDefault(); // staff contact type
                if (Stfcontacttype != null)
                    stfaddress = _IAddressService.GetAllAddresss(ref count, contactId: model.StaffId, contacttypeId: Stfcontacttype.ContactTypeId).ToList();
                foreach (var item in stfaddress)
                    addressIds.Add((int)item.AddressTypeId);
            }
            // address type
            if (addressIds.Count > 0)
            {
                var addressType = _IAddressMasterService.GetAllAddressTypes(ref count);
                if (addressType.Count > 0)
                    addressType = addressType.Where(p => !addressIds.Contains(p.AddressTypeId)).ToList();
                model.AddressModel.AvailableAddresstype.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
                foreach (var item in addressType)
                    model.AddressModel.AvailableAddresstype.Add(new SelectListItem { Selected = false, Value = item.AddressTypeId.ToString(), Text = item.AddressType1 });
            }
            else
                model.AddressModel.AvailableAddresstype = _IDropDownService.AddressTypeList();
            // cities
            model.AddressModel.AvailableCities = _IDropDownService.AddressCityList();
            // states
            model.AddressModel.AddressCityModel.AvailableStates = _IDropDownService.AddressStateList();
            //genderlist
            model.GenderList.Add(new SelectListItem { Selected = false, Text = "--Select--", Value = "" });
            foreach (var item in _ICatalogMasterService.GetAllGenders(ref count))
                model.GenderList.Add(new SelectListItem { Selected = false, Text = item.Gender1, Value = item.GenderId.ToString() });
            // countries
            model.AddressModel.AddressCityModel.AddressStateModel.AvailableCountries = _IDropDownService.AddressCountryList();
            //document type 
            if (model.StaffId > 0)
            {
                var allStaffDocumentTypes = _IStaffService.GetAllStaffDocumentTypes(ref count);
                var staffDocuments = _IStaffService.GetAllStaffDocuments(ref count, StaffId: model.StaffId);
                staffDocuments = staffDocuments.Where(p => p.StaffDocumentType.IsMultipleAllowed == false && p.StaffDocumentType.DocumentType.ToLower() != "image").ToList();
                var documentTypeIds = staffDocuments.Select(p => p.DocumentTypeId).ToList();
                allStaffDocumentTypes = allStaffDocumentTypes.Where(p => !documentTypeIds.Contains(p.DocumentTypeId) && p.DocumentType.ToLower() != "image").ToList();
                var documentTypeList = new List<SelectListItem>();
                documentTypeList.Add(new SelectListItem { Selected = false, Value = "", Text = "---Select---" });
                foreach (var item in allStaffDocumentTypes)
                    documentTypeList.Add(new SelectListItem { Selected = false, Value = item.DocumentTypeId.ToString(), Text = item.DocumentType });
                model.AttachedDocumentModel.AvailableDocumentType = documentTypeList;
            }
            else
                model.AttachedDocumentModel.AvailableDocumentType = _IDropDownService.StaffDocumentTypeList();
            //contact info type
            model.AvailableContactInfoType = _IDropDownService.ContactInfoTypeList();
            // Status List
            model.AvailableStatus = _IDropDownService.StatusList();
            //staff type
            model.StaffTypeList = staffTypeList;

            // Staff Category
            model.AvailableStaffCategories.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            var allstaffcategories = _IStaffService.GetAllStaffCategories(ref count);
            foreach (var item in allstaffcategories)
                model.AvailableStaffCategories.Add(new SelectListItem { Selected = false, Text = item.CategoryName, Value = item.Id.ToString() });

            model.AvailableBloodGroups.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            var bloodgroup = _ICatalogMasterService.GetAllBloodGroups(ref count);
            foreach (var group in bloodgroup)
                model.AvailableBloodGroups.Add(new SelectListItem { Selected = false, Text = group.BloodGroup1, Value = group.BloodGroupId.ToString() });

            var contactinfoType = _IContactService.GetAllContactInfoTypes(ref count).Where(m => m.ContactInfoType1.ToLower() == "mobile").FirstOrDefault();
            if (contactinfoType != null && model.StaffId != null)
            {
                var stafftypeId = model.StaffTypeId;
                var contacttype = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1.ToLower() == _IStaffService.GetStaffTypeById((int)stafftypeId).StaffType1.ToLower()).FirstOrDefault();
                if (contacttype != null)
                {
                    var contactinfo = _IContactService.GetAllContactInfos(ref count).Where(m => m.ContactId == model.StaffId && m.ContactInfoTypeId == contactinfoType.ContactInfoTypeId && m.ContactTypeId == contacttype.ContactTypeId && m.IsDefault == true).FirstOrDefault();
                    model.MobileNo = contactinfo != null ? contactinfo.ContactInfo1 : "";
                }
            }

            //Fill StaffServiceType
            model.AvailableStaffServiceTypes.Add(new SelectListItem { Value = "", Selected = true, Text = "--Select--" });
            var AllStaffServiceType = _IStaffService.GetAllStaffServiceType(ref count, IsActive: true);
            foreach (var sst in AllStaffServiceType)
                model.AvailableStaffServiceTypes.Add(new SelectListItem { Value = sst.StaffServiceTypeId.ToString(), Text = sst.StaffServiceType1});

            model.IsPassingYearRequiredInStaffQualification= _ISchoolSettingService.GetorSetSchoolData("IsPassingYearRequiredInStaffQualification", "1").AttributeValue == "1" ? true : false;
            return model;
        }

        public ActionResult Edit(string Id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffList", "Edit Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                // check id exist
                if (!string.IsNullOrEmpty(Id))
                {


                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(Id);
                    int staffid = 0;
                    if (int.TryParse(qsid, out staffid))
                    {
                        var model = new StaffModel();
                        model.IsauthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "Edit", "Add");
                        model.AddressModel.IsauthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "Edit", "Add");
                        model.IsAdmin = false;
                        if (user.ContactType.ContactType1.ToLower() == "admin")
                            model.IsAdmin = true;
                        var Staff = _IStaffService.GetStaffById(staffid, true);

                        var StaffEmpCodePrefix = _ISchoolSettingService.GetAttributeValue("StaffEmpCodePrefix");
                        var StaffEmpCodeSplitter = _ISchoolSettingService.GetAttributeValue("StaffEmpCodeSplitter");
                        // staff conatct type
                        ContactType Stfcontacttype = null;
                        if (Staff != null)
                        {
                            Stfcontacttype = _IAddressMasterService.GetAllContactTypes(ref count, _IStaffService.GetStaffTypeById((int)Staff.StaffTypeId).StaffType1).SingleOrDefault(); // staff contact type
                            if (TempData["Staff_Info_ViewData"] != null)
                            {
                                var StaffInfo = (StaffModel)TempData["Staff_Info_ViewData"];
                                model.FirstName = StaffInfo.FirstName;
                                model.LastName = StaffInfo.LastName;
                                model.StaffTypeId = StaffInfo.StaffTypeId;
                                model.IsActive = StaffInfo.IsActive;
                                model.PanNo = StaffInfo.PanNo;
                                model.FatherName = StaffInfo.FatherName;
                                model.FatherMobNo = StaffInfo.FatherMobNo;
                                model.MotherName = StaffInfo.MotherName;
                                model.MotherMobNo = StaffInfo.MotherMobNo;
                                if (StaffInfo.IsMarried == null)
                                    model.IsMarried = false;
                                else
                                    model.IsMarried = StaffInfo.IsMarried;
                                
                                model.SpouseName = StaffInfo.SpouseName;
                                model.SpouseMobNo = StaffInfo.SpouseMobNo;
                                var userContactType = _IAddressMasterService.GetAllContactTypes(ref count).Where(contact => contact.ContactType1.ToLower().Contains("student")).FirstOrDefault();

                                //update user status in user table
                                var usertoupdte = _IUserService.GetAllUsers(ref count).Where(p => p.UserContactId == StaffInfo.StaffId && p.UserTypeId == userContactType.ContactTypeId).FirstOrDefault();
                                if (usertoupdte != null)
                                {
                                    var usertoupdate = _IUserService.GetUserById(UserId: usertoupdte.UserId, IsTrack: true);
                                    usertoupdate.Status = StaffInfo.IsActive;
                                    _IUserService.UpdateUser(usertoupdate);
                                }

                                model.DateOfJoin = StaffInfo.DateOfJoin != null ? StaffInfo.DateOfJoin : "";
                                model.PunchMcId = StaffInfo.PunchMcId;

                                //get empcode number by splitting it with splitter
                                var splittedstaffEmpCode = new string(StaffInfo.Employeecode.ToCharArray()
                                                                                .Where(c => char.IsDigit(c)).ToArray());
                                var empCodeSeries = splittedstaffEmpCode.ToString();
                                model.Empcode = StaffEmpCodePrefix + StaffEmpCodeSplitter + empCodeSeries;
                                model.Employeecode = empCodeSeries;
                                model.StaffId = StaffInfo.StaffId;
                                model.DepartmentId = StaffInfo.DepartmentId;
                                model.DesignationId = StaffInfo.DesignationId;
                                model.AadharCard = StaffInfo.AadharCard;
                                model.GenderId = StaffInfo.GenderId;
                                model.BloodGroupId = StaffInfo.BloodGroupId;
                                model.DateOfBirth = StaffInfo.DOB.HasValue ? ConvertDate(StaffInfo.DOB.Value.Date) : "";
                                model.StaffCategoryId = StaffInfo.StaffCategoryId;
                                model.StaffTypeToShow = Staff.StaffType.StaffType1;
                                model.EncStaffId = _ICustomEncryption.base64e(model.StaffId.ToString());
                                model = checkStaffModelError(model);
                                model = prepareStaffModel(model);

                                model.DesignationsList = _IDropDownService.DesignationList(Convert.ToInt32(model.StaffTypeId));
                            }
                            else
                            {
                                #region staffInformation
                                model.FirstName = Staff.FName;
                                model.LastName = Staff.LName;
                                model.StaffTypeId = Staff.StaffTypeId;
                                model.StaffTypeToShow = Staff.StaffType.StaffType1;
                                model.DesignationId = Staff.DesignationId;
                                model.DepartmentId = Staff.DepartmentId;
                                model.GenderId = Staff.GenderId;
                                model.BloodGroupId = Staff.BloodGroupId;
                                model.IsActive = (bool)Staff.IsActive;
                                model.DateOfJoin = Staff.DOJ.HasValue ? ConvertDate(Staff.DOJ.Value.Date) : "";
                                model.DateOfBirth = Staff.DOB.HasValue ? ConvertDate(Staff.DOB.Value.Date) : "";
                                model.PunchMcId = Convert.ToString(Staff.PunchMcId);
                                model.PanNo = Staff.PanNo;
                                model.FatherName = Staff.FatherName;
                                model.FatherMobNo = Staff.FatherMobile;
                                model.MotherName = Staff.MotherName;
                                model.MotherMobNo = Staff.MotherMobile;

                                if (Staff.IsMarried == null)
                                    model.IsMarried = false;
                                else
                                    model.IsMarried = (bool)Staff.IsMarried;
                              
                                model.SpouseName = Staff.SpouseName;
                                model.SpouseMobNo = Staff.SpouseMobile;
                                //get empcode number by splitting it with splitter
                                var splittedstaffEmpCode = new string(Staff.EmpCode.ToCharArray()
                                                                           .Where(c => char.IsDigit(c)).ToArray());
                                var empCodeSeries = splittedstaffEmpCode.ToString();
                                model.Empcode = StaffEmpCodePrefix + StaffEmpCodeSplitter + empCodeSeries;
                                model.Employeecode = empCodeSeries;
                                model.AadharCard = Staff.AadharCardNo;
                                model.StaffId = Staff.StaffId;
                                model.StaffCategoryId = Staff.StaffCategoryId;
                                var userContactType = _IAddressMasterService.GetAllContactTypes(ref count).Where(contact => contact.ContactType1.ToLower().Contains("student")).FirstOrDefault();

                                //update user status in user table
                                var usertoupdte = _IUserService.GetAllUsers(ref count).Where(p => p.UserContactId == Staff.StaffId && p.UserTypeId == userContactType.ContactTypeId).FirstOrDefault();
                                if (usertoupdte != null)
                                {
                                    var usertoupdate = _IUserService.GetUserById(UserId: usertoupdte.UserId, IsTrack: true);
                                    usertoupdate.Status = Staff.IsActive;
                                    _IUserService.UpdateUser(usertoupdate);
                                }

                                model.DesignationsList = _IDropDownService.DesignationList(Convert.ToInt32(model.StaffTypeId));
                                // to freez the stafftype in case of edit
                                if (Staff.TeacherSubjects.Count > 0 || Staff.TeacherClasses.Count > 0 || Staff.ClassPeriodDetails.Count > 0 || Staff.ClassTeachers.Count > 0)
                                    model.StaffType = Staff.StaffType.StaffType1;
                                var doctypeid = _IStaffService.GetAllStaffDocumentTypes(ref count, "Image").FirstOrDefault();
                                var image = _IStaffService.GetAllStaffDocuments(ref count, doctypeid.DocumentTypeId,
                                                                                    StaffId: Staff.StaffId).FirstOrDefault();

                                //fetch image
                                if (image != null)
                                {
                                    if (image.Image.ToString().Contains("_image"))
                                    {
                                        var SchoolDbId1 = Convert.ToString(HttpContext.Session["SchoolId"]);
                                        model.StaffImageId = image.DocumentId;
                                        model.StaffImagepath = _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId1 + "/Teacher/Photos/" + image.Image;
                                    }
                                    else
                                    {
                                        string defaultmaleimg = _WebHelper.GetStoreLocation() + "Images/male-avtar.png";
                                        string defaultFemaleimg = _WebHelper.GetStoreLocation() + "Images/Female-avtar.png";
                                        //check user gender
                                        if (model.IsAdmin == true)
                                        {
                                            var genderid = _IStaffService.GetStaffById((int)staffid).GenderId;
                                            var gender = _ICatalogMasterService.GetGenderById((int)genderid).Gender1;
                                            var defaultpic = defaultmaleimg;
                                            if (gender.ToLower().Contains("female"))
                                            {
                                                defaultpic = defaultFemaleimg;
                                            }
                                            model.StaffImagepath = defaultpic;
                                        }
                                        else
                                        {
                                            var genderid = _IStaffService.GetStaffById((int)user.UserContactId).GenderId;
                                            var gender = _ICatalogMasterService.GetGenderById((int)genderid).Gender1;
                                            var defaultpic = defaultmaleimg;
                                            if (gender.ToLower().Contains("female"))
                                            {
                                                defaultpic = defaultFemaleimg;
                                            }
                                            model.StaffImagepath = defaultpic;
                                        }
                                    }
                                }
                                else
                                {
                                    string defaultmaleimg = _WebHelper.GetStoreLocation() + "Images/male-avtar.png";
                                    string defaultFemaleimg = _WebHelper.GetStoreLocation() + "Images/Female-avtar.png";
                                    //check user gender
                                    if (model.IsAdmin == true)
                                    {
                                        var genderid = _IStaffService.GetStaffById((int)staffid).GenderId;
                                        var gender = _ICatalogMasterService.GetGenderById((int)genderid).Gender1;
                                        var defaultpic = defaultmaleimg;
                                        if (gender.ToLower().Contains("female"))
                                        {
                                            defaultpic = defaultFemaleimg;
                                        }
                                        model.StaffImagepath = defaultpic;
                                    }
                                    else
                                    {
                                        var genderid = _IStaffService.GetStaffById((int)user.UserContactId).GenderId;
                                        var gender = _ICatalogMasterService.GetGenderById((int)genderid).Gender1;
                                        var defaultpic = defaultmaleimg;
                                        if (gender.ToLower().Contains("female"))
                                        {
                                            defaultpic = defaultFemaleimg;
                                        }
                                        model.StaffImagepath = defaultpic;
                                    }
                                }

                                #endregion
                            }
                            model.AlreadyInAttendace = false;
                            var IsStaffAttendanceExists = _IStaffService.GetAllStaffAttendances(ref count, StaffId: (int)Staff.StaffId);
                            if (IsStaffAttendanceExists.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(model.DateOfJoin))
                                {
                                    DateTime? DJ = DateTime.Now.Date;
                                    //if (!string.IsNullOrEmpty(model.DateOfJoin) || !string.IsNullOrWhiteSpace(model.DateOfJoin))
                                    //{
                                    //    DJ = DateTime.ParseExact(model.DateOfJoin, "dd/MM/yyyy", null);
                                    //    DJ = DJ.Value.Date;
                                    //}
                                    var MaxDate = IsStaffAttendanceExists.Where(m => m.AttendanceDate
                                                    >= Staff.DOJ).OrderBy(x => x.AttendanceDate).FirstOrDefault();
                                    if (MaxDate != null)
                                    {
                                        model.MaxDateDOJ = MaxDate != null ? MaxDate.AttendanceDate.Value.Date : DJ;
                                        model.AlreadyInAttendace = true;
                                    }
                                }

                            }

                            #region Address
                            var empAddress = (StaffModel)TempData["Staff_Address"];
                            if (empAddress != null)
                            {
                                // assign address field values
                                model.AddressModel = empAddress.AddressModel;
                                bool modelValid;
                                model = checkAddressModelErrors(model, out modelValid); // check form validate
                                model = PostPrepareStaffAddressModel(model);
                            }
                            else
                            {
                                AddressModel Staffaddress = new AddressModel();
                                var stdaddress = _IAddressService.GetAllAddresss(ref count, contactId: Staff.StaffId, contacttypeId: Stfcontacttype.ContactTypeId).ToList();
                                if (stdaddress != null)
                                {
                                    foreach (var sa in stdaddress)
                                    {
                                        Staffaddress = new AddressModel();
                                        Staffaddress.EncAddressId = _ICustomEncryption.base64e(sa.AddressId.ToString());
                                        Staffaddress.AddressTypeId = (int)sa.AddressTypeId;
                                        Staffaddress.AddressType = sa.AddressType.AddressType1;
                                        Staffaddress.PlotNo = sa.PlotNo;
                                        Staffaddress.Sector_Street = sa.Sector_Street;
                                        Staffaddress.Locality = sa.Locality;
                                        Staffaddress.Landmark = sa.Landmark;
                                        Staffaddress.AddressDistrict = sa.District;
                                        Staffaddress.Address_CityId = (int)sa.CityId;
                                        Staffaddress.AddressCity = sa.AddressCity.City;
                                        Staffaddress.Zip = sa.Zip;
                                        model.AddressModel.AddressModelList.Add(Staffaddress);
                                        // assign address field values
                                        //model = PostPrepareStudentAddressModel(model);
                                    }
                                }
                                #endregion
                            }

                            #region documents
                            var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);

                            var doctypeidimg = _IStaffService.GetAllStaffDocumentTypes(ref count, "Image").FirstOrDefault();
                            var documnts = _IStaffService.GetAllStaffDocuments(ref count, StaffId: Staff.StaffId).ToList();
                            foreach (var doc in documnts)
                            {
                                if (doc.DocumentTypeId == doctypeidimg.DocumentTypeId)
                                    continue;

                                StaffAttachedDocumentModel document = new StaffAttachedDocumentModel();
                                document.AttachDocumentId = doc.DocumentId;
                                document.AttachDocumentTypeId = doc.DocumentTypeId;
                                document.Description = doc.Description;
                                document.IsMultipleAllowed = doc.StaffDocumentType.IsMultipleAllowed;
                                document.DocumentType = doc.StaffDocumentType.DocumentType;
                                document.Image = _WebHelper.GetStoreLocation() + "/Images/" + SchoolDbId + "/Teacher/" + Staff.StaffId + "/" + doc.Image;

                                model.AttachedDocumentModellist.Add(document);
                                model.AttachedDocumentModel.AttachDocumentId = doc.DocumentId;
                            }
                            #endregion

                            #region staffqualification
                            //var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                            var staffqualificaionlist = _IStaffService.GetAllStaffQualifications(ref count).Where(m => m.StaffId == Staff.StaffId);
                            ViewModel.ViewModel.Staff.StaffQualification staffQualification = new ViewModel.ViewModel.Staff.StaffQualification();
                            foreach (var item in staffqualificaionlist)
                            {
                                staffQualification = new ViewModel.ViewModel.Staff.StaffQualification();
                                staffQualification.ExamPassed = item.ExamPassed;
                                staffQualification.BoardUniversity = item.BoardUniversity;
                                staffQualification.InstitutionName = item.InstitutionName;
                                staffQualification.ObtainedMarks = item.ObtainedMarks;
                                staffQualification.PassingYear = item.PassingYear != null? ConvertDate((DateTime)item.PassingYear):""; 
                                staffQualification.TotalMarks = item.TotalMarks;
                                staffQualification.StaffQualificationId = item.StaffQualificationId;
                                staffQualification.IsQualificationDocument = false;
                                if (!string.IsNullOrEmpty(item.QualificationImage))
                                {
                                    staffQualification.IsQualificationDocument = true;
                                    staffQualification.imgDocumentType = item.QualificationImage.Split('.').Last();
                                }

                                staffQualification.QualificationImage = _WebHelper.GetStoreLocation() + "/Images/" + SchoolDbId + "/Teacher/" + Staff.StaffId + "/" + item.QualificationImage;

                                model.StaffQualificationList.Add(staffQualification);


                            }

                            #endregion

                            #region staffExperience
                            var staffExperiencelist = _IStaffService.GetAllStaffExperiences(ref count).Where(m => m.StaffId == Staff.StaffId);
                            ViewModel.ViewModel.Staff.StaffExperience staffExperience = new ViewModel.ViewModel.Staff.StaffExperience();
                            foreach (var item in staffExperiencelist)
                            {
                                staffExperience = new ViewModel.ViewModel.Staff.StaffExperience();
                                staffExperience.Organsation = item.Organsation;
                                staffExperience.Designation = item.Designation;
                                staffExperience.City = item.City;
                                staffExperience.JoiningDate = item.JoiningDate!=null? ConvertDate((DateTime)item.JoiningDate): "";
                                staffExperience.LeavingDate = item.LeavingDate!=null?ConvertDate((DateTime)item.LeavingDate):"";
                                staffExperience.ExperienceImage = item.ExperienceImage;
                                staffExperience.StaffExperienceId = item.StaffExperienceId;
                                staffExperience.IsExperienceDocument = false;
                                if (!string.IsNullOrEmpty(item.ExperienceImage))
                                {
                                    staffExperience.IsExperienceDocument = true;
                                    staffExperience.imgExDocumentType = item.ExperienceImage.Split('.').Last();
                                }

                                staffExperience.ExperienceImage = _WebHelper.GetStoreLocation() + "/Images/" + SchoolDbId + "/Teacher/" + Staff.StaffId + "/" + item.ExperienceImage;

                                model.StaffExperienceList.Add(staffExperience);


                            }

                            #endregion

                            #region StaffBankDetails
                            var StaffBankDetails = _IStaffService.GetAllStaffBankDetail(ref count, StaffId: Staff.StaffId);
                            var bnkdtl = new StaffBankDetails();
                            foreach (var item in StaffBankDetails)
                            {
                                bnkdtl = new StaffBankDetails();
                                bnkdtl.BankName = item.BankName;
                                bnkdtl.StaffBankDetailId= item.StaffBankDetailId;
                                bnkdtl.AccountNo = item.AccountNo;
                                bnkdtl.Branch = item.Branch;
                                bnkdtl.BranchCode = item.BranchCode;
                                bnkdtl.IFSCCode = item.IFSCCode;
                                bnkdtl.MICR = item.MICR;
                                if(item.FromDate!=null)
                                    bnkdtl.BankDetailFromDate = item.FromDate.Value.Date.ToString("dd/MM/yyyy");
                                if (item.ToDate != null)
                                    bnkdtl.BankDetailToDate = item.ToDate.Value.Date.ToString("dd/MM/yyyy");
                                model.StaffBankDetailList.Add(bnkdtl);
                            }
                            #endregion

                            #region contacts

                            var staffcontacttype = _IAddressMasterService.GetAllContactTypes(ref count, Staff.StaffType.StaffType1).FirstOrDefault();
                            if (staffcontacttype != null)
                            {
                                var contactInfo = _IContactService.GetAllContactInfos(ref count, ContactId: staffid, ContactTypeId: staffcontacttype.ContactTypeId).ToList();
                                if (contactInfo.Count > 0)
                                {
                                    foreach (var item in contactInfo)
                                    {
                                        var status = item.Status == true ? "Active" : "In-Active";
                                        var statusid = item.Status == true ? "1" : "2";
                                        var contacttype = _IContactService.GetContactInfoTypeById((int)item.ContactInfoTypeId).ContactInfoType1;
                                        model.ContactInfoModellist.Add(new ContactInfoModel { ContactInfoId = item.ContactInfoId, ContactInfo = item.ContactInfo1, status1 = status, IsDefault = item.IsDefault, ContactInfoType = contacttype, ContactInfoTypeId = item.ContactInfoTypeId, statusId = statusid });
                                    }
                                    var info = contactInfo.FirstOrDefault(p => p.IsDefault == true && p.Status == true && p.ContactInfoType.ContactInfoType1 == "Mobile");
                                    if (info != null)
                                        model.ContactInfoModel.ContactToShow = info.ContactInfo1;

                                }

                            }

                            #endregion

                            #region User

                            //model.IsStaffUser
                            var StaffType = Staff.StaffType.StaffType1;
                            if (StaffType != null)
                            {
                                model.IsStaffDependency = false;
                                var HW = _IHomeWorkService.GetAllHWs(ref count, StaffId: Staff.StaffId);
                                if (HW.Count() > 0)
                                    model.IsStaffDependency = true;

                                if (model.IsStaffDependency != true)
                                {
                                    var ClassTeacher = _IStaffService.GetAllClassTeachers(ref count).Where(m=>m.TeacherId==Staff.StaffId);
                                    if (ClassTeacher.Count() > 0)
                                        model.IsStaffDependency = true;
                                }
                                if (model.IsStaffDependency != true)
                                {
                                    var ClassTest = _IClassPeriodService.GetAllClassTests(ref count).Where(m => m.StaffId == Staff.StaffId);
                                    if (ClassTest.Count() > 0)
                                        model.IsStaffDependency = true;
                                }
                                if (model.IsStaffDependency != true)
                                {
                                    var TeacherSubjectAssign=_IStaffService.GetAllTeacherSubjectAssigns(ref count).Where(m => m.TeacherId == Staff.StaffId);
                                    if (TeacherSubjectAssign.Count() > 0)
                                        model.IsStaffDependency = true;
                                }
                                if (model.IsStaffDependency != true)
                                {
                                    var LessonPlan = _ISubjectService.GetAllLessonPlans(ref count).Where(m => m.TeacherId == Staff.StaffId);
                                    if (LessonPlan.Count() > 0)
                                        model.IsStaffDependency = true;
                                }
                                if (model.IsStaffDependency != true)
                                {
                                    var ClassPeriodDetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count, TeacherId: Staff.StaffId);
                                    if (ClassPeriodDetails.Count() > 0)
                                        model.IsStaffDependency = true;
                                }
                                if (model.IsStaffDependency != true)
                                {
                                    var TeacherClass = _IStaffService.GetAllTeacherClasss(ref count).Where(m => m.TeacherId == Staff.StaffId);
                                    if (TeacherClass.Count() > 0)
                                        model.IsStaffDependency = true;
                                }
                                if (model.IsStaffDependency != true)
                                {
                                    var TeacherSubject = _IStaffService.GetAllTeacherSubjects(ref count).Where(m => m.TeacherId == Staff.StaffId);
                                    if (TeacherSubject.Count() > 0)
                                        model.IsStaffDependency = true;
                                }
                                if (model.IsStaffDependency != true)
                                {
                                    var TimeTablePeriodQuotaDetail = _IClassPeriodService.GetAllTimeTableQuotaDetails(ref count).Where(m => m.StaffId == Staff.StaffId);
                                    if (TimeTablePeriodQuotaDetail.Count() > 0)
                                        model.IsStaffDependency = true;
                                }

                                var ContactType = _IAddressMasterService.GetAllContactTypes(ref count, contactType: StaffType.ToLower()).FirstOrDefault();
                                if (ContactType != null)
                                {
                                    var StaffUser = _IUserService.GetAllUsers(ref count).Where(m => m.UserTypeId == ContactType.ContactTypeId && m.UserContactId == Staff.StaffId && m.IsDeleted!=true).FirstOrDefault();
                                    if (StaffUser != null)
                                    {
                                        model.IsStaffUser = true;
                                    }
                                    else
                                    {
                                        model.IsStaffUser = false;
                                    }
                                }
                            }

                            var StaffServiceType = _IStaffService.GetAllStaffServiceTypeDetail(ref count,StaffId: Staff.StaffId).OrderByDescending(f => f.StaffServiceTypeDetailId).FirstOrDefault();
                            if (StaffServiceType != null)
                            {
                                model.StaffServiceTypeId = (int)StaffServiceType.StaffServiceTypeId;
                            }
                            #endregion
                        }
                        else
                        {
                            TempData["ErrorNotification"] = "Employee not found";
                            return RedirectToAction("ManageStaff", "Staff");
                        }

                        model = prepareStaffModel(model);
                        model.EncStaffId = _ICustomEncryption.base64e(model.StaffId.ToString());
                        return View(model);
                    }
                }

                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult GetStaffServiceTypeDetails(string Id)
        {
            try
            {
            var qsid = _ICustomEncryption.base64d(Id);
            int staffid = 0;
            if (int.TryParse(qsid, out staffid))
            {
                    if (staffid > 0)
                    {
                        var ServiceTypes = _IStaffService.GetAllStaffServiceType(ref count);
                        var StaffServiceTypeDetails = _IStaffService.GetAllStaffServiceTypeDetail(ref count, StaffId: staffid).Where(f=>f.ChangeDate!=null).OrderByDescending(f=>f.StaffServiceTypeDetailId);
                        IList<SelectListItem> ServiceTypeList = new List<SelectListItem>();
                        IList<SelectListItem> ServiceTypeTableList = new List<SelectListItem>();
                        foreach (var item in StaffServiceTypeDetails)
                        {
                            ServiceTypeTableList.Add(new SelectListItem { Text = item.StaffServiceType.StaffServiceType1, Value = item.ChangeDate.Value.Date.ToString("dd/MM/yyyy")+"_"+ item.REmarks});
                        }
                        var CurrentServiceType = StaffServiceTypeDetails.FirstOrDefault();
                        if (CurrentServiceType != null)
                            ServiceTypes = ServiceTypes.Where(f => f.StaffServiceTypeId != CurrentServiceType.StaffServiceTypeId).ToList();
                        foreach (var item in ServiceTypes)
                        {
                            ServiceTypeList.Add(new SelectListItem { Text = item.StaffServiceType1, Value = item.StaffServiceTypeId.ToString() });
                        }
                        return Json(new
                        {
                            status = "success",
                            ServiceTypeTableList,
                            ServiceTypeList
                        }, JsonRequestBehavior.AllowGet);
                    }
            }
            return Json(new
            {
                status = "error",
                data="invalid Staff"
            }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "error",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateServiceType(int ServiceTypeId, string Id,string Remarks)
        {
            try
            {
                if (!string.IsNullOrEmpty(Id))
                {
                    var qsid = _ICustomEncryption.base64d(Id);
                    int staffid = 0;
                    if (int.TryParse(qsid, out staffid))
                    {
                        if (staffid > 0 && ServiceTypeId>0)
                        {
                            var newServicetypedetail = new StaffServiceTypeDetail();
                            newServicetypedetail.ChangeDate = DateTime.Now.Date;
                            newServicetypedetail.StaffId = staffid;
                            newServicetypedetail.StaffServiceTypeId = ServiceTypeId;
                            newServicetypedetail.REmarks = Remarks;
                            _IStaffService.InsertStaffServiceTypeDetail(newServicetypedetail);

                            return Json(new
                            {
                                status = "success",
                               data="Service Type Updated SuccessFully"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new
                {
                    status = "error",
                    data = "Invalid Details"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "error",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        // save staff image
        public string SaveStaffImage(StaffModel model)
        {
            string newpath = "";
            string filename = "";
            string newpathstring = string.Empty;
            string path = model.StaffImagepath;

            // alter path 
            var pathary = path.Split('\\');
            if (pathary.Length > 1)
            {
                // file name
                var file = path.Split('\\').Last();
                var filenameext = Path.GetExtension(file).ToLower();
                pathary = pathary.Take(pathary.Count() - 1).ToArray();
                newpath = String.Join("\\", pathary);

                // new path with image ename
                newpathstring = newpath + "\\" + model.StaffId + "_image" + filenameext;
                filename = model.StaffId + "_image" + filenameext;

                // rename image
                if (System.IO.File.Exists(newpathstring))
                    System.IO.File.Delete(newpathstring);
                System.IO.File.Move(model.StaffImagepath, newpathstring);
            }
            else
            {
                pathary = path.Split('/');
                if (pathary.Length > 1)
                    filename = pathary[pathary.Length - 1];
            }

            return filename;
        }

        public ActionResult ManageStaff()
        {
            StaffModel model = new StaffModel();
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;


                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "ManageStaff", "View");
                    //if (!isauth)
                    //    return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }
                model = prepareStaffModel(model);

                model.IsActive = true;
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        //save the basic info
        [HttpPost]
        public ActionResult ManageStaff(StaffModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "ManageStaff", "Add");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            bool editmode = false;
            int? nullablevariable = null;
            if (!string.IsNullOrEmpty(model.EncStaffId))
            {
                // decrypt Id
                var qsid = _ICustomEncryption.base64d(model.EncStaffId);
                int staff_id = 0;
                if (int.TryParse(qsid, out staff_id))
                {
                    model.StaffId = staff_id;
                }
            }

            var StaffEmpCodePrefix = _ISchoolSettingService.GetAttributeValue("StaffEmpCodePrefix");
            var StaffEmpCodeSplitter = _ISchoolSettingService.GetAttributeValue("StaffEmpCodeSplitter");
            //check uniqueness of empcode
            model.Empcode = StaffEmpCodePrefix + StaffEmpCodeSplitter + model.Employeecode;
            model = checkStaffModelError(model);
            if (ModelState.IsValid)
            {
                try
                {
                    Staff staffInfo = new Staff();
                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    var doctypeid = _IStaffService.GetAllStaffDocumentTypes(ref count, "Image").FirstOrDefault(); // document type Id
                    // Attedance log Table
                    var Stafftable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "Staff").FirstOrDefault();



                    bool IsFlag = false;
                    //Insert and Update record 
                    if (model.StaffId > 0)
                    {
                        staffInfo = _IStaffService.GetStaffById((int)model.StaffId, true);
                        var Stfcontacttype = _IAddressMasterService.GetAllContactTypes(ref count, _IStaffService.GetStaffTypeById((int)staffInfo.StaffTypeId).StaffType1).FirstOrDefault(); // staff contact type
                        var contactInfo = _IContactService.GetAllContactInfos(ref count, ContactId: model.StaffId);

                        if (staffInfo != null)
                        {
                            if(staffInfo.StaffTypeId!= model.StaffTypeId)
                            {
                                var OldSTFTypeUser = _IUserService.GetAllUsers(ref count, UserTypeId: staffInfo.StaffTypeId, UserContactId: model.StaffTypeId);
                                foreach(var oldusr in OldSTFTypeUser)
                                {
                                    oldusr.IsDeleted = true;
                                    _IUserService.UpdateUser(oldusr);
                                }
                                var OldContactType = _IContactService.GetAllContactInfos(ref count, ContactId: staffInfo.StaffId, ContactTypeId: staffInfo.ContactTypeId);
                                foreach(var oldcntct in OldContactType)
                                {
                                    _IContactService.DeleteContactInfo(oldcntct.ContactInfoId);
                                }
                                Stfcontacttype = _IAddressMasterService.GetAllContactTypes(ref count, _IStaffService.GetStaffTypeById((int)model.StaffTypeId).StaffType1).FirstOrDefault(); // staff contact type
                            }
                            staffInfo.StaffTypeId = model.StaffTypeId;
                            staffInfo.FName = (!string.IsNullOrEmpty(model.FirstName)) ? model.FirstName.Trim() : "";
                            staffInfo.LName = (!string.IsNullOrEmpty(model.LastName)) ? model.LastName.Trim() : "";
                            staffInfo.DepartmentId = model.DepartmentId;
                            staffInfo.DesignationId = model.DesignationId;
                            staffInfo.AadharCardNo = model.AadharCard;
                            staffInfo.GenderId = model.GenderId;
                            staffInfo.StaffCategoryId = model.StaffCategoryId;
                            staffInfo.BloodGroupId = model.BloodGroupId;
                            staffInfo.PanNo = model.PanNo;
                            staffInfo.FatherName= model.FatherName;
                            staffInfo.FatherMobile = model.FatherMobNo;
                            staffInfo.MotherName = model.MotherName;
                            staffInfo.MotherMobile = model.MotherMobNo;
                            if (model.IsMarried!=null && model.IsMarried==true)
                            {
                                staffInfo.IsMarried = model.IsMarried;
                                staffInfo.SpouseName= model.SpouseName; 
                                staffInfo.SpouseMobile = model.SpouseMobNo;
                            }
                            else
                            {
                                staffInfo.IsMarried = model.IsMarried;
                                staffInfo.SpouseName = null;
                                staffInfo.SpouseMobile = null;
                            }
                                //if(staffInfo.IsMarried)
                                int counter = 0;
                            if (staffInfo.IsActive == true)
                            {
                                if (staffInfo.ClassPeriodDetails.Count == 0)
                                {
                                    IsFlag = true;
                                    if (staffInfo.ClassTeachers.Count > 0)
                                    {
                                        IsFlag = true;
                                        foreach (var item in staffInfo.ClassTeachers)
                                        {
                                            if (item.EndDate <= DateTime.Now)
                                                counter += counter + 1;
                                        }
                                        if (counter == staffInfo.ClassTeachers.Count)
                                            staffInfo.IsActive = staffInfo.IsActive;
                                    }
                                    else
                                        staffInfo.IsActive = staffInfo.IsActive;
                                }
                            }
                            else
                                staffInfo.IsActive = staffInfo.IsActive;

                            staffInfo.EmpCode = StaffEmpCodePrefix + StaffEmpCodeSplitter + model.Employeecode;

                            if (!string.IsNullOrEmpty(model.DateOfJoin))
                            {
                                DateTime date = DateTime.ParseExact(model.DateOfJoin, "dd/MM/yyyy", null);
                                staffInfo.DOJ = date;
                            }

                            if (!string.IsNullOrEmpty(model.DateOfBirth))
                            {
                                DateTime date = DateTime.ParseExact(model.DateOfBirth, "dd/MM/yyyy", null);
                                staffInfo.DOB = date;
                            }

                            int? machineID = null;
                            if (!string.IsNullOrEmpty(model.PunchMcId))
                                machineID = Convert.ToInt32(model.PunchMcId);

                            staffInfo.PunchMcId = machineID;
                            staffInfo.ContactTypeId = Stfcontacttype.ContactTypeId;

                            _IStaffService.UpdateStaff(staffInfo);

                            //Save mobile no 
                            if (model.MobileNo != null && model.MobileNo != "")
                            {
                                var contactinfoType = _IContactService.GetAllContactInfoTypes(ref count).Where(m => m.ContactInfoType1.ToLower() == "mobile").FirstOrDefault();
                                if (contactinfoType != null)
                                {
                                    var contactinfoTypeId = contactinfoType.ContactInfoTypeId;
                                    var stafftypeId = staffInfo.StaffTypeId;
                                    var contacttype = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1.ToLower() == _IStaffService.GetStaffTypeById((int)stafftypeId).StaffType1.ToLower()).FirstOrDefault();
                                    if (contacttype != null)
                                    {
                                        var contacttypeid = contacttype.ContactTypeId;
                                        var contactinfo = _IContactService.GetAllContactInfos(ref count).Where(m => m.ContactId == staffInfo.StaffId && m.ContactInfoTypeId == contactinfoTypeId && m.ContactTypeId == contacttypeid && m.IsDefault == true).FirstOrDefault();
                                        if (contactinfo != null)
                                        {
                                            contactinfo.ContactInfo1 = model.MobileNo;
                                            _IContactService.UpdateContactInfo(contactinfo);
                                        }
                                        else
                                        {
                                            var contact_info = new ContactInfo();
                                            contact_info.ContactId = staffInfo.StaffId;
                                            contact_info.ContactInfo1 = model.MobileNo;
                                            contact_info.ContactInfoTypeId = contactinfoTypeId;
                                            contact_info.IsDefault = true;
                                            contact_info.Status = true;
                                            contact_info.ContactTypeId = contacttypeid;
                                            _IContactService.InsertContactInfo(contact_info);
                                        }
                                    }
                                }
                            }
                            editmode = true;
                            // activity log
                            // table Id
                            var stafftable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "Staff").FirstOrDefault();
                            if (stafftable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, stafftable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(staffInfo.StaffId, stafftable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update Staff with StaffId:{0} and EmployeeName:{1}", staffInfo.StaffId, model.FirstName + model.LastName), Request.Browser.Browser);
                            }

                            //update staff image
                            if (model.StaffImageId > 0)
                            {
                                if (!string.IsNullOrEmpty(model.StaffImagepath))
                                {
                                    // var arrayImgName = model.StaffImagepath.Substring(model.StaffImagepath.LastIndexOf("Teacher\\Photos\\") + 1).Split('\\')[2];
                                    //var arrayImgName = model.StaffImagepath.Substring(model.StaffImagepath.LastIndexOf("Teacher\\Photos\\") + 1).Split('\\')[2];
                                    var arrayImgName = model.StaffImagepath.Split('\\').LastOrDefault().Split('/').LastOrDefault();


                                    var staffimage = _IStaffService.GetStaffDocumentById((int)model.StaffImageId, true);
                                    if (staffimage != null)
                                    {
                                        //var filename = SaveStaffImage(model);
                                        staffimage.Image = arrayImgName;
                                        // staffimage.Image = arrayImgName;
                                        _IStaffService.UpdateStaffDocument(staffimage);

                                        // activity log
                                        // table Id
                                        var AttachedDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StaffDocuments").FirstOrDefault();
                                        if (AttachedDocumenttable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AttachedDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(staffimage.DocumentId, AttachedDocumenttable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update StaffDocuments with DocumentId:{0} and StaffId:{1}", staffimage.DocumentId, staffimage.StaffId), Request.Browser.Browser);
                                        }
                                    }
                                }
                            }
                            //insert staff image
                            else
                            {
                                if (!string.IsNullOrEmpty(model.StaffImagepath))
                                {
                                    // var arrayImgName = model.StaffImagepath.Substring(model.StaffImagepath.LastIndexOf("Teacher\\Photos\\") + 1).Split('\\')[2];
                                    var arrayImgName = model.StaffImagepath.Split('\\').LastOrDefault().Split('/').LastOrDefault();
                                    StaffDocument AttachedDocument = new StaffDocument();
                                    //  var filename = SaveStaffImage(model);
                                    /// var filename = SaveStaffImage(model);
                                    var filename = arrayImgName;
                                    // get doc type

                                    if (filename.Contains("_image"))
                                    {

                                        AttachedDocument.DocumentTypeId = doctypeid != null ? doctypeid.DocumentTypeId : nullablevariable;
                                        AttachedDocument.StaffId = staffInfo.StaffId;
                                        AttachedDocument.Image = filename;
                                        _IStaffService.InsertStaffDocument(AttachedDocument);
                                    }
                                    // activity log
                                    // table Id
                                    var AttachedDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StaffDocuments").FirstOrDefault();
                                    if (AttachedDocumenttable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AttachedDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(AttachedDocument.DocumentId, AttachedDocumenttable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add new AttachedDocument with DocumentId:{0} and StaffId:{1}", AttachedDocument.DocumentId, AttachedDocument.StaffId), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                    }
                    //insert staff info
                    else
                    {
                        var Stfcontacttype = _IAddressMasterService.GetAllContactTypes(ref count, _IStaffService.GetStaffTypeById((int)model.StaffTypeId).StaffType1).FirstOrDefault(); // staff contact type

                        //insert into staff table
                        staffInfo = new Staff();
                        staffInfo.FName = model.FirstName;
                        staffInfo.LName = model.LastName;
                        staffInfo.StaffTypeId = model.StaffTypeId;
                        staffInfo.IsActive = true;

                        int? machineID = null;
                        if (!string.IsNullOrEmpty(model.PunchMcId))
                            machineID = Convert.ToInt32(model.PunchMcId);

                        staffInfo.PunchMcId = machineID;
                        staffInfo.EmpCode = StaffEmpCodePrefix + StaffEmpCodeSplitter + model.Employeecode;
                        if (!string.IsNullOrEmpty(model.DateOfJoin))
                        {
                            DateTime date = DateTime.ParseExact(model.DateOfJoin, "dd/MM/yyyy", null);
                            staffInfo.DOJ = date;
                        }
                        if (!string.IsNullOrEmpty(model.DateOfBirth))
                        {
                            DateTime date = DateTime.ParseExact(model.DateOfBirth, "dd/MM/yyyy", null);
                            staffInfo.DOB = date;
                        }
                        staffInfo.BloodGroupId = model.BloodGroupId;
                        staffInfo.GenderId = model.GenderId;
                        staffInfo.DepartmentId = model.DepartmentId;
                        staffInfo.DesignationId = model.DesignationId;
                        staffInfo.AadharCardNo = model.AadharCard;
                        staffInfo.StaffCategoryId = model.StaffCategoryId;
                        staffInfo.ContactTypeId = Stfcontacttype.ContactTypeId;
                        staffInfo.PanNo = model.PanNo;
                        staffInfo.FatherName = model.FatherName;
                        staffInfo.FatherMobile = model.FatherMobNo;
                        staffInfo.MotherName = model.MotherName;
                        staffInfo.MotherMobile = model.MotherMobNo;
                        if (model.IsMarried != null && model.IsMarried == true)
                        {
                            staffInfo.IsMarried = model.IsMarried;
                            staffInfo.SpouseName = model.SpouseName;
                            staffInfo.SpouseMobile = model.SpouseMobNo;
                        }
                        else
                        {
                            staffInfo.IsMarried = model.IsMarried;
                            staffInfo.SpouseName = null;
                            staffInfo.SpouseMobile = null;
                        }
                       

                        _IStaffService.InsertStaff(staffInfo);

                        model.StaffId = staffInfo.StaffId;

                        //Insert Service Type
                        if (staffInfo.StaffId>0 && model.StaffServiceTypeId !=null&& model.StaffServiceTypeId > 0)
                        {
                            var StaffServiceTypeDetail = new StaffServiceTypeDetail();
                            StaffServiceTypeDetail.StaffId = staffInfo.StaffId;
                            StaffServiceTypeDetail.StaffServiceTypeId = model.StaffServiceTypeId;
                            if (!string.IsNullOrEmpty(model.DateOfJoin))
                            {
                                DateTime date = DateTime.ParseExact(model.DateOfJoin, "dd/MM/yyyy", null);
                                StaffServiceTypeDetail.ChangeDate = date;
                            }
                            else
                            {
                                StaffServiceTypeDetail.ChangeDate = DateTime.Now.Date;
                            }
                            StaffServiceTypeDetail.ChangeDate = DateTime.Now.Date;
                            _IStaffService.InsertStaffServiceTypeDetail(StaffServiceTypeDetail);
                        }
                        //Save mobile no 
                        if (model.MobileNo != null && model.MobileNo != "")
                        {
                            var contactinfoType = _IContactService.GetAllContactInfoTypes(ref count).Where(m => m.ContactInfoType1.ToLower() == "mobile").FirstOrDefault();
                            if (contactinfoType != null)
                            {
                                var contactinfoTypeId = contactinfoType.ContactInfoTypeId;
                                var stafftypeId = staffInfo.StaffTypeId;
                                var contacttype = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1.ToLower() == _IStaffService.GetStaffTypeById((int)stafftypeId).StaffType1.ToLower()).FirstOrDefault();
                                if (contacttype != null)
                                {
                                    var contacttypeid = contacttype.ContactTypeId;
                                    var contactinfo = _IContactService.GetAllContactInfos(ref count).Where(m => m.ContactId == staffInfo.StaffId && m.ContactInfoTypeId == contactinfoTypeId && m.ContactTypeId == contacttypeid && m.IsDefault == true).FirstOrDefault();
                                    if (contactinfo != null)
                                    {
                                        contactinfo.ContactInfo1 = model.MobileNo;
                                        _IContactService.UpdateContactInfo(contactinfo);
                                    }
                                    else
                                    {
                                        var contact_info = new ContactInfo();
                                        contact_info.ContactId = staffInfo.StaffId;
                                        contact_info.ContactInfo1 = model.MobileNo;
                                        contact_info.ContactInfoTypeId = contactinfoTypeId;
                                        contact_info.IsDefault = true;
                                        contact_info.Status = true;
                                        contact_info.ContactTypeId = contacttypeid;
                                        _IContactService.InsertContactInfo(contact_info);
                                    }
                                }
                            }
                        }
                        if (Stafftable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, Stafftable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog((int)staffInfo.StaffId, (int)staffInfo.StaffTypeId, logtype.ActivityLogTypeId, user, String.Format("Add Staff with StaffId:{0} and StaffTypeId:{1}", staffInfo.StaffId, staffInfo.StaffTypeId), Request.Browser.Browser);
                        }

                        //insert staff image
                        if (!string.IsNullOrEmpty(model.StaffImagepath))
                        {
                            StaffDocument AttachedDocument = new StaffDocument();
                            var filename = SaveStaffImage(model);
                            // get doc type

                            AttachedDocument.DocumentTypeId = doctypeid != null ? doctypeid.DocumentTypeId : nullablevariable;
                            AttachedDocument.StaffId = staffInfo.StaffId;
                            AttachedDocument.Image = filename;
                            _IStaffService.InsertStaffDocument(AttachedDocument);

                            // activity log
                            // table Id
                            var AttachedDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StaffDocuments").FirstOrDefault();
                            if (AttachedDocumenttable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AttachedDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(AttachedDocument.DocumentId, AttachedDocumenttable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add new AttachedDocument with DocumentId:{0} and StaffId:{1}", AttachedDocument.DocumentId, AttachedDocument.StaffId), Request.Browser.Browser);
                            }
                        }
                    }
                    // insert StaffStatusDetail
                    var StaffStatusDetil = new StaffStatusDetail();
                    StaffStatusDetil.StaffId = model.StaffId;
                    if (!string.IsNullOrEmpty(model.DateOfJoin))
                    {
                        DateTime date = DateTime.ParseExact(model.DateOfJoin, "dd/MM/yyyy", null);
                        StaffStatusDetil.StatusChangeDate = date;
                    }
                    else
                    {
                        StaffStatusDetil.StatusChangeDate = DateTime.Now;
                    }
                    StaffStatusDetil.StaffStatus = true;
                    _IStaffService.InsertStaffStatus(StaffStatusDetil);


                    //check ths user logg in process
                    KSModel.Models.UserLoginCheck LoginCheckData = new KSModel.Models.UserLoginCheck();
                    var getLoginCheck = _IUserService.GetAllUserLoginChecks(ref count);
                    var currentStatus = staffInfo.IsActive;
                    var getUserType = _IAddressMasterService.GetAllContactTypes(ref count);
                    var getStatus = _IStaffService.GetStaffById(StaffId: staffInfo.StaffId);

                    if (model.IsActive != getStatus.IsActive)
                    {
                        if (getUserType.Count() > 0)
                        {
                            var getstaffInfo = _IUserService.GetAllUsers(ref count);
                            foreach (var type in getUserType)
                            {
                                switch (type.ContactType1.ToLower())
                                {
                                    case "teacher":
                                        var getInfo = getstaffInfo.Where(x => x.UserContactId == staffInfo.StaffId && x.UserTypeId == type.ContactTypeId).FirstOrDefault();
                                        if (getInfo != null)
                                        {
                                            var isUserLoginCheck = getLoginCheck.Where(x => x.UserId == (int)getInfo.UserId).FirstOrDefault();
                                            if (isUserLoginCheck != null)
                                            {

                                                if (getStatus != null)
                                                {
                                                    if ((bool)currentStatus)
                                                    {
                                                        //update 
                                                        LoginCheckData = _IUserService.GetUserLoginCheckById(isUserLoginCheck.UserLoginCheckId, true);
                                                        if (LoginCheckData != null)
                                                        {
                                                            LoginCheckData.Datefrom = DateTime.UtcNow.Date;
                                                            LoginCheckData.DateTill = DateTime.UtcNow.Date.AddDays(7);

                                                            _IUserService.UpdateUserLoginCheck(LoginCheckData);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //update 
                                                        LoginCheckData = _IUserService.GetUserLoginCheckById(isUserLoginCheck.UserLoginCheckId, true);
                                                        if (LoginCheckData != null)
                                                        {
                                                            LoginCheckData.DateTill = DateTime.UtcNow.Date;
                                                            _IUserService.UpdateUserLoginCheck(LoginCheckData);
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                        break;
                                    case "non-teaching":
                                        var getNonTeachInfo = getstaffInfo.Where(x => x.UserContactId == staffInfo.StaffId && x.UserTypeId == type.ContactTypeId).FirstOrDefault();
                                        if (getNonTeachInfo != null)
                                        {
                                            var isUserLoginCheck = getLoginCheck.Where(x => x.UserId == (int)getNonTeachInfo.UserId).FirstOrDefault();
                                            if (isUserLoginCheck != null)
                                            {
                                                if (getStatus != null)
                                                {
                                                    if ((bool)currentStatus)
                                                    {
                                                        //update 
                                                        LoginCheckData = _IUserService.GetUserLoginCheckById(isUserLoginCheck.UserLoginCheckId, true);
                                                        if (LoginCheckData != null)
                                                        {
                                                            LoginCheckData.Datefrom = DateTime.UtcNow.Date;
                                                            LoginCheckData.DateTill = DateTime.UtcNow.Date.AddDays(7);

                                                            _IUserService.UpdateUserLoginCheck(LoginCheckData);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        //update 
                                                        LoginCheckData = _IUserService.GetUserLoginCheckById(isUserLoginCheck.UserLoginCheckId, true);
                                                        if (LoginCheckData != null)
                                                        {
                                                            LoginCheckData.DateTill = DateTime.UtcNow.Date;

                                                            _IUserService.UpdateUserLoginCheck(LoginCheckData);
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                        break;
                                }
                            }
                        }
                    }


                    if (editmode)
                        TempData["SuccessNotification"] = model.FirstName + " " + model.LastName + " has been updated successfully";
                    else
                        TempData["SuccessNotification"] = model.FirstName + " " + model.LastName + " has been added successfully";

                    var staffid = _ICustomEncryption.base64e(model.StaffId.ToString());
                    return RedirectToAction("Edit", new { Id = staffid });
                }
                catch (Exception ex)
                {
                    _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                    return RedirectToAction("General", "Error");
                }
            }
            if (model.StaffId > 0)
            {
                TempData["Staff_Info_ViewData"] = model;
                var staffid = _ICustomEncryption.base64e(model.StaffId.ToString());
                return RedirectToAction("Edit", new { Id = staffid });
            }
            else
            {
                model = prepareStaffModel(model);
                return View(model);
            }
        }

        public StaffModel PostPrepareStaffAddressModel(StaffModel model)
        {
            #region // bind model with form // Staff Address

            // address
            TempData["AddressId_Student"] = model.AddressModel.AddressId == 0 ? "" : model.AddressModel.AddressId.ToString();
            TempData["AddressTypeId_Student"] = model.AddressModel.AddressTypeId == 0 ? "" : model.AddressModel.AddressTypeId.ToString();
            TempData["PlotNo_Student"] = model.AddressModel.PlotNo;
            TempData["Sector_Street_Student"] = model.AddressModel.Sector_Street;
            TempData["Locality_Student"] = model.AddressModel.Locality;
            TempData["Landmark_Student"] = model.AddressModel.Landmark;
            TempData["District_Student"] = model.AddressModel.AddressDistrict;
            TempData["Address_CityId_Student"] = model.AddressModel.Address_CityId == 0 ? "" : model.AddressModel.Address_CityId.ToString();
            TempData["Zip_Student"] = model.AddressModel.Zip;

            #endregion

            return model;
        }
        //check error for address
        public StaffModel checkAddressModelErrors(StaffModel model, out bool modelstate)
        {
            modelstate = true;

            // address validate
            if (model.AddressModel.AddressTypeId == null || model.AddressModel.AddressTypeId == 0)
            {
                modelstate = false;
                ModelState.AddModelError("AddressTypeIdStudent", "Address Type required");
            }

            //if (string.IsNullOrWhiteSpace(model.AddressModel.PlotNo))
            //{
            //    modelstate = false;
            //    ModelState.AddModelError("PlotNoStudent", "PlotNo required");
            //}

            //if (string.IsNullOrWhiteSpace(model.AddressModel.Sector_Street))
            //{
            //    modelstate = false;
            //    ModelState.AddModelError("SectorStreet_Student", "Sector_Street required");
            //}

            if (model.AddressModel.Address_CityId == 0)
            {
                modelstate = false;
                ModelState.AddModelError("Address.CityId_Student", "Address City required");
            }

            if (!string.IsNullOrWhiteSpace(model.AddressModel.Zip.Trim('0')) && model.AddressModel.Zip.Length != 6)
            {
                modelstate = false;
                ModelState.AddModelError("Zip.Student", "Zip Code not valid");
            }
            return model;
        }

        //bind the address with model
        public StaffModel BindStaffAddressWithStaffModel(StaffModel model, FormCollection form)
        {
            #region // bind form with model // Staff Address

            // address
            if (form["AddressTypeId_Staff"] != "")
                model.AddressModel.AddressTypeId = Convert.ToInt32(form["AddressTypeId_Staff"]);

            if (form["Address_CityId_Staff"] != "")
                model.AddressModel.Address_CityId = Convert.ToInt32(form["Address_CityId_Staff"]);

            model.AddressModel.PlotNo = form["PlotNo_Staff"];
            model.AddressModel.Sector_Street = form["Sector_Street_Staff"];
            model.AddressModel.Locality = form["Locality_Staff"];
            model.AddressModel.Landmark = form["Landmark_Staff"];
            model.AddressModel.AddressDistrict = form["District_Staff"];
            model.AddressModel.Zip = form["Zip_Staff"];

            #endregion

            return model;
        }

        //save the address
        [HttpPost]
        public ActionResult Address(StaffModel model, FormCollection form)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            bool modelValid;
            model = BindStaffAddressWithStaffModel(model, form); // bindform with model
            model = checkAddressModelErrors(model, out modelValid); // check form validate
            bool editmode = false;
            // check id exist
            if (!string.IsNullOrEmpty(model.EncStaffId))
            {
                // decrypt Id
                var qsid = _ICustomEncryption.base64d(model.EncStaffId);
                int staff_id = 0;
                if (int.TryParse(qsid, out staff_id))
                {
                    model.StaffId = staff_id;
                }
            }
            if (!string.IsNullOrEmpty(form["AddressId_Staff"]))
            {
                // decrypt Id
                var qsid = _ICustomEncryption.base64d(form["AddressId_Staff"]);
                int id = 0;
                if (int.TryParse(qsid, out id))
                {
                    model.AddressModel.AddressId = id;
                }
            }
            var staffid = _ICustomEncryption.base64e(model.StaffId.ToString());
            if (modelValid)
            {
                try
                {
                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();

                    #region  Staff Address

                    var addressId = form["AddressId_Staff"] != "" ? (int)model.AddressModel.AddressId : 0;
                    if (addressId > 0)
                    {
                        var address = _IAddressService.GetAddressById(addressId, true);
                        if (address != null)
                        {
                            address.AddressTypeId = model.AddressModel.AddressTypeId;
                            address.CityId = model.AddressModel.Address_CityId;
                            address.Landmark = model.AddressModel.Landmark != null ? model.AddressModel.Landmark.TrimEnd(',') : null;
                            address.District = model.AddressModel.AddressDistrict != null ? model.AddressModel.AddressDistrict.TrimEnd(',') : null;
                            address.Locality = model.AddressModel.Locality != null ? model.AddressModel.Locality.TrimEnd(',') : null;
                            address.PlotNo = model.AddressModel.PlotNo != null ? model.AddressModel.PlotNo.TrimEnd(',') : null;
                            address.Sector_Street = model.AddressModel.Sector_Street != null ? model.AddressModel.Sector_Street.TrimEnd(',') : null;
                            address.Zip = model.AddressModel.Zip;

                            _IAddressService.UpdateAddress(address);
                            editmode = true;
                            // table Id
                            var tableaddress = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "Address").FirstOrDefault();
                            if (tableaddress != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, tableaddress.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(address.AddressId, tableaddress.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update Address with AddressId:{0} and ContactId:{1}", address.AddressId, model.StaffId), Request.Browser.Browser);
                            }
                        }
                    }
                    else
                    {

                        KSModel.Models.Address address = new KSModel.Models.Address();
                        address.ContactId = model.StaffId;
                        address.AddressTypeId = model.AddressModel.AddressTypeId;
                        address.ContactTypeId = _IAddressMasterService.GetAllContactTypes(ref count, _IStaffService.GetStaffTypeById((int)model.StaffTypeId).StaffType1).FirstOrDefault().ContactTypeId;
                        address.CityId = model.AddressModel.Address_CityId;
                        address.Landmark = model.AddressModel.Landmark != null ? model.AddressModel.Landmark.TrimEnd(',') : null;
                        address.District = model.AddressModel.AddressDistrict != null ? model.AddressModel.AddressDistrict.TrimEnd(',') : null;
                        address.Locality = model.AddressModel.Locality != null ? model.AddressModel.Locality.TrimEnd(',') : null;
                        address.PlotNo = model.AddressModel.PlotNo != null ? model.AddressModel.PlotNo.TrimEnd(',') : null;
                        address.Sector_Street = model.AddressModel.Sector_Street != null ? model.AddressModel.Sector_Street.TrimEnd(',') : null;
                        address.Zip = model.AddressModel.Zip;
                        _IAddressService.InsertAddress(address);

                        // table Id
                        var tableaddress = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "Address").FirstOrDefault();
                        if (tableaddress != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, tableaddress.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(address.AddressId, tableaddress.TableId, logtype.ActivityLogTypeId, user, String.Format("Add new Address with AddressId:{0} and ContactId:{1}", address.AddressId, model.StaffId), Request.Browser.Browser);
                        }
                    }

                    #endregion

                    if (editmode)
                        TempData["SuccessNotification"] = model.FirstName + " " + model.LastName + " Address has been updated successfully";
                    else
                        TempData["SuccessNotification"] = model.FirstName + " " + model.LastName + " Address has been added successfully";

                    return RedirectToAction("Edit", new { Id = staffid });
                }
                catch (Exception ex)
                {
                    _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                    return RedirectToAction("General", "Error");
                }
            }
            model = prepareStaffModel(model);
            model = PostPrepareStaffAddressModel(model);
            TempData["Staff_Address"] = model;

            return RedirectToAction("Edit", new { Id = staffid });
        }

        //save the uploaded documents
        [HttpPost]
        public ActionResult Documents(StaffModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            bool editmode = false;
            // check id exist
            if (!string.IsNullOrEmpty(model.EncStaffId))
            {
                // decrypt Id
                var qsid = _ICustomEncryption.base64d(model.EncStaffId);
                int staff_id = 0;
                if (int.TryParse(qsid, out staff_id))
                {
                    model.StaffId = staff_id;
                }
            }

            var staffid = _ICustomEncryption.base64e(model.StaffId.ToString());
            try
            {
                // Activity log type add
                var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();

                #region Documents
                var singleUploadDocumentIds = _IStaffService.GetAllStaffDocumentTypes(ref count).Where(p => p.IsMultipleAllowed == false && p.DocumentType.ToLower() != "image").Select(p => p.DocumentTypeId).ToList();
                System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<AttachedDocumentModel> DocumentArray = js.Deserialize<List<AttachedDocumentModel>>(model.DocumentArray);
                foreach (var Document in DocumentArray)
                {
                    if (Document.AttachDocumentTypeId > 0 && !string.IsNullOrEmpty(Document.Image))
                    {
                        string filename = string.Empty;
                        string path = Document.Image;
                        // move directory one time
                        var pathary = path.Split('\\');
                        if (pathary.Length > 1)
                            filename = pathary[pathary.Length - 1];
                        else
                        {
                            pathary = path.Split('/');
                            if (pathary.Length > 1)
                                filename = pathary[pathary.Length - 1];
                        }

                        if (Document.AttachDocumentId > 0)
                        {
                            var AttachedDocument = _IStaffService.GetStaffDocumentById((int)Document.AttachDocumentId, true);
                            if (AttachedDocument != null)
                            {
                                AttachedDocument.DocumentTypeId = Document.AttachDocumentTypeId;
                                AttachedDocument.Image = filename;
                                AttachedDocument.Description = Document.Description;
                                _IStaffService.UpdateStaffDocument(AttachedDocument);
                                editmode = true;

                                // AttachedDocument Id
                                var AttachedDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StaffDocuments").FirstOrDefault();
                                if (AttachedDocumenttable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AttachedDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(AttachedDocument.DocumentId, AttachedDocumenttable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update StaffDocuments with DocumentId:{0} and StaffId:{1}", AttachedDocument.DocumentId, AttachedDocument.StaffId), Request.Browser.Browser);
                                }
                            }
                        }
                        else
                        {
                            if (singleUploadDocumentIds.Contains((int)Document.AttachDocumentTypeId))
                            {
                                var arrayducuments = DocumentArray.Where(p => singleUploadDocumentIds.Contains((int)p.AttachDocumentTypeId)).ToList();
                                if (arrayducuments.Count > 0)
                                {
                                    var query = arrayducuments.GroupBy(x => x.AttachDocumentTypeId).Where(g => g.Count() > 1).ToList();
                                    if (query.Count > 0)
                                    {
                                        TempData["ErrorNotification"] = "Single uploadable document shouldn,t be multiple.";
                                        continue;
                                    }
                                }
                            }
                            StaffDocument AttachedDocument = new StaffDocument()
                            {
                                DocumentTypeId = Document.AttachDocumentTypeId,
                                StaffId = model.StaffId,
                                Image = filename,
                                Description = Document.Description
                            };
                            _IStaffService.InsertStaffDocument(AttachedDocument);
                            // AttachedDocument Id
                            var AttachedDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StaffDocuments").FirstOrDefault();
                            if (AttachedDocumenttable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AttachedDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(AttachedDocument.DocumentId, AttachedDocumenttable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add new StaffDocuments with DocumentId:{0} and StaffId:{1}", AttachedDocument.DocumentId, AttachedDocument.StaffId), Request.Browser.Browser);
                            }
                        }
                    }
                }

                if (DocumentArray.Count > 0)
                {
                    if (!editmode && TempData["ErrorNotification"] == null)
                        TempData["SuccessNotification"] = model.FirstName + " " + model.LastName + " Documents has been added successfully";
                    else if (editmode && TempData["ErrorNotification"] == null)
                        TempData["SuccessNotification"] = model.FirstName + " " + model.LastName + " Documents has been updated successfully";
                    return RedirectToAction("Edit", new { Id = staffid });
                }
                #endregion
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }

            model = prepareStaffModel(model);
            return RedirectToAction("Edit", new { Id = staffid });
        }

        public ActionResult DeleteContact(int ContactInfoId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            var logtypeupdate = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
            var staffcontactinfo = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ContactInfo").FirstOrDefault();
            try
            {
                if (ContactInfoId > 0)
                {
                    var contact = _IContactService.GetContactInfoById(ContactInfoId);
                    if (contact != null)
                    {
                        _IContactService.DeleteContactInfo(ContactInfoId);
                        if (staffcontactinfo != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, staffcontactinfo.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog((int)contact.ContactId, staffcontactinfo.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete ContactInfo with ContactId:{0} and ContactInfoTypeId:{1}", contact.ContactId, contact.ContactInfoTypeId), Request.Browser.Browser);
                        }
                    }
                    return Json(new { status = "success", data = "Deleted Successfully." });
                }
                else
                {
                    return Json(new { status = "failed", data = "error" });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "error",
                    msg = "Error occurred. Error details: " + ex.Message
                });
            }
        }
        //save staff contacts
        [HttpPost]
        public ActionResult Contacts(StaffModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            int? nullablevariable = null;
            bool editmode = false;

            // check id exist
            if (!string.IsNullOrEmpty(model.EncStaffId))
            {
                // decrypt Id
                var qsid = _ICustomEncryption.base64d(model.EncStaffId);
                int staff_id = 0;
                if (int.TryParse(qsid, out staff_id))
                {
                    model.StaffId = staff_id;
                }
            }

            var staffid = _ICustomEncryption.base64e(model.StaffId.ToString());
            int staffIdk = Convert.ToInt32(model.StaffId);
            var staff = _IStaffService.GetStaffById(StaffId: staffIdk);
            try
            {
                // Activity log type add
                var errorstring = "";
                var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                var staffcontactinfo = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ContactInfo").FirstOrDefault();
                List<ContactInfoModel> deserializedContactInfoList = (List<ContactInfoModel>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.ContactsArray, typeof(List<ContactInfoModel>));
                if (deserializedContactInfoList.Count > 0)
                {
                    var contatinfotype = _IAddressMasterService.GetAllContactTypes(ref count, _IStaffService.GetStaffTypeById((int)model.StaffTypeId).StaffType1).FirstOrDefault();
                    foreach (var contactinfo in deserializedContactInfoList)
                    {
                        var isContactExist = _IContactService.GetAllContactInfos(ref count, ContactInfo: contactinfo.ContactInfo, ContactTypeId: staff.ContactTypeId).FirstOrDefault();
                        if (isContactExist != null && isContactExist.ContactId != model.StaffId)
                        {
                            errorstring += errorstring == "" ? isContactExist.ContactInfoType.ContactInfoType1 : "," + isContactExist.ContactInfoType.ContactInfoType1;
                        }
                        else
                        {
                            var ContactInfoTypeId = _IContactService.GetAllContactInfoTypes(ref count, contactinfo.ContactInfoType).FirstOrDefault().ContactInfoTypeId;
                            if (contactinfo.ContactInfoId > 0)
                            {
                                var staffContactInfo = _IContactService.GetContactInfoById((int)contactinfo.ContactInfoId, true);
                                if (staffContactInfo != null)
                                {
                                    staffContactInfo.ContactInfo1 = contactinfo.ContactInfo;
                                    staffContactInfo.IsDefault = contactinfo.IsDefault;
                                    staffContactInfo.Status = contactinfo.Status;
                                    _IContactService.UpdateContactInfo(staffContactInfo);
                                    editmode = true;
                                    // contactinfo Id
                                    if (staffcontactinfo != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, staffcontactinfo.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog((int)contactinfo.ContactInfoId, staffcontactinfo.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update ContactInfo with ContactInfoId:{0} and ContactId:{1}", contactinfo.ContactInfoId, contactinfo.ContactId), Request.Browser.Browser);
                                    }
                                }
                            }
                            else
                            {
                                ContactInfo ContactInfo = new ContactInfo()
                                {
                                    ContactInfo1 = contactinfo.ContactInfo,
                                    ContactInfoTypeId = ContactInfoTypeId,
                                    ContactId = model.StaffId,
                                    ContactTypeId = contatinfotype != null ? contatinfotype.ContactTypeId : nullablevariable,
                                    Status = contactinfo.Status,
                                    IsDefault = contactinfo.IsDefault
                                };
                                _IContactService.InsertContactInfo(ContactInfo);
                                if (staffcontactinfo != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, staffcontactinfo.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(ContactInfo.ContactInfoId, staffcontactinfo.TableId, logtype.ActivityLogTypeId, user, String.Format("Add new ContactInfo with ContactInfoId:{0} and ContactId:{1}", ContactInfo.ContactInfoId, ContactInfo.ContactId), Request.Browser.Browser);
                                }
                            }
                        }
                    }
                    if (errorstring != "")
                        TempData["ErrorNotification"] = errorstring + " already exist for another staff member.";
                    if (deserializedContactInfoList.Count > 0)
                    {
                        if (editmode && TempData["ErrorNotification"] == null)
                            TempData["SuccessNotification"] = model.FirstName + " " + model.LastName + " contacts has been updated successfully";
                        else if (!editmode && TempData["ErrorNotification"] == null)
                            TempData["SuccessNotification"] = model.FirstName + " " + model.LastName + " contacts has been added successfully";

                        return RedirectToAction("Edit", new { Id = staffid });
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }

            model = prepareStaffModel(model);

            return RedirectToAction("Edit", new { Id = staffid });
        }

        //upload staff file
        public ActionResult UploadStaffProfileImage()
        {
            if (Request.Files.Count > 0)
            {
                string path = String.Empty;
                try
                {
                    int maxfilesize = 100; // default size
                    var ProfilePicMaxSizeSetting = _ISchoolSettingService.GetAttributeValue("ProfilePicMaxSize");
                    if (ProfilePicMaxSizeSetting != null && !string.IsNullOrEmpty(ProfilePicMaxSizeSetting))
                        maxfilesize = Convert.ToInt32(ProfilePicMaxSizeSetting);

                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //file
                        HttpPostedFileBase file = files[i];
                        // get Filename
                        string fileName;
                        string uniqueno = Request.Form["uniqueno"];

                        int StaffId = 0;
                        // decrypt Id
                        if (!string.IsNullOrEmpty(uniqueno) && uniqueno != "0")
                        {
                            try
                            {
                                //int.TryParse(_ICustomEncryption.base64d(uniqueno), out StaffId);
                                StaffId = Convert.ToInt32(uniqueno);
                            }
                            catch
                            {
                                StaffId = Convert.ToInt32(uniqueno);
                            }
                        }

                        //check that the file is of the permitted file type
                        string fileExtension = Path.GetExtension(file.FileName).ToLower();
                        string[] acceptedFileTypes = new string[3];
                        acceptedFileTypes[0] = ".jpg";
                        acceptedFileTypes[1] = ".jpeg";
                        acceptedFileTypes[2] = ".png";

                        string uniqueimage = Request.Form["uniqueimage"];
                        var Isphoto = Convert.ToInt32(Request.Form["IsPhoto"]);

                        bool acceptFile = false;
                        //should we accept the file?
                        for (int j = 0; j < acceptedFileTypes.Count(); j++)
                        {
                            if (fileExtension == acceptedFileTypes[j])
                            {
                                //accept the file, yay!
                                Isphoto = 1;
                                acceptFile = true;
                                break;
                            }
                        }

                        if (Isphoto == 1)
                        {
                            fileName = StaffId.ToString() + "_image" + Path.GetExtension(file.FileName).ToLower();
                        }
                        else
                        {
                            fileName = StaffId.ToString() + "_doc" + uniqueimage + Path.GetExtension(file.FileName).ToLower();
                        }

                        //Is the file too big to upload?
                        int fileSize = file.ContentLength;
                        if (fileSize > (maxfilesize * 1024))
                        {
                            return Json(new
                            {
                                status = "error",
                                msg = "Filesize of image is too large. Maximum file size permitted is " + maxfilesize + "KB"
                            });
                        }


                        if (!acceptFile)
                            return Json(new
                            {
                                status = "error",
                                msg = "The file you are trying to upload is not a permitted file type!"
                            });

                        // check directory exist or not
                        var schoolid = HttpContext.Session["SchoolId"];
                        // var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Teacher/Photos/" + StaffId.ToString()));
                        var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Teacher/Photos/"));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Teacher/Photos/"), fileName);

                        // Get the complete folder path and store the file inside it.  
                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);

                        if (Isphoto == 1)
                        {
                            var resizeratio = _ISchoolSettingService.GetAttributeValue("StaffPhotoRatio");

                            var height = Convert.ToInt32(resizeratio.Split(',')[0]);
                            var width = Convert.ToInt32(resizeratio.Split(',')[1]);

                            var resizedimg = _ISchoolDataService.ResizeImage(file, "staffimage", height, width);
                            resizedimg.Save(path);
                        }
                        else
                            file.SaveAs(path);
                    }
                    // Returns message that successfully uploaded  
                    return Json(new
                    {
                        status = "success",
                        path = path,
                        msg = "File Uploaded Successfully!"
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        status = "error",
                        msg = "Error occurred. Error details: " + ex.Message
                    });
                }
            }
            else
            {
                return Json(new
                {
                    status = "error",
                    msg = "No files selected."
                });
            }
        }

        //upload staff file
        public ActionResult UploadStaffFile()
        {
            if (Request.Files.Count > 0)
            {
                string path = String.Empty;
                try
                {
                    int maxfilesize = 100; // default size
                    var ProfilePicMaxSizeSetting = _ISchoolSettingService.GetAttributeValue("ProfilePicMaxSize");
                    if (ProfilePicMaxSizeSetting != null && !string.IsNullOrEmpty(ProfilePicMaxSizeSetting))
                        maxfilesize = Convert.ToInt32(ProfilePicMaxSizeSetting);

                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //file
                        HttpPostedFileBase file = files[i];
                        // get Filename
                        string fileName;
                        string uniqueno = Request.Form["uniqueno"];

                        int StaffId = 0;
                        // decrypt Id
                        if (!string.IsNullOrEmpty(uniqueno) && uniqueno != "0")
                        {
                            try
                            {
                                int.TryParse(_ICustomEncryption.base64d(uniqueno), out StaffId);
                            }
                            catch
                            {
                                StaffId = Convert.ToInt32(uniqueno);
                            }
                        }

                        //check that the file is of the permitted file type
                        string fileExtension = Path.GetExtension(file.FileName).ToLower();
                        string[] acceptedFileTypes = new string[3];
                        acceptedFileTypes[0] = ".jpg";
                        acceptedFileTypes[1] = ".jpeg";
                        acceptedFileTypes[2] = ".png";

                        string uniqueimage = Request.Form["uniqueimage"];
                        var Isphoto = Convert.ToInt32(Request.Form["IsPhoto"]);

                        bool acceptFile = false;
                        //should we accept the file?
                        for (int j = 0; j < acceptedFileTypes.Count(); j++)
                        {
                            if (fileExtension == acceptedFileTypes[j])
                            {
                                //accept the file, yay!
                                Isphoto = 1;
                                acceptFile = true;
                                break;
                            }
                        }

                        if (Isphoto == 1)
                        {
                            fileName = StaffId.ToString() + "_image" + uniqueimage + Path.GetExtension(file.FileName).ToLower();
                        }
                        else
                        {
                            fileName = StaffId.ToString() + "_doc" + uniqueimage + Path.GetExtension(file.FileName).ToLower();
                        }

                        //Is the file too big to upload?
                        int fileSize = file.ContentLength;
                        if (fileSize > (maxfilesize * 1024))
                        {
                            return Json(new
                            {
                                status = "error",
                                msg = "Filesize of image is too large. Maximum file size permitted is " + maxfilesize + "KB"
                            });
                        }


                        if (!acceptFile)
                            return Json(new
                            {
                                status = "error",
                                msg = "The file you are trying to upload is not a permitted file type!"
                            });

                        // check directory exist or not
                        var schoolid = HttpContext.Session["SchoolId"];
                        var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Teacher/" + StaffId.ToString()));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Teacher/" + StaffId.ToString()), fileName);

                        // Get the complete folder path and store the file inside it.  
                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);

                        if (Isphoto == 1)
                        {
                            var resizeratio = _ISchoolSettingService.GetAttributeValue("StaffPhotoRatio");

                            var height = Convert.ToInt32(resizeratio.Split(',')[0]);
                            var width = Convert.ToInt32(resizeratio.Split(',')[1]);

                            var resizedimg = _ISchoolDataService.ResizeImage(file, "staffimage", height, width);
                            resizedimg.Save(path);
                        }
                        else
                            file.SaveAs(path);
                    }
                    // Returns message that successfully uploaded  
                    return Json(new
                    {
                        status = "success",
                        path = path,
                        msg = "File Uploaded Successfully!"
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        status = "error",
                        msg = "Error occurred. Error details: " + ex.Message
                    });
                }
            }
            else
            {
                return Json(new
                {
                    status = "error",
                    msg = "No files selected."
                });
            }
        }

        //private string GetMimeType(string fileName)
        //{
        //    string mimeType = "application/unknown";
        //    string ext = System.IO.Path.GetExtension(fileName).ToLower();
        //    Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
        //    if (regKey != null && regKey.GetValue("Content Type") != null)
        //        mimeType = regKey.GetValue("Content Type").ToString();
        //    return mimeType;
        //}

        private static IDictionary<string, string> BuildMappings()
        {
            var mappings = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase) {
 
                #region Big freaking list of mime types
            
                // maps both ways,
                // extension -> mime type
                //   and
                // mime type -> extension
                //
                // any mime types on left side not pre-loaded on right side, are added automatically
                // some mime types can map to multiple extensions, so to get a deterministic mapping,
                // add those to the dictionary specifcially
                //
                // combination of values from Windows 7 Registry and 
                // from C:\Windows\System32\inetsrv\config\applicationHost.config
                // some added, including .7z and .dat
                //
                // Some added based on http://www.iana.org/assignments/media-types/media-types.xhtml
                // which lists mime types, but not extensions
                //
                {".323", "text/h323"},
                {".3g2", "video/3gpp2"},
                {".3gp", "video/3gpp"},
                {".3gp2", "video/3gpp2"},
                {".3gpp", "video/3gpp"},
                {".7z", "application/x-7z-compressed"},
                {".aa", "audio/audible"},
                {".AAC", "audio/aac"},
                {".aaf", "application/octet-stream"},
                {".aax", "audio/vnd.audible.aax"},
                {".ac3", "audio/ac3"},
                {".aca", "application/octet-stream"},
                {".accda", "application/msaccess.addin"},
                {".accdb", "application/msaccess"},
                {".accdc", "application/msaccess.cab"},
                {".accde", "application/msaccess"},
                {".accdr", "application/msaccess.runtime"},
                {".accdt", "application/msaccess"},
                {".accdw", "application/msaccess.webapplication"},
                {".accft", "application/msaccess.ftemplate"},
                {".acx", "application/internet-property-stream"},
                {".AddIn", "text/xml"},
                {".ade", "application/msaccess"},
                {".adobebridge", "application/x-bridge-url"},
                {".adp", "application/msaccess"},
                {".ADT", "audio/vnd.dlna.adts"},
                {".ADTS", "audio/aac"},
                {".afm", "application/octet-stream"},
                {".ai", "application/postscript"},
                {".aif", "audio/aiff"},
                {".aifc", "audio/aiff"},
                {".aiff", "audio/aiff"},
                {".air", "application/vnd.adobe.air-application-installer-package+zip"},
                {".amc", "application/mpeg"},
                {".anx", "application/annodex"},
                {".apk", "application/vnd.android.package-archive" },
                {".application", "application/x-ms-application"},
                {".art", "image/x-jg"},
                {".asa", "application/xml"},
                {".asax", "application/xml"},
                {".ascx", "application/xml"},
                {".asd", "application/octet-stream"},
                {".asf", "video/x-ms-asf"},
                {".ashx", "application/xml"},
                {".asi", "application/octet-stream"},
                {".asm", "text/plain"},
                {".asmx", "application/xml"},
                {".aspx", "application/xml"},
                {".asr", "video/x-ms-asf"},
                {".asx", "video/x-ms-asf"},
                {".atom", "application/atom+xml"},
                {".au", "audio/basic"},
                {".avi", "video/x-msvideo"},
                {".axa", "audio/annodex"},
                {".axs", "application/olescript"},
                {".axv", "video/annodex"},
                {".bas", "text/plain"},
                {".bcpio", "application/x-bcpio"},
                {".bin", "application/octet-stream"},
                {".bmp", "image/bmp"},
                {".c", "text/plain"},
                {".cab", "application/octet-stream"},
                {".caf", "audio/x-caf"},
                {".calx", "application/vnd.ms-office.calx"},
                {".cat", "application/vnd.ms-pki.seccat"},
                {".cc", "text/plain"},
                {".cd", "text/plain"},
                {".cdda", "audio/aiff"},
                {".cdf", "application/x-cdf"},
                {".cer", "application/x-x509-ca-cert"},
                {".cfg", "text/plain"},
                {".chm", "application/octet-stream"},
                {".class", "application/x-java-applet"},
                {".clp", "application/x-msclip"},
                {".cmd", "text/plain"},
                {".cmx", "image/x-cmx"},
                {".cnf", "text/plain"},
                {".cod", "image/cis-cod"},
                {".config", "application/xml"},
                {".contact", "text/x-ms-contact"},
                {".coverage", "application/xml"},
                {".cpio", "application/x-cpio"},
                {".cpp", "text/plain"},
                {".crd", "application/x-mscardfile"},
                {".crl", "application/pkix-crl"},
                {".crt", "application/x-x509-ca-cert"},
                {".cs", "text/plain"},
                {".csdproj", "text/plain"},
                {".csh", "application/x-csh"},
                {".csproj", "text/plain"},
                {".css", "text/css"},
                {".csv", "text/csv"},
                {".cur", "application/octet-stream"},
                {".cxx", "text/plain"},
                {".dat", "application/octet-stream"},
                {".datasource", "application/xml"},
                {".dbproj", "text/plain"},
                {".dcr", "application/x-director"},
                {".def", "text/plain"},
                {".deploy", "application/octet-stream"},
                {".der", "application/x-x509-ca-cert"},
                {".dgml", "application/xml"},
                {".dib", "image/bmp"},
                {".dif", "video/x-dv"},
                {".dir", "application/x-director"},
                {".disco", "text/xml"},
                {".divx", "video/divx"},
                {".dll", "application/x-msdownload"},
                {".dll.config", "text/xml"},
                {".dlm", "text/dlm"},
                {".doc", "application/msword"},
                {".docm", "application/vnd.ms-word.document.macroEnabled.12"},
                {".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"},
                {".dot", "application/msword"},
                {".dotm", "application/vnd.ms-word.template.macroEnabled.12"},
                {".dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template"},
                {".dsp", "application/octet-stream"},
                {".dsw", "text/plain"},
                {".dtd", "text/xml"},
                {".dtsConfig", "text/xml"},
                {".dv", "video/x-dv"},
                {".dvi", "application/x-dvi"},
                {".dwf", "drawing/x-dwf"},
                {".dwp", "application/octet-stream"},
                {".dxr", "application/x-director"},
                {".eml", "message/rfc822"},
                {".emz", "application/octet-stream"},
                {".eot", "application/vnd.ms-fontobject"},
                {".eps", "application/postscript"},
                {".etl", "application/etl"},
                {".etx", "text/x-setext"},
                {".evy", "application/envoy"},
                {".exe", "application/octet-stream"},
                {".exe.config", "text/xml"},
                {".fdf", "application/vnd.fdf"},
                {".fif", "application/fractals"},
                {".filters", "application/xml"},
                {".fla", "application/octet-stream"},
                {".flac", "audio/flac"},
                {".flr", "x-world/x-vrml"},
                {".flv", "video/x-flv"},
                {".fsscript", "application/fsharp-script"},
                {".fsx", "application/fsharp-script"},
                {".generictest", "application/xml"},
                {".gif", "image/gif"},
                {".gpx", "application/gpx+xml"},
                {".group", "text/x-ms-group"},
                {".gsm", "audio/x-gsm"},
                {".gtar", "application/x-gtar"},
                {".gz", "application/x-gzip"},
                {".h", "text/plain"},
                {".hdf", "application/x-hdf"},
                {".hdml", "text/x-hdml"},
                {".hhc", "application/x-oleobject"},
                {".hhk", "application/octet-stream"},
                {".hhp", "application/octet-stream"},
                {".hlp", "application/winhlp"},
                {".hpp", "text/plain"},
                {".hqx", "application/mac-binhex40"},
                {".hta", "application/hta"},
                {".htc", "text/x-component"},
                {".htm", "text/html"},
                {".html", "text/html"},
                {".htt", "text/webviewhtml"},
                {".hxa", "application/xml"},
                {".hxc", "application/xml"},
                {".hxd", "application/octet-stream"},
                {".hxe", "application/xml"},
                {".hxf", "application/xml"},
                {".hxh", "application/octet-stream"},
                {".hxi", "application/octet-stream"},
                {".hxk", "application/xml"},
                {".hxq", "application/octet-stream"},
                {".hxr", "application/octet-stream"},
                {".hxs", "application/octet-stream"},
                {".hxt", "text/html"},
                {".hxv", "application/xml"},
                {".hxw", "application/octet-stream"},
                {".hxx", "text/plain"},
                {".i", "text/plain"},
                {".ico", "image/x-icon"},
                {".ics", "application/octet-stream"},
                {".idl", "text/plain"},
                {".ief", "image/ief"},
                {".iii", "application/x-iphone"},
                {".inc", "text/plain"},
                {".inf", "application/octet-stream"},
                {".ini", "text/plain"},
                {".inl", "text/plain"},
                {".ins", "application/x-internet-signup"},
                {".ipa", "application/x-itunes-ipa"},
                {".ipg", "application/x-itunes-ipg"},
                {".ipproj", "text/plain"},
                {".ipsw", "application/x-itunes-ipsw"},
                {".iqy", "text/x-ms-iqy"},
                {".isp", "application/x-internet-signup"},
                {".ite", "application/x-itunes-ite"},
                {".itlp", "application/x-itunes-itlp"},
                {".itms", "application/x-itunes-itms"},
                {".itpc", "application/x-itunes-itpc"},
                {".IVF", "video/x-ivf"},
                {".jar", "application/java-archive"},
                {".java", "application/octet-stream"},
                {".jck", "application/liquidmotion"},
                {".jcz", "application/liquidmotion"},
                {".jfif", "image/pjpeg"},
                {".jnlp", "application/x-java-jnlp-file"},
                {".jpb", "application/octet-stream"},
                {".jpe", "image/jpeg"},
                {".jpeg", "image/jpeg"},
                {".jpg", "image/jpeg"},
                {".js", "application/javascript"},
                {".json", "application/json"},
                {".jsx", "text/jscript"},
                {".jsxbin", "text/plain"},
                {".latex", "application/x-latex"},
                {".library-ms", "application/windows-library+xml"},
                {".lit", "application/x-ms-reader"},
                {".loadtest", "application/xml"},
                {".lpk", "application/octet-stream"},
                {".lsf", "video/x-la-asf"},
                {".lst", "text/plain"},
                {".lsx", "video/x-la-asf"},
                {".lzh", "application/octet-stream"},
                {".m13", "application/x-msmediaview"},
                {".m14", "application/x-msmediaview"},
                {".m1v", "video/mpeg"},
                {".m2t", "video/vnd.dlna.mpeg-tts"},
                {".m2ts", "video/vnd.dlna.mpeg-tts"},
                {".m2v", "video/mpeg"},
                {".m3u", "audio/x-mpegurl"},
                {".m3u8", "audio/x-mpegurl"},
                {".m4a", "audio/m4a"},
                {".m4b", "audio/m4b"},
                {".m4p", "audio/m4p"},
                {".m4r", "audio/x-m4r"},
                {".m4v", "video/x-m4v"},
                {".mac", "image/x-macpaint"},
                {".mak", "text/plain"},
                {".man", "application/x-troff-man"},
                {".manifest", "application/x-ms-manifest"},
                {".map", "text/plain"},
                {".master", "application/xml"},
                {".mda", "application/msaccess"},
                {".mdb", "application/x-msaccess"},
                {".mde", "application/msaccess"},
                {".mdp", "application/octet-stream"},
                {".me", "application/x-troff-me"},
                {".mfp", "application/x-shockwave-flash"},
                {".mht", "message/rfc822"},
                {".mhtml", "message/rfc822"},
                {".mid", "audio/mid"},
                {".midi", "audio/mid"},
                {".mix", "application/octet-stream"},
                {".mk", "text/plain"},
                {".mmf", "application/x-smaf"},
                {".mno", "text/xml"},
                {".mny", "application/x-msmoney"},
                {".mod", "video/mpeg"},
                {".mov", "video/quicktime"},
                {".movie", "video/x-sgi-movie"},
                {".mp2", "video/mpeg"},
                {".mp2v", "video/mpeg"},
                {".mp3", "audio/mpeg"},
                {".mp4", "video/mp4"},
                {".mp4v", "video/mp4"},
                {".mpa", "video/mpeg"},
                {".mpe", "video/mpeg"},
                {".mpeg", "video/mpeg"},
                {".mpf", "application/vnd.ms-mediapackage"},
                {".mpg", "video/mpeg"},
                {".mpp", "application/vnd.ms-project"},
                {".mpv2", "video/mpeg"},
                {".mqv", "video/quicktime"},
                {".ms", "application/x-troff-ms"},
                {".msi", "application/octet-stream"},
                {".mso", "application/octet-stream"},
                {".mts", "video/vnd.dlna.mpeg-tts"},
                {".mtx", "application/xml"},
                {".mvb", "application/x-msmediaview"},
                {".mvc", "application/x-miva-compiled"},
                {".mxp", "application/x-mmxp"},
                {".nc", "application/x-netcdf"},
                {".nsc", "video/x-ms-asf"},
                {".nws", "message/rfc822"},
                {".ocx", "application/octet-stream"},
                {".oda", "application/oda"},
                {".odb", "application/vnd.oasis.opendocument.database"},
                {".odc", "application/vnd.oasis.opendocument.chart"},
                {".odf", "application/vnd.oasis.opendocument.formula"},
                {".odg", "application/vnd.oasis.opendocument.graphics"},
                {".odh", "text/plain"},
                {".odi", "application/vnd.oasis.opendocument.image"},
                {".odl", "text/plain"},
                {".odm", "application/vnd.oasis.opendocument.text-master"},
                {".odp", "application/vnd.oasis.opendocument.presentation"},
                {".ods", "application/vnd.oasis.opendocument.spreadsheet"},
                {".odt", "application/vnd.oasis.opendocument.text"},
                {".oga", "audio/ogg"},
                {".ogg", "audio/ogg"},
                {".ogv", "video/ogg"},
                {".ogx", "application/ogg"},
                {".one", "application/onenote"},
                {".onea", "application/onenote"},
                {".onepkg", "application/onenote"},
                {".onetmp", "application/onenote"},
                {".onetoc", "application/onenote"},
                {".onetoc2", "application/onenote"},
                {".opus", "audio/ogg"},
                {".orderedtest", "application/xml"},
                {".osdx", "application/opensearchdescription+xml"},
                {".otf", "application/font-sfnt"},
                {".otg", "application/vnd.oasis.opendocument.graphics-template"},
                {".oth", "application/vnd.oasis.opendocument.text-web"},
                {".otp", "application/vnd.oasis.opendocument.presentation-template"},
                {".ots", "application/vnd.oasis.opendocument.spreadsheet-template"},
                {".ott", "application/vnd.oasis.opendocument.text-template"},
                {".oxt", "application/vnd.openofficeorg.extension"},
                {".p10", "application/pkcs10"},
                {".p12", "application/x-pkcs12"},
                {".p7b", "application/x-pkcs7-certificates"},
                {".p7c", "application/pkcs7-mime"},
                {".p7m", "application/pkcs7-mime"},
                {".p7r", "application/x-pkcs7-certreqresp"},
                {".p7s", "application/pkcs7-signature"},
                {".pbm", "image/x-portable-bitmap"},
                {".pcast", "application/x-podcast"},
                {".pct", "image/pict"},
                {".pcx", "application/octet-stream"},
                {".pcz", "application/octet-stream"},
                {".pdf", "application/pdf"},
                {".pfb", "application/octet-stream"},
                {".pfm", "application/octet-stream"},
                {".pfx", "application/x-pkcs12"},
                {".pgm", "image/x-portable-graymap"},
                {".pic", "image/pict"},
                {".pict", "image/pict"},
                {".pkgdef", "text/plain"},
                {".pkgundef", "text/plain"},
                {".pko", "application/vnd.ms-pki.pko"},
                {".pls", "audio/scpls"},
                {".pma", "application/x-perfmon"},
                {".pmc", "application/x-perfmon"},
                {".pml", "application/x-perfmon"},
                {".pmr", "application/x-perfmon"},
                {".pmw", "application/x-perfmon"},
                {".png", "image/png"},
                {".pnm", "image/x-portable-anymap"},
                {".pnt", "image/x-macpaint"},
                {".pntg", "image/x-macpaint"},
                {".pnz", "image/png"},
                {".pot", "application/vnd.ms-powerpoint"},
                {".potm", "application/vnd.ms-powerpoint.template.macroEnabled.12"},
                {".potx", "application/vnd.openxmlformats-officedocument.presentationml.template"},
                {".ppa", "application/vnd.ms-powerpoint"},
                {".ppam", "application/vnd.ms-powerpoint.addin.macroEnabled.12"},
                {".ppm", "image/x-portable-pixmap"},
                {".pps", "application/vnd.ms-powerpoint"},
                {".ppsm", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"},
                {".ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow"},
                {".ppt", "application/vnd.ms-powerpoint"},
                {".pptm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12"},
                {".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"},
                {".prf", "application/pics-rules"},
                {".prm", "application/octet-stream"},
                {".prx", "application/octet-stream"},
                {".ps", "application/postscript"},
                {".psc1", "application/PowerShell"},
                {".psd", "application/octet-stream"},
                {".psess", "application/xml"},
                {".psm", "application/octet-stream"},
                {".psp", "application/octet-stream"},
                {".pub", "application/x-mspublisher"},
                {".pwz", "application/vnd.ms-powerpoint"},
                {".qht", "text/x-html-insertion"},
                {".qhtm", "text/x-html-insertion"},
                {".qt", "video/quicktime"},
                {".qti", "image/x-quicktime"},
                {".qtif", "image/x-quicktime"},
                {".qtl", "application/x-quicktimeplayer"},
                {".qxd", "application/octet-stream"},
                {".ra", "audio/x-pn-realaudio"},
                {".ram", "audio/x-pn-realaudio"},
                {".rar", "application/x-rar-compressed"},
                {".ras", "image/x-cmu-raster"},
                {".rat", "application/rat-file"},
                {".rc", "text/plain"},
                {".rc2", "text/plain"},
                {".rct", "text/plain"},
                {".rdlc", "application/xml"},
                {".reg", "text/plain"},
                {".resx", "application/xml"},
                {".rf", "image/vnd.rn-realflash"},
                {".rgb", "image/x-rgb"},
                {".rgs", "text/plain"},
                {".rm", "application/vnd.rn-realmedia"},
                {".rmi", "audio/mid"},
                {".rmp", "application/vnd.rn-rn_music_package"},
                {".roff", "application/x-troff"},
                {".rpm", "audio/x-pn-realaudio-plugin"},
                {".rqy", "text/x-ms-rqy"},
                {".rtf", "application/rtf"},
                {".rtx", "text/richtext"},
                {".ruleset", "application/xml"},
                {".s", "text/plain"},
                {".safariextz", "application/x-safari-safariextz"},
                {".scd", "application/x-msschedule"},
                {".scr", "text/plain"},
                {".sct", "text/scriptlet"},
                {".sd2", "audio/x-sd2"},
                {".sdp", "application/sdp"},
                {".sea", "application/octet-stream"},
                {".searchConnector-ms", "application/windows-search-connector+xml"},
                {".setpay", "application/set-payment-initiation"},
                {".setreg", "application/set-registration-initiation"},
                {".settings", "application/xml"},
                {".sgimb", "application/x-sgimb"},
                {".sgml", "text/sgml"},
                {".sh", "application/x-sh"},
                {".shar", "application/x-shar"},
                {".shtml", "text/html"},
                {".sit", "application/x-stuffit"},
                {".sitemap", "application/xml"},
                {".skin", "application/xml"},
                {".sldm", "application/vnd.ms-powerpoint.slide.macroEnabled.12"},
                {".sldx", "application/vnd.openxmlformats-officedocument.presentationml.slide"},
                {".slk", "application/vnd.ms-excel"},
                {".sln", "text/plain"},
                {".slupkg-ms", "application/x-ms-license"},
                {".smd", "audio/x-smd"},
                {".smi", "application/octet-stream"},
                {".smx", "audio/x-smd"},
                {".smz", "audio/x-smd"},
                {".snd", "audio/basic"},
                {".snippet", "application/xml"},
                {".snp", "application/octet-stream"},
                {".sol", "text/plain"},
                {".sor", "text/plain"},
                {".spc", "application/x-pkcs7-certificates"},
                {".spl", "application/futuresplash"},
                {".spx", "audio/ogg"},
                {".src", "application/x-wais-source"},
                {".srf", "text/plain"},
                {".SSISDeploymentManifest", "text/xml"},
                {".ssm", "application/streamingmedia"},
                {".sst", "application/vnd.ms-pki.certstore"},
                {".stl", "application/vnd.ms-pki.stl"},
                {".sv4cpio", "application/x-sv4cpio"},
                {".sv4crc", "application/x-sv4crc"},
                {".svc", "application/xml"},
                {".svg", "image/svg+xml"},
                {".swf", "application/x-shockwave-flash"},
                {".step", "application/step"},
                {".stp", "application/step"},
                {".t", "application/x-troff"},
                {".tar", "application/x-tar"},
                {".tcl", "application/x-tcl"},
                {".testrunconfig", "application/xml"},
                {".testsettings", "application/xml"},
                {".tex", "application/x-tex"},
                {".texi", "application/x-texinfo"},
                {".texinfo", "application/x-texinfo"},
                {".tgz", "application/x-compressed"},
                {".thmx", "application/vnd.ms-officetheme"},
                {".thn", "application/octet-stream"},
                {".tif", "image/tiff"},
                {".tiff", "image/tiff"},
                {".tlh", "text/plain"},
                {".tli", "text/plain"},
                {".toc", "application/octet-stream"},
                {".tr", "application/x-troff"},
                {".trm", "application/x-msterminal"},
                {".trx", "application/xml"},
                {".ts", "video/vnd.dlna.mpeg-tts"},
                {".tsv", "text/tab-separated-values"},
                {".ttf", "application/font-sfnt"},
                {".tts", "video/vnd.dlna.mpeg-tts"},
                {".txt", "text/plain"},
                {".u32", "application/octet-stream"},
                {".uls", "text/iuls"},
                {".user", "text/plain"},
                {".ustar", "application/x-ustar"},
                {".vb", "text/plain"},
                {".vbdproj", "text/plain"},
                {".vbk", "video/mpeg"},
                {".vbproj", "text/plain"},
                {".vbs", "text/vbscript"},
                {".vcf", "text/x-vcard"},
                {".vcproj", "application/xml"},
                {".vcs", "text/plain"},
                {".vcxproj", "application/xml"},
                {".vddproj", "text/plain"},
                {".vdp", "text/plain"},
                {".vdproj", "text/plain"},
                {".vdx", "application/vnd.ms-visio.viewer"},
                {".vml", "text/xml"},
                {".vscontent", "application/xml"},
                {".vsct", "text/xml"},
                {".vsd", "application/vnd.visio"},
                {".vsi", "application/ms-vsi"},
                {".vsix", "application/vsix"},
                {".vsixlangpack", "text/xml"},
                {".vsixmanifest", "text/xml"},
                {".vsmdi", "application/xml"},
                {".vspscc", "text/plain"},
                {".vss", "application/vnd.visio"},
                {".vsscc", "text/plain"},
                {".vssettings", "text/xml"},
                {".vssscc", "text/plain"},
                {".vst", "application/vnd.visio"},
                {".vstemplate", "text/xml"},
                {".vsto", "application/x-ms-vsto"},
                {".vsw", "application/vnd.visio"},
                {".vsx", "application/vnd.visio"},
                {".vtx", "application/vnd.visio"},
                {".wav", "audio/wav"},
                {".wave", "audio/wav"},
                {".wax", "audio/x-ms-wax"},
                {".wbk", "application/msword"},
                {".wbmp", "image/vnd.wap.wbmp"},
                {".wcm", "application/vnd.ms-works"},
                {".wdb", "application/vnd.ms-works"},
                {".wdp", "image/vnd.ms-photo"},
                {".webarchive", "application/x-safari-webarchive"},
                {".webm", "video/webm"},
                {".webp", "image/webp"}, /* https://en.wikipedia.org/wiki/WebP */
                {".webtest", "application/xml"},
                {".wiq", "application/xml"},
                {".wiz", "application/msword"},
                {".wks", "application/vnd.ms-works"},
                {".WLMP", "application/wlmoviemaker"},
                {".wlpginstall", "application/x-wlpg-detect"},
                {".wlpginstall3", "application/x-wlpg3-detect"},
                {".wm", "video/x-ms-wm"},
                {".wma", "audio/x-ms-wma"},
                {".wmd", "application/x-ms-wmd"},
                {".wmf", "application/x-msmetafile"},
                {".wml", "text/vnd.wap.wml"},
                {".wmlc", "application/vnd.wap.wmlc"},
                {".wmls", "text/vnd.wap.wmlscript"},
                {".wmlsc", "application/vnd.wap.wmlscriptc"},
                {".wmp", "video/x-ms-wmp"},
                {".wmv", "video/x-ms-wmv"},
                {".wmx", "video/x-ms-wmx"},
                {".wmz", "application/x-ms-wmz"},
                {".woff", "application/font-woff"},
                {".wpl", "application/vnd.ms-wpl"},
                {".wps", "application/vnd.ms-works"},
                {".wri", "application/x-mswrite"},
                {".wrl", "x-world/x-vrml"},
                {".wrz", "x-world/x-vrml"},
                {".wsc", "text/scriptlet"},
                {".wsdl", "text/xml"},
                {".wvx", "video/x-ms-wvx"},
                {".x", "application/directx"},
                {".xaf", "x-world/x-vrml"},
                {".xaml", "application/xaml+xml"},
                {".xap", "application/x-silverlight-app"},
                {".xbap", "application/x-ms-xbap"},
                {".xbm", "image/x-xbitmap"},
                {".xdr", "text/plain"},
                {".xht", "application/xhtml+xml"},
                {".xhtml", "application/xhtml+xml"},
                {".xla", "application/vnd.ms-excel"},
                {".xlam", "application/vnd.ms-excel.addin.macroEnabled.12"},
                {".xlc", "application/vnd.ms-excel"},
                {".xld", "application/vnd.ms-excel"},
                {".xlk", "application/vnd.ms-excel"},
                {".xll", "application/vnd.ms-excel"},
                {".xlm", "application/vnd.ms-excel"},
                {".xls", "application/vnd.ms-excel"},
                {".xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12"},
                {".xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12"},
                {".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"},
                {".xlt", "application/vnd.ms-excel"},
                {".xltm", "application/vnd.ms-excel.template.macroEnabled.12"},
                {".xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template"},
                {".xlw", "application/vnd.ms-excel"},
                {".xml", "text/xml"},
                {".xmta", "application/xml"},
                {".xof", "x-world/x-vrml"},
                {".XOML", "text/plain"},
                {".xpm", "image/x-xpixmap"},
                {".xps", "application/vnd.ms-xpsdocument"},
                {".xrm-ms", "text/xml"},
                {".xsc", "application/xml"},
                {".xsd", "text/xml"},
                {".xsf", "text/xml"},
                {".xsl", "text/xml"},
                {".xslt", "text/xml"},
                {".xsn", "application/octet-stream"},
                {".xss", "application/xml"},
                {".xspf", "application/xspf+xml"},
                {".xtp", "application/octet-stream"},
                {".xwd", "image/x-xwindowdump"},
                {".z", "application/x-compress"},
                {".zip", "application/zip"},

                {"application/fsharp-script", ".fsx"},
                {"application/msaccess", ".adp"},
                {"application/msword", ".doc"},
                {"application/octet-stream", ".bin"},
                {"application/onenote", ".one"},
                {"application/postscript", ".eps"},
                {"application/step", ".step"},
                {"application/vnd.ms-excel", ".xls"},
                {"application/vnd.ms-powerpoint", ".ppt"},
                {"application/vnd.ms-works", ".wks"},
                {"application/vnd.visio", ".vsd"},
                {"application/x-director", ".dir"},
                {"application/x-shockwave-flash", ".swf"},
                {"application/x-x509-ca-cert", ".cer"},
                {"application/x-zip-compressed", ".zip"},
                {"application/xhtml+xml", ".xhtml"},
                {"application/xml", ".xml"},  // anomoly, .xml -> text/xml, but application/xml -> many thingss, but all are xml, so safest is .xml
                {"audio/aac", ".AAC"},
                {"audio/aiff", ".aiff"},
                {"audio/basic", ".snd"},
                {"audio/mid", ".midi"},
                {"audio/wav", ".wav"},
                {"audio/x-m4a", ".m4a"},
                {"audio/x-mpegurl", ".m3u"},
                {"audio/x-pn-realaudio", ".ra"},
                {"audio/x-smd", ".smd"},
                {"image/bmp", ".bmp"},
                {"image/jpeg", ".jpg"},
                {"image/pict", ".pic"},
                {"image/png", ".png"},
                {"image/tiff", ".tiff"},
                {"image/x-macpaint", ".mac"},
                {"image/x-quicktime", ".qti"},
                {"message/rfc822", ".eml"},
                {"text/html", ".html"},
                {"text/plain", ".txt"},
                {"text/scriptlet", ".wsc"},
                {"text/xml", ".xml"},
                {"video/3gpp", ".3gp"},
                {"video/3gpp2", ".3gp2"},
                {"video/mp4", ".mp4"},
                {"video/mpeg", ".mpg"},
                {"video/quicktime", ".mov"},
                {"video/vnd.dlna.mpeg-tts", ".m2t"},
                {"video/x-dv", ".dv"},
                {"video/x-la-asf", ".lsf"},
                {"video/x-ms-asf", ".asf"},
                {"x-world/x-vrml", ".xof"},
 
                #endregion
 
                };

            var cache = mappings.ToList(); // need ToList() to avoid modifying while still enumerating

            foreach (var mapping in cache)
            {
                if (!mappings.ContainsKey(mapping.Value))
                {
                    mappings.Add(mapping.Value, mapping.Key);
                }
            }

            return mappings;
        }

        public static string GetMimeType(string extension)
        {
            if (extension == null)
            {
                throw new ArgumentNullException("extension");
            }

            if (!extension.StartsWith("."))
            {
                extension = "." + extension;
            }

            string mime;

            return _mappings.Value.TryGetValue(extension, out mime) ? mime : "application/octet-stream";
        }
        private static readonly Lazy<IDictionary<string, string>> _mappings = new Lazy<IDictionary<string, string>>(BuildMappings);

        public string UploadStaffQualificationFile(string StaffId = "")
        {
            if (Request.Files.Count > 0)
            {
                string path = String.Empty;
                try
                {
                    int maxfilesize = 1000; // default size
                    var ProfilePicMaxSizeSetting = _ISchoolSettingService.GetorSetSchoolData("StaffDocumentMaxSize", "1000", null, "Size in kb").AttributeValue;
                    if (ProfilePicMaxSizeSetting != null && !string.IsNullOrEmpty(ProfilePicMaxSizeSetting))
                        maxfilesize = Convert.ToInt32(ProfilePicMaxSizeSetting);
                    string fileName = "";
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //file
                        HttpPostedFileBase file = files[i];
                        // get Filename

                        string uniqueno = Request.Form["uniqueno"];


                        //int StaffId = 0;
                        //// decrypt Id
                        //if (!string.IsNullOrEmpty(uniqueno) && uniqueno != "0")
                        //{
                        //    try
                        //    {
                        //        int.TryParse(_ICustomEncryption.base64d(uniqueno), out StaffId);
                        //    }
                        //    catch
                        //    {
                        //        StaffId = Convert.ToInt32(uniqueno);
                        //    }
                        //}

                        //check that the file is of the permitted file type
                        string fileExtension = Path.GetExtension(file.FileName).ToLower();
                        string mimetype = GetMimeType(fileExtension).ToLower();
                        string[] acceptedFileTypes = new string[4];
                        acceptedFileTypes[0] = "image/jpg";
                        acceptedFileTypes[1] = "image/jpeg";
                        acceptedFileTypes[2] = "image/png";
                        acceptedFileTypes[3] = "application/pdf";

                        string uniqueimage = Request.Form["uniqueimage"] != null ? Request.Form["uniqueimage"] : "";
                        var Isphoto = Convert.ToInt32(Request.Form["IsPhoto"]);

                        bool acceptFile = false;
                        //should we accept the file?
                        for (int j = 0; j < acceptedFileTypes.Count(); j++)
                        {
                            if (mimetype == acceptedFileTypes[j])
                            {
                                //accept the file, yay!
                                // Isphoto = 1;
                                acceptFile = true;
                                break;
                            }
                        }

                        //if (Isphoto == 1)
                        //{
                        //    fileName = StaffId.ToString() + "_image" + uniqueimage + Path.GetExtension(file.FileName).ToLower();
                        //}
                        //else
                        //{
                        fileName = StaffId.ToString() + "_doc" + Path.GetRandomFileName() + Path.GetExtension(file.FileName).ToLower();
                        //}

                        //Is the file too big to upload?
                        int fileSize = file.ContentLength;
                        if (fileSize > (maxfilesize * 1000))
                        {
                            //return Json(new
                            //{
                            //    status = "error",
                            //    msg = "Filesize of image is too large. Maximum file size permitted is " + maxfilesize + "KB"
                            //});
                        }


                        if (!acceptFile)
                            break;
                        //    return Json(new
                        //    {
                        //        status = "error",
                        //        msg = "The file you are trying to upload is not a permitted file type!"
                        //    });

                        // check directory exist or not
                        var schoolid = HttpContext.Session["SchoolId"];
                        var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Teacher/" + StaffId.ToString()));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Teacher/" + StaffId.ToString()), fileName);

                        // Get the complete folder path and store the file inside it.  
                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);

                        //if (Isphoto == 1)
                        //{
                        //    var resizeratio = _ISchoolSettingService.GetAttributeValue("StaffPhotoRatio");

                        //    var height = Convert.ToInt32(resizeratio.Split(',')[0]);
                        //    var width = Convert.ToInt32(resizeratio.Split(',')[1]);

                        //    var resizedimg = _ISchoolDataService.ResizeImage(file, "staffimage", height, width);
                        //    resizedimg.Save(path);
                        //}
                        //else
                        file.SaveAs(path);
                        return fileName;
                    }
                    return "";
                    // Returns message that successfully uploaded  
                    //return Json(new
                    //{
                    //    status = "success",
                    //    path = path,
                    //    msg = "File Uploaded Successfully!"
                    //});
                }
                catch (Exception ex)
                {
                    return "";
                    //return Json(new
                    //{
                    //    status = "error",
                    //    msg = "Error occurred. Error details: " + ex.Message
                    //});
                }
            }
            else
            {
                return "";
                //return Json(new
                //{
                //    status = "error",
                //    msg = "No files selected."
                //});
            }
        }
        public ActionResult ValidateStaffQualificationFile()
        {
            if (Request.Files.Count > 0)
            {
                string path = String.Empty;
                try
                {
                    int maxfilesize = 1000; // default size
                    var ProfilePicMaxSizeSetting = _ISchoolSettingService.GetorSetSchoolData("StaffDocumentMaxSize", "1000", null, "Size in kb").AttributeValue;
                    if (ProfilePicMaxSizeSetting != null && !string.IsNullOrEmpty(ProfilePicMaxSizeSetting))
                        maxfilesize = Convert.ToInt32(ProfilePicMaxSizeSetting);

                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //file
                        HttpPostedFileBase file = files[i];
                        // get Filename
                        string fileName;
                        string uniqueno = Request.Form["uniqueno"];

                        int StaffId = 0;
                        // decrypt Id
                        if (!string.IsNullOrEmpty(uniqueno) && uniqueno != "0")
                        {
                            try
                            {
                                int.TryParse(_ICustomEncryption.base64d(uniqueno), out StaffId);
                            }
                            catch
                            {
                                StaffId = Convert.ToInt32(uniqueno);
                            }
                        }

                        //check that the file is of the permitted file type
                        string fileExtension = Path.GetExtension(file.FileName).ToLower();
                        string[] acceptedFileTypes = new string[4];
                        acceptedFileTypes[0] = ".jpg";
                        acceptedFileTypes[1] = ".jpeg";
                        acceptedFileTypes[2] = ".png";
                        acceptedFileTypes[3] = ".pdf";

                        string uniqueimage = Request.Form["uniqueimage"] != null ? Request.Form["uniqueimage"] : "";
                        var Isphoto = Convert.ToInt32(Request.Form["IsPhoto"]);

                        bool acceptFile = false;
                        //should we accept the file?
                        for (int j = 0; j < acceptedFileTypes.Count(); j++)
                        {
                            if (fileExtension == acceptedFileTypes[j])
                            {
                                //accept the file, yay!
                                if (fileExtension != "pdf")
                                {
                                    Isphoto = 1;
                                }
                                acceptFile = true;
                                break;
                            }
                        }

                        if (Isphoto == 1)
                        {
                            fileName = StaffId.ToString() + "_image" + uniqueimage + Path.GetExtension(file.FileName).ToLower();
                        }
                        else
                        {
                            fileName = StaffId.ToString() + "_doc" + uniqueimage + Path.GetExtension(file.FileName).ToLower();
                        }

                        //Is the file too big to upload?
                        int fileSize = file.ContentLength;
                        if (fileSize > (maxfilesize * 1000))
                        {
                            return Json(new
                            {
                                status = "error",
                                msg = "Filesize of image is too large. Maximum file size permitted is " + maxfilesize + "KB"
                            });
                        }


                        if (!acceptFile)
                            return Json(new
                            {
                                status = "error",
                                msg = "The file you are trying to upload is not a permitted file type!"
                            });

                        // check directory exist or not
                        var schoolid = HttpContext.Session["SchoolId"];
                        var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Teacher/" + StaffId.ToString() + "/QualificationDocument"));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Teacher/" + StaffId.ToString() + "/QualificationDocument"), fileName);

                        // Get the complete folder path and store the file inside it.  
                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);

                        if (Isphoto == 1)
                        {
                            var resizeratio = _ISchoolSettingService.GetAttributeValue("StaffPhotoRatio");

                            var height = Convert.ToInt32(resizeratio.Split(',')[0]);
                            var width = Convert.ToInt32(resizeratio.Split(',')[1]);

                            var resizedimg = _ISchoolDataService.ResizeImage(file, "staffimage", height, width);
                            resizedimg.Save(path);
                        }
                        else
                            file.SaveAs(path);
                    }
                    // Returns message that successfully uploaded  
                    return Json(new
                    {
                        status = "success",
                        path = path,
                        //msg = "File Uploaded Successfully!"
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        status = "error",
                        msg = "Error occurred. Error details: " + ex.Message
                    });
                }
            }
            else
            {
                return Json(new
                {
                    status = "error",
                    msg = "No files selected."
                });
            }
        }

        //list of staff
        public ActionResult StaffList()
        {

            var aa = Request.QueryString["type"];
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffList", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                StaffListModel model = new StaffListModel();
                //model.AvailableStatus.Add(new SelectListItem{Selected=true,Text="All"},new SelectListItem{})
                model.AvailableStatus.Add(new SelectListItem { Selected = false, Text = "--All--", Value = "" });
                model.AvailableStatus.Add(new SelectListItem { Selected = false, Text = "Active", Value = "True" });
                model.AvailableStatus.Add(new SelectListItem { Selected = false, Text = "InActive", Value = "False" });
                model.FilterStatusId = "True";

                model.StaffTypeList = _IDropDownService.StaffTypeList();

                model.DepartmentList = _IDropDownService.DepartmentList();
                if (Request.QueryString["type"] != null)
                {
                    if (Request.QueryString["type"].ToString().ToLower() == "1")
                    {
                        model.IsTeacher = true;
                        var stafftype = _IStaffService.GetAllStaffTypes(ref count).Where(e => e.IsTeacher == true).ToList();
                        foreach (var item in stafftype)
                        {
                            model.StaffTypeId = item.StaffTypeId;
                        }
                    }
                    else
                    {
                        model.IsTeacher = false;
                        var stafftype = _IStaffService.GetAllStaffTypes(ref count).Where(e => e.IsTeacher == false).ToList();
                        foreach (var item in stafftype)
                        {
                            model.StaffTypeId = item.StaffTypeId;
                        }
                    }
                }

                model.DesignationList = _IDropDownService.DesignationList(Convert.ToInt32(model.StaffTypeId));
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffList", "Add Record");
                model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffList", "Edit Record");
                model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffList", "Delete Record");
                model.IsAuthToExport = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffList", "Export");

                //Fill StaffServiceType
                model.AvailableStaffServiceTypes.Add(new SelectListItem { Value = "", Selected = true, Text = "--Select--" });
                var AllStaffServiceType = _IStaffService.GetAllStaffServiceType(ref count, IsActive: true);
                foreach (var sst in AllStaffServiceType)
                    model.AvailableStaffServiceTypes.Add(new SelectListItem { Value = sst.StaffServiceTypeId.ToString(), Text = sst.StaffServiceType1 });

                //values for kendo paging
                model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
                model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");
                DataTable StaffList = _IStaffService.ExportStaffListWithFilter(ref count);
                string cols = "";
                foreach (var item in StaffList.Columns)
                {
                    cols += item.ToString() + ",";
                }
                TempData["columnname"] = cols.ToString().TrimEnd(',');
                //TempData["columnname"] = "Name" + "," + "Designation" + "," + "Department" + "," + "EmpCode" + "," + "IsActive";
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        public ActionResult Export()
        {
            return Json(new
            {
                status = "success",
                data = ""
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportData(string StaffTypeId, string DesignationId, string DepartmentId,string Empocode,string status,
            string searchBox, string IsExcel, string Unseletedcolumn, string SelectedColumn)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                string Subtitle = "Staff";

                DataTable dt = new DataTable();
                dt = GetAllKendoGridInformation(StaffTypeId, DesignationId, DepartmentId,Empocode,status.ToLower(),
                                    searchBox, IsExcel, Unseletedcolumn, SelectedColumn);
                if (dt.Rows.Count > 0)
                {
                    if (IsExcel == "1")
                    {
                        _IExportHelper.ExportToExcel(dt, Title: "StaffList", Subtitle: Subtitle,HeadingName:"List Of Staff");
                    }
                    else if (IsExcel == "2")
                    {
                        _IExportHelper.ExportToPrint(dt, Title: "", Subtitle: Subtitle);
                    }
                    else
                    {
                        _IExportHelper.ExportToPDF(dt, Title: "", Subtitle: Subtitle);
                    }
                }
                //else
                //{
                //    return Json(new
                //    {
                //        status = "Error",
                //        data = "There is No data Found so You Can,t Export"
                //    }, JsonRequestBehavior.AllowGet); 
                //}
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public DataTable GetAllKendoGridInformation(string StaffTypeId, string DesignationId,
            string DepartmentId, string Empocode, string status, string searchBox, string IsExcel, string Unseletedcolumn, string SelectedColumn)
        {
            DataTable dt1 = new DataTable();
            DataTable dt = new DataTable();
            try
            {
                int? stafftypeid = null;
                int? designationid = null;
                int? departmentid = null;
                if (StaffTypeId != null && StaffTypeId != "")
                    stafftypeid = Convert.ToInt32(StaffTypeId);
                if (DesignationId != null && DesignationId != "")
                    designationid = Convert.ToInt32(DesignationId);
                if (DepartmentId != null && DepartmentId != "")
                    departmentid = Convert.ToInt32(DepartmentId);
                DataTable StaffList = _IStaffService.ExportStaffListWithFilter(ref count, StaffTypeId: stafftypeid, DesignationId: designationid, DepartmentId: departmentid, SearchBox: searchBox,IsActive:status,EmpCode:Empocode);
                dt = StaffList;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["Status"].ToString() == "1")
                    {
                        dt.Rows[i]["Status"] = "Active";
                    }
                    else
                    {
                        dt.Rows[i]["Status"] = "InActive";
                    }
                }
                if (SelectedColumn != null && SelectedColumn != "")
                {
                    var seletedcolumn = SelectedColumn.TrimEnd(',');
                    var SplitedselectedColumns = seletedcolumn.Split(',');
                    //foreach (var item in SplitedselectedColumns)
                    //{
                    //    dt1.Columns.Add(item);
                    //}
                    int columnIndex = 0;
                    foreach (var columnName in SplitedselectedColumns)
                    {
                        dt.Columns[columnName].SetOrdinal(columnIndex);
                        columnIndex++;
                    }
                }
                if (Unseletedcolumn != null && Unseletedcolumn != "")
                {
                    var RemovableColumn = Unseletedcolumn.TrimEnd(',');
                    var SplitedColumns = RemovableColumn.Split(',');
                    foreach (var item in SplitedColumns)
                    {
                        dt.Columns.Remove(item);
                    }
                }

                dt1 = dt;

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
            }
            return dt1;
        }

        public ActionResult DeleteStaff(string Id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "ManageStaff", "Delete");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            int staff_id = 0;
            if (!string.IsNullOrEmpty(Id))
            {
                // decrypt Id
                var qsid = _ICustomEncryption.base64d(Id);

                if (int.TryParse(qsid, out staff_id))
                {


                    if (int.TryParse(qsid, out staff_id))
                    {

                    }
                }

                var logtypeupdate = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
                // TimeTable log Table
                var Stafftable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "Staff").FirstOrDefault();
                var Addresstable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "Address").FirstOrDefault();
                var StaffDocumentstable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StaffDocuments").FirstOrDefault();
                var ContactInfotable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ContactInfo").FirstOrDefault();
                var ClassTeachertable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassTeacher").FirstOrDefault();

                var staff = _IStaffService.GetStaffById(StaffId: staff_id, IsTrack: true);
                var Stfcontacttype = _IAddressMasterService.GetAllContactTypes(ref count, _IStaffService.GetStaffTypeById((int)staff.StaffTypeId).StaffType1).FirstOrDefault(); // staff contact type
                if (staff != null && Stfcontacttype != null)
                {
                    if (staff.TeacherClasses.Count == 0 && staff.TeacherSubjects.Count == 0 && staff.ClassPeriodDetails.Count == 0)
                    {
                        var allAddresses = _IAddressService.GetAllAddresss(ref count, contactId: staff_id, contacttypeId: Stfcontacttype.ContactTypeId).ToList();
                        if (allAddresses.Count > 0)
                        {
                            foreach (var item in allAddresses)
                            {
                                _IAddressService.DeleteAddress(item.AddressId);
                                if (Addresstable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, Addresstable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(item.AddressId, Addresstable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete Address with AddressId:{0} and AddressTypeId:{1}", item.AddressId, item.AddressTypeId), Request.Browser.Browser);
                                }
                            }
                        }
                        var allDocuments = _IStaffService.GetAllStaffDocuments(ref count, StaffId: staff_id).ToList();
                        var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                        var staffdoc = "";
                        if (allDocuments.Count > 0)
                        {
                            foreach (var item in allDocuments)
                            {
                                _IStaffService.DeleteStaffDocument(item.DocumentId);
                                if (item.StaffDocumentType != null)
                                {
                                    if (item.StaffDocumentType.DocumentType == "Image")
                                    {
                                        staffdoc = Path.Combine(Server.MapPath("~/Images/" + SchoolDbId + "/Teacher/Photos/" + item.Image));
                                        if ((System.IO.File.Exists(staffdoc)))
                                            System.IO.File.Delete(staffdoc);
                                    }
                                    else
                                    {
                                        staffdoc = Path.Combine(Server.MapPath("~/Images/" + SchoolDbId + "/Teacher/" + item.StaffId + "/" + item.Image));
                                        if ((System.IO.File.Exists(staffdoc)))
                                            System.IO.File.Delete(staffdoc);
                                    }
                                    if (StaffDocumentstable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, StaffDocumentstable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(item.DocumentId, StaffDocumentstable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete StaffDocument with DocumentId:{0} and DocumentTypeId:{1}", item.DocumentId, item.DocumentTypeId), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        var allContacts = _IContactService.GetAllContactInfos(ref count, ContactId: staff_id);
                        if (allContacts.Count > 0)
                        {
                            foreach (var item in allContacts)
                            {
                                _IContactService.DeleteContactInfo(item.ContactInfoId);
                                if (ContactInfotable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ContactInfotable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog((int)item.ContactId, ContactInfotable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete ContactInfo with DocumentId:{0} and ContactInfoTypeId:{1}", item.ContactId, item.ContactInfoTypeId), Request.Browser.Browser);
                                }
                            }
                        }
                        var statushistory = _IStaffService.GetAllStaffStatusDetail(ref count).Where(m => m.StaffId == staff_id).OrderByDescending(m => m.StatusChangeDate).ToList();
                        foreach (var history in statushistory)
                        {
                            _IStaffService.DeleteStaffStatus(history.StaffStatusDetailId);
                        }
                        _IStaffService.DeleteStaff(StaffId: staff_id);

                        if (Stafftable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ContactInfotable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog((int)staff.StaffId, Stafftable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete Staff with StaffId:{0} and StaffTypeId:{1}", staff.StaffId, staff.StaffTypeId), Request.Browser.Browser);
                        }
                        var classTeacher = _IStaffService.GetAllClassTeachers(ref count, TeacherId: staff_id);
                        if (classTeacher.Count > 0)
                        {
                            foreach (var item in classTeacher)
                            {
                                _IStaffService.DeleteClassTeacher(item.ClassTeacherId);
                                if (ClassTeachertable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassTeachertable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(item.ClassTeacherId, ClassTeachertable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete ClassTecher with ClassTeacherId:{0}", item.ClassTeacherId), Request.Browser.Browser);
                                }
                            }
                        }
                    }
                }
            }
            return Json(new
            {
                status = "success",
                msg = "Deleted Sucessfully"
            });
        }

        public ActionResult StaffProfile()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffProfile", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {

                // decrypt Id
                var userdetail = _IUserService.GetUserById(user.UserId);
                int staffid = 0;
                if (userdetail != null)
                    staffid = (int)userdetail.UserContactId;

                var model = new StaffProfileModel();

                var Staff = _IStaffService.GetStaffById(staffid, true);
                // staff conatct type
                ContactType Stfcontacttype = null;
                if (Staff != null)
                {
                    var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);

                    Stfcontacttype = _IAddressMasterService.GetAllContactTypes(ref count, _IStaffService.GetStaffTypeById((int)Staff.StaffTypeId).StaffType1).SingleOrDefault(); // staff contact type

                    #region staffInfo
                    model.StaffName = Staff.FName;
                    if (!String.IsNullOrEmpty(Staff.LName))
                        model.StaffName = Staff.FName + " " + Staff.LName;
                    model.FirstName = Staff.FName;
                    model.LastName = Staff.LName;

                    //public string MobileNo { get; set; }
                    //public string Email { get; set; }
                    model.EmpCode = Staff.EmpCode;
                    var staffcontacttype = _IAddressMasterService.GetAllContactTypes(ref count, Staff.StaffType.StaffType1).FirstOrDefault();
                    var contactinfotypemobile = _IContactService.GetAllContactInfoTypes(ref count, ContactInfoType: "Mobile").FirstOrDefault();
                    var contactinfotypeemail = _IContactService.GetAllContactInfoTypes(ref count, ContactInfoType: "Email").FirstOrDefault();
                    var contactInfolistmobile = _IContactService.GetAllContactInfos(ref count, ContactInfoTypeId: contactinfotypemobile.ContactInfoTypeId, ContactTypeId: staffcontacttype.ContactTypeId, ContactId: Staff.StaffId, Isdefault: true).ToList();
                    var contactInfolistemail = _IContactService.GetAllContactInfos(ref count, ContactInfoTypeId: contactinfotypeemail.ContactInfoTypeId, ContactTypeId: staffcontacttype.ContactTypeId, ContactId: Staff.StaffId, Isdefault: true).ToList();
                    if (contactInfolistmobile.Count > 0)
                        foreach (var contactinfo in contactInfolistmobile)
                            model.MobileNo = contactinfo.ContactInfo1;

                    if (contactInfolistemail.Count > 0)
                        foreach (var contactinfo in contactInfolistemail)
                            model.Email = contactinfo.ContactInfo1;

                    model.Department = Staff.Department.Department1;
                    model.StaffType = Staff.StaffType.StaffType1;
                    model.CurrentStatus = (bool)Staff.IsActive;
                    model.Designation = Staff.Designation.Designation1;
                    model.EmployeeCode = Staff.EmpCode;
                    model.AdhaarNo = Staff.AadharCardNo;
                    var genderdetail = _ICatalogMasterService.GetGenderById((int)Staff.GenderId);
                    model.Gender = "";
                    if (genderdetail != null)
                        model.Gender = genderdetail.Gender1;
                    model.StaffCategory = "";
                    if (Staff.StaffCategory != null)
                        model.StaffCategory = Staff.StaffCategory.CategoryName;
                    model.IsActive = (bool)Staff.IsActive;

                    var doctypeid = _IStaffService.GetAllStaffDocumentTypes(ref count, "Image").FirstOrDefault();
                    var image = _IStaffService.GetAllStaffDocuments(ref count,
                        doctypeid.DocumentTypeId,
                        StaffId: Staff.StaffId
                        ).FirstOrDefault();
                    //fetch image
                    if (image != null)
                        model.StaffImage = _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/Teacher/Photos/" + image.Image;

                    #endregion


                    #region Address
                    AddressModel Staffaddress = new AddressModel();
                    var stdaddress = _IAddressService.GetAllAddresss(ref count, contactId: Staff.StaffId, contacttypeId: Stfcontacttype.ContactTypeId).ToList();
                    if (stdaddress != null)
                        foreach (var sa in stdaddress)
                        {
                            Staffaddress = new AddressModel();
                            Staffaddress.EncAddressId = _ICustomEncryption.base64e(sa.AddressId.ToString());
                            Staffaddress.AddressTypeId = (int)sa.AddressTypeId;
                            Staffaddress.AddressType = sa.AddressType.AddressType1;
                            Staffaddress.PlotNo = sa.PlotNo;
                            Staffaddress.Sector_Street = sa.Sector_Street;
                            Staffaddress.Locality = sa.Locality;
                            Staffaddress.Landmark = sa.Landmark;
                            Staffaddress.AddressDistrict = sa.District;
                            Staffaddress.Address_CityId = (int)sa.CityId;
                            Staffaddress.AddressCity = sa.AddressCity.City;
                            Staffaddress.Zip = sa.Zip;
                            model.AddressModelList.Add(Staffaddress);
                        }

                    #endregion

                    #region documents

                    var doctypeidimg = _IStaffService.GetAllStaffDocumentTypes(ref count, "Image").FirstOrDefault();
                    var documnts = _IStaffService.GetAllStaffDocuments(ref count, StaffId: Staff.StaffId).ToList();
                    foreach (var doc in documnts)
                    {
                        if (doc.DocumentTypeId == doctypeidimg.DocumentTypeId)
                            continue;

                        StaffDocuments document = new StaffDocuments();

                        document.DocumentType = doc.StaffDocumentType.DocumentType;
                        document.DocumentDescription = doc.Description;
                        document.DocumentImagePath = _WebHelper.GetStoreLocation() + "/Images/" + SchoolDbId + "/Teacher/" + Staff.StaffId + "/" + doc.Image;
                        model.StaffDocumentList.Add(document);
                    }
                    #endregion

                    #region contacts

                    if (staffcontacttype != null)
                    {
                        var contactInfo = _IContactService.GetAllContactInfos(ref count, ContactId: staffid, ContactTypeId: staffcontacttype.ContactTypeId).ToList();
                        if (contactInfo.Count > 0)
                        {
                            ContactInfos contact = new ContactInfos();
                            foreach (var item in contactInfo)
                            {
                                contact = new ContactInfos();
                                contact.ContactType = item.ContactInfoType.ContactInfoType1;
                                var status = item.Status == true ? "Active" : "In-Active";
                                var IsDefault = item.IsDefault == true ? "Active" : "In-Active";
                                contact.ContactStatus = status;
                                contact.ContactInfo1 = item.ContactInfo1;
                                contact.IsDefault = IsDefault;
                                // var contacttype = _IContactService.GetContactInfoTypeById((int)item.ContactInfoTypeId).ContactInfoType1;
                                model.ContactInfoList.Add(contact);
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    TempData["ErrorNotification"] = "Employee not found";
                    return RedirectToAction("ManageStaff", "Staff");
                }

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult UpdateGetStaffStatusHistory(string StaffId, string Status, string refSave, string DOL)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                // decrypt values
                int StaffIdK = 0;
                if (!string.IsNullOrEmpty(StaffId))
                    int.TryParse(_ICustomEncryption.base64d(StaffId), out StaffIdK);

                bool IsActiveStatus = false;
                bool? isActive = false;
                var MinStatusDate = DateTime.Now.Date.ToString();
                if (StaffIdK > 0)
                {
                    var staffinfo = _IStaffService.GetStaffById(StaffIdK);
                    isActive = staffinfo.IsActive;
                    if (staffinfo.DOJ != null)
                        MinStatusDate = ConvertDate((DateTime)staffinfo.DOJ);

                    if (refSave == "Save")
                    {
                        var getStaff = _IStaffService.GetStaffById(StaffIdK);
                        var statusDate = DateTime.Now;
                        if (getStaff != null)
                        {
                            IsActiveStatus = Status == "Active" ? true : false;
                            if (DOL != null && DOL != "")
                            {
                                statusDate = Convert.ToDateTime(DOL);
                            }
                            int counter = 0;
                            var StaffStatusDetail = new StaffStatusDetail();
                            StaffStatusDetail.StaffId = StaffIdK;
                            StaffStatusDetail.StatusChangeDate = statusDate;
                            StaffStatusDetail.StaffStatus = IsActiveStatus;
                            _IStaffService.InsertStaffStatus(StaffStatusDetail);
                            //if (getStaff.IsActive == true)
                            // {
                            //if (getStaff.ClassPeriodDetails.Count == 0)
                            //{
                            //if (getStaff.ClassTeachers.Count > 0)
                            //{
                            //foreach (var item in getStaff.ClassTeachers)
                            //{
                            //    if (item.EndDate <= DateTime.Now)
                            //        counter += counter + 1;
                            //}
                            //if (counter == getStaff.ClassTeachers.Count)
                            // getStaff.IsActive = IsActiveStatus;
                            //else
                            //{
                            //    return Json(new
                            //    {
                            //        status = "failed",
                            //        data = "Classes and Class periods are assigned to this Staff. You cannot change the status ε"
                            //    }, JsonRequestBehavior.AllowGet);
                            //}
                            //}
                            //else
                            //getStaff.IsActive = IsActiveStatus;
                            //}
                            //else
                            //{
                            //    return Json(new
                            //    {
                            //        status = "failed",
                            //        data = "Classes and Class periods are assigned to this Staff. You cannot change the status ε"
                            //    }, JsonRequestBehavior.AllowGet);
                            //}
                            //}
                            //else
                            getStaff.IsActive = IsActiveStatus;
                            if (IsActiveStatus == false)
                            {
                                getStaff.DOL = Convert.ToDateTime(DOL);
                            }
                            else
                            {
                                getStaff.DOL = null;
                            }
                            _IStaffService.UpdateStaff(getStaff);
                            if (IsActiveStatus == false)
                            {
                                var StaffChildList = _IStudentService.GetAllStudents(ref count).Where(m => m.ParentStaffId == getStaff.StaffId).ToList();
                                foreach (var studnt in StaffChildList)
                                {
                                    studnt.IsStaffChild = false;
                                    _IStudentService.UpdateStudent(studnt);
                                }
                            }
                            if (IsActiveStatus == true)
                            {
                                var StaffChildList = _IStudentService.GetAllStudents(ref count).Where(m => m.ParentStaffId == getStaff.StaffId).ToList();
                                foreach (var studnt in StaffChildList)
                                {
                                    studnt.IsStaffChild = true;
                                    _IStudentService.UpdateStudent(studnt);
                                }
                            }

                        }
                    }
                    if (refSave == "Save")
                    {
                        //check ths user logg in process
                        KSModel.Models.UserLoginCheck LoginCheckData = new KSModel.Models.UserLoginCheck();
                        var getLoginCheck = _IUserService.GetAllUserLoginChecks(ref count);
                        var currentStatus = IsActiveStatus;
                        var getUserType = _IAddressMasterService.GetAllContactTypes(ref count);
                        var getStatus = _IStaffService.GetStaffById(StaffId: StaffIdK);

                        if (IsActiveStatus != getStatus.IsActive)
                        {
                            if (getUserType.Count() > 0)
                            {
                                var getstaffInfo = _IUserService.GetAllUsers(ref count);
                                foreach (var type in getUserType)
                                {
                                    switch (type.ContactType1.ToLower())
                                    {
                                        case "teacher":
                                            #region teacher
                                            var getInfo = getstaffInfo.Where(x => x.UserContactId == StaffIdK && x.UserTypeId == type.ContactTypeId).FirstOrDefault();
                                            if (getInfo != null)
                                            {
                                                var isUserLoginCheck = getLoginCheck.Where(x => x.UserId == (int)getInfo.UserId).FirstOrDefault();
                                                if (isUserLoginCheck != null)
                                                {

                                                    if (getStatus != null)
                                                    {
                                                        if ((bool)currentStatus)
                                                        {
                                                            //update 
                                                            LoginCheckData = _IUserService.GetUserLoginCheckById(isUserLoginCheck.UserLoginCheckId, true);
                                                            if (LoginCheckData != null)
                                                            {
                                                                LoginCheckData.Datefrom = DateTime.UtcNow.Date;
                                                                LoginCheckData.DateTill = DateTime.UtcNow.Date.AddDays(7);

                                                                _IUserService.UpdateUserLoginCheck(LoginCheckData);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //update 
                                                            LoginCheckData = _IUserService.GetUserLoginCheckById(isUserLoginCheck.UserLoginCheckId, true);
                                                            if (LoginCheckData != null)
                                                            {
                                                                LoginCheckData.DateTill = DateTime.UtcNow.Date;
                                                                _IUserService.UpdateUserLoginCheck(LoginCheckData);
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                            #endregion
                                            break;
                                        case "non-teaching":
                                            #region non-teaching
                                            var getNonTeachInfo = getstaffInfo.Where(x => x.UserContactId == StaffIdK && x.UserTypeId == type.ContactTypeId).FirstOrDefault();
                                            if (getNonTeachInfo != null)
                                            {
                                                var isUserLoginCheck = getLoginCheck.Where(x => x.UserId == (int)getNonTeachInfo.UserId).FirstOrDefault();
                                                if (isUserLoginCheck != null)
                                                {
                                                    if (getStatus != null)
                                                    {
                                                        if ((bool)currentStatus)
                                                        {
                                                            //update 
                                                            LoginCheckData = _IUserService.GetUserLoginCheckById(isUserLoginCheck.UserLoginCheckId, true);
                                                            if (LoginCheckData != null)
                                                            {
                                                                LoginCheckData.Datefrom = DateTime.UtcNow.Date;
                                                                LoginCheckData.DateTill = DateTime.UtcNow.Date.AddDays(7);

                                                                _IUserService.UpdateUserLoginCheck(LoginCheckData);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            //update 
                                                            LoginCheckData = _IUserService.GetUserLoginCheckById(isUserLoginCheck.UserLoginCheckId, true);
                                                            if (LoginCheckData != null)
                                                            {
                                                                LoginCheckData.DateTill = DateTime.UtcNow.Date;

                                                                _IUserService.UpdateUserLoginCheck(LoginCheckData);
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                            #endregion
                                            break;
                                    }
                                }
                            }
                        }

                    }
                }
                var statushistory = _IStaffService.GetAllStaffStatusDetail(ref count).Where(m => m.StaffId == StaffIdK).OrderByDescending(m => m.StatusChangeDate).ToList();
                IList<StaffModel> StaffStatusHistory = new List<StaffModel>();
                foreach (var history in statushistory)
                {
                    var StaffModel = new StaffModel();
                    if (history.StaffStatus == true)
                        StaffModel.StaffStatus = "Joined";
                    if (history.StaffStatus == false)
                        StaffModel.StaffStatus = "Left";

                    StaffModel.StatusDate = ConvertDate((DateTime)history.StatusChangeDate);
                    StaffStatusHistory.Add(StaffModel);
                }

                if (statushistory.Count > 0)
                    MinStatusDate = ConvertDate((DateTime)(statushistory.FirstOrDefault().StatusChangeDate));


                return Json(new { IsActiveStatus, MinStatusDate, isActive, status = "success", list = StaffStatusHistory }, JsonRequestBehavior.AllowGet);
                /*,List=statushistory */
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }


        //save the Qualification
        [HttpPost]
        public ActionResult StaffQualification(StaffModel model, FormCollection form, HttpPostedFileBase file)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }


            // check id exist
            if (!string.IsNullOrEmpty(model.EncStaffId))
            {
                // decrypt Id
                var qsid = _ICustomEncryption.base64d(model.EncStaffId);
                int staff_id = 0;
                if (int.TryParse(qsid, out staff_id))
                {
                    model.StaffId = staff_id;
                }
            }

            try
            {
                var image = "";
                DateTime? nullableDateTime = null;
                if (Request.Files.Count > 0)
                {
                    image = UploadStaffQualificationFile(Convert.ToString(model.StaffId));
                }
                if (model.StaffQualification.StaffQualificationId > 0)
                {
                    var staffQualification = _IStaffService.GetStaffQualificationById(model.StaffQualification.StaffQualificationId);
                    staffQualification.ExamPassed = model.StaffQualification.ExamPassed;
                    staffQualification.BoardUniversity = model.StaffQualification.BoardUniversity;
                    staffQualification.InstitutionName = model.StaffQualification.InstitutionName;
                    staffQualification.ObtainedMarks = model.StaffQualification.ObtainedMarks;
                    if (model.StaffQualification.PassingYear != "" && model.StaffQualification.PassingYear != null)
                        staffQualification.PassingYear = (FormatDate(model.StaffQualification.PassingYear));
                    staffQualification.TotalMarks = model.StaffQualification.TotalMarks;
                    if (image != "")
                        staffQualification.QualificationImage = image;
                    _IStaffService.UpdateStaffQualification(staffQualification);
                    TempData["Successmsg"] = "Staff Qualification updated Successfully";
                    return RedirectToAction("Edit", new { Id = model.EncStaffId });
                }
                else
                {
                    KSModel.Models.StaffQualification staffQualification = new KSModel.Models.StaffQualification();
                    staffQualification.ExamPassed = model.StaffQualification.ExamPassed;
                    staffQualification.BoardUniversity = model.StaffQualification.BoardUniversity;
                    staffQualification.InstitutionName = model.StaffQualification.InstitutionName;
                    staffQualification.ObtainedMarks = model.StaffQualification.ObtainedMarks;
                    if (model.StaffQualification.PassingYear != "" && model.StaffQualification.PassingYear != null)
                        staffQualification.PassingYear = FormatDate(model.StaffQualification.PassingYear);
                    staffQualification.TotalMarks = model.StaffQualification.TotalMarks;
                    staffQualification.StaffId = (int)model.StaffId;
                    if (image != "")
                        staffQualification.QualificationImage = image;
                    _IStaffService.InsertStaffQualification(staffQualification);
                    TempData["Successmsg"] = "Staff Qualification Saved Successfully";
                    return RedirectToAction("Edit", new { Id = model.EncStaffId });
                }


                return RedirectToAction("Edit", new { Id = model.EncStaffId });
            }
            catch (Exception ex)
            {
                TempData["Errormsg"] = ex.Message;
                return RedirectToAction("Edit", new { Id = model.EncStaffId });
            }


            return RedirectToAction("Edit", new { Id = model.EncStaffId });
        }

        //save the Qualification
        [HttpPost]
        public ActionResult StaffExperience(StaffModel model, FormCollection form)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                // check id exist
                if (!string.IsNullOrEmpty(model.EncStaffId))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(model.EncStaffId);
                    int staff_id = 0;
                    if (int.TryParse(qsid, out staff_id))
                    {
                        model.StaffId = staff_id;
                    }
                }
                DateTime? nullableDateTime = null;
                var image = "";
                if (Request.Files.Count > 0)
                {
                    image = UploadStaffQualificationFile(Convert.ToString(model.StaffId));
                }
                var StaffexperienceListOverall =_IStaffService.GetAllStaffExperiences(ref count).Where(m=>m.StaffId== model.StaffId);
                //validate
                if (model.StaffExperience.JoiningDate!="")
                {
                    var date = FormatDate(model.StaffExperience.JoiningDate);
                    if (model.StaffExperience.StaffExperienceId > 0)
                    {
                        var StaffExperienceList = StaffexperienceListOverall.Where(m => m.StaffExperienceId != model.StaffExperience.StaffExperienceId && m.StaffId== model.StaffId);
                        if(StaffExperienceList.Where(m=>m.JoiningDate<= date && (m.LeavingDate<= date|| m.LeavingDate==null)).Count() > 0)
                        {
                            TempData["Errormsge"] = "Experience for selected dates already exists";
                            return RedirectToAction("Edit", new { Id = model.EncStaffId });
                        }
                    }
                    else
                    {
                        var StaffExperienceList = StaffexperienceListOverall;
                        if (StaffExperienceList.Where(m => m.JoiningDate <= date && (m.LeavingDate <= date || m.LeavingDate == null)).Count() > 0)
                        {
                            TempData["Errormsge"] = "Experience for selected dates already exists";
                            return RedirectToAction("Edit", new { Id = model.EncStaffId });
                        }
                    }
                }
                if (model.StaffExperience.LeavingDate != "")
                {
                    var date = FormatDate(model.StaffExperience.LeavingDate);
                    if (model.StaffExperience.StaffExperienceId > 0)
                    {
                        var StaffExperienceList = StaffexperienceListOverall.Where(m => m.StaffExperienceId != model.StaffExperience.StaffExperienceId);
                        if (StaffExperienceList.Where(m => m.LeavingDate <= date && (m.LeavingDate <= date || m.LeavingDate == null)).Count() > 0)
                        {
                            TempData["Errormsge"] = "Experience for selected dates already exists";
                            return RedirectToAction("Edit", new { Id = model.EncStaffId });
                        }
                    }
                    else
                    {
                        var StaffExperienceList = StaffexperienceListOverall;
                        if (StaffExperienceList.Where(m => m.LeavingDate <= date && (m.LeavingDate <= date || m.LeavingDate == null)).Count() > 0)
                        {
                            TempData["Errormsge"] = "Experience for selected dates already exists";
                            return RedirectToAction("Edit", new { Id = model.EncStaffId });
                        }
                    }
                }
                if (model.StaffExperience.StaffExperienceId > 0)
                {
                    var StaffExperience = _IStaffService.GetStaffExperienceById(model.StaffExperience.StaffExperienceId,true);
                    StaffExperience.City = model.StaffExperience.City;
                    StaffExperience.Designation = model.StaffExperience.Designation;

                    if (model.StaffExperience.JoiningDate != "" && model.StaffExperience.JoiningDate!=null)
                        StaffExperience.JoiningDate = FormatDate(model.StaffExperience.JoiningDate);
                    if (model.StaffExperience.LeavingDate != "" && model.StaffExperience.LeavingDate!=null)
                        StaffExperience.LeavingDate = FormatDate(model.StaffExperience.LeavingDate);
                    StaffExperience.Organsation = model.StaffExperience.Organsation;
                    if (image != "")
                        StaffExperience.ExperienceImage = image;
                    else
                    {
                        StaffExperience.ExperienceImage = StaffExperience.ExperienceImage;
                    }
                    _IStaffService.UpdateStaffExperience(StaffExperience);
                    TempData["Successmsge"] = "Staff Experience Updated Successfully";
                    return RedirectToAction("Edit", new { Id = model.EncStaffId });
                }
                else
                {
                    KSModel.Models.StaffExperience StaffExperience = new KSModel.Models.StaffExperience();
                    StaffExperience.City = model.StaffExperience.City;
                    StaffExperience.Designation = model.StaffExperience.Designation;
                    if (model.StaffExperience.JoiningDate != "" && model.StaffExperience.JoiningDate != null)
                        StaffExperience.JoiningDate = FormatDate(model.StaffExperience.JoiningDate);
                    if (model.StaffExperience.LeavingDate != "" && model.StaffExperience.LeavingDate != null)
                        StaffExperience.LeavingDate = FormatDate(model.StaffExperience.LeavingDate);

                    StaffExperience.Organsation = model.StaffExperience.Organsation;
                    StaffExperience.StaffId = (int)model.StaffId;
                    StaffExperience.ExperienceImage = image;
                    _IStaffService.InsertStaffExperience(StaffExperience);
                    TempData["Successmsge"] = "Staff Experience Saved Successfully";
                    return RedirectToAction("Edit", new { Id = model.EncStaffId });
                }
            }
            catch (Exception ex){

                TempData["Errormsge"] = ex.Message;
                return RedirectToAction("Edit", new { Id = model.EncStaffId });
            }
            return RedirectToAction("Edit", new { Id = model.EncStaffId });
        }

        //save the BankDetails
        [HttpPost]
        public ActionResult StaffBankDetails(StaffModel model, FormCollection form)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                // check id exist
                if (!string.IsNullOrEmpty(model.EncStaffId))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(model.EncStaffId);
                    int staff_id = 0;
                    if (int.TryParse(qsid, out staff_id))
                    {
                        model.StaffId = staff_id;
                    }
                }
                var bnkmodel = model.StaffBankDetails;
                if (bnkmodel.StaffBankDetailId > 0 && model.StaffId > 0)
                {
                    var StaffBankDetail = _IStaffService.GetStaffBankDetailById(bnkmodel.StaffBankDetailId);
                    if (StaffBankDetail != null)
                    {
                        StaffBankDetail.AccountNo = bnkmodel.AccountNo;
                        StaffBankDetail.BankName = bnkmodel.BankName;
                        StaffBankDetail.Branch = bnkmodel.Branch;
                        StaffBankDetail.BranchCode = bnkmodel.BranchCode;
                        StaffBankDetail.IFSCCode = bnkmodel.IFSCCode;
                        StaffBankDetail.MICR = bnkmodel.MICR;

                        if (!string.IsNullOrEmpty(bnkmodel.BankDetailFromDate))
                        {
                            DateTime date = DateTime.ParseExact(bnkmodel.BankDetailFromDate, "dd/MM/yyyy", null);
                            StaffBankDetail.FromDate = date;
                        }
                        if (!string.IsNullOrEmpty(bnkmodel.BankDetailToDate))
                        {
                            DateTime date = DateTime.ParseExact(bnkmodel.BankDetailToDate, "dd/MM/yyyy", null);
                            StaffBankDetail.ToDate = date;
                        }

                        _IStaffService.UpdateStaffBankDetail(StaffBankDetail);
                        TempData["Successmsge"] = "Bank Detail Updated Successfully";
                        return RedirectToAction("Edit", new { Id = model.EncStaffId });
                    }
                    TempData["Errormsge"] = "Invalid Bank Detail";
                    return RedirectToAction("Edit", new { Id = model.EncStaffId });
                }
                else if(model.StaffId > 0)
                {
                    var StaffBankDetail = new KSModel.Models.StaffBankDetail();
                    StaffBankDetail.StaffId = model.StaffId;
                    StaffBankDetail.AccountNo = bnkmodel.AccountNo;
                    StaffBankDetail.BankName = bnkmodel.BankName;
                    StaffBankDetail.Branch = bnkmodel.Branch;
                    StaffBankDetail.BranchCode = bnkmodel.BranchCode;
                    StaffBankDetail.IFSCCode = bnkmodel.IFSCCode;
                    StaffBankDetail.MICR = bnkmodel.MICR;

                    if (!string.IsNullOrEmpty(bnkmodel.BankDetailFromDate))
                    {
                        DateTime date = DateTime.ParseExact(bnkmodel.BankDetailFromDate, "dd/MM/yyyy", null);
                        StaffBankDetail.FromDate = date;
                    }
                    else
                        StaffBankDetail.FromDate = null;

                    if (!string.IsNullOrEmpty(bnkmodel.BankDetailToDate))
                    {
                        DateTime date = DateTime.ParseExact(bnkmodel.BankDetailToDate, "dd/MM/yyyy", null);
                        StaffBankDetail.ToDate = date;
                    }
                    else
                    {
                        StaffBankDetail.ToDate = null;
                    }

                    _IStaffService.InsertStaffBankDetail(StaffBankDetail);
                    TempData["Successmsge"] = "Bank Detail Saved Successfully";
                    return RedirectToAction("Edit", new { Id = model.EncStaffId });
                }
                TempData["Errormsge"] = "Invalid Staff";
                return RedirectToAction("ManageStaff");
            }
            catch (Exception ex)
            {

                TempData["Errormsge"] = ex.Message;
                return RedirectToAction("Edit", new { Id = model.EncStaffId });
            }
            return RedirectToAction("Edit", new { Id = model.EncStaffId });
        }
        #endregion

        public ActionResult DeleteStaffBankDetail(int StaffbankDteailId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                if (StaffbankDteailId > 0)
                {
                    _IStaffService.DeleteStaffBankDetail(StaffbankDteailId);
                    return Json(new
                    {
                        status = "success",
                        data = "Bank Detail Deleted SuccessFully"
                    });
                }
                return Json(new
                {
                    status = "error",
                    data = "Invalid bank Detail"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }
        #region Staff report by Class and Subject

        public StaffReportModel preparestaffreportmodel(StaffReportModel model)
        {
            // class list
            var allstandards = _ICatalogMasterService.GetAllStandardMasters(ref count);
            allstandards = allstandards.OrderBy(c => c.StandardIndex).ToList();

            model.AvailableStandard.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            model.AvailableStandard.Add(new SelectListItem { Selected = false, Text = "All Standard", Value = "0" });
            foreach (var item in allstandards)
                model.AvailableStandard.Add(new SelectListItem { Selected = false, Text = item.Standard, Value = item.StandardId.ToString() });

            var allsubjects = _ISubjectService.GetAllSubjects(ref count);

            model.AvailableSubjects.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            model.AvailableSubjects.Add(new SelectListItem { Selected = false, Text = "All Subjects", Value = "0" });
            foreach (var item in allsubjects)
                model.AvailableSubjects.Add(new SelectListItem { Selected = false, Text = item.Subject1, Value = item.SubjectId.ToString() });

            return model;
        }

        public ActionResult StaffReports()
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;


                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffReports", "View");
                    if (!isauth)
                        return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                StaffReportModel model = new StaffReportModel();
                model = preparestaffreportmodel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult StaffReports(StaffReportModel model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;


                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffReports", "View");
                    if (!isauth)
                        return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                if (model.ViewBy == 1) // class
                {
                    // get class teacher
                    var teacherclasses = _IStaffService.GetAllTeacherClasss(ref count);
                    if (model.StandardId > 0)
                        teacherclasses = teacherclasses.Where(c => c.StandardId == model.StandardId).ToList();

                    // distinct teacher
                    var teacherids = teacherclasses.Select(t => t.TeacherId).Distinct().ToArray();

                    foreach (var item in teacherids)
                    {
                        StaffReportByClassSubjectModel element = new StaffReportByClassSubjectModel();

                        // get staff by id
                        var staff = _IStaffService.GetStaffById((int)item);
                        element.TeacherName = staff != null ? staff.FName + " " + staff.LName : "";
                        element.TeacherStatus = (bool)staff.IsActive;
                        // get teacher subject
                        var teachersubject = _IStaffService.GetAllTeacherSubjects(ref count, TeacherId: item);
                        element.SubjectName = String.Join(" , ", teachersubject.Select(t => t.Subject.Subject1).ToArray());
                        element.StandardName = "";

                        model.StaffReportByClassSubjectList.Add(element);
                    }

                }
                else if (model.ViewBy == 2) // subject
                {
                    // get class teacher
                    var teachersubject = _IStaffService.GetAllTeacherSubjects(ref count);
                    if (model.SubjectId > 0)
                        teachersubject = teachersubject.Where(c => c.SubjectId == model.SubjectId).ToList();

                    // distinct teacher
                    var teacherids = teachersubject.Select(t => t.TeacherId).Distinct().ToArray();

                    foreach (var item in teacherids)
                    {
                        StaffReportByClassSubjectModel element = new StaffReportByClassSubjectModel();

                        // get staff by id
                        var staff = _IStaffService.GetStaffById((int)item);
                        element.TeacherName = staff != null ? staff.FName + " " + staff.LName : "";
                        element.TeacherStatus = (bool)staff.IsActive;
                        // get teacher subject
                        var teacherclass = _IStaffService.GetAllTeacherClasss(ref count, TeacherId: item);
                        // standardids 
                        var standardids = teacherclass.Select(t => t.StandardId).Distinct().ToArray();
                        // teacher standard
                        var Stdclasses = _ICatalogMasterService.GetAllStandardMasters(ref count);
                        Stdclasses = Stdclasses.Where(s => standardids.Contains(s.StandardId)).ToList();

                        element.StandardName = String.Join(" , ", Stdclasses.Select(t => t.Standard).ToArray());
                        element.SubjectName = "";

                        model.StaffReportByClassSubjectList.Add(element);
                    }
                }

                model = preparestaffreportmodel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #region ajax methods

        public ActionResult DeleteDocument(int docId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
            var StaffDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StaffDocuments").FirstOrDefault();
            try
            {
                if (docId > 0)
                {
                    var document = _IStaffService.GetStaffDocumentById(StaffDocumentId: docId);
                    if (document != null)
                    {
                        _IStaffService.DeleteStaffDocument(StaffDocumentId: docId);
                        var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                        var staffdoc = Path.Combine(Server.MapPath("~/Images/" + SchoolDbId + "/Teacher/" + document.StaffId + "/" + document.Image));
                        if ((System.IO.File.Exists(staffdoc)))
                        {
                            System.IO.File.Delete(staffdoc);
                        }
                    }
                    if (StaffDocumenttable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, StaffDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog(document.DocumentId, StaffDocumenttable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Delete StaffDocuments with DocumentId:{0}", document.DocumentId), Request.Browser.Browser);
                    }
                    return Json(new { status = "success", data = "Document deleted successfully." });
                }
                return Json(new { status = "success", data = "" });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }


        public ActionResult DeleteStaffQualification(int docId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
            var StaffDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StaffQualification").FirstOrDefault();
            try
            {
                if (docId > 0)
                {
                    var document = _IStaffService.GetStaffQualificationById(docId);
                    if (document != null)
                    {
                        _IStaffService.DeleteStaffQualification(docId);
                        var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                        var staffdoc = Path.Combine(Server.MapPath("~/Images/" + SchoolDbId + "/Teacher/" + document.StaffId + "/" + document.QualificationImage));
                        if ((System.IO.File.Exists(staffdoc)))
                        {
                            System.IO.File.Delete(staffdoc);
                        }
                    }
                    if (StaffDocumenttable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, StaffDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog(document.StaffQualificationId, StaffDocumenttable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Delete StaffDocuments with DocumentId:{0}", document.StaffQualificationId), Request.Browser.Browser);
                    }
                    return Json(new { status = "success", data = "Staff Qualification deleted successfully." });
                }
                return Json(new { status = "success", data = "" });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }
        public ActionResult DeleteStaffExperience(int docId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
            var StaffDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StaffExperience").FirstOrDefault();
            try
            {
                if (docId > 0)
                {
                    var document = _IStaffService.GetStaffExperienceById(docId);
                    if (document != null)
                    {
                        _IStaffService.DeleteStaffExperience(docId);
                        var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                        var staffdoc = Path.Combine(Server.MapPath("~/Images/" + SchoolDbId + "/Teacher/" + document.StaffId + "/" + document.ExperienceImage));
                        if ((System.IO.File.Exists(staffdoc)))
                        {
                            System.IO.File.Delete(staffdoc);
                        }
                    }
                    if (StaffDocumenttable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, StaffDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog(document.StaffExperienceId, StaffDocumenttable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Delete StaffDocuments with DocumentId:{0}", document.StaffExperienceId), Request.Browser.Browser);
                    }
                    return Json(new { status = "success", data = "Staff Experience deleted successfully." });
                }
                return Json(new { status = "success", data = "" });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }
        [HttpPost]
        public ActionResult GetStaffList(DataSourceRequest command, StaffListModel model, IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            int count = 0;
            var staffs = new List<Staff>();
            bool? status = null;
            if (!string.IsNullOrEmpty(model.FilterStatus))
            {
                if (model.FilterStatus == "True")
                    status = true;
                else
                    status = false;
            }


            // if (model.DepartmentId > 0 || model.DesignationId > 0 || model.Name != null)
            staffs = _IStaffService.GetAllStaffs(ref count, Fullname: model.Name,
                                                     DepartmentId: model.DepartmentId, DesignationId: model.DesignationId,
                                                     StaffTypeId: model.StaffTypeId, IsActive: status, EmpCode: model.EmpCode).OrderBy(p => p.EmpCode).ToList();
            var contactinfotype = _IContactService.GetAllContactInfos(ref count).Where(m => m.ContactInfoType.ContactInfoType1.ToLower() == "mobile").FirstOrDefault();
            //else
            var contactinfotypeID = 0;
            if (contactinfotype != null)
            {
                contactinfotypeID = (int)contactinfotype.ContactInfoTypeId;
            }
            var AllStaffServiceTypeDetail = _IStaffService.GetAllStaffServiceTypeDetail(ref count);
            // staffs = _IStaffService.GetAllStaffs(ref count);
            var stfs = staffs.PagedForCommand(command).Select(x =>
            {
                int counter = 0;
                var staffGridModel = new StaffGridModel();
                staffGridModel.IsStaffDeletable = true;
                if (x.ClassPeriodDetails.Count > 0 || x.ClassTeachers.Count > 0 ||
                    x.TeacherClasses.Count > 0 || x.HWs.Count > 0 || x.TeacherSubjects.Count > 0 ||
                    x.StaffLeaveApplications.Count > 0 || x.StaffAttendances.Count > 0
                )
                {
                    if (x.ClassTeachers.Count > 0)
                    {
                        foreach (var item in x.ClassTeachers)
                        {
                            if (item.EndDate <= DateTime.Now)
                                counter += counter + 1;
                        }
                        if (counter != x.ClassTeachers.Count)
                            staffGridModel.IsStaffDeletable = false;
                    }
                    else
                        staffGridModel.IsStaffDeletable = true;
                    if (x.ClassPeriodDetails.Count > 0 || x.TeacherClasses.Count > 0 || x.HWs.Count > 0
                        || x.TeacherSubjects.Count > 0 || x.StaffLeaveApplications.Count > 0 || x.StaffAttendances.Count > 0)
                    {
                        staffGridModel.IsStaffDeletable = false;
                    }
                }
                staffGridModel.IsExpAcademicsDetail = false;
                if ( x.StaffQualifications.Count>0 || x.StaffExperiences.Count > 0)
                {
                    staffGridModel.IsExpAcademicsDetail = true;
                }
                //staffGridModel.StaffId = x.StaffId;
                var IfStaffServiceType = AllStaffServiceTypeDetail.Where(f => f.StaffId == x.StaffId).OrderByDescending(f => f.StaffServiceTypeDetailId).FirstOrDefault();
                if (IfStaffServiceType != null)
                    staffGridModel.ServiceType = IfStaffServiceType.StaffServiceType.StaffServiceType1;
                else
                    staffGridModel.ServiceType = "...";

                staffGridModel.DateOfjoin = x.DOJ.HasValue ? ConvertDate(x.DOJ.Value.Date) : "";
                staffGridModel.EncStaffId = _ICustomEncryption.base64e(x.StaffId.ToString());
                staffGridModel.Name = x.FName + " " + x.LName;
                staffGridModel.Designation = x.Designation == null ? "N/A" : x.Designation.Designation1;
                staffGridModel.Department = x.Department == null ? "N/A" : x.Department.Department1;
                staffGridModel.EmpCode = x.EmpCode;
                staffGridModel.IsActive = (bool)x.IsActive == true ? "Active" : "InActive";
                staffGridModel.MachineId = x.PunchMcId != null ? x.PunchMcId.ToString() : "";
                staffGridModel.PunchMcId = x.PunchMcId != null ? x.PunchMcId : 0;
                staffGridModel.status = x.IsActive;
                staffGridModel.MobileNo = _IContactService.GetAllContactInfos(ref count, ContactTypeId: x.ContactTypeId, Isdefault: true, ContactInfoTypeId: contactinfotypeID, ContactId: x.StaffId).FirstOrDefault() != null ? _IContactService.GetAllContactInfos(ref count, ContactTypeId: x.ContactTypeId, Isdefault: true, ContactInfoTypeId: contactinfotypeID, ContactId: x.StaffId).FirstOrDefault().ContactInfo1 : "";
                return staffGridModel;
            }).AsQueryable().Sort(sort);
            var gridModel = new DataSourceResult
            {
                Data = stfs.ToList(),
                Total = staffs.Count()
            };
            return Json(gridModel);
        }

        //Delete Staff
        public ActionResult Delete(string Id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffList", "Delete");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            int staff_id = 0;
            if (!string.IsNullOrEmpty(Id))
            {
                // decrypt Id
                var qsid = _ICustomEncryption.base64d(Id);

                if (int.TryParse(qsid, out staff_id))
                {


                    if (int.TryParse(qsid, out staff_id))
                    {

                    }
                }

                var logtypeupdate = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
                // TimeTable log Table
                var Stafftable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "Staff").FirstOrDefault();
                var Addresstable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "Address").FirstOrDefault();
                var StaffDocumentstable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StaffDocuments").FirstOrDefault();
                var ContactInfotable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ContactInfo").FirstOrDefault();
                var ClassTeachertable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassTeacher").FirstOrDefault();

                var staff = _IStaffService.GetStaffById(StaffId: staff_id, IsTrack: true);
                var Stfcontacttype = _IAddressMasterService.GetAllContactTypes(ref count, _IStaffService.GetStaffTypeById((int)staff.StaffTypeId).StaffType1).FirstOrDefault(); // staff contact type
                if (staff != null && Stfcontacttype != null)
                {
                    if (staff.TeacherClasses.Count == 0 && staff.TeacherSubjects.Count == 0 && staff.ClassPeriodDetails.Count == 0)
                    {
                        var allAddresses = _IAddressService.GetAllAddresss(ref count, contactId: staff_id, contacttypeId: Stfcontacttype.ContactTypeId).ToList();
                        if (allAddresses.Count > 0)
                        {
                            foreach (var item in allAddresses)
                            {
                                _IAddressService.DeleteAddress(item.AddressId);
                                if (Addresstable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, Addresstable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(item.AddressId, Addresstable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete Address with AddressId:{0} and AddressTypeId:{1}", item.AddressId, item.AddressTypeId), Request.Browser.Browser);
                                }
                            }
                        }
                        var allDocuments = _IStaffService.GetAllStaffDocuments(ref count, StaffId: staff_id).ToList();
                        var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                        var staffdoc = "";
                        if (allDocuments.Count > 0)
                        {
                            foreach (var item in allDocuments)
                            {
                                _IStaffService.DeleteStaffDocument(item.DocumentId);
                                if (item.StaffDocumentType.DocumentType == "Image")
                                {
                                    staffdoc = Path.Combine(Server.MapPath("~/Images/" + SchoolDbId + "/Teacher/Photos/" + item.Image));
                                    if ((System.IO.File.Exists(staffdoc)))
                                        System.IO.File.Delete(staffdoc);
                                }
                                else
                                {
                                    staffdoc = Path.Combine(Server.MapPath("~/Images/" + SchoolDbId + "/Teacher/" + item.StaffId + "/" + item.Image));
                                    if ((System.IO.File.Exists(staffdoc)))
                                        System.IO.File.Delete(staffdoc);
                                }
                                if (StaffDocumentstable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, StaffDocumentstable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(item.DocumentId, StaffDocumentstable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete StaffDocument with DocumentId:{0} and DocumentTypeId:{1}", item.DocumentId, item.DocumentTypeId), Request.Browser.Browser);
                                }
                            }
                        }
                        var allContacts = _IContactService.GetAllContactInfos(ref count, ContactId: staff_id);
                        if (allContacts.Count > 0)
                        {
                            foreach (var item in allContacts)
                            {
                                _IContactService.DeleteContactInfo(item.ContactInfoId);
                                if (ContactInfotable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ContactInfotable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog((int)item.ContactId, ContactInfotable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete ContactInfo with DocumentId:{0} and ContactInfoTypeId:{1}", item.ContactId, item.ContactInfoTypeId), Request.Browser.Browser);
                                }
                            }
                        }
                        _IStaffService.DeleteStaff(StaffId: staff_id);
                        if (Stafftable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ContactInfotable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog((int)staff.StaffId, Stafftable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete Staff with StaffId:{0} and StaffTypeId:{1}", staff.StaffId, staff.StaffTypeId), Request.Browser.Browser);
                        }
                        var classTeacher = _IStaffService.GetAllClassTeachers(ref count, TeacherId: staff_id);
                        if (classTeacher.Count > 0)
                        {
                            foreach (var item in classTeacher)
                            {
                                _IStaffService.DeleteClassTeacher(item.ClassTeacherId);
                                if (ClassTeachertable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassTeachertable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(item.ClassTeacherId, ClassTeachertable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete ClassTecher with ClassTeacherId:{0}", item.ClassTeacherId), Request.Browser.Browser);
                                }
                            }
                        }
                        //var teacherSubject = _IStaffService.GetAllTeacherSubjects(ref count, TeacherId: staff_id);
                        //if (teacherSubject.Count > 0)
                        //{
                        //    foreach (var item in teacherSubject)
                        //    {
                        //        _IStaffService.DeleteTeacherSubject(item.TeacherSubjectId);
                        //    }
                        //}
                        //var teacherClasses = _IStaffService.GetAllTeacherClasss(ref count, TeacherId: staff_id);
                        //if (teacherClasses.Count > 0)
                        //{
                        //    foreach (var item in teacherClasses)
                        //    {
                        //        _IStaffService.DeleteTeacherClass(item.TeacherClassId);
                        //    }
                        //}
                    }
                }
            }
            return RedirectToAction("StaffList");
        }

        public ActionResult IsStaffAssignedTimeTable(string StaffId = "")
        {
            try
            {
                int staff_id = 0;
                if (!string.IsNullOrEmpty(StaffId))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(StaffId);
                    if (int.TryParse(qsid, out staff_id))
                    {
                    }
                    var List = _IStaffService.StaffAssignedTimeTable(ref count, StaffId: staff_id);
                    if (List.Count > 0)
                    {
                        var name = List.FirstOrDefault().Teacher;
                        return Json(new
                        {
                            status = "success",
                            IsStaffAssignedTimeTable = true,
                            StaffName= name,
                        });
                    }
                    return Json(new
                    {
                        status = "success",
                        IsStaffAssignedTimeTable = false
                    });
                }
                return Json(new
                {
                    status = "error",
                    data = "Invalid Staff"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }

        }
        
        public ActionResult UnAssignTimeTablePeriods(string StaffId = "")
        {
            try
            {
                int staff_id = 0;
                if (!string.IsNullOrEmpty(StaffId))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(StaffId);
                    if (int.TryParse(qsid, out staff_id))
                    {
                    }
                    var List = _IClassPeriodService.GetAllClassPeriodDetails(ref count).Where(f=>f.TeacherId==staff_id);
                    var ClassPeriodDetailIds = List.Select(m => m.ClassPeriodDetailId).ToArray();
                    if(ClassPeriodDetailIds!=null && ClassPeriodDetailIds.Count() > 0)
                    {
                        var ClassPeriodDays= _IClassPeriodService.GetAllClassPeriodDays(ref count).Where(f =>ClassPeriodDetailIds.Contains((int)f.ClassPeriodDetailId));
                        foreach(var cpd in ClassPeriodDays)
                        {
                            _IClassPeriodService.DeleteClassPeriodDay(cpd.DayDetailId);
                        }
                    }
                    foreach (var id in List)
                        _IClassPeriodService.DeleteClassPeriodDetail(id.ClassPeriodDetailId);
                    
                    return Json(new
                    {
                        status = "success",
                        data="Periods UnAssigned SuccessFully"
                    });
                }
                return Json(new
                {
                    status = "error",
                    data = "Invalid Staff"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }

        }
        public ActionResult DesignationList(string StafftypeId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            try
            {
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                IList<SelectListItem> DesignationList = new List<SelectListItem>();
                DesignationList.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });

                int Stafftypeid = Convert.ToInt32(StafftypeId);

                var designation = _IStaffService.GetAllDesignations(ref count, StafftypeId: Stafftypeid);
                foreach (var item in designation)
                    DesignationList.Add(new SelectListItem { Selected = false, Text = item.Designation1, Value = item.DesignationId.ToString() });

                return Json(new
                {
                    status = "success",
                    data = DesignationList
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult GetExpAcdemicsList (string StaffId)
        {
            try
            {
                if (!string.IsNullOrEmpty(StaffId))
                {
                    int id = 0;
                    var qsid = _ICustomEncryption.base64d(StaffId);
                    if (int.TryParse(qsid, out id))
                    {
                        IList<ViewModel.ViewModel.Staff.StaffExperience> StaffExperienceList = new List<ViewModel.ViewModel.Staff.StaffExperience>();
                        var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                        var ExpList = _IStaffService.GetAllStaffExperiences(ref count).Where(f => f.StaffId == id);
                        var staffExperience = new ViewModel.ViewModel.Staff.StaffExperience();
                        foreach (var item in ExpList)
                        {
                            staffExperience = new ViewModel.ViewModel.Staff.StaffExperience();
                            staffExperience.Organsation = item.Organsation;
                            staffExperience.Designation = item.Designation;
                            staffExperience.City = item.City;
                            staffExperience.JoiningDate = item.JoiningDate != null ? ConvertDate((DateTime)item.JoiningDate) : "";
                            staffExperience.LeavingDate = item.LeavingDate != null ? ConvertDate((DateTime)item.LeavingDate) : "";
                            staffExperience.ExperienceImage = item.ExperienceImage;
                            staffExperience.StaffExperienceId = item.StaffExperienceId;
                            staffExperience.IsExperienceDocument = false;
                            if (!string.IsNullOrEmpty(item.ExperienceImage))
                            {
                                staffExperience.IsExperienceDocument = true;
                                staffExperience.imgExDocumentType = item.ExperienceImage.Split('.').Last();
                            }

                            staffExperience.ExperienceImage = _WebHelper.GetStoreLocation() + "/Images/" + SchoolDbId + "/Teacher/" + id + "/" + item.ExperienceImage;

                            StaffExperienceList.Add(staffExperience);
                        }

                        IList<ViewModel.ViewModel.Staff.StaffQualification> StaffQualificationList = new List<ViewModel.ViewModel.Staff.StaffQualification>();
                        var QualList = _IStaffService.GetAllStaffQualifications(ref count).Where(f => f.StaffId == id);
                        var staffQualification = new ViewModel.ViewModel.Staff.StaffQualification();
                        foreach (var item in QualList)
                        {
                            staffQualification = new ViewModel.ViewModel.Staff.StaffQualification();
                            staffQualification.ExamPassed = item.ExamPassed;
                            staffQualification.BoardUniversity = item.BoardUniversity;
                            staffQualification.InstitutionName = item.InstitutionName;
                            staffQualification.ObtainedMarks = item.ObtainedMarks;
                            staffQualification.PassingYear = item.PassingYear != null ? ConvertDate((DateTime)item.PassingYear) : "";
                            staffQualification.TotalMarks = item.TotalMarks;
                            staffQualification.StaffQualificationId = item.StaffQualificationId;
                            staffQualification.IsQualificationDocument = false;
                            if (!string.IsNullOrEmpty(item.QualificationImage))
                            {
                                staffQualification.IsQualificationDocument = true;
                                staffQualification.imgDocumentType = item.QualificationImage.Split('.').Last();
                            }
                            staffQualification.QualificationImage = _WebHelper.GetStoreLocation() + "/Images/" + SchoolDbId + "/Teacher/" + id + "/" + item.QualificationImage;
                            StaffQualificationList.Add(staffQualification);
                        }
                        return Json(new
                        {
                            status = "success",
                            StaffQualificationList= StaffQualificationList,
                            StaffExperienceList= StaffExperienceList,
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new
                {
                    status = "error",
                    data = "Invalid Staff"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult StaffAssignedTimeTableList(DataSourceRequest command, IEnumerable<Sort> sort = null, string EncStaff_Id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            var staffTimeTableList = new List<StaffTimeTable>();
            int staff_id=0;
            if (!string.IsNullOrEmpty(EncStaff_Id))
            {
                var qsid = _ICustomEncryption.base64d(EncStaff_Id);
                if (int.TryParse(qsid, out staff_id))
                {
                }
                staffTimeTableList = _IStaffService.StaffAssignedTimeTable(ref count, StaffId: staff_id).ToList();
            }
           
            var gridModel = new DataSourceResult
            {
                Data = staffTimeTableList.PagedForCommand(command).AsQueryable().Sort(sort).ToList(),
                Total = staffTimeTableList.Count()
            };
            return Json(gridModel);
        }
        #endregion

        #region ClassIncharge
        //prepare model for manageclassincharge
        public ManageClassIncharge prepareClassInchargeModel(ManageClassIncharge model)
        {
            Session currentSession = _ICatalogMasterService.GetCurrentSession();
            model.CurrentSessionEndDate = ConvertDateForKendo((DateTime)currentSession.EndDate);
            model.CurrentSessionStartDate = ConvertDateForKendo((DateTime)currentSession.StartDate);
            model.TomorrowsDate = ConvertDateForKendo(DateTime.Now.AddDays(1));
            var classList = new List<SelectListItem>();
            var teacherList = new List<SelectListItem>();
            var allClasses = new List<SelectListItem>();
            var ClassListForFilter = new List<SelectListItem>();
            var TeacherListForFilter = new List<SelectListItem>();
            var classTeachers = _IStaffService.GetAllClassTeachers(ref count);

            var InchargeFutureEntries = _ISchoolSettingService.GetorSetSchoolData("InchargeFutureEntries", "1").AttributeValue;
            var IsMultipleClassIncharge = _ISchoolSettingService.GetorSetSchoolData("IsMultipleClassIncharge", "1").AttributeValue;
            if (InchargeFutureEntries != "1")
                InchargeFutureEntries = "0";
            if (IsMultipleClassIncharge != "1")
                IsMultipleClassIncharge = "0";

            var classIds = classTeachers.Where(p => p.EndDate > DateTime.Now.Date || p.EndDate == null).Select(p => p.ClassId).Distinct().ToList();
            var teacherIds = classTeachers.Select(p => p.TeacherId).Distinct().ToList();

            //allclasses
            var classes = _ICatalogMasterService.GetAllClassMasters(ref count);

            //dropdown having all classes
            allClasses.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
            foreach (var item in classes)
                allClasses.Add(new SelectListItem { Selected = false, Value = item.ClassId.ToString(), Text = item.Class });

            //dropdown having non assigned classes
            classes = classes.Where(p => !classIds.Contains(p.ClassId)).ToList();
            classList.Add(new SelectListItem { Selected = false, Text = "--Select--", Value = "" });
            foreach (var item in classes)
                classList.Add(new SelectListItem { Selected = false, Text = item.Class.ToString(), Value = item.ClassId.ToString() });

            var teachers = _IStaffService.GetAllStaffs(ref count, IsActive: true);
            teacherList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
            var inchargeTeachers = teachers.Where(p => p.StaffType.StaffType1 == "Teacher" && teacherIds.Contains(p.StaffId))
                                        .OrderBy(x => x.FName.Trim()).ToList();

            //dropdown for filter
            TeacherListForFilter.Add(new SelectListItem { Selected = false, Text = "--Select--", Value = "" });
            foreach (var item in inchargeTeachers)
            {
                TeacherListForFilter.Add(new SelectListItem
                {
                    Selected = false,
                    Text = item.FName + " " + item.LName,
                    Value = item.StaffId.ToString()
                });
            }
            model.TeacherListForFilter = TeacherListForFilter;

            model.ClassListForFilter = _IDropDownService.ClassList();
            model.ClassList = classList;
            model.TeacherList = teacherList;

            if (IsMultipleClassIncharge == "1")
                model.ClassList = classList;
            else if (InchargeFutureEntries == "1")
                model.ClassList = allClasses;

            //values for kendo paging
            model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
            model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");
            return model;
        }

        public ActionResult ManageClassIncharge()
        {
            ManageClassIncharge model = new ManageClassIncharge();
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;


                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "ManageClassIncharge", "View");
                    if (!isauth)
                        return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                model = prepareClassInchargeModel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "ManageClassIncharge", "Add Record");
                model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "ManageClassIncharge", "Delete Record");
                model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "ManageClassIncharge", "Edit Record");

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public string CheckClassInchargeModel(ManageClassIncharge InchargeData)
        {
            Session currentSession = _ICatalogMasterService.GetCurrentSession();
            string errorString = "";
            if (!InchargeData.ClassId.HasValue)
                errorString += "Please select Class ε";
            if (!InchargeData.TeacherId.HasValue)
                errorString += "Please select Teacher ε";
            if (InchargeData.EffectiveDate == null)
                errorString += "Please provide Effective Date ε";
            if (InchargeData.EndDate < InchargeData.EffectiveDate)
                errorString += "End Date should be greater than effective date  ε";
            //if (InchargeData.EffectiveDate > currentSession.EndDate || InchargeData.EffectiveDate < currentSession.StartDate)
            //    errorString += "Invalid Effective Date ε";
            if (InchargeData.EndDate < DateTime.Now.Date || InchargeData.EndDate > currentSession.EndDate || InchargeData.EndDate < currentSession.StartDate)
                errorString += "Invalid End Date ε";

            if (InchargeData.TeacherId != null)
            {
                //var reqclass = _IStaffService.GetAllClassTeachers(ref count, ClassId: InchargeData.ClassId)
                //                  .Where(p => p.EffectiveDate.Value.Date <= DateTime.Now.Date && p.EndDate.Value.Date >= DateTime.Now.Date).FirstOrDefault();

                if (InchargeData.ClassTeacherId == null)
                {
                    var reqclass = _IStaffService.GetAllClassTeachers(ref count, ClassId: InchargeData.ClassId)
                                      .Where(p => p.EffectiveDate.Value.Date <= DateTime.Now.Date);

                    if (reqclass.FirstOrDefault() != null)
                    {
                        if (reqclass.FirstOrDefault().EndDate != null)
                        {
                            reqclass = reqclass.Where(p => p.EndDate.Value.Date >= DateTime.Now.Date);
                        }

                        if (reqclass.FirstOrDefault() != null)
                        {
                            if (reqclass.FirstOrDefault().TeacherId != InchargeData.TeacherId)
                                errorString += "Class already have a class incharge ε";
                        }
                    }
                }
            }

            return errorString;
        }

        public ActionResult CreateOrUpdateClassIncharge(ManageClassIncharge InchargeData)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;


                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "ManageClassIncharge", "Add");
                    //if (!isauth)
                    //    return RedirectToAction("AccessDenied", "Error");
                }

                var InchargeFutureEntries = _ISchoolSettingService.GetAttributeValue("InchargeFutureEntries");
                var IsMultipleClassIncharge = _ISchoolSettingService.GetAttributeValue("IsMultipleClassIncharge");
                if (InchargeFutureEntries == "" || InchargeFutureEntries == null)
                    InchargeFutureEntries = "0";
                if (IsMultipleClassIncharge == "" || IsMultipleClassIncharge == null)
                    IsMultipleClassIncharge = "0";

                var errorString = CheckClassInchargeModel(InchargeData);
                var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                var classTeacherTable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassTeacher").FirstOrDefault();

                var classTeacherExist = _IStaffService.GetAllClassTeachers(ref count, TeacherId: InchargeData.TeacherId).ToList();
                classTeacherExist = classTeacherExist.Where(p => (p.EndDate == null || p.EndDate != null) && p.EndDate >= DateTime.Now.Date)
                                                                                       .OrderByDescending(x => x.ClassTeacherId).ToList();
                var classExist = _IStaffService.GetAllClassTeachers(ref count, ClassId: InchargeData.ClassId).ToList();
                var classExistWithNullEndDate = classExist.Where(p => p.EndDate == null).ToList();
                var classExistWithFutureEndDate = classExist.Where(p => p.EndDate >= DateTime.Now.Date).FirstOrDefault();

                if (errorString != "")
                    return Json(new { status = "failed", error = errorString });
                else
                {
                    if (InchargeData.ClassTeacherId == null)
                    {
                        #region insertClassIncharge
                        if ((IsMultipleClassIncharge == "1" || IsMultipleClassIncharge == "0") && InchargeFutureEntries == "0")
                        {
                            if (classTeacherExist.Count > 0 && IsMultipleClassIncharge == "0")
                                return Json(new { status = "failed", data = "Teacher is restricted to be incharge of multiple classes." });
                            else
                            {
                                ClassTeacher ClassTeacher = new ClassTeacher();
                                ClassTeacher.ClassId = InchargeData.ClassId;
                                ClassTeacher.TeacherId = InchargeData.TeacherId;
                                ClassTeacher.EffectiveDate = InchargeData.EffectiveDate;
                                ClassTeacher.EndDate = InchargeData.EndDate;
                                _IStaffService.InsertClassTeacher(ClassTeacher);
                                if (classTeacherTable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, classTeacherTable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(ClassTeacher.ClassTeacherId, (int)ClassTeacher.ClassId, logtype.ActivityLogTypeId, user, String.Format("Add ClassTeacher with ClassId:{0} and ClassTeacherId:{1}", ClassTeacher.ClassId, ClassTeacher.ClassTeacherId), Request.Browser.Browser);
                                }
                            }
                        }
                        if (InchargeFutureEntries == "1")
                        {
                            if (classExist.Count > 0)
                            {
                                var cls = classExist.Where(x => x.EndDate != null && x.EndDate >= DateTime.Now.Date).FirstOrDefault();
                                if (cls != null)
                                {
                                    if (classExist.Any(p => p.TeacherId == InchargeData.TeacherId))
                                        return Json(new { status = "failed", data = "Teacher is already incharge of this class." });
                                }
                            }
                            if (IsMultipleClassIncharge == "0")
                            {
                                if (classTeacherExist.Count > 0)
                                    return Json(new { status = "failed", data = "Teacher is restricted to be Incharge of multiple classes." });
                            }
                            else
                            {
                                if (classExistWithNullEndDate.Count > 0)
                                    return Json(new { status = "failed", data = "To change class incharge first set the End Date for current incharge and then add incharge." });
                                if (classExistWithFutureEndDate != null)
                                {
                                    if (classExistWithFutureEndDate.EndDate >= InchargeData.EffectiveDate)
                                        return Json(new { status = "failed", data = "New Incharge Effective Date should be Greater than End Date of current incharge." });
                                }
                            }
                            ClassTeacher ClassTeacher = new ClassTeacher();
                            ClassTeacher.ClassId = InchargeData.ClassId;
                            ClassTeacher.TeacherId = InchargeData.TeacherId;
                            ClassTeacher.EffectiveDate = InchargeData.EffectiveDate;
                            ClassTeacher.EndDate = InchargeData.EndDate;
                            _IStaffService.InsertClassTeacher(ClassTeacher);
                            if (classTeacherTable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, classTeacherTable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(ClassTeacher.ClassTeacherId, (int)ClassTeacher.ClassId, logtype.ActivityLogTypeId, user, String.Format("Add ClassTeacher with ClassId:{0} and ClassTeacherId:{1}", ClassTeacher.ClassId, ClassTeacher.ClassTeacherId), Request.Browser.Browser);
                            }
                        }
                        return Json(new { status = "success", data = "Incharge saved successfully." });
                        #endregion
                    }
                    else
                    {
                        #region updateClassIncharge
                        var ClassTeacher = _IStaffService.GetClassTeacherById((int)InchargeData.ClassTeacherId, true);
                        var futureClassIncharge = _IStaffService.GetAllClassTeachers(ref count, ClassId: InchargeData.ClassId).ToList();
                        futureClassIncharge = futureClassIncharge.Where(p => p.EffectiveDate > DateTime.Now.Date).OrderByDescending(x => x.ClassTeacherId).ToList();
                        //if (futureClassIncharge.Count > 0)
                        //{
                        //if (InchargeData.TeacherId != futureClassIncharge.FirstOrDefault().TeacherId)
                        //{
                        //    return Json(new { status = "failed", data = "This class already assigned to a teacher for future." });
                        //}
                        //}

                        //var IsInchargeExist = _IStaffService.GetAllClassTeachers(ref count, ClassId: InchargeData.ClassId).Where(p => p.EffectiveDate);
                        if (ClassTeacher.ClassTeacherId == InchargeData.ClassTeacherId)
                        {
                            //if (ClassTeacher.TeacherId == InchargeData.TeacherId)
                            //{
                            ClassTeacher.ClassId = InchargeData.ClassId;
                            ClassTeacher.TeacherId = InchargeData.TeacherId;
                            ClassTeacher.EffectiveDate = InchargeData.EffectiveDate;
                            ClassTeacher.EndDate = InchargeData.EndDate;
                            _IStaffService.UpdateClassTeacher(ClassTeacher);
                            if (classTeacherTable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, classTeacherTable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(ClassTeacher.ClassTeacherId, (int)ClassTeacher.ClassId, logtype.ActivityLogTypeId, user, String.Format("update ClassTeacher with ClassId:{0} and ClassTeacherId:{1}", ClassTeacher.ClassId, ClassTeacher.ClassTeacherId), Request.Browser.Browser);
                            }
                        }
                        else
                        {
                            if (InchargeFutureEntries == "0" && (IsMultipleClassIncharge == "0" || IsMultipleClassIncharge == "1"))
                            {
                                if (classTeacherExist.Count > 0 && IsMultipleClassIncharge == "0")
                                    return Json(new { status = "failed", data = "Teacher is restricted to be incharge of multiple classes." });
                                else
                                {
                                    ClassTeacher.ClassId = InchargeData.ClassId;
                                    ClassTeacher.TeacherId = InchargeData.TeacherId;
                                    ClassTeacher.EffectiveDate = InchargeData.EffectiveDate;
                                    ClassTeacher.EndDate = InchargeData.EndDate;
                                    _IStaffService.UpdateClassTeacher(ClassTeacher);
                                    if (classTeacherTable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, classTeacherTable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassTeacher.ClassTeacherId, (int)ClassTeacher.ClassId, logtype.ActivityLogTypeId, user, String.Format("update ClassTeacher with ClassId:{0} and ClassTeacherId:{1}", ClassTeacher.ClassId, ClassTeacher.ClassTeacherId), Request.Browser.Browser);
                                    }
                                    return Json(new { status = "success", data = "Incharge updated successfully." });
                                }
                            }
                            if (InchargeFutureEntries == "1")
                            {
                                if (IsMultipleClassIncharge == "0")
                                {
                                    if (classTeacherExist.Count > 0)
                                        return Json(new { status = "failed", data = "Teacher is restricted to be Incharge of multiple classes." });
                                }
                                else
                                {
                                    //if (classExistWithNullEndDate.Count > 0)
                                    //    return Json(new { status = "failed", data = "To change class incharge first set the End Date as current date for current incharge and then add incharge." });
                                    if (classExistWithFutureEndDate != null)
                                    {
                                        if (classExistWithFutureEndDate.EndDate >= InchargeData.EffectiveDate)
                                            return Json(new { status = "failed", data = "New Incharge Effective Date should be Greater than End Date of current incharge." });
                                    }
                                }
                                ClassTeacher.ClassId = InchargeData.ClassId;
                                ClassTeacher.TeacherId = InchargeData.TeacherId;
                                ClassTeacher.EffectiveDate = InchargeData.EffectiveDate;
                                ClassTeacher.EndDate = InchargeData.EndDate;
                                _IStaffService.InsertClassTeacher(ClassTeacher);
                                if (classTeacherTable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, classTeacherTable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(ClassTeacher.ClassTeacherId, (int)ClassTeacher.ClassId, logtype.ActivityLogTypeId, user, String.Format("Add ClassTeacher with ClassId:{0} and ClassTeacherId:{1}", ClassTeacher.ClassId, ClassTeacher.ClassTeacherId), Request.Browser.Browser);
                                }
                            }
                        }
                        return Json(new { status = "success", data = "Incharge updated successfully." });
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public ActionResult DeleteIncharge(int id)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "ManageClassIncharge", "Delete");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
            // classPeriodDetail log Table
            var ClassTeachertable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassTeacher").FirstOrDefault();
            try
            {
                if (id > 0)
                {
                    var classTeacher = _IStaffService.GetClassTeacherById(id, false);
                    if (classTeacher != null)
                    {
                        _IStaffService.DeleteClassTeacher(id);
                        if (ClassTeachertable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassTeachertable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(classTeacher.ClassTeacherId, ClassTeachertable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Delete ClassTeacher with ClassTeacherId:{0}", classTeacher.ClassTeacherId), Request.Browser.Browser);
                        }
                    }
                }
                return Json(new { status = "success", data = "Incharge deleted successfully." });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public ActionResult GetAssignedClassTeachers(int classId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                var teacherList = new List<SelectListItem>();
                teacherList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
                var classStandardsIds = _ICatalogMasterService.GetAllClassMasters(ref count).Where(p => p.ClassId == classId).Select(p => p.StandardId).Distinct().ToList();
                if (classStandardsIds.Count > 0)
                {
                    var classTeacher = _IStaffService.GetAllClassTeachers(ref count).Where(p => p.ClassId == classId).FirstOrDefault();
                    if (classTeacher != null)
                        teacherList.Add(new SelectListItem { Selected = false, Value = classTeacher.Staff.StaffId.ToString(), Text = classTeacher.Staff.FName + " " + classTeacher.Staff.LName });
                    var teacherClasses = _IStaffService.GetAllTeacherClasss(ref count).ToList();
                    if (classTeacher != null)
                        teacherClasses = teacherClasses.Where(p => classStandardsIds.Contains(p.StandardId) && p.Staff.IsActive == true && classTeacher.TeacherId != p.TeacherId).ToList();
                    else
                        teacherClasses = teacherClasses.Where(p => classStandardsIds.Contains(p.StandardId) && p.Staff.IsActive == true).ToList();
                    foreach (var item in teacherClasses.OrderBy(x => x.Staff.FName.Trim()).ToList())
                    {
                        string StaffName = "";

                        if (!string.IsNullOrEmpty(item.Staff.FName))
                            item.Staff.FName = item.Staff.FName.Trim();

                        if (!string.IsNullOrEmpty(item.Staff.LName))
                            item.Staff.LName = item.Staff.LName.Trim();

                        StaffName = (item.Staff.FName + " " + item.Staff.LName).Trim();
                        teacherList.Add(new SelectListItem
                        {
                            Selected = false,
                            Value = item.Staff.StaffId.ToString(),
                            Text = StaffName
                        });
                    }
                    return Json(new { status = "success", data = teacherList });
                }
                else
                {
                    return Json(new { status = "failed" });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public ActionResult GetDatesForEditClassIncharge(int classId, string EffectiveDate, string TeacherId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                string effDateMinVal = "";
                string effCurrentSessionStrtDate = "";
                DateTime? SelectedEffDate = null;
                if (!string.IsNullOrEmpty(EffectiveDate) || !string.IsNullOrWhiteSpace(EffectiveDate))
                {
                    SelectedEffDate = DateTime.ParseExact(EffectiveDate, "dd/MM/yyyy", null);
                    SelectedEffDate = SelectedEffDate.Value.Date;
                }

                int teacherIDk = 0;
                if (!string.IsNullOrEmpty(TeacherId))
                    teacherIDk = Convert.ToInt32(TeacherId);

                var currentSession = _ICatalogMasterService.GetCurrentSession();

                if (classId > 0)
                {
                    var getClassOldClassIncharge = _IStaffService.GetAllClassTeachers(ref count, ClassId: classId).OrderByDescending(x => x.ClassTeacherId)
                                                            .FirstOrDefault();
                    if (getClassOldClassIncharge != null)
                    {
                        if (teacherIDk != getClassOldClassIncharge.TeacherId)
                        {
                            // one class cannot have multiple classincharge at a same time
                            effDateMinVal = Convert.ToDateTime(getClassOldClassIncharge.EndDate).AddDays(1).ToString("MM/dd/yyyy");
                        }
                        else
                            effCurrentSessionStrtDate = Convert.ToDateTime(currentSession.StartDate).ToString("MM/dd/yyyy"); //current session start date
                    }
                }
                return Json(new { effDateMinVal, effCurrentSessionStrtDate }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public ActionResult GetClassTeachers(DataSourceRequest command, InchargeGridModel model, IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            int count = 0;
            DateTime? EffectiveDate = null;
            if (!string.IsNullOrEmpty(model.EffDt) || !string.IsNullOrWhiteSpace(model.EffDt))
            {
                EffectiveDate = DateTime.ParseExact(model.EffDt, "dd/MM/yyyy", null);
                EffectiveDate = EffectiveDate.Value.Date;
            }

            DateTime? EndDate = null;
            if (!string.IsNullOrEmpty(model.EndDt) || !string.IsNullOrWhiteSpace(model.EndDt))
            {
                EndDate = DateTime.ParseExact(model.EndDt, "dd/MM/yyyy", null);
                EndDate = EndDate.Value.Date;
            }


            var classTeachers = _IStaffService.GetAllClassTeachers(ref count, model.ClassId, model.TeacherId)
                                                                    .OrderByDescending(p => p.ClassTeacherId).ToList();
            if (EffectiveDate != null)
                classTeachers = classTeachers.Where(p => p.EffectiveDate >= EffectiveDate).ToList();
            if (EndDate != null)
                classTeachers = classTeachers.Where(p => p.EndDate <= EndDate).ToList();

            //if (model.EffectiveDate != null && model.EndDate != null)
            //    classTeachers = classTeachers.Where(p => p.EffectiveDate >= model.EffectiveDate && (p.EndDate <= model.EndDate && p.EndDate >= model.EffectiveDate)).ToList();
            if (model.IsActive == true)
                classTeachers = classTeachers.Where(p => p.EndDate == null || p.EndDate >= DateTime.Now.Date).ToList();

            var ClsTeach = classTeachers.Select(x =>
            {
                InchargeGridModel inchargeGridModel = new InchargeGridModel();
                if (x.EffectiveDate >= DateTime.Now.Date)
                    inchargeGridModel.IsInchargeDeletable = true;
                else
                    inchargeGridModel.IsInchargeDeletable = false;
                inchargeGridModel.ClassId = x.ClassId;
                inchargeGridModel.TeacherId = x.TeacherId;
                inchargeGridModel.ClassTeacherId = x.ClassTeacherId;
                inchargeGridModel.Class = x.ClassMaster.Class;
                inchargeGridModel.Teacher = x.Staff.FName + " " + x.Staff.LName;
                inchargeGridModel.EffectiveDate = (DateTime)x.EffectiveDate;
                inchargeGridModel.EndDate = x.EndDate;
                return inchargeGridModel;
            }).AsQueryable().Sort(sort);
            var gridModel = new DataSourceResult
            {
                Data = ClsTeach.PagedForCommand(command).ToList(),
                Total = ClsTeach.Count()
            };
            return Json(gridModel);
        }

        public ActionResult GetClassTeachersStandardWise()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                string gridHtml = "";
                int count = 0;
                var classTeachers = new ClassTeacher();
                List<SelectListItem> standardList = new List<SelectListItem>();
                List<SelectListItem> HeaderCLassList = new List<SelectListItem>();
                List<SelectListItem> HeaderClassSectionList = new List<SelectListItem>();
                List<SelectListItem> ClassSectionListAccStandard = new List<SelectListItem>();
                List<InchargeGridModel> VList = new List<InchargeGridModel>();

                gridHtml += "<table id='tblClassTeaStandard' class='table'> ";
                gridHtml += "<thead><tr>";
                gridHtml += "<th class='k-header' hidden> StandardId </th>";
                gridHtml += "<th class='k-header'> Standard </th>";

                var AllClasses = _ICatalogMasterService.GetAllClassMasters(ref count).OrderBy(x => x.Section.Section1).ToList();
                foreach (var Classection in AllClasses)
                {
                    var ExistsSection = HeaderClassSectionList.Where(x => x.Value == Classection.Section.SectionId.ToString()).FirstOrDefault();
                    if (ExistsSection == null)
                    {
                        gridHtml += "<th class='k-header' > " + Classection.Section.Section1 + " </th>";
                        HeaderClassSectionList.Add(new SelectListItem
                        {
                            Text = Classection.Section.Section1,
                            Value = Classection.Section.SectionId.ToString(),
                            Selected = false
                        });
                    }
                }
                gridHtml += "</tr></thead>";
                gridHtml += "<tbody>";
                var AllStandards = _ICatalogMasterService.GetAllStandardMasters(ref count);
                foreach (var standrd in AllStandards)
                {
                    gridHtml += "<tr>";
                    gridHtml += "<td hidden>" + standrd.StandardId.ToString() + "</td>";

                    var ExistsStandrd = standardList.Where(x => x.Value == standrd.StandardId.ToString()).FirstOrDefault();
                    if (ExistsStandrd == null)
                    {
                        standardList.Add(new SelectListItem
                        {
                            Text = standrd.Standard,
                            Value = standrd.StandardId.ToString(),
                            Selected = false
                        });
                    }
                    int counter = 0;
                    var CLassSections = _ICatalogMasterService.GetAllSections(ref count).OrderBy(x => x.Section1);
                    foreach (var clss in CLassSections)
                    {
                        #region
                        counter++;
                        var classes = _ICatalogMasterService.GetAllClassMasters(ref count, StandardId: standrd.StandardId, SectionId: clss.SectionId).FirstOrDefault();
                        if (classes != null)
                        {
                            classTeachers = _IStaffService.GetAllClassTeachers(ref count, ClassId: classes.ClassId)
                                                .OrderByDescending(p => p.ClassTeacherId)
                                                .Where(p => p.EndDate == null || p.EndDate >= DateTime.Now.Date).FirstOrDefault();
                            if (classTeachers != null)
                            {

                                string EffDate = "";
                                if (classTeachers.EffectiveDate != null)
                                    EffDate = classTeachers.EffectiveDate.Value.Date.ToString("dd/MM/yyyy");

                                string EndDate = "";
                                if (classTeachers.EndDate != null)
                                    EndDate = classTeachers.EndDate.Value.Date.ToString("dd/MM/yyyy");

                                if (counter == 1)
                                    gridHtml += "<td>" + standrd.Standard + "</td>"; //standard

                                string staffname = "";
                                if (!string.IsNullOrEmpty(classTeachers.Staff.FName))
                                    classTeachers.Staff.FName = classTeachers.Staff.FName.Trim();
                                if (!string.IsNullOrEmpty(classTeachers.Staff.LName))
                                    classTeachers.Staff.LName = classTeachers.Staff.LName.Trim();

                                staffname = classTeachers.Staff.FName + " " + classTeachers.Staff.LName;


                                gridHtml += "<td><label title='Edit ClassIncharge' class='ClassteacherEdit' data-teacherId='" + classTeachers.TeacherId
                                                        + "' data-effDate='" + EffDate
                                                        + "' data-endDate='" + EndDate
                                                        + "' data-ClassTeacherId='" + classTeachers.ClassTeacherId
                                                        + "' data-ClassId='" + classTeachers.ClassId
                                                        + "' data-Class='" + classTeachers.ClassMaster.Class + "'>" + staffname + "</label></td>";
                            }
                            else
                            {
                                if (counter == 1)
                                {
                                    gridHtml += "<td>" + standrd.Standard + "</td>"; //standard
                                    gridHtml += "<td><label title='Add ClassIncharge' class='ClassteacherEdit' data-teacherId='' data-effDate='' data-endDate='' ";
                                    gridHtml += " data-ClassId='" + classes.ClassId + "' data-Class='" + classes.Class + "' data-ClassTeacherId=''>";
                                    gridHtml += "...</label></td>";
                                }
                                else
                                {
                                    gridHtml += "<td><label title='Add ClassIncharge' class='ClassteacherEdit' data-teacherId='' data-effDate='' data-endDate='' ";
                                    gridHtml += " data-ClassId='" + classes.ClassId + "' data-Class='" + classes.Class + "' data-ClassTeacherId=''>";
                                    gridHtml += "...</label></td>";
                                }
                            }
                        }
                        else
                        {
                            if (counter == 1)
                            {
                                gridHtml += "<td>" + standrd.Standard + "</td>"; //standard
                                gridHtml += "<td></td>";
                            }
                            else
                                gridHtml += "<td></td>";
                        }

                        #endregion
                    }
                    gridHtml += "</tr>";
                }
                gridHtml += "</tbody></table>";

                return Json(new
                {
                    gridHtml
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult CheckValidation(ManageClassIncharge InchargeData)
        {
            var currentsession = _ICatalogMasterService.GetCurrentSession();

            var InchargeFutureEntries = _ISchoolSettingService.GetAttributeValue("InchargeFutureEntries");
            var IsMultipleClassIncharge = _ISchoolSettingService.GetAttributeValue("IsMultipleClassIncharge");
            if (InchargeFutureEntries != "1")
                InchargeFutureEntries = "0";
            if (IsMultipleClassIncharge != "1")
                IsMultipleClassIncharge = "0";
            if (InchargeData.ClassTeacherId == null)
            {
                if ((IsMultipleClassIncharge == "1" || IsMultipleClassIncharge == "0") && InchargeFutureEntries == "0")
                {
                    var classTeacherExist = _IStaffService.GetAllClassTeachers(ref count, TeacherId: InchargeData.TeacherId).ToList();
                    classTeacherExist = classTeacherExist.Where(p => p.EndDate == null || (p.EndDate != null
                                                        && p.EndDate >= DateTime.Now.Date)).OrderByDescending(x => x.ClassTeacherId).ToList();
                    if (classTeacherExist.Count > 0 && IsMultipleClassIncharge == "0")
                        return Json(new { status = "failed", data = "Teacher is restricted to be incharge of multiple classes." });
                    else
                        return Json(new { status = "success" });
                }
                if (InchargeFutureEntries == "1")
                {
                    var classTeacherExist = _IStaffService.GetAllClassTeachers(ref count, TeacherId: InchargeData.TeacherId).ToList();
                    classTeacherExist = classTeacherExist.Where(p => (p.EndDate == null || p.EndDate != null)
                                                    && p.EndDate >= DateTime.Now.Date).OrderByDescending(x => x.ClassTeacherId).ToList();
                    var classExist = _IStaffService.GetAllClassTeachers(ref count, ClassId: InchargeData.ClassId).ToList();
                    var classExistWithNullEndDate = classExist.Where(p => p.EndDate == null).ToList();
                    var classExistWithFutureEndDate = classExist.Where(p => p.EndDate >= DateTime.Now.Date).FirstOrDefault();
                    if (classExist.Count > 0)
                    {
                        var cls = classExist.Where(x => x.EndDate != null && x.EndDate >= DateTime.Now.Date).FirstOrDefault();
                        if (cls != null)
                        {
                            if (classExist.Any(p => p.TeacherId == InchargeData.TeacherId))
                            {
                                return Json(new { status = "failed", data = "Teacher is already assigned as a incharge of a class." });
                                //return Json(new { status = "failed", data = "Teacher is already incharge of this class." });
                            }
                        }
                    }
                    if (IsMultipleClassIncharge == "0")
                    {
                        if (classTeacherExist.Count > 0)
                            return Json(new { status = "failed", data = "Teacher is restricted to be Incharge of multiple classes." });
                    }
                    else
                    {

                        if (classExistWithNullEndDate.Count > 0)
                        {
                            return Json(new
                            {
                                status = "failed",
                                data = "To change class incharge first set the End Date of current incharge and then add incharge."
                            });
                        }
                        if (classExistWithFutureEndDate != null)
                        {
                            if (classTeacherExist.Count > 0)
                            {
                                //string startDate = "";
                                //if (classTeacherExist.FirstOrDefault().EndDate <= DateTime.Now.Date.Date)
                                //    startDate = Convert.ToDateTime(DateTime.Now.Date.Date).ToString("MM/dd/yyyy");
                                //else
                                //    startDate = Convert.ToDateTime(classTeacherExist.FirstOrDefault().EndDate).AddDays(1).ToString("MM/dd/yyyy");

                                return Json(new
                                {
                                    status = "success",
                                    startdate = Convert.ToDateTime(currentsession.StartDate).ToString("MM/dd/yyyy"),
                                    enddate = Convert.ToDateTime(currentsession.EndDate).ToString("MM/dd/yyyy")//session end date
                                });
                            }
                            else
                            {

                                string startDate = "";
                                if (currentsession.StartDate.Value.Date <= DateTime.Now.Date.Date)
                                    startDate = Convert.ToDateTime(DateTime.Now.Date.Date).ToString("MM/dd/yyyy");
                                else
                                    startDate = Convert.ToDateTime(currentsession.StartDate).ToString("MM/dd/yyyy");


                                return Json(new
                                {
                                    status = "success",
                                    startdate = Convert.ToDateTime(currentsession.StartDate).ToString("MM/dd/yyyy"),
                                    enddate = Convert.ToDateTime(currentsession.EndDate).ToString("MM/dd/yyyy")
                                });
                            }
                        }
                        if (classExistWithNullEndDate.Count == 0 && classExistWithFutureEndDate == null)
                        {
                            if (classTeacherExist.Count > 0)
                            {
                                string startDate = "";
                                if (classTeacherExist.FirstOrDefault().EndDate <= DateTime.Now.Date.Date)
                                    startDate = Convert.ToDateTime(DateTime.Now.Date.Date).ToString("MM/dd/yyyy");
                                else
                                    startDate = Convert.ToDateTime(classTeacherExist.FirstOrDefault().EndDate).AddDays(1).ToString("MM/dd/yyyy");

                                return Json(new
                                {
                                    status = "success",
                                    startdate = Convert.ToDateTime(currentsession.StartDate).ToString("MM/dd/yyyy"),
                                    enddate = Convert.ToDateTime(currentsession.EndDate).ToString("MM/dd/yyyy")
                                });
                            }
                            else
                            {
                                string startDate = "";
                                if (currentsession.StartDate.Value.Date <= DateTime.Now.Date.Date)
                                    startDate = Convert.ToDateTime(DateTime.Now.Date.Date).ToString("MM/dd/yyyy");
                                else
                                    startDate = Convert.ToDateTime(currentsession.StartDate).ToString("MM/dd/yyyy");


                                return Json(new
                                {
                                    status = "success",
                                    startdate = Convert.ToDateTime(currentsession.StartDate).ToString("MM/dd/yyyy"),
                                    enddate = Convert.ToDateTime(currentsession.EndDate).ToString("MM/dd/yyyy")
                                });
                            }
                        }
                    }
                }
            }
            else
            {
                var ClassTeacher = _IStaffService.GetClassTeacherById((int)InchargeData.ClassTeacherId, true);
                if (ClassTeacher.TeacherId == InchargeData.TeacherId)//edit classincharge
                {
                    return Json(new
                    {
                        status = "success",
                        //startdate = Convert.ToDateTime(ClassTeacher.EffectiveDate).ToString("MM/dd/yyyy"),
                        //enddate = Convert.ToDateTime(currentsession.EndDate).ToString("MM/dd/yyyy") //session end date});
                    });
                }
                else
                {
                    if (InchargeFutureEntries == "0" && (IsMultipleClassIncharge == "0" || IsMultipleClassIncharge == "1"))
                    {
                        var classTeacherExist = _IStaffService.GetAllClassTeachers(ref count, TeacherId: InchargeData.TeacherId).ToList();
                        classTeacherExist = classTeacherExist.Where(p => (p.EndDate == null || p.EndDate != null)
                                                    && p.EndDate >= DateTime.Now.Date).OrderByDescending(x => x.ClassTeacherId).ToList();
                        if (classTeacherExist.Count > 0 && IsMultipleClassIncharge == "0")
                            return Json(new { status = "failed", data = "Teacher is restricted to be incharge of multiple classes." });
                        else
                            return Json(new { status = "success" });
                    }
                    if (InchargeFutureEntries == "1")
                    {
                        var classTeacherExist = _IStaffService.GetAllClassTeachers(ref count, TeacherId: InchargeData.TeacherId).ToList();
                        classTeacherExist = classTeacherExist.Where(p => (p.EndDate == null || p.EndDate != null)
                                                            && p.EndDate >= DateTime.Now.Date).OrderByDescending(x => x.ClassTeacherId).ToList();
                        var classExist = _IStaffService.GetAllClassTeachers(ref count, ClassId: InchargeData.ClassId).ToList();
                        var classExistWithNullEndDate = classExist.Where(p => p.EndDate == null).ToList();
                        var classExistWithFutureEndDate = classExist.Where(p => p.EndDate >= DateTime.Now.Date).FirstOrDefault();
                        if (IsMultipleClassIncharge == "0")
                        {
                            if (classTeacherExist.Count > 0)
                                return Json(new { status = "failed", data = "Teacher is restricted to be Incharge of multiple classes." });
                        }
                        else
                        {
                            //if (classExistWithNullEndDate.Count > 0)
                            //    return Json(new { status = "failed", data = "To change class incharge first set the End Date of current incharge and then add incharge." });
                            if (classExistWithFutureEndDate != null)
                            {
                                if (classTeacherExist.Count > 0)
                                {
                                    string startDate = "";
                                    if (classTeacherExist.FirstOrDefault().EndDate <= DateTime.Now.Date.Date)
                                        startDate = Convert.ToDateTime(DateTime.Now.Date.Date).ToString("MM/dd/yyyy");
                                    else
                                        startDate = Convert.ToDateTime(classTeacherExist.FirstOrDefault().EndDate).AddDays(1).ToString("MM/dd/yyyy");

                                    return Json(new
                                    {
                                        status = "success",
                                        startdate = Convert.ToDateTime(currentsession.StartDate).ToString("MM/dd/yyyy"),
                                        enddate = Convert.ToDateTime(currentsession.EndDate).ToString("MM/dd/yyyy") //session end date
                                    });
                                }
                                else
                                {
                                    string startDate = "";
                                    if (currentsession.StartDate.Value.Date <= DateTime.Now.Date.Date)
                                        startDate = Convert.ToDateTime(DateTime.Now.Date.Date).ToString("MM/dd/yyyy");
                                    else
                                        startDate = Convert.ToDateTime(currentsession.StartDate).ToString("MM/dd/yyyy");


                                    return Json(new
                                    {
                                        status = "success",
                                        startdate = Convert.ToDateTime(currentsession.StartDate).ToString("MM/dd/yyyy"),
                                        enddate = Convert.ToDateTime(currentsession.EndDate).ToString("MM/dd/yyyy")
                                    });
                                }
                            }
                            if (classExistWithNullEndDate.Count == 0 && classExistWithFutureEndDate == null)
                            {
                                if (classTeacherExist.Count > 0)
                                {
                                    string startDate = "";
                                    if (classTeacherExist.FirstOrDefault().EndDate <= DateTime.Now.Date.Date)
                                        startDate = Convert.ToDateTime(DateTime.Now.Date.Date).ToString("MM/dd/yyyy");
                                    else
                                        startDate = Convert.ToDateTime(classTeacherExist.FirstOrDefault().EndDate).AddDays(1).ToString("MM/dd/yyyy");

                                    return Json(new
                                    {
                                        status = "success",
                                        startdate = Convert.ToDateTime(currentsession.StartDate).ToString("MM/dd/yyyy"),
                                        enddate = Convert.ToDateTime(currentsession.EndDate).ToString("MM/dd/yyyy")
                                    });
                                }
                                else
                                {
                                    string startDate = "";
                                    if (currentsession.StartDate.Value.Date <= DateTime.Now.Date.Date)
                                        startDate = Convert.ToDateTime(DateTime.Now.Date.Date).ToString("MM/dd/yyyy");
                                    else
                                        startDate = Convert.ToDateTime(currentsession.StartDate).ToString("MM/dd/yyyy");

                                    return Json(new
                                    {
                                        status = "success",
                                        startdate = Convert.ToDateTime(currentsession.StartDate).ToString("MM/dd/yyyy"),
                                        enddate = Convert.ToDateTime(currentsession.EndDate).ToString("MM/dd/yyyy")
                                    });
                                }
                            }
                        }
                    }
                }
            }
            return Json(new { status = "success" });
        }

        #endregion

        #region leaveapproval

        public ActionResult LeaveApproval()
        {
            SchoolUser user = new SchoolUser();
            var model = new LeaveApprovalModel();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);

                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "LeaveApproval", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");


                model.AvailableSendMessageMobileNoOptions.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
                model.AvailableSendMessageMobileNoOptions.Add(new SelectListItem { Text = "Student SMS Mobile No.", Value = "SMS", Selected = false });
                model.AvailableSendMessageMobileNoOptions.Add(new SelectListItem { Text = "Student Mobile No.", Value = "SMN", Selected = false });
                model.AvailableSendMessageMobileNoOptions.Add(new SelectListItem { Text = "Guardian Mobile No.", Value = "GMN", Selected = false });
                model.SMSSelectionStudentMobileNo = _ISchoolSettingService.GetorSetSchoolData("LeaveApprovalSmsMobileNoSelectionForStudent", "SMN", null,
                                            "SMS : Student SMS Mobile No , SMN: Student mobile no. , GMN : Guardian Mobile No")
                                            .AttributeValue;

                if (user.ContactType.ContactType1.ToLower() == "teacher")
                {
                    model.LoggedInUser = "teacher";
                    var isClassIncharge = _IStaffService.GetAllClassTeachers(ref count).Any(p => p.TeacherId == user.UserContactId && (p.EndDate >= DateTime.UtcNow.Date || p.EndDate == null));
                    if (!isClassIncharge)
                        return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    model.LoggedInUser = "admin";
                    model.IsAdmin = true;
                    var DefaultSettings = _ICommonMethodService.GetOrSetSMSDefaultSetting("LeaveApply/Approval", true, false, true, "StaffMobileNumber", "");
                    model.SMS = (bool)DefaultSettings.SMS;
                    model.Notification = (bool)DefaultSettings.Notification;
                    model.NonAppUser = (bool)DefaultSettings.ToNonAppUser;


                    var StuDefaultSettings = _ICommonMethodService.GetOrSetSMSDefaultSetting("LeaveApply/ApprovalStudent", true, false, true, model.SMSSelectionStudentMobileNo, "");
                    model.StuSMS = (bool)DefaultSettings.SMS;
                    model.StuNotification = (bool)DefaultSettings.Notification;
                    model.StuNonAppUser = (bool)DefaultSettings.ToNonAppUser;
                }
            }

            model.LeaveApplyFromTime = _ISchoolSettingService.GetorSetSchoolData("LeaveApplyAllowedTimeFrom", "23:59", null,
                                                       "Apply leave from time limit").AttributeValue;
            model.LeaveApplyToTime = _ISchoolSettingService.GetorSetSchoolData("LeaveApplyAllowedToTime", "19:00", null,
                                              "Apply leave To time limit").AttributeValue;


            model.IsAuthApprove = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "LeaveApproval", "Approve");
            model.IsAuthDisapprove = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "LeaveApproval", "Disapprove");

            //values for kendo paging
            model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
            model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");

            model.AviableLeaveStatus.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            model.AviableLeaveStatus.Add(new SelectListItem { Text = "Pending", Value = "Pending", Selected = false });
            model.AviableLeaveStatus.Add(new SelectListItem { Text = "Approved", Value = "Approved", Selected = false });
            model.AviableLeaveStatus.Add(new SelectListItem { Text = "Disapproved", Value = "Disapproved", Selected = false });
            model.LeaveStatus = "Pending";
            model.AviableDepartments = _IDropDownService.DepartmentList();
            var getLeaveTypes = _IStaffService.GetStaffAllAttendanceStatus(ref count).Where(x => x.IsLeave == true).ToList();
            model.AviableLeaveTypes.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var leavetype in getLeaveTypes)
            {
                model.AviableLeaveTypes.Add(new SelectListItem
                {
                    Text = leavetype.AttendanceStatus,
                    Value = _ICustomEncryption.base64e(leavetype.AttendanceStatusId.ToString()),
                    Selected = false
                });
            }

            model.AviableStaffList.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            var teachingStaff = _IStaffService.GetAllStaffs(ref count, IsActive: true).Where(x => x.StaffType.IsTeacher == true).ToList();
            foreach (var staff in teachingStaff)
            {
                string staffName = "";
                if (!string.IsNullOrEmpty(staff.FName))
                    staff.FName = staff.FName.Trim();

                if (!string.IsNullOrEmpty(staff.LName))
                    staff.LName = staff.LName.Trim();

                staffName = staff.FName + " " + staff.LName;
                model.AviableStaffList.Add(new SelectListItem
                {
                    Text = staffName,
                    Value = _ICustomEncryption.base64e(staff.StaffId.ToString()),
                    Selected = false
                });
            }

            model.AviableStaffDesignationList.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            var teachingStaffDesgination = _IStaffService.GetAllDesignations(ref count).Where(x => x.StaffType.IsTeacher == true).ToList();
            foreach (var desig in teachingStaffDesgination)
            {
                model.AviableStaffDesignationList.Add(new SelectListItem
                {
                    Text = desig.Designation1,
                    Value = _ICustomEncryption.base64e(desig.DesignationId.ToString()),
                    Selected = false
                });
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult LeaveApproval(DataSourceRequest command, LeaveApprovalModel model, IEnumerable<Sort> sort = null)
        {

            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);

                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "LeaveApproval", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");

            }

            // decrypt values
            int LeaveType_id = 0;
            int? EncLeaveTypeId = null;
            if (!string.IsNullOrEmpty(model.LeaveTypeId))
            {
                int.TryParse(_ICustomEncryption.base64d(model.LeaveTypeId), out LeaveType_id);
                EncLeaveTypeId = LeaveType_id;
            }

            DateTime? FromDate = null;
            if (!string.IsNullOrEmpty(model.FromAppiledDate) || !string.IsNullOrWhiteSpace(model.FromAppiledDate))
            {
                FromDate = DateTime.ParseExact(model.FromAppiledDate, "dd/MM/yyyy", null);
                FromDate = FromDate.Value.Date;
            }

            DateTime? ToDate = null;
            if (!string.IsNullOrEmpty(model.ToAppiledDate) || !string.IsNullOrWhiteSpace(model.ToAppiledDate))
            {
                ToDate = DateTime.ParseExact(model.ToAppiledDate, "dd/MM/yyyy", null);
                ToDate = ToDate.Value.Date;
            }

            bool? StatusApproved = null;
            bool checkApprovedByCondition = false;
            if (!string.IsNullOrEmpty(model.LeaveStatus))
            {
                switch (model.LeaveStatus.ToLower())
                {
                    case "pending":
                        checkApprovedByCondition = true;
                        break;

                    case "approved":
                        StatusApproved = true;
                        break;

                    case "disapproved":
                        StatusApproved = false;
                        break;
                }
            }

            int StaffID = 0;
            int? encStaffId = null;
            if (!string.IsNullOrEmpty(model.StaffId))
            {
                int.TryParse(_ICustomEncryption.base64d(model.StaffId), out StaffID);
                encStaffId = StaffID;
            }

            int Designation_Id = 0;
            if (!string.IsNullOrEmpty(model.DesginationId))
                int.TryParse(_ICustomEncryption.base64d(model.DesginationId), out Designation_Id);

            int Department_Id = 0;
            if (!string.IsNullOrEmpty(model.DepartmentId))
                Department_Id = Convert.ToInt32(model.DepartmentId);

            var currentSession = _ICatalogMasterService.GetCurrentSession();
            if (user.ContactType.ContactType1.ToLower() == "admin")
            {
                var staffs = _IStaffService.GetAllStaffs(ref count).Where(p => p.IsActive == true).ToList();
                if (staffs != null)
                {
                    var staffIds = staffs.Select(p => p.StaffId).ToList();
                    var staffLeaveList = _IStaffService.GetAllStaffLeaveApplication(ref count, LeaveTypeId: EncLeaveTypeId,
                                                         StaffId: encStaffId).Where(p => staffIds.Contains((int)p.StaffId))
                                                                .OrderByDescending(m => m.AppliedDate).ToList();

                    if (checkApprovedByCondition)
                        staffLeaveList = staffLeaveList.Where(x => x.IsApproved == null).ToList(); //case of pending filter selection
                    else if (StatusApproved != null)
                        staffLeaveList = staffLeaveList.Where(x => x.IsApproved == StatusApproved).ToList();


                    if (!string.IsNullOrEmpty(model.FilterDateBy))
                    {
                        switch (model.FilterDateBy.ToLower())
                        {
                            case "1"://filter on Appiled Date

                                if (FromDate != null)
                                    staffLeaveList = staffLeaveList.Where(x => FromDate <= x.AppliedDate.Value.Date).ToList();
                                if (ToDate != null)
                                    staffLeaveList = staffLeaveList.Where(x => ToDate >= x.AppliedDate.Value.Date).ToList();
                                break;

                            case "2"://filter on Leave Dates

                                if (FromDate != null)
                                    staffLeaveList = staffLeaveList.Where(x => x.LeaveFrom >= FromDate).ToList();
                                if (ToDate != null)
                                    staffLeaveList = staffLeaveList.Where(x => x.LeaveTill <= ToDate).ToList();
                                break;

                            case "3"://filter on Response Date
                                if (!checkApprovedByCondition)
                                {
                                    if (FromDate != null)
                                        staffLeaveList = staffLeaveList.Where(x => x.ResponseDate >= FromDate).ToList();
                                    if (ToDate != null)
                                        staffLeaveList = staffLeaveList.Where(x => x.ResponseDate <= ToDate).ToList();
                                }
                                break;
                        }
                    }

                    if (Designation_Id > 0)
                        staffLeaveList = staffLeaveList.Where(x => x.Staff.DesignationId == Designation_Id).ToList();

                    if (Department_Id > 0)
                        staffLeaveList = staffLeaveList.Where(x => x.Staff.DepartmentId == Department_Id).ToList();

                    var lvlist = staffLeaveList.Select(x =>
                    {
                        var leaveapproveModel = new LeaveApprovalGridModel();
                        leaveapproveModel.LeaveId = x.StaffLeaveApplicationId;
                        leaveapproveModel.LeaveFrom = x.LeaveFrom;
                        leaveapproveModel.LeaveTill = x.LeaveTill;
                        leaveapproveModel.Name = x.Staff.FName + " " + x.Staff.LName;
                        leaveapproveModel.AppliedDate = x.AppliedDate;
                        leaveapproveModel.AppliedTime = x.AppliedDate.Value.ToString("hh:mm tt");
                        leaveapproveModel.Designation = staffs.Where(p => p.StaffId == x.StaffId).FirstOrDefault().Designation.Designation1;
                        leaveapproveModel.Department = staffs.Where(p => p.StaffId == x.StaffId).FirstOrDefault().Department.Department1;
                        leaveapproveModel.ReasonDesc = x.Reason;
                        if (x.Reason.Length > 10)
                            leaveapproveModel.Reason = x.Reason.Substring(0, 9);
                        else
                            leaveapproveModel.Reason = x.Reason;

                        if (x.ApprovedBy != null)
                        {
                            if ((bool)x.IsApproved)
                                leaveapproveModel.Status = "Approved";
                            else
                                leaveapproveModel.Status = "Disapproved";
                        }

                        var attendancestatus = _IStaffService.GetStaffAttendanceStatusById((int)x.LeaveTypeId);
                        if (attendancestatus != null)
                            leaveapproveModel.LeaveType = attendancestatus.AttendanceStatus;
                        leaveapproveModel.Empcode = _IStaffService.GetStaffById((int)x.StaffId).EmpCode;

                        return leaveapproveModel;
                    }).AsQueryable().Sort(sort);
                    var gridModel = new DataSourceResult
                    {
                        Data = lvlist.PagedForCommand(command).ToList(),
                        Total = lvlist.Count()
                    };
                    return Json(gridModel);
                }
            }
            return Json(new { status = "failed" });
        }

        [HttpPost]
        public ActionResult InsertDefaultSettings(LeaveApprovalModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {

                //insert LeaveApply timings
                var LeaveApplyFromTime = _ISchoolSettingService.GetSchoolSettingByAttribute("LeaveApplyAllowedTimeFrom");
                if (LeaveApplyFromTime != null)
                {
                    LeaveApplyFromTime.AttributeValue = model.LeaveApplyFromTime;
                    _ISchoolSettingService.UpdateSchoolSetting(LeaveApplyFromTime);
                }
                var LeaveApplyToTime = _ISchoolSettingService.GetSchoolSettingByAttribute("LeaveApplyAllowedToTime");
                if (LeaveApplyToTime != null)
                {
                    LeaveApplyToTime.AttributeValue = model.LeaveApplyToTime;
                    _ISchoolSettingService.UpdateSchoolSetting(LeaveApplyToTime);
                }

                //insert attendance SMs for TEacher settings
                var SMSDefaultSetting = _ICommonMethodService.GetOrSetSMSDefaultSetting("LeaveApply/Approval", true, false, true, "StaffMobileNumber", "");
                if (SMSDefaultSetting.SMSDefaultSettingsId != null)
                {
                    SMSDefaultSetting.FormKey = "LeaveApply/Approval";
                    SMSDefaultSetting.Notification = (bool)model.Notification;
                    SMSDefaultSetting.SMS = (bool)model.SMS;
                    SMSDefaultSetting.ToNonAppUser = (bool)model.NonAppUser;

                    _ICommonMethodService.UpdateSMSDefaultSetting(SMSDefaultSetting);
                }

                //insert attendance SMs students settings
                var SMSDefaultSettingstu = _ICommonMethodService.GetOrSetSMSDefaultSetting("LeaveApply/ApprovalStudent", true, false, true, "SMN", "");
                if (SMSDefaultSettingstu.SMSDefaultSettingsId != null)
                {
                    SMSDefaultSettingstu.FormKey = "LeaveApply/ApprovalStudent";
                    SMSDefaultSettingstu.Notification = (bool)model.Notification;
                    SMSDefaultSettingstu.SMS = (bool)model.SMS;
                    SMSDefaultSettingstu.ToNonAppUser = (bool)model.NonAppUser;
                    SMSDefaultSettingstu.SelectedMobile = model.SMSSelectionStudentMobileNo;

                    _ICommonMethodService.UpdateSMSDefaultSetting(SMSDefaultSettingstu);
                }
                return Json(new
                {
                    status = "Success",
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = "Error happened"
                });
            }
        }

        public ActionResult ApprovalAction(int Id, string status)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);

                // check authorization
                //var isapproveauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "LeaveApproval", "Approve");
                //if (!isapproveauth)
                //    return RedirectToAction("AccessDenied", "Error");

                //// check authorization
                //var isdisapproveauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "LeaveApproval", "DisApprove");
                //if (!isdisapproveauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            try
            {
                if (user.ContactType.ContactType1.ToLower() == "admin")
                {
                    if (Id > 0)
                    {
                        var staffLeave = _IStaffService.GetStaffLeaveApplicationById(StaffLeaveApplicationId: Id);
                        if (staffLeave != null && status == "Approved")
                        {
                            staffLeave.ApprovedBy = user.UserContactId;
                            staffLeave.ResponseDate = DateTime.UtcNow;
                            staffLeave.IsApproved = true;
                            _IStaffService.UpdateStaffLeaveApplication(staffLeave);

                            foreach (DateTime day in _ICommonMethodService.EachDay((DateTime)staffLeave.LeaveFrom, (DateTime)staffLeave.LeaveTill))
                            {
                                var staffAttendance = _IStaffService.GetAllStaffAttendances(ref count).Where(m => m.AttendanceDate == day && m.StaffId == staffLeave.StaffId).FirstOrDefault();
                                //after leave approval staff leave attendance for teacher
                                if (staffAttendance != null)
                                {
                                    staffAttendance.AttendanceStatusId = (int)staffLeave.LeaveTypeId;
                                    staffAttendance.IsAuto = false;
                                    _IStaffService.UpdateStaffAttendance(staffAttendance);
                                }
                                else
                                {
                                    var staffatn = new StaffAttendance();
                                    staffatn.StaffId = staffLeave.StaffId;
                                    staffatn.AttendanceStatusId = (int)staffLeave.LeaveTypeId;
                                    staffatn.AttendanceDate = day;
                                    staffatn.IsAuto = false;
                                    staffatn.IsOutAuto = false;
                                    _IStaffService.InsertStaffAttendance(staffatn);
                                }
                            }
                        }
                        else if (staffLeave != null && status == "Disapproved")
                        {
                            staffLeave.ApprovedBy = user.UserContactId;
                            staffLeave.ResponseDate = DateTime.UtcNow;
                            staffLeave.IsApproved = false;
                            _IStaffService.UpdateStaffLeaveApplication(staffLeave);
                        }

                        //trigger for appove leave application for teachers
                        var DefaultSettings = _ICommonMethodService.GetOrSetSMSDefaultSetting("LeaveApply/Approval", true, false, true, "StaffMobileNumber", "");
                        _ICommonMethodService.TiggerNotificationSMS(TriggerEventName: "LeaveApproval", IsStaff: true,
                                                  SMS: (bool)DefaultSettings.SMS, Notification: (bool)DefaultSettings.Notification,
                                                  IsNonAPPUser: (bool)DefaultSettings.ToNonAppUser,
                                                  LeaveApprovalStatus: status, LeaveFrom: staffLeave.LeaveFrom,
                                                  LeaveTill: staffLeave.LeaveTill, StaffIds: staffLeave.StaffId.ToString());
                        return Json(new { status = "success" });
                    }
                }

                return Json(new { status = "failed" });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }



        #region ExportMethods
        public ActionResult ExportLeaveApprovalData(LeaveApprovalModel model, string IsExcel = "")
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string title = "Staff Leave Approval";
                string Subtitle = "";

                DataTable dt = ExportGridData(model);
                //dt = GetAllKendoGridInformation(CityId, Sellername);
                if (dt.Rows.Count > 0)
                {
                    if (IsExcel == "1")
                    {
                        _IExportHelper.ExportToExcel(dt, Title: title, Subtitle: Subtitle,HeadingName:"Staff Leave Approval");
                    }
                    else
                    {
                        _IExportHelper.ExportToPrint(dt, Title: title, Subtitle: Subtitle);
                    }
                }
            }
            catch (Exception ex)
            { }
            return null;
        }

        public DataTable ExportGridData(LeaveApprovalModel model)
        {
            DataTable dt = new DataTable();
            // decrypt values
            int LeaveType_id = 0;
            int? EncLeaveTypeId = null;
            if (!string.IsNullOrEmpty(model.LeaveTypeId))
            {
                int.TryParse(_ICustomEncryption.base64d(model.LeaveTypeId), out LeaveType_id);
                EncLeaveTypeId = LeaveType_id;
            }

            DateTime? FromDate = null;
            if (!string.IsNullOrEmpty(model.FromAppiledDate) || !string.IsNullOrWhiteSpace(model.FromAppiledDate))
            {
                FromDate = DateTime.ParseExact(model.FromAppiledDate, "dd/MM/yyyy", null);
                FromDate = FromDate.Value.Date;
            }

            DateTime? ToDate = null;
            if (!string.IsNullOrEmpty(model.ToAppiledDate) || !string.IsNullOrWhiteSpace(model.ToAppiledDate))
            {
                ToDate = DateTime.ParseExact(model.ToAppiledDate, "dd/MM/yyyy", null);
                ToDate = ToDate.Value.Date;
            }

            bool? StatusApproved = null;
            bool checkApprovedByCondition = false;
            if (!string.IsNullOrEmpty(model.LeaveStatus))
            {
                switch (model.LeaveStatus.ToLower())
                {
                    case "pending":
                        checkApprovedByCondition = true;
                        break;

                    case "approved":
                        StatusApproved = true;
                        break;

                    case "disapproved":
                        StatusApproved = false;
                        break;
                }
            }

            int StaffID = 0;
            int? encStaffId = null;
            if (!string.IsNullOrEmpty(model.StaffId))
            {
                int.TryParse(_ICustomEncryption.base64d(model.StaffId), out StaffID);
                encStaffId = StaffID;
            }

            int Designation_Id = 0;
            if (!string.IsNullOrEmpty(model.DesginationId))
                int.TryParse(_ICustomEncryption.base64d(model.DesginationId), out Designation_Id);

            int Department_Id = 0;
            if (!string.IsNullOrEmpty(model.DepartmentId))
                Department_Id = Convert.ToInt32(model.DepartmentId);

            var staffs = _IStaffService.GetAllStaffs(ref count).Where(p => p.IsActive == true
                                                                        && p.StaffType.IsTeacher == true).ToList();
            if (staffs != null)
            {
                var staffIds = staffs.Select(p => p.StaffId).ToList();
                var staffLeaveList = _IStaffService.GetAllStaffLeaveApplication(ref count, LeaveTypeId: EncLeaveTypeId,
                                                     StaffId: encStaffId).Where(p => staffIds.Contains((int)p.StaffId))
                                                            .OrderByDescending(m => m.AppliedDate).ToList();

                if (checkApprovedByCondition)
                    staffLeaveList = staffLeaveList.Where(x => x.IsApproved == null).ToList(); //case of pending filter selection
                else if (StatusApproved != null)
                    staffLeaveList = staffLeaveList.Where(x => x.IsApproved == StatusApproved).ToList();


                if (!string.IsNullOrEmpty(model.FilterDateBy))
                {
                    switch (model.FilterDateBy.ToLower())
                    {
                        case "1"://filter on Appiled Date

                            if (FromDate != null)
                                staffLeaveList = staffLeaveList.Where(x => FromDate <= x.AppliedDate.Value.Date).ToList();
                            if (ToDate != null)
                                staffLeaveList = staffLeaveList.Where(x => ToDate >= x.AppliedDate.Value.Date).ToList();
                            break;

                        case "2"://filter on Leave Dates

                            if (FromDate != null)
                                staffLeaveList = staffLeaveList.Where(x => x.LeaveFrom >= FromDate).ToList();
                            if (ToDate != null)
                                staffLeaveList = staffLeaveList.Where(x => x.LeaveTill <= ToDate).ToList();
                            break;

                        case "3"://filter on Response Date
                            if (!checkApprovedByCondition)
                            {
                                if (FromDate != null)
                                    staffLeaveList = staffLeaveList.Where(x => x.ResponseDate >= FromDate).ToList();
                                if (ToDate != null)
                                    staffLeaveList = staffLeaveList.Where(x => x.ResponseDate <= ToDate).ToList();
                            }
                            break;
                    }
                }

                if (Designation_Id > 0)
                    staffLeaveList = staffLeaveList.Where(x => x.Staff.DesignationId == Designation_Id).ToList();

                if (Department_Id > 0)
                    staffLeaveList = staffLeaveList.Where(x => x.Staff.DepartmentId == Department_Id).ToList();


                //create temp table
                dt.Columns.Add("Name");
                dt.Columns.Add("Designation");
                dt.Columns.Add("Department");
                dt.Columns.Add("Leave Type");
                dt.Columns.Add("Leave From");
                dt.Columns.Add("Leave Till");
                dt.Columns.Add("Applied Date");
                dt.Columns.Add("Reason");
                dt.Columns.Add("Status");
                foreach (var leave in staffLeaveList)
                {
                    string staffName = "";

                    if (!string.IsNullOrEmpty(leave.Staff.FName))
                        leave.Staff.FName = leave.Staff.FName.Trim();
                    if (!string.IsNullOrEmpty(leave.Staff.LName))
                        leave.Staff.LName = leave.Staff.LName.Trim();

                    staffName = leave.Staff.FName + " " + leave.Staff.LName;
                    if (!string.IsNullOrEmpty(leave.Staff.EmpCode))
                        staffName = staffName + " (" + leave.Staff.EmpCode + ")";

                    DataRow row = dt.NewRow();
                    row[0] = staffName;
                    row[1] = leave.Staff.Designation.Designation1;
                    row[2] = leave.Staff.Department.Department1;
                    row[3] = leave.StaffAttendanceStatu.AttendanceStatus;
                    row[4] = leave.LeaveFrom.Value.Date.ToString("dd/MM/yyyy");
                    row[5] = leave.LeaveTill.Value.Date.ToString("dd/MM/yyyy");
                    row[6] = leave.AppliedDate.Value.Date.ToString("dd/MM/yyyy");
                    row[7] = !string.IsNullOrEmpty(leave.Reason) ? leave.Reason.Trim() : "";

                    if (leave.IsApproved == null)
                        row[8] = "Pending";
                    else
                    {
                        if ((bool)leave.IsApproved)
                            row[8] = "Approved";
                        else
                            row[8] = "DisApproved";
                    }

                    dt.Rows.Add(row);
                    dt.AcceptChanges();
                }
            }
            return dt;
        }
        #endregion

        #endregion

        #region Gatepass Approval
        public GatePassModel PrePareModelForPass(GatePassModel model, int? GatePassTypeId = null, int? Session_id = null)
        {
            var allGatePassTypes = _IGatePassService.GetAllGatePassType(ref count).ToList();
            model.GatePassTypeList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
            if (allGatePassTypes.Count > 0)
            {
                foreach (var item in allGatePassTypes)
                    model.GatePassTypeList.Add(new SelectListItem { Selected = false, Value = _ICustomEncryption.base64e(item.GatePassTypeId.ToString()), Text = item.GatePassType1 });
            }

            var allVehicleTypes = _ITransportService.GetAllVehicleTypes(ref count).ToList();
            model.VehicleTypeList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
            if (allVehicleTypes.Count > 0)
            {
                foreach (var item in allVehicleTypes)
                    model.VehicleTypeList.Add(new SelectListItem { Selected = false, Value = _ICustomEncryption.base64e(item.VehicleTypeId.ToString()), Text = item.VehicleType1 });
            }

            var allStaffs = _IStaffService.GetAllStaffs(ref count, IsActive: true).OrderBy(p => p.FName).ToList();
            model.StaffList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
            model.ConcernedStaffList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });

            var currentSession = _ICatalogMasterService.GetCurrentSession();
            model.SessionList = _IDropDownService.SessionList();
            model.SessionFilterList = _IDropDownService.SessionList();

            string FirstNumber = "";
            var getLastGatePassEntry = _IGatePassService.GetAllGatePass(ref count).Where(x => x.SerialNo != null)
                                                        .OrderByDescending(x => x.GatePassId).FirstOrDefault();
            FirstNumber = getLastGatePassEntry != null ? getLastGatePassEntry.GatePassId.ToString() : "";

            model.SerialNo = FirstNumber + "001";
            if (getLastGatePassEntry != null)
                model.SerialNo = getLastGatePassEntry.SerialNo != null ? (getLastGatePassEntry.SerialNo + 1).ToString() : (FirstNumber + "001");

            var getSession = new Session();
            if (GatePassTypeId > 0)
            {
                if (allStaffs.Count > 0)
                {
                    foreach (var item in allStaffs)
                        model.StaffList.Add(new SelectListItem { Selected = false, Value = _ICustomEncryption.base64e(item.StaffId.ToString()), Text = item.FName + " " + item.LName + " (" + item.Designation.Designation1 + ")" });
                }
                //var allcontacttypes = _IAddressMasterService.GetAllContactTypes(ref count).ToList();

                //var adminuser = _IUserService.GetAllUsers(ref count, username: "admin").FirstOrDefault();
                //var contacttype = _IAddressMasterService.GetAllContactTypes(ref count).Where(p => p.ContactType1.ToLower() == "admin").FirstOrDefault();

                var gatepassauth = _IGatePassService.GetAllGatePassAuthority(ref count, GatePassTypeId: (int)GatePassTypeId).ToList();
                if (allStaffs.Count > 0)
                {
                    foreach (var item in allStaffs)
                    {
                        if (gatepassauth.Any(p => p.AuthorityId == item.StaffId && p.ContactTypeId == item.ContactTypeId && p.IsActive == true))
                            model.ConcernedStaffList.Add(new SelectListItem { Selected = false, Value = _ICustomEncryption.base64e(item.StaffId.ToString()), Text = item.FName + " " + item.LName + " (" + item.Designation.Designation1 + ")" });
                    }
                }
                //if (contacttype != null)
                //{
                //    if (gatepassauth.Any(p => p.AuthorityId == adminuser.UserContactId && p.ContactTypeId == contacttype.ContactTypeId && p.IsActive == true))
                //        model.ConcernedStaffList.Add(new SelectListItem { Selected = false, Value = _ICustomEncryption.base64e(adminuser.UserContactId.ToString()), Text = adminuser.UserName });
                //}
            }
            else
            {
                getSession = _ICatalogMasterService.GetAllSessions(ref count).Where(x => x.StartDate <= DateTime.UtcNow.Date &&
                                                         x.EndDate >= DateTime.UtcNow.Date).FirstOrDefault();

                if (allStaffs.Count > 0)
                {
                    foreach (var item in allStaffs)
                    {
                        model.StaffList.Add(new SelectListItem { Selected = false, Value = _ICustomEncryption.base64e(item.StaffId.ToString()), Text = item.FName + " " + item.LName + " (" + item.Designation.Designation1 + ")" });
                        model.ConcernedStaffList.Add(new SelectListItem { Selected = false, Value = _ICustomEncryption.base64e(item.StaffId.ToString()), Text = item.FName + " " + item.LName + " (" + item.Designation.Designation1 + ")" });
                    }
                }
            }




            var allstudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: model.SessionId).ToList();
            model.StudentList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });

            if (allstudents.Count > 0 && model.ClassId != null)
            {
                var classid = Convert.ToInt32(model.ClassId);
                var requiredstudents = allstudents.Where(p => p.ClassId == classid).ToList();
                foreach (var item in requiredstudents)
                    model.StudentList.Add(new SelectListItem { Selected = false, Value = item.StudentId.ToString(), Text = item.Student.FName + " " + item.Student.LName + "- (R.No.-" + item.RollNo + ")" });
            }

            model.StudentListForFilter.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
            if (allstudents.Count > 0)
            {
                foreach (var item in allstudents)
                    model.StudentListForFilter.Add(new SelectListItem { Selected = false, Value = item.StudentId.ToString(), Text = item.Student.FName + " " + item.Student.LName });
            }

            //show hide the visitor info in gatepass
            var isvisitorinfoon = _ISchoolSettingService.GetAttributeValue("ShowVehicleInfo");
            if (isvisitorinfoon == "True")
                model.IsVisitorInfoOn = true;

            //show hide the id proof in gatepass
            var isproofon = _ISchoolSettingService.GetAttributeValue("ShowIdProof");
            if (isproofon == "True")
                model.IsIdProofOn = true;


            //show hide the pic while print
            var isviewpic = _ISchoolSettingService.GetAttributeValue("ViewPhotoInPrint");
            if (isviewpic == "True")
                model.IsShowPic = true;

            var schoologo = _ISchoolDataService.GetSchoolDataDetail(ref count).FirstOrDefault();
            if (schoologo != null)
            {
                var schoolid = HttpContext.Session["SchoolId"];
                model.Schoollogo = _WebHelper.GetStoreLocation() + "Images/" + schoolid + "/" + schoologo.Logo;
                model.SchoolName = schoologo.SchoolName;
            }
            else
            {
                model.Schoollogo = "";
                model.SchoolName = "";
            }

            model.ClassList = _IDropDownService.ClassList();

            model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
            model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");

            var IsIssueDateDisabled = _ISchoolSettingService.GetAttributeValue(AttributeName: "IssueDate");
            if (IsIssueDateDisabled == "True")
                model.IsIssueDateDisabled = true;

            var crrntssn = _ICatalogMasterService.GetAllSessions(ref count).Where(f => f.IsDefualt == true).FirstOrDefault();
            if (crrntssn != null)
            {
                model.CurrentSessionId = crrntssn.SessionId;
            }
            //Session_id = getSession != null ? getSession.SessionId : Session_id;
            if (Session_id > 0)
                model.SessionId = Session_id;
            else
            {
                if (crrntssn != null) {
                    model.SessionId = crrntssn.SessionId;
                }
            }
            return model;
        }

        public ActionResult GatePassApproval()
        {
            SchoolUser user = new SchoolUser();
            var model = new GatePassModel();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }


            model = PrePareModelForPass(model);
            //values for kendo paging
            model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
            model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");



            return View(model);
        }

        #endregion

        #region StaffAttendance

        public ActionResult StaffAttendance()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffAttendance", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            var model = new StaffAttendanceModel();
            var allStaffAttendanceStatus = _IStaffService.GetStaffAllAttendanceStatus(ref count);
            string ChangedText = _ISchoolSettingService.GetorSetSchoolData("TextForNotMarkedAttendance", "UnMarked").AttributeValue;
            model.StatusList.Add(new SelectListItem { Text = ChangedText, Value = "UM" });
            foreach (var item in allStaffAttendanceStatus)
                model.StatusList.Add(new SelectListItem { Selected = false, Text = item.AttendanceStatus, Value = item.AttendanceStatusId.ToString() });

            model.date = ConvertDate(DateTime.Now.Date);
            if (TempData["UpdatedStaffAttendanceDate"] != null && TempData["UpdatedStaffAttendanceDate"].ToString() != "")
            {
                var tempdate = TempData["UpdatedStaffAttendanceDate"].ToString();
                model.date = tempdate;
            }
            TempData["UpdatedStaffAttendanceDate"] = "";


            model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffAttendance", "Add");


            TimeSpan? DefaultInTime = null;
            var getStaffInTime = _ISchoolSettingService.GetorSetSchoolData("StaffInTime", "8:00");
            if (getStaffInTime != null)
                DefaultInTime = _ICommonMethodService.ConvertStringToTimeSpan(getStaffInTime.AttributeValue);

            TimeSpan? DefaultOutTime = null;
            var getStaffOutTime = _ISchoolSettingService.GetorSetSchoolData("StaffOutTime", "14:30");
            if (getStaffOutTime != null)
                DefaultOutTime = _ICommonMethodService.ConvertStringToTimeSpan(getStaffOutTime.AttributeValue);

            model.IntTime = DefaultInTime.Value.ToString(@"hh\:mm");
            model.OutTime = DefaultOutTime.Value.ToString(@"hh\:mm");
            model.TextForUnMarkedAtt = _ISchoolSettingService.GetorSetSchoolData("TextForNotMarkedAttendance", "UnMarked")
                                                            .AttributeValue;

            model.HasCountAttStatus = "";
            var ListHasAttStatus = _IStaffService.GetStaffAllAttendanceStatus(ref count).Where(x => x.AttendanceCount > 0).ToList();
            foreach (var att in ListHasAttStatus)
            {
                model.HasCountAttStatus += att.AttendanceStatus + ",";
            }
            model.HasCountAttStatus = model.HasCountAttStatus.Trim(',');

            var allStaffStatus = _IStaffService.GetStaffAllAttendanceStatus(ref count);
            model.StaffAttendanceStatusList.Add(new SelectListItem
            {
                Selected = false,
                Text = "--Select--",
                Value = ""
            });
            model.StaffAttendanceStatusList.Add(new SelectListItem
            {
                Selected = false,
                Text = model.TextForUnMarkedAtt,
                Value = "UM"
            });
            foreach (var item in allStaffStatus)
            {
                model.StaffAttendanceStatusList.Add(new SelectListItem
                {
                    Selected = false,
                    Text = item.AttendanceStatus,
                    Value = item.AttendanceStatusId.ToString()
                });
            }
            model.StaffAttendanceStatusCloseList = model.StaffAttendanceStatusList;
            model.StaffAttendanceStatusCloseId = "";

            model.DefaultAttendanceStatusId = "UM";
            var getStatusid = _ISchoolSettingService.GetorSetSchoolData("StaffDefaultAttendanceStatus", model.TextForUnMarkedAtt);
            if (getStatusid != null)
            {
                var AttendanceStatus = _ISubjectService.GetAllAttendanceStatus(ref count,
                                                  AttnStatus: getStatusid.AttributeValue.ToLower()).FirstOrDefault();
                model.DefaultAttendanceStatusId = AttendanceStatus != null ?
                                                  AttendanceStatus.AttendanceStatusId.ToString() : "UM";
            }
            if (!string.IsNullOrEmpty(model.DefaultAttendanceStatusId))
                model.StaffAttendanceStatusId = model.DefaultAttendanceStatusId;


            var DefaultAttenCloseTime = _ISchoolSettingService.GetorSetSchoolData("StaffAttendanceCloseTime", "9:30")
                                                        .AttributeValue;
            model.CloseAttendTime = DefaultAttenCloseTime;
            var DefaultAttenAfterCloseStaus = _ISchoolSettingService.GetorSetSchoolData("StaffAttendanceStatusAfterCloseTime", "");
            if (DefaultAttenAfterCloseStaus != null)
            {
                var AttendanceStatus = _ISubjectService.GetAllAttendanceStatus(ref count,
                                                  AttnStatus: DefaultAttenAfterCloseStaus.AttributeValue.ToLower()).FirstOrDefault();
                model.StaffAttendanceStatusCloseId = AttendanceStatus != null ?
                                                   AttendanceStatus.AttendanceStatusId.ToString() : "UM";
            }
            var DefaultSettings = _ICommonMethodService.GetOrSetSMSDefaultSetting("StaffAttendance", true, false, true, "StaffMobileNumber", "");
            model.SMS = (bool)DefaultSettings.SMS;
            model.Notification = (bool)DefaultSettings.Notification;
            model.NonAppUser = (bool)DefaultSettings.ToNonAppUser;

            return View(model);
        }

        public ActionResult StaffAttendanceList(DateTime? date = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffAttendance", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            try
            {
                var DefaultAttendanceStatus = _ISchoolSettingService.GetorSetSchoolData("TextForNotMarkedAttendance", "UnMarked")
                                                            .AttributeValue;

                //if the current time is grater than closed time then it will 
                //update the attendance according to selection of after closed status of attendance to all staff
                string DefaultAttendanceStatusId = "";
                var getStatusid = _ISchoolSettingService.GetorSetSchoolData("StaffDefaultAttendanceStatus", "UnMarked");
                if (getStatusid != null)
                {
                    var AttendanceStatus = _IStaffService.GetStaffAllAttendanceStatus(ref count,
                                                      AttnStatus: getStatusid.AttributeValue.ToLower()).FirstOrDefault();
                    DefaultAttendanceStatusId = AttendanceStatus != null ?
                                                      AttendanceStatus.AttendanceStatusId.ToString() : "UM";
                }
                if (DefaultAttendanceStatusId != "UM")
                {
                    var DefaultAttendanceStatus_Id = Convert.ToInt32(DefaultAttendanceStatusId);
                    DefaultAttendanceStatus = _IStaffService.GetStaffAttendanceStatusById(DefaultAttendanceStatus_Id).AttendanceStatus;
                }


                TimeSpan? DefaultInTime = null;
                var getStaffInTime = _ISchoolSettingService.GetorSetSchoolData("StaffInTime", "8:00");
                if (getStaffInTime != null)
                    DefaultInTime = _ICommonMethodService.ConvertStringToTimeSpan(getStaffInTime.AttributeValue);

                TimeSpan? DefaultOutTime = null;
                var getStaffOutTime = _ISchoolSettingService.GetorSetSchoolData("StaffOutTime", "14:30");
                if (getStaffOutTime != null)
                    DefaultOutTime = _ICommonMethodService.ConvertStringToTimeSpan(getStaffOutTime.AttributeValue);


                var StatusList = new List<SelectListItem>();
                string ChangedText = _ISchoolSettingService.GetorSetSchoolData("TextForNotMarkedAttendance", "UnMarked").AttributeValue;
                var allStaffAttendanceStatus = _IStaffService.GetStaffAllAttendanceStatus(ref count);
                StatusList.Add(new SelectListItem
                {
                    Text = ChangedText,
                    Value = "UM"
                });
                foreach (var item in allStaffAttendanceStatus)
                    StatusList.Add(new SelectListItem { Selected = false, Text = item.AttendanceStatus, Value = item.AttendanceStatusId.ToString() });



                //string HasCountAttStatus = "";
                //var ListHasAttStatus = _IStaffService.GetStaffAllAttendanceStatus(ref count).Where(x => x.AttendanceCount > 0).ToList();
                //foreach (var att in ListHasAttStatus)
                //{
                //    HasCountAttStatus += att.AttendanceStatus + ",";
                //}
                //HasCountAttStatus = HasCountAttStatus.Trim(',');

                var staffList = new List<StaffAttendanceListModel>();
                var allStaffs = _IStaffService.GetAllStaffs(ref count, DOJ: date.Value.Date).Where(p => p.IsActive == true).ToList();
                var noofpastAttendannceeditabledays = _ISchoolSettingService.GetAttributeValue("StaffBackDateAttendDays");
                var lasteditabledate = DateTime.UtcNow.Date.AddDays(-Convert.ToInt32(noofpastAttendannceeditabledays));
                if (date != null && date < lasteditabledate)
                {
                    var prvDateAttendances = _IStaffService.GetAllStaffAttendances(ref count, AttendanceDate: date).ToList();
                    if (prvDateAttendances.Count > 0)
                    {
                        foreach (var item in prvDateAttendances)
                        {
                            staffList.Add(new StaffAttendanceListModel
                            {
                                StaffId = item.StaffId,
                                AttendanceId = item.StaffAttendanceId,
                                AttendanceStatusId = item.AttendanceStatusId != null ? item.AttendanceStatusId.ToString() : DefaultAttendanceStatusId,
                                Name = item.Staff.FName + " " + item.Staff.LName,
                                StaffType = item.Staff.StaffType.StaffType1,
                                IsAuto = item.IsAuto != null ? (bool)item.IsAuto : false,
                                EmpCode = item.Staff.EmpCode,
                                AttendanceStatusAbbr = item.StaffAttendanceStatu.AttendanceAbbr,
                                AttendanceStatus = item.StaffAttendanceStatu != null ? item.StaffAttendanceStatu.AttendanceStatus : DefaultAttendanceStatus,
                                IntTime = item.InTime != null ? item.InTime.Value.ToString(@"hh\:mm") : "",
                                OutTime = item.OutTime != null ? item.OutTime.Value.ToString(@"hh\:mm") : ""
                            });
                        }
                        return Json(new { status = "success", staffList = staffList, IsPrvDate = true, msg = "", StatusList });
                    }
                    else
                    {
                        foreach (var item in allStaffs)
                        {
                            staffList.Add(new StaffAttendanceListModel
                            {
                                StaffId = item.StaffId,
                                Name = item.FName + " " + item.LName,
                                StaffType = item.StaffType.StaffType1,
                                AttendanceStatusId = DefaultAttendanceStatusId,
                                EmpCode = item.EmpCode,
                                IsAuto = false,
                                AttendanceStatus = DefaultAttendanceStatus,
                                IntTime = DefaultInTime.Value.ToString(@"hh\:mm"),
                                OutTime = DefaultOutTime.Value.ToString(@"hh\:mm")
                            });
                        }
                        return Json(new
                        {
                            status = "success",
                            staffList = staffList,
                            msg = "Attendance is not marked yet. Press 'Save' to mark attendance",
                            StatusList,
                            IsPrvDate = true
                        });
                    }
                    //else
                    //    return Json(new { status = "failed", data = "No Records found.", IsPrvDate = true, msg = "", StatusList });
                }
                else
                {
                    var currentDateAttendances = _IStaffService.GetAllStaffAttendances(ref count, AttendanceDate: date).ToList();
                    if (currentDateAttendances.Count > 0)
                    {
                        foreach (var item in allStaffs)
                        {
                            if (currentDateAttendances.Select(p => p.StaffId).ToList().Contains(item.StaffId))
                            {

                                var getstaffDet = currentDateAttendances.Where(p => p.StaffId == item.StaffId).FirstOrDefault();
                                staffList.Add(new StaffAttendanceListModel
                                {
                                    StaffId = item.StaffId,
                                    AttendanceId = getstaffDet.StaffAttendanceId,
                                    AttendanceStatusId = getstaffDet.AttendanceStatusId == null ? DefaultAttendanceStatusId : getstaffDet.AttendanceStatusId.ToString(),
                                    Name = item.FName + " " + item.LName,
                                    StaffType = item.StaffType.StaffType1,
                                    EmpCode = item.EmpCode,
                                    IsAuto = getstaffDet.IsAuto != null ? (bool)getstaffDet.IsAuto : false,
                                    AttendanceStatusAbbr = getstaffDet.StaffAttendanceStatu.AttendanceAbbr,
                                    AttendanceStatus = getstaffDet.StaffAttendanceStatu != null ? getstaffDet.StaffAttendanceStatu.AttendanceStatus : DefaultAttendanceStatus,
                                    IntTime = getstaffDet.InTime != null ? getstaffDet.InTime.Value.ToString(@"hh\:mm") : "",
                                    OutTime = getstaffDet.OutTime != null ? getstaffDet.OutTime.Value.ToString(@"hh\:mm") : ""

                                });
                            }
                            else
                            {
                                staffList.Add(new StaffAttendanceListModel
                                {
                                    StaffId = item.StaffId,
                                    AttendanceStatusId = DefaultAttendanceStatusId,
                                    Name = item.FName + " " + item.LName,
                                    StaffType = item.StaffType.StaffType1,
                                    EmpCode = item.EmpCode,
                                    IsAuto = false,
                                    AttendanceStatus = DefaultAttendanceStatus,
                                    IntTime = DefaultInTime.Value.ToString(@"hh\:mm"),
                                    OutTime = DefaultOutTime.Value.ToString(@"hh\:mm")
                                });
                            }
                        }
                        return Json(new { status = "success", staffList = staffList, msg = "", StatusList });
                    }
                    else
                    {
                        foreach (var item in allStaffs)
                        {
                            staffList.Add(new StaffAttendanceListModel
                            {
                                StaffId = item.StaffId,
                                Name = item.FName + " " + item.LName,
                                StaffType = item.StaffType.StaffType1,
                                AttendanceStatusId = DefaultAttendanceStatusId,
                                EmpCode = item.EmpCode,
                                IsAuto = false,
                                AttendanceStatus = DefaultAttendanceStatus,
                                IntTime = DefaultInTime.Value.ToString(@"hh\:mm"),
                                OutTime = DefaultOutTime.Value.ToString(@"hh\:mm")
                            });
                        }
                        return Json(new
                        {
                            status = "success",
                            staffList = staffList,
                            msg = "Attendance is not marked yet. Press 'Save' to mark attendance",
                            StatusList
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public ActionResult BindStaffStatusList(string EnteredText = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffAttendance", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            try
            {
                var RebindStatusList = new List<SelectListItem>();
                string ChangedText = "";
                if (!string.IsNullOrEmpty(EnteredText))
                    ChangedText = EnteredText;
                else
                    ChangedText = _ISchoolSettingService.GetorSetSchoolData("TextForNotMarkedAttendance", "UnMarked").AttributeValue;


                var allStaffStatus = _IStaffService.GetStaffAllAttendanceStatus(ref count);
                RebindStatusList.Add(new SelectListItem
                {
                    Selected = false,
                    Text = "--Select--",
                    Value = ""
                });
                RebindStatusList.Add(new SelectListItem
                {
                    Selected = false,
                    Text = ChangedText,
                    Value = "UM"
                });
                foreach (var item in allStaffStatus)
                {
                    RebindStatusList.Add(new SelectListItem
                    {
                        Selected = false,
                        Text = item.AttendanceStatus,
                        Value = item.AttendanceStatusId.ToString()
                    });
                }

                return Json(new
                {
                    RebindStatusList
                });

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public ActionResult GetStaffAttendance(string date)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {

                string NotInAttendanceStu = "";
                DateTime? attDate = null;
                if (!string.IsNullOrEmpty(date) || !string.IsNullOrWhiteSpace(date))
                {
                    attDate = Convert.ToDateTime(date);
                    attDate = attDate.Value.Date;
                }

                var currentSession = _ICatalogMasterService.GetCurrentSession();
                var AttenDateStaffs = _IStaffService.GetAllStaffAttendances(ref count, AttendanceDate: attDate)
                                        .Select(x => x.StaffId).Distinct().ToArray();


                var listStus = new List<string>();
                var getNotAttstaffs = _IStaffService.GetAllStaffs(ref count).Where(x => !AttenDateStaffs.Contains(x.StaffId) && x.IsActive == true)
                                                                 .Select(x => x.StaffId).Distinct().ToArray();

                for (int i = 0; i < getNotAttstaffs.Count(); i++)
                    listStus.Add(getNotAttstaffs[i].ToString());

                return Json(new
                {
                    listStus
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = "Error happened"
                });
            }
        }

        [HttpPost]
        public ActionResult StaffAttendance(StaffAttendanceModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffAttendance", "Add");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            try
            {
                var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                var StaffAttendancetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StaffAttendance").FirstOrDefault();
                List<StaffAttendanceListModel> deserializedStudentAttedanceStatusList = (List<StaffAttendanceListModel>)Newtonsoft.Json.JsonConvert.DeserializeObject(model.StaffAttendanceList, typeof(List<StaffAttendanceListModel>));
                var date = DateTime.Now;




                //insertion in attendance table
                foreach (var item in deserializedStudentAttedanceStatusList)
                {
                    date = item.AttendanceDate;
                    bool isSaved = false;
                    if (!string.IsNullOrEmpty(item.AttendanceStatusId))
                    {
                        if (item.AttendanceStatusId != "UM")
                            isSaved = true;
                    }

                    if (isSaved)
                    {
                        int AttendanceStatus_Id = 0;
                        AttendanceStatus_Id = Convert.ToInt32(item.AttendanceStatusId);

                        if (item.AttendanceId > 0)
                        {

                            var attendance = _IStaffService.GetStaffAttendanceById(StaffAttendanceId: item.AttendanceId);
                            if (attendance != null)
                            {
                                //update attendance 
                                attendance.AttendanceDate = item.AttendanceDate;
                                attendance.AttendanceStatusId = Convert.ToInt32(item.AttendanceStatusId);
                              //  attendance.IsAuto = false;
                               // attendance.InTime = null;
                               // attendance.OutTime = null;
                                if (!string.IsNullOrEmpty(model.HasCountAttStatus))
                                {
                                    var getstatus = _IStaffService.GetStaffAttendanceStatusById((int)AttendanceStatus_Id);
                                    if (!model.HasCountAttStatus.Contains(getstatus.AttendanceStatus))
                                    {
                                        item.IntTime = "";
                                        item.OutTime = "";
                                    }
                                }
                                if (item.IsIntimeInserted==true)
                                {
                                    if (!string.IsNullOrEmpty(item.IntTime))
                                    {
                                        var newinTime = _ICommonMethodService.ConvertStringToTimeSpan(item.IntTime);
                                        if (newinTime != attendance.InTime)
                                            attendance.IsAuto = false;

                                        attendance.InTime = newinTime;

                                    }
                                    else
                                    {
                                        attendance.InTime = null;
                                    }
                                }
                                
                                if (item.IsOuttimeInserted == true)
                                {
                                    if (!string.IsNullOrEmpty(item.OutTime))
                                    {
                                        var newouttime = _ICommonMethodService.ConvertStringToTimeSpan(item.OutTime);
                                        if (newouttime != attendance.OutTime)
                                            attendance.IsOutAuto = false;

                                        attendance.OutTime = newouttime;
                                    }
                                    else
                                    {
                                        attendance.OutTime = null;
                                    }
                                }
                               
                                _IStaffService.UpdateStaffAttendance(attendance);
                                if (StaffAttendancetable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, StaffAttendancetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(attendance.StaffAttendanceId, StaffAttendancetable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update StaffAttendance with AttendanceId:{0} and AttendanceStatusId:{1}", attendance.StaffAttendanceId, attendance.AttendanceStatusId), Request.Browser.Browser);
                                }
                                TempData["SuccessNotificaition"] = "Attendance Updated successfully.";
                            }

                        }
                        else
                        {
                            StaffAttendance attendanceData = new StaffAttendance();
                            attendanceData.AttendanceDate = item.AttendanceDate;
                            attendanceData.StaffId = item.StaffId;
                            attendanceData.AttendanceStatusId = Convert.ToInt32(item.AttendanceStatusId);
                            
                            attendanceData.InTime = null;
                            attendanceData.OutTime = null;

                            var getstatus = _IStaffService.GetStaffAttendanceStatusById((int)AttendanceStatus_Id);
                            if (!string.IsNullOrEmpty(model.HasCountAttStatus))
                            {
                                if (!model.HasCountAttStatus.Contains(getstatus.AttendanceStatus))
                                {
                                    item.IntTime = "";
                                    item.OutTime = "";
                                }
                            }

                            if (!string.IsNullOrEmpty(item.IntTime))
                            {
                                attendanceData.InTime = _ICommonMethodService.ConvertStringToTimeSpan(item.IntTime);
                                attendanceData.IsAuto = false;
                            }
                            if (!string.IsNullOrEmpty(item.OutTime))
                            {
                                attendanceData.OutTime = _ICommonMethodService.ConvertStringToTimeSpan(item.OutTime);
                                attendanceData.IsOutAuto = false;
                            }

                            _IStaffService.InsertStaffAttendance(attendanceData);
                            if (StaffAttendancetable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, StaffAttendancetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(attendanceData.StaffAttendanceId, StaffAttendancetable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add StaffAttendance with AttendanceId:{0} and AttendanceStatusId:{1}", attendanceData.StaffAttendanceId, attendanceData.AttendanceStatusId), Request.Browser.Browser);
                            }

                            var staffInfo = _IStaffService.GetStaffById((int)item.StaffId);
                            var SMSDefaultSetting = _ICommonMethodService.GetOrSetSMSDefaultSetting("StaffAttendance", true, false, true, "StaffMobileNumber", "");
                            _ICommonMethodService.TiggerNotificationSMS(TriggerEventName: "StaffAttendance",
                                                      SMS: (bool)SMSDefaultSetting.SMS, Notification: (bool)SMSDefaultSetting.Notification,
                                                      IsNonAPPUser: (bool)SMSDefaultSetting.ToNonAppUser,
                                                      IsStaff: true, StaffIds: item.StaffId.ToString(),
                                                      IsUserFullDetail: true, attendanceStatus: getstatus.AttendanceStatus,
                                                      AttendanceDate: item.AttendanceDate.Date, Staff: staffInfo);

                            TempData["SuccessNotificaition"] = "Attendance Saved successfully.";
                        }
                    }
                    else
                    {
                        //if status changed from present or some other status to unmarked then the saved record should be deleted.
                        if (item.AttendanceId > 0)
                        {
                            _IStaffService.DeleteStaffAttendance(item.AttendanceId);
                            TempData["SuccessNotificaition"] = "Attendance Updated successfully.";
                        }
                    }
                }
                var allStaffAttendanceStatus = _IStaffService.GetStaffAllAttendanceStatus(ref count);
                foreach (var item in allStaffAttendanceStatus)
                    model.StatusList.Add(new SelectListItem { Selected = false, Text = item.AttendanceStatus, Value = item.AttendanceStatusId.ToString() });

                model.date = (ConvertDate(date));
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffAttendance", "Add");
                TempData["UpdatedStaffAttendanceDate"] = model.date;
                return RedirectToAction("StaffAttendance");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult InsertDefaultValues(StaffAttendanceModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffAttendance", "Add");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            try
            {
                //textvalue for not marked attendance
                var schoolSettingText = _ISchoolSettingService.GetSchoolSettingByAttribute("TextForNotMarkedAttendance");
                if (schoolSettingText != null)
                {
                    schoolSettingText.AttributeValue = model.TextForUnMarkedAtt;
                    _ISchoolSettingService.UpdateSchoolSetting(schoolSettingText);
                }
                else
                {
                    schoolSettingText = new SchoolSetting();
                    schoolSettingText.AttributeName = "TextForNotMarkedAttendance";
                    schoolSettingText.Description = "TextForNotMarkedAttendance";
                    schoolSettingText.AttributeValue = model.TextForUnMarkedAtt;
                    _ISchoolSettingService.InsertSchoolSetting(schoolSettingText);
                }

                //textvalue for staff closing time 
                var schoolSettingStaffAttendanceCloseText = _ISchoolSettingService.GetSchoolSettingByAttribute("StaffAttendanceCloseTime");
                if (schoolSettingStaffAttendanceCloseText != null)
                {
                    schoolSettingStaffAttendanceCloseText.AttributeValue = model.CloseAttendTime;
                    _ISchoolSettingService.UpdateSchoolSetting(schoolSettingStaffAttendanceCloseText);
                }
                else
                {
                    schoolSettingStaffAttendanceCloseText = new SchoolSetting();
                    schoolSettingStaffAttendanceCloseText.AttributeName = "StaffAttendanceCloseTime";
                    schoolSettingStaffAttendanceCloseText.Description = "StaffAttendanceCloseTime";
                    schoolSettingStaffAttendanceCloseText.AttributeValue = model.CloseAttendTime;
                    _ISchoolSettingService.InsertSchoolSetting(schoolSettingStaffAttendanceCloseText);
                }

                //textvalue for staff closing time 
                var SettingStaffAttendanceCloseStatusText = _ISchoolSettingService.GetSchoolSettingByAttribute("StaffAttendanceStatusAfterCloseTime");
                if (SettingStaffAttendanceCloseStatusText != null)
                {
                    SettingStaffAttendanceCloseStatusText.AttributeValue = model.StaffAttendanceStatusCloseId;
                    _ISchoolSettingService.UpdateSchoolSetting(SettingStaffAttendanceCloseStatusText);
                }
                else
                {
                    SettingStaffAttendanceCloseStatusText = new SchoolSetting();
                    SettingStaffAttendanceCloseStatusText.AttributeName = "StaffAttendanceStatusAfterCloseTime";
                    SettingStaffAttendanceCloseStatusText.Description = "StaffAttendanceStatusAfterCloseTime";
                    SettingStaffAttendanceCloseStatusText.AttributeValue = model.StaffAttendanceStatusCloseId;
                    _ISchoolSettingService.InsertSchoolSetting(SettingStaffAttendanceCloseStatusText);
                }

                //staff In time
                if (!string.IsNullOrEmpty(model.IntTime))
                {
                    var schoolSettingStaffInTime = _ISchoolSettingService.GetSchoolSettingByAttribute("StaffInTime");
                    if (schoolSettingStaffInTime != null)
                    {
                        schoolSettingStaffInTime.AttributeValue = model.IntTime;
                        _ISchoolSettingService.UpdateSchoolSetting(schoolSettingStaffInTime);
                    }
                    else
                    {
                        schoolSettingStaffInTime = new SchoolSetting();
                        schoolSettingStaffInTime.AttributeName = "StaffInTime";
                        schoolSettingStaffInTime.Description = "StaffInTime";
                        schoolSettingStaffInTime.AttributeValue = model.IntTime;
                        _ISchoolSettingService.InsertSchoolSetting(schoolSettingStaffInTime);
                    }
                }

                //staff Outtime
                if (!string.IsNullOrEmpty(model.OutTime))
                {

                    var schoolSettingStaffOuttime = _ISchoolSettingService.GetSchoolSettingByAttribute("StaffOutTime");
                    if (schoolSettingStaffOuttime != null)
                    {
                        schoolSettingStaffOuttime.AttributeValue = model.OutTime;
                        _ISchoolSettingService.UpdateSchoolSetting(schoolSettingStaffOuttime);
                    }
                    else
                    {
                        schoolSettingStaffOuttime = new SchoolSetting();
                        schoolSettingStaffOuttime.AttributeName = "StaffOutTime";
                        schoolSettingStaffOuttime.Description = "StaffOutTime";
                        schoolSettingStaffOuttime.AttributeValue = model.OutTime;
                        _ISchoolSettingService.InsertSchoolSetting(schoolSettingStaffOuttime);

                    }
                }

                DateTime? attDate = null;
                if (!string.IsNullOrEmpty(model.date) || !string.IsNullOrWhiteSpace(model.date))
                {
                    attDate = DateTime.ParseExact(model.date, "dd/MM/yyyy", null);
                    attDate = attDate.Value.Date;
                }

                var currentSession = _ICatalogMasterService.GetCurrentSession();
                var AttenDateStaffs = _IStaffService.GetAllStaffAttendances(ref count, AttendanceDate: attDate)
                                        .Select(x => x.StaffId).Distinct().ToArray();


                //var listStus = new List<string>();
                //var getNotAttstaffs = _IStaffService.GetAllStaffs(ref count).Where(x => !AttenDateStaffs.Contains(x.StaffId) && x.IsActive == true)
                //                                                 .Select(x => x.StaffId).Distinct().ToArray();

                //for (int i = 0; i < getNotAttstaffs.Count(); i++)
                //    listStus.Add(getNotAttstaffs[i].ToString());


                //save default StatusValue
                if (!string.IsNullOrEmpty(model.StaffAttendanceStatusId))
                {
                    string AttenStatus = schoolSettingText.AttributeValue;
                    if (model.StaffAttendanceStatusId != schoolSettingText.AttributeValue)
                    {
                        var StuAttenStatus = _IStaffService.GetStaffAllAttendanceStatus(ref count, AttnStatus: model.StaffAttendanceStatusId).FirstOrDefault();
                        AttenStatus = StuAttenStatus != null ? StuAttenStatus.AttendanceStatus : schoolSettingText.AttributeValue;
                    }

                    var schoolSettingStuOuttime = _ISchoolSettingService.GetSchoolSettingByAttribute("StaffDefaultAttendanceStatus");
                    if (schoolSettingStuOuttime != null)
                    {
                        schoolSettingStuOuttime.AttributeValue = AttenStatus;
                        _ISchoolSettingService.UpdateSchoolSetting(schoolSettingStuOuttime);
                    }
                    else
                    {
                        schoolSettingStuOuttime = new SchoolSetting();
                        schoolSettingStuOuttime.AttributeName = "StaffDefaultAttendanceStatus";
                        schoolSettingStuOuttime.Description = "StaffDefaultAttendanceStatus";
                        schoolSettingStuOuttime.AttributeValue = AttenStatus;
                        _ISchoolSettingService.InsertSchoolSetting(schoolSettingStuOuttime);
                    }
                }

                var DefaultSettings = _ICommonMethodService.GetOrSetSMSDefaultSetting("StaffAttendance", true, false, true, "StaffMobileNumber", "");
                if (DefaultSettings != null)
                {
                    DefaultSettings.Notification = model.Notification;
                    DefaultSettings.SMS = model.SMS;
                    DefaultSettings.ToNonAppUser = model.NonAppUser;
                    _ICommonMethodService.UpdateSMSDefaultSetting(DefaultSettings);
                }
                return Json(new
                {
                    status = "Success",
                    StatusId = model.StaffAttendanceStatusId,
                    TextForUnMarkedAtt = model.TextForUnMarkedAtt,
                    InTime = model.IntTime,
                    OutTime = model.OutTime,
                    //listStus,
                    SMS = model.SMS,
                    Notification = model.Notification,
                    NonAppUser = model.NonAppUser
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = "Error happened"
                });
            }
        }

        public ActionResult GetStaffWithAttendance(DataSourceRequest command, StaffAttendanceReportModel model,
                                    IEnumerable<DAL.DAL.Kendo.Sort> sort = null, string Nm_ClassId = "")
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                DateTime? AttendanceDate = null;
                if (!string.IsNullOrEmpty(model.Date) || !string.IsNullOrWhiteSpace(model.Date))
                {
                    AttendanceDate = DateTime.ParseExact(model.Date, "dd/MM/yyyy", null);
                    AttendanceDate = AttendanceDate.Value.Date;
                }
                string Ids = "";
                if (model.AttendanceStatusIds != null)
                {
                    foreach (var AttendanceStatusId in model.AttendanceStatusIds)
                    {
                        Ids += AttendanceStatusId + ",";
                    }
                    Ids = Ids.Trim(',').Replace("0", "");
                }

                var currentSession_id = _ICatalogMasterService.GetCurrentSession().SessionId;
                var Staffs = new List<StaffAttendanceReportModel>();

                Staffs = _IStaffService.GetStaffForAttendanceReport(ref count, Date: AttendanceDate,
                                                                                    AttendanceStatusIds: Ids);
                if (!string.IsNullOrEmpty(model.ViewAttnStatus))
                {

                    switch (model.ViewAttnStatus)
                    {
                        case "P":
                            Staffs = Staffs.Where(x => x.AttendanceAbbr == "P").ToList();
                            break;

                        case "A":
                            Staffs = Staffs.Where(x => x.AttendanceAbbr == "A").ToList();
                            break;

                        case "L":
                            Staffs = Staffs.Where(x => x.AttendanceAbbr == "L" || x.AttendanceAbbr == "SL" || x.AttendanceAbbr == "HD").ToList();
                            break;
                        case "NM":
                            //int Class_Id = 0;

                            //var allstudentStatusdetail = _IStudentMasterService.GetAllStudentStatusDetail(ref count);
                            //Students = new List<StudentAttendanceReport>();
                            //foreach (var stu in getStudent)
                            //{
                            //    string studentStatus = "";
                            //    var studentStatusdetail = allstudentStatusdetail.Where(p => p.StudentId == stu.StudentId).ToList();
                            //    if (studentStatusdetail.Count > 0)
                            //        studentStatus = studentStatusdetail.OrderByDescending(p => p.StatusChangeDate)
                            //                                .ThenByDescending(p => p.StudentStatusDetailId)
                            //                                .FirstOrDefault().StudentStatu.StudentStatus;
                            //    if (studentStatus == "Left")
                            //        continue;
                            //    else
                            //    {
                            //        var NotMarkedAttendance = Students.Where(x => x.StudentId == stu.StudentId).FirstOrDefault();
                            //        if (NotMarkedAttendance == null)
                            //        {
                            //            Students.Add(new StudentAttendanceReport
                            //            {
                            //                StudentName = stu.Student.FName,
                            //                Student_Id = _ICustomEncryption.base64e(stu.StudentId.ToString()),
                            //                RollNo = stu.RollNo,
                            //                Class_Id = _ICustomEncryption.base64e(stu.ClassId.ToString()),
                            //                ClassName = stu.ClassMaster.Class,
                            //                AttendanceStatus = "Not Marked",
                            //            });
                            //        }
                            //    }
                            //}
                            break;
                    }

                }
                if (!string.IsNullOrEmpty(model.AttendanceTypeId) || !string.IsNullOrEmpty(model.AttendaceModeId))
                {
                    if (model.AttendanceTypeId == "auto")
                    {
                        if (model.AttendaceModeId == "in")
                        {
                            Staffs = Staffs.Where(f => (!string.IsNullOrEmpty(f.IsAuto)) && f.IsAuto.ToLower() == "true").ToList();
                        }
                        else if (model.AttendaceModeId == "out")
                        {
                            Staffs = Staffs.Where(f => (!string.IsNullOrEmpty(f.IsOutAuto)) && f.IsOutAuto.ToLower() == "true").ToList();
                        }
                        else if (model.AttendaceModeId == "inout")
                        {
                            Staffs = Staffs.Where(f => (!string.IsNullOrEmpty(f.IsAuto)) && f.IsAuto.ToLower() == "true" && (!string.IsNullOrEmpty(f.IsOutAuto)) && f.IsOutAuto.ToLower() == "true").ToList();
                        }
                        else
                        {
                            Staffs = Staffs.Where(f => ((!string.IsNullOrEmpty(f.IsAuto)) && f.IsAuto.ToLower() == "true") || ((!string.IsNullOrEmpty(f.IsOutAuto)) && f.IsOutAuto.ToLower() == "true")).ToList();
                        }
                    }
                    if (model.AttendanceTypeId == "manu")
                    {
                        if (model.AttendaceModeId == "in")
                        {
                            Staffs = Staffs.Where(f => (!string.IsNullOrEmpty(f.IsAuto)) && (f.IsAuto.ToLower() == "false")).ToList();
                        }
                        else if (model.AttendaceModeId == "out")
                        {
                            Staffs = Staffs.Where(f => (!string.IsNullOrEmpty(f.IsOutAuto)) && (f.IsOutAuto.ToLower() == "false")).ToList();
                        }
                        else if (model.AttendaceModeId == "inout")
                        {
                            Staffs = Staffs.Where(f => (!string.IsNullOrEmpty(f.IsOutAuto)) && (f.IsOutAuto.ToLower() == "false") && (!string.IsNullOrEmpty(f.IsAuto)) && (f.IsAuto.ToLower() == "false")).ToList();
                        }
                        else
                        {
                            Staffs = Staffs.Where(f => ((!string.IsNullOrEmpty(f.IsAuto)) && f.IsAuto.ToLower() == "false") || ((!string.IsNullOrEmpty(f.IsOutAuto)) && f.IsOutAuto.ToLower() == "false")).ToList();
                        }
                    }
                    if (model.AttendanceTypeId == "miss")
                    {
                        var StaffList = _IStaffService.GetAllStaffs(ref count, IsActive: true);
                        var StffAttendanceIds = StaffList.Select(f => f.StaffId).ToArray();
                        //if (model.AttendaceModeId == "in")
                        //{
                        //    // StdAttendanceIds = Students.Where(f => f.IsAuto != "" && f.IsAuto != null && f.AttendanceInTime != "" && f.AttendanceInTime != null).Select(f => f.StudentId).ToArray();
                        //    StffAttendanceIds = Staffs.Where(f => f.IsAuto != "" && f.IsAuto != null && f.AttendanceInTime != "" && f.AttendanceInTime != null).Select(f => f.StaffId).ToArray();
                        //}
                        //else if (model.AttendaceModeId == "out")
                        //{
                        //    StffAttendanceIds = Staffs.Where(f => f.IsOutAuto != "" && f.IsOutAuto != null && f.AttendanceOutTime != "" && f.AttendanceOutTime != null).Select(f => f.StaffId).ToArray();
                        //}
                        //else if (model.AttendaceModeId == "inout")
                        //{
                        //    StffAttendanceIds = Staffs.Where(f => f.IsAuto != null && f.IsAuto != "" && f.IsOutAuto == null || f.IsOutAuto == "" && f.AttendanceInTime != "" && f.AttendanceInTime != null && f.AttendanceOutTime != "" && f.AttendanceOutTime != null).Select(f => f.StaffId).ToArray();
                        //}
                        //else
                        //{
                        //StffAttendanceIds = Staffs.Where(f => (f.IsOutAuto != "" && f.IsOutAuto != null && f.AttendanceOutTime != "" && f.AttendanceOutTime != null) || (f.IsAuto != "" && f.IsAuto != null && f.AttendanceInTime != "" && f.AttendanceInTime != null)).Select(f => f.StaffId).ToArray();
                        StffAttendanceIds = Staffs.Where(f =>f.AttendanceStatusId>0).Select(f => f.StaffId).ToArray();
                        //  }
                        StaffList = StaffList.Where(f => !StffAttendanceIds.Contains((int)f.StaffId)).ToList();
                        var missgridModel = new DataSourceResult
                        {
                            Data = StaffList.Select(p =>
                            {
                                var stafflist = new StaffAttendanceReportModel();
                                stafflist.Staff_Id = _ICustomEncryption.base64e(p.StaffId.ToString());
                                stafflist.StaffName = p.FName+ " "+p.LName;
                                stafflist.EmpCode = p.EmpCode;
                                stafflist.Designation = p.Designation.Designation1;
                                //stafflist.AttendanceStatus = p.AttendanceStatus;
                                //stafflist.AttendanceInTime = string.IsNullOrEmpty(p.AttendanceInTime) ? "" : Convert.ToDateTime(p.AttendanceInTime).TimeOfDay.ToString(@"hh\:mm");
                                //stafflist.AttendanceOutTime = string.IsNullOrEmpty(p.AttendanceOutTime) ? "" : Convert.ToDateTime(p.AttendanceOutTime).TimeOfDay.ToString(@"hh\:mm");
                                //stafflist.AttendanceMode = p.AttendanceMode;

                                //if (!string.IsNullOrEmpty(p.IsAuto) && !string.IsNullOrEmpty(p.IsOutAuto))
                                //{
                                //    if (p.IsAuto == "True" || p.IsOutAuto == "True")
                                //        stafflist.ShowViewPunchesBtn = true;
                                //}

                                stafflist.ModeInTime = "";
                                //if (!string.IsNullOrEmpty(p.IsAuto))
                                //{
                                //    if (stafflist.AttendanceInTime != "")
                                //        stafflist.ModeInTime = p.IsAuto == "True" ? "Automatic" : "Manual";
                                //}

                                stafflist.ModeOutTime = "";
                                //if (!string.IsNullOrEmpty(p.IsOutAuto))
                                //{
                                //    if (stafflist.AttendanceOutTime != "")
                                //        stafflist.ModeOutTime = p.IsOutAuto == "True" ? "Automatic" : "Manual";
                                //}

                                return stafflist;
                            }).AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                            Total = StaffList.Count()
                        };
                        return Json(missgridModel);
                    }
                }
                var gridModel = new DataSourceResult
                {
                    Data = Staffs.Select(p =>
                    {
                        var stafflist = new StaffAttendanceReportModel();
                        stafflist.Staff_Id = _ICustomEncryption.base64e(p.StaffId.ToString());
                        stafflist.StaffName = p.StaffName;
                        stafflist.EmpCode = p.EmpCode;
                        stafflist.Designation = p.Designation;
                        stafflist.AttendanceStatus = p.AttendanceStatus;
                        stafflist.AttendanceInTime = string.IsNullOrEmpty(p.AttendanceInTime) ? "" : Convert.ToDateTime(p.AttendanceInTime).TimeOfDay.ToString(@"hh\:mm");
                        stafflist.AttendanceOutTime = string.IsNullOrEmpty(p.AttendanceOutTime) ? "" : Convert.ToDateTime(p.AttendanceOutTime).TimeOfDay.ToString(@"hh\:mm");
                        stafflist.AttendanceMode = p.AttendanceMode;

                        if (!string.IsNullOrEmpty(p.IsAuto) && !string.IsNullOrEmpty(p.IsOutAuto))
                        {
                            if (p.IsAuto == "True" || p.IsOutAuto == "True")
                                stafflist.ShowViewPunchesBtn = true;
                        }

                        stafflist.ModeInTime = "";
                        if (!string.IsNullOrEmpty(p.IsAuto))
                        {
                            if (stafflist.AttendanceInTime != "")
                                stafflist.ModeInTime = p.IsAuto == "True" ? "Automatic" : "Manual";
                        }

                        stafflist.ModeOutTime = "";
                        if (!string.IsNullOrEmpty(p.IsOutAuto))
                        {
                            if (stafflist.AttendanceOutTime != "")
                                stafflist.ModeOutTime = p.IsOutAuto == "True" ? "Automatic" : "Manual";
                        }

                        return stafflist;
                    }).AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = Staffs.Count()
                };

                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        #region staffattendancereport

        public ActionResult GetTeachersList()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                var stafftypes = _IStaffService.GetAllStaffTypes(ref count).Where(m => m.StaffType1.ToLower() == "teacher").FirstOrDefault();
                var stafftypeid = 0;
                if (stafftypes != null)
                {
                    stafftypeid = stafftypes.StaffTypeId;
                }
                if (stafftypeid > 0)
                {
                    var staffList = _IStaffService.GetAllStaffs(ref count).Where(m => m.StaffTypeId == stafftypeid && m.DOJ <= DateTime.Now.Date && m.IsActive == true);
                    var AvailableTeacherList = new List<SelectListItem>();
                    foreach (var item in staffList)
                    {
                        AvailableTeacherList.Add(new SelectListItem { Text = item.LName != null ? item.FName + " " + item.LName : item.FName, Value = item.StaffId.ToString() });
                    }

                    return Json(new { status = "success", AvailableTeacherList = AvailableTeacherList });
                }
                return Json(new { status = "failed" });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public ActionResult GetNonTeachingList()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                var stafftypes = _IStaffService.GetAllStaffTypes(ref count).Where(m => m.StaffType1.ToLower() == "non-teaching").FirstOrDefault();
                var stafftypeid = 0;
                if (stafftypes != null)
                {
                    stafftypeid = stafftypes.StaffTypeId;
                }
                if (stafftypeid > 0)
                {
                    var staffList = _IStaffService.GetAllStaffs(ref count).Where(m => m.StaffTypeId == stafftypeid && m.DOJ <= DateTime.Now.Date && m.IsActive == true);
                    var AvailableTeacherList = new List<SelectListItem>();
                    foreach (var item in staffList)
                    {
                        AvailableTeacherList.Add(new SelectListItem { Text = item.LName != null ? item.FName + " " + item.LName : item.FName, Value = item.StaffId.ToString() });
                    }

                    return Json(new { status = "success", AvailableTeacherList = AvailableTeacherList });
                }
                return Json(new { status = "failed" });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public ActionResult GetMonthList()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                var currentsession = _ICatalogMasterService.GetCurrentSession();
                var MONTHSLIST = _IDropDownService.MonthsList((DateTime)currentsession.StartDate, (DateTime)currentsession.EndDate);
                var AvailableMonthList = new List<SelectListItem>();
                AvailableMonthList.Add(new SelectListItem { Text = "--Select--", Value = "" });
                foreach (var item in MONTHSLIST)
                {
                    var monthid = DateTime.ParseExact(item, "MMMM", System.Globalization.CultureInfo.InvariantCulture).Month;
                    AvailableMonthList.Add(new SelectListItem { Text = item, Value = monthid.ToString() });
                }

                return Json(new { status = "success", AvailableMonthList = AvailableMonthList });

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public ActionResult StaffAttendanceReport()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffAttendanceReport", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            var model = new StaffAttendanceReportModel();
            var currentSession = _ICatalogMasterService.GetCurrentSession();
            var sessionlist = _ICatalogMasterService.GetAllSessions(ref count);
            foreach (var item in sessionlist)
            {
                model.AvailableSessions.Add(new SelectListItem { Text = item.Session1, Value = item.SessionId.ToString() });
            }

            model.SessionId = currentSession.SessionId;
            model.ClassList = _IDropDownService.ClassList();
            model.AttendanceStatusList = _IDropDownService.StaffAttendanceStatusList();

            model.AttendanceTypes.Add(new SelectListItem { Selected = true, Text = "--Select--", Value = "" });
            model.AttendanceTypes.Add(new SelectListItem { Selected = false, Text = "Automatic", Value = "auto" });
            model.AttendanceTypes.Add(new SelectListItem { Selected = false, Text = "Manual", Value = "manu" });
            model.AttendanceTypes.Add(new SelectListItem { Selected = false, Text = "Missing", Value = "miss" });

            model.AttendanceModes.Add(new SelectListItem { Selected = true, Text = "--Select--", Value = "" });
            model.AttendanceModes.Add(new SelectListItem { Selected = false, Text = "In", Value = "in" });
            model.AttendanceModes.Add(new SelectListItem { Selected = false, Text = "Out", Value = "out" });
            model.AttendanceModes.Add(new SelectListItem { Selected = false, Text = "In and Out", Value = "inout" });

            //values for kendo paging
            model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
            model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");
            return View(model);
        }


        public ActionResult GetPunchStaffDetail(DataSourceRequest command, StaffAttendanceReportModel model,
                         IEnumerable<DAL.DAL.Kendo.Sort> sort = null, string Nm_ClassId = "")
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                DateTime? AttendanceDate = null;
                if (!string.IsNullOrEmpty(model.Date) || !string.IsNullOrWhiteSpace(model.Date))
                {
                    AttendanceDate = DateTime.ParseExact(model.Date, "dd/MM/yyyy", null);
                    AttendanceDate = AttendanceDate.Value.Date;
                }
                //string Ids = "";
                //if (model.AttendanceStatusIds != null)
                //{
                //    foreach (var AttendanceStatusId in model.AttendanceStatusIds)
                //    {
                //        Ids += AttendanceStatusId + ",";
                //    }
                //    Ids = Ids.Trim(',').Replace("0", "");
                //}

                var currentSession_id = _ICatalogMasterService.GetCurrentSession().SessionId;
                //var Students = new List<StudentAttendanceReport>();
                int StaffId = 0;
                if (!string.IsNullOrEmpty(model.Staff_Id))
                    int.TryParse(_ICustomEncryption.base64d(model.Staff_Id), out StaffId);

                var Punchtime = _ISubjectService.GetAllAttendancePunch(ref count).Where(m => m.UserId == StaffId
                                       && (m.ContactType.ContactType1 == "Teacher" || m.ContactType.ContactType1 == "Non-Teaching")
                                       && m.PunchTime.Value.Date == AttendanceDate.Value.Date)
                                       .OrderByDescending(d => d.PunchTime.Value);

                var staffpunchdetailist = new List<StaffAttendanceDetailReport>();
                var a = 0;
                foreach (var i in Punchtime)
                {
                    a++;
                    string locationName = "";
                    string DeviceSerialNo = "";

                    var PunchDevice = _ISubjectService.GetAllAttendanceDevices(ref count)
                                                    .Where(m => m.AttendanceDeviceId == i.AttendanceDeviceId)
                                                    .FirstOrDefault();
                    if (PunchDevice != null)
                    {
                        DeviceSerialNo = !string.IsNullOrEmpty(PunchDevice.SerialNo) ? PunchDevice.SerialNo : "";
                        var GETLOCATIONS = PunchDevice.AttendanceDeviceLocations.Where(x => x.AttendanceDeviceId == i.AttendanceDeviceId).FirstOrDefault();
                        if (GETLOCATIONS != null)
                        {
                            var locations = _ISubjectService.GetAllLocations(ref count).Where(x => x.AttendanceLocationId == GETLOCATIONS.AttendanceLocationId).FirstOrDefault();
                            locationName = locations != null ? locations.AttendanceLocation1 : "";
                        }
                    }
                    var testdate = i.PunchTime.Value.ToString("hh:mm tt");

                    var staffInfo = _IStaffService.GetStaffById((int)i.UserId);

                    string Staffname = "";
                    if (staffInfo != null)
                    {
                        Staffname = _ICommonMethodService.TrimStaffName(staffInfo, Staffname);
                        staffpunchdetailist.Add(new StaffAttendanceDetailReport()
                        {
                            SN = a,
                            StaffId = _ICustomEncryption.base64e(i.UserId.ToString()),
                            Date = Convert.ToString(i.PunchTime.Value.Date),
                            StaffName = Staffname,
                            EmpCode = staffInfo.EmpCode,
                            Time = testdate,
                            Location = locationName,
                            DeviceSerialNo = DeviceSerialNo,
                        });
                    }
                }
                var gridModel = new DataSourceResult
                {
                    Data = staffpunchdetailist.AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = Punchtime.Count()
                };

                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult GenerateAtnReport(int MonthId, int SessionId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                if (MonthId > 0 && SessionId > 0)
                {
                    var Session = _ICatalogMasterService.GetSessionById(SessionId);
                    var dates = new List<DateTime>();
                    if (Session != null)
                    {
                        var startDate = Session.StartDate;
                        var endDate = Session.EndDate;

                        dates = _IDropDownService.GetAllDatesAndInitializeTickets((DateTime)startDate, (DateTime)endDate, MonthId);
                    }
                    var EventTypeId = _ISchedularService.GetAllEventTypeList(ref count).Where(m => m.EventType1.ToLower() == "holiday").FirstOrDefault().EventTypeId;

                    var holidayList = _ISchedularService.GetAllEventList(ref count).Where(m => m.StartDate.Value.Month == MonthId
                                                           && m.EventTypeId == EventTypeId && m.IsDeleted == false);
                    var IsIncludeAttendanceOnHoliday = _ISchoolSettingService.GetorSetSchoolData("IncludeAttendanceOnHoliday", "1").AttributeValue == "1" ? true : false;
                    var weekendOff = _ISchoolSettingService.GetorSetSchoolData("SaturdayOff", "2,4").AttributeValue;
                    var saturdayOffs = weekendOff.Split(',');

                    var AbbrForWeakOff = _ISchoolSettingService.GetorSetSchoolData("AbbrForWeakOff", "WO").AttributeValue;
                    var AbbrForHoliday = _ISchoolSettingService.GetorSetSchoolData("AbbrForHoliday", "H").AttributeValue;
                    var headlist = new List<string>();
                    for (var i = 0; i <= dates.Count; i++)
                    {
                        if (i == 0)
                        {
                            headlist.Add("Teacher Name");
                            headlist.Add("Emp Code");
                        }
                        else
                            headlist.Add(i.ToString());
                    }

                    var allstaffattendances = _IStaffService.GetAllStaffAttendances(ref count).ToList();
                    //var allTeachers = _IStaffService.GetAllStaffs(ref count).Where(p => p.StaffType.StaffType1.ToLower() == "teacher" && p.IsActive == true).ToList();
                    var allTeachers = _IStaffService.GetAllStaffs(ref count).Where(p => p.IsActive == true).ToList();
                    var attendancelist = new List<AttendanceReport>();
                    foreach (var item in allTeachers)
                    {
                        var AttendanceReport = new AttendanceReport();
                        AttendanceReport.TeacherName = item.FName + " " + item.LName;
                        AttendanceReport.EmpCode = item.EmpCode;
                        var satuardaycount = 0;
                        foreach (var date in dates)
                        {
                            var TeacherStatus = "";
                            var atnstatus = allstaffattendances.Where(p => p.StaffId == item.StaffId && p.AttendanceDate == date).FirstOrDefault();
                            if (atnstatus != null)
                                TeacherStatus = atnstatus.StaffAttendanceStatu.AttendanceAbbr;

                            if (date.DayOfWeek.ToString() == "Saturday")
                            {
                                satuardaycount++;
                            }

                            if (holidayList.Where(m => m.StartDate == date).Count() > 0)
                            {
                                if (IsIncludeAttendanceOnHoliday && TeacherStatus!="")
                                    AttendanceReport.AttendanceStatus.Add(TeacherStatus);
                                else
                                AttendanceReport.AttendanceStatus.Add(AbbrForHoliday);
                            }
                            else if (date.DayOfWeek.ToString() == "Sunday")
                            {
                                if (TeacherStatus == "P")
                                    AttendanceReport.AttendanceStatus.Add(TeacherStatus);
                                else
                                    AttendanceReport.AttendanceStatus.Add("S");
                            }
                            else if (date.DayOfWeek.ToString() == "Saturday")
                            {
                                bool IsAttendacneMark = false;
                                //satuardaycount++;
                                if (TeacherStatus != "")
                                {
                                    IsAttendacneMark = true;
                                    AttendanceReport.AttendanceStatus.Add(TeacherStatus);
                                }
                                else
                                {
                                    foreach (var weekend in saturdayOffs)
                                    {
                                        if (Convert.ToInt32(weekend) == satuardaycount)
                                        {
                                            IsAttendacneMark = true;
                                            AttendanceReport.AttendanceStatus.Add(AbbrForWeakOff);
                                        }
                                    }
                                }
                                if (!IsAttendacneMark)
                                    AttendanceReport.AttendanceStatus.Add("");
                            }
                            else
                                AttendanceReport.AttendanceStatus.Add(TeacherStatus);
                        }
                        attendancelist.Add(AttendanceReport);
                    }

                    return Json(new
                    {
                        status = "success",
                        headlist = headlist,
                        attendancelist = attendancelist
                    });
                }
                else
                {
                    return Json(new { status = "failed", data = "No data found." });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public ActionResult GenerateAtnReportTeacherWise(int TeacherId, int SessionId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                if (TeacherId > 0 && SessionId > 0)
                {
                    var Session = _ICatalogMasterService.GetSessionById(SessionId);
                    var dates = new List<DateTime>();
                    var maxdates = 31;
                    var Months = new List<int>();
                    if (Session != null)
                    {
                        var startDate = Session.StartDate;
                        var endDate = Session.EndDate;

                        Months = _IDropDownService.GetDatesAndInitializeTickets((DateTime)startDate, (DateTime)endDate);
                        var EventTypeId = _ISchedularService.GetAllEventTypeList(ref count).Where(m => m.EventType1.ToLower() == "holiday").FirstOrDefault().EventTypeId;

                        var IsIncludeAttendanceOnHoliday = _ISchoolSettingService.GetorSetSchoolData("IncludeAttendanceOnHoliday", "1").AttributeValue == "1" ? true : false;

                        var weekendOff = _ISchoolSettingService.GetorSetSchoolData("SaturdayOff", "2,4").AttributeValue;
                        var saturdayOffs = weekendOff.Split(',');
                        var AbbrForWeakOff = _ISchoolSettingService.GetorSetSchoolData("AbbrForWeakOff", "WO").AttributeValue;
                        var AbbrForHoliday = _ISchoolSettingService.GetorSetSchoolData("AbbrForHoliday", "H").AttributeValue;

                        var headlist = new List<string>();
                        for (var i = 0; i <= maxdates; i++)
                        {
                            if (i == 0)
                                headlist.Add("Months");
                            else
                                headlist.Add(i.ToString());
                        }

                        var allstaffattendances = _IStaffService.GetAllStaffAttendances(ref count).ToList();

                        var attendancelist = new List<AttendanceReport>();
                        foreach (var item in Months)
                        {
                            var holidayList = _ISchedularService.GetAllEventList(ref count).Where(m => m.StartDate.Value.Month == item
                                                            && m.EventTypeId == EventTypeId && m.IsDeleted == false);

                            dates = _IDropDownService.GetAllDatesAndInitializeTickets((DateTime)startDate, (DateTime)endDate, item);
                            var AttendanceReport = new AttendanceReport();
                            AttendanceReport.MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(item);
                            var satuardaycount = 0;
                            foreach (var date in dates)
                            {
                                var TeacherStatus = "";
                                var atnstatus = allstaffattendances.Where(p => p.StaffId == TeacherId && p.AttendanceDate == date).FirstOrDefault();
                                if (atnstatus != null)
                                    TeacherStatus = atnstatus.StaffAttendanceStatu.AttendanceAbbr;
                                if (date.DayOfWeek.ToString() == "Saturday")
                                {
                                    satuardaycount++;
                                }

                                //if (holidayList.Where(m => m.StartDate == date).Count() > 0)
                                //{
                                //    AttendanceReport.AttendanceStatus.Add(AbbrForHoliday);
                                //}
                                //else 

                                if (holidayList.Where(m => m.StartDate == date).Count() > 0)
                                {
                                    if (IsIncludeAttendanceOnHoliday && TeacherStatus != "")
                                        AttendanceReport.AttendanceStatus.Add(TeacherStatus);
                                    else
                                        AttendanceReport.AttendanceStatus.Add(AbbrForHoliday);
                                }
                                else if(date.DayOfWeek.ToString() == "Sunday")
                                {
                                    if (TeacherStatus == "P")
                                        AttendanceReport.AttendanceStatus.Add(TeacherStatus);
                                    else
                                        AttendanceReport.AttendanceStatus.Add("S");

                                }
                                else if (date.DayOfWeek.ToString() == "Saturday")
                                {

                                    bool IsAttendacneMark = false;
                                    //satuardaycount++;
                                    if (TeacherStatus != "")
                                    {
                                        IsAttendacneMark = true;
                                        AttendanceReport.AttendanceStatus.Add(TeacherStatus);
                                    }
                                    else
                                    {
                                        foreach (var weekend in saturdayOffs)
                                        {
                                            if (Convert.ToInt32(weekend) == satuardaycount)
                                            {
                                                IsAttendacneMark = true;
                                                AttendanceReport.AttendanceStatus.Add(AbbrForWeakOff);
                                            }
                                        }
                                    }
                                    if (!IsAttendacneMark)
                                        AttendanceReport.AttendanceStatus.Add("");
                                    //satuardaycount++;
                                    //if (saturdayOffs.Contains(satuardaycount.ToString()))
                                    //{
                                    //    AttendanceReport.AttendanceStatus.Add(AbbrForWeakOff);
                                    //}
                                    //else
                                    //    AttendanceReport.AttendanceStatus.Add(TeacherStatus);
                                }
                                else
                                    AttendanceReport.AttendanceStatus.Add(TeacherStatus);
                            }
                            attendancelist.Add(AttendanceReport);
                        }


                        return Json(new
                        {
                            status = "success",
                            headlist = headlist,
                            attendancelist = attendancelist
                        });
                    }
                }

                return Json(new { status = "failed", data = "No data found." });

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public ActionResult GenerateAtnReportSummaryWise(string SummaryType, int SessionId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                if (!String.IsNullOrEmpty(SummaryType) && SessionId > 0)
                {
                    var Session = _ICatalogMasterService.GetSessionById(SessionId);
                    var months = new List<DateTime>();
                    if (Session != null)
                    {
                        var startDate = Session.StartDate;
                        var endDate = Session.EndDate;

                        months = _IDropDownService.GetMonthCountBetweenDates((DateTime)startDate, (DateTime)endDate);
                        var Curentdate = DateTime.UtcNow.Date;

                        var headlist = new List<string>();
                        //for (var i = 0; i <= months.Count; i++)
                        //{
                        //    if (i == 0)
                        //    {
                        //        headlist.Add("S.No");
                        //        headlist.Add("Teacher Name");
                        //        headlist.Add("Emp Code");
                        //    }
                        //    else
                        //        headlist.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(months[i - 1].Month).ToUpper().ToString());
                        //}

                        var allstaffattendances = _IStaffService.GetAllStaffAttendances(ref count).ToList();
                        var allTeachers = _IStaffService.GetAllStaffs(ref count).Where(p => p.StaffType.StaffType1.ToLower() == "teacher" && p.IsActive == true).ToList();
                        var EventTypeId = _ISchedularService.GetAllEventTypeList(ref count).Where(m => m.EventType1.ToLower() == "holiday").FirstOrDefault().EventTypeId;
                        var attendancelist = new List<AttendanceReport>();
                        //get value from schoolsetting table
                        var weekendOff = _ISchoolSettingService.GetorSetSchoolData("SaturdayOff", "2,4").AttributeValue;


                        string StaffAttendanceSummHtml = "<table id='tblstaffSummaryRept' class='table table-responsive table-condensed table-striped table-bordered' style='margin-bottom:0px!important;'>";
                        StaffAttendanceSummHtml += "<thead class='thead-default'><tr>";


                        DataSet dsSummaryRept = _IStaffService.GetSummaryRept(ref count, Measurement: SummaryType);
                        var OnlClickSortFunction = "";

                        if (dsSummaryRept.Tables[0].Columns.Count > 0)
                        {
                            //StaffAttendanceSummHtml += "<th>S.No.</th>";
                            for (int i = 0; i < dsSummaryRept.Tables[0].Columns.Count; i++)
                            {
                                OnlClickSortFunction = "sortSummaryTeacherName(" + i + ")";
                                string ColName = dsSummaryRept.Tables[0].Columns[i].ColumnName.ToString();

                                if (ColName.Contains("StaffId"))
                                    StaffAttendanceSummHtml += "<th hidden>" + ColName + "</th>";
                                else if (ColName.Contains("Emp"))
                                    StaffAttendanceSummHtml += "<th onclick='" + OnlClickSortFunction + "' style='cursor:pointer; title='sort column'>" + ColName + "</th>";
                                else
                                    StaffAttendanceSummHtml += "<th>" + ColName + "</th>";
                            }
                        }
                        StaffAttendanceSummHtml += "</tr></thead>";
                        StaffAttendanceSummHtml += "<tbody>";

                        var bodylist = new List<string>();
                        if (dsSummaryRept.Tables[0].Rows.Count > 0)
                        {
                            // int rowcount = 0;
                            foreach (DataRow dr in dsSummaryRept.Tables[0].Rows)
                            {
                                //rowcount++;
                                StaffAttendanceSummHtml += "<tr>";
                                //StaffAttendanceSummHtml += "<td>" + rowcount + "</td>";
                                foreach (DataColumn dc in dsSummaryRept.Tables[0].Columns)
                                {
                                    string ColName = dc.ColumnName.ToString();
                                    if (ColName.Contains("StaffId"))
                                        StaffAttendanceSummHtml += "<td hidden>" + dr[dc.ColumnName.ToString()] + "</td>";
                                    else
                                    {
                                        if (SummaryType == "Percentage")
                                        {
                                            if (!dc.ColumnName.ToString().Contains("Emp"))
                                                StaffAttendanceSummHtml += "<td>" + dr[dc.ColumnName.ToString()] + "%</td>";
                                            else
                                                StaffAttendanceSummHtml += "<td>" + dr[dc.ColumnName.ToString()] + "</td>";
                                        }
                                        else
                                            StaffAttendanceSummHtml += "<td>" + dr[dc.ColumnName.ToString()] + "</td>";

                                    }
                                }
                                StaffAttendanceSummHtml += "</tr>";
                            }
                        }

                        StaffAttendanceSummHtml += "</tbody></table>";

                        //DataTable dtSatff = _IStaffService.GetSummaryRept(ref count);
                        //var AttendanceReport = new AttendanceReport();
                        //if (dtSatff.Rows.Count > 0)
                        //{

                        //    foreach(DataRow r in dtSatff.Rows)
                        //    {

                        //        AttendanceReport.TeacherName = r["StaffName"].ToString();
                        //    }
                        //}

                        //foreach (var item in allTeachers)
                        //{
                        //    var AttendanceReport = new AttendanceReport();
                        //    AttendanceReport.TeacherName = item.FName + " " + item.LName;
                        //    AttendanceReport.EmpCode = item.EmpCode;
                        //    foreach (var ml in months)
                        //    {
                        //        //months start and end dates
                        //        var firstDayOfMonth = new DateTime(ml.Date.Year, ml.Date.Month, 1); // first day of month
                        //        var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1); // last day of month
                        //        decimal StaffAttendanceTotal = 0;

                        //        DataTable dtSummaryRept = _IStaffService.GetSummaryRept(ref count, Month: ml.Date.Month, StaffId: item.StaffId);
                        //        if (dtSummaryRept.Rows.Count > 0)
                        //        {
                        //            foreach (DataRow i in dtSummaryRept.Rows)
                        //            {
                        //                StaffAttendanceTotal = Convert.ToInt32(i["TotalWorkingDays"]);
                        //            }
                        //        }


                        //        int days = _IStaffService.GetWorkingDayByMonth(firstDayOfMonth, lastDayOfMonth);

                        //        //get total saturdays in a month
                        //        DateTime[] dates = { firstDayOfMonth, lastDayOfMonth}; //etc....
                        //        var TotalSaturdays = CalculateWeekends(firstDayOfMonth, lastDayOfMonth);

                        //        int Weekenoff = 0;
                        //        if(!string.IsNullOrEmpty(weekendOff))
                        //        {
                        //           var  arryWeeks = weekendOff.Split(',');
                        //           Weekenoff = arryWeeks.Count();
                        //        }

                        //        if (TotalSaturdays > Weekenoff)
                        //            Weekenoff = TotalSaturdays - Weekenoff;
                        //        else if(TotalSaturdays < Weekenoff)
                        //            Weekenoff = Weekenoff - TotalSaturdays;

                        //        decimal TotalWorkingDayOfMonth = days - Weekenoff;

                        //        //DataTable dtSummaryTotalPresents = _IStaffService.GetSummaryRept(ref count, Month: ml.Date.Month, StaffId: item.StaffId,Status: "P");
                        //        //if (dtSummaryTotalPresents.Rows.Count > 0)
                        //        //{
                        //        //    foreach (DataRow i in dtSummaryTotalPresents.Rows)
                        //        //    {
                        //        //        Presents = Convert.ToInt32(i["Total"]);
                        //        //    }
                        //        //}

                        //        // get attendance by student
                        //        //var staffattendance = _IStaffService.GetAllStaffAttendances(ref count).Where(a => a.StaffId == item.StaffId
                        //        //                        && a.StaffAttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= firstDayOfMonth
                        //        //                        && a.AttendanceDate <= lastDayOfMonth).Distinct().ToList();
                        //        //if (staffattendance.Count > 0)
                        //        //{
                        //        //    TimeSpan diff = lastDayOfMonth.AddDays(1) - firstDayOfMonth;

                        //        //    // calculate attendance %
                        //        //    if (ml.Month == Curentdate.Month)
                        //        //        diff = Curentdate.AddDays(1) - firstDayOfMonth;

                        //        //    var holidaycount = _ISchedularService.GetAllEventList(ref count).Where(m => m.StartDate.Value.Month == ml.Date.Month
                        //        //                                && m.EventTypeId == EventTypeId && m.IsDeleted == false).Count();

                        //        //    int days = (diff.Days);

                        //        //    var StaffAttendancePresent = Convert.ToDecimal(staffattendance.Count());
                        //        //    var absentrecords = _IStaffService.GetAllStaffAttendances(ref count).Where(a => a.StaffId == item.StaffId
                        //        //                                        && a.StaffAttendanceStatu.AttendanceAbbr == "A"
                        //        //                                        && a.AttendanceDate >= firstDayOfMonth
                        //        //                                        && a.AttendanceDate <= lastDayOfMonth).ToList();
                        //        //    var StaffAttendanceAbsent = Convert.ToString(absentrecords.Count());

                        //        //    int diffDays = 0;
                        //        //    //int Dins = 0;
                        //        //    int Presents = 0;
                        //        //    var SaturdayCount = 0;
                        //        //    int LeaveCount = 0;
                        //        //    int NotMarkedCount = 0;
                        //        //    for (DateTime i = firstDayOfMonth; i <= lastDayOfMonth; i = i.AddDays(1))
                        //        //    {
                        //        //        // check sunday
                        //        //        var dayno = i.DayOfWeek;
                        //        //        var day = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(dayno);

                        //        //        // check holiday 
                        //        //        EventTypeId = _ISchedularService.GetAllEventTypeList(ref count).Where(m => m.EventType1.ToLower().Contains("holiday")).FirstOrDefault().EventTypeId;
                        //        //        var Event = _ISchedularService.GetAllEventList(ref count).Where(n => n.StartDate == i && n.IsActive == true && n.IsDeleted == false
                        //        //                                                          && n.EventTypeId == EventTypeId).ToList();

                        //        //        // check attendance
                        //        //        // get attendance by staff
                        //        //        var staffattendanceAbbr = _IStaffService.GetAllStaffAttendances(ref count).Where(a => a.StaffId == item.StaffId
                        //        //                                                        && a.AttendanceDate == i.Date).FirstOrDefault();

                        //        //        var monthwiseAttendanceStatus = staffattendanceAbbr != null ?
                        //        //                                     staffattendanceAbbr.StaffAttendanceStatu.AttendanceAbbr : "";

                        //        //        var saturdayOffs = weekendOff.Split(',');
                        //        //        //if staff is present on sunday then it will show P
                        //        //        if (day == "Sunday")
                        //        //        {
                        //        //            if (monthwiseAttendanceStatus != "P" || staffattendanceAbbr == null)
                        //        //            {
                        //        //                diffDays++;
                        //        //                monthwiseAttendanceStatus = "Sunday";
                        //        //            }
                        //        //        }
                        //        //        else if (Event.Count > 0)
                        //        //        {
                        //        //            if (monthwiseAttendanceStatus != "P" || staffattendanceAbbr == null)
                        //        //            {
                        //        //                diffDays++;
                        //        //                monthwiseAttendanceStatus = "Holiday";
                        //        //            }
                        //        //        }
                        //        //        else if (day == "Saturday")
                        //        //        {
                        //        //            SaturdayCount++;
                        //        //            //monthwiseAttendanceStatus = "";
                        //        //            if (monthwiseAttendanceStatus != "P" || staffattendanceAbbr == null)
                        //        //            {
                        //        //                foreach (var weekend in saturdayOffs)
                        //        //                {
                        //        //                    if (Convert.ToInt32(weekend) == SaturdayCount)
                        //        //                    {
                        //        //                        diffDays++;
                        //        //                        monthwiseAttendanceStatus = "Saturday Off";
                        //        //                    }
                        //        //                }
                        //        //            }

                        //        //        }
                        //        //        else
                        //        //        {
                        //        //            monthwiseAttendanceStatus = "";
                        //        //            if (staffattendanceAbbr != null)
                        //        //            {
                        //        //                if (!string.IsNullOrEmpty(staffattendanceAbbr.StaffAttendanceStatu.AttendanceAbbr))
                        //        //                {
                        //        //                    monthwiseAttendanceStatus = staffattendanceAbbr.StaffAttendanceStatu.AttendanceAbbr;
                        //        //                    absentcount += 0;
                        //        //                }
                        //        //                else
                        //        //                    absentcount += 1;
                        //        //            }
                        //        //        }

                        //        //        // if current month day exceed the current date
                        //        //        if (i > Curentdate)
                        //        //            break;

                        //        //        if (monthwiseAttendanceStatus == "P")
                        //        //            Presents++;

                        //        //        if (monthwiseAttendanceStatus == "CL" || monthwiseAttendanceStatus == "SL" || monthwiseAttendanceStatus == "HD")
                        //        //            LeaveCount++;

                        //        //        if (monthwiseAttendanceStatus == "")
                        //        //            NotMarkedCount++;
                        //        //    }
                        //        //In a year totalworking days with minus holiday/weekendoff/sundays/vacations 
                        //        //var StaffAttendanceTotal = (Presents + LeaveCount + NotMarkedCount 
                        //        //                            + (Convert.ToInt32(StaffAttendanceAbsent)
                        //        //                            + absentcount));

                        //        ////WorkingTotalDays += StaffAttendanceTotal;
                        //        ////TotalPresents += Presents;

                        //        if (SummaryType == "Percentage")
                        //        {
                        //            decimal percentage = 0;
                        //            if (StaffAttendanceTotal > 0)
                        //            {
                        //                percentage = (StaffAttendanceTotal / TotalWorkingDayOfMonth);
                        //                percentage = percentage * 100;
                        //            }
                        //            AttendanceReport.AttendanceStatus.Add(percentage.ToString("0.00") + " %");
                        //        }
                        //        else if (SummaryType == "Day")
                        //            AttendanceReport.AttendanceStatus.Add(StaffAttendanceTotal + "/" + TotalWorkingDayOfMonth);

                        //        //}
                        //        //else
                        //        //    AttendanceReport.AttendanceStatus.Add("");
                        //    }
                        //    attendancelist.Add(AttendanceReport);
                        //}
                        var jsonResult = Json(new
                        {
                            status = "success",
                            StaffAttendanceSummHtml
                        });
                        jsonResult.MaxJsonLength = int.MaxValue;
                        return jsonResult;
                    }
                    else
                        return Json(new { status = "failed", data = "Session not found." });
                }
                else
                {
                    return Json(new { status = "failed", data = "No data found." });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public int CalculateWeekends(DateTime DateTime1, DateTime DateTime2)
        {
            int iReturn = 0;
            TimeSpan xTimeSpan;
            if (DateTime2 > DateTime1)
                xTimeSpan = DateTime2.Subtract(DateTime1);
            else
                xTimeSpan = DateTime1.Subtract(DateTime2);
            int iDays = 5 + System.Convert.ToInt32(xTimeSpan.TotalDays);
            iReturn = (iDays / 7);
            return iReturn;
        }

        public ActionResult Export_Atn()
        {
            return Json(new
            {
                status = "success",
                data = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportAtnReport(int MonthId, int Year)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                DateTime startDate = new DateTime(Year, MonthId, 1);
                DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                var dates = new List<DateTime>();
                for (var dt = startDate; dt <= endDate; dt = dt.AddDays(1))
                {
                    dates.Add(dt);
                }

                var headlist = new List<string>();
                for (var i = 0; i <= dates.Count; i++)
                {
                    if (i == 0)
                        headlist.Add("Teacher Name");
                    else
                        headlist.Add(i.ToString());
                }

                var allstaffattendances = _IStaffService.GetAllStaffAttendances(ref count).ToList();
                var allTeachers = _IStaffService.GetAllStaffs(ref count).Where(p => p.StaffType.StaffType1.ToLower() == "teacher" && p.IsActive == true).ToList();

                var attendancelist = new List<AttendanceReport>();
                foreach (var item in allTeachers)
                {
                    var AttendanceReport = new AttendanceReport();
                    AttendanceReport.TeacherName = item.FName + " " + item.LName;
                    foreach (var date in dates)
                    {
                        var TeacherStatus = "";
                        var atnstatus = allstaffattendances.Where(p => p.StaffId == item.StaffId && p.AttendanceDate == date).FirstOrDefault();
                        if (atnstatus != null)
                            TeacherStatus = atnstatus.StaffAttendanceStatu.AttendanceAbbr;

                        AttendanceReport.AttendanceStatus.Add(TeacherStatus);
                    }
                    attendancelist.Add(AttendanceReport);
                }


                DataTable dtt = new DataTable();

                foreach (var item in attendancelist)
                {
                    dtt.Columns.Add();
                    dtt.Rows.Add(item.AttendanceStatus.ToArray());
                }

                if (dtt.Rows.Count > 0)
                {
                    string title = "StaffAttendaceReport";

                    //if (IsExcel == "1")
                    //{
                    _IExportHelper.ExportToExcel(dtt, Title: title,HeadingName:"Staff Attendace Report");
                    //}
                    //else if (IsExcel == "2")
                    //{
                    //    _IExportHelper.ExportToPrint(dtt, Title: title);
                    //}
                    //else
                    //{
                    //    _IExportHelper.ExportToPDF(dtt, Title: title);
                    //}
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                },
                    JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #endregion

        #region
        public ActionResult StaffLeaveReport()
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "StaffAttendanceReport", "View");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            var model = new StaffLeaveListReport();
            var monthsDropdown = new List<SelectListItem>();
            monthsDropdown.Add(new SelectListItem { Selected = false, Text = "--Select--", Value = "" });
            monthsDropdown.Add(new SelectListItem { Selected = false, Text = "January", Value = "1" });
            monthsDropdown.Add(new SelectListItem { Selected = false, Text = "February", Value = "2" });
            monthsDropdown.Add(new SelectListItem { Selected = false, Text = "March", Value = "3" });
            monthsDropdown.Add(new SelectListItem { Selected = false, Text = "April", Value = "4" });
            monthsDropdown.Add(new SelectListItem { Selected = false, Text = "May", Value = "5" });
            monthsDropdown.Add(new SelectListItem { Selected = false, Text = "June", Value = "6" });
            monthsDropdown.Add(new SelectListItem { Selected = false, Text = "July", Value = "7" });
            monthsDropdown.Add(new SelectListItem { Selected = false, Text = "August", Value = "8" });
            monthsDropdown.Add(new SelectListItem { Selected = false, Text = "September", Value = "9" });
            monthsDropdown.Add(new SelectListItem { Selected = false, Text = "October", Value = "10" });
            monthsDropdown.Add(new SelectListItem { Selected = false, Text = "November", Value = "11" });
            monthsDropdown.Add(new SelectListItem { Selected = false, Text = "December", Value = "12" });
            model.MonthList = monthsDropdown;


            var GetAllStafftypeList = _IStaffService.GetAllStaffTypes(ref count).ToList();
            if (GetAllStafftypeList.Count > 0)
            {
                model.StaffTypelist.Add(new SelectListItem { Selected = false, Value = "0", Text = "--Select--" });
                foreach (var item in GetAllStafftypeList)
                    model.StaffTypelist.Add(new SelectListItem { Value = item.StaffTypeId.ToString(), Text = item.StaffType1 });
            }
            //model.StaffList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
            var allteachingstaffs = _IStaffService.GetAllStaffs(ref count).Where(p => p.IsActive == true).ToList();
            if (allteachingstaffs.Count > 0)
            {
                foreach (var item in allteachingstaffs)
                    model.StaffList.Add(new SelectListItem { Value = item.StaffId.ToString(), Text = item.FName + " " + item.LName });
            }
            return View(model);
        }
        #endregion

        #region

        public ActionResult GenerateStaffLeaveReport(int MonthId, int Year, int[] StafIds)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                //DateTime startDate = new DateTime(Year, MonthId, 1);
                //DateTime endDate = startDate.AddMonths(1).AddDays(-1);
                //var dates = new List<DateTime>();
                //for (var dt = startDate; dt <= endDate; dt = dt.AddDays(1))
                //    dates.Add(dt);

                var model = new StaffLeaveListReport();
                var allstaffleaves = _IStaffService.GetAllStaffLeaveApplication(ref count).Where(p => p.IsApproved == true).ToList();
                var allstaffs = _IStaffService.GetAllStaffs(ref count).ToList();
                foreach (var item in StafIds)
                {
                    var data = new StaffLeaveReport();

                    data.Count = allstaffleaves.Where(p => ((p.LeaveFrom.Value.Month == MonthId && p.LeaveFrom.Value.Year == Year) || (p.LeaveTill.Value.Month == MonthId && p.LeaveTill.Value.Year == Year)) && p.StaffId == item).Count().ToString();
                    data.Name = allstaffs.Where(p => p.StaffId == item).Select(p => p.FName + " " + p.LName).FirstOrDefault();
                    data.StaffId = item.ToString();
                    //data.Date = allstaffleaves.Where((p => p.LeaveFrom.Value.Month == MonthId && p.LeaveTill.Value.Year == Year)).ToString();
                    model.DataList.Add(data);

                    //if (reqstaffleaves.Count > 0)
                    //{
                    //    foreach (var itm in reqstaffleaves)
                    //    {
                    //        var data = new StaffLeaveReport();
                    //        data.ApprovedBy = "Admin";
                    //        data.Date = item.Date.ToString("dd/MM/yyyy");
                    //        data.LeaveType = itm.StaffAttendanceStatu.AttendanceStatus;
                    //        data.Reason = itm.Reason;
                    //    }
                    //}
                }

                if (model.DataList.Count > 0)
                    return Json(new { status = "success", data = model.DataList });
                else
                    return Json(new { status = "failed", data = "No Record Found " });

                return null;
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }
        #endregion

        #region
        public ActionResult GetAllStaffNames(int stafftypid)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                //var model = new StaffLeaveListReport();
                var stafflist = new List<SelectListItem>();

                var allstaffs = _IStaffService.GetAllStaffs(ref count).ToList();
                if (allstaffs.Count > 0)
                {
                    if (stafftypid > 0)
                    {
                        var AllStaffByType = allstaffs.Where(p => p.StaffTypeId == stafftypid).ToList();
                        if (AllStaffByType.Count > 0)
                        {
                            foreach (var item in AllStaffByType)
                                stafflist.Add(new SelectListItem { Selected = false, Value = item.StaffId.ToString(), Text = item.FName + " " + item.LName });
                        }

                    }
                    else
                    {
                        var AllStaffByType = allstaffs;
                        if (AllStaffByType.Count > 0)
                        {
                            foreach (var item in AllStaffByType)
                                stafflist.Add(new SelectListItem { Selected = false, Value = item.StaffId.ToString(), Text = item.FName + " " + item.LName });
                        }
                    }
                    return Json(new { status = "success", data = stafflist });
                }
                else
                    return Json(new { status = "failed", data = "No data found." });

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    p3 = ex.Message
                });
            }

        }
        #endregion

        public ActionResult StaffLeaveReportBystaffid(int MonthId, int Year, int StafIds)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                var model = new StaffLeaveListReport();
                var allstaffleaves = _IStaffService.GetAllStaffLeaveApplication(ref count).Where(p => p.IsApproved == true).ToList();
                var reqstaffleaves = allstaffleaves.Where(p => (p.LeaveFrom.Value.Month == MonthId || p.LeaveFrom.Value.Month == MonthId) && (p.LeaveTill.Value.Year == Year && p.LeaveTill.Value.Year == Year) && p.StaffId == StafIds).ToList();
                if (reqstaffleaves.Count > 0)
                {
                    foreach (var itm in reqstaffleaves)
                    {
                        var data = new StaffLeaveDetail();
                        data.ApprovedBy = "Admin";
                        data.Date = itm.LeaveFrom.Value.ToString("dd/MM/yyyy") + "  To  " + itm.LeaveTill.Value.ToString("dd/MM/yyyy");
                        data.LeaveType = itm.StaffAttendanceStatu.AttendanceStatus;
                        data.Reason = itm.Reason;
                        model.DetailList.Add(data);
                    }
                }


                if (model.DetailList.Count > 0)
                    return Json(new { status = "success", data = model.DetailList });
                else
                    return Json(new { status = "failed", data = "No Record Found " });

                return null;
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }



        public ActionResult StaffAttendanceView(FormCollection Form)
        {
             StudentAttendance model = new StudentAttendance();
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                var weekendOff = _ISchoolSettingService.GetorSetSchoolData("SaturdayOff", "2, 4").AttributeValue;
                var AllEventList = _ISchedularService.GetAllEventList(ref count);
                var AllEventTypes = _ISchedularService.GetAllEventTypeList(ref count);
                var AllAttendances = _IStaffService.GetAllStaffAttendances(ref count);
                var AllcolorCodes = _ICommonMethodService.GetAllColorCodes(ref count);

                model.TotalAbsent = 0;
                model.TotalLeave = 0;
                model.TotalNotMarked = 0;
                var IsIncludeAttendanceOnHoliday = _ISchoolSettingService.GetorSetSchoolData("IncludeAttendanceOnHoliday", "1").AttributeValue == "1" ? true : false;
                // check user exist and find User info
                if (user != null && (user.ContactType.ContactType1.ToLower() == "teacher" || user.ContactType.ContactType1.ToLower() == "non-teaching"))
                {
                    var staff = new Staff();
                    staff = _IStaffService.GetStaffById((int)user.UserContactId);

                    // Entrance Class
                    var currentSession = _ICatalogMasterService.GetAllSessions(ref count, Isdeafult: true).FirstOrDefault();
                    var Curentdate = DateTime.UtcNow.Date;
                    var allHolidaysforattendance = AllEventList.Where(p => p.IsActive == true && p.IsDeleted == false
                                                      && p.EventType.EventType1 == "Holiday" && p.StartDate >= currentSession.StartDate
                                                      && (p.EndDate <= Curentdate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();
                    model.WorkingTotalDays = 0;
                    // Sundays count
                    var Sundaycount = 0;
                    var sundayslist = _ICatalogMasterService.GetSundayCountBetweenDates(ref Sundaycount, currentSession.StartDate.Value, Curentdate);
                    // calculate attendance %
                    TimeSpan difference = Curentdate.AddDays(1) - currentSession.StartDate.Value;
                    int totaldays = difference.Days - (Convert.ToInt32(allHolidaysforattendance.Count) +
                                                                     Convert.ToInt32(sundayslist.Count));
                    model.Attendance = "";
                    if (staff.StaffId > 0)
                    {
                        model.Name = staff.FName + " " + staff.LName;
                        model.EmpCode = staff.EmpCode;

                        var doctypeid = _IStaffService.GetAllStaffDocumentTypes(ref count, "Image").FirstOrDefault();
                        var image = _IStaffService.GetAllStaffDocuments(ref count, doctypeid.DocumentTypeId,
                                                                            StaffId: staff.StaffId).FirstOrDefault();
                        var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                        if (image != null)
                        {
                            if (image.Image.ToString().Contains("_image"))
                            {
                                model.StudentImage = _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/Teacher/Photos/" + image.Image;
                            }

                        }
                        if (string.IsNullOrEmpty(model.StudentImage) && staff.GenderId != null && staff.GenderId > 0)
                        {
                            string defaultmaleimg = _WebHelper.GetStoreLocation() + "Images/male-avtar.png";
                            string defaultFemaleimg = _WebHelper.GetStoreLocation() + "Images/Female-avtar.png";
                            //check user gender

                            var gender = _ICatalogMasterService.GetGenderById((int)staff.GenderId).Gender1;
                            var defaultpic = defaultmaleimg;
                            if (gender.ToLower().Contains("female"))
                            {
                                defaultpic = defaultFemaleimg;
                            }
                            model.StudentImage = defaultpic;
                        }

                        model.TotalPresents = 0;
                        var AllAttendanceStatus = _IStaffService.GetStaffAllAttendanceStatus(ref count);
                        //monthsCount
                        model.monthscount = 0;
                        int monthcont = model.monthscount;
                        model.absentcount = 0;
                        var Monthslist = _ICatalogMasterService.GetMonthCountBetweenDates(ref monthcont, currentSession.StartDate.Value, Curentdate);
                        var StaffAttendance = new ViewModel.ViewModel.Student.AppStudentAttendanceModel();
                        IList<ViewModel.ViewModel.Student.AppStudentAttendanceModel> monthlyAttendance = new List<ViewModel.ViewModel.Student.AppStudentAttendanceModel>();
                        ViewModel.ViewModel.Student.AppMothlyAttendanceStatusModel monthwiseAttendance = new ViewModel.ViewModel.Student.AppMothlyAttendanceStatusModel();
                        var EventTypeId = AllEventTypes.Where(m => m.EventType1.ToLower().Contains("holiday")).FirstOrDefault().EventTypeId;
                        //get value from schoolsetting table
                        foreach (var ml in Monthslist)
                        {
                            StaffAttendance = new ViewModel.ViewModel.Student.AppStudentAttendanceModel();
                            model.absentcount = 0;
                            //months start and end dates
                            var firstDayOfMonth = new DateTime(ml.Date.Year, ml.Date.Month, 1); // first day of month
                            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1); // last day of month


                            TimeSpan diff = lastDayOfMonth.AddDays(1) - firstDayOfMonth;
                            // calculate attendance %
                            if (ml.Month == Curentdate.Month)
                                diff = Curentdate.AddDays(1) - firstDayOfMonth;


                            var holidaycount = AllEventList.Where(m => m.StartDate.Value.Month == ml.Date.Month
                                                        && m.EventTypeId == EventTypeId && m.IsDeleted == false).Count();
                            int days = (diff.Days);

                            StaffAttendance.Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(ml.Month);
                            StaffAttendance.Month = StaffAttendance.Month.Substring(0, 3);
                            var absentrecords = AllAttendances.Where(a => a.StaffId == staff.StaffId
                                                            && a.StaffAttendanceStatu.AttendanceAbbr == "A"
                                                            && a.AttendanceDate >= firstDayOfMonth
                                                            && a.AttendanceDate <= lastDayOfMonth).ToList();
                            StaffAttendance.Absent = Convert.ToString(absentrecords.Count());

                            //  int diffDays = 0;
                            //int Dins = 0;
                            var SaturdayCount = 0;
                            decimal Presents = 0;
                            int LeaveCount = 0;
                            int NotMarkedCount = 0;
                            int OnDutyCount = 0;
                            var IsHoliday = false;
                            for (DateTime i = firstDayOfMonth; i <= lastDayOfMonth; i = i.AddDays(1))
                            {
                                IsHoliday = false;
                                var colorCodes = AllcolorCodes.ToList();
                                monthwiseAttendance = new ViewModel.ViewModel.Student.AppMothlyAttendanceStatusModel();
                                monthwiseAttendance.Date = ConvertDate(i);
                                // check sunday
                                var dayno = i.DayOfWeek;
                                var day = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(dayno);

                                // check holiday changes 05April2018
                                EventTypeId = AllEventTypes.Where(m => m.EventType1.ToLower().Contains("holiday")).FirstOrDefault().EventTypeId;
                                var Event = AllEventList.Where(n => n.StartDate == i && n.IsActive == true && n.IsDeleted == false
                                                                                  && n.EventTypeId == EventTypeId).ToList();
                                // check attendance
                                // get attendance by staff
                                var staffattendanceAbbr = AllAttendances.Where(a => a.StaffId == staff.StaffId
                                                                        && a.AttendanceDate == i)
                                                                        .OrderByDescending(x => x.StaffAttendanceId)
                                                                        .FirstOrDefault();

                                monthwiseAttendance.Status = staffattendanceAbbr != null ?
                                                             staffattendanceAbbr.StaffAttendanceStatu.AttendanceAbbr : "";


                                var saturdayOffs = weekendOff.Split(',');
                                //if staff is present on sunday then it will show P
                                if (day == "Sunday")
                                {
                                    IsHoliday = true;
                                    if (monthwiseAttendance.Status != "P" || staffattendanceAbbr == null)
                                    {
                                        monthwiseAttendance.Status = "Sunday";
                                    }
                                }
                                else if (Event.Count > 0)
                                {
                                    IsHoliday = true;
                                    if (monthwiseAttendance.Status != "P" || staffattendanceAbbr == null)
                                    {
                                        monthwiseAttendance.Status = "Holiday";
                                    }
                                }
                                else if (day == "Saturday")
                                {
                                    SaturdayCount++;
                                    if (monthwiseAttendance.Status != "P" || staffattendanceAbbr == null)
                                    {
                                        monthwiseAttendance.Status = "Not marked";
                                        foreach (var weekend in saturdayOffs)
                                        {
                                            if (Convert.ToInt32(weekend) == SaturdayCount)
                                            {
                                                IsHoliday = true;
                                                monthwiseAttendance.Status = "Saturday Off";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var weekend in saturdayOffs)
                                        {
                                            if (Convert.ToInt32(weekend) == SaturdayCount)
                                            {
                                                IsHoliday = true;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    monthwiseAttendance.Status = "Not marked";
                                    if (staffattendanceAbbr != null)
                                    {
                                        if (!string.IsNullOrEmpty(staffattendanceAbbr.StaffAttendanceStatu.AttendanceAbbr))
                                        {
                                            monthwiseAttendance.Status = staffattendanceAbbr.StaffAttendanceStatu.AttendanceAbbr;
                                            model.absentcount += 0;
                                        }
                                        else
                                            model.absentcount += 1;
                                    }
                                }

                                // if current month day exceed the current date
                                if (i > Curentdate)
                                    break;

                                if (IsIncludeAttendanceOnHoliday == false)
                                {
                                    if (!IsHoliday)
                                    {
                                        if (monthwiseAttendance.Status == "P")
                                            Presents++;

                                        if (monthwiseAttendance.Status == "CL" || monthwiseAttendance.Status == "SL" || monthwiseAttendance.Status == "HD")
                                            LeaveCount++;

                                        if (monthwiseAttendance.Status == "OD")
                                        {
                                            var odstatus = AllAttendanceStatus.Where(f => f.AttendanceAbbr.ToLower() == "od").FirstOrDefault();
                                            if (odstatus != null && odstatus.AttendanceCount != null)
                                            {
                                                Presents = Presents + (decimal)odstatus.AttendanceCount;
                                                // OnDutyCount=OnDutyCount + Convert.ToInt32(odstatus.AttendanceCount);
                                            }

                                        }

                                        if (monthwiseAttendance.Status == "Not marked")
                                            NotMarkedCount++;
                                    }
                                }
                                else
                                {
                                    if (monthwiseAttendance.Status == "P")
                                        Presents++;

                                    if (monthwiseAttendance.Status == "CL" || monthwiseAttendance.Status == "SL" || monthwiseAttendance.Status == "HD")
                                        LeaveCount++;

                                    if (monthwiseAttendance.Status == "OD")
                                    {
                                        var odstatus = AllAttendanceStatus.Where(f => f.AttendanceAbbr.ToLower() == "od").FirstOrDefault();
                                        if (odstatus != null && odstatus.AttendanceCount != null)
                                        {
                                            Presents = Presents + (decimal)odstatus.AttendanceCount;
                                            // OnDutyCount=OnDutyCount + Convert.ToInt32(odstatus.AttendanceCount);
                                        }

                                    }

                                    if (monthwiseAttendance.Status == "Not marked")
                                        NotMarkedCount++;
                                }
                                StaffAttendance.AttendanceList.Add(monthwiseAttendance);
                            }

                            StaffAttendance.Others = Convert.ToString(NotMarkedCount);
                            StaffAttendance.Leave = Convert.ToString(LeaveCount);
                            StaffAttendance.Present = decimal.Parse(Presents.ToString()).ToString("G29");
                            StaffAttendance.OnDuty = Convert.ToString(OnDutyCount);
                            //In a month totalworking days with minus holiday/weekendoff/sundays/vacations is someone not present
                            //        string SDays = (days - diffDays).ToString().Replace("-", "");
                            //StaffAttendance.Total = Convert.ToString(SDays);

                            //In a year totalworking days with minus holiday/weekendoff/sundays/vacations 
                            StaffAttendance.Total = (Presents + LeaveCount + NotMarkedCount + (Convert.ToInt32(StaffAttendance.Absent) +
                                    model.absentcount)).ToString();
                            StaffAttendance.Total = decimal.Parse(StaffAttendance.Total).ToString("G29");
                            //WorkingTotalDays += (days - diffDays);

                            model.WorkingTotalDays += Convert.ToDecimal(StaffAttendance.Total);
                            StaffAttendance.Absent = (Convert.ToInt32(StaffAttendance.Absent) + model.absentcount).ToString();
                            model.monthlyAttendance.Add(StaffAttendance);
                            model.TotalOnDuty += OnDutyCount;
                            model.TotalLeave += LeaveCount;
                            model.TotalNotMarked += NotMarkedCount;
                            model.TotalAbsent += Convert.ToInt32(StaffAttendance.Absent);
                            model.TotalPresents += Presents;
                        }
                        model.Attendance = model.TotalPresents.ToString() + "/" + model.WorkingTotalDays;
                        return View(model);
                    }
                }
                else if (user != null && user.ContactType.ContactType1.ToLower() == "admin")
                {
                    int id = 0;
                    string strstaffId = Form["staffid"];
                    if (!string.IsNullOrEmpty(strstaffId))
                        id = Convert.ToInt32(strstaffId);
                    model.IsAdmin = true;
                    var allstaffs = _IStaffService.GetAllStaffs(ref count, IsActive: true).ToList();
                    model.StaffList.Add(new SelectListItem { Selected = true, Value = "", Text = "--Select Staff--" });
                    foreach (var stf in allstaffs.OrderBy(f => f.FName))
                        model.StaffList.Add(new SelectListItem { Selected = false, Value = stf.StaffId.ToString(), Text = stf.FName + " " + stf.LName });
                    if (id > 0)
                    {
                        model.staffid = id;
                        var staff = new Staff();
                        staff = _IStaffService.GetStaffById(id);

                        // Entrance Class
                        var currentSession = _ICatalogMasterService.GetAllSessions(ref count, Isdeafult: true).FirstOrDefault();
                        var Curentdate = DateTime.UtcNow.Date;
                        var allHolidaysforattendance = AllEventList.Where(p => p.IsActive == true && p.IsDeleted == false
                                                          && p.EventType.EventType1 == "Holiday" && p.StartDate >= currentSession.StartDate
                                                          && (p.EndDate <= Curentdate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();
                        model.WorkingTotalDays = 0;
                        // Sundays count
                        var Sundaycount = 0;
                        var sundayslist = _ICatalogMasterService.GetSundayCountBetweenDates(ref Sundaycount, currentSession.StartDate.Value, Curentdate);
                        // calculate attendance %
                        TimeSpan difference = Curentdate.AddDays(1) - currentSession.StartDate.Value;
                        int totaldays = difference.Days - (Convert.ToInt32(allHolidaysforattendance.Count) +
                                                                         Convert.ToInt32(sundayslist.Count));
                        model.Attendance = "";
                        if (staff.StaffId > 0)
                        {
                            model.TotalPresents = 0;
                            var AllAttendanceStatus = _IStaffService.GetStaffAllAttendanceStatus(ref count);
                            //monthsCount
                            model.monthscount = 0;
                            int monthcont = model.monthscount;
                            model.absentcount = 0;
                            var Monthslist = _ICatalogMasterService.GetMonthCountBetweenDates(ref monthcont, currentSession.StartDate.Value, Curentdate);
                            var StaffAttendance = new ViewModel.ViewModel.Student.AppStudentAttendanceModel();
                            IList<ViewModel.ViewModel.Student.AppStudentAttendanceModel> monthlyAttendance = new List<ViewModel.ViewModel.Student.AppStudentAttendanceModel>();
                            ViewModel.ViewModel.Student.AppMothlyAttendanceStatusModel monthwiseAttendance = new ViewModel.ViewModel.Student.AppMothlyAttendanceStatusModel();
                            var EventTypeId = AllEventTypes.Where(m => m.EventType1.ToLower().Contains("holiday")).FirstOrDefault().EventTypeId;
                            //get value from schoolsetting table
                            foreach (var ml in Monthslist)
                            {
                                StaffAttendance = new ViewModel.ViewModel.Student.AppStudentAttendanceModel();
                                model.absentcount = 0;
                                //months start and end dates
                                var firstDayOfMonth = new DateTime(ml.Date.Year, ml.Date.Month, 1); // first day of month
                                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1); // last day of month


                                TimeSpan diff = lastDayOfMonth.AddDays(1) - firstDayOfMonth;
                                // calculate attendance %
                                if (ml.Month == Curentdate.Month)
                                    diff = Curentdate.AddDays(1) - firstDayOfMonth;


                                var holidaycount = AllEventList.Where(m => m.StartDate.Value.Month == ml.Date.Month
                                                            && m.EventTypeId == EventTypeId && m.IsDeleted == false).Count();
                                int days = (diff.Days);

                                StaffAttendance.Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(ml.Month);
                                StaffAttendance.Month = StaffAttendance.Month.Substring(0, 3);
                                var absentrecords = AllAttendances.Where(a => a.StaffId == staff.StaffId
                                                                && a.StaffAttendanceStatu.AttendanceAbbr == "A"
                                                                && a.AttendanceDate >= firstDayOfMonth
                                                                && a.AttendanceDate <= lastDayOfMonth).ToList();
                                StaffAttendance.Absent = Convert.ToString(absentrecords.Count());

                                int diffDays = 0;
                                //int Dins = 0;
                                var SaturdayCount = 0;
                                decimal Presents = 0;
                                int LeaveCount = 0;
                                int NotMarkedCount = 0;
                                int OnDutyCount = 0;
                                var IsHoliday = false;
                                for (DateTime i = firstDayOfMonth; i <= lastDayOfMonth; i = i.AddDays(1))
                                {
                                    IsHoliday = false;
                                    monthwiseAttendance = new ViewModel.ViewModel.Student.AppMothlyAttendanceStatusModel();
                                    monthwiseAttendance.Date = ConvertDate(i);
                                    // check sunday
                                    var dayno = i.DayOfWeek;
                                    var day = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(dayno);

                                    // check holiday changes 05April2018
                                    EventTypeId = AllEventTypes.Where(m => m.EventType1.ToLower().Contains("holiday")).FirstOrDefault().EventTypeId;
                                    var Event = AllEventList.Where(n => n.StartDate == i && n.IsActive == true && n.IsDeleted == false
                                                                                      && n.EventTypeId == EventTypeId).ToList();
                                    // check attendance
                                    // get attendance by staff
                                    var staffattendanceAbbr = AllAttendances.Where(a => a.StaffId == staff.StaffId
                                                                            && a.AttendanceDate == i)
                                                                            .OrderByDescending(x => x.StaffAttendanceId)
                                                                            .FirstOrDefault();

                                    monthwiseAttendance.Status = staffattendanceAbbr != null ?
                                                                 staffattendanceAbbr.StaffAttendanceStatu.AttendanceAbbr : "";


                                    var saturdayOffs = weekendOff.Split(',');
                                    //if staff is present on sunday then it will show P
                                    if (day == "Sunday")
                                    {
                                        IsHoliday = true;
                                        if (monthwiseAttendance.Status != "P" || staffattendanceAbbr == null)
                                        {
                                            monthwiseAttendance.Status = "Sunday";
                                        }
                                    }
                                    else if (Event.Count > 0)
                                    {
                                        IsHoliday = true;
                                        if (monthwiseAttendance.Status != "P" || staffattendanceAbbr == null)
                                        {
                                            monthwiseAttendance.Status = "Holiday";
                                        }
                                    }
                                    else if (day == "Saturday")
                                    {
                                        SaturdayCount++;
                                        if (monthwiseAttendance.Status != "P" || staffattendanceAbbr == null)
                                        {
                                            monthwiseAttendance.Status = "Not marked";
                                            foreach (var weekend in saturdayOffs)
                                            {
                                                if (Convert.ToInt32(weekend) == SaturdayCount)
                                                {
                                                    IsHoliday = true;
                                                    monthwiseAttendance.Status = "Saturday Off";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            foreach (var weekend in saturdayOffs)
                                            {
                                                if (Convert.ToInt32(weekend) == SaturdayCount)
                                                {
                                                    IsHoliday = true;
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        monthwiseAttendance.Status = "Not marked";
                                        if (staffattendanceAbbr != null)
                                        {
                                            if (!string.IsNullOrEmpty(staffattendanceAbbr.StaffAttendanceStatu.AttendanceAbbr))
                                            {
                                                monthwiseAttendance.Status = staffattendanceAbbr.StaffAttendanceStatu.AttendanceAbbr;
                                                model.absentcount += 0;
                                            }
                                            else
                                                model.absentcount += 1;
                                        }
                                    }

                                    // if current month day exceed the current date
                                    if (i > Curentdate)
                                        break;

                                    if (IsIncludeAttendanceOnHoliday == false)
                                    {
                                        if (!IsHoliday)
                                        {
                                            if (monthwiseAttendance.Status == "P")
                                                Presents++;

                                            if (monthwiseAttendance.Status == "CL" || monthwiseAttendance.Status == "SL" || monthwiseAttendance.Status == "HD")
                                                LeaveCount++;

                                            if (monthwiseAttendance.Status == "OD")
                                            {
                                                var odstatus = AllAttendanceStatus.Where(f => f.AttendanceAbbr.ToLower() == "od").FirstOrDefault();
                                                if (odstatus != null && odstatus.AttendanceCount != null)
                                                {
                                                    Presents = Presents + (decimal)odstatus.AttendanceCount;
                                                    // OnDutyCount=OnDutyCount + Convert.ToInt32(odstatus.AttendanceCount);
                                                }
                                            }

                                            if (monthwiseAttendance.Status == "Not marked")
                                                NotMarkedCount++;
                                        }
                                    }
                                    else
                                    {
                                        if (monthwiseAttendance.Status == "P")
                                            Presents++;

                                        if (monthwiseAttendance.Status == "CL" || monthwiseAttendance.Status == "SL" || monthwiseAttendance.Status == "HD")
                                            LeaveCount++;

                                        if (monthwiseAttendance.Status == "OD")
                                        {
                                            var odstatus = AllAttendanceStatus.Where(f => f.AttendanceAbbr.ToLower() == "od").FirstOrDefault();
                                            if (odstatus != null && odstatus.AttendanceCount != null)
                                            {
                                                Presents = Presents + (decimal)odstatus.AttendanceCount;
                                                // OnDutyCount=OnDutyCount + Convert.ToInt32(odstatus.AttendanceCount);
                                            }
                                        }

                                        if (monthwiseAttendance.Status == "Not marked")
                                            NotMarkedCount++;
                                    }

                                    StaffAttendance.AttendanceList.Add(monthwiseAttendance);
                                }

                                StaffAttendance.Others = Convert.ToString(NotMarkedCount);
                                StaffAttendance.Leave = Convert.ToString(LeaveCount);
                                StaffAttendance.Present = decimal.Parse(Presents.ToString()).ToString("G29");
                                StaffAttendance.OnDuty = Convert.ToString(OnDutyCount);
                                //In a month totalworking days with minus holiday/weekendoff/sundays/vacations is someone not present
                                //  string SDays = (days - diffDays).ToString().Replace("-", "");
                                //StaffAttendance.Total = Convert.ToString(SDays);

                                //In a year totalworking days with minus holiday/weekendoff/sundays/vacations 
                                StaffAttendance.Total = (Presents + LeaveCount + NotMarkedCount + (Convert.ToInt32(StaffAttendance.Absent) +
                                        model.absentcount)).ToString();
                                StaffAttendance.Total = decimal.Parse(StaffAttendance.Total).ToString("G29");
                                //WorkingTotalDays += (days - diffDays);

                                model.WorkingTotalDays += Convert.ToDecimal(StaffAttendance.Total);
                                StaffAttendance.Absent = (Convert.ToInt32(StaffAttendance.Absent) + model.absentcount).ToString();
                                model.monthlyAttendance.Add(StaffAttendance);
                                model.TotalOnDuty += OnDutyCount;
                                model.TotalLeave += LeaveCount;
                                model.TotalNotMarked += NotMarkedCount;
                                model.TotalAbsent += Convert.ToInt32(StaffAttendance.Absent);
                                model.TotalPresents += Presents;
                            }
                            model.Attendance = model.TotalPresents.ToString() + "/" + model.WorkingTotalDays;
                        }
                    }
                    return View(model);
                }

                return RedirectToAction("Login", "Home");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult UpdateCloseDateOfStaffBankDetail(int StaffBankDetailId ,string EndDate)
        {
            try
            {
                if (StaffBankDetailId > 0 && !string.IsNullOrEmpty(EndDate))
                {
                    var StaffBankDetail = _IStaffService.GetStaffBankDetailById(StaffBankDetailId);
                    if (!string.IsNullOrEmpty(EndDate))
                    {
                        DateTime date = DateTime.ParseExact(EndDate, "dd/MM/yyyy", null);
                        StaffBankDetail.ToDate = date;
                        _IStaffService.UpdateStaffBankDetail(StaffBankDetail);
                        return Json(new { status = "success", data = "Bank Detail Updated SuccessFully" });
                    }
                }
                return Json(new { status = "failed", data = "Invalid Bank Detail" });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }
    }


  

}


