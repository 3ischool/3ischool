﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using KS_SmartChild.BAL.Common;
using System.Web.Security;
using KS_SmartChild.Models;
using System.Web.Configuration;
using KS_SmartChild.DAL.ErrorLogModule;
using KS_SmartChild.DAL.Common;
using KS_SmartChild.DAL.Security;
using KS_SmartChild.BAL.Security;
using KS_SmartChild.BAL;
using System.Text;
using System.Security.Cryptography;
using System.Data.Entity.Validation;
using KS_SmartChild.ViewModel.API;
using System.Globalization;
using KS_SmartChild.DAL.Schedular;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.IO;
using KS_SmartChild.ViewModel.Transport;
using KS_SmartChild.DAL.CatalogMaster;
using KS_SmartChild.DAL.ClassPeriodModule;

namespace KS_SmartChild.Controllers
{
    public class APIController : Controller
    {
        #region fields
        public readonly int maxFileSize = 1024;
        public int count = 0;
        public readonly ICustomEncryption _ICustomEncryption;
        private readonly IWebHelper _WebHelper;
        private readonly IEncryptionService _IEncryptionService;
        private readonly IMainService _IMainService;
        private readonly ISMSSender _ISMSSender;
        private readonly IUserAuthorizationService _IUserAuthorizationService;

        public class domainname
        {
            public string Domain { get; set; }
        }

        #endregion

        #region ctor

        public APIController(ICustomEncryption ICustomEncryption,
            IWebHelper WebHelper,
            IEncryptionService IEncryptionService,
            IMainService IMainService,
            ISchedularService ISchedularService,
            ISMSSender ISMSSender,
            IUserAuthorizationService IUserAuthorizationService
          )
        {
            this._ICustomEncryption = ICustomEncryption;
            this._WebHelper = WebHelper;
            this._IEncryptionService = IEncryptionService;
            this._IMainService = IMainService;
            this._ISMSSender = ISMSSender;
            this._IUserAuthorizationService = IUserAuthorizationService;

        }

        #endregion

        #region Methods

        public string ConvertTime(TimeSpan time)
        {
            TimeSpan timespan = new TimeSpan(time.Hours, time.Minutes, time.Seconds);
            DateTime datetime = DateTime.Today.Add(timespan);
            string sdisplayTime = datetime.ToString("hh:mm tt");
            return sdisplayTime;
        }
        //get all dates for a month for the year specified
        public static List<DateTime> GetDates(int year, int month)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(year, month))
            .Select(day => new DateTime(year, month, day))
            .ToList();
        }
        public string FormatDate(string Date)
        {
            string sdisplayTime = Date.Trim('"');
            var dd = Date.Split('-');
            sdisplayTime = dd[1] + "-" + dd[0] + "-" + dd[2];
            return sdisplayTime;
        }

        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }

        public string ConvertDateTime(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy hh:mm tt");
            return sdisplayTime;
        }

        public bool IsTokenValid(string APIToken)
        {
            // check api token valid
            if (Request.Url.AbsoluteUri.Contains("localhost"))
                return true;
            else
            {
                var crntdate = DateTime.UtcNow;
                var istdate = TimeZoneInfo.ConvertTimeFromUtc(crntdate, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                var tokentime = istdate.Hour + istdate.Minute;
                var date = istdate.Date.ToString().Split('/')[1];
                var tokendatemonth = Convert.ToInt32(date) + Convert.ToInt32(istdate.Month);
                // decrypt api token
                var dectoken = _ICustomEncryption.base64d(APIToken);
                dectoken = dectoken.TrimStart('K').TrimStart('S').TrimStart('A').TrimStart('P').TrimStart('I');
                var requesttime = Convert.ToInt32(dectoken.Split('#')[1]);
                var requestslice = (dectoken.Split('#')[0]).ToString();
                var requestmonthdate = Convert.ToInt32(requestslice);
                if (tokendatemonth == requestmonthdate)
                {
                    if (requesttime >= tokentime && (tokentime + 5) >= requesttime) // check if token expires
                        return true;
                }
                return false;
            }
        }

        public Session GetCurrentSession(string User)
        {
            // database connection
            KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
            string DataSource = ConnectionService.Connectionstring(User);
            KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(DataSource);

            Session crntsession = new Session();
            DateTime? CurrentSessionFromDate;
            DateTime? CurrentSessionToDate;

            // today date
            var today = DateTime.UtcNow;
            if (today.Month > 3) // means new session start
            {
                CurrentSessionFromDate = Convert.ToDateTime("01-April-" + today.Year);
                CurrentSessionToDate = Convert.ToDateTime("31-March-" + (today.Year + 1));
                crntsession = db.Sessions.Where(s => s.StartDate == CurrentSessionFromDate && s.EndDate == CurrentSessionToDate).FirstOrDefault();
            }
            else // prev session
            {
                CurrentSessionFromDate = Convert.ToDateTime("01-April-" + (today.Year - 1));
                CurrentSessionToDate = Convert.ToDateTime("31-March-" + today.Year);
                crntsession = db.Sessions.Where(s => s.StartDate == CurrentSessionFromDate && s.EndDate == CurrentSessionToDate).FirstOrDefault();
            }

            return crntsession;
        }

        public bool IsUserAuthorize(KS_SmartChild.Models.KS_ChildEntities db, SchoolUser user, string AppForm, string Action)
        {
            // database connection
            bool IsAuth = false;
            // get user role 
            var userrolecontacttype = user.ContactType.UserRoleContactTypes.ToList();
            var userrole = userrolecontacttype.FirstOrDefault();
            if (userrole != null)
            {
                if (!userrole.UserRole.UserRole1.Contains("Admin"))
                {
                    var userroleId = userrole.UserRole.UserRoleId;
                    // get app form and action
                    var appforms = db.AppForms.ToList();
                    var appform = appforms.Where(a => a.AppForm1.ToLower().Contains(AppForm.ToLower())).FirstOrDefault();
                    if (appform != null)
                    {
                        if (Action.ToLower().Contains("view"))
                        {
                            // check user role permission
                            var userrolepermissions = db.UserRoleActionPermissions.Where(m => m.UserRoleId == userroleId && m.AppFormId == appform.AppFormId).ToList();
                            var isformpermission = userrolepermissions.Where(u => u.AppFormActionId == null).FirstOrDefault();
                            if (isformpermission != null)
                            {
                                if (isformpermission.IsForm == true && isformpermission.IsPermitted == true)
                                    IsAuth = true;
                            }
                        }
                        else
                        {
                            var appformactions = db.AppFormActions.Where(m => m.AppFormId == appform.AppFormId).ToList();
                            var appformaction = appformactions.Where(a => a.AppFormActionType.FormActionType.ToLower().Contains(Action.ToLower())).FirstOrDefault();
                            if (appformaction != null)
                            {
                                // check user role permission
                                var isformpermission = db.UserRoleActionPermissions.Where(m => m.UserRoleId == userroleId && m.AppFormId == appform.AppFormId && m.AppFormActionId == appformaction.AppFormActionId).FirstOrDefault();
                                if (isformpermission != null)
                                {
                                    if (isformpermission.IsPermitted != null && isformpermission.IsPermitted == true)
                                        IsAuth = true;
                                }
                            }
                        }
                    }
                }
                else
                    IsAuth = true;
            }


            return IsAuth;
        }

        public DatabaseModel AppDBConnection(HttpRequestBase Request)
        {
            string DataSource = string.Empty;
            string User = "";
            string Subdomain = Request.Url.AbsolutePath;
            Subdomain = Subdomain.Split('/')[1];
            // API url
            string MainUrl = "";
            string suburl = Convert.ToString(WebConfigurationManager.AppSettings["APISubUrl"]);
            string synccompany = Convert.ToString(WebConfigurationManager.AppSettings["syncapiname"]);
            string syncsub = Convert.ToString(WebConfigurationManager.AppSettings["syncsub"]);
            string syncext = Convert.ToString(WebConfigurationManager.AppSettings["syncext"]);
            string APIName = Convert.ToString(WebConfigurationManager.AppSettings["APIName"]);

            if (Request.Url.AbsoluteUri.Contains("localhost"))
            {
                User = Convert.ToString(WebConfigurationManager.AppSettings["APIUser"]);
                //MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["APIUrl"]) + "/" + APIName + "/" + suburl;
                //User = "stroop";
                //User = "Dev-KS_School";
                //User = "3is_demo";
                // User = "demo";
                //User = "kschild";
                //User = "bpsambala";
                //User = "hgdemo";
                //User = "dtsk8";
                //MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
                //MainUrl = "http://3ischools.com/api" + "/" + suburl;
                MainUrl = "http://api.digitech.co.in" + "/" + suburl;
            }
            else if (Request.Url.AbsoluteUri.Contains("digitech.com") || Request.Url.AbsoluteUri.Contains("172.16.1.2"))
            {
                User = "KsmartChild";
                // MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
                MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["APIUrl"]) + "/" + APIName + "/" + suburl;
            }
            else if (Request.Url.AbsoluteUri.Contains("digitech.co.in"))
            {
                User = Subdomain;
                // MainUrl = "http://api.digitech.co.in" + "/" + suburl;
                MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["APIUrl"]) + "/" + suburl;
            }
            else
            {
                User = Subdomain;
                //User = "bpsambala";
                // MainUrl = "http://3ischools.com/api" + "/" + suburl;
                MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["APIUrl"]) + "/" + suburl;
            }

            HttpClient client = new HttpClient();
            client.Timeout = new TimeSpan(0, 10, 1);
            MainUrl = MainUrl + "?DBName=" + User; // url with parameter
            client.DefaultRequestHeaders.Accept.Add(
            new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync(MainUrl).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            var IsDatabase = JsonConvert.DeserializeObject<APIDatabaseModel>(content);

            DatabaseModel model = new DatabaseModel();
            if (IsDatabase != null && !string.IsNullOrEmpty(IsDatabase.DBName))
            {
                // database connection
                KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                DataSource = ConnectionService.Connectionstring(IsDatabase.DBName);

                model.DataSource = DataSource;
                model.DBId = IsDatabase.DBId;
                model.DBName = IsDatabase.DBName;
            }

            return model;
        }

        #endregion

        #region Auth Apps Login and Sign Up APi's

        public JsonResult CheckUserAuth()
        {
            return Json(new
            {
                status = "failed",
                message = "Invalid request"
            }, JsonRequestBehavior.AllowGet);
        }

        // Post: API
        [HttpPost]
        public ActionResult CheckUserAuth(FormCollection form)
        {
            try
            {
                string APIToken = "", TableName = "", AuthKey = "";

                if (form["Token"] != null)
                    APIToken = Convert.ToString(form["Token"]);
                if (form["TableName"] != null)
                    TableName = Convert.ToString(form["TableName"]);
                if (form["AuthKey"] != null)
                    AuthKey = Convert.ToString(form["AuthKey"]);

                if (string.IsNullOrEmpty(APIToken))
                {
                    return Json(new
                    {
                        status = "failed",
                        message = "Token not found"
                    }, JsonRequestBehavior.AllowGet);
                }
                if (string.IsNullOrEmpty(TableName))
                {
                    return Json(new
                    {
                        status = "failed",
                        message = "TableName not found"
                    }, JsonRequestBehavior.AllowGet);
                }
                if (string.IsNullOrEmpty(AuthKey))
                {
                    return Json(new
                    {
                        status = "failed",
                        message = "Auth key invalid"
                    }, JsonRequestBehavior.AllowGet);
                }

                // current date time 
                // check api token valid
                var crntdate = DateTime.UtcNow;
                var istdate = TimeZoneInfo.ConvertTimeFromUtc(crntdate, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
                var tokentime = istdate.Hour + istdate.Minute;
                var date = istdate.Date.ToString().Split('/')[1];
                var tokendatemonth = Convert.ToInt32(date) + Convert.ToInt32(istdate.Month);
                // decrypt api token
                var dectoken = _ICustomEncryption.base64d(APIToken);
                dectoken = dectoken.TrimStart('K').TrimStart('S').TrimStart('A').TrimStart('P').TrimStart('I');
                var requesttime = Convert.ToInt32(dectoken.Split('#')[1]);
                var requestslice = (dectoken.Split('#')[0]).ToString();
                var requestmonthdate = Convert.ToInt32(requestslice);
                if (tokendatemonth == requestmonthdate)
                {
                    if (tokentime + 5 < requesttime) // check if token expires
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "Token expire"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        message = "Token expire"
                    }, JsonRequestBehavior.AllowGet);
                }

                // check auth key validate
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(AuthKey);
                if (authTicket != null && !authTicket.Expired)
                {
                    var roles = authTicket.UserData.Split(',');
                    HttpContext.User = new System.Security.Principal.GenericPrincipal(new FormsIdentity(authTicket), roles);
                    if (HttpContext.User.Identity.Name == "")
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "Auth expire"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        message = "Auth expire"
                    }, JsonRequestBehavior.AllowGet);
                }

                var dbdetail = AppDBConnection(Request);
                if (!string.IsNullOrEmpty(dbdetail.DBName))
                {
                    string User = Request.Url.AbsolutePath;
                    User = User.Split('/')[1];

                    KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                    // get user
                    SchoolUser user = new SchoolUser();
                    user = db.SchoolUsers.ToList().Where(u => u.UserName == HttpContext.User.Identity.Name && u.Status == true).FirstOrDefault();
                    if (user == null)
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);
                    }

                    // get user role 
                    IList<string> permissionlist = new List<string>();

                    var userrole = db.UserRoleContactTypes.ToList().Where(u => u.ContactTypeId == user.UserTypeId).FirstOrDefault();
                    if (userrole.UserRole.UserRole1 == "Administrator")
                    {
                        permissionlist.Add("Add Record");
                        permissionlist.Add("Edit Record");
                        permissionlist.Add("Delete Record");
                        permissionlist.Add("View Record");

                        return Json(new
                        {
                            status = "success",
                            message = "Valid",
                            permissions = permissionlist
                        }, JsonRequestBehavior.AllowGet);
                    }
                    // get app from by table name
                    var appform = db.AppForms.ToList().Where(a => a.AppForm1 == TableName).FirstOrDefault();
                    if (appform == null)
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "TableName not exist"
                        }, JsonRequestBehavior.AllowGet);
                    }

                    // check user role permission
                    var userrolepermission = db.UserRoleActionPermissions.ToList();
                    if (userrole != null)
                        userrolepermission = userrolepermission.Where(u => u.UserRoleId == userrole.UserRoleId).ToList();
                    if (appform != null)
                        userrolepermission = userrolepermission.Where(u => u.AppFormId == appform.AppFormId).ToList();

                    // check user has form permmission
                    var IsFormPermission = userrolepermission.Where(u => u.IsPermitted == true && u.IsForm == true).FirstOrDefault();
                    if (IsFormPermission == null)
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "false"
                        }, JsonRequestBehavior.AllowGet);
                    }

                    // get all permission list 
                    var userpermission = userrolepermission.Where(u => u.IsPermitted == true).ToList();
                    foreach (var item in userpermission)
                    {
                       // if (item.AppFormAction != null)
                          //  permissionlist.Add(item.AppFormAction.AppFormActionType.FormActionType);
                    }

                    return Json(new
                    {
                        status = "success",
                        message = "Valid",
                        permissions = permissionlist
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        message = "School not exist",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid request"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CheckValidUserId(FormCollection form)
        {
            string domain = "", ContactInfo = "", Token = "", Username = "", School = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserNO"] != null)
                ContactInfo = form["UserNO"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["UserId"] != null)
                Username = form["UserId"].Trim();
            if (form["School"] != null)
                School = form["School"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserNO"] != null)
                ContactInfo = Convert.ToString(Request.Headers["UserNO"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["UserId"] != null)
                Username = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["School"] != null)
                School = Convert.ToString(Request.Headers["School"]);
            if (string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {

                    //check valid school or not 
                    string suburl = "School/SchoolDatabase";
                    //string MainUrl = "http://localhost:50184" + "/" + suburl;
                    string MainUrl = "";
                    if (Request.Url.AbsoluteUri.Contains("localhost"))
                    {


                        //MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
                        //MainUrl = "http://3ischools.com/api" + "/" + suburl;
                        MainUrl = "http://api.digitech.co.in" + "/" + suburl;
                    }
                    else if (Request.Url.AbsoluteUri.Contains("digitech.co.in"))
                    {
                        MainUrl = "http://api.digitech.co.in" + "/" + suburl;
                    }
                    else
                    {
                        MainUrl = "http://3ischools.com/api" + "/" + suburl;
                    }

                    HttpClient client = new HttpClient();
                    client.Timeout = new TimeSpan(0, 10, 1);
                    MainUrl = MainUrl + "?DBName=" + School; // url with parameter
                    client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync(MainUrl).Result;
                    var content = response.Content.ReadAsStringAsync().Result;

                    //DataTable dt = new DataTable();

                    //using (SqlConnection conn = new SqlConnection("data source=SERVER;initial catalog=Ksmart;user id=sa;password=D1g1@sa#12345;MultipleActiveResultSets=True"))
                    //{
                    //    using (SqlCommand sqlComm = new SqlCommand("Select * from School ", conn))
                    //    {
                    //        conn.Open();
                    //        SqlDataAdapter da = new SqlDataAdapter();
                    //        da.SelectCommand = sqlComm;
                    //        da.Fill(dt);
                    //        conn.Close();
                    //    }
                    //}
                    //domainname dbname = new domainname();
                    //IList<domainname> domainlist = new List<domainname>();
                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                    //    dbname = new domainname();
                    //    dbname.Domain = dt.Rows[i]["Domain"].ToString();
                    //    domainlist.Add(dbname);
                    //}
                    //domainlist = domainlist.Where(m => m.Domain == School).ToList();

                    if (content != null)
                    {
                        //return Json(new
                        //{
                        //    status = "success",
                        //    message = "User already exist."
                        //}, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "School Not Exists"
                        }, JsonRequestBehavior.AllowGet);
                    }

                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        var user = db.SchoolUsers.Where(u => u.UserName == Username).FirstOrDefault();

                        var mobtype = db.ContactInfoTypes.Where(c => c.ContactInfoType1 == "Mobile").FirstOrDefault();
                        var contactinfos = db.ContactInfoes.Where(c => c.ContactInfo1 == ContactInfo).ToList();
                        if (mobtype != null)
                            contactinfos = contactinfos.Where(c => c.ContactInfoTypeId == mobtype.ContactInfoTypeId && c.IsDefault == true).ToList();

                        if (Username == "")
                        {
                            if (contactinfos.Count > 0)
                            {
                                var ismobilnoexist = contactinfos.FirstOrDefault();
                                // check if contact id already exist in school user
                                var IsUseralreadyexist = db.SchoolUsers.Where(s => s.UserContactId == ismobilnoexist.ContactId && s.Status == true);
                                if (IsUseralreadyexist.Count() > 0)
                                {
                                    return Json(new
                                    {
                                        status = "failed",
                                        message = "User already exist."
                                    }, JsonRequestBehavior.AllowGet);
                                }

                                switch (ismobilnoexist.ContactType.ContactType1)
                                {
                                    case "Guardian":
                                        var guardian = db.Guardians.Where(g => g.GuardianId == (int)ismobilnoexist.ContactId).FirstOrDefault();
                                        var studentid = guardian != null ? guardian.StudentId : 0;
                                        if (studentid > 0)
                                        {
                                            var isGStudentAdmitted = db.Students.Where(s => s.StudentId == (int)studentid).FirstOrDefault();
                                            if (string.IsNullOrEmpty(isGStudentAdmitted.AdmnNo))
                                                return Json(new
                                                {
                                                    status = "failed",
                                                    message = "The student is not admitted yet."
                                                }, JsonRequestBehavior.AllowGet);
                                        }
                                        break;
                                    case "Student":
                                        var isStudentAdmitted = db.Students.Where(s => s.StudentId == (int)ismobilnoexist.ContactId).FirstOrDefault();
                                        if (isStudentAdmitted != null)
                                        {
                                            if (string.IsNullOrEmpty(isStudentAdmitted.AdmnNo))
                                                return Json(new
                                                {
                                                    status = "failed",
                                                    message = "The student is not admitted yet."
                                                }, JsonRequestBehavior.AllowGet);
                                        }
                                        break;
                                    case "Teacher":
                                        var staffstatusdetail = db.Staffs.Where(s => s.StaffId == (int)ismobilnoexist.ContactId).FirstOrDefault();
                                        if (staffstatusdetail != null && staffstatusdetail.IsActive == false)
                                        {
                                            return Json(new
                                            {
                                                status = "failed",
                                                data = "Teacher is InActive"
                                            });
                                        }

                                        break;
                                }

                                // get otp length setting
                                int? lengthofotp = null;
                                var otplength = db.SchoolSettings.Where(s => s.AttributeName == "LengthOfOTP").FirstOrDefault();
                                if (otplength != null && !string.IsNullOrEmpty(otplength.AttributeValue))
                                    lengthofotp = Convert.ToInt32(otplength.AttributeValue);

                                // generate otp
                                string retOTP = "";
                                var otp = _IMainService.OneTimePasswordGenerate(ref retOTP, lengthofotp);
                                temp_otp_detail optdetial = new temp_otp_detail();
                                optdetial.OTP = otp;
                                optdetial.OTPCreatedTime = DateTime.Now;
                                optdetial.PhoneNo = ContactInfo;
                                optdetial.ValidateTime = 15;
                                db.temp_otp_detail.Add(optdetial);
                                db.SaveChanges();
                                var errormessage = "";
                                _ISMSSender.SendSMS(ContactInfo, "Your One Time Password for KSSmart is " + retOTP + " . Valid Untill 15 mins ", true,out errormessage);
                                // send otp on mobile no
                                return Json(new
                                {
                                    status = "success",
                                    otp = retOTP
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "Mobile No not exist"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            if (user != null)
                            {

                                // get otp length setting
                                var contactinfo = db.ContactInfoes.Where(c => c.ContactId == user.UserContactId && c.ContactTypeId == user.UserTypeId).FirstOrDefault();
                                ContactInfo = contactinfo.ContactInfo1;

                                // get otp length setting
                                int? lengthofotp = null;
                                var otplength = db.SchoolSettings.Where(s => s.AttributeName == "LengthOfOTP").FirstOrDefault();
                                if (otplength != null && !string.IsNullOrEmpty(otplength.AttributeValue))
                                    lengthofotp = Convert.ToInt32(otplength.AttributeValue);

                                // generate otp
                                string retOTP = "";
                                var otp = _IMainService.OneTimePasswordGenerate(ref retOTP, lengthofotp);
                                temp_otp_detail optdetial = new temp_otp_detail();
                                optdetial.OTP = otp;
                                optdetial.OTPCreatedTime = DateTime.Now;
                                optdetial.PhoneNo = ContactInfo;
                                optdetial.ValidateTime = 15;
                                db.temp_otp_detail.Add(optdetial);
                                db.SaveChanges();
                                var errormessage = "";
                                _ISMSSender.SendSMS(ContactInfo, "Your One Time Password for KSSmart is " + retOTP + " . Valid Untill 15 mins ", true, out errormessage);
                                // send otp on mobile no
                                return Json(new
                                {
                                    status = "success",
                                    otp = retOTP
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not exist"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }


                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (DbEntityValidationException ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CheckValidOtp(FormCollection form)
        {
            string domain = "", UserName = "", OTP = "", Token = "", UserId = "";
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserNO"] != null)
                UserName = form["UserNO"].Trim();
            if (form["UserId"] != null)
                UserId = Convert.ToString(form["UserId"].Trim());
            if (form["OTP"] != null)
                OTP = form["OTP"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserNO"] != null)
                UserName = Convert.ToString(Request.Headers["UserNO"]);
            if (Request.Headers["UserId"] != null)
                UserId = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["OTP"] != null)
                OTP = Convert.ToString(Request.Headers["OTP"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(OTP) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];

                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        var user = db.SchoolUsers.Where(m => m.UserName == UserId).FirstOrDefault();
                        var mobtype = db.ContactInfoTypes.Where(c => c.ContactInfoType1 == "Mobile").FirstOrDefault();
                        if (user != null)
                        {
                            var contactinfo = mobtype != null ? db.ContactInfoes.Where(m => m.ContactId == user.UserContactId && m.ContactTypeId == user.UserTypeId && m.ContactInfoTypeId == mobtype.ContactInfoTypeId && m.IsDefault == true).FirstOrDefault() : db.ContactInfoes.Where(m => m.ContactId == user.UserContactId && m.IsDefault == true).FirstOrDefault();
                            if (UserName == "")
                                UserName = contactinfo.ContactInfo1;
                        }

                        // get otp by phone no
                        var isotpexistbyphone = db.temp_otp_detail.Where(t => t.PhoneNo == UserName).OrderByDescending(m => m.Id).FirstOrDefault();
                        if (isotpexistbyphone != null)
                        {
                            MD5CryptoServiceProvider md5hasher = new MD5CryptoServiceProvider();
                            Byte[] hashedbytes;
                            UTF8Encoding encoder = new UTF8Encoding();
                            hashedbytes = md5hasher.ComputeHash(encoder.GetBytes(OTP));

                            var currenttiem = isotpexistbyphone.OTPCreatedTime.Value.AddMinutes(Convert.ToDouble(isotpexistbyphone.ValidateTime));
                            var now = DateTime.Now;
                            if (now <= currenttiem)
                            {
                                StringBuilder kspregotp = new StringBuilder();
                                foreach (var h in hashedbytes)
                                    kspregotp.Append(h.ToString("x2"));

                                if (isotpexistbyphone.OTP != kspregotp.ToString())
                                {
                                    // delete all data against this no
                                    // db.Database.ExecuteSqlCommand("Delete from temp_otp_detail where PhoneNo = " + isotpexistbyphone.PhoneNo);

                                    return Json(new
                                    {
                                        status = "failed",
                                        message = "Invalid OTP"
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    // delete all data against this no
                                    db.Database.ExecuteSqlCommand("Delete from temp_otp_detail where PhoneNo = " + isotpexistbyphone.PhoneNo);

                                    return Json(new
                                    {
                                        status = "success",
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                                return Json(new
                                {
                                    status = "failed",
                                    message = "OTP timeout"
                                }, JsonRequestBehavior.AllowGet);
                        }
                        else
                            return Json(new
                            {
                                status = "failed",
                                message = "Incorrect Mobile No."
                            }, JsonRequestBehavior.AllowGet);

                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SignUp(FormCollection form)
        {
            string domain = "", UserName = "", Password = "", ConfirmPassWord = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserNO"] != null)
                UserName = form["UserNO"].Trim();
            if (form["Password"] != null)
                Password = form["Password"].Trim();
            if (form["ConfirmPassWord"] != null)
                ConfirmPassWord = form["ConfirmPassWord"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserNO"] != null)
                UserName = Convert.ToString(Request.Headers["UserNO"]);
            if (Request.Headers["Password"] != null)
                Password = Convert.ToString(Request.Headers["Password"]);
            if (Request.Headers["ConfirmPassWord"] != null)
                ConfirmPassWord = Convert.ToString(Request.Headers["ConfirmPassWord"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password) || string.IsNullOrEmpty(ConfirmPassWord) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }
            if (!string.IsNullOrEmpty(Password) || !string.IsNullOrEmpty(ConfirmPassWord))
            {
                if (Password != ConfirmPassWord)
                    return Json(new
                    {
                        status = "failed",
                        message = "Password not match with confirm password"
                    }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];

                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        var mobtype = db.ContactInfoTypes.Where(c => c.ContactInfoType1 == "Mobile").FirstOrDefault();
                        var contactinfos = db.ContactInfoes.Where(c => c.ContactInfo1 == UserName).ToList();
                        if (mobtype != null)
                            contactinfos = contactinfos.Where(c => c.ContactInfoTypeId == mobtype.ContactInfoTypeId).ToList();

                        if (contactinfos.Count > 0)
                        {
                            var ismobilnoexist = contactinfos.FirstOrDefault();

                            var usertypeid = ismobilnoexist.ContactType.ContactTypeId;
                            var userId = ismobilnoexist.ContactId;
                            // checkuser already exist
                            var isuserexist = db.SchoolUsers.Where(u => u.UserTypeId == usertypeid && u.UserContactId == userId).ToList();
                            if (isuserexist.Count > 0)
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User already exist"
                                }, JsonRequestBehavior.AllowGet);
                            }

                            var usertype = ismobilnoexist.ContactType.ContactType1;
                            // add new School User
                            SchoolUser user = new SchoolUser();
                            user.RegdDate = DateTime.UtcNow;
                            user.Password = _IEncryptionService.EncryptText(Password);
                            user.Status = true;
                            user.UserTypeId = ismobilnoexist.ContactType.ContactTypeId;
                            user.UserContactId = userId;
                            string UserImage = "";
                            var SchoolDbId = dbdetail.DBId;
                            var staffdoctypeid = db.StaffDocumentTypes.Where(d => d.DocumentType == "Image").FirstOrDefault();
                            string defaultimg = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/default.png";
                            var doctypeid = db.DocumentTypes.Where(d => d.DocumentType1 == "Image").FirstOrDefault();
                            var username = "";
                            if (usertype.ToLower() == "student")
                            {
                                var image = db.AttachedDocuments.Where(d => d.DocumentTypeId == doctypeid.DocumentTypeId && d.StudentId == user.UserContactId).FirstOrDefault();
                                UserImage = image != null ? _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/StudentPhotos/" + image.Image : defaultimg;
                                // get by id
                                var student = db.Students.Where(s => s.StudentId == (int)userId).FirstOrDefault();
                                if (student != null)
                                {
                                    user.UserName = "S" + userId;
                                    username = student.LName == null ? student.FName : student.FName + " " + student.LName;
                                }
                            }
                            else if (usertype.ToLower() == "guardian")
                            {
                                var senderstudentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault() != null ?
                                           db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId : 0;
                                var image = db.AttachedDocuments.Where(d => d.DocumentTypeId == doctypeid.DocumentTypeId && d.StudentId == senderstudentid).FirstOrDefault();
                                UserImage = image != null ? _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/StudentPhotos/" + image.Image : defaultimg;
                                // get gurdian by id
                                var guardian = db.Guardians.Where(s => s.GuardianId == (int)userId).FirstOrDefault();
                                if (guardian != null)
                                {
                                    user.UserName = "P" + guardian.StudentId;  // get gurdian student Id
                                    username = guardian.LastName == null ? guardian.FirstName : guardian.FirstName + " " + guardian.LastName;
                                }
                            }
                            else if (usertype.ToLower() == "staff" || usertype.ToLower() == "teacher")
                            {
                                var staff = db.Staffs.Where(s => s.StaffId == (int)userId).FirstOrDefault();
                                var teacherimage = staff.StaffDocuments.Where(d => d.DocumentTypeId == staffdoctypeid.DocumentTypeId).FirstOrDefault();
                                UserImage = teacherimage != null ? _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/Teacher/Photos/" + teacherimage.Image : defaultimg;
                                // get staff by id

                                if (staff != null)
                                {
                                    user.UserName = staff.FName + " " + staff.LName;
                                    username = !string.IsNullOrEmpty(staff.LName) ? staff.FName + " " + staff.LName : staff.FName;
                                }
                            }

                            db.SchoolUsers.Add(user);
                            db.SaveChanges();
                            var errormessage = "";
                            // send otp on mobile no
                            _ISMSSender.SendSMS(ismobilnoexist.ContactInfo1, "Your Login Credentials for KSSmart are: UserId:" + user.UserName + " Password:" + Password, true, out errormessage);
                            return Json(new
                            {
                                status = "success",
                                UserId = user.UserName,
                                usertype = usertype,
                                UserImage = UserImage,
                                domain = domain,
                                username = username
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "Mobile No not exist"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Login(FormCollection form)
        {
            string domain = "", UserName = "", Password = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Password"] != null)
                Password = form["Password"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Password"] != null)
                Password = Convert.ToString(Request.Headers["Password"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required",
                    domain = domain,
                    UserName = UserName,
                    Password = Password,
                    Token = Token,

                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];

                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        string message = "";
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();

                        var loginResult = new UserLoginResults();

                        var contacttype = db.ContactTypes.Where(c => c.ContactTypeId == (int)user.UserTypeId).FirstOrDefault();
                        if (contacttype.ContactType1.ToLower() == "student")
                        {
                            var studentclass = db.StudentClasses.Where(m => m.Student.StudentId == user.UserContactId).FirstOrDefault();
                            if (studentclass == null)
                            {

                                return Json(new
                                {
                                    status = "failed",
                                    message = "Class Not Assigned"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        if (user == null)
                            loginResult = UserLoginResults.CustomerNotExist;
                        else
                        {
                            string pwd;
                            pwd = _IEncryptionService.EncryptText(Password);

                            bool isValid = pwd == user.Password;
                            if (!isValid)
                                loginResult = UserLoginResults.WrongPassword;
                            else
                                loginResult = UserLoginResults.Successful;

                            bool isActive = (bool)user.Status;
                            if (!isActive)
                                loginResult = UserLoginResults.UserNotActive;
                        }

                        switch (loginResult)
                        {
                            case UserLoginResults.Successful:
                                {
                                    // insert support user login info
                                    UserLoginInfo logininfo = new UserLoginInfo();
                                    logininfo.LoginTime = DateTime.UtcNow;
                                    logininfo.UserId = user.UserId;
                                    db.UserLoginInfoes.Add(logininfo);
                                    db.SaveChanges();

                                    // get user type
                                    var usertype = db.ContactTypes.Where(c => c.ContactTypeId == (int)user.UserTypeId).FirstOrDefault();
                                    var userName = "";
                                    var SchoolDbId = dbdetail.DBId;
                                    var staffdoctypeid = db.StaffDocumentTypes.Where(d => d.DocumentType == "Image").FirstOrDefault();
                                    string defaultimg = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/default.png";
                                    string UserImage = "";
                                    int BusTransportPermission = 0;

                                    var doctypeid = db.DocumentTypes.Where(d => d.DocumentType1 == "Image").FirstOrDefault();
                                    if (usertype.ContactType1 == "Student")
                                    {
                                        var image = db.AttachedDocuments.Where(d => d.DocumentTypeId == doctypeid.DocumentTypeId && d.StudentId == user.UserContactId).FirstOrDefault();
                                        UserImage = image != null ? _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/StudentPhotos/" + image.Image : defaultimg;
                                        var student = db.Students.Where(m => m.StudentId == user.UserContactId).FirstOrDefault();
                                        if (student != null)
                                            userName = !string.IsNullOrEmpty(student.LName) ? student.FName + " " + student.LName : student.FName;

                                        // check bus transport permission
                                        if (student != null && student.TptFacilities.Count() > 0)
                                        {
                                            var currentdate = DateTime.UtcNow.Date;
                                            // check student tpt facility
                                            var studenttptfacility = student.TptFacilities.Where(t => t.EffectiveDate <= currentdate &&
                                                (t.EndDate == null || (t.EndDate != null && t.EndDate >= currentdate))
                                                && t.Status == true && t.TptTypeDetail.TptType.TptType1 == "School").FirstOrDefault();

                                            if (studenttptfacility != null)
                                                BusTransportPermission = 1;
                                        }
                                    }
                                    else if (usertype.ContactType1 == "Guardian")
                                    {
                                        var senderstudent = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault() != null ?
                                            db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().Student : null;
                                        if (senderstudent != null)
                                        {
                                            var image = db.AttachedDocuments.Where(d => d.DocumentTypeId == doctypeid.DocumentTypeId && d.StudentId == senderstudent.StudentId).FirstOrDefault();
                                            UserImage = image != null ? _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/StudentPhotos/" + image.Image : defaultimg;
                                            var guardian = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault();
                                            if (guardian != null)
                                                userName = !string.IsNullOrEmpty(guardian.Student.LName) ? guardian.Student.FName + " " + guardian.Student.LName : guardian.Student.FName;

                                            // check bus transport permission
                                            if (senderstudent.TptFacilities.Count() > 0)
                                            {
                                                var currentdate = DateTime.UtcNow.Date;
                                                // check student tpt facility
                                                var studenttptfacility = senderstudent.TptFacilities.Where(t => t.EffectiveDate <= currentdate &&
                                                    (t.EndDate == null || (t.EndDate != null && t.EndDate >= currentdate))
                                                    && t.Status == true && t.TptTypeDetail.TptType.TptType1 == "School").FirstOrDefault();

                                                if (studenttptfacility != null)
                                                    BusTransportPermission = 1;
                                            }
                                        }

                                    }
                                    else if (usertype.ContactType1 == "Teacher")
                                    {
                                        var staff = db.Staffs.Where(d => d.StaffId == user.UserContactId).FirstOrDefault();
                                        var teacherimage = staff.StaffDocuments.Where(d => d.DocumentTypeId == staffdoctypeid.DocumentTypeId).FirstOrDefault();
                                        UserImage = teacherimage != null ? _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/Teacher/Photos/" + teacherimage.Image : defaultimg;
                                        if (staff != null)
                                            userName = staff.LName != null ? staff.FName + " " + staff.LName : staff.FName;
                                    }

                                    return Json(new
                                    {
                                        status = "success",
                                        message = "Login Successfully",
                                        usertype = usertype.ContactType1,
                                        UserImage = UserImage,
                                        UserName = userName,
                                        domain = domain,
                                        UserId = UserName,
                                        BusTransportPermission = BusTransportPermission
                                    }, JsonRequestBehavior.AllowGet);
                                }

                            case UserLoginResults.CustomerNotExist:
                                message = "Wrong User Id";
                                break;
                            case UserLoginResults.WrongPassword:
                                message = "Wrong Password";
                                break;
                            default:
                                message = "Wrong Credentials";
                                break;
                        }

                        return Json(new
                        {
                            status = "failed",
                            message = message
                        }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult PasswordRecovery(FormCollection form)
        {
            string domain = "", UserName = "", Password = "", ConfirmPassWord = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Password"] != null)
                Password = form["Password"].Trim();
            if (form["ConfirmPassWord"] != null)
                ConfirmPassWord = form["ConfirmPassWord"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Password"] != null)
                Password = Convert.ToString(Request.Headers["Password"]);
            if (Request.Headers["ConfirmPassWord"] != null)
                ConfirmPassWord = Convert.ToString(Request.Headers["ConfirmPassWord"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password) || string.IsNullOrEmpty(ConfirmPassWord) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }
            if (!string.IsNullOrEmpty(Password) || !string.IsNullOrEmpty(ConfirmPassWord))
            {
                if (Password != ConfirmPassWord)
                    return Json(new
                    {
                        status = "failed",
                        message = "Password not match with confirm password"
                    }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        var isuserexist = db.SchoolUsers.ToList();
                        isuserexist = isuserexist.Where(u => u.UserName.ToLower() == UserName.ToLower()).ToList();
                        if (isuserexist.Count == 0)
                            return Json(new
                            {
                                status = "failed",
                                message = "User not exist"
                            }, JsonRequestBehavior.AllowGet);

                        var user = isuserexist.FirstOrDefault();
                        user.Password = _IEncryptionService.EncryptText(Password);
                        db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();

                        var mobtype = db.ContactInfoTypes.Where(c => c.ContactInfoType1 == "Mobile").FirstOrDefault();

                        var contactinfos = mobtype != null ? db.ContactInfoes.Where(c => c.ContactId == user.UserContactId && c.ContactInfoTypeId == mobtype.ContactInfoTypeId).ToList() : db.ContactInfoes.Where(c => c.ContactId == user.UserContactId).ToList();
                        var errormessage = "";
                        // send otp on mobile no
                        if (contactinfos.Count > 0)
                        {
                            var ismobilnoexist = contactinfos.FirstOrDefault();
                            _ISMSSender.SendSMS(ismobilnoexist.ContactInfo1, "Your new Password for KSSmart is " + Password, true, out errormessage);
                        }
                        return Json(new
                        {
                            status = "success",
                            message = "Password Changed"
                        }, JsonRequestBehavior.AllowGet);

                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult NavigationMenu(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];

                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        // naigation list 
                        IList<NavList> NavigationList = new List<NavList>();

                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null)
                        {
                            // get user name and image
                            // default image
                            var SchoolDbId = dbdetail.DBId;
                            string defaultimg = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/default.png";
                            string UserImage = "";
                            string UserFullName = "";
                            int userroleId = 0;
                            int documentid = 0;
                            var IsClassIncharge = false;
                            string Class = "";
                            string AdmissionNo = "";
                            // get all staff types
                            var stafftypes = db.StaffTypes.ToList();
                            if (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian")
                            {

                                var usercontactId = user.UserContactId;

                                if (user.ContactType.ContactType1.ToLower() == "student")
                                {
                                    // get student by id
                                    var student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                                    if (student != null)
                                        UserFullName = student.FName + " " + student.LName;

                                    userroleId = (int)user.ContactType.UserRoleContactTypes.FirstOrDefault().UserRoleId;
                                    AdmissionNo = student.AdmnNo;

                                    var studentcls = from t1 in db.StudentClasses
                                                     join t2 in db.ClassMasters
                                                     on t1.ClassId equals t2.ClassId
                                                     where t1.StudentId == student.StudentId
                                                     select t2.Class;

                                    Class = studentcls.FirstOrDefault();
                                }
                                else
                                {
                                    // get guardian by id
                                    var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                    if (Guardian != null)
                                        UserFullName = Guardian.FirstName + " " + Guardian.LastName;

                                    // get guardian's student user
                                    string studentusername = UserName.ToLower().Replace('p', 's');
                                    var Studentuser = db.SchoolUsers.Where(u => u.UserName == studentusername).FirstOrDefault();
                                    var student = db.Students.Where(p => p.StudentId == (int)Studentuser.UserContactId).FirstOrDefault();
                                    if (student != null)
                                        UserFullName = student.FName + " " + student.LName;

                                    userroleId = Studentuser != null ? (int)Studentuser.ContactType.UserRoleContactTypes.FirstOrDefault().UserRoleId : 0;
                                    AdmissionNo = student.AdmnNo;
                                    var studentcls = from t1 in db.StudentClasses
                                                     join t2 in db.ClassMasters
                                                     on t1.ClassId equals t2.ClassId
                                                     where t1.StudentId == student.StudentId
                                                     select t2.Class;
                                    Class = studentcls.FirstOrDefault();
                                    usercontactId = Studentuser.UserContactId;
                                }

                                // Student image path
                                // get doc type
                                var doctypeid = db.DocumentTypes.Where(d => d.DocumentType1 == "Image").FirstOrDefault();
                                var image = db.AttachedDocuments.Where(d => d.DocumentTypeId == doctypeid.DocumentTypeId && d.StudentId == usercontactId).FirstOrDefault();
                                UserImage = image != null ? _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/StudentPhotos/" + image.Image : defaultimg;
                                if (image != null)
                                    documentid = image.DocumentId;
                            }
                            else if (stafftypes.Where(s => s.StaffType1.ToLower() == user.ContactType.ContactType1.ToLower()).FirstOrDefault() != null)
                            {
                                // Student image path
                                // get doc type
                                var staff = db.Staffs.Where(d => d.StaffId == user.UserContactId).FirstOrDefault();
                                var doctypeid = db.StaffDocumentTypes.Where(d => d.DocumentType == "Image").FirstOrDefault();
                                var image = db.StaffDocuments.Where(d => d.DocumentTypeId == doctypeid.DocumentTypeId && d.StaffId == user.UserContactId).FirstOrDefault();
                                if (image != null)
                                    documentid = image.DocumentId;
                                UserImage = image != null ? _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/Teacher/Photos/" + image.Image : defaultimg;
                                if (staff.StaffId > 0)
                                    UserFullName = !String.IsNullOrEmpty(staff.LName) ? staff.FName + " " + staff.LName : staff.FName;

                                userroleId = (int)user.ContactType.UserRoleContactTypes.FirstOrDefault().UserRoleId;
                                var date = DateTime.UtcNow.Date;
                                var ClassTeacher = db.ClassTeachers.Where(p => p.TeacherId == user.UserContactId && (p.EndDate >= date || p.EndDate == null)).ToList();
                                if (ClassTeacher.Count > 0)
                                {
                                    IsClassIncharge = true;
                                }
                            }

                            var navigationlist = db.UserMobNavigationItems.ToList().Where(u => u.UserRoleId == userroleId && u.IsActive == true).ToList();
                            navigationlist = navigationlist.OrderBy(n => n.SortingIndex).ToList();
                            var NavigationIconsPathMobile = db.SchoolSettings.Where(m => m.AttributeName == "NavigationPathMobile").Select(m => m.AttributeValue).FirstOrDefault();
                            string ActivityIcon = "";
                            NavList NavList = new NavList();
                            foreach (var item in navigationlist)
                            {
                                NavList = new NavList();
                                NavList.Name = item.NavigationItem;
                                NavList.URl = _WebHelper.GetStoreHost(false) + "/" + User + "/" + item.NavigationItem1.ControllerName + "/" + item.NavigationItem1.ActionName;
                                if (NavigationIconsPathMobile == null)
                                    ActivityIcon = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/NaviagtionIcons/MobileNavigationIcons/" + item.NavigationItem1.NavigationIcon;
                                else
                                    ActivityIcon = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/" + NavigationIconsPathMobile + "/" + item.NavigationItem1.NavigationIcon;


                                NavList.Icon = ActivityIcon;
                                NavigationList.Add(NavList);
                            }
                            NavList = new NavList();
                            NavList.Name = "Dashboard";
                            NavList.URl = _WebHelper.GetStoreHost(false) + "/" + User + "/API/DashboardNew";
                            NavList.Icon = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/NaviagtionIcons/MobileNavigationIcons/profile.png";
                            NavigationList.Add(NavList);

                            // Logout
                            var ActivityIconlogout = "";
                            //NavList = new NavList();
                            //NavList.Name = "Log Out";
                            if (NavigationIconsPathMobile == null)
                                ActivityIconlogout = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/NaviagtionIcons/MobileNavigationIcons/logout.png";
                            else
                                ActivityIconlogout = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/" + NavigationIconsPathMobile + "/logout.png";

                            //NavList.Icon = ActivityIconlogout;
                            //NavList.URl = Request.Url.AbsoluteUri + "/API/Logout";
                            //NavigationList.Add(NavList);
                            var IsAuthToAddRecord = IsUserAuthorize(db, user, "Message History", "Add Record");
                            var currentSession = GetCurrentSession(dbdetail.DBName);
                            var StudentBoardingpoint = "";
                            if (user.ContactType.ContactType1.ToLower() == "student")
                            {
                                var tptdetail = db.TptFacilities.Where(t => t.StudentId == user.UserContactId).OrderByDescending(t => t.EffectiveDate).FirstOrDefault();
                                if (tptdetail.BoardingPoint != null)
                                    StudentBoardingpoint = tptdetail.BoardingPoint.BoardingPoint1;
                            }
                            if (user.ContactType.ContactType1.ToLower() == "guardian")
                            {
                                var guardian = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault();
                                if (guardian != null)
                                {
                                    var studentid = guardian.StudentId;
                                    var tptdetail = db.TptFacilities.Where(t => t.StudentId == studentid).OrderByDescending(t => t.EffectiveDate).FirstOrDefault();
                                    if (tptdetail != null)
                                        StudentBoardingpoint = tptdetail.BoardingPoint.BoardingPoint1;
                                }
                            }
                            return Json(new
                            {
                                status = "success",
                                NavList = NavigationList,
                                documentid = documentid,
                                UserImage = UserImage,
                                UserFullName = UserFullName,
                                IsAuthorizedToSendRecord = IsAuthToAddRecord,
                                CurrentSessionStartDate = ConvertDate((DateTime)currentSession.StartDate),
                                IsClassIncharge = IsClassIncharge,
                                StudentBoardingpoint = StudentBoardingpoint,
                                AdmissionNo = AdmissionNo,
                                Class = Class
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Student API

        [HttpPost]
        public ActionResult StudentInfo(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];

                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student != null)
                            {
                                IList<NavList> NavList = new List<NavList>();
                                NavList Nav = new NavList();
                                Nav.Name = "StudentProfile";
                                Nav.URl = _WebHelper.GetStoreHost(false) + "/" + User + "/API/StudentProfile";
                                NavList.Add(Nav);

                                Nav = new NavList();
                                Nav.Name = "HealthDetail";
                                Nav.URl = _WebHelper.GetStoreHost(false) + "/" + User + "/API/HealthDetail";
                                NavList.Add(Nav);

                                return Json(new
                                {
                                    status = "success",
                                    Tabs = NavList
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid Token"
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StudentProfile(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];

                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var StudentProfile = new StudentInfoModel();
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student.StudentId > 0)
                            {
                                // student conatct type
                                var Stdcontacttype = db.ContactTypes.Where(c => c.ContactType1 == "Student").FirstOrDefault(); // Student contact type
                                // guardian contact type
                                var Gurdcontacttype = db.ContactTypes.Where(c => c.ContactType1 == "Guardian").FirstOrDefault(); // Guardian contact type
                                // Sibling Relation type
                                var SiblingRelationType = db.RelationTypes.Where(c => c.RelationType1 == "Sibling").FirstOrDefault();
                                // sibling relations
                                var SiblingRelation = db.Relations.Where(r => r.RelationTypeId == SiblingRelationType.RelationTypeId).ToList();

                                StudentProfile.DOB = student.DOB.HasValue ? student.DOB.Value.ToString("dd/MM/yyyy") : "";
                                StudentProfile.AdmissionNo = student.AdmnNo;
                                StudentProfile.DOJ = student.DOJ.HasValue ? student.DOJ.Value.ToString("dd/MM/yyyy") : "";
                                StudentProfile.Gender = student.Gender != null ? student.Gender.Gender1 : "";
                                StudentProfile.Category = student.StudentCategory != null ? student.StudentCategory.StudentCategory1 : "";
                                StudentProfile.BloodGroup = student.BloodGroup != null ? student.BloodGroup.BloodGroup1 : "";
                                StudentProfile.Allergy = student.Allergies != null ? student.Allergies : "";
                                // Contact info
                                var contatinfotype = db.ContactInfoTypes.Where(c => c.ContactInfoType1 == "Email").FirstOrDefault(); // info type 
                                var stdcontactinfo = db.ContactInfoes.Where(c => c.ContactInfoTypeId == contatinfotype.ContactInfoTypeId &&
                                    c.ContactId == student.StudentId && c.ContactTypeId == Stdcontacttype.ContactTypeId).FirstOrDefault();

                                var phonecontactinfoType = db.ContactInfoTypes.Where(c => c.ContactInfoType1 == "Mobile").FirstOrDefault();
                                var stdPhoneContactInfo = db.ContactInfoes.Where(c => c.ContactInfoTypeId == phonecontactinfoType.ContactInfoTypeId &&
                                    c.ContactId == student.StudentId && c.ContactTypeId == Stdcontacttype.ContactTypeId).FirstOrDefault();

                                StudentProfile.Email = stdcontactinfo != null ? stdcontactinfo.ContactInfo1 : "";
                                StudentProfile.PhoneNo = stdPhoneContactInfo != null ? stdPhoneContactInfo.ContactInfo1 : "";

                                // Address
                                var stdaddress = db.Addresses.Where(a => a.ContactId == student.StudentId && a.ContactTypeId == Stdcontacttype.ContactTypeId).ToList();
                                if (stdaddress.Count > 0)
                                {
                                    // check permanent Address
                                    var ParmenentAddress = stdaddress.Where(a => a.AddressType.AddressType1 == "Parmanent").FirstOrDefault();
                                    if (ParmenentAddress == null)
                                        ParmenentAddress = stdaddress.FirstOrDefault();

                                    StudentProfile.AddressType = ParmenentAddress.AddressType.AddressType1;
                                    StudentProfile.PlotNo = ParmenentAddress.PlotNo;
                                    StudentProfile.SectorOrStreet = ParmenentAddress.Sector_Street;
                                    StudentProfile.Locality = ParmenentAddress.Locality;
                                    StudentProfile.Zip = ParmenentAddress.Zip;
                                    StudentProfile.City = ParmenentAddress.AddressCity.City;
                                    StudentProfile.State = ParmenentAddress.AddressCity.AddressState.State;
                                }

                                // Guaridan
                                var parentdetail = db.Guardians.Where(g => g.StudentId == student.StudentId).ToList();
                                if (parentdetail.Count > 0)
                                {
                                    var IsFatherDetail = parentdetail.Any(p => p.Relation.Relation1 == "Father");
                                    var IsMotherDetail = parentdetail.Any(p => p.Relation.Relation1 == "Mother");
                                    var IsGuardianDetail = parentdetail.Any(p => p.Relation.Relation1 == "Guardian");

                                    if (IsFatherDetail || IsMotherDetail || IsGuardianDetail)
                                    {
                                        var father = parentdetail.Where(p => p.Relation.Relation1 == "Father").FirstOrDefault();
                                        StudentProfile.FatherName = father != null ? father.FirstName + " " + father.LastName : "";

                                        var mother = parentdetail.Where(p => p.Relation.Relation1 == "Mother").FirstOrDefault();
                                        StudentProfile.MotherName = mother != null ? mother.FirstName + " " + mother.LastName : "";

                                        var Guardian = parentdetail.Where(p => p.Relation.Relation1 == "Guardian").FirstOrDefault();
                                        StudentProfile.GuardianName = Guardian != null ? Guardian.FirstName + " " + Guardian.LastName : "";
                                    }
                                }

                                // Entrance Class
                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                // current Class and Rollno and Entarnce class
                                var currentstudentclass = db.StudentClasses.Where(s => s.StudentId == student.StudentId);
                                var currentstdclas = currentstudentclass.Where(m => m.SessionId == currentSession.SessionId).FirstOrDefault();
                                if (currentstdclas != null)
                                {
                                    StudentProfile.CurrentClass = currentstdclas.ClassMaster != null ? currentstdclas.ClassMaster.Class : "";
                                    StudentProfile.CurrentClassRollno = currentstdclas.RollNo != null ? currentstdclas.RollNo : "";
                                    StudentProfile.BoardRollno = currentstdclas.BoardRollNo != null ? currentstdclas.BoardRollNo : "";
                                }

                                return Json(new
                                {
                                    status = "success",
                                    Profile = StudentProfile
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult HealthDetail(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];

                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var StudentProfile = new StudentInfoModel();
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student.StudentId > 0)
                            {
                                IList<StudentHealthDetail> StudentHealthList = new List<StudentHealthDetail>();

                                // health detail
                                //dropdown Student other Details
                                var Current_students_classes = db.StudentClasses.Where(s => s.StudentId == student.StudentId && s.ClassId != null);
                                if (Current_students_classes.Count() > 0)
                                {
                                    var studentsclassids = Current_students_classes.Select(c => c.StudentClassId).ToArray();
                                    var studentclassdetaillist = db.StudentClassDetails.Where(c => studentsclassids.Contains((int)c.StudentClassId)).ToList();
                                    foreach (var scd in studentclassdetaillist)
                                    {
                                        var StudentHealthDetail = new StudentHealthDetail();
                                        StudentHealthDetail.Class = scd.StudentClass.ClassMaster.Class;
                                        StudentHealthDetail.Height = scd.Height;
                                        StudentHealthDetail.Weight = scd.Weight.HasValue ? scd.Weight.ToString() : "";
                                        StudentHealthDetail.VisionL = scd.Vision_L != null ? scd.Vision_L : "";
                                        StudentHealthDetail.VisionR = scd.Vision_R != null ? scd.Vision_R : "";
                                        StudentHealthList.Add(StudentHealthDetail);
                                    }
                                }

                                return Json(new
                                {
                                    status = "success",
                                    HealthList = StudentHealthList
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StudentAttendance(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];

                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var StudentProfile = new StudentInfoModel();
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student.StudentId > 0)
                            {
                                // Entrance Class
                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                var Curentdate = DateTime.UtcNow.Date;
                                // Sundays count
                                int Sundaycount = 0;
                                var sundayslist = _IMainService.GetSundayCountBetweenDates(ref Sundaycount, currentSession.StartDate.Value, Curentdate);

                                // get holidays
                                var holidayslist = db.Events.Where(e => e.EventType.EventType1 == "Holidays" && e.StartDate >= currentSession.StartDate
                                    && e.StartDate <= Curentdate && e.IsActive == true && e.IsDeleted != true).Select(e => (DateTime)e.StartDate).ToList();

                                // combine both list and get distinct
                                var alldatelist = sundayslist.Concat(holidayslist).Distinct().ToList();

                                // get attendance by student
                                var studentattendance = db.Attendances.Where(a => a.StudentId == student.StudentId && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();

                                // calculate attendance %
                                TimeSpan diff = Curentdate - currentSession.StartDate.Value;
                                int days = diff.Days - alldatelist.Count;

                                var PresentPerc = studentattendance.Count > 0 ? (studentattendance.Count * 100) / days : 0;
                                var AbsentPerc = 100 - PresentPerc;

                                return Json(new
                                {
                                    status = "success",
                                    Present = PresentPerc,
                                    Absent = AbsentPerc
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StudentMonthWiseAttendance(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var StudentProfile = new StudentInfoModel();
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student.StudentId > 0)
                            {
                                // Entrance Class
                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                var Curentdate = DateTime.UtcNow.Date;
                                //monthsCount
                                int monthscount = 0;
                                int absentcount = 0;
                                var Monthslist = _IMainService.GetMonthCountBetweenDates(ref monthscount, currentSession.StartDate.Value, Curentdate);
                                AppStudentAttendanceModel StudentAttendance = new AppStudentAttendanceModel();
                                IList<AppStudentAttendanceModel> monthlyAttendance = new List<AppStudentAttendanceModel>();
                                AppMothlyAttendanceStatusModel monthwiseAttendance = new AppMothlyAttendanceStatusModel();
                                var EventTypeId = db.EventTypes.Where(m => m.EventType1.ToLower().Contains("Holiday")).FirstOrDefault().EventTypeId;
                                foreach (var ml in Monthslist)
                                {
                                    StudentAttendance = new AppStudentAttendanceModel();
                                    absentcount = 0;
                                    //months start and end dates
                                    var firstDayOfMonth = new DateTime(ml.Date.Year, ml.Date.Month, 1); // first day of month
                                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1); // last day of month
                                    // Sundays count
                                    //int Sundaycount = 0;
                                    //var sundayslist = ml.Month == Curentdate.Month ? _IMainService.GetSundayCountBetweenDates(ref Sundaycount, firstDayOfMonth, Curentdate) : _IMainService.GetSundayCountBetweenDates(ref Sundaycount, firstDayOfMonth, lastDayOfMonth);

                                    // get holidays
                                    //var holidayslist = ml.Month == Curentdate.Month ? db.Events.Where(e => e.EventType.EventType1 == "Holiday" && e.StartDate >= firstDayOfMonth
                                    //     && e.StartDate <= Curentdate && e.IsActive == true && e.IsDeleted == false).Select(e => (DateTime)e.StartDate).ToList() : db.Events.Where(e => e.EventType.EventType1 == "Holiday" && e.StartDate >= firstDayOfMonth
                                    //       && e.StartDate <= lastDayOfMonth && e.IsActive == true && e.IsDeleted == false).Select(e => (DateTime)e.StartDate).ToList();

                                    // combine both list and get distinct
                                    //var alldatelist = sundayslist.Concat(holidayslist).Distinct().ToList();

                                    // get attendance by student
                                    var studentattendance = db.Attendances.Where(a => a.StudentId == student.StudentId && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= firstDayOfMonth && a.AttendanceDate <= lastDayOfMonth).ToList();
                                    TimeSpan diff = lastDayOfMonth.AddDays(1) - firstDayOfMonth;
                                    // calculate attendance %
                                    if (ml.Month == Curentdate.Month)
                                        diff = Curentdate.AddDays(1) - firstDayOfMonth;

                                    var holidaycount = db.Events.AsNoTracking().Where(m => m.StartDate.Value.Month == ml.Date.Month && m.EventTypeId == EventTypeId).Count();
                                    int sundaycount = 0;
                                    for (int i = 0; i < diff.Days; i++)
                                    {
                                        DateTime dt = new DateTime(ml.Date.Year, ml.Date.Month, i + 1);
                                        if (dt.DayOfWeek == DayOfWeek.Sunday)
                                        {
                                            sundaycount++;
                                        }
                                    }
                                    //workingDays
                                    int days = diff.Days - (Convert.ToInt32(holidaycount) + sundaycount); //-alldatelist.Count;
                                    string SDays = days.ToString().Replace("-", "");
                                    StudentAttendance.Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(ml.Month);
                                    StudentAttendance.Total = Convert.ToString(SDays);
                                    StudentAttendance.Present = Convert.ToString(studentattendance.Count());
                                    var absentrecords = db.Attendances.Where(a => a.StudentId == student.StudentId && a.AttendanceStatu.AttendanceAbbr == "A" && a.AttendanceDate >= firstDayOfMonth && a.AttendanceDate <= lastDayOfMonth).ToList();
                                    var leaverecords = db.Attendances.Where(a => a.StudentId == student.StudentId && (a.AttendanceStatu.AttendanceAbbr == "L" || a.AttendanceStatu.AttendanceAbbr == "SL") && a.AttendanceDate >= firstDayOfMonth && a.AttendanceDate <= lastDayOfMonth).ToList();
                                    StudentAttendance.Absent = Convert.ToString(absentrecords.Count());
                                    StudentAttendance.Leave = Convert.ToString(leaverecords.Count());

                                    //StudentAttendance.Attendance = studentattendance.Count() + " / " + days;

                                    for (DateTime i = firstDayOfMonth; i <= lastDayOfMonth; i = i.AddDays(1))
                                    {
                                        monthwiseAttendance = new AppMothlyAttendanceStatusModel();
                                        monthwiseAttendance.Date = ConvertDate(i);

                                        // get day name
                                        var dayno = i.DayOfWeek;
                                        var day = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(dayno);
                                        // get holiday
                                        EventTypeId = db.EventTypes.Where(m => m.EventType1.ToLower().Contains("Holiday")).FirstOrDefault().EventTypeId;
                                        var Event = db.Events.Where(n => n.StartDate == i && n.EventTypeId == EventTypeId).ToList();
                                        // get attendance by student
                                        var studentattendanceAbbr = db.Attendances.Where(a => a.StudentId == student.StudentId && a.AttendanceDate == i).FirstOrDefault();

                                        if (day == "Sunday")
                                            monthwiseAttendance.Status = "Sunday";
                                        else if (Event.Count > 0)
                                            monthwiseAttendance.Status = "Holiday";
                                        else
                                        {
                                            monthwiseAttendance.Status = studentattendanceAbbr != null ? studentattendanceAbbr.AttendanceStatu.AttendanceAbbr : "";
                                            //absentcount += studentattendanceAbbr != null ? 0 : 1;
                                        }

                                        // if current month day exceed the current date
                                        if (i > Curentdate)
                                            break;

                                        StudentAttendance.AttendanceList.Add(monthwiseAttendance);
                                    }

                                    // change Absent Days Count
                                    StudentAttendance.Absent = (Convert.ToInt32(StudentAttendance.Absent) + absentcount).ToString();

                                    monthlyAttendance.Add(StudentAttendance);
                                }

                                return Json(new
                                {
                                    status = "success",
                                    MonthlyAttendance = monthlyAttendance
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StudentActivities(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];

                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var StudentProfile = new StudentInfoModel();
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student.StudentId > 0)
                            {
                                IList<AppStudentActivity> StudentActivities = new List<AppStudentActivity>();

                                AppStudentActivity AppStudentActivity = new AppStudentActivity();
                                var studentactivitylist = db.StudentActivities.Where(s => s.StudentId == student.StudentId).ToList();
                                foreach (var sal in studentactivitylist)
                                {
                                    AppStudentActivity = new AppStudentActivity();
                                    AppStudentActivity.Description = sal.Description;
                                    AppStudentActivity.EffectiveDate = sal.EffectiveDate.HasValue ? sal.EffectiveDate.Value.ToString("dd/MM/yyyy") : "";
                                    AppStudentActivity.EndDate = sal.EndDate.HasValue ? sal.EndDate.Value.ToString("dd/MM/yyyy") : "";
                                    //get ExtraActivity name
                                    AppStudentActivity.Activity = sal.ExtraActivity.ExtraActivity1;
                                    StudentActivities.Add(AppStudentActivity);
                                }

                                return Json(new
                                {
                                    status = "success",
                                    StudentActivities = StudentActivities
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StudentFees(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];

                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student.StudentId > 0)
                            {
                                IList<AppStudentFee> AppStudentFeeList = new List<AppStudentFee>();

                                AppStudentFee AppStudentFee = new AppStudentFee();

                                // chcek student Current Session fee Receipts
                                var FeeReceipts = db.FeeReceipts.Where(f => f.StudentId == student.StudentId).ToList();
                                foreach (var feercpt in FeeReceipts)
                                {
                                    AppStudentFee = new AppStudentFee();
                                    AppStudentFee.ReceiptNo = feercpt.ReceiptNo.ToString();
                                    AppStudentFee.ReceiptDate = feercpt.ReceiptDate.Value.ToString("dd/MM/yyyy");
                                    // Fee Receipt Detail
                                    // Calculate Amount
                                    var paidamount = feercpt.PaidAmount;
                                    // get fee receipt detail
                                    var totalfeeamt = feercpt.FeeReceiptDetails.Where(f => f.GroupFeeHeadId != null).Select(f => f.Amount).Sum();
                                    var totaladdonfeeamt = feercpt.FeeReceiptDetails.Where(f => f.AddOnHeadId != null).Select(f => f.Amount).Sum();
                                    var totalfeeconsession = feercpt.FeeReceiptDetails.Where(f => f.ConsessionId != null).Select(f => f.Amount).Sum();
                                    var payableamt = (totalfeeamt + totaladdonfeeamt) - totalfeeconsession;

                                    AppStudentFee.Amount = feercpt.PaidAmount.ToString();
                                    AppStudentFee.TotalAmount = payableamt.ToString();
                                    AppStudentFee.PendingAmount = (payableamt - feercpt.PaidAmount).ToString();

                                    AppStudentFee.FeeType = feercpt.FeePeriod.FeeFrequency.FeeFrequency1;
                                    AppStudentFee.FeePeriod = feercpt.FeePeriod != null ? feercpt.FeePeriod.FeePeriodDescription : "";
                                    AppStudentFeeList.Add(AppStudentFee);
                                }

                                return Json(new
                                {
                                    status = "success",
                                    FeeList = AppStudentFeeList
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StudentTimeTable(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];

                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student.StudentId > 0)
                            {
                                // get student class

                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                // current Class and Rollno and Entarnce class
                                var currentstudentclass = db.StudentClasses.Where(s => s.StudentId == student.StudentId);
                                var currentstdclas = currentstudentclass.Where(m => m.SessionId == currentSession.SessionId).FirstOrDefault();

                                var CurrentClass = "";
                                if (currentstdclas != null)
                                    CurrentClass = currentstdclas.ClassMaster != null ? currentstdclas.ClassMaster.Class : "";

                                var curentdate = DateTime.UtcNow.Date;
                                // get current time table
                                var timetable = db.TimeTables.Where(t => t.EffectiveFrom <= curentdate && t.EffectiveTill >= curentdate).FirstOrDefault();
                                if (timetable != null)
                                {
                                    return Json(new
                                    {
                                        status = "success",
                                        TimeTableDates = timetable.EffectiveFrom.Value.ToString("dd/MM/yyyy") + " to " + timetable.EffectiveTill.Value.ToString("dd/MM/yyyy"),
                                        Class = CurrentClass,
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new
                                    {
                                        status = "failed",
                                        message = "TimeTable not found"
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StudentTimeTableByDay(FormCollection form)
        {
            string domain = "", UserName = "", Day = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Day"] != null)
                Day = form["Day"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Day"] != null)
                Day = Convert.ToString(Request.Headers["Day"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Day) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student.StudentId > 0)
                            {
                                // get student class
                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                // current Class and Rollno and Entarnce class
                                var currentstudentclass = db.StudentClasses.Where(s => s.StudentId == student.StudentId);
                                var currentstdclas = currentstudentclass.Where(m => m.SessionId == currentSession.SessionId).FirstOrDefault();

                                var CurrentClassId = 0;
                                if (currentstdclas != null)
                                    CurrentClassId = (int)currentstdclas.ClassId;

                                var curentdate = DateTime.UtcNow.Date;
                                // get current time table
                                var timetable = db.TimeTables.Where(t => t.EffectiveFrom <= curentdate && t.EffectiveTill >= curentdate).FirstOrDefault();
                                if (timetable != null)
                                {
                                    var DayExist = db.DayMasters.Where(d => d.DayName == Day).FirstOrDefault();

                                    // get class detail
                                    var ClassMaster = db.ClassMasters.Where(c => c.ClassId == CurrentClassId).FirstOrDefault();
                                    // get period detail
                                    var periods = db.Periods.OrderBy(p => p.PeriodIndex);
                                    // get all class subject
                                    var classsubjectids = db.ClassSubjects.Where(s => s.ClassId == CurrentClassId && s.IsActive == true)
                                        .Select(s => s.ClassSubjectId).ToArray();

                                    IList<AppStudentTimeTable> TimeTableGridModellist = new List<AppStudentTimeTable>();
                                    foreach (var period in periods)
                                    {
                                        AppStudentTimeTable TimeTableGridModel = new AppStudentTimeTable();
                                        TimeTableGridModel.Period = period.Period1;
                                        TimeTableGridModel.PeriodTime = ConvertTime((TimeSpan)period.StartTime) + "-" + ConvertTime((TimeSpan)period.EndTime);

                                        // get class period by periodid
                                        var classperiod = db.ClassPeriods.Where(c => c.PeriodId == period.PeriodId).ToList();
                                        foreach (var clasprd in classperiod)
                                        {
                                            if (classsubjectids.Contains((int)clasprd.ClassSubjectId))
                                            {
                                                var classperioddetail = db.ClassPeriodDetails.Where(c => c.ClassPeriodId == clasprd.ClassPeriodId
                                                    && c.TimeTableId == timetable.TimeTableId);

                                                foreach (var classperioddet in classperioddetail)
                                                {
                                                    var clasprdaydetail = db.ClassPeriodDays.Where(c => c.ClassPeriodDetailId == classperioddet.ClassPeriodDetailId && c.DayId == DayExist.DayId).FirstOrDefault();
                                                    if (clasprdaydetail != null)
                                                    {
                                                        TimeTableGridModel.Teacher = classperioddet.Staff.FName + " " + classperioddet.Staff.LName;

                                                        if (clasprd.ClassSubject.SkillId == null)
                                                            TimeTableGridModel.Subject = clasprd.ClassSubject.Subject.Subject1;
                                                        else
                                                        {
                                                            var skillsubject = db.SubjectSkills.Where(s => s.SubjectId == clasprd.ClassSubject.SubjectId
                                                                && s.SkillId == clasprd.ClassSubject.SkillId).FirstOrDefault();
                                                            TimeTableGridModel.Subject = skillsubject != null ? skillsubject.SubjectSkill1 : "";
                                                        }

                                                        TimeTableGridModellist.Add(TimeTableGridModel);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    return Json(new
                                    {
                                        status = "success",
                                        TimeTableList = TimeTableGridModellist
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new
                                    {
                                        status = "failed",
                                        message = "TimeTable not found"
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StudentExam(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student.StudentId > 0)
                            {
                                // get student class
                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                // current Class and Rollno and Entarnce class
                                var currentstudentclass = db.StudentClasses.Where(s => s.StudentId == student.StudentId);
                                var currentstdclas = currentstudentclass.Where(m => m.SessionId == currentSession.SessionId).FirstOrDefault();
                                if (currentstdclas != null)
                                {
                                    IList<AppStudentExams> AppStudentExamList = new List<AppStudentExams>();

                                    // current Date
                                    var currentdate = DateTime.UtcNow.Date;
                                    // class Standard
                                    int studentclassid = (int)currentstdclas.ClassId;
                                    var StandardId = currentstdclas.ClassMaster.StandardId;

                                    // get class subjects
                                    var classsubjects = db.ClassSubjects.Where(c => c.ClassId == studentclassid);

                                    // get Exam DateSheet
                                    var examdatesheets = db.ExamDateSheets.Where(e => e.TillDate >= currentdate && e.Ispublish == true).ToList();
                                    //loop
                                    foreach (var datesheet in examdatesheets)
                                    {
                                        // get all subject ids added into Exam Date sheet
                                        var examdatesheetdetail = db.ExamDateSheetDetails.Where(e => e.ExamDateSheetId == datesheet.ExamDateSheetId && e.StandardId == StandardId).OrderBy(e => e.ExamDate);
                                        foreach (var examdatesheetdet in examdatesheetdetail)
                                        {
                                            var IsClasssubjectsExist = classsubjects.Where(c => examdatesheetdet.SubjectId == c.SubjectId).FirstOrDefault();
                                            if (IsClasssubjectsExist != null)
                                            {
                                                AppStudentExams AppStudentExams = new AppStudentExams();

                                                // exam date
                                                AppStudentExams.ExamDate = examdatesheetdet.ExamDate.Value.ToString("dd/MM/yyyy");
                                                // Time slot
                                                AppStudentExams.TimeSlot = examdatesheetdet.ExamTimeSlot.ExamTimeSlot1;
                                                // marks pattren
                                                var markspatrn = examdatesheetdet.MarksPattern.MaxMarks + "-" + examdatesheetdet.MarksPattern.PassMarks;
                                                if (examdatesheetdet.MarksPattern.GradePattern != null)
                                                    markspatrn += "(" + examdatesheetdet.MarksPattern.GradePattern.GradePattern1 + ")";
                                                AppStudentExams.MarksPattern = markspatrn;

                                                // class and subject
                                                if (IsClasssubjectsExist.SkillId != null)
                                                {
                                                    var skillsubject = db.SubjectSkills.Where(s => s.SubjectId == IsClasssubjectsExist.SubjectId &&
                                                        s.SkillId == examdatesheetdet.SkillId).FirstOrDefault();
                                                    AppStudentExams.Subject = skillsubject != null ? skillsubject.SubjectSkill1 : "";
                                                }
                                                else
                                                    AppStudentExams.Subject = IsClasssubjectsExist.Subject.Subject1;

                                                AppStudentExamList.Add(AppStudentExams);
                                            }
                                        }
                                    }


                                    return Json(new
                                    {
                                        status = "success",
                                        Class = currentstdclas.ClassMaster.Class,
                                        Exams = AppStudentExamList
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new
                                    {
                                        status = "failed",
                                        message = "Class not assigned to Student"
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StudentTransport(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student.StudentId > 0)
                            {
                                IList<AppStudentTransport> AppStudentTransportList = new List<AppStudentTransport>();

                                // Transport History
                                var tptdetail = db.TptFacilities.Where(t => t.StudentId == student.StudentId).OrderByDescending(t => t.EffectiveDate);
                                foreach (var item in tptdetail)
                                {
                                    AppStudentTransport tpthistory = new AppStudentTransport();
                                    tpthistory.TransportType = item.TptTypeDetail.TptName;
                                    if (item.BoardingPoint != null)
                                    {
                                        tpthistory.BoardingPoint = item.BoardingPoint != null ? item.BoardingPoint.BoardingPoint1 : "";
                                        tpthistory.BusRoute = item.BusRoute != null ? item.BusRoute.BusRoute1 : "";
                                    }
                                    else
                                    {
                                        tpthistory.VehicleType = item.VehicleType != null ? item.VehicleType.VehicleType1 : "";
                                        tpthistory.VehicleNo = item.VehicleNo;
                                        tpthistory.Parking = item.ParkingRequired == true ? "Yes" : "No";
                                    }

                                    tpthistory.EffectiveDate = item.EffectiveDate.HasValue ? item.EffectiveDate.Value.ToString("dd/MM/yyyy") : "";
                                    tpthistory.EndDate = item.EndDate.HasValue ? item.EndDate.Value.ToString("dd/MM/yyyy") : "";

                                    AppStudentTransportList.Add(tpthistory);
                                }

                                return Json(new
                                {
                                    status = "success",
                                    TransportList = AppStudentTransportList
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StudentSchedular(FormCollection form)
        {
            string domain = "", UserName = "", EventType = "", EventTitle = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Event"] != null)
                EventType = form["Event"].Trim();
            if (form["Title"] != null)
                EventTitle = form["Title"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Event"] != null)
                EventType = Convert.ToString(Request.Headers["Event"]);
            if (Request.Headers["Title"] != null)
                EventTitle = Convert.ToString(Request.Headers["Title"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(EventType) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student.StudentId > 0)
                            {
                                // current date
                                var currentdate = DateTime.UtcNow.Date;
                                // previous days
                                // var prvdate = currentdate.AddDays(-30);

                                // get student class
                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                // current Class and Rollno and Entarnce class
                                var currentstudentclass = db.StudentClasses.Where(s => s.StudentId == student.StudentId);
                                var currentstdclas = currentstudentclass.Where(m => m.SessionId == currentSession.SessionId).FirstOrDefault();

                                AppStudentEvent AppStudentEvent = new AppStudentEvent();

                                // events
                                var events = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == EventType && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                                //var EventTitles = events.ToList();
                                //foreach (var titles in EventTitles)
                                //{
                                //    AppStudentEvent.EventTitles.Add(new AppStudentEventTitles { Title = titles.EventTitle });
                                //    if (string.IsNullOrEmpty(EventTitle))
                                //        EventTitle = titles.EventTitle;
                                //}
                                // create title list
                                // eventids 
                                if (events.Count == 0)
                                {
                                    return Json(new
                                    {
                                        status = "failed",
                                        message = "Record not found"
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                var eventtitleids = events.Select(e => e.EventId).ToArray();
                                if (currentstdclas != null)
                                {
                                    // get event classes
                                    var eventclassdetailEvents = db.EventDetails.Where(e => eventtitleids.Contains((int)e.EventId) && e.ClassId == currentstdclas.ClassId).Select(e => e.EventId).ToArray();

                                    if (!string.IsNullOrEmpty(EventType) && string.IsNullOrEmpty(EventTitle))
                                    {
                                        if (eventclassdetailEvents.Length > 0 && EventType == "Activity")
                                        {
                                            foreach (var item in eventclassdetailEvents)
                                            {
                                                AppStudentEvent.EventTitles.Add(new AppStudentEventTitles { Title = events.Where(p => p.EventId == item).FirstOrDefault().EventTitle });
                                                if (string.IsNullOrEmpty(EventTitle))
                                                    EventTitle = events.Where(p => p.EventId == item).FirstOrDefault().EventTitle;
                                            }
                                        }

                                        if (events.Count > 0 && eventclassdetailEvents.Length > 0 && EventType == "Activity")
                                        {
                                            //insert all titles except for the class specifically
                                            var newevents = events.Where(p => !eventclassdetailEvents.Contains(p.EventId)).ToList();

                                            foreach (var item in newevents)
                                            {
                                                AppStudentEvent.EventTitles.Add(new AppStudentEventTitles { Title = item.EventTitle });
                                                if (string.IsNullOrEmpty(EventTitle))
                                                    EventTitle = item.EventTitle;
                                            }
                                        }

                                        if (events.Count > 0 && eventclassdetailEvents.Length == 0 && EventType == "Activity")
                                        {
                                            var latestevents = events.Where(p => p.IsAllSchool == true).ToList();
                                            if (latestevents.Count > 0)
                                            {
                                                foreach (var item in latestevents)
                                                {
                                                    AppStudentEvent.EventTitles.Add(new AppStudentEventTitles { Title = item.EventTitle });
                                                    if (string.IsNullOrEmpty(EventTitle))
                                                        EventTitle = item.EventTitle;
                                                }
                                            }
                                        }

                                        if (EventType != "Activity")
                                        {
                                            foreach (var item in events)
                                            {
                                                AppStudentEvent.EventTitles.Add(new AppStudentEventTitles { Title = item.EventTitle });
                                                if (string.IsNullOrEmpty(EventTitle))
                                                    EventTitle = item.EventTitle;
                                            }
                                        }

                                        //return Json(new
                                        //{
                                        //    status = "success",
                                        //    Schedular = AppStudentEvent
                                        //}, JsonRequestBehavior.AllowGet);
                                    }
                                    else if (!string.IsNullOrEmpty(EventType))
                                    {
                                        foreach (var titles in events)
                                        {
                                            AppStudentEvent.EventTitles.Add(new AppStudentEventTitles { Title = titles.EventTitle });
                                            if (string.IsNullOrEmpty(EventTitle))
                                                EventTitle = titles.EventTitle;
                                        }
                                    }

                                    if (events.Count > 0)
                                    {
                                        var requiredevent = events.Where(p => p.EventTitle.Contains(EventTitle) && p.EventType.EventType1 == EventType).FirstOrDefault();
                                        if (requiredevent.IsAllSchool == true || requiredevent.IsAllSchool == false)
                                        {
                                            AppStudentEvent.StartTime = "";
                                            AppStudentEvent.Class = currentstdclas.ClassMaster.Class;
                                            AppStudentEvent.EventTitle = requiredevent.EventTitle;
                                            AppStudentEvent.Description = requiredevent != null ? requiredevent.Description : ""; ;
                                            if (EventType == "Activity" || EventType == "Vacation")
                                                AppStudentEvent.EndDate = requiredevent != null ? requiredevent.EndDate.Value.ToString("dd/MM/yyyy") : "";
                                            AppStudentEvent.StartDate = requiredevent != null ? requiredevent.StartDate.Value.ToString("dd/MM/yyyy") : "";
                                            AppStudentEvent.Status = requiredevent != null && requiredevent.IsActive == true ? "Active" : "InActive";
                                            // get Event images
                                            var eventimages = requiredevent.EventImages;
                                            foreach (var item in eventimages)
                                            {
                                                AppStudentEventImages AppStudentEventImages = new AppStudentEventImages();
                                                AppStudentEventImages.Image = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + dbdetail.DBId + "/Events/Activity/" + requiredevent.EventId + "/" + item.ImageName;
                                                AppStudentEvent.EventImages.Add(AppStudentEventImages);
                                            }
                                            return Json(new
                                            {
                                                status = "success",
                                                Schedular = AppStudentEvent
                                            }, JsonRequestBehavior.AllowGet);
                                        }
                                        else
                                        {

                                            if (eventclassdetailEvents.Contains(requiredevent.EventId))
                                            {
                                                var eve = events.Where(p => p.EventId == requiredevent.EventId).FirstOrDefault();
                                                AppStudentEvent.StartTime = "";
                                                AppStudentEvent.Class = currentstdclas.ClassMaster.Class;
                                                AppStudentEvent.EventTitle = eve.EventTitle;
                                                AppStudentEvent.Description = eve != null ? eve.Description : ""; ;
                                                AppStudentEvent.EndDate = eve != null ? eve.EndDate.Value.ToString("dd/MM/yyyy") : "";
                                                AppStudentEvent.StartDate = eve != null ? eve.StartDate.Value.ToString("dd/MM/yyyy") : "";
                                                AppStudentEvent.Status = eve != null && eve.IsActive == true ? "Active" : "InActive";
                                                // get Event images
                                                var eventimages = eve.EventImages;
                                                foreach (var item in eventimages)
                                                {
                                                    AppStudentEventImages AppStudentEventImages = new AppStudentEventImages();
                                                    AppStudentEventImages.Image = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + dbdetail.DBId + "/Events/Activity/" + eve.EventId + "/" + item.ImageName;
                                                    AppStudentEvent.EventImages.Add(AppStudentEventImages);
                                                }
                                                return Json(new
                                                {
                                                    status = "success",
                                                    Schedular = AppStudentEvent
                                                }, JsonRequestBehavior.AllowGet);
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    return Json(new
                                    {
                                        status = "success",
                                        message = "Class Not Assigned"
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    exception = ex.Message,
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult Calender(FormCollection form)
        {
            string domain = "", UserName = "", Token = "", date = "September 2017";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["date"] != null)
                date = form["date"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);

            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["date"] != null)
                date = Convert.ToString(Request.Headers["date"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        //-------------------------Common Parameters--------------------------
                        //standard
                        var standardid = 0;
                        // current date
                        var currentdate = DateTime.UtcNow.Date;
                        //Get CurrentSession
                        var currentSession = GetCurrentSession(dbdetail.DBName);

                        //'month' 'year' splitted from 'date' parameter
                        var splitdate = date.Split(' ');
                        var currentYear = Convert.ToInt32(splitdate[1]);
                        var currentMonth = DateTime.ParseExact(splitdate[0], "MMMM", new CultureInfo("en-US")).Month;

                        //get days in month
                        int days = DateTime.DaysInMonth(currentYear, currentMonth);



                        // events
                        var events = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.StartDate.Value.Month == currentMonth && p.StartDate.Value.Year == currentYear && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();

                        //get all examniation
                        // var Exams = db.ExamDateSheetDetails.ToList();

                        //-------------------------Common Parameters--------------------------

                        //-------------------------Common Model objects-----------------------
                        //declare 1nd level model
                        var CalenderData = new CalenderModel();
                        //declare 2nd level model
                        var DailyData = new DailyData();
                        //declare 3nd level model
                        var detail = new CommonDetailEvents();

                        //-------------------------Common Model objects-----------------------

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var studentcls = new StudentClass();
                            //get user standard
                            if (user.ContactType.ContactType1.ToLower() == "student")
                            {
                                studentcls = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault();
                                if (studentcls != null)
                                    standardid = (int)db.ClassMasters.Where(m => m.ClassId == studentcls.ClassId).FirstOrDefault().StandardId;
                            }
                            else if (user.ContactType.ContactType1.ToLower() == "guardian")
                            {
                                var student = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault();
                                studentcls = db.StudentClasses.Where(m => m.StudentId == student.StudentId).FirstOrDefault();
                                if (studentcls != null)
                                    standardid = (int)db.ClassMasters.Where(m => m.ClassId == studentcls.ClassId).FirstOrDefault().StandardId;
                            }
                            var datalist = new List<dynamic>();
                            //loop over days in month
                            for (int day = 1; day <= days; day++)
                            {

                                //initialize Ist level model
                                CalenderData = new CalenderModel();
                                DailyData = new DailyData();

                                //prepare date according to day year month
                                var newdate = new DateTime(currentYear, currentMonth, day);

                                CalenderData.Date = Convert.ToString(day);
                                #region Add events Lists
                                //get events data
                                if (events.Count > 0)
                                {
                                    //loop over events againt 'newdate'
                                    foreach (var evt in events.Where(m => m.StartDate == newdate))
                                    {
                                        //initialize 2nd level model
                                        // DailyData = new DailyData();
                                        detail = new CommonDetailEvents();



                                        //check if event for all school

                                        detail.Description = !string.IsNullOrEmpty(evt.Description) ? evt.Description : "";
                                        detail.EndDate = evt.EndDate != null ? ConvertDate((DateTime)evt.EndDate) : "";
                                        detail.EndTime = evt.EndTime != null ? ConvertTime((TimeSpan)evt.EndTime) : "";
                                        detail.EventTitle = !string.IsNullOrEmpty(evt.EventTitle) ? evt.EventTitle : "";
                                        detail.StartDate = evt.StartDate != null ? ConvertDate((DateTime)evt.StartDate) : "";
                                        detail.StartTime = evt.StartTime != null ? ConvertTime((TimeSpan)evt.StartTime) : "";
                                        detail.Type = (evt.EventType != null) ? evt.EventType.EventType1 : "";
                                        detail.Venue = !string.IsNullOrEmpty(evt.Venue) ? evt.Venue : "";
                                        if ((bool)evt.IsAllSchool)
                                        {
                                            //get class
                                            detail.Class = "All School";
                                            DailyData.Details.Add(detail);
                                        }
                                        else
                                        {
                                            var classstring = "";
                                            //get event details based on event id to get classes
                                            var eventdetails = db.EventDetails.Where(m => m.EventId == evt.EventId && m.ClassId == studentcls.ClassId).ToList();
                                            if (eventdetails.Count > 0)
                                            {
                                                foreach (var ed in eventdetails)
                                                {
                                                    var classdetail = db.ClassMasters.Where(m => m.ClassId == ed.ClassId).FirstOrDefault();
                                                    if (classdetail != null)
                                                    {
                                                        if (classstring != "")
                                                            classstring += ",";
                                                        classstring += classdetail.Class;
                                                    }
                                                }
                                                detail.Class = classstring;
                                                DailyData.Details.Add(detail);
                                            }
                                            else if (evt.EventType.EventType1.ToLower().Contains("announcement") || evt.EventType.EventType1.ToLower().Contains("holiday") || evt.EventType.EventType1.ToLower().Contains("vacation"))
                                            {
                                                detail.Class = "";
                                                DailyData.Details.Add(detail);
                                            }
                                        }
                                    }
                                }
                                #endregion

                                #region Add Examination Details
                                var Exams = db.ExamDateSheetDetails.Where(m => m.StandardId == (int)standardid && m.ExamDate == newdate).ToList();

                                if (Exams.Count > 0)
                                {
                                    var ExamsList = new CommonDetailEvents();
                                    foreach (var item in Exams)
                                    {
                                        //initialize 2nd level model
                                        // DailyData = new DailyData();
                                        var standard = db.StandardMasters.Where(m => m.StandardId == (int)item.StandardId).FirstOrDefault();
                                        var subject = db.Subjects.Where(m => m.SubjectId == (int)item.SubjectId).FirstOrDefault();
                                        var timeSlot = item.ExamTimeSlot.ExamTimeSlot1;
                                        if (standard != null && subject != null)
                                        {
                                            ExamsList.Class = "";
                                            ExamsList.Description = standard.Standard + "/" + subject.Subject1 + "(" + timeSlot + ")";
                                            ExamsList.EndDate = "";
                                            ExamsList.EndTime = "";
                                            ExamsList.EventTitle = "";
                                            ExamsList.StartDate = ConvertDate((DateTime)item.ExamDate);
                                            ExamsList.StartTime = "";
                                            ExamsList.Type = "Exam";
                                            ExamsList.Venue = "";
                                        }
                                        DailyData.Details.Add(ExamsList);
                                    }
                                }

                                #endregion

                                #region ExamResult
                                var exmsrsts = db.ExamResultDates.Where(m => m.ResultPublishDate == newdate && m.StandardId == standardid).ToList();
                                if (exmsrsts.Count > 0)
                                {
                                    var ExamsResultList = new CommonDetailEvents();
                                    var ResultStandardIds = exmsrsts.Select(p => p.StandardId).ToList();
                                    var ResultStandards = db.StandardMasters.Where(p => ResultStandardIds.Contains(p.StandardId));
                                    foreach (var item in exmsrsts)
                                    {
                                        ExamsResultList = new CommonDetailEvents();
                                        ExamsResultList.Class = "";
                                        ExamsResultList.Description = ResultStandards.Where(p => p.StandardId == item.StandardId).FirstOrDefault().Standard;
                                        ExamsResultList.EndDate = "";
                                        ExamsResultList.EndTime = "";
                                        ExamsResultList.EventTitle = "";
                                        ExamsResultList.StartDate = ConvertDate((DateTime)item.ResultPublishDate);
                                        ExamsResultList.StartTime = "";
                                        ExamsResultList.Type = "Exam Result";
                                        ExamsResultList.Venue = "";
                                        DailyData.Details.Add(ExamsResultList);
                                    }
                                    // ExamsResultList.Add(new ExamResultNotification { Standard = ResultStandards.Where(p => p.StandardId == item.StandardId).FirstOrDefault().Standard });

                                }
                                #endregion


                                if (DailyData.Details.Count > 0)
                                {
                                    CalenderData.Activities = events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Activity").ToList().Count() > 0 ? events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Activity").ToList().Count().ToString() : "0";
                                    CalenderData.Announcements = events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Announcement").ToList().Count() > 0 ? events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Announcement").ToList().Count().ToString() : "0";
                                    CalenderData.Holidays = events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Holiday").ToList().Count() > 0 ? events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Holiday").ToList().Count().ToString() : "0";
                                    CalenderData.Vacations = events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Vacation").ToList().Count() > 0 ? events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Vacation").ToList().Count().ToString() : "0";
                                    CalenderData.Exams = Exams.Count().ToString();
                                    CalenderData.ExamResult = exmsrsts.Count().ToString();
                                    CalenderData.DailyData.Add(DailyData);
                                    datalist.Add(CalenderData);
                                }
                            }
                            return Json(new
                            {
                                status = "Success",
                                // CalenderData = CalenderData,
                                datalist = datalist
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else if (user != null && (user.ContactType.ContactType1.ToLower() == "teacher"))
                        {
                            var staff = new Staff();
                            staff = db.Staffs.Where(s => s.StaffId == (int)user.UserContactId).FirstOrDefault();

                            var datalist = new List<dynamic>();
                            //loop over days in month
                            for (int day = 1; day <= days; day++)
                            {

                                //initialize Ist level model
                                CalenderData = new CalenderModel();
                                DailyData = new DailyData();

                                //prepare date according to day year month
                                var newdate = new DateTime(currentYear, currentMonth, day);

                                CalenderData.Date = Convert.ToString(day);
                                #region Add events Lists
                                //get events data
                                if (events.Count > 0)
                                {
                                    //loop over events againt 'newdate'
                                    foreach (var evt in events.Where(m => m.StartDate == newdate))
                                    {
                                        //initialize 2nd level model
                                        // DailyData = new DailyData();
                                        detail = new CommonDetailEvents();



                                        //check if event for all school

                                        detail.Description = !string.IsNullOrEmpty(evt.Description) ? evt.Description : "";
                                        detail.EndDate = evt.EndDate != null ? ConvertDate((DateTime)evt.EndDate) : "";
                                        detail.EndTime = evt.EndTime != null ? ConvertTime((TimeSpan)evt.EndTime) : "";
                                        detail.EventTitle = !string.IsNullOrEmpty(evt.EventTitle) ? evt.EventTitle : "";
                                        detail.StartDate = evt.StartDate != null ? ConvertDate((DateTime)evt.StartDate) : "";
                                        detail.StartTime = evt.StartTime != null ? ConvertTime((TimeSpan)evt.StartTime) : "";
                                        detail.Type = (evt.EventType != null) ? evt.EventType.EventType1 : "";
                                        detail.Venue = !string.IsNullOrEmpty(evt.Venue) ? evt.Venue : "";
                                        if ((bool)evt.IsAllSchool)
                                        {
                                            //get class
                                            detail.Class = "All School";
                                            DailyData.Details.Add(detail);
                                        }
                                        else
                                        {
                                            var classstring = "";
                                            //get event details based on event id to get classes
                                            var eventdetails = db.EventDetails.Where(m => m.EventId == evt.EventId).ToList();
                                            if (eventdetails.Count > 0)
                                            {
                                                foreach (var ed in eventdetails)
                                                {
                                                    var classdetail = db.ClassMasters.Where(m => m.ClassId == ed.ClassId).FirstOrDefault();
                                                    if (classdetail != null)
                                                    {
                                                        if (classstring != "")
                                                            classstring += ",";
                                                        classstring += classdetail.Class;
                                                    }
                                                }
                                                detail.Class = classstring;
                                                DailyData.Details.Add(detail);
                                            }
                                            else if (evt.EventType.EventType1.ToLower().Contains("announcement") || evt.EventType.EventType1.ToLower().Contains("holiday") || evt.EventType.EventType1.ToLower().Contains("vacation"))
                                            {
                                                detail.Class = "";
                                                DailyData.Details.Add(detail);
                                            }
                                        }
                                    }
                                }
                                #endregion

                                #region Add Examination Details
                                var Exams = db.ExamDateSheetDetails.Where(m => m.ExamDate == newdate).ToList();

                                if (Exams.Count > 0)
                                {
                                    var ExamsList = new CommonDetailEvents();
                                    foreach (var item in Exams)
                                    {
                                        //initialize 2nd level model
                                        // DailyData = new DailyData();
                                        var standard = db.StandardMasters.Where(m => m.StandardId == (int)item.StandardId).FirstOrDefault();
                                        var subject = db.Subjects.Where(m => m.SubjectId == (int)item.SubjectId).FirstOrDefault();
                                        var timeSlot = item.ExamTimeSlot.ExamTimeSlot1;
                                        if (standard != null && subject != null)
                                        {
                                            ExamsList.Class = "";
                                            ExamsList.Description = standard.Standard + "/" + subject.Subject1 + "(" + timeSlot + ")";
                                            ExamsList.EndDate = "";
                                            ExamsList.EndTime = "";
                                            ExamsList.EventTitle = "";
                                            ExamsList.StartDate = ConvertDate((DateTime)item.ExamDate);
                                            ExamsList.StartTime = "";
                                            ExamsList.Type = "Exam";
                                            ExamsList.Venue = "";
                                        }
                                        DailyData.Details.Add(ExamsList);
                                    }
                                }

                                #endregion

                                //#region ExamResult
                                //var exmsrsts = db.ExamResultDates.Where(m => m.ResultPublishDate == newdate).ToList();
                                //if (exmsrsts.Count > 0)
                                //{
                                //    var ExamsResultList = new List<ExamResultNotification>();
                                //    var ResultStandardIds = exmsrsts.Select(p => p.StandardId).ToList();
                                //    var ResultStandards = db.StandardMasters.Where(p => ResultStandardIds.Contains(p.StandardId));
                                //    foreach (var item in exmsrsts)
                                //        ExamsResultList.Add(new ExamResultNotification { Standard = ResultStandards.Where(p => p.StandardId == item.StandardId).FirstOrDefault().Standard });
                                //    DailyData.Details.Add(ExamsResultList);
                                //}
                                #region ExamResult
                                var exmsrsts = db.ExamResultDates.Where(m => m.ResultPublishDate == newdate).ToList();
                                if (exmsrsts.Count > 0)
                                {
                                    var ExamsResultList = new CommonDetailEvents();
                                    var ResultStandardIds = exmsrsts.Select(p => p.StandardId).ToList();
                                    var ResultStandards = db.StandardMasters.Where(p => ResultStandardIds.Contains(p.StandardId));
                                    foreach (var item in exmsrsts)
                                    {
                                        ExamsResultList = new CommonDetailEvents();
                                        ExamsResultList.Class = "";
                                        ExamsResultList.Description = ResultStandards.Where(p => p.StandardId == item.StandardId).FirstOrDefault().Standard;
                                        ExamsResultList.EndDate = "";
                                        ExamsResultList.EndTime = "";
                                        ExamsResultList.EventTitle = "";
                                        ExamsResultList.StartDate = ConvertDate((DateTime)item.ResultPublishDate);
                                        ExamsResultList.StartTime = "";
                                        ExamsResultList.Type = "Exam";
                                        ExamsResultList.Venue = "";
                                        DailyData.Details.Add(ExamsResultList);
                                    }
                                    // ExamsResultList.Add(new ExamResultNotification { Standard = ResultStandards.Where(p => p.StandardId == item.StandardId).FirstOrDefault().Standard });

                                }
                                #endregion
                                //  #endregion


                                if (DailyData.Details.Count > 0)
                                {
                                    CalenderData.Activities = events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Activity").ToList().Count() > 0 ? events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Activity").ToList().Count().ToString() : "0";
                                    CalenderData.Announcements = events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Announcement").ToList().Count() > 0 ? events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Announcement").ToList().Count().ToString() : "0";
                                    CalenderData.Holidays = events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Holiday").ToList().Count() > 0 ? events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Holiday").ToList().Count().ToString() : "0";
                                    CalenderData.Vacations = events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Vacation").ToList().Count() > 0 ? events.Where(m => m.StartDate == newdate && m.EventType.EventType1 == "Vacation").ToList().Count().ToString() : "0";
                                    CalenderData.Exams = Exams.Count().ToString();
                                    CalenderData.ExamResult = exmsrsts.Count().ToString();
                                    CalenderData.DailyData.Add(DailyData);
                                    datalist.Add(CalenderData);
                                }
                            }
                            return Json(new
                            {
                                status = "Success",
                                // CalenderData = CalenderData,
                                datalist = datalist
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StudentExamList(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();


            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);


            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student.StudentId > 0)
                            {
                                // get student class
                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                // current Class and Rollno and Entarnce class
                                var currentstudentclass = db.StudentClasses.Where(s => s.StudentId == student.StudentId);
                                var currentstdclas = currentstudentclass.Where(m => m.SessionId == currentSession.SessionId).FirstOrDefault();
                                if (currentstdclas != null)
                                {
                                    IList<AppStudentExamsList> AppStudentExamList = new List<AppStudentExamsList>();

                                    // current Date
                                    var currentdate = DateTime.UtcNow.Date;
                                    // class Standard
                                    int studentclassid = (int)currentstdclas.ClassId;
                                    var StandardId = currentstdclas.ClassMaster.StandardId;

                                    // get class subjects
                                    var classsubjects = db.ClassSubjects.Where(c => c.ClassId == studentclassid);

                                    // get Exam DateSheet
                                    var examdatesheets = db.ExamDateSheets.Where(e => e.Ispublish == true).OrderByDescending(e => e.FromDate).ToList();
                                    //loop
                                    foreach (var datesheet in examdatesheets)
                                    {

                                        AppStudentExamsList AppStudentExams = new AppStudentExamsList();
                                        // get all subject ids added into Exam Date sheet
                                        var examdatesheetdetail = db.ExamDateSheetDetails.Where(e => e.ExamDateSheetId == datesheet.ExamDateSheetId && e.StandardId == StandardId).OrderBy(e => e.ExamDate);
                                        if (examdatesheetdetail.FirstOrDefault() != null)
                                        {
                                            AppStudentExams.ExamActivity = datesheet.ExamTermActivity.ExamActivity.ExamActivity1;
                                            AppStudentExams.DateSheetName = datesheet.ExamDateSheet1;
                                            DateTime dtfrom = Convert.ToDateTime(datesheet.FromDate);
                                            AppStudentExams.FromDate = dtfrom.ToString("dd/MM/yyyy");
                                            DateTime dtTill = Convert.ToDateTime(datesheet.TillDate);
                                            AppStudentExams.TillDate = dtTill.ToString("dd/MM/yyyy");
                                            if (datesheet.FromDate <= currentdate && datesheet.TillDate >= currentdate)
                                            {
                                                AppStudentExams.Status = "Processing";
                                            }
                                            else if (datesheet.TillDate < currentdate)
                                            {
                                                AppStudentExams.Status = "Published";
                                            }
                                            else if (datesheet.FromDate > currentdate)
                                            {
                                                AppStudentExams.Status = "Upcoming";
                                            }
                                        }
                                        foreach (var examdatesheetdet in examdatesheetdetail)
                                        {
                                            string Marks = "";

                                            var examresult = db.ExamResults.Where(x => x.ExamDateSheetDetailId == examdatesheetdet.ExamDateSheetDetailId && x.StudentId == student.StudentId).FirstOrDefault();
                                            if (examresult != null)
                                            {
                                                Marks = Convert.ToString(examresult.ObtainedMarks);

                                            }

                                            var IsClasssubjectsExist = classsubjects.Where(c => examdatesheetdet.SubjectId == c.SubjectId).FirstOrDefault();
                                            if (IsClasssubjectsExist != null)
                                            {

                                                ExaminationDateSheetDetail DateSheetDetail = new ExaminationDateSheetDetail();
                                                // exam date
                                                DateSheetDetail.ExamDate = examdatesheetdet.ExamDate.Value.ToString("dd/MM/yyyy");
                                                // Time slot
                                                DateSheetDetail.TimeSlot = examdatesheetdet.ExamTimeSlot.ExamTimeSlot1;
                                                // marks pattren
                                                var markspatrn = examdatesheetdet.MarksPattern.MaxMarks + "-" + examdatesheetdet.MarksPattern.PassMarks;
                                                if (examdatesheetdet.MarksPattern.GradePattern != null)
                                                    markspatrn += "(" + examdatesheetdet.MarksPattern.GradePattern.GradePattern1 + ")";
                                                DateSheetDetail.MarksPattern = markspatrn;

                                                // class and subject
                                                if (IsClasssubjectsExist.SkillId != null)
                                                {
                                                    var skillsubject = db.SubjectSkills.Where(s => s.SubjectId == IsClasssubjectsExist.SubjectId &&
                                                        s.SkillId == examdatesheetdet.SkillId).FirstOrDefault();
                                                    DateSheetDetail.Subject = skillsubject != null ? skillsubject.SubjectSkill1 : "";
                                                }
                                                else
                                                    DateSheetDetail.Subject = IsClasssubjectsExist.Subject.Subject1;
                                                DateSheetDetail.MaxMarks = Convert.ToString(examdatesheetdet.MarksPattern.MaxMarks);
                                                DateSheetDetail.obtainedMarks = Marks;

                                                AppStudentExams.DateSheetDetail.Add(DateSheetDetail);
                                            }

                                        }
                                        if (AppStudentExams.DateSheetName != null)
                                        {
                                            AppStudentExamList.Add(AppStudentExams);
                                        }

                                    }


                                    return Json(new
                                    {
                                        status = "success",
                                        Class = currentstdclas.ClassMaster.Class,
                                        Exams = AppStudentExamList
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new
                                    {
                                        status = "failed",
                                        message = "Class not assigned to Student"
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Staff API's

        [HttpPost]
        public ActionResult StaffInfo(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "teacher" || user.ContactType.ContactType1.ToLower() == "non-teaching"))
                        {
                            var staff = new Staff();
                            staff = db.Staffs.Where(s => s.StaffId == (int)user.UserContactId).FirstOrDefault();

                            if (staff != null)
                            {
                                IList<NavList> NavList = new List<NavList>();
                                NavList Nav = new NavList();
                                Nav.Name = "StaffProfile";
                                Nav.URl = _WebHelper.GetStoreHost(false) + "/" + User + "/API/StaffProfile";
                                NavList.Add(Nav);


                                return Json(new
                                {
                                    status = "success",
                                    Tabs = NavList
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StaffProfile(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "teacher" || user.ContactType.ContactType1.ToLower() == "non-teaching"))
                        {
                            var StaffProfile = new StaffInfoModel();
                            var staff = new Staff();
                            staff = db.Staffs.Where(s => s.StaffId == (int)user.UserContactId).FirstOrDefault();


                            if (staff.StaffId > 0)
                            {
                                // taecher conatct type
                                var staffcontacttype = db.ContactTypes.Where(c => c.ContactType1.ToLower() == "teacher").FirstOrDefault(); // Student contact type
                                // non-teaching contact type
                                var nonteachingcontacttype = db.ContactTypes.Where(c => c.ContactType1.ToLower() == "non-teaching").FirstOrDefault(); // Guardian contact type


                                var genderslist = db.Genders;
                                // StudentProfile.DOB = staff.DOB.HasValue ? staff.DOB.Value.ToString("dd/MM/yyyy") : "";
                                StaffProfile.Employeecode = staff.EmpCode;
                                StaffProfile.Department = staff.Department != null ? staff.Department.Department1 : "";
                                StaffProfile.Designation = staff.Designation != null ? staff.Designation.Designation1 : "";
                                var gender = genderslist.Where(m => m.GenderId == staff.GenderId).FirstOrDefault();
                                StaffProfile.Gender = gender != null ? gender.Gender1 : "";


                                // Contact info
                                var contatinfotype = db.ContactInfoTypes.Where(c => c.ContactInfoType1 == "Email").FirstOrDefault(); // info type 
                                var staffcontactinfo = db.ContactInfoes.Where(c => c.ContactInfoTypeId == contatinfotype.ContactInfoTypeId &&
                                    c.ContactId == staff.StaffId && (c.ContactTypeId == staffcontacttype.ContactTypeId || c.ContactTypeId == nonteachingcontacttype.ContactTypeId)).FirstOrDefault();

                                var phonecontactinfoType = db.ContactInfoTypes.Where(c => c.ContactInfoType1 == "Mobile").FirstOrDefault();
                                var staffPhoneContactInfo = db.ContactInfoes.Where(c => c.ContactInfoTypeId == phonecontactinfoType.ContactInfoTypeId &&
                                    c.ContactId == staff.StaffId && (c.ContactTypeId == staffcontacttype.ContactTypeId || c.ContactTypeId == nonteachingcontacttype.ContactTypeId)).FirstOrDefault();

                                StaffProfile.Email = staffcontactinfo != null ? staffcontactinfo.ContactInfo1 : "";
                                StaffProfile.PhoneNo = staffPhoneContactInfo != null ? staffPhoneContactInfo.ContactInfo1 : "";

                                // Address
                                var staffaddress = db.Addresses.Where(a => a.ContactId == staff.StaffId && (a.ContactTypeId == staffcontacttype.ContactTypeId || a.ContactTypeId == nonteachingcontacttype.ContactTypeId)).ToList();
                                if (staffaddress.Count > 0)
                                {
                                    // check permanent Address
                                    var ParmenentAddress = staffaddress.Where(a => a.AddressType.AddressType1 == "Parmanent").FirstOrDefault();
                                    if (ParmenentAddress == null)
                                        ParmenentAddress = staffaddress.FirstOrDefault();

                                    StaffProfile.AddressType = ParmenentAddress.AddressType.AddressType1;
                                    StaffProfile.PlotNo = ParmenentAddress.PlotNo;
                                    StaffProfile.SectorOrStreet = ParmenentAddress.Sector_Street;
                                    StaffProfile.Locality = ParmenentAddress.Locality;
                                    StaffProfile.Zip = ParmenentAddress.Zip;
                                    StaffProfile.City = ParmenentAddress.AddressCity.City;
                                }


                                return Json(new
                                {
                                    status = "success",
                                    Profile = StaffProfile
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StaffAttendance(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "teacher" || user.ContactType.ContactType1.ToLower() == "non-teaching"))
                        {
                            var Staff = new Staff();
                            Staff = db.Staffs.Where(s => s.StaffId == (int)user.UserContactId).FirstOrDefault();


                            if (Staff.StaffId > 0)
                            {
                                // Entrance Class
                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                var Curentdate = DateTime.UtcNow.Date;
                                // Sundays count
                                int Sundaycount = 0;
                                var sundayslist = _IMainService.GetSundayCountBetweenDates(ref Sundaycount, currentSession.StartDate.Value, Curentdate);

                                // get holidays
                                var holidayslist = db.Events.Where(e => e.EventType.EventType1 == "Holidays" && e.StartDate >= currentSession.StartDate
                                    && e.StartDate <= Curentdate && e.IsActive == true && e.IsDeleted != true).Select(e => (DateTime)e.StartDate).ToList();

                                // combine both list and get distinct
                                var alldatelist = sundayslist.Concat(holidayslist).Distinct().ToList();

                                // get attendance by student
                                var staffattendance = db.StaffAttendances.Where(a => a.StaffId == Staff.StaffId && a.StaffAttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();

                                // calculate attendance %
                                TimeSpan diff = Curentdate - currentSession.StartDate.Value;
                                int days = diff.Days - alldatelist.Count;

                                var PresentPerc = staffattendance.Count > 0 ? (staffattendance.Count * 100) / days : 0;
                                var AbsentPerc = 100 - PresentPerc;

                                return Json(new
                                {
                                    status = "success",
                                    Present = PresentPerc,
                                    Absent = AbsentPerc
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StaffMonthWiseAttendance(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "teacher" || user.ContactType.ContactType1.ToLower() == "non-teaching"))
                        {
                            var StudentProfile = new StudentInfoModel();
                            var staff = new Staff();
                            staff = db.Staffs.Where(s => s.StaffId == (int)user.UserContactId).FirstOrDefault();

                            if (staff.StaffId > 0)
                            {
                                // Entrance Class
                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                var Curentdate = DateTime.UtcNow.Date;
                                //monthsCount
                                int monthscount = 0;
                                int absentcount = 0;
                                var Monthslist = _IMainService.GetMonthCountBetweenDates(ref monthscount, currentSession.StartDate.Value, Curentdate);
                                AppStudentAttendanceModel StaffAttendance = new AppStudentAttendanceModel();
                                IList<AppStudentAttendanceModel> monthlyAttendance = new List<AppStudentAttendanceModel>();
                                AppMothlyAttendanceStatusModel monthwiseAttendance = new AppMothlyAttendanceStatusModel();
                                var EventTypeId = db.EventTypes.Where(m => m.EventType1.ToLower().Contains("Holiday")).FirstOrDefault().EventTypeId;
                                foreach (var ml in Monthslist)
                                {
                                    StaffAttendance = new AppStudentAttendanceModel();
                                    absentcount = 0;
                                    //months start and end dates
                                    var firstDayOfMonth = new DateTime(ml.Date.Year, ml.Date.Month, 1); // first day of month
                                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1); // last day of month
                                    // Sundays count
                                    //int Sundaycount = 0;
                                    // var sundayslist = _IMainService.GetSundayCountBetweenDates(ref Sundaycount, currentSession.StartDate.Value, Curentdate);
                                    //var sundayslist = _IMainService.GetSundayCountBetweenDates(ref Sundaycount, firstDayOfMonth, lastDayOfMonth);

                                    // get holidays
                                    //var holidayslist = db.Events.Where(e => e.EventType.EventType1 == "Holidays" && e.StartDate >= currentSession.StartDate
                                    //    && e.StartDate <= Curentdate && e.IsActive == true && e.IsDeleted != true).Select(e => (DateTime)e.StartDate).ToList();

                                    // combine both list and get distinct
                                    //var alldatelist = sundayslist.Concat(holidayslist).Distinct().ToList();

                                    // get attendance by student
                                    var staffattendance = db.StaffAttendances.Where(a => a.StaffId == staff.StaffId && a.StaffAttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= firstDayOfMonth && a.AttendanceDate <= lastDayOfMonth).ToList();
                                    TimeSpan diff = lastDayOfMonth.AddDays(1) - firstDayOfMonth;
                                    // calculate attendance %
                                    if (ml.Month == Curentdate.Month)
                                        diff = Curentdate.AddDays(1) - firstDayOfMonth;
                                    var holidaycount = db.Events.AsNoTracking().Where(m => m.StartDate.Value.Month == ml.Date.Month && m.EventTypeId == EventTypeId).Count();
                                    int sundaycount = 0;
                                    for (int i = 0; i < diff.Days; i++)
                                    {
                                        DateTime dt = new DateTime(ml.Date.Year, ml.Date.Month, i + 1);
                                        if (dt.DayOfWeek == DayOfWeek.Sunday)
                                        {
                                            sundaycount++;
                                        }
                                    }
                                    //workingDays
                                    int days = diff.Days - (Convert.ToInt32(holidaycount) + sundaycount);
                                    string SDays = days.ToString().Replace("-", "");
                                    //workingDays
                                    // int days = diff.Days;  //-alldatelist.Count;
                                    StaffAttendance.Month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(ml.Month);

                                    //workingDays
                                    StaffAttendance.Total = Convert.ToString(SDays);
                                    StaffAttendance.Present = Convert.ToString(staffattendance.Count());
                                    var absentrecords = db.StaffAttendances.Where(a => a.StaffId == staff.StaffId && a.StaffAttendanceStatu.AttendanceAbbr == "A" && a.AttendanceDate >= firstDayOfMonth && a.AttendanceDate <= lastDayOfMonth).ToList();
                                    var leaverecords = db.StaffAttendances.Where(a => a.StaffId == staff.StaffId && (a.StaffAttendanceStatu.AttendanceAbbr == "L" || a.StaffAttendanceStatu.AttendanceAbbr == "SL") && a.AttendanceDate >= firstDayOfMonth && a.AttendanceDate <= lastDayOfMonth).ToList();
                                    StaffAttendance.Absent = Convert.ToString(absentrecords.Count());
                                    StaffAttendance.Leave = Convert.ToString(leaverecords.Count());



                                    for (DateTime i = firstDayOfMonth; i <= lastDayOfMonth; i = i.AddDays(1))
                                    {
                                        if (i >= Curentdate)
                                            break;

                                        monthwiseAttendance = new AppMothlyAttendanceStatusModel();
                                        monthwiseAttendance.Date = ConvertDate(i);
                                        // check sunday
                                        var dayno = i.DayOfWeek;
                                        var day = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(dayno);
                                        // check holiday
                                        EventTypeId = db.EventTypes.Where(m => m.EventType1.ToLower().Contains("Holiday")).FirstOrDefault().EventTypeId;
                                        var Event = db.Events.Where(n => n.StartDate == i && n.EventTypeId == EventTypeId).ToList();
                                        // check attendance
                                        // get attendance by staff
                                        var staffattendanceAbbr = db.StaffAttendances.Where(a => a.StaffId == staff.StaffId && a.AttendanceDate == i).FirstOrDefault();

                                        if (day == "Sunday")
                                            monthwiseAttendance.Status = "Sunday";
                                        else if (Event.Count > 0)
                                            monthwiseAttendance.Status = "Holiday";
                                        else
                                        {
                                            monthwiseAttendance.Status = staffattendanceAbbr != null ? staffattendanceAbbr.StaffAttendanceStatu.AttendanceAbbr : "";
                                            absentcount += staffattendanceAbbr != null ? 0 : 1;
                                        }

                                        StaffAttendance.AttendanceList.Add(monthwiseAttendance);
                                    }

                                    StaffAttendance.Absent = (Convert.ToInt32(StaffAttendance.Absent) + absentcount).ToString();

                                    monthlyAttendance.Add(StaffAttendance);
                                }


                                return Json(new
                                {
                                    status = "success",
                                    MonthlyAttendance = monthlyAttendance
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StaffSchedular(FormCollection form)
        {
            string domain = "", UserName = "", EventType = "", EventTitle = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Event"] != null)
                EventType = form["Event"].Trim();
            if (form["Title"] != null)
                EventTitle = form["Title"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Event"] != null)
                EventType = Convert.ToString(Request.Headers["Event"]);
            if (Request.Headers["Title"] != null)
                EventTitle = Convert.ToString(Request.Headers["Title"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(EventType) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "teacher" || user.ContactType.ContactType1.ToLower() == "non-teaching"))
                        {
                            var staff = new Staff();
                            staff = db.Staffs.Where(s => s.StaffId == (int)user.UserContactId).FirstOrDefault();

                            if (staff.StaffId > 0)
                            {
                                // current date
                                var currentdate = DateTime.UtcNow.Date;
                                // previous days
                                //var prvdate = currentdate.AddDays(-30);
                                // get student class
                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                // current Class and Rollno and Entarnce class
                                //var currentstudentclass = db.StudentClasses.Where(s => s.StudentId == student.StudentId);
                                //var currentstdclas = currentstudentclass.Where(m => m.SessionId == currentSession.SessionId).FirstOrDefault();

                                AppStudentEvent AppStudentEvent = new AppStudentEvent();
                                AppStudentEvent.EventImages = new List<AppStudentEventImages>();
                                AppStudentEvent.EventTitles = new List<AppStudentEventTitles>();
                                AppStudentEvent.EndDate = "";
                                // events
                                var events = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == EventType && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                                if (events.Count == 0)
                                    return Json(new
                                    {
                                        status = "failed",
                                        message = "Record Not Found"
                                    }, JsonRequestBehavior.AllowGet);
                                // create title list
                                var EventTitles = events.ToList();
                                foreach (var titles in EventTitles)
                                {
                                    AppStudentEvent.EventTitles.Add(new AppStudentEventTitles { Title = titles.EventTitle });
                                    if (string.IsNullOrEmpty(EventTitle))
                                        EventTitle = titles.EventTitle;
                                }
                                if (events.Count() > 0)
                                {
                                    var Stdevent = new Event();
                                    // check if Event type is Activity
                                    if (EventType == "Activity")
                                    {
                                        if (!string.IsNullOrEmpty(EventTitle))
                                            Stdevent = events.Where(e => e.EventTitle == EventTitle).FirstOrDefault();
                                        else
                                            Stdevent = events.FirstOrDefault();
                                        if (Stdevent != null)
                                        {
                                            AppStudentEvent.Class = "";
                                            AppStudentEvent.StartTime = "";

                                            AppStudentEvent.EventTitle = Stdevent.EventTitle;
                                            AppStudentEvent.Description = Stdevent != null ? Stdevent.Description : ""; ;
                                            AppStudentEvent.EndDate = Stdevent != null ? Stdevent.EndDate.Value.ToString("dd/MM/yyyy") : "";
                                            AppStudentEvent.StartDate = Stdevent != null ? Stdevent.StartDate.Value.ToString("dd/MM/yyyy") : "";
                                            AppStudentEvent.Status = Stdevent != null && Stdevent.IsActive == true ? "Active" : "InActive";
                                            AppStudentEvent.Venue = Stdevent.Venue != null ? Stdevent.Venue : "";
                                            // get Event images
                                            var eventimages = Stdevent.EventImages;
                                            foreach (var item in eventimages)
                                            {
                                                AppStudentEventImages AppStudentEventImages = new AppStudentEventImages();
                                                AppStudentEventImages.Image = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + dbdetail.DBId + "/Events/Activity/" + Stdevent.EventId + "/" + item.ImageName;
                                                AppStudentEvent.EventImages.Add(AppStudentEventImages);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(EventTitle))
                                            Stdevent = events.Where(e => e.EventTitle.Contains(EventTitle)).FirstOrDefault();
                                        else
                                            Stdevent = events.FirstOrDefault();

                                        if (Stdevent != null)
                                        {
                                            AppStudentEvent.Class = "";
                                            AppStudentEvent.StartTime = "";

                                            AppStudentEvent.EventTitle = Stdevent.EventTitle;
                                            AppStudentEvent.Description = !string.IsNullOrEmpty(Stdevent.Description) ? Stdevent.Description : "";
                                            AppStudentEvent.EndDate = Stdevent.EndDate.HasValue ? Stdevent.EndDate.Value.ToString("dd/MM/yyyy") : "";
                                            AppStudentEvent.StartDate = Stdevent.StartDate.HasValue ? Stdevent.StartDate.Value.ToString("dd/MM/yyyy") : "";
                                            AppStudentEvent.Status = Stdevent.IsActive == true ? "Active" : "InActive";
                                            AppStudentEvent.Venue = Stdevent.Venue != null ? Stdevent.Venue : "";
                                        }
                                    }

                                    return Json(new
                                    {
                                        status = "success",
                                        Schedular = AppStudentEvent
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new
                                    {
                                        status = "success",
                                        message = "No Event found"
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StaffTimeTable(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "teacher"))
                        {
                            var staff = new Staff();
                            staff = db.Staffs.Where(s => s.StaffId == (int)user.UserContactId).FirstOrDefault();

                            if (staff.StaffId > 0)
                            {

                                var curentdate = DateTime.UtcNow.Date;
                                // get current time table
                                var timetable = db.TimeTables.Where(t => t.EffectiveFrom <= curentdate && t.EffectiveTill >= curentdate).FirstOrDefault();
                                if (timetable != null)
                                {
                                    return Json(new
                                    {
                                        status = "success",
                                        TimeTableDates = timetable.EffectiveFrom.Value.ToString("dd/MM/yyyy") + " to " + timetable.EffectiveTill.Value.ToString("dd/MM/yyyy"),
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new
                                    {
                                        status = "failed",
                                        message = "TimeTable not found"
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StaffTimeTableByDay(FormCollection form)
        {
            string domain = "", UserName = "", Day = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Day"] != null)
                Day = form["Day"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Day"] != null)
                Day = Convert.ToString(Request.Headers["Day"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Day) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "teacher"))
                        {
                            var staff = new Staff();
                            staff = db.Staffs.Where(s => s.StaffId == (int)user.UserContactId).FirstOrDefault();

                            if (staff.StaffId > 0)
                            {

                                var curentdate = DateTime.UtcNow.Date;
                                // get current time table
                                var timetable = db.TimeTables.Where(t => t.EffectiveFrom <= curentdate && t.EffectiveTill >= curentdate).FirstOrDefault();
                                if (timetable != null)
                                {
                                    var DayExist = db.DayMasters.Where(d => d.DayName == Day).FirstOrDefault();

                                    // get period detail
                                    var periods = db.Periods.OrderBy(p => p.PeriodIndex);

                                    IList<AppStaffTimeTable> TimeTableGridModellist = new List<AppStaffTimeTable>();
                                    foreach (var period in periods)
                                    {
                                        AppStaffTimeTable TimeTableGridModel = new AppStaffTimeTable();
                                        TimeTableGridModel.Period = period.Period1;
                                        TimeTableGridModel.PeriodTime = ConvertTime((TimeSpan)period.StartTime) + "-" + ConvertTime((TimeSpan)period.EndTime);

                                        // get class period by periodid
                                        var classperiodIds = db.ClassPeriods.Where(c => c.PeriodId == period.PeriodId).Select(c => c.ClassPeriodId).ToArray();
                                        //foreach (var clasprd in classperiod)
                                        //{
                                        //if (classsubjectids.Contains((int)clasprd.ClassSubjectId))
                                        //{
                                        var classperioddetail = db.ClassPeriodDetails.Where(c => classperiodIds.Contains((int)c.ClassPeriodId)
                                            && c.TimeTableId == timetable.TimeTableId && c.TeacherId == user.UserContactId);

                                        foreach (var classperioddet in classperioddetail)
                                        {
                                            var clasprdaydetail = db.ClassPeriodDays.Where(c => c.ClassPeriodDetailId == classperioddet.ClassPeriodDetailId && c.DayId == DayExist.DayId).FirstOrDefault();
                                            if (clasprdaydetail != null)
                                            {
                                                TimeTableGridModel.Class = classperioddet.ClassPeriod.ClassSubject.ClassMaster.Class;

                                                if (classperioddet.ClassPeriod.ClassSubject.SkillId == null)
                                                    TimeTableGridModel.Subject = classperioddet.ClassPeriod.ClassSubject.Subject.Subject1;
                                                else
                                                {
                                                    var skillsubject = db.SubjectSkills.Where(s => s.SubjectId == classperioddet.ClassPeriod.ClassSubject.SubjectId
                                                        && s.SkillId == classperioddet.ClassPeriod.ClassSubject.SkillId).FirstOrDefault();
                                                    TimeTableGridModel.Subject = skillsubject != null ? skillsubject.SubjectSkill1 : "";
                                                }

                                                TimeTableGridModellist.Add(TimeTableGridModel);
                                            }
                                        }
                                        //}
                                        //}
                                    }

                                    return Json(new
                                    {
                                        status = "success",
                                        TimeTableList = TimeTableGridModellist
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new
                                    {
                                        status = "failed",
                                        message = "TimeTable not found"
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StaffExaminationClasses(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "teacher"))
                        {
                            var staff = new Staff();
                            staff = db.Staffs.Where(s => s.StaffId == (int)user.UserContactId).FirstOrDefault();

                            if (staff.StaffId > 0)
                            {

                                var curentdate = DateTime.UtcNow.Date;

                                // current Date
                                var currentdate = DateTime.UtcNow.Date;

                                // get current time table
                                var ExamStandardIdlist = db.ExamDateSheetDetails.Where(t => t.ExamDateSheet.TillDate >= currentdate && t.ExamDateSheet.Ispublish == true).Select(t => t.StandardId).Distinct();
                                var StandardList = db.TeacherClasses.Where(m => ExamStandardIdlist.Contains(m.StandardId) && m.TeacherId == user.UserContactId).Select(m => m.StandardMaster.Standard);
                                // Class List
                                List<ClassList> AvailableClassList = new List<ClassList>();
                                var selecteddate = DateTime.UtcNow.Date;
                                var selectedDay = selecteddate.DayOfWeek;
                                var day = db.DayMasters.Where(m => m.DayName == selectedDay.ToString()).FirstOrDefault();
                                var timetables = db.TimeTables.ToList();
                                var timetable = timetables.Where(t => t.EffectiveFrom <= selecteddate.Date && t.EffectiveTill >= selecteddate.Date).FirstOrDefault();
                                if (timetable != null)
                                {
                                    var classDropdownlist = new List<SelectListItem>();
                                    var allclassPerioddetails = db.ClassPeriodDetails.ToList();
                                    // get all class period detail by time table andt  teacher
                                    var classperioddetails = db.ClassPeriodDetails.Where(e => e.TeacherId == user.UserContactId && e.TimeTableId == timetable.TimeTableId).ToList();
                                    if (classperioddetails.Count > 0)
                                    {
                                        var classperiodday = db.ClassPeriodDays.Where(m => m.DayId == day.DayId).ToList();
                                        var ClassPeriodDetailIdarray = classperiodday.Count > 0 ? classperiodday.Select(m => (int)m.ClassPeriodDetailId).ToArray() : new int[] { };
                                        ClassPeriodDetailIdarray = classperioddetails.Where(m => ClassPeriodDetailIdarray.Contains((int)m.ClassPeriodDetailId)).Select(m => m.ClassPeriodDetailId).ToArray();
                                        var adjustmentclasssprddetails = db.TimeTableAdjustments.Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate == selecteddate.Date).Select(m => (int)m.ClassPeriodDetailId).ToArray();
                                        var finalClassPeriodDetailIdarray = ClassPeriodDetailIdarray.Union(adjustmentclasssprddetails).Distinct().ToArray();
                                        if (finalClassPeriodDetailIdarray.Length > 0)
                                        {
                                            classperioddetails = allclassPerioddetails.Where(m => finalClassPeriodDetailIdarray.Contains(m.ClassPeriodDetailId)).ToList();
                                            var classidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassId).Distinct().ToList();
                                            var classsubjectidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassSubjectId).ToList();
                                            if (classidlist.Count > 0 && classsubjectidlist.Count > 0)
                                            {

                                                foreach (var classid in classidlist)
                                                {
                                                    var ClassMaster = new ClassMaster();
                                                    ClassMaster = (from s in db.ClassMasters.AsNoTracking() where s.ClassId == classid select s).FirstOrDefault();
                                                    AvailableClassList.Add(new ClassList { Class = ClassMaster.Class, ClassId = classid.ToString() });
                                                }
                                            }
                                        }
                                    }
                                }



                                if (StandardList.Count() > 0)
                                {
                                    return Json(new
                                    {
                                        status = "success",
                                        Standards = StandardList,
                                        Classes = AvailableClassList
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new
                                    {
                                        status = "failed",
                                        message = "Classes not found"
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StaffExam(FormCollection form)
        {
            string domain = "", UserName = "", Standard = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Standard"] != null)
                Standard = form["Standard"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Standard"] != null)
                Standard = Convert.ToString(Request.Headers["Standard"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Standard) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "teacher"))
                        {
                            var staff = new Staff();
                            staff = db.Staffs.Where(s => s.StaffId == user.UserContactId).FirstOrDefault();

                            if (staff.StaffId > 0)
                            {
                                // get student class
                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                IList<AppStudentExams> AppStudentExamList = new List<AppStudentExams>();

                                // current Date
                                var currentdate = DateTime.UtcNow.Date;
                                var StandardId = db.StandardMasters.Where(m => m.Standard == Standard).FirstOrDefault().StandardId;

                                // get Exam DateSheet
                                var examdatesheets = db.ExamDateSheets.Where(e => e.TillDate >= currentdate && e.Ispublish == true).ToList();
                                //loop
                                foreach (var datesheet in examdatesheets)
                                {
                                    // get all subject ids added into Exam Date sheet
                                    var examdatesheetdetail = db.ExamDateSheetDetails.Where(e => e.ExamDateSheetId == datesheet.ExamDateSheetId && e.StandardId == StandardId).OrderBy(e => e.ExamDate);
                                    foreach (var examdatesheetdet in examdatesheetdetail)
                                    {
                                        var IsClasssubjectsExist = db.ClassSubjects.Where(c => c.ClassMaster.StandardMaster.StandardId == StandardId && examdatesheetdet.SubjectId == c.SubjectId).FirstOrDefault();
                                        if (IsClasssubjectsExist != null)
                                        {
                                            AppStudentExams AppStudentExams = new AppStudentExams();

                                            // exam date
                                            AppStudentExams.ExamDate = examdatesheetdet.ExamDate.Value.ToString("dd/MM/yyyy");
                                            // Time slot
                                            AppStudentExams.TimeSlot = examdatesheetdet.ExamTimeSlot.ExamTimeSlot1;
                                            // marks pattren
                                            var markspatrn = examdatesheetdet.MarksPattern.MaxMarks + "-" + examdatesheetdet.MarksPattern.PassMarks;
                                            if (examdatesheetdet.MarksPattern.GradePattern != null)
                                                markspatrn += "(" + examdatesheetdet.MarksPattern.GradePattern.GradePattern1 + ")";
                                            AppStudentExams.MarksPattern = markspatrn;

                                            // class and subject
                                            if (IsClasssubjectsExist.SkillId != null)
                                            {
                                                var skillsubject = db.SubjectSkills.Where(s => s.SubjectId == IsClasssubjectsExist.SubjectId &&
                                                    s.SkillId == examdatesheetdet.SkillId).FirstOrDefault();
                                                AppStudentExams.Subject = skillsubject != null ? skillsubject.SubjectSkill1 : "";
                                            }
                                            else
                                                AppStudentExams.Subject = IsClasssubjectsExist.Subject.Subject1;

                                            AppStudentExamList.Add(AppStudentExams);
                                        }
                                    }
                                }


                                return Json(new
                                {
                                    status = "success",
                                    //Class = currentstdclas.ClassMaster.Class,
                                    Exams = AppStudentExamList
                                }, JsonRequestBehavior.AllowGet);

                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StaffExamList(FormCollection form)
        {
            string domain = "", UserName = "", ClassId = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Class"] != null)
                ClassId = form["Class"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Class"] != null)
                ClassId = Convert.ToString(Request.Headers["Class"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(ClassId) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "teacher"))
                        {
                            var staff = new Staff();
                            staff = db.Staffs.Where(s => s.StaffId == user.UserContactId).FirstOrDefault();

                            if (staff.StaffId > 0)
                            {
                                // get student class
                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                IList<AppStudentExamsList> AppStudentExamList = new List<AppStudentExamsList>();

                                // current Date
                                var currentdate = DateTime.UtcNow.Date;
                                int classid = Convert.ToInt32(ClassId);
                                var StandardId = db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
                                // var StandardId = db.StandardMasters.Where(m => m.Standard == Standard).FirstOrDefault().StandardId;

                                // get Exam DateSheet
                                var examdatesheets = db.ExamDateSheets.Where(e => e.Ispublish == true).OrderByDescending(p => p.FromDate).ToList();
                                //loop
                                foreach (var datesheet in examdatesheets)
                                {
                                    AppStudentExamsList AppStudentExams = new AppStudentExamsList();
                                    // get all subject ids added into Exam Date sheet
                                    var examdatesheetdetail = db.ExamDateSheetDetails.Where(e => e.ExamDateSheetId == datesheet.ExamDateSheetId && e.StandardId == StandardId).OrderBy(e => e.ExamDate);
                                    if (examdatesheetdetail.FirstOrDefault() != null)
                                    {
                                        AppStudentExams.ExamActivity = datesheet.ExamTermActivity.ExamActivity.ExamActivity1;
                                        AppStudentExams.DateSheetName = datesheet.ExamDateSheet1;
                                        DateTime dtfrom = Convert.ToDateTime(datesheet.FromDate);
                                        AppStudentExams.FromDate = dtfrom.ToString("dd/MM/yyyy");
                                        DateTime dtTill = Convert.ToDateTime(datesheet.TillDate);
                                        AppStudentExams.TillDate = dtTill.ToString("dd/MM/yyyy");
                                        if (datesheet.FromDate <= currentdate && datesheet.TillDate >= currentdate)
                                        {
                                            AppStudentExams.Status = "Processing";
                                        }
                                        else if (datesheet.TillDate < currentdate)
                                        {
                                            AppStudentExams.Status = "Published";
                                        }
                                        else if (datesheet.FromDate > currentdate)
                                        {
                                            AppStudentExams.Status = "Upcoming";
                                        }
                                    }
                                    foreach (var examdatesheetdet in examdatesheetdetail)
                                    {
                                        var IsClasssubjectsExist = db.ClassSubjects.Where(c => c.ClassMaster.StandardMaster.StandardId == StandardId && examdatesheetdet.SubjectId == c.SubjectId).FirstOrDefault();
                                        if (IsClasssubjectsExist != null)
                                        {
                                            ExaminationDateSheetDetail DateSheetDetail = new ExaminationDateSheetDetail();
                                            DateSheetDetail.DateSheetDetailId = Convert.ToString(examdatesheetdet.ExamDateSheetDetailId);
                                            // exam date
                                            DateSheetDetail.ExamDate = examdatesheetdet.ExamDate.Value.ToString("dd/MM/yyyy");
                                            // Time slot
                                            DateSheetDetail.TimeSlot = examdatesheetdet.ExamTimeSlot.ExamTimeSlot1;
                                            // marks pattren
                                            var markspatrn = examdatesheetdet.MarksPattern.MaxMarks + "-" + examdatesheetdet.MarksPattern.PassMarks;
                                            if (examdatesheetdet.MarksPattern.GradePattern != null)
                                                markspatrn += "(" + examdatesheetdet.MarksPattern.GradePattern.GradePattern1 + ")";
                                            DateSheetDetail.MarksPattern = markspatrn;

                                            // class and subject
                                            if (IsClasssubjectsExist.SkillId != null)
                                            {
                                                var skillsubject = db.SubjectSkills.Where(s => s.SubjectId == IsClasssubjectsExist.SubjectId &&
                                                    s.SkillId == examdatesheetdet.SkillId).FirstOrDefault();
                                                DateSheetDetail.Subject = skillsubject != null ? skillsubject.SubjectSkill1 : "";
                                                DateSheetDetail.SubjectId = Convert.ToString(IsClasssubjectsExist.SubjectId);
                                            }
                                            else
                                            {
                                                DateSheetDetail.Subject = IsClasssubjectsExist.Subject.Subject1;
                                                DateSheetDetail.SubjectId = Convert.ToString(IsClasssubjectsExist.SubjectId);
                                            }
                                            DateSheetDetail.MaxMarks = Convert.ToString(examdatesheetdet.MarksPattern.MaxMarks);
                                            DateSheetDetail.obtainedMarks = "";
                                            AppStudentExams.DateSheetDetail.Add(DateSheetDetail);
                                        }
                                    }
                                    if (AppStudentExams.DateSheetName != null)
                                    {
                                        AppStudentExamList.Add(AppStudentExams);
                                    }
                                }


                                return Json(new
                                {
                                    status = "success",
                                    //Class = currentstdclas.ClassMaster.Class,
                                    Exams = AppStudentExamList
                                }, JsonRequestBehavior.AllowGet);

                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ExamMarksList(FormCollection form)
        {
            string domain = "", UserName = "", ClassId = "", Token = "", SubjectId = "", DateSheetDetailId = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Class"] != null)
                ClassId = form["Class"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["Subject"] != null)
                SubjectId = form["Subject"].Trim();
            if (form["DateSheetDetailId"] != null)
                DateSheetDetailId = form["DateSheetDetailId"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Class"] != null)
                ClassId = Convert.ToString(Request.Headers["Class"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["Subject"] != null)
                SubjectId = Convert.ToString(Request.Headers["Subject"]);
            if (Request.Headers["DateSheetDetailId"] != null)
                DateSheetDetailId = Convert.ToString(Request.Headers["DateSheetDetailId"]);


            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(ClassId) || string.IsNullOrEmpty(Token) || string.IsNullOrEmpty(SubjectId) || string.IsNullOrEmpty(DateSheetDetailId))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);


                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "teacher"))
                        {
                            var staff = new Staff();
                            staff = db.Staffs.Where(s => s.StaffId == user.UserContactId).FirstOrDefault();

                            if (staff.StaffId > 0)
                            {
                                // get student class
                                var currentSession = GetCurrentSession(dbdetail.DBName);
                                IList<StudentMarksList> MarksList = new List<StudentMarksList>();
                                int classid = Convert.ToInt32(ClassId);
                                // current Date
                                var currentdate = DateTime.UtcNow.Date;
                                var StandardId = db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
                                var sessionId = currentSession.SessionId;

                                var StudentList = db.StudentClasses.Where(m => m.ClassId == classid && m.SessionId == sessionId).ToList();
                                foreach (var student in StudentList)
                                {
                                    var studentdetail = db.Students.Where(m => m.StudentId == student.StudentId).FirstOrDefault();
                                    StudentMarksList SMList = new StudentMarksList();
                                    SMList.StudentName = studentdetail.FName + " " + studentdetail.LName;
                                    SMList.RollNo = student.RollNo;
                                    var marksdetail = db.ExamResults.Where(m => m.StudentId == student.StudentId).FirstOrDefault();
                                    var examdatesheetdetail = marksdetail == null ? null : db.ExamDateSheetDetails.Where(p => p.ExamDateSheetDetailId == marksdetail.ExamDateSheetDetailId).FirstOrDefault();
                                    var markspatten = examdatesheetdetail == null ? null : db.MarksPatterns.Where(p => p.MarksPatternId == examdatesheetdetail.MarksPatternId).FirstOrDefault();
                                    if (marksdetail != null)
                                        SMList.Marks = Convert.ToString(marksdetail.ObtainedMarks) + "/" + markspatten.MaxMarks;
                                    MarksList.Add(SMList);
                                }
                                return Json(new
                                {
                                    status = "success",
                                    //Class = currentstdclas.ClassMaster.Class,
                                    MarksList = MarksList
                                }, JsonRequestBehavior.AllowGet);

                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not found"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult Messenger(FormCollection form)
        {
            string domain = "", UserName = "", ViewName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["ViewName"] != null)
                ViewName = form["ViewName"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["ViewName"] != null)
                ViewName = Convert.ToString(Request.Headers["ViewName"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(ViewName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }
            string SqlDataSource = "";

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        inboxoutboxmessage model = new inboxoutboxmessage();
                        StringBuilder senderlist = new StringBuilder();
                        List<inboxoutboxmessage> messageslist = new List<inboxoutboxmessage>();

                        var allusers = db.SchoolUsers.Where(m => m.Status == true).ToList();
                        var IsAuthToUndoDeleteMessage = false;
                        var IsAuthToReplyMessage = false;
                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            string currentusername = "";
                            // get login user actual name
                            var loginuser_contacttype = user.ContactType.ContactType1;
                            switch (loginuser_contacttype)
                            {
                                case "Student":
                                    var studentdet = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                                    currentusername = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
                                    break;
                                case "Teacher":
                                    var staffdet = db.Staffs.Where(s => s.StaffId == (int)user.UserContactId).FirstOrDefault();
                                    currentusername = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
                                    break;
                                case "Non-Teaching":
                                    var nonstaffdet = db.Staffs.Where(s => s.StaffId == (int)user.UserContactId).FirstOrDefault();
                                    currentusername = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
                                    break;
                                case "Guardian":
                                    var guardiandet = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                    currentusername = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
                                    break;
                                case "Admin":
                                    currentusername = user.UserName;
                                    break;
                            }


                            switch (ViewName)
                            {
                                case "Recieved":

                                    #region Inbox

                                    var Allrecipientslist = db.MessageRecipients.Where(m => m.RecipientId == user.UserId && m.InActive == false && m.IsDeleted == false).ToList();
                                    var messageIdlist = Allrecipientslist.Select(m => (int)m.MessageId).ToArray();
                                    var Allmessegeslist = db.Messages.Where(m => messageIdlist.Contains(m.MessageId)).OrderByDescending(p => p.MessageTime).ToList();
                                    if (Allmessegeslist.Count > 0)
                                    {
                                        foreach (var aml in Allmessegeslist)
                                        {
                                            // default image
                                            var SchoolDbId = dbdetail.DBId;
                                            // get user by sender
                                            var message_sender = allusers.Where(m => m.UserId == aml.SenderId).FirstOrDefault();
                                            var usertypeid = message_sender != null ? message_sender.UserTypeId : 0;
                                            var Contacttype = db.ContactTypes.Where(m => m.ContactTypeId == usertypeid).FirstOrDefault() != null ?
                                                 db.ContactTypes.Where(m => m.ContactTypeId == usertypeid).FirstOrDefault().ContactType1 : "";

                                            var doctypeid = db.DocumentTypes.Where(d => d.DocumentType1 == "Image").FirstOrDefault();
                                            var staffdoctypeid = db.StaffDocumentTypes.Where(d => d.DocumentType == "Image").FirstOrDefault();

                                            model = new inboxoutboxmessage();
                                            senderlist = new StringBuilder();

                                            //Get message id
                                            model.messageid = aml.MessageId;
                                            // message sender id for reply
                                          //  model.MessageSenderId = aml.SchoolUser.UserId;
                                            // description
                                            model.MessageDescription = aml.MessageDescription;
                                            //get message title
                                            model.MessageTitle = aml.MessageTitle;
                                            //get mesage date time
                                            model.messagetime = ConvertDateTime(aml.MessageTime.Value);
                                            //get message class

                                            //messages sent by user
                                            var user_contacttype = message_sender.ContactType.ContactType1;
                                            string CurrUserName = "";
                                            string UserImage = "";
                                            string defaultimg = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/default.png";

                                            switch (user_contacttype)
                                            {
                                                case "Student":
                                                    var studentdet = db.Students.Where(s => s.StudentId == (int)message_sender.UserContactId).FirstOrDefault();
                                                    CurrUserName = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : message_sender.UserName;
                                                    senderlist.Append(CurrUserName);

                                                    var Studentimage = db.AttachedDocuments.Where(d => d.DocumentTypeId == doctypeid.DocumentTypeId && d.StudentId == message_sender.UserContactId).FirstOrDefault();
                                                    UserImage = Studentimage != null ? _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/StudentPhotos/" + Studentimage.Image : defaultimg;

                                                    break;
                                                case "Teacher":
                                                    var staffdet = db.Staffs.Where(s => s.StaffId == (int)message_sender.UserContactId).FirstOrDefault();
                                                    CurrUserName = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : message_sender.UserName;
                                                    senderlist.Append(CurrUserName);

                                                    var staff = db.Staffs.Where(d => d.StaffId == message_sender.UserContactId).FirstOrDefault();
                                                    var teacherimage = staff.StaffDocuments.Where(d => d.DocumentTypeId == staffdoctypeid.DocumentTypeId).FirstOrDefault();
                                                    UserImage = teacherimage != null ? _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/Teacher/Photos/" + teacherimage.Image : defaultimg;

                                                    break;
                                                case "Non-Teaching":
                                                    var nonstaffdet = db.Staffs.Where(s => s.StaffId == (int)message_sender.UserContactId).FirstOrDefault();
                                                    CurrUserName = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : message_sender.UserName;
                                                    senderlist.Append(CurrUserName);
                                                    break;
                                                case "Guardian":
                                                    var guardiandet = db.Guardians.Where(s => s.GuardianId == (int)message_sender.UserContactId).FirstOrDefault();
                                                    CurrUserName = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : message_sender.UserName;
                                                    senderlist.Append(CurrUserName);

                                                    var senderstudentid = db.Guardians.Where(m => m.GuardianId == message_sender.UserContactId).FirstOrDefault() != null ?
                                                    db.Guardians.Where(m => m.GuardianId == message_sender.UserContactId).FirstOrDefault().StudentId : 0;
                                                    var Guardianimage = db.AttachedDocuments.Where(d => d.DocumentTypeId == doctypeid.DocumentTypeId && d.StudentId == senderstudentid).FirstOrDefault();
                                                    UserImage = Guardianimage != null ? _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + SchoolDbId + "/StudentPhotos/" + Guardianimage.Image : defaultimg;

                                                    break;
                                                case "Admin":
                                                    senderlist.Append(message_sender.UserName);
                                                    break;
                                            }

                                            var senderslist = senderlist.ToString();
                                            model.username = senderslist;
                                            model.currentusername = currentusername;
                                            model.UserImage = UserImage;
                                            messageslist.Add(model);
                                        }
                                    }

                                    IsAuthToReplyMessage = IsUserAuthorize(db, user, "Message History", "Others");

                                    break;

                                    #endregion

                                case "Sent Item":

                                    #region Outbox

                                    DataTable dt = new DataTable();

                                    using (SqlConnection conn = new SqlConnection(SqlDataSource))
                                    {
                                        using (SqlCommand sqlComm = new SqlCommand("sp_outboxmessages", conn))
                                        {
                                            conn.Open();
                                            sqlComm.CommandTimeout = 999;
                                            sqlComm.CommandType = CommandType.StoredProcedure;
                                            sqlComm.Parameters.AddWithValue("@userid", user.UserId);
                                            SqlDataAdapter da = new SqlDataAdapter();
                                            da.SelectCommand = sqlComm;
                                            da.Fill(dt);
                                            conn.Close();
                                        }
                                    }

                                    inboxoutboxmessage messagelist = new inboxoutboxmessage();
                                    string Username = "";
                                    currentusername = "";
                                    IList<string> outboxuserstring = new List<string>();
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        currentusername = "";
                                        Username = "";
                                        outboxuserstring = new List<string>();
                                        messagelist = new inboxoutboxmessage();

                                        messagelist.MessageDescription = dt.Rows[i]["MessageDescription"].ToString();
                                        messagelist.MessageTitle = dt.Rows[i]["MessageTitle"].ToString();
                                        messagelist.messagetime = ConvertDateTime(Convert.ToDateTime((dt.Rows[i]["MessageTime"]).ToString()));
                                        if (dt.Rows[i]["RecipientInfo"].ToString() != "Selected" && dt.Rows[i]["RecipientInfo"].ToString() != "Single")
                                            messagelist.username = dt.Rows[i]["RecipientInfo"].ToString();
                                        else
                                        {
                                            Username = dt.Rows[i]["UserDet"].ToString().TrimEnd(',');
                                            // split user names and get actual name
                                            var userarray = Username.Split(',');
                                            foreach (var item in userarray)
                                            {
                                                Username = "";
                                                var UserId = Convert.ToInt32(item);
                                                var message_receiver = db.SchoolUsers.Where(u => u.UserId == UserId).FirstOrDefault();
                                                //messages sent by user
                                                var receiveruser_contacttype = message_receiver.ContactType.ContactType1;
                                                switch (receiveruser_contacttype)
                                                {
                                                    case "Student":
                                                        var studentdet = db.Students.Where(u => u.StudentId == (int)message_receiver.UserContactId).FirstOrDefault();
                                                        Username = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : message_receiver.UserName;
                                                        break;
                                                    case "Teacher":
                                                        var staffdet = db.Staffs.Where(u => u.StaffId == (int)message_receiver.UserContactId).FirstOrDefault();
                                                        Username = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : message_receiver.UserName;
                                                        break;
                                                    case "Non-Teaching":
                                                        var nonstaffdet = db.Staffs.Where(u => u.StaffId == (int)message_receiver.UserContactId).FirstOrDefault();
                                                        Username = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : message_receiver.UserName;
                                                        break;
                                                    case "Guardian":
                                                        var guardiandet = db.Guardians.Where(u => u.GuardianId == (int)message_receiver.UserContactId).FirstOrDefault();
                                                        Username = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : message_receiver.UserName;
                                                        break;
                                                    case "Admin":
                                                        Username = message_receiver.UserName;
                                                        break;
                                                }
                                                outboxuserstring.Add(Username);
                                            }
                                            messagelist.username = String.Join(",", outboxuserstring);
                                        }

                                        messagelist.messageid = Convert.ToInt32(dt.Rows[i]["MessageId"]);
                                        messagelist.Inactive = Convert.ToBoolean((dt.Rows[i]["InActive"]));
                                        messagelist.Isdeleted = Convert.ToBoolean((dt.Rows[i]["IsDeleted"]));
                                        messagelist.currentusername = currentusername;
                                        messageslist.Add(messagelist);
                                    }

                                    messageslist = (from m in messageslist where m.Inactive == false select m).ToList();
                                    messageslist = (from m in messageslist where m.Isdeleted == false select m).ToList();

                                    count = messageslist.Count;
                                    messageslist = messageslist.OrderByDescending(M => M.messageid).ToList();

                                    #endregion

                                    break;

                                case "Trash":

                                    #region inbox deleted messages

                                    //inbox deleted messages
                                    List<inboxoutboxmessage> messageslist_inbox = new List<inboxoutboxmessage>();
                                    var Allrecipientslist_inbox = db.MessageRecipients.Where(m => m.RecipientId == user.UserId && m.InActive == true && m.IsDeleted == false).ToList();
                                    var messageIdlist_Inbox = Allrecipientslist_inbox.Select(m => (int)m.MessageId).ToArray();
                                    var Allmessegeslist_inbox = db.Messages.Where(m => messageIdlist_Inbox.Contains(m.MessageId)).OrderByDescending(p => p.MessageTime).ToList();
                                    if (Allmessegeslist_inbox.Count > 0)
                                    {
                                        foreach (var aml in Allmessegeslist_inbox)
                                        {
                                            Username = "";
                                            model = new inboxoutboxmessage();
                                            senderlist = new StringBuilder();
                                            var message_sender = allusers.Where(m => m.UserId == aml.SenderId).FirstOrDefault();

                                            //Get message id
                                            model.messageid = aml.MessageId;
                                            model.MessageDescription = aml.MessageDescription;
                                            //get message title
                                            model.MessageTitle = aml.MessageTitle;
                                            //get mesage date time
                                            model.messagetime = ConvertDateTime(aml.MessageTime.Value);
                                            //get message class
                                            model.MessageSenderId = message_sender.UserId;

                                            //messages sent by user
                                            var user_contacttype = message_sender.ContactType.ContactType1;
                                            switch (user_contacttype)
                                            {
                                                case "Student":
                                                    var studentdet = db.Students.Where(u => u.StudentId == (int)message_sender.UserContactId).FirstOrDefault();
                                                    Username = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : message_sender.UserName;
                                                    break;
                                                case "Teacher":
                                                    var staffdet = db.Staffs.Where(u => u.StaffId == (int)message_sender.UserContactId).FirstOrDefault();
                                                    Username = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : message_sender.UserName;
                                                    break;
                                                case "Non-Teaching":
                                                    var nonstaffdet = db.Staffs.Where(u => u.StaffId == (int)message_sender.UserContactId).FirstOrDefault();
                                                    Username = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : message_sender.UserName;
                                                    break;
                                                case "Guardian":
                                                    var guardiandet = db.Guardians.Where(u => u.GuardianId == (int)message_sender.UserContactId).FirstOrDefault();
                                                    Username = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : message_sender.UserName;
                                                    break;
                                                case "Admin":
                                                    Username = message_sender.UserName;
                                                    break;
                                            }

                                            model.username = Username;
                                            model.currentusername = currentusername;
                                            model.From = "Recieved";
                                            messageslist_inbox.Add(model);
                                        }
                                    }

                                    #endregion

                                    #region outbox deleted Messages

                                    List<inboxoutboxmessage> messageslist_outbox = new List<inboxoutboxmessage>();
                                    DataTable dt_outbox = new DataTable();

                                    using (SqlConnection conn = new SqlConnection(SqlDataSource))
                                    {
                                        using (SqlCommand sqlComm = new SqlCommand("sp_outboxmessages", conn))
                                        {
                                            conn.Open();
                                            sqlComm.CommandTimeout = 999;
                                            sqlComm.CommandType = CommandType.StoredProcedure;
                                            sqlComm.Parameters.AddWithValue("@userid", user.UserId);
                                            SqlDataAdapter da = new SqlDataAdapter();
                                            da.SelectCommand = sqlComm;
                                            da.Fill(dt_outbox);
                                            conn.Close();
                                        }
                                    }

                                    for (int i = 0; i < dt_outbox.Rows.Count; i++)
                                    {
                                        Username = "";
                                        outboxuserstring = new List<string>();
                                        messagelist = new inboxoutboxmessage();
                                        //messagelist.messageid = Convert.ToInt32(dt.Rows[i][""]);
                                        messagelist.MessageDescription = dt_outbox.Rows[i]["MessageDescription"].ToString();
                                        messagelist.MessageTitle = dt_outbox.Rows[i]["MessageTitle"].ToString();
                                        messagelist.messagetime = ConvertDateTime(Convert.ToDateTime((dt_outbox.Rows[i]["MessageTime"]).ToString()));

                                        if (dt_outbox.Rows[i]["RecipientInfo"].ToString() != "Selected" && dt_outbox.Rows[i]["RecipientInfo"].ToString() != "Single")
                                            messagelist.username = dt_outbox.Rows[i]["RecipientInfo"].ToString();
                                        else
                                        {
                                            Username = dt_outbox.Rows[i]["UserDet"].ToString().TrimEnd(',');
                                            // split user names and get actual name
                                            var userarray = Username.Split(',');
                                            foreach (var item in userarray)
                                            {
                                                Username = "";
                                                var UserId = Convert.ToInt32(item);
                                                var message_receiver = db.SchoolUsers.Where(u => u.UserId == UserId).FirstOrDefault();
                                                //messages sent by user
                                                var receiveruser_contacttype = message_receiver.ContactType.ContactType1;
                                                switch (receiveruser_contacttype)
                                                {
                                                    case "Student":
                                                        var studentdet = db.Students.Where(u => u.StudentId == (int)message_receiver.UserContactId).FirstOrDefault();
                                                        Username = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : message_receiver.UserName;
                                                        break;
                                                    case "Teacher":
                                                        var staffdet = db.Staffs.Where(u => u.StaffId == (int)message_receiver.UserContactId).FirstOrDefault();
                                                        Username = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : message_receiver.UserName;
                                                        break;
                                                    case "Non-Teaching":
                                                        var nonstaffdet = db.Staffs.Where(u => u.StaffId == (int)message_receiver.UserContactId).FirstOrDefault();
                                                        Username = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : message_receiver.UserName;
                                                        break;
                                                    case "Guardian":
                                                        var guardiandet = db.Guardians.Where(u => u.GuardianId == (int)message_receiver.UserContactId).FirstOrDefault();
                                                        Username = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : message_receiver.UserName;
                                                        break;
                                                    case "Admin":
                                                        Username = message_receiver.UserName;
                                                        break;
                                                }
                                                outboxuserstring.Add(Username);
                                            }
                                            messagelist.currentusername = String.Join(",", outboxuserstring);
                                        }

                                        messagelist.messageid = Convert.ToInt32(dt_outbox.Rows[i]["MessageId"]);
                                        messagelist.Inactive = Convert.ToBoolean((dt_outbox.Rows[i]["InActive"]));
                                        messagelist.Isdeleted = Convert.ToBoolean((dt_outbox.Rows[i]["IsDeleted"]));
                                        messagelist.username = currentusername;
                                        messagelist.From = "SentItem";
                                        messageslist.Add(messagelist);
                                    }

                                    messageslist = (from m in messageslist where m.Inactive == true select m).ToList();
                                    messageslist = (from m in messageslist where m.Isdeleted == false select m).ToList();

                                    count = messageslist.Count;
                                    messageslist_outbox = messageslist.OrderByDescending(M => M.messageid).ToList();

                                    #endregion

                                    messageslist = (messageslist_inbox.Concat(messageslist_outbox)).OrderByDescending(m => m.messagetime).ToList();
                                    IsAuthToUndoDeleteMessage = IsUserAuthorize(db, user, "Message History", "UndoDelete");
                                    break;
                            }

                            var IsAuthToAddRecord = IsUserAuthorize(db, user, "Message History", "Add Record");
                            var IsAuthToDeleteMessage = IsUserAuthorize(db, user, "Message History", "Delete Record");

                            return Json(new
                            {
                                status = "success",
                                message = messageslist,
                                IsAuthorizedToSendRecord = IsAuthToAddRecord,
                                IsAuthToDeleteMessage = IsAuthToDeleteMessage,
                                IsAuthToReplyMessage = IsAuthToReplyMessage,
                                IsAuthToUndoDeleteMessage = IsAuthToUndoDeleteMessage

                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }

                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetContactType(FormCollection form)
        {
            string domain = "", UserName = "", Token = "", msgpermission = "", usertype = "";
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["msgpermission"] != null)
                msgpermission = form["msgpermission"].Trim();
            if (form["usertype"] != null)
                usertype = form["usertype"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["msgpermission"] != null)
                msgpermission = Convert.ToString(Request.Headers["msgpermission"]);
            if (Request.Headers["usertype"] != null)
                usertype = Convert.ToString(Request.Headers["usertype"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);
                        // check user exist and find User info
                        Message messages = new Message();
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {

                            IList<lists> ContacttpeList = new List<lists>();
                            int usertype_id = db.ContactTypes.Where(m => m.ContactType1.ToLower().Contains(usertype)).FirstOrDefault().ContactTypeId;
                            int msgpermissionid = db.MsgPermissions.Where(m => m.MsgPermission1.ToLower().Contains(msgpermission)).FirstOrDefault().MsgPermissionId;
                            var msggroupid = db.ContactTypes.Where(m => m.ContactTypeId == (int)usertype_id).FirstOrDefault().MsgGroupId;
                            var msgrecepientidlist = db.MsgGroupPermissions.Where(m => m.MsgGroupId == msggroupid && m.MsgPermissionId == msgpermissionid).Select(m => (int)m.RecipientId).Distinct().ToArray();
                            var Contacttypelist = db.ContactTypes.ToList();
                            var contacttypelist = Contacttypelist.Where(c => msgrecepientidlist.Contains(c.ContactTypeId)).ToList();
                            foreach (var ct in contacttypelist)
                                ContacttpeList.Add(new lists { Text = ct.ContactType1 });

                            return Json(new
                            {
                                status = "success",
                                ContacttypeList = ContacttpeList
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddMessage(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();

            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);

            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        var allusers = db.SchoolUsers.Where(m => m.Status == true);
                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            //msg permission list
                            IList<lists> MsgPermissionList = new List<lists>();
                            IList<lists> MsgPriorityList = new List<lists>();
                            IList<lists> ClassList = new List<lists>();
                            var messagePermission = db.MsgPermissions;
                            if (user.UserTypeId != null && user.UserTypeId > 0)
                            {
                                MsgPermissionList = new List<lists>();
                                var msggroupid = db.ContactTypes.Where(m => m.ContactTypeId == user.UserTypeId).FirstOrDefault().MsgGroupId;
                                var msgpermissionidlist = db.MsgGroupPermissions.Where(m => m.MsgGroupId == msggroupid).Select(m => (int)m.MsgPermissionId).Distinct().ToArray();
                                var msgpermission = db.MsgPermissions.Where(m => msgpermissionidlist.Contains(m.MsgPermissionId));
                                foreach (var item in msgpermission)
                                    MsgPermissionList.Add(new lists { Text = item.MsgPermission1 });
                            }
                            //msg priority list
                            MsgPriorityList = new List<lists>();
                            var prioritylist = db.MessagePriorities;
                            foreach (var item in prioritylist)
                                MsgPriorityList.Add(new lists { Text = item.Priority });

                            //list of classes
                            ClassList = new List<lists>();
                            foreach (var item in db.ClassMasters)
                                ClassList.Add(new lists { Text = item.Class });

                            return Json(new
                            {
                                status = "success",
                                MsgPermissionList = MsgPermissionList,
                                MsgPriorityList = MsgPriorityList,
                                ClassList = ClassList

                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ReplyMessage(FormCollection form)
        {
            string domain = "", UserName = "", Token = "", MessageTitle = "", MessageDescription = "";
            int refMessageid = 0, MessageRecipientId = 0;
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["MessageTitle"] != null)
                MessageTitle = form["MessageTitle"].Trim();
            if (form["MessageDescription"] != null)
                MessageDescription = form["MessageDescription"].Trim();
            if (form["refMessageid"] != null)
                refMessageid = Convert.ToInt32(form["refMessageid"].Trim());
            if (form["MessageRecipientId"] != null)
                MessageRecipientId = Convert.ToInt32(form["MessageRecipientId"].Trim());

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["MessageTitle"] != null)
                MessageTitle = Convert.ToString(Request.Headers["MessageTitle"]);
            if (Request.Headers["MessageDescription"] != null)
                MessageDescription = Convert.ToString(Request.Headers["MessageDescription"]);
            if (Request.Headers["refMessageid"] != null)
                refMessageid = Convert.ToInt32(Request.Headers["refMessageid"]);
            if (Request.Headers["Token"] != null)
                MessageRecipientId = Convert.ToInt32(Request.Headers["MessageRecipientId"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token) || string.IsNullOrEmpty(MessageDescription) || refMessageid == 0 || MessageRecipientId == 0)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        // check user exist and find User info
                        Message messages = new Message();
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            messages.SenderId = user.UserId;
                            messages.MessageTitle = MessageTitle;
                            messages.MessageDescription = MessageDescription;
                            messages.MessageTime = DateTime.UtcNow;
                            messages.InActive = false;
                            messages.IsDeleted = false;
                            messages.RecipientInfo = "Single";
                            messages.RefId = refMessageid;
                            db.Messages.Add(messages);

                            //get latest message id
                            MessageRecipient messagerecipient = new MessageRecipient();
                            //in case of all


                            messagerecipient.RecipientId = MessageRecipientId;
                            messagerecipient.MessageId = messages.MessageId;
                            messagerecipient.InActive = false;
                            messagerecipient.IsDeleted = false;
                            db.MessageRecipients.Add(messagerecipient);
                            db.SaveChanges();
                            return Json(new
                            {
                                status = "success",
                                message = "Message has been sent",
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteMessage(FormCollection form)
        {

            string domain = "", UserName = "", ViewName = "", Token = "", refdelete = "";
            System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
            string[] messageidarray = new string[] { };
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["ViewName"] != null)
                ViewName = form["ViewName"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            var idarraybody = form["MessageIdArray"];
            if (form["MessageIdArray"] != null)
                messageidarray = form["MessageIdArray"].TrimStart('[').TrimEnd(']').Split(',');
            if (form["refdelete"] != null)
                refdelete = form["refdelete"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["ViewName"] != null)
                ViewName = Convert.ToString(Request.Headers["ViewName"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            var idarrayheader = Request.Headers["MessageIdArray"];
            if (Request.Headers["MessageIdArray"] != null)
                messageidarray = Request.Headers["MessageIdArray"].TrimStart('[').TrimEnd(']').Split(',');
            if (Request.Headers["refdelete"] != null)
                refdelete = Convert.ToString(Request.Headers["refdelete"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(ViewName) || string.IsNullOrEmpty(Token) || messageidarray == null)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);
                        //List<MessagesListModel> messageidarray = new List<MessagesListModel>();
                        //System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                        //messageidarray = js.Deserialize<List<MessagesListModel>>(MessengerModel.MessageIdArray);
                        // check user exist and find User info
                        Message messages = new Message();
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            foreach (var id in messageidarray)
                            {

                                var messageid = Convert.ToInt32(id);
                                if (messageid > 0)
                                {
                                    var message = db.Messages.Where(m => m.MessageId == messageid).FirstOrDefault();
                                    var msgRecipient = db.MessageRecipients.Where(m => m.MessageId == messageid && m.RecipientId == user.UserId).FirstOrDefault();
                                    if (refdelete == "undodelete")
                                    {
                                        if (message.SenderId == user.UserId)
                                        {
                                            message.InActive = false;
                                            db.Entry(message).State = System.Data.Entity.EntityState.Modified;
                                            db.SaveChanges();
                                        }
                                        else if (msgRecipient.RecipientId == user.UserId)
                                        {
                                            msgRecipient.InActive = false;
                                            db.Entry(msgRecipient).State = System.Data.Entity.EntityState.Modified;
                                            db.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        switch (ViewName)
                                        {
                                            case "Sent Item":
                                                message.InActive = true;
                                                db.Entry(message).State = System.Data.Entity.EntityState.Modified;
                                                db.SaveChanges();
                                                break;
                                            case "Recieved":
                                                msgRecipient.InActive = true;
                                                db.Entry(message).State = System.Data.Entity.EntityState.Modified;
                                                db.SaveChanges();
                                                break;
                                            case "Trash":
                                                if (message.SenderId == user.UserId)
                                                {
                                                    message.IsDeleted = true;
                                                    db.Entry(message).State = System.Data.Entity.EntityState.Modified;
                                                    db.SaveChanges();
                                                }
                                                else if (msgRecipient.RecipientId == user.UserId)
                                                {
                                                    msgRecipient.IsDeleted = true;
                                                    db.Entry(message).State = System.Data.Entity.EntityState.Modified;
                                                    db.SaveChanges();
                                                }
                                                break;
                                        }
                                    }
                                }
                            }
                            return Json(new
                            {
                                status = "success",
                                //idarrayheader = idarrayheader,
                                //idarraybody = idarraybody,
                                //idarray = Request.Headers["MessageIdArray"],
                                message = "Message has been Deleted Sucessfully",
                            }, JsonRequestBehavior.AllowGet);

                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School",
                        idarray = Request.Headers["MessageIdArray"],
                        idarrayheader = idarrayheader,
                        idarraybody = idarraybody,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid Token"
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    exception = ex.Message,
                    status = "failed",
                    message = Request.Headers["MessageIdArray"],
                    idarrayheader = idarrayheader,
                    idarraybody = idarraybody,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult TeachersNonTeachersList(FormCollection form)
        {
            string domain = "", UserName = "", Token = "", contacttype = "";
            var contacttypeid = 0;
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["contacttype"] != null)
                contacttype = form["contacttype"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["contacttype"] != null)
                contacttype = Convert.ToString(Request.Headers["contacttype"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        Message messages = new Message();
                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            IList<SelectListItem> teachersnonteachresList = new List<SelectListItem>();
                            var currentusertypeid = db.SchoolUsers.Where(m => m.UserId == user.UserId).FirstOrDefault().UserTypeId;
                            var currentuserContactid = db.SchoolUsers.Where(m => m.UserId == user.UserId).FirstOrDefault().UserContactId;
                            if (!String.IsNullOrEmpty(contacttype))
                            {
                                contacttypeid = db.ContactTypes.Where(m => m.ContactType1.ToLower().Contains(contacttype)).FirstOrDefault().ContactTypeId;
                            }

                            if (user.UserTypeId == 1 && contacttypeid == 2)
                            {

                                switch (currentusertypeid)
                                {
                                    case 1:
                                        var studentclassid = db.StudentClasses.Where(m => m.StudentId == currentuserContactid).FirstOrDefault().ClassId;
                                        var studentclass_standardId = db.ClassMasters.Where(m => m.ClassId == (int)studentclassid).FirstOrDefault().StandardId;
                                        var teachers_id_list = db.TeacherClasses.Where(m => m.StandardId == studentclass_standardId).ToList().Select(m => m.TeacherId).ToList();
                                        var allactiveteachers = db.SchoolUsers.Where(m => m.Status == true && m.UserTypeId == 2).ToList();
                                        var activeteachers_currentclass = (from aat in allactiveteachers where teachers_id_list.Contains(aat.UserContactId) select aat).ToList();
                                        foreach (var atcc in activeteachers_currentclass)
                                        {
                                            if (atcc.UserId != user.UserId)
                                                teachersnonteachresList.Add(new SelectListItem { Text = atcc.UserName, Value = atcc.UserContactId.ToString() });
                                        }
                                        break;
                                }
                            }
                            else if (user.UserTypeId == 4 && contacttypeid == 2)
                            {

                                switch (currentusertypeid)
                                {
                                    case 4:
                                        var gaurdian_studentid = db.Guardians.Where(m => m.GuardianId == (int)currentuserContactid).FirstOrDefault().StudentId;
                                        var studentclassid = db.StudentClasses.Where(m => m.StudentId == gaurdian_studentid).FirstOrDefault().ClassId;
                                        var studentclass_standardId = db.ClassMasters.Where(m => m.ClassId == (int)studentclassid).FirstOrDefault().StandardId;
                                        var teachers_id_list = db.TeacherClasses.Where(m => m.StandardId == studentclass_standardId).ToList().Select(m => m.TeacherId).ToList();
                                        var allactiveteachers = db.SchoolUsers.Where(m => m.Status == true && m.UserTypeId == 2).ToList();
                                        var activeteachers_currentclass = (from aat in allactiveteachers where teachers_id_list.Contains(aat.UserContactId) select aat).ToList();
                                        foreach (var atcc in activeteachers_currentclass)
                                        {
                                            if (atcc.UserId != user.UserId)
                                                teachersnonteachresList.Add(new SelectListItem { Text = atcc.UserName, Value = atcc.UserContactId.ToString() });
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                var userlist = db.SchoolUsers.Where(m => m.Status == true && m.UserTypeId == contacttypeid).ToList();
                                var useridlist = userlist.Select(m => m.UserContactId).ToList();
                                var allstafflist = db.Staffs;
                                var stafflist = allstafflist.Where(m => useridlist.Contains(m.StaffId) && m.IsActive == true).ToList();
                                var staffids = (from sl in stafflist select sl.StaffId).ToList();
                                var stafflist_schooluser = (from ul in userlist where staffids.Contains((int)ul.UserContactId) select ul).ToList();
                                foreach (var sl in stafflist_schooluser)
                                {
                                    if (sl.UserId != user.UserId)
                                        teachersnonteachresList.Add(new SelectListItem { Text = sl.UserName, Value = sl.UserContactId.ToString() });
                                }
                            }
                            return Json(new
                            {
                                status = "success",
                                teachersnonteacherslist = teachersnonteachresList
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult StudentsList(FormCollection form)
        {
            string domain = "", UserName = "", Token = "", ClassName = "", ContactType = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["ClassName"] != null)
                ClassName = form["ClassName"].Trim();
            if (form["ContactType"] != null)
                ContactType = form["ContactType"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["ClassName"] != null)
                ClassName = Convert.ToString(Request.Headers["ClassName"]);
            if (Request.Headers["ContactType"] != null)
                ContactType = Convert.ToString(Request.Headers["ContactType"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token) || string.IsNullOrEmpty(ClassName) || string.IsNullOrEmpty(ContactType))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        Message messages = new Message();
                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            int Contacttypeid = 0, classid = 0;
                            Contacttypeid = db.ContactTypes.Where(m => m.ContactType1 == ContactType).FirstOrDefault().ContactTypeId;
                            classid = db.ClassMasters.Where(m => m.Class == ClassName).FirstOrDefault().ClassId;
                            var userlist = db.SchoolUsers.Where(m => m.Status == true && m.UserTypeId == Contacttypeid).ToList();
                            var contacttype = db.ContactTypes.Where(m => m.ContactTypeId == Contacttypeid).FirstOrDefault().ContactType1;
                            var contacttypeidlist = (from ul in userlist select ul.UserContactId).ToArray();
                            IList<SelectListItem> userselectlist = new List<SelectListItem>();
                            //get current session id
                            var cursession = GetCurrentSession(dbdetail.DBName);
                            var studentclasslist = db.StudentClasses.Where(m => m.ClassId == classid && m.SessionId == cursession.SessionId).ToList();
                            var NonLeftstudentIds = db.StudentStatusDetails.Where(m => !m.StudentStatu.StudentStatus.ToLower().Contains("left")).Select(m => m.StudentId).ToArray();
                            studentclasslist = studentclasslist.Where(m => NonLeftstudentIds.Contains(m.StudentId)).ToList();
                            if (contacttype.ToLower() == "student")
                            {
                                var studentidlist = studentclasslist.Where(m => contacttypeidlist.Contains(m.StudentId)).Select(m => m.StudentId).ToList();
                                var allstudentlist = db.Students;

                                var studentlist = allstudentlist.Where(m => studentidlist.Contains(m.StudentId)).ToList();
                                var studentids = (from sl in studentlist select sl.StudentId).ToList();
                                var studentlist_schooluser = (from ul in userlist where studentids.Contains((int)ul.UserContactId) select ul).ToList();

                                foreach (var sl in studentlist_schooluser)
                                {
                                    if (sl.UserId != user.UserId)
                                    {
                                        var student = db.Students.Where(s => s.StudentId == sl.UserContactId).FirstOrDefault();
                                        string studentusername = !string.IsNullOrEmpty(student.LName) ? student.FName + " " + student.LName : student.FName;
                                        userselectlist.Add(new SelectListItem { Text = studentusername, Value = sl.UserContactId.ToString() });
                                    }
                                }
                            }
                            else if (contacttype.ToLower() == "guardian")
                            {
                                var gaurdianidlist = userlist.Select(m => m.UserContactId).ToList();
                                var allgaurdians = db.Guardians.ToList();
                                var studentidlist = allgaurdians.Select(m => m.StudentId).ToList();
                                var idofstudents = (from m in studentclasslist select m.StudentId).ToList();
                                allgaurdians = allgaurdians.Where(m => gaurdianidlist.Contains(m.GuardianId) && idofstudents.Contains(m.StudentId)).ToList();

                                foreach (var sl in allgaurdians)
                                {
                                    var studentClassdetail = db.StudentClasses.Where(m => m.StudentId == sl.Student.StudentId && m.SessionId == cursession.SessionId).FirstOrDefault();
                                    var rollno = studentClassdetail.RollNo == null ? "" : "/Roll No" + studentClassdetail.RollNo;
                                    var guardiantype = db.Relations.Where(m => m.RelationId == (int)sl.GuardianTypeId).FirstOrDefault();
                                    var type = guardiantype != null ? guardiantype.Relation1 : "";

                                    userselectlist.Add(new SelectListItem { Text = sl.FirstName + " " + sl.LastName + " " + guardiantype.Relation1.ToLower() + " of " + sl.Student.FName + " " + sl.Student.LName + rollno, Value = sl.GuardianId.ToString() });
                                }
                            }
                            else if (contacttype.ToLower() == "teacher")
                            {
                                var stafflist = db.Staffs.ToList();
                                stafflist = stafflist.Where(m => contacttypeidlist.Contains(m.StaffId) && m.IsActive == true).ToList();
                                var staffids = (from sl in stafflist select sl.StaffId).ToList();
                                var stafflist_schooluser = (from ul in userlist where staffids.Contains((int)ul.UserContactId) select ul).ToList();

                                foreach (var sl in stafflist_schooluser)
                                {
                                    if (sl.UserId != user.UserId)
                                    {
                                        var staff = db.Staffs.Where(s => s.StaffId == sl.UserContactId).FirstOrDefault();
                                        string staffusername = !string.IsNullOrEmpty(staff.LName) ? staff.FName + " " + staff.LName : staff.FName;
                                        userselectlist.Add(new SelectListItem { Text = staffusername, Value = sl.UserContactId.ToString() });
                                    }
                                }
                            }

                            return Json(new
                            {
                                status = "success",
                                userselectlistajax = userselectlist

                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SendMessage(FormCollection form)
        {
            string domain = "", UserName = "", Token = "", MessageTitle = "", MessageDecription = "", MessagePermission = "", MessagePriority = "", contacttype = "";
            int Contacttypeid = 0, PriorityId = 0, MessagePermissionId = 0;
            string[] selectedusers = new string[] { };
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["ContactType"] != null)
                contacttype = form["ContactType"].Trim();
            if (form["MessagePriority"] != null)
                MessagePriority = form["MessagePriority"].Trim();
            if (form["MessagePermission"] != null)
                MessagePermission = form["MessagePermission"].Trim();
            if (form["MessageTitle"] != null)
                MessageTitle = form["MessageTitle"].Trim();
            if (form["MessageDecription"] != null)
                MessageDecription = form["MessageDecription"].Trim();
            if (form["SelectedUsersArray"] != null)
                selectedusers = form["SelectedUsersArray"].TrimStart('[').TrimEnd(']').Split(',');


            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["contacttype"] != null)
                contacttype = Convert.ToString(Request.Headers["contacttype"]);
            if (Request.Headers["MessagePriority"] != null)
                MessagePriority = Convert.ToString(Request.Headers["MessagePriority"]);
            if (Request.Headers["MessagePermission"] != null)
                MessagePermission = Convert.ToString(Request.Headers["MessagePermission"]);
            if (Request.Headers["MessageTitle"] != null)
                MessageTitle = Convert.ToString(Request.Headers["MessageTitle"]);
            if (Request.Headers["MessageDecription"] != null)
                MessageDecription = Convert.ToString(Request.Headers["MessageDecription"]);
            if (Request.Headers["SelectedUsersArray"] != null)
                selectedusers = (Request.Headers["SelectedUsersArray"]).TrimStart('[').TrimEnd(']').Split(',');


            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token) || string.IsNullOrEmpty(contacttype) || string.IsNullOrEmpty(MessagePriority) || string.IsNullOrEmpty(MessagePermission) || (selectedusers.Length == 0 && contacttype.ToLower() != "admin"))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        Message messages = new Message();
                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            //List<users> selectedusers = new List<users>();
                            Contacttypeid = db.ContactTypes.Where(m => m.ContactType1 == contacttype).FirstOrDefault().ContactTypeId;
                            PriorityId = db.MessagePriorities.Where(m => m.Priority == MessagePriority).FirstOrDefault().PriorityId;
                            MessagePermissionId = db.MsgPermissions.Where(m => m.MsgPermission1 == MessagePermission).FirstOrDefault().MsgPermissionId;
                            #region save message

                            //SaveMessage(MessengerModel, user.UserId, contacttype);
                            messages.SenderId = user.UserId;
                            messages.MessageTitle = MessageTitle;
                            messages.MessageDescription = MessageDecription;
                            messages.PriorityId = PriorityId;
                            messages.MessageTime = DateTime.UtcNow.ToLocalTime();
                            messages.InActive = false;
                            messages.IsDeleted = false;
                            if (MessagePermissionId == 1)
                                messages.RecipientInfo = "All " + contacttype;
                            else if (MessagePermissionId == 2)
                                messages.RecipientInfo = "Selected";
                            else if (MessagePermissionId == 3)
                                messages.RecipientInfo = "Single";

                            #endregion

                            //get latest message id
                            MessageRecipient messagerecipient = new MessageRecipient();
                            var activeuserslist = db.SchoolUsers.Where(m => m.Status == true && m.UserTypeId == Contacttypeid).ToList();

                            //in case of all
                            if (MessagePermissionId == 1 && activeuserslist.Count > 0)
                            {
                                db.Messages.Add(messages);
                                db.SaveChanges();
                                //get the list of all students/ teachers/gaurdians etc based on contacttypeid
                                foreach (var aul in activeuserslist)
                                {
                                    messagerecipient.RecipientId = aul.UserId;
                                    messagerecipient.MessageId = messages.MessageId;
                                    messagerecipient.InActive = false;
                                    messagerecipient.IsDeleted = false;
                                    db.MessageRecipients.Add(messagerecipient);
                                    db.SaveChanges();

                                }
                                return Json(new
                                {
                                    status = "success",
                                    msg = "Message sent successfully",
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else if ((MessagePermissionId == 3 && selectedusers.Length > 0) || (MessagePermissionId == 3 && activeuserslist.Count > 0))
                            {
                                if (selectedusers.Length > 0 && selectedusers[0] != "")
                                {
                                    db.Messages.Add(messages);
                                    db.SaveChanges();
                                    foreach (var su in selectedusers)
                                    {
                                        var selecteduser = Convert.ToInt32(su);
                                        var Selecteduserid = db.SchoolUsers.Where(m => m.Status == true && m.UserTypeId == Contacttypeid && m.UserContactId == selecteduser).FirstOrDefault().UserId;
                                        messagerecipient.RecipientId = Selecteduserid;
                                        messagerecipient.MessageId = messages.MessageId;
                                        messagerecipient.InActive = false;
                                        messagerecipient.IsDeleted = false;
                                        db.MessageRecipients.Add(messagerecipient);
                                        db.SaveChanges();
                                    }
                                    return Json(new
                                    {
                                        status = "success",
                                        msg = "Message sent successfully",
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                if (activeuserslist.Count > 0)
                                {
                                    db.Messages.Add(messages);
                                    var Selecteduserid = db.SchoolUsers.Where(m => m.Status == true && m.UserTypeId == Contacttypeid).FirstOrDefault().UserId;
                                    messagerecipient.RecipientId = Selecteduserid;
                                    messagerecipient.MessageId = messages.MessageId;
                                    messagerecipient.InActive = false;
                                    messagerecipient.IsDeleted = false;
                                    db.MessageRecipients.Add(messagerecipient);
                                    db.SaveChanges();
                                    return Json(new
                                    {
                                        status = "success",
                                        msg = "Message sent successfully",
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                return Json(new
                                {
                                    status = "failed",
                                    msg = "NotValid",
                                    data = "Please Select Any Userε"
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else if (MessagePermissionId == 2 && selectedusers.Length > 0)
                            {
                                db.Messages.Add(messages);
                                db.SaveChanges();
                                foreach (var su in selectedusers)
                                {
                                    var selecteduser = Convert.ToInt32(su);
                                    var Selecteduserid = db.SchoolUsers.Where(m => m.Status == true && m.UserTypeId == Contacttypeid && m.UserContactId == selecteduser).FirstOrDefault().UserId;
                                    messagerecipient.RecipientId = Selecteduserid;
                                    messagerecipient.MessageId = messages.MessageId;
                                    messagerecipient.InActive = false;
                                    messagerecipient.IsDeleted = false;
                                    db.MessageRecipients.Add(messagerecipient);
                                    db.SaveChanges();
                                }
                                return Json(new
                                {
                                    status = "success",
                                    msg = "Message sent successfully",
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    msg = "NotValid",
                                    data = "Please Select Any Userε"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }

                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Dashboard(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);
                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            IList<Dashboard> Dashboard = new List<Dashboard>();
                            var currentSession = GetCurrentSession(dbdetail.DBName);

                            var currentdate = DateTime.Now.Date;
                            //all type of events
                            var allAnnoucements = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Announcement" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();
                            var allHolidays = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Holiday" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();
                            var allVacations = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Vacation" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();
                            var allActivity = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Activity" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();


                            var studentstandardid = 0;
                            if (user.ContactType.ContactType1.ToLower().Contains("student"))
                            {
                                var classid = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault().ClassId;
                                var eventids = db.EventDetails.Where(m => m.ClassId == classid).Select(m => m.EventId).ToArray();
                                allActivity = allActivity.Where(m => eventids.Contains(m.EventId) || m.IsAllSchool == true).ToList();
                                studentstandardid = (int)db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
                            }
                            else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                            {
                                var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                var classid = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault().ClassId;
                                var eventids = db.EventDetails.Where(m => m.ClassId == classid).Select(m => m.EventId).ToArray();
                                allActivity = allActivity.Where(m => eventids.Contains(m.EventId) || m.IsAllSchool == true).ToList();
                                studentstandardid = (int)db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
                            }

                            if (allAnnoucements.Count > 0)
                            {
                                foreach (var item in allAnnoucements)
                                {
                                    var AnouncementsEvent = new Dashboard();
                                    AnouncementsEvent.Type = "Anouncement";
                                    AnouncementsEvent.StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                    AnouncementsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                    AnouncementsEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");
                                    Dashboard.Add(AnouncementsEvent);
                                }
                            }
                            if (allHolidays.Count > 0)
                            {
                                foreach (var item in allHolidays)
                                {
                                    var HolidaysEvent = new Dashboard();
                                    HolidaysEvent.Type = "Holiday";
                                    HolidaysEvent.StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                    HolidaysEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                    HolidaysEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");

                                    Dashboard.Add(HolidaysEvent);
                                }
                            }
                            if (allVacations.Count > 0)
                            {
                                foreach (var item in allVacations)
                                {
                                    var VacationsEvent = new Dashboard();
                                    VacationsEvent.Type = "Vacation";
                                    VacationsEvent.StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                    VacationsEvent.EndDate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : null;
                                    VacationsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                    VacationsEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");
                                    Dashboard.Add(VacationsEvent);
                                }
                            }
                            if (allActivity.Count > 0)
                            {
                                var allclasses = db.ClassMasters.ToList();

                                foreach (var item in allActivity)
                                {
                                    var ActivityEvent = new Dashboard();
                                    ActivityEvent.Type = "Activity";
                                    ActivityEvent.StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                    ActivityEvent.EndDate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : null;
                                    ActivityEvent.StartTime = item.StartTime.HasValue ? Convert.ToString(item.StartTime) : null;
                                    ActivityEvent.EndTime = item.EndTime.HasValue ? Convert.ToString(item.EndTime) : null;
                                    ActivityEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");
                                    ActivityEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                    ActivityEvent.Venue = item.Venue;

                                    if (item.EventDetails.Count > 0)
                                    {
                                        var eventClasses = "";
                                        var classids = item.EventDetails.Select(e => e.ClassId).ToArray();
                                        eventClasses = String.Join(",", allclasses.Where(c => classids.Contains(c.ClassId)).Select(c => c.Class).ToList());

                                        //if (studentclassid > 0 && classids.Contains(studentclassid))
                                        //    eventClasses = _ICatalogMasterService.GetClassMasterById(studentclassid).Class;

                                        ActivityEvent.Class = eventClasses;
                                    }
                                    else
                                        ActivityEvent.Class = "All Classes";

                                    Dashboard.Add(ActivityEvent);
                                }
                            }
                            //exams
                            var Exams = db.ExamDateSheetDetails.Where(m => m.StandardId == studentstandardid).ToList();
                            var ExamDetail = Exams.GroupBy(p => p.StandardId).Select(m => m.First()).ToList();

                            var datesheetAcctoPublisheddate = db.ExamPublishDetails.ToList();
                            if (datesheetAcctoPublisheddate.Count > 0)
                            {
                                datesheetAcctoPublisheddate = datesheetAcctoPublisheddate.Where(m => m.ExamPublishDate <= DateTime.Now.Date).ToList();
                                Exams = Exams.Where(m => (datesheetAcctoPublisheddate.Select(n => n.ExamDateSheetId).ToArray()).Contains((int)m.ExamDateSheetId)).ToList();
                                Exams = Exams.Where(p => p.ExamDate >= DateTime.Now.Date).OrderByDescending(e => e.ExamDate).ToList();
                                if (Exams.Count > 0)
                                {
                                    foreach (var item in Exams)
                                    {
                                        var standard = db.StandardMasters.Where(m => m.StandardId == (int)item.StandardId).FirstOrDefault();
                                        var subject = db.Subjects.Where(m => m.SubjectId == (int)item.SubjectId).FirstOrDefault();
                                        var timeSlot = item.ExamTimeSlot.ExamTimeSlot1;

                                        if (standard != null && subject != null)
                                            Dashboard.Add(new Dashboard { ClassSubjectName = standard.Standard + "/" + subject.Subject1, TimeSlot = timeSlot, ExamDate = ConvertDate((DateTime)item.ExamDate), Type = "Exam" });
                                    }
                                }
                            }

                            //Attendance
                            //attendance
                            // Entrance Class
                            var Curentdate = DateTime.UtcNow.Date;
                            // Sundays count
                            //int Sundaycount = 0;
                            //var sundayslist = _IMainService.GetSundayCountBetweenDates(ref Sundaycount, currentSession.StartDate.Value, Curentdate);
                            //// get holidays
                            //var holidayslist = db.Events.Where(e => e.EventType.EventType1 == "Holidays" && e.StartDate >= currentSession.StartDate
                            //    && e.StartDate <= Curentdate && e.IsActive == true && e.IsDeleted != true).Select(e => (DateTime)e.StartDate).ToList();

                            // combine both list and get distinct
                            //var alldatelist = sundayslist.Concat(holidayslist).Distinct().ToList();

                            // calculate attendance %
                            TimeSpan diff = Curentdate.AddDays(1) - currentSession.StartDate.Value;
                            int totaldays = diff.Days;
                            var Attendance = "";

                            if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
                            {
                                // get attendance by student
                                var staffattendance = db.StaffAttendances.Where(a => a.StaffId == user.UserContactId && a.StaffAttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
                                Attendance = staffattendance.Count + "/" + totaldays;
                            }

                            //Fee
                            if (user.ContactType.ContactType1.ToLower().Contains("student"))
                            {
                                var Fees = new Dashboard();
                                var FeeReceipts = db.FeeReceipts.Where(m => m.StudentId == user.UserContactId).OrderByDescending(n => n.ReceiptDate).FirstOrDefault();
                                if (FeeReceipts != null)
                                {
                                    // Fee Receipt Detail
                                    // Calculate Amount
                                    var paidamount = FeeReceipts.PaidAmount;
                                    // get fee receipt detail
                                    var totalfeeamt = FeeReceipts.FeeReceiptDetails.Where(f => f.GroupFeeHeadId != null).Select(f => f.Amount).Sum();
                                    var totalfeeconsession = FeeReceipts.FeeReceiptDetails.Where(f => f.ConsessionId != null).Select(f => f.Amount).Sum();
                                    var payableamt = totalfeeamt - totalfeeconsession;

                                    var PendingFee = (paidamount - payableamt);
                                    var PaidFee = payableamt.ToString();

                                    if (PendingFee > 0)
                                    {
                                        Fees.Type = "Fee";
                                        Fees.Fee = PendingFee + " Fee Pending";
                                        Dashboard.Add(Fees);
                                    }

                                }
                                //Attendance
                                var staffattendance = db.Attendances.Where(a => a.StudentId == user.UserContactId && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
                                Attendance = staffattendance.Count + "/" + totaldays;

                            }
                            if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                            {
                                var Fees = new Dashboard();
                                var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                var FeeReceipts = db.FeeReceipts.Where(m => m.StudentId == studentid).OrderByDescending(n => n.ReceiptDate).FirstOrDefault();
                                if (FeeReceipts != null)
                                {

                                    // Fee Receipt Detail
                                    // Calculate Amount
                                    var paidamount = FeeReceipts.PaidAmount;
                                    // get fee receipt detail
                                    var totalfeeamt = FeeReceipts.FeeReceiptDetails.Where(f => f.GroupFeeHeadId != null).Select(f => f.Amount).Sum();
                                    var totalfeeconsession = FeeReceipts.FeeReceiptDetails.Where(f => f.ConsessionId != null).Select(f => f.Amount).Sum();
                                    var payableamt = totalfeeamt - totalfeeconsession;

                                    var PendingFee = (paidamount - payableamt);
                                    var PaidFee = payableamt.ToString();

                                    if (PendingFee > 0)
                                    {
                                        Fees.Type = "Fee";
                                        Fees.Fee = PendingFee + " Fee Pending";
                                    }
                                    else
                                    {
                                        Fees.Type = "Fee";
                                        Fees.Fee = PaidFee + "Fee Paid on" + FeeReceipts.ReceiptDate;
                                    }
                                    Dashboard.Add(Fees);
                                }
                                //Attendance
                                var staffattendance = db.Attendances.Where(a => a.StudentId == studentid && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
                                Attendance = staffattendance.Count + "/" + totaldays;
                            }

                            //HW
                            var homeworklist = new List<HW>();
                            var seldate = DateTime.UtcNow.Date;
                            if (user.ContactType.ContactType1.ToLower() == "student")
                            {
                                var classId = 0;
                                //get student class id 
                                var studentClass = db.StudentClasses.Where(m => m.StudentId == user.UserContactId && m.SessionId == currentSession.SessionId).FirstOrDefault();
                                if (studentClass != null)
                                    classId = (int)studentClass.ClassId;

                                //get classsubjectid
                                var classsubjectidarray = db.ClassSubjects.Where(m => m.ClassId == classId && m.IsActive == true).Select(n => n.ClassSubjectId).ToArray();
                                //get homework
                                homeworklist = db.HWs.Where(m => classsubjectidarray.Contains((int)m.ClassSubjectId)).OrderByDescending(m => m.Date).Take(3).ToList();

                            }
                            else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                            {
                                var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                var classId = 0;
                                //get student class id 
                                var studentClass = db.StudentClasses.Where(m => m.StudentId == studentid && m.SessionId == currentSession.SessionId).FirstOrDefault();
                                if (studentClass != null)
                                    classId = (int)studentClass.ClassId;

                                var classsubjectidarray = db.ClassSubjects.Where(m => m.ClassId == classId && m.IsActive == true).Select(n => n.ClassSubjectId).ToArray();
                                //get homework
                                homeworklist = db.HWs.Where(m => classsubjectidarray.Contains((int)m.ClassSubjectId)).OrderByDescending(m => m.Date).Take(3).ToList();
                            }
                            else if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
                                homeworklist = db.HWs.Where(m => m.StaffId == user.UserContactId).OrderByDescending(m => m.Date).Take(3).ToList();

                            IList<HomeWorkModel> homeworllistrecords = new List<HomeWorkModel>();
                            var homework = new Dashboard();
                            var homeworkdetails = new HomeWorkDetails();
                            foreach (var hw in homeworklist)
                            {
                                homework = new Dashboard();
                                if (hw.ClassSubject != null)
                                {
                                    homework.Class = hw.ClassSubject.ClassMaster.Class;
                                    homework.Subject = hw.ClassSubject.Subject.Subject1;
                                }
                                homework.Date = ConvertDate((DateTime)hw.Date);
                                homework.Type = hw.HWType.HWType1;
                                homework.teacher = hw.Staff.LName != null ? hw.Staff.FName + " " + hw.Staff.LName : hw.Staff.FName;

                                Dashboard.Add(homework);
                            }

                            return Json(new
                            {
                                status = "success",
                                Dashboard = Dashboard,
                                Attendance = Attendance,
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DashboardNew(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);
                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            string postedby = string.Empty;
                            IList<DashBoardNew> DashBoardNew = new List<DashBoardNew>();
                            var currentSession = GetCurrentSession(dbdetail.DBName);

                            var currentdate = DateTime.Now.Date;
                            //all type of events
                            var allAnnoucements = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Announcement" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();
                            var allHolidays = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Holiday" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();
                            var allVacations = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Vacation" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();
                            var allActivity = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Activity" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();


                            var studentstandardid = 0;
                            if (user.ContactType.ContactType1.ToLower().Contains("student"))
                            {
                                var studentclass = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault();
                                int classid = 0;
                                if (studentclass != null)
                                    classid = (int)studentclass.ClassId;
                                if (classid > 0)
                                {
                                    var eventids = db.EventDetails.Where(m => m.ClassId == classid).Select(m => m.EventId).ToArray();
                                    allActivity = allActivity.Where(m => eventids.Contains(m.EventId) || m.IsAllSchool == true).ToList();
                                    studentstandardid = (int)db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
                                }

                            }
                            else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                            {
                                var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                var classid = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault().ClassId;
                                var eventids = db.EventDetails.Where(m => m.ClassId == classid).Select(m => m.EventId).ToArray();
                                allActivity = allActivity.Where(m => eventids.Contains(m.EventId) || m.IsAllSchool == true).ToList();
                                studentstandardid = (int)db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
                            }
                            var AnouncementsEvent = new DashBoardNew();

                            if (allAnnoucements.Count > 0)
                            {

                                foreach (var item in allAnnoucements)
                                {
                                    AnouncementsEvent = new DashBoardNew();
                                    AnouncementsEvent.Type = "Anouncement";
                                    AnouncementsEvent.Date = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                    AnouncementsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                    AnouncementsEvent.Title = item.EventTitle.Replace("<", "&lt;");
                                    AnouncementsEvent.PostedOn = "";
                                    if (item.PostedOn != null)
                                        AnouncementsEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
                                    SchoolUser usedcontact = new SchoolUser();
                                    if (item.PostedBy > 0)
                                        usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();
                                    if (usedcontact.ContactType != null)
                                    {
                                        var contacttype = usedcontact.ContactType.ContactType1;

                                        switch (contacttype)
                                        {
                                            case "Student":
                                                var studentdet = db.Students.Where(m => m.StudentId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
                                                break;
                                            case "Teacher":
                                                var staffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
                                                break;
                                            case "Non-Teaching":
                                                var nonstaffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
                                                break;
                                            case "Guardian":
                                                var guardiandet = db.Guardians.Where(m => m.GuardianId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
                                                break;
                                            case "Admin":
                                                postedby = usedcontact.UserName;
                                                break;
                                        }
                                    }
                                    AnouncementsEvent.PostedBy = postedby;
                                    DashBoardNew.Add(AnouncementsEvent);
                                }
                            }
                            if (allHolidays.Count > 0)
                            {
                                foreach (var item in allHolidays)
                                {
                                    AnouncementsEvent = new DashBoardNew();
                                    AnouncementsEvent.Type = "Anouncement";
                                    AnouncementsEvent.Date = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                    AnouncementsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                    AnouncementsEvent.Title = item.EventTitle.Replace("<", "&lt;");
                                    AnouncementsEvent.PostedOn = "";
                                    if (item.PostedOn != null)
                                        AnouncementsEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
                                    SchoolUser usedcontact = new SchoolUser();
                                    if (item.PostedBy > 0)
                                        usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();
                                    if (usedcontact.ContactType != null)
                                    {
                                        var contacttype = usedcontact.ContactType.ContactType1;
                                        switch (contacttype)
                                        {
                                            case "Student":
                                                var studentdet = db.Students.Where(m => m.StudentId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
                                                break;
                                            case "Teacher":
                                                var staffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
                                                break;
                                            case "Non-Teaching":
                                                var nonstaffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
                                                break;
                                            case "Guardian":
                                                var guardiandet = db.Guardians.Where(m => m.GuardianId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
                                                break;
                                            case "Admin":
                                                postedby = usedcontact.UserName;
                                                break;
                                        }
                                    }
                                    AnouncementsEvent.PostedBy = postedby;
                                    DashBoardNew.Add(AnouncementsEvent);
                                }
                            }
                            if (allVacations.Count > 0)
                            {
                                foreach (var item in allVacations)
                                {
                                    AnouncementsEvent = new DashBoardNew();
                                    AnouncementsEvent.Type = "Anouncement";
                                    var StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                    var EndDate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : null;
                                    if (!string.IsNullOrEmpty(EndDate))
                                        AnouncementsEvent.Date = StartDate + '-' + EndDate;
                                    else
                                        AnouncementsEvent.Date = StartDate;
                                    AnouncementsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                    AnouncementsEvent.Title = item.EventTitle.Replace("<", "&lt;");
                                    AnouncementsEvent.PostedOn = "";
                                    if (item.PostedOn != null)
                                        AnouncementsEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
                                    SchoolUser usedcontact = new SchoolUser();
                                    if (item.PostedBy > 0)
                                        usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();
                                    if (usedcontact.ContactType != null)
                                    {
                                        var contacttype = usedcontact.ContactType.ContactType1;
                                        switch (contacttype)
                                        {
                                            case "Student":
                                                var studentdet = db.Students.Where(m => m.StudentId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
                                                break;
                                            case "Teacher":
                                                var staffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
                                                break;
                                            case "Non-Teaching":
                                                var nonstaffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
                                                break;
                                            case "Guardian":
                                                var guardiandet = db.Guardians.Where(m => m.GuardianId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
                                                break;
                                            case "Admin":
                                                postedby = usedcontact.UserName;
                                                break;
                                        }
                                    }


                                    AnouncementsEvent.PostedBy = postedby;
                                    DashBoardNew.Add(AnouncementsEvent);
                                }
                            }
                            if (allActivity.Count > 0)
                            {
                                var allclasses = db.ClassMasters.ToList();

                                foreach (var item in allActivity)
                                {
                                    AnouncementsEvent = new DashBoardNew();
                                    AnouncementsEvent.Type = "Anouncement";

                                    var StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                    var EndDate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : null;
                                    if (!string.IsNullOrEmpty(EndDate))
                                        AnouncementsEvent.Date = StartDate + '-' + EndDate;
                                    else
                                        AnouncementsEvent.Date = StartDate;

                                    //ActivityEvent.StartTime = item.StartTime.HasValue ? Convert.ToString(item.StartTime) : null;
                                    //ActivityEvent.EndTime = item.EndTime.HasValue ? Convert.ToString(item.EndTime) : null;


                                    AnouncementsEvent.Title = item.EventTitle.Replace("<", "&lt;");
                                    AnouncementsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                    //ActivityEvent.Venue = item.Venue;

                                    //if (item.EventDetails.Count > 0)
                                    //{
                                    //    var eventClasses = "";
                                    //    var classids = item.EventDetails.Select(e => e.ClassId).ToArray();
                                    //    eventClasses = String.Join(",", allclasses.Where(c => classids.Contains(c.ClassId)).Select(c => c.Class).ToList());

                                    //    //if (studentclassid > 0 && classids.Contains(studentclassid))
                                    //    //    eventClasses = _ICatalogMasterService.GetClassMasterById(studentclassid).Class;

                                    //    ActivityEvent.Class = eventClasses;
                                    //}
                                    //else
                                    //    ActivityEvent.Class = "All Classes";
                                    AnouncementsEvent.PostedOn = "";
                                    if (item.PostedOn != null)
                                        AnouncementsEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
                                    SchoolUser usedcontact = new SchoolUser();
                                    if (item.PostedBy > 0)
                                        usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();
                                    if (usedcontact.ContactType != null)
                                    {
                                        var contacttype = usedcontact.ContactType.ContactType1;
                                        switch (contacttype)
                                        {
                                            case "Student":
                                                var studentdet = db.Students.Where(m => m.StudentId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
                                                break;
                                            case "Teacher":
                                                var staffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
                                                break;
                                            case "Non-Teaching":
                                                var nonstaffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
                                                break;
                                            case "Guardian":
                                                var guardiandet = db.Guardians.Where(m => m.GuardianId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                postedby = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
                                                break;
                                            case "Admin":
                                                postedby = usedcontact.UserName;
                                                break;
                                        }
                                    }
                                    AnouncementsEvent.PostedBy = postedby;
                                    DashBoardNew.Add(AnouncementsEvent);
                                }
                            }
                            //exams
                            var Exams = db.ExamDateSheetDetails.Where(m => m.StandardId == studentstandardid).ToList();
                            var ExamDetail = Exams.GroupBy(p => p.StandardId).Select(m => m.First()).ToList();

                            var datesheetAcctoPublisheddate = db.ExamPublishDetails.ToList();
                            if (datesheetAcctoPublisheddate.Count > 0)
                            {
                                datesheetAcctoPublisheddate = datesheetAcctoPublisheddate.Where(m => m.ExamPublishDate <= DateTime.Now.Date).ToList();
                                Exams = Exams.Where(m => (datesheetAcctoPublisheddate.Select(n => n.ExamDateSheetId).ToArray()).Contains((int)m.ExamDateSheetId)).ToList();
                                Exams = Exams.Where(p => p.ExamDate >= DateTime.Now.Date).OrderByDescending(e => e.ExamDate).ToList();
                                if (Exams.Count > 0)
                                {
                                    foreach (var item in Exams)
                                    {
                                        var standard = db.StandardMasters.Where(m => m.StandardId == (int)item.StandardId).FirstOrDefault();
                                        var subject = db.Subjects.Where(m => m.SubjectId == (int)item.SubjectId).FirstOrDefault();
                                        var timeSlot = item.ExamTimeSlot.ExamTimeSlot1;
                                        var postedon = "";
                                        var examdatesheet = db.ExamDateSheets.Where(m => m.ExamDateSheetId == item.ExamDateSheetId).FirstOrDefault();
                                        if (examdatesheet.CreatedOn != null)
                                        {
                                            postedon = ((DateTime)examdatesheet.CreatedOn).ToString("dd/MM/yyyy HH:mm:ss");
                                        }
                                        if (examdatesheet.UserId != null)
                                        {
                                            var userdetail = db.SchoolUsers.Where(m => m.UserId == examdatesheet.UserId).FirstOrDefault();
                                            var contacttype = userdetail.ContactType.ContactType1;
                                            switch (contacttype)
                                            {
                                                case "Student":
                                                    var studentdet = db.Students.Where(m => m.StudentId == (int)userdetail.UserContactId).FirstOrDefault();
                                                    postedby = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
                                                    break;
                                                case "Teacher":
                                                    var staffdet = db.Staffs.Where(m => m.StaffId == (int)userdetail.UserContactId).FirstOrDefault();
                                                    postedby = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
                                                    break;
                                                case "Non-Teaching":
                                                    var nonstaffdet = db.Staffs.Where(m => m.StaffId == (int)userdetail.UserContactId).FirstOrDefault();
                                                    postedby = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
                                                    break;
                                                case "Guardian":
                                                    var guardiandet = db.Guardians.Where(m => m.GuardianId == (int)userdetail.UserContactId).FirstOrDefault();
                                                    postedby = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
                                                    break;
                                                case "Admin":
                                                    postedby = userdetail.UserName;
                                                    break;
                                            }
                                        }

                                        if (standard != null && subject != null)
                                            DashBoardNew.Add(new DashBoardNew { Title = standard.Standard + "/" + subject.Subject1, Description = timeSlot, Date = ConvertDate((DateTime)item.ExamDate), Type = "Anouncement", PostedBy = postedby, PostedOn = postedon });
                                    }
                                }
                            }

                            DashBoardNew = DashBoardNew.OrderByDescending(m => m.PostedOn).ToList();
                            //Attendance
                            //attendance
                            // Entrance Class
                            var Curentdate = DateTime.UtcNow.Date;
                            // Sundays count
                            int Sundaycount = 0;
                            var sundayslist = _IMainService.GetSundayCountBetweenDates(ref Sundaycount, currentSession.StartDate.Value, Curentdate);
                            //// get holidays
                            //var holidayslist = db.Events.Where(e => e.EventType.EventType1 == "Holidays" && e.StartDate >= currentSession.StartDate
                            //    && e.StartDate <= Curentdate && e.IsActive == true && e.IsDeleted != true).Select(e => (DateTime)e.StartDate).ToList();

                            // combine both list and get distinct
                            //var alldatelist = sundayslist.Concat(holidayslist).Distinct().ToList();

                            // calculate attendance %
                            TimeSpan diff = Curentdate.AddDays(1) - currentSession.StartDate.Value;

                            int totaldays = diff.Days - (Convert.ToInt32(allHolidays.Count) + Convert.ToInt32(sundayslist.Count));
                            var Attendance = "";

                            if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
                            {
                                // get attendance by student
                                var staffattendance = db.StaffAttendances.Where(a => a.StaffId == user.UserContactId && a.StaffAttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
                                Attendance = staffattendance.Count + "/" + totaldays;
                            }

                            //Fee
                            if (user.ContactType.ContactType1.ToLower().Contains("student"))
                            {
                                var Fees = new DashBoardNew();
                                Fees.PostedBy = "";
                                Fees.PostedOn = "";
                                Fees.Date = "";
                                Fees.Description = "";
                                Fees.Title = "";
                                Fees.Type = "";
                                var FeeReceipts = db.FeeReceipts.Where(m => m.StudentId == user.UserContactId).OrderByDescending(n => n.ReceiptDate).FirstOrDefault();
                                if (FeeReceipts != null)
                                {
                                    // Fee Receipt Detail
                                    // Calculate Amount
                                    var paidamount = FeeReceipts.PaidAmount;
                                    // get fee receipt detail
                                    var totalfeeamt = FeeReceipts.FeeReceiptDetails.Where(f => f.GroupFeeHeadId != null).Select(f => f.Amount).Sum();
                                    var totalfeeconsession = FeeReceipts.FeeReceiptDetails.Where(f => f.ConsessionId != null).Select(f => f.Amount).Sum();
                                    var payableamt = totalfeeamt - totalfeeconsession;

                                    var PendingFee = (paidamount - payableamt);
                                    var PaidFee = payableamt.ToString();

                                    if (PendingFee > 0)
                                    {
                                        Fees.Type = "Fee";
                                        Fees.Description = PendingFee + " Fee Pending";
                                        Fees.Title = "Fee";
                                        Fees.Date = "";
                                        DashBoardNew.Add(Fees);
                                    }
                                    else
                                    {
                                        Fees.Type = "Fee";
                                        Fees.Description = PaidFee + "Fee Paid on" + FeeReceipts.ReceiptDate;
                                        Fees.Title = "Fee";
                                        Fees.Date = "";
                                    }

                                }
                                //Attendance
                                var staffattendance = db.Attendances.Where(a => a.StudentId == user.UserContactId && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
                                Attendance = staffattendance.Count + "/" + totaldays;

                            }
                            if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                            {
                                var Fees = new DashBoardNew();
                                Fees.PostedBy = "";
                                Fees.PostedOn = "";
                                Fees.Date = "";
                                Fees.Description = "";
                                Fees.Title = "";
                                Fees.Type = "";
                                var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                var FeeReceipts = db.FeeReceipts.Where(m => m.StudentId == studentid).OrderByDescending(n => n.ReceiptDate).FirstOrDefault();
                                if (FeeReceipts != null)
                                {

                                    // Fee Receipt Detail
                                    // Calculate Amount
                                    var paidamount = FeeReceipts.PaidAmount;
                                    // get fee receipt detail
                                    var totalfeeamt = FeeReceipts.FeeReceiptDetails.Where(f => f.GroupFeeHeadId != null).Select(f => f.Amount).Sum();
                                    var totalfeeconsession = FeeReceipts.FeeReceiptDetails.Where(f => f.ConsessionId != null).Select(f => f.Amount).Sum();
                                    var payableamt = totalfeeamt - totalfeeconsession;

                                    var PendingFee = (paidamount - payableamt);
                                    var PaidFee = payableamt.ToString();

                                    if (PendingFee > 0)
                                    {
                                        Fees.Type = "Fee";
                                        Fees.Description = PendingFee + " Fee Pending";
                                        Fees.Title = "Fee";
                                        Fees.Date = "";
                                    }
                                    else
                                    {
                                        Fees.Type = "Fee";
                                        Fees.Description = PaidFee + "Fee Paid on" + FeeReceipts.ReceiptDate;
                                        Fees.Title = "Fee";
                                        Fees.Date = "";
                                    }
                                    DashBoardNew.Add(Fees);
                                }
                                //Attendance
                                var staffattendance = db.Attendances.Where(a => a.StudentId == studentid && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
                                Attendance = staffattendance.Count + "/" + totaldays;
                            }

                            //HW
                            var homeworklist = new List<HW>();
                            var seldate = DateTime.UtcNow.Date;
                            if (user.ContactType.ContactType1.ToLower() == "student")
                            {
                                var classId = 0;
                                //get student class id 
                                var studentClass = db.StudentClasses.Where(m => m.StudentId == user.UserContactId && m.SessionId == currentSession.SessionId).FirstOrDefault();
                                if (studentClass != null)
                                    classId = (int)studentClass.ClassId;

                                //get classsubjectid
                                var classsubjectidarray = db.ClassSubjects.Where(m => m.ClassId == classId && m.IsActive == true).Select(n => n.ClassSubjectId).ToArray();
                                //get homework
                                homeworklist = db.HWs.Where(m => classsubjectidarray.Contains((int)m.ClassSubjectId)).OrderByDescending(m => m.Date).Take(3).ToList();

                            }
                            else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                            {
                                var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                var classId = 0;
                                //get student class id 
                                var studentClass = db.StudentClasses.Where(m => m.StudentId == studentid && m.SessionId == currentSession.SessionId).FirstOrDefault();
                                if (studentClass != null)
                                    classId = (int)studentClass.ClassId;

                                var classsubjectidarray = db.ClassSubjects.Where(m => m.ClassId == classId && m.IsActive == true).Select(n => n.ClassSubjectId).ToArray();
                                //get homework
                                homeworklist = db.HWs.Where(m => classsubjectidarray.Contains((int)m.ClassSubjectId)).OrderByDescending(m => m.Date).Take(3).ToList();
                            }
                            else if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
                                homeworklist = db.HWs.Where(m => m.StaffId == user.UserContactId).OrderByDescending(m => m.Date).Take(3).ToList();

                            IList<HomeWorkModel> homeworllistrecords = new List<HomeWorkModel>();
                            var homework = new DashBoardNew();

                            var homeworkdetails = new HomeWorkDetails();
                            foreach (var hw in homeworklist)
                            {
                                homework = new DashBoardNew();
                                homework.PostedBy = hw.Staff.LName != null ? hw.Staff.FName + " " + hw.Staff.LName : hw.Staff.FName;

                                homework.PostedOn = ((DateTime)hw.Date).ToString("dd/MM/yyyy HH:mm:ss");
                                homework.Date = "";
                                homework.Description = "";
                                homework.Title = "";
                                homework.Type = "";
                                if (hw.ClassSubject != null)
                                {
                                    if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
                                    {
                                        homework.Description = hw.ClassSubject.ClassMaster.Class;
                                    }
                                    homework.Title = hw.ClassSubject.Subject.Subject1;
                                }
                                homework.Date = ConvertDate((DateTime)hw.Date);
                                homework.Type = "HomeWork";
                                if (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower().Contains("guardian"))
                                {
                                    homework.Description = hw.Staff.LName != null ? hw.Staff.FName + " " + hw.Staff.LName : hw.Staff.FName;
                                }
                                DashBoardNew.Add(homework);
                            }

                            return Json(new
                            {
                                status = "success",
                                Dashboard = DashBoardNew,
                                Attendance = Attendance,
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    execption = ex.Message,
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //[HttpPost]
        //public ActionResult DashboardNew(FormCollection form)
        //{
        //    string domain = "", UserName = "", Token = "";
        //    // form keys
        //    if (form["Domain"] != null)
        //        domain = form["Domain"].Trim();
        //    if (form["UserId"] != null)
        //        UserName = form["UserId"].Trim();
        //    if (form["Token"] != null)
        //        Token = form["Token"].Trim();

        //    // headers
        //    if (Request.Headers["Domain"] != null)
        //        domain = Convert.ToString(Request.Headers["Domain"]);
        //    if (Request.Headers["UserId"] != null)
        //        UserName = Convert.ToString(Request.Headers["UserId"]);
        //    if (Request.Headers["Token"] != null)
        //        Token = Convert.ToString(Request.Headers["Token"]);

        //    if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
        //    {
        //        return Json(new
        //        {
        //            status = "failed",
        //            message = "Parameter required"
        //        }, JsonRequestBehavior.AllowGet);
        //    }

        //    try
        //    {
        //        if (IsTokenValid(Token))
        //        {
        //            var dbdetail = AppDBConnection(Request);
        //            if (!string.IsNullOrEmpty(dbdetail.DBName))
        //            {
        //                string User = Request.Url.AbsolutePath;
        //                User = User.Split('/')[1];
        //                KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
        //                KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
        //                string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);
        //                // check user exist and find User info
        //                var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
        //                if (user != null)
        //                {
        //                    string postedby = string.Empty;
        //                    IList<DashBoardNew> DashBoardNew = new List<DashBoardNew>();
        //                    var currentSession = GetCurrentSession(dbdetail.DBName);

        //                    var currentdate = DateTime.Now.Date;
        //                    //all type of events
        //                    var allAnnoucements = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Announcement" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();
        //                    var allHolidays = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Holiday" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();
        //                    var allVacations = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Vacation" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();
        //                    var allActivity = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Activity" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderByDescending(p => p.StartDate).ToList();


        //                    var studentstandardid = 0;
        //                    if (user.ContactType.ContactType1.ToLower().Contains("student"))
        //                    {
        //                        var studentclass = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault();
        //                        int classid = 0;
        //                        if (studentclass != null)
        //                            classid = (int)studentclass.ClassId;
        //                        if (classid > 0)
        //                        {
        //                            var eventids = db.EventDetails.Where(m => m.ClassId == classid).Select(m => m.EventId).ToArray();
        //                            allActivity = allActivity.Where(m => eventids.Contains(m.EventId) || m.IsAllSchool == true).ToList();
        //                            studentstandardid = (int)db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
        //                        }

        //                    }
        //                    else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
        //                    {
        //                        var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
        //                        var classid = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault().ClassId;
        //                        var eventids = db.EventDetails.Where(m => m.ClassId == classid).Select(m => m.EventId).ToArray();
        //                        allActivity = allActivity.Where(m => eventids.Contains(m.EventId) || m.IsAllSchool == true).ToList();
        //                        studentstandardid = (int)db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
        //                    }

        //                    if (allAnnoucements.Count > 0)
        //                    {
        //                        foreach (var item in allAnnoucements)
        //                        {
        //                            var AnouncementsEvent = new DashBoardNew();
        //                            AnouncementsEvent.Type = "Anouncement";
        //                            AnouncementsEvent.Date = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
        //                            AnouncementsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
        //                            AnouncementsEvent.Title = item.EventTitle.Replace("<", "&lt;");
        //                            AnouncementsEvent.PostedOn = "";
        //                            if (item.PostedOn!=null)
        //                            AnouncementsEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
        //                            if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
        //                            {
        //                                var usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();

        //                                var username = db.Staffs.Where(p => p.StaffId == usedcontact.UserContactId).FirstOrDefault();
        //                                postedby = username.FName;
        //                                if (string.IsNullOrEmpty(username.LName))
        //                                    postedby = username.FName + " " + username.LName;
        //                            }
        //                            else if (user.ContactType.ContactType1.ToLower().Contains("admin"))
        //                            {
        //                                postedby = "Admin";
        //                            }
        //                            AnouncementsEvent.PostedBy = postedby;
        //                            DashBoardNew.Add(AnouncementsEvent);
        //                        }
        //                    }
        //                    if (allHolidays.Count > 0)
        //                    {
        //                        foreach (var item in allHolidays)
        //                        {
        //                            var HolidaysEvent = new DashBoardNew();
        //                            HolidaysEvent.Type = "Holiday";
        //                            HolidaysEvent.Date = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
        //                            HolidaysEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
        //                            HolidaysEvent.Title = item.EventTitle.Replace("<", "&lt;");
        //                            HolidaysEvent.PostedOn = "";
        //                            if (item.PostedOn != null)
        //                            HolidaysEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
        //                            if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
        //                            {
        //                                var usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();

        //                                var username = db.Staffs.Where(p => p.StaffId == usedcontact.UserContactId).FirstOrDefault();
        //                                postedby = username.FName;
        //                                if (string.IsNullOrEmpty(username.LName))
        //                                    postedby = username.FName + " " + username.LName;
        //                            }
        //                            else if (user.ContactType.ContactType1.ToLower().Contains("admin"))
        //                            {
        //                                postedby = "Admin";
        //                            }
        //                            HolidaysEvent.PostedBy = postedby;
        //                            DashBoardNew.Add(HolidaysEvent);
        //                        }
        //                    }
        //                    if (allVacations.Count > 0)
        //                    {
        //                        foreach (var item in allVacations)
        //                        {
        //                            var VacationsEvent = new DashBoardNew();
        //                            VacationsEvent.Type = "Vacation";
        //                            var StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
        //                            var EndDate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : null;
        //                            if (!string.IsNullOrEmpty(EndDate))
        //                                VacationsEvent.Date = StartDate + '-' + EndDate;
        //                            else
        //                                VacationsEvent.Date = StartDate;
        //                            VacationsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
        //                            VacationsEvent.Title = item.EventTitle.Replace("<", "&lt;");
        //                            VacationsEvent.PostedOn = "";
        //                            if (item.PostedOn != null)
        //                            VacationsEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
        //                            if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
        //                            {
        //                                var usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();

        //                                var username = db.Staffs.Where(p => p.StaffId == usedcontact.UserContactId).FirstOrDefault();
        //                                postedby = username.FName;
        //                                if (string.IsNullOrEmpty(username.LName))
        //                                    postedby = username.FName + " " + username.LName;
        //                            }
        //                            else if (user.ContactType.ContactType1.ToLower().Contains("admin"))
        //                            {
        //                                postedby = "Admin";
        //                            }
        //                            VacationsEvent.PostedBy = postedby;
        //                            DashBoardNew.Add(VacationsEvent);
        //                        }
        //                    }
        //                    if (allActivity.Count > 0)
        //                    {
        //                        var allclasses = db.ClassMasters.ToList();

        //                        foreach (var item in allActivity)
        //                        {
        //                            var ActivityEvent = new DashBoardNew();
        //                            ActivityEvent.Type = "Activity";

        //                            var StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
        //                            var EndDate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : null;
        //                            if (!string.IsNullOrEmpty(EndDate))
        //                                ActivityEvent.Date = StartDate + '-' + EndDate;
        //                            else
        //                                ActivityEvent.Date = StartDate;

        //                            //ActivityEvent.StartTime = item.StartTime.HasValue ? Convert.ToString(item.StartTime) : null;
        //                            //ActivityEvent.EndTime = item.EndTime.HasValue ? Convert.ToString(item.EndTime) : null;


        //                            ActivityEvent.Title = item.EventTitle.Replace("<", "&lt;");
        //                            ActivityEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
        //                            //ActivityEvent.Venue = item.Venue;

        //                            //if (item.EventDetails.Count > 0)
        //                            //{
        //                            //    var eventClasses = "";
        //                            //    var classids = item.EventDetails.Select(e => e.ClassId).ToArray();
        //                            //    eventClasses = String.Join(",", allclasses.Where(c => classids.Contains(c.ClassId)).Select(c => c.Class).ToList());

        //                            //    //if (studentclassid > 0 && classids.Contains(studentclassid))
        //                            //    //    eventClasses = _ICatalogMasterService.GetClassMasterById(studentclassid).Class;

        //                            //    ActivityEvent.Class = eventClasses;
        //                            //}
        //                            //else
        //                            //    ActivityEvent.Class = "All Classes";
        //                            ActivityEvent.PostedOn = "";
        //                            if (item.PostedOn != null)
        //                            ActivityEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
        //                            if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
        //                            {
        //                                var usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();

        //                                var username = db.Staffs.Where(p => p.StaffId == usedcontact.UserContactId).FirstOrDefault();
        //                                postedby = username.FName;
        //                                if (string.IsNullOrEmpty(username.LName))
        //                                    postedby = username.FName + " " + username.LName;
        //                            }
        //                            else if (user.ContactType.ContactType1.ToLower().Contains("admin"))
        //                            {
        //                                postedby = "Admin";
        //                            }
        //                            ActivityEvent.PostedBy = postedby;
        //                            DashBoardNew.Add(ActivityEvent);
        //                        }
        //                    }
        //                    //exams
        //                    var Exams = db.ExamDateSheetDetails.Where(m => m.StandardId == studentstandardid).ToList();
        //                    var ExamDetail = Exams.GroupBy(p => p.StandardId).Select(m => m.First()).ToList();

        //                    var datesheetAcctoPublisheddate = db.ExamPublishDetails.ToList();
        //                    if (datesheetAcctoPublisheddate.Count > 0)
        //                    {
        //                        datesheetAcctoPublisheddate = datesheetAcctoPublisheddate.Where(m => m.ExamPublishDate <= DateTime.Now.Date).ToList();
        //                        Exams = Exams.Where(m => (datesheetAcctoPublisheddate.Select(n => n.ExamDateSheetId).ToArray()).Contains((int)m.ExamDateSheetId)).ToList();
        //                        Exams = Exams.Where(p => p.ExamDate >= DateTime.Now.Date).OrderByDescending(e => e.ExamDate).ToList();
        //                        if (Exams.Count > 0)
        //                        {
        //                            foreach (var item in Exams)
        //                            {
        //                                var standard = db.StandardMasters.Where(m => m.StandardId == (int)item.StandardId).FirstOrDefault();
        //                                var subject = db.Subjects.Where(m => m.SubjectId == (int)item.SubjectId).FirstOrDefault();
        //                                var timeSlot = item.ExamTimeSlot.ExamTimeSlot1;

        //                                if (standard != null && subject != null)
        //                                    DashBoardNew.Add(new DashBoardNew { Title = standard.Standard + "/" + subject.Subject1, Description = timeSlot, Date = ConvertDate((DateTime)item.ExamDate), Type = "Exam" });
        //                            }
        //                        }
        //                    }

        //                    //Attendance
        //                    //attendance
        //                    // Entrance Class
        //                    var Curentdate = DateTime.UtcNow.Date;
        //                    // Sundays count
        //                    //int Sundaycount = 0;
        //                    //var sundayslist = _IMainService.GetSundayCountBetweenDates(ref Sundaycount, currentSession.StartDate.Value, Curentdate);
        //                    //// get holidays
        //                    //var holidayslist = db.Events.Where(e => e.EventType.EventType1 == "Holidays" && e.StartDate >= currentSession.StartDate
        //                    //    && e.StartDate <= Curentdate && e.IsActive == true && e.IsDeleted != true).Select(e => (DateTime)e.StartDate).ToList();

        //                    // combine both list and get distinct
        //                    //var alldatelist = sundayslist.Concat(holidayslist).Distinct().ToList();

        //                    // calculate attendance %
        //                    TimeSpan diff = Curentdate.AddDays(1) - currentSession.StartDate.Value;
        //                    int totaldays = diff.Days;
        //                    var Attendance = "";

        //                    if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
        //                    {
        //                        // get attendance by student
        //                        var staffattendance = db.StaffAttendances.Where(a => a.StaffId == user.UserContactId && a.StaffAttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
        //                        Attendance = staffattendance.Count + "/" + totaldays;
        //                    }

        //                    //Fee
        //                    if (user.ContactType.ContactType1.ToLower().Contains("student"))
        //                    {
        //                        var Fees = new DashBoardNew();
        //                        var FeeReceipts = db.FeeReceipts.Where(m => m.StudentId == user.UserContactId).OrderByDescending(n => n.ReceiptDate).FirstOrDefault();
        //                        if (FeeReceipts != null)
        //                        {
        //                            // Fee Receipt Detail
        //                            // Calculate Amount
        //                            var paidamount = FeeReceipts.PaidAmount;
        //                            // get fee receipt detail
        //                            var totalfeeamt = FeeReceipts.FeeReceiptDetails.Where(f => f.GroupFeeHeadId != null).Select(f => f.Amount).Sum();
        //                            var totalfeeconsession = FeeReceipts.FeeReceiptDetails.Where(f => f.ConsessionId != null).Select(f => f.Amount).Sum();
        //                            var payableamt = totalfeeamt - totalfeeconsession;

        //                            var PendingFee = (paidamount - payableamt);
        //                            var PaidFee = payableamt.ToString();

        //                            if (PendingFee > 0)
        //                            {
        //                                Fees.Type = "Fee";
        //                                Fees.Description = PendingFee + " Fee Pending";
        //                                Fees.Title = "Fee";
        //                                Fees.Date = "";
        //                                DashBoardNew.Add(Fees);
        //                            }

        //                        }
        //                        //Attendance
        //                        var staffattendance = db.Attendances.Where(a => a.StudentId == user.UserContactId && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
        //                        Attendance = staffattendance.Count + "/" + totaldays;

        //                    }
        //                    if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
        //                    {
        //                        var Fees = new DashBoardNew();
        //                        var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
        //                        var FeeReceipts = db.FeeReceipts.Where(m => m.StudentId == studentid).OrderByDescending(n => n.ReceiptDate).FirstOrDefault();
        //                        if (FeeReceipts != null)
        //                        {

        //                            // Fee Receipt Detail
        //                            // Calculate Amount
        //                            var paidamount = FeeReceipts.PaidAmount;
        //                            // get fee receipt detail
        //                            var totalfeeamt = FeeReceipts.FeeReceiptDetails.Where(f => f.GroupFeeHeadId != null).Select(f => f.Amount).Sum();
        //                            var totalfeeconsession = FeeReceipts.FeeReceiptDetails.Where(f => f.ConsessionId != null).Select(f => f.Amount).Sum();
        //                            var payableamt = totalfeeamt - totalfeeconsession;

        //                            var PendingFee = (paidamount - payableamt);
        //                            var PaidFee = payableamt.ToString();

        //                            if (PendingFee > 0)
        //                            {
        //                                Fees.Type = "Fee";
        //                                Fees.Description = PendingFee + " Fee Pending";
        //                                Fees.Title = "Fee";
        //                                Fees.Date = "";
        //                            }
        //                            else
        //                            {
        //                                Fees.Type = "Fee";
        //                                Fees.Description = PaidFee + "Fee Paid on" + FeeReceipts.ReceiptDate;
        //                                Fees.Title = "Fee";
        //                                Fees.Date = "";
        //                            }
        //                            DashBoardNew.Add(Fees);
        //                        }
        //                        //Attendance
        //                        var staffattendance = db.Attendances.Where(a => a.StudentId == studentid && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
        //                        Attendance = staffattendance.Count + "/" + totaldays;
        //                    }

        //                    //HW
        //                    var homeworklist = new List<HW>();
        //                    var seldate = DateTime.UtcNow.Date;
        //                    if (user.ContactType.ContactType1.ToLower() == "student")
        //                    {
        //                        var classId = 0;
        //                        //get student class id 
        //                        var studentClass = db.StudentClasses.Where(m => m.StudentId == user.UserContactId && m.SessionId == currentSession.SessionId).FirstOrDefault();
        //                        if (studentClass != null)
        //                            classId = (int)studentClass.ClassId;

        //                        //get classsubjectid
        //                        var classsubjectidarray = db.ClassSubjects.Where(m => m.ClassId == classId && m.IsActive == true).Select(n => n.ClassSubjectId).ToArray();
        //                        //get homework
        //                        homeworklist = db.HWs.Where(m => classsubjectidarray.Contains((int)m.ClassSubjectId)).OrderByDescending(m => m.Date).Take(3).ToList();

        //                    }
        //                    else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
        //                    {
        //                        var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
        //                        var classId = 0;
        //                        //get student class id 
        //                        var studentClass = db.StudentClasses.Where(m => m.StudentId == studentid && m.SessionId == currentSession.SessionId).FirstOrDefault();
        //                        if (studentClass != null)
        //                            classId = (int)studentClass.ClassId;

        //                        var classsubjectidarray = db.ClassSubjects.Where(m => m.ClassId == classId && m.IsActive == true).Select(n => n.ClassSubjectId).ToArray();
        //                        //get homework
        //                        homeworklist = db.HWs.Where(m => classsubjectidarray.Contains((int)m.ClassSubjectId)).OrderByDescending(m => m.Date).Take(3).ToList();
        //                    }
        //                    else if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
        //                        homeworklist = db.HWs.Where(m => m.StaffId == user.UserContactId).OrderByDescending(m => m.Date).Take(3).ToList();

        //                    IList<HomeWorkModel> homeworllistrecords = new List<HomeWorkModel>();
        //                    var homework = new DashBoardNew();
        //                    var homeworkdetails = new HomeWorkDetails();
        //                    foreach (var hw in homeworklist)
        //                    {
        //                        homework = new DashBoardNew();
        //                        if (hw.ClassSubject != null)
        //                        {
        //                            if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
        //                            {
        //                                homework.Description = hw.ClassSubject.ClassMaster.Class;
        //                            }
        //                            homework.Title = hw.ClassSubject.Subject.Subject1;
        //                        }
        //                        homework.Date = ConvertDate((DateTime)hw.Date);
        //                        homework.Type = hw.HWType.HWType1;
        //                        if (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower().Contains("guardian"))
        //                        {
        //                            homework.Description = hw.Staff.LName != null ? hw.Staff.FName + " " + hw.Staff.LName : hw.Staff.FName;
        //                        }
        //                        DashBoardNew.Add(homework);
        //                    }

        //                    return Json(new
        //                    {
        //                        status = "success",
        //                        Dashboard = DashBoardNew,
        //                        Attendance = Attendance,
        //                    }, JsonRequestBehavior.AllowGet);

        //                }
        //            }
        //            else
        //            {
        //                return Json(new
        //                {
        //                    status = "failed",
        //                    message = "User not found"
        //                }, JsonRequestBehavior.AllowGet);
        //            }
        //        }

        //        return Json(new
        //        {
        //            status = "failed",
        //            message = "Invalid Token"
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new
        //        {
        //            execption = ex.Message,
        //            status = "failed",
        //            message = "Invalid School"
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        [HttpPost]
        public ActionResult DashboardFilter(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";
            string[] requiredlist = new string[] { };
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["SelectedItems"] != null)
                requiredlist = (form["SelectedItems"]).TrimStart('[').TrimEnd(']').Split(',');

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["SelectedItems"] != null)
                requiredlist = Request.Headers["SelectedItems"].TrimStart('[').TrimEnd(']').Split(',');

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);
                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            IList<Dashboard> Dashboard = new List<Dashboard>();


                            var currentSession = GetCurrentSession(dbdetail.DBName);

                            var currentdate = DateTime.Now.Date;
                            //all type of events
                            var allAnnoucements = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Announcement" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                            var allHolidays = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Holiday" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                            var allVacations = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Vacation" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                            var allActivity = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Activity" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                            var studentstandardid = 0;
                            if (user.ContactType.ContactType1.ToLower().Contains("student"))
                            {
                                var classid = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault().ClassId;
                                var eventids = db.EventDetails.Where(m => m.ClassId == classid).Select(m => m.EventId).ToArray();
                                allActivity = db.Events.Where(m => eventids.Contains(m.EventId) && m.IsAllSchool == true).ToList();
                                studentstandardid = (int)db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
                            }
                            else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                            {
                                var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                var classid = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault().ClassId;
                                var eventids = db.EventDetails.Where(m => m.ClassId == classid).Select(m => m.EventId).ToArray();
                                allActivity = db.Events.Where(m => eventids.Contains(m.EventId) && m.IsAllSchool == true).ToList();
                                studentstandardid = (int)db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
                            }

                            var listitemtype = string.Empty;
                            foreach (var listitem in requiredlist)
                            {
                                listitemtype = listitem.Trim().Replace("\"", "");
                                if (string.IsNullOrEmpty(listitemtype))
                                    continue;

                                if (listitemtype == "Anouncement")
                                {
                                    if (allAnnoucements.Count > 0)
                                    {
                                        foreach (var item in allAnnoucements)
                                        {
                                            var AnouncementsEvent = new Dashboard();
                                            AnouncementsEvent.Type = "Anouncement";
                                            AnouncementsEvent.StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                            AnouncementsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                            AnouncementsEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");
                                            Dashboard.Add(AnouncementsEvent);
                                        }
                                    }
                                }
                                if (listitemtype == "Holiday")
                                {
                                    if (allHolidays.Count > 0)
                                    {
                                        foreach (var item in allHolidays)
                                        {
                                            var HolidaysEvent = new Dashboard();
                                            HolidaysEvent.Type = "Holiday";
                                            HolidaysEvent.StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                            HolidaysEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                            HolidaysEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");

                                            Dashboard.Add(HolidaysEvent);
                                        }
                                    }
                                }
                                if (listitemtype == "Vacation")
                                {
                                    if (allVacations.Count > 0)
                                    {
                                        foreach (var item in allVacations)
                                        {
                                            var VacationsEvent = new Dashboard();
                                            VacationsEvent.Type = "Vacation";
                                            VacationsEvent.StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                            VacationsEvent.EndDate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : null;
                                            VacationsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                            VacationsEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");
                                            Dashboard.Add(VacationsEvent);
                                        }
                                    }
                                }
                                if (listitemtype == "Activity")
                                {
                                    if (allActivity.Count > 0)
                                    {
                                        var allclasses = db.ClassMasters.ToList();

                                        foreach (var item in allActivity)
                                        {
                                            var ActivityEvent = new Dashboard();
                                            ActivityEvent.Type = "Activity";
                                            ActivityEvent.StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                            ActivityEvent.EndDate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : null;
                                            ActivityEvent.StartTime = item.StartTime.HasValue ? Convert.ToString(item.StartTime) : null;
                                            ActivityEvent.EndTime = item.EndTime.HasValue ? Convert.ToString(item.EndTime) : null;
                                            ActivityEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");
                                            ActivityEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                            ActivityEvent.Venue = item.Venue;

                                            if (item.EventDetails.Count > 0)
                                            {
                                                var eventClasses = "";
                                                var classids = item.EventDetails.Select(e => e.ClassId).ToArray();
                                                eventClasses = String.Join(",", allclasses.Where(c => classids.Contains(c.ClassId)).Select(c => c.Class).ToList());
                                                ActivityEvent.Class = eventClasses;
                                            }
                                            else
                                                ActivityEvent.Class = "All Classes";

                                            Dashboard.Add(ActivityEvent);
                                        }
                                    }
                                }
                                if (listitemtype == "Exam")
                                {
                                    //exams
                                    var Exams = db.ExamDateSheetDetails.Where(m => m.StandardId == studentstandardid).ToList();
                                    var ExamDetail = Exams.GroupBy(p => p.StandardId).Select(m => m.First()).ToList();

                                    var datesheetAcctoPublisheddate = db.ExamPublishDetails.ToList();
                                    if (datesheetAcctoPublisheddate.Count > 0)
                                    {
                                        datesheetAcctoPublisheddate = datesheetAcctoPublisheddate.Where(m => m.ExamPublishDate <= DateTime.Now.Date).ToList();
                                        Exams = Exams.Where(m => (datesheetAcctoPublisheddate.Select(n => n.ExamDateSheetId).ToArray()).Contains((int)m.ExamDateSheetId)).ToList();
                                        Exams = Exams.Where(p => p.ExamDate >= DateTime.Now.Date).OrderBy(e => e.ExamDate).ToList();
                                        if (Exams.Count > 0)
                                        {
                                            foreach (var item in Exams)
                                            {
                                                var standard = db.StandardMasters.Where(m => m.StandardId == (int)item.StandardId).FirstOrDefault();
                                                var subject = db.Subjects.Where(m => m.SubjectId == (int)item.SubjectId).FirstOrDefault();
                                                var timeSlot = item.ExamTimeSlot.ExamTimeSlot1;

                                                if (standard != null && subject != null)
                                                    Dashboard.Add(new Dashboard { ClassSubjectName = standard.Standard + "/" + subject.Subject1, TimeSlot = timeSlot, ExamDate = ConvertDate((DateTime)item.ExamDate), Type = "Exam" });
                                            }
                                        }
                                    }
                                }
                                if (listitemtype == "Fee")
                                {
                                    //Fee
                                    if (user.ContactType.ContactType1.ToLower().Contains("student"))
                                    {
                                        var Fees = new Dashboard();
                                        var FeeReceipts = db.FeeReceipts.Where(m => m.StudentId == user.UserContactId).OrderByDescending(n => n.ReceiptDate).FirstOrDefault();
                                        if (FeeReceipts != null)
                                        {

                                            // Fee Receipt Detail
                                            // Calculate Amount
                                            var paidamount = FeeReceipts.PaidAmount;
                                            // get fee receipt detail
                                            var totalfeeamt = FeeReceipts.FeeReceiptDetails.Where(f => f.GroupFeeHeadId != null).Select(f => f.Amount).Sum();
                                            var totalfeeconsession = FeeReceipts.FeeReceiptDetails.Where(f => f.ConsessionId != null).Select(f => f.Amount).Sum();
                                            var payableamt = totalfeeamt - totalfeeconsession;

                                            var PendingFee = (paidamount - payableamt);
                                            var PaidFee = payableamt.ToString();

                                            if (PendingFee > 0)
                                            {
                                                Fees.Type = "Fee";
                                                Fees.Fee = PendingFee + " Fee Pending";
                                            }
                                            else
                                            {
                                                Fees.Type = "Fee";
                                                Fees.Fee = PaidFee + "Fee Paid on" + FeeReceipts.ReceiptDate;
                                            }
                                            Dashboard.Add(Fees);
                                        }


                                    }
                                    if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                                    {
                                        var Fees = new Dashboard();
                                        var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                        var FeeReceipts = db.FeeReceipts.Where(m => m.StudentId == studentid).OrderByDescending(n => n.ReceiptDate).FirstOrDefault();
                                        if (FeeReceipts != null)
                                        {

                                            // Fee Receipt Detail
                                            // Calculate Amount
                                            var paidamount = FeeReceipts.PaidAmount;
                                            // get fee receipt detail
                                            var totalfeeamt = FeeReceipts.FeeReceiptDetails.Where(f => f.GroupFeeHeadId != null).Select(f => f.Amount).Sum();
                                            var totalfeeconsession = FeeReceipts.FeeReceiptDetails.Where(f => f.ConsessionId != null).Select(f => f.Amount).Sum();
                                            var payableamt = totalfeeamt - totalfeeconsession;

                                            var PendingFee = (paidamount - payableamt);
                                            var PaidFee = payableamt.ToString();

                                            if (PendingFee > 0)
                                            {
                                                Fees.Type = "Fee";
                                                Fees.Fee = PendingFee + " Fee Pending";
                                            }
                                            else
                                            {
                                                Fees.Type = "Fee";
                                                Fees.Fee = PaidFee + "Fee Paid on" + FeeReceipts.ReceiptDate;
                                            }
                                            Dashboard.Add(Fees);
                                        }

                                    }
                                }


                                if (listitemtype.ToLower().Contains("homework") || listitemtype.ToLower().Contains("assignment"))
                                {
                                    //HW
                                    var homeworklist = new List<HW>();
                                    var seldate = DateTime.Now;
                                    if (user.ContactType.ContactType1.ToLower() == "student")
                                    {
                                        var classId = 0;
                                        //get student class id 
                                        var studentClass = db.StudentClasses.Where(m => m.StudentId == user.UserContactId && m.SessionId == currentSession.SessionId).FirstOrDefault();
                                        if (studentClass != null)
                                            classId = (int)studentClass.ClassId;

                                        //get classsubjectid
                                        var classsubjectidarray = db.ClassSubjects.Where(m => m.ClassId == classId && m.IsActive == true).Select(n => n.ClassSubjectId).ToArray();
                                        //get homework
                                        homeworklist = db.HWs.Where(m => classsubjectidarray.Contains((int)m.ClassSubjectId) && m.HWType.HWType1.ToLower().Contains(listitemtype.ToLower())).OrderByDescending(m => m.Date).Take(3).ToList();

                                    }
                                    else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                                    {
                                        var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                        var classId = 0;
                                        //get student class id 
                                        var studentClass = db.StudentClasses.Where(m => m.StudentId == studentid && m.SessionId == currentSession.SessionId).FirstOrDefault();
                                        if (studentClass != null)
                                            classId = (int)studentClass.ClassId;

                                        var classsubjectidarray = db.ClassSubjects.Where(m => m.ClassId == classId && m.IsActive == true).Select(n => n.ClassSubjectId).ToArray();
                                        //get homework
                                        homeworklist = db.HWs.Where(m => classsubjectidarray.Contains((int)m.ClassSubjectId) && m.HWType.HWType1.ToLower().Contains(listitemtype.ToLower())).OrderByDescending(m => m.Date).Take(3).ToList();
                                    }
                                    else if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
                                    {
                                        homeworklist = db.HWs.Where(m => m.StaffId == user.UserContactId && m.HWType.HWType1.ToLower().Contains(listitemtype.ToLower())).OrderByDescending(m => m.Date).Take(3).ToList();
                                    }
                                    IList<HomeWorkModel> homeworllistrecords = new List<HomeWorkModel>();
                                    var homework = new Dashboard();
                                    var homeworkdetails = new HomeWorkDetails();
                                    foreach (var hw in homeworklist)
                                    {
                                        homework = new Dashboard();
                                        if (hw.ClassSubject != null)
                                        {
                                            homework.Class = hw.ClassSubject.ClassMaster.Class;
                                            homework.Subject = hw.ClassSubject.Subject.Subject1;
                                        }
                                        homework.Date = ConvertDate((DateTime)hw.Date);
                                        homework.Type = hw.HWType.HWType1;
                                        homework.teacher = hw.Staff.LName != null ? hw.Staff.FName + " " + hw.Staff.LName : hw.Staff.FName;

                                        Dashboard.Add(homework);
                                    }

                                }

                            }
                            //Attendance
                            //attendance
                            // Entrance Class
                            var Curentdate = DateTime.UtcNow.Date;
                            // Sundays count
                            //int Sundaycount = 0;
                            //var sundayslist = _IMainService.GetSundayCountBetweenDates(ref Sundaycount, currentSession.StartDate.Value, Curentdate);

                            //// get holidays
                            //var holidayslist = db.Events.Where(e => e.EventType.EventType1 == "Holidays" && e.StartDate >= currentSession.StartDate
                            //    && e.StartDate <= Curentdate && e.IsActive == true && e.IsDeleted != true).Select(e => (DateTime)e.StartDate).ToList();

                            //// combine both list and get distinct
                            //var alldatelist = sundayslist.Concat(holidayslist).Distinct().ToList();
                            // calculate attendance %
                            TimeSpan diff = Curentdate - currentSession.StartDate.Value;
                            int totaldays = diff.Days; //-alldatelist.Count;
                            var Attendance = "";
                            if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
                            {
                                // get attendance by student
                                var staffattendance = db.StaffAttendances.Where(a => a.StaffId == user.UserContactId && a.StaffAttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
                                Attendance = staffattendance.Count + "/" + totaldays;
                            }
                            else if (user.ContactType.ContactType1.ToLower().Contains("student"))
                            {
                                //Attendance
                                var staffattendance = db.Attendances.Where(a => a.StudentId == user.UserContactId && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
                                Attendance = staffattendance.Count + "/" + totaldays;
                            }
                            else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                            {
                                var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                //Attendance
                                var staffattendance = db.Attendances.Where(a => a.StudentId == studentid && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
                                Attendance = staffattendance.Count + "/" + totaldays;
                            }

                            return Json(new
                            {
                                status = "success",
                                Dashboard = Dashboard,
                                Attendance = Attendance,
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DashboardFilterNew(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";
            string[] requiredlist = new string[] { };
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["SelectedItems"] != null)
                requiredlist = (form["SelectedItems"]).TrimStart('[').TrimEnd(']').Split(',');

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["SelectedItems"] != null)
                requiredlist = Request.Headers["SelectedItems"].TrimStart('[').TrimEnd(']').Split(',');

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);
                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            IList<DashBoardNew> DashBoardNew = new List<DashBoardNew>();

                            string postedby = string.Empty;
                            var currentSession = GetCurrentSession(dbdetail.DBName);

                            var currentdate = DateTime.Now.Date;
                            //all type of events
                            var allAnnoucements = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Announcement" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                            var allHolidays = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Holiday" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                            var allVacations = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Vacation" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                            var allActivity = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Activity" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                            var studentstandardid = 0;
                            var classid = 0;
                            if (user.ContactType.ContactType1.ToLower().Contains("student"))
                            {
                                var studentclass = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault();
                                if (studentclass != null)
                                    classid = (int)studentclass.ClassId;
                                if (classid > 0)
                                {
                                    var eventids = db.EventDetails.Where(m => m.ClassId == classid).Select(m => m.EventId).ToArray();
                                    allActivity = allActivity.Where(m => eventids.Contains(m.EventId) || m.IsAllSchool == true).ToList();
                                    studentstandardid = (int)db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
                                }
                            }
                            else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                            {
                                var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                // var classid = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault().ClassId;
                                var studentclass = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault();
                                if (studentclass != null)
                                    classid = (int)studentclass.ClassId;
                                if (classid > 0)
                                {
                                    var eventids = db.EventDetails.Where(m => m.ClassId == classid).Select(m => m.EventId).ToArray();
                                    allActivity = allActivity.Where(m => eventids.Contains(m.EventId) || m.IsAllSchool == true).ToList();
                                    studentstandardid = (int)db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
                                }
                            }

                            var listitemtype = string.Empty;
                            foreach (var listitem in requiredlist)
                            {
                                listitemtype = listitem.Trim().Replace("\"", "");
                                if (string.IsNullOrEmpty(listitemtype))
                                    continue;

                                if (listitemtype == "Anouncement")
                                {
                                    var AnouncementsEvent = new DashBoardNew();
                                    if (allAnnoucements.Count > 0)
                                    {
                                        foreach (var item in allAnnoucements)
                                        {
                                            AnouncementsEvent = new DashBoardNew();
                                            AnouncementsEvent.Type = "Anouncement";
                                            AnouncementsEvent.Date = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                            AnouncementsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                            AnouncementsEvent.Title = item.EventTitle.Replace("<", "&lt;");
                                            AnouncementsEvent.PostedOn = "";
                                            if (item.PostedOn != null)
                                                AnouncementsEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
                                            SchoolUser usedcontact = new SchoolUser();
                                            if (item.PostedBy > 0)
                                                usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();
                                            if (usedcontact.ContactType != null)
                                            {
                                                var contacttype = usedcontact.ContactType.ContactType1;
                                                switch (contacttype)
                                                {
                                                    case "Student":
                                                        var studentdet = db.Students.Where(m => m.StudentId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                        postedby = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
                                                        break;
                                                    case "Teacher":
                                                        var staffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                        postedby = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
                                                        break;
                                                    case "Non-Teaching":
                                                        var nonstaffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                        postedby = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
                                                        break;
                                                    case "Guardian":
                                                        var guardiandet = db.Guardians.Where(m => m.GuardianId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                        postedby = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
                                                        break;
                                                    case "Admin":
                                                        postedby = usedcontact.UserName;
                                                        break;
                                                }
                                            }
                                            AnouncementsEvent.PostedBy = postedby;
                                            DashBoardNew.Add(AnouncementsEvent);
                                        }
                                    }
                                    if (listitemtype == "Anouncement")
                                    {
                                        if (allHolidays.Count > 0)
                                        {
                                            foreach (var item in allHolidays)
                                            {
                                                AnouncementsEvent = new DashBoardNew();
                                                AnouncementsEvent.Type = "Anouncement";
                                                AnouncementsEvent.Date = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                                AnouncementsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                                AnouncementsEvent.Title = item.EventTitle.Replace("<", "&lt;");
                                                AnouncementsEvent.PostedOn = "";
                                                if (item.PostedOn != null)
                                                    AnouncementsEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
                                                SchoolUser usedcontact = new SchoolUser();
                                                if (item.PostedBy > 0)
                                                    usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();
                                                if (usedcontact.ContactType != null)
                                                {
                                                    var contacttype = usedcontact.ContactType.ContactType1;
                                                    switch (contacttype)
                                                    {
                                                        case "Student":
                                                            var studentdet = db.Students.Where(m => m.StudentId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                            postedby = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
                                                            break;
                                                        case "Teacher":
                                                            var staffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                            postedby = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
                                                            break;
                                                        case "Non-Teaching":
                                                            var nonstaffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                            postedby = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
                                                            break;
                                                        case "Guardian":
                                                            var guardiandet = db.Guardians.Where(m => m.GuardianId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                            postedby = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
                                                            break;
                                                        case "Admin":
                                                            postedby = usedcontact.UserName;
                                                            break;
                                                    }
                                                }
                                                AnouncementsEvent.PostedBy = postedby;
                                                DashBoardNew.Add(AnouncementsEvent);
                                            }
                                        }
                                    }
                                    if (listitemtype == "Anouncement")
                                    {
                                        if (allVacations.Count > 0)
                                        {
                                            foreach (var item in allVacations)
                                            {
                                                AnouncementsEvent = new DashBoardNew();
                                                AnouncementsEvent.Type = "Anouncement";
                                                var StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                                var EndDate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : null;
                                                if (!string.IsNullOrEmpty(EndDate))
                                                    AnouncementsEvent.Date = StartDate + '-' + EndDate;
                                                else
                                                    AnouncementsEvent.Date = StartDate;
                                                AnouncementsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                                AnouncementsEvent.Title = item.EventTitle.Replace("<", "&lt;");
                                                AnouncementsEvent.PostedOn = "";
                                                if (item.PostedOn != null)
                                                    AnouncementsEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
                                                SchoolUser usedcontact = new SchoolUser();
                                                if (item.PostedBy > 0)
                                                    usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();
                                                if (usedcontact.ContactType != null)
                                                {
                                                    var contacttype = usedcontact.ContactType.ContactType1;
                                                    switch (contacttype)
                                                    {
                                                        case "Student":
                                                            var studentdet = db.Students.Where(m => m.StudentId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                            postedby = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
                                                            break;
                                                        case "Teacher":
                                                            var staffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                            postedby = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
                                                            break;
                                                        case "Non-Teaching":
                                                            var nonstaffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                            postedby = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
                                                            break;
                                                        case "Guardian":
                                                            var guardiandet = db.Guardians.Where(m => m.GuardianId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                            postedby = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
                                                            break;
                                                        case "Admin":
                                                            postedby = usedcontact.UserName;
                                                            break;
                                                    }
                                                }
                                                AnouncementsEvent.PostedBy = postedby;
                                                DashBoardNew.Add(AnouncementsEvent);
                                            }
                                        }
                                    }
                                    if (listitemtype == "Anouncement")
                                    {
                                        if (allActivity.Count > 0)
                                        {
                                            var allclasses = db.ClassMasters.ToList();

                                            foreach (var item in allActivity)
                                            {
                                                AnouncementsEvent = new DashBoardNew();
                                                AnouncementsEvent.Type = "Anouncement";

                                                var StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
                                                var EndDate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : null;
                                                if (!string.IsNullOrEmpty(EndDate))
                                                    AnouncementsEvent.Date = StartDate + '-' + EndDate;
                                                else
                                                    AnouncementsEvent.Date = StartDate;

                                                //ActivityEvent.StartTime = item.StartTime.HasValue ? Convert.ToString(item.StartTime) : null;
                                                //ActivityEvent.EndTime = item.EndTime.HasValue ? Convert.ToString(item.EndTime) : null;


                                                AnouncementsEvent.Title = item.EventTitle.Replace("<", "&lt;");
                                                AnouncementsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                                                //ActivityEvent.Venue = item.Venue;

                                                //if (item.EventDetails.Count > 0)
                                                //{
                                                //    var eventClasses = "";
                                                //    var classids = item.EventDetails.Select(e => e.ClassId).ToArray();
                                                //    eventClasses = String.Join(",", allclasses.Where(c => classids.Contains(c.ClassId)).Select(c => c.Class).ToList());

                                                //    //if (studentclassid > 0 && classids.Contains(studentclassid))
                                                //    //    eventClasses = _ICatalogMasterService.GetClassMasterById(studentclassid).Class;

                                                //    ActivityEvent.Class = eventClasses;
                                                //}
                                                //else
                                                //    ActivityEvent.Class = "All Classes";
                                                AnouncementsEvent.PostedOn = "";
                                                if (item.PostedOn != null)
                                                    AnouncementsEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
                                                SchoolUser usedcontact = new SchoolUser();
                                                if (item.PostedBy > 0)
                                                    usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();
                                                if (usedcontact.ContactType != null)
                                                {
                                                    var contacttype = usedcontact.ContactType.ContactType1;
                                                    switch (contacttype)
                                                    {
                                                        case "Student":
                                                            var studentdet = db.Students.Where(m => m.StudentId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                            postedby = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
                                                            break;
                                                        case "Teacher":
                                                            var staffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                            postedby = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
                                                            break;
                                                        case "Non-Teaching":
                                                            var nonstaffdet = db.Staffs.Where(m => m.StaffId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                            postedby = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
                                                            break;
                                                        case "Guardian":
                                                            var guardiandet = db.Guardians.Where(m => m.GuardianId == (int)usedcontact.UserContactId).FirstOrDefault();
                                                            postedby = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
                                                            break;
                                                        case "Admin":
                                                            postedby = usedcontact.UserName;
                                                            break;
                                                    }
                                                }
                                                AnouncementsEvent.PostedBy = postedby;
                                                DashBoardNew.Add(AnouncementsEvent);
                                            }
                                        }
                                    }
                                    if (listitemtype == "Anouncement")
                                    {
                                        //exams
                                        var Exams = db.ExamDateSheetDetails.Where(m => m.StandardId == studentstandardid).ToList();
                                        var ExamDetail = Exams.GroupBy(p => p.StandardId).Select(m => m.First()).ToList();

                                        var datesheetAcctoPublisheddate = db.ExamPublishDetails.ToList();
                                        if (datesheetAcctoPublisheddate.Count > 0)
                                        {
                                            datesheetAcctoPublisheddate = datesheetAcctoPublisheddate.Where(m => m.ExamPublishDate <= DateTime.Now.Date).ToList();
                                            Exams = Exams.Where(m => (datesheetAcctoPublisheddate.Select(n => n.ExamDateSheetId).ToArray()).Contains((int)m.ExamDateSheetId)).ToList();
                                            Exams = Exams.Where(p => p.ExamDate >= DateTime.Now.Date).OrderBy(e => e.ExamDate).ToList();
                                            if (Exams.Count > 0)
                                            {
                                                foreach (var item in Exams)
                                                {
                                                    var standard = db.StandardMasters.Where(m => m.StandardId == (int)item.StandardId).FirstOrDefault();
                                                    var subject = db.Subjects.Where(m => m.SubjectId == (int)item.SubjectId).FirstOrDefault();
                                                    var timeSlot = item.ExamTimeSlot.ExamTimeSlot1;

                                                    if (standard != null && subject != null)
                                                        DashBoardNew.Add(new DashBoardNew { Title = standard.Standard + "/" + subject.Subject1, Description = timeSlot, Date = ConvertDate((DateTime)item.ExamDate), Type = "Anouncement", PostedBy = "", PostedOn = "" });
                                                }
                                            }
                                        }
                                    }
                                }
                                DashBoardNew = DashBoardNew.OrderByDescending(m => m.PostedOn).ToList();
                                if (listitemtype == "Fee")
                                {
                                    //Fee
                                    if (user.ContactType.ContactType1.ToLower().Contains("student"))
                                    {
                                        var Fees = new DashBoardNew();
                                        Fees.PostedBy = "";
                                        Fees.PostedOn = "";
                                        Fees.Date = "";
                                        Fees.Description = "";
                                        Fees.Title = "";
                                        Fees.Type = "";
                                        var FeeReceipts = db.FeeReceipts.Where(m => m.StudentId == user.UserContactId).OrderByDescending(n => n.ReceiptDate).FirstOrDefault();
                                        if (FeeReceipts != null)
                                        {

                                            // Fee Receipt Detail
                                            // Calculate Amount
                                            var paidamount = FeeReceipts.PaidAmount;
                                            // get fee receipt detail
                                            var totalfeeamt = FeeReceipts.FeeReceiptDetails.Where(f => f.GroupFeeHeadId != null).Select(f => f.Amount).Sum();
                                            var totalfeeconsession = FeeReceipts.FeeReceiptDetails.Where(f => f.ConsessionId != null).Select(f => f.Amount).Sum();
                                            var payableamt = totalfeeamt - totalfeeconsession;

                                            var PendingFee = (paidamount - payableamt);
                                            var PaidFee = payableamt.ToString();

                                            if (PendingFee > 0)
                                            {

                                                Fees.Type = "Fee";
                                                Fees.Description = PendingFee + " Fee Pending";
                                                Fees.Title = "Fee";
                                                Fees.Date = "";
                                            }
                                            else
                                            {
                                                Fees.Type = "Fee";
                                                Fees.Description = PaidFee + "Fee Paid on" + FeeReceipts.ReceiptDate;
                                                Fees.Title = "Fee";
                                                Fees.Date = "";
                                            }
                                            DashBoardNew.Add(Fees);
                                        }


                                    }
                                    if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                                    {
                                        var Fees = new DashBoardNew();
                                        Fees.PostedBy = "";
                                        Fees.PostedOn = "";
                                        Fees.Date = "";
                                        Fees.Description = "";
                                        Fees.Title = "";
                                        Fees.Type = "";

                                        var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                        var FeeReceipts = db.FeeReceipts.Where(m => m.StudentId == studentid).OrderByDescending(n => n.ReceiptDate).FirstOrDefault();
                                        if (FeeReceipts != null)
                                        {

                                            // Fee Receipt Detail
                                            // Calculate Amount
                                            var paidamount = FeeReceipts.PaidAmount;
                                            // get fee receipt detail
                                            var totalfeeamt = FeeReceipts.FeeReceiptDetails.Where(f => f.GroupFeeHeadId != null).Select(f => f.Amount).Sum();
                                            var totalfeeconsession = FeeReceipts.FeeReceiptDetails.Where(f => f.ConsessionId != null).Select(f => f.Amount).Sum();
                                            var payableamt = totalfeeamt - totalfeeconsession;

                                            var PendingFee = (paidamount - payableamt);
                                            var PaidFee = payableamt.ToString();

                                            if (PendingFee > 0)
                                            {

                                                Fees.Type = "Fee";
                                                Fees.Description = PendingFee + " Fee Pending";
                                                Fees.Title = "Fee";
                                                Fees.Date = "";
                                            }
                                            else
                                            {
                                                Fees.Type = "Fee";
                                                Fees.Description = PaidFee + "Fee Paid on" + FeeReceipts.ReceiptDate;
                                                Fees.Title = "Fee";
                                                Fees.Date = "";
                                            }
                                            DashBoardNew.Add(Fees);
                                        }

                                    }
                                }


                                if (listitemtype.ToLower().Contains("homework") || listitemtype.ToLower().Contains("assignment"))
                                {
                                    //HW
                                    var homeworklist = new List<HW>();
                                    var seldate = DateTime.Now;
                                    if (user.ContactType.ContactType1.ToLower() == "student")
                                    {
                                        var classId = 0;
                                        //get student class id 
                                        var studentClass = db.StudentClasses.Where(m => m.StudentId == user.UserContactId && m.SessionId == currentSession.SessionId).FirstOrDefault();
                                        if (studentClass != null)
                                            classId = (int)studentClass.ClassId;

                                        //get classsubjectid
                                        var classsubjectidarray = db.ClassSubjects.Where(m => m.ClassId == classId && m.IsActive == true).Select(n => n.ClassSubjectId).ToArray();
                                        //get homework
                                        homeworklist = db.HWs.Where(m => classsubjectidarray.Contains((int)m.ClassSubjectId) && m.HWType.HWType1.ToLower().Contains(listitemtype.ToLower())).OrderByDescending(m => m.Date).Take(3).ToList();

                                    }
                                    else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                                    {
                                        var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                        var classId = 0;
                                        //get student class id 
                                        var studentClass = db.StudentClasses.Where(m => m.StudentId == studentid && m.SessionId == currentSession.SessionId).FirstOrDefault();
                                        if (studentClass != null)
                                            classId = (int)studentClass.ClassId;

                                        var classsubjectidarray = db.ClassSubjects.Where(m => m.ClassId == classId && m.IsActive == true).Select(n => n.ClassSubjectId).ToArray();
                                        //get homework
                                        homeworklist = db.HWs.Where(m => classsubjectidarray.Contains((int)m.ClassSubjectId) && m.HWType.HWType1.ToLower().Contains(listitemtype.ToLower())).OrderByDescending(m => m.Date).Take(3).ToList();
                                    }
                                    else if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
                                    {
                                        homeworklist = db.HWs.Where(m => m.StaffId == user.UserContactId && m.HWType.HWType1.ToLower().Contains(listitemtype.ToLower())).OrderByDescending(m => m.Date).Take(3).ToList();
                                    }
                                    IList<HomeWorkModel> homeworllistrecords = new List<HomeWorkModel>();
                                    var homework = new DashBoardNew();

                                    var homeworkdetails = new HomeWorkDetails();

                                    foreach (var hw in homeworklist)
                                    {
                                        homework = new DashBoardNew();
                                        homework.PostedBy = hw.Staff.LName != null ? hw.Staff.FName + " " + hw.Staff.LName : hw.Staff.FName;
                                        homework.PostedOn = ((DateTime)hw.Date).ToString("dd/MM/yyyy HH:mm:ss");
                                        homework.Date = "";
                                        homework.Description = "";
                                        homework.Title = "";
                                        homework.Type = "";
                                        if (hw.ClassSubject != null)
                                        {
                                            if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
                                            {
                                                homework.Description = hw.ClassSubject.ClassMaster.Class;
                                            }
                                            homework.Title = hw.ClassSubject.Subject.Subject1;
                                        }
                                        homework.Date = ConvertDate((DateTime)hw.Date);
                                        homework.Type = "HomeWork";
                                        if (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower().Contains("guardian"))
                                        {
                                            homework.Description = hw.Staff.LName != null ? hw.Staff.FName + " " + hw.Staff.LName : hw.Staff.FName;
                                        }
                                        DashBoardNew.Add(homework);
                                    }

                                }

                            }
                            //Attendance
                            //attendance
                            // Entrance Class
                            var Curentdate = DateTime.UtcNow.Date;
                            // Sundays count
                            //int Sundaycount = 0;
                            //var sundayslist = _IMainService.GetSundayCountBetweenDates(ref Sundaycount, currentSession.StartDate.Value, Curentdate);

                            //// get holidays
                            //var holidayslist = db.Events.Where(e => e.EventType.EventType1 == "Holidays" && e.StartDate >= currentSession.StartDate
                            //    && e.StartDate <= Curentdate && e.IsActive == true && e.IsDeleted != true).Select(e => (DateTime)e.StartDate).ToList();

                            //// combine both list and get distinct
                            //var alldatelist = sundayslist.Concat(holidayslist).Distinct().ToList();
                            // calculate attendance %
                            TimeSpan diff = Curentdate - currentSession.StartDate.Value;
                            int totaldays = diff.Days; //-alldatelist.Count;
                            var Attendance = "";
                            if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
                            {
                                // get attendance by student
                                var staffattendance = db.StaffAttendances.Where(a => a.StaffId == user.UserContactId && a.StaffAttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
                                Attendance = staffattendance.Count + "/" + totaldays;
                            }
                            else if (user.ContactType.ContactType1.ToLower().Contains("student"))
                            {
                                //Attendance
                                var staffattendance = db.Attendances.Where(a => a.StudentId == user.UserContactId && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
                                Attendance = staffattendance.Count + "/" + totaldays;
                            }
                            else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
                            {
                                var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                //Attendance
                                var staffattendance = db.Attendances.Where(a => a.StudentId == studentid && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
                                Attendance = staffattendance.Count + "/" + totaldays;
                            }

                            return Json(new
                            {
                                status = "success",
                                Dashboard = DashBoardNew,
                                Attendance = Attendance,
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //[HttpPost]
        //public ActionResult DashboardFilterNew(FormCollection form)
        //{
        //    string domain = "", UserName = "", Token = "";
        //    string[] requiredlist = new string[] { };
        //    // form keys
        //    if (form["Domain"] != null)
        //        domain = form["Domain"].Trim();
        //    if (form["UserId"] != null)
        //        UserName = form["UserId"].Trim();
        //    if (form["Token"] != null)
        //        Token = form["Token"].Trim();
        //    if (form["SelectedItems"] != null)
        //        requiredlist = (form["SelectedItems"]).TrimStart('[').TrimEnd(']').Split(',');

        //    // headers
        //    if (Request.Headers["Domain"] != null)
        //        domain = Convert.ToString(Request.Headers["Domain"]);
        //    if (Request.Headers["UserId"] != null)
        //        UserName = Convert.ToString(Request.Headers["UserId"]);
        //    if (Request.Headers["Token"] != null)
        //        Token = Convert.ToString(Request.Headers["Token"]);
        //    if (Request.Headers["SelectedItems"] != null)
        //        requiredlist = Request.Headers["SelectedItems"].TrimStart('[').TrimEnd(']').Split(',');

        //    if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
        //    {
        //        return Json(new
        //        {
        //            status = "failed",
        //            message = "Parameter required"
        //        }, JsonRequestBehavior.AllowGet);
        //    }

        //    try
        //    {
        //        if (IsTokenValid(Token))
        //        {
        //            var dbdetail = AppDBConnection(Request);
        //            if (!string.IsNullOrEmpty(dbdetail.DBName))
        //            {
        //                string User = Request.Url.AbsolutePath;
        //                User = User.Split('/')[1];
        //                KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
        //                KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
        //                string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);
        //                // check user exist and find User info
        //                var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
        //                if (user != null)
        //                {
        //                    IList<DashBoardNew> DashBoardNew = new List<DashBoardNew>();

        //                    string postedby = string.Empty;
        //                    var currentSession = GetCurrentSession(dbdetail.DBName);

        //                    var currentdate = DateTime.Now.Date;
        //                    //all type of events
        //                    var allAnnoucements = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Announcement" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
        //                    var allHolidays = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Holiday" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
        //                    var allVacations = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Vacation" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
        //                    var allActivity = db.Events.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Activity" && p.StartDate >= currentdate && (p.EndDate <= currentSession.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
        //                    var studentstandardid = 0;
        //                    var classid = 0;
        //                    if (user.ContactType.ContactType1.ToLower().Contains("student"))
        //                    {
        //                        var studentclass = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault();
        //                        if (studentclass != null)
        //                            classid = (int)studentclass.ClassId;
        //                        if (classid > 0)
        //                        {
        //                            var eventids = db.EventDetails.Where(m => m.ClassId == classid).Select(m => m.EventId).ToArray();
        //                            allActivity = allActivity.Where(m => eventids.Contains(m.EventId) || m.IsAllSchool == true).ToList();
        //                            studentstandardid = (int)db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
        //                        }
        //                    }
        //                    else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
        //                    {
        //                        var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
        //                        // var classid = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault().ClassId;
        //                        var studentclass = db.StudentClasses.Where(m => m.StudentId == user.UserContactId).FirstOrDefault();
        //                        if (studentclass != null)
        //                            classid = (int)studentclass.ClassId;
        //                        if (classid > 0)
        //                        {
        //                            var eventids = db.EventDetails.Where(m => m.ClassId == classid).Select(m => m.EventId).ToArray();
        //                            allActivity = allActivity.Where(m => eventids.Contains(m.EventId) || m.IsAllSchool == true).ToList();
        //                            studentstandardid = (int)db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().StandardId;
        //                        }
        //                    }

        //                    var listitemtype = string.Empty;
        //                    foreach (var listitem in requiredlist)
        //                    {
        //                        listitemtype = listitem.Trim().Replace("\"", "");
        //                        if (string.IsNullOrEmpty(listitemtype))
        //                            continue;

        //                        if (listitemtype == "Anouncement")
        //                        {
        //                            if (allAnnoucements.Count > 0)
        //                            {
        //                                foreach (var item in allAnnoucements)
        //                                {
        //                                    var AnouncementsEvent = new DashBoardNew();
        //                                    AnouncementsEvent.Type = "Anouncement";
        //                                    AnouncementsEvent.Date = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
        //                                    AnouncementsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
        //                                    AnouncementsEvent.Title = item.EventTitle.Replace("<", "&lt;");
        //                                    AnouncementsEvent.PostedOn = "";
        //                                    if (item.PostedOn != null)
        //                                    AnouncementsEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
        //                                    var usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();
        //                                    if (usedcontact != null)
        //                                    {
        //                                        var contacttype = usedcontact.ContactType.ContactType1;
        //                                        switch (contacttype.ToLower())
        //                                        {
        //                                            case "Student":
        //                                                var studentdet = db.Students.Where(m => m.StudentId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
        //                                                break;
        //                                            case "Teacher":
        //                                                var staffdet = db.Staffs.Where(m => m.StaffId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
        //                                                break;
        //                                            case "Non-Teaching":
        //                                                var nonstaffdet = db.Staffs.Where(m => m.StaffId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
        //                                                break;
        //                                            case "Guardian":
        //                                                var guardiandet = db.Guardians.Where(m => m.GuardianId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
        //                                                break;
        //                                            case "Admin":
        //                                                postedby = user.UserName;
        //                                                break;
        //                                        }
        //                                    }
        //                                    AnouncementsEvent.PostedBy = postedby;
        //                                    DashBoardNew.Add(AnouncementsEvent);
        //                                }
        //                            }
        //                        }
        //                        if (listitemtype == "Holiday")
        //                        {
        //                            if (allHolidays.Count > 0)
        //                            {
        //                                foreach (var item in allHolidays)
        //                                {
        //                                    var HolidaysEvent = new DashBoardNew();
        //                                    HolidaysEvent.Type = "Holiday";
        //                                    HolidaysEvent.Date = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
        //                                    HolidaysEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
        //                                    HolidaysEvent.Title = item.EventTitle.Replace("<", "&lt;");
        //                                    HolidaysEvent.PostedOn = "";
        //                                    if (item.PostedOn != null)
        //                                    HolidaysEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
        //                                    var usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();
        //                                    if (usedcontact != null)
        //                                    {
        //                                        var contacttype = usedcontact.ContactType.ContactType1;
        //                                        switch (contacttype.ToLower())
        //                                        {
        //                                            case "Student":
        //                                                var studentdet = db.Students.Where(m => m.StudentId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
        //                                                break;
        //                                            case "Teacher":
        //                                                var staffdet = db.Staffs.Where(m => m.StaffId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
        //                                                break;
        //                                            case "Non-Teaching":
        //                                                var nonstaffdet = db.Staffs.Where(m => m.StaffId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
        //                                                break;
        //                                            case "Guardian":
        //                                                var guardiandet = db.Guardians.Where(m => m.GuardianId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
        //                                                break;
        //                                            case "Admin":
        //                                                postedby = user.UserName;
        //                                                break;
        //                                        }
        //                                    }
        //                                    HolidaysEvent.PostedBy = postedby;
        //                                    DashBoardNew.Add(HolidaysEvent);
        //                                }
        //                            }
        //                        }
        //                        if (listitemtype == "Vacation")
        //                        {
        //                            if (allVacations.Count > 0)
        //                            {
        //                                foreach (var item in allVacations)
        //                                {
        //                                    var VacationsEvent = new DashBoardNew();
        //                                    VacationsEvent.Type = "Vacation";
        //                                    var StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
        //                                    var EndDate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : null;
        //                                    if (!string.IsNullOrEmpty(EndDate))
        //                                        VacationsEvent.Date = StartDate + '-' + EndDate;
        //                                    else
        //                                        VacationsEvent.Date = StartDate;
        //                                    VacationsEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
        //                                    VacationsEvent.Title = item.EventTitle.Replace("<", "&lt;");
        //                                    VacationsEvent.PostedOn = "";
        //                                    if (item.PostedOn != null)
        //                                    VacationsEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
        //                                    var usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();
        //                                    if (usedcontact != null)
        //                                    {
        //                                        var contacttype = usedcontact.ContactType.ContactType1;
        //                                        switch (contacttype.ToLower())
        //                                        {
        //                                            case "Student":
        //                                                var studentdet = db.Students.Where(m => m.StudentId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
        //                                                break;
        //                                            case "Teacher":
        //                                                var staffdet = db.Staffs.Where(m => m.StaffId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
        //                                                break;
        //                                            case "Non-Teaching":
        //                                                var nonstaffdet = db.Staffs.Where(m => m.StaffId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
        //                                                break;
        //                                            case "Guardian":
        //                                                var guardiandet = db.Guardians.Where(m => m.GuardianId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
        //                                                break;
        //                                            case "Admin":
        //                                                postedby = user.UserName;
        //                                                break;
        //                                        }
        //                                    }
        //                                    VacationsEvent.PostedBy = postedby;
        //                                    DashBoardNew.Add(VacationsEvent);
        //                                }
        //                            }
        //                        }
        //                        if (listitemtype == "Activity")
        //                        {
        //                            if (allActivity.Count > 0)
        //                            {
        //                                var allclasses = db.ClassMasters.ToList();

        //                                foreach (var item in allActivity)
        //                                {
        //                                    var ActivityEvent = new DashBoardNew();
        //                                    ActivityEvent.Type = "Activity";

        //                                    var StartDate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : null;
        //                                    var EndDate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : null;
        //                                    if (!string.IsNullOrEmpty(EndDate))
        //                                        ActivityEvent.Date = StartDate + '-' + EndDate;
        //                                    else
        //                                        ActivityEvent.Date = StartDate;

        //                                    //ActivityEvent.StartTime = item.StartTime.HasValue ? Convert.ToString(item.StartTime) : null;
        //                                    //ActivityEvent.EndTime = item.EndTime.HasValue ? Convert.ToString(item.EndTime) : null;


        //                                    ActivityEvent.Title = item.EventTitle.Replace("<", "&lt;");
        //                                    ActivityEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
        //                                    //ActivityEvent.Venue = item.Venue;

        //                                    //if (item.EventDetails.Count > 0)
        //                                    //{
        //                                    //    var eventClasses = "";
        //                                    //    var classids = item.EventDetails.Select(e => e.ClassId).ToArray();
        //                                    //    eventClasses = String.Join(",", allclasses.Where(c => classids.Contains(c.ClassId)).Select(c => c.Class).ToList());

        //                                    //    //if (studentclassid > 0 && classids.Contains(studentclassid))
        //                                    //    //    eventClasses = _ICatalogMasterService.GetClassMasterById(studentclassid).Class;

        //                                    //    ActivityEvent.Class = eventClasses;
        //                                    //}
        //                                    //else
        //                                    //    ActivityEvent.Class = "All Classes";
        //                                    ActivityEvent.PostedOn = "";
        //                                    if (item.PostedOn != null)
        //                                    ActivityEvent.PostedOn = ((DateTime)item.PostedOn).ToString("dd/MM/yyyy HH:mm:ss");
        //                                    var usedcontact = db.SchoolUsers.Where(s => s.UserId == (int)item.PostedBy).FirstOrDefault();
        //                                    if (usedcontact != null)
        //                                    {
        //                                        var contacttype = usedcontact.ContactType.ContactType1;
        //                                        switch (contacttype.ToLower())
        //                                        {
        //                                            case "Student":
        //                                                var studentdet = db.Students.Where(m => m.StudentId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = studentdet != null ? !string.IsNullOrEmpty(studentdet.LName) ? studentdet.FName + " " + studentdet.LName : studentdet.FName : user.UserName;
        //                                                break;
        //                                            case "Teacher":
        //                                                var staffdet = db.Staffs.Where(m => m.StaffId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = staffdet != null ? !string.IsNullOrEmpty(staffdet.LName) ? staffdet.FName + " " + staffdet.LName : staffdet.FName : user.UserName;
        //                                                break;
        //                                            case "Non-Teaching":
        //                                                var nonstaffdet = db.Staffs.Where(m => m.StaffId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = nonstaffdet != null ? !string.IsNullOrEmpty(nonstaffdet.LName) ? nonstaffdet.FName + " " + nonstaffdet.LName : nonstaffdet.FName : user.UserName;
        //                                                break;
        //                                            case "Guardian":
        //                                                var guardiandet = db.Guardians.Where(m => m.GuardianId == (int)user.UserContactId).FirstOrDefault();
        //                                                postedby = guardiandet != null ? !string.IsNullOrEmpty(guardiandet.LastName) ? guardiandet.FirstName + " " + guardiandet.LastName : guardiandet.FirstName : user.UserName;
        //                                                break;
        //                                            case "Admin":
        //                                                postedby = user.UserName;
        //                                                break;
        //                                        }
        //                                    }
        //                                    ActivityEvent.PostedBy = postedby;
        //                                    DashBoardNew.Add(ActivityEvent);
        //                                }
        //                            }
        //                        }
        //                        if (listitemtype == "Exam")
        //                        {
        //                            //exams
        //                            var Exams = db.ExamDateSheetDetails.Where(m => m.StandardId == studentstandardid).ToList();
        //                            var ExamDetail = Exams.GroupBy(p => p.StandardId).Select(m => m.First()).ToList();

        //                            var datesheetAcctoPublisheddate = db.ExamPublishDetails.ToList();
        //                            if (datesheetAcctoPublisheddate.Count > 0)
        //                            {
        //                                datesheetAcctoPublisheddate = datesheetAcctoPublisheddate.Where(m => m.ExamPublishDate <= DateTime.Now.Date).ToList();
        //                                Exams = Exams.Where(m => (datesheetAcctoPublisheddate.Select(n => n.ExamDateSheetId).ToArray()).Contains((int)m.ExamDateSheetId)).ToList();
        //                                Exams = Exams.Where(p => p.ExamDate >= DateTime.Now.Date).OrderBy(e => e.ExamDate).ToList();
        //                                if (Exams.Count > 0)
        //                                {
        //                                    foreach (var item in Exams)
        //                                    {
        //                                        var standard = db.StandardMasters.Where(m => m.StandardId == (int)item.StandardId).FirstOrDefault();
        //                                        var subject = db.Subjects.Where(m => m.SubjectId == (int)item.SubjectId).FirstOrDefault();
        //                                        var timeSlot = item.ExamTimeSlot.ExamTimeSlot1;

        //                                        if (standard != null && subject != null)
        //                                            DashBoardNew.Add(new DashBoardNew { Title = standard.Standard + "/" + subject.Subject1, Description = timeSlot, Date = ConvertDate((DateTime)item.ExamDate), Type = "Exam" });
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        if (listitemtype == "Fee")
        //                        {
        //                            //Fee
        //                            if (user.ContactType.ContactType1.ToLower().Contains("student"))
        //                            {
        //                                var Fees = new DashBoardNew();
        //                                Fees.PostedBy = "";
        //                                Fees.PostedOn = "";
        //                                var FeeReceipts = db.FeeReceipts.Where(m => m.StudentId == user.UserContactId).OrderByDescending(n => n.ReceiptDate).FirstOrDefault();
        //                                if (FeeReceipts != null)
        //                                {

        //                                    // Fee Receipt Detail
        //                                    // Calculate Amount
        //                                    var paidamount = FeeReceipts.PaidAmount;
        //                                    // get fee receipt detail
        //                                    var totalfeeamt = FeeReceipts.FeeReceiptDetails.Where(f => f.GroupFeeHeadId != null).Select(f => f.Amount).Sum();
        //                                    var totalfeeconsession = FeeReceipts.FeeReceiptDetails.Where(f => f.ConsessionId != null).Select(f => f.Amount).Sum();
        //                                    var payableamt = totalfeeamt - totalfeeconsession;

        //                                    var PendingFee = (paidamount - payableamt);
        //                                    var PaidFee = payableamt.ToString();

        //                                    if (PendingFee > 0)
        //                                    {

        //                                        Fees.Type = "Fee";
        //                                        Fees.Description = PendingFee + " Fee Pending";
        //                                        Fees.Title = "Fee";
        //                                        Fees.Date = "";
        //                                    }
        //                                    else
        //                                    {
        //                                        Fees.Type = "Fee";
        //                                        Fees.Description = PaidFee + "Fee Paid on" + FeeReceipts.ReceiptDate;
        //                                        Fees.Title = "Fee";
        //                                        Fees.Date = "";
        //                                    }
        //                                    DashBoardNew.Add(Fees);
        //                                }


        //                            }
        //                            if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
        //                            {
        //                                var Fees = new DashBoardNew();
        //                                Fees.PostedBy = "";
        //                                Fees.PostedOn = "";
        //                                var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
        //                                var FeeReceipts = db.FeeReceipts.Where(m => m.StudentId == studentid).OrderByDescending(n => n.ReceiptDate).FirstOrDefault();
        //                                if (FeeReceipts != null)
        //                                {

        //                                    // Fee Receipt Detail
        //                                    // Calculate Amount
        //                                    var paidamount = FeeReceipts.PaidAmount;
        //                                    // get fee receipt detail
        //                                    var totalfeeamt = FeeReceipts.FeeReceiptDetails.Where(f => f.GroupFeeHeadId != null).Select(f => f.Amount).Sum();
        //                                    var totalfeeconsession = FeeReceipts.FeeReceiptDetails.Where(f => f.ConsessionId != null).Select(f => f.Amount).Sum();
        //                                    var payableamt = totalfeeamt - totalfeeconsession;

        //                                    var PendingFee = (paidamount - payableamt);
        //                                    var PaidFee = payableamt.ToString();

        //                                    if (PendingFee > 0)
        //                                    {

        //                                        Fees.Type = "Fee";
        //                                        Fees.Description = PendingFee + " Fee Pending";
        //                                        Fees.Title = "Fee";
        //                                        Fees.Date = "";
        //                                    }
        //                                    else
        //                                    {
        //                                        Fees.Type = "Fee";
        //                                        Fees.Description = PaidFee + "Fee Paid on" + FeeReceipts.ReceiptDate;
        //                                        Fees.Title = "Fee";
        //                                        Fees.Date = "";
        //                                    }
        //                                    DashBoardNew.Add(Fees);
        //                                }

        //                            }
        //                        }


        //                        if (listitemtype.ToLower().Contains("homework") || listitemtype.ToLower().Contains("assignment"))
        //                        {
        //                            //HW
        //                            var homeworklist = new List<HW>();
        //                            var seldate = DateTime.Now;
        //                            if (user.ContactType.ContactType1.ToLower() == "student")
        //                            {
        //                                var classId = 0;
        //                                //get student class id 
        //                                var studentClass = db.StudentClasses.Where(m => m.StudentId == user.UserContactId && m.SessionId == currentSession.SessionId).FirstOrDefault();
        //                                if (studentClass != null)
        //                                    classId = (int)studentClass.ClassId;

        //                                //get classsubjectid
        //                                var classsubjectidarray = db.ClassSubjects.Where(m => m.ClassId == classId && m.IsActive == true).Select(n => n.ClassSubjectId).ToArray();
        //                                //get homework
        //                                homeworklist = db.HWs.Where(m => classsubjectidarray.Contains((int)m.ClassSubjectId) && m.HWType.HWType1.ToLower().Contains(listitemtype.ToLower())).OrderByDescending(m => m.Date).Take(3).ToList();

        //                            }
        //                            else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
        //                            {
        //                                var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
        //                                var classId = 0;
        //                                //get student class id 
        //                                var studentClass = db.StudentClasses.Where(m => m.StudentId == studentid && m.SessionId == currentSession.SessionId).FirstOrDefault();
        //                                if (studentClass != null)
        //                                    classId = (int)studentClass.ClassId;

        //                                var classsubjectidarray = db.ClassSubjects.Where(m => m.ClassId == classId && m.IsActive == true).Select(n => n.ClassSubjectId).ToArray();
        //                                //get homework
        //                                homeworklist = db.HWs.Where(m => classsubjectidarray.Contains((int)m.ClassSubjectId) && m.HWType.HWType1.ToLower().Contains(listitemtype.ToLower())).OrderByDescending(m => m.Date).Take(3).ToList();
        //                            }
        //                            else if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
        //                            {
        //                                homeworklist = db.HWs.Where(m => m.StaffId == user.UserContactId && m.HWType.HWType1.ToLower().Contains(listitemtype.ToLower())).OrderByDescending(m => m.Date).Take(3).ToList();
        //                            }
        //                            IList<HomeWorkModel> homeworllistrecords = new List<HomeWorkModel>();
        //                            var homework = new DashBoardNew();
        //                            homework.PostedBy = "";
        //                            homework.PostedOn = "";
        //                            var homeworkdetails = new HomeWorkDetails();

        //                            foreach (var hw in homeworklist)
        //                            {
        //                                homework = new DashBoardNew();
        //                                if (hw.ClassSubject != null)
        //                                {
        //                                    if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
        //                                    {
        //                                        homework.Description = hw.ClassSubject.ClassMaster.Class;
        //                                    }
        //                                    homework.Title = hw.ClassSubject.Subject.Subject1;
        //                                }
        //                                homework.Date = ConvertDate((DateTime)hw.Date);
        //                                homework.Type = hw.HWType.HWType1;
        //                                if (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower().Contains("guardian"))
        //                                {
        //                                    homework.Description = hw.Staff.LName != null ? hw.Staff.FName + " " + hw.Staff.LName : hw.Staff.FName;
        //                                }
        //                                DashBoardNew.Add(homework);
        //                            }

        //                        }

        //                    }
        //                    //Attendance
        //                    //attendance
        //                    // Entrance Class
        //                    var Curentdate = DateTime.UtcNow.Date;
        //                    // Sundays count
        //                    //int Sundaycount = 0;
        //                    //var sundayslist = _IMainService.GetSundayCountBetweenDates(ref Sundaycount, currentSession.StartDate.Value, Curentdate);

        //                    //// get holidays
        //                    //var holidayslist = db.Events.Where(e => e.EventType.EventType1 == "Holidays" && e.StartDate >= currentSession.StartDate
        //                    //    && e.StartDate <= Curentdate && e.IsActive == true && e.IsDeleted != true).Select(e => (DateTime)e.StartDate).ToList();

        //                    //// combine both list and get distinct
        //                    //var alldatelist = sundayslist.Concat(holidayslist).Distinct().ToList();
        //                    // calculate attendance %
        //                    TimeSpan diff = Curentdate - currentSession.StartDate.Value;
        //                    int totaldays = diff.Days; //-alldatelist.Count;
        //                    var Attendance = "";
        //                    if (user.ContactType.ContactType1.ToLower().Contains("teacher"))
        //                    {
        //                        // get attendance by student
        //                        var staffattendance = db.StaffAttendances.Where(a => a.StaffId == user.UserContactId && a.StaffAttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
        //                        Attendance = staffattendance.Count + "/" + totaldays;
        //                    }
        //                    else if (user.ContactType.ContactType1.ToLower().Contains("student"))
        //                    {
        //                        //Attendance
        //                        var staffattendance = db.Attendances.Where(a => a.StudentId == user.UserContactId && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
        //                        Attendance = staffattendance.Count + "/" + totaldays;
        //                    }
        //                    else if (user.ContactType.ContactType1.ToLower().Contains("guardian"))
        //                    {
        //                        var studentid = db.Guardians.Where(m => m.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
        //                        //Attendance
        //                        var staffattendance = db.Attendances.Where(a => a.StudentId == studentid && a.AttendanceStatu.AttendanceAbbr == "P" && a.AttendanceDate >= currentSession.StartDate && a.AttendanceDate <= Curentdate).ToList();
        //                        Attendance = staffattendance.Count + "/" + totaldays;
        //                    }

        //                    return Json(new
        //                    {
        //                        status = "success",
        //                        Dashboard = DashBoardNew,
        //                        Attendance = Attendance,
        //                    }, JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //            else
        //            {
        //                return Json(new
        //                {
        //                    status = "failed",
        //                    message = "User not found"
        //                }, JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        return Json(new
        //        {
        //            status = "failed",
        //            message = "Invalid Token"
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new
        //        {
        //            status = "failed",
        //            message = ex.Message
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        [HttpPost]
        public ActionResult HomeWork(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";
            string[] requiredlist = new string[] { };
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);
                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            IList<HomeWorkTypeModel> HomeWorkType = new List<HomeWorkTypeModel>();
                            IList<PeriodModel> Period = new List<PeriodModel>();
                            //var issundayenabled = false;
                            //string currentTimetableStartDate = "";
                            //string currentTimeTableEndDate = "";
                            var HomeWorkTypeModel = new HomeWorkTypeModel();
                            var HWType = db.HWTypes.ToList();
                            foreach (var hwt in HWType)
                            {
                                HomeWorkTypeModel = new HomeWorkTypeModel();
                                HomeWorkTypeModel.HomeWorkType = hwt.HWType1;
                                HomeWorkType.Add(HomeWorkTypeModel);
                            }

                            var currentSession = GetCurrentSession(dbdetail.DBName);
                            var currentSessionStartDate = ConvertDate((DateTime)currentSession.StartDate);

                            return Json(new
                            {
                                status = "success",
                                HomeWorkType = HomeWorkType,
                                SessionStartDate = currentSessionStartDate,
                                //issundayenabled = issundayenabled,
                                //currentTimetableStartDate = currentTimetableStartDate,
                                //currentTimeTableEndDate = currentTimeTableEndDate,
                                //Period = Period,
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult classSubjects(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";
            DateTime selecteddate = DateTime.Now;
            string[] requiredlist = new string[] { };
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["selecteddate"] != null)
                selecteddate = Convert.ToDateTime(FormatDate(form["selecteddate"].Trim()));

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["selecteddate"] != null)
                selecteddate = Convert.ToDateTime(FormatDate(Request.Headers["selecteddate"]));

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);
                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            var selectedDay = selecteddate.DayOfWeek;
                            var day = db.DayMasters.Where(m => m.DayName == selectedDay.ToString()).FirstOrDefault();
                            var timetables = db.TimeTables.ToList();
                            var timetable = timetables.Where(t => t.EffectiveFrom <= selecteddate.Date && t.EffectiveTill >= selecteddate.Date).FirstOrDefault();
                            if (timetable != null)
                            {
                                // get all class period detail by time table andt  teacher
                                var classperioddetails = db.ClassPeriodDetails.Where(m => m.TeacherId == user.UserContactId && m.TimeTableId == timetable.TimeTableId).ToList();
                                if (classperioddetails.Count > 0)
                                {
                                    var classperiodday = db.ClassPeriodDays.Where(m => m.DayId == day.DayId).ToList();

                                    var ClassPeriodDetailIdarray = classperiodday.Count > 0 ? classperiodday.Select(m => (int)m.ClassPeriodDetailId).ToArray() : new int[] { };

                                    ClassPeriodDetailIdarray = classperioddetails.Where(m => ClassPeriodDetailIdarray.Contains((int)m.ClassPeriodDetailId)).Select(m => m.ClassPeriodDetailId).ToArray();

                                    var adjustmentclasssprddetails = db.TimeTableAdjustments.Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate == selecteddate).Select(m => (int)m.ClassPeriodDetailId).ToArray();
                                    var finalClassPeriodDetailIdarray = ClassPeriodDetailIdarray.Union(ClassPeriodDetailIdarray).Distinct().ToArray();
                                    if (finalClassPeriodDetailIdarray.Length > 0)
                                    {
                                        classperioddetails = classperioddetails.Where(m => finalClassPeriodDetailIdarray.Contains(m.ClassPeriodDetailId)).ToList();
                                        var classidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassId).Distinct().ToList();
                                        var classsubjectidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassSubjectId).ToList();
                                        if (classidlist.Count > 0 && classsubjectidlist.Count > 0)
                                        {
                                            IList<classlist> classlist = new List<classlist>();
                                            classlist classmodel = new classlist();
                                            IList<SubjectList> SubjectList = new List<SubjectList>();
                                            SubjectList Subjectmodel = new SubjectList();

                                            foreach (var classid in classidlist)
                                            {
                                                SubjectList = new List<SubjectList>();
                                                classmodel = new classlist();
                                                classmodel.Class = db.ClassMasters.Where(m => m.ClassId == classid).FirstOrDefault().Class;
                                                foreach (var classsubjectid in classsubjectidlist)
                                                {
                                                    Subjectmodel = new SubjectList();
                                                    var classsubject = db.ClassSubjects.Where(m => m.ClassId == classid && m.ClassSubjectId == classsubjectid && m.IsActive == true).FirstOrDefault();
                                                    if (classsubject != null)
                                                    {
                                                        if (classsubject.SkillId == null)
                                                        {
                                                            Subjectmodel.Subject = classsubject.Subject.Subject1;
                                                            Subjectmodel.ClassSubjectId = _ICustomEncryption.base64e((classsubjectid.ToString()));
                                                        }
                                                        else
                                                        {
                                                            Subjectmodel.Subject = db.SubjectSkills.Where(m => m.SubjectId == classsubject.SubjectId).FirstOrDefault().SubjectSkill1;
                                                            Subjectmodel.ClassSubjectId = _ICustomEncryption.base64e((classsubjectid.ToString()));
                                                        }
                                                        SubjectList.Add(Subjectmodel);
                                                    }
                                                }
                                                classmodel.subjectlist = SubjectList;
                                                classlist.Add(classmodel);
                                            }
                                            return Json(new
                                            {
                                                status = "success",
                                                classlist = classlist,
                                            }, JsonRequestBehavior.AllowGet);

                                        }
                                    }
                                    else
                                        return Json(new
                                        {
                                            status = "failed",
                                            msg = "Time Table does not Exists"
                                        }, JsonRequestBehavior.AllowGet);

                                    //if (classperiodDetail != null)
                                    //{
                                    //    var classperiodday = db.ClassPeriodDays.Where(m => m.ClassPeriodDetailId == classperiodDetail.ClassPeriodDetailId && m.DayId == day.DayId).FirstOrDefault();
                                    //    if (classperiodday != null)
                                    //    {
                                    //        // get class and subject name

                                    //        var ClassSubject = classperiodDetail.ClassPeriod.ClassSubject;
                                    //        if (ClassSubject != null)
                                    //            PeriodModel.ClassSubjectId = _ICustomEncryption.base64e((ClassSubject.ClassSubjectId.ToString()));
                                    //    }
                                    //}
                                }
                            }
                            else
                                return Json(new
                                {
                                    status = "failed",
                                    msg = "Time Table does not Exists"
                                }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
                //}

                //return Json(new
                //{
                //    status = "failed",
                //    message = "Invalid Token"
                //}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveHomeWork(FormCollection form)
        {
            string domain = "", UserName = "", Token = "", encClassSubjectId = "", HWType = "", Description = "", Remarks = "";
            string[] requiredlist = new string[] { };
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["ClassSubjectId"] != null)
                encClassSubjectId = form["ClassSubjectId"].Trim();
            if (form["HWType"] != null)
                HWType = form["HWType"].Trim();
            if (form["Description"] != null)
                Description = form["Description"].Trim();
            if (form["Remarks"] != null)
                Remarks = form["Remarks"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["ClassSubjectId"] != null)
                encClassSubjectId = Convert.ToString(Request.Headers["ClassSubjectId"]);
            if (Request.Headers["HWType"] != null)
                HWType = Convert.ToString(Request.Headers["HWType"]);
            if (Request.Headers["Description"] != null)
                Description = Convert.ToString(Request.Headers["Description"]);
            if (Request.Headers["Remarks"] != null)
                Remarks = Convert.ToString(Request.Headers["Remarks"]);


            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            var hwid = 0;
                            //get classsubjectid
                            var ClassSubjectId = 0;
                            if (!string.IsNullOrEmpty(encClassSubjectId))
                                int.TryParse(_ICustomEncryption.base64d(encClassSubjectId), out ClassSubjectId);
                            //get homeworktypeid
                            var typeid = 0;
                            var HWTyperecord = db.HWTypes.Where(m => m.HWType1.ToLower() == HWType.ToLower()).FirstOrDefault();
                            if (HWTyperecord != null)
                                typeid = HWTyperecord.HWTypeId;
                            var date = DateTime.UtcNow.Date;
                            //check if record already Exists
                            var existinghomwwork = db.HWs.Where(m => m.Date == date && m.HWTypeId == typeid && m.StaffId == user.UserContactId && m.ClassSubjectId == ClassSubjectId).ToList();
                            if (existinghomwwork.Count > 0)
                            {
                                hwid = existinghomwwork.FirstOrDefault().HWId;
                            }
                            else
                            {
                                //Save HW
                                HW hw = new HW();
                                hw.HWTypeId = typeid;
                                hw.ClassSubjectId = ClassSubjectId;
                                hw.StaffId = user.UserContactId;
                                hw.Date = DateTime.UtcNow;
                                db.HWs.Add(hw);
                                db.SaveChanges();

                                hwid = hw.HWId;
                            }

                            //Save HW Details
                            HWDetail HWDetail = new HWDetail();
                            HWDetail.HWId = hwid;
                            HWDetail.Description = Description;
                            HWDetail.Remarks = Remarks;
                            db.HWDetails.Add(HWDetail);
                            db.SaveChanges();

                            if (Request.Files.Count > 0)
                            {
                                string path = String.Empty;
                                try
                                {
                                    //  Get all files from Request object  
                                    HttpFileCollectionBase files = Request.Files;
                                    for (int i = 0; i < files.Count; i++)
                                    {

                                        //file
                                        HttpPostedFileBase file = files[i];
                                        // get Filename
                                        string fileName = "";

                                        var noofimages = db.HWDetails.Where(m => m.HWId == hwid && m.HWDetailId == HWDetail.HWDetailId && m.FileName != null).ToList();

                                        fileName = hwid + "_" + HWDetail.HWDetailId + "_" + HWType + Path.GetExtension(file.FileName).ToLower();

                                        var updatehwdetail = db.HWDetails.Where(m => m.HWDetailId == HWDetail.HWDetailId).FirstOrDefault();
                                        updatehwdetail.FileName = fileName;
                                        db.Entry(updatehwdetail).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                        //Is the file too big to upload?
                                        int fileSize = file.ContentLength;
                                        if (fileSize > (maxFileSize * 1024 * 5))
                                        {
                                            return Json(new
                                            {
                                                status = "error",
                                                msg = "Filesize of image is too large. Maximum file size permitted is " + maxFileSize + "KB"
                                            });
                                        }

                                        //check that the file is of the permitted file type
                                        string fileExtension = Path.GetExtension(fileName).ToLower();

                                        string[] acceptedFileTypes = new string[5];
                                        acceptedFileTypes[0] = ".jpg";
                                        acceptedFileTypes[1] = ".jpeg";
                                        acceptedFileTypes[2] = ".png";
                                        acceptedFileTypes[3] = ".pdf";
                                        acceptedFileTypes[4] = ".doc";


                                        bool acceptFile = false;
                                        //should we accept the file?
                                        for (int j = 0; j < acceptedFileTypes.Count(); j++)
                                        {
                                            if (fileExtension == acceptedFileTypes[j])
                                            {
                                                //accept the file, yay!
                                                acceptFile = true;
                                                break;
                                            }
                                        }

                                        if (!acceptFile)
                                            return Json(new
                                            {
                                                status = "error",
                                                msg = "The file you are trying to upload is not a permitted file type!"
                                            });

                                        // check directory exist or not
                                        var schoolid = dbdetail.DBId;

                                        var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/HomeWork/"));
                                        if (!Directory.Exists(directory))
                                            Directory.CreateDirectory(directory);

                                        path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/HomeWork/"), fileName);

                                        if (System.IO.File.Exists(path))
                                            System.IO.File.Delete(path);

                                        file.SaveAs(path);
                                    }
                                    // Returns message that successfully uploaded  
                                    return Json(new
                                    {
                                        status = "success",
                                        msg = "File Uploaded Successfully!"
                                    });
                                }
                                catch (Exception ex)
                                {
                                    return Json(new
                                    {
                                        status = "error",
                                        msg = "Error occurred. Error details: " + ex.Message
                                    });
                                }
                            }
                            return Json(new
                            {
                                status = "success",
                                HWid = _ICustomEncryption.base64e((hwid.ToString())),
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }



                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult HomeworkView(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";
            string[] requiredlist = new string[] { };
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();


            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);


            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            //var currentmonth = DateTime.Now.Month;

                            //var homeworkdates = db.HWs.Where(m => m.StaffId==user.UserContactId && m.Date.Value.Month == currentmonth).Select(m=>m.Date.ToString()).ToList();

                            var currentSession = GetCurrentSession(dbdetail.DBName);
                            var currentSessionStartDate = ConvertDate((DateTime)currentSession.StartDate);
                            return Json(new
                            {
                                status = "success",
                                currentSessionStartDate = currentSessionStartDate,
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult nextmonthHomeworkView(FormCollection form)
        {
            string domain = "", UserName = "", Token = "", month = "";
            string[] requiredlist = new string[] { };
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["month"] != null)
                month = form["month"].Trim();


            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["month"] != null)
                month = Convert.ToString(Request.Headers["month"]);


            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token) || string.IsNullOrEmpty(month))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            var currentmonth = DateTime.ParseExact(month, "MMMM", CultureInfo.CurrentCulture).Month;
                            var homeworkdates = db.HWs.Where(m => m.StaffId == user.UserContactId && m.Date.Value.Month == currentmonth).Select(m => m.Date.ToString()).ToList();

                            return Json(new
                            {
                                status = "success",
                                homeworkdates = homeworkdates,
                            }, JsonRequestBehavior.AllowGet);

                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult HomeWorkList(FormCollection form)
        {
            string domain = "", UserName = "", Token = "", selectedDate = "";
            string[] requiredlist = new string[] { };
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (!string.IsNullOrEmpty(form["selectedDate"]))
                selectedDate = FormatDate(form["selectedDate"].Trim());


            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (!string.IsNullOrEmpty(Request.Headers["selectedDate"]))
                selectedDate = FormatDate(Convert.ToString(Request.Headers["selectedDate"]));


            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            var currentSession = GetCurrentSession(dbdetail.DBName);

                            IList<HW> homeworklist = new List<HW>();
                            HW homeworkdata = new HW();
                            IList<HomeWork> homeworllistrecords = new List<HomeWork>();
                            HomeWork homework = new HomeWork();
                            HomeWorkDetails homeworkdetails = new HomeWorkDetails();
                            // check directory exist or not
                            var schoolid = dbdetail.DBId;

                            DateTime seldate = DateTime.Now;
                            List<DateTime> datearray = new List<DateTime> { };
                            //get datearray
                            if (!String.IsNullOrEmpty(selectedDate))
                            {
                                seldate = Convert.ToDateTime(selectedDate);
                                datearray.Add((DateTime)seldate);
                            }

                            if (user.ContactType.ContactType1.ToLower() == "teacher")
                            {
                                if (string.IsNullOrEmpty(selectedDate))
                                {
                                    //get teachers class
                                    var selectedDay = DateTime.Now.DayOfWeek;
                                    var day = db.DayMasters.Where(m => m.DayName == selectedDay.ToString()).FirstOrDefault();
                                    var timetables = db.TimeTables.ToList();
                                    var timetable = timetables.Where(t => t.EffectiveFrom <= DateTime.Now.Date && t.EffectiveTill >= DateTime.Now.Date).FirstOrDefault();
                                    if (timetable != null)
                                    {
                                        // get all class period detail by time table andt  teacher
                                        var classperioddetails = db.ClassPeriodDetails.Where(m => m.TeacherId == user.UserContactId && m.TimeTableId == timetable.TimeTableId).ToList();
                                        if (classperioddetails.Count > 0)
                                        {
                                            var classperiodday = db.ClassPeriodDays.Where(m => m.DayId == day.DayId).ToList();

                                            var ClassPeriodDetailIdarray = classperiodday.Count > 0 ? classperiodday.Select(m => (int)m.ClassPeriodDetailId).ToArray() : new int[] { };

                                            ClassPeriodDetailIdarray = classperioddetails.Where(m => ClassPeriodDetailIdarray.Contains((int)m.ClassPeriodDetailId)).Select(m => m.ClassPeriodDetailId).ToArray();

                                            var adjustmentclasssprddetails = db.TimeTableAdjustments.Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate == seldate).Select(m => (int)m.ClassPeriodDetailId).ToArray();
                                            var finalClassPeriodDetailIdarray = ClassPeriodDetailIdarray.Union(ClassPeriodDetailIdarray).Distinct().ToArray();
                                            if (finalClassPeriodDetailIdarray.Length > 0)
                                            {
                                                classperioddetails = classperioddetails.Where(m => finalClassPeriodDetailIdarray.Contains(m.ClassPeriodDetailId)).ToList();
                                                var classidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassId).Distinct().ToList();
                                                var classsubjectidsarray = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassSubjectId).Distinct().ToArray();
                                                var lastsevendays = seldate.AddDays(-7);
                                                datearray = db.HWs.Where(m => classsubjectidsarray.Contains((int)m.ClassSubjectId) && m.Date >= lastsevendays).OrderByDescending(m => m.Date).Select(m => (DateTime)m.Date).Distinct().Take(7).ToList();
                                            }
                                        }
                                    }

                                }
                                foreach (var sel_date in datearray)
                                {

                                    homeworklist = db.HWs.Where(m => m.StaffId == user.UserContactId && m.Date == sel_date).ToList();

                                }
                            }
                            else if (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian")
                            {
                                var classId = 0;
                                if (user.ContactType.ContactType1.ToLower() == "guardian")
                                {
                                    var studentId = db.Guardians.Where(p => p.GuardianId == user.UserContactId).FirstOrDefault().StudentId;
                                    if (studentId != null && studentId > 0)
                                    {
                                        var studentClass = db.StudentClasses.Where(m => m.StudentId == studentId && m.SessionId == currentSession.SessionId).FirstOrDefault();
                                        if (studentClass != null)
                                            classId = (int)studentClass.ClassId;
                                    }
                                }
                                else
                                {
                                    //get student class id 
                                    var studentClass = db.StudentClasses.Where(m => m.StudentId == user.UserContactId && m.SessionId == currentSession.SessionId).FirstOrDefault();
                                    if (studentClass != null)
                                        classId = (int)studentClass.ClassId;
                                }




                                var selectedDay = seldate.DayOfWeek;
                                var day = db.DayMasters.Where(m => m.DayName == selectedDay.ToString()).FirstOrDefault();
                                var timetables = db.TimeTables.ToList();
                                var timetable = timetables.Where(t => t.EffectiveFrom <= seldate.Date && t.EffectiveTill >= seldate.Date).FirstOrDefault();
                                if (timetable != null)
                                {
                                    // get all class period detail by time table andt  teacher
                                    var classperioddetails = db.ClassPeriodDetails.Where(m => m.TimeTableId == timetable.TimeTableId).ToList();
                                    if (classperioddetails.Count > 0)
                                    {
                                        var classperiodday = db.ClassPeriodDays.Where(m => m.DayId == day.DayId).ToList();

                                        var ClassPeriodDetailIdarray = classperiodday.Count > 0 ? classperiodday.Select(m => (int)m.ClassPeriodDetailId).ToArray() : new int[] { };

                                        ClassPeriodDetailIdarray = classperioddetails.Where(m => ClassPeriodDetailIdarray.Contains((int)m.ClassPeriodDetailId)).Select(m => m.ClassPeriodDetailId).ToArray();

                                        var adjustmentclasssprddetails = db.TimeTableAdjustments.Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate == seldate).Select(m => (int)m.ClassPeriodDetailId).ToArray();
                                        var finalClassPeriodDetailIdarray = ClassPeriodDetailIdarray.Union(adjustmentclasssprddetails).Distinct().ToArray();
                                        if (finalClassPeriodDetailIdarray.Length > 0)
                                        {
                                            classperioddetails = classperioddetails.Where(m => finalClassPeriodDetailIdarray.Contains(m.ClassPeriodDetailId)).ToList();
                                            // var classidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassId).Distinct().ToList();
                                            classperioddetails = classperioddetails.Where(m => m.ClassPeriod.ClassSubject.ClassId == classId).ToList();
                                            var classsubjectidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassSubjectId).ToList();
                                            if (classsubjectidlist.Count > 0)
                                            {
                                                IList<classlist> classlist = new List<classlist>();
                                                classlist classmodel = new classlist();
                                                IList<SubjectList> SubjectList = new List<SubjectList>();
                                                SubjectList Subjectmodel = new SubjectList();


                                                foreach (var classsubjectid in classsubjectidlist)
                                                {
                                                    Subjectmodel = new SubjectList();
                                                    var classsubject = db.ClassSubjects.Where(m => m.ClassId == classId && m.ClassSubjectId == classsubjectid && m.IsActive == true).FirstOrDefault();
                                                    if (classsubject != null)
                                                    {
                                                        if (classsubject.SkillId == null)
                                                        {
                                                            Subjectmodel.Subject = classsubject.Subject.Subject1;
                                                            Subjectmodel.ClassSubjectId = (classsubjectid.ToString());
                                                        }
                                                        else
                                                        {
                                                            Subjectmodel.Subject = db.SubjectSkills.Where(m => m.SubjectId == classsubject.SubjectId).FirstOrDefault().SubjectSkill1;
                                                            Subjectmodel.ClassSubjectId = (classsubjectid.ToString());
                                                        }
                                                        SubjectList.Add(Subjectmodel);
                                                    }
                                                }
                                                var classsubjectidsarray = SubjectList.Select(m => m.ClassSubjectId).Select(int.Parse).ToArray();

                                                if (string.IsNullOrEmpty(selectedDate))
                                                {
                                                    var lastsevendays = seldate.AddDays(-7);
                                                    datearray = db.HWs.Where(m => classsubjectidsarray.Contains((int)m.ClassSubjectId) && m.Date >= lastsevendays).OrderByDescending(m => m.Date).Select(m => (DateTime)m.Date).Distinct().Take(7).ToList();
                                                }

                                                foreach (var sel_date in datearray)
                                                {

                                                    var home_work = db.HWs.Where(m => m.Date == sel_date.Date && classsubjectidsarray.Contains((int)m.ClassSubjectId)).ToList();
                                                    foreach (var hw in home_work)
                                                    {
                                                        homeworkdata = new HW();
                                                        homeworkdata.ClassSubject = hw.ClassSubject;
                                                        homeworkdata.ClassSubjectId = hw.ClassSubjectId;
                                                        homeworkdata.Date = hw.Date;
                                                        homeworkdata.HWDetails = hw.HWDetails;
                                                        homeworkdata.HWId = hw.HWId;
                                                        homeworkdata.HWType = hw.HWType;
                                                        homeworkdata.HWTypeId = hw.HWTypeId;
                                                        homeworkdata.Staff = hw.Staff;
                                                        homeworkdata.StaffId = hw.StaffId;
                                                        homeworklist.Add(homeworkdata);
                                                    }
                                                }

                                            }
                                        }
                                        else
                                            return Json(new
                                            {
                                                status = "failed",
                                                msg = "Time Table does not Exists"
                                            }, JsonRequestBehavior.AllowGet);
                                    }
                                }

                            }
                            foreach (var hml in homeworklist)
                            {

                                var details = hml.HWDetails;
                                foreach (var item in details)
                                {
                                    homework = new HomeWork();
                                    if (hml.ClassSubject != null)
                                    {
                                        homework.Class = hml.ClassSubject.ClassMaster.Class;
                                        homework.Subject = hml.ClassSubject.Subject.Subject1;
                                        homework.SubjectCode = hml.ClassSubject.Subject.SubjectCode;
                                    }
                                    homework.Date = ConvertDate((DateTime)hml.Date);
                                    homework.Type = hml.HWType.HWType1;
                                    homework.teacher = hml.Staff.LName != null ? hml.Staff.FName + " " + hml.Staff.LName : hml.Staff.FName;

                                    if (item.Description != null)
                                        homework.Description = item.Description;
                                    var fileName = "";
                                    homework.File = "";
                                    if (!string.IsNullOrEmpty(item.FileName))
                                    {
                                        fileName = hml.HWId + "_" + item.HWDetailId + "_" + hml.HWType.HWType1 + Path.GetExtension(item.FileName).ToLower();
                                        homework.File = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + schoolid + "/HomeWork/" + fileName;
                                    }
                                    if (!string.IsNullOrEmpty(item.Remarks))
                                        homework.Remarks = item.Remarks;
                                    //homework.HomeworkDetails.Add(homeworkdetails);
                                    homeworllistrecords.Add(homework);
                                }

                            }
                            return Json(new
                            {
                                status = "success",
                                homework = homeworllistrecords.OrderByDescending(m => m.Date)
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ResetPassword(FormCollection form)
        {
            string domain = "", UserName = "", OldPassword = "", NewPassword = "", ConfirmPassWord = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["NewPassword"] != null)
                NewPassword = form["NewPassword"].Trim();
            if (form["ConfirmPassWord"] != null)
                ConfirmPassWord = form["ConfirmPassWord"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["OldPassword"] != null)
                OldPassword = form["OldPassword"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["NewPassword"] != null)
                NewPassword = Convert.ToString(Request.Headers["NewPassword"]);
            if (Request.Headers["ConfirmPassWord"] != null)
                ConfirmPassWord = Convert.ToString(Request.Headers["ConfirmPassWord"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["OldPassword"] != null)
                OldPassword = Convert.ToString(Request.Headers["OldPassword"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(OldPassword) || string.IsNullOrEmpty(NewPassword) || string.IsNullOrEmpty(ConfirmPassWord) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }
            if (!string.IsNullOrEmpty(NewPassword) || !string.IsNullOrEmpty(ConfirmPassWord))
            {
                if (NewPassword != ConfirmPassWord)
                    return Json(new
                    {
                        status = "failed",
                        message = "NewPassword not match with confirm password"
                    }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        var isuserexist = db.SchoolUsers.ToList();
                        isuserexist = isuserexist.Where(u => u.UserName.ToLower() == UserName.ToLower()).ToList();
                        if (isuserexist.Count == 0)
                            return Json(new
                            {
                                status = "failed",
                                message = "User not exist"
                            }, JsonRequestBehavior.AllowGet);

                        var user = isuserexist.FirstOrDefault();
                        if (_IEncryptionService.EncryptText(OldPassword) == user.Password)
                        {
                            user.Password = _IEncryptionService.EncryptText(NewPassword);
                            db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "OldPassword is incorrect"
                            }, JsonRequestBehavior.AllowGet);
                        }
                        //var mobtype = db.ContactInfoTypes.Where(c => c.ContactInfoType1 == "Mobile").FirstOrDefault();

                        //var contactinfos = mobtype != null ? db.ContactInfoes.Where(c => c.ContactId == user.UserContactId && c.ContactInfoTypeId == mobtype.ContactInfoTypeId).ToList() : db.ContactInfoes.Where(c => c.ContactId == user.UserContactId).ToList();

                        //// send otp on mobile no
                        //if (contactinfos.Count > 0)
                        //{
                        //    var ismobilnoexist = contactinfos.FirstOrDefault();
                        //    _ISMSSender.SendSMS("91" + ismobilnoexist.ContactInfo1, "Your new Password for KSSmart is " + Password);
                        //}
                        return Json(new
                        {
                            status = "success",
                            message = "Password Changed"
                        }, JsonRequestBehavior.AllowGet);

                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "School not found"
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ChangeProfileImage(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";
            int documentid = 0;
            string[] requiredlist = new string[] { };
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["DocumentId"] != null)
                documentid = Convert.ToInt32(form["DocumentId"].Trim());


            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["DocumentId"] != null)
                documentid = Convert.ToInt32(Request.Headers["DocumentId"]);



            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);
                        var schoolid = dbdetail.DBId;
                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            if (Request.Files.Count > 0)
                            {
                                string path = String.Empty;
                                try
                                {
                                    //  Get all files from Request object  
                                    HttpFileCollectionBase files = Request.Files;
                                    var UserImage = "";
                                    string defaultimg = _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + schoolid + "/default.png";
                                    for (int i = 0; i < files.Count; i++)
                                    {

                                        //file
                                        HttpPostedFileBase file = files[i];

                                        //Is the file too big to upload?
                                        int maxfilesize = 100; // default size
                                        var ProfilePicMaxSizeSetting = db.SchoolSettings.Where(m => m.AttributeName == "ProfilePicMaxSize").FirstOrDefault().AttributeValue;
                                        if (ProfilePicMaxSizeSetting != null && !string.IsNullOrEmpty(ProfilePicMaxSizeSetting))
                                            maxfilesize = Convert.ToInt32(ProfilePicMaxSizeSetting);
                                        int fileSize = file.ContentLength;
                                        if (fileSize > (maxFileSize * 1024))
                                        {
                                            return Json(new
                                            {
                                                status = "error",
                                                msg = "Filesize of image is too large. Maximum file size permitted is " + maxFileSize + "KB"
                                            });
                                        }

                                        // get Filename
                                        string fileName = "";


                                        //check that the file is of the permitted file type
                                        string fileExtension = Path.GetExtension(file.FileName).ToLower();

                                        string[] acceptedFileTypes = new string[3];
                                        acceptedFileTypes[0] = ".jpg";
                                        acceptedFileTypes[1] = ".jpeg";
                                        acceptedFileTypes[2] = ".png";

                                        bool acceptFile = false;
                                        //should we accept the file?
                                        for (int j = 0; j < acceptedFileTypes.Count(); j++)
                                        {
                                            if (fileExtension == acceptedFileTypes[j])
                                            {
                                                //accept the file, yay!
                                                acceptFile = true;
                                                break;
                                            }
                                        }

                                        if (!acceptFile)
                                            return Json(new
                                            {
                                                status = "error",
                                                msg = "The file you are trying to upload is not a permitted file type!"
                                            });

                                        if (user.ContactType.ContactType1 == "Student")
                                        {
                                            var doctypeid = db.DocumentTypes.Where(m => m.DocumentType1 == "Image").FirstOrDefault();
                                            var studentid = user.UserContactId;
                                            fileName = studentid + "_image" + Path.GetExtension(file.FileName).ToLower();
                                            if (documentid > 0)
                                            {
                                                var updateAttachedDocuments = db.AttachedDocuments.Where(m => m.DocumentId == documentid).FirstOrDefault();
                                                updateAttachedDocuments.Image = fileName;
                                                db.Entry(updateAttachedDocuments).State = System.Data.Entity.EntityState.Modified;
                                                db.SaveChanges();
                                            }
                                            else
                                            {
                                                var AttachedDocument = new AttachedDocument();
                                                AttachedDocument.Image = fileName;
                                                AttachedDocument.StudentId = studentid;
                                                AttachedDocument.DocumentTypeId = doctypeid.DocumentTypeId;
                                                db.AttachedDocuments.Add(AttachedDocument);
                                                db.SaveChanges();
                                            }
                                            // check directory exist or not


                                            var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/StudentPhotos/"));
                                            if (!Directory.Exists(directory))
                                                Directory.CreateDirectory(directory);

                                            path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/StudentPhotos/"), fileName);

                                            if (System.IO.File.Exists(path))
                                                System.IO.File.Delete(path);

                                            file.SaveAs(path);
                                            var image = db.AttachedDocuments.Where(d => d.DocumentTypeId == doctypeid.DocumentTypeId && d.StudentId == studentid).FirstOrDefault();
                                            UserImage = image != null ? _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + schoolid + "/StudentPhotos/" + image.Image : defaultimg;

                                        }
                                        if (user.ContactType.ContactType1 == "Teacher")
                                        {
                                            var doctypeid = db.StaffDocumentTypes.Where(m => m.DocumentType == "Image").FirstOrDefault();
                                            var teacherid = user.UserContactId;
                                            fileName = teacherid + "_image" + Path.GetExtension(file.FileName).ToLower();
                                            if (documentid > 0)
                                            {
                                                var updateAttachedDocuments = db.StaffDocuments.Where(m => m.DocumentId == documentid).FirstOrDefault();
                                                updateAttachedDocuments.Image = fileName;
                                                db.Entry(updateAttachedDocuments).State = System.Data.Entity.EntityState.Modified;
                                                db.SaveChanges();
                                            }
                                            else
                                            {
                                                var AttachedDocument = new AttachedDocument();
                                                AttachedDocument.Image = fileName;
                                                AttachedDocument.StudentId = teacherid;
                                                AttachedDocument.DocumentTypeId = doctypeid.DocumentTypeId;
                                                db.AttachedDocuments.Add(AttachedDocument);
                                                db.SaveChanges();
                                            }
                                            // check directory exist or not


                                            var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Teacher/Photos/"));
                                            if (!Directory.Exists(directory))
                                                Directory.CreateDirectory(directory);

                                            path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Teacher/Photos/"), fileName);

                                            if (System.IO.File.Exists(path))
                                                System.IO.File.Delete(path);

                                            file.SaveAs(path);
                                            var image = db.AttachedDocuments.Where(d => d.DocumentTypeId == doctypeid.DocumentTypeId && d.StudentId == teacherid).FirstOrDefault();
                                            UserImage = image != null ? _WebHelper.GetStoreHost(false) + "/" + User + "/Images/" + schoolid + "/Teacher/Photos/" + image.Image : defaultimg;

                                        }


                                    }


                                    // Returns message that successfully uploaded  
                                    return Json(new
                                    {
                                        Image = UserImage,
                                        status = "success",
                                        msg = "File Uploaded Successfully!"
                                    });
                                }
                                catch (Exception ex)
                                {
                                    return Json(new
                                    {
                                        status = "error",
                                        msg = "Error occurred. Error details: " + ex.Message
                                    });
                                }
                            }
                            else
                                return Json(new
                                {
                                    status = "success",
                                    msg = "Image Required"
                                });
                        }
                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid Token"
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LeaveApplication(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";
            string[] requiredlist = new string[] { };
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();



            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);



            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            var currentSession = GetCurrentSession(dbdetail.DBName);
                            var staffLeaveAppList = new List<StaffLeaveApplication>();
                            var studentLeaveAppList = new List<StudentLeaveApplication>();

                            if (user.ContactType.ContactType1.ToLower() == "student")
                                studentLeaveAppList = db.StudentLeaveApplications.Where(p => p.AppliedDate >= currentSession.StartDate.Value && p.AppliedDate <= currentSession.EndDate.Value && p.StudentId == user.UserContactId).OrderByDescending(x => x.LeaveFrom).ToList();
                            else if (user.ContactType.ContactType1.ToLower() == "teacher")
                                staffLeaveAppList = db.StaffLeaveApplications.Where(p => p.AppliedDate >= currentSession.StartDate.Value && p.AppliedDate <= currentSession.EndDate.Value && p.StaffId == user.UserContactId).OrderByDescending(x => x.LeaveFrom).ToList();

                            var leaveModel = new LeaveModel();
                            List<LeaveModel> leaveList = new List<LeaveModel>();

                            if (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian")
                            {
                                leaveList = studentLeaveAppList.Select(x =>
                               {
                                   leaveModel = new LeaveModel();
                                   leaveModel.LeaveType = x.AttendanceStatu.AttendanceStatus;
                                   leaveModel.LeaveId = x.StudentLeaveApplicationId;
                                   leaveModel.LeaveFrom = ConvertDate((DateTime)x.LeaveFrom);
                                   leaveModel.LeaveTill = ConvertDate((DateTime)x.LeaveTill); ;
                                   leaveModel.ReasonDesc = x.Reason;
                                   if (x.Reason.Length > 10)
                                       leaveModel.Reason = x.Reason.Substring(0, 9);
                                   else
                                       leaveModel.Reason = x.Reason;


                                   if (x.ApprovedBy != null)
                                   {
                                       if ((bool)x.IsApproved)
                                           leaveModel.Status = "Approved";
                                       else
                                           leaveModel.Status = "Disapproved";
                                   }
                                   else
                                       leaveModel.Status = "Approval Pending";

                                   return leaveModel;
                               }).ToList();
                            };
                            if (user.ContactType.ContactType1.ToLower() == "admin" || user.ContactType.ContactType1.ToLower() == "teacher")
                            {
                                leaveList = staffLeaveAppList.Select(x =>
                               {
                                   leaveModel = new LeaveModel();
                                   leaveModel.LeaveType = x.StaffAttendanceStatu.AttendanceStatus;
                                   leaveModel.LeaveId = x.StaffLeaveApplicationId;
                                   leaveModel.LeaveFrom = ConvertDate((DateTime)x.LeaveFrom); ;
                                   leaveModel.LeaveTill = ConvertDate((DateTime)x.LeaveTill); ;
                                   leaveModel.ReasonDesc = x.Reason;
                                   if (x.Reason.Length > 10)
                                       leaveModel.Reason = x.Reason.Substring(0, 9);
                                   else
                                       leaveModel.Reason = x.Reason;
                                   if (x.ApprovedBy != null)
                                   {
                                       if ((bool)x.IsApproved)
                                           leaveModel.Status = "Approved";
                                       else
                                           leaveModel.Status = "Disapproved";
                                   }
                                   else
                                       leaveModel.Status = "Approval Pending";

                                   return leaveModel;
                               }).ToList();

                            };

                            return Json(new
                            {
                                status = "success",
                                leaveList = leaveList
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "DBName not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveLeaveApplication(FormCollection form)
        {
            string domain = "", UserName = "", Token = "", LeaveType = "", Reason = "", LeaveFrom = "", LeaveTill = "";
            int LeaveApplicationId = 0;
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["LeaveType"] != null)
                LeaveType = form["LeaveType"].Trim();
            if (form["Reason"] != null)
                Reason = form["Reason"].Trim();
            if (!String.IsNullOrEmpty(form["LeaveFrom"]))
                LeaveFrom = (form["LeaveFrom"].Trim());
            if (!String.IsNullOrEmpty(form["LeaveTill"]))
                LeaveTill = (form["LeaveTill"].Trim());
            if (form["LeaveApplicationId"] != null)
                LeaveApplicationId = Convert.ToInt32(form["LeaveApplicationId"].Trim());

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["LeaveType"] != null)
                LeaveType = Convert.ToString(Request.Headers["LeaveType"]);
            if (Request.Headers["Reason"] != null)
                Reason = Convert.ToString(Request.Headers["Reason"]);
            if (!String.IsNullOrEmpty(Request.Headers["LeaveFrom"]))
                LeaveFrom = (Request.Headers["LeaveFrom"]);
            if (!String.IsNullOrEmpty(Request.Headers["LeaveTill"]))
                LeaveTill = (Request.Headers["LeaveTill"]);
            if (Request.Headers["LeaveApplicationId"] != null)
                LeaveApplicationId = Convert.ToInt32(Request.Headers["LeaveApplicationId"]);



            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token) || string.IsNullOrEmpty(LeaveType) || string.IsNullOrEmpty(Reason) || String.IsNullOrEmpty(LeaveFrom) || String.IsNullOrEmpty(LeaveTill))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {

                            if (LeaveApplicationId > 0)
                            {
                                if (user.ContactType.ContactType1.ToLower() == "student")
                                {
                                    var studentLeave = db.StudentLeaveApplications.Where(m => m.StudentLeaveApplicationId == LeaveApplicationId).FirstOrDefault();
                                    if (studentLeave != null)
                                    {
                                        studentLeave.LeaveFrom = Convert.ToDateTime(LeaveFrom);
                                        studentLeave.LeaveTill = Convert.ToDateTime(LeaveTill);
                                        var LeaveTypeId = db.AttendanceStatus.Where(m => m.AttendanceStatus == LeaveType && m.IsLeave == true).Select(m => m.AttendanceStatusId).FirstOrDefault();
                                        studentLeave.LeaveTypeId = LeaveTypeId;
                                        studentLeave.Reason = Reason;
                                        studentLeave.AppliedDate = DateTime.Now;
                                        studentLeave.StudentId = user.UserContactId;
                                        db.Entry(studentLeave).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                }
                                else
                                {
                                    var staffLeave = db.StaffLeaveApplications.Where(m => m.StaffLeaveApplicationId == LeaveApplicationId).FirstOrDefault();
                                    if (staffLeave != null)
                                    {
                                        staffLeave.LeaveFrom = Convert.ToDateTime(FormatDate(Convert.ToString(LeaveFrom)));
                                        staffLeave.LeaveTill = Convert.ToDateTime(FormatDate(Convert.ToString(LeaveTill)));
                                        var LeaveTypeId = db.StaffAttendanceStatus.Where(m => m.AttendanceStatus == LeaveType && m.IsLeave == true).Select(m => m.AttendanceStatusId).FirstOrDefault();
                                        staffLeave.LeaveTypeId = LeaveTypeId;
                                        staffLeave.Reason = Reason;
                                        staffLeave.AppliedDate = DateTime.Now;
                                        staffLeave.StaffId = user.UserContactId;
                                        db.Entry(staffLeave).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                }
                                return Json(new
                                {
                                    status = "success",
                                    msg = "Leave Updated Successfully"

                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                if (user.ContactType.ContactType1.ToLower() == "student")
                                {
                                    StudentLeaveApplication studentLeave = new StudentLeaveApplication();
                                    studentLeave.LeaveFrom = Convert.ToDateTime(LeaveFrom);
                                    studentLeave.LeaveTill = Convert.ToDateTime(LeaveTill);
                                    var LeaveTypeId = db.AttendanceStatus.Where(m => m.AttendanceStatus == LeaveType && m.IsLeave == true).Select(m => m.AttendanceStatusId).FirstOrDefault();
                                    studentLeave.LeaveTypeId = LeaveTypeId;
                                    studentLeave.Reason = Reason;
                                    studentLeave.AppliedDate = DateTime.Now;
                                    studentLeave.StudentId = user.UserContactId;
                                    db.StudentLeaveApplications.Add(studentLeave);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    StaffLeaveApplication staffLeave = new StaffLeaveApplication();
                                    staffLeave.LeaveFrom = Convert.ToDateTime(FormatDate(Convert.ToString(LeaveFrom)));
                                    staffLeave.LeaveTill = Convert.ToDateTime(FormatDate(Convert.ToString(LeaveTill)));
                                    var LeaveTypeId = db.StaffAttendanceStatus.Where(m => m.AttendanceStatus == LeaveType && m.IsLeave == true).Select(m => m.AttendanceStatusId).FirstOrDefault();
                                    staffLeave.LeaveTypeId = LeaveTypeId;
                                    staffLeave.Reason = Reason;
                                    staffLeave.AppliedDate = DateTime.Now;
                                    staffLeave.StaffId = user.UserContactId;
                                    db.StaffLeaveApplications.Add(staffLeave);
                                    db.SaveChanges();
                                }
                                return Json(new
                                {
                                    status = "success",
                                    msg = "Leave Applied Successfully"

                                }, JsonRequestBehavior.AllowGet);
                            }

                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "DBName not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteLeaveApplication(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";
            int LeaveApplicationId = 0;
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["LeaveApplicationId"] != null)
                LeaveApplicationId = Convert.ToInt32(form["LeaveApplicationId"].Trim());

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["LeaveApplicationId"] != null)
                LeaveApplicationId = Convert.ToInt32(Request.Headers["LeaveApplicationId"]);



            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {

                            if (LeaveApplicationId > 0)
                            {
                                if (user.ContactType.ContactType1.ToLower() == "student")
                                {
                                    var studentLeave = db.StudentLeaveApplications.Where(m => m.StudentLeaveApplicationId == LeaveApplicationId).FirstOrDefault();
                                    if (studentLeave != null)
                                    {
                                        db.StudentLeaveApplications.Remove(studentLeave);
                                        db.SaveChanges();
                                    }
                                }
                                else
                                {
                                    var staffLeave = db.StaffLeaveApplications.Where(m => m.StaffLeaveApplicationId == LeaveApplicationId).FirstOrDefault();
                                    if (staffLeave != null)
                                    {
                                        db.StaffLeaveApplications.Remove(staffLeave);
                                        db.SaveChanges();
                                    }
                                }
                                return Json(new
                                {
                                    status = "success",
                                    msg = "Leave Deleted Successfully"

                                }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "DBName not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult LeaveApproval(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();




            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);



            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            var currentsession = GetCurrentSession(dbdetail.DBName);
                            var date = DateTime.UtcNow.Date;
                            var classIncharge = db.ClassTeachers.Where(p => p.TeacherId == user.UserContactId && (p.EndDate >= date || p.EndDate == null)).ToList();
                            if (classIncharge.Count > 0)
                            {
                                var classIds = classIncharge.Select(p => p.ClassId).ToList();
                                var studentClasses = db.StudentClasses.Where(p => classIds.Contains(p.ClassId) && p.SessionId == currentsession.SessionId && p.RollNo != null).ToList();
                                var studentIds = studentClasses.Select(p => p.StudentId).ToList();
                                var studentLeaveList = db.StudentLeaveApplications.Where(p => studentIds.Contains(p.StudentId)).ToList();
                                var lvlist = studentLeaveList.Select(x =>
                                {
                                    var leaveapproveModel = new LeaveApprovalModel();
                                    leaveapproveModel.LeaveId = x.StudentLeaveApplicationId;
                                    leaveapproveModel.LeaveFrom = ConvertDate((DateTime)(x.LeaveFrom));
                                    leaveapproveModel.LeaveTill = ConvertDate((DateTime)(x.LeaveTill));
                                    leaveapproveModel.Name = x.Student.FName + " " + x.Student.LName;
                                    leaveapproveModel.AppliedDate = ConvertDate((DateTime)(x.AppliedDate));
                                    leaveapproveModel.Class = studentClasses.Where(p => p.StudentId == x.StudentId).FirstOrDefault().ClassMaster.Class;
                                    leaveapproveModel.RollNo = studentClasses.Where(p => p.StudentId == x.StudentId).FirstOrDefault().RollNo;
                                    leaveapproveModel.ReasonDesc = x.Reason;
                                    leaveapproveModel.LeaveType = "";
                                    if (x.Reason.Length > 10)
                                        leaveapproveModel.Reason = x.Reason.Substring(0, 9);
                                    else
                                        leaveapproveModel.Reason = x.Reason;
                                    if (x.ApprovedBy != null)
                                    {
                                        if ((bool)x.IsApproved)
                                            leaveapproveModel.Status = "Approved";
                                        else
                                            leaveapproveModel.Status = "Disapproved";
                                    }
                                    else
                                    {
                                        leaveapproveModel.Status = "Action Required";
                                    }

                                    return leaveapproveModel;
                                }).AsQueryable();

                                return Json(new
                                {
                                    status = "success",
                                    LeaveApprovalList = lvlist
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else
                                return Json(new
                                {
                                    status = "success",
                                    message = "Not A Class Incharge"
                                }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "DBName not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApproveLeave(FormCollection form)
        {
            string domain = "", UserName = "", Token = "", Status = "";
            int LeaveId = 0;
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["Status"] != null)
                Status = form["Status"].Trim();
            if (form["LeaveId"] != null)
                LeaveId = Convert.ToInt32(form["LeaveId"].Trim());




            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["Status"] != null)
                Status = Convert.ToString(Request.Headers["Status"]);
            if (Request.Headers["LeaveId"] != null)
                LeaveId = Convert.ToInt32(Request.Headers["LeaveId"]);




            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        var message = "";
                        if (user != null)
                        {
                            var studentLeave = db.StudentLeaveApplications.Where(m => m.StudentLeaveApplicationId == LeaveId).FirstOrDefault();
                            if (studentLeave != null && Status == "Approved")
                            {
                                studentLeave.ApprovedBy = user.UserContactId;
                                studentLeave.ResponseDate = DateTime.UtcNow;
                                studentLeave.IsApproved = true;
                                db.Entry(studentLeave).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                message = "Leave Approved Succesfully";
                            }
                            else if (studentLeave != null && Status == "Disapproved")
                            {
                                studentLeave.ApprovedBy = user.UserContactId;
                                studentLeave.ResponseDate = DateTime.UtcNow;
                                studentLeave.IsApproved = false;
                                db.Entry(studentLeave).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                message = "Leave Disapproved";
                            }

                            return Json(new
                            {
                                status = "success",
                                msg = message
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "DBName not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LeaveApplicationTypes(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";
            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();





            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);





            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        string User = Request.Url.AbsolutePath;
                        User = User.Split('/')[1];
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);
                        KS_SmartChild.DAL.Common.ConnectionService ConnectionService = new KS_SmartChild.DAL.Common.ConnectionService();
                        string SqlDataSource = ConnectionService.SqlConnectionstring(dbdetail.DBName);
                        List<dynamic> ApplicationTypeList = new List<dynamic>();
                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == (UserName)).FirstOrDefault();
                        if (user != null)
                        {
                            if (user.ContactType.ContactType1.ToLower() == "student")
                            {
                                var studentattendanceStatus = db.AttendanceStatus.Where(m => m.IsLeave == true);
                                foreach (var list in studentattendanceStatus)
                                {
                                    ApplicationTypeList.Add(new { Type = list.AttendanceStatus });
                                }
                            }
                            else
                            {
                                var studentattendanceStatus = db.StaffAttendanceStatus.Where(m => m.IsLeave == true);
                                foreach (var list in studentattendanceStatus)
                                {
                                    ApplicationTypeList.Add(new { Type = list.AttendanceStatus });
                                }
                            }

                            return Json(new
                            {
                                status = "success",
                                ApplicationTypeList = ApplicationTypeList
                            }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            message = "DBName not found"
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new
                {
                    status = "failed",
                    message = "Invalid Token"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }





        #endregion

        #region GPS API's

        [HttpPost]
        public ActionResult GetStudentBusRoutePoints(FormCollection form)
        {
            string domain = "", UserName = "", Token = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            if (student != null && student.TptFacilities.Count() > 0)
                            {
                                var currentdate = DateTime.UtcNow.Date;
                                var currenttime = DateTime.UtcNow.ToLocalTime().TimeOfDay;

                                // check student tpt facility
                                var studenttptfacility = student.TptFacilities.Where(t => t.EffectiveDate <= currentdate &&
                                    (t.EndDate == null || (t.EndDate != null && t.EndDate >= currentdate))
                                    && t.Status == true && t.TptTypeDetail.TptType.TptType1 == "School").FirstOrDefault();

                                if (studenttptfacility != null)
                                {
                                    if ((studenttptfacility.BusRoute.EffectiveDate <= currentdate) &&
                                        ((studenttptfacility.BusRoute.EndDate == null) || (studenttptfacility.BusRoute.EndDate != null
                                        && studenttptfacility.BusRoute.EndDate >= currentdate)))
                                    {
                                        // get route type
                                        var routetype = db.BusRouteTimings.Where(t => t.StartTime <= currenttime && t.EndTime >= currenttime).FirstOrDefault();
                                        if (routetype != null)
                                        {
                                            APIBusRouteModel APIBusRoute = new APIBusRouteModel();
                                            APIBusRouteDetail APIBusRouteDetail = new APIBusRouteDetail();
                                            int loopcount = 0;
                                            string DeviceId = string.Empty;

                                            // bus route
                                            APIBusRoute.BusRouteIk = _ICustomEncryption.base64e(studenttptfacility.BusRoute.BusRouteId.ToString());
                                            // get bus route bus and device id
                                            var BusDetail = studenttptfacility.BusRoute.TptRouteDetails.Where(t => t.EffectiveDate <= currentdate &&
                                                    (t.EndDate == null || (t.EndDate != null && t.EndDate >= currentdate))).FirstOrDefault();

                                            if (BusDetail != null)
                                            {
                                                var gspdetail = BusDetail.Bus.TptBusGPSDeviceDetails.Where(t => t.EffectiveDate <= currentdate &&
                                                     (t.EndDate == null || (t.EndDate != null && t.EndDate >= currentdate))).FirstOrDefault();
                                                if (gspdetail != null)
                                                    DeviceId = gspdetail.TptGPSDevice.TptGPSDeviceSN;
                                            }

                                            var busroutedetail = studenttptfacility.BusRoute.BusRouteDetails.Where(t => t.EffectiveDate <= currentdate &&
                                                    (t.EndDate == null || (t.EndDate != null && t.EndDate >= currentdate))).OrderBy(t => t.BoardingIndex).ToList();
                                            if (routetype.BusRouteType == "Pick")
                                            {
                                                busroutedetail = busroutedetail.Where(b => b.PickupTime != null).ToList();
                                                foreach (var item in busroutedetail)
                                                {
                                                    APIBusRouteDetail = new APIBusRouteDetail();
                                                    APIBusRouteDetail.RoutePointName = item.BoardingPoint.BoardingPoint1;
                                                    APIBusRouteDetail.BusRouteLat = item.BoardingPoint.Latitude;
                                                    APIBusRouteDetail.BusRouteLong = item.BoardingPoint.Longitude;

                                                    if (loopcount == 0)
                                                        APIBusRouteDetail.RoutePointType = "Source";
                                                    else if (loopcount == busroutedetail.Count - 1)
                                                        APIBusRouteDetail.RoutePointType = "Destination";
                                                    else
                                                        APIBusRouteDetail.RoutePointType = "Waypoint";

                                                    APIBusRoute.BusRouteDetail.Add(APIBusRouteDetail);
                                                    loopcount++;
                                                }
                                            }
                                            else if (routetype.BusRouteType == "Drop")
                                            {
                                                busroutedetail = busroutedetail.Where(b => b.DropTime != null).ToList();
                                                foreach (var item in busroutedetail)
                                                {
                                                    APIBusRouteDetail = new APIBusRouteDetail();
                                                    APIBusRouteDetail.RoutePointName = item.BoardingPoint.BoardingPoint1;
                                                    APIBusRouteDetail.BusRouteLat = item.BoardingPoint.Latitude;
                                                    APIBusRouteDetail.BusRouteLong = item.BoardingPoint.Longitude;

                                                    if (loopcount == 0)
                                                        APIBusRouteDetail.RoutePointType = "Source";
                                                    else if (loopcount == busroutedetail.Count - 1)
                                                        APIBusRouteDetail.RoutePointType = "Destination";
                                                    else
                                                        APIBusRouteDetail.RoutePointType = "Waypoint";

                                                    APIBusRoute.BusRouteDetail.Add(APIBusRouteDetail);
                                                    loopcount++;
                                                }
                                            }

                                            return Json(new
                                            {
                                                status = "success",
                                                data = APIBusRoute,
                                                DeviceId = DeviceId
                                            }, JsonRequestBehavior.AllowGet);
                                        }

                                        return Json(new
                                        {
                                            status = "failed",
                                            message = "Bus route drop time expire"
                                        }, JsonRequestBehavior.AllowGet);
                                    }
                                }

                                return Json(new
                                {
                                    status = "failed",
                                    message = "User not mapped with Bus transport"
                                }, JsonRequestBehavior.AllowGet);
                            }
                        }

                        return Json(new
                        {
                            status = "failed",
                            message = "User not found"
                        }, JsonRequestBehavior.AllowGet);

                    }
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid School"
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new
                    {
                        status = "failed",
                        message = "Invalid Token"
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult refreshGPSLocation(FormCollection form)
        {
            string domain = "", UserName = "", Token = "", BusRoute = "";

            // form keys
            if (form["Domain"] != null)
                domain = form["Domain"].Trim();
            if (form["UserId"] != null)
                UserName = form["UserId"].Trim();
            if (form["Token"] != null)
                Token = form["Token"].Trim();
            if (form["BusRoute"] != null)
                BusRoute = form["BusRoute"].Trim();

            // headers
            if (Request.Headers["Domain"] != null)
                domain = Convert.ToString(Request.Headers["Domain"]);
            if (Request.Headers["UserId"] != null)
                UserName = Convert.ToString(Request.Headers["UserId"]);
            if (Request.Headers["Token"] != null)
                Token = Convert.ToString(Request.Headers["Token"]);
            if (Request.Headers["BusRoute"] != null)
                BusRoute = Convert.ToString(Request.Headers["BusRoute"]);

            if (string.IsNullOrEmpty(domain) || string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Token))
            {
                return Json(new
                {
                    status = "failed",
                    message = "Parameter required"
                }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (IsTokenValid(Token))
                {
                    var dbdetail = AppDBConnection(Request);
                    if (!string.IsNullOrEmpty(dbdetail.DBName))
                    {
                        KS_SmartChild.Models.KS_ChildEntities db = new KS_SmartChild.Models.KS_ChildEntities(dbdetail.DataSource);

                        // check user exist and find User info
                        var user = db.SchoolUsers.Where(u => u.UserName == UserName).FirstOrDefault();
                        if (user != null && (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian"))
                        {
                            var student = new Student();
                            if (user.ContactType.ContactType1.ToLower() == "student")
                                student = db.Students.Where(s => s.StudentId == (int)user.UserContactId).FirstOrDefault();
                            else
                            {
                                // get guardian by id
                                var Guardian = db.Guardians.Where(s => s.GuardianId == (int)user.UserContactId).FirstOrDefault();
                                if (Guardian != null)
                                    student = Guardian.Student;
                            }

                            // decrypt bus route id
                            int busrouteid = 0;
                            int.TryParse(_ICustomEncryption.base64d(BusRoute), out busrouteid);
                            var currentdate = DateTime.UtcNow.Date;
                            // get bus by bus route
                            var busdetail = db.TptRouteDetails.Where(t => t.BusRouteId == busrouteid && t.BusId != null &&
                                t.EffectiveDate <= currentdate &&
                                (t.EndDate == null || (t.EndDate != null && t.EndDate >= currentdate)))
                                .OrderByDescending(t => t.TptRouteDetailId).FirstOrDefault();
                            if (busdetail != null)
                            {
                                var gpsdata = busdetail.Bus.GPSDatas.Where(g => g.RequestDate == currentdate && (g.IsUsed == null || g.IsUsed == false)).FirstOrDefault();
                                if (gpsdata != null)
                                {
                                    APIBusGPSDataModel Obj = new APIBusGPSDataModel();
                                    Obj.Direction = gpsdata.Direction;
                                    Obj.Latitude = gpsdata.Latitude;
                                    Obj.LatitudeDirection = gpsdata.LatDirection;
                                    Obj.Longitude = gpsdata.Longitude;
                                    Obj.LongitudeDirection = gpsdata.LongDirection;
                                    Obj.Speed = gpsdata.Speed;

                                    // update row
                                    gpsdata.IsUsed = true;
                                    db.Entry(gpsdata).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();

                                    return Json(new
                                    {
                                        status = "success",
                                        data = Obj
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }

                            return Json(new
                            {
                                status = "failed",
                                message = "Data not found"
                            }, JsonRequestBehavior.AllowGet);

                        }
                        else
                            return Json(new
                            {
                                status = "failed",
                                message = "User not found"
                            }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new
                        {
                            status = "failed",
                            message = "Invalid School"
                        }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new
                    {
                        status = "failed",
                        message = "Token expire"
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    message = "Invalid School"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

    }
}