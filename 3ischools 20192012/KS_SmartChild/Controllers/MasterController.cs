﻿using BAL.BAL.Common;
using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.ClassPeriodModule;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.ExaminationModule;
using DAL.DAL.StudentModule;
using DAL.DAL.MenuModel;
using DAL.DAL.SettingService;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Master;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.Controllers
{
    public class MasterController : Controller
    {
        #region fields

        public int count = 0;
        private readonly IUserService _IUserService;
        private readonly ILogService _Logger;
        private readonly IConnectionService _IConnectionService;
        private readonly IWebHelper _WebHelper;
        private readonly INavigationService _INavigationService;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IExamService _IExamService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IClassPeriodService _IClassPeriodService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IStudentService _IStudentService;



        #endregion

        #region ctor

        public MasterController(IUserService IUserService,
            ILogService Logger,
            IConnectionService IConnectionService,
            IWebHelper WebHelper,
            INavigationService INavigationService,
            IDropDownService IDropDownService,
            IActivityLogMasterService IActivityLogMasterService,
            IExamService IExamService,
            IActivityLogService IActivityLogService,
            ICatalogMasterService ICatalogMasterService,
            IClassPeriodService IClassPeriodService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption,
            ISchoolSettingService ISchoolSettingService,
            IStudentService IStudentService)
        {
            this._IUserService = IUserService;
            this._Logger = Logger;
            this._IConnectionService = IConnectionService;
            this._WebHelper = WebHelper;
            this._INavigationService = INavigationService;
            this._IDropDownService = IDropDownService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._IExamService = IExamService;
            this._IActivityLogService = IActivityLogService;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IClassPeriodService = IClassPeriodService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._IStudentService = IStudentService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        public string ConvertTime(TimeSpan time)
        {
            TimeSpan timespan = new TimeSpan(time.Hours, time.Minutes, time.Seconds);
            DateTime datetime = DateTime.Today.Add(timespan);
            string sdisplayTime = datetime.ToString("hh:mm tt");
            return sdisplayTime;
        }

        #endregion

        #region Methods
        //Convert date
        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }

        public ActionResult All(string TN)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                if (!string.IsNullOrEmpty(TN))
                {
                    // get title name from encrypted query string 
                    //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                    //if (titlename != null)
                    //{
                    //    ViewBag.titlename = titlename.NavigationItem1;
                    //}
                    //else
                    string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/form/forms/";
                    ViewBag.url = phprequesturl + TN;
                    return View();
                }
                else
                    return RedirectToAction("HttpError404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        public ActionResult Form(string u)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                if (!string.IsNullOrEmpty(u))
                {
                    // get title name from encrypted query string 
                    //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                    //if (titlename != null)
                    //{
                    //    ViewBag.titlename = titlename.NavigationItem1;
                    //}
                    //else
                    string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/";
                    ViewBag.url = phprequesturl + u;
                    return View();
                }
                else
                    return RedirectToAction("HttpError404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult AdmitCard()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/admitcard";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult InventoryReport()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/reports/inventory_report";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult StudentBrowser()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/student_browser/browser";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        public ActionResult ManageProductReturn()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/inventory/manage_product_return";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        public ActionResult ManageProductTransaction()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/inventory/manage_product_transaction";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult ClassTest()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/class_test/manage_class_subject_test_activity";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult ClassTestReport()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/class_test/generate_report";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult FreePeriodReport()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "/module/index.php/reports/free_period_report";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult HealthRecord()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/health_record/manage_page_data/PtvRiSrif_S0L0H_w_E0L0S_";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult BalanceSheetHead()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/ledger_account/manage_bshead";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult LedgerAccount()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/ledger_account/manage_account";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult Vouchers()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/voucher";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        public ActionResult ManageReportGuardian()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/class_test/manage_report_forGuardian";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        public ActionResult Common(string u)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/" + u;
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        public ActionResult LessonPlanReport()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/lesson_plan/lesson_plan_report";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        public ActionResult LessonPlan()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/lesson_plan/manage_lesson_plan";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult OnlineRegistration()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/online_regn/manage_online_regn";
                ViewBag.url = phprequesturl;
                return View();
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult Payment()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/online_regn/prePayment";
                ViewBag.url = phprequesturl;
                return View();
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult AccountLedger()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/reports/accountledger";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult TrialBalance()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/reports/trial_balance";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult CashBook()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/reports/cash_book";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult BankBook()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                // get title name from encrypted query string 
                //var titlename = _INavigationService.GetAllNavigationItems(ref count, QueryString: TN).FirstOrDefault();
                //if (titlename != null)
                //{
                //    ViewBag.titlename = titlename.NavigationItem1;
                //}
                //else
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/reports/bank_book";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }


        #region period

        public ManagePeriod preparemodelforperiod(ManagePeriod model)
        {
            var maxPeriodIndex = Convert.ToInt32(_ISchoolSettingService.GetAttributeValue("NoOfPeriods"));
            var periodIndexList = new List<SelectListItem>();
            periodIndexList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
            var allIndexes = new List<int>();
            for (int i = 1; i <= maxPeriodIndex; i++)
            {
                allIndexes.Add(i);
            }
            var savedPeriodIndexes = _IClassPeriodService.GetAllPeriods(ref count).Select(p => p.PeriodIndex).ToList();
            if (savedPeriodIndexes.Count > 0)
            {
                var unsavedIndexes = allIndexes.Where(p => !savedPeriodIndexes.Contains(p)).ToList();
                foreach (var item in unsavedIndexes)
                {
                    periodIndexList.Add(new SelectListItem { Selected = false, Value = item.ToString(), Text = item.ToString() });
                }
            }
            else
            {
                foreach (var item in allIndexes)
                {
                    periodIndexList.Add(new SelectListItem { Selected = false, Value = item.ToString(), Text = item.ToString() });
                }
            }
            var defaultPeriodTime = _ISchoolSettingService.GetAttributeValue("DefaultPeriodTime");
            model.EndTime = ConvertTime(DateTime.Now.AddMinutes(Convert.ToDouble(defaultPeriodTime)).TimeOfDay);
            //model.EndTimehi = ConvertTime(DateTime.Now.AddMinutes(Convert.ToDouble(defaultPeriodTime)).TimeOfDay);
            model.StartTime = ConvertTime(DateTime.Now.TimeOfDay);
            //model.StartTimehi = ConvertTime(DateTime.Now.TimeOfDay);
            model.PeriodIndexList = periodIndexList;

            return model;
        }

        public ActionResult Period()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);

                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "Period", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            var model = new ManagePeriod();
            if (TempData["PeriodSaveUpdateMessage"] != null)
            {
                ViewBag.PeriodSaveUpdateMessage = TempData["PeriodSaveUpdateMessage"];
            }
            model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "Period", "Add Record");
            model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "Period", "Edit Record");
            model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "Period", "Delete Record");

            model = preparemodelforperiod(model);
            model.IsPeriodExist = false;
            model.IsPeriodTimeInValid = false;
            return View(model);
        }
        //check the errors for ManagePeriod model
        public ManagePeriod checkmanageperiodemodelerror(ManagePeriod model)
        {
            if (string.IsNullOrWhiteSpace(model.PeriodName))
                ModelState.AddModelError("PeriodName", "PeriodName is required.");
            if (model.StartTime == "")
                ModelState.AddModelError("StartTime", "StartTime is required");
            if (model.EndTime == "")
                ModelState.AddModelError("EndTime", "EndTime is required");
            return model;
        }

        [HttpPost]
        public ActionResult Period(ManagePeriod model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);

                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "Period", "Add Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                // Activity log type add
                var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                // period log Table
                var periodtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "Period").FirstOrDefault();
                DateTime st = DateTime.ParseExact(model.StartTime, "hh:mm tt", CultureInfo.InvariantCulture);
                DateTime et = DateTime.ParseExact(model.EndTime, "hh:mm tt", CultureInfo.InvariantCulture);

                var allPeriods = _IClassPeriodService.GetAllPeriods(ref count);
                model.IsPeriodTimeInValid = false;
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "Period", "Add Record");
                model = checkmanageperiodemodelerror(model);
                if (ModelState.IsValid)
                {
                    //check the time for the period is valid or not
                    if (ConvertTime(st.TimeOfDay) == ConvertTime(et.TimeOfDay))
                        model.IsSameStartEndTimes = true;
                    foreach (var item in allPeriods)
                    {
                        if (model.PeriodId > 0)
                        {
                            if (string.Equals(item.Period1.Trim().ToLower(), model.PeriodName.Trim().ToLower()) && item.PeriodId == model.PeriodId)
                                model.IsPeriodExist = false;
                            if (string.Equals(item.Period1.Trim().ToLower(), model.PeriodName.Trim().ToLower()) && item.PeriodId != model.PeriodId)
                                model.IsPeriodExist = true;
                        }
                        if (string.Equals(item.Period1.Trim().ToLower(), model.PeriodName.Trim().ToLower()) && model.PeriodId == null)
                            model.IsPeriodExist = true;
                        if (model.PeriodId == item.PeriodId)
                            continue;
                        if (st.TimeOfDay < item.EndTime && item.StartTime < et.TimeOfDay)
                            model.IsPeriodTimeInValid = true;
                    }
                    //insertion of record
                    if (model.PeriodId == null && !model.IsPeriodTimeInValid && !model.IsPeriodExist && !model.IsSameStartEndTimes)
                    {
                        var newperiod = new Period();
                        newperiod.Period1 = model.PeriodName;
                        newperiod.StartTime = st.TimeOfDay;
                        newperiod.EndTime = et.TimeOfDay;
                        newperiod.PeriodIndex = model.PeriodIndex;
                        _IClassPeriodService.InsertPeriod(newperiod);
                        if (periodtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, periodtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog((int)newperiod.PeriodId, (int)newperiod.PeriodIndex, logtype.ActivityLogTypeId, user, String.Format("Add PeriodDetail with periodId:{0} and Period:{1}", newperiod.PeriodId, newperiod.Period1), Request.Browser.Browser);
                        }
                        TempData["PeriodSaveUpdateMessage"] = "Period saved successfully.";
                    }
                    //updation of period
                    else if (model.PeriodId > 0 && !model.IsPeriodTimeInValid && !model.IsPeriodExist && !model.IsSameStartEndTimes)
                    {
                        var peroiddetail = _IClassPeriodService.GetPeriodById((int)model.PeriodId, true);
                        peroiddetail.Period1 = model.PeriodName;
                        peroiddetail.StartTime = st.TimeOfDay;
                        peroiddetail.EndTime = et.TimeOfDay;
                        peroiddetail.PeriodIndex = model.PeriodIndex;
                        _IClassPeriodService.UpdatePeriod(peroiddetail);
                        if (periodtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, periodtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog((int)peroiddetail.PeriodId, (int)peroiddetail.PeriodIndex, logtype.ActivityLogTypeId, user, String.Format("update PeriodDetail with periodId:{0} and Period:{1}", peroiddetail.PeriodId, peroiddetail.Period1), Request.Browser.Browser);
                        }
                        TempData["PeriodSaveUpdateMessage"] = "Period updated successfully.";
                    }
                    else
                    {
                        if (model.PeriodId != null)
                            TempData["PeriodIndex"] = model.PeriodIndex;
                        TempData["StartTime"] = model.StartTime;
                        TempData["EndTime"] = model.EndTime;
                        model = preparemodelforperiod(model);
                        return View(model);
                    }
                    return RedirectToAction("Period");
                }
                model = preparemodelforperiod(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        //delete the period
        public ActionResult DeletePeriod(int periodId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);

                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "Period", "Delete");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            try
            {
                var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                // period log Table
                var periodtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "Period").FirstOrDefault();
                var peroiddetail = _IClassPeriodService.GetPeriodById(periodId);
                //deletion of record from period table
                if (peroiddetail != null && peroiddetail.ClassPeriods.Count == 0 && peroiddetail.ClassPeriods.Count == 0)
                {
                    _IClassPeriodService.DeletePeriod(periodId);
                    if (periodtable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, periodtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog((int)peroiddetail.PeriodId, (int)peroiddetail.PeriodIndex, logtypeEdit.ActivityLogTypeId, user, String.Format("delete PeriodDetail with periodId:{0} and Period:{1}", peroiddetail.PeriodId, peroiddetail.Period1), Request.Browser.Browser);
                    }
                }
                else
                    return Json(new { status = "failed", data = "Period is not accessible for deletion." });

                return Json(new { status = "success", data = "Period deleted successfully." });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }

        }
        //get the list of all periods
        public ActionResult GetAllPeriods()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                var allPeriods = _IClassPeriodService.GetAllPeriods(ref count);
                var model = new Periods();
                model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "Period", "Delete Record");
                model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "Period", "Edit Record");
                var periods = new List<ManagePeriod>();
                if (allPeriods != null)
                {
                    //list of all periods
                    foreach (var item in allPeriods)
                    {
                        if (item.ClassPeriods.Count == 0 && item.AttendancePeriodWises.Count == 0)
                            periods.Add(new ManagePeriod { PeriodName = item.Period1, PeriodId = item.PeriodId, StartTime = ConvertTime((TimeSpan)item.StartTime), EndTime = ConvertTime((TimeSpan)item.EndTime), IsPeriodDeletable = true, PeriodIndex = item.PeriodIndex });
                        else
                            periods.Add(new ManagePeriod { PeriodName = item.Period1, PeriodId = item.PeriodId, StartTime = ConvertTime((TimeSpan)item.StartTime), EndTime = ConvertTime((TimeSpan)item.EndTime), IsPeriodDeletable = false, PeriodIndex = item.PeriodIndex });
                    }
                }
                model.PeriodsDetail = periods.OrderBy(p => p.PeriodIndex).ToList();
                return PartialView("_periodsList", model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }

        }
        //get times by periodIndex
        public ActionResult GetTimesByIndex(int periodIndex)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                TimeSpan startTime = TimeSpan.Zero;
                TimeSpan endTime = TimeSpan.Zero;
                var periods = _IClassPeriodService.GetAllPeriods(ref count);
                if (periods.Count > 0)
                {
                    Period reqPeriod = null;
                    int counter = 0;
                    for (int i = periodIndex; i >= 1; i--)
                    {
                        counter = ++counter;
                        reqPeriod = periods.SingleOrDefault(p => p.PeriodIndex == i);
                        if (reqPeriod != null)
                            break;
                    }
                    if (reqPeriod != null)
                    {
                        if (counter == 1)
                            startTime = (TimeSpan)reqPeriod.StartTime;
                        else
                            startTime = (TimeSpan)reqPeriod.EndTime;
                        var defaultPeriodTime = _ISchoolSettingService.GetAttributeValue("DefaultPeriodTime");
                        endTime = startTime.Add(TimeSpan.Parse("00" + ":" + defaultPeriodTime));
                    }
                }
                else
                {
                    var defaultPeriodTime = _ISchoolSettingService.GetAttributeValue("DefaultPeriodTime");
                    endTime = DateTime.Now.AddMinutes(Convert.ToDouble(defaultPeriodTime)).TimeOfDay;
                    startTime = DateTime.Now.TimeOfDay;
                }
                return Json(new { status = "success", startTime = ConvertTime(startTime), endTime = ConvertTime(endTime) });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }

        public ActionResult GetIndexes(int? PeriodIndex = null)
        {

            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                var maxPeriodIndex = Convert.ToInt32(_ISchoolSettingService.GetAttributeValue("NoOfPeriods"));
                var periodIndexList = new List<SelectListItem>();
                var allIndexes = new List<int>();
                for (int i = 1; i <= maxPeriodIndex; i++)
                {
                    allIndexes.Add(i);
                }
                var savedPeriodIndexes = _IClassPeriodService.GetAllPeriods(ref count).Select(p => p.PeriodIndex).ToList();
                if (PeriodIndex != null && savedPeriodIndexes.Count > 0)
                {
                    var unsavedIndexes = allIndexes.Where(p => !savedPeriodIndexes.Contains(p)).ToList();
                    var selectedPeriod = savedPeriodIndexes.Where(p => p == PeriodIndex).FirstOrDefault();
                    periodIndexList.Add(new SelectListItem { Selected = true, Value = selectedPeriod.ToString(), Text = selectedPeriod.ToString() });
                    foreach (var item in unsavedIndexes)
                    {
                        periodIndexList.Add(new SelectListItem { Selected = false, Value = item.ToString(), Text = item.ToString() });
                    }
                }
                else if (PeriodIndex == null && savedPeriodIndexes.Count > 0)
                {
                    var unsavedIndexes = allIndexes.Where(p => !savedPeriodIndexes.Contains(p)).ToList();
                    periodIndexList.Add(new SelectListItem { Selected = true, Value = "", Text = "--Select--" });
                    foreach (var item in unsavedIndexes)
                    {
                        periodIndexList.Add(new SelectListItem { Selected = false, Value = item.ToString(), Text = item.ToString() });
                    }
                }
                else
                {
                    periodIndexList.Add(new SelectListItem { Selected = true, Value = "", Text = "--Select--" });
                    foreach (var item in allIndexes)
                    {
                        periodIndexList.Add(new SelectListItem { Selected = false, Value = item.ToString(), Text = item.ToString() });
                    }
                }
                return Json(new { status = "success", periodIndexList = periodIndexList });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }
        #endregion

        #region Examination marks Pattern

        public MarksPatternModel prepareMarksPatternmodel(MarksPatternModel model)
        {
            // prepare model 
            // Drop Down Lists
            // Grade pattern
            IList<SelectListItem> dropdownlist = new List<SelectListItem>();
            var gradepatternlist = _IExamService.GetAllGradePatterns(ref count);
            var gradepatternDetaillist = _IExamService.GetAllGradePatternDetails(ref count);
            var gradepatternidArray = gradepatternDetaillist.Count > 0 ? gradepatternDetaillist.Select(x => (int)x.GradePatternId).ToArray() : new int[0];
            gradepatternlist = gradepatternlist.Count > 0 ? gradepatternlist.Where(x => gradepatternidArray.Contains(x.GradePatternId)).ToList() : new List<GradePattern>();

            foreach (var gpl in gradepatternlist)
            {
                dropdownlist.Add(new SelectListItem { Selected = false, Text = gpl.GradePattern1, Value = gpl.GradePatternId.ToString() });
            }

            model.AvailableGradePattern = dropdownlist;
            var markspatternlist = _IExamService.GetAllMarksPatterns(ref count);
            if (markspatternlist != null)
            {
                MarksPatternModel MarksPatternModel = new MarksPatternModel();
                foreach (var mpl in markspatternlist)
                {
                    MarksPatternModel = new MarksPatternModel();
                    MarksPatternModel.MaxMarks = mpl.MaxMarks;
                    MarksPatternModel.PassMarks = mpl.PassMarks;
                    MarksPatternModel.IAMaxMarks = mpl.IAMaxMarks;
                    MarksPatternModel.IAPassMarks = mpl.IAPassMarks;
                    var gradepattern = _IExamService.GetGradePatternById((int)mpl.GradePatternId);
                    if (gradepattern != null)
                        MarksPatternModel.GradePattern = gradepattern.GradePattern1;
                    MarksPatternModel.MarksPatternId = mpl.MarksPatternId;
                    MarksPatternModel.EncMarksPatternId = _ICustomEncryption.base64e(mpl.MarksPatternId.ToString());
                    MarksPatternModel.GradePatternId = mpl.GradePatternId;
                    var examdatesheetdetail = _IExamService.GetAllExamDateSheetDetails(ref count, MarksPatternId: mpl.MarksPatternId);
                    if (examdatesheetdetail.Count > 0)
                        MarksPatternModel.MarksPatternUsed = false;
                    else
                        MarksPatternModel.MarksPatternUsed = true;
                    model.MarksPatternList.Add(MarksPatternModel);
                }
            }
            return model;
        }

        public ActionResult MarksPattern()
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "MarksPattern", "View");
                    if (!isauth)
                        return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                var model = new MarksPatternModel();
                var ExamInternalAssesmentApplicable = _ISchoolSettingService.GetorSetSchoolData("ExamInternalAssesmentApplicable", "1").AttributeValue;
                model.IsExamInternalAssesmentApplicable = ExamInternalAssesmentApplicable == "1" ? true : false;
                var ExamInternalAssessmentText = _ISchoolSettingService.GetorSetSchoolData("ExamInternalAssessmentText", "IA").AttributeValue;
                model.ExamInternalAssessmentText = ExamInternalAssessmentText;
                model = prepareMarksPatternmodel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "MarksPattern", "Add Record");
                model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "MarksPattern", "Edit Record");
                model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "MarksPattern", "Delete Record");
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public GradePatternModel prepareGradePatternmodel(GradePatternModel model)
        {
            // prepare model 
            // Drop Down Lists
            // Grade pattern
            model.AvailableGrade = _IDropDownService.GradeList();
            IList<SelectListItem> dropdownlist = new List<SelectListItem>();
            dropdownlist = _IDropDownService.GradePatternList();
            dropdownlist.RemoveAt(0);
            dropdownlist.Insert(0, new SelectListItem { Selected = true, Text = "Add New", Value = "" });
            model.AvailableGradePattern = dropdownlist;

            GradePatternModel GradePatternModel = new GradePatternModel();
            //get all grade pattern
            var GradePattern = _IExamService.GetAllGradePatterns(ref count);
            foreach (var gp in GradePattern)
            {
                //get grade pattern detail
                var GradePatternDetail = _IExamService.GetAllGradePatternDetails(ref count, GradePatternId: gp.GradePatternId).ToList();

                if (GradePatternDetail != null)
                {
                    foreach (var gpd in GradePatternDetail)
                    {
                        GradePatternModel = new GradePatternModel();
                        GradePatternModel.GradePatternId = gp.GradePatternId;
                        GradePatternModel.GradePattern = gp.GradePattern1;
                        var boardlist = _ICatalogMasterService.GetBoardById((int)gp.BoardId);
                        if (boardlist != null)
                            GradePatternModel.Board = boardlist.Board1;
                        //GradePatternModel.GradePatternDetailId = gpd.GradePatternDetailId;
                        GradePatternModel.EncGradePatternDetailId = _ICustomEncryption.base64e(gpd.GradePatternDetailId.ToString());
                        GradePatternModel.StartingMarks = gpd.StartingMarks;
                        GradePatternModel.EndingMarks = gpd.EndingMarks;
                        GradePatternModel.GradeId = gpd.GradeId;
                        var gradelist = _IExamService.GetGradeById((int)gpd.GradeId);
                        if (gradelist != null)
                            GradePatternModel.Grade = gradelist.Grade1;
                        GradePatternModel.EffectiveDate = ConvertDate((DateTime)gpd.EffectiveDate);
                        if (gpd.EndDate != null)
                            GradePatternModel.EndDate = ConvertDate((DateTime)gpd.EndDate);
                        if (GradePatternDetail.Count > 1)
                            GradePatternModel.hasgradepatterndetial = 1;
                        model.GradePatternList.Add(GradePatternModel);
                    }
                }
            }


            return model;
        }

        public ActionResult GradePattern()
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "GradePattern", "View");
                    if (!isauth)
                        return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                var model = new GradePatternModel();
                model = prepareGradePatternmodel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "GradePattern", "Add Record");
                model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "GradePattern", "Edit Record");
                model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "GradePattern", "Delete Record");
                model.IsAuthToAddGradePattern = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "GradePattern", "Add Record");
                model.IsAuthToClose = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "GradePattern", "Close");


                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        #endregion

        #region Exam Term Activity

        public ExamTermActivityModel prepareexamtermactivitymodel(ExamTermActivityModel model)
        {
            // get all exam term
            model.AvailableExamTerm.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            foreach (var item in _IExamService.GetAllExamTerms(ref count))
                model.AvailableExamTerm.Add(new SelectListItem { Selected = false, Text = item.ExamTerm1, Value = _ICustomEncryption.base64e(item.ExamTermId.ToString()) });

            var AllSessions = _ICatalogMasterService.GetAllSessions(ref count);
            var crrntsessn= AllSessions.Where(f => f.IsDefualt == true).FirstOrDefault();
            model.filtSessionId = crrntsessn != null ? crrntsessn.SessionId.ToString() : "";
            foreach (var item in AllSessions)
                model.AvailableSessions.Add(new SelectListItem { Selected = false, Text = item.Session1, Value = item.SessionId.ToString() });

            return model;
        }

        public ActionResult ExamTermActivity()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            var model = new ExamTermActivityModel();
            model = prepareexamtermactivitymodel(model);

            return View(model);
        }

        [HttpPost]
        public ActionResult SaveorUpdateExamTermActivity(ExamTermActivityModel model)
        {
            SchoolUser user = new SchoolUser();
            string Host = Request.Url.AbsolutePath;
            Host = Host.Split('/')[1];
            if (Request.Url.AbsoluteUri.Contains("localhost"))
                Host = "";
            string url = _WebHelper.GetStoreLocation() + Host + "/Home/Login";

            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new { status = "expire", url = url });

                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
                return Json(new { status = "expire", url = url });

            int ExamTermId = 0, ExamTermActivityId = 0, ExamStandardId = 0, ExamTermStandardId = 0, ExamActivityId = 0;
            // Activity log type add
            var logtypeAdd = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
            var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
            var logtypedelete = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
            var ExamTermActivityTable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ExamTermActivity").FirstOrDefault();
            var ExamTermStandardTable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ExamTermStandard").FirstOrDefault();

            // decrypt Values
            int.TryParse(_ICustomEncryption.base64d(model.ExamTermId), out ExamTermId);

            System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
            IList<ExamActivityModel> ActivityArray = js.Deserialize<List<ExamActivityModel>>(model.ActivityArray);

            ExamTermActivity ExamTermActivity = new ExamTermActivity();
            foreach (var examact in ActivityArray)
            {
                ExamActivityId = 0;
                ExamTermActivityId = 0;
                int.TryParse(_ICustomEncryption.base64d(examact.ExamActivityId), out ExamActivityId);

                if (string.IsNullOrEmpty(examact.ExamTermActivityId))
                {
                    if (examact.IsDeleted == 0) // delete record
                    {
                        ExamTermActivity = new ExamTermActivity();
                        ExamTermActivity.ExamActivityId = ExamActivityId;
                        ExamTermActivity.ExamTermId = ExamTermId;
                        ExamTermActivity.IsTermEnd = examact.IsTermEnd != null ? (bool)examact.IsTermEnd : false;
                        _IExamService.InsertExamTermActivity(ExamTermActivity);

                        // TimeTable log Table
                        if (ExamTermActivityTable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ExamTermActivityTable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(ExamTermActivity.ExamTermActivityId, ExamTermActivityTable.TableId, logtypeAdd.ActivityLogTypeId, user, String.Format("Add ExamTermActivity with ExamTermActivityId:{0} and ExamTermId:{1}", ExamTermActivity.ExamTermActivityId, ExamTermActivity.ExamTermId), Request.Browser.Browser);
                        }
                    }
                }
                else
                {
                    int.TryParse(_ICustomEncryption.base64d(examact.ExamTermActivityId), out ExamTermActivityId);
                    // get by id
                    ExamTermActivity = _IExamService.GetExamTermActivityById(ExamTermActivityId);
                    if (ExamTermActivity != null)
                    {
                        // check if record deleted
                        if (examact.IsDeleted == 1) // delete record
                        {
                            if (ExamTermActivity.ExamTermActivitySkills.Count == 0 && ExamTermActivity.ExamDateSheets.Count == 0)
                            {
                                _IExamService.DeleteExamTermActivity(ExamTermActivity.ExamTermActivityId);

                                // TimeTable log Table
                                if (ExamTermActivityTable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ExamTermActivityTable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(ExamTermActivity.ExamTermActivityId, ExamTermActivityTable.TableId, logtypedelete.ActivityLogTypeId, user, String.Format("Delete ExamTermActivity with ExamTermActivityId:{0} and ExamTermId:{1}", ExamTermActivity.ExamTermActivityId, ExamTermActivity.ExamTermId), Request.Browser.Browser);
                                }
                            }
                        }
                        else
                        {
                            ExamTermActivity.IsTermEnd = examact.IsTermEnd != null ? (bool)examact.IsTermEnd : false;
                            _IExamService.UpdateExamTermActivity(ExamTermActivity);

                            // TimeTable log Table
                            if (ExamTermActivityTable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ExamTermActivityTable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(ExamTermActivity.ExamTermActivityId, ExamTermActivityTable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update ExamTermActivity with ExamTermActivityId:{0} and ExamTermId:{1}", ExamTermActivity.ExamTermActivityId, ExamTermActivity.ExamTermId), Request.Browser.Browser);
                            }
                        }
                    }
                }
            }

            js = new System.Web.Script.Serialization.JavaScriptSerializer();
            IList<ExamTermStandardModel> StandardArray = js.Deserialize<List<ExamTermStandardModel>>(model.StandardArray);

            ExamTermStandard ExamTermStandard = new ExamTermStandard();
            foreach (var examstd in StandardArray)
            {
                ExamStandardId = 0;
                ExamTermStandardId = 0;
                int.TryParse(_ICustomEncryption.base64d(examstd.StandardId), out ExamStandardId);

                if (string.IsNullOrEmpty(examstd.ExamTermStandardId))
                {
                    if (examstd.IsDeleted == 0)
                    {
                        ExamTermStandard = new ExamTermStandard();
                        ExamTermStandard.ExamTermId = ExamTermId;
                        ExamTermStandard.ExamStandardId = ExamStandardId;
                        _IExamService.InsertExamTermStandard(ExamTermStandard);

                        // TimeTable log Table
                        if (ExamTermStandardTable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ExamTermStandardTable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(ExamTermStandard.ExamTermStandardId, ExamTermStandardTable.TableId, logtypeAdd.ActivityLogTypeId, user, String.Format("Add ExamTermStandard with ExamTermStandardId:{0} and ExamTermId:{1}", ExamTermStandard.ExamTermStandardId, ExamTermStandard.ExamTermId), Request.Browser.Browser);
                        }
                    }
                }
                else
                {
                    int.TryParse(_ICustomEncryption.base64d(examstd.ExamTermStandardId), out ExamTermStandardId);
                    // get by id
                    ExamTermStandard = _IExamService.GetExamTermStandardById(ExamTermStandardId);
                    if (ExamTermStandard != null)
                    {
                        // check if record deleted
                        if (examstd.IsDeleted == 1) // delete record
                        {
                            _IExamService.DeleteExamTermStandard(ExamTermStandard.ExamTermStandardId);
                            // TimeTable log Table
                            if (ExamTermStandardTable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ExamTermStandardTable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(ExamTermStandard.ExamTermStandardId, ExamTermStandardTable.TableId, logtypedelete.ActivityLogTypeId, user, String.Format("Delete ExamTermStandard with ExamTermStandardId:{0} and ExamTermId:{1}", ExamTermStandard.ExamTermStandardId, ExamTermStandard.ExamTermId), Request.Browser.Browser);
                            }
                        }
                    }
                }
            }

            return Json(new { status = "success", msg = "Data saved successfully" });
        }

        [HttpPost]
        public ActionResult GetExamTermActivitiesandStandards(string ExamTerm,int SessionId=0)
        {
            SchoolUser user = new SchoolUser();
            string Host = Request.Url.AbsolutePath;
            Host = Host.Split('/')[1];
            if (Request.Url.AbsoluteUri.Contains("localhost"))
                Host = "";
            string url = _WebHelper.GetStoreLocation() + Host + "/Home/Login";

            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new { status = "expire", url = url });

                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
                return Json(new { status = "expire", url = url });

            //decrypt id
            int ExamTermId = 0;
            if (!string.IsNullOrEmpty(ExamTerm))
                int.TryParse(_ICustomEncryption.base64d(ExamTerm), out ExamTermId);
            else
                return Json(new { status = "failed", msg = "No Exam Term found" });

            // get Exam Term Activities if has otherwise default
            IList<ExamActivityModel> ExamActivityList = new List<ExamActivityModel>();
            ExamActivityModel ExamActivity = new ExamActivityModel();
            // exam term activities
            var examactivities = _IExamService.GetAllExamActivities(ref count);
            var ExamTermActivitySkills = _IExamService.GetAllExamTermActivitySkills(ref count);
            var ExamTermActivityDetails = _IExamService.GetAllExamTermActivityDetails(ref count);
            var IsAnyTermMarksFilled = false;
            foreach (var item in examactivities)
            {
                ExamActivity = new ExamActivityModel();
                var examtermactivity = item.ExamTermActivities.Where(e => e.ExamTermId == ExamTermId).FirstOrDefault();
                if (examtermactivity != null)
                {
                    ExamActivity.ExamActivity = examtermactivity.ExamActivity.ExamActivity1;
                    ExamActivity.ExamActivityId = _ICustomEncryption.base64e(item.ExamActivityId.ToString());
                    ExamActivity.ExamTermActivityId = _ICustomEncryption.base64e(examtermactivity.ExamTermActivityId.ToString());
                    ExamActivity.IsTermEnd = examtermactivity.IsTermEnd == true ? true : false;
                    ExamActivity.IsDisabled = examtermactivity.ExamDateSheets.Count > 0 || examtermactivity.ExamTermActivitySkills.Count > 0 ? true : false;
                    ExamActivity.IsTermEndDisabled = false;
                    if (examtermactivity.IsTermEnd == true)
                    {
                        var IfExamTermActivitySkills = ExamTermActivitySkills.Where(f => f.ExamTermActivityId == examtermactivity.ExamTermActivityId).Select(f => f.ExamTermActivitySkillId).ToArray();
                        if (IfExamTermActivitySkills != null && IfExamTermActivitySkills.Count() > 0)
                        {
                            var IfExamTermActivityDetails = ExamTermActivityDetails.Where(f => IfExamTermActivitySkills.Contains((int)f.ExamTermActivitySkillsId));
                            if (IfExamTermActivityDetails.Count() > 0)
                            {
                                ExamActivity.IsTermEndDisabled = true;
                                IsAnyTermMarksFilled = true;
                            }
                        }
                    }
                }
                else
                {
                    ExamActivity.ExamActivity = item.ExamActivity1;
                    ExamActivity.ExamActivityId = _ICustomEncryption.base64e(item.ExamActivityId.ToString());
                    ExamActivity.ExamTermActivityId = "";
                    ExamActivity.IsTermEnd = false;
                    ExamActivity.IsDisabled = false;
                }

                ExamActivityList.Add(ExamActivity);
            }

            var GetAllExamResult = _IExamService.GetAllExamResults(ref count);
            IList<ExamDateSheetDetail> GetAllExamdateSheetDetails = new List<ExamDateSheetDetail>();

            var GetSession =new KSModel.Models.Session();
            if (SessionId > 0) 
                GetSession = _ICatalogMasterService.GetAllSessions(ref count).Where(f => f.SessionId == SessionId).FirstOrDefault();
            else
                GetSession = _ICatalogMasterService.GetAllSessions(ref count,Isdeafult:true).FirstOrDefault();

            var GetAllExamTermActivitiesOfExamTerm = _IExamService.GetAllExamTermActivities(ref count, ExamTermId: ExamTermId).Select(f=>f.ExamTermActivityId).ToArray();
            if (GetAllExamTermActivitiesOfExamTerm.Count() > 0)
            {
                var ExamDateSheetOfExamTermActivities = _IExamService.GetAllExamDateSheets(ref count).Where(f => GetAllExamTermActivitiesOfExamTerm.Contains((int)f.ExamTermActivityId)).Select(f => f.ExamDateSheetId).ToArray();
                if (ExamDateSheetOfExamTermActivities.Count()>0)
                {
                    GetAllExamdateSheetDetails = _IExamService.GetAllExamDateSheetDetails(ref count).Where(f => ExamDateSheetOfExamTermActivities.Contains((int)f.ExamDateSheetId)&& f.ExamDateSheet.FromDate>=GetSession.StartDate && f.ExamDateSheet.TillDate <= GetSession.EndDate).ToList();
                }
            }

            var GetAllExamTermActivitySkills = _IExamService.GetAllExamTermActivitySkills(ref count);
            var GetAllExamTermActivityDetails = _IExamService.GetAllExamTermActivityDetails(ref count);

            var SessionStudents = _IStudentService.GetAllStudentClasss(ref count).Where(m => m.SessionId == GetSession.SessionId);

                             // get Exam Term Standards if has otherwise default
             IList < ExamTermStandardModel> ExamTermStandardList = new List<ExamTermStandardModel>();
            ExamTermStandardModel ExamTermStandard = new ExamTermStandardModel();
            // exam term activities
            var standards = _ICatalogMasterService.GetAllStandardMasters(ref count).OrderBy(s => s.StandardIndex);
            foreach (var item in standards)
            {
                ExamTermStandard = new ExamTermStandardModel();
                ExamTermStandard.ExamTermStandardTitle = "";
                var IfExamStandardDateSheetSetail = GetAllExamdateSheetDetails.Where(f => f.StandardId == item.StandardId);
                var IfExamStandardDateSheetSetailIds = GetAllExamdateSheetDetails.Where(f => f.StandardId == item.StandardId).Select(f=>f.ExamDateSheetDetailId).ToArray();
                
                var StandardStudents = SessionStudents.Where(f => f.ClassId != null && f.ClassMaster.StandardId == item.StandardId).Select(f => f.StudentId).ToArray();
                
                if(IfExamStandardDateSheetSetailIds != null && IfExamStandardDateSheetSetailIds.Count()>0 && StandardStudents!=null && StandardStudents.Count()>0)
                {
                    var IfExamResults = GetAllExamResult.Where(f => IfExamStandardDateSheetSetailIds.Contains((int)f.ExamDateSheetDetailId));
                    if (IfExamResults.Count() > 0)
                    {
                        ExamTermStandard.IsStandardMarksFilled = true;
                        ExamTermStandard.ExamTermStandardTitle = "Please Clear Subject Marks";
                    }
                    var GetExamTermActivityIdForSkill = IfExamStandardDateSheetSetail.Select(f=>f.ExamDateSheet.ExamTermActivityId).Distinct().ToArray();
                    if (GetExamTermActivityIdForSkill != null && GetExamTermActivityIdForSkill.Count()>0)
                    {
                        var IfExamTermActivitySkills = GetAllExamTermActivitySkills.Where(f => GetExamTermActivityIdForSkill.Contains(f.ExamTermActivityId)
                        //== GetExamTermActivityIdForSkill
                         && StandardStudents.Contains(f.StudentId)).Select(f=>f.ExamTermActivitySkillId).ToArray();

                        if(IfExamTermActivitySkills!=null && IfExamTermActivitySkills.Count() > 0)
                        {
                            var IfExamtermActivityDetails = GetAllExamTermActivityDetails.Where(f => IfExamTermActivitySkills.Contains((int)f.ExamTermActivitySkillsId)).Count();
                            if (IfExamtermActivityDetails > 0)
                            {
                                ExamTermStandard.IsStandardMarksFilled = true;
                                if(ExamTermStandard.ExamTermStandardTitle!="")
                                    ExamTermStandard.ExamTermStandardTitle = "Please Clear Subject Marks and Skill Marks First";
                                else
                                    ExamTermStandard.ExamTermStandardTitle = "Please Clear Skill Marks First";
                            }
                        }

                    }
                }

                var examtermstandard = item.ExamTermStandards.Where(e => e.ExamTermId == ExamTermId).FirstOrDefault();
                if (examtermstandard != null)
                {
                    ExamTermStandard.StandardName = item.Standard;
                    ExamTermStandard.ExamTermStandardId = _ICustomEncryption.base64e(examtermstandard.ExamTermStandardId.ToString());
                    ExamTermStandard.StandardId = _ICustomEncryption.base64e(examtermstandard.ExamStandardId.ToString());
                }
                else
                {
                    ExamTermStandard.StandardName = item.Standard;
                    ExamTermStandard.ExamTermStandardId = "";
                    ExamTermStandard.StandardId = _ICustomEncryption.base64e(item.StandardId.ToString());
                }

                ExamTermStandardList.Add(ExamTermStandard);
            }

            return Json(new { status = "success", IsAnyTermMarksFilled= IsAnyTermMarksFilled, collection1 = ExamActivityList, collection2 = ExamTermStandardList });
        }

        #endregion

        public ActionResult Alumni()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                string phprequesturl = _WebHelper.GetStoreLocation() + "module/index.php/alumni/manage_alumni";
                ViewBag.url = phprequesturl;
                return View();

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        #endregion

        #region Ajax Methods

        #region Marks Pattern

        //Validation for Creating and Updating Marks Pattern
        public string checkMarksPatternvalidationerror(MarksPatternModel model)
        {
            StringBuilder validation = new StringBuilder();
            if (model.GradePatternId == null || model.GradePatternId == 0)
                validation.Append("Grade Pattern is requiredε");
            if (model.MaxMarks == null || model.MaxMarks == 0)
                validation.Append("Max Marks is requiredε");
            if (model.PassMarks == null || model.PassMarks == 0)
                validation.Append("Pass Marks is requiredε");
            if (model.PassMarks > model.MaxMarks)
                validation.Append("Pass Marks can not be greater than Max Marksε");
            var markspattern = _IExamService.GetAllMarksPatternDetailsnotrack();
            if (markspattern != null && (model.MarksPatternId == null || model.MarksPatternId == 0))
            {
                if ((model.GradePatternId != null || model.GradePatternId != 0) && (model.MaxMarks != null || model.MaxMarks != 0) && (model.PassMarks != null || model.PassMarks != 0))
                    markspattern = (from mp in markspattern where mp.GradePatternId == model.GradePatternId && mp.MaxMarks == model.MaxMarks && mp.PassMarks == model.PassMarks select mp).ToList();
                if (markspattern.Count > 0)
                {
                    validation.Append("Same marks pattern already existsε");
                }
            }
            else
            {
                if ((model.GradePatternId != null || model.GradePatternId != 0) && (model.MaxMarks != null || model.MaxMarks != 0) && (model.PassMarks != null || model.PassMarks != 0))
                {
                    var marks_pattern = (from mp in markspattern where mp.GradePatternId == model.GradePatternId && mp.MaxMarks == model.MaxMarks && mp.PassMarks == model.PassMarks select mp).FirstOrDefault();
                    if (marks_pattern != null)
                        if (marks_pattern.MarksPatternId != model.MarksPatternId)
                        {
                            validation.Append("Same marks pattern already existsε");
                        }
                }
            }


            return validation.ToString();
        }
        //Save and Update Marks Pattern
        public ActionResult SaveNewMarksPattern(MarksPatternModel model)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                // check id exist
                if (!string.IsNullOrEmpty(model.EncMarksPatternId))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(model.EncMarksPatternId);
                    int markspatern_id = 0;
                    if (int.TryParse(qsid, out markspatern_id))
                    {
                        model.MarksPatternId = markspatern_id;
                    }
                }

                string validation = checkMarksPatternvalidationerror(model);
                if (string.IsNullOrEmpty(validation))
                {
                    // and add new Date Sheet
                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    // Object
                    MarksPattern MarksPattern = new MarksPattern();
                    MarksPattern.GradePatternId = model.GradePatternId > 0 ? model.GradePatternId : null;
                    MarksPattern.MaxMarks = model.MaxMarks;
                    MarksPattern.PassMarks = model.PassMarks;
                    MarksPattern.IAMaxMarks = model.IAMaxMarks;
                    MarksPattern.IAPassMarks = model.IAPassMarks;
                    if (model.MarksPatternId != null && model.MarksPatternId != 0)
                    {
                        // check authorization
                        //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "MarksPattern", "Edit Record");
                        //if (!isauth)
                        //    return RedirectToAction("AccessDenied", "Error");

                        MarksPattern.MarksPatternId = (int)model.MarksPatternId;
                        _IExamService.UpdateMarksPattern(MarksPattern);
                        var admin = true;
                        if (user.ContactType.ContactType1 == "Admin")
                            admin = true;
                        else
                            admin = false;
                        MarksPatternModel MarksPatternModel = new MarksPatternModel();

                        MarksPatternModel = new MarksPatternModel();
                        MarksPatternModel.MaxMarks = MarksPattern.MaxMarks;
                        MarksPatternModel.PassMarks = MarksPattern.PassMarks;
                        MarksPatternModel.IAMaxMarks = MarksPattern.IAMaxMarks;
                        MarksPatternModel.IAPassMarks = MarksPattern.IAPassMarks;
                        var gradepattern = _IExamService.GetGradePatternById((int)MarksPattern.GradePatternId);
                        if (gradepattern != null)
                            MarksPatternModel.GradePattern = gradepattern.GradePattern1;
                        //MarksPatternModel.MarksPatternId = MarksPattern.MarksPatternId;
                        MarksPatternModel.EncMarksPatternId = _ICustomEncryption.base64e(MarksPattern.MarksPatternId.ToString());
                        MarksPatternModel.GradePatternId = MarksPattern.GradePatternId;
                        var examdatesheetdetail = _IExamService.GetAllExamDateSheetDetails(ref count, MarksPatternId: model.MarksPatternId);
                        if (examdatesheetdetail.Count > 0)
                            MarksPatternModel.MarksPatternUsed = false;
                        else
                            MarksPatternModel.MarksPatternUsed = true;
                        // TimeTable log Table
                        var MarksPatterntable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "MarksPattern").FirstOrDefault();
                        // TimeTable log Table
                        if (MarksPatterntable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, MarksPatterntable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(MarksPattern.MarksPatternId, MarksPatterntable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add MarksPattern with ExamDateSheetId:{0} and MaxMarks:{1}", MarksPattern.MarksPatternId, MarksPattern.MaxMarks), Request.Browser.Browser);
                        }
                        return Json(new
                        {
                            status = "success",
                            msg = "Marks Pattern has been successfully Updated",
                            todo = "update",
                            admin = admin,
                            data = MarksPatternModel
                        });
                    }
                    else
                    {
                        // check authorization
                        //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "MarksPattern", "Add Record");
                        //if (!isauth)
                        //    return RedirectToAction("AccessDenied", "Error");

                        _IExamService.InsertMarksPattern(MarksPattern);
                        var admin = true;
                        if (user.ContactType.ContactType1 == "Admin")
                            admin = true;
                        else
                            admin = false;
                        MarksPatternModel MarksPatternModel = new MarksPatternModel();

                        MarksPatternModel = new MarksPatternModel();
                        MarksPatternModel.MaxMarks = MarksPattern.MaxMarks;
                        MarksPatternModel.PassMarks = MarksPattern.PassMarks;
                        MarksPatternModel.IAMaxMarks = MarksPattern.IAMaxMarks;
                        MarksPatternModel.IAPassMarks = MarksPattern.IAPassMarks;
                        var gradepattern = _IExamService.GetGradePatternById((int)MarksPattern.GradePatternId);
                        if (gradepattern != null)
                            MarksPatternModel.GradePattern = gradepattern.GradePattern1;
                        //MarksPatternModel.MarksPatternId = MarksPattern.MarksPatternId;
                        MarksPatternModel.EncMarksPatternId = _ICustomEncryption.base64e(MarksPattern.MarksPatternId.ToString());
                        MarksPatternModel.GradePatternId = MarksPattern.GradePatternId;
                        //var examdatesheetdetail = _IExamService.GetAllExamDateSheetDetails(ref count, MarksPatternId: model.MarksPatternId);
                        //if (examdatesheetdetail.Count > 0)
                        //    MarksPatternModel.MarksPatternUsed = true;
                        //else
                        MarksPatternModel.MarksPatternUsed = false;
                        // TimeTable log Table
                        var MarksPatterntable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "MarksPattern").FirstOrDefault();
                        // TimeTable log Table
                        if (MarksPatterntable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, MarksPatterntable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(MarksPattern.MarksPatternId, MarksPatterntable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add MarksPattern with ExamDateSheetId:{0} and MaxMarks:{1}", MarksPattern.MarksPatternId, MarksPattern.MaxMarks), Request.Browser.Browser);
                        }

                        return Json(new
                        {
                            status = "success",
                            msg = "Marks Pattern has been successfully saved",
                            admin = admin,
                            data = MarksPatternModel
                        });
                    }
                }
                else
                    return Json(new
                    {
                        status = "validationfailed",
                        msg = "NotValid",
                        data = validation
                    });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }

        //Delete MarksPattern 
        public ActionResult DeleteMarksPattern(string markspatternid)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "MarksPattern", "Delete Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }

            try
            {
                int MarksPatternId = 0;
                // check id exist
                if (!string.IsNullOrEmpty(markspatternid))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(markspatternid);
                    int markspatern_id = 0;
                    if (int.TryParse(qsid, out markspatern_id))
                    {
                        MarksPatternId = markspatern_id;
                    }
                }
                // int MarksPatternId = Convert.ToInt32(markspatternid);
                // Log delete Type
                var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
                // classPeriodDetail log Table
                var MarksPatternIdtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "MarksPatternId").FirstOrDefault();

                // Get MarksPattern If Exist
                var MarksPattern = _IExamService.GetMarksPatternById(MarksPatternId, false);


                if (MarksPattern != null)
                {
                    _IExamService.DeleteMarksPattern(MarksPatternId);
                    if (MarksPatternIdtable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, MarksPatternIdtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog(MarksPatternId, MarksPatternIdtable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Delete MarksPattern with MarksPatternId:{0}", MarksPatternId), Request.Browser.Browser);
                    }

                    return Json(new
                    {
                        status = "success",
                        msg = "MarksPattern Deleted successfully",

                    });
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        msg = "No Record Found to be Deleted"
                    });
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                });
            }

        }

        #endregion

        #region Grade Pattern

        //get list if gradepatterndetail list
        public ActionResult getgradepatterndetaillist(string gradepatternid)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                int GradePatternId = Convert.ToInt32(gradepatternid);
                GradePatternModel model = new GradePatternModel();
                GradePatternModel GradePatternModel = new GradePatternModel();
                //get all grade pattern
                var GradePattern = _IExamService.GetGradePatternById(GradePatternId);
                //get grade pattern detail
                var GradePatternDetail = _IExamService.GetAllGradePatternDetails(ref count, GradePatternId: GradePatternId).OrderBy(p => p.EndDate != null).ThenByDescending(p => p.EndDate).ToList();

                if (GradePatternDetail != null)
                {
                    foreach (var gpd in GradePatternDetail)
                    {
                        GradePatternModel = new GradePatternModel();
                        GradePatternModel.GradePatternId = GradePatternId;
                        GradePatternModel.GradePattern = GradePattern.GradePattern1;
                        var boardlist = _ICatalogMasterService.GetBoardById((int)GradePattern.BoardId);
                        if (boardlist != null)
                            GradePatternModel.Board = boardlist.Board1;
                        //GradePatternModel.GradePatternDetailId = gpd.GradePatternDetailId;
                        GradePatternModel.EncGradePatternDetailId = _ICustomEncryption.base64e(gpd.GradePatternDetailId.ToString());
                        GradePatternModel.StartingMarks = gpd.StartingMarks;
                        GradePatternModel.EndingMarks = gpd.EndingMarks;
                        GradePatternModel.GradeId = gpd.GradeId;
                        var gradelist = _IExamService.GetGradeById((int)gpd.GradeId);
                        if (gradelist != null)
                            GradePatternModel.Grade = gradelist.Grade1;
                        GradePatternModel.EffectiveDate = ConvertDate((DateTime)gpd.EffectiveDate);
                        if (gpd.EndDate != null)
                            GradePatternModel.EndDate = ConvertDate((DateTime)gpd.EndDate);
                        else
                            GradePatternModel.EndDate = "";
                        if (GradePatternDetail.Count > 1)
                            GradePatternModel.hasgradepatterndetial = 1;
                        model.GradePatternList.Add(GradePatternModel);
                    }
                }

                return Json(new
                {
                    status = "success",
                    data = model.GradePatternList
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                });
            }

        }
        //Validation for Creating and Updating Grade Pattern
        public string checkGradePatternDetailvalidationerror(GradePatternModel model)
        {
            StringBuilder validation = new StringBuilder();
            if (model.GradePatternId == null || model.GradePatternId == 0)
                validation.Append("Grade Pattern is requiredε");

            if (model.GradeId == null || model.GradeId == 0)
                validation.Append("Grade is requiredε");

            if (!model.StartingMarks.HasValue)
                validation.Append("Starting Marks is requiredε");

            if (!model.EndingMarks.HasValue)
                validation.Append("Ending Marks is requiredε");

            if (model.StartingMarks.HasValue && model.EndingMarks.HasValue)
            {
                if (model.StartingMarks > model.EndingMarks)
                    validation.Append("Starting Marks should be less than from Ending Marksε");
            }

            if (String.IsNullOrEmpty(model.EffectiveDate))
                validation.Append("Effective Date is requiredε");
            //else
            //    if (Convert.ToDateTime(model.EffectiveDate) < DateTime.Now.Date)
            //        validation.Append("Can't save Grade Pattern Detail for past datesε");

            if (model.GradePatternId > 0 && model.GradeId > 0 && !String.IsNullOrEmpty(model.EffectiveDate))
            {
                var gradepatterndetails = _IExamService.GetAllGradePatternDetailsnotrack();
                gradepatterndetails = gradepatterndetails.Where(g => g.GradePatternId == model.GradePatternId
                    && g.GradePatternDetailId != model.GradePatternDetailId).ToList();

                var grade_pattern = gradepatterndetails.Where(gpd => gpd.GradeId == model.GradeId && gpd.EffectiveDate <= Convert.ToDateTime(model.EffectiveDate)
                                   && ((gpd.EndDate != null && gpd.EndDate >= Convert.ToDateTime(model.EffectiveDate))
                                   || gpd.EndDate == null)).ToList();
                if (grade_pattern.Count > 0)
                    validation.Append("Grade already exist ε");

                var grade_patterndetail = gradepatterndetails.Where(gpd => gpd.EffectiveDate <= Convert.ToDateTime(model.EffectiveDate)
                                        && ((gpd.EndDate != null && gpd.EndDate >= Convert.ToDateTime(model.EffectiveDate))
                                        || gpd.EndDate == null)).ToList();
                foreach (var item in grade_patterndetail)
                {
                    if (model.GradePatternDetailId > 0)
                        if (item.GradePatternDetailId == (int)model.GradePatternDetailId)
                            continue;

                    //if ((item.StartingMarks >= model.StartingMarks && item.StartingMarks <= model.EndingMarks)
                    //    || (item.EndingMarks >= model.StartingMarks && item.EndingMarks <= model.EndingMarks)
                    //    || (model.StartingMarks >= item.StartingMarks && model.StartingMarks <= item.EndingMarks)
                    //    || (model.EndingMarks >= item.StartingMarks && model.EndingMarks <= item.EndingMarks))
                    //{
                    //    validation.Append("Starting and Ending Marks already exist ε");
                    //    break;
                    //}
                }
            }

            return validation.ToString();
        }
        // Save and Update Grade Pattern
        public ActionResult SaveUpdateGradePatternDetail(GradePatternModel model)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                // check id exist
                if (!string.IsNullOrEmpty(model.EncGradePatternDetailId))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(model.EncGradePatternDetailId);
                    int gradepatterndetailid_id = 0;
                    if (int.TryParse(qsid, out gradepatterndetailid_id))
                    {
                        model.GradePatternDetailId = gradepatterndetailid_id;
                    }
                }

                string validation = checkGradePatternDetailvalidationerror(model);
                if (string.IsNullOrEmpty(validation))
                {
                    // and add new Date Sheet
                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    //Object Grade Pattern Detail
                    GradePatternDetail GradePatternDetail = new GradePatternDetail();
                    GradePatternDetail.StartingMarks = model.StartingMarks;
                    GradePatternDetail.EndingMarks = model.EndingMarks;
                    GradePatternDetail.GradeId = model.GradeId;
                    GradePatternDetail.EffectiveDate = Convert.ToDateTime(model.EffectiveDate);
                    GradePatternDetail.GradePatternId = model.GradePatternId;
                    if (model.GradePatternDetailId != null && model.GradePatternDetailId != 0)
                    {
                        // check authorization
                        //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "GradePattern", "Edit Record");
                        //if (!isauth)
                        //    return RedirectToAction("AccessDenied", "Error");
                        //Update Grade Pattern Details
                        GradePatternDetail.GradePatternId = model.GradePatternId;
                        GradePatternDetail.GradePatternDetailId = (int)model.GradePatternDetailId;
                        _IExamService.UpdateGradePatternDetail(GradePatternDetail);

                        //pass data to view
                        var admin = true;
                        if (user.ContactType.ContactType1 == "Admin")
                            admin = true;
                        else
                            admin = false;
                        GradePatternModel GradePatternModel = new GradePatternModel();

                        GradePatternModel.GradePatternId = model.GradePatternId;
                        var gradepattern = _IExamService.GetGradePatternById((int)model.GradePatternId, false);
                        if (gradepattern != null)
                        {
                            GradePatternModel.GradePattern = gradepattern.GradePattern1;
                            var boardlist = _ICatalogMasterService.GetBoardById((int)gradepattern.BoardId);
                            if (boardlist != null)
                                GradePatternModel.Board = boardlist.Board1;
                        }
                        //GradePatternModel.GradePatternDetailId = GradePatternDetail.GradePatternDetailId;
                        GradePatternModel.EncGradePatternDetailId = _ICustomEncryption.base64e(GradePatternDetail.GradePatternDetailId.ToString());
                        GradePatternModel.StartingMarks = (decimal)GradePatternDetail.StartingMarks;
                        GradePatternModel.EndingMarks = (decimal)GradePatternDetail.EndingMarks;
                        GradePatternModel.GradeId = (int)GradePatternDetail.GradeId;
                        var gradelist = _IExamService.GetGradeById((int)GradePatternDetail.GradeId);
                        if (gradelist != null)
                            GradePatternModel.Grade = gradelist.Grade1;
                        GradePatternModel.EffectiveDate = ConvertDate((DateTime)GradePatternDetail.EffectiveDate);
                        if (GradePatternDetail.EndDate != null)
                            GradePatternModel.EndDate = ConvertDate((DateTime)GradePatternDetail.EndDate);
                        //get grade pattern detail
                        var grade_pattern_detail = _IExamService.GetAllGradePatternDetails(ref count, GradePatternId: model.GradePatternId).ToList();
                        if (grade_pattern_detail.Count > 1)
                            GradePatternModel.hasgradepatterndetial = 1;

                        // TimeTable log Table
                        var GradePatternDetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "GradePatternDetail").FirstOrDefault();
                        // TimeTable log Table
                        if (GradePatternDetailtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, GradePatternDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog((int)model.GradePatternId, GradePatternDetailtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add GradePatternDetail with GradePatternDetailId:{0}", model.GradePatternId), Request.Browser.Browser);
                        }
                        return Json(new
                        {
                            status = "success",
                            msg = "Grade Pattern Detail has been successfully Updated",
                            todo = "update",
                            admin = admin,
                            data = GradePatternModel
                        });
                    }
                    else
                    {
                        // check authorization
                        //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "GradePattern", "Add Record");
                        //if (!isauth)
                        //    return RedirectToAction("AccessDenied", "Error");
                        //Insert Grade Pattern Details
                        _IExamService.InsertGradePatternDetail(GradePatternDetail);
                        //pass data to view
                        var admin = true;
                        if (user.ContactType.ContactType1 == "Admin")
                            admin = true;
                        else
                            admin = false;
                        GradePatternModel GradePatternModel = new GradePatternModel();

                        GradePatternModel.GradePatternId = model.GradePatternId;
                        var gradepattern = _IExamService.GetGradePatternById((int)model.GradePatternId, false);
                        if (gradepattern != null)
                        {
                            GradePatternModel.GradePattern = gradepattern.GradePattern1;
                            var boardlist = _ICatalogMasterService.GetBoardById((int)gradepattern.BoardId);
                            if (boardlist != null)
                                GradePatternModel.Board = boardlist.Board1;
                        }
                        //GradePatternModel.GradePatternDetailId = GradePatternDetail.GradePatternDetailId;
                        GradePatternModel.EncGradePatternDetailId = _ICustomEncryption.base64e(GradePatternDetail.GradePatternDetailId.ToString());
                        GradePatternModel.StartingMarks = (decimal)GradePatternDetail.StartingMarks;
                        GradePatternModel.EndingMarks = (decimal)GradePatternDetail.EndingMarks;
                        GradePatternModel.GradeId = (int)GradePatternDetail.GradeId;
                        var gradelist = _IExamService.GetGradeById((int)GradePatternDetail.GradeId);
                        if (gradelist != null)
                            GradePatternModel.Grade = gradelist.Grade1;
                        GradePatternModel.EffectiveDate = ConvertDate((DateTime)GradePatternDetail.EffectiveDate);
                        if (GradePatternDetail.EndDate != null)
                            GradePatternModel.EndDate = ConvertDate((DateTime)GradePatternDetail.EndDate);
                        //get grade pattern detail
                        var grade_pattern_detail = _IExamService.GetAllGradePatternDetails(ref count, GradePatternId: model.GradePatternId).ToList();
                        if (grade_pattern_detail.Count > 1)
                            GradePatternModel.hasgradepatterndetial = 1;


                        // TimeTable log Table
                        var GradePatternDetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "GradePatternDetail").FirstOrDefault();
                        // TimeTable log Table
                        if (GradePatternDetailtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, GradePatternDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog((int)model.GradePatternId, GradePatternDetailtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add GradePatternDetail with GradePatternDetailId:{0}", model.GradePatternId), Request.Browser.Browser);
                        }
                        return Json(new
                        {
                            status = "success",
                            msg = "Grade Pattern Detail has been successfully saved",
                            admin = admin,
                            data = GradePatternModel
                        });
                    }
                }
                else
                    return Json(new
                    {
                        status = "validationfailed",
                        msg = "NotValid",
                        data = validation
                    });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }

        //Delete GradePattern 
        public ActionResult DeleteGradePatternDetail(string gardepatterndetailid)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "GradePattern", "Delete Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }

            try
            {
                int GradePatternDetailId = 0;
                // check id exist
                if (!string.IsNullOrEmpty(gardepatterndetailid))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(gardepatterndetailid);

                    if (int.TryParse(qsid, out GradePatternDetailId))
                    {
                    }
                }

                // int GradePatternDetailId = Convert.ToInt32(gardepatterndetailid);
                // Log delete Type
                var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
                // classPeriodDetail log Table
                var GradePatterntable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "GradePattern").FirstOrDefault();

                // Get GradePatternDetail If Exist
                var GradePatternDetail = _IExamService.GetGradePatternDetailById(GradePatternDetailId, false);

                if (GradePatternDetail != null)
                {
                    //Delete Grade Pattern Detail
                    _IExamService.DeleteGradePatternDetail(GradePatternDetailId);

                    if (GradePatterntable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, GradePatterntable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog(GradePatternDetailId, GradePatterntable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Delete GradePatternDetail with GradePatternDetailId:{0}", GradePatternDetailId), Request.Browser.Browser);
                    }

                    return Json(new
                    {
                        status = "success",
                        msg = "Grade Pattern Detail Deleted successfully",

                    });
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        msg = "No Record Found to be Deleted"
                    });
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                });
            }

        }
        //Validation for Creating and Updating Grade Pattern
        public string checkGradePatternvalidationerror(GradePatternModel model)
        {
            StringBuilder validation = new StringBuilder();
            if (String.IsNullOrEmpty(model.GradePattern))
                validation.Append("Grade Pattern is requiredε");
            else
                if (String.IsNullOrEmpty(model.GradePattern.Trim()))
                    validation.Append("Grade Pattern is requiredε");
            //get gradepattern
            var gradepattern = _IExamService.GetAllGradePatterns(ref count);
            if (gradepattern != null && !String.IsNullOrEmpty(model.GradePattern))
            {
                gradepattern = (from gp in gradepattern where gp.GradePattern1.ToLower() == model.GradePattern.ToLower() && gp.GradePatternId != model.GradePatternId select gp).ToList();
                if (gradepattern.Count > 0)
                {
                    validation.Append("Already have same grade patternε");
                }
            }

            return validation.ToString();
        }
        //save grade Pattern
        public ActionResult SaveGradePattern(GradePatternModel model)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Master", "GradePattern", "Add Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }

            try
            {
                string validation = checkGradePatternvalidationerror(model);
                if (string.IsNullOrEmpty(validation))
                {
                    // and add new Date Sheet


                    // Object Grade Pattern
                    GradePattern GradePattern = new GradePattern();
                    GradePattern.GradePattern1 = model.GradePattern;
                    //MarksPattern.GradePatternId = model.GradePatternId > 0 ? model.GradePatternId : null;
                    var board = _ICatalogMasterService.GetAllBoards(ref count).Where(m => m.IsDefault == true).FirstOrDefault();
                    if (board != null)
                        GradePattern.BoardId = board.BoardId;
                    ///update
                    if (model.GradePatternId > 0)
                    {
                        // Activity log type add
                        var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                        var gradepattern = _IExamService.GetGradePatternById((int)model.GradePatternId, true);
                        gradepattern.GradePattern1 = model.GradePattern;
                        //Update garde Pattern
                        _IExamService.UpdateGradePattern(gradepattern);
                        // TimeTable log Table
                        var GradePatterntable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "GradePattern").FirstOrDefault();
                        // TimeTable log Table
                        if (GradePatterntable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, GradePatterntable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(gradepattern.GradePatternId, GradePatterntable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add GradePattern with GradePatternId:{0}", GradePattern.GradePatternId), Request.Browser.Browser);
                        }

                        var gradePattern = _IExamService.GetAllGradePatterns(ref count);
                        IList<SelectListItem> gradepatternlist = new List<SelectListItem>();
                        gradepatternlist.Add(new SelectListItem { Selected = false, Text = "Add New", Value = "" });
                        foreach (var gpl in gradePattern)
                        {

                            if (gpl.GradePatternId == gradepattern.GradePatternId)
                            {
                                gradepatternlist.Add(new SelectListItem { Selected = true, Text = gpl.GradePattern1, Value = gpl.GradePatternId.ToString() });
                            }
                            else
                            {
                                gradepatternlist.Add(new SelectListItem { Selected = false, Text = gpl.GradePattern1, Value = gpl.GradePatternId.ToString() });
                            }
                        }
                        return Json(new
                        {
                            status = "success",
                            msg = "Grade Pattern has been updated successfully ",
                            GradePatternId = gradepattern.GradePatternId,
                            //GradePattern = GradePattern.GradePattern1,
                            SelectList = gradepatternlist
                        });
                    }
                    else
                    {
                        // Activity log type add
                        var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                        //Insert garde Pattern
                        _IExamService.InsertGradePattern(GradePattern);
                        // TimeTable log Table
                        var GradePatterntable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "GradePattern").FirstOrDefault();
                        // TimeTable log Table
                        if (GradePatterntable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, GradePatterntable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(GradePattern.GradePatternId, GradePatterntable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add GradePattern with GradePatternId:{0}", GradePattern.GradePatternId), Request.Browser.Browser);
                        }
                        var gradePattern = _IExamService.GetAllGradePatterns(ref count);
                        IList<SelectListItem> gradepatternlist = new List<SelectListItem>();
                        gradepatternlist.Add(new SelectListItem { Selected = false, Text = "Add New", Value = "" });
                        foreach (var gpl in gradePattern)
                        {

                            if (gpl.GradePatternId == GradePattern.GradePatternId)
                            {
                                gradepatternlist.Add(new SelectListItem { Selected = true, Text = gpl.GradePattern1, Value = gpl.GradePatternId.ToString() });
                            }
                            else
                            {
                                gradepatternlist.Add(new SelectListItem { Selected = false, Text = gpl.GradePattern1, Value = gpl.GradePatternId.ToString() });
                            }
                        }

                        return Json(new
                        {
                            status = "success",
                            msg = "Grade Pattern has been successfully saved",
                            GradePatternId = GradePattern.GradePatternId,
                            //GradePattern = GradePattern.GradePattern1
                            SelectList = gradepatternlist
                        });
                    }
                }

                else
                    return Json(new
                    {
                        status = "validationfailed",
                        msg = "NotValid",
                        data = validation
                    });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }
        //Set Grade Pattern End date
        public ActionResult CloseGradePattern(string gradepatterndetailid, string EndDate)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {

                int GradePatternDetailId = 0;
                // check id exist
                if (!string.IsNullOrEmpty(gradepatterndetailid))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(gradepatterndetailid);

                    if (int.TryParse(qsid, out GradePatternDetailId))
                    {
                    }
                }
                // int GradePatternDetailId = Convert.ToInt32(gradepatterndetailid);
                DateTime ClosingDate = Convert.ToDateTime(EndDate);
                // Log delete Type
                var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                // classPeriodDetail log Table
                var GradePatternDetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "GradePatternDetail").FirstOrDefault();
                // Get GradePAttern Detail 
                var GradePatternDetail = _IExamService.GetGradePatternDetailById(GradePatternDetailId, false);


                if (GradePatternDetail != null)
                {

                    if (ClosingDate >= (DateTime)GradePatternDetail.EffectiveDate)
                    {
                        GradePatternDetail.GradePatternDetailId = GradePatternDetailId;
                        GradePatternDetail.EndDate = ClosingDate;
                        _IExamService.UpdateGradePatternDetail(GradePatternDetail);
                        if (GradePatternDetailtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, GradePatternDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(GradePatternDetail.GradePatternDetailId, GradePatternDetailtable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update GradePatternDetail with GradePatternDetailId:{0}", GradePatternDetail.GradePatternDetailId), Request.Browser.Browser);
                        }

                        return Json(new
                        {
                            status = "success",
                            msg = "Grade Pattern Detail closed successfully",
                            data = ConvertDate(GradePatternDetail.EndDate.Value)
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            msg = "End date can't be less than Effective date",
                        });
                    }
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        msg = "You can't close this Grade Pattern Detail"
                    });
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                });
            }

        }

        //Set Grade Pattern End date
        public ActionResult GetGradePattern(string gradepatternId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {

                int GradePatternId = Convert.ToInt32(gradepatternId);
                // Get GradePAttern Detail 
                var GradePatternDetail = _IExamService.GetAllGradePatternDetails(ref count, GradePatternId: GradePatternId);
                GradePatternDetail = GradePatternDetail.Where(g => g.EffectiveDate <= DateTime.Now.Date &&
                    ((g.EndDate == null) || (g.EndDate != null && g.EndDate >= DateTime.Now.Date)))
                    .OrderBy(g => g.EffectiveDate).ThenBy(g => g.GradeId).ToList();

                if (GradePatternDetail.Count > 0)
                {
                    var gradepatterndetail = new GradePatternModel();
                    var gradepatternlist = new List<GradePatternModel>();
                    foreach (var gpl in GradePatternDetail)
                    {
                        gradepatterndetail = new GradePatternModel();
                        gradepatterndetail.Grade = gpl.Grade.Grade1;
                        gradepatterndetail.StartingMarks = gpl.StartingMarks;
                        gradepatterndetail.EndingMarks = gpl.EndingMarks;
                        gradepatternlist.Add(gradepatterndetail);
                    }

                    return Json(new
                    {
                        status = "success",
                        List = gradepatternlist
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        msg = "Grade Pattern has no detail"
                    });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                });
            }

        }
        #endregion
        #endregion
    }
}