﻿using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using BAL.BAL.Common;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.StaffModule;
using DAL.DAL.UserModule;
using DAL.DAL.SettingService;
using DAL.DAL.StudentModule;
using KSModel.Models;
using ViewModel.ViewModel.Leave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using DAL.DAL.ExaminationModule;
using DAL.DAL.DocumentModule;
using DAL.DAL.Kendo;
using ViewModel.ViewModel.Common;
using Newtonsoft.Json;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using ViewModel.ViewModel.Student;
using DAL.DAL.GuardianModule;
using ViewModel.ViewModel.Staff;
using System.Text;
using Newtonsoft.Json.Linq;
using ViewModel.ViewModel.Security;

namespace KS_SmartChild.Controllers
{
    public class CommonController : Controller
    {
        #region fields

        public int count = 0;
        private readonly IConnectionService _IConnectionService;
        private readonly ILogService _Logger;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IWebHelper _WebHelper;
        private readonly IUserService _IUserService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly ISubjectService _ISubjectService;
        private readonly IStaffService _IStaffService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly IDocumentService _IDocumentService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IStudentService _IStudentService;
        private readonly IExportHelper _IExportHelper;
        private readonly IGuardianService _IGuardianService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICommonMethodService _ICommonMethodService;
        private readonly ISMSSender _ISMSSender;
        #endregion

        #region ctor

        public CommonController(IConnectionService IConnectionService,
            ILogService Logger,
            IAddressMasterService IAddressMasterService,
            ICatalogMasterService ICatalogMasterService,
            IUserService IUserService, IExportHelper IExportHelper,
            ICustomEncryption ICustomEncryption,
            IWebHelper WebHelper,
            ISubjectService ISubjectService,
            IStaffService IStaffService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            IDocumentService IDocumentService,
            ISchoolSettingService ISchoolSettingService,
            IStudentService IStudentService,
            IGuardianService IGuardianService, IUserAuthorizationService IUserAuthorizationService,
            ICommonMethodService ICommonMethodService, ISMSSender ISMSSender)
        {
            this._IConnectionService = IConnectionService;
            this._Logger = Logger;
            this._IAddressMasterService = IAddressMasterService;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IUserService = IUserService;
            this._ICustomEncryption = ICustomEncryption;
            this._WebHelper = WebHelper;
            this._ISubjectService = ISubjectService;
            this._IStaffService = IStaffService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._IActivityLogService = IActivityLogService;
            this._IDocumentService = IDocumentService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._IStudentService = IStudentService;
            this._IExportHelper = IExportHelper;
            this._IGuardianService = IGuardianService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICommonMethodService = ICommonMethodService;
            this._ISMSSender = ISMSSender;
        }

        #endregion

        #region utility
        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }
        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }
        public string ConvertDateForKendo(DateTime Date)
        {
            string sdisplayTime = Date.ToString("MM/dd/yyyy");
            return sdisplayTime;
        }

        public string FormatDate(string Date)
        {
            string sdisplayTime = Date.Trim('"');
            var dd = Date.Split('/');
            sdisplayTime = dd[1] + "-" + dd[0] + "-" + dd[2];
            return sdisplayTime;
        }

        public DateTime FormatDatedatetime(string Date)
        {
            string sdisplayTime = Date.Trim('"');
            var dd = Date.Split('/');
            sdisplayTime = dd[1] + "-" + dd[0] + "-" + dd[2];
            return Convert.ToDateTime(sdisplayTime);
        }
        #endregion

        #region Methods
        
        public string GetSetCurrentSchoolDataBase(HttpRequestBase Request, HttpSessionStateBase Session, HttpServerUtilityBase Server)
        {
            try
            {




                // get subdomain
                string User = "";
                string Subdomain = Request.Url.AbsolutePath;
                Subdomain = Subdomain.Split('/')[1];
                // API url
                //string MainUrl = "";
                //string suburl = Convert.ToString(WebConfigurationManager.AppSettings["SubUrl"]);
                //string synccompany = Convert.ToString(WebConfigurationManager.AppSettings["syncapiname"]);
                //string syncsub = Convert.ToString(WebConfigurationManager.AppSettings["syncsub"]);
                //string syncext = Convert.ToString(WebConfigurationManager.AppSettings["syncext"]);
                //string APIName = Convert.ToString(WebConfigurationManager.AppSettings["APIName"]);

                if (Request.Url.AbsoluteUri.Contains("localhost") || Request.Url.AbsoluteUri.Contains("172.16.1.2"))
                {
                    User = Convert.ToString(WebConfigurationManager.AppSettings["User"]);
                    // MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                    //  // User = "stroop";
                    //  // User="atsderabassi";
                    //   User = "Dev-KS_School";
                    //  //User = "3is_demo";
                    //  User = "demo";
                    //  // User = "kschild";
                    //  //User = "bpsambala";
                    //  //User = "hgdemo";
                    //  //User = "dtsk8";
                    //  //User = "";
                    // MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
                    // MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;

                    // MainUrl = "http://app.3ischools.com/api_v1-11" + "/" + suburl;
                    //MainUrl = "http://mobileapi.digitech.co.in" + "/" + suburl;

                }
                else if (Request.Url.AbsoluteUri.Contains("digitech.com"))
                {
                    User = Subdomain;
                    //MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
                    //MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                }
                else if (Request.Url.AbsoluteUri.Contains("digitech.co.in"))
                {
                    User = Subdomain;
                    // MainUrl = "http://api.digitech.co.in" + "/" + suburl;
                    //MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;

                }
                else
                {
                    User = Subdomain;
                    //  MainUrl = "http://3ischools.com/api" + "/" + suburl;
                    //MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;
                   // User = "ks001";
                }

                #region get database details from json

                var dynamicnavigationpath = "";

                dynamicnavigationpath = "navigationpath";
                string file = "";
                file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(WebConfigurationManager.AppSettings["Credential"]));
                string Json = System.IO.File.ReadAllText(file);
                System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();

                dynamic stuff1 = Newtonsoft.Json.JsonConvert.DeserializeObject(Json);
                JObject o = stuff1;

                var nameOfProperty = _ICustomEncryption.base64e(User);

                IList<string> keys = o.Properties().Select(p => p.Name).ToList();
                JToken entireJson = JToken.Parse(Json);
                JArray inner = entireJson[nameOfProperty].Value<JArray>();
                var db_id = Convert.ToInt32(inner.First);
                var db_name = Convert.ToInt32(inner.Last);

                var dbid = (db_id - 1987) / 6;
                var dbname = (db_name - 1947) / 3;
                #endregion
                //HttpClient client = new HttpClient();
                //client.Timeout = new TimeSpan(0, 10, 1);
                //MainUrl = MainUrl + "?DBName=" + User; // url with parameter
                //client.DefaultRequestHeaders.Accept.Add(
                //new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //var response = client.GetAsync(MainUrl).Result;
                //var content = response.Content.ReadAsStringAsync().Result;
                var IsDatabase = new APIDatabaseModel();
                IsDatabase.DBId = dbid;
                if (Request.Url.AbsoluteUri.Contains("localhost") || Request.Url.AbsoluteUri.Contains("172.16.1.2"))
                {
                    IsDatabase.DBName = "ks_" + dbname.ToString().PadLeft(5, '0');
                }
                else if (Request.Url.AbsoluteUri.Contains("digitech.co.in"))
                {
                    IsDatabase.DBName = "tst_" + dbname.ToString().PadLeft(5, '0');
                }
                else
                {
                    IsDatabase.DBName = "3is_" + dbname.ToString().PadLeft(5, '0');
                }
                if (IsDatabase != null && !string.IsNullOrEmpty(IsDatabase.DBName))
                {
                    // change connection string 
                    //var cn = _IConnectionService.Connectionstring(IsDatabase.DBName);
                    // check directory exist or not
                    Session.Timeout = 520000;
                    
                    Session["SchoolDB"] = IsDatabase.DBName;
                    Session["SchoolId"] = IsDatabase.DBId.ToString();

                    var directory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId));
                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    var teacherdirectory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId + "/Teacher/Photos"));
                    if (!Directory.Exists(teacherdirectory))
                        Directory.CreateDirectory(teacherdirectory);
                }
                else
                    Connectivity.Error.ToString();

                // current user
                string user = System.Web.HttpContext.Current.User.Identity.Name;
                if (string.IsNullOrEmpty(user))
                    return Connectivity.Login.ToString();
                else
                    return Connectivity.Done.ToString();















                //// get subdomain
                //string User = "";
                //string Subdomain = Request.Url.AbsolutePath;
                //Subdomain = Subdomain.Split('/')[1];
                //// API url
                //string MainUrl = "";
                //string suburl = Convert.ToString(WebConfigurationManager.AppSettings["SubUrl"]);
                //string synccompany = Convert.ToString(WebConfigurationManager.AppSettings["syncapiname"]);
                //string syncsub = Convert.ToString(WebConfigurationManager.AppSettings["syncsub"]);
                //string syncext = Convert.ToString(WebConfigurationManager.AppSettings["syncext"]);
                //string APIName = Convert.ToString(WebConfigurationManager.AppSettings["APIName"]);

                //if (Request.Url.AbsoluteUri.Contains("localhost"))
                //{
                //    User = Convert.ToString(WebConfigurationManager.AppSettings["User"]);
                //    // MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                //    //  // User = "stroop";
                //    //  // User="atsderabassi";
                //    //   User = "Dev-KS_School";
                //    //  //User = "3is_demo";
                //    //  User = "demo";
                //    //  // User = "kschild";
                //    //  //User = "bpsambala";
                //    //  //User = "hgdemo";
                //    //  //User = "dtsk8";
                //    //  //User = "";
                //    // MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
                //  //  MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;

                //    MainUrl = "http://app.3ischools.com/api_v1-10" + "/" + suburl;
                //    //MainUrl = "http://mobileapi.digitech.co.in" + "/" + suburl;

                //}
                //else if (Request.Url.AbsoluteUri.Contains("digitech.com") || Request.Url.AbsoluteUri.Contains("172.16.1.2"))
                //{
                //    User = Subdomain;
                //    //MainUrl = "http://172.16.1.2" + "/" + APIName + "/" + suburl;
                //    MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + APIName + "/" + suburl;
                //}
                //else if (Request.Url.AbsoluteUri.Contains("digitech.co.in"))
                //{
                //    User = Subdomain;
                //    // MainUrl = "http://api.digitech.co.in" + "/" + suburl;
                //    MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;

                //}
                //else
                //{
                //    User = Subdomain;
                //    //  MainUrl = "http://3ischools.com/api" + "/" + suburl;
                //    MainUrl = Convert.ToString(WebConfigurationManager.AppSettings["Url"]) + "/" + suburl;

                //}

                //HttpClient client = new HttpClient();
                //client.Timeout = new TimeSpan(0, 10, 1);
                //MainUrl = MainUrl + "?DBName=" + User; // url with parameter
                //client.DefaultRequestHeaders.Accept.Add(
                //new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                //var response = client.GetAsync(MainUrl).Result;
                //var content = response.Content.ReadAsStringAsync().Result;

                //var IsDatabase = JsonConvert.DeserializeObject<APIDatabaseModel>(content);
                //if (IsDatabase != null && !string.IsNullOrEmpty(IsDatabase.DBName))
                //{
                //    // change connection string 
                //    var cn = _IConnectionService.Connectionstring(IsDatabase.DBName);
                //    // check directory exist or not
                //    Session.Timeout = 60;
                //    Session["SchoolDB"] = IsDatabase.DBName;
                //    Session["SchoolId"] = IsDatabase.DBId.ToString();

                //    var directory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId));
                //    if (!Directory.Exists(directory))
                //        Directory.CreateDirectory(directory);

                //    var teacherdirectory = Path.Combine(Server.MapPath("~/Images/" + IsDatabase.DBId + "/Teacher/Photos"));
                //    if (!Directory.Exists(teacherdirectory))
                //        Directory.CreateDirectory(teacherdirectory);
                //}
                //else
                //    Connectivity.Error.ToString();

                //// current user
                //string user = System.Web.HttpContext.Current.User.Identity.Name;
                //if (string.IsNullOrEmpty(user))
                //    return Connectivity.Login.ToString();
                //else
                //    return Connectivity.Done.ToString();
            }
             catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Connectivity.Error.ToString();
            }
        }

        public ActionResult DummyAction(string ctrl, string act)
        {
            try
            {
                var result = GetSetCurrentSchoolDataBase(Request, Session, Server);
                if (!string.IsNullOrEmpty(result))
                {
                    var user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                    {
                        if (act.Contains("Register"))
                            return RedirectToAction("Register", "Home");
                        else if (act.Contains("PasswordRecovery"))
                            return RedirectToAction("PasswordRecovery", "Home");
                        else
                            return RedirectToAction("Login", "Home");
                    }
                    else
                    {
                        // get user type
                        var usertype = _IAddressMasterService.GetContactTypeById((int)user.UserTypeId);
                        // get current session
                        var currentsession = _ICatalogMasterService.GetCurrentSession();
                        // get login user by name
                        HttpContext.Session["LoginUserId"] = user.UserName;
                        HttpContext.Session["LoginUserType"] = user.ContactType.ContactType1;
                        HttpContext.Session["SchoolCurrentSession"] = currentsession.Session1;
                        HttpContext.Session["UserTypeId"] = usertype.ContactTypeId;

                        // get all staff types
                        var stafftypes = _IStaffService.GetAllStaffTypes(ref count);
                        // check if gurdian or student

                        // default image
                        var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                        string defaultimg = _WebHelper.GetStoreLocation() + "Images/default.png";
                        string defaultmaleimg = _WebHelper.GetStoreLocation() + "Images/male-avtar.png";
                        string defaultFemaleimg = _WebHelper.GetStoreLocation() + "Images/Female-avtar.png";

                        var Is_Lite = _ISchoolSettingService.GetorSetSchoolData("Is_Lite", "0").AttributeValue == "1" ? true : false;


                        // check if gurdian or student
                        if (usertype.ContactType1.ToLower() == "admin")
                        {
                            HttpContext.Session["UserImage"] = defaultmaleimg;
                            HttpContext.Session["StudentId"] = "";
                            HttpContext.Session["StaffId"] = "";
                        }
                        else if (usertype.ContactType1.ToLower() == "student")
                        {
                            // Student image path
                            // get doc type
                            string StudentImage = "";
                            var doctypeid = _IDocumentService.GetAllDocumentTypes(ref count, "Image").FirstOrDefault();
                            var image = _IDocumentService.GetAllAttachedDocuments(ref count,
                                doctypeid.DocumentTypeId,
                                StudentId: user.UserContactId
                                ).FirstOrDefault();

                            //check user gender
                            var gender = _IStudentService.GetStudentById((int)user.UserContactId).Gender.Gender1;
                            var defaultpic = defaultmaleimg;
                            if (gender.ToLower().Contains("female"))
                            {
                                defaultpic = defaultFemaleimg;
                            }


                            StudentImage = image != null ? _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/StudentPhotos/" + image.Image : defaultpic;

                            HttpContext.Session["UserImage"] = StudentImage;
                            HttpContext.Session["StudentId"] = _ICustomEncryption.base64e(user.UserContactId.ToString());
                            HttpContext.Session["StaffId"] = "";
                        }
                        else if (usertype.ContactType1.ToLower() == "guardian")
                        {
                            // Student image path
                            // get doc type
                            string StudentImage = "";
                            var doctypeid = _IDocumentService.GetAllDocumentTypes(ref count, "Image").FirstOrDefault();

                            //check user gender
                            var relation = _IGuardianService.GetGuardianById((int)user.UserContactId).Relation.Relation1;
                            var defaultpic = defaultmaleimg;
                            if (relation.ToLower().Contains("mother"))
                            {
                                defaultpic = defaultFemaleimg;
                            }
                            StudentImage = _IGuardianService.GetGuardianById((int)user.UserContactId).Image != null ? _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/GuardianPhotos/" + _IGuardianService.GetGuardianById((int)user.UserContactId).Image : defaultpic;

                            HttpContext.Session["UserImage"] = StudentImage;
                            HttpContext.Session["StudentId"] = _ICustomEncryption.base64e(user.UserContactId.ToString());
                            HttpContext.Session["StaffId"] = "";
                        }
                        else if (stafftypes.Where(s => s.StaffType1.ToLower() == usertype.ContactType1.ToLower()).FirstOrDefault() != null)
                        {
                            // Student image path
                            // get doc type
                            string StaffImage = "";
                            var doctypeid = _IStaffService.GetAllStaffDocumentTypes(ref count, "Image").FirstOrDefault();
                            var image = _IStaffService.GetAllStaffDocuments(ref count,
                                doctypeid.DocumentTypeId,
                                StaffId: user.UserContactId
                                ).FirstOrDefault();

                            //check user gender
                            var genderid = _IStaffService.GetStaffById((int)user.UserContactId).GenderId;
                            var gender = _ICatalogMasterService.GetGenderById((int)genderid).Gender1;
                            var defaultpic = defaultmaleimg;
                            if (gender.ToLower().Contains("female"))
                            {
                                defaultpic = defaultFemaleimg;
                            }

                            StaffImage = image != null ? _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/Teacher/Photos/" + image.Image : defaultpic;

                            HttpContext.Session["UserImage"] = StaffImage;
                            HttpContext.Session["StudentId"] = "";
                            HttpContext.Session["StaffId"] = _ICustomEncryption.base64e(user.UserContactId.ToString());
                        }
                        else if (usertype.ContactType1.ToLower() == "teacher")
                        {
                            // Student image path
                            // get doc type
                            string StaffImage = "";
                            var doctypeid = _IStaffService.GetAllStaffDocumentTypes(ref count, "Image").FirstOrDefault();
                            var image = _IStaffService.GetAllStaffDocuments(ref count,
                                doctypeid.DocumentTypeId,
                                StaffId: user.UserContactId
                                ).FirstOrDefault();

                            //check user gender
                            var genderid = _IStaffService.GetStaffById((int)user.UserContactId).GenderId;
                            var genderdata = _ICatalogMasterService.GetGenderById((int)genderid).Gender1;
                            var defaultpic = defaultmaleimg;
                            if (genderdata.ToLower().Contains("female"))
                                defaultpic = defaultFemaleimg;

                            // get staff and check gender
                            var staffgender = _IStaffService.GetStaffById((int)user.UserContactId);
                            StaffImage = image != null ? _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/Teacher/Photos/" + image.Image : defaultpic;
                            //if (string.IsNullOrEmpty(StaffImage))
                            //{
                            //    if (staffgender.GenderId > 0)
                            //    {
                            //        // get gender by id
                            //        var gender = _ICatalogMasterService.GetGenderById((int)staffgender.GenderId);
                            //        if (gender.Gender1.ToLower() == "male")
                            //            StaffImage = _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/male-avtar.png";
                            //        else if (gender.Gender1.ToLower() == "female")
                            //            StaffImage = _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/Female-avtar.png";
                            //    }
                            //}

                            HttpContext.Session["LoginUserId"] = !string.IsNullOrEmpty(staffgender.LName) ? staffgender.FName + " " + staffgender.LName : staffgender.FName;
                            HttpContext.Session["UserImage"] = StaffImage;
                            HttpContext.Session["StudentId"] = "";
                            HttpContext.Session["StaffId"] = _ICustomEncryption.base64e(user.UserContactId.ToString());
                        }
                    }

                    if (result == Connectivity.Done.ToString())
                        return RedirectToAction(act, ctrl);
                    else if (result == Connectivity.Login.ToString())
                        return RedirectToAction("Login", "Home");
                    else if (result == Connectivity.Error.ToString())
                        return RedirectToAction("AccessDenied", "Error");
                    else
                        return RedirectToAction(act, ctrl);
                }
                else
                    return RedirectToAction("General", "Error");
            }
            catch (Exception ex)
            {
                return RedirectToAction("DummyAction", new { ctrl = ctrl, act = act });
            }
        }

        #endregion

        #region Student / Staff Leave Application

        //leave application for student or teacher
        public LeaveApplication PrepareModelForLeaveApplication(LeaveApplication model, string contactType)
        {
            var currentSession = _ICatalogMasterService.GetCurrentSession();
            model.CurrentSessionEndDate = ConvertDateForKendo((DateTime)currentSession.EndDate);
            //model.CurrentSessionStartDate = ConvertDateForKendo((DateTime)currentSession.StartDate);
            var leaveTypeList = new List<SelectListItem>();
            if (contactType.ToLower() == "student" || contactType.ToLower() == "guardian")
            {
                leaveTypeList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
                var leavetypes = _ISubjectService.GetAllAttendanceStatus(ref count);
                leavetypes = leavetypes.Where(p => p.IsLeave == true).ToList();
                foreach (var item in leavetypes)
                    leaveTypeList.Add(new SelectListItem { Selected = false, Value = item.AttendanceStatusId.ToString(), Text = item.AttendanceStatus });
            }
            else
            {
                leaveTypeList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
                var leavetypes = _IStaffService.GetStaffAllAttendanceStatus(ref count);
                leavetypes = leavetypes.Where(p => p.IsLeave == true).ToList();
                foreach (var item in leavetypes)
                    leaveTypeList.Add(new SelectListItem { Selected = false, Value = item.AttendanceStatusId.ToString(), Text = item.AttendanceStatus });
            }
            model.LeaveTypeList = leaveTypeList;

            model.FromTimeLimit = _ISchoolSettingService.GetorSetSchoolData("LeaveApplyAllowedTimeFrom", "23:59", null,
                                                       "Apply leave from time limit").AttributeValue;
            model.ToTimeLimit = _ISchoolSettingService.GetorSetSchoolData("LeaveApplyAllowedToTime", "19:00", null,
                                              "Apply leave To time limit").AttributeValue;

            if (Convert.ToDateTime(model.FromTimeLimit).TimeOfDay <= DateTime.Now.TimeOfDay)
            {
                if (Convert.ToDateTime(model.ToTimeLimit).TimeOfDay >= DateTime.Now.TimeOfDay)
                    model.CanLeaveApply = true;
            }
            //values for kendo paging
            model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
            model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");
            return model;
        }

        public string CheckLeaveApplicationModel(LeaveApplication model)
        {
            StringBuilder errorMsg = new StringBuilder();
            if (string.IsNullOrEmpty(model.LeaveFrom))
                errorMsg.Append("Please provide leave start date ε");
            if (string.IsNullOrEmpty(model.LeaveTill))
                errorMsg.Append("Please provide leave end date ε");
            if (!model.LeaveTypeId.HasValue)
                errorMsg.Append("Please provide leave type ε");


            DateTime? LeaveFrom = null, LeaveTill = null;
            if (!string.IsNullOrEmpty(model.LeaveFrom) || !string.IsNullOrWhiteSpace(model.LeaveFrom))
            {
                LeaveFrom = DateTime.ParseExact(model.LeaveFrom, "dd/MM/yyyy", null);
                LeaveFrom = LeaveFrom.Value.Date;
            }
            if (!string.IsNullOrEmpty(model.LeaveTill) || !string.IsNullOrWhiteSpace(model.LeaveTill))
            {
                LeaveTill = DateTime.ParseExact(model.LeaveTill, "dd/MM/yyyy", null);
                LeaveTill = LeaveTill.Value.Date;
            }

            if (LeaveFrom > LeaveTill)
                errorMsg.Append("LeaveFrom Date should be equal to or less than LeaveTill Date ε");
            if (model.Reason == null)
                errorMsg.Append("Please provide reason for leave ε");
            return errorMsg.ToString();
        }

        public ActionResult LeaveApplication()
        {
            var model = new LeaveApplication();
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;


                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Common", "LeaveApplication", "View");
                    if (!isauth)
                        return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }
                if (user.ContactType.ContactType1.ToLower() == "admin" || user.ContactType.ContactType1.ToLower() == "teacher" 
                    || user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian")
                {
                    if (TempData["IsSuccessfull"] != null)
                        ViewBag.SuccessMessage = TempData["IsSuccessfull"];
                    if (TempData["IsFailed"] != null)
                        ViewBag.FailedMessage = TempData["IsFailed"];

                    model.IsAuthToApply = _IUserAuthorizationService.IsUserAuthorize(user, "Common", "LeaveApplication", "Apply");
                    model = PrepareModelForLeaveApplication(model, user.ContactType.ContactType1);

                   
                    model.ContactType = user.ContactType.ContactType1;
                    var MaxLeaveApplyDaysForStudent = _ISchoolSettingService.GetorSetSchoolData("MaxLeaveDaysAllowedforStudentLeaveApplication", "3").AttributeValue;
                    var MaxLeaveApplyDaysForStaff   = _ISchoolSettingService.GetorSetSchoolData("MaxLeaveDaysAllowedforStaffLeaveApplication", "3").AttributeValue;
                    if (user.ContactType.ContactType1.ToLower() == "admin" || user.ContactType.ContactType1.ToLower() == "staff" || user.ContactType.ContactType1.ToLower() == "teacher" || user.ContactType.ContactType1.ToLower() == "non-teacher")
                    {
                        if (MaxLeaveApplyDaysForStaff != null)
                            model.MaxLeaveApplyDays = Convert.ToInt32(MaxLeaveApplyDaysForStaff);
                    }
                    else if(user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian")
                    {
                        if (MaxLeaveApplyDaysForStudent != null)
                            model.MaxLeaveApplyDays = Convert.ToInt32(MaxLeaveApplyDaysForStudent);
                    }
                        return View(model);
                }
                else
                    return RedirectToAction("AccessDenied", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult LeaveApplication(LeaveApplication model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;


                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                var StudentLeaveTable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StudentLeaveApplication").FirstOrDefault();
                var StaffLeaveTable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StaffLeaveApplication").FirstOrDefault();


                string errors = CheckLeaveApplicationModel(model);
                var leave_from = Convert.ToDateTime(FormatDate(model.LeaveFrom));
                var leave_till = Convert.ToDateTime(FormatDate(model.LeaveTill));

                var Student_Id = 0;
                if (user.ContactType.ContactType1.ToLower() == "student")
                    Student_Id = (int)user.UserContactId;
                else if (user.ContactType.ContactType1.ToLower() == "guardian")
                    Student_Id = (int)_IGuardianService.GetGuardianById((int)user.UserContactId).StudentId;

                var allow = true;
                var leaveApplications = _ISubjectService.GetAllStudentLeaveApplication(ref count).Where(m => (m.LeaveFrom >= leave_from && m.LeaveFrom <= leave_till)
                            || (m.LeaveTill >= leave_from && m.LeaveTill <= leave_till) || (m.LeaveFrom <= leave_from && m.LeaveFrom <= leave_till
                            && m.LeaveTill >= leave_from && m.LeaveTill >= leave_till) || (m.LeaveFrom >= leave_from && m.LeaveTill <= leave_till))
                            .Where(x => x.StudentId == Student_Id);
                if (leaveApplications.Count() > 0)
                {
                    allow = false;
                    //ModelState.AddModelError("LeaveFrom", " ");
                    errors += "Already has leave for selected date ε";
                }
                model.ContactType = user.ContactType.ContactType1;
                if (string.IsNullOrEmpty(errors) && allow)
                {
                    if (model.LeaveId != null)
                    {
                        var id = Convert.ToInt32(model.LeaveId);
                        if (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian")
                        {
                            var studentLeave = _ISubjectService.GetStudentLeaveApplicationById(StudentLeaveApplicationId: id, IsTrack: true);
                            studentLeave.LeaveFrom = Convert.ToDateTime(FormatDate(model.LeaveFrom));
                            studentLeave.LeaveTill = Convert.ToDateTime(FormatDate(model.LeaveTill));
                            studentLeave.LeaveTypeId = model.LeaveTypeId;
                            studentLeave.Reason = model.Reason;
                            studentLeave.AppliedDate = DateTime.Now;
                            if (user.ContactType.ContactType1.ToLower() == "guardian")
                            {
                                var studentid = _IGuardianService.GetGuardianById((int)user.UserContactId).StudentId;
                                studentLeave.StudentId = studentid;
                            }
                            else
                                studentLeave.StudentId = user.UserContactId;
                            _ISubjectService.UpdateStudentLeaveApplication(studentLeave);
                        }
                        else
                        {
                            var staffLeave = _IStaffService.GetStaffLeaveApplicationById(StaffLeaveApplicationId: id, IsTrack: true);
                            staffLeave.LeaveFrom = Convert.ToDateTime(FormatDate(model.LeaveFrom));
                            staffLeave.LeaveTill = Convert.ToDateTime(FormatDate(model.LeaveTill));
                            staffLeave.LeaveTypeId = model.LeaveTypeId;
                            staffLeave.Reason = model.Reason;
                            staffLeave.AppliedDate = DateTime.Now;
                            staffLeave.StaffId = user.UserContactId;
                            var ExistingLeave = _IStaffService.GetStaffLeaveApplicationbyLeavDate(staffLeave);
                            if (ExistingLeave != null && ExistingLeave.StaffLeaveApplicationId != staffLeave.StaffLeaveApplicationId)
                            {
                                TempData["IsSuccessfull"] = "Leave can not be applied On Existing Date ε";
                                return RedirectToAction("LeaveApplication");
                            }
                            else
                            {
                                _IStaffService.UpdateStaffLeaveApplication(staffLeave);
                            }
                        }
                        TempData["IsSuccessfull"] = "Leave Updated successfully ε";
                        return RedirectToAction("LeaveApplication");
                    }
                    else
                    {
                        if (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian")
                        {
                            StudentLeaveApplication studentLeave = new StudentLeaveApplication();
                            studentLeave.LeaveFrom = Convert.ToDateTime(FormatDate(model.LeaveFrom));
                            studentLeave.LeaveTill = Convert.ToDateTime(FormatDate(model.LeaveTill));
                            studentLeave.LeaveTypeId = model.LeaveTypeId;
                            studentLeave.Reason = model.Reason;
                            studentLeave.AppliedDate = DateTime.Now;
                            if (user.ContactType.ContactType1.ToLower() == "guardian")
                            {
                                var studentid = _IGuardianService.GetGuardianById((int)user.UserContactId).StudentId;
                                studentLeave.StudentId = studentid;
                            }
                            else
                                studentLeave.StudentId = user.UserContactId;
                            _ISubjectService.InsertStudentLeaveApplication(studentLeave);
                            if (StudentLeaveTable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, StudentLeaveTable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(studentLeave.StudentLeaveApplicationId, StudentLeaveTable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add StudentLeaveApplication with StudentLeaveApplicationId:{0} and StudentId:{1}", studentLeave.StudentLeaveApplicationId, studentLeave.StudentId), Request.Browser.Browser);
                            }

                            var currentSession = _ICatalogMasterService.GetCurrentSession();
                            var leaveType = _ISubjectService.GetAllAttendanceStatus(ref count)
                                             .Where(x => x.IsLeave == true && x.AttendanceStatusId == studentLeave.LeaveTypeId).FirstOrDefault();
                            var LeaveTypeStatus = leaveType != null ? leaveType.AttendanceStatus : "";
                            //trigger send to class incharge teacher
                            var DefaultSettings = _ICommonMethodService.GetOrSetSMSDefaultSetting("LeaveApply/Approval", true, false, true, "StaffMobileNumber", "");
                            var getStudentClass = _IStudentService.GetAllStudentClasss(ref count)
                                                    .Where(x => x.SessionId == currentSession.SessionId
                                                          && x.StudentId == studentLeave.StudentId).OrderByDescending(x => x.StudentClassId)
                                                                                       .FirstOrDefault();

                            string ClassInchargeStaffId = "";
                            if (getStudentClass != null)
                            {
                                var StudentInfo = _IStudentService.GetAllStudents(ref count)
                                                        .Where(x => x.StudentId == studentLeave.StudentId).FirstOrDefault();
                                DateTime CurrentDate = DateTime.Now.Date;
                                var getCurrentInchargeStaffId = _IStaffService.GetAllClassTeachers(ref count)
                                                                .Where(p => p.EffectiveDate <= CurrentDate
                                                                        && (p.EndDate >= CurrentDate || p.EndDate == null)
                                                                        && p.ClassId == getStudentClass.ClassId)
                                                                .OrderByDescending(x => x.ClassTeacherId).FirstOrDefault();

                                if (getCurrentInchargeStaffId != null)
                                {
                                    ClassInchargeStaffId = getCurrentInchargeStaffId.TeacherId.ToString();
                                    _ICommonMethodService.TiggerNotificationSMS(IsStaff: true, StaffIds: ClassInchargeStaffId,
                                                             LeaveStatus: LeaveTypeStatus, LeaveFrom: studentLeave.LeaveFrom,
                                                             LeaveTill: studentLeave.LeaveTill,
                                                             TriggerEventName: "LeaveApply",
                                                             IsNonAPPUser: (bool)DefaultSettings.ToNonAppUser,
                                                             SMS: (bool)DefaultSettings.SMS,
                                                             Notification: (bool)DefaultSettings.Notification,
                                                             student: StudentInfo, IsUserFullDetail: true);
                                }
                            }
                        }
                        else
                        {
                            StaffLeaveApplication staffLeave = new StaffLeaveApplication();
                            staffLeave.LeaveFrom = Convert.ToDateTime(FormatDate(model.LeaveFrom));
                            staffLeave.LeaveTill = Convert.ToDateTime(FormatDate(model.LeaveTill));
                            staffLeave.LeaveTypeId = model.LeaveTypeId;
                            staffLeave.Reason = model.Reason;
                            staffLeave.AppliedDate = DateTime.Now;
                            staffLeave.StaffId = user.UserContactId;

                            //get details for existing date leave apllication 29March
                            var ExistingLeave = _IStaffService.GetStaffLeaveApplicationbyLeavDate(staffLeave);
                            if (ExistingLeave == null)
                            {
                                //if (staffLeave.IsApproved != null)
                                //{
                                //    StaffAttendance Stf = new StaffAttendance();
                                //    Stf.StaffId = staffLeave.StaffId;
                                //    Stf.AttendanceDate = DateTime.Today;
                                //    Stf.AttendanceStatusId = staffLeave.LeaveTypeId;
                                //    //insert Staff LeaveRecord In StaffAttendance Table on  the date
                                //    _IStaffService.InsertStaffAttendance(Stf);
                                //}
                                _IStaffService.InsertStaffLeaveApplication(staffLeave);


                                //notifications and sms to Admin 
                                //insert attendance SMs settings
                                var DefaultSettings = _ICommonMethodService.GetOrSetSMSDefaultSetting("LeaveApply/Approval", true, false, true, "StaffMobileNumber", "");
                                var leaveType = _IStaffService.GetStaffAllAttendanceStatus(ref count)
                                                .Where(x => x.IsLeave == true && x.AttendanceStatusId == staffLeave.LeaveTypeId).FirstOrDefault();
                                var LeaveTypeStatus = leaveType != null ? leaveType.AttendanceStatus : "";
                                var StaffInfo = _IStaffService.GetStaffById((int)user.UserContactId);
                                var AdminStaffIds = "";
                                var AllAdmins =_IUserService.GetAllUsers(ref count).
                                                Where(x => x.ContactType.ContactType1.ToLower() == "admin").ToList();
                                foreach (var i in AllAdmins)
                                {
                                    string Role = "";
                                    Role =_ICommonMethodService.GetUserSuperRole(Role,i);
                                    if (Role.ToLower() == "admin")
                                        AdminStaffIds += i.UserContactId + ",";

                                }
                                AdminStaffIds = AdminStaffIds.TrimEnd(',');
                                DateTime CurrentDate = DateTime.Now.Date;
                               _ICommonMethodService.TiggerNotificationSMS(IsStaff: true, StaffIds: AdminStaffIds,
                                                        LeaveStatus: LeaveTypeStatus, LeaveFrom: staffLeave.LeaveFrom, LeaveTill: staffLeave.LeaveTill,
                                                        TriggerEventName: "LeaveApply",
                                                        IsNonAPPUser: (bool)DefaultSettings.ToNonAppUser,
                                                        SMS: (bool)DefaultSettings.SMS,
                                                        Notification: (bool)DefaultSettings.Notification,
                                                        Staff: StaffInfo, IsUserFullDetail: true,IsAdmin: true);
                            }
                            else
                            {
                                TempData["IsFailed"] = "Leave can not be applied On Existing Date ε";
                                return RedirectToAction("LeaveApplication");
                            }
                            //end here
                            if (StaffLeaveTable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, StaffLeaveTable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(staffLeave.StaffLeaveApplicationId, StaffLeaveTable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add StaffLeaveApplication with StaffLeaveApplicationId:{0} and StaffId:{1}", staffLeave.StaffLeaveApplicationId, staffLeave.StaffId), Request.Browser.Browser);
                            }
                        }
                        TempData["IsSuccessfull"] = "Leave applied successfully ε";
                        return RedirectToAction("LeaveApplication");
                    }
                }
                else
                {
                    TempData["IsFailed"] = errors;
                    //if (TempData["IsFailed"] != null)
                    //    ViewBag.FailedMessage = TempData["IsFailed"];
                    //TempData["LeaveFrom"] = ConvertDateForKendo(Convert.ToDateTime(FormatDate(model.LeaveFrom)));
                    //TempData["LeaveTill"] = ConvertDateForKendo(Convert.ToDateTime(FormatDate(model.LeaveTill)));
                    model.IsAuthToApply = _IUserAuthorizationService.IsUserAuthorize(user, "Common", "LeaveApplication", "Apply");
                    return View(PrepareModelForLeaveApplication(model, user.ContactType.ContactType1.ToLower()));
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult GetLeaveList(DataSourceRequest command, LeaveApplication model, IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            var currentSession = _ICatalogMasterService.GetCurrentSession();
            var staffLeaveAppList = new List<StaffLeaveApplication>();
            var studentLeaveAppList = new List<StudentLeaveApplication>();

            if (user.ContactType.ContactType1.ToLower() == "student")
                studentLeaveAppList = _ISubjectService.GetAllStudentLeaveApplication(ref count).Where(p => p.AppliedDate >= currentSession.StartDate.Value && p.AppliedDate <= currentSession.EndDate.Value && p.StudentId == user.UserContactId).OrderByDescending(m => m.AppliedDate).ToList();
            if (user.ContactType.ContactType1.ToLower() == "guardian")
            {
                var studentid = _IGuardianService.GetGuardianById((int)user.UserContactId).StudentId;
                studentLeaveAppList = _ISubjectService.GetAllStudentLeaveApplication(ref count).Where(p => p.AppliedDate >= currentSession.StartDate.Value && p.AppliedDate <= currentSession.EndDate.Value && p.StudentId == studentid).OrderByDescending(m => m.AppliedDate).ToList();
            }
            else if (user.ContactType.ContactType1.ToLower() == "teacher")
                staffLeaveAppList = _IStaffService.GetAllStaffLeaveApplication(ref count).Where(p => p.AppliedDate >= currentSession.StartDate.Value && p.AppliedDate <= currentSession.EndDate.Value && p.StaffId == user.UserContactId).OrderByDescending(m => m.AppliedDate).ToList();

            if (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian")
            {
                var leaveList = studentLeaveAppList.Select(x =>
                    {
                        var leaveModel = new LeaveModel();

                        leaveModel.LeaveTypeId = x.LeaveTypeId.ToString();
                        leaveModel.LeaveType = x.AttendanceStatu.AttendanceStatus;
                        leaveModel.LeaveId = x.StudentLeaveApplicationId;
                        leaveModel.LeaveFrom = x.LeaveFrom;
                        leaveModel.LeaveTill = x.LeaveTill;
                        leaveModel.ReasonDesc = x.Reason;
                        if (x.Reason.Length > 10)
                            leaveModel.Reason = x.Reason.Substring(0, 9) + "...";
                        else
                            leaveModel.Reason = x.Reason;


                        if (x.ApprovedBy != null)
                        {
                            if ((bool)x.IsApproved)
                                leaveModel.Status = "Approved";
                            else
                                leaveModel.Status = "Disapproved";
                        }
                        else
                        {
                            leaveModel.Status = "Approval Pending";
                            leaveModel.IsLeaveEditable = true;
                            leaveModel.IsLeavedeleteable = true;
                        }

                        return leaveModel;
                    }).AsQueryable().Sort(sort);
                var gridModel = new DataSourceResult
                {
                    Data = leaveList.PagedForCommand(command).ToList(),
                    Total = leaveList.Count()
                };
                return Json(gridModel);

            };
            if (user.ContactType.ContactType1.ToLower() == "admin" || user.ContactType.ContactType1.ToLower() == "teacher")
            {
                var leaveList = staffLeaveAppList.Select(x =>
                    {
                        var leaveModel = new LeaveModel();

                        leaveModel.LeaveTypeId = x.LeaveTypeId.ToString();
                        leaveModel.LeaveType = x.StaffAttendanceStatu.AttendanceStatus;
                        leaveModel.LeaveId = x.StaffLeaveApplicationId;
                        leaveModel.LeaveFrom = x.LeaveFrom;
                        leaveModel.LeaveTill = x.LeaveTill;
                        leaveModel.ReasonDesc = x.Reason;
                        if (x.Reason.Length > 10)
                            leaveModel.Reason = x.Reason.Substring(0, 9) + "...";
                        else
                            leaveModel.Reason = x.Reason;
                        if (x.ApprovedBy != null)
                        {
                            if ((bool)x.IsApproved)
                                leaveModel.Status = "Approved";
                            else
                                leaveModel.Status = "Disapproved";
                        }
                        else
                        {
                            leaveModel.Status = "Approval Pending";
                            leaveModel.IsLeaveEditable = true;
                            leaveModel.IsLeavedeleteable = true;
                        }

                        return leaveModel;
                    }).AsQueryable().Sort(sort);
                var gridModel = new DataSourceResult
               {
                   Data = leaveList.PagedForCommand(command).ToList(),
                   Total = leaveList.Count()
               };
                return Json(gridModel);
            };
            return Json(false);
        }

        public ActionResult DeleteLeave(string Id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "ManageStaff", "Delete");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }

            if (Id != null && Id != "")
            {
                var id = Convert.ToInt32(Id);
                if (user.ContactType.ContactType1.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian")
                {
                    var studentleave = _ISubjectService.GetStudentLeaveApplicationById(StudentLeaveApplicationId: id);
                    if (studentleave != null)
                        _ISubjectService.DeleteStudentLeaveApplication(id);
                }
                else
                {
                    var staffleave = _IStaffService.GetStaffLeaveApplicationById(StaffLeaveApplicationId: id);
                    if (staffleave != null)
                        _IStaffService.DeleteStaffLeaveApplication(id);
                }
                return Json(new { status = "success", data = "Leave deleted successfully." });
            }
            else
                return Json(new { status = "failed", data = "Error record can't be deleted." });
        }
        #endregion

       

        #region ExportMethods

        [HttpPost]
        public ActionResult CreatePdf(StudentModel StudentModel)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                _IExportHelper.ExportToPDF(html: StudentModel.htmldata, Title: "Form");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
            }
            return null;
        }

        [HttpPost]
        public ActionResult CreatePdfadmitted(AssignRollnoModel StudentModel)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                _IExportHelper.ExportToPDF(html: StudentModel.htmldata, Title: "Form");
            }


            //if (!string.IsNullOrEmpty(HTML))
            //{
            //    HtmlToPdf HtmlToPdf = new IronPdf.HtmlToPdf();
            //    PdfDocument PDF = HtmlToPdf.RenderHtmlAsPdf(HTML);
            //    PDF.SaveAs(@"C:\Users\DELL\Desktop\File.Pdf");
            //}

            //using (MemoryStream stream = new System.IO.MemoryStream())
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
            }
            return null;
        }


        [HttpPost]
        public ActionResult CreateHtmlExcel(StaffAttendanceReportModel staffAttntModel)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                _IExportHelper.ExportToExcel(dt: null, html: staffAttntModel.htmldata, Title: "StaffAttendence");
            }

            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
            }
            return null;
        }

        #endregion

        #region Surveillance


        public ActionResult CheckSurveillanceAlarm()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "ManageStaff", "Delete");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            try
            {
                var surveillancealarms = _ICommonMethodService.GetAllSurveillanceAlarms(ref count).Where(m =>m.AlarmActivatedAt==null && m.StaffId == user.UserContactId && m.SurviellancePunch.OutTime == null && (m.AlarmTime>=DateTime.Now.AddMinutes(-1) && m.AlarmTime<=DateTime.Now.AddMinutes(1)));
                
              

                var model = new SurveillanceAlarmModel();
                var modelList = new List<SurveillanceAlarmModel>();
                foreach (var item in surveillancealarms)
                {
                    
                    var student=_IStudentService.GetStudentById((int)item.SurviellancePunch.ContactId);
                    model = new SurveillanceAlarmModel();
                    var contacttype=_IAddressMasterService.GetContactTypeById((int)item.SurviellancePunch.ContactTypeId).ContactType1.ToLower();
                   if(contacttype=="student")
                       model.StudentName=student.FName+" "+student.FName!=null?student.FName:"";
                   model.SurveillancePunchId = (int)item.SurviellancePunchId;
                   model.SurveillanceAllarmId = item.SurveillanceAlarmId;
                   model.StudentId = (int)item.SurviellancePunch.ContactId;
                    model.AdmnNo = student.AdmnNo!=null?student.AdmnNo:"";
                    var studentClass = _IStudentService.GetAllStudentClasss(ref count).Where(m => m.StudentId == student.StudentId && m.SessionId==_ICatalogMasterService.GetCurrentSession().SessionId).FirstOrDefault();
                    model.ClassName = studentClass != null ? studentClass.ClassMaster.Class : "";
                    model.DeviceLocation = item.SurviellancePunch.AttendanceLocation.AttendanceLocation1;
                    model.EntryTime = ConvertDate((DateTime)item.SurviellancePunch.InTime);

                    var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                    var StudentImagepath = "";
                    var doctypeid = _IDocumentService.GetAllDocumentTypes(ref count, "Image").FirstOrDefault();
                    var image = _IDocumentService.GetAllAttachedDocuments(ref count,
                        doctypeid.DocumentTypeId,
                        StudentId: student.StudentId
                        ).FirstOrDefault();

                    if (image != null)
                    {
                        //model.StudentImageId = image.DocumentId;
                       
                       StudentImagepath = _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/StudentPhotos/" + image.Image;
                    }
                    else
                       StudentImagepath = "";
                    model.StudentImage = StudentImagepath;
                    modelList.Add(model);

                    var messageSMS = "Surveillance alert : "+model.StudentName+" , "+model.AdmnNo+" , "+model.DeviceLocation+" , "+model.EntryTime;
                    var errormessage="";
                    var content="";
                    _ISMSSender.SendSMS(item.StaffPhone, messageSMS, false, out errormessage, out content);

                }
                if (surveillancealarms.Count() > 0)
                {
                    foreach (var item in surveillancealarms)
                    {
                        item.AlarmActivatedAt = DateTime.Now;
                        _ICommonMethodService.UpdateSurveillanceAlarmsActivatedDate(item);
                    }
                }
                return Json(new { status = "success", data = modelList });
            }
            catch (Exception ex)
            {
                return Json(new { status = "failed", data = ex.Message });
                
            }
         
            
                //return Json(new { status = "failed", data = "Error record can't be deleted." });



        }

        public ActionResult UpdateSurveillancePunch(int SurviellancePunchId=0, string Surviellance_remarks="", string SurviellancePunch_outtime="")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Staff", "ManageStaff", "Delete");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            try
            {
                if (SurviellancePunchId > 0)
                {
                   var SurviellancePunch= _ICommonMethodService.GetSurveillancePunchById(SurviellancePunchId);
                   if (SurviellancePunch != null)
                   {
                       if (SurviellancePunch.OutTime != null )
                       {
                           return Json(new { status = "failed", data = "Record can't be saved. Automated outtime already punched" });
                       }
                       else
                       {
                           SurviellancePunch.OutTime = Convert.ToDateTime(SurviellancePunch_outtime);
                           SurviellancePunch.Remarks = Surviellance_remarks;
                           SurviellancePunch.ClosedTime = DateTime.Now;
                           _ICommonMethodService.UpdateSurveillancePunch(SurviellancePunch);
                           return Json(new { status = "success", data = "SurviellancePunch updated successfully" });
                       }

                  
                   }
                   else
                   {
                       return Json(new { status = "failed", data = "SurviellancePunch not found" });
                   }
                }else
                    return Json(new { status = "failed", data = "SurviellancePunch not found" });
            }
            catch (Exception ex)
            {
                return Json(new { status = "failed", data = ex.Message });

            }


            //return Json(new { status = "failed", data = "Error record can't be deleted." });



        }

        #endregion
    }
}