﻿using DocumentFormat.OpenXml.Bibliography;
using BAL.BAL.Security;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.ExaminationModule;
using DAL.DAL.Schedular;
using DAL.DAL.UserModule;
using DAL.DAL.StudentModule;
using DAL.DAL.GuardianModule;
using DAL.DAL.StaffModule;
using DAL.DAL.ClassPeriodModule;
using KSModel.Models;
using ViewModel.ViewModel;
using ViewModel.ViewModel.Calender;
using ViewModel.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.DAL.SettingService;

namespace KS_SmartChild.Controllers
{
    public class KCalenderController : Controller
    {
        #region fields

        public int count = 0;
        private readonly IUserService _IUserService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly IWebHelper _WebHelper;
        private readonly ISchedularService _ISchedularService;
        private readonly IExamService _IExamService;
        private readonly ILogService _Logger;
        private readonly ISubjectService _ISubjectService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStudentService _IStudentService;
        private readonly IGuardianService _IGuardianService;
        private readonly IStaffService _IStaffService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        #endregion

        #region ctor

        public KCalenderController(IUserService IUserService,
            IUserAuthorizationService IUserAuthorizationService,
            IWebHelper WebHelper,
            ISchedularService ISchedularService,
            IExamService IExamService,
            ILogService Logger,
            ISubjectService ISubjectService,
            ICatalogMasterService ICatalogMasterService, IStudentService IStudentService, IGuardianService IGuardianService, IStaffService IStaffService,
            ISchoolSettingService ISchoolSettingService)
        {
            this._IUserService = IUserService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._WebHelper = WebHelper;
            this._ISchedularService = ISchedularService;
            this._IExamService = IExamService;
            this._Logger = Logger;
            this._ISubjectService = ISubjectService;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IStudentService = IStudentService;
            this._IGuardianService = IGuardianService;
            this._IStaffService = IStaffService;
            this._ISchoolSettingService = ISchoolSettingService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        #endregion

        #region methods
        public ActionResult Calender()
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            var model = GetCalender(month, year);
           

            return View(model);
        }

        [HttpPost]
        public ActionResult Calender(string reqmonth = null)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }



            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            if (reqmonth != null)
            {
                var splittedreqmonth = reqmonth.Split('/');
                month = Convert.ToInt32(splittedreqmonth[0]);
                year = Convert.ToInt32(splittedreqmonth[1]);
            }
            var model = GetCalender(month, year);
            var currentMonth = month + "/" + year;
            var monthToShow = Convert.ToDateTime(currentMonth);
            model.Month = monthToShow.ToString("MMMM");
            model.Year = monthToShow.Year.ToString();
            return View(model);
        }

        public WeekForMonth GetCalender(int month, int year)
        {

            var user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;

            var userType = user.ContactType.ContactType1;
            var usercontactId = user.UserContactId;
            var currentSession = _ICatalogMasterService.GetCurrentSession();

            var classId = (int?)null;
            var requiredEventIds = new List<int?>();
            var requiredStandardIds = new List<int?>();


            WeekForMonth weeks = new WeekForMonth();
            List<DateTime> dt = new List<DateTime>();
            dt = GetDates(year, month);
            var monthStartDate = dt.First();
            var monthLastDate = dt.Last();
            var eventsOfMonth = _ISchedularService.GetAllEventList(ref count).Where(p => p.StartDate >= monthStartDate && p.StartDate <= monthLastDate && p.IsActive == true && p.IsDeleted == false).ToList();
            var alleventDetails = _ISchedularService.GetAllEventDetailList(ref count).ToList();
            var allExams = _IExamService.GetAllExamDateSheetDetails(ref count).Where(p => p.ExamDate >= monthStartDate && p.ExamDate <= monthLastDate && p.ExamDateSheet.Ispublish == true).ToList();
            var allResults = _IExamService.GetAllExamResultDates(ref count).Where(p => p.ExamResultDate1 >= monthStartDate && p.ExamResultDate1 <= monthLastDate).ToList();
            var allClasses = _ICatalogMasterService.GetAllClassMasters(ref count).ToList();


            weeks.Eventtypelist = _ISchedularService.GetAllEventTypeList(ref count);
            weeks.IsExamShow = true;
            var ExamShow = _ISchoolSettingService.GetorSetSchoolData("ShowExaminationInAppCalendar", "1"
                                                        , null, "Show Hide Exam In Calendar").AttributeValue;
            if (ExamShow == "0")
                weeks.IsExamShow = false;

            if(weeks.IsExamShow==false)
            {
               allExams.Clear();
            }
            var EvntType = _ISchedularService.GetAllEventTypeList(ref count).ToList();

            if (EvntType.Where(m => m.EventType1 == "Activity").FirstOrDefault().ShowInAppCalendar == 0)
                eventsOfMonth = eventsOfMonth.Where(m => m.EventType.EventType1 != "Activity").ToList();
          
            if (EvntType.Where(m => m.EventType1 == "Announcement").FirstOrDefault().ShowInAppCalendar == 0)
                eventsOfMonth = eventsOfMonth.Where(m => m.EventType.EventType1 != "Announcement").ToList();

            if (EvntType.Where(m => m.EventType1 == "Holiday").FirstOrDefault().ShowInAppCalendar == 0)
                eventsOfMonth = eventsOfMonth.Where(m => m.EventType.EventType1 != "Holiday").ToList();

            if (EvntType.Where(m => m.EventType1 == "Vacation").FirstOrDefault().ShowInAppCalendar == 0)
                eventsOfMonth = eventsOfMonth.Where(m => m.EventType.EventType1 != "Vacation").ToList();

            if (EvntType.Where(m => m.EventType1 == "Circular").FirstOrDefault().ShowInAppCalendar == 0)
                eventsOfMonth = eventsOfMonth.Where(m => m.EventType.EventType1 != "Circular").ToList();

            if (EvntType.Where(m => m.EventType1 == "News").FirstOrDefault().ShowInAppCalendar == 0)
                eventsOfMonth = eventsOfMonth.Where(m => m.EventType.EventType1 != "News").ToList();

            if (EvntType.Where(m => m.EventType1 == "Thought").FirstOrDefault().ShowInAppCalendar == 0)
                eventsOfMonth = eventsOfMonth.Where(m => m.EventType.EventType1 != "Thought").ToList();


            foreach (DateTime day in dt)
            {
                var todaysevents = new List<Event>();
                var todayancs = new List<Event>();
                var todaysact = new List<Event>();
                var todayexam = new List<ExamDateSheetDetail>();
                var todayResult = new List<ExamResultDate>();

                todaysevents = eventsOfMonth.Where(p => (p.StartDate == day || p.EndDate == day) && p.EventType.EventType1 != "Announcement" && p.EventType.EventType1 != "Activity").ToList();
               
                
                todayancs = eventsOfMonth.Where(p => p.StartDate == day && p.EventType.EventType1 == "Announcement").ToList();
                todaysact = eventsOfMonth.Where(p => p.StartDate == day && p.EventType.EventType1 == "Activity").ToList();
                todayexam = allExams.Where(p => p.ExamDate == day).ToList();
                todayResult = allResults.Where(p => p.ResultPublishDate == day).ToList();

                if (userType.ToLower() == "student")
                {
                    var studentClass = _IStudentService.GetAllStudentClasss(ref count).Where(p => p.SessionId == currentSession.SessionId && p.StudentId == user.UserContactId).FirstOrDefault();
                    if (studentClass != null)
                    {
                        classId = studentClass.ClassId;
                        requiredEventIds = alleventDetails.Where(p => p.ClassId == classId).Select(p => p.EventId).Distinct().ToList();
                        requiredStandardIds = allClasses.Where(p => p.ClassId == classId).Select(p => p.StandardId).Distinct().ToList();
                    }
                }
                else if (userType.ToLower() == "guardian")
                {
                    var guardian = _IGuardianService.GetGuardianById(GuardianId: (int)user.UserContactId);
                    if (guardian != null)
                    {
                        var studentClass = _IStudentService.GetAllStudentClasss(ref count).Where(p => p.SessionId == currentSession.SessionId && p.StudentId == guardian.StudentId).FirstOrDefault();
                        if (studentClass != null)
                        {
                            classId = studentClass.ClassId;
                            requiredEventIds = alleventDetails.Where(p => p.ClassId == classId).Select(p => p.EventId).Distinct().ToList();
                            requiredStandardIds = allClasses.Where(p => p.ClassId == classId).Select(p => p.StandardId).Distinct().ToList();
                        }
                    }
                }
                else if (userType.ToLower() == "teacher")
                {
                    var TeacherStandardIds = _IStaffService.GetAllTeacherClasss(ref count).Where(p => p.TeacherId == user.UserContactId).Select(p => p.StandardId).ToList();
                    if (TeacherStandardIds.Count > 0)
                    {
                        var classIds = allClasses.Where(p => TeacherStandardIds.Contains(p.StandardId)).Select(p => p.ClassId).ToList();
                        requiredEventIds = alleventDetails.Where(p => classIds.Contains((int)p.ClassId)).Select(p => p.EventId).Distinct().ToList();
                        requiredStandardIds = allClasses.Where(p => classIds.Contains(p.ClassId)).Select(p => p.StandardId).Distinct().ToList();
                    }
                }

                if (userType.ToLower() == "guardian" || userType.ToLower() == "student" || userType.ToLower() == "teacher")
                {
                    if (requiredEventIds.Count > 0)
                    {
                        var IsAllTrueExist = eventsOfMonth.Where(p => p.IsAllSchool == true && p.StartDate == day && p.EventType.EventType1 == "Activity").ToList();
                        todaysact = eventsOfMonth.Where(p => p.StartDate == day && p.EventType.EventType1 == "Activity" && requiredEventIds.Contains(p.EventId)).ToList();
                        if (IsAllTrueExist.Count > 0)
                        {
                            foreach (var item in IsAllTrueExist)
                                todaysact.Add(item);
                        }
                    }
                    else
                        todaysact = eventsOfMonth.Where(p => p.StartDate == day && p.EventType.EventType1 == "Activity" && p.IsAllSchool == true).ToList();

                    todayexam = allExams.Where(p => p.ExamDate == day && requiredStandardIds.Contains(p.StandardId)).ToList();
                    todayResult = allResults.Where(p => p.ResultPublishDate == day && requiredStandardIds.Contains(p.StandardId)).ToList();
                }

                switch (GetWeekOfMonth(day))
                {
                    case 1:
                        CalenderDay dy1 = new CalenderDay();
                        dy1.Date = day;
                        dy1.dateStr = day.ToString("dd/MM/yyyy");
                        dy1.dtDay = day.Day;
                        dy1.daycolumn = GetDateInfo(dy1.Date);
                        foreach (var item in todaysevents)
                        {
                            dy1.Events.Add(new EventsCalenderDay {ColorCode=item.EventType.EventColor, EventName = item.EventType.EventType1, startday = Convert.ToDateTime(item.StartDate).Day.ToString(), endday = Convert.ToDateTime(item.EndDate).Day.ToString() });
                        }
                        if (todayexam.Count > 0)
                        {
                            dy1.IsTodayExam = true;
                            dy1.ExamCount = todayexam.Count.ToString();
                        }
                        if (todayancs.Count > 0)
                        {
                            dy1.IsTodayanc = true;
                            dy1.AncCount = todayancs.Count.ToString();
                        }
                        if (todaysact.Count > 0)
                        {
                            dy1.IsTodayact = true;
                            dy1.ActCount = todaysact.Count.ToString();
                        }
                        if (todayResult.Count > 0)
                        {
                            dy1.IsTodayResult = true;
                            dy1.ResultCount = todayResult.Count.ToString();
                        }
                        weeks.Week1.Add(dy1);
                        break;
                    case 2:
                        CalenderDay dy2 = new CalenderDay();
                        dy2.Date = day;
                        dy2.dateStr = day.ToString("dd/MM/yyyy");
                        dy2.dtDay = day.Day;
                        dy2.daycolumn = GetDateInfo(dy2.Date);
                        foreach (var item in todaysevents)
                        {
                            dy2.Events.Add(new EventsCalenderDay { ColorCode = item.EventType.EventColor, EventName = item.EventType.EventType1, startday = Convert.ToDateTime(item.StartDate).Day.ToString(), endday = Convert.ToDateTime(item.EndDate).Day.ToString() });
                        }
                        if (todayexam.Count > 0)
                        {
                            dy2.IsTodayExam = true;
                            dy2.ExamCount = todayexam.Count.ToString();
                        }
                        if (todayancs.Count > 0)
                        {
                            dy2.IsTodayanc = true;
                            dy2.AncCount = todayancs.Count.ToString();
                        }
                        if (todaysact.Count > 0)
                        {
                            dy2.IsTodayact = true;
                            dy2.ActCount = todaysact.Count.ToString();
                        }
                        if (todayResult.Count > 0)
                        {
                            dy2.IsTodayResult = true;
                            dy2.ResultCount = todayResult.Count.ToString();
                        }
                        weeks.Week2.Add(dy2);
                        break;
                    case 3:
                        CalenderDay dy3 = new CalenderDay();
                        dy3.Date = day;
                        dy3.dateStr = day.ToString("dd/MM/yyyy");
                        dy3.dtDay = day.Day;
                        dy3.daycolumn = GetDateInfo(dy3.Date);
                        foreach (var item in todaysevents)
                        {
                            dy3.Events.Add(new EventsCalenderDay { ColorCode = item.EventType.EventColor, EventName = item.EventType.EventType1, startday = Convert.ToDateTime(item.StartDate).Day.ToString(), endday = Convert.ToDateTime(item.EndDate).Day.ToString() });
                        }
                        if (todayexam.Count > 0)
                        {
                            dy3.IsTodayExam = true;
                            dy3.ExamCount = todayexam.Count.ToString();
                        }
                        if (todayancs.Count > 0)
                        {
                            dy3.IsTodayanc = true;
                            dy3.AncCount = todayancs.Count.ToString();
                        }
                        if (todaysact.Count > 0)
                        {
                            dy3.IsTodayact = true;
                            dy3.ActCount = todaysact.Count.ToString();
                        }
                        if (todayResult.Count > 0)
                        {
                            dy3.IsTodayResult = true;
                            dy3.ResultCount = todayResult.Count.ToString();
                        }
                        weeks.Week3.Add(dy3);
                        break;
                    case 4:
                        CalenderDay dy4 = new CalenderDay();
                        dy4.Date = day;
                        dy4.dateStr = day.ToString("dd/MM/yyyy");
                        dy4.dtDay = day.Day;
                        dy4.daycolumn = GetDateInfo(dy4.Date);
                        foreach (var item in todaysevents)
                        {
                            dy4.Events.Add(new EventsCalenderDay { ColorCode = item.EventType.EventColor, EventName = item.EventType.EventType1, startday = Convert.ToDateTime(item.StartDate).Day.ToString(), endday = Convert.ToDateTime(item.EndDate).Day.ToString() });
                        }
                        if (todayexam.Count > 0)
                        {
                            dy4.IsTodayExam = true;
                            dy4.ExamCount = todayexam.Count.ToString();
                        }
                        if (todayancs.Count > 0)
                        {
                            dy4.IsTodayanc = true;
                            dy4.AncCount = todayancs.Count.ToString();
                        }
                        if (todaysact.Count > 0)
                        {
                            dy4.IsTodayact = true;
                            dy4.ActCount = todaysact.Count.ToString();
                        }
                        if (todayResult.Count > 0)
                        {
                            dy4.IsTodayResult = true;
                            dy4.ResultCount = todayResult.Count.ToString();
                        }
                        weeks.Week4.Add(dy4);
                        break;
                    case 5:
                        CalenderDay dy5 = new CalenderDay();
                        dy5.Date = day;
                        dy5.dateStr = day.ToString("dd/MM/yyyy");
                        dy5.dtDay = day.Day;
                        dy5.daycolumn = GetDateInfo(dy5.Date);
                        foreach (var item in todaysevents)
                        {
                            dy5.Events.Add(new EventsCalenderDay { ColorCode = item.EventType.EventColor, EventName = item.EventType.EventType1, startday = Convert.ToDateTime(item.StartDate).Day.ToString(), endday = Convert.ToDateTime(item.EndDate).Day.ToString() });
                        }
                        if (todayexam.Count > 0)
                        {
                            dy5.IsTodayExam = true;
                            dy5.ExamCount = todayexam.Count.ToString();
                        }
                        if (todayancs.Count > 0)
                        {
                            dy5.IsTodayanc = true;
                            dy5.AncCount = todayancs.Count.ToString();
                        }
                        if (todaysact.Count > 0)
                        {
                            dy5.IsTodayact = true;
                            dy5.ActCount = todaysact.Count.ToString();
                        }
                        if (todayResult.Count > 0)
                        {
                            dy5.IsTodayResult = true;
                            dy5.ResultCount = todayResult.Count.ToString();
                        }
                        weeks.Week5.Add(dy5);
                        break;
                    case 6:
                        CalenderDay dy6 = new CalenderDay();
                        dy6.Date = day;
                        dy6.dateStr = day.ToString("dd/MM/yyyy");
                        dy6.dtDay = day.Day;
                        dy6.daycolumn = GetDateInfo(dy6.Date);
                        foreach (var item in todaysevents)
                        {
                            dy6.Events.Add(new EventsCalenderDay { ColorCode = item.EventType.EventColor, EventName = item.EventType.EventType1, startday = Convert.ToDateTime(item.StartDate).Day.ToString(), endday = Convert.ToDateTime(item.EndDate).Day.ToString() });
                        }
                        if (todayexam.Count > 0)
                        {
                            dy6.IsTodayExam = true;
                            dy6.ExamCount = todayexam.Count.ToString();
                        }
                        if (todayancs.Count > 0)
                        {
                            dy6.IsTodayanc = true;
                            dy6.AncCount = todayancs.Count.ToString();
                        }
                        if (todaysact.Count > 0)
                        {
                            dy6.IsTodayact = true;
                            dy6.ActCount = todaysact.Count.ToString();
                        }
                        if (todayResult.Count > 0)
                        {
                            dy6.IsTodayResult = true;
                            dy6.ResultCount = todayResult.Count.ToString();
                        }
                        weeks.Week6.Add(dy6);
                        break;
                };
            }

            while (weeks.Week1.Count < 7)
            {
                CalenderDay dy = null;
                weeks.Week1.Insert(0, dy);
            }
            while (weeks.Week4.Count < 7)
            {
                CalenderDay dy = null;
                weeks.Week4.Insert(weeks.Week4.Count, dy);
            }
            if (weeks.Week5.Count > 0)
            {
                while (weeks.Week5.Count < 7)
                {
                    CalenderDay dy = null;
                    weeks.Week5.Insert(weeks.Week5.Count, dy);
                }
            }
            if (weeks.Week6.Count > 0)
            {
                while (weeks.Week6.Count < 7)
                {
                    CalenderDay dy = null;
                    weeks.Week6.Insert(weeks.Week6.Count, dy);
                }
            }
            if (month == 12)
            {
                weeks.nextMonth = (01).ToString() + "/" + (year + 1).ToString();
                weeks.prevMonth = (month - 1).ToString() + "/" + (year).ToString();
            }
            else if (month == 1)
            {
                weeks.nextMonth = (month + 1).ToString() + "/" + (year).ToString();
                weeks.prevMonth = (12).ToString() + "/" + (year - 1).ToString();
            }
            else
            {
                weeks.nextMonth = (month + 1).ToString() + "/" + (year).ToString();
                weeks.prevMonth = (month - 1).ToString() + "/" + (year).ToString();
            }

            return weeks;
        }

        //get all dates for a month for the year specified
        public static List<DateTime> GetDates(int year, int month)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(year, month))
            .Select(day => new DateTime(year, month, day))
            .ToList();
        }

        //get number of week for the selected month by passing in a date value
        public static int GetWeekOfMonth(DateTime date)
        {
            DateTime beginningOfMonth = new DateTime(date.Year, date.Month, 1);
            while (date.Date.AddDays(1).DayOfWeek != DayOfWeek.Sunday)
                date = date.AddDays(1);
            return (int)Math.Truncate((double)date.Subtract(beginningOfMonth).TotalDays / 7f) + 1;
        }

        //translate each day to a day number for mapping to week
        public int GetDateInfo(DateTime now)
        {
            int dayNumber = 0;
            DateTime dt = now.Date;
            string dayStr = Convert.ToString(dt.DayOfWeek);

            if (dayStr.ToLower() == "sunday")
            {
                dayNumber = 0;
            }
            else if (dayStr.ToLower() == "monday")
            {
                dayNumber = 1;
            }
            else if (dayStr.ToLower() == "tuesday")
            {
                dayNumber = 2;
            }
            else if (dayStr.ToLower() == "wednesday")
            {
                dayNumber = 3;
            }
            else if (dayStr.ToLower() == "thursday")
            {
                dayNumber = 4;
            }
            else if (dayStr.ToLower() == "friday")
            {
                dayNumber = 5;
            }
            else if (dayStr.ToLower() == "saturday")
            {
                dayNumber = 6;
            }
            return dayNumber;
        }

        #region ajaxmethods
        public ActionResult GetEventInfo(DateTime evtdate, string evtname)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                var userType = user.ContactType.ContactType1;
                var usercontactId = user.UserContactId;
                var currentSession = _ICatalogMasterService.GetCurrentSession();
                var classId = (int?)null;
                var requiredEventIds = new List<int?>();

                var evnt = _ISchedularService.GetAllEventList(ref count);
                var alleventDetails = _ISchedularService.GetAllEventDetailList(ref count).ToList();
                var allClasses = _ICatalogMasterService.GetAllClassMasters(ref count).ToList();
                evnt = evnt.Where(p => (p.StartDate == evtdate || p.EndDate == evtdate) && p.EventType.EventType1 == evtname.Trim()).ToList();

                if (userType.ToLower() == "student")
                {
                    var studentClass = _IStudentService.GetAllStudentClasss(ref count).Where(p => p.SessionId == currentSession.SessionId && p.StudentId == user.UserContactId).FirstOrDefault();
                    if (studentClass != null)
                    {
                        classId = studentClass.ClassId;
                        requiredEventIds = alleventDetails.Where(p => p.ClassId == classId).Select(p => p.EventId).Distinct().ToList();
                    }
                }
                else if (userType.ToLower() == "guardian")
                {
                    var guardian = _IGuardianService.GetGuardianById(GuardianId: (int)user.UserContactId);
                    if (guardian != null)
                    {
                        var studentClass = _IStudentService.GetAllStudentClasss(ref count).Where(p => p.SessionId == currentSession.SessionId && p.StudentId == guardian.StudentId).FirstOrDefault();
                        if (studentClass != null)
                        {
                            classId = studentClass.ClassId;
                            requiredEventIds = alleventDetails.Where(p => p.ClassId == classId).Select(p => p.EventId).Distinct().ToList();
                        }
                    }
                }
                else if (userType.ToLower() == "teacher")
                {
                    var TeacherStandardIds = _IStaffService.GetAllTeacherClasss(ref count).Where(p => p.TeacherId == user.UserContactId).Select(p => p.StandardId).ToList();
                    if (TeacherStandardIds.Count > 0)
                    {
                        var classIds = allClasses.Where(p => TeacherStandardIds.Contains(p.StandardId)).Select(p => p.ClassId).ToList();
                        requiredEventIds = alleventDetails.Where(p => classIds.Contains((int)p.ClassId)).Select(p => p.EventId).Distinct().ToList();
                    }
                }

                if (userType.ToLower() == "guardian" || userType.ToLower() == "student" || userType.ToLower() == "teacher")
                {
                    if (requiredEventIds.Count > 0)
                    {
                        if (evtname.Trim() == "Activity")
                        {
                            var IsAllTrueExist = evnt.Where(p => p.IsAllSchool == true && (p.StartDate == evtdate || p.EndDate == evtdate) && p.EventType.EventType1 == evtname.Trim()).ToList();
                            evnt = evnt.Where(p => (p.StartDate == evtdate || p.EndDate == evtdate) && p.EventType.EventType1 == evtname.Trim() && requiredEventIds.Contains(p.EventId)).ToList();
                            if (IsAllTrueExist.Count > 0)
                            {
                                foreach (var item in IsAllTrueExist)
                                    evnt.Add(item);
                            }
                        }
                        else
                            evnt = evnt.Where(p => (p.StartDate == evtdate || p.EndDate == evtdate) && p.EventType.EventType1 == evtname.Trim()).ToList();
                    }
                    else
                    {
                        if (evtname.Trim() == "Activity")
                            evnt = evnt.Where(p => (p.StartDate == evtdate || p.EndDate == evtdate) && p.EventType.EventType1 == evtname.Trim() && p.IsAllSchool == true).ToList();
                        else
                            evnt = evnt.Where(p => (p.StartDate == evtdate || p.EndDate == evtdate) && p.EventType.EventType1 == evtname.Trim()).ToList();
                    }
                }


                if (evtname.Trim() == "Announcement" || evtname.Trim() == "Activity")
                {
                    var data = new List<EventGrid>();
                    foreach (var item in evnt)
                    {
                        data.Add(new EventGrid { EventId = item.EventId, EventTitle = item.EventTitle });
                    }
                    return Json(new { status = "success", data = data, IsGridData = true });
                }
                var popUpData = new EventPopUp();
                //if (evtname.Trim() == "Holiday" || evtname.Trim() == "Vacation" || evtname.Trim() == "Thought" || evtname.Trim() == "News" || evtname.Trim() == "Circular")
                    if (evnt.Count() > 0)
                   {
                    var eventdata = evnt.FirstOrDefault();
                    popUpData.EventTitle = eventdata.EventTitle;
                    popUpData.EventType = eventdata.EventType.EventType1;
                    popUpData.EventDesc = eventdata.Description;
                    popUpData.FromDate = String.Format("{0:dd/MM/yyyy}", eventdata.StartDate);
                    if (eventdata.EndDate != null)
                        popUpData.ToDate = String.Format("{0:dd/MM/yyyy}", eventdata.EndDate);
                    else
                        popUpData.ToDate = eventdata.EndDate.ToString();
                }
                return Json(new { status = "success", data = popUpData, IsGridData = false });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }
        public ActionResult GetExamInfo(DateTime evtdate)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);

            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {

                var userType = user.ContactType.ContactType1;
                var usercontactId = user.UserContactId;
                var currentSession = _ICatalogMasterService.GetCurrentSession();
                var classId = (int?)null;
                var requiredStandardIds = new List<int?>();

                var exms = _IExamService.GetAllExamDateSheetDetails(ref count, ExamDate: evtdate);
                var allStandard = _ICatalogMasterService.GetAllStandardMasters(ref count).ToList();
                var allSubjects = _ISubjectService.GetAllSubjects(ref count).ToList();
                var allClasses = _ICatalogMasterService.GetAllClassMasters(ref count).ToList();

                if (userType.ToLower() == "student")
                {
                    var studentClass = _IStudentService.GetAllStudentClasss(ref count).Where(p => p.SessionId == currentSession.SessionId && p.StudentId == user.UserContactId).FirstOrDefault();
                    if (studentClass != null)
                    {
                        classId = studentClass.ClassId;
                        requiredStandardIds = allClasses.Where(p => p.ClassId == classId).Select(p => p.StandardId).Distinct().ToList();
                    }
                }
                else if (userType.ToLower() == "guardian")
                {
                    var guardian = _IGuardianService.GetGuardianById(GuardianId: (int)user.UserContactId);
                    if (guardian != null)
                    {
                        var studentClass = _IStudentService.GetAllStudentClasss(ref count).Where(p => p.SessionId == currentSession.SessionId && p.StudentId == guardian.StudentId).FirstOrDefault();
                        if (studentClass != null)
                        {
                            classId = studentClass.ClassId;
                            requiredStandardIds = allClasses.Where(p => p.ClassId == classId).Select(p => p.StandardId).Distinct().ToList();
                        }
                    }
                }
                else if (userType.ToLower() == "teacher")
                {
                    var TeacherStandardIds = _IStaffService.GetAllTeacherClasss(ref count).Where(p => p.TeacherId == user.UserContactId).Select(p => p.StandardId).ToList();
                    if (TeacherStandardIds.Count > 0)
                    {
                        var classIds = allClasses.Where(p => TeacherStandardIds.Contains(p.StandardId)).Select(p => p.ClassId).ToList();
                        requiredStandardIds = allClasses.Where(p => classIds.Contains(p.ClassId)).Select(p => p.StandardId).Distinct().ToList();
                    }
                }

                if (userType.ToLower() == "guardian" || userType.ToLower() == "student" || userType.ToLower() == "teacher")
                    exms = exms.Where(p => requiredStandardIds.Contains(p.StandardId)).ToList();


                if (exms.Count > 0)
                {
                    var ExamsList = new List<ExamNotification>();
                    foreach (var item in exms)
                    {
                        var standard = allStandard.Where(p => p.StandardId == item.StandardId).FirstOrDefault();
                        var subject = allSubjects.Where(p => p.SubjectId == item.SubjectId).FirstOrDefault();
                        var timeSlot = item.ExamTimeSlot.ExamTimeSlot1;
                        if (standard != null && subject != null)
                            ExamsList.Add(new ExamNotification { ClassSubjectName = standard.Standard + "/" + subject.Subject1, TimeSlot = timeSlot, ExamDate = item.ExamDate });
                    }
                    return Json(new { status = "success", data = ExamsList });
                }
                else
                {
                    return Json(new { status = "failed" });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public ActionResult GetExamResultInfo(DateTime exmrstdate)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);

            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                var userType = user.ContactType.ContactType1;
                var usercontactId = user.UserContactId;
                var currentSession = _ICatalogMasterService.GetCurrentSession();
                var classId = (int?)null;
                var requiredStandardIds = new List<int?>();

                var exmsrsts = _IExamService.GetAllExamResultDates(ref count).Where(p => p.ResultPublishDate == exmrstdate).ToList();
                var allStandards = _ICatalogMasterService.GetAllStandardMasters(ref count).ToList();
                var allClasses = _ICatalogMasterService.GetAllClassMasters(ref count).ToList();

                if (userType.ToLower() == "student")
                {
                    var studentClass = _IStudentService.GetAllStudentClasss(ref count).Where(p => p.SessionId == currentSession.SessionId && p.StudentId == user.UserContactId).FirstOrDefault();
                    if (studentClass != null)
                    {
                        classId = studentClass.ClassId;
                        requiredStandardIds = allClasses.Where(p => p.ClassId == classId).Select(p => p.StandardId).Distinct().ToList();
                    }
                }
                else if (userType.ToLower() == "guardian")
                {
                    var guardian = _IGuardianService.GetGuardianById(GuardianId: (int)user.UserContactId);
                    if (guardian != null)
                    {
                        var studentClass = _IStudentService.GetAllStudentClasss(ref count).Where(p => p.SessionId == currentSession.SessionId && p.StudentId == guardian.StudentId).FirstOrDefault();
                        if (studentClass != null)
                        {
                            classId = studentClass.ClassId;
                            requiredStandardIds = allClasses.Where(p => p.ClassId == classId).Select(p => p.StandardId).Distinct().ToList();
                        }
                    }
                }
                else if (userType.ToLower() == "teacher")
                {
                    var TeacherStandardIds = _IStaffService.GetAllTeacherClasss(ref count).Where(p => p.TeacherId == user.UserContactId).Select(p => p.StandardId).ToList();
                    if (TeacherStandardIds.Count > 0)
                    {
                        var classIds = allClasses.Where(p => TeacherStandardIds.Contains(p.StandardId)).Select(p => p.ClassId).Distinct().ToList();
                        requiredStandardIds = allClasses.Where(p => classIds.Contains(p.ClassId)).Select(p => p.StandardId).Distinct().ToList();
                    }
                }

                if (userType.ToLower() == "guardian" || userType.ToLower() == "student" || userType.ToLower() == "teacher")
                    exmsrsts = exmsrsts.Where(p => requiredStandardIds.Contains(p.StandardId)).ToList();

                if (exmsrsts.Count > 0)
                {
                    var ExamsResultList = new List<ExamResultNotification>();
                    var ResultStandardIds = exmsrsts.Select(p => p.StandardId).ToList();
                    var ResultStandards = allStandards.Where(p => ResultStandardIds.Contains(p.StandardId));
                    foreach (var item in exmsrsts)
                        ExamsResultList.Add(new ExamResultNotification { Standard = ResultStandards.Where(p => p.StandardId == item.StandardId).FirstOrDefault().Standard });
                    return Json(new { status = "success", data = ExamsResultList });
                }
                else
                {
                    return Json(new { status = "failed" });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }

        public ActionResult GetEventById(int id)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                if (id > 0)
                {
                    var evnt = _ISchedularService.GetEventById(EventId: id);
                    var eventClasses = "";
                    if (evnt != null)
                    {
                        var popUpData = new EventPopUp();
                        if (evnt.EventDetails.Count > 0)
                        {
                            if (evnt.EventDetails.Count == 1)
                            {
                                foreach (var evt in evnt.EventDetails)
                                    popUpData.Class = _ICatalogMasterService.GetClassMasterById((int)evt.ClassId).Class;
                            }
                            else
                            {
                                int counter = 0;
                                foreach (var evt in evnt.EventDetails)
                                {
                                    counter = counter + 1;
                                    if (counter == evnt.EventDetails.Count)
                                        eventClasses += _ICatalogMasterService.GetClassMasterById((int)evt.ClassId).Class;
                                    else
                                        eventClasses += _ICatalogMasterService.GetClassMasterById((int)evt.ClassId).Class + " , ";
                                }
                                popUpData.Class = eventClasses;
                            }
                        }
                        if (evnt.EventDetails.Count == 0 && evnt.EventType.EventType1 == "Activity")
                        {
                            popUpData.Class = "All Classes";
                        }
                        popUpData.EventType = evnt.EventType.EventType1;
                        popUpData.EventDesc = evnt.Description;
                        popUpData.EventTitle = evnt.EventTitle;
                        popUpData.EventVenue = evnt.Venue;
                        popUpData.FromDate = String.Format("{0:dd/MM/yyyy}", evnt.StartDate);
                        popUpData.ToDate = String.Format("{0:dd/MM/yyyy}", evnt.EndDate);
                        if (evnt.StartTime != null)
                            popUpData.FromTime = String.Format("{0:hh:mm tt}", DateTime.Today.Add((TimeSpan)(evnt.StartTime)));
                        else
                            popUpData.FromTime = evnt.StartTime.ToString();
                        if (evnt.EndTime != null)
                            popUpData.TillTime = String.Format("{0:hh:mm tt}", DateTime.Today.Add((TimeSpan)(evnt.EndTime)));
                        else
                            popUpData.TillTime = evnt.EndTime.ToString();
                        return Json(new { status = "success", data = popUpData });
                    }
                    else
                        return Json(new { status = "failed" });
                }
                else
                    return Json(new { status = "failed" });

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    p3 = ex.Message
                });
            }
        }
        #endregion

        #endregion
    }
}