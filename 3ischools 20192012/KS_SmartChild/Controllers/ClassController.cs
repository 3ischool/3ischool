﻿using BAL.BAL.Common;
using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.ClassPeriodModule;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.ExaminationModule;
using DAL.DAL.Kendo;
using DAL.DAL.MenuModel;
using DAL.DAL.Schedular;
using DAL.DAL.SettingService;
using DAL.DAL.StaffModule;
using DAL.DAL.StudentModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Class;
using ViewModel.ViewModel.Student;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using DAL.DAL.StockModule;

namespace KS_SmartChild.Controllers
{
    public class ClassController : Controller
    {
        #region fields

        public int count = 0;
        private readonly IUserService _IUserService;
        private readonly ILogService _Logger;
        private readonly IConnectionService _IConnectionService;
        private readonly IWebHelper _WebHelper;
        private readonly INavigationService _INavigationService;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IExamService _IExamService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IClassPeriodService _IClassPeriodService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IStaffService _IStaffService;
        private readonly IStudentService _IStudentService;
        private readonly ISchedularService _ISchedularService;
        private readonly ISubjectService _ISubjectService;
        private readonly IExportHelper _IExportHelper;
        private readonly IStockService _IStockService;



        #endregion

        #region ctor

        public ClassController(IUserService IUserService,
            ILogService Logger,
            IConnectionService IConnectionService,
            IWebHelper WebHelper,
            INavigationService INavigationService,
            IDropDownService IDropDownService,
            IActivityLogMasterService IActivityLogMasterService,
            IExamService IExamService,
            IActivityLogService IActivityLogService,
            ICatalogMasterService ICatalogMasterService,
            IClassPeriodService IClassPeriodService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption,
            ISchoolSettingService ISchoolSettingService,
            IStaffService IStaffService,
            IStudentService IStudentService,
            ISchedularService ISchedularService,
            ISubjectService ISubjectService, IExportHelper IExportHelper,
            IStockService IStockService)
        {
            this._IUserService = IUserService;
            this._Logger = Logger;
            this._IConnectionService = IConnectionService;
            this._WebHelper = WebHelper;
            this._INavigationService = INavigationService;
            this._IDropDownService = IDropDownService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._IExamService = IExamService;
            this._IActivityLogService = IActivityLogService;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IClassPeriodService = IClassPeriodService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._IStaffService = IStaffService;
            this._IStudentService = IStudentService;
            this._ISchedularService = ISchedularService;
            this._ISubjectService = ISubjectService;
            this._IExportHelper = IExportHelper;
            this._IStockService = IStockService;
        }

        #endregion

        #region utility
        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        public string ConvertTime(TimeSpan time)
        {
            TimeSpan timespan = new TimeSpan(time.Hours, time.Minutes, time.Seconds);
            DateTime datetime = DateTime.Today.Add(timespan);
            string sdisplayTime = datetime.ToString("hh:mm tt");
            return sdisplayTime;
        }
        #endregion

        #region Methods

        public ActionResult GetStudentsList(string id,int sessionId=0)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                IList<StudentModel> studentdetaillist = new List<StudentModel>();
                StudentModel studentclass = new StudentModel();
                var ClassId = 0;
                if (!string.IsNullOrEmpty(id))
                    int.TryParse(_ICustomEncryption.base64d(id), out ClassId);
                var currentSession = _ICatalogMasterService.GetCurrentSession();
                if (sessionId > 0)
                {
                    currentSession = _ICatalogMasterService.GetAllSessions(ref count).Where(m=>m.SessionId==sessionId).FirstOrDefault();
                }
                var StudentsClasslist = _IStudentService.GetAllStudentClasss(ref count, ClassId: ClassId, SessionId: currentSession.SessionId).Where(m => m.Student != null && m.Student.StudentStatusDetails != null && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault() != null && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus.ToLower() != "left");
                if (StudentsClasslist.Count() > 0)
                    foreach (var item in StudentsClasslist)
                    {
                        studentclass = new StudentModel();
                        studentclass.FullName = item.Student.FName;
                        if (item.Student.LName != null)
                            studentclass.FullName += " " + item.Student.LName;
                        studentclass.AdmnNo = item.Student.AdmnNo;
                        studentclass.CurrentClass = item != null ? item.ClassMaster.Class : "NA";
                        studentclass.RollNo = item.RollNo != null ? item.RollNo : "";
                        studentclass.EncStudentId = _ICustomEncryption.base64e(item.StudentId.ToString());
                        studentdetaillist.Add(studentclass);
                    }

                return Json(new
                {
                    status = "success",
                    data = studentdetaillist,
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }

        #region Class Master

        public ClassMasterModel prepareClassMastermodel(ClassMasterModel model)
        {
            // prepare model 
            // Drop Down Lists
            //model.AvailableStandard = _IDropDownService.StandardList();
            model.AvailableSection = new List<SelectListItem>();
            var standardList = _ICatalogMasterService.GetAllStandardMasters(ref count);
            var sectionList = _ICatalogMasterService.GetAllSections(ref count);
            var classmasterList = _ICatalogMasterService.GetAllClassMasters(ref count);

            List<SelectListItem> Dropdownliststandard = new List<SelectListItem>();
            Dropdownliststandard.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });


            List<SelectListItem> Dropdownlistsection = new List<SelectListItem>();
            Dropdownlistsection.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });

            foreach (var stdl in standardList)
            {
                var sectionrecordpending = 0;
                foreach (var secl in sectionList)
                {
                    var entryexists = classmasterList.Where(m => m.SectionId == secl.SectionId && m.StandardId == stdl.StandardId).ToList();
                    if (entryexists.Count > 0)
                    {

                    }
                    else
                    {
                        sectionrecordpending++;
                    }

                }
                if (sectionrecordpending > 0)
                {
                    Dropdownliststandard.Add(new SelectListItem { Selected = false, Text = stdl.Standard, Value = stdl.StandardId.ToString() });
                }
            }
            model.CurrentSession = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.IsDefualt == true).FirstOrDefault().SessionId;

            model.AvailableStandard = Dropdownliststandard;
            model.AvailableSessions = _IDropDownService.SessionList().Where(m=>m.Value!="" && m.Value!=null).ToList();
            model.AvailableStandardFilter = _IDropDownService.StandardList();
            model.AvailableSectionFilter = _IDropDownService.SectionList();
            model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
            model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");

            model.IsClassNameEditable = false;
            var isClassNameEditEnabled = _ISchoolSettingService.GetorSetSchoolData("IsClassNameEnable","0"
                                                         ,null,"Enabled/Disabled Dropdown Class").AttributeValue;
            if (isClassNameEditEnabled == "1")
                model.IsClassNameEditable = true;

            if (model.StandardId!=null &&  model.StandardId > 0)
            {
                // get standard classes
                var classmasters = _ICatalogMasterService.GetAllClassMasters(ref count, StandardId: model.StandardId);
                var SectionIds = classmasters.Select(s => s.SectionId).ToArray();

                // get all sections
                var allsections = _ICatalogMasterService.GetAllSections(ref count);
                allsections = allsections.Where(s => !SectionIds.Contains(s.SectionId)).ToList();

                foreach (var section in allsections)
                     model.AvailableSection.Add(new SelectListItem { Selected = false, Text = section.Section1, Value = section.SectionId.ToString() });
                
            }


            return model;
        }

        public ActionResult ClassMaster()
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "View");
                    if (!isauth)
                        return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                var model = new ClassMasterModel();

                model = prepareClassMastermodel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "Add Record");
                model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "Edit Record");
                model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "Delete Record");
                model.IsAuthToView = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "View Record");
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult ClassMaster(ClassMasterModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                int Classid = 0;
                if (!string.IsNullOrEmpty(model.EncClassId))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(model.EncClassId);

                    if (int.TryParse(qsid, out Classid))
                    {

                    }
                }
                if (model.StandardId > 0 && model.SectionId > 0)
                {
                    var ClassMasterInsertCheck = _ICatalogMasterService.GetAllClassMasters(ref count, StandardId: model.StandardId, SectionId: model.SectionId);
                    ClassMasterInsertCheck = ClassMasterInsertCheck.Where(c => c.ClassId != Classid).ToList();
                    if (ClassMasterInsertCheck.Count > 0)
                        ModelState.AddModelError("Class", "Already exist");
                }
                if (model.Index!=null   && model.Index > 0)
                {
                    var ClassMasterInsertCheck = _ICatalogMasterService.GetAllClassMasters(ref count, SortingIndex: model.Index).ToList();
                    if (ClassMasterInsertCheck.Count > 0)
                        ModelState.AddModelError("Index", "Already exist");
                }

                if (ModelState.IsValid)
                {
                    var standard = "";
                    var session = "";
                    ClassMaster ClassMaster = new ClassMaster();
                    // Class add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    var ClassMastertable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassMaster").FirstOrDefault();


                    if (Classid > 0)
                    {
                        // get clas by id
                        ClassMaster = _ICatalogMasterService.GetClassMasterById((int)Classid, true);

                        ClassMaster.SectionId = model.SectionId;
                        ClassMaster.StandardId = model.StandardId;
                        ClassMaster.SortingIndex = model.Index;

                        if (model.StandardId > 0)
                            standard = _ICatalogMasterService.GetStandardMasterById(model.StandardId).Standard;
                        if (model.SectionId > 0)
                            session = _ICatalogMasterService.GetSectionById(model.SectionId).Section1;
                        ClassMaster.Class = standard + " " + session;

                        var isClassNameEditEnabled = Convert.ToInt32(_ISchoolSettingService.GetAttributeValue("IsClassNameEnable"));
                        if (isClassNameEditEnabled > 0)
                            ClassMaster.Class = model.Class;

                        _ICatalogMasterService.UpdateClassMaster(ClassMaster);

                        // TimeTable log Table
                        if (ClassMastertable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassMastertable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(ClassMaster.ClassId, ClassMastertable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update ClassMaster with ClassId:{0} and Name:{1}", ClassMaster.ClassId, ClassMaster.Class), Request.Browser.Browser);
                        }
                    }
                    else
                    {
                        ClassMaster = new ClassMaster();
                        ClassMaster.SectionId = model.SectionId;
                        ClassMaster.StandardId = model.StandardId;
                        ClassMaster.SortingIndex = model.Index;

                        if (model.StandardId > 0)
                            standard = _ICatalogMasterService.GetStandardMasterById(model.StandardId).Standard;
                        if (model.SectionId > 0)
                            session = _ICatalogMasterService.GetSectionById(model.SectionId).Section1;
                        ClassMaster.Class = standard + " " + session;

                        var isClassNameEditEnabled = Convert.ToInt32(_ISchoolSettingService.GetAttributeValue("IsClassNameEnable"));
                        if (isClassNameEditEnabled > 0)
                            ClassMaster.Class = model.Class;

                        _ICatalogMasterService.InsertClassMaster(ClassMaster);

                        // TimeTable log Table
                        if (ClassMastertable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassMastertable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(ClassMaster.ClassId, ClassMastertable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassMaster with ClassId:{0} and Name:{1}", ClassMaster.ClassId, ClassMaster.Class), Request.Browser.Browser);
                        }
                        // get subject type Compulsory
                        var CompulsorySubjectType = _ISubjectService.GetAllSubjectTypes(ref count, "Compulsory").FirstOrDefault();
                        //create core subjects for new created class.
                        var ClassSubjecttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassSubject").FirstOrDefault();
                        var ClassSubjectDettable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassSubjectDetail").FirstOrDefault();
                        var coreSubjects = _ISubjectService.GetAllStandardSubjects(ref count, StandardId: model.StandardId);
                        if (coreSubjects.Count > 0)
                        {
                            foreach (var item in coreSubjects)
                            {

                                ClassSubject ClassSubject = new ClassSubject();
                                ClassSubject.ClassId = ClassMaster.ClassId;
                                ClassSubject.SubjectTypeId = CompulsorySubjectType.SubjectTypeId;
                                ClassSubject.IsActive = true;
                                ClassSubject.SubjectId = item.SubjectId;
                                ClassSubject.SkillId = null;
                                _ISubjectService.InsertClassSubject(ClassSubject);

                                // activity log ClassSubject
                                if (ClassSubjecttable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassSubjecttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(ClassSubject.ClassSubjectId, ClassSubjecttable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubject with ClassSubjectId:{0} and ClassId:{1}", ClassSubject.ClassSubjectId, ClassSubject.ClassId), Request.Browser.Browser);
                                }

                                // Class Subject Detail
                                ClassSubjectDetail ClassSubjectDetail = new ClassSubjectDetail();
                                ClassSubjectDetail.ClassSubjectID = ClassSubject.ClassSubjectId;
                                ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                // activity log ClassSubjectDetail
                                if (ClassSubjectDettable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassSubjectDettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                }
                            }
                        }
                    }
                    // TimeTable log Table
                    TempData["SuccessNotification"] = "Class saved successfully";
                    model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "Add Record");
                    model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "Edit Record");
                    model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "Delete Record");
                    model.IsAuthToView = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "View Record");
                    model = prepareClassMastermodel(model);
                    //model.EncClassId = _ICustomEncryption.base64e(Convert.ToString(Classid));
                    return View(model);
                    //return RedirectToAction("ClassMaster");
                }
                else
                {
                    model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "Add Record");
                    model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "Edit Record");
                    model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "Delete Record");
                    model.IsAuthToView = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "View Record");
                    model = prepareClassMastermodel(model);
                    if (Classid != 0)
                        model.EncClassId = _ICustomEncryption.base64e(Convert.ToString(Classid));
                    model.IsModelError = true;
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult UpdateStandardSortingIndex(string EncClassId, string SortingIndex)
        {
            try
            {
                int Classid = 0;
                if (!string.IsNullOrEmpty(EncClassId))
                {
                    // decrypt Id
                    var qsid = _ICustomEncryption.base64d(EncClassId);
                    if (int.TryParse(qsid, out Classid))
                    {
                    }
                    int? intSortingIndex = null;
                    if (!string.IsNullOrEmpty(SortingIndex))
                        intSortingIndex = Convert.ToInt32(SortingIndex);

                    var Class = _ICatalogMasterService.GetClassMasterById(Classid, true);
                    if (Class != null)
                    {
                        if (intSortingIndex > 0 && intSortingIndex!=null)
                        {
                            var IsExistIndex = _ICatalogMasterService.GetAllClassMasters(ref count).Where(f => f.ClassId != Classid && f.SortingIndex == intSortingIndex).ToList();
                            if (IsExistIndex.Count > 0)
                            {
                                return Json(new
                                {
                                    status = "Exist",
                                    msg = "Index Already Exist"
                                });
                            }
                        }
                        Class.SortingIndex = intSortingIndex;
                        _ICatalogMasterService.UpdateClassMaster(Class);
                        return Json(new
                        {
                            status = "success",
                            msg = "Class Index Updated Successfully"
                        });
                    }
                    return Json(new
                    {
                        status = "error",
                        msg = "Invalid Class"
                    });
                }
                return Json(new
                {
                    status = "error",
                    msg = "Invalid Class"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return RedirectToAction("General", "Error");
            }

        }

        //public ActionResult SelectClassMasterList(DataSourceRequest command, ClassMasterModel ClassMasterModel, bool Inactive = false, IEnumerable<Sort> sort = null)
        //{
        //    SchoolUser user = new SchoolUser();
        //    // current user
        //    if (HttpContext.Session["SchoolDB"] != null)
        //    {
        //        // re-assign session
        //        var sessiondb = HttpContext.Session["SchoolDB"];
        //        HttpContext.Session["SchoolDB"] = sessiondb;

        //        user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
        //        if (user == null)
        //            return RedirectToAction("Login", "Home");
        //        SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
        //    }
        //    else
        //    {
        //        // get action name
        //        var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
        //        return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
        //    }

        //    try
        //    {
        //        int ttlcount = 0;
        //        var classlist = _ICatalogMasterService.GetAllClassMasters(ref ttlcount, StandardId: ClassMasterModel.StandardId, SectionId: ClassMasterModel.SectionId);
        //        var Classs = classlist.Select(x =>
        //            {
        //                var ClassMasterlist = new ClassMasterModel();
        //                ClassMasterlist.IsAllSectionsBound = false;
        //                var sectionList = _ICatalogMasterService.GetAllSections(ref count);
        //                var classmasterList = _ICatalogMasterService.GetAllClassMasters(ref count);

        //                var sectionrecordpending = 0;
        //                foreach (var secl in sectionList)
        //                {
        //                    var entryexists = classmasterList.Where(m => m.SectionId == secl.SectionId && m.StandardId == x.StandardId).ToList();
        //                    if (entryexists.Count > 0)
        //                    {

        //                    }
        //                    else
        //                    {
        //                        sectionrecordpending++;
        //                    }
        //                    if (sectionrecordpending > 0)
        //                    {
        //                        ClassMasterlist.IsAllSectionsBound = true;
        //                    }

        //                }

        //                ClassMasterlist.EncClassId = _ICustomEncryption.base64e(x.ClassId.ToString());
        //                ClassMasterlist.Class = x.Class;
        //                if (x.SectionId != null && x.SectionId != 0)
        //                    ClassMasterlist.Section = _ICatalogMasterService.GetSectionById((int)x.SectionId).Section1;

        //                if (x.StandardId != null && x.StandardId != 0)
        //                    ClassMasterlist.Standard = _ICatalogMasterService.GetStandardMasterById((int)x.StandardId).Standard;

        //                var classExitsinEvents = _ISchedularService.GetAllEventDetailList(ref count, ClassId: x.ClassId).ToList();
        //                ClassMasterlist.ClassDeletionFlag = true;
        //                if (x.StudentClasses.Count > 0 || classExitsinEvents.Count > 0)
        //                    ClassMasterlist.ClassDeletionFlag = false;
        //                else if (x.ClassSubjects.Count > 0)
        //                {
        //                    var classperiodslist = x.ClassSubjects.Where(m => m.ClassPeriods.Count() > 0).Select(p => p.ClassPeriods).ToList();
        //                    if (classperiodslist.Count() > 0)
        //                        ClassMasterlist.ClassDeletionFlag = false;

        //                }

        //                var classteacherrecords = _ICatalogMasterService.GetAllVirtualClassBindings(ref count).Where(m => m.ClassId == x.ClassId).ToList();
        //                var stockpackageclasses = _IStockService.GetAllStockPackageClasses(ref count).Where(m => m.ClassId == x.ClassId).ToList();
        //                var ClassteacherRecords = _IStaffService.GetAllClassTeachers(ref count).Where(m => m.ClassId == x.ClassId).ToList();
        //                if (classteacherrecords.Count() > 0 || stockpackageclasses.Count() > 0 || ClassteacherRecords.Count() > 0)
        //                {
        //                    ClassMasterlist.ClassDeletionFlag = false;
        //                }

        //                var currentSession = _ICatalogMasterService.GetCurrentSession();

        //                ClassMasterlist.StudentCount = x.StudentClasses.Where(m => m.ClassId == x.ClassId && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault() != null && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus.ToLower() != "left" && m.SessionId == currentSession.SessionId).Count().ToString();
        //                if (x.StudentClasses.Where(m => m.ClassId == x.ClassId && m.SessionId == currentSession.SessionId).Count() > 0)
        //                {
        //                    ClassMasterlist.StudentCount += "( ";
        //                    ClassMasterlist.MaleCount = x.StudentClasses.Where(m => m.ClassId == x.ClassId && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault() != null && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus.ToLower() != "left" && m.SessionId == currentSession.SessionId && m.Student.Gender.Gender1.ToLower() == "male").Count();
        //                    ClassMasterlist.FemaleCount = x.StudentClasses.Where(m => m.ClassId == x.ClassId && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault() != null && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus.ToLower() != "left" && m.SessionId == currentSession.SessionId && m.Student.Gender.Gender1.ToLower() == "female").Count();
        //                    ClassMasterlist.OtherCount = x.StudentClasses.Where(m => m.ClassId == x.ClassId && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault() != null && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus.ToLower() != "left" && m.SessionId == currentSession.SessionId && m.Student.Gender.Gender1.ToLower() == "transgender").Count();

        //                    if (ClassMasterlist.MaleCount > 0)
        //                        ClassMasterlist.StudentCount += ("M" + '-' + ClassMasterlist.MaleCount.ToString() + ",");
        //                    if (ClassMasterlist.FemaleCount > 0)
        //                        ClassMasterlist.StudentCount += ("F" + '-' + ClassMasterlist.FemaleCount.ToString() + ",");
        //                    if (ClassMasterlist.OtherCount > 0)
        //                        ClassMasterlist.StudentCount += ("T" + '-' + ClassMasterlist.OtherCount.ToString());

        //                    ClassMasterlist.StudentCount = ClassMasterlist.StudentCount.TrimEnd(',');
        //                    ClassMasterlist.StudentCount += " )";
        //                }
        //                ClassMasterlist.Class = x.Class;
        //                ClassMasterlist.RoomNo = x.RoomNo;
        //                ClassMasterlist.StandardId = (int)x.StandardId;
        //                ClassMasterlist.SectionId = (int)x.SectionId;
        //                return ClassMasterlist;
        //            }).AsQueryable().Sort(sort);

        //        var gridModel = new DataSourceResult
        //        {
        //            Data = Classs.PagedForCommand(command).ToList(),
        //            Total = Classs.Count()
        //        };
        //        return Json(gridModel);
        //    }
        //    catch (Exception ex)
        //    {
        //        _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
        //        return Json(new { status = "failed", data = "Error Happened" });
        //    }
        //}
        public ActionResult SelectClassMasterList(DataSourceRequest command, ClassMasterModel ClassMasterModel, bool Inactive = false, IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                int ttlcount = 0;
                var classlist = _ICatalogMasterService.GetAllClassMasters_Sp(ref ttlcount, StandardId: ClassMasterModel.StandardId, SectionId: ClassMasterModel.SectionId,sessionId:ClassMasterModel.Session,SortingIndex:ClassMasterModel.Index);
                var Classs = classlist.AsQueryable().Sort(sort);

                var gridModel = new DataSourceResult
                {
                   
                    Data = Classs.PagedForCommand(command).ToList(),
                    Total = Classs.Count()
                };
                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", data = "Error Happened" });
            }
        }


        public ActionResult ViewStudentsListGird(DataSourceRequest command, string Id,int sessionId=0, bool Inactive = false,
                                                IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                var currentSession = _ICatalogMasterService.GetCurrentSession();
                if (sessionId > 0)
                {
                    currentSession = _ICatalogMasterService.GetAllSessions(ref count).Where(m=>m.SessionId==sessionId).FirstOrDefault();
                }

                var ClassId = 0;
                if (!string.IsNullOrEmpty(Id))
                {
                    int.TryParse(_ICustomEncryption.base64d(Id), out ClassId);
                }
                var StudentsClasslist = _IStudentService.GetAllStudentClasss(ref count, ClassId: ClassId, SessionId: currentSession.SessionId)
                                                        .Where(m => m.Student != null && m.Student.StudentStatusDetails != null
                                                                             && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId)
                                                        .FirstOrDefault() != null && m.Student.StudentStatusDetails
                                                        .OrderByDescending(n => n.StudentStatusDetailId)
                                                        .FirstOrDefault().StudentStatu.StudentStatus.ToLower() != "left").ToList();
                var Classs = StudentsClasslist.Select(x =>
                {
                    var studentclass = new StudentModel();
                    studentclass.FullName = x.Student.FName;
                    if (x.Student.LName != null)
                        studentclass.FullName += " " + x.Student.LName;
                    studentclass.AdmnNo = x.Student.AdmnNo;
                    studentclass.CurrentClass = x != null ? x.ClassMaster.Class : "NA";
                    studentclass.RollNo = (!string.IsNullOrEmpty(x.RollNo)) ? x.RollNo : "";

                    int? No = null;
                    if (!string.IsNullOrEmpty(x.RollNo))
                        No = Convert.ToInt32(x.RollNo);

                    studentclass.RollNoInt = No;
                    studentclass.EncStudentId = _ICustomEncryption.base64e(x.StudentId.ToString());
                    return studentclass;
                }).AsQueryable().Sort(sort);
                var gridModel = new DataSourceResult
                {
                    Data = Classs.PagedForCommand(command).ToList(),
                    Total = Classs.Count()
                };

                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", data ="Error Happened" });
            }
         
        }

        public ActionResult DeleteClassMaster(string id)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "ClassMaster", "Delete Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
            var ClassMastertable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassMaster").FirstOrDefault();
            var ClassSubjecttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassSubject").FirstOrDefault();
            var ClassSubjectdetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassSubjectDetail").FirstOrDefault();

            if (!string.IsNullOrEmpty(id))
            {
                // decrypt Id
                var qsid = _ICustomEncryption.base64d(id);
                int class_id = 0;
                if (int.TryParse(qsid, out class_id))
                {
                    var classMaster = _ICatalogMasterService.GetClassMasterById(class_id);
                    var classExitsinEvents = _ISchedularService.GetAllEventDetailList(ref count, ClassId: class_id).ToList();
                    if (classMaster.ClassTeachers.Count > 0 || classMaster.StudentClasses.Count > 0 || classExitsinEvents.Count > 0)
                    {
                        return Json(new
                        {
                            status = "failed",
                            msg = "Can't Delete Class Already in Use"
                        });
                    }
                    else
                    {
                        if (classMaster.ClassSubjects.Count > 0)
                        {
                            foreach (var item in classMaster.ClassSubjects)
                            {
                                var classSubjectDetail = _ISubjectService.GetAllClassSubjectDetails(ref count, ClassSubjectId: item.ClassSubjectId);
                                if (classSubjectDetail.Count > 0)
                                {
                                    foreach (var classSubject in classSubjectDetail)
                                    {
                                        _ISubjectService.DeleteClassSubjectDetail(ClassSubjectDetailId: classSubject.ClassSubjectDetailId);
                                        if (ClassSubjectdetailtable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassSubjectdetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(classSubject.ClassSubjectDetailId, ClassSubjectdetailtable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Delete ClassSubjectDetail with ClassSubjectDetailId:{0}", classSubject.ClassSubjectDetailId), Request.Browser.Browser);
                                        }
                                    }
                                }
                                _ISubjectService.DeleteClassSubject(item.ClassSubjectId);
                                if (ClassSubjecttable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassSubjecttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(item.ClassSubjectId, ClassSubjecttable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Delete ClassSubject with ClassSubjectId:{0}", item.ClassSubjectId), Request.Browser.Browser);
                                }
                            }
                        }
                        _ICatalogMasterService.DeleteClassMaster(class_id);
                        if (ClassMastertable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassMastertable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(classMaster.ClassId, ClassMastertable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Delete ClassMaster with ClassId:{0}", classMaster.ClassId), Request.Browser.Browser);
                        }
                    }
                }
                return Json(new
                {
                    status = "success",
                    msg = "Class Deleted Sucessfully"
                });
            }
            else
                return RedirectToAction("Page404", "Error");
        }

        #endregion

        #region Student Board RollNo

        public StudentBoardRollNoModel preapreboardrollnomodel(StudentBoardRollNoModel model)
        {
            // get standard by is board true
            var allstandards = _ICatalogMasterService.GetAllStandardMasters(ref count);
            allstandards = allstandards.Where(s => s.IsBoard == true).ToList();

            model.AvailableStandards.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            foreach (var item in allstandards)
                model.AvailableStandards.Add(new SelectListItem { Selected = false, Text = item.Standard, Value = item.StandardId.ToString() });

            return model;

        }

        public ActionResult BoardRollNo()
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "BoardRollNo", "View");
                    if (!isauth)
                        return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                var model = new StudentBoardRollNoModel();
                model.IsAuthToUpdate = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "BoardRollNo", "Update Record");
                model = preapreboardrollnomodel(model);
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public bool CheckBoardRollNoError(int StudentClassId, string BoardRollNo)
        {
            bool IsExist = false;

            if (!string.IsNullOrEmpty(BoardRollNo))
            {
                var uniqueboardrollno = _IStudentService.GetAllStudentClasss(ref count, BoardRollNo: BoardRollNo);
                uniqueboardrollno = uniqueboardrollno.Where(s => s.StudentClassId != StudentClassId).ToList();
                if (uniqueboardrollno.Count > 0)
                    IsExist = true;
            }

            return IsExist;
        }

        [HttpPost]
        public ActionResult BoardRollNo(StudentBoardRollNoModel model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Class", "BoardRollNo", "Update Record");
                    //if (!isauth)
                    //    return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }
                if (!string.IsNullOrEmpty(model.DataArray))
                {
                    string Notoficationmsg = string.Empty;
                    IList<string> UniqueBoardRollNo = new List<string>();
                    long boardrollno = 0;
                    bool InValid = false;

                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();

                    System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                    List<BoardClassStudent> DataArray = js.Deserialize<List<BoardClassStudent>>(model.DataArray);
                    if (DataArray.Count > 0)
                    {
                        foreach (var da in DataArray)
                        {
                            boardrollno = 0;
                            // check if not numeric
                            if (long.TryParse(da.BoardRollNo, out boardrollno))
                            {
                                var studentclass = _IStudentService.GetStudentClassById(da.StudentClassId, true);
                                if (studentclass != null)
                                {
                                    bool Unique = CheckBoardRollNoError(studentclass.StudentClassId, da.BoardRollNo);
                                    if (!Unique)
                                    {
                                        studentclass.BoardRollNo = da.BoardRollNo;
                                        _IStudentService.UpdateStudentClass(studentclass);

                                        var studentclasstable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StudentClass").FirstOrDefault();
                                        if (studentclasstable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, studentclasstable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(studentclass.StudentClassId, studentclasstable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update StudentClass with StudentClassId:{0} and BoardRollNo:{1}", studentclass.StudentId, studentclass.BoardRollNo), Request.Browser.Browser);
                                        }
                                    }
                                    else
                                        UniqueBoardRollNo.Add(da.BoardRollNo);
                                }
                            }
                            else
                            {
                                UniqueBoardRollNo.Add(da.BoardRollNo);
                                InValid = true;
                            }
                        }

                        if (UniqueBoardRollNo.Count != DataArray.Count)
                            Notoficationmsg = "Board Roll No Saved Sucessfully";

                        return Json(new
                        {
                            status = "success",
                            msg = Notoficationmsg,
                            data = String.Join(" , ", UniqueBoardRollNo),
                            InValid = InValid
                        });
                    }

                    return Json(new
                    {
                        status = "failed",
                        msg = "No data found"
                    });
                }

                return Json(new
                {
                    status = "failed",
                });
                //model = preapreboardrollnomodel(model);
                //return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult GetSectionsList(int SI)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                var StandardId = Convert.ToInt32(SI);

                var sectionList = _ICatalogMasterService.GetAllSections(ref count);
                // get standard classes
                var classmasters = _ICatalogMasterService.GetAllClassMasters(ref count, StandardId: StandardId);
                var SectionIds = classmasters.Select(s => s.SectionId).ToArray();

                IList<StandardSections> SectionList = new List<StandardSections>();

                // get all sections
                var allsections = _ICatalogMasterService.GetAllSections(ref count);
                allsections = allsections.Where(s => !SectionIds.Contains(s.SectionId)).ToList();

                foreach (var section in allsections)
                {
                    StandardSections Sections = new StandardSections();
                    Sections.SectionId = section.SectionId;
                    Sections.SectionName = section.Section1;

                    SectionList.Add(Sections);
                }

                return Json(new
                {
                    status = "success",
                    data = SectionList
                });

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    msg = ex.Message
                });
            }
        }

        public ActionResult GetStandardSections(string Standard)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                var StandardId = Convert.ToInt32(Standard);
                // get standard classes
                var standardclasses = _ICatalogMasterService.GetAllClassMasters(ref count, StandardId: StandardId);
                var SectionIds = standardclasses.Select(s => s.SectionId).ToArray();

                IList<StandardSections> StandardSectionList = new List<StandardSections>();

                // get all sections
                var allsections = _ICatalogMasterService.GetAllSections(ref count);
                allsections = allsections.Where(s => SectionIds.Contains(s.SectionId)).ToList();

                foreach (var section in allsections)
                {
                    StandardSections StandardSections = new StandardSections();
                    StandardSections.SectionId = section.SectionId;
                    StandardSections.SectionName = section.Section1;

                    StandardSectionList.Add(StandardSections);
                }

                return Json(new
                {
                    status = "success",
                    data = StandardSectionList
                });

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    msg = ex.Message
                });
            }
        }

        public ActionResult GetStudentListByStandard(string Standard, string Sections)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session+
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                var StandardId = Convert.ToInt32(Standard);
                int[] Sectionids = new int[] { };
                // sections
                if (!string.IsNullOrEmpty(Sections))
                {
                    var sectionarray = Sections.Split(',');
                    Sectionids = sectionarray.Select(s => Convert.ToInt32(s)).ToArray();
                }

                // get standard classes
                var standardclasses = _ICatalogMasterService.GetAllClassMasters(ref count, StandardId: StandardId);
                if (Sectionids.Count() > 0)
                    standardclasses = standardclasses.Where(s => Sectionids.Contains((int)s.SectionId)).ToList();

                // classids
                var classids = standardclasses.Select(s => s.ClassId).ToArray();

                IList<BoardClassStudent> BoardClassStudentList = new List<BoardClassStudent>();

                // get all Student
                // current session
                var session = _ICatalogMasterService.GetCurrentSession();
                var allstudents = _IStudentService.GetAllStudentClasss(ref count);
                allstudents = allstudents.Where(s => s.RollNo != null && s.RollNo != "" && classids.Contains((int)s.ClassId) && s.SessionId == session.SessionId).ToList();
                allstudents = allstudents.OrderBy(s => s.ClassMaster.StandardMaster.StandardIndex).ThenBy(s => s.ClassId).ThenBy(s => Convert.ToInt32(s.RollNo)).ToList();

                foreach (var student in allstudents)
                {
                    // get student status detail
                    var studentstatusdetail = student.Student.StudentStatusDetails.OrderByDescending(s => s.StudentStatusDetailId).FirstOrDefault();
                    if (studentstatusdetail != null && studentstatusdetail.StudentStatu.StudentStatus != "Left")
                    {
                        BoardClassStudent BoardClassStudent = new BoardClassStudent();
                        BoardClassStudent.StudentClassId = student.StudentClassId;
                        BoardClassStudent.StudentName = student.Student.FName + " " + student.Student.LName;
                        BoardClassStudent.ClassName = student.ClassMaster.Class;
                        BoardClassStudent.BoardRollNo = student.BoardRollNo != null ? student.BoardRollNo : "";
                        BoardClassStudent.RollNo = student.RollNo;
                        BoardClassStudent.StudentStatus = studentstatusdetail.StudentStatu.StudentStatus;

                        BoardClassStudentList.Add(BoardClassStudent);
                    }
                }

                return Json(new
                {
                    status = "success",
                    data = BoardClassStudentList
                });

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    msg = ex.Message
                });
            }
        }

        #endregion

        #region Class Test

        public ActionResult ClassTest()
        {
            return View();
        }

        #endregion

        #region virtualclass
        public ActionResult VirtualClass()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);

            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                var model = new VirtualClassModel();
                var allvirtualclassses = _ICatalogMasterService.GetAllVirtualClasses(ref count).ToList();

                if (allvirtualclassses.Count > 0)
                {
                    model.VirtualClassList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
                    foreach (var item in allvirtualclassses)
                        model.VirtualClassList.Add(new SelectListItem { Selected = false, Value = item.VirtualClassId.ToString(), Text = item.VirtualClass1 });
                }

                model.SessionList = _IDropDownService.SessionList();
                var currentsession = _ICatalogMasterService.GetCurrentSession();
                model.SessionId = currentsession.SessionId.ToString();
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult GetAllClassesByVirtual(int SessionId, int VirtualClassId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            if (SessionId > 0 && VirtualClassId > 0)
            {
                var classlist = new List<SelectListItem>();
                var reqdata = new List<VirtualClassDataModel>();
                int?[] ClassIds = { };
                var virtualclass = _ICatalogMasterService.GetAllVirtualClasses(ref count).Where(p => p.VirtualClassId == VirtualClassId).FirstOrDefault();
                if (virtualclass != null)
                {

                    var allstandards = _ICatalogMasterService.GetAllStandardMasters(ref count);
                    var virtualindex = allstandards.Where(p => p.StandardId == virtualclass.StandardId).FirstOrDefault().VirtualIndex;
                    var allreqstandardids = allstandards.Where(p => p.VirtualIndex == virtualindex).Select(p => p.StandardId).ToList();

                    var allreqclasses = _ICatalogMasterService.GetAllClassMasters(ref count).Where(p => allreqstandardids.Contains((int)p.StandardId)).Distinct().ToList();

                    if (allreqclasses.Count > 0)
                    {
                        foreach (var item in allreqclasses)
                            classlist.Add(new SelectListItem { Selected = false, Value = item.ClassId.ToString(), Text = item.Class });
                    }

                    ClassIds = _ICatalogMasterService.GetAllVirtualClassBindings(ref count).Where(p => p.VirtualClassId == VirtualClassId && p.IsActive == true).Select(p => p.ClassId).ToArray();

                    var allstudentclasses = _IStudentService.GetAllStudentClasss(ref count).ToList();
                    var reqstudentclassses = allstudentclasses.Where(p => ClassIds.Contains((int)p.ClassId) && p.SessionId == SessionId).ToList();
                    var virtualclassession = _ICatalogMasterService.GetAllVirtualClassSessions(ref count).Where(p => p.SessionId == SessionId && p.VirtualClassId == VirtualClassId).FirstOrDefault();

                    int?[] allstudentsessionids = { };

                    if (virtualclassession != null)
                        allstudentsessionids = _ICatalogMasterService.GetAllVirtualClassStudents(ref count).Where(p => p.VirtualClassSessionId == virtualclassession.VirtualClassSessionId).Select(p => p.StudentClassId).ToArray();

                    var srno = 0;
                    if (reqstudentclassses.Count > 0)
                    {
                        foreach (var item in reqstudentclassses)
                        {
                            var data = new VirtualClassDataModel();
                            srno = srno + 1;
                            data.SrNo = srno;
                            data.Class = item.ClassMaster.Class;
                            data.StudentName = item.Student.FName + " " + item.Student.LName;
                            data.StudentClassId = item.StudentClassId.ToString();
                            data.AdmnNo = item.Student.AdmnNo;
                            data.RollNo = item.RollNo;
                            if (allstudentsessionids.Contains(item.StudentClassId))
                                data.IsSaved = true;

                            reqdata.Add(data);
                        }
                    }
                }
                return Json(new
                {
                    status = "success",
                    data = classlist,
                    reqdata = reqdata,
                    ClassIds = ClassIds
                });
            }
            else
            {
                return Json(new
                {
                    status = "failed",
                    data = "Data Unsufficient"
                });
            }

        }

        public ActionResult SaveVirtualClassBindings(int SessionId, int VirtualClassId, int[] ClassIds)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            if (SessionId > 0 && VirtualClassId > 0 && ClassIds.Length > 0)
            {
                var classlist = new List<SelectListItem>();
                var reqdata = new List<VirtualClassDataModel>();

                var virtualclass = _ICatalogMasterService.GetAllVirtualClasses(ref count).Where(p => p.VirtualClassId == VirtualClassId).FirstOrDefault();
                if (virtualclass != null)
                {

                    var allstandards = _ICatalogMasterService.GetAllStandardMasters(ref count);
                    var virtualindex = allstandards.Where(p => p.StandardId == virtualclass.StandardId).FirstOrDefault().VirtualIndex;
                    var allreqstandardids = allstandards.Where(p => p.VirtualIndex == virtualindex && p.VirtualIndex != null).Select(p => p.StandardId).ToList();

                    var allreqclasses = _ICatalogMasterService.GetAllClassMasters(ref count).Where(p => allreqstandardids.Contains((int)p.StandardId)).Distinct().ToList();
                    var allvirtualclassbindings = _ICatalogMasterService.GetAllVirtualClassBindings(ref count).ToList();

                    if (allreqclasses.Count > 0)
                    {
                        foreach (var item in allreqclasses)
                        {
                            var Isalreadyexist = allvirtualclassbindings.Where(p => p.VirtualClassId == VirtualClassId && p.ClassId == item.ClassId).FirstOrDefault();
                            if (Isalreadyexist != null)
                            {
                                Isalreadyexist.ClassId = item.ClassId;
                                Isalreadyexist.VirtualClassId = VirtualClassId;
                                if (ClassIds.Contains(item.ClassId))
                                {

                                    Isalreadyexist.IsActive = true;
                                    _ICatalogMasterService.UpdateVirtualClassBinding(Isalreadyexist);
                                }
                                else
                                {
                                    Isalreadyexist.IsActive = false;
                                    _ICatalogMasterService.DeleteVirtualClassBinding(Isalreadyexist.VirtualClassBindingId);
                                }

                            }
                            else
                            {
                                var data = new VirtualClassBinding();
                                data.ClassId = item.ClassId;
                                data.VirtualClassId = VirtualClassId;
                                if (ClassIds.Contains(item.ClassId))
                                {
                                    data.IsActive = true;
                                    _ICatalogMasterService.InsertVirtualClassBindings(data);

                                }


                            }
                        }

                        var allstudentclasseids = _IStudentService.GetAllStudentClasss(ref count).Where(p => ClassIds.Contains((int)p.ClassId) && p.SessionId == SessionId).Select(p => p.StudentClassId).ToList();
                        //foreach (var item in collection)
                        //{

                        //}
                        var virtualclassession = _ICatalogMasterService.GetAllVirtualClassSessions(ref count).Where(p => p.SessionId == SessionId && p.VirtualClassId == VirtualClassId).FirstOrDefault();

                        var allvirtualclassstudents = new List<VirtualClassStudentId>();
                        if (virtualclassession != null)
                        {
                            allvirtualclassstudents = _ICatalogMasterService.GetAllVirtualClassStudents(ref count).Where(p => p.VirtualClassSessionId == virtualclassession.VirtualClassSessionId).ToList();
                        }
                        else
                        {
                            var VSC = new VirtualClassSession();
                            VSC.SessionId = SessionId;
                            VSC.VirtualClassId = VirtualClassId;
                            _ICatalogMasterService.InsertVirtualClassSession(VSC);

                            virtualclassession = _ICatalogMasterService.GetAllVirtualClassSessions(ref count).Where(p => p.SessionId == SessionId && p.VirtualClassId == VirtualClassId).FirstOrDefault();
                            allvirtualclassstudents = _ICatalogMasterService.GetAllVirtualClassStudents(ref count).Where(p => p.VirtualClassSessionId == virtualclassession.VirtualClassSessionId).ToList();
                        }
                        if (allvirtualclassstudents.Count > 0)
                        {
                            foreach (var item in allvirtualclassstudents)
                            {
                                if (!allstudentclasseids.Contains((int)item.StudentClassId))
                                    _ICatalogMasterService.DeleteVirtualClassStudents(VirtualClassStudentId: (int)item.VirtualClassStudentId1);
                            }
                        }


                    }

                    var allstudentclasses = _IStudentService.GetAllStudentClasss(ref count).ToList();
                    var reqstudentclassses = allstudentclasses.Where(p => ClassIds.Contains((int)p.ClassId) && p.SessionId == SessionId).ToList();


                    var virtualclassessions = _ICatalogMasterService.GetAllVirtualClassSessions(ref count).Where(p => p.SessionId == SessionId && p.VirtualClassId == VirtualClassId).FirstOrDefault();

                    int?[] allstudentsessionids = { };

                    if (virtualclassessions != null)
                        allstudentsessionids = _ICatalogMasterService.GetAllVirtualClassStudents(ref count).Where(p => p.VirtualClassSessionId == virtualclassessions.VirtualClassSessionId).Select(p => p.StudentClassId).ToArray();
                    var srno = 0;
                    if (reqstudentclassses.Count > 0)
                    {
                        foreach (var item in reqstudentclassses)
                        {
                            var data = new VirtualClassDataModel();
                            srno = srno + 1;
                            data.SrNo = srno;
                            data.Class = item.ClassMaster.Class;
                            data.StudentName = item.Student.FName + " " + item.Student.LName;
                            data.StudentClassId = item.StudentClassId.ToString();
                            data.AdmnNo = item.Student.AdmnNo;
                            data.RollNo = item.RollNo;
                            if (allstudentsessionids.Contains(item.StudentClassId))
                                data.IsSaved = true;
                            reqdata.Add(data);
                        }
                    }

                }
                return Json(new
                {
                    status = "success",
                    data = "Virtual Class bound to Classes successfully",
                    reqdata = reqdata
                });
            }
            else
            {
                return Json(new
                {
                    status = "failed",
                    data = "Data Unsufficient"
                });
            }

        }

        public ActionResult SaveVirtualClassStudent(int SessionId, int VirtualClassId, int[] ClassStudentClassIds)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            var IsVirtualsessionexist = _ICatalogMasterService.GetAllVirtualClassSessions(ref count).Where(p => p.SessionId == SessionId && p.VirtualClassId == VirtualClassId).FirstOrDefault();
            if (SessionId > 0 && VirtualClassId > 0 && ClassStudentClassIds != null)
            {
                var virclasssession = new VirtualClassSession();
                if (IsVirtualsessionexist == null)
                {
                    virclasssession.VirtualClassId = VirtualClassId;
                    virclasssession.SessionId = SessionId;

                    _ICatalogMasterService.InsertVirtualClassSession(virclasssession);
                }

                int?[] allstudentclassstudentIds = { };

                var allstudentclassstudent = _ICatalogMasterService.GetAllVirtualClassStudents(ref count).ToList();
                if (IsVirtualsessionexist == null)
                    allstudentclassstudentIds = allstudentclassstudent.Where(p => p.VirtualClassSessionId == virclasssession.VirtualClassSessionId).Select(p => p.StudentClassId).ToArray();
                else
                    allstudentclassstudentIds = allstudentclassstudent.Where(p => p.VirtualClassSessionId == IsVirtualsessionexist.VirtualClassSessionId).Select(p => p.StudentClassId).ToArray();

                foreach (var item in ClassStudentClassIds)
                {
                    if (allstudentclassstudentIds.Contains(item))
                        continue;
                    else if (!allstudentclassstudentIds.Contains(item))
                    {

                        var virclassstudent = new VirtualClassStudentId();
                        if (IsVirtualsessionexist == null)
                            virclassstudent.VirtualClassSessionId = virclasssession.VirtualClassSessionId;
                        else
                            virclassstudent.VirtualClassSessionId = IsVirtualsessionexist.VirtualClassSessionId;

                        virclassstudent.StudentClassId = item;

                        _ICatalogMasterService.InsertVirtualClassStudents(virclassstudent);
                    }

                }

                var studentclassafterupdation = new List<VirtualClassStudentId>();

                if (IsVirtualsessionexist == null)
                    studentclassafterupdation = _ICatalogMasterService.GetAllVirtualClassStudents(ref count).ToList().Where(p => p.VirtualClassSessionId == virclasssession.VirtualClassSessionId).ToList();
                else
                    studentclassafterupdation = _ICatalogMasterService.GetAllVirtualClassStudents(ref count).ToList().Where(p => p.VirtualClassSessionId == IsVirtualsessionexist.VirtualClassSessionId).ToList();

                foreach (var item in studentclassafterupdation)
                {
                    if (!ClassStudentClassIds.Contains((int)item.StudentClassId))
                        _ICatalogMasterService.DeleteVirtualClassStudents(VirtualClassStudentId: item.VirtualClassStudentId1);
                }

                return Json(new
                {
                    status = "success",
                    data = "Virtual Class Student saved successfully"
                });
            }
            else
            {
                var virtualclassbinding = _ICatalogMasterService.GetAllVirtualClassSessions(ref count).Where(m => m.SessionId == SessionId && m.VirtualClassId == VirtualClassId).FirstOrDefault();
                if (virtualclassbinding != null)
                {

                    var virtualclassstudents = _ICatalogMasterService.GetAllVirtualClassStudents(ref count).Where(m => m.VirtualClassSessionId == virtualclassbinding.VirtualClassSessionId).ToList();
                    foreach (var item in virtualclassstudents)
                    {
                        _ICatalogMasterService.DeleteVirtualClassStudents(item.VirtualClassStudentId1);
                    }
                    _ICatalogMasterService.DeleteVirtualClassSession(virtualclassbinding.VirtualClassSessionId);

                }
                var virtualclassbindingsrecords = _ICatalogMasterService.GetAllVirtualClassBindings(ref count).Where(m => m.VirtualClassId == VirtualClassId).ToList();
                foreach (var item in virtualclassbindingsrecords)
                {
                    _ICatalogMasterService.DeleteVirtualClassBinding(item.VirtualClassBindingId);
                }
                return Json(new
                {
                    status = "success",
                    data = "Virtual Class Student removed successfully"
                });
            }
            //else
            //{
            //    return Json(new
            //    {
            //        status = "failed",
            //        data = "Data Unsufficient"
            //    });
            //}

        }
        #endregion

        #endregion

        #region Export methods



        public ActionResult Export()
        {
            return Json(new
            {
                status = "success",
                data = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportData(string StandardId, string SectionId, string IsExcel)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                string Subtitle = "";
                string Title = "Classes";

                DataTable dt = new DataTable();
                dt = GetAllKendoGridInformation(StandardId, SectionId);
                if (dt.Rows.Count > 0)
                {
                    if (IsExcel == "1")
                    {
                        _IExportHelper.ExportToExcel(dt, Title: Title, Subtitle: Subtitle);
                    }
                    else if (IsExcel == "2")
                    {
                        _IExportHelper.ExportToPrint(dt, Title: Title, Subtitle: Subtitle);
                    }
                    else
                    {
                        _IExportHelper.ExportToPDF(dt, Title: Title, Subtitle: Subtitle);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public DataTable GetAllKendoGridInformation(string StandardId, string SectionId)
        {
            DataTable dt = new DataTable();
            try
            {

                int? standid = null;
                int? secid = null;
                if (SectionId != null && SectionId != "")
                    secid = Convert.ToInt32(SectionId);

                if (StandardId != null && StandardId != "")
                    standid = Convert.ToInt32(StandardId);

                var classlist = _ICatalogMasterService.GetAllClassMasters(ref count, StandardId: standid, SectionId: secid);

                // Add columns in your DataTable
                dt.Columns.Add("Class");
                dt.Columns.Add("Standard");
                dt.Columns.Add("Section");
                dt.Columns.Add("StudentCount");

                DataRow row;
                if (classlist.Count() > 0)
                {
                    var currentSession = _ICatalogMasterService.GetCurrentSession();
                    foreach (var data in classlist)
                    {
                        row = dt.NewRow();
                        row[0] = data.Class == null ? "" : data.Class.ToString();
                        row[1] = data.StandardMaster == null ? "" : data.StandardMaster.Standard.ToString();
                        row[2] = data.Section == null ? "" : data.Section.Section1.ToString();


                        var countdata = data.StudentClasses.Where(m => m.ClassId == data.ClassId && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault() != null && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus.ToLower() != "left" && m.SessionId == currentSession.SessionId).Count().ToString() + " (";
                        if (data.StudentClasses.Where(m => m.ClassId == data.ClassId && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault() != null && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus.ToLower() != "left" && m.SessionId == currentSession.SessionId).Count() == 0)
                        {
                            row[3] = 0;
                        }
                        if (data.StudentClasses.Where(m => m.ClassId == data.ClassId && m.SessionId == currentSession.SessionId).Count() > 0)
                        {
                            var MaleCount = data.StudentClasses.Where(m => m.ClassId == data.ClassId && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault() != null && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus.ToLower() != "left" && m.SessionId == currentSession.SessionId && m.Student.Gender.Gender1.ToLower() == "male").Count();
                            var FemaleCount = data.StudentClasses.Where(m => m.ClassId == data.ClassId && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault() != null && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus.ToLower() != "left" && m.SessionId == currentSession.SessionId && m.Student.Gender.Gender1.ToLower() == "female").Count();
                            var OtherCount = data.StudentClasses.Where(m => m.ClassId == data.ClassId && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault() != null && m.Student.StudentStatusDetails.OrderByDescending(n => n.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus.ToLower() != "left" && m.SessionId == currentSession.SessionId && m.Student.Gender.Gender1.ToLower() == "transgender").Count();

                            if (MaleCount > 0)
                                countdata += ("M" + '-' + MaleCount.ToString() + ",");
                            if (FemaleCount > 0)
                                countdata += ("F" + '-' + FemaleCount.ToString() + ",");
                            if (OtherCount > 0)
                                countdata += ("T" + '-' + OtherCount.ToString());

                            row[3] = countdata.TrimEnd(',');
                            row[3] += " )";
                        }

                        dt.Rows.Add(row);
                        dt.AcceptChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
            }
            return dt;
        }

        #endregion
    }
}