﻿using BAL.BAL.Common;
using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.GuardianModule;
using DAL.DAL.Kendo;
using DAL.DAL.LibraryModule;
using DAL.DAL.SettingService;
using DAL.DAL.StaffModule;
using DAL.DAL.StudentModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.Controllers
{
    public class UserLoginController : Controller
    {
        // GET: UserLogin

        #region fields

        public int count = 0;
        public static string BookID = "";
        public static string BookTitle = "";
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ILogService _Logger;
        private readonly IWebHelper _WebHelper;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly IUserManagementService _IUserManagementService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly IStudentService _IStudentService;
        private readonly IStaffService _IStaffService;
        private readonly IGuardianService _IGuardianService;

        #endregion

        #region ctor

        public UserLoginController(
            IConnectionService IConnectionService,
            IDropDownService IDropDownService, IUserManagementService IUserManagementService,
            IActivityLogMasterService IActivityLogMasterService, IStaffService IStaffService,
            IActivityLogService IActivityLogService, IAddressMasterService IAddressMasterService,
            ILogService Logger, IUserService IUserService, IGuardianService IGuardianService,
            IWebHelper WebHelper, ICatalogMasterService ICatalogMasterService,
            ISchoolSettingService ISchoolSettingService, IStudentService IStudentService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption)
        {
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._Logger = Logger;
            this._WebHelper = WebHelper;
            this._ISchoolSettingService = ISchoolSettingService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IUserManagementService = IUserManagementService;
            this._IAddressMasterService = IAddressMasterService;
            this._IStudentService = IStudentService;
            this._IStaffService = IStaffService;
            this._IGuardianService = IGuardianService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        #endregion

        #region Methods

        #region UserLoginCheck

        public UserLoginCheckViewModel PrepareUserLoginCheckModel(UserLoginCheckViewModel model)
        {
            //paging
            model.gridPageSizes = _ISchoolSettingService.GetorSetSchoolData("gridPageSizes", "20,50,100,250").AttributeValue;
            model.defaultGridPageSize = _ISchoolSettingService.GetorSetSchoolData("defaultGridPageSize", "20").AttributeValue;

            //get Public IP Address
            string Publicip = new WebClient().DownloadString("http://icanhazip.com").TrimEnd('\n');
            model.IP = _ISchoolSettingService.GetorSetSchoolData("UserLoginCheckIP", Publicip).AttributeValue;

            // UserTypes List
            var getContacttypes = _IAddressMasterService.GetAllContactTypes(ref count);
            model.AaviableUserTypes.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            foreach (var contact in getContacttypes)
            {
                if (contact.ContactType1.ToLower() != "admin")
                {
                    model.AaviableUserTypes.Add(new SelectListItem
                    {
                        Text = contact.ContactType1,
                        Value = _ICustomEncryption.base64e(contact.ContactTypeId.ToString()),
                        Selected = false
                    });
                }
            }

            return model;
        }

        public ActionResult ManageUserLoginCheck()
        {
            SchoolUser user = new SchoolUser();
            var model = new UserLoginCheckViewModel();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                model = PrepareUserLoginCheckModel(model);


                //authentication
                //model.IsAuthToGirdAdd = _IUserAuthorizationService.IsUserAuthorize(user, "BookMaster Attributes", "Add Record");
                //model.IsAuthToGridDelete = _IUserAuthorizationService.IsUserAuthorize(user, "BookMaster Attributes", "Delete Record");
                //model.IsAuthToGridEdit = _IUserAuthorizationService.IsUserAuthorize(user, "BookMaster Attributes", "Edit Record");

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return View(model);
            }
        }

        public ActionResult GetUserLoginChecks(DataSourceRequest command, UserLoginCheckViewModel model
                                                , IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values

                int TypeIdK = 0;
                int? encTypeId = null;
                if (!string.IsNullOrEmpty(model.UserTypeId))
                {
                    int.TryParse(_ICustomEncryption.base64d(model.UserTypeId), out TypeIdK);
                    encTypeId = TypeIdK;
                }

                // decrypt values
                string UserIdK = "";
                int UserId = 0;
                if (model.UserIds != null)
                {
                    foreach (var id in model.UserIds)
                    {
                        int.TryParse(_ICustomEncryption.base64d(id), out UserId);
                        UserIdK += Convert.ToString(UserId) + ",";
                    }
                    UserIdK = UserIdK.Trim(',');
                }

                var UserLoginchecksList = _IUserService.GetAllUserLoginCheckBySPs(ref count, UserTypeId: encTypeId, UserIds: UserIdK);
                var gridModel = new DataSourceResult
                {
                    Data = UserLoginchecksList.PagedForCommand(command).Select(p =>
                    {
                        var LoginChecklist = new UserLoginCheckViewModel();
                        LoginChecklist.UserLoginCheckId = _ICustomEncryption.base64e(p.UserLoginCheckId);
                        LoginChecklist.UserId = _ICustomEncryption.base64e(p.UserId);
                        LoginChecklist.UserContactId = _ICustomEncryption.base64e(p.UserContactId);
                        LoginChecklist.UserTypeId = _ICustomEncryption.base64e(p.UserTypeId);
                        LoginChecklist.UserTypeValue = p.UserTypeValue;

                        switch (p.UserTypeValue.ToLower())
                        {
                            case "teacher":
                                var getTeacher = _IStaffService.GetStaffById(StaffId: Convert.ToInt32(p.UserContactId));
                                if (getTeacher != null)
                                {
                                    if ((bool)getTeacher.StaffType.IsTeacher)
                                    {
                                        LoginChecklist.FullName = getTeacher.FName + " " + getTeacher.LName;
                                    }
                                }
                                break;

                            case "student":
                                var getStudent = _IStudentService.GetStudentById(StudentId: Convert.ToInt32(p.UserContactId));
                                if (getStudent != null)
                                {
                                    LoginChecklist.FullName = getStudent.FName + " " + getStudent.LName;
                                }
                                break;


                            case "non-teaching":
                                var getNonTeacher = _IStaffService.GetStaffById(StaffId: Convert.ToInt32(p.UserContactId));
                                if (getNonTeacher != null)
                                {
                                    if (getNonTeacher.StaffType.StaffType1.ToLower() == "non-teaching")
                                    {
                                        LoginChecklist.FullName = getNonTeacher.FName + " " + getNonTeacher.LName;
                                    }
                                }
                                break;
                            case "guardian":
                                var getguardian = _IGuardianService.GetGuardianById(GuardianId: Convert.ToInt32(p.UserContactId));
                                if (getguardian != null)
                                {
                                    LoginChecklist.FullName = getguardian.FirstName + " " + getguardian.LastName;
                                }
                                break;

                        }

                        LoginChecklist.IP = p.IP == null ? "" : p.IP;

                        LoginChecklist.Datefrom = p.Datefrom == null || p.Datefrom == "" ? ""
                                                            : Convert.ToDateTime(p.Datefrom).Date.ToString("dd/MM/yyyy");
                        LoginChecklist.DateTill = p.DateTill == null || p.DateTill == "" ? ""
                                                             : Convert.ToDateTime(p.DateTill).Date.ToString("dd/MM/yyyy");
                        LoginChecklist.TimeFrom = p.TimeFrom == "" || p.TimeFrom == null ? ""
                                                    : Convert.ToDateTime(p.TimeFrom).TimeOfDay.ToString();
                        LoginChecklist.TimeTill = p.TimeTill == "" || p.TimeTill == null ? ""
                                                    : Convert.ToDateTime(p.TimeTill).TimeOfDay.ToString();

                        LoginChecklist.IsAuthToDelete = false;

                        return LoginChecklist;
                    }).AsQueryable().Sort(sort).ToList(),
                    Total = UserLoginchecksList.Count()
                };

                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public string CheckUserLoginCheckError(UserLoginCheckViewModel model)
        {
            StringBuilder errormsg = new StringBuilder();

            if (string.IsNullOrEmpty(model.UserTypeId) || string.IsNullOrWhiteSpace(model.UserTypeId))
                errormsg.Append("User Type required ε");

            if (model.UserIds.ElementAt(0).ToString() == "")
                errormsg.Append("User required ε");

            return errormsg.ToString();
        }

        public ActionResult FillUsers(string TypeId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                UserLoginCheckViewModel model = new UserLoginCheckViewModel();
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int TypeIdK = 0;
                if (!string.IsNullOrEmpty(TypeId))
                    int.TryParse(_ICustomEncryption.base64d(TypeId), out TypeIdK);

                List<SelectListItem> paramsList = new List<SelectListItem>();

                var gettype = _IAddressMasterService.GetContactTypeById(ContactTypeId: TypeIdK);
                if (gettype != null)
                {
                    switch (gettype.ContactType1.ToLower())
                    {
                        case "student":
                            var getSchoolUser = _IUserService.GetAllUsers(ref count, UserTypeId: gettype.ContactTypeId);
                            foreach (var schoolUser in getSchoolUser)
                            {
                                var getStudent = _IStudentService.GetStudentById(StudentId: (int)schoolUser.UserContactId);
                                if (getStudent != null)
                                {
                                    paramsList.Add(new SelectListItem
                                    {
                                        Text = (getStudent.FName + " " + getStudent.LName).Trim(),
                                        Value = _ICustomEncryption.base64e(schoolUser.UserId.ToString()),
                                        Selected = false
                                    });
                                }
                            }
                            break;

                        case "teacher":
                            var getteacherUser = _IUserService.GetAllUsers(ref count, UserTypeId: gettype.ContactTypeId);
                            foreach (var teacherUser in getteacherUser)
                            {
                                var getTeacher = _IStaffService.GetStaffById(StaffId: (int)teacherUser.UserContactId);
                                if (getTeacher != null)
                                {
                                    if ((bool)getTeacher.StaffType.IsTeacher)
                                    {
                                        paramsList.Add(new SelectListItem
                                        {
                                            Text = (getTeacher.FName + " " + getTeacher.LName).Trim(),
                                            Value = _ICustomEncryption.base64e(teacherUser.UserId.ToString()),
                                            Selected = false
                                        });
                                    }
                                }
                            }
                            break;
                        case "non-teaching":
                            var getNonTeachUser = _IUserService.GetAllUsers(ref count, UserTypeId: gettype.ContactTypeId);
                            foreach (var nonTeachUser in getNonTeachUser)
                            {
                                var getNonTeacher = _IStaffService.GetStaffById(StaffId: (int)nonTeachUser.UserContactId);
                                if (getNonTeacher != null)
                                {
                                    if (getNonTeacher.StaffType.StaffType1.ToLower() == "non-teaching")
                                    {
                                        paramsList.Add(new SelectListItem
                                        {
                                            Text = (getNonTeacher.FName + " " + getNonTeacher.LName).Trim(),
                                            Value = _ICustomEncryption.base64e(nonTeachUser.UserId.ToString()),
                                            Selected = false
                                        });
                                    }
                                }
                            }
                            break;
                        case "guardian":
                            var getguardUser = _IUserService.GetAllUsers(ref count, UserTypeId: gettype.ContactTypeId);
                            foreach (var guardUser in getguardUser)
                            {
                                var getguardian = _IGuardianService.GetGuardianById(GuardianId: (int)guardUser.UserContactId);
                                if (getguardian != null)
                                {
                                    paramsList.Add(new SelectListItem
                                    {
                                        Text = (getguardian.FirstName + " " + getguardian.LastName).Trim(),
                                        Value = _ICustomEncryption.base64e(guardUser.UserId.ToString()),
                                        Selected = false
                                    });
                                }
                            }
                            break;
                    }
                }
                model.AaviableUsers = paramsList;
                return Json(new { paramsList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult InsertUpdateUserLoginCheck(UserLoginCheckViewModel model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                string errormsg = CheckUserLoginCheckError(model);
                if (string.IsNullOrEmpty(errormsg))
                {
                    // decrypt values
                    int UserLoginCheckIdK = 0;
                    if (!string.IsNullOrEmpty(model.UserLoginCheckId))
                        int.TryParse(_ICustomEncryption.base64d(model.UserLoginCheckId), out UserLoginCheckIdK);

                    //int UserIdK = 0;
                    //if (!string.IsNullOrEmpty(model.UserId))
                    //    int.TryParse(_ICustomEncryption.base64d(model.UserId), out UserIdK);

                    int UserTypeIdK = 0;
                    if (!string.IsNullOrEmpty(model.UserTypeId))
                        int.TryParse(_ICustomEncryption.base64d(model.UserTypeId), out UserTypeIdK);

                    DateTime? Datefrom = null, DateTill = null;
                    if (!string.IsNullOrEmpty(model.Datefrom) || !string.IsNullOrWhiteSpace(model.Datefrom))
                    {
                        Datefrom = DateTime.ParseExact(model.Datefrom, "dd/MM/yyyy", null);
                        Datefrom = Datefrom.Value.Date;
                    }
                    if (!string.IsNullOrEmpty(model.DateTill) || !string.IsNullOrWhiteSpace(model.DateTill))
                    {
                        DateTill = DateTime.ParseExact(model.DateTill, "dd/MM/yyyy", null);
                        DateTill = DateTill.Value.Date;
                    }

                    bool editmode = false;
                    KSModel.Models.UserLoginCheck LoginCheckData = new KSModel.Models.UserLoginCheck();
                    foreach (var id in model.UserIds)
                    {
                        LoginCheckData = new KSModel.Models.UserLoginCheck();
                        int UserIdK = 0;
                        if (!string.IsNullOrEmpty(id))
                            int.TryParse(_ICustomEncryption.base64d(id), out UserIdK);

                        TimeSpan? encnullTimeFrom = null, encnullTimeTill = null;
                        if (Convert.ToDateTime(model.TimeFrom).TimeOfDay != TimeSpan.Zero)
                        {
                            encnullTimeFrom = Convert.ToDateTime(model.TimeFrom).TimeOfDay;
                        }
                        if (Convert.ToDateTime(model.TimeTill).TimeOfDay != TimeSpan.Zero)
                        {
                            encnullTimeTill = Convert.ToDateTime(model.TimeTill).TimeOfDay;
                        }

                        var IsUserIdExists = _IUserService.GetAllUserLoginChecks(ref count, UserId: UserIdK);
                        if (IsUserIdExists.Count > 0)
                        {
                            //update 
                            LoginCheckData = _IUserService.GetUserLoginCheckById(IsUserIdExists.FirstOrDefault().UserLoginCheckId, true);
                            if (LoginCheckData != null)
                            {
                                LoginCheckData.IP = model.IP;
                                LoginCheckData.Datefrom = Datefrom;
                                LoginCheckData.DateTill = DateTill;
                                LoginCheckData.TimeFrom = encnullTimeFrom;
                                LoginCheckData.TimeTill = encnullTimeTill;
                                LoginCheckData.UserId = UserIdK;
                            }
                            _IUserService.UpdateUserLoginCheck(LoginCheckData);

                            editmode = true;
                        }
                        else
                        {
                            // Insert new 
                            LoginCheckData.IP = model.IP;
                            LoginCheckData.Datefrom = Datefrom;
                            LoginCheckData.DateTill = DateTill;
                            LoginCheckData.TimeFrom = encnullTimeFrom;
                            LoginCheckData.TimeTill = encnullTimeTill;
                            LoginCheckData.UserId = UserIdK;

                            _IUserService.InsertUserLoginCheck(LoginCheckData);

                            editmode = false;
                        }
                    }
                    if (editmode)
                        message = "Updated Successfully";
                    else
                        message = "Inserted Successfully";
                }
                else
                    return Json(new { status = "failed", data = errormsg });

                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult DeleteUserLoginCheck(string UserLoginCheckId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";

                // decrypt values
                int UserLoginCheckIdIK = 0;
                if (!string.IsNullOrEmpty(UserLoginCheckId))
                    int.TryParse(_ICustomEncryption.base64d(UserLoginCheckId), out UserLoginCheckIdIK);

                if (UserLoginCheckIdIK > 0)
                {
                    _IUserService.DeleteUserLoginCheck(UserLoginCheckIdIK);
                    message = "Deleted successfully";
                }
                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        #endregion

        #endregion
    }
}