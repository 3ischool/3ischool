﻿using BAL.BAL.Common;
using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.ClassPeriodModule;
using DAL.DAL.Common;
using DAL.DAL.DocumentModule;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.Kendo;
using DAL.DAL.SettingService;
using DAL.DAL.StaffModule;
using DAL.DAL.StudentModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Common;
using ViewModel.ViewModel.HomeWork;
using ViewModel.ViewModel.Student;
using ViewModel.ViewModel.Subject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.Controllers
{
    public class SubjectController : Controller
    {
        #region fields

        public int count = 0;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ILogService _Logger;
        private readonly ISubjectService _ISubjectService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStaffService _IStaffService;
        private readonly IDocumentService _IDocumentService;
        private readonly IWebHelper _WebHelper;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IClassPeriodService _IClassPeriodService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IStudentService _IStudentService;
        private readonly IStudentMasterService _IStudentMasterService;
        private readonly ICommonMethodService _ICommonMethodService;
        #endregion

        #region ctor

        public SubjectController(IDropDownService IDropDownService,
            IActivityLogService IActivityLogService,
            IActivityLogMasterService IActivityLogMasterService,
            ILogService Logger, ISchoolSettingService ISchoolSettingService,
            ISubjectService ISubjectService,
            ICatalogMasterService ICatalogMasterService,
            IStaffService IStaffService, IStudentService IStudentService,
            IDocumentService IDocumentService,
            IWebHelper WebHelper, ICustomEncryption ICustomEncryption,
            IUserService IUserService, IStudentMasterService IStudentMasterService,
            IConnectionService IConnectionService,
            IClassPeriodService IClassPeriodService,
            IUserAuthorizationService IUserAuthorizationService,
            ICommonMethodService ICommonMethodService)
        {
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._Logger = Logger;
            this._ISubjectService = ISubjectService;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IStaffService = IStaffService;
            this._IDocumentService = IDocumentService;
            this._WebHelper = WebHelper;
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._IClassPeriodService = IClassPeriodService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._ICustomEncryption = ICustomEncryption;
            this._IStudentService = IStudentService;
            this._IStudentMasterService = IStudentMasterService;
            this._ICommonMethodService = ICommonMethodService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        #endregion

        #region Bind Core Subject with Standard

        public StandardSubjectModel prepareStdSubjModel(StandardSubjectModel model)
        {
            int?[] stdids = null;
            // copy from standard
            model.AvailableStandardForCopy.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });
            var stdsubjects = _ISubjectService.GetAllStandardSubjects(ref count);
            if (stdsubjects.Count > 0)
            {
                stdids = stdsubjects.Select(s => s.StandardId).Distinct().ToArray();
                foreach (var StdId in stdids)
                {
                    var standards = _ICatalogMasterService.GetStandardMasterById((int)StdId);
                    if (standards != null)
                        model.AvailableStandardForCopy.Add(new SelectListItem
                        {
                            Selected = false,
                            Text = standards.Standard,
                            Value = standards.StandardId.ToString()
                        });
                }
            }

            // standard
            model.AvailableStandard = _IDropDownService.StandardList();

            // Core Subjects
            var coresubjects = _ISubjectService.GetAllSubjects(ref count, IsCore: true);
            foreach (var sub in coresubjects)
            {
                if (model.SubjectIds != null && model.SubjectIds.Count() > 0)
                {
                    if (!model.SubjectIds.Contains(sub.SubjectId))
                        model.AvailableCoreSubject.Add(new SelectListItem { Selected = false, Text = sub.Subject1, Value = sub.SubjectId.ToString() });
                }
                else
                    model.AvailableCoreSubject.Add(new SelectListItem { Selected = false, Text = sub.Subject1, Value = sub.SubjectId.ToString() });
            }

            // persist model data
            if (model.SubjectIds != null && model.SubjectIds.Count() > 0)
            {
                foreach (var subjectid in model.SubjectIds)
                {
                    var subject = coresubjects.Where(s => s.SubjectId == subjectid).FirstOrDefault();
                    model.AvailableSubject.Add(new SelectListItem { Selected = true, Text = subject.Subject1, Value = subject.SubjectId.ToString() });
                }
            }

            return model;
        }

        // check model valid or not
        public StandardSubjectModel CheckErrors(StandardSubjectModel model)
        {
            if (model.StandardId == null || model.StandardId == 0)
            {
                ModelState.AddModelError("StandardId", "Standrad required");
            }
            // check if Standard Subject already saved than not check the validation condition
            if (model.StandardId > 0)
            {
                var standardsubject = _ISubjectService.GetAllStandardSubjects(ref count, model.StandardId);
                if (standardsubject.Count == 0)
                {
                    if (model.SubjectIds == null || model.SubjectIds[0] == null)
                        ModelState.AddModelError("SubjectIds", "No Subject Bind with Standard");
                }
            }

            return model;
        }

        public ActionResult StandardSubject()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "StandardSubject", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new StandardSubjectModel();
                model = prepareStdSubjModel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "StandardSubject", "Save");
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult StandardSubject(StandardSubjectModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "StandardSubject", "Add");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                model = CheckErrors(model);
                if (ModelState.IsValid)
                {
                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    var logtypedel = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();


                    if (!string.IsNullOrEmpty(model.SubIds))
                    {
                        var ArrayList = model.SubIds.Trim(',').Split(',');
                        model.SubjectIds = new int?[ArrayList.Length];
                        for (int i = 0; i < ArrayList.Length; i++)
                        {
                            model.SubjectIds[i] = Convert.ToInt32(ArrayList[i]);
                        }
                    }



                    // delete Standard Subject if already exist
                    var existingstdsubjts = _ISubjectService.GetAllStandardSubjects(ref count, model.StandardId);
                    foreach (var existingstdsub in existingstdsubjts)
                    {
                        // stdclass Id
                        var stdSubtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StandardSubject").FirstOrDefault();
                        if (stdSubtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, stdSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(existingstdsub.StandardSubjectId, stdSubtable.TableId, logtypedel.ActivityLogTypeId, user, String.Format("Delete StandardSubject with StandardSubjectId:{0} and StandardId:{1}", existingstdsub.StandardSubjectId, existingstdsub.StandardId), Request.Browser.Browser);
                        }
                        _ISubjectService.DeleteStandardSubject(existingstdsub.StandardSubjectId);
                    }

                    // insert Standard Subject
                    if (model.SubjectIds != null)
                    {
                        foreach (var Id in model.SubjectIds)
                        {
                            StandardSubject standardsubject = new StandardSubject();
                            standardsubject.StandardId = model.StandardId;
                            standardsubject.SubjectId = Id;
                            _ISubjectService.InsertStandardSubject(standardsubject);

                            // activity log StandardSubject
                            var stdSubtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "StandardSubject").FirstOrDefault();
                            if (stdSubtable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, stdSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(standardsubject.StandardSubjectId, stdSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add StandardSubject with StandardSubjectId:{0} and StandardId:{1}", standardsubject.StandardSubjectId, standardsubject.StandardId), Request.Browser.Browser);
                            }
                        }
                    }

                    var ClassSubjecttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassSubject").FirstOrDefault();
                    var ClassSubjectDettable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassSubjectDetail").FirstOrDefault();
                    // Bind Core Subject With class under current standard
                    // get subject type Compulsory
                    var CompulsorySubjectType = _ISubjectService.GetAllSubjectTypes(ref count, "Compulsory").FirstOrDefault();
                    // get all classes under standard
                    var classes = _ICatalogMasterService.GetAllClassMasters(ref count, StandardId: model.StandardId);
                    // check changes subject list
                    var classIds = classes.Select(c => c.ClassId).ToArray();
                    if (classIds.Length > 0)
                    {
                        // check for optional subjetcs
                        IList<CommonModels> DBcoresubjectarray = new List<CommonModels>();
                        IList<CommonModels> Viewcoresubjectarray = new List<CommonModels>();
                        CommonModels subitem = new CommonModels();
                        ClassSubjectDetail ClassSubjectDetail = new ClassSubjectDetail();

                        foreach (var classid in classIds)
                        {
                            var classsubjects = _ISubjectService.GetAllClassSubjects(ref count, ClassId: classid);
                            var coresubjects = classsubjects.Where(c => c.ClassId == classid &&
                                                    c.SubjectTypeId == CompulsorySubjectType.SubjectTypeId).ToList();
                            // db list
                            foreach (var item in coresubjects)
                            {
                                var IsCoreSubject = _ISubjectService.GetAllStandardSubjects(ref count, StandardId: model.StandardId, SubjectId: item.SubjectId).FirstOrDefault();
                                if (IsCoreSubject == null)
                                {
                                    subitem = new CommonModels();
                                    subitem.Id = (int)item.SubjectId;
                                    DBcoresubjectarray.Add(subitem);
                                }
                            }
                            // Interface list
                            if (model.SubjectIds != null)
                            {
                                foreach (var item in model.SubjectIds)
                                {
                                    subitem = new CommonModels();
                                    subitem.Id = (int)item;
                                    Viewcoresubjectarray.Add(subitem);
                                }
                            }

                            // check user View subject exist in database 
                            foreach (var element in Viewcoresubjectarray)
                            {
                                // if classid , subject typeid same do nothing
                                var viewsubjectexistinDb = DBcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                                if (viewsubjectexistinDb != null)
                                    continue;
                                else
                                {
                                    // else change typeId
                                    var classubjectexistwithouttype = _ISubjectService.GetAllClassSubjects(ref count, ClassId: classid, SubjectId: element.Id).FirstOrDefault();
                                    if (classubjectexistwithouttype != null)
                                    {
                                        if (classubjectexistwithouttype.SubjectTypeId != CompulsorySubjectType.SubjectTypeId)
                                        {
                                            // Class Subject Detail
                                            ClassSubjectDetail = new ClassSubjectDetail();
                                            ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                            ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                            ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                            _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                            // activity log ClassSubjectDetail
                                            if (ClassSubjectDettable != null)
                                            {
                                                // table log saved or not
                                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassSubjectDettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                                // save activity log
                                                if (tabledetail != null)
                                                    _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                            }
                                        }
                                        else
                                        {
                                            classubjectexistwithouttype.ClassId = classid;
                                            classubjectexistwithouttype.SubjectTypeId = CompulsorySubjectType.SubjectTypeId;
                                            classubjectexistwithouttype.IsActive = true;
                                            classubjectexistwithouttype.SubjectId = element.Id;
                                            classubjectexistwithouttype.SkillId = null;
                                            classubjectexistwithouttype.IsActive = true;
                                            classubjectexistwithouttype.SubjectTypeId = CompulsorySubjectType.SubjectTypeId;
                                            _ISubjectService.UpdateClassSubject(classubjectexistwithouttype);
                                            // activity log ClassSubject
                                            if (ClassSubjecttable != null)
                                            {
                                                // table log saved or not
                                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassSubjecttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                                // save activity log
                                                if (tabledetail != null)
                                                    _IActivityLogService.SaveActivityLog(classubjectexistwithouttype.ClassSubjectId, ClassSubjecttable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classubjectexistwithouttype.ClassSubjectId, classubjectexistwithouttype.ClassId), Request.Browser.Browser);
                                            }

                                            // Class Subject Detail
                                            ClassSubjectDetail = new ClassSubjectDetail();
                                            ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                            ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                            ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                            _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                            // activity log ClassSubjectDetail
                                            if (ClassSubjecttable != null)
                                            {
                                                // table log saved or not
                                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassSubjecttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                                // save activity log
                                                if (tabledetail != null)
                                                    _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, ClassSubjecttable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ClassSubject ClassSubject = new ClassSubject();
                                        ClassSubject.ClassId = classid;
                                        ClassSubject.SubjectTypeId = CompulsorySubjectType.SubjectTypeId;
                                        ClassSubject.IsActive = true;
                                        ClassSubject.SubjectId = element.Id;
                                        ClassSubject.SkillId = null;
                                        _ISubjectService.InsertClassSubject(ClassSubject);

                                        // activity log ClassSubject
                                        if (ClassSubjecttable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassSubjecttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(ClassSubject.ClassSubjectId, ClassSubjecttable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubject with ClassSubjectId:{0} and ClassId:{1}", ClassSubject.ClassSubjectId, ClassSubject.ClassId), Request.Browser.Browser);
                                        }

                                        // Class Subject Detail
                                        ClassSubjectDetail = new ClassSubjectDetail();
                                        ClassSubjectDetail.ClassSubjectID = ClassSubject.ClassSubjectId;
                                        ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                        ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                        _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                        // activity log ClassSubjectDetail
                                        if (ClassSubjectDettable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassSubjectDettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                        }
                                    }
                                }
                            }
                            // end foreach                    
                            // check Database subject exist in view
                            foreach (var element in DBcoresubjectarray)
                            {
                                // if classid , subject typeid same do nothing
                                var Dbsubjectexistinview = Viewcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                                if (Dbsubjectexistinview != null)
                                    continue;
                                else
                                {
                                    var classoptsubject = _ISubjectService.GetAllClassSubjects(ref count, ClassId: classid, SubjectId: element.Id, SubjectTypeId: CompulsorySubjectType.SubjectTypeId, IsActive: true).FirstOrDefault();
                                    if (classoptsubject != null)
                                    {
                                        classoptsubject.IsActive = false;
                                        _ISubjectService.UpdateClassSubject(classoptsubject);

                                        // activity log ClassSubject
                                        if (ClassSubjecttable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassSubjecttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(classoptsubject.ClassSubjectId, ClassSubjecttable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classoptsubject.ClassSubjectId, classoptsubject.ClassId), Request.Browser.Browser);
                                        }

                                        // Class Subject Detail
                                        ClassSubjectDetail = new ClassSubjectDetail();
                                        ClassSubjectDetail.ClassSubjectID = classoptsubject.ClassSubjectId;
                                        ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                        ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                        _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                        // activity log ClassSubjectDetail
                                        if (ClassSubjectDettable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, ClassSubjectDettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    TempData["SuccessNotification"] = "Standard Subject binding has been done successfully";
                    return RedirectToAction("StandardSubject");
                }

                model = prepareStdSubjModel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "StandardSubject", "Save");
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #region Bind Optional and Additional Subject with Class

        public ClassSubjectModel prepareClassSubjModel(ClassSubjectModel model)
        {

            model.ClassTypeList.Add(new SelectListItem { Selected = false, Value = "1", Text = "Regular Class" });
            model.ClassTypeList.Add(new SelectListItem { Selected = false, Value = "2", Text = "Virtual Class" });

            // Classes
            model.AvailableClasses = _IDropDownService.ClassList();
            // merge all array into one
            IList<int> viewsubjectIds = new List<int>();
            if (model.CoreSubjectIds != null)
            {
                foreach (var coreid in model.CoreSubjectIds)
                    viewsubjectIds.Add((int)coreid);
            }
            if (model.OptionalSubjectIds != null)
            {
                foreach (var optid in model.OptionalSubjectIds)
                    viewsubjectIds.Add((int)optid);
            }
            if (model.AddtionalSubjectIds != null)
            {
                foreach (var addid in model.AddtionalSubjectIds)
                    viewsubjectIds.Add((int)addid);
            }

            // All Subjects
            var Allsubjects = _ISubjectService.GetAllSubjects(ref count, IsCore: false);
            foreach (var sub in Allsubjects)
            {
                if (viewsubjectIds.Count > 0)
                {
                    if (!viewsubjectIds.Contains(sub.SubjectId))
                        model.AvailableAllSubjects.Add(new SelectListItem { Selected = false, Text = sub.Subject1, Value = sub.SubjectId.ToString() });
                }
                else
                    model.AvailableAllSubjects.Add(new SelectListItem { Selected = false, Text = sub.Subject1, Value = sub.SubjectId.ToString() });
            }

            // Skill Subjects
            var AllPracticalSubject = _ISubjectService.GetAllSubjectSkills(ref count);
            foreach (var skill in AllPracticalSubject)
            {
                if (model.SkillSubjectIds != null)
                {
                    if (!model.SkillSubjectIds.Contains(skill.SubjectSkillId))
                        model.AvailableAllSkillSubjects.Add(new SelectListItem { Selected = false, Text = skill.SubjectSkill1, Value = skill.SubjectSkillId.ToString() });
                }
                else
                    model.AvailableAllSkillSubjects.Add(new SelectListItem { Selected = false, Text = skill.SubjectSkill1, Value = skill.SubjectSkillId.ToString() });
            }

            // bind view core subjects
            if (model.CoreSubjectIds != null)
            {
                foreach (var coresubId in model.CoreSubjectIds)
                {
                    var compsubject = _ISubjectService.GetSubjectById((int)coresubId);
                    if (compsubject != null)
                        model.AvailableCoreSubjects.Add(new SelectListItem { Selected = false, Text = compsubject.Subject1, Value = compsubject.SubjectId.ToString() });
                }
            }

            // bind view optional subjects
            if (model.OptionalSubjectIds != null)
            {
                foreach (var optsubId in model.OptionalSubjectIds)
                {
                    var compsubject = _ISubjectService.GetSubjectById((int)optsubId);
                    if (compsubject != null)
                        model.AvailableOptionalSubject.Add(new SelectListItem { Selected = false, Text = compsubject.Subject1, Value = compsubject.SubjectId.ToString() });
                }
            }

            // bind view additional subjects
            if (model.AddtionalSubjectIds != null)
            {
                foreach (var addsubId in model.AddtionalSubjectIds)
                {
                    var compsubject = _ISubjectService.GetSubjectById((int)addsubId);
                    if (compsubject != null)
                        model.AvailableAddtionalSubject.Add(new SelectListItem { Selected = false, Text = compsubject.Subject1, Value = compsubject.SubjectId.ToString() });
                }
            }

            // bind view Skill subjects
            if (model.SkillSubjectIds != null)
            {
                foreach (var skillId in model.SkillSubjectIds)
                {
                    var skillsubject = _ISubjectService.GetSubjectSkillById((int)skillId);
                    if (skillsubject != null)
                        model.AvailableSkillSubjects.Add(new SelectListItem { Selected = false, Text = skillsubject.SubjectSkill1, Value = skillsubject.SubjectSkillId.ToString() });
                }
            }

            model.IsDynamicNavigationPath = false;
            var AllowVirtualClassCreation = _ISchoolSettingService.GetorSetSchoolData("AllowVirtualClassCreation_ForLite", "0").AttributeValue == "1" ? true : false;
            model.IsDynamicNavigationPath = AllowVirtualClassCreation;

            return model;
        }

        // check model valid or not
        public ClassSubjectModel CheckClassSubjectModelErrors(ClassSubjectModel model)
        {
            if (model.ClassId == null || model.ClassId == 0)
                ModelState.AddModelError("ClassId", "Class required");
            //if (model.CoreSubjectIds == null)
            //    ModelState.AddModelError("CoreSubjectIds", "Compulsory subjects required");

            return model;
        }

        public ActionResult ClassSubject(int Id = 0)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "ClassSubject", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new ClassSubjectModel();
                model = prepareClassSubjModel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "ClassSubject", "Save");
                model.ClassId = Id;
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult ClassSubject(ClassSubjectModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "ClassSubject", "Add");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                bool IsData = false;
                model = CheckClassSubjectModelErrors(model);
                if (ModelState.IsValid)
                {
                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    var logtypedel = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
                    var clsSubtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassSubject").FirstOrDefault();
                    var clsSubdettable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "ClassSubjectDetail").FirstOrDefault();

                    // get subject type Optional
                    var CoreSubjectType = _ISubjectService.GetAllSubjectTypes(ref count, "Compulsory").FirstOrDefault();
                    // get subject type Optional
                    var OptionalSubjectType = _ISubjectService.GetAllSubjectTypes(ref count, "Optional").FirstOrDefault();
                    // get subject type Additional
                    var AdditionalSubjectType = _ISubjectService.GetAllSubjectTypes(ref count, "Additional").FirstOrDefault();
                    // check subject of class already exist
                    var classsubjects = _ISubjectService.GetAllClassSubjects(ref count, ClassId: model.ClassId);
                    // Standard from class
                    var currentclass = _ICatalogMasterService.GetClassMasterById((int)model.ClassId);
                    var StandardId = currentclass != null ? currentclass.StandardId : 0;

                    if (model.ClassTypeId == 2)
                    {
                        var virtualclass = _ICatalogMasterService.GetAllVirtualClasses(ref count).Where(p => p.VirtualClassId == model.ClassId).FirstOrDefault();
                        StandardId = virtualclass.StandardId;
                    }

                    // check for optional subjetcs
                    IList<CommonModels> DBcoresubjectarray = new List<CommonModels>();
                    IList<CommonModels> Viewcoresubjectarray = new List<CommonModels>();
                    CommonModels subitem = new CommonModels();
                    ClassSubjectDetail ClassSubjectDetail = new ClassSubjectDetail();

                    if (model.ClassTypeId == 1)
                    {

                        #region Core Subjects

                        IsData = true;
                        var coresubjects = classsubjects.Where(c => c.ClassId == model.ClassId && c.SubjectTypeId == CoreSubjectType.SubjectTypeId
                            && c.IsActive == true).ToList();
                        // db list
                        foreach (var item in coresubjects)
                        {
                            var IsCoreSubject = _ISubjectService.GetAllStandardSubjects(ref count, StandardId: StandardId, SubjectId: item.SubjectId).FirstOrDefault();
                            if (IsCoreSubject == null)
                            {
                                subitem = new CommonModels();
                                subitem.Id = (int)item.SubjectId;
                                DBcoresubjectarray.Add(subitem);
                            }
                        }
                        // Interface list
                        if (model.CoreSubjectIds != null)
                        {
                            foreach (var item in model.CoreSubjectIds)
                            {
                                subitem = new CommonModels();
                                subitem.Id = (int)item;
                                Viewcoresubjectarray.Add(subitem);
                            }
                        }
                        // check user View subject exist in database 
                        foreach (var element in Viewcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var viewsubjectexistinDb = DBcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (viewsubjectexistinDb != null)
                                continue;
                            else
                            {
                                // else change typeId
                                var classubjectexistwithouttype = _ISubjectService.GetAllClassSubjects(ref count, ClassId: model.ClassId, SubjectId: element.Id).Where(m => m.SkillId == null).FirstOrDefault();
                                if (classubjectexistwithouttype != null)
                                {
                                    if (classubjectexistwithouttype.SubjectTypeId != CoreSubjectType.SubjectTypeId)
                                    {
                                        // Class Subject Detail
                                        ClassSubjectDetail = new ClassSubjectDetail();
                                        ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                        ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                        ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                        _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                        // activity log ClassSubjectDetail
                                        if (clsSubdettable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                        }
                                    }

                                    classubjectexistwithouttype.IsActive = true;
                                    classubjectexistwithouttype.SubjectTypeId = CoreSubjectType.SubjectTypeId;
                                    _ISubjectService.UpdateClassSubject(classubjectexistwithouttype);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classubjectexistwithouttype.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classubjectexistwithouttype.ClassSubjectId, classubjectexistwithouttype.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                                else
                                {
                                    ClassSubject ClassSubject = new ClassSubject();
                                    ClassSubject.ClassId = model.ClassId;
                                    ClassSubject.SubjectTypeId = CoreSubjectType.SubjectTypeId;
                                    ClassSubject.IsActive = true;
                                    ClassSubject.SubjectId = element.Id;
                                    ClassSubject.SkillId = null;
                                    _ISubjectService.InsertClassSubject(ClassSubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubject with ClassSubjectId:{0} and ClassId:{1}", ClassSubject.ClassSubjectId, ClassSubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = ClassSubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach                    
                        // check Database subject exist in view
                        foreach (var element in DBcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var Dbsubjectexistinview = Viewcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (Dbsubjectexistinview != null)
                                continue;
                            else
                            {
                                var classoptsubject = _ISubjectService.GetAllClassSubjects(ref count, ClassId: model.ClassId, SubjectId: element.Id, SubjectTypeId: CoreSubjectType.SubjectTypeId, IsActive: true).FirstOrDefault();
                                if (classoptsubject != null)
                                {
                                    classoptsubject.IsActive = false;
                                    _ISubjectService.UpdateClassSubject(classoptsubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classoptsubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classoptsubject.ClassSubjectId, classoptsubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classoptsubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach
                        // end core subject 

                        #endregion

                        #region optional Subject

                        IsData = true;
                        DBcoresubjectarray = new List<CommonModels>();
                        Viewcoresubjectarray = new List<CommonModels>();

                        var optionalsubjects = classsubjects.Where(c => c.ClassId == model.ClassId && c.SubjectTypeId == OptionalSubjectType.SubjectTypeId
                            && c.IsActive == true).ToList();
                        // DB List
                        foreach (var item in optionalsubjects)
                        {
                            subitem = new CommonModels();
                            subitem.Id = (int)item.SubjectId;
                            DBcoresubjectarray.Add(subitem);
                        }
                        if (model.OptionalSubjectIds != null)
                        {
                            // View list
                            foreach (var item in model.OptionalSubjectIds)
                            {
                                subitem = new CommonModels();
                                subitem.Id = (int)item;
                                Viewcoresubjectarray.Add(subitem);
                            }
                        }

                        // check user View subject exist in database 
                        foreach (var element in Viewcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var viewsubjectexistinDb = DBcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (viewsubjectexistinDb != null)
                                continue;
                            else
                            {
                                // else change typeId
                                var classubjectexistwithouttype = _ISubjectService.GetAllClassSubjects(ref count, ClassId: model.ClassId, SubjectId: element.Id).Where(m => m.SkillId == null).FirstOrDefault();
                                if (classubjectexistwithouttype != null)
                                {
                                    if (classubjectexistwithouttype.SubjectTypeId != OptionalSubjectType.SubjectTypeId)
                                    {
                                        // Class Subject Detail
                                        ClassSubjectDetail = new ClassSubjectDetail();
                                        ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                        ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                        ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                        _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                        // activity log ClassSubjectDetail
                                        if (clsSubdettable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                        }
                                    }

                                    classubjectexistwithouttype.IsActive = true;
                                    classubjectexistwithouttype.SubjectTypeId = OptionalSubjectType.SubjectTypeId;
                                    _ISubjectService.UpdateClassSubject(classubjectexistwithouttype);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classubjectexistwithouttype.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classubjectexistwithouttype.ClassSubjectId, classubjectexistwithouttype.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                                else
                                {
                                    ClassSubject ClassSubject = new ClassSubject();
                                    ClassSubject.ClassId = model.ClassId;
                                    ClassSubject.SubjectTypeId = OptionalSubjectType.SubjectTypeId;
                                    ClassSubject.IsActive = true;
                                    ClassSubject.SubjectId = element.Id;
                                    ClassSubject.SkillId = null;
                                    _ISubjectService.InsertClassSubject(ClassSubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubject with ClassSubjectId:{0} and ClassId:{1}", ClassSubject.ClassSubjectId, ClassSubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = ClassSubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach

                        // check Database subject exist in view
                        foreach (var element in DBcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var Dbsubjectexistinview = Viewcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (Dbsubjectexistinview != null)
                                continue;
                            else
                            {
                                var classoptsubject = _ISubjectService.GetAllClassSubjects(ref count, ClassId: model.ClassId, SubjectId: element.Id, SubjectTypeId: OptionalSubjectType.SubjectTypeId, IsActive: true).FirstOrDefault();
                                if (classoptsubject != null)
                                {
                                    classoptsubject.IsActive = false;
                                    _ISubjectService.UpdateClassSubject(classoptsubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classoptsubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classoptsubject.ClassSubjectId, classoptsubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classoptsubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach
                        #endregion

                        #region Additional Subject

                        IsData = true;
                        DBcoresubjectarray = new List<CommonModels>();
                        Viewcoresubjectarray = new List<CommonModels>();

                        var additionalsubjects = classsubjects.Where(c => c.ClassId == model.ClassId && c.SubjectTypeId == AdditionalSubjectType.SubjectTypeId
                            && c.IsActive == true).ToList();
                        // DB List
                        foreach (var item in additionalsubjects)
                        {
                            subitem = new CommonModels();
                            subitem.Id = (int)item.SubjectId;
                            DBcoresubjectarray.Add(subitem);
                        }
                        if (model.AddtionalSubjectIds != null)
                        {
                            // View list
                            foreach (var item in model.AddtionalSubjectIds)
                            {
                                subitem = new CommonModels();
                                subitem.Id = (int)item;
                                Viewcoresubjectarray.Add(subitem);
                            }
                        }

                        // check user View subject exist in database 
                        foreach (var element in Viewcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var viewsubjectexistinDb = DBcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (viewsubjectexistinDb != null)
                                continue;
                            else
                            {
                                // else change typeId
                                var classubjectexistwithouttype = _ISubjectService.GetAllClassSubjects(ref count, ClassId: model.ClassId, SubjectId: element.Id).Where(m => m.SkillId == null).FirstOrDefault();
                                if (classubjectexistwithouttype != null)
                                {
                                    if (classubjectexistwithouttype.SubjectTypeId != AdditionalSubjectType.SubjectTypeId)
                                    {
                                        // Class Subject Detail
                                        ClassSubjectDetail = new ClassSubjectDetail();
                                        ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                        ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                        ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                        _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                        // activity log ClassSubjectDetail
                                        if (clsSubdettable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                        }
                                    }

                                    classubjectexistwithouttype.IsActive = true;
                                    classubjectexistwithouttype.SubjectTypeId = AdditionalSubjectType.SubjectTypeId;
                                    _ISubjectService.UpdateClassSubject(classubjectexistwithouttype);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classubjectexistwithouttype.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classubjectexistwithouttype.ClassSubjectId, classubjectexistwithouttype.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                                else
                                {
                                    ClassSubject ClassSubject = new ClassSubject();
                                    ClassSubject.ClassId = model.ClassId;
                                    ClassSubject.SubjectTypeId = AdditionalSubjectType.SubjectTypeId;
                                    ClassSubject.IsActive = true;
                                    ClassSubject.SubjectId = element.Id;
                                    ClassSubject.SkillId = null;
                                    _ISubjectService.InsertClassSubject(ClassSubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubject with ClassSubjectId:{0} and ClassId:{1}", ClassSubject.ClassSubjectId, ClassSubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = ClassSubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach

                        // check Database subject exist in view
                        foreach (var element in DBcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var Dbsubjectexistinview = Viewcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (Dbsubjectexistinview != null)
                                continue;
                            else
                            {
                                var classoptsubject = _ISubjectService.GetAllClassSubjects(ref count, ClassId: model.ClassId, SubjectId: element.Id, SubjectTypeId: AdditionalSubjectType.SubjectTypeId, IsActive: true).FirstOrDefault();
                                if (classoptsubject != null)
                                {
                                    classoptsubject.IsActive = false;
                                    _ISubjectService.UpdateClassSubject(classoptsubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classoptsubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classoptsubject.ClassSubjectId, classoptsubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classoptsubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach
                        #endregion

                        #region Skill Subject

                        IsData = true;
                        DBcoresubjectarray = new List<CommonModels>();
                        Viewcoresubjectarray = new List<CommonModels>();

                        var skillsubjects = classsubjects.Where(c => c.ClassId == model.ClassId && c.SubjectTypeId == AdditionalSubjectType.SubjectTypeId
                            && c.IsActive == true && c.SkillId != null).ToList();
                        // DB List
                        foreach (var item in skillsubjects)
                        {
                            subitem = new CommonModels();
                            var skillsubject = _ISubjectService.GetAllSubjectSkills(ref count, SubjectId: item.SubjectId, SkillId: item.SkillId).FirstOrDefault();
                            if (skillsubject != null)
                            {
                                subitem.Id = skillsubject.SubjectSkillId;
                                DBcoresubjectarray.Add(subitem);
                            }
                        }
                        if (model.SkillSubjectIds != null)
                        {
                            // View list
                            foreach (var item in model.SkillSubjectIds)
                            {

                                subitem = new CommonModels();
                                subitem.Id = (int)item;
                                Viewcoresubjectarray.Add(subitem);
                            }
                        }

                        // check user View subject exist in database 
                        foreach (var element in Viewcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var viewsubjectexistinDb = DBcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (viewsubjectexistinDb != null)
                                continue;
                            else
                            {
                                // else change typeId
                                var skillsubject = _ISubjectService.GetSubjectSkillById(element.Id);
                                var classubjectexistwithouttype = _ISubjectService.GetAllClassSubjects(ref count, ClassId: model.ClassId, SubjectId: skillsubject.SubjectId, SkillId: skillsubject.SkillId).FirstOrDefault();
                                if (classubjectexistwithouttype != null)
                                {
                                    if (classubjectexistwithouttype.SubjectTypeId != AdditionalSubjectType.SubjectTypeId)
                                    {
                                        // Class Subject Detail
                                        ClassSubjectDetail = new ClassSubjectDetail();
                                        ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                        ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                        ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                        _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                        // activity log ClassSubjectDetail
                                        if (clsSubdettable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                        }
                                    }

                                    classubjectexistwithouttype.IsActive = true;
                                    classubjectexistwithouttype.SubjectTypeId = AdditionalSubjectType.SubjectTypeId;
                                    _ISubjectService.UpdateClassSubject(classubjectexistwithouttype);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classubjectexistwithouttype.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classubjectexistwithouttype.ClassSubjectId, classubjectexistwithouttype.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                                else
                                {
                                    ClassSubject ClassSubject = new ClassSubject();
                                    ClassSubject.ClassId = model.ClassId;
                                    ClassSubject.SubjectTypeId = OptionalSubjectType.SubjectTypeId;
                                    ClassSubject.IsActive = true;
                                    ClassSubject.SubjectId = skillsubject.SubjectId;
                                    ClassSubject.SkillId = skillsubject.SkillId;
                                    _ISubjectService.InsertClassSubject(ClassSubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubject with ClassSubjectId:{0} and ClassId:{1}", ClassSubject.ClassSubjectId, ClassSubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = ClassSubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach

                        // check Database subject exist in view
                        foreach (var element in DBcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var Dbsubjectexistinview = Viewcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (Dbsubjectexistinview != null)
                                continue;
                            else
                            {
                                var skillsubject = _ISubjectService.GetSubjectSkillById(element.Id);
                                var classoptsubject = _ISubjectService.GetAllClassSubjects(ref count, ClassId: model.ClassId, SubjectId: skillsubject.SubjectId, SubjectTypeId: AdditionalSubjectType.SubjectTypeId, IsActive: true).FirstOrDefault();
                                if (classoptsubject != null)
                                {
                                    classoptsubject.IsActive = false;
                                    _ISubjectService.UpdateClassSubject(classoptsubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classoptsubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classoptsubject.ClassSubjectId, classoptsubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classoptsubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach
                        #endregion
                    }
                    else
                    {

                        #region Core Subjects

                        IsData = true;
                        var coresubjects = _ISubjectService.GetAllClassSubjects(ref count).Where(c => c.VirtualClassId == model.ClassId && c.SubjectTypeId == CoreSubjectType.SubjectTypeId
                            && c.IsActive == true).ToList();
                        // db list
                        foreach (var item in coresubjects)
                        {
                            var IsCoreSubject = _ISubjectService.GetAllStandardSubjects(ref count, StandardId: StandardId, SubjectId: item.SubjectId).FirstOrDefault();
                            if (IsCoreSubject == null)
                            {
                                subitem = new CommonModels();
                                subitem.Id = (int)item.SubjectId;
                                DBcoresubjectarray.Add(subitem);
                            }
                        }
                        // Interface list
                        if (model.CoreSubjectIds != null)
                        {
                            foreach (var item in model.CoreSubjectIds)
                            {
                                subitem = new CommonModels();
                                subitem.Id = (int)item;
                                Viewcoresubjectarray.Add(subitem);
                            }
                        }
                        // check user View subject exist in database 
                        foreach (var element in Viewcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var viewsubjectexistinDb = DBcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (viewsubjectexistinDb != null)
                                continue;
                            else
                            {
                                // else change typeId
                                var classubjectexistwithouttype = _ISubjectService.GetAllClassSubjects(ref count, VirtualClassId: model.ClassId, SubjectId: element.Id).Where(m => m.SkillId == null).FirstOrDefault();
                                if (classubjectexistwithouttype != null)
                                {
                                    if (classubjectexistwithouttype.SubjectTypeId != CoreSubjectType.SubjectTypeId)
                                    {
                                        // Class Subject Detail
                                        ClassSubjectDetail = new ClassSubjectDetail();
                                        ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                        ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                        ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                        _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                        // activity log ClassSubjectDetail
                                        if (clsSubdettable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                        }
                                    }

                                    classubjectexistwithouttype.IsActive = true;
                                    classubjectexistwithouttype.SubjectTypeId = CoreSubjectType.SubjectTypeId;
                                    _ISubjectService.UpdateClassSubject(classubjectexistwithouttype);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classubjectexistwithouttype.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classubjectexistwithouttype.ClassSubjectId, classubjectexistwithouttype.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                                else
                                {
                                    ClassSubject ClassSubject = new ClassSubject();
                                    ClassSubject.VirtualClassId = model.ClassId;
                                    ClassSubject.SubjectTypeId = CoreSubjectType.SubjectTypeId;
                                    ClassSubject.IsActive = true;
                                    ClassSubject.SubjectId = element.Id;
                                    ClassSubject.SkillId = null;
                                    _ISubjectService.InsertClassSubject(ClassSubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubject with ClassSubjectId:{0} and ClassId:{1}", ClassSubject.ClassSubjectId, ClassSubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = ClassSubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach                    
                        // check Database subject exist in view
                        foreach (var element in DBcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var Dbsubjectexistinview = Viewcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (Dbsubjectexistinview != null)
                                continue;
                            else
                            {
                                var classoptsubject = _ISubjectService.GetAllClassSubjects(ref count, VirtualClassId: model.ClassId, SubjectId: element.Id, SubjectTypeId: CoreSubjectType.SubjectTypeId, IsActive: true).FirstOrDefault();
                                if (classoptsubject != null)
                                {
                                    classoptsubject.IsActive = false;
                                    _ISubjectService.UpdateClassSubject(classoptsubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classoptsubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classoptsubject.ClassSubjectId, classoptsubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classoptsubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach
                        // end core subject 

                        #endregion

                        #region optional Subject

                        IsData = true;
                        DBcoresubjectarray = new List<CommonModels>();
                        Viewcoresubjectarray = new List<CommonModels>();

                        var optionalsubjects = classsubjects.Where(c => c.VirtualClassId == model.ClassId && c.SubjectTypeId == OptionalSubjectType.SubjectTypeId
                            && c.IsActive == true).ToList();
                        // DB List
                        foreach (var item in optionalsubjects)
                        {
                            subitem = new CommonModels();
                            subitem.Id = (int)item.SubjectId;
                            DBcoresubjectarray.Add(subitem);
                        }
                        if (model.OptionalSubjectIds != null)
                        {
                            // View list
                            foreach (var item in model.OptionalSubjectIds)
                            {
                                subitem = new CommonModels();
                                subitem.Id = (int)item;
                                Viewcoresubjectarray.Add(subitem);
                            }
                        }

                        // check user View subject exist in database 
                        foreach (var element in Viewcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var viewsubjectexistinDb = DBcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (viewsubjectexistinDb != null)
                                continue;
                            else
                            {
                                // else change typeId
                                var classubjectexistwithouttype = _ISubjectService.GetAllClassSubjects(ref count, VirtualClassId: model.ClassId, SubjectId: element.Id).Where(m => m.SkillId == null).FirstOrDefault();
                                if (classubjectexistwithouttype != null)
                                {
                                    if (classubjectexistwithouttype.SubjectTypeId != OptionalSubjectType.SubjectTypeId)
                                    {
                                        // Class Subject Detail
                                        ClassSubjectDetail = new ClassSubjectDetail();
                                        ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                        ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                        ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                        _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                        // activity log ClassSubjectDetail
                                        if (clsSubdettable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                        }
                                    }

                                    classubjectexistwithouttype.IsActive = true;
                                    classubjectexistwithouttype.SubjectTypeId = OptionalSubjectType.SubjectTypeId;
                                    _ISubjectService.UpdateClassSubject(classubjectexistwithouttype);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classubjectexistwithouttype.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classubjectexistwithouttype.ClassSubjectId, classubjectexistwithouttype.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                                else
                                {
                                    ClassSubject ClassSubject = new ClassSubject();
                                    ClassSubject.VirtualClassId = model.ClassId;
                                    ClassSubject.SubjectTypeId = OptionalSubjectType.SubjectTypeId;
                                    ClassSubject.IsActive = true;
                                    ClassSubject.SubjectId = element.Id;
                                    ClassSubject.SkillId = null;
                                    _ISubjectService.InsertClassSubject(ClassSubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubject with ClassSubjectId:{0} and ClassId:{1}", ClassSubject.ClassSubjectId, ClassSubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = ClassSubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach

                        // check Database subject exist in view
                        foreach (var element in DBcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var Dbsubjectexistinview = Viewcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (Dbsubjectexistinview != null)
                                continue;
                            else
                            {
                                var classoptsubject = _ISubjectService.GetAllClassSubjects(ref count, VirtualClassId: model.ClassId, SubjectId: element.Id, SubjectTypeId: OptionalSubjectType.SubjectTypeId, IsActive: true).FirstOrDefault();
                                if (classoptsubject != null)
                                {
                                    classoptsubject.IsActive = false;
                                    _ISubjectService.UpdateClassSubject(classoptsubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classoptsubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classoptsubject.ClassSubjectId, classoptsubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classoptsubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach
                        #endregion

                        #region Additional Subject

                        IsData = true;
                        DBcoresubjectarray = new List<CommonModels>();
                        Viewcoresubjectarray = new List<CommonModels>();

                        var additionalsubjects = classsubjects.Where(c => c.VirtualClassId == model.ClassId && c.SubjectTypeId == AdditionalSubjectType.SubjectTypeId
                            && c.IsActive == true).ToList();
                        // DB List
                        foreach (var item in additionalsubjects)
                        {
                            subitem = new CommonModels();
                            subitem.Id = (int)item.SubjectId;
                            DBcoresubjectarray.Add(subitem);
                        }
                        if (model.AddtionalSubjectIds != null)
                        {
                            // View list
                            foreach (var item in model.AddtionalSubjectIds)
                            {
                                subitem = new CommonModels();
                                subitem.Id = (int)item;
                                Viewcoresubjectarray.Add(subitem);
                            }
                        }

                        // check user View subject exist in database 
                        foreach (var element in Viewcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var viewsubjectexistinDb = DBcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (viewsubjectexistinDb != null)
                                continue;
                            else
                            {
                                // else change typeId
                                var classubjectexistwithouttype = _ISubjectService.GetAllClassSubjects(ref count, VirtualClassId: model.ClassId, SubjectId: element.Id).Where(m => m.SkillId == null).FirstOrDefault();
                                if (classubjectexistwithouttype != null)
                                {
                                    if (classubjectexistwithouttype.SubjectTypeId != AdditionalSubjectType.SubjectTypeId)
                                    {
                                        // Class Subject Detail
                                        ClassSubjectDetail = new ClassSubjectDetail();
                                        ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                        ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                        ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                        _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                        // activity log ClassSubjectDetail
                                        if (clsSubdettable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                        }
                                    }

                                    classubjectexistwithouttype.IsActive = true;
                                    classubjectexistwithouttype.SubjectTypeId = AdditionalSubjectType.SubjectTypeId;
                                    _ISubjectService.UpdateClassSubject(classubjectexistwithouttype);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classubjectexistwithouttype.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classubjectexistwithouttype.ClassSubjectId, classubjectexistwithouttype.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                                else
                                {
                                    ClassSubject ClassSubject = new ClassSubject();
                                    ClassSubject.VirtualClassId = model.ClassId;
                                    ClassSubject.SubjectTypeId = AdditionalSubjectType.SubjectTypeId;
                                    ClassSubject.IsActive = true;
                                    ClassSubject.SubjectId = element.Id;
                                    ClassSubject.SkillId = null;
                                    _ISubjectService.InsertClassSubject(ClassSubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubject with ClassSubjectId:{0} and ClassId:{1}", ClassSubject.ClassSubjectId, ClassSubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = ClassSubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach

                        // check Database subject exist in view
                        foreach (var element in DBcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var Dbsubjectexistinview = Viewcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (Dbsubjectexistinview != null)
                                continue;
                            else
                            {
                                var classoptsubject = _ISubjectService.GetAllClassSubjects(ref count, VirtualClassId: model.ClassId, SubjectId: element.Id, SubjectTypeId: AdditionalSubjectType.SubjectTypeId, IsActive: true).FirstOrDefault();
                                if (classoptsubject != null)
                                {
                                    classoptsubject.IsActive = false;
                                    _ISubjectService.UpdateClassSubject(classoptsubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classoptsubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classoptsubject.ClassSubjectId, classoptsubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classoptsubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach
                        #endregion

                        #region Skill Subject

                        IsData = true;
                        DBcoresubjectarray = new List<CommonModels>();
                        Viewcoresubjectarray = new List<CommonModels>();

                        var skillsubjects = classsubjects.Where(c => c.VirtualClassId == model.ClassId && c.SubjectTypeId == AdditionalSubjectType.SubjectTypeId
                            && c.IsActive == true && c.SkillId != null).ToList();
                        // DB List
                        foreach (var item in skillsubjects)
                        {
                            subitem = new CommonModels();
                            var skillsubject = _ISubjectService.GetAllSubjectSkills(ref count, SubjectId: item.SubjectId, SkillId: item.SkillId).FirstOrDefault();
                            if (skillsubject != null)
                            {
                                subitem.Id = skillsubject.SubjectSkillId;
                                DBcoresubjectarray.Add(subitem);
                            }
                        }
                        if (model.SkillSubjectIds != null)
                        {
                            // View list
                            foreach (var item in model.SkillSubjectIds)
                            {

                                subitem = new CommonModels();
                                subitem.Id = (int)item;
                                Viewcoresubjectarray.Add(subitem);
                            }
                        }

                        // check user View subject exist in database 
                        foreach (var element in Viewcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var viewsubjectexistinDb = DBcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (viewsubjectexistinDb != null)
                                continue;
                            else
                            {
                                // else change typeId
                                var skillsubject = _ISubjectService.GetSubjectSkillById(element.Id);
                                var classubjectexistwithouttype = _ISubjectService.GetAllClassSubjects(ref count, VirtualClassId: model.ClassId, SubjectId: skillsubject.SubjectId, SkillId: skillsubject.SkillId).FirstOrDefault();
                                if (classubjectexistwithouttype != null)
                                {
                                    if (classubjectexistwithouttype.SubjectTypeId != AdditionalSubjectType.SubjectTypeId)
                                    {
                                        // Class Subject Detail
                                        ClassSubjectDetail = new ClassSubjectDetail();
                                        ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                        ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                        ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                        _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                        // activity log ClassSubjectDetail
                                        if (clsSubdettable != null)
                                        {
                                            // table log saved or not
                                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                            // save activity log
                                            if (tabledetail != null)
                                                _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                        }
                                    }

                                    classubjectexistwithouttype.IsActive = true;
                                    classubjectexistwithouttype.SubjectTypeId = AdditionalSubjectType.SubjectTypeId;
                                    _ISubjectService.UpdateClassSubject(classubjectexistwithouttype);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classubjectexistwithouttype.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classubjectexistwithouttype.ClassSubjectId, classubjectexistwithouttype.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classubjectexistwithouttype.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                                else
                                {
                                    ClassSubject ClassSubject = new ClassSubject();
                                    ClassSubject.VirtualClassId = model.ClassId;
                                    ClassSubject.SubjectTypeId = OptionalSubjectType.SubjectTypeId;
                                    ClassSubject.IsActive = true;
                                    ClassSubject.SubjectId = skillsubject.SubjectId;
                                    ClassSubject.SkillId = skillsubject.SkillId;
                                    _ISubjectService.InsertClassSubject(ClassSubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubject with ClassSubjectId:{0} and ClassId:{1}", ClassSubject.ClassSubjectId, ClassSubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = ClassSubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.Start);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach

                        // check Database subject exist in view
                        foreach (var element in DBcoresubjectarray)
                        {
                            // if classid , subject typeid same do nothing
                            var Dbsubjectexistinview = Viewcoresubjectarray.Where(d => d.Id == element.Id).FirstOrDefault();
                            if (Dbsubjectexistinview != null)
                                continue;
                            else
                            {
                                var skillsubject = _ISubjectService.GetSubjectSkillById(element.Id);
                                var classoptsubject = _ISubjectService.GetAllClassSubjects(ref count, VirtualClassId: model.ClassId, SubjectId: skillsubject.SubjectId, SubjectTypeId: AdditionalSubjectType.SubjectTypeId, IsActive: true).FirstOrDefault();
                                if (classoptsubject != null)
                                {
                                    classoptsubject.IsActive = false;
                                    _ISubjectService.UpdateClassSubject(classoptsubject);

                                    // activity log ClassSubject
                                    if (clsSubtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(classoptsubject.ClassSubjectId, clsSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update ClassSubject with ClassSubjectId:{0} and ClassId:{1}", classoptsubject.ClassSubjectId, classoptsubject.ClassId), Request.Browser.Browser);
                                    }

                                    // Class Subject Detail
                                    ClassSubjectDetail = new ClassSubjectDetail();
                                    ClassSubjectDetail.ClassSubjectID = classoptsubject.ClassSubjectId;
                                    ClassSubjectDetail.EntryType = Convert.ToInt32(EntryType.End);
                                    ClassSubjectDetail.EntryDate = DateTime.UtcNow;
                                    _ISubjectService.InsertClassSubjectDetail(ClassSubjectDetail);

                                    // activity log ClassSubjectDetail
                                    if (clsSubdettable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, clsSubdettable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(ClassSubjectDetail.ClassSubjectDetailId, clsSubdettable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add ClassSubjectDetail with ClassSubjectDetailId:{0} and ClassSubjectID:{1}", ClassSubjectDetail.ClassSubjectDetailId, ClassSubjectDetail.ClassSubjectID), Request.Browser.Browser);
                                    }
                                }
                            }
                        }
                        // end foreach
                        #endregion
                    }

                    if (IsData)
                        TempData["SuccessNotification"] = "Class Subject binding has been done successfully";
                    else
                        TempData["ErrorNotification"] = "No Subject bind with class";

                    return RedirectToAction("ClassSubject");
                }

                model = prepareClassSubjModel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "ClassSubject", "Add");
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #region Bind Class and Subject with Teacher

        public TeacherSubjectClassModel prepareteachermodel(TeacherSubjectClassModel model)
        {
            // teacher
            var stafftype = _IStaffService.GetAllStaffTypes(ref count).Where(m => m.IsTeacher == true).FirstOrDefault();
            var teachers = _IStaffService.GetAllStaffs(ref count, IsActive: true, StaffTypeId: stafftype.StaffTypeId).ToList();
            teachers = teachers.OrderBy(p => p.FName).ToList();

            model.AvailableTeachers.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            foreach (var teacher in teachers)
            {
                string TeacheNameWithEmpCode = teacher.FName + " " + teacher.LName;
                if (!string.IsNullOrEmpty(teacher.EmpCode))
                    TeacheNameWithEmpCode = teacher.FName + " " + teacher.LName + " (" + teacher.EmpCode + ")";

                model.AvailableTeachers.Add(new SelectListItem
                {
                    Selected = false,
                    Text = TeacheNameWithEmpCode,
                    Value = teacher.StaffId.ToString()
                });
            }

            // subjects
            var allsubjects = _ISubjectService.GetAllSubjects(ref count);
            foreach (var subject in allsubjects)
            {
                if (model.SelectedSubjectIds != null)
                {
                    if (!model.SelectedSubjectIds.Contains(subject.SubjectId))
                    {
                        model.AvailableAllSubjects.Add(new SelectListItem
                        {
                            Selected = false,
                            Text = subject.Subject1,
                            Value = subject.SubjectId.ToString()
                        });
                    }
                }
                else
                {
                    model.AvailableAllSubjects.Add(new SelectListItem
                    {
                        Selected = false,
                        Text = subject.Subject1,
                        Value = subject.SubjectId.ToString()
                    });
                }
            }

            // Class
            var allstandard = _ICatalogMasterService.GetAllStandardMasters(ref count);
            foreach (var standard in allstandard)
            {
                if (model.SelectedClassIds != null)
                {
                    if (!model.SelectedClassIds.Contains(standard.StandardId))
                        model.AvailableClasses.Add(new SelectListItem { Selected = false, Text = standard.Standard, Value = standard.StandardId.ToString() });
                }
                else
                    model.AvailableClasses.Add(new SelectListItem { Selected = false, Text = standard.Standard, Value = standard.StandardId.ToString() });
            }

            // bind view standrad with teacher
            if (model.SelectedSubjectIds != null)
            {
                foreach (var subId in model.SelectedSubjectIds)
                {
                    var subject = _ISubjectService.GetSubjectById((int)subId);
                    if (subject != null)
                        model.AvailableSelectedSubjects.Add(new SelectListItem { Selected = false, Text = subject.Subject1, Value = subject.SubjectId.ToString() });
                }
            }

            // bind view standrad with teacher
            if (model.SelectedClassIds != null)
            {
                foreach (var classId in model.SelectedClassIds)
                {
                    var claas = _ICatalogMasterService.GetStandardMasterById((int)classId);
                    if (claas != null)
                        model.AvailableSelectedClasses.Add(new SelectListItem { Selected = false, Text = claas.Standard, Value = claas.StandardId.ToString() });
                }
            }

            return model;
        }

        // check model valid or not
        public TeacherSubjectClassModel CheckTeacherSubjectClassModelErrors(TeacherSubjectClassModel model)
        {
            if (model.TeacherId == null || model.TeacherId == 0)
            {
                ModelState.AddModelError("TeacherId", "Teacher required");
            }
            if ((model.SelectedSubjectIds == null || model.SelectedSubjectIds[0] == null) && (model.SelectedClassIds == null || model.SelectedClassIds[0] == null))
            { }
            else
            {


                if (model.SelectedSubjectIds == null || model.SelectedSubjectIds[0] == null)
                {
                    ModelState.AddModelError("SelectedSubjectIds", "Please Bind Subjects with Teacher");
                }
                if (model.SelectedClassIds == null || model.SelectedClassIds[0] == null)
                {
                    ModelState.AddModelError("SelectedClassIds", "Please Bind Classes with Teacher");
                }
            }

            return model;
        }

        public ActionResult TeacherClassSubject()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "TeacherClassSubject", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new TeacherSubjectClassModel();
                model = prepareteachermodel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "TeacherClassSubject", "Save");
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult TeacherClassSubject(TeacherSubjectClassModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "TeacherClassSubject", "Add");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                model = CheckTeacherSubjectClassModelErrors(model);
                if (ModelState.IsValid)
                {
                    // Activity log type add
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    var logtypedel = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();

                    #region Save or Update Teacher Subjects

                    // delete record if already exist
                    var existingteachersubs = _IStaffService.GetAllTeacherSubjects(ref count, TeacherId: model.TeacherId);
                    foreach (var existingteachersub in existingteachersubs)
                    {
                        // stdclass Id
                        var teachSubtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "TeacherSubject").FirstOrDefault();
                        if (teachSubtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, teachSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(existingteachersub.TeacherSubjectId, teachSubtable.TableId, logtypedel.ActivityLogTypeId, user, String.Format("Delete TeacherSubject with TeacherSubjectId:{0} and SubjectId:{1}", existingteachersub.TeacherSubjectId, existingteachersub.SubjectId), Request.Browser.Browser);
                        }
                        _IStaffService.DeleteTeacherSubject(existingteachersub.TeacherSubjectId);
                    }
                    if (model.SelectedSubjectIds != null)
                        // insert teacher subjects
                        foreach (var item in model.SelectedSubjectIds)
                        {
                            // check subject exist
                            var subject = _ISubjectService.GetSubjectById(item);
                            if (subject == null)
                                continue;

                            TeacherSubject TeacherSubject = new TeacherSubject();
                            TeacherSubject.TeacherId = model.TeacherId;
                            TeacherSubject.SubjectId = item;
                            _IStaffService.InsertTeacherSubject(TeacherSubject);

                            // stdclass Id
                            var teachSubtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "TeacherSubject").FirstOrDefault();
                            if (teachSubtable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, teachSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(TeacherSubject.TeacherSubjectId, teachSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add TeacherSubject with TeacherSubjectId:{0} and SubjectId:{1}", TeacherSubject.TeacherSubjectId, TeacherSubject.SubjectId), Request.Browser.Browser);
                            }
                        }

                    #endregion

                    #region Save or Update Teacher Classes

                    // delete record if already exist
                    var existingteachclasses = _IStaffService.GetAllTeacherClasss(ref count, TeacherId: model.TeacherId);
                    foreach (var existingteachclass in existingteachclasses)
                    {
                        // stdclass Id
                        var teachclass = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "TeacherClass").FirstOrDefault();
                        if (teachclass != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, teachclass.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(existingteachclass.TeacherClassId, teachclass.TableId, logtypedel.ActivityLogTypeId, user, String.Format("Delete TeacherClass with TeacherClassId:{0} and StandardId:{1}", existingteachclass.TeacherClassId, existingteachclass.StandardId), Request.Browser.Browser);
                        }
                        _IStaffService.DeleteTeacherClass(existingteachclass.TeacherClassId);
                    }
                    if (model.SelectedClassIds != null)
                        // insert teacher Class
                        foreach (var item in model.SelectedClassIds)
                        {
                            // check subject exist
                            var standard = _ICatalogMasterService.GetStandardMasterById(item);
                            if (standard == null)
                                continue;

                            TeacherClass TeacherClass = new TeacherClass();
                            TeacherClass.TeacherId = model.TeacherId;
                            TeacherClass.StandardId = item;
                            _IStaffService.InsertTeacherClass(TeacherClass);

                            // stdclass Id
                            var teachSubtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "TeacherClass").FirstOrDefault();
                            if (teachSubtable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, teachSubtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(TeacherClass.TeacherClassId, teachSubtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add TeacherClass with TeacherClassId:{0} and SubjectId:{1}", TeacherClass.TeacherClassId, TeacherClass.StandardId), Request.Browser.Browser);
                            }
                        }

                    #endregion

                    TempData["SuccessNotification"] = "Teacher Subject&Class binding has been done successfully";
                    return RedirectToAction("TeacherClassSubject");
                }

                model = prepareteachermodel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "TeacherClassSubject", "Save");

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #region Ajax Methods

        public ActionResult GetClassesByType(string Type)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            var selectlist = new List<SelectListItem>();

            if (Type == "Regular Class")
            {
                foreach (var item in _ICatalogMasterService.GetAllClassMasters(ref count))
                    selectlist.Add(new SelectListItem { Selected = false, Text = item.Class, Value = item.ClassId.ToString() });
            }
            else
            {
                foreach (var item in _ICatalogMasterService.GetAllVirtualClasses(ref count))
                    selectlist.Add(new SelectListItem { Selected = false, Value = item.VirtualClassId.ToString(), Text = item.VirtualClass1 });
            }

            return Json(new
            {
                status = "success",
                data = selectlist
            });
        }

        public ActionResult CopyStandardSubject(string StandardId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            if (string.IsNullOrEmpty(StandardId))
                return Json(new
                {
                    status = "failed",
                    msg = "Standard should not be empty"
                });

            IList<SelectListItem> selectlist = new List<SelectListItem>();
            int StdId = Convert.ToInt32(StandardId);
            var stdsubjets = _ISubjectService.GetAllStandardSubjects(ref count, StandardId: StdId);
            var allclasses = _ICatalogMasterService.GetAllClassMasters(ref count).Where(m => m.StandardId == StdId);
            // get time table by today
            var timetables = _IClassPeriodService.GetAllTimeTables(ref count);
            var timetable = timetables.Where(t => t.EffectiveFrom <= DateTime.UtcNow.Date && t.EffectiveTill >= DateTime.UtcNow.Date).FirstOrDefault();

            foreach (var stdsub in stdsubjets)
            {
                var ClassPeriodSubject = _IClassPeriodService.GetAllClassPeriodDetails(ref count, TimeTableId: timetable.TimeTableId)
                                            .Where(c => c.ClassPeriod != null && c.ClassPeriod.ClassSubject != null
                                                && c.ClassPeriod.ClassSubject.SubjectId == stdsub.SubjectId
                                                && c.ClassPeriod.ClassSubject.ClassMaster != null
                                                && c.ClassPeriod.ClassSubject.ClassMaster.StandardId == stdsub.StandardId).FirstOrDefault();
                if (ClassPeriodSubject != null)
                    selectlist.Add(new SelectListItem { Selected = true, Disabled = true, Text = stdsub.Subject.Subject1, Value = stdsub.SubjectId.ToString() });
                else
                    selectlist.Add(new SelectListItem { Selected = true, Disabled = false, Text = stdsub.Subject.Subject1, Value = stdsub.SubjectId.ToString() });

            }
            return Json(new
            {
                status = "success",
                data = selectlist
            });
        }

        public ActionResult GetCoreSubjectsforClass(string Class, string ClassType)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            IList<SelectListItem> Coreselectlist = new List<SelectListItem>();
            IList<SelectListItem> Compulsoryselectlist = new List<SelectListItem>();
            IList<SelectListItem> Optionalselectlist = new List<SelectListItem>();
            IList<SelectListItem> Additionalselectlist = new List<SelectListItem>();
            IList<SelectListItem> practicalsublist = new List<SelectListItem>();
            IList<SelectListItem> unselectedcoresubject = new List<SelectListItem>();
            int ClassId = Convert.ToInt32(Class);

            // get subject type Compulsory
            var CompulsorySubjectType = _ISubjectService.GetAllSubjectTypes(ref count, "Compulsory").FirstOrDefault();
            // get subject type Optional
            var OptionalSubjectType = _ISubjectService.GetAllSubjectTypes(ref count, "Optional").FirstOrDefault();
            // get subject type Additional
            var AdditionalSubjectType = _ISubjectService.GetAllSubjectTypes(ref count, "Additional").FirstOrDefault();
            // get time table by today
            var timetables = _IClassPeriodService.GetAllTimeTables(ref count);
            var timetable = timetables.Where(t => t.EffectiveFrom <= DateTime.UtcNow.Date && t.EffectiveTill >= DateTime.UtcNow.Date).FirstOrDefault();
            // Get Standard is core subject 
            IList<StandardSubject> stdsublist = new List<StandardSubject>();
            int?[] stdsubjectarray = { };
            var clssubjets = new List<ClassSubject>();
            // all skill subject
            var SubjectSkills = _ISubjectService.GetAllSubjectSkills(ref count);
            if (ClassType.ToLower().Contains("regular"))
            {
                var standardId = _ISubjectService.GetAllClassSubjects(ref count, ClassId: ClassId)
                    .Select(c => c.ClassMaster.StandardId).FirstOrDefault();
                if (standardId != null)
                    stdsublist = _ISubjectService.GetAllStandardSubjects(ref count, (int)standardId);

                stdsubjectarray = stdsublist.Select(s => s.SubjectId).ToArray();
                clssubjets = _ISubjectService.GetAllClassSubjects(ref count, ClassId: ClassId);
            }
            else
            {
                var standardId = _ICatalogMasterService.GetAllVirtualClasses(ref count).Where(p => p.VirtualClassId == ClassId).FirstOrDefault().StandardId;
                if (standardId != null)
                    stdsublist = _ISubjectService.GetAllStandardSubjects(ref count, (int)standardId);

                stdsubjectarray = stdsublist.Select(s => s.SubjectId).ToArray();
                clssubjets = _ISubjectService.GetAllClassSubjects(ref count, VirtualClassId: ClassId);
            }
            foreach (var clssub in clssubjets)
            {
                bool IsInTimeTable = false;
                if (timetable != null)
                {
                    var ClassPeriodSubject = _IClassPeriodService.GetAllClassPeriodDetails(ref count, TimeTableId: timetable.TimeTableId).Where(c => c.ClassPeriod != null && c.ClassPeriod.ClassSubjectId == clssub.ClassSubjectId).FirstOrDefault();
                    if (ClassPeriodSubject != null)
                        IsInTimeTable = true;
                }

                if (clssub.IsActive == true)
                {
                    // check subject is core
                    if (clssub.Subject.IsCore == true && clssub.SkillId == null)
                    {
                        if (stdsubjectarray.Contains(clssub.SubjectId))
                        {
                            if (clssub.SubjectTypeId == CompulsorySubjectType.SubjectTypeId)
                            {
                                if (IsInTimeTable)
                                    Coreselectlist.Add(new SelectListItem { Selected = false, Disabled = true, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                                else
                                    Coreselectlist.Add(new SelectListItem { Selected = false, Disabled = false, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                            }
                        }
                        else
                        {
                            if (clssub.SubjectTypeId == CompulsorySubjectType.SubjectTypeId)
                            {
                                if (IsInTimeTable)
                                    Compulsoryselectlist.Add(new SelectListItem { Selected = false, Disabled = true, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                                else
                                    Compulsoryselectlist.Add(new SelectListItem { Selected = false, Disabled = false, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                            }
                            else if (clssub.SubjectTypeId == OptionalSubjectType.SubjectTypeId)
                            {
                                if (IsInTimeTable)
                                    Optionalselectlist.Add(new SelectListItem { Selected = false, Disabled = true, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                                else
                                    Optionalselectlist.Add(new SelectListItem { Selected = false, Disabled = false, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                            }
                            // check subject is Additional
                            else if (clssub.SubjectTypeId == AdditionalSubjectType.SubjectTypeId && clssub.SkillId == null)
                            {
                                if (IsInTimeTable)
                                    Additionalselectlist.Add(new SelectListItem { Selected = false, Disabled = true, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                                else
                                    Additionalselectlist.Add(new SelectListItem { Selected = false, Disabled = false, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                            }
                        }
                    }
                    else if ((clssub.Subject.IsCore == null || clssub.Subject.IsCore == false) && clssub.SkillId == null)
                    {
                        if (clssub.SubjectTypeId == CompulsorySubjectType.SubjectTypeId)
                        {
                            if (IsInTimeTable)
                                Compulsoryselectlist.Add(new SelectListItem { Selected = false, Disabled = true, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                            else
                                Compulsoryselectlist.Add(new SelectListItem { Selected = false, Disabled = false, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                        }
                        // check subject is Optional
                        else if (clssub.SubjectTypeId == OptionalSubjectType.SubjectTypeId)
                        {
                            if (IsInTimeTable)
                                Optionalselectlist.Add(new SelectListItem { Selected = false, Disabled = true, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                            else
                                Optionalselectlist.Add(new SelectListItem { Selected = false, Disabled = false, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                        }
                        // check subject is Additional
                        else if (clssub.SubjectTypeId == AdditionalSubjectType.SubjectTypeId && clssub.SkillId == null)
                        {
                            if (IsInTimeTable)
                                Additionalselectlist.Add(new SelectListItem { Selected = false, Disabled = true, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                            else
                                Additionalselectlist.Add(new SelectListItem { Selected = false, Disabled = false, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                        }
                    }
                    else
                    {
                        var SubjectSkill = SubjectSkills.Where(s => s.SubjectId == clssub.SubjectId && s.SkillId == clssub.SkillId).FirstOrDefault();
                        if (SubjectSkill != null)
                        {
                            if (IsInTimeTable)
                                practicalsublist.Add(new SelectListItem { Selected = false, Disabled = true, Text = SubjectSkill.SubjectSkill1, Value = SubjectSkill.SubjectSkillId.ToString() });
                            else
                                practicalsublist.Add(new SelectListItem { Selected = false, Disabled = false, Text = SubjectSkill.SubjectSkill1, Value = SubjectSkill.SubjectSkillId.ToString() });
                        }
                    }
                }
                else
                {
                    if (IsInTimeTable)
                        unselectedcoresubject.Add(new SelectListItem { Selected = false, Disabled = true, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                    else
                        unselectedcoresubject.Add(new SelectListItem { Selected = false, Disabled = false, Text = clssub.Subject.Subject1, Value = clssub.SubjectId.ToString() });
                }
            }

            // set unselected core subject 
            var subjects = _ISubjectService.GetAllSubjects(ref count, IsCore: true);
            var classsubarray = clssubjets.Select(c => c.SubjectId).ToArray();
            foreach (var sub in subjects)
            {
                if (!classsubarray.Contains(sub.SubjectId))
                    unselectedcoresubject.Add(new SelectListItem { Selected = false, Text = sub.Subject1, Value = sub.SubjectId.ToString() });
            }

            return Json(new
            {
                status = "success",
                corelist = Coreselectlist,
                complist = Compulsoryselectlist,
                Optlist = Optionalselectlist,
                Addlist = Additionalselectlist,
                unselectedcorelist = unselectedcoresubject,
                practicalsublist = practicalsublist
            });
        }

        public ActionResult GetSubjectsClassForTeacher(string Teacher)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            if (string.IsNullOrEmpty(Teacher))
                return Json(new
                {
                    status = "failed",
                    msg = "Teacher should not be empty"
                });

            IList<SelectListItem> Subjectselectlist = new List<SelectListItem>();
            IList<SelectListItem> Classselectlist = new List<SelectListItem>();
            int TeacherId = Convert.ToInt32(Teacher);
            // get time table by today
            var timetables = _IClassPeriodService.GetAllTimeTables(ref count);
            var timetable = timetables.Where(t => t.EffectiveFrom <= DateTime.UtcNow.Date && t.EffectiveTill >= DateTime.UtcNow.Date).FirstOrDefault();
            // Selected Subjects
            var teachersubjets = _IStaffService.GetAllTeacherSubjects(ref count, TeacherId: TeacherId);
            foreach (var teachersub in teachersubjets)
            {
                var classperioddetail = _IClassPeriodService.GetAllClassPeriodDetails(ref count, TeacherId: TeacherId);
                if (classperioddetail.Count == 0)
                    Subjectselectlist.Add(new SelectListItem { Selected = false, Disabled = false, Text = teachersub.Subject.Subject1, Value = teachersub.SubjectId.ToString() });
                else
                {
                    if (timetable != null)
                    {
                        var subjectIds = classperioddetail.Where(c => c.TimeTableId == timetable.TimeTableId).Select(c => c.ClassPeriod.ClassSubject.SubjectId).Distinct().ToArray();
                        if (!subjectIds.Contains(teachersub.SubjectId))
                        {
                            var element = Subjectselectlist.Where(ts => ts.Value == teachersub.SubjectId.ToString()).FirstOrDefault();
                            if (element == null)
                                Subjectselectlist.Add(new SelectListItem { Selected = false, Disabled = false, Text = teachersub.Subject.Subject1, Value = teachersub.SubjectId.ToString() });
                        }
                        else
                        {
                            var element = Subjectselectlist.Where(ts => ts.Value == teachersub.SubjectId.ToString()).FirstOrDefault();
                            if (element == null)
                                Subjectselectlist.Add(new SelectListItem { Selected = false, Disabled = true, Text = teachersub.Subject.Subject1, Value = teachersub.SubjectId.ToString() });
                        }
                    }
                    else
                    {
                        var element = Subjectselectlist.Where(ts => ts.Value == teachersub.SubjectId.ToString()).FirstOrDefault();
                        if (element == null)
                            Subjectselectlist.Add(new SelectListItem { Selected = false, Disabled = false, Text = teachersub.Subject.Subject1, Value = teachersub.SubjectId.ToString() });
                    }
                }
            }

            // Select classes
            var teacherclassess = _IStaffService.GetAllTeacherClasss(ref count, TeacherId: TeacherId);
            foreach (var teacherclass in teacherclassess)
            {
                var classperioddetail = _IClassPeriodService.GetAllClassPeriodDetails(ref count, TeacherId: TeacherId);
                if (classperioddetail.Count == 0)
                    Classselectlist.Add(new SelectListItem { Selected = true, Disabled = false, Text = teacherclass.StandardMaster.Standard, Value = teacherclass.StandardId.ToString() });
                else
                {
                    if (timetable != null)
                    {
                        var standardIds = classperioddetail.Where(c => c.TimeTableId == timetable.TimeTableId).Select(c => c.ClassPeriod.ClassSubject.ClassMaster != null ? c.ClassPeriod.ClassSubject.ClassMaster.StandardId : c.ClassPeriod.ClassSubject.VirtualClass.StandardId).Distinct().ToArray();
                        if (!standardIds.Contains(teacherclass.StandardId))
                        {
                            var element = Classselectlist.Where(ts => ts.Value == teacherclass.StandardId.ToString()).FirstOrDefault();
                            if (element == null)
                                Classselectlist.Add(new SelectListItem { Selected = true, Disabled = false, Text = teacherclass.StandardMaster.Standard, Value = teacherclass.StandardId.ToString() });
                        }
                        else
                        {
                            var element = Classselectlist.Where(ts => ts.Value == teacherclass.StandardId.ToString()).FirstOrDefault();
                            if (element == null)
                                Classselectlist.Add(new SelectListItem { Selected = true, Disabled = true, Text = teacherclass.StandardMaster.Standard, Value = teacherclass.StandardId.ToString() });
                        }
                    }
                    else
                    {
                        var element = Classselectlist.Where(ts => ts.Value == teacherclass.StandardId.ToString()).FirstOrDefault();
                        if (element == null)
                            Classselectlist.Add(new SelectListItem { Selected = true, Disabled = false, Text = teacherclass.StandardMaster.Standard, Value = teacherclass.StandardId.ToString() });
                    }
                }
            }

            // teacher image src
            string imgsrc = string.Empty;
            var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
            var doctype = _IStaffService.GetAllStaffDocumentTypes(ref count, "Image").FirstOrDefault();
            var teacherimage = _IStaffService.GetAllStaffDocuments(ref count, doctype.DocumentTypeId, StaffId: TeacherId).FirstOrDefault();
            if (teacherimage != null)
                imgsrc = _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/Teacher/Photos/" + teacherimage.Image;
            else
                imgsrc = _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/default.png";

            return Json(new
            {
                status = "success",
                Sublist = Subjectselectlist,
                Clslist = Classselectlist,
                Image = imgsrc
            });
        }

        #endregion

        #region TeacheSubjectClassAssignment
        // check model valid or not
        public string CheckTeacherAssignmentErrors(TeacherClassSubAssignmentModel model)
        {
            StringBuilder errorMsg = new StringBuilder();
            if (model.TeacherId == null && model.TeacherId == 0)
                errorMsg.Append("Please select Teacher ε");
            if (model.ClassId == 0 && model.VirtualId == 0)
                errorMsg.Append("Please select Class ε");
            if (model.SubjectId == null && model.SubjectId == 0)
                errorMsg.Append("Please select Subject ε");
            if (model.TeacherId != null && model.ClassId != null && model.SubjectId != null)
            {
                var getDetail = _IStaffService.GetAllTeacherSubjectAssigns(ref count, TeacherId: model.TeacherId,
                    ClassId: model.ClassId, SubjectId: model.SubjectId,VirtualId: model.VirtualId).ToList();

                if (getDetail.Count > 0)
                {
                    errorMsg.Append("Cannot insert duplicate Record ε");
                }
            }
            return errorMsg.ToString();
        }

        public ActionResult TeacherSubjectAssignment()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "TeacherClassSubject", "View");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new TeacherClassSubAssignmentModel();
                model.AvailableTeachers = _IDropDownService.TeacheringStaffList().OrderByDescending(x=>x.Text).ToList();
                model.AvailableSubjects.Add(new SelectListItem { Text = "--Select--", Value = "" });
                model.AvailableClasses.Add(new SelectListItem { Text = "--Select--", Value = "" });
                model.IsUpdate = false;

                //paging
                model.gridPageSizes = _ISchoolSettingService.GetorSetSchoolData("gridPageSizes", "20,50,100,250").AttributeValue;
                model.defaultGridPageSize = _ISchoolSettingService.GetorSetSchoolData("defaultGridPageSize", "20").AttributeValue;
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult GetTeacherSubjectAssignments(DataSourceRequest command, TeacherClassSubAssignmentModel SubAssignModel, 
                                                        IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            var getAssignments = _IStaffService.GetAllTeacherSubjectAssigns(ref count, TeacherId: SubAssignModel.TeacherId,
                                    ClassId: SubAssignModel.ClassId,VirtualId: SubAssignModel.VirtualId,
                                    SubjectId: SubAssignModel.SubjectId);

            var TeacherAssignedData = getAssignments.Select(x =>
            {
                var model = new TeacherClassSubAssignmentModel();
                model.EncTeacherSubjectAssignId = _ICustomEncryption.base64e(x.TeacherSubjectAssignId.ToString());

                string ClassVritual_Id = "";
                if (x.ClassId != null)
                    ClassVritual_Id += _ICustomEncryption.base64e(x.ClassId.ToString());
                else
                    ClassVritual_Id += "0";

                if (x.VirtualClassId != null)
                    ClassVritual_Id += "-" + _ICustomEncryption.base64e(x.ClassId.ToString());
                else
                    ClassVritual_Id += "-"+"0";

                model.EncClassIdVirtualId = ClassVritual_Id;
                model.EncTeacherId = _ICustomEncryption.base64e(x.TeacherId.ToString());
                model.EncSubjectId = _ICustomEncryption.base64e(x.SubjectId.ToString());
               
                if(x.ClassId != null)
                     model.Class = x.ClassMaster != null ? x.ClassMaster.Class : "";
                else
                    model.Class = x.VirtualClass != null ? x.VirtualClass.VirtualClass1 : "";
               
                model.Subject = x.Subject != null ? x.Subject.Subject1 : "";
                string StaffName = "";
                if (x.Staff != null)
                    StaffName = _ICommonMethodService.TrimStaffName(x.Staff, StaffName);

                model.Teacher = StaffName;
             

                return model;
            }).AsQueryable().Sort(sort);

            var gridModel = new DataSourceResult
            {
                Data = TeacherAssignedData.PagedForCommand(command).ToList(),
                Total = TeacherAssignedData.Count()
            };
            return Json(gridModel);
        }
        [HttpGet]
        public ActionResult getAssignedClassFromStandard(string TeacherId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "TeacherClassSubject", "Add");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var ClassList = new List<CustomSelectClassitem>();
                int Teacher_Id = 0;
                if(!string.IsNullOrEmpty(TeacherId))
                {
                    Teacher_Id = Convert.ToInt32(TeacherId);
                    var getstandard_Ids = _IStaffService.GetAllTeacherClasss(ref count, TeacherId: Teacher_Id)
                                                        .Select(x=>x.StandardId).ToArray();
                    var NormalClass_Ids = _ICatalogMasterService.GetAllClassMasters(ref count)
                                        .Where(x => getstandard_Ids.Contains(x.StandardId)).Select(x=>x.ClassId).ToArray();

                    var getTeacherSubjects = _IStaffService.GetAllTeacherSubjects(ref count, TeacherId: (int)Teacher_Id)
                                                   .Select(x => x.SubjectId).ToArray();

                    var NormalClasses = _ISubjectService.GetAllClassSubjects(ref count, IsActive: true)
                                        .Where(x =>x.ClassId != null && NormalClass_Ids.Contains((int)x.ClassId)
                                            && getTeacherSubjects.Contains(x.SubjectId))
                                            .Select(x => x.ClassMaster).Distinct().ToList();

                    foreach(var cls in NormalClasses)
                    {
                        ClassList.Add(new CustomSelectClassitem()
                        {
                            Text = cls.Class,
                            Value = cls.ClassId.ToString(),
                            VirtualClassValue = "0"
                        });
                    }

                    var VirtualClass_Ids = _ICatalogMasterService.GetAllVirtualClasses(ref count)
                                            .Where(x => getstandard_Ids.Contains(x.StandardId))
                                            .Select(x=>x.VirtualClassId).ToArray();

                    var VirtualClasses = _ISubjectService.GetAllClassSubjects(ref count, IsActive: true)
                                          .Where(x =>x.VirtualClassId != null && VirtualClass_Ids.Contains((int)x.VirtualClassId)
                                                 && getTeacherSubjects.Contains(x.SubjectId)).Distinct()
                                          .Select(x => x.VirtualClass).Distinct().ToList();

                    foreach (var cls in VirtualClasses)
                    {
                        ClassList.Add(new CustomSelectClassitem()
                        {
                            Text = cls.VirtualClass1,
                            Value = "0",
                            VirtualClassValue = cls.VirtualClassId.ToString()
                        });
                    }
                }
                return Json(new { status="success",ClassList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpGet]
        public ActionResult getAssignedSubjectFromTeacherClass(string TeacherId, string ClassId, string VirtualId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "TeacherClassSubject", "Add");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var subjectList = new List<SelectListItem>();
                
                int Teacher_Id = 0, Class_Id = 0, Virtual_Id =0;
                if (!string.IsNullOrEmpty(ClassId))
                    Class_Id = Convert.ToInt32(ClassId);

                if (!string.IsNullOrEmpty(VirtualId))
                    Virtual_Id = Convert.ToInt32(VirtualId);

                if (!string.IsNullOrEmpty(TeacherId))
                {
                    Teacher_Id = Convert.ToInt32(TeacherId);
                    var getTeacherSubjects = _IStaffService.GetAllTeacherSubjects(ref count, TeacherId: (int)Teacher_Id)
                                                     .Select(x => x.SubjectId).ToArray();

                    var getClassSubjects = new List<Subject>();
                    if(Class_Id > 0)
                    {
                        //for class 
                        getClassSubjects = _ISubjectService.GetAllClassSubjects(ref count, IsActive: true)
                                                .Where(x =>x.ClassId != null && x.ClassId == Class_Id 
                                                            && getTeacherSubjects.Contains(x.SubjectId))
                                                .Select(x => x.Subject).Distinct().ToList();
                    }
                    else
                    {
                        var getVirtualClassSubjects = _ISubjectService.GetAllClassSubjects(ref count, IsActive: true,
                                                      VirtualClassId: Virtual_Id).Where(x => getTeacherSubjects.Contains(x.SubjectId))
                                                     .Select(x => x.Subject).Distinct().ToList();
                        //virtual Class
                        getClassSubjects = getVirtualClassSubjects;
                    }

                    foreach (var sub in getClassSubjects)
                    {
                        subjectList.Add(new SelectListItem()
                        {
                            Text = sub.Subject1,
                            Value = sub.SubjectId.ToString(),
                        });
                    }
                }
                return Json(new { status = "success", subjectList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult InsertUpdateTeacherSubAssignment(TeacherClassSubAssignmentModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "TeacherClassSubject", "Add");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var ErrorMsg = CheckTeacherAssignmentErrors(model);
                if (string.IsNullOrEmpty(ErrorMsg))
                {
                    int? Class_Id = null;
                    if (model.ClassId > 0)
                        Class_Id = model.ClassId;


                    int? Subject_Id = null;
                    if (model.SubjectId > 0)
                        Subject_Id = model.SubjectId;

                    int? Virtual_Id = null;
                    if (model.VirtualId > 0)
                        Virtual_Id = model.VirtualId;


                    if(model.TeacherSubjectAssignId == 0)
                    {
                        //insertion
                        TeacherSubjectAssign SubAssign = new TeacherSubjectAssign();
                        SubAssign.TeacherId = model.TeacherId;
                        SubAssign.ClassId = Class_Id;
                        SubAssign.SubjectId = Subject_Id;
                        SubAssign.VirtualClassId = Virtual_Id;
                        _IStaffService.InsertTeacherSubjectAssign(SubAssign);
                    }
                    else
                    {
                        //updation
                        var getSubAssign = _IStaffService.GetTeacherSubjectAssignById(model.TeacherSubjectAssignId);
                        if (getSubAssign != null)
                        {
                            getSubAssign.ClassId = Class_Id;
                            getSubAssign.SubjectId = Subject_Id;
                            getSubAssign.VirtualClassId = Virtual_Id;
                            _IStaffService.UpdateTeacherSubjectAssign(getSubAssign);
                        }
                    }
                    return Json(new { status = "success" });
                }
                else
                {
                    return Json(new { status = "failed", data = ErrorMsg });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        #region StudentClassSubjects
        public StudentClassSubjectModel PrepareStudentClassSub(StudentClassSubjectModel model)
        {
            //paging
            model.gridPageSizes = _ISchoolSettingService.GetorSetSchoolData("gridPageSizes", "20,50,100,250").AttributeValue;
            model.defaultGridPageSize = _ISchoolSettingService.GetorSetSchoolData("defaultGridPageSize", "20").AttributeValue;

            model.AvaiableStuSessions = _IDropDownService.SessionList();

            var currentSession = _ICatalogMasterService.GetCurrentSession();
            model.SessionId = Convert.ToString(currentSession.SessionId);

            var ClassHasStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: currentSession.SessionId)
                                    .Distinct().OrderBy(x => x.ClassMaster.Class).ToList();

            model.AvaiableClasses.Add(new SelectListItem
            {
                Text = "--Select--",
                Value = "",
                Selected = false
            });

            var studentStatusDetail = _IStudentMasterService.GetAllStudentStatusDetail(ref count);
            foreach (var clas in ClassHasStudents)
            {
                var IsStudentActive = studentStatusDetail.Where(p => p.StudentId == clas.StudentId)
                                                        .OrderByDescending(p => p.StatusChangeDate)
                                                        .ThenByDescending(p => p.StudentStatusDetailId).ToList();
                if (IsStudentActive.Count() > 0)
                {
                    if (IsStudentActive.FirstOrDefault().StudentStatu.StudentStatus == "Left")
                        continue;
                    else
                    {
                        var getclass = _ICatalogMasterService.GetClassMasterById((int)clas.ClassId);
                        if (getclass != null)
                        {
                            var AlreadyExists = model.AvaiableClasses.Where(x => x.Value == clas.ClassId.ToString()).FirstOrDefault();
                            if (AlreadyExists == null)
                            {
                                model.AvaiableClasses.Add(new SelectListItem
                                {
                                    Text = getclass.Class,
                                    Value = getclass.ClassId.ToString(),
                                    Selected = false
                                });
                            }
                        }
                    }
                }
            }
            return model;
        }

        public ActionResult StudentListbyClass(string Class, string Id, bool CurrentSession, string Session_id = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }


            if (string.IsNullOrEmpty(Class))
                return Json(new
                {
                    status = "failed",
                    msg = "Class should not be empty"
                });

            int classid = Convert.ToInt32(Class);
            // current session
            var Session = new Session();
            if (!string.IsNullOrEmpty(Session_id))
                Session = _ICatalogMasterService.GetSessionById(Convert.ToInt32(Session_id));
            else
                Session = _ICatalogMasterService.GetCurrentSession();

            var studentlist = _IStudentService.StudentListbyClassId(classid, 0, Session.SessionId, AdmnStatus: true);
            var studentStatusDetail = _IStudentMasterService.GetAllStudentStatusDetail(ref count);

            IList<SelectListItem> StudentDDL = new List<SelectListItem>();
            foreach (var item in studentlist)
            {
                if (string.IsNullOrEmpty(item.Value))
                {
                    StudentDDL.Add(item);
                    continue;
                }

                var IsStudentActive = studentStatusDetail.Where(p => p.StudentId == Convert.ToInt32(item.Value)).OrderByDescending(p => p.StatusChangeDate).ThenByDescending(p => p.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus;
                if (IsStudentActive == "Left")
                    continue;
                else
                    StudentDDL.Add(item);
            }

            return Json(new
            {
                status = "success",
                data = StudentDDL
            });

        }

        public ActionResult ManageStudentClassSubject(StudentClassSubjectModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");

                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "ManageStudentClassSubject", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                model = PrepareStudentClassSub(model);
                //model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "StandardSubject", "Save");
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult getKendoGridColumns(StudentClassSubjectModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                List<SelectListItem> SubjectsList = new List<SelectListItem>();
                List<StudentProfileModel> StudentInfo = new List<StudentProfileModel>();
                List<StudentClassesModel> SubjectClassList = new List<StudentClassesModel>();

                int session_Id = 0;
                if (!string.IsNullOrEmpty(model.SessionId))
                    session_Id = Convert.ToInt32(model.SessionId);

                int Class_Id = 0;
                if (!string.IsNullOrEmpty(model.ClassId))
                    Class_Id = Convert.ToInt32(model.ClassId);

                //int Student_Id = 0;
                //if (!string.IsNullOrEmpty(model.StudentId))
                //    Student_Id = Convert.ToInt32(model.StudentId);

                var studentStatusDetail = _IStudentMasterService.GetAllStudentStatusDetail(ref count);
                var Students = _IStudentService.GetAllStudentClasss(ref count, SessionId: session_Id,
                                                ClassId: Class_Id).Select(x => x.Student).ToList();
                var StudentClassList=_IStudentService.GetAllStudentClasss(ref count);

                var ClassSubjectsList=_ISubjectService.GetAllClassSubjects(ref count);

                var studentClassSubjectsList = _ISubjectService.GetAllStudentClassSubjects(ref count);

               var subjectskills= _ISubjectService.GetAllSubjectSkills(ref count);
                foreach (var stu in Students)
                {

                    var StudentRollNo = StudentClassList.Where(m=>m.SessionId== session_Id && 
                                                                           m.ClassId== Class_Id && m.StudentId== stu.StudentId);

                    var IsStudentActive = studentStatusDetail.Where(p => p.StudentId == stu.StudentId)
                                                        .OrderByDescending(p => p.StatusChangeDate)
                                                        .ThenByDescending(p => p.StudentStatusDetailId).ToList();

                    if (IsStudentActive.Count() > 0)
                    {
                        if (IsStudentActive.FirstOrDefault().StudentStatu.StudentStatus == "Left")
                            continue;
                        else
                        {
                            string StudentName = "";
                            if (!string.IsNullOrEmpty(stu.FName))
                                stu.FName = stu.FName.Trim();

                            if (!string.IsNullOrEmpty(stu.LName))
                                stu.LName = stu.LName.Trim();



                            StudentName = stu.FName + " " + stu.LName;
                            StudentInfo.Add(new StudentProfileModel
                            {
                                StudentName = StudentName,
                                StudentId = stu.StudentId,
                                CurrentClassRollno = (!string.IsNullOrEmpty(StudentRollNo.FirstOrDefault().RollNo)) ? StudentRollNo.FirstOrDefault().RollNo : "",
                                StuClassId = StudentRollNo.FirstOrDefault().StudentClassId.ToString()
                            });
                        }


                        foreach (var subjects in ClassSubjectsList.Where(m=>m.ClassId== Class_Id &&  m.IsActive== true).ToList())
                        {
                            var selectedStuClassSubs = studentClassSubjectsList.Where(m=>m.ClassSubjectId== subjects.ClassSubjectId &&
                                                            m.StudentClassId== StudentRollNo.FirstOrDefault().StudentClassId).FirstOrDefault();

                            string Val = "";
                            if (selectedStuClassSubs != null)
                                Val = subjects.ClassSubjectId.ToString() + "_" + StudentRollNo.FirstOrDefault().StudentClassId.ToString();
                            else
                                Val = subjects.ClassSubjectId.ToString() + "_" + StudentRollNo.FirstOrDefault().StudentClassId.ToString();


                            string SubCode = "";
                            var getSKillSubjects = ClassSubjectsList.Where(m=>m.ClassSubjectId==subjects.ClassSubjectId).FirstOrDefault();
                            if (getSKillSubjects != null)
                            {
                                if (getSKillSubjects.SkillId != null)
                                {
                                    var getSKill = subjectskills.Where(m=>m.SkillId ==(int)getSKillSubjects.SkillId &&
                                                                m.SubjectId== subjects.Subject.SubjectId).FirstOrDefault();
                                    if (getSKill != null)
                                    {
                                        SubCode = getSKill.SubjectSkill1;
                                    }
                                    else
                                        SubCode = subjects.Subject.SubjectCode;
                                }
                                else
                                    SubCode = subjects.Subject.SubjectCode;
                            }


                            SubjectsList.Add(new SelectListItem
                            {
                                Text = SubCode,
                                Value = Val,
                                Selected = selectedStuClassSubs != null ? true : false
                            });
                            //}
                            var AlreadyExistsHeader = SubjectClassList.Where(x => x.SubjectCode == SubCode).ToList();
                            if (AlreadyExistsHeader.Count() == 0)
                            {
                                SubjectClassList.Add(new StudentClassesModel
                                {
                                    SubjectCode = SubCode,
                                    ClassSubjectId = subjects.ClassSubjectId.ToString(),
                                    StudentClassId = StudentRollNo.FirstOrDefault().StudentClassId.ToString(),
                                    Selected = selectedStuClassSubs != null ? true : false
                                });
                            }
                        }
                    }
                }

                return Json(new { SubjectsList, StudentInfo, SubjectClassList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult InsertUpdateStudentClassSubs(StudentClassSubjectModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Subject", "TeacherClassSubject", "Add");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                string StudentClassId = "";
                string Sub_Id = "";

                KSModel.Models.StudentClassSubject objclassSubs = new KSModel.Models.StudentClassSubject();
                System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<StudentClassSubjectModel> DataArray = js.Deserialize<List<StudentClassSubjectModel>>(model.arrayIds);
                var StudentClassSubjectList=_ISubjectService.GetAllStudentClassSubjects(ref count);
                var list = new List<StudentClassSubject>();
                var listtoadd = new List<StudentClassSubject>();
                if (DataArray.Count() > 0)
                {
                    foreach (var subs in DataArray)
                    {
                        int StuClass_Id = Convert.ToInt32(subs.StudentClassId);
                        var existingData = StudentClassSubjectList.Where(m=>m.StudentClassId==(int)StuClass_Id);
                        //if some exiting values then delete them first before inserting new ones
                        if (existingData.Count() > 0)
                        {
                            list.AddRange(existingData.ToList());
                            //_ISubjectService.DeleteStudentClassSubjectThroStuClassId((int)StuClass_Id);
                        }

                        if (subs.ClassSubjectId.Count() > 0)
                        {
                            foreach (var ids in subs.ClassSubjectId.Trim(',').Split(','))
                            {
                                int ClassSubId = Convert.ToInt32(ids);
                                if (ClassSubId > 0)
                                {
                                    objclassSubs = new KSModel.Models.StudentClassSubject();
                                    objclassSubs.StudentClassId = StuClass_Id;
                                    objclassSubs.ClassSubjectId = ClassSubId;
                                    listtoadd.Add(objclassSubs);
                                    //_ISubjectService.InsertStudentClassSubject(objclassSubs);
                                }
                            }
                        }
                    }

                    _ISubjectService.DeleteStudentClassSubjectsList(list);
                    _ISubjectService.AddStudentClassSubjectsList(listtoadd);
                    //TempData["successStuClassSubsMsgs"] = "Inserted Successfully";
                    return Json(new
                    {
                        data = "success",
                        msg = "Saved Successfully"
                    });
                }
                else
                {
                    return Json(new
                    {
                        data = "error",
                        msg = "Error Happended"
                    });
                }
                //TempData["ErrorStuClassSubsMsgs"] = "Error Happended";

                //return RedirectToAction("ManageStudentClassSubject");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                TempData["ErrorStuClassSubsMsgs"] = "Error Happended";
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult GetStudentAssignedSubjects(string Student,string Class)
        {
            int Student_id = 0;
            int Class_id = 0;
            var IsSubjectAssigned = false;
            if (!string.IsNullOrEmpty(Student) && !string.IsNullOrEmpty(Class))
            {
                int.TryParse(_ICustomEncryption.base64d(Student), out Student_id);
                int.TryParse(_ICustomEncryption.base64d(Class), out Class_id);

                if (Class_id > 0 && Student_id > 0)
             {
                var StudentClass = _IStudentService.GetAllStudentClasss(ref count).Where(f => f.StudentId == Student_id && f.ClassId == Class_id).FirstOrDefault();
                if (StudentClass != null)
                {
                    var studentSUbjects = _ISubjectService.GetAllStudentClassSubjects(ref count).Where(f => f.StudentClassId == StudentClass.StudentClassId).Select(f => f.ClassSubjectId).ToArray();
                    if (studentSUbjects != null && studentSUbjects.Count() > 0)
                    {
                        var ClassSubjects = _ISubjectService.GetAllClassSubjects(ref count).Where(f => studentSUbjects.Contains(f.ClassSubjectId) && f.IsActive == true);
                            if (ClassSubjects.Count() > 0)
                            {
                                IsSubjectAssigned = true;
                                return Json(new
                                {
                                    status = "success",
                                    IsSubjectAssigned= IsSubjectAssigned
                                }, JsonRequestBehavior.AllowGet) ;
                            }
                            IsSubjectAssigned = true;
                            return Json(new
                            {
                                status = "success",
                                IsSubjectAssigned = IsSubjectAssigned
                            }, JsonRequestBehavior.AllowGet);
                        }
                        return Json(new
                        {
                            status = "success",
                            IsSubjectAssigned = IsSubjectAssigned
                        }, JsonRequestBehavior.AllowGet);
                    }
            }
            }
            return Json(new
            {
                status = "error",
                error="Invalid Student",
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}