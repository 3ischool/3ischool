﻿using BAL.BAL.Common;
using BAL.BAL.Message;
using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.ClassPeriodModule;
using DAL.DAL.Common;
using DAL.DAL.ContactModule;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.FeeModule;
using DAL.DAL.GuardianModule;
using DAL.DAL.HomeWorkModule;
using DAL.DAL.Kendo;
using DAL.DAL.LateFeeModule;
using DAL.DAL.SettingService;
using DAL.DAL.StaffModule;
using DAL.DAL.StudentModule;
using DAL.DAL.TriggerEventModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.HomeWork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using DAL.DAL.Message;

namespace KS_SmartChild.Controllers
{
    public class HomeWorkController : Controller
    {
        #region fields

        public readonly int maxFileSize = 1024;
        public int count = 0;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ILogService _Logger;
        private readonly IWebHelper _WebHelper;
        private readonly IFeeService _IFeeService;
        private readonly ILateFeeService _ILateFeeService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStudentService _IStudentService;
        private readonly IStudentAddOnService _IStudentAddOnService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IGuardianService _IGuardianService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly IContactService _IContactService;
        private readonly IStudentMasterService _IStudentMasterService;
        private readonly IClassPeriodService _IClassPeriodService;
        private readonly ISubjectService _ISubjectService;
        private readonly IHomeWorkService _IHomeWorkService;
        private readonly IStaffService _IStaffService;
        private readonly ITriggerEventService _ITriggerEventService;
        private readonly IWorkflowMessageService _IWorkflowMessageService;
        private readonly ISMSSender _ISMSSender;
        private readonly ICommonMethodService _ICommonMethodService;
        #endregion

        #region ctor

        public HomeWorkController(IUserService IUserService,
            IConnectionService IConnectionService,
            IDropDownService IDropDownService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            ILogService Logger,
            IWebHelper WebHelper,
            IFeeService IFeeService,
            ILateFeeService ILateFeeService,
            ISchoolSettingService ISchoolSettingService,
            ICatalogMasterService ICatalogMasterService,
            IStudentService IStudentService,
            IStudentAddOnService IStudentAddOnService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption,
            IGuardianService IGuardianService,
            IAddressMasterService IAddressMasterService,
            IContactService IContactService,
            IStudentMasterService IStudentMasterService,
            IClassPeriodService IClassPeriodService,
            ISubjectService ISubjectService,
            IHomeWorkService IHomeWorkService, IStaffService IStaffService
            , ITriggerEventService TriggerEventService
            , IWorkflowMessageService IWorkflowMessageService,
             ISMSSender ISMSSender, ICommonMethodService ICommonMethodService)
        {
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._Logger = Logger;
            this._WebHelper = WebHelper;
            this._IFeeService = IFeeService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IStudentService = IStudentService;
            this._IStudentAddOnService = IStudentAddOnService;
            this._ILateFeeService = ILateFeeService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._IGuardianService = IGuardianService;
            this._IAddressMasterService = IAddressMasterService;
            this._IContactService = IContactService;
            this._IStudentMasterService = IStudentMasterService;
            this._IClassPeriodService = IClassPeriodService;
            this._ISubjectService = ISubjectService;
            this._IHomeWorkService = IHomeWorkService;
            this._IStaffService = IStaffService;
            this._ITriggerEventService = TriggerEventService;
            this._IWorkflowMessageService = IWorkflowMessageService;
            this._ISMSSender = ISMSSender;
            this._ICommonMethodService = ICommonMethodService;
        }

        #endregion

        #region utility

        public static List<DateTime> GetDates(int year, int month)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(year, month))  // Days: 1, 2 ... 31 etc.
                             .Select(day => new DateTime(year, month, day)) // Map each day to a date
                             .ToList(); // Load dates into a list
        }

        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }
        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }
        // Generate a random password  
        public string Randomno()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(4, true));
            builder.Append(RandomString(2, false));
            return builder.ToString();
        }
        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        #endregion

        #region Methods

        public HomeWorkModel prepareHomeWork(HomeWorkModel model, SchoolUser user)
        {
            // prepare model 
            // Drop Down Lists
            model.AvailableHomwWorkType = _IDropDownService.HomeWorkTypeList();
            model.Date = ConvertDate(DateTime.Now);
            var selectedDay = DateTime.Now.DayOfWeek;
            var day = _IClassPeriodService.GetAllDayMasters(ref count).Where(m => m.DayName == selectedDay.ToString()).FirstOrDefault();
            var timetables = _IClassPeriodService.GetAllTimeTables(ref count).ToList();
            var timetable = timetables.Where(t => t.EffectiveFrom <= DateTime.Now.Date && t.EffectiveTill >= DateTime.Now.Date).FirstOrDefault();
            int InchargedClass = 0;
            string UserRole = "";
            UserRole = _ICommonMethodService.GetUserSuperRole(UserRole, user);

            bool linkedTimetable = false;
            var TeacherHWLinkedTimetable = _ISchoolSettingService.GetorSetSchoolData("TeacherHWLinkedTimetable", "true", null,
                                                    "In Homework for filter class").AttributeValue;
            if (TeacherHWLinkedTimetable == "true")
                linkedTimetable = true;

            var ClassPeriodDetailIdarray = new int[] { };
            var classidlist = new List<int?>();
            var Virtualclassidlist = new List<int?>();
            var classsubjectidlist = new List<int>();

            var AllowHWDescription = _ISchoolSettingService.GetorSetSchoolData("EnableHomeWorkDescription", "1").AttributeValue;
            model.IsAllowHWDescription = true;
            if (AllowHWDescription == "0")
                model.IsAllowHWDescription = false;

            if (UserRole.ToLower() != "admin")
            {
                if (linkedTimetable)
                {
                    #region //if timetable linked then check that is teacher have any timetable
                    if (timetable != null)
                    {
                        // get all class period detail by time table andt  teacher
                        var classperioddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count, TeacherId: user.UserContactId, TimeTableId: timetable.TimeTableId).ToList();
                        if (classperioddetails.Count > 0)
                        {
                            //var classperiodday = _IClassPeriodService.GetAllClassPeriodDays(ref count).Where(m => m.DayId == day.DayId).ToList();

                            //ClassPeriodDetailIdarray = classperiodday.Count > 0 ? classperiodday.Select(m => (int)m.ClassPeriodDetailId).ToArray() : new int[] { };

                            //ClassPeriodDetailIdarray = classperioddetails.Where(m => ClassPeriodDetailIdarray.Contains((int)m.ClassPeriodDetailId)).Select(m => m.ClassPeriodDetailId).ToArray();
                            ClassPeriodDetailIdarray = classperioddetails.Select(m => m.ClassPeriodDetailId).ToArray();
                            //var adjustmentclasssprddetails = _IClassPeriodService.GetAllTimeTableAdjustments(ref count).Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate == DateTime.Now.Date).Select(m => (int)m.ClassPeriodDetailId).ToArray();
                        }
                        var adjustments = _IClassPeriodService.GetAllTimeTableAdjustments(ref count).Where(m => m.TeacherId == user.UserContactId
                                                            && m.AdjustmentDate == DateTime.Now.Date);
                        var adjustmentperiodIds = adjustments.Select(m => m.ClassPeriod.PeriodId).ToArray();
                        var adjustmentClassIds = adjustments.Select(m => m.ClassPeriod.ClassSubject.ClassId).ToArray();
                        var adjustmentclasssprddetails = new List<int>();

                        if (adjustmentClassIds.Count() > 0)
                        {
                            var AdjustmentVirtualClassIds = adjustments.Where(m => m.ClassPeriod.ClassSubject.ClassId == null)
                                                                        .Select(m => (int)m.ClassPeriod.ClassSubject.VirtualClassId)
                                                                        .ToArray();

                            //currentday classperiodddetailids
                            var classperioddetailIds_daywise = _IClassPeriodService.GetAllClassPeriodDays(ref count)
                                .Where(m => m.DayId == DateTime.Now.Day).Select(m => m.ClassPeriodDetailId).ToArray();

                            var classperiodIds = _IClassPeriodService.GetAllClassPeriods(ref count)
                                .Where(m => m.ClassSubject.VirtualClassId == null && adjustmentperiodIds.Contains(m.PeriodId)
                                    && adjustmentClassIds.Contains(m.ClassSubject.ClassId)).Select(m => m.ClassPeriodId).ToArray();

                            var VirtualclassperiodIds = _IClassPeriodService.GetAllClassPeriods(ref count)
                                                        .Where(m => m.ClassSubject.ClassId == null && adjustmentperiodIds.Contains(m.PeriodId)
                                                            && AdjustmentVirtualClassIds.Contains((int)m.ClassSubject.VirtualClassId))
                                                            .Select(m => m.ClassPeriodId).ToArray();

                            classperiodIds = classperiodIds.Concat(VirtualclassperiodIds).ToArray();
                            adjustmentclasssprddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count)
                                                            .Where(m => classperioddetailIds_daywise.Contains(m.ClassPeriodDetailId)
                                                                && classperiodIds.Contains((int)m.ClassPeriodId))
                                                                .Select(m => m.ClassPeriodDetailId).ToList();
                        }
                        var finalClassPeriodDetailIdarray = ClassPeriodDetailIdarray.Union(adjustmentclasssprddetails)
                                                                .Distinct().ToArray();
                        if (finalClassPeriodDetailIdarray.Length > 0)
                        {
                            classperioddetails = classperioddetails.Where(m => finalClassPeriodDetailIdarray
                                                 .Contains(m.ClassPeriodDetailId)).ToList();
                            classidlist = classperioddetails.Where(m => m.ClassPeriod.ClassSubject.VirtualClassId == null)
                                                 .Select(m => m.ClassPeriod.ClassSubject.ClassId).Distinct().ToList();
                            classsubjectidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassSubjectId).ToList();
                            Virtualclassidlist = classperioddetails.Where(m => m.ClassPeriod != null && m.ClassPeriod.ClassSubject.ClassId == null)
                                                  .Select(m => m.ClassPeriod.ClassSubject.VirtualClassId)
                                                  .Distinct().ToList();

                            #region//get incharge teacher class
                            var teacherInfo = _IStaffService.GetAllClassTeachers(ref count, TeacherId: user.UserContactId)
                                                                .Where(p => p.EffectiveDate <= DateTime.Now.Date && (p.EndDate >= DateTime.Now.Date
                                                                    || p.EndDate == null))
                                                                 .OrderByDescending(x => x.ClassTeacherId).ToList();
                            if (teacherInfo.Count() > 0)
                            {
                                model.IsClassIncharge = true;
                                int row = 0;
                                foreach (var incharge in teacherInfo)
                                {
                                    row++;
                                    InchargedClass = (int)incharge.ClassId;
                                    classidlist.Add(InchargedClass);

                                    if (row > 1)
                                        model.InchargedClassName += ", " + incharge.ClassMaster.Class;
                                    else
                                        model.InchargedClassName += incharge.ClassMaster.Class;
                                }

                                model.InchargedClassName = model.InchargedClassName.Trim(',');
                            }


                            if ((classidlist.Count > 0 || Virtualclassidlist.Count > 0) && classsubjectidlist.Count > 0)
                            {
                                var classDropdownlist = new List<CustomSelectClassitem>();
                                foreach (var classid in classidlist.Distinct())
                                {
                                    var classname = _ICatalogMasterService.GetClassMasterById((int)classid).Class;
                                    model.AvailableClassList.Add(new CustomSelectClassitem { Text = classname, Value = classid.ToString(), VirtualClassValue = "0" });
                                }

                                foreach (var virtualclassid in Virtualclassidlist)
                                {
                                    var classname = _ICatalogMasterService.GetAllVirtualClasses(ref count).Where(m => m.VirtualClassId == virtualclassid).FirstOrDefault().VirtualClass1;
                                    model.AvailableClassList.Add(new CustomSelectClassitem
                                    {
                                        Text = classname,
                                        Value = "0",
                                        VirtualClassValue = virtualclassid.ToString()
                                    });
                                }
                            }
                            #endregion
                        }
                    }
                    #endregion
                }
                else
                {
                    ExtractClassesNotLinkedTimetable(model, user);
                }
            }
            else
            {
                #region Admin
                var classList = _ICatalogMasterService.GetAllClassMasters(ref count);
                foreach (var item in classList)
                {
                    if (model.ClassId == item.ClassId)
                    {
                        model.AvailableClassList.Add(new CustomSelectClassitem { Text = item.Class, Value = item.ClassId.ToString(), VirtualClassValue = "0", Selected = true });
                    }
                    else
                        model.AvailableClassList.Add(new CustomSelectClassitem { Text = item.Class, Value = item.ClassId.ToString(), VirtualClassValue = "0" });

                }
                var Virtualclasslist = _ICatalogMasterService.GetAllVirtualClasses(ref count);

                foreach (var virtualclass in Virtualclasslist)
                {

                    if (model.VirtualClassId == virtualclass.VirtualClassId)
                    {
                        model.AvailableClassList.Add(new CustomSelectClassitem { Text = virtualclass.VirtualClass1, Value = "0", VirtualClassValue = virtualclass.VirtualClassId.ToString(), Selected = true });
                    }
                    else
                        model.AvailableClassList.Add(new CustomSelectClassitem { Text = virtualclass.VirtualClass1, Value = "0", VirtualClassValue = virtualclass.VirtualClassId.ToString() });
                }

                #endregion
            }

            return model;
        }

        public void ExtractClassesNotLinkedTimetable(HomeWorkModel model, SchoolUser user)
        {
            try
            {
                model.AvailableClassList.Add(new CustomSelectClassitem() { Text = "--Select--", Value = "", VirtualClassValue = "0" });
                //get tecaher's main classes
                var getTeacherstandardIds = _IStaffService.GetAllTeacherClasss(ref count, TeacherId: user.UserContactId)
                                                        .Select(x => x.StandardMaster.StandardId)
                                                        .Distinct().ToArray();
                var getTeacherCLass_Ids = _ICatalogMasterService.GetAllClassMasters(ref count)
                                            .Where(x => getTeacherstandardIds.Contains((int)x.StandardId))
                                            .Select(x => x.ClassId).ToArray();

                var getTeacherAssinedClasses = _IStaffService.GetAllTeacherSubjectAssigns(ref count, TeacherId: user.UserContactId)
                                                             .Where(x => x.ClassId != null && getTeacherCLass_Ids.Contains((int)x.ClassId))
                                                             .Select(x => x.ClassMaster).Distinct().ToList();

                var TeacherVirtualClass_Ids = _ICatalogMasterService.GetAllVirtualClasses(ref count)
                                            .Where(x => getTeacherstandardIds.Contains((int)x.StandardId))
                                            .Select(x => x.VirtualClassId).ToArray();

                var TeacherVirtualClass = _IStaffService.GetAllTeacherSubjectAssigns(ref count, TeacherId: user.UserContactId)
                                                           .Where(x => x.VirtualClassId != null &&
                                                                   TeacherVirtualClass_Ids.Contains((int)x.VirtualClassId))
                                                           .Select(x => x.VirtualClass).Distinct().ToList();

                var teacherInfo = _IStaffService.GetAllClassTeachers(ref count, TeacherId: user.UserContactId)
                                                               .Where(p => p.EffectiveDate <= DateTime.Now.Date
                                                                           && (p.EndDate >= DateTime.Now.Date
                                                                           || p.EndDate == null))
                                                                .OrderByDescending(x => x.ClassTeacherId).ToList();
                if (teacherInfo.Count() > 0)
                {
                    model.IsClassIncharge = true;
                    int row = 0;
                    foreach (var incharge in teacherInfo)
                    {
                        row++;
                        if (row > 1)
                            model.InchargedClassName += ", " + incharge.ClassMaster.Class;
                        else
                            model.InchargedClassName += ", " + incharge.ClassMaster.Class;
                    }

                    model.InchargedClassName = model.InchargedClassName.Trim(',');
                }

                var inchargeClasses = teacherInfo.Select(x => x.ClassMaster);
                getTeacherAssinedClasses.AddRange(inchargeClasses);

                //normal classes
                foreach (var item in getTeacherAssinedClasses.Distinct())
                {
                    //check teacher is  incharge  of this class if yes then bind all subjects of that class

                    var classDropdownlist = new List<CustomSelectClassitem>();
                    var classname = _ICatalogMasterService.GetClassMasterById((int)item.ClassId).Class;
                    model.AvailableClassList.Add(new CustomSelectClassitem
                    {
                        Text = classname,
                        Value = item.ClassId.ToString(),
                        VirtualClassValue = "0"
                    });
                    //}
                }

                //virtual classes
                foreach (var item in TeacherVirtualClass.Distinct())
                {
                    var virclassname = _ICatalogMasterService.GetAllVirtualClasses(ref count)
                                           .Where(x => x.VirtualClassId == item.VirtualClassId).FirstOrDefault().VirtualClass1;

                    model.AvailableClassList.Add(new CustomSelectClassitem
                    {
                        Text = virclassname,
                        Value = "0",
                        VirtualClassValue = item.VirtualClassId.ToString()
                    });
                }
            }
            catch { }
        }

        // GET: HomeWork
        public ActionResult HomeWork()
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "HomeWork", "HomeWorkList", "Add");
                    //if (!isauth)
                    //    return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                var model = new HomeWorkModel();
                prepareHomeWork(model, user);

                model.SMSSelectionStudentMobileNo = _ISchoolSettingService.GetorSetSchoolData("HWSmsMobileNoSelectionForStudentRecepient", "SMS",
                                                    Desc: "SMS : Student SMS Mobile No , SMN: Student mobile no. , GMN : Guardian Mobile No")
                                                   .AttributeValue;
                model.IsAllowHWBackDateEntry = true;
                model.IsAllowHWFutureDateEntry = true;
               
                var AllowHWBackDateEntry = _ISchoolSettingService.GetorSetSchoolData("AllowHWBackDateEntry", "0").AttributeValue;
                var AllowHWFutureDateEntry = _ISchoolSettingService.GetorSetSchoolData("AllowHWFutureDateEntry", "0").AttributeValue;
                var HWDescriptionPrmssn = _ISchoolSettingService.GetSchoolSettingByAttribute("EnableHomeWorkDescription");
                model.IsAllowHWDescription = HWDescriptionPrmssn.AttributeValue!="1"?false:true;

                if (AllowHWBackDateEntry == "0")
                    model.IsAllowHWBackDateEntry = false;

                if (AllowHWFutureDateEntry == "0")
                    model.IsAllowHWFutureDateEntry = false;

                model.minDate=null;
                if (model.IsAllowHWBackDateEntry==false)
                    model.minDate = DateTime.Now.Date.ToString("MM/dd/yyyy");

                model.maxDate = null;
                if (model.IsAllowHWFutureDateEntry == false)
                    model.maxDate = DateTime.Now.Date.ToString("MM/dd/yyyy");

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public string getClassNameFromClassSubjectId(int ClassSubjectId)
        {
            var classname = "";
            var classsubject = _ISubjectService.GetAllClassSubjects(ref count).Where(m => m.ClassSubjectId == ClassSubjectId).FirstOrDefault();
            if (classsubject != null)
            {
                if (classsubject.ClassMaster != null)
                {
                    classname = classsubject.ClassMaster.Class;
                }
                else if (classsubject.VirtualClass != null)
                {
                    classname = classsubject.VirtualClass.VirtualClass1;
                }
            }
            return classname;

        }

        public ActionResult InsertDefaultValues(string MobileNoSelection)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;


                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                //save default  student Mobile no selection
                if (!string.IsNullOrEmpty(MobileNoSelection))
                {
                    var schoolSettingSMSMobileNo = _ISchoolSettingService.GetSchoolSettingByAttribute("HWSmsMobileNoSelectionForStudentRecepient");
                    if (schoolSettingSMSMobileNo != null)
                    {
                        schoolSettingSMSMobileNo.AttributeValue = MobileNoSelection;
                        _ISchoolSettingService.UpdateSchoolSetting(schoolSettingSMSMobileNo);
                    }
                    else
                    {
                        schoolSettingSMSMobileNo = new SchoolSetting();
                        schoolSettingSMSMobileNo.AttributeName = "HWSmsMobileNoSelectionForStudentRecepient";
                        schoolSettingSMSMobileNo.Description = "SMS : Student SMS Mobile No , SMN: Student mobile no. , GMN : Guardian Mobile No";
                        schoolSettingSMSMobileNo.AttributeValue = MobileNoSelection;
                        _ISchoolSettingService.InsertSchoolSetting(schoolSettingSMSMobileNo);
                    }
                }
                return Json(new
                {
                    status = "success",
                    MobileNoSelection,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = "Error happened"
                });
            }
        }

        public List<Student> getStudentsFromClassSubjectId(int ClassSubjectId)
        {

            var classId = 0;
            var virtualClassId = 0;
            var classsubject = _ISubjectService.GetAllClassSubjects(ref count).Where(m => m.ClassSubjectId == ClassSubjectId).FirstOrDefault();
            if (classsubject != null)
            {
                if (classsubject.ClassMaster != null)
                {
                    classId = classsubject.ClassMaster.ClassId;
                }
                else if (classsubject.VirtualClass != null)
                {
                    virtualClassId = classsubject.VirtualClass.VirtualClassId;
                }
            }
            var session = _ICatalogMasterService.GetCurrentSession();
            var students = new List<Student>();
            if (classId > 0)
            {
                var studentlists = _IStudentService.GetAllStudentClasss(ref count).Where(m => m.ClassId == classId && m.SessionId == session.SessionId);
                foreach (var item in studentlists)
                {
                    students.Add(item.Student);
                }
            }
            if (virtualClassId > 0)
            {
                var virtualclassSession = _ICatalogMasterService.GetAllVirtualClassSessions(ref count).Where(m => m.VirtualClassId == virtualClassId && m.SessionId == session.SessionId).FirstOrDefault();
                if (virtualclassSession != null)
                {
                    var VirtualClassstudentlists = _ICatalogMasterService.GetAllVirtualClassStudents(ref count).Where(m => m.VirtualClassSessionId == virtualclassSession.VirtualClassSessionId).Select(m => m.StudentClassId).ToArray(); ;

                    var studentlists = _IStudentService.GetAllStudentClasss(ref count).Where(m => VirtualClassstudentlists.Contains(m.StudentClassId));
                    foreach (var item in studentlists)
                    {
                        students.Add(item.Student);
                    }
                }
            }
            return students;

        }

        [HttpPost]
        public ActionResult HomeWork(HomeWorkModel model)
        {
            SchoolUser user = new SchoolUser();
            //HomeWorkModel model = new HomeWorkModel();
            //TryUpdateModel<HomeWorkModel>(model,);
            // get action name
            var UserRequestReferalUrl = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.UrlReferrer.ToString());
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            // check id exist
            //if (!string.IsNullOrEmpty(model.EncStudentId))
            //{
            //    // decrypt Id
            //    int student_id = 0;
            //    if (int.TryParse(_ICustomEncryption.base64d(model.EncStudentId), out student_id))
            //        model.StudentId = student_id;
            //}
            //else
            //    return RedirectToAction("Page404", "Error");
            bool editmode = false;
            // var studentid = _ICustomEncryption.base64e(model.StudentId.ToString());
            try
            {

                var schoolSettingSMSMobileNo = _ISchoolSettingService.GetorSetSchoolData("HWSmsMobileNoSelectionForStudentRecepient", "SMN");


                // Activity log type add
                var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();

                #region HomeWork
                //var singleUploadDocumentIds = _IDocumentService.GetAllDocumentTypes(ref count).Where(p => p.IsMultipleAllowed == false && p.DocumentType1.ToLower() != "image").Select(p => p.DocumentTypeId).ToList();
                System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<HomeWorkModel> DocumentArray = js.Deserialize<List<HomeWorkModel>>(model.DocumentArray);
                if (model.SubjectId == 0)
                {
                    if (!editmode && TempData["ErrorNotification"] == null)
                        TempData["ErrorNotification"] = "No Class Assigned to Teacher";
                    prepareHomeWork(model, user);
                    return View(model);
                }
                var date = DateTime.UtcNow;
                if (!string.IsNullOrEmpty(model.Date))
                {
                    date = DateTime.ParseExact(model.Date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                if (model.HWId > 0)
                {
                    var homework = _IHomeWorkService.GetHWById((int)model.HWId, true);
                   
                    if (homework != null)
                    {
                        homework.HWTypeId = model.HomeWorkTypeId;
                        homework.ClassSubjectId = model.SubjectId;
                        homework.StaffId = user.UserContactId;
                        homework.Date = date;
                        _IHomeWorkService.UpdateHW(homework);
                        editmode = true;



                        // AttachedDocument Id
                        var AttachedDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AttachedDocuments").FirstOrDefault();
                        if (AttachedDocumenttable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AttachedDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog((int)homework.HWId, AttachedDocumenttable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update AttachedDocument with DocumentId:{0} and StudentId:{1}", homework.HWId, homework.ClassSubject), Request.Browser.Browser);
                        }
                    }

                }
                else
                {
                    HW homework = new HW();
                    if (homework != null)
                    {
                        homework.HWTypeId = model.HomeWorkTypeId;
                        homework.ClassSubjectId = model.SubjectId;
                        homework.StaffId = user.UserContactId;
                        homework.Date = date;
                        _IHomeWorkService.InsertHW(homework);
                        editmode = false;
                        model.HWId = homework.HWId;

                        // AttachedDocument Id
                        var HWtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AttachedDocuments").FirstOrDefault();
                        if (HWtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, HWtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog((int)homework.HWId, HWtable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update AttachedDocument with DocumentId:{0} and StudentId:{1}", homework.HWId, homework.ClassSubject), Request.Browser.Browser);
                        }
                    }
                }
                foreach (var Document in DocumentArray)
                {
                    if (!string.IsNullOrEmpty(Document.HomeworkDetailId))
                        Document.HWDetailId = Convert.ToInt32(Document.HomeworkDetailId);
                    string filename = string.Empty;

                    if (!string.IsNullOrEmpty(Document.FileName))
                    {
                        string path = Document.FileName;
                        // move directory one time
                        var pathary = path.Split('\\');
                        if (pathary.Length > 1)
                            filename = pathary[pathary.Length - 1];
                        else
                        {
                            pathary = path.Split('/');
                            if (pathary.Length > 1)
                                filename = pathary[pathary.Length - 1];
                        }
                    }

                    if (Document.HWDetailId > 0)
                    {
                        var homeworkdetail = _IHomeWorkService.GetHWDetailById((int)Document.HWDetailId, true);
                        if (homeworkdetail != null)
                        {
                            homeworkdetail.Remarks = Document.Remarks;
                            homeworkdetail.FileName = filename;
                            homeworkdetail.Description = !string.IsNullOrEmpty(Document.Description) ? Document.Description.Trim() : null;
                            _IHomeWorkService.UpdateHWDetail(homeworkdetail);
                            editmode = true;

                            // AttachedDocument Id
                            var AttachedDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AttachedDocuments").FirstOrDefault();
                            if (AttachedDocumenttable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AttachedDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog((int)homeworkdetail.HWId, AttachedDocumenttable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update AttachedDocument with DocumentId:{0} and StudentId:{1}", homeworkdetail.Description, homeworkdetail.HWId), Request.Browser.Browser);
                            }
                        }
                    }
                    else
                    {
                        HWDetail homeworkdetail = new HWDetail();
                        homeworkdetail.Remarks = Document.Remarks;
                        homeworkdetail.HWId = model.HWId;
                        homeworkdetail.FileName = filename;
                        homeworkdetail.Description = !string.IsNullOrEmpty(Document.Description) ? Document.Description.Trim() : null;
                        _IHomeWorkService.InsertHWDetail(homeworkdetail);
                        editmode = false;
                        var homeworkdetails = _IHomeWorkService.GetAllHWs(ref count).Where(m => m.HWId == model.HWId).FirstOrDefault();
                        var ClassNAme = getClassNameFromClassSubjectId((int)homeworkdetail.HW.ClassSubjectId);
                        var Students = getStudentsFromClassSubjectId((int)homeworkdetail.HW.ClassSubjectId);

                        string Subject = "";
                        var getStuSubject = _ISubjectService.GetAllClassSubjects(ref count)
                                                        .Where(m => m.ClassSubjectId == homeworkdetail.HW.ClassSubjectId)
                                                        .FirstOrDefault();
                        if (getStuSubject != null)
                            Subject = getStuSubject.Subject.Subject1;

                        var triggerEvents = _ITriggerEventService.GetAllTriggerEventList(ref count);

                        //send HW notification or sms 
                        _ICommonMethodService.TiggerNotificationSMS(homeworkdetail: homeworkdetail, Students: Students,
                                                TriggerEventName: "Homework", HWDescription: Document.Description,
                                                Subject: Subject, ClassName: ClassNAme,
                                                SendSMSMobileOptionId: schoolSettingSMSMobileNo.AttributeValue,
                                                IsNonAPPUser: false, SMS: true, Notification: true);

                        // AttachedDocument Id
                        var HomeWorkDetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AttachedDocuments").FirstOrDefault();
                        if (HomeWorkDetailtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, HomeWorkDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog((int)homeworkdetail.HWId, HomeWorkDetailtable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update AttachedDocument with DocumentId:{0} and StudentId:{1}", homeworkdetail.Description, homeworkdetail.HWId), Request.Browser.Browser);
                        }


                        //if (singleUploadDocumentIds.Contains((int)Document.AttachDocumentTypeId))
                        //{
                        //    var arrayducuments = DocumentArray.Where(p => singleUploadDocumentIds.Contains((int)p.AttachDocumentTypeId)).ToList();
                        //    if (arrayducuments.Count > 0)
                        //    {
                        //        var query = arrayducuments.GroupBy(x => x.AttachDocumentTypeId).Where(g => g.Count() > 1).ToList();
                        //        if (query.Count > 0)
                        //        {
                        //            TempData["ErrorNotification"] = "Single uploadable document shouldn,t be multiple.";
                        //            continue;
                        //        }
                        //    }
                        //}
                        //AttachedDocument AttachedDocument = new AttachedDocument()
                        //{
                        //    DocumentTypeId = Document.AttachDocumentTypeId,
                        //    StudentId = model.StudentId,
                        //    Image = filename,
                        //    Description = Document.Description
                        //};
                        //_IDocumentService.InsertAttachedDocument(AttachedDocument);
                        //// AttachedDocument Id
                        //var AttachedDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AttachedDocuments").FirstOrDefault();
                        //if (AttachedDocumenttable != null)
                        //{
                        //    // table log saved or not
                        //    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AttachedDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        //    // save activity log
                        //    if (tabledetail != null)
                        //        _IActivityLogService.SaveActivityLog(AttachedDocument.DocumentId, AttachedDocumenttable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add new AttachedDocument with DocumentId:{0} and StudentId:{1}", AttachedDocument.DocumentId, AttachedDocument.StudentId), Request.Browser.Browser);
                        //}

                    }
                }
                if (DocumentArray.Count > 0)
                {
                    if (!editmode && TempData["ErrorNotification"] == null)
                        TempData["SuccessNotification"] = " HomeWork has been added successfully";
                    else if (editmode && TempData["ErrorNotification"] == null)
                        TempData["SuccessNotification"] = " HomeWork has been updated successfully";

                    //if (model.DocumentsUpdated != null)
                    //    TempData["DocumentsUpdated"] = model.DocumentsUpdated;

                    //return RedirectToAction(UserRequestReferalUrl.ActionName, new { Id = studentid });
                }
                #endregion

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }

            // prepareHomeWork(model, user);
            return RedirectToAction("HomeWorkList");
        }

        [HttpPost]
        public ActionResult EditHomeWork(HomeWorkModel model)
        {
            SchoolUser user = new SchoolUser();
            // get action name
            var UserRequestReferalUrl = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.UrlReferrer.ToString());
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            // check id exist
            //if (!string.IsNullOrEmpty(model.EncStudentId))
            //{
            //    // decrypt Id
            //    int student_id = 0;
            //    if (int.TryParse(_ICustomEncryption.base64d(model.EncStudentId), out student_id))
            //        model.StudentId = student_id;
            //}
            //else
            //    return RedirectToAction("Page404", "Error");
            bool editmode = false;
            // var studentid = _ICustomEncryption.base64e(model.StudentId.ToString());
            try
            {

                // Activity log type add
                var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();

                #region HomeWork
                //var singleUploadDocumentIds = _IDocumentService.GetAllDocumentTypes(ref count).Where(p => p.IsMultipleAllowed == false && p.DocumentType1.ToLower() != "image").Select(p => p.DocumentTypeId).ToList();
                System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<HomeWorkModel> DocumentArray = js.Deserialize<List<HomeWorkModel>>(model.DocumentArray);

                if (model.HWId > 0)
                {
                    var homework = _IHomeWorkService.GetHWById((int)model.HWId, true);
                    if (homework != null)
                    {
                        homework.HWTypeId = model.HomeWorkTypeId;
                        homework.ClassSubjectId = model.SubjectId;
                        homework.StaffId = user.UserContactId;
                        homework.Date = DateTime.UtcNow;
                        _IHomeWorkService.UpdateHW(homework);
                        editmode = true;

                        // AttachedDocument Id
                        var AttachedDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AttachedDocuments").FirstOrDefault();
                        if (AttachedDocumenttable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AttachedDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog((int)homework.HWId, AttachedDocumenttable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update AttachedDocument with DocumentId:{0} and StudentId:{1}", homework.HWId, homework.ClassSubject), Request.Browser.Browser);
                        }
                    }

                }
                else
                {
                    HW homework = new HW();
                    if (homework != null)
                    {
                        homework.HWTypeId = model.HomeWorkTypeId;
                        homework.ClassSubjectId = model.SubjectId;
                        homework.StaffId = user.UserContactId;
                        homework.Date = DateTime.UtcNow;
                        _IHomeWorkService.InsertHW(homework);
                        editmode = true;
                        model.HWId = homework.HWId;
                        // AttachedDocument Id
                        var HWtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AttachedDocuments").FirstOrDefault();
                        if (HWtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, HWtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog((int)homework.HWId, HWtable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update AttachedDocument with DocumentId:{0} and StudentId:{1}", homework.HWId, homework.ClassSubject), Request.Browser.Browser);
                        }
                    }
                }
                var detialsids = _IHomeWorkService.GetAllHWDetails(ref count, HWId: model.HWId).ToList();
                foreach (var detial in detialsids)
                    _IHomeWorkService.DeleteHWDetail(detial.HWDetailId);
                foreach (var Document in DocumentArray)
                {
                    string filename = string.Empty;
                    if (!string.IsNullOrEmpty(Document.HomeworkDetailId))
                        Document.HWDetailId = Convert.ToInt32(Document.HomeworkDetailId);
                    if (!string.IsNullOrEmpty(Document.FileName))
                    {

                        string path = Document.FileName;
                        // move directory one time
                        var pathary = path.Split('\\');
                        if (pathary.Length > 1)
                            filename = pathary[pathary.Length - 1];
                        else
                        {
                            pathary = path.Split('/');
                            if (pathary.Length > 1)
                                filename = pathary[pathary.Length - 1];
                        }
                    }
                    //if (Document.HWDetailId > 0)
                    //{


                    //    var homeworkdetail = _IHomeWorkService.GetHWDetailById((int)Document.HWDetailId, true);
                    //    if (homeworkdetail != null)
                    //    {
                    //        homeworkdetail.Remarks = Document.Remarks;
                    //        homeworkdetail.FileName = filename;
                    //        homeworkdetail.Description = !string.IsNullOrEmpty(Document.Description) ? Document.Description.Trim() : null;
                    //        _IHomeWorkService.UpdateHWDetail(homeworkdetail);
                    //        editmode = true;

                    //        // AttachedDocument Id
                    //        var AttachedDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AttachedDocuments").FirstOrDefault();
                    //        if (AttachedDocumenttable != null)
                    //        {
                    //            // table log saved or not
                    //            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AttachedDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                    //            // save activity log
                    //            if (tabledetail != null)
                    //                _IActivityLogService.SaveActivityLog((int)homeworkdetail.HWId, AttachedDocumenttable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update AttachedDocument with DocumentId:{0} and StudentId:{1}", homeworkdetail.Description, homeworkdetail.HWId), Request.Browser.Browser);
                    //        }
                    //    }
                    //}
                    //else
                    //{

                    HWDetail homeworkdetail = new HWDetail();
                    homeworkdetail.Remarks = Document.Remarks;
                    homeworkdetail.HWId = model.HWId;
                    homeworkdetail.FileName = filename;
                    homeworkdetail.Description = !string.IsNullOrEmpty(Document.Description) ? Document.Description.Trim() : null;
                    _IHomeWorkService.InsertHWDetail(homeworkdetail);
                    editmode = true;

                    // AttachedDocument Id
                    var HomeWorkDetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AttachedDocuments").FirstOrDefault();
                    if (HomeWorkDetailtable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, HomeWorkDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog((int)homeworkdetail.HWId, HomeWorkDetailtable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update AttachedDocument with DocumentId:{0} and StudentId:{1}", homeworkdetail.Description, homeworkdetail.HWId), Request.Browser.Browser);
                    }




                    //if (singleUploadDocumentIds.Contains((int)Document.AttachDocumentTypeId))
                    //{
                    //    var arrayducuments = DocumentArray.Where(p => singleUploadDocumentIds.Contains((int)p.AttachDocumentTypeId)).ToList();
                    //    if (arrayducuments.Count > 0)
                    //    {
                    //        var query = arrayducuments.GroupBy(x => x.AttachDocumentTypeId).Where(g => g.Count() > 1).ToList();
                    //        if (query.Count > 0)
                    //        {
                    //            TempData["ErrorNotification"] = "Single uploadable document shouldn,t be multiple.";
                    //            continue;
                    //        }
                    //    }
                    //}
                    //AttachedDocument AttachedDocument = new AttachedDocument()
                    //{
                    //    DocumentTypeId = Document.AttachDocumentTypeId,
                    //    StudentId = model.StudentId,
                    //    Image = filename,
                    //    Description = Document.Description
                    //};
                    //_IDocumentService.InsertAttachedDocument(AttachedDocument);
                    //// AttachedDocument Id
                    //var AttachedDocumenttable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "AttachedDocuments").FirstOrDefault();
                    //if (AttachedDocumenttable != null)
                    //{
                    //    // table log saved or not
                    //    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, AttachedDocumenttable.TableId, DateTime.UtcNow, true).LastOrDefault();
                    //    // save activity log
                    //    if (tabledetail != null)
                    //        _IActivityLogService.SaveActivityLog(AttachedDocument.DocumentId, AttachedDocumenttable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add new AttachedDocument with DocumentId:{0} and StudentId:{1}", AttachedDocument.DocumentId, AttachedDocument.StudentId), Request.Browser.Browser);
                    //}

                    //}

                }
                if (DocumentArray.Count > 0)
                {
                    if (!editmode && TempData["ErrorNotification"] == null)
                        TempData["SuccessNotification"] = " HomeWork has been added successfully";
                    else if (editmode && TempData["ErrorNotification"] == null)
                        TempData["SuccessNotification"] = " HomeWork has been updated successfully";

                    //if (model.DocumentsUpdated != null)
                    //    TempData["DocumentsUpdated"] = model.DocumentsUpdated;

                    //return RedirectToAction(UserRequestReferalUrl.ActionName, new { Id = studentid });
                }
                #endregion
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
            prepareHomeWork(model, user);


            var newhomework = _IHomeWorkService.GetHWById(model.HWId);
            if (newhomework != null)
            {
                model.HomeWorkTypeId = (int)newhomework.HWTypeId;
                model.ClassId = newhomework.ClassSubject.ClassMaster.ClassId;
                model.SubjectId = newhomework.ClassSubject.ClassSubjectId;
                model.SubId = newhomework.ClassSubject.ClassSubjectId;
                model.HWId = model.HWId;
                model.Date = ConvertDate((DateTime)newhomework.Date);
                var schoolid = HttpContext.Session["SchoolId"];
                //if (homeworkDetailsid == 0)
                //    homeworkDetailsid = homework.HWDetails.FirstOrDefault().HWDetailId;
                var HWDetails = newhomework.HWDetails;
                IList<HomeWorkModel> detat = new List<HomeWorkModel>();
                var hwmodel = new HomeWorkModel();
                foreach (var det in HWDetails)
                {
                    hwmodel = new HomeWorkModel();
                    var homeworkDetails = _IHomeWorkService.GetHWDetailById(det.HWDetailId);
                    if (homeworkDetails != null)
                    {
                        hwmodel.Description = !string.IsNullOrEmpty(homeworkDetails.Description) ? homeworkDetails.Description : "";
                        hwmodel.Remarks = !string.IsNullOrEmpty(homeworkDetails.Remarks) ? homeworkDetails.Remarks : "";
                        hwmodel.File = !string.IsNullOrEmpty(homeworkDetails.FileName) ?
                            _WebHelper.GetStoreLocation() + "/Images/" + schoolid + "/HomeWork/" + homeworkDetails.FileName : "";

                        hwmodel.HWDetailId = homeworkDetails.HWDetailId;
                    }
                    model.HWList.Add(hwmodel);
                }
            }
            return RedirectToAction("HomeWorkList");
        }

        public ActionResult EditHomeWork(string HWId, string HWDetailId = "")
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "HomeWork", "EditHomeWork", "View");

                    //if (!isauth)
                    //    return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                var model = new HomeWorkModel();
                var schoolid = HttpContext.Session["SchoolId"];
                var homeworkid = Convert.ToInt32(HWId);
                var homeworkDetailsid = !string.IsNullOrEmpty(HWDetailId) ? Convert.ToInt32(HWDetailId) : 0;

                var homework = _IHomeWorkService.GetHWById(homeworkid);
                if (homework != null)
                {
                    model.HomeWorkTypeId = (int)homework.HWTypeId;
                    model.ClassId = homework.ClassSubject.ClassMaster != null ? (int)homework.ClassSubject.ClassId : 0;
                    model.VirtualClassId = homework.ClassSubject.ClassMaster == null ? (int)homework.ClassSubject.VirtualClassId : 0;
                    model.SubjectId = (int)homework.ClassSubjectId;
                    model.HWId = homeworkid;
                    model.SubId = (int)homework.ClassSubjectId;
                    model.Date = ConvertDate((DateTime)homework.Date);

                    //if (homeworkDetailsid == 0)
                    //    homeworkDetailsid = homework.HWDetails.FirstOrDefault().HWDetailId;
                    var HWDetails = homework.HWDetails;
                    IList<HomeWorkModel> detat = new List<HomeWorkModel>();
                    var hwmodel = new HomeWorkModel();
                    foreach (var det in HWDetails)
                    {
                        hwmodel = new HomeWorkModel();
                        var homeworkDetails = _IHomeWorkService.GetHWDetailById(det.HWDetailId);
                        if (homeworkDetails != null)
                        {
                            hwmodel.Description = !string.IsNullOrEmpty(homeworkDetails.Description) ? homeworkDetails.Description : "";
                            hwmodel.Remarks = !string.IsNullOrEmpty(homeworkDetails.Remarks) ? homeworkDetails.Remarks : "";
                            hwmodel.File = !string.IsNullOrEmpty(homeworkDetails.FileName) ?
                                _WebHelper.GetStoreLocation() + "/Images/" + schoolid + "/HomeWork/" + homeworkDetails.FileName : "";

                            hwmodel.HWDetailId = homeworkDetails.HWDetailId;
                        }
                        model.HWList.Add(hwmodel);
                    }
                }
                model = prepareHomeWork(model, user);

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult HomeWorkList()
        {

            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "HomeWork", "HomeWorkList", "View");
                    if (!isauth)
                        return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

                var model = new HomeWorkModel();
                model.IsTeacher = false;
                model.IsStudent = false;
                string UserRole = "";
                UserRole = _ICommonMethodService.GetUserSuperRole(UserRole, user);
                if (UserRole.ToLower() == "teacher")
                    model.IsTeacher = true;

                if (UserRole.ToLower() == "admin")
                    model.IsAdmin = true;
                if (UserRole.ToLower() == "student" || user.ContactType.ContactType1.ToLower() == "guardian")
                    model.IsStudent = true;
                var studentid = 0;
                var staffid = 0;

                bool linkedTimetable = false;
                var TeacherHWLinkedTimetable = _ISchoolSettingService.GetorSetSchoolData("TeacherHWLinkedTimetable", "true", null,
                                                        "In Homework for filter class").AttributeValue;
                if (TeacherHWLinkedTimetable == "true")
                    linkedTimetable = true;

                switch (user.ContactType.ContactType1.ToLower())
                {
                    case "student":
                        studentid = (int)user.UserContactId;
                        break;

                    case "guardian":
                        var guardian = _IGuardianService.GetGuardianById((int)user.UserContactId);
                        studentid = guardian != null ? (int)guardian.StudentId : 0;
                        break;

                    case "teacher":
                        staffid = (int)user.UserContactId;
                        break;

                    default:
                        studentid = 0;
                        staffid = 0;
                        break;
                }
                model.ContactType = user.ContactType.ContactType1;
                int ClassId = 0;
                if (staffid > 0)
                {
                    var selecteddate = DateTime.UtcNow.Date;
                    var selectedDay = selecteddate.DayOfWeek;
                    var day = _IClassPeriodService.GetAllDayMasters(ref count).Where(m => m.DayName == selectedDay.ToString()).FirstOrDefault();
                    var timetables = _IClassPeriodService.GetAllTimeTables(ref count).ToList();
                    var timetable = timetables.Where(t => t.EffectiveFrom <= selecteddate.Date && t.EffectiveTill >= selecteddate.Date).FirstOrDefault();
                    int InchargedClass = 0;

                    if (linkedTimetable)
                    {
                        #region //Linked with timetable
                        if (timetable != null)
                        {

                            var classDropdownlist = new List<SelectListItem>();
                            var ClassPeriodDetailIdarray = new List<int>();
                            var allclassPerioddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count).ToList();
                            // get all class period detail by time table andt  teacher
                            var classperioddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count, TeacherId: user.UserContactId, TimeTableId: timetable.TimeTableId).ToList();
                            if (classperioddetails.Count > 0)
                            {
                                //var classperiodday = _IClassPeriodService.GetAllClassPeriodDays(ref count).Where(m => m.DayId == day.DayId).ToList();
                                //var ClassPeriodDetailIdarray = classperiodday.Count > 0 ? classperiodday.Select(m => (int)m.ClassPeriodDetailId).ToArray() : new int[] { };
                                // ClassPeriodDetailIdarray = classperioddetails.Where(m => ClassPeriodDetailIdarray.Contains((int)m.ClassPeriodDetailId)).Select(m => m.ClassPeriodDetailId).ToArray();
                                ClassPeriodDetailIdarray = classperioddetails.Select(m => m.ClassPeriodDetailId).ToList();
                                // var adjustmentclasssprddetails = _IClassPeriodService.GetAllTimeTableAdjustments(ref count).Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate == selecteddate.Date).Select(m => (int)m.ClassPeriodDetailId).ToArray();
                            }

                            var adjustments = _IClassPeriodService.GetAllTimeTableAdjustments(ref count).Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate == DateTime.Now.Date);
                            var adjustmentperiodIds = adjustments.Select(m => m.ClassPeriod.PeriodId).ToArray();
                            var adjustmentClassIds = adjustments.Where(m => m.ClassPeriod.ClassSubject.VirtualClassId == null).Select(m => (int)m.ClassPeriod.ClassSubject.ClassId).ToArray();
                            var AdjustmentVirtualClassIds = adjustments.Where(m => m.ClassPeriod.ClassSubject.ClassId == null).Select(m => (int)m.ClassPeriod.ClassSubject.VirtualClassId).ToArray();

                            var adjustmentclasssprddetails = new List<int>();
                            if (adjustmentClassIds.Count() > 0)
                            {
                                //currentday classperiodddetailids
                                var classperioddetailIds_daywise = _IClassPeriodService.GetAllClassPeriodDays(ref count).Where(m => m.DayId == DateTime.Now.Day).Select(m => m.ClassPeriodDetailId).ToArray();

                                var classperiodIds = _IClassPeriodService.GetAllClassPeriods(ref count).Where(m => m.ClassSubject.VirtualClassId == null && adjustmentperiodIds.Contains(m.PeriodId) && adjustmentClassIds.Contains((int)m.ClassSubject.ClassId)).Select(m => m.ClassPeriodId).ToArray();
                                var VirtualclassperiodIds = _IClassPeriodService.GetAllClassPeriods(ref count).Where(m => m.ClassSubject.ClassId == null && adjustmentperiodIds.Contains(m.PeriodId) && AdjustmentVirtualClassIds.Contains((int)m.ClassSubject.VirtualClassId)).Select(m => m.ClassPeriodId).ToArray();
                                classperiodIds = classperiodIds.Concat(VirtualclassperiodIds).ToArray();
                                adjustmentclasssprddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count)
                                                                 .Where(m => classperioddetailIds_daywise.Contains(m.ClassPeriodDetailId)
                                                                     && m.ClassPeriodId != null
                                                                     && classperiodIds.Contains((int)m.ClassPeriodId))
                                                                     .Select(m => m.ClassPeriodDetailId).ToList();
                            }

                            var finalClassPeriodDetailIdarray = ClassPeriodDetailIdarray.Union(adjustmentclasssprddetails).Distinct().ToArray();
                            if (finalClassPeriodDetailIdarray.Length > 0)
                            {
                                classperioddetails = allclassPerioddetails.Where(m => finalClassPeriodDetailIdarray.Contains(m.ClassPeriodDetailId)).ToList();
                                var classidlist = classperioddetails.Where(m => m.ClassPeriod != null && m.ClassPeriod.ClassSubject.VirtualClass == null).Select(m => m.ClassPeriod.ClassSubject.ClassId).Distinct().ToList();
                                var classsubjectidlist = classperioddetails.Where(m => m.ClassPeriod != null).Select(m => m.ClassPeriod.ClassSubject.ClassSubjectId).ToList();
                                var Virtualclassidlist = classperioddetails.Where(m => m.ClassPeriod != null && m.ClassPeriod.ClassSubject.ClassId == null).Select(m => m.ClassPeriod.ClassSubject.VirtualClassId).Distinct().ToList();

                                //get incharge teacher class 
                                var teacherInfo = _IStaffService.GetAllClassTeachers(ref count, TeacherId: user.UserContactId)
                                                                    .Where(p => p.EffectiveDate <= DateTime.Now.Date && (p.EndDate >= DateTime.Now.Date || p.EndDate == null))
                                                                     .OrderByDescending(x => x.ClassTeacherId).ToList();
                                if (teacherInfo.Count() > 0)
                                {
                                    foreach (var incharge in teacherInfo)
                                    {
                                        InchargedClass = (int)incharge.ClassId;
                                        classidlist.Add(InchargedClass);
                                    }
                                }


                                if ((classidlist.Count > 0 || Virtualclassidlist.Count > 0) && classsubjectidlist.Count > 0)
                                {
                                    if (user.ContactType.ContactType1.ToLower() == "teacher" || user.ContactType.ContactType1.ToLower() == "admin")
                                    {
                                        model.AvailableClassList.Add(new CustomSelectClassitem { Text = "All", Value = "0", VirtualClassValue = "0" });
                                    }
                                    foreach (var classid in classidlist.Distinct())
                                    {
                                        var classname = _ICatalogMasterService.GetClassMasterById((int)classid).Class;
                                        model.AvailableClassList.Add(new CustomSelectClassitem { Text = classname, Value = classid.ToString(), VirtualClassValue = "0" });
                                    }
                                    foreach (var virtualclassid in Virtualclassidlist)
                                    {
                                        var classname = _ICatalogMasterService.GetAllVirtualClasses(ref count).Where(m => m.VirtualClassId == virtualclassid).FirstOrDefault().VirtualClass1;


                                        model.AvailableClassList.Add(new CustomSelectClassitem { Text = classname, Value = "0", VirtualClassValue = virtualclassid.ToString() });
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        ExtractClassesNotLinkedTimetable(model, user);
                    }
                }
                else if (studentid > 0)
                {
                    // get class by student id
                    var session = _ICatalogMasterService.GetCurrentSession();
                    var stduentclass = _IStudentService.GetAllStudentClasss(ref count, SessionId: session.SessionId, StudentId: studentid);
                    ClassId = Convert.ToInt32(stduentclass[0].ClassId);
                    foreach (var stdclass in stduentclass)
                        model.AvailableClassList.Add(new CustomSelectClassitem { Text = stdclass.ClassMaster.Class, Value = stdclass.ClassId.ToString() });
                }
                else if (studentid == 0 && staffid == 0)
                {
                    var session = _ICatalogMasterService.GetCurrentSession();
                    var stduentclass = _ICatalogMasterService.GetAllClassMasters(ref count);
                    if (user.ContactType.ContactType1.ToLower() == "teacher" || user.ContactType.ContactType1.ToLower() == "admin")
                    {
                        model.AvailableClassList.Add(new CustomSelectClassitem { Text = "All", Value = "0" });
                    }

                    foreach (var stdclass in stduentclass)
                        model.AvailableClassList.Add(new CustomSelectClassitem { Text = stdclass.Class, Value = stdclass.ClassId.ToString() });

                }
                var staffTypeid = _IStaffService.GetAllStaffTypesUpdated(ref count, IsTeacher: true).FirstOrDefault();
                var teacherlist = _IStaffService.GetAllStaffs(ref count, StaffTypeId: staffTypeid.StaffTypeId);
                if (user.ContactType.ContactType1.ToLower() == "teacher")
                { }
                else
                {
                    model.AvailableTeacherList.Add(new SelectListItem { Selected = false, Text = "--Select--", Value = "0" });
                }
                foreach (var tcList in teacherlist)
                    model.AvailableTeacherList.Add(new SelectListItem { Selected = false, Text = tcList.FName + " " + tcList.LName, Value = tcList.StaffId.ToString() });
                var subjectlist = _ISubjectService.GetAllClassSubjects(ref count, ClassId: ClassId, IsActive: true);
                model.AvailableSubjectList.Add(new SelectListItem { Selected = false, Text = "--Select--", Value = "0" });
                foreach (var SubList in subjectlist)
                    model.AvailableSubjectList.Add(new SelectListItem { Selected = false, Text = SubList.Subject.Subject1, Value = SubList.SubjectId.ToString() });


                model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
                model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");
                var currentSession = _ICatalogMasterService.GetCurrentSession();
                if (currentSession != null)
                {
                    model.SessionStartDate = currentSession.StartDate;
                    model.SessionEndDate = currentSession.EndDate;
                }

                model.IsAllowHWBackDateEntry = true;
                var AllowHWBackDateEntry = _ISchoolSettingService.GetorSetSchoolData("AllowHWBackDateEntry", "0").AttributeValue;
                if (AllowHWBackDateEntry == "0")
                    model.IsAllowHWBackDateEntry = false;

                // auth
                model.IsAuthToAddHomeWork = _IUserAuthorizationService.IsUserAuthorize(user, "HomeWork", "HomeWorkList", "Add Record");
                model.IsAuthToEditHomeWork = _IUserAuthorizationService.IsUserAuthorize(user, "HomeWork", "HomeWorkList", "Edit Record");
                model.IsAuthToDeleteHomeWork = _IUserAuthorizationService.IsUserAuthorize(user, "HomeWork", "HomeWorkList", "Delete Record");
                model.IsAuthToViewHomeWork = _IUserAuthorizationService.IsUserAuthorize(user, "HomeWork", "HomeWorkList", "View Details");
                model.IsAuthToDownload = _IUserAuthorizationService.IsUserAuthorize(user, "HomeWork", "HomeWorkList", "Download");
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        #endregion

        #region Ajax Methods

        public ActionResult DeleteHomeWork(string id)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "HomeWork", "HomeWorkList", "Delete Record");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            // decrypt Id
            int message_id = Convert.ToInt32(id);

            var message = _IHomeWorkService.GetHWById(message_id, true);
            var messagedetailsids = _IHomeWorkService.GetAllHWDetails(ref count, HWId: message_id);
            foreach (var det in messagedetailsids)
            {
                _IHomeWorkService.DeleteHWDetail(det.HWDetailId);
            }
            _IHomeWorkService.DeleteHW(message.HWId);


            return Json(new
            {
                status = "success",
                msg = "HomeWork Deleted Sucessfully"
            });
        }

        public ActionResult GetSubjectsList(string ClsId, int? VirtualClassId = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                int classid = Convert.ToInt32(ClsId);
                var selectedDay = DateTime.Now.DayOfWeek;
                var day = _IClassPeriodService.GetAllDayMasters(ref count).Where(m => m.DayName == selectedDay.ToString()).FirstOrDefault();
                var timetables = _IClassPeriodService.GetAllTimeTables(ref count).ToList();
                var timetable = timetables.Where(t => t.EffectiveFrom <= DateTime.Now.Date && t.EffectiveTill >= DateTime.Now.Date).FirstOrDefault();
                int InchargedClass = 0;
                string UserRole = "";

                bool linkedTimetable = false;
                var TeacherHWLinkedTimetable = _ISchoolSettingService.GetorSetSchoolData("TeacherHWLinkedTimetable", "true", null,
                                                        "In Homework for filter class").AttributeValue;
                if (TeacherHWLinkedTimetable == "true")
                    linkedTimetable = true;

                UserRole = _ICommonMethodService.GetUserSuperRole(UserRole, user);

                bool IsInchargeAllowedForAtn = false;
                var date = DateTime.UtcNow;

                if (classid > 0)
                {
                    ////get incharge teacher class 
                    var IsTeacherIncharge = _IStaffService.GetAllClassTeachers(ref count,
                                                           TeacherId: user.UserContactId, ClassId: classid)
                                                           .Where(p => p.EffectiveDate <= date && (p.EndDate >= date || p.EndDate == null))
                                                           .ToList();
                    if (IsTeacherIncharge.Count > 0)
                        IsInchargeAllowedForAtn = true;
                }

                var ClassSubjects = new List<ClassSubject>();
                var subjectDropdownlist = new List<SelectListItem>();

                var classsubjectidlist = new List<int>();
                string Subject = "", ClassSubjectId = "";

                var ClassPeriodDetailIdarray = new int[] { };
                if (UserRole.ToLower() != "admin")
                {
                    if (linkedTimetable)
                    {
                        #region//if timetable linked then check that is teacher have any timetable
                        if (timetable != null)
                        {
                            // get all class period detail by time table and  teacher
                            var classperioddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count, TeacherId: user.UserContactId, TimeTableId: timetable.TimeTableId).ToList();
                            if (classperioddetails.Count > 0)
                            {
                                // var classperiodday = _IClassPeriodService.GetAllClassPeriodDays(ref count).Where(m => m.DayId == day.DayId).ToList();
                                // var ClassPeriodDetailIdarray = classperiodday.Count > 0 ? classperiodday.Select(m => (int)m.ClassPeriodDetailId).ToArray() : new int[] { };
                                // ClassPeriodDetailIdarray = classperioddetails.Where(m => ClassPeriodDetailIdarray.Contains((int)m.ClassPeriodDetailId)).Select(m => m.ClassPeriodDetailId).ToArray();

                                ClassPeriodDetailIdarray = classperioddetails.Select(m => m.ClassPeriodDetailId).ToArray();
                                // var adjustmentclasssprddetails = _IClassPeriodService.GetAllTimeTableAdjustments(ref count).Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate == DateTime.Now.Date).Select(m => (int)m.ClassPeriodDetailId).ToArray();
                            }

                            var adjustments = _IClassPeriodService.GetAllTimeTableAdjustments(ref count).Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate == DateTime.Now.Date);
                            var adjustmentperiodIds = adjustments.Select(m => m.ClassPeriod.PeriodId).ToArray();
                            var adjustmentClassIds = adjustments.Select(m => m.ClassPeriod.ClassSubject.ClassId).ToArray();
                            var adjustmentclasssprddetails = new List<int>();
                            if (adjustmentClassIds.Count() > 0)
                            {
                                var AdjustmentVirtualClassIds = adjustments.Where(m => m.ClassPeriod.ClassSubject.ClassId == null)
                                                                            .Select(m => (int)m.ClassPeriod.ClassSubject.VirtualClassId).ToArray();

                                //currentday classperiodddetailids
                                var classperioddetailIds_daywise = _IClassPeriodService.GetAllClassPeriodDays(ref count)
                                    .Where(m => m.DayId == DateTime.Now.Day).Select(m => m.ClassPeriodDetailId).ToArray();

                                var classperiodIds = _IClassPeriodService.GetAllClassPeriods(ref count)
                                    .Where(m => m.ClassSubject.VirtualClassId == null && adjustmentperiodIds.Contains(m.PeriodId)
                                        && adjustmentClassIds.Contains(m.ClassSubject.ClassId)).Select(m => m.ClassPeriodId).ToArray();
                                var VirtualclassperiodIds = _IClassPeriodService.GetAllClassPeriods(ref count)
                                    .Where(m => m.ClassSubject.ClassId == null && adjustmentperiodIds.Contains(m.PeriodId)
                                        && AdjustmentVirtualClassIds.Contains((int)m.ClassSubject.VirtualClassId))
                                        .Select(m => m.ClassPeriodId).ToArray();

                                classperiodIds = classperiodIds.Concat(VirtualclassperiodIds).ToArray();

                                adjustmentclasssprddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count)
                                    .Where(m => classperioddetailIds_daywise.Contains(m.ClassPeriodDetailId)
                                        && classperiodIds.Contains((int)m.ClassPeriodId)).Select(m => m.ClassPeriodDetailId).ToList();
                            }

                            var finalClassPeriodDetailIdarray = ClassPeriodDetailIdarray.Union(adjustmentclasssprddetails).Distinct().ToArray();
                            if (finalClassPeriodDetailIdarray.Length > 0)
                            {
                                classperioddetails = classperioddetails.Where(m => finalClassPeriodDetailIdarray
                                                                        .Contains(m.ClassPeriodDetailId)).ToList();
                                var classidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassId)
                                                                    .Distinct().ToList();
                                //add incharged tecaher class in other classes.
                                // classidlist.Add(InchargedClass);

                                classsubjectidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassSubjectId)
                                                                       .Distinct().ToList();

                                //get incharge teacher class 
                                var teacherInfo = _IStaffService.GetAllClassTeachers(ref count, TeacherId: user.UserContactId,
                                                         ClassId: classid).Where(p => p.EffectiveDate <= DateTime.Now.Date
                                                                        && (p.EndDate >= DateTime.Now.Date || p.EndDate == null))
                                                                     .OrderByDescending(x => x.ClassTeacherId).ToList();
                                if (teacherInfo.Count() > 0)
                                {
                                    foreach (var incharge in teacherInfo)
                                    {
                                        InchargedClass = (int)incharge.ClassId;
                                        classidlist.Add(InchargedClass);
                                    }
                                }

                                if (!IsInchargeAllowedForAtn)
                                {
                                    //if not incharge of current class
                                    if (classsubjectidlist.Count > 0)
                                    {
                                        foreach (var classsubjectid in classsubjectidlist)
                                        {
                                            var classsubject = _ISubjectService.GetAllClassSubjects(ref count)
                                                                    .Where(m => m.ClassId != null && m.ClassId == classid
                                                                        && m.ClassSubjectId == classsubjectid && m.IsActive == true).FirstOrDefault();
                                            if (classsubject != null)
                                            {
                                                if (classsubject.SkillId == null)
                                                {
                                                    Subject = classsubject.Subject.Subject1;
                                                    ClassSubjectId = classsubjectid.ToString();

                                                }
                                                else
                                                {
                                                    Subject = _ISubjectService.GetAllSubjectSkills(ref count).Where(m => m.SubjectId == classsubject.SubjectId
                                                                                    && m.SkillId == classsubject.SkillId).FirstOrDefault().SubjectSkill1;
                                                    ClassSubjectId = classsubjectid.ToString();
                                                }
                                                subjectDropdownlist.Add(new SelectListItem { Text = Subject, Value = ClassSubjectId });

                                            }
                                            var Virtualclasssubject = _ISubjectService.GetAllClassSubjects(ref count).Where(m => m.ClassId == null && m.VirtualClassId == VirtualClassId && m.ClassSubjectId == classsubjectid && m.IsActive == true).FirstOrDefault();
                                            if (Virtualclasssubject != null)
                                            {
                                                if (Virtualclasssubject.SkillId == null)
                                                {
                                                    Subject = Virtualclasssubject.Subject.Subject1;
                                                    ClassSubjectId = classsubjectid.ToString();

                                                }
                                                else
                                                {
                                                    Subject = _ISubjectService.GetAllSubjectSkills(ref count).Where(m => m.SubjectId == Virtualclasssubject.SubjectId
                                                                              && m.SkillId == Virtualclasssubject.SkillId).FirstOrDefault().SubjectSkill1;
                                                    ClassSubjectId = classsubjectid.ToString();
                                                }
                                                subjectDropdownlist.Add(new SelectListItem { Text = Subject, Value = ClassSubjectId });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                msg = "Time Table does not Exists"
                            });
                        }
                        #endregion
                    }

                    #region BindSubjectList
                    if (IsInchargeAllowedForAtn)
                    {
                        //if tecaher is incharge of class
                        var classsubject = _ISubjectService.GetAllClassSubjects(ref count).Where(m => m.ClassId != null
                                                && m.ClassId == classid && m.IsActive == true).ToList();
                        foreach (var sub in classsubject)
                        {
                            if (sub.SkillId == null)
                            {
                                Subject = sub.Subject.Subject1;
                                ClassSubjectId = sub.ClassSubjectId.ToString();
                            }
                            else
                            {
                                Subject = _ISubjectService.GetAllSubjectSkills(ref count).Where(m => m.SubjectId == sub.SubjectId
                                                                && m.SkillId == sub.SkillId).FirstOrDefault().SubjectSkill1;
                                ClassSubjectId = sub.ClassSubjectId.ToString();
                            }
                            subjectDropdownlist.Add(new SelectListItem { Text = Subject, Value = ClassSubjectId });
                        }
                    }
                    else
                    {
                        if (!linkedTimetable)
                        {
                            //if tecaher is  not incharge of class
                            if (classid > 0)
                            {
                                var GetAssignedSubject_Ids = _IStaffService.GetAllTeacherSubjectAssigns(ref count,
                                                            TeacherId: user.UserContactId, ClassId: classid)
                                                            .Select(x => x.SubjectId).ToArray();

                                var classsubject = _ISubjectService.GetAllClassSubjects(ref count).Where(m => m.ClassId != null
                                                              && m.ClassId == classid && m.IsActive == true
                                                              && GetAssignedSubject_Ids.Contains(m.SubjectId)).Distinct().ToList();

                                foreach (var sub in classsubject)
                                {
                                    if (sub.SkillId == null)
                                    {
                                        Subject = sub.Subject.Subject1;
                                        ClassSubjectId = sub.ClassSubjectId.ToString();
                                    }
                                    else
                                    {
                                        Subject = _ISubjectService.GetAllSubjectSkills(ref count).Where(m => m.SubjectId == sub.SubjectId
                                                                        && m.SkillId == sub.SkillId).FirstOrDefault().SubjectSkill1;
                                        ClassSubjectId = sub.ClassSubjectId.ToString();
                                    }
                                    subjectDropdownlist.Add(new SelectListItem { Text = Subject, Value = ClassSubjectId });
                                }
                            }
                            else if (VirtualClassId > 0)
                            {
                                var GetAssignedSubject_Ids = _IStaffService.GetAllTeacherSubjectAssigns(ref count,
                                                            TeacherId: user.UserContactId, VirtualId: VirtualClassId)
                                                            .Select(x => x.SubjectId).ToArray();

                                var VirtualClasssubject = _ISubjectService.GetAllClassSubjects(ref count).Where(m => m.ClassId == null
                                                            && m.VirtualClassId == VirtualClassId && m.IsActive == true
                                                            && GetAssignedSubject_Ids.Contains(m.SubjectId)).ToList();

                                foreach (var sub in VirtualClasssubject)
                                {
                                    if (sub.SkillId == null)
                                    {
                                        Subject = sub.Subject.Subject1;
                                        ClassSubjectId = sub.ClassSubjectId.ToString();
                                    }
                                    else
                                    {
                                        Subject = _ISubjectService.GetAllSubjectSkills(ref count).Where(m => m.SubjectId == sub.SubjectId
                                                                        && m.SkillId == sub.SkillId).FirstOrDefault().SubjectSkill1;
                                        ClassSubjectId = sub.ClassSubjectId.ToString();
                                    }
                                    subjectDropdownlist.Add(new SelectListItem { Text = Subject, Value = ClassSubjectId });
                                }
                            }
                        }
                    }
                    #endregion

                    return Json(new
                    {
                        status = "success",
                        subjectDropdownlist = subjectDropdownlist,
                    });
                }
                else
                {
                    #region Admin
                    var classsubject = _ISubjectService.GetAllClassSubjects(ref count).Where(m => m.ClassId != null && m.ClassId == classid
                                            && m.IsActive == true);

                    foreach (var classsubjectitem in classsubject)
                    {
                        if (classsubjectitem.SkillId == null)
                        {
                            Subject = classsubjectitem.Subject.Subject1;
                            ClassSubjectId = classsubjectitem.ClassSubjectId.ToString();

                        }
                        else
                        {
                            var Subjectlist = _ISubjectService.GetAllSubjectSkills(ref count).Where(m => m.SubjectId == classsubjectitem.SubjectId
                                                                    && m.SkillId == classsubjectitem.SkillId).FirstOrDefault();
                            if (Subjectlist != null)
                            {
                                Subject = Subjectlist.SubjectSkill1;
                                ClassSubjectId = classsubjectitem.ClassSubjectId.ToString();
                            }
                        }
                        subjectDropdownlist.Add(new SelectListItem { Text = Subject, Value = ClassSubjectId });
                    }
                    var Virtualclasssubject = _ISubjectService.GetAllClassSubjects(ref count).Where(m => m.ClassId == null
                                                                && m.VirtualClassId == VirtualClassId && m.IsActive == true);
                    foreach (var Virtualclasssubjectitem in Virtualclasssubject)
                    {
                        if (Virtualclasssubjectitem.SkillId == null)
                        {
                            Subject = Virtualclasssubjectitem.Subject.Subject1;
                            ClassSubjectId = Virtualclasssubjectitem.ClassSubjectId.ToString();

                        }
                        else
                        {
                            var SubjectList = _ISubjectService.GetAllSubjectSkills(ref count).Where(m => m.SubjectId == Virtualclasssubjectitem.SubjectId
                                                    && m.SkillId == Virtualclasssubjectitem.SkillId).FirstOrDefault();

                            if (SubjectList != null)
                            {
                                Subject = SubjectList.SubjectSkill1;
                                ClassSubjectId = Virtualclasssubjectitem.ClassSubjectId.ToString();
                            }
                        }
                        subjectDropdownlist.Add(new SelectListItem { Text = Subject, Value = ClassSubjectId });
                    }

                    return Json(new
                    {
                        status = "success",
                        subjectDropdownlist = subjectDropdownlist,
                    });

                    #endregion
                }




            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                });
            }
        }

        public ActionResult GetClassListMonthWise(string date)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                var studentid = 0;
                var staffid = 0;

                switch (user.ContactType.ContactType1.ToLower())
                {
                    case "student":
                        studentid = (int)user.UserContactId;
                        break;

                    case "guardian":
                        var guardian = _IGuardianService.GetGuardianById((int)user.UserContactId);
                        studentid = guardian != null ? (int)guardian.StudentId : 0;
                        break;

                    case "teacher":
                        staffid = (int)user.UserContactId;
                        break;

                    default:
                        studentid = 0;
                        staffid = 0;
                        break;
                }

                var classDropdownlist = new List<CustomSelectClassitem>();
                var startdate = new DateTime();
                var enddate = new DateTime();
                var selecteddate = (date);
                var selectedDay = new List<string>();
                var splitdate = selecteddate.Split('/');
                var currentYear = Convert.ToInt32(splitdate[0]);
                var currentMonth = Convert.ToInt32(splitdate[1]);
                //var currentMonth = DateTime.ParseExact(splitdate[1], "MMMM", new CultureInfo("en-US")).Month;
                var dateslist = GetDates(currentYear, currentMonth);
                if (dateslist.Count > 0)
                    foreach (var datelist in dateslist)
                        selectedDay.Add(datelist.DayOfWeek.ToString());
                selectedDay.Distinct();
                startdate = dateslist.FirstOrDefault().Date;
                enddate = dateslist.LastOrDefault().Date;

                DateTime? CurrDate = null;
                if (!string.IsNullOrEmpty(date) || !string.IsNullOrWhiteSpace(date))
                {
                    CurrDate = DateTime.ParseExact(date, "dd/MM/yyyy", null);
                    CurrDate = CurrDate.Value.Date;
                }
                int InchargedClass = 0;
                if (staffid > 0)
                {
                    var days = _IClassPeriodService.GetAllDayMasters(ref count).Where(m => selectedDay.Contains(m.DayName)).ToList();
                    var day = days.Select(m => m.DayId).ToArray();
                    var timetables = _IClassPeriodService.GetAllTimeTables(ref count).ToList();
                    var timetable = timetables.Where(t => (t.EffectiveFrom.Value.Date >= startdate.Date || t.EffectiveFrom.Value.Date <= startdate.Date) && (t.EffectiveTill.Value.Date <= enddate.Date || t.EffectiveTill.Value.Date >= enddate.Date)).ToList();
                    if (timetable != null)
                    {

                        // get all class period detail by time table andt  teacher
                        var classperioddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count, TeacherId: user.UserContactId)
                                                    .Where(m => timetable.Select(n => n.TimeTableId).ToArray()
                                                                        .Contains((int)m.TimeTableId)).ToList();
                        if (classperioddetails.Count > 0)
                        {
                            var classperiodday = _IClassPeriodService.GetAllClassPeriodDays(ref count)
                                                                .Where(m => day.Contains((int)m.DayId)).ToList();

                            var ClassPeriodDetailIdarray = classperiodday.Count > 0 ? classperiodday
                                                            .Select(m => (int)m.ClassPeriodDetailId)
                                                            .Distinct().ToArray() : new int[] { };
                            ClassPeriodDetailIdarray = classperioddetails.Where(m => ClassPeriodDetailIdarray
                                                        .Contains((int)m.ClassPeriodDetailId)).Select(m => m.ClassPeriodDetailId)
                                                        .ToArray();
                            // var adjustmentclasssprddetails = _IClassPeriodService.GetAllTimeTableAdjustments(ref count).Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate.Value.Date >= startdate.Date && m.AdjustmentDate.Value.Date >= enddate.Date).Select(m => (int)m.ClassPeriodDetailId).ToArray();

                            var adjustments = _IClassPeriodService.GetAllTimeTableAdjustments(ref count)
                                                            .Where(m => m.TeacherId == user.UserContactId
                                                                && m.AdjustmentDate == DateTime.Now.Date);
                            var adjustmentperiodIds = adjustments.Select(m => m.ClassPeriod.PeriodId).ToArray();
                            var adjustmentClassIds = adjustments.Select(m => m.ClassPeriod.ClassSubject.ClassId).ToArray();
                            //currentday classperiodddetailids
                            var classperioddetailIds_daywise = _IClassPeriodService.GetAllClassPeriodDays(ref count)
                                .Where(m => m.DayId == DateTime.Now.Day).Select(m => m.ClassPeriodDetailId).ToArray();

                            var classperiodIds = _IClassPeriodService.GetAllClassPeriods(ref count)
                                                    .Where(m => m.ClassSubject.VirtualClassId == null
                                                        && adjustmentperiodIds.Contains(m.PeriodId)
                                                        && adjustmentClassIds.Contains(m.ClassSubject.ClassId))
                                                        .Select(m => m.ClassPeriodId).ToArray();

                            var adjustmentclasssprddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count)
                                                            .Where(m => classperioddetailIds_daywise.Contains(m.ClassPeriodDetailId)
                                                                && classperiodIds.Contains((int)m.ClassPeriodId))
                                                                .Select(m => m.ClassPeriodDetailId).ToList();

                            var finalClassPeriodDetailIdarray = ClassPeriodDetailIdarray.Union(ClassPeriodDetailIdarray)
                                                                                        .Distinct().ToArray();
                            if (finalClassPeriodDetailIdarray.Length > 0)
                            {
                                classperioddetails = classperioddetails.Where(m => finalClassPeriodDetailIdarray
                                                                         .Contains(m.ClassPeriodDetailId)).ToList();
                                var classidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassId)
                                                                     .Distinct().ToList();
                                var classsubjectidlist = classperioddetails.Where(m => m.ClassPeriod != null)
                                                            .Select(m => m.ClassPeriod.ClassSubject.ClassSubjectId).ToList();
                                var Virtualclassidlist = classperioddetails.Where(m => m.ClassPeriod != null
                                                            && m.ClassPeriod.ClassSubject.ClassId == null)
                                                            .Select(m => m.ClassPeriod.ClassSubject.VirtualClassId).Distinct().ToList();


                                //get incharge teacher class 
                                var teacherInfo = _IStaffService.GetAllClassTeachers(ref count, TeacherId: user.UserContactId)
                                                                    .Where(p => p.EffectiveDate <= DateTime.Now.Date
                                                                        && (p.EndDate >= DateTime.Now.Date || p.EndDate == null))
                                                                     .OrderByDescending(x => x.ClassTeacherId).ToList();
                                if (teacherInfo.Count() > 0)
                                {
                                    foreach (var incharge in teacherInfo)
                                    {
                                        InchargedClass = (int)incharge.ClassId;
                                        classidlist.Add(InchargedClass);
                                    }
                                }

                                if ((classidlist.Count > 0 || Virtualclassidlist.Count > 0) && classsubjectidlist.Count > 0)
                                {
                                    foreach (var classid in classidlist.Distinct())
                                    {
                                        var classname = _ICatalogMasterService.GetClassMasterById((int)classid).Class;
                                        classDropdownlist.Add(new CustomSelectClassitem
                                        {
                                            Text = classname,
                                            VirtualClassValue = "0",
                                            Value = classid.ToString()
                                        });
                                    }
                                    foreach (var virtualclassid in Virtualclassidlist)
                                    {
                                        var classname = _ICatalogMasterService.GetAllVirtualClasses(ref count)
                                                                                .Where(m => m.VirtualClassId == virtualclassid)
                                                                                .FirstOrDefault().VirtualClass1;
                                        classDropdownlist.Add(new CustomSelectClassitem
                                        {
                                            Text = classname,
                                            Value = "0",
                                            VirtualClassValue = virtualclassid.ToString()
                                        });
                                    }
                                }
                            }
                        }
                    }
                    else
                    {

                        return Json(new
                        {
                            status = "failed",
                            msg = "Time Table does not Exists"
                        });
                    }
                }
                else if (studentid > 0)
                {
                    // get class by student id
                    var session = _ICatalogMasterService.GetCurrentSession();
                    var stduentclass = _IStudentService.GetAllStudentClasss(ref count, SessionId: session.SessionId, StudentId: studentid);
                    foreach (var stdclass in stduentclass)
                        classDropdownlist.Add(new CustomSelectClassitem { Text = stdclass.ClassMaster.Class, Value = stdclass.ClassId.ToString() });
                }
                else
                {
                    var session = _ICatalogMasterService.GetCurrentSession();
                    var stduentclass = _ICatalogMasterService.GetAllClassMasters(ref count);
                    foreach (var stdclass in stduentclass)
                        classDropdownlist.Add(new CustomSelectClassitem { Text = stdclass.Class, Value = stdclass.ClassId.ToString() });
                }
                return Json(new
                {
                    status = "success",
                    classDropdownlist = classDropdownlist
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                });
            }
        }

        public ActionResult GetClassList(string date)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                var studentid = 0;
                var staffid = 0;

                switch (user.ContactType.ContactType1.ToLower())
                {
                    case "student":
                        studentid = (int)user.UserContactId;
                        break;

                    case "guardian":
                        var guardian = _IGuardianService.GetGuardianById((int)user.UserContactId);
                        studentid = guardian != null ? (int)guardian.StudentId : 0;
                        break;

                    case "teacher":
                        staffid = (int)user.UserContactId;
                        break;

                    default:
                        studentid = 0;
                        staffid = 0;
                        break;
                }

                var classDropdownlist = new List<CustomSelectClassitem>();
                int InchargedClass = 0;
                if (staffid > 0)
                {
                    var selecteddate = Convert.ToDateTime(date);
                    var selectedDay = selecteddate.DayOfWeek;
                    var day = _IClassPeriodService.GetAllDayMasters(ref count).Where(m => m.DayName == selectedDay.ToString()).FirstOrDefault();
                    var timetables = _IClassPeriodService.GetAllTimeTables(ref count).ToList();
                    var timetable = timetables.Where(t => t.EffectiveFrom <= selecteddate.Date && t.EffectiveTill >= selecteddate.Date).FirstOrDefault();
                    if (timetable != null)
                    {
                        // get all class period detail by time table andt  teacher
                        var classperioddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count, TeacherId: user.UserContactId, TimeTableId: timetable.TimeTableId).ToList();
                        if (classperioddetails.Count > 0)
                        {
                            var classperiodday = _IClassPeriodService.GetAllClassPeriodDays(ref count).Where(m => m.DayId == day.DayId).ToList();
                            var ClassPeriodDetailIdarray = classperiodday.Count > 0 ? classperiodday.Select(m => (int)m.ClassPeriodDetailId).ToArray() : new int[] { };
                            ClassPeriodDetailIdarray = classperioddetails.Where(m => ClassPeriodDetailIdarray.Contains((int)m.ClassPeriodDetailId)).Select(m => m.ClassPeriodDetailId).ToArray();

                            //  var adjustmentclasssprddetails = _IClassPeriodService.GetAllTimeTableAdjustments(ref count).Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate == selecteddate.Date).Select(m => (int)m.ClassPeriodDetailId).ToArray();

                            var adjustments = _IClassPeriodService.GetAllTimeTableAdjustments(ref count).Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate == DateTime.Now.Date);
                            var adjustmentperiodIds = adjustments.Select(m => m.ClassPeriod.PeriodId).ToArray();
                            var adjustmentClassIds = adjustments.Select(m => m.ClassPeriod.ClassSubject.ClassId).ToArray();
                            //currentday classperiodddetailids
                            var classperioddetailIds_daywise = _IClassPeriodService.GetAllClassPeriodDays(ref count).Where(m => m.DayId == DateTime.Now.Day).Select(m => m.ClassPeriodDetailId).ToArray();

                            var classperiodIds = _IClassPeriodService.GetAllClassPeriods(ref count).Where(m => m.ClassSubject.VirtualClassId == null && adjustmentperiodIds.Contains(m.PeriodId) && adjustmentClassIds.Contains(m.ClassSubject.ClassId)).Select(m => m.ClassPeriodId).ToArray();

                            var adjustmentclasssprddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count).Where(m => classperioddetailIds_daywise.Contains(m.ClassPeriodDetailId) && classperiodIds.Contains((int)m.ClassPeriodId)).Select(m => m.ClassPeriodDetailId).ToList();

                            var finalClassPeriodDetailIdarray = ClassPeriodDetailIdarray.Union(ClassPeriodDetailIdarray).Distinct().ToArray();
                            if (finalClassPeriodDetailIdarray.Length > 0)
                            {
                                classperioddetails = classperioddetails.Where(m => finalClassPeriodDetailIdarray.Contains(m.ClassPeriodDetailId)).ToList();
                                var classidlist = classperioddetails.Where(m => m.ClassPeriod != null && m.ClassPeriod.ClassSubject.VirtualClass == null).Select(m => m.ClassPeriod.ClassSubject.ClassId).Distinct().ToList();
                                var classsubjectidlist = classperioddetails.Where(m => m.ClassPeriod != null).Select(m => m.ClassPeriod.ClassSubject.ClassSubjectId).ToList();
                                var Virtualclassidlist = classperioddetails.Where(m => m.ClassPeriod != null && m.ClassPeriod.ClassSubject.ClassId == null).Select(m => m.ClassPeriod.ClassSubject.VirtualClassId).Distinct().ToList();

                                //get incharge teacher class 
                                var teacherInfo = _IStaffService.GetAllClassTeachers(ref count, TeacherId: user.UserContactId)
                                                                    .Where(p => p.EffectiveDate <= DateTime.Now.Date && (p.EndDate >= DateTime.Now.Date || p.EndDate == null))
                                                                     .OrderByDescending(x => x.ClassTeacherId).ToList();
                                if (teacherInfo.Count() > 0)
                                {
                                    foreach (var incharge in teacherInfo)
                                    {
                                        InchargedClass = (int)incharge.ClassId;
                                        classidlist.Add(InchargedClass);
                                    }
                                }

                                if ((classidlist.Count > 0 || Virtualclassidlist.Count > 0) && classsubjectidlist.Count > 0)
                                {
                                    foreach (var classid in classidlist.Distinct())
                                    {
                                        var classname = _ICatalogMasterService.GetClassMasterById((int)classid).Class;
                                        classDropdownlist.Add(new CustomSelectClassitem { Text = classname, Value = classid.ToString(), VirtualClassValue = "0" });
                                    }
                                    foreach (var virtualclassid in Virtualclassidlist)
                                    {
                                        var classname = _ICatalogMasterService.GetAllVirtualClasses(ref count).Where(m => m.VirtualClassId == virtualclassid).FirstOrDefault().VirtualClass1;
                                        classDropdownlist.Add(new CustomSelectClassitem { Text = classname, Value = "0", VirtualClassValue = virtualclassid.ToString() });
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            msg = "Time Table does not Exists"
                        });
                    }
                }
                else if (studentid > 0)
                {
                    // get class by student id
                    var session = _ICatalogMasterService.GetCurrentSession();
                    var stduentclass = _IStudentService.GetAllStudentClasss(ref count, SessionId: session.SessionId, StudentId: studentid);
                    foreach (var stdclass in stduentclass)
                        classDropdownlist.Add(new CustomSelectClassitem { Text = stdclass.ClassMaster.Class, Value = stdclass.ClassId.ToString() });
                }
                else
                {
                    var session = _ICatalogMasterService.GetCurrentSession();
                    var stduentclass = _ICatalogMasterService.GetAllClassMasters(ref count);
                    foreach (var stdclass in stduentclass)
                        classDropdownlist.Add(new CustomSelectClassitem { Text = stdclass.Class, Value = stdclass.ClassId.ToString() });

                }

                return Json(new
                {
                    status = "success",
                    classDropdownlist = classDropdownlist
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                });
            }
        }

        //public ActionResult GetHomeWorkList(string clsId, string date)
        //{
        //    SchoolUser user = new SchoolUser();
        //    // current user
        //    if (HttpContext.Session["SchoolDB"] != null)
        //    {
        //        // re-assign session
        //        var sessiondb = HttpContext.Session["SchoolDB"];
        //        HttpContext.Session["SchoolDB"] = sessiondb;

        //        user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
        //        if (user == null)
        //            return RedirectToAction("Login", "Home");
        //        SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
        //    }
        //    try
        //    {
        //        var SelectedDate = Convert.ToDateTime(date);
        //        var ClassId = Convert.ToInt32(clsId);
        //        var selectedDay = SelectedDate.DayOfWeek;
        //        var day = _IClassPeriodService.GetAllDayMasters(ref count).Where(m => m.DayName == selectedDay.ToString()).FirstOrDefault();
        //        var timetables = _IClassPeriodService.GetAllTimeTables(ref count).ToList();
        //        var timetable = timetables.Where(t => t.EffectiveFrom <= SelectedDate.Date && t.EffectiveTill >= SelectedDate.Date).FirstOrDefault();
        //        List<int> ClassSubjectIdList = new List<int>();

        //        if (timetable != null)
        //        {
        //            // get all class period detail by time table andt  teacher
        //            var classperioddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count, TeacherId: user.UserContactId, TimeTableId: timetable.TimeTableId).ToList();
        //            if (classperioddetails.Count > 0)
        //            {
        //                var classperiodday = _IClassPeriodService.GetAllClassPeriodDays(ref count).Where(m => m.DayId == day.DayId).ToList();

        //                var ClassPeriodDetailIdarray = classperiodday.Count > 0 ? classperiodday.Select(m => (int)m.ClassPeriodDetailId).ToArray() : new int[] { };

        //                ClassPeriodDetailIdarray = classperioddetails.Where(m => ClassPeriodDetailIdarray.Contains((int)m.ClassPeriodDetailId)).Select(m => m.ClassPeriodDetailId).ToArray();

        //                var adjustmentclasssprddetails = _IClassPeriodService.GetAllTimeTableAdjustments(ref count).Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate == SelectedDate.Date).Select(m => (int)m.ClassPeriodDetailId).ToArray();
        //                var finalClassPeriodDetailIdarray = ClassPeriodDetailIdarray.Union(ClassPeriodDetailIdarray).Distinct().ToArray();
        //                if (finalClassPeriodDetailIdarray.Length > 0)
        //                {
        //                    classperioddetails = classperioddetails.Where(m => finalClassPeriodDetailIdarray.Contains(m.ClassPeriodDetailId)).ToList();
        //                    var classidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassId).Distinct().ToList();
        //                    var classsubjectidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassSubjectId).ToList();
        //                    if (classidlist.Count > 0 && classsubjectidlist.Count > 0)
        //                    {
        //                        foreach (var classsubjectid in classsubjectidlist)
        //                        {
        //                            var classsubject = _ISubjectService.GetAllClassSubjects(ref count).Where(m => m.ClassId == ClassId && m.ClassSubjectId == classsubjectid && m.IsActive == true).FirstOrDefault();
        //                            if (classsubject != null)
        //                                ClassSubjectIdList.Add(classsubjectid);
        //                        }
        //                    }

        //                }


        //            }

        //        }
        //        var ClassSubjectIdArray = ClassSubjectIdList.ToArray();

        //        var HomeWork = _IHomeWorkService.GetAllHWs(ref count, Date: SelectedDate).Where(m => ClassSubjectIdArray.Contains((int)m.ClassSubjectId));
        //        IList<HomeWorkModel> HomeWorkList = new List<HomeWorkModel>();
        //        foreach (var homework in HomeWork)
        //        {
        //            HomeWorkModel HomeWorkModel = new HomeWorkModel();

        //            HomeWorkModel.HWId = homework.HWId;
        //            if (homework.ClassSubjectId > 0)
        //            {
        //                var classSubjectId = (int)homework.ClassSubjectId;
        //                HomeWorkModel.ClassName = homework.ClassSubject.ClassMaster.Class;
        //                HomeWorkModel.Subject = homework.ClassSubject.Subject.Subject1;
        //            }
        //            if (homework.HWTypeId > 0)
        //                HomeWorkModel.HomeWorkType = homework.HWType.HWType1;
        //            HomeWorkList.Add(HomeWorkModel);
        //        }
        //        return Json(new
        //        {
        //            status = "success",
        //            HomeWorkList = HomeWorkList
        //        });
        //    }
        //    catch (Exception ex)
        //    {
        //        _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

        //        return Json(new
        //        {
        //            status = "Error",
        //            msg = ex.Message
        //        });
        //    }
        //}

        public ActionResult GetHomeWorkList(DataSourceRequest command, HomeWorkModel HomeWorkModel, bool Inactive = false, IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            string strName = "";
            if (!string.IsNullOrEmpty(HomeWorkModel.IsDateormonth))
                strName = HomeWorkModel.IsDateormonth;

            var selectedDay = new List<string>();
            var startdate = new DateTime();
            var enddate = new DateTime();
            var VirtualClassId = Convert.ToInt32(HomeWorkModel.VirtualClassId);
            int? StaffId = null;
            if (HomeWorkModel.StaffId > 0)
                StaffId = Convert.ToInt32(HomeWorkModel.StaffId);

            int? ClassId = null;
            if (HomeWorkModel.ClassId > 0)
                ClassId = Convert.ToInt32(HomeWorkModel.ClassId);

            int? SubjectId = 0;
            if (HomeWorkModel.SubjectId > 0)
                SubjectId = Convert.ToInt32(HomeWorkModel.SubjectId);

            if (strName == "Monthly")
            {
                var splitdate = HomeWorkModel.Month.Split('/');
                var currentYear = Convert.ToInt32(splitdate[0]);
                var currentMonth = Convert.ToInt32(splitdate[1]);
                //var currentMonth = DateTime.ParseExact(splitdate[1], "MMMM", new CultureInfo("en-US")).Month;
                var dateslist = GetDates(currentYear, currentMonth);
                if (dateslist.Count > 0)
                    foreach (var date in dateslist)
                        selectedDay.Add(date.DayOfWeek.ToString());
                selectedDay.Distinct();
                startdate = dateslist.FirstOrDefault().Date;
                enddate = dateslist.LastOrDefault().Date;
            }
            else
            {
                DateTime? SelectedDate = null;
                if (!string.IsNullOrEmpty(HomeWorkModel.Date) || !string.IsNullOrWhiteSpace(HomeWorkModel.Date))
                {
                    SelectedDate = DateTime.ParseExact(HomeWorkModel.Date, "dd/MM/yyyy", null);
                    SelectedDate = SelectedDate.Value.Date;
                }
                selectedDay.Add(SelectedDate.Value.DayOfWeek.ToString());
                startdate = SelectedDate.Value.Date;
                enddate = SelectedDate.Value.Date;
            }
            var days = _IClassPeriodService.GetAllDayMasters(ref count).Where(m => selectedDay.Contains(m.DayName)).ToList();
            var day = days.Select(m => m.DayId).ToArray();
            var timetables = _IClassPeriodService.GetAllTimeTables(ref count).ToList();
            var timetable = new List<TimeTable>();
            if (strName == "Monthly")
            {
                timetable = timetables.Where(t => (t.EffectiveFrom.Value.Date >= startdate.Date 
                    || t.EffectiveFrom.Value.Date <= startdate.Date) && (t.EffectiveTill.Value.Date <= enddate.Date 
                    || t.EffectiveTill.Value.Date >= enddate.Date)).ToList();
            }
            else
            {
                timetable = timetables.Where(t => t.EffectiveFrom.Value.Date >= startdate.Date 
                                            && t.EffectiveTill.Value.Date >= enddate.Date).ToList();
            }
            var studentid = 0;
            var staffid = 0;

            switch (user.ContactType.ContactType1.ToLower())
            {
                case "student":
                    studentid = (int)user.UserContactId;
                    break;

                case "guardian":
                    var guardian = _IGuardianService.GetGuardianById((int)user.UserContactId);
                    studentid = guardian != null ? (int)guardian.StudentId : 0;
                    break;

                case "teacher":
                    staffid = (int)user.UserContactId;
                    StaffId = staffid;
                    break;

                default:
                    studentid = 0;
                    staffid = 0;
                    break;
            }

            IList<int> ClassSubjectIdList = new List<int>();
            int InchargedClass = 0;
            if (staffid > 0)
            {
                #region StaffLogin
                if (timetable != null)
                {
                    // get all class period detail by time table andt  teacher
                    var classperioddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count, TeacherId: user.UserContactId).Where(m => timetable.Select(n => n.TimeTableId).ToArray().Contains((int)m.TimeTableId)).ToList();
                    if (classperioddetails.Count > 0)
                    {
                        var classperiodday = _IClassPeriodService.GetAllClassPeriodDays(ref count).Where(m => day.Contains((int)m.DayId)).ToList();

                        var ClassPeriodDetailIdarray = classperiodday.Count > 0 ? classperiodday.Select(m => (int)m.ClassPeriodDetailId).Distinct().ToArray() : new int[] { };

                        ClassPeriodDetailIdarray = classperioddetails.Where(m => ClassPeriodDetailIdarray.Contains((int)m.ClassPeriodDetailId)).Select(m => m.ClassPeriodDetailId).ToArray();

                        // var adjustmentclasssprddetails = _IClassPeriodService.GetAllTimeTableAdjustments(ref count).Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate >= startdate.Date && m.AdjustmentDate >= enddate.Date).Select(m => (int)m.ClassPeriodDetailId).ToArray();

                        var adjustments = _IClassPeriodService.GetAllTimeTableAdjustments(ref count).Where(m => m.TeacherId == user.UserContactId && m.AdjustmentDate == DateTime.Now.Date);
                        var adjustmentperiodIds = adjustments.Select(m => m.ClassPeriod.PeriodId).ToArray();
                        var adjustmentClassIds = adjustments.Select(m => m.ClassPeriod.ClassSubject.ClassId).ToArray();

                        var AdjustmentVirtualClassIds = adjustments.Where(m => m.ClassPeriod.ClassSubject.ClassId == null).Select(m => (int)m.ClassPeriod.ClassSubject.VirtualClassId).ToArray();



                        //currentday classperiodddetailids
                        var classperioddetailIds_daywise = _IClassPeriodService.GetAllClassPeriodDays(ref count).Where(m => m.DayId == DateTime.Now.Day).Select(m => m.ClassPeriodDetailId).ToArray();

                        var classperiodIds = _IClassPeriodService.GetAllClassPeriods(ref count).Where(m => m.ClassSubject.VirtualClassId == null && adjustmentperiodIds.Contains(m.PeriodId) && adjustmentClassIds.Contains(m.ClassSubject.ClassId)).Select(m => m.ClassPeriodId).ToArray();

                        var VirtualclassperiodIds = _IClassPeriodService.GetAllClassPeriods(ref count).Where(m => m.ClassSubject.ClassId == null && adjustmentperiodIds.Contains(m.PeriodId) && AdjustmentVirtualClassIds.Contains((int)m.ClassSubject.VirtualClassId)).Select(m => m.ClassPeriodId).ToArray();

                        classperiodIds = classperiodIds.Concat(VirtualclassperiodIds).ToArray();

                        var adjustmentclasssprddetails = _IClassPeriodService.GetAllClassPeriodDetails(ref count).Where(m => classperioddetailIds_daywise.Contains(m.ClassPeriodDetailId) && m.ClassPeriodId != null && classperiodIds.Contains((int)m.ClassPeriodId)).Select(m => m.ClassPeriodDetailId).ToList();

                        var finalClassPeriodDetailIdarray = ClassPeriodDetailIdarray.Union(adjustmentclasssprddetails).Distinct().ToArray();
                        if (finalClassPeriodDetailIdarray.Length > 0)
                        {
                            classperioddetails = classperioddetails.Where(m => finalClassPeriodDetailIdarray.Contains(m.ClassPeriodDetailId)).ToList();
                            var classidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassId).Distinct().ToList();
                            //get incharge teacher class 
                            var teacherInfo = _IStaffService.GetAllClassTeachers(ref count, TeacherId: user.UserContactId)
                                                                .Where(p => p.EffectiveDate <= DateTime.Now.Date && (p.EndDate >= DateTime.Now.Date || p.EndDate == null))
                                                                 .OrderByDescending(x => x.ClassTeacherId).ToList();
                            if (teacherInfo.Count() > 0)
                            {
                                foreach (var incharge in teacherInfo)
                                {
                                    InchargedClass = (int)incharge.ClassId;
                                    classidlist.Add(InchargedClass);
                                }
                            }
                            var classsubjectidlist = classperioddetails.Select(m => m.ClassPeriod.ClassSubject.ClassSubjectId).ToList();
                            if (classsubjectidlist.Count > 0)
                            {
                                foreach (var classsubjectid in classsubjectidlist)
                                {
                                    if (ClassId > 0)
                                    {
                                        var classsubject = _ISubjectService.GetAllClassSubjects(ref count).Where(m => m.ClassId == ClassId && m.ClassSubjectId == classsubjectid && m.IsActive == true).FirstOrDefault();
                                        if (classsubject != null)
                                            ClassSubjectIdList.Add(classsubjectid);
                                    }
                                    else if (VirtualClassId > 0)
                                    {
                                        var classsubject = _ISubjectService.GetAllClassSubjects(ref count).Where(m => m.VirtualClassId == VirtualClassId && m.ClassSubjectId == classsubjectid && m.IsActive == true).FirstOrDefault();
                                        if (classsubject != null)
                                            ClassSubjectIdList.Add(classsubjectid);
                                    }
                                    else
                                    {
                                        var classsubject = _ISubjectService.GetAllClassSubjects(ref count).Where(m => m.IsActive == true && m.ClassSubjectId == classsubjectid).ToList();
                                        if (classsubject.Count > 0)
                                            foreach (var CLS in classsubject)
                                                ClassSubjectIdList.Add(CLS.ClassSubjectId);
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            else if (studentid > 0)
            {
                #region Student login
                // get student class subject
                // get class by student id
                var session = _ICatalogMasterService.GetCurrentSession();
                var stduentclass = _IStudentService.GetAllStudentClasss(ref count, SessionId: session.SessionId, StudentId: studentid).FirstOrDefault();
                // get class subject
                var classsubjects = _ISubjectService.GetAllClassSubjects(ref count);
                var classsubject = new List<ClassSubject>();
                if (stduentclass != null)
                {
                    classsubject = classsubjects.Where(c => c.ClassId == stduentclass.ClassId && c.IsActive == true).ToList();
                    var virtualclassIds = stduentclass.VirtualClassStudentIds.Where(m => m.VirtualClassSession.SessionId == session.SessionId).Select(m => m.VirtualClassSession.VirtualClassId).ToList();
                    var virtualclasssubject = classsubjects.Where(c => c.ClassId == null && virtualclassIds.Contains(c.VirtualClassId) && c.IsActive == true).ToList();
                    classsubject = classsubject.Concat(virtualclasssubject).ToList();
                }

                foreach (var item in classsubject)
                    ClassSubjectIdList.Add(item.ClassSubjectId);

                #endregion
            }


            var ClassSubjectIdArray = ClassSubjectIdList.ToArray();
            IEnumerable<HW> HomeWork = new List<HW>();
            if (strName == "Monthly")
            {
                HomeWork = _IHomeWorkService.GetAllHWs(ref count).Where(m => m.Date.Value.Date >= startdate
                                               && m.Date.Value.Date <= enddate);
            }
            else
            {
                HomeWork = _IHomeWorkService.GetAllHWs(ref count).Where(m =>  m.Date.Value.Date == startdate.Date);
            }

            if (studentid == 0 && staffid == 0)
            {


                if (VirtualClassId > 0)
                    HomeWork = HomeWork.Where(m => m.ClassSubject.VirtualClassId == VirtualClassId);

                if (ClassId > 0)
                    HomeWork = HomeWork.Where(m => m.ClassSubject.ClassId == ClassId);

                if (SubjectId > 0)
                    HomeWork = HomeWork.Where(m => m.ClassSubject.SubjectId == SubjectId);

                if (StaffId > 0)
                    HomeWork = HomeWork.Where(m => m.StaffId == StaffId);

                //if (ClassId > 0 && StaffId > 0 && SubjectId > 0)
                //    HomeWork = HomeWork.Where(x=>x.StaffId== StaffId && x.ClassSubject.SubjectId == 
                //                      && x.ClassSubject.ClassId == ClassId 
                //                     && x.Date.Value.Date >= startdate && x.Date.Value.Date <= enddate);
                //else if (ClassId > 0 && StaffId > 0)
                //    HomeWork = HomeWork.Where(m => m.StaffId == StaffId && m.ClassSubject.ClassId == ClassId
                //                    && m.Date.Value.Date >= startdate && m.Date.Value.Date <= enddate);
                //else if (ClassId > 0 && SubjectId > 0)
                //    HomeWork =  HomeWork.Where(m=>m.ClassSubject.SubjectId == SubjectId
                //                        && m.ClassSubject.ClassId == ClassId 
                //                        && m.Date.Value.Date >= startdate 
                //                        && m.Date.Value.Date <= enddate);
                //else if (ClassId > 0)
                //    HomeWork = HomeWork.Where(m => m.ClassSubject.ClassId == ClassId
                //                            && m.Date.Value.Date >= startdate
                //                            && m.Date.Value.Date <= enddate);

                //if (VirtualClassId > 0 && StaffId > 0)
                //    HomeWork = _IHomeWorkService.GetAllHWs(ref count, StaffId: StaffId, SubjectId: SubjectId)
                //                                .Where(m => m.ClassSubject.VirtualClassId == VirtualClassId 
                //                                    && m.Date.Value.Date >= startdate.Date
                //                                    && m.Date.Value.Date <= enddate.Date);
                //else if (VirtualClassId > 0)
                //    HomeWork = _IHomeWorkService.GetAllHWs(ref count, SubjectId: SubjectId)
                //                                .Where(m => m.ClassSubject.VirtualClassId == VirtualClassId 
                //                                                && m.Date.Value.Date >= startdate.Date
                //                                                && m.Date.Value.Date <= enddate.Date);
                //else if (StaffId > 0 && SubjectId > 0)
                //    HomeWork = _IHomeWorkService.GetAllHWs(ref count, StaffId: StaffId, SubjectId: SubjectId)
                //                                .Where(m => m.Date.Value.Date >= startdate.Date 
                //                                        && m.Date.Value.Date <= enddate.Date);
                //else if (StaffId > 0)
                //    HomeWork = _IHomeWorkService.GetAllHWs(ref count, StaffId: StaffId)
                //                                .Where(m => m.Date.Value.Date >= startdate.Date
                //                                        && m.Date.Value.Date <= enddate.Date);
                //else if (SubjectId > 0)
                //    HomeWork = _IHomeWorkService.GetAllHWs(ref count, SubjectId: SubjectId)
                //        .Where(m => m.Date.Value.Date >= startdate.Date && m.Date.Value.Date <= enddate.Date);
            }
            else if (staffid == 0 && SubjectId == 0)
            {
                HomeWork = _IHomeWorkService.GetAllHWs(ref count)
                                            .Where(m => ClassSubjectIdList.Contains((int)m.ClassSubjectId)
                                                && m.Date.Value.Date >= startdate.Date
                                                && m.Date.Value.Date <= enddate.Date).ToList();

            }
            else if (staffid == 0 && SubjectId > 0)
            {
                HomeWork = _IHomeWorkService.GetAllHWs(ref count, SubjectId: SubjectId)
                                     .Where(m => ClassSubjectIdList.Contains((int)m.ClassSubjectId)
                                          && m.Date.Value.Date >= startdate.Date && m.Date.Value.Date <= enddate.Date).ToList();
            }
            else if (staffid > 0)
            {
                //HomeWork = _IHomeWorkService.GetAllHWs(ref count, StaffId: StaffId, SubjectId: SubjectId).Where(m => ClassSubjectIdList.Contains((int)m.ClassSubjectId) && m.Date.Value.Date >= startdate.Date && m.Date.Value.Date <= enddate.Date).ToList();
                HomeWork = _IHomeWorkService.GetAllHWs(ref count).Where(m => m.StaffId == staffid
                                                            && m.Date.Value.Date >= startdate.Date
                                                            && m.Date.Value.Date <= enddate.Date).ToList();
            }
            var IsEditDeleteHW = _ISchoolSettingService.GetorSetSchoolData("AllowEditHWCurrentDate", "true").AttributeValue;
            var HomeWorkData = HomeWork.OrderByDescending(x=>x.Date).PagedForCommand(command).Select(x =>
            {
                var model = new HomeWorkModel();
                model.HWId = x.HWId;
                model.HomeWorkType = x.HWType.HWType1;
                model.ClassName = x.ClassSubject.ClassMaster != null ? x.ClassSubject.ClassMaster.Class : x.ClassSubject.VirtualClass.VirtualClass1;
                model.Subject = x.ClassSubject.Subject.Subject1;
                model.TeacherName = x.Staff.FName + " " + x.Staff.LName;
                model.Date = _ICommonMethodService.ConvertDate((DateTime)x.Date);
                model.IsEditable = false;
                model.IsDeleteable = false;

                //if (x.HWDetails.Count > 1)
                //    model.IsEditable = false;
                if (IsEditDeleteHW.ToLower() == "true")
                {
                    if (x.Date.Value.Date == DateTime.Now.Date)
                    {
                        model.IsEditable = true;
                        model.IsDeleteable = true;
                    }
                }

                return model;
            }).AsQueryable().Sort(sort);

            var gridModel = new DataSourceResult
            {
                Data = HomeWorkData.ToList(),
                Total = HomeWorkData.Count()
            };

            return Json(gridModel);
        }

        public ActionResult GetHomeWorkDetailsList(DataSourceRequest command, HomeWorkModel HomeWorkModel, bool Inactive = false, IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            var HomeWorkdetails = _IHomeWorkService.GetAllHWDetails(ref count).Where(m => m.HWId == HomeWorkModel.HWId);
            var HomweWorkbyId = _IHomeWorkService.GetHWById(HomeWorkModel.HWId, true);
            var schoolid = HttpContext.Session["SchoolId"];
            var HWDataDetail = HomeWorkdetails.Select(x =>
            {
                var model = new HomeWorkModel();
                model.HWDetailId = x.HWDetailId;
                model.HWId = (int)x.HWId;
                model.Description = !string.IsNullOrEmpty(x.Description) ? x.Description : "";
                model.ShortDescription = !string.IsNullOrEmpty(x.Description) ? x.Description.Length > 30 ? x.Description.Substring(0, 30) + "..." : x.Description : "";
                model.TeacherName = HomweWorkbyId.Staff.FName + " " + HomweWorkbyId.Staff.LName;
                model.ClassName = HomweWorkbyId.ClassSubject.ClassMaster != null ? HomweWorkbyId.ClassSubject.ClassMaster.Class : HomweWorkbyId.ClassSubject.VirtualClass.VirtualClass1;
                model.Subject = HomweWorkbyId.ClassSubject.Subject.Subject1;
                model.HomeWorkType = HomweWorkbyId.HWType.HWType1;
                DateTime dttime = Convert.ToDateTime(HomweWorkbyId.Date);
                model.Date = dttime.ToString("dd/MM/yyyy");
                model.Remarks = !string.IsNullOrEmpty(x.Remarks) ? x.Remarks : "";
                model.ShortRemarks = !string.IsNullOrEmpty(x.Remarks) ? x.Remarks.Length > 30 ? x.Remarks.Substring(0, 30) + "..." : x.Remarks : "";

                if (x.FileName != null)
                {
                    model.File = _WebHelper.GetStoreLocation() + "/Images/" + schoolid + "/HomeWork/" + x.FileName;
                    model.FileName = x.FileName;
                }
                else
                {
                    model.FileName = "";
                }

                return model;
            }).AsQueryable().Sort(sort);

            var gridModel = new DataSourceResult
            {
                Data = HWDataDetail.PagedForCommand(command).ToList(),
                Total = HWDataDetail.Count()
            };

            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult UploadFile()
        {
            if (Request.Files.Count > 0)
            {
                string path = String.Empty;
                try
                {
                    int maxfilesize = 100; // default size
                    var ProfilePicMaxSizeSetting = _ISchoolSettingService.GetAttributeValue("ProfilePicMaxSize");
                    if (ProfilePicMaxSizeSetting != null && !string.IsNullOrEmpty(ProfilePicMaxSizeSetting))
                        maxfilesize = Convert.ToInt32(ProfilePicMaxSizeSetting);

                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //file
                        HttpPostedFileBase file = files[i];
                        // get Filename
                        string fileName;
                        string uniqueno = Request.Form["uniqueno"];
                        string uniqueimage = Request.Form["uniqueimage"];
                        var Isphoto = Convert.ToInt32(Request.Form["IsPhoto"]);
                        var randonno = Randomno();
                        fileName = uniqueno + "_" + randonno + Path.GetExtension(file.FileName).ToLower();


                        //Is the file too big to upload?
                        int fileSize = file.ContentLength;
                        if (fileSize > (maxfilesize * 1024))
                        {
                            return Json(new
                            {
                                status = "error",
                                msg = "Filesize of image is too large. Maximum file size permitted is " + maxfilesize + "KB"
                            });
                        }

                        //check that the file is of the permitted file type
                        string fileExtension = Path.GetExtension(fileName).ToLower();

                        string[] acceptedFileTypes = new string[6];
                        acceptedFileTypes[0] = ".jpg";
                        acceptedFileTypes[1] = ".jpeg";
                        acceptedFileTypes[2] = ".png";
                        acceptedFileTypes[3] = ".doc";
                        acceptedFileTypes[4] = ".pdf";
                        acceptedFileTypes[5] = ".xlsx";

                        bool acceptFile = false;
                        //should we accept the file?
                        for (int j = 0; j < acceptedFileTypes.Count(); j++)
                        {
                            if (fileExtension == acceptedFileTypes[j])
                            {
                                //accept the file, yay!
                                acceptFile = true;
                                break;
                            }
                        }

                        if (!acceptFile)
                            return Json(new
                            {
                                status = "error",
                                msg = "The file you are trying to upload is not a permitted file type!"
                            });

                        // check directory exist or not
                        var schoolid = HttpContext.Session["SchoolId"];
                        var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/HomeWork"));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/HomeWork"), fileName);

                        // Get the complete folder path and store the file inside it.  

                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);

                        file.SaveAs(path);
                    }
                    // Returns message that successfully uploaded  
                    return Json(new
                    {
                        status = "success",
                        path = path,
                        msg = "File Uploaded Successfully!"
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        status = "error",
                        msg = "Error occurred. Error details: " + ex.Message
                    });
                }
            }
            else
            {
                return Json(new
                {
                    status = "error",
                    msg = "No files selected."
                });
            }
        }

        //[HttpPost]
        //public ActionResult CheckFileFormat()
        //{
        //    if (Request.Files.Count > 0)
        //    {
        //        string path = String.Empty;
        //        try
        //        {
        //            //  Get all files from Request object  
        //            HttpFileCollectionBase files = Request.Files;
        //            for (int i = 0; i < files.Count; i++)
        //            {
        //                //file
        //                HttpPostedFileBase file = files[i];
        //                // get Filename
        //                string fileName = "";
        //                string unique_no = Request.Form["uniqueno"];
        //                int uniqueno = 0;
        //                // check id exist
        //                if (!string.IsNullOrEmpty(unique_no))
        //                {
        //                    // decrypt Id
        //                    var qsid = _ICustomEncryption.base64d(unique_no);

        //                    if (int.TryParse(qsid, out uniqueno))
        //                    {

        //                    }
        //                }

        //                string uniqueimage = Request.Form["uniqueimage"];

        //                //Is the file too big to upload?
        //                int fileSize = file.ContentLength;
        //                if (fileSize > (maxFileSize * 1024 * 5))
        //                {
        //                    return Json(new
        //                    {
        //                        status = "error",
        //                        msg = "Filesize of image is too large. Maximum file size permitted is " + maxFileSize + "KB"
        //                    });
        //                }

        //                //check that the file is of the permitted file type
        //                string fileExtension = Path.GetExtension(file.FileName).ToLower();

        //                string[] acceptedFileTypes = new string[5];
        //                acceptedFileTypes[0] = ".jpg";
        //                acceptedFileTypes[1] = ".jpeg";
        //                acceptedFileTypes[2] = ".png";
        //                acceptedFileTypes[3] = ".doc";
        //                acceptedFileTypes[4] = ".pdf";

        //                bool acceptFile = false;
        //                //should we accept the file?
        //                for (int j = 0; j < acceptedFileTypes.Count(); j++)
        //                {
        //                    if (fileExtension == acceptedFileTypes[j])
        //                    {
        //                        //accept the file, yay!
        //                        acceptFile = true;
        //                        break;
        //                    }
        //                }

        //                if (!acceptFile)
        //                    return Json(new
        //                    {
        //                        status = "error",
        //                        msg = "The file you are trying to upload is not a permitted file type!"
        //                    });

        //            }
        //            // Returns message that successfully uploaded  
        //            return Json(new
        //            {
        //                status = "success",
        //            });
        //        }
        //        catch (Exception ex)
        //        {
        //            return Json(new
        //            {
        //                status = "error",
        //                msg = "Error occurred. Error details: " + ex.Message
        //            });
        //        }
        //    }
        //    else
        //    {
        //        return Json(new
        //        {
        //            status = "error",
        //            msg = "No files selected."
        //        });
        //    }
        //}

        [HttpPost]
        public ActionResult SaveHomeWork(HomeWorkModel HomeWorkModel)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "HomeWork", "HomeWorkList", "Add");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                // check validations
                if (string.IsNullOrEmpty(HomeWorkModel.Description) && string.IsNullOrEmpty(HomeWorkModel.Remarks) && Request.Files.Count == 0)
                {
                    return Json(new
                    {
                        status = "failed",
                        msg = "Atleast one is mandatory out of File/Description/Remarks!"
                    });
                }

                // Class add
                var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                var HomeWorktable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "HW").FirstOrDefault();
                var HWDetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "HWDetail").FirstOrDefault();

                //get HomeWork if already exists
                HW HomeWork = new HW();
                HWDetail HWDetail = new HWDetail();

                // home work
                if (HomeWorkModel.HWId > 0)
                    HomeWork = _IHomeWorkService.GetHWById(HomeWorkModel.HWId, true);
                else
                    HomeWork = _IHomeWorkService.GetAllHWs(ref count, HWtypeId: HomeWorkModel.HomeWorkTypeId, ClassSubjectId: HomeWorkModel.SubjectId,
                        StaffId: user.UserContactId, Date: DateTime.Now).FirstOrDefault();

                // home work detail
                if (HomeWorkModel.HWDetailId > 0)
                    HWDetail = _IHomeWorkService.GetHWDetailById(HomeWorkModel.HWDetailId, true);
                else if (HomeWorkModel.HWId > 0)
                    HWDetail = HomeWork.HWDetails.FirstOrDefault();

                //get Homework id
                var homeworkid = 0;
                if (HomeWork != null)
                    homeworkid = HomeWork.HWId;

                //Save Home Work If not Already Exists
                if (HomeWork == null)
                {
                    HomeWork = new HW();
                    HomeWork.HWTypeId = HomeWorkModel.HomeWorkTypeId;
                    HomeWork.StaffId = user.UserContactId;
                    HomeWork.ClassSubjectId = HomeWorkModel.SubjectId;
                    HomeWork.Date = DateTime.Now;
                    _IHomeWorkService.InsertHW(HomeWork);

                    homeworkid = HomeWork.HWId;
                    // HomeWork log Table
                    if (HomeWorktable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, HomeWorktable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog(HomeWork.HWId, HomeWorktable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add HomeWork with HWId:{0} and Type:{1}", HomeWork.HWId, HomeWork.HWType), Request.Browser.Browser);
                    }
                }
                else
                {
                    HomeWork.HWTypeId = HomeWorkModel.HomeWorkTypeId;
                    HomeWork.ClassSubjectId = HomeWorkModel.SubjectId;
                    _IHomeWorkService.UpdateHW(HomeWork);

                    homeworkid = HomeWork.HWId;
                    // HomeWork log Table
                    if (HomeWorktable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, HomeWorktable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog(HomeWork.HWId, HomeWorktable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update HomeWork with HWId:{0} and Type:{1}", HomeWork.HWId, HomeWork.HWType), Request.Browser.Browser);
                    }
                }

                if (HomeWorkModel.HWDetailId > 0)
                {
                    HWDetail.Description = HomeWorkModel.Description;
                    HWDetail.Remarks = HomeWorkModel.Remarks;
                    _IHomeWorkService.UpdateHWDetail(HWDetail);
                    // HomeWork log Table
                    if (HWDetailtable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, HWDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog(HWDetail.HWDetailId, HWDetailtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update HomeWorkDetail with HWDetailId:{0} and HWId:{1}", HWDetail.HWDetailId, HWDetail.HWId), Request.Browser.Browser);
                    }
                }
                else
                {
                    //Save HW Details
                    HWDetail = new HWDetail();
                    HWDetail.HWId = homeworkid;
                    HWDetail.Description = HomeWorkModel.Description;
                    HWDetail.Remarks = HomeWorkModel.Remarks;
                    _IHomeWorkService.InsertHWDetail(HWDetail);
                    // HomeWork log Table
                    if (HWDetailtable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, HWDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog(HWDetail.HWDetailId, HWDetailtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add HomeWorkDetail with HWDetailId:{0} and HWId:{1}", HWDetail.HWDetailId, HWDetail.HWId), Request.Browser.Browser);
                    }
                }

                var HWType = _IHomeWorkService.GetHWTypeById(HomeWorkModel.HomeWorkTypeId).HWType1;
                //Save File
                if (Request.Files.Count > 0)
                {
                    string path = String.Empty;
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //file
                        HttpPostedFileBase file = files[i];
                        // get Filename
                        string fileName = "";

                        fileName = homeworkid + "_" + HWDetail.HWDetailId + "_" + HWType + Path.GetExtension(file.FileName).ToLower();
                        var updatehwdetail = _IHomeWorkService.GetHWDetailById((int)HWDetail.HWDetailId, true);
                        updatehwdetail.FileName = fileName;
                        _IHomeWorkService.UpdateHWDetail(updatehwdetail);

                        if (HWDetailtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, HWDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(updatehwdetail.HWDetailId, HWDetailtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update HomeWorkDetail with HWDetailId:{0} and HWId:{1}", updatehwdetail.HWDetailId, updatehwdetail.HWId), Request.Browser.Browser);
                        }

                        // check directory exist or not
                        var schoolid = HttpContext.Session["SchoolId"];
                        var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/HomeWork/"));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/HomeWork/"), fileName);
                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);

                        file.SaveAs(path);
                    }
                }

                return Json(new
                {
                    status = "success",
                    msg = "Home Work Details Uploaded Successfully!"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "error",
                    msg = "Error occurred. Error details: " + ex.Message
                });
            }
        }
        public ActionResult IsHWDescriptionAllow()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "HomeWork", "HomeWorkList", "Add");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                var AllowHWDescription = _ISchoolSettingService.GetorSetSchoolData("EnableHomeWorkDescription", "1").AttributeValue;
               var IsAllowHWDescription = true;
                if (AllowHWDescription == "0")
                    IsAllowHWDescription = false;

                return Json(new
                {
                    status = "success",
                    IsAllow = IsAllowHWDescription
                },JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "error",
                    msg = "Error occurred. Error details: " + ex.Message
                });
            }
        }

        public ActionResult UpdateHWDescriptionAllow(string IsAllowDescription)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "HomeWork", "HomeWorkList", "Add");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                var HWDescriptionPrmssn = _ISchoolSettingService.GetSchoolSettingByAttribute("EnableHomeWorkDescription");
                if (HWDescriptionPrmssn != null && IsAllowDescription=="0"|| IsAllowDescription=="1")
                {
                    HWDescriptionPrmssn.AttributeValue = IsAllowDescription;
                    _ISchoolSettingService.UpdateSchoolSetting(HWDescriptionPrmssn);
                    return Json(new
                    {
                        status = "success",
                        mes="Description status changed Successfully.",
                        IsAllowDescription
                    }, JsonRequestBehavior.AllowGet);
                }
               
                return Json(new
                {
                    status = "Error",
                    mes="Invalid Status"
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "error",
                    msg = "Error occurred. Error details: " + ex.Message
                });
            }
        }
        #endregion
    }
}