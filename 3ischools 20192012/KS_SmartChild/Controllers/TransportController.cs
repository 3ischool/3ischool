﻿using BAL.BAL.Common;
using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ContactModule;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.FeeModule;
using DAL.DAL.GuardianModule;
using DAL.DAL.Kendo;
using DAL.DAL.SettingService;
using DAL.DAL.StudentModule;
using DAL.DAL.TransportModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.API;
using ViewModel.ViewModel.Student;
using ViewModel.ViewModel.Transport;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace KS_SmartChild.Controllers
{
    public class TransportController : Controller
    {
        #region fields

        public int count = 0;
        private readonly IDropDownService _IDropDownService;
        private readonly ILogService _Logger;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStudentService _IStudentService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IWebHelper _WebHelper;
        private readonly IBusService _IBusService;
        private readonly ITransportService _ITransportService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IContactService _IContactService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly IGuardianService _IGuardianService;
        private readonly IFeeService _IFeeService;
        private readonly IModuleVersionService _IModuleVersionService;
        private readonly IExportHelper _IExportHelper;
        private readonly IStudentMasterService _IStudentMasterService;
        #endregion

        #region ctor

        public TransportController(IDropDownService IDropDownService,
            ILogService Logger,
            ICatalogMasterService ICatalogMasterService,
            IStudentService IStudentService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            IUserService IUserService,
            IConnectionService IConnectionService,
            IWebHelper WebHelper,
            IBusService IBusService,
            ITransportService ITransportService,
            ISchoolSettingService ISchoolSettingService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption,
            IContactService IContactService,
            IAddressMasterService IAddressMasterService,
            IGuardianService IGuardianService,
            IFeeService IFeeService, IModuleVersionService IModuleVersionService,
            IExportHelper IExportHelper, IStudentMasterService IStudentMasterService)
        {
            this._IDropDownService = IDropDownService;
            this._Logger = Logger;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IStudentService = IStudentService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._IActivityLogService = IActivityLogService;
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._WebHelper = WebHelper;
            this._IBusService = IBusService;
            this._ITransportService = ITransportService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._IContactService = IContactService;
            this._IAddressMasterService = IAddressMasterService;
            this._IGuardianService = IGuardianService;
            this._IFeeService = IFeeService;
            this._IExportHelper = IExportHelper;
            this._IModuleVersionService = IModuleVersionService;
            this._IStudentMasterService = IStudentMasterService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }

        public DateTime FormatDate(string Date)
        {
            string sdisplayTime = Date.Trim('"');
            var dd = Date.Split('/');
            sdisplayTime = dd[1] + "-" + dd[0] + "-" + dd[2];
            return Convert.ToDateTime(sdisplayTime);
        }

        public string ConvertTime(TimeSpan time, bool Is24HourFormat = false)
        {
            TimeSpan timespan = new TimeSpan(time.Hours, time.Minutes, time.Seconds);
            DateTime datetime = DateTime.Today.Add(timespan);
            string sdisplayTime = "";
            if (Is24HourFormat)
                sdisplayTime = datetime.ToString("HH:mm");
            else
                sdisplayTime = datetime.ToString("hh:mm tt");

            return sdisplayTime;
        }

        #endregion

        #region Methods

        #region Transport Route

        public ActionResult FilterBusRoute(TransportRouteModel model)
        {
            try
            {
                var session = _ICatalogMasterService.GetCurrentSession();
                var allroutes = _IBusService.GetAllBusRoutes(ref count, Routestatus: true);


                if (model.BoardingPointId != null && model.BoardingPointId > 0)
                {
                    var getBusRouteDetails = _IBusService.GetAllBusRouteDetails(ref count, status: true, BoardingPointId: model.BoardingPointId);
                    allroutes = allroutes.Join(getBusRouteDetails, a => a.BusRouteId, b => b.BusRouteId, (a, b) => new { a })
                                          .Select(x=>x.a).ToList();
                }

                if (model.SessionId != null && model.SessionId>0)
                {
                    session = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.SessionId == model.SessionId).FirstOrDefault();
                    allroutes = allroutes.Where(m => m.EffectiveDate <= session.EndDate && m.EndDate>=session.StartDate||m.EffectiveDate>=session.StartDate&&m.EffectiveDate<=session.EndDate||m.EffectiveDate<=session.EndDate&&m.EndDate==null).ToList();
                    
                    //allroutes = allroutes.Where(m => m.EndDate == null || m.EndDate >= session.StartDate).ToList();
                  //  allroutes = allroutes.Where(m =>m.EffectiveDate<=session.EndDate).ToList();
                   // allroutes = allroutes.Where(m => m.EndDate == null || m.EndDate>=session.StartDate).ToList();
                
                }

                
                var StuStatus = _IStudentMasterService.GetAllStudentStatus(ref count, "Active").FirstOrDefault();
                var AllTptFaciliTies = _ITransportService.GetAllTptFacilitys(ref count);
                var current_Session = _ICatalogMasterService.GetCurrentSession();
                var AllStudentClass = _IStudentService.GetAllStudentClasss(ref count, SessionId: current_Session.SessionId);
                var AllBusRouteDetail = _IBusService.GetAllBusRouteDetails(ref count);
                if (model.SessionId > 0)
                    current_Session.SessionId = model.SessionId;

                foreach (var item in allroutes.Distinct())
                {
                    BusRouteList BusRouteList = new BusRouteList();
                    // get bus route bus detail
                    var tptroutedetail = item.TptRouteDetails.ToList();

                    item.BusRouteDetails.Where(x => x.BoardingPointId == model.BoardingPointId).ToList();
                    var tptroutebus = tptroutedetail.Where(t => t.BusId != null && t.EndDate == null).FirstOrDefault();
                    BusRouteList.BusId = tptroutebus != null ? (int)tptroutebus.BusId : 0;
                    BusRouteList.BusName = tptroutebus != null ? tptroutebus.Bus.BusNo : "";
                    // get bus route driver detail
                    var tptroutedriver = tptroutedetail.Where(t => t.DriverId != null && t.EndDate == null).FirstOrDefault();
                    BusRouteList.DriverId = tptroutedriver != null ? (int)tptroutedriver.DriverId : 0;
                    BusRouteList.DriverName = tptroutedriver != null ? tptroutedriver.TptDriver.DriverName : "";
                    BusRouteList.TptRouteDetailId= tptroutedriver != null ? tptroutedriver.TptRouteDetailId.ToString() : "";
                    BusRouteList.BusRouteId = item.BusRouteId;
                    BusRouteList.EncBusRouteId = _ICustomEncryption.base64e(item.BusRouteId.ToString());
                    BusRouteList.BusRouteName = item.BusRoute1;
                    BusRouteList.BusRouteStatus = (bool)item.RouteStatus;
                    BusRouteList.BusRouteNo = item.RouteNo;
                    BusRouteList.EffectiveDate = ConvertDate(item.EffectiveDate.Value);
                    BusRouteList.dtEffectiveDate = (DateTime)item.EffectiveDate;
                    BusRouteList.dtEndDate = item.EndDate;
                    BusRouteList.EndDate = item.EndDate.HasValue ? ConvertDate(item.EndDate.Value) : "";
                    var tptrouteAttendant = tptroutedetail.Where(t => t.TptAttendantId != null && t.EndDate == null).FirstOrDefault();
                    if (tptrouteAttendant != null)
                    {
                        BusRouteList.AttendantId = tptroutedriver != null ? (int)tptroutedriver.TptAttendantId : 0;

                        BusRouteList.AttendantName = "";
                        if (tptroutedriver != null)
                            BusRouteList.AttendantName = tptroutedriver.TptAttendant != null ? tptroutedriver.TptAttendant.AttendantName : "";
                    }
                    BusRouteList.IsDeleted = true;
                    if (item.BusRouteDetails.Count > 0)
                    {
                        BusRouteList.IsDeleted = false;
                        BusRouteList.IsAuthToViewDetails = true;
                    }

                    //var StudentCount = _ITransportService.GetAllTptFacilitys(ref count, BusRoutrId: item.BusRouteId)
                    //                    .Where(m => m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                    //                    && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)).ToList();

                    var StudentCount = AllTptFaciliTies
                                        .Where(m => m.BusRouteId== item.BusRouteId && m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                                        && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)).Count();

                    BusRouteList.StudentCount = StudentCount.ToString();

                   // var current_Session = _ICatalogMasterService.GetCurrentSession();
                    //if (model.SessionId > 0)
                    //    current_Session.SessionId = model.SessionId;

                    //pick students for current session
                    //var PickStudentids = _ITransportService.GetAllTptFacilitys(ref count, BusRoutrId: item.BusRouteId)
                    //                            .Where(m => m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                    //                            && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                    //                            && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                    //                            && (m.TptTypeDetail.TptName.ToLower() == "pick" || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                    //                            .Select(x => x.StudentId).ToArray();

                    var PickStudentids = AllTptFaciliTies
                                              .Where(m => m.BusRouteId == item.BusRouteId && m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                                              && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                              && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                                              && (m.TptTypeDetail.TptName.ToLower() == "pick" || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                                              .Select(x => x.StudentId).ToArray();

                    //var countPickStuCurrentSession = _IStudentService.GetAllStudentClasss(ref count, SessionId: current_Session.SessionId)
                    //                                 .Where(x => PickStudentids.Contains(x.StudentId)).Count();

                    var countPickStuCurrentSession = AllStudentClass.Where(x => PickStudentids.Contains(x.StudentId)).Count();

                    //drop students for current session
                    //var DropStudentids = _ITransportService.GetAllTptFacilitys(ref count)
                    //                            .Where(m => m.DropRouteId == item.BusRouteId
                    //                            && m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                    //                            && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                    //                            && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                    //                            && (m.TptTypeDetail.TptName.ToLower() == "drop" || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                    //                            .Select(x => x.StudentId).ToArray();

                    var DropStudentids = AllTptFaciliTies
                                               .Where(m => m.DropRouteId == item.BusRouteId
                                               && m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                                               && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                               && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                                               && (m.TptTypeDetail.TptName.ToLower() == "drop" || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                                               .Select(x => x.StudentId).ToArray();

                    //var countDropStuCurrentSession = _IStudentService.GetAllStudentClasss(ref count, SessionId: current_Session.SessionId)
                    //                                    .Where(x => DropStudentids.Contains(x.StudentId)).Count();

                    var countDropStuCurrentSession = AllStudentClass.Where(x => DropStudentids.Contains(x.StudentId)).Count();

                    BusRouteList.PickCount = countPickStuCurrentSession.ToString();
                    BusRouteList.DropCount = countDropStuCurrentSession.ToString();
                    BusRouteList.NoAction = false;
                    if (item.EndDate != null && item.EndDate < DateTime.UtcNow.Date)
                      BusRouteList.NoAction = true;

                    if (!item.EndDate.HasValue)
                    {
                        if (item.EffectiveDate.Value >= DateTime.UtcNow.Date)
                            BusRouteList.MinEndDate = item.EffectiveDate.Value;
                        else
                            BusRouteList.MinEndDate = DateTime.UtcNow.Date;

                        BusRouteList.MaxEndDate = session.EndDate;
                    }

                    var pckBoardingPoints= AllBusRouteDetail.Where(f => f.BusRouteId == item.BusRouteId).Where(f=>f.PickupTime!=null).Count().ToString();
                    var drpBoardingPoints = AllBusRouteDetail.Where(f => f.BusRouteId == item.BusRouteId).Where(f=>f.DropTime!=null).Count().ToString();
                    BusRouteList.PickBoardingPoints = pckBoardingPoints != "0" ? pckBoardingPoints : "";
                    BusRouteList.DropBoardingPoints= drpBoardingPoints != "0" ? drpBoardingPoints : "";
                    model.BusRouteList.Add(BusRouteList);
                }

                //filter
                if (model.BusRouteList.Count > 0)
                {
                    if (model.BusId > 0 && model.BusId != null)
                        model.BusRouteList = model.BusRouteList.Where(m => m.BusId == (int)model.BusId).ToList();
                    if(!string.IsNullOrEmpty(model.BusRouteName))
                        model.BusRouteList = model.BusRouteList.Where(x => x.BusRouteName.ToLower().Contains(model.BusRouteName.ToLower())).ToList();
                    if (!string.IsNullOrEmpty(model.BusRouteNo))
                        model.BusRouteList = model.BusRouteList.Where(x => x.BusRouteNo.ToLower().Contains(model.BusRouteNo.ToLower())).ToList();

                    if (model.DriverId > 0 && model.DriverId != null)
                        model.BusRouteList = model.BusRouteList.Where(m => m.DriverId == (int)model.DriverId).ToList();
                    if (model.AttendantId > 0 && model.AttendantId != null)
                        model.BusRouteList = model.BusRouteList.Where(m => m.AttendantId == (int)model.AttendantId).ToList();
                    //if (model.BoardingPointId > 0 && model.BoardingPointId != null)
                    //    model.BusRouteList = model.BusRouteList.Where(m => m.Busroute == (int)model.BoardingPointId).ToList();

                    if (!string.IsNullOrEmpty(model.EffDate))
                    {
                        var startDate = (model.EffDate).Split('/');
                        var ndate = DateTime.Parse(startDate[1] + '/' + startDate[0] + '/' + startDate[2]);
                        model.BusRouteList = model.BusRouteList.Where(p => p.dtEffectiveDate >= ndate).ToList();
                    }
                    if (!string.IsNullOrEmpty(model.ToDate))
                    {
                        var endDate = (model.ToDate).Split('/');
                        var ndate = DateTime.Parse(endDate[1] + '/' + endDate[0] + '/' + endDate[2]);
                        model.BusRouteList = model.BusRouteList.Where(p => p.dtEffectiveDate <= ndate).ToList();
                    }
                    if (model.IsShowActiveOnly)
                    {
                        model.BusRouteList = model.BusRouteList.Where(m => m.BusRouteStatus == true
                                         && (m.dtEndDate >= DateTime.Now.Date || m.dtEndDate == null)).ToList();
                    }
                }
                return Json(new
                {
                    status = "success",
                    BusRouteList = model.BusRouteList
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult SaveCloseRoutes(string CloseDate, string BusRouteIds)
        {
            try
            {
                if (BusRouteIds != "")
                {
                    DateTime dtCloseDate = DateTime.Parse(CloseDate);
                    if (BusRouteIds.Contains(";"))
                    {
                    var routeidarray = BusRouteIds.Split(';');
                    foreach (var strId in routeidarray)
                    {
                        int id = Convert.ToInt32(strId);
                        var BusRoute=_IBusService.GetBusRouteById(id);
                        if (BusRoute != null)
                        {
                            BusRoute.EndDate=dtCloseDate.Date;
                            _IBusService.UpdateBusRoute(BusRoute);
                        }
                    }
                    }
                    else
                    {
                        int id = Convert.ToInt32(BusRouteIds);
                        var BusRoute = _IBusService.GetBusRouteById(id);
                        if (BusRoute != null)
                        {
                            BusRoute.EndDate = dtCloseDate.Date;
                            _IBusService.UpdateBusRoute(BusRoute);
                        }
                    }
                    return Json(new
                    {
                        status = "success",
                        msg = "Added SuccessFully"
                    });
                } 
                return Json(new
                {
                    status = "error",
                    msg = "Invalid Session"
                });
            }

            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult SaveCloseBoardingPoints(string CloseDate, string BoardingPointFeeIds)
        {
            try
            {
                if (BoardingPointFeeIds != "")
                {
                    DateTime dtCloseDate = DateTime.Parse(CloseDate);
                    if (BoardingPointFeeIds.Contains(";"))
                    {
                        var brdnigpointfeeidarray = BoardingPointFeeIds.Split(';');
                        foreach (var strId in brdnigpointfeeidarray)
                        {
                            int id = Convert.ToInt32(strId);
                            var BoardingPointFee = _IBusService.GetBoardingPointFeeById(id);
                            if (BoardingPointFee != null)
                            {
                                BoardingPointFee.EndDate = dtCloseDate.Date;
                                _IBusService.UpdateBoardingPointFee(BoardingPointFee);
                            }
                        }
                    }
                    else
                    {
                        int id = Convert.ToInt32(BoardingPointFeeIds);
                        var BoardingPointFee = _IBusService.GetBoardingPointFeeById(id);
                        if (BoardingPointFee != null)
                        {
                            BoardingPointFee.EndDate = dtCloseDate.Date;
                            _IBusService.UpdateBoardingPointFee(BoardingPointFee);
                        }
                    }
                    return Json(new
                    {
                        status = "success",
                        msg = "Added SuccessFully"
                    });
                }
                return Json(new
                {
                    status = "error",
                    msg = "Invalid Session"
                });
            }

            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult OpenBusRouteList(int SessionId)
        {
            try
            {
                if (SessionId > 0) { 
               var model =new TransportRouteModel();
                var session = _ICatalogMasterService.GetAllSessions(ref count).Where(m=>m.SessionId==SessionId).FirstOrDefault();
                var allroutes = _IBusService.GetAllBusRoutes(ref count, Routestatus: true);

                if (SessionId > 0)
                {
                    session = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.SessionId == SessionId).FirstOrDefault();
                   // allroutes = allroutes.Where(m => m.EffectiveDate <= session.EndDate && m.EndDate >= session.StartDate || m.EffectiveDate >= session.StartDate && m.EffectiveDate <= session.EndDate || m.EffectiveDate <= session.EndDate && m.EndDate == null).ToList();
                    allroutes = allroutes.Where(m=>m.EffectiveDate<=session.EndDate && m.EndDate==null).ToList();
                }
                foreach (var item in allroutes.Distinct())
                {
                    BusRouteList BusRouteList = new BusRouteList();
                    // get bus route bus detail
                    var tptroutedetail = item.TptRouteDetails.ToList();
                    var tptroutebus = tptroutedetail.Where(t => t.BusId != null && t.EndDate == null).FirstOrDefault();
                    BusRouteList.BusId = tptroutebus != null ? (int)tptroutebus.BusId : 0;
                    BusRouteList.BusName = tptroutebus != null ? tptroutebus.Bus.BusNo : "";
                    // get bus route driver detail
                    var tptroutedriver = tptroutedetail.Where(t => t.DriverId != null && t.EndDate == null).FirstOrDefault();
                    BusRouteList.DriverId = tptroutedriver != null ? (int)tptroutedriver.DriverId : 0;
                    BusRouteList.DriverName = tptroutedriver != null ? tptroutedriver.TptDriver.DriverName : "";
                    BusRouteList.BusRouteId = item.BusRouteId;
                    BusRouteList.EncBusRouteId = _ICustomEncryption.base64e(item.BusRouteId.ToString());
                    BusRouteList.BusRouteName = item.BusRoute1;
                    BusRouteList.BusRouteStatus = (bool)item.RouteStatus;
                    BusRouteList.BusRouteNo = item.RouteNo;
                    BusRouteList.EffectiveDate = ConvertDate(item.EffectiveDate.Value);
                    BusRouteList.dtEffectiveDate = (DateTime)item.EffectiveDate;
                    BusRouteList.dtEndDate = item.EndDate;
                    BusRouteList.EndDate = item.EndDate.HasValue ? ConvertDate(item.EndDate.Value) : "";
                    var tptrouteAttendant = tptroutedetail.Where(t => t.TptAttendantId != null && t.EndDate == null).FirstOrDefault();
                    if (tptrouteAttendant != null)
                    {
                        BusRouteList.AttendantId = tptroutedriver != null ? (int)tptroutedriver.TptAttendantId : 0;

                        BusRouteList.AttendantName = "";
                        if (tptroutedriver != null)
                            BusRouteList.AttendantName = tptroutedriver.TptAttendant != null ? tptroutedriver.TptAttendant.AttendantName : "";
                    }
                    BusRouteList.IsDeleted = true;
                    if (item.BusRouteDetails.Count > 0)
                    {
                        BusRouteList.IsDeleted = false;
                        BusRouteList.IsAuthToViewDetails = true;
                    }
                    var StuStatus = _IStudentMasterService.GetAllStudentStatus(ref count, "Active").FirstOrDefault();
                    var StudentCount = _ITransportService.GetAllTptFacilitys(ref count, BusRoutrId: item.BusRouteId)
                                        .Where(m => m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                                        && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)).ToList();
                    BusRouteList.StudentCount = StudentCount.Count().ToString();

                    //pick students for current session
                    var PickStudentids = _ITransportService.GetAllTptFacilitys(ref count, BusRoutrId: item.BusRouteId)
                                                .Where(m => m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                                                && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                                && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                                                && (m.TptTypeDetail.TptName.ToLower() == "pick" || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                                                .Select(x => x.StudentId).ToArray();
                    var countPickStuCurrentSession = _IStudentService.GetAllStudentClasss(ref count, SessionId: SessionId)
                                                     .Where(x => PickStudentids.Contains(x.StudentId)).Count();


                    //drop students for current session
                    var DropStudentids = _ITransportService.GetAllTptFacilitys(ref count)
                                                .Where(m => m.DropRouteId == item.BusRouteId
                                                && m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                                                && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                                && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                                                && (m.TptTypeDetail.TptName.ToLower() == "drop" || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                                                .Select(x => x.StudentId).ToArray();
                    var countDropStuCurrentSession = _IStudentService.GetAllStudentClasss(ref count, SessionId: SessionId)
                                                        .Where(x => DropStudentids.Contains(x.StudentId)).Count();

                    BusRouteList.PickCount = countPickStuCurrentSession.ToString();
                    BusRouteList.DropCount = countDropStuCurrentSession.ToString();
                    BusRouteList.NoAction = false;
                    if (item.EndDate != null && item.EndDate < DateTime.UtcNow.Date)
                        BusRouteList.NoAction = true;

                    if (!item.EndDate.HasValue)
                    {
                        if (item.EffectiveDate.Value >= DateTime.UtcNow.Date)
                            BusRouteList.MinEndDate = item.EffectiveDate.Value;
                        else
                            BusRouteList.MinEndDate = DateTime.UtcNow.Date;

                        BusRouteList.MaxEndDate = session.EndDate;
                    }
                    model.BusRouteList.Add(BusRouteList);
                }

                return Json(new
                {
                    status = "success",
                    BusRouteList = model.BusRouteList
                });
            }
                return Json(new
                {
                    status = "error",
                    msg="Invalid Session"
                });
          }

            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return RedirectToAction("General", "Error");
            }
        }


        public TransportRouteModel preparemodel(TransportRouteModel model)
        {
            // current session
            var session = _ICatalogMasterService.GetCurrentSession();
            // all buses select list
            model.AvailableBuses = _IDropDownService.BusList();
            // all driver list
            model.AvailableDrivers = _IDropDownService.DriverList();
            //all AttendantList
            model.AvailableAttendants = _IDropDownService.BusAttendantList();
            //all BoardingPoints
            model.BoardingPointList = _IDropDownService.BoardingPointList();
            // get list of bus route
            model.SessionList = _IDropDownService.SessionList().Where(m => m.Value != "").ToList();
            model.SessionId = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.IsDefualt == true).FirstOrDefault().SessionId;

            var allroutes = _IBusService.GetAllBusRoutes(ref count, Routestatus: true);



            var StuStatus = _IStudentMasterService.GetAllStudentStatus(ref count, "Active").FirstOrDefault();
            var AllTptFaciliTies = _ITransportService.GetAllTptFacilitys(ref count);
            var current_Session = _ICatalogMasterService.GetCurrentSession();
            var AllStudentClass = _IStudentService.GetAllStudentClasss(ref count, SessionId: current_Session.SessionId);
            var AllBusRouteDetail = _IBusService.GetAllBusRouteDetails(ref count);

            foreach (var item in allroutes)
            {
                BusRouteList BusRouteList = new BusRouteList();
                // get bus route bus detail
                var tptroutedetail = item.TptRouteDetails.ToList();
                var tptroutebus = tptroutedetail.Where(t => t.BusId != null && t.EndDate == null).FirstOrDefault();
                BusRouteList.BusId = tptroutebus != null ? (int)tptroutebus.BusId : 0;
                BusRouteList.BusName = tptroutebus != null ? tptroutebus.Bus.BusNo : "";
                // get bus route driver detail
                var tptroutedriver = tptroutedetail.Where(t => t.DriverId != null && t.EndDate == null).FirstOrDefault();
                BusRouteList.DriverId = tptroutedriver != null ? (int)tptroutedriver.DriverId : 0;
                BusRouteList.DriverName = tptroutedriver != null ? tptroutedriver.TptDriver.DriverName : "";
                BusRouteList.TptRouteDetailId = tptroutedriver != null ? tptroutedriver.TptRouteDetailId.ToString() : "";
                BusRouteList.BusRouteId = item.BusRouteId;
                BusRouteList.EncBusRouteId = _ICustomEncryption.base64e(item.BusRouteId.ToString());
                BusRouteList.BusRouteName = item.BusRoute1;
                BusRouteList.BusRouteNo = item.RouteNo;
                BusRouteList.BusRouteStatus =  (bool)item.RouteStatus ;
                BusRouteList.EffectiveDate = ConvertDate(item.EffectiveDate.Value);
                BusRouteList.dtEffectiveDate = (DateTime)item.EffectiveDate;
                BusRouteList.EndDate = item.EndDate.HasValue ? ConvertDate(item.EndDate.Value) : "";
                BusRouteList.dtEndDate = item.EndDate;
                var tptrouteAttendant = tptroutedetail.Where(t => t.TptAttendantId != null && t.EndDate == null).FirstOrDefault();
                if (tptrouteAttendant != null)
                {
                    BusRouteList.AttendantId = tptroutedriver != null ? (int)tptroutedriver.TptAttendantId : 0;

                    BusRouteList.AttendantName = "";
                    if (tptroutedriver != null)
                        BusRouteList.AttendantName = tptroutedriver.TptAttendant != null ? tptroutedriver.TptAttendant.AttendantName : "";
                }
                BusRouteList.IsDeleted = true;
                if (item.BusRouteDetails.Count > 0)
                {
                    BusRouteList.IsDeleted = false;
                    BusRouteList.IsAuthToViewDetails = true;
                }
                //  var StuStatus = _IStudentMasterService.GetAllStudentStatus(ref count, "Active").FirstOrDefault();


                //var StudentCount = _ITransportService.GetAllTptFacilitys(ref count, BusRoutrId: item.BusRouteId)
                //                    .Where(m => m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                //                    && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)).ToList();
                //BusRouteList.StudentCount = StudentCount.Count().ToString();

                var StudentCount = AllTptFaciliTies
                                    .Where(m => m.BusRouteId== item.BusRouteId && m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                                    && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)).ToList();
                BusRouteList.StudentCount = StudentCount.Count().ToString();

                // var current_Session =_ICatalogMasterService.GetCurrentSession();
                //pick students for current session
                //var PickStudentids = _ITransportService.GetAllTptFacilitys(ref count, BusRoutrId: item.BusRouteId)
                //                            .Where(m => m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                //                            && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                //                            && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                //                            && (m.TptTypeDetail.TptName.ToLower() == "pick" || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                //                            .Select(x => x.StudentId).ToArray();

                var PickStudentids = AllTptFaciliTies
                                          .Where(m => m.BusRouteId== item.BusRouteId && m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                                          && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                          && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                                          && (m.TptTypeDetail.TptName.ToLower() == "pick" || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                                          .Select(x => x.StudentId).ToArray();

                //var countPickStuCurrentSession = _IStudentService.GetAllStudentClasss(ref count, SessionId: current_Session.SessionId)
                //                                 .Where(x => PickStudentids.Contains(x.StudentId)).Count();

                var countPickStuCurrentSession = AllStudentClass.Where(x => PickStudentids.Contains(x.StudentId)).Count();

                //drop students for current session
                //var DropStudentids = _ITransportService.GetAllTptFacilitys(ref count)
                //                            .Where(m => m.DropRouteId == item.BusRouteId
                //                            && m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                //                            && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                //                            && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                //                            && (m.TptTypeDetail.TptName.ToLower() == "drop" || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                //                            .Select(x=>x.StudentId).ToArray();

                var DropStudentids = AllTptFaciliTies
                                            .Where(m => m.DropRouteId == item.BusRouteId
                                            && m.VehicleTypeId == null && m.Student.StudentStatusId == StuStatus.StudentStatusId
                                            && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                            && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                                            && (m.TptTypeDetail.TptName.ToLower() == "drop" || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                                            .Select(x => x.StudentId).ToArray();

                var countDropStuCurrentSession = AllStudentClass.Where(x => DropStudentids.Contains(x.StudentId)).Count();

                BusRouteList.PickCount = countPickStuCurrentSession.ToString();
                BusRouteList.DropCount = countDropStuCurrentSession.ToString();
                BusRouteList.NoAction = false;
                if (item.EndDate != null && item.EndDate < DateTime.UtcNow.Date)
                    BusRouteList.NoAction = true;

                if (!item.EndDate.HasValue)
                {
                    if (item.EffectiveDate.Value >= DateTime.UtcNow.Date)
                        BusRouteList.MinEndDate = item.EffectiveDate.Value;
                    else
                        BusRouteList.MinEndDate = DateTime.UtcNow.Date;

                    BusRouteList.MaxEndDate = session.EndDate;
                }

                var pckBoardingPoints = AllBusRouteDetail.Where(f => f.BusRouteId == item.BusRouteId).Where(f => f.PickupTime != null).Count().ToString();
                var drpBoardingPoints = AllBusRouteDetail.Where(f => f.BusRouteId == item.BusRouteId).Where(f => f.DropTime != null).Count().ToString();
                BusRouteList.PickBoardingPoints = pckBoardingPoints != "0" ? pckBoardingPoints : "";
                BusRouteList.DropBoardingPoints = drpBoardingPoints != "0" ? drpBoardingPoints : "";

                model.BusRouteList.Add(BusRouteList);
            }
            model.IsShowActiveOnly = true;
            if (model.BusRouteList.Count > 0)
            {
                if (model.IsShowActiveOnly)
                {
                    model.BusRouteList = model.BusRouteList.Where(m => m.BusRouteStatus == true
                                           && (m.dtEndDate >= DateTime.Now.Date || m.dtEndDate == null)).ToList();
                }
            }


            model.MinEffectiveDate = session.StartDate.Value.Date;
            model.MaxEffectiveDate = session.EndDate;
            // check if School Location not set Yet
            model.IsSchoolLocationSet = true;
            var SchoolLatitude = _ISchoolSettingService.GetAttributeValue("SchoolLatitude");
            var SchoolLongitude = _ISchoolSettingService.GetAttributeValue("SchoolLongitude");
            if (SchoolLatitude == null && SchoolLongitude == null)
            {
                model.IsSchoolLocationSet = false;
                model.Latitude = "30.7144035";
                model.Longitude = "76.7556298";
            }
            else
            {
                model.IsSchoolLocationSet = true;
                model.Latitude = SchoolLatitude;
                model.Longitude = SchoolLongitude;
            }


            return model;
        }

        public ActionResult TransportRoute()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "View");
                if (!isauth)
                    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var model = new TransportRouteModel();
                model = preparemodel(model);
                var getModuleName = _IModuleVersionService.GetAllModules(ref count, Module: "Transport").FirstOrDefault();
                if (getModuleName != null)
                    model.ModuleId = getModuleName.ModuleId;

                model.IsAdmin = false;
                var getRole = _IUserService.GetUserRoleById((int)user.UserRoleId);
                if (getRole.UserRole1.ToLower() == "admin")
                    model.IsAdmin = true;

                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Add Record");
                model.IsAuthToClose = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Close");
                model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Edit Record");
                model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Delete Record");
                model.IsAuthToCreateRouteDetail = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Set");
                model.IsAuthToViewDetail = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "View Details");
                model.IsAuthToViewRecord = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "View Record");

                var AllowDuplicateBoardingPointInRoute = _ISchoolSettingService.GetorSetSchoolData("AllowDuplicateBoardingPointInRoute", "0").AttributeValue;

                var Crntsession = _ICatalogMasterService.GetAllSessions(ref count).FirstOrDefault();
                // effective dates
                model.StudentModel.MinTptFacilityEffectiveDate = Crntsession.StartDate;
                model.StudentModel.MaxTptFacilityEffectiveDate = Crntsession.EndDate;
                #region Transport facility

                model.StudentModel.TransportFacility.AvailableVehicleType = _IDropDownService.VehicleList();
                model.StudentModel.TransportFacility.AvailableTptType = _IDropDownService.TptTypeList();

                model.StudentModel.TransportFacility.TptTypeId = _ITransportService.GetAllTptTypes(ref count).Where(m => m.TptType1.ToLower() == "school").FirstOrDefault().TptTypeId;


                // get School TPT Type
                var schooltpttype = _ITransportService.GetAllTptTypes(ref count, "School").FirstOrDefault();
                if (schooltpttype != null)
                    model.StudentModel.TransportFacility.AvailableTptTypeDet = _IDropDownService.TptTypeDetailList(schooltpttype.TptTypeId);
                else
                    model.StudentModel.TransportFacility.AvailableTptTypeDet = _IDropDownService.TptTypeDetailList(2);

                var transportoptionpickanddrop = _ITransportService.GetAllTptTypeDetails(ref count).Where(m => m.TptTypeId == schooltpttype.TptTypeId && m.TptName == "Pick Drop").FirstOrDefault();
                if (transportoptionpickanddrop != null)
                    model.StudentModel.TransportFacility.TptTypeDetailId = transportoptionpickanddrop.TptTypeDetailId;
                // Boarding Point
                model.StudentModel.TransportFacility.AvailableBoardingPoint = _IDropDownService.BoardingPointList();
                // Bus Route
                if (model.StudentModel.TransportFacility.AvailableBusRoute.Count == 0)
                    model.StudentModel.TransportFacility.AvailableBusRoute.Add(new SelectListItem { Selected = true, Text = "---Select---", Value = "" });


                var allowershiptypes = _ITransportService.GetAllTptOwnerShips(ref count).ToList();
                if (allowershiptypes.Count > 0)
                {
                    model.StudentModel.TransportFacility.OwnershipType.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = false });
                    foreach (var item in allowershiptypes)
                    {
                        model.StudentModel.TransportFacility.OwnershipType.Add(new SelectListItem { Value = item.TptOwnerShipId.ToString(), Text = item.TptOwnerShip1, Selected = false });
                    }
                }
                #endregion

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public TransportRouteModel checkmodelerrors(TransportRouteModel model)
        {
            if (string.IsNullOrEmpty(model.BusRouteName) || string.IsNullOrWhiteSpace(model.BusRouteName))
                ModelState.AddModelError("BusRouteName", "Route name required");
            else
            {
                //check unique bus route name
                var busroute = _IBusService.GetBusRouteNoTracking();
                var busroutenamelist = busroute.Where(br => br.BusRoute1.Trim().ToLower() == model.BusRouteName.Trim().ToLower() &&
                    br.EffectiveDate <= model.EffectiveDate.Value
                    && ((br.EndDate != null && br.EndDate >= model.EffectiveDate.Value)
                    || (br.EndDate == null))).FirstOrDefault();

                if (busroutenamelist != null && busroutenamelist.BusRouteId != model.BusRouteId)
                    ModelState.AddModelError("BusRouteName", "Route Name Already Exists");
            }

            if (string.IsNullOrEmpty(model.BusRouteNo) || string.IsNullOrWhiteSpace(model.BusRouteNo))
                ModelState.AddModelError("BusRouteNo", "Route no required");
            else
            {
                //check unique bus route no
                var busroute = _IBusService.GetBusRouteNoTracking();
                var busroutenolist = busroute.Where(br => br.RouteNo.Trim().ToLower() == model.BusRouteNo.Trim().ToLower() &&
                    br.EffectiveDate <= model.EffectiveDate.Value
                    && ((br.EndDate != null && br.EndDate >= model.EffectiveDate.Value)
                    || (br.EndDate == null))).FirstOrDefault();

                if (busroutenolist != null && busroutenolist.BusRouteId != model.BusRouteId)
                    ModelState.AddModelError("BusRouteNo", "Route No Already Exists");
            }

            if (model.BusId == null)
                ModelState.AddModelError("BusId", "Bus required");

            if (model.DriverId == null)
                ModelState.AddModelError("DriverId", "Driver required");

            if (model.AttendantId == null)
                ModelState.AddModelError("DriverId", "Attendant required");

            if (!model.EffectiveDate.HasValue)
                ModelState.AddModelError("EffectiveDate", "Effective date required");
            //else
            //{
            //    if (model.EffectiveDate.Value < DateTime.UtcNow.Date)
            //        ModelState.AddModelError("EffectiveDate", "Effective date invalid");
            //}

            if (model.BusId > 0 && model.DriverId > 0 && model.EffectiveDate.HasValue)
            {
                // check unique bus driver
                var busroute = _IBusService.GetBusRouteNoTracking();
                var busroutenolist = busroute.Where(br => br.BusRouteId != model.BusRouteId && br.EffectiveDate <= model.EffectiveDate.Value
                   && ((br.EndDate != null && br.EndDate >= model.EffectiveDate.Value)
                   || (br.EndDate == null))).ToList();
                // ver bus route ids
                var busrouteids = busroutenolist.Select(b => b.BusRouteId).ToArray();

                if (busroutenolist != null)
                {
                    var TptRouteDetails = _IBusService.GetAllTptRouteDetails(ref count);

                    // check bus exist
                    var IsDriverBusexist = TptRouteDetails.Where(t => busrouteids.Contains(t.BusRouteId) && t.BusId == model.BusId
                        && t.EffectiveDate <= model.EffectiveDate.Value
                        && ((t.EndDate != null && t.EndDate >= model.EffectiveDate.Value)
                        || (t.EndDate == null))).ToList();

                    // check driver exist
                    var IsDriverexist = TptRouteDetails.Where(t => busrouteids.Contains(t.BusRouteId) && t.DriverId == model.DriverId
                        && t.EffectiveDate <= model.EffectiveDate.Value
                        && ((t.EndDate != null && t.EndDate >= model.EffectiveDate.Value)
                        || (t.EndDate == null))).ToList();

                    // check is unique bus driver Setting true/false 1/0
                    var UniqueBusDriver = _ISchoolSettingService.GetAttributeValue("UniqueBusDriver");
                    if (UniqueBusDriver == "1")
                    {
                        if (IsDriverBusexist.Count > 0 && IsDriverexist.Count > 0)
                            ModelState.AddModelError("DriverId", "Bus Driver should be unique");
                    }

                    var UniqueRouteDriver = _ISchoolSettingService.GetAttributeValue("UniqueRouteDriver");
                    if (UniqueRouteDriver == "1")
                    {
                        if (IsDriverexist.Count > 0)
                            ModelState.AddModelError("DriverId", "Route Driver should be unique");
                    }
                }
            }

            return model;
        }

        [HttpPost]
        public ActionResult TransportRoute(TransportRouteModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);

            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                model = checkmodelerrors(model);
                if (ModelState.IsValid)
                {
                    // Activity log type add , edit
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    // stdclass Id                    
                    var BusRoutetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "BusRoute").FirstOrDefault();
                    var TptRouteDetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "TptRouteDetail").FirstOrDefault();
                    // update bus route
                    if (model.BusRouteId > 0)
                    {
                        // check authorization
                        //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Edit Record");
                        //if (!isauth)
                        //    return RedirectToAction("AccessDenied", "Error");
                        // get bus route by id
                        var busroute = _IBusService.GetBusRouteById((int)model.BusRouteId, true);
                        if (busroute != null)
                        {
                            busroute.BusRoute1 = model.BusRouteName;
                            busroute.EffectiveDate = model.EffectiveDate;
                            busroute.RouteNo = model.BusRouteNo;
                            busroute.RouteStatus = true;
                            _IBusService.UpdateBusRoute(busroute);
                            if (BusRoutetable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BusRoutetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(busroute.BusRouteId, BusRoutetable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update BusRoute with BusRouteId:{0} and BusRoute1:{1}", busroute.BusRouteId, busroute.BusRoute1), Request.Browser.Browser);
                            }

                            // get bus route bus detail
                            var tptroutebus = busroute.TptRouteDetails.Where(t => t.BusId != null && t.EndDate == null).FirstOrDefault();
                            if (tptroutebus != null)
                            {
                                // compare model bus id with database bus id
                                // if change update old entry and insert new one
                                if (tptroutebus.BusId != model.BusId)
                                {
                                    tptroutebus.EndDate = DateTime.UtcNow.Date;
                                    _IBusService.UpdateTptRouteDetail(tptroutebus);
                                    if (TptRouteDetailtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, TptRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(tptroutebus.TptRouteDetailId, TptRouteDetailtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update TptRouteDetail with TptRouteDetailId:{0} and BusRouteId:{1}", tptroutebus.TptRouteDetailId, tptroutebus.BusRouteId), Request.Browser.Browser);
                                    }

                                    // insert Tpt route Bus
                                    TptRouteDetail TptRouteDetail = new TptRouteDetail();
                                    TptRouteDetail.BusRouteId = busroute.BusRouteId;
                                    TptRouteDetail.BusId = model.BusId;
                                    TptRouteDetail.DriverId = model.DriverId;
                                    TptRouteDetail.EffectiveDate = model.EffectiveDate;
                                    TptRouteDetail.EndDate = null;
                                    TptRouteDetail.TptAttendantId = model.AttendantId;
                                    _IBusService.InsertTptRouteDetail(TptRouteDetail);
                                    if (TptRouteDetailtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, TptRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(TptRouteDetail.TptRouteDetailId, TptRouteDetailtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add TptRouteDetail with TptRouteDetailId:{0} and BusRouteId:{1}", TptRouteDetail.TptRouteDetailId, TptRouteDetail.BusRouteId), Request.Browser.Browser);
                                    }
                                }
                                else
                                {
                                    tptroutebus.EffectiveDate = model.EffectiveDate.Value;
                                    _IBusService.UpdateTptRouteDetail(tptroutebus);
                                    if (TptRouteDetailtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, TptRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(tptroutebus.TptRouteDetailId, TptRouteDetailtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update TptRouteDetail with TptRouteDetailId:{0} and BusRouteId:{1}", tptroutebus.TptRouteDetailId, tptroutebus.BusRouteId), Request.Browser.Browser);
                                    }
                                }
                            }

                            // get bus route Driver detail
                            var tptroutedriver = busroute.TptRouteDetails.Where(t => t.DriverId != null && t.EndDate == null).FirstOrDefault();
                            if (tptroutedriver != null)
                            {
                                // compare model bus id with database bus id
                                // if change update old entry and insert new one
                                if (tptroutedriver.DriverId != model.DriverId)
                                {
                                    tptroutedriver.EndDate = DateTime.UtcNow.Date;
                                    _IBusService.UpdateTptRouteDetail(tptroutedriver);
                                    if (TptRouteDetailtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, TptRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(tptroutedriver.TptRouteDetailId, TptRouteDetailtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update TptRouteDetail with TptRouteDetailId:{0} and BusRouteId:{1}", tptroutedriver.TptRouteDetailId, tptroutedriver.BusRouteId), Request.Browser.Browser);
                                    }

                                    // insert Tpt route Bus
                                    TptRouteDetail TptRouteDetail = new TptRouteDetail();
                                    TptRouteDetail.BusRouteId = busroute.BusRouteId;
                                    TptRouteDetail.BusId = model.BusId;
                                    TptRouteDetail.DriverId = model.DriverId;
                                    TptRouteDetail.EffectiveDate = model.EffectiveDate;
                                    TptRouteDetail.EndDate = null;
                                    TptRouteDetail.TptAttendantId = model.AttendantId;
                                    _IBusService.InsertTptRouteDetail(TptRouteDetail);
                                    if (TptRouteDetailtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, TptRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(TptRouteDetail.TptRouteDetailId, TptRouteDetailtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add TptRouteDetail with TptRouteDetailId:{0} and BusRouteId:{1}", TptRouteDetail.TptRouteDetailId, TptRouteDetail.BusRouteId), Request.Browser.Browser);
                                    }
                                }
                                else
                                {
                                    tptroutedriver.EffectiveDate = model.EffectiveDate.Value;
                                    _IBusService.UpdateTptRouteDetail(tptroutedriver);
                                    if (TptRouteDetailtable != null)
                                    {
                                        // table log saved or not
                                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, TptRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                        // save activity log
                                        if (tabledetail != null)
                                            _IActivityLogService.SaveActivityLog(tptroutedriver.TptRouteDetailId, TptRouteDetailtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update TptRouteDetail with TptRouteDetailId:{0} and BusRouteId:{1}", tptroutedriver.TptRouteDetailId, tptroutedriver.BusRouteId), Request.Browser.Browser);
                                    }
                                }
                            }

                        }
                    }
                    else
                    {
                        // check authorization
                        //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Add Record");
                        //if (!isauth)
                        //    return RedirectToAction("AccessDenied", "Error");
                        // insert bus route
                        BusRoute BusRoute = new BusRoute();
                        BusRoute.BusRoute1 = model.BusRouteName;
                        BusRoute.EffectiveDate = model.EffectiveDate;
                        BusRoute.RouteNo = model.BusRouteNo;
                        BusRoute.RouteStatus = true;
                        _IBusService.InsertBusRoute(BusRoute);
                        if (BusRoutetable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BusRoutetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(BusRoute.BusRouteId, BusRoutetable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add BusRoute with BusRouteId:{0} and BusRoute1:{1}", BusRoute.BusRouteId, BusRoute.BusRoute1), Request.Browser.Browser);
                        }

                        // insert Tpt route Bus
                        TptRouteDetail TptRouteDetail = new TptRouteDetail();
                        TptRouteDetail.BusRouteId = BusRoute.BusRouteId;
                        TptRouteDetail.BusId = model.BusId;
                        TptRouteDetail.DriverId = model.DriverId;
                        TptRouteDetail.EffectiveDate = model.EffectiveDate;
                        TptRouteDetail.EndDate = null;
                        TptRouteDetail.TptAttendantId = model.AttendantId;
                        _IBusService.InsertTptRouteDetail(TptRouteDetail);
                        if (TptRouteDetailtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, TptRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(TptRouteDetail.TptRouteDetailId, TptRouteDetailtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add TptRouteDetail with TptRouteDetailId:{0} and BusRouteId:{1}", TptRouteDetail.TptRouteDetailId, TptRouteDetail.BusRouteId), Request.Browser.Browser);
                        }

                        //// insert Tpt route Driver
                        //TptRouteDetail = new TptRouteDetail();
                        //TptRouteDetail.BusRouteId = BusRoute.BusRouteId;
                        //TptRouteDetail.BusId = model.BusId;
                        //TptRouteDetail.DriverId = model.DriverId;
                        //TptRouteDetail.EffectiveDate = model.EffectiveDate;
                        //TptRouteDetail.EndDate = null;
                        //TptRouteDetail.TptAttendantId = model.AttendantId;
                        //_IBusService.InsertTptRouteDetail(TptRouteDetail);
                        //if (TptRouteDetailtable != null)
                        //{
                        //    // table log saved or not
                        //    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, TptRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        //    // save activity log
                        //    if (tabledetail != null)
                        //        _IActivityLogService.SaveActivityLog(TptRouteDetail.TptRouteDetailId, TptRouteDetailtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add TptRouteDetail with TptRouteDetailId:{0} and BusRouteId:{1}", TptRouteDetail.TptRouteDetailId, TptRouteDetail.BusRouteId), Request.Browser.Browser);
                        //}

                    }
                    return RedirectToAction("TransportRoute");
                }

                model = preparemodel(model);
                model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Add Record");
                model.IsAuthToClose = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Close");
                model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Edit Record");
                model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Delete");
                model.IsAuthToCreateRouteDetail = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Set");

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public TransportRouteModel prepareBusroutedetmodel(TransportRouteModel model)
        {
            // check if School Location not set Yet
            model.IsSchoolLocationSet = true;
            var SchoolLatitude = _ISchoolSettingService.GetAttributeValue("SchoolLatitude");
            var SchoolLongitude = _ISchoolSettingService.GetAttributeValue("SchoolLongitude");
            if (SchoolLatitude == null && SchoolLongitude == null)
            {
                // default chandigarh
                model.Latitude = "30.7144035";
                model.Longitude = "76.7556298";
            }
            else
            {
                model.Latitude = SchoolLatitude;
                model.Longitude = SchoolLongitude;
            }

            // bus route detail 
            model.AvailableBoardingPoint.Add(new BoardingPointModel { BoardingPoint = "---Add New---", BoardingPointId = "", Lat = "", Long = "" });
            var allboardingpoints = _IBusService.GetAllBoardingPoints(ref count).OrderBy(f => f.BoardingPoint1);
            var ALlBoardingPointFees = _IBusService.GetAllBoardingPointFees(ref count);
            foreach (var item in allboardingpoints)
            {
                BoardingPointModel BoardingPointModel = new BoardingPointModel();
                BoardingPointModel.BoardingPoint = item.BoardingPoint1;
                BoardingPointModel.BoardingPointId = item.BoardingPointId.ToString();
                BoardingPointModel.Lat = item.Latitude;
                BoardingPointModel.Long = item.Longitude;
                var boardingpointfeedetail = ALlBoardingPointFees.Where(m => m.BoardingPointId == item.BoardingPointId).OrderByDescending(m => m.BoardingPointFeeId).FirstOrDefault();
                if (boardingpointfeedetail != null)
                {
                    BoardingPointModel.BoardingPointFeeId = boardingpointfeedetail.BoardingPointFeeId;
                    BoardingPointModel.SinglePickOrDropFee = boardingpointfeedetail.TransportFee_1;
                    BoardingPointModel.PickAndDropFee = boardingpointfeedetail.TransportFee_2;
                    BoardingPointModel.EffectiveDate = ConvertDate((DateTime)boardingpointfeedetail.EfectiveDate);
                    BoardingPointModel.FeeFrequencyId = (int)boardingpointfeedetail.FeeFrequencyId;
                }
                model.AvailableBoardingPoint.Add(BoardingPointModel);
            }

            // Bus route detail
            // get bus route and check end date
            var busroute = _IBusService.GetBusRouteById((int)model.BusRouteId);
            ViewBag.BusRouteTitleName = "Assign Boarding Points to " + busroute.BusRoute1;

            if (busroute != null && (busroute.EndDate == null || (busroute.EndDate != null && busroute.EndDate <= DateTime.UtcNow.Date)))
            {
                var busroutedetail = _IBusService.GetAllBusRouteDetails(ref count, BusRouteId: busroute.BusRouteId);
                // check last entity is pick or Drop
                if (!string.IsNullOrEmpty(Convert.ToString(TempData["RouteType"])))
                {
                    if (Convert.ToString(TempData["RouteType"]) == "Pick")
                        busroutedetail = busroutedetail.Where(b => b.Status != false && b.EndDate == null && b.PickupTime != null).OrderBy(b => b.BoardingIndex).ToList();
                    else
                        busroutedetail = busroutedetail.Where(b => b.Status != false && b.EndDate == null && b.DropTime != null).OrderBy(b => b.BoardingIndex).ToList();
                }
                else
                {
                    if (busroutedetail.LastOrDefault() != null && busroutedetail.LastOrDefault().PickupTime != null)
                        busroutedetail = busroutedetail.Where(b => b.Status != false && b.EndDate == null && b.PickupTime != null).OrderBy(b => b.BoardingIndex).ToList();
                    else
                        busroutedetail = busroutedetail.Where(b => b.Status != false && b.EndDate == null && b.DropTime != null).OrderBy(b => b.BoardingIndex).ToList();
                }

                decimal ttldistance = 0;
                var AllTransPortFacility = _ITransportService.GetAllTptFacilitys(ref count);
                foreach (var item in busroutedetail)
                {
                    BusRouteDetailModel TransportRouteModel = new BusRouteDetailModel();
                    TransportRouteModel.BusRouteDetailId = item.BusRouteDetailId;
                    TransportRouteModel.BoardingIndex = item.BoardingIndex;
                    TransportRouteModel.BoardingPoint = item.BoardingPoint.BoardingPoint1;
                    TransportRouteModel.BoardingPointId = item.BoardingPointId;
                    TransportRouteModel.Lat = item.BoardingPoint.Latitude;
                    TransportRouteModel.Long = item.BoardingPoint.Longitude;

                    TransportRouteModel.Distance = Math.Round((decimal)item.Distance, 1);

                    ttldistance = Math.Round(ttldistance + (decimal)item.Distance, 1);
                    TransportRouteModel.RelventDistance = ttldistance;

                    //TransportRouteModel.PickTime = item.PickupTime.HasValue ? ConvertTime(item.PickupTime.Value, true) : "";
                    TransportRouteModel.DisplayPickTime = item.PickupTime.HasValue ? ConvertTime(item.PickupTime.Value) : "";

                    //TransportRouteModel.DropTime = item.DropTime.HasValue ? ConvertTime(item.DropTime.Value, true) : "";
                    TransportRouteModel.DisplayDropTime = item.DropTime.HasValue ? ConvertTime(item.DropTime.Value) : "";

                    TransportRouteModel.IsPickRoot = item.PickupTime.HasValue ? true : false;

                    // check if use by student
                    TransportRouteModel.IsDeleted = true;
                    TransportRouteModel.IsInUse = false;
                    var tptfacility = AllTransPortFacility.Where(f=>f.BusRouteId == item.BusRouteId && f.BoardingPointId== item.BoardingPointId).Count();
                    if (tptfacility > 0)
                    {
                        TransportRouteModel.IsDeleted = false;
                        TransportRouteModel.IsInUse = true;
                    }

                    model.BusRouteDetailList.Add(TransportRouteModel);
                }

                // next boarding index
                model.BoardingIndex = busroutedetail.LastOrDefault() != null ? busroutedetail.LastOrDefault().BoardingIndex + 1 : 1;
            }

            return model;
        }

        public ActionResult TransportRouteDetail(string id)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRouteDetail", "View");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    var model = new TransportRouteModel();

                    int BusRouteId = 0;
                    int.TryParse(_ICustomEncryption.base64d(id), out BusRouteId);

                    model.BusRouteId = BusRouteId;
                    model = prepareBusroutedetmodel(model);
                    model.FeeFrequencyList = _IDropDownService.FeeFrequencyList();
                    return View(model);
                }

                return RedirectToAction("Page404", "Error");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public TransportRouteModel prepareBusroutedetViewmodel(TransportRouteModel model)
        {
            // check if School Location not set Yet
            model.IsSchoolLocationSet = true;
            var SchoolLatitude = _ISchoolSettingService.GetAttributeValue("SchoolLatitude");
            var SchoolLongitude = _ISchoolSettingService.GetAttributeValue("SchoolLongitude");
            if (SchoolLatitude == null && SchoolLongitude == null)
            {
                // default chandigarh
                model.Latitude = "30.7144035";
                model.Longitude = "76.7556298";
            }
            else
            {
                model.Latitude = SchoolLatitude;
                model.Longitude = SchoolLongitude;
            }

            // Bus route detail
            // get bus route and check end date
            var busroute = _IBusService.GetBusRouteById((int)model.BusRouteId);
            if (busroute != null)
            {
                ViewBag.BusRouteTitleName = "View Boarding Points of " + busroute.BusRoute1;

                var busroutedetail = _IBusService.GetAllBusRouteDetails(ref count, BusRouteId: busroute.BusRouteId);
                busroutedetail = busroutedetail.OrderBy(b => b.BoardingIndex).ToList();
                // check route drop or pick
                //set flag for drop and pick
                if (model.IsPickRoot)
                {
                    busroutedetail = busroutedetail.Where(b => b.PickupTime != null).ToList();
                }
                else
                {
                    busroutedetail = busroutedetail.Where(b => b.DropTime != null).ToList();
                }
                decimal ttldistance = 0;
                foreach (var item in busroutedetail)
                {
                    BusRouteDetailModel TransportRouteModel = new BusRouteDetailModel();
                    TransportRouteModel.BusRouteDetailId = item.BusRouteDetailId;
                    TransportRouteModel.BoardingIndex = item.BoardingIndex;
                    TransportRouteModel.BoardingPoint = item.BoardingPoint.BoardingPoint1;
                    TransportRouteModel.Distance = Math.Round((decimal)item.Distance, 1);

                    ttldistance = Math.Round(ttldistance + (decimal)item.Distance, 1);
                    TransportRouteModel.RelventDistance = ttldistance;

                    if (item.PickupTime.HasValue)
                        TransportRouteModel.PickTime = ConvertTime(item.PickupTime.Value);
                    else
                        TransportRouteModel.PickTime = "";

                    if (item.DropTime.HasValue)
                        TransportRouteModel.DropTime = ConvertTime(item.DropTime.Value);
                    else
                        TransportRouteModel.DropTime = "";

                    if (item.EffectiveDate.HasValue)
                        TransportRouteModel.EffectiveDate = ConvertDate((DateTime)item.EffectiveDate);
                    else
                        TransportRouteModel.EffectiveDate = "";

                    if (item.EndDate.HasValue)
                        TransportRouteModel.EndDate = ConvertDate((DateTime)item.EndDate);
                    else
                        TransportRouteModel.EndDate = "";

                    TransportRouteModel.IsPickRoot = item.PickupTime.HasValue ? true : false;

                    TransportRouteModel.Lat = item.BoardingPoint.Latitude;

                    TransportRouteModel.Long = item.BoardingPoint.Longitude;

                    model.BusRouteDetailList.Add(TransportRouteModel);
                }

            }

            return model;
        }

        public ActionResult ViewTransportRouteDetail(string id)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "ViewTransportRouteDetail", "View");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                int BusRouteId = 0;
                if (!string.IsNullOrEmpty(id))
                    int.TryParse(_ICustomEncryption.base64d(id), out BusRouteId);

                var model = new TransportRouteModel();

                model.BusRouteId = BusRouteId;

                model.IsPickRoot = true;
                model = prepareBusroutedetViewmodel(model);
                //if (model.Rows.Count > 0)
                //{
                //if (IsExcel == "1")
                //{
                //    _IExportHelper.ExportToExcel(dt, Title: Title, Subtitle: Subtitle);
                //}
                //else if (IsExcel == "2")
                //{
                //   _IExportHelper.ExportToPrint(dt, Title: Title, Subtitle: Subtitle);
                //}
                //else
                //{
                //    _IExportHelper.ExportToPDF(dt, Title: Title, Subtitle: Subtitle);
                //}
                // }
                return View(model);

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public ActionResult ExporttransportRouteDetail(string BRI, string IsExcel, string Type = "")
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                IList<BusRouteStudents> TransportRouteList = new List<BusRouteStudents>();
                var BusRouteId = 0;
                if (!string.IsNullOrEmpty(BRI))
                    int.TryParse(_ICustomEncryption.base64d(BRI), out BusRouteId);


                var TrsptFacility = new List<TptFacility>();
                var getStudentActiveStatus = _IStudentMasterService.GetAllStudentStatus(ref count, "Active").FirstOrDefault();
                TrsptFacility = _ITransportService.GetAllTptFacilitys(ref count, BusRoutrId: BusRouteId)
                                                    .Where(m => m.VehicleTypeId == null
                                                          && m.Student.StudentStatusId == getStudentActiveStatus.StudentStatusId
                                                          && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                                          && m.TptTypeDetail.TptType.TptType1.ToLower() == "school").ToList();

                var droprouteList = _ITransportService.GetAllTptFacilitys(ref count)
                                                   .Where(m => m.DropRouteId == BusRouteId && m.VehicleTypeId == null
                                                         && m.Student.StudentStatusId == getStudentActiveStatus.StudentStatusId
                                                         && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                                         && m.TptTypeDetail.TptType.TptType1.ToLower() == "school").ToList();

                TrsptFacility.AddRange(droprouteList);

                if (!string.IsNullOrEmpty(Type))
                {
                    if (Type.ToLower() == "pick")
                    {
                        TrsptFacility = _ITransportService.GetAllTptFacilitys(ref count, BusRoutrId: BusRouteId)
                                                  .Where(m => m.VehicleTypeId == null
                                                        && m.Student.StudentStatusId == getStudentActiveStatus.StudentStatusId
                                                        && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                                        && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                                                        && (m.TptTypeDetail.TptName.ToLower() == Type.ToLower()
                                                        || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                                                  .OrderBy(m => m.BoardingPointId).ToList();
                    }
                    else
                    {
                        TrsptFacility = _ITransportService.GetAllTptFacilitys(ref count)
                                                 .Where(m => m.DropRouteId == BusRouteId
                                                       && m.VehicleTypeId == null
                                                       && m.Student.StudentStatusId == getStudentActiveStatus.StudentStatusId
                                                       && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                                       && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                                                       && (m.TptTypeDetail.TptName.ToLower() == Type.ToLower()
                                                       || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                                                 .OrderBy(m => m.BoardingPointId).ToList();

                    }
                }

                string studentguardainreltion = "";
                BusRouteStudents TransportRouteModel = new BusRouteStudents();

                // Guardian Relation type
                var GurdRelationType = _IGuardianService.GetAllRelationTypes(ref count, "Guardian").FirstOrDefault();

                // guardian relations
                var FatherRelation = _IGuardianService.GetAllRelations(ref count, Relation: "Father", RelationtypeId: GurdRelationType.RelationTypeId).FirstOrDefault();
                var MotherRelation = _IGuardianService.GetAllRelations(ref count, Relation: "Mother", RelationtypeId: GurdRelationType.RelationTypeId).FirstOrDefault();
                var GuardianRelation = _IGuardianService.GetAllRelations(ref count, Relation: "Guardian", RelationtypeId: GurdRelationType.RelationTypeId).FirstOrDefault();

                //get contact info type id of contact type mobile
                var contactinfotypeid = _IContactService.GetAllContactInfoTypes(ref count).Where(m => m.ContactInfoType1.ToLower().Contains("mobile")).FirstOrDefault().ContactInfoTypeId;
                //get contact type id of student
                var Studentcontacttypeid = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1.ToLower().Contains("student")).FirstOrDefault().ContactTypeId;
                //get contact type id of guardian/
                var guardiancontacttypeid = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1.ToLower().Contains("guardian")).FirstOrDefault().ContactTypeId;
                //get all guardians
                var Alllguardians = _IGuardianService.GetAllGuardians(ref count).ToList();
                //get All StudentCustodyRIght
                var AllStudentCustodyRights = _IStudentService.GetAllStudentCustodyRights(ref count).Where(f => f.CustodyRight.RelationId != null).ToList();
                //get AllContactInfo
                var AllContactInfo = _IContactService.GetAllContactInfos(ref count);

                foreach (var item in TrsptFacility.Distinct())
                {
                    // check student status
                    // var studentstatusdetail = item.Student.StudentStatusDetails.OrderByDescending(s => s.StatusChangeDate).FirstOrDefault();
                    //if (studentstatusdetail != null && studentstatusdetail.StudentStatu.StudentStatus == "Active")
                    //{
                    studentguardainreltion = "";
                    TransportRouteModel = new BusRouteStudents();
                    TransportRouteModel.StudentName = item.Student.FName + " " + item.Student.LName;
                    var currentSession = _ICatalogMasterService.GetCurrentSession();
                    var Studentclass = _IStudentService.GetAllStudentClasss(ref count, StudentId: item.StudentId, SessionId: currentSession.SessionId).FirstOrDefault();
                    TransportRouteModel.ClassName = Studentclass != null ? Studentclass.ClassMaster.Class : "";
                    TransportRouteModel.RollNo = Studentclass != null ? Studentclass.RollNo : "";
                    TransportRouteModel.AdmnNo = item.Student.AdmnNo;
                    TransportRouteModel.TptType = item.TptTypeDetail.TptName;

                    if (item.TptTypeDetail.TptName.ToLower() == "pick")
                        TransportRouteModel.BoardingPoint = item.BoardingPoint.BoardingPoint1;
                    else if (item.TptTypeDetail.TptName.ToLower() == "drop")
                    {
                        var getBoradingPiont = _IBusService.GetBoardingPointById((int)item.DropPointId);
                        TransportRouteModel.BoardingPoint = getBoradingPiont != null ? getBoradingPiont.BoardingPoint1 : "";
                    }
                    else if (item.TptTypeDetail.TptName.ToLower() == "pick drop")
                    {
                        if (item.BoardingPoint != null)
                            TransportRouteModel.BoardingPoint = item.BoardingPoint.BoardingPoint1;
                        else
                        {
                            var getBoradingPiont = _IBusService.GetBoardingPointById((int)item.DropPointId);
                            TransportRouteModel.BoardingPoint = getBoradingPiont != null ? getBoradingPiont.BoardingPoint1 : "";
                        }
                    }

                    // check student Gender
                    if (item.Student.Gender.Gender1 == "Male")
                        studentguardainreltion = "S/O ";
                    else
                        studentguardainreltion = "D/O ";

                    ////get contact info type id of contact type mobile
                    //var contactinfotypeid = _IContactService.GetAllContactInfoTypes(ref count).Where(m => m.ContactInfoType1.ToLower().Contains("mobile")).FirstOrDefault().ContactInfoTypeId;
                    ////get contact type id of student
                    //var Studentcontacttypeid = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1.ToLower().Contains("student")).FirstOrDefault().ContactTypeId;
                    ////get contact type id of guardian/
                    //var guardiancontacttypeid = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1.ToLower().Contains("guardian")).FirstOrDefault().ContactTypeId;
                    //get all guardians
                    var allguardians = Alllguardians.Where(m => m.StudentId == item.Student.StudentId && m.Relation.Relation1 != null).ToList();
                    // Contact no 
                    var contactinfo = AllContactInfo.Where(m => m.ContactId == item.Student.StudentId && m.ContactInfoTypeId == contactinfotypeid && m.ContactTypeId == Studentcontacttypeid);
                    TransportRouteModel.ContactNo = "";
                    string MobileNo = ""; var guardianname = "";
                    var StudentCustodyRight = AllStudentCustodyRights.Where(f => f.StudentId == item.Student.StudentId && f.IsActive == true).ToList();
                    //var InactiveCustodyRights= allguardians.Where(f => f.StudentId == item.Student.StudentId).ToList();
                    if (StudentCustodyRight.Count > 0)
                    {

                        KSModel.Models.Guardian Guardguardian = new KSModel.Models.Guardian();
                        //Father
                        var RltnCustodyRight = StudentCustodyRight.Where(f => f.CustodyRight.RelationId == FatherRelation.RelationId).FirstOrDefault();
                        if (RltnCustodyRight != null)
                        {
                            Guardguardian = allguardians.Where(f => f.Relation.RelationId == RltnCustodyRight.CustodyRight.RelationId).FirstOrDefault();
                            if (Guardguardian != null)
                            {
                                guardianname = Guardguardian.FirstName + " " + Guardguardian.LastName;
                                var grdcontactinfo = AllContactInfo.Where(f => f.ContactId == Guardguardian.GuardianId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == guardiancontacttypeid && f.Status == true).FirstOrDefault();
                                if (grdcontactinfo != null && grdcontactinfo.ContactInfo1 != null && grdcontactinfo.ContactInfo1 != "")
                                    MobileNo = grdcontactinfo.ContactInfo1;
                            }
                        }
                        if (MobileNo == "")
                        {
                            //Mother
                            RltnCustodyRight = StudentCustodyRight.Where(f => f.CustodyRight.RelationId == MotherRelation.RelationId).FirstOrDefault();
                            if (RltnCustodyRight != null)
                            {
                                Guardguardian = allguardians.Where(f => f.Relation.RelationId == RltnCustodyRight.CustodyRight.RelationId).FirstOrDefault();
                                if (Guardguardian != null)
                                {
                                    guardianname = Guardguardian.FirstName + " " + Guardguardian.LastName;
                                    var grdcontactinfo = AllContactInfo.Where(f => f.ContactId == Guardguardian.GuardianId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == guardiancontacttypeid && f.Status == true).FirstOrDefault();
                                    if (grdcontactinfo != null && grdcontactinfo.ContactInfo1 != null && grdcontactinfo.ContactInfo1 != "")
                                        MobileNo = grdcontactinfo.ContactInfo1;
                                }
                            }
                        }
                        if (MobileNo == "")
                        {
                            //Guardian
                            RltnCustodyRight = StudentCustodyRight.Where(f => f.CustodyRight.RelationId == GuardianRelation.RelationId).FirstOrDefault();
                            if (RltnCustodyRight != null)
                            {
                                Guardguardian = allguardians.Where(f => f.Relation.RelationId == RltnCustodyRight.CustodyRight.RelationId).FirstOrDefault();
                                if (Guardguardian != null)
                                {
                                    guardianname = Guardguardian.FirstName + " " + Guardguardian.LastName;
                                    var grdcontactinfo = AllContactInfo.Where(f => f.ContactId == Guardguardian.GuardianId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == guardiancontacttypeid && f.Status == true).FirstOrDefault();
                                    if (grdcontactinfo != null && grdcontactinfo.ContactInfo1 != null && grdcontactinfo.ContactInfo1 != "")
                                        MobileNo = grdcontactinfo.ContactInfo1;
                                    else if (item.Student.SMSMobileNo != null && item.Student.SMSMobileNo != "")
                                    {
                                        MobileNo = item.Student.SMSMobileNo;
                                    }
                                }
                            }
                        }
                        if (MobileNo != "")
                            TransportRouteModel.ContactNo = studentguardainreltion + guardianname + ": " + MobileNo;
                        else if (item.Student.SMSMobileNo != null && item.Student.SMSMobileNo != "")
                            TransportRouteModel.ContactNo = "Student" + ": " + item.Student.SMSMobileNo;
                    }
                    if (MobileNo == "" && allguardians.Count > 0)
                    {
                        KSModel.Models.Guardian Guardguardian = new KSModel.Models.Guardian();
                        //Father
                        Guardguardian = allguardians.Where(f => f.Relation.RelationId == FatherRelation.RelationId).FirstOrDefault();
                        if (Guardguardian != null)
                        {
                            guardianname = Guardguardian.FirstName + " " + Guardguardian.LastName;
                            var grdcontactinfo = AllContactInfo.Where(f => f.ContactId == Guardguardian.GuardianId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == guardiancontacttypeid && f.Status == true).FirstOrDefault();
                            if (grdcontactinfo != null && grdcontactinfo.ContactInfo1 != null && grdcontactinfo.ContactInfo1 != "")
                                MobileNo = grdcontactinfo.ContactInfo1;
                        }
                        if (MobileNo == "")
                        {
                            //Mother
                            Guardguardian = allguardians.Where(f => f.Relation.RelationId == FatherRelation.RelationId).FirstOrDefault();
                            if (Guardguardian != null)
                            {
                                guardianname = Guardguardian.FirstName + " " + Guardguardian.LastName;
                                var grdcontactinfo = AllContactInfo.Where(f => f.ContactId == Guardguardian.GuardianId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == guardiancontacttypeid && f.Status == true).FirstOrDefault();
                                if (grdcontactinfo != null && grdcontactinfo.ContactInfo1 != null && grdcontactinfo.ContactInfo1 != "")
                                    MobileNo = grdcontactinfo.ContactInfo1;
                            }
                        }
                        if (MobileNo == "")
                        {
                            //Guardian
                            Guardguardian = allguardians.Where(f => f.Relation.RelationId == GuardianRelation.RelationId).FirstOrDefault();
                            if (Guardguardian != null)
                            {
                                guardianname = Guardguardian.FirstName + " " + Guardguardian.LastName;
                                var grdcontactinfo = AllContactInfo.Where(f => f.ContactId == Guardguardian.GuardianId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == guardiancontacttypeid && f.Status == true).FirstOrDefault();
                                if (grdcontactinfo != null && grdcontactinfo.ContactInfo1 != null && grdcontactinfo.ContactInfo1 != "")
                                    MobileNo = grdcontactinfo.ContactInfo1;
                                else if (item.Student.SMSMobileNo != null && item.Student.SMSMobileNo != "")
                                {
                                    MobileNo = item.Student.SMSMobileNo;
                                }
                            }
                        }

                        if (MobileNo != "")
                            TransportRouteModel.ContactNo = studentguardainreltion + guardianname + ": " + MobileNo;
                        else if (item.Student.SMSMobileNo != null && item.Student.SMSMobileNo != "")
                            MobileNo = item.Student.SMSMobileNo;
                    }
                    else if (MobileNo == "" && item.Student.SMSMobileNo != null && item.Student.SMSMobileNo != "")
                    {
                        //guardianname="Student";
                        //  MobileNo = item.Student.SMSMobileNo;
                        TransportRouteModel.ContactNo = "Student" + ": " + item.Student.SMSMobileNo; ;
                    }
                    else if (MobileNo == "")
                    {
                        var stdcontactinfo = AllContactInfo.Where(f => f.ContactId == item.StudentId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == Studentcontacttypeid).FirstOrDefault();
                        if (stdcontactinfo != null && !(string.IsNullOrEmpty(stdcontactinfo.ContactInfo1)))
                            TransportRouteModel.ContactNo = "Student" + ": " + stdcontactinfo.ContactInfo1; ;
                    }
                    //if (contactinfo.Count > 0)
                    //    TransportRouteModel.ContactNo = "Student: " + contactinfo.FirstOrDefault().ContactInfo1;
                    //else
                    //{
                    //    // check custody rights
                    //    KSModel.Models.Guardian Guardguardian = new KSModel.Models.Guardian();
                    //    // get custody right
                    //    var custodyright = item.Student.CustodyRight != null ? item.Student.CustodyRight.CustodyRight1 : "All";
                    //    switch (custodyright)
                    //    {
                    //        case "All":
                    //            Guardguardian = allguardians.FirstOrDefault();
                    //            break;
                    //        case "Father":
                    //            Guardguardian = allguardians.Where(g => g.GuardianTypeId == FatherRelation.RelationId).FirstOrDefault();
                    //            break;
                    //        case "Mother":
                    //            Guardguardian = allguardians.Where(g => g.GuardianTypeId == MotherRelation.RelationId).FirstOrDefault();
                    //            break;
                    //        case "Guardian":
                    //            Guardguardian = allguardians.Where(g => g.GuardianTypeId == GuardianRelation.RelationId).FirstOrDefault();
                    //            break;
                    //    }

                    //    if (Guardguardian != null)
                    //    {
                    //        var guardianname = !string.IsNullOrEmpty(Guardguardian.LastName) ? Guardguardian.FirstName + " " + Guardguardian.LastName : Guardguardian.FirstName;
                    //        contactinfo = _IContactService.GetAllContactInfos(ref count, ContactId: Guardguardian.GuardianId, ContactInfoTypeId: contactinfotypeid, ContactTypeId: guardiancontacttypeid, status: true);
                    //        if (contactinfo.Count > 0)
                    //            TransportRouteModel.ContactNo = studentguardainreltion + guardianname + ": " + contactinfo.FirstOrDefault().ContactInfo1;
                    //    }
                    //}

                    TransportRouteList.Add(TransportRouteModel);
                    // }
                }
                string Title = "Students by Boarding Points";
                string Subtitle = "";
                DataTable dt = new DataTable();
                dt.Columns.Add("AdmnNo", typeof(string));
                dt.Columns.Add("StudentName", typeof(string));
                dt.Columns.Add("ContactNo", typeof(string));
                dt.Columns.Add("ClassName", typeof(string));
                dt.Columns.Add("BoardingPoint", typeof(string));


                dt.Columns.Add("RollNo", typeof(string));

                dt.Columns.Add("TptType", typeof(string));

                foreach (var item in TransportRouteList)
                {
                    DataRow dr = dt.NewRow();
                    dr["AdmnNo"] = item.AdmnNo;
                    dr["StudentName"] = item.StudentName;
                    dr["ContactNo"] = item.ContactNo;
                    dr["ClassName"] = item.ClassName;
                    dr["BoardingPoint"] = item.BoardingPoint;
                    dr["RollNo"] = item.RollNo;
                    dr["TptType"] = item.TptType;
                    dt.Rows.Add(dr);
                }

                if (dt.Rows.Count > 0)
                {
                    if (IsExcel == "1")
                    {
                        _IExportHelper.ExportToExcel(dt, Title: Title, Subtitle: Subtitle);
                    }
                    else if (IsExcel == "2")
                    {
                        _IExportHelper.ExportToPrint(dt, Title: Title, Subtitle: Subtitle);
                    }
                    else if (IsExcel == "3")
                    {
                        _IExportHelper.ExportToPDF(dt, Title: Title, Subtitle: Subtitle);
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
            return null;
        }

        [HttpPost]
        public ActionResult ViewTransportRouteDetail(TransportRouteModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "ViewTransportRouteDetail", "View");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                int BusRouteId = Convert.ToInt32(model.BusRouteId);





                model = prepareBusroutedetViewmodel(model);

                return View(model);

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        #endregion

        public BoardingPointModel PrepareBoardingPoint(BoardingPointModel model)
        {
            // bus route detail 
            model.AvailableBoardingPoint.Add(new BoardingPointModel { BoardingPoint = "---Select---", BoardingPointId = "", Lat = "", Long = "" });
            var allboardingpoints = _IBusService.GetAllBoardingPoints(ref count).Where(f=> (!string.IsNullOrEmpty(f.BoardingPoint1))).OrderBy(f=>f.BoardingPoint1);
            foreach (var item in allboardingpoints)
            {
                BoardingPointModel BoardingPointModel = new BoardingPointModel();
                BoardingPointModel.BoardingPoint = item.BoardingPoint1;
                BoardingPointModel.BoardingPointId = item.BoardingPointId.ToString();
                BoardingPointModel.Lat = item.Latitude;
                BoardingPointModel.Long = item.Longitude;
                model.AvailableBoardingPoint.Add(BoardingPointModel);
            }
            // model.AvailableBoardingPoint = _IDropDownService.BoardingPointList();
            model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
            model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");
            var SchoolLatitude = _ISchoolSettingService.GetAttributeValue("SchoolLatitude");
            var SchoolLongitude = _ISchoolSettingService.GetAttributeValue("SchoolLongitude");
            if (SchoolLatitude == null && SchoolLongitude == null)
            {
                model.Lat = "30.7144035";
                model.Long = "76.7556298";
            }
            else
            {
                model.Lat = SchoolLatitude;
                model.Long = SchoolLongitude;
            }

            //var RouteList=
            model.BusRouteList = _IDropDownService.BusRouteList();

            var currentsession = _ICatalogMasterService.GetCurrentSession();
            model.Sessionstartdate = currentsession.StartDate.ToString();
            model.FeeFrequencyList = _IDropDownService.FeeFrequencyList();

            var CurrentSessiondt = _ICatalogMasterService.GetAllSessions(ref count, Isdeafult: true).FirstOrDefault();
            if (CurrentSessiondt != null)
            {
                model.CurrentSessionStartDate = CurrentSessiondt.StartDate.Value.ToString("dd/MM/yyyy");
                model.CurrentDateForTodate = DateTime.Now.Date.ToString("dd/MM/yyyy");
            }
            return model;
        }

        public ActionResult BoardingPoint()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Bus Route", "View");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {

                var model = new BoardingPointModel();

                PrepareBoardingPoint(model);
                return View(model);

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }

        public BoardingPointModel checkvalidationBoardingPoint(BoardingPointModel model, out bool modelstate)
        {
            modelstate = true;
            if (model.BoardingPointId != "new" && model.BoardingPointId != null && Convert.ToInt32(model.BoardingPointId) > 0)
            {

                var boardingpointfee = _IBusService.GetAllBoardingPointFees(ref count).Where(m => m.BoardingPointId == Convert.ToInt32(model.BoardingPointId) && m.BoardingPointFeeId != model.BoardingPointFeeId && m.EndDate == null).ToList();
                if (boardingpointfee.Count() > 0)
                {
                    modelstate = false;
                    ViewBag.ErrorMsg = "Close Previous Boarding Point";
                }
            }
            return model;
        }

        [HttpPost]
        public ActionResult BoardingPoint(BoardingPointModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Bus Route", "View");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }

            try
            {
                var modelvalid = true;
                var valid = checkvalidationBoardingPoint(model, out modelvalid);
                if (modelvalid)
                {
                    if (model.BoardingPointId != "new" && Convert.ToInt32(model.BoardingPointId) > 0)
                    {
                        var boardingpoint = _IBusService.GetBoardingPointById(Convert.ToInt32(model.BoardingPointId));
                        boardingpoint.BoardingPoint1 = model.BoardingPoint;
                        boardingpoint.Latitude = !String.IsNullOrEmpty(model.BoardingPointLatitude) ? model.BoardingPointLatitude : "";
                        boardingpoint.Longitude = !String.IsNullOrEmpty(model.BoardingPointLongitude) ? model.BoardingPointLongitude : "";

                        _IBusService.UpdateBoardingPoint(boardingpoint);
                        if (model.BoardingPointFeeId > 0)
                        {
                        if (model.strIsExistInAnotherTable.ToLower() == "false") { 
                            var boardingpointFee = _IBusService.GetBoardingPointFeeById(model.BoardingPointFeeId, true);
                            if (boardingpointFee != null && boardingpointFee.EndDate == null)
                            {
                                boardingpointFee.TransportFee_1 = model.SinglePickOrDropFee != null ? model.SinglePickOrDropFee : null;
                                boardingpointFee.TransportFee_2 = model.PickAndDropFee != null ? model.PickAndDropFee : null;
                                if (model.FeeFrequencyId != null)
                                    boardingpointFee.FeeFrequencyId = model.FeeFrequencyId;
                                else
                                    boardingpointFee.FeeFrequencyId = null;
                                boardingpointFee.EfectiveDate = FormatDate(model.EffectiveDate);
                                boardingpointFee.BoardingPointId = boardingpoint.BoardingPointId;
                                _IBusService.UpdateBoardingPointFee(boardingpointFee);
                              //  ViewBag.Msg = "Boarding point Updated Successfully";
                                TempData["SuccessNotification"]= "Boarding point Updated Successfully";
                                }
                            }
                            else
                            {
                                BoardingPointFee BoardingPointFee = new BoardingPointFee();
                                BoardingPointFee.TransportFee_1 = model.SinglePickOrDropFee != null ? model.SinglePickOrDropFee : null;
                                BoardingPointFee.TransportFee_2 = model.PickAndDropFee != null ? model.PickAndDropFee : null;
                                if (model.FeeFrequencyId != null)
                                    BoardingPointFee.FeeFrequencyId = model.FeeFrequencyId;
                                else
                                    BoardingPointFee.FeeFrequencyId = null;
                                BoardingPointFee.EfectiveDate = FormatDate(model.EffectiveDate);
                                BoardingPointFee.BoardingPointId = boardingpoint.BoardingPointId;
                                _IBusService.InsertBoardingPointFee(BoardingPointFee);
                               // ViewBag.Msg = "Boarding point Added Successfully";
                                TempData["SuccessNotification"] = "Boarding point Added Successfully";
                            }
                        }
                        else
                        {
                            BoardingPointFee BoardingPointFee = new BoardingPointFee();
                            BoardingPointFee.TransportFee_1 = model.SinglePickOrDropFee != null ? model.SinglePickOrDropFee : null;
                            BoardingPointFee.TransportFee_2 = model.PickAndDropFee != null ? model.PickAndDropFee : null;
                            if (model.FeeFrequencyId != null)
                                BoardingPointFee.FeeFrequencyId = model.FeeFrequencyId;
                            else
                                BoardingPointFee.FeeFrequencyId = null;
                            BoardingPointFee.EfectiveDate = FormatDate(model.EffectiveDate);
                            BoardingPointFee.BoardingPointId = boardingpoint.BoardingPointId;
                            _IBusService.InsertBoardingPointFee(BoardingPointFee);
                           // ViewBag.Msg = "Boarding point Added Successfully";
                            TempData["SuccessNotification"] = "Boarding point Added Successfully";
                        }
                    }
                    else
                    {
                        BoardingPoint boardingpoint = new BoardingPoint();
                        boardingpoint.BoardingPoint1 = !String.IsNullOrEmpty(model.BoardingPoint) ? model.BoardingPoint : "";
                        boardingpoint.Latitude = !String.IsNullOrEmpty(model.BoardingPointLatitude) ? model.BoardingPointLatitude : "";
                        boardingpoint.Longitude = !String.IsNullOrEmpty(model.BoardingPointLongitude) ? model.BoardingPointLongitude : "";

                        _IBusService.InsertBoardingPoint(boardingpoint);
                        BoardingPointFee BoardingPointFee = new BoardingPointFee();
                        BoardingPointFee.TransportFee_1 = model.SinglePickOrDropFee != null ? model.SinglePickOrDropFee : null;
                        BoardingPointFee.TransportFee_2 = model.PickAndDropFee != null ? model.PickAndDropFee : null;
                        if (model.FeeFrequencyId != null)
                            BoardingPointFee.FeeFrequencyId = model.FeeFrequencyId;
                        else
                            BoardingPointFee.FeeFrequencyId = null;
                        BoardingPointFee.EfectiveDate = FormatDate(model.EffectiveDate);
                        BoardingPointFee.BoardingPointId = boardingpoint.BoardingPointId;
                        _IBusService.InsertBoardingPointFee(BoardingPointFee);
                      //  ViewBag.Msg = "Boarding point Added Successfully";
                        TempData["SuccessNotification"] = "Boarding point Added Successfully";
                    }

                    //ModelState.Clear();
                    //var model1 = new BoardingPointModel();
                    //model1 = PrepareBoardingPoint(model1);
                    return RedirectToAction("BoardingPoint");
                }
                else
                {
                    model = PrepareBoardingPoint(model);
                    return View(model);
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        #endregion

        #region Ajax Methods

        #region Export
        public ActionResult Export()
        {
            return Json(new
            {
                status = "success",
                data = ""
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Bus Route

        public ActionResult DeleteBusRoute(string BRI)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Delete");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }

            try
            {
                var BusRouteId = Convert.ToInt32(BRI);
                var logtypeupdate = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
                // TimeTable log Table
                var BusRoutetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "BusRoute").FirstOrDefault();
                var TptRouteDetailstable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "TptRouteDetails").FirstOrDefault();
                // get tiem table by id for validation
                var BusRoute = _IBusService.GetBusRouteById(BusRouteId, true);
                if (BusRoute != null)
                {
                    if (BusRoute.BusRouteDetails.Count == 0)
                    {
                        // firstly , Delete data from TPT Route Detail
                        var tptroutedetail = BusRoute.TptRouteDetails.ToList();
                        foreach (var item in tptroutedetail)
                        {
                            _IBusService.DeleteTptRouteDetail(item.TptRouteDetailId);
                            if (TptRouteDetailstable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, TptRouteDetailstable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(item.TptRouteDetailId, TptRouteDetailstable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete TptRouteDetail with TptRouteDetailId:{0} and BusId:{1}", item.TptRouteDetailId, item.BusId), Request.Browser.Browser);
                            }
                        }

                        _IBusService.DeleteBusRoutes(BusRouteId);
                        if (BusRoutetable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BusRoutetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(BusRoute.BusRouteId, BusRoutetable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete BusRoute with BusRouteId:{0} and BusRoute1:{1}", BusRoute.BusRouteId, BusRoute.BusRoute1), Request.Browser.Browser);
                        }

                        return Json(new
                        {
                            status = "success",
                            data = "Deleted Successfully"
                        });
                    }
                }

                return Json(new
                {
                    status = "failed",
                    data = "Deleted failed"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }

        public ActionResult EndBusRoute(int BRID, DateTime? EndDate)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "TransportRoute", "Close");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }

            try
            {
                var logtypeupdate = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                // TimeTable log Table
                var BusRoutetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "BusRoute").FirstOrDefault();
                // get tiem table by id for validation
                var BusRoute = _IBusService.GetBusRouteById(BRID, true);
                if (BusRoute != null && BusRoute.EffectiveDate > EndDate)
                {
                    return Json(new
                    {
                        status = "failed",
                        msg = "End Date should be greater than Effective Date"
                    });
                }

                if (BusRoute != null)
                {
                    // check if Student exist against bus route 
                    var tptfacilities = BusRoute.TptFacilities.ToList();
                    tptfacilities = tptfacilities.Where(t => t.EffectiveDate >= EndDate && (t.EndDate == null || (t.EndDate != null && t.EndDate >= EndDate))).ToList();
                    if (tptfacilities.Count > 0)
                    {
                        return Json(new
                        {
                            status = "failed",
                            msg = "Warning: Students are assigned to this Bus Route."
                        });
                    }
                    else
                    {
                        BusRoute.EndDate = EndDate;
                        _IBusService.UpdateBusRoute(BusRoute);
                        if (BusRoutetable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BusRoutetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(BusRoute.BusRouteId, BusRoutetable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Update BusRoute with BusRouteId:{0} and BusRoute:{1}", BusRoute.BusRouteId, BusRoute.BusRoute1), Request.Browser.Browser);
                        }

                        return Json(new
                        {
                            status = "success"
                        });
                    }
                }

                return Json(new
                {
                    status = "failed",
                    msg = "Deleted failed"
                });

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }

        public ActionResult InsertSchoolLocation(string lat, string lng)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                string Validationerrors = "";
                // check validation
                if (string.IsNullOrEmpty(lat))
                    Validationerrors += "Latitude required ε";
                if (string.IsNullOrEmpty(lng))
                    Validationerrors += "Longitude required ε";
                if (!string.IsNullOrEmpty(Validationerrors))
                {
                    return Json(new
                    {
                        status = "failed",
                        data = Validationerrors
                    });
                }
                else
                {
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    // TimeTable log Table
                    var SchoolSettingtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "SchoolSetting").FirstOrDefault();
                    var BoardingPointtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "BoardingPoint").FirstOrDefault();
                    // get tiem table by id for validation
                    var SchoolLatitude = _ISchoolSettingService.GetSchoolSettingByAttribute("SchoolLatitude");
                    if (SchoolLatitude != null)
                    {
                        SchoolLatitude.AttributeValue = lat;
                        _ISchoolSettingService.UpdateSchoolSetting(SchoolLatitude);
                        if (SchoolSettingtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, SchoolSettingtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(SchoolLatitude.SchoolSettingid, SchoolSettingtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add SchoolSetting with SchoolSettingid:{0} and AttributeName:{1}", SchoolLatitude.SchoolSettingid, SchoolLatitude.AttributeName), Request.Browser.Browser);
                        }
                    }

                    var SchoolLongitude = _ISchoolSettingService.GetSchoolSettingByAttribute("SchoolLongitude");
                    if (SchoolLongitude != null)
                    {
                        SchoolLongitude.AttributeValue = lng;
                        _ISchoolSettingService.UpdateSchoolSetting(SchoolLongitude);
                        if (SchoolSettingtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, SchoolSettingtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(SchoolLongitude.SchoolSettingid, SchoolSettingtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add SchoolSetting with SchoolSettingid:{0} and AttributeName:{1}", SchoolLongitude.SchoolSettingid, SchoolLongitude.AttributeName), Request.Browser.Browser);
                        }
                    }

                    // insert School Location Boarding Point
                    BoardingPoint BoardingPoint = new BoardingPoint();
                    BoardingPoint.BoardingPoint1 = "School";
                    BoardingPoint.Latitude = lat;
                    BoardingPoint.Longitude = lng;
                    _IBusService.InsertBoardingPoint(BoardingPoint);
                    if (BoardingPointtable != null)
                    {
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BoardingPointtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog(BoardingPoint.BoardingPointId, BoardingPointtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add BoardingPoint with BoardingPointId:{0} and BoardingPoint:{1}", BoardingPoint.BoardingPointId, BoardingPoint.BoardingPoint1), Request.Browser.Browser);
                    }

                    return Json(new
                    {
                        status = "success",
                        data = "School location added successfully"
                    });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }

        public ActionResult InsertBoardingPoint(BoardingPointModel model)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                string Validationerrors = "";
                int BoardingpointId = 0;
                if (!string.IsNullOrEmpty(model.BoardingPointId))
                    BoardingpointId = Convert.ToInt32(model.BoardingPointId);

                // get all boarding points
                var boardingpoints = _IBusService.GetAllBoardingPoints(ref count);
                boardingpoints = boardingpoints.Where(b => b.BoardingPointId != BoardingpointId).ToList();

                // check validation
                if (string.IsNullOrEmpty(model.BoardingPoint))
                    Validationerrors += "Boarding point required ε";
                else
                {
                    if (model.BoardingPoint.Length > 100)
                        Validationerrors += "Boarding point length invalid ε";
                    // check unique
                    var isboardpointexist = boardingpoints.Where(b => b.BoardingPoint1.ToLower() == model.BoardingPoint.ToLower()).ToList();
                    if (isboardpointexist.Count > 0)
                        Validationerrors += "Boarding point already exist ε";
                }

                if (string.IsNullOrEmpty(model.Lat))
                    Validationerrors += "Latitude required ε";
                else
                {
                    if (model.Lat.Length > 100)
                        Validationerrors += "Latitude length invalid ε";
                    // check unique
                    var isboardpointexist = boardingpoints.Where(b => b.Latitude == model.Lat).ToList();
                    if (isboardpointexist.Count > 0)
                        Validationerrors += "Latitude already exist ε";
                }

                if (string.IsNullOrEmpty(model.Long))
                    Validationerrors += "Longitude required ε";
                else
                {
                    if (model.Long.Length > 100)
                        Validationerrors += "Longitude length invalid ε";
                    // check unique
                    var isboardpointexist = boardingpoints.Where(b => b.Longitude == model.Long).ToList();
                    if (isboardpointexist.Count > 0)
                        Validationerrors += "Longitude already exist ε";
                }

                if (!string.IsNullOrEmpty(Validationerrors))
                {
                    return Json(new
                    {
                        status = "failed",
                        data = Validationerrors
                    });
                }
                else
                {
                    string messgae = "";
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    // TimeTable log Table
                    var BoardingPointtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "BoardingPoint").FirstOrDefault();

                    BoardingPoint BoardingPoint = new BoardingPoint();
                    if (BoardingpointId > 0)
                    {
                        BoardingPoint = _IBusService.GetBoardingPointById(BoardingpointId, true);
                        BoardingPoint.BoardingPoint1 = model.BoardingPoint;
                        BoardingPoint.Latitude = model.Lat;
                        BoardingPoint.Longitude = model.Long;
                        _IBusService.UpdateBoardingPoint(BoardingPoint);
                        if (model.BoardingPointFeeId > 0)
                        {
                            var boardingpointfee = _IBusService.GetBoardingPointFeeById(model.BoardingPointFeeId);

                            boardingpointfee.TransportFee_1 = model.SinglePickOrDropFee != null ? model.SinglePickOrDropFee : null;
                            boardingpointfee.TransportFee_2 = model.PickAndDropFee != null ? model.PickAndDropFee : null;
                            if (model.FeeFrequencyId != null)
                                boardingpointfee.FeeFrequencyId = model.FeeFrequencyId;
                            else
                                boardingpointfee.FeeFrequencyId = null;
                            boardingpointfee.EfectiveDate = FormatDate(model.EffectiveDate);
                            boardingpointfee.BoardingPointId = BoardingPoint.BoardingPointId;
                            _IBusService.UpdateBoardingPointFee(boardingpointfee);
                        }
                        else
                        {
                            BoardingPointFee BoardingPointFee = new BoardingPointFee();
                            BoardingPointFee.TransportFee_1 = model.SinglePickOrDropFee != null ? model.SinglePickOrDropFee : null;
                            BoardingPointFee.TransportFee_2 = model.PickAndDropFee != null ? model.PickAndDropFee : null;
                            if (model.FeeFrequencyId != null)
                                BoardingPointFee.FeeFrequencyId = model.FeeFrequencyId;
                            else
                                BoardingPointFee.FeeFrequencyId = null;
                            BoardingPointFee.EfectiveDate = FormatDate(model.EffectiveDate);
                            BoardingPointFee.BoardingPointId = BoardingPoint.BoardingPointId;
                            _IBusService.InsertBoardingPointFee(BoardingPointFee);
                        }
                        if (BoardingPointtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BoardingPointtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(BoardingPoint.BoardingPointId, BoardingPointtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update BoardingPoint with BoardingPointId:{0} and BoardingPoint:{1}", BoardingPoint.BoardingPointId, BoardingPoint.BoardingPoint1), Request.Browser.Browser);
                        }
                        messgae = "Update Successfully";
                    }
                    else
                    {
                        BoardingPoint = new BoardingPoint();
                        BoardingPoint.BoardingPoint1 = model.BoardingPoint;
                        BoardingPoint.Latitude = model.Lat;
                        BoardingPoint.Longitude = model.Long;
                        _IBusService.InsertBoardingPoint(BoardingPoint);

                        BoardingPointFee BoardingPointFee = new BoardingPointFee();
                        BoardingPointFee.TransportFee_1 = model.SinglePickOrDropFee != null ? model.SinglePickOrDropFee : null;
                        BoardingPointFee.TransportFee_2 = model.PickAndDropFee != null ? model.PickAndDropFee : null;
                        if (model.FeeFrequencyId != null)
                            BoardingPointFee.FeeFrequencyId = model.FeeFrequencyId;
                        else
                            BoardingPointFee.FeeFrequencyId = null;
                        BoardingPointFee.EfectiveDate = FormatDate(model.EffectiveDate);
                        BoardingPointFee.BoardingPointId = BoardingPoint.BoardingPointId;
                        _IBusService.InsertBoardingPointFee(BoardingPointFee);
                        if (BoardingPointtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BoardingPointtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(BoardingPoint.BoardingPointId, BoardingPointtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add BoardingPoint with BoardingPointId:{0} and BoardingPoint:{1}", BoardingPoint.BoardingPointId, BoardingPoint.BoardingPoint1), Request.Browser.Browser);
                        }
                        messgae = "Added Successfully";
                    }

                    // boarding point list
                    IList<BoardingPointModel> BoardingPointddl = new List<BoardingPointModel>();
                    var allboardingpoints = _IBusService.GetAllBoardingPoints(ref count);
                    foreach (var item in allboardingpoints)
                    {
                        BoardingPointModel BoardingPointModel = new BoardingPointModel();
                        BoardingPointModel.BoardingPoint = item.BoardingPoint1;
                        BoardingPointModel.BoardingPointId = item.BoardingPointId.ToString();
                        BoardingPointModel.Lat = item.Latitude;
                        BoardingPointModel.Long = item.Longitude;
                        BoardingPointddl.Add(BoardingPointModel);
                    }

                    return Json(new
                    {
                        status = "success",
                        msg = messgae,
                        BPddl = BoardingPointddl,
                        BPIK = BoardingPoint.BoardingPointId
                    });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }

        }

        // get existing bus route detail
        public ActionResult GetBusRoutedetailByRouteId(int BRID, bool IsPickRoute = false)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                var BoardingIndex = 0;
                // get bus route detail by bus route id
                IList<BusRouteDetailModel> TransportRouteList = new List<BusRouteDetailModel>();
                // get bus route and check end date
                var busroute = _IBusService.GetBusRouteById(BRID);
                if (busroute != null && (busroute.EndDate == null || (busroute.EndDate != null && busroute.EndDate <= DateTime.UtcNow.Date)))
                {
                    var busroutedetail = _IBusService.GetAllBusRouteDetails(ref count, BusRouteId: BRID);
                    busroutedetail = busroutedetail.Where(b => b.Status != false && b.EndDate == null).OrderBy(b => b.BoardingIndex).ToList();
                    // check route drop or pick
                    if (IsPickRoute)
                    {
                        busroutedetail = busroutedetail.Where(b => b.PickupTime != null).ToList();
                        foreach (var item in busroutedetail)
                        {
                            BusRouteDetailModel TransportRouteModel = new BusRouteDetailModel();
                            TransportRouteModel.BusRouteDetailId = item.BusRouteDetailId;
                            TransportRouteModel.BoardingIndex = item.BoardingIndex;
                            TransportRouteModel.BoardingPointId = item.BoardingPointId;
                            TransportRouteModel.BoardingPoint = item.BoardingPoint.BoardingPoint1;
                            TransportRouteModel.Lat = item.BoardingPoint.Latitude;
                            TransportRouteModel.Long = item.BoardingPoint.Longitude;
                            TransportRouteModel.Distance = item.Distance;

                            TransportRouteModel.PickTime = item.PickupTime.HasValue ? ConvertTime(item.PickupTime.Value, true) : "";
                            TransportRouteModel.DisplayPickTime = item.PickupTime.HasValue ? ConvertTime(item.PickupTime.Value) : "";
                            TransportRouteModel.DropTime = "";
                            TransportRouteModel.DisplayDropTime = "";

                            TransportRouteModel.IsPickRoot = true;

                            // check if use by student
                            TransportRouteModel.IsDeleted = true;
                            TransportRouteModel.IsInUse = false;
                            var tptfacility = _ITransportService.GetAllTptFacilitys(ref count, BusRoutrId: item.BusRouteId, BoardingPointId: item.BoardingPointId);
                            if (tptfacility.Count > 0)
                            {
                                TransportRouteModel.IsDeleted = false;
                                TransportRouteModel.IsInUse = true;
                            }

                            TransportRouteList.Add(TransportRouteModel);
                        }
                    }
                    else
                    {
                        busroutedetail = busroutedetail.Where(b => b.DropTime != null).ToList();
                        foreach (var item in busroutedetail)
                        {
                            BusRouteDetailModel TransportRouteModel = new BusRouteDetailModel();
                            TransportRouteModel.BusRouteDetailId = item.BusRouteDetailId;
                            TransportRouteModel.BoardingIndex = item.BoardingIndex;
                            TransportRouteModel.BoardingPointId = item.BoardingPointId;
                            TransportRouteModel.BoardingPoint = item.BoardingPoint.BoardingPoint1;
                            TransportRouteModel.Lat = item.BoardingPoint.Latitude;
                            TransportRouteModel.Long = item.BoardingPoint.Longitude;
                            TransportRouteModel.Distance = item.PickupTime.HasValue ? 0 : item.Distance;

                            TransportRouteModel.PickTime = "";
                            TransportRouteModel.DisplayPickTime = "";
                            TransportRouteModel.DropTime = item.DropTime.HasValue ? ConvertTime(item.DropTime.Value, true) : "";
                            TransportRouteModel.DisplayDropTime = item.DropTime.HasValue ? ConvertTime(item.DropTime.Value) : "";

                            TransportRouteModel.IsPickRoot = false;

                            // check if use by student
                            TransportRouteModel.IsDeleted = true;
                            TransportRouteModel.IsInUse = false;
                            var tptfacility = _ITransportService.GetAllTptFacilitys(ref count, BusRoutrId: item.BusRouteId, BoardingPointId: item.BoardingPointId);
                            if (tptfacility.Count > 0)
                            {
                                TransportRouteModel.IsDeleted = false;
                                TransportRouteModel.IsInUse = true;
                            }

                            TransportRouteList.Add(TransportRouteModel);
                        }
                    }

                    BoardingIndex = busroutedetail.LastOrDefault() != null ? (int)busroutedetail.LastOrDefault().BoardingIndex + 1 : 1;
                }

                return Json(new
                {
                    status = "success",
                    data = TransportRouteList,
                    BoardingIndex = BoardingIndex
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }

        // get existing bus route detail
        public ActionResult GetStudentsList(string BRI, string RouteType = "", int sessionId = 0)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                IList<BusRouteStudents> TransportRouteList = new List<BusRouteStudents>();
                var BusRouteId = 0;
                if (!string.IsNullOrEmpty(BRI))
                    int.TryParse(_ICustomEncryption.base64d(BRI), out BusRouteId);

                var StuStatus = _IStudentMasterService.GetAllStudentStatus(ref count, "Active").FirstOrDefault();
                var TrsptFacility = new List<TptFacility>();
                TrsptFacility = _ITransportService.GetAllTptFacilitys(ref count, BusRoutrId: BusRouteId)
                                                   .Where(m => m.VehicleTypeId == null

                                                         && m.Student.StudentStatusId == StuStatus.StudentStatusId
                                                         && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                                         && m.TptTypeDetail.TptType.TptType1.ToLower() == "school").ToList();

                var droprouteList = _ITransportService.GetAllTptFacilitys(ref count)
                                                   .Where(m => m.DropRouteId == BusRouteId && m.VehicleTypeId == null
                                                         && m.Student.StudentStatusId == StuStatus.StudentStatusId
                                                         && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                                         && m.TptTypeDetail.TptType.TptType1.ToLower() == "school").ToList();

                TrsptFacility.AddRange(droprouteList);
                if (!string.IsNullOrEmpty(RouteType))
                {
                    if (RouteType.ToLower() == "pick")
                    {
                        TrsptFacility = _ITransportService.GetAllTptFacilitys(ref count, BusRoutrId: BusRouteId)
                                                  .Where(m => m.VehicleTypeId == null
                                                        && m.Student.StudentStatusId == StuStatus.StudentStatusId
                                                        && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                                        && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                                                        && (m.TptTypeDetail.TptName.ToLower() == RouteType.ToLower()
                                                        || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                                                  .OrderBy(m => m.BoardingPointId).ToList();
                    }
                    else
                    {
                        TrsptFacility = _ITransportService.GetAllTptFacilitys(ref count)
                                                 .Where(m => m.DropRouteId == BusRouteId
                                                       && m.VehicleTypeId == null
                                                       && m.Student.StudentStatusId == StuStatus.StudentStatusId
                                                       && (m.EndDate == null || m.EndDate >= DateTime.Now.Date)
                                                       && m.TptTypeDetail.TptType.TptType1.ToLower() == "school"
                                                       && (m.TptTypeDetail.TptName.ToLower() == RouteType.ToLower()
                                                       || m.TptTypeDetail.TptName.ToLower() == "pick drop"))
                                                 .OrderBy(m => m.BoardingPointId).ToList();

                    }
                }


                string studentguardainreltion = "";
                BusRouteStudents TransportRouteModel = new BusRouteStudents();
                // Guardian Relation type
                var GurdRelationType = _IGuardianService.GetAllRelationTypes(ref count, "Guardian").FirstOrDefault();

                // guardian relations
                var FatherRelation = _IGuardianService.GetAllRelations(ref count, Relation: "Father", RelationtypeId: GurdRelationType.RelationTypeId).FirstOrDefault();
                var MotherRelation = _IGuardianService.GetAllRelations(ref count, Relation: "Mother", RelationtypeId: GurdRelationType.RelationTypeId).FirstOrDefault();
                var GuardianRelation = _IGuardianService.GetAllRelations(ref count, Relation: "Guardian", RelationtypeId: GurdRelationType.RelationTypeId).FirstOrDefault();


                //get contact info type id of contact type mobile
                var contactinfotypeid = _IContactService.GetAllContactInfoTypes(ref count).Where(m => m.ContactInfoType1.ToLower().Contains("mobile")).FirstOrDefault().ContactInfoTypeId;
                //get contact type id of student
                var Studentcontacttypeid = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1.ToLower().Contains("student")).FirstOrDefault().ContactTypeId;
                //get contact type id of guardian/
                var guardiancontacttypeid = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1.ToLower().Contains("guardian")).FirstOrDefault().ContactTypeId;
                //get all guardians
                var Alllguardians = _IGuardianService.GetAllGuardians(ref count).ToList();
                //get All StudentCustodyRIght
                var AllStudentCustodyRights = _IStudentService.GetAllStudentCustodyRights(ref count).Where(f=>f.CustodyRight.RelationId!=null).ToList();
                //get AllContactInfo
                var AllContactInfo = _IContactService.GetAllContactInfos(ref count);

                foreach (var item in TrsptFacility.Distinct())
                {
                    // check student status
                    // var studentstatusdetail = item.Student.StudentStatusDetails.OrderByDescending(s => s.StatusChangeDate).FirstOrDefault();
                    // if (studentstatusdetail != null && studentstatusdetail.StudentStatu.StudentStatus == "Active")
                    
                    var currentSession = _ICatalogMasterService.GetCurrentSession();
                    if (sessionId > 0)
                    {
                        currentSession = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.SessionId == sessionId).FirstOrDefault();
                    }
                    //get students only for current session 
                    var Studentclass = _IStudentService.GetAllStudentClasss(ref count, StudentId: item.StudentId, SessionId: currentSession.SessionId).FirstOrDefault();
                    if (Studentclass != null)
                    {
                        studentguardainreltion = "";
                        TransportRouteModel = new BusRouteStudents();
                        TransportRouteModel.StudentName = item.Student.FName + " " + item.Student.LName;
                        TransportRouteModel.ClassName = Studentclass != null ? Studentclass.ClassMaster.Class : "";

                        TransportRouteModel.RollNo = "";
                        if (Studentclass != null)
                            TransportRouteModel.RollNo = !string.IsNullOrEmpty(Studentclass.RollNo) ? Studentclass.RollNo : "";

                        TransportRouteModel.AdmnNo = item.Student.AdmnNo;
                        TransportRouteModel.TptType = item.TptTypeDetail.TptName;

                        if (item.TptTypeDetail.TptName.ToLower() == "pick")
                            TransportRouteModel.BoardingPoint = item.BoardingPoint.BoardingPoint1;
                        else if (item.TptTypeDetail.TptName.ToLower() == "drop")
                        {
                            var getBoradingPiont = _IBusService.GetBoardingPointById((int)item.DropPointId);
                            TransportRouteModel.BoardingPoint = getBoradingPiont != null ? getBoradingPiont.BoardingPoint1 : "";
                        }
                        else if (item.TptTypeDetail.TptName.ToLower() == "pick drop")
                        {
                            if (item.BoardingPoint != null)
                                TransportRouteModel.BoardingPoint = item.BoardingPoint.BoardingPoint1;
                            else
                            {
                                var getBoradingPiont = _IBusService.GetBoardingPointById((int)item.DropPointId);
                                TransportRouteModel.BoardingPoint = getBoradingPiont != null ? getBoradingPiont.BoardingPoint1 : "";
                            }
                        }

                        // check student Gender
                        if (item.Student.Gender.Gender1 == "Male")
                            studentguardainreltion = "S/O ";
                        else
                            studentguardainreltion = "D/O ";

                        ////get contact info type id of contact type mobile
                        //var contactinfotypeid = _IContactService.GetAllContactInfoTypes(ref count).Where(m => m.ContactInfoType1.ToLower().Contains("mobile")).FirstOrDefault().ContactInfoTypeId;
                        ////get contact type id of student
                        //var Studentcontacttypeid = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1.ToLower().Contains("student")).FirstOrDefault().ContactTypeId;
                        ////get contact type id of guardian/
                        //var guardiancontacttypeid = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1.ToLower().Contains("guardian")).FirstOrDefault().ContactTypeId;
                        //get all guardians
                        var allguardians = Alllguardians.Where(m=>m.StudentId== item.Student.StudentId && m.Relation.Relation1 != null).ToList();
                        // Contact no 
                        var contactinfo = AllContactInfo.Where(m=>m.ContactId==item.Student.StudentId&& m.ContactInfoTypeId== contactinfotypeid &&m.ContactTypeId== Studentcontacttypeid);
                        TransportRouteModel.ContactNo = "";
                        string MobileNo = ""; var guardianname = "";
                        var StudentCustodyRight = AllStudentCustodyRights.Where(f => f.StudentId == item.Student.StudentId && f.IsActive == true).ToList();
                        //var InactiveCustodyRights= allguardians.Where(f => f.StudentId == item.Student.StudentId).ToList();
                        if (StudentCustodyRight.Count >0)
                        {
                           
                            KSModel.Models.Guardian Guardguardian = new KSModel.Models.Guardian();
                            //Father
                            var RltnCustodyRight = StudentCustodyRight.Where(f => f.CustodyRight.RelationId == FatherRelation.RelationId).FirstOrDefault();
                            if (RltnCustodyRight != null)
                            {
                                Guardguardian = allguardians.Where(f => f.Relation.RelationId == RltnCustodyRight.CustodyRight.RelationId).FirstOrDefault();
                                if (Guardguardian != null)
                                {
                                    guardianname = Guardguardian.FirstName + " " + Guardguardian.LastName;
                                    var grdcontactinfo = AllContactInfo.Where(f => f.ContactId == Guardguardian.GuardianId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == guardiancontacttypeid && f.Status == true).FirstOrDefault();
                                    if (grdcontactinfo != null && grdcontactinfo.ContactInfo1 != null && grdcontactinfo.ContactInfo1 != "")
                                        MobileNo = grdcontactinfo.ContactInfo1;
                                }
                            }
                            if (MobileNo=="")
                            {
                                //Mother
                                RltnCustodyRight = StudentCustodyRight.Where(f => f.CustodyRight.RelationId == MotherRelation.RelationId).FirstOrDefault();
                                if (RltnCustodyRight != null)
                                {
                                    Guardguardian = allguardians.Where(f => f.Relation.RelationId == RltnCustodyRight.CustodyRight.RelationId).FirstOrDefault();
                                    if (Guardguardian != null)
                                    {
                                        guardianname = Guardguardian.FirstName + " " + Guardguardian.LastName;
                                        var grdcontactinfo = AllContactInfo.Where(f => f.ContactId == Guardguardian.GuardianId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == guardiancontacttypeid && f.Status == true).FirstOrDefault();
                                        if (grdcontactinfo != null && grdcontactinfo.ContactInfo1 != null && grdcontactinfo.ContactInfo1 != "")
                                            MobileNo = grdcontactinfo.ContactInfo1;
                                    }
                                }
                            }
                            if (MobileNo == "")
                            {
                                //Guardian
                                RltnCustodyRight = StudentCustodyRight.Where(f => f.CustodyRight.RelationId == GuardianRelation.RelationId).FirstOrDefault();
                                if (RltnCustodyRight != null)
                                {
                                    Guardguardian = allguardians.Where(f => f.Relation.RelationId == RltnCustodyRight.CustodyRight.RelationId).FirstOrDefault();
                                    if (Guardguardian != null)
                                    {
                                        guardianname = Guardguardian.FirstName + " " + Guardguardian.LastName;
                                        var grdcontactinfo = AllContactInfo.Where(f => f.ContactId == Guardguardian.GuardianId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == guardiancontacttypeid && f.Status == true).FirstOrDefault();
                                        if (grdcontactinfo != null && grdcontactinfo.ContactInfo1 != null && grdcontactinfo.ContactInfo1 != "")
                                            MobileNo = grdcontactinfo.ContactInfo1;
                                        else if (item.Student.SMSMobileNo != null && item.Student.SMSMobileNo != "")
                                        {
                                            MobileNo = item.Student.SMSMobileNo;
                                        }
                                    }
                                }
                            }
                            if(MobileNo!="")
                              TransportRouteModel.ContactNo = studentguardainreltion + guardianname + ": " + MobileNo;
                            else if (item.Student.SMSMobileNo != null && item.Student.SMSMobileNo != "")
                                TransportRouteModel.ContactNo = "Student" + ": " + item.Student.SMSMobileNo;
                        }
                        if(MobileNo == "" && allguardians.Count >0)
                        {
                            KSModel.Models.Guardian Guardguardian = new KSModel.Models.Guardian();
                            //Father
                                Guardguardian = allguardians.Where(f => f.Relation.RelationId == FatherRelation.RelationId).FirstOrDefault();
                                if (Guardguardian != null)
                                {
                                    guardianname = Guardguardian.FirstName + " " + Guardguardian.LastName;
                                    var grdcontactinfo = AllContactInfo.Where(f => f.ContactId == Guardguardian.GuardianId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == guardiancontacttypeid && f.Status == true).FirstOrDefault();
                                    if (grdcontactinfo != null && grdcontactinfo.ContactInfo1 != null && grdcontactinfo.ContactInfo1 != "")
                                        MobileNo = grdcontactinfo.ContactInfo1;
                                }
                            if (MobileNo=="")
                            {
                                //Mother
                                    Guardguardian = allguardians.Where(f => f.Relation.RelationId == FatherRelation.RelationId).FirstOrDefault();
                                    if (Guardguardian != null)
                                    {
                                        guardianname = Guardguardian.FirstName + " " + Guardguardian.LastName;
                                        var grdcontactinfo = AllContactInfo.Where(f => f.ContactId == Guardguardian.GuardianId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == guardiancontacttypeid && f.Status == true).FirstOrDefault();
                                        if (grdcontactinfo != null && grdcontactinfo.ContactInfo1 != null && grdcontactinfo.ContactInfo1 != "")
                                            MobileNo = grdcontactinfo.ContactInfo1;
                                    }
                            }
                            if (MobileNo=="")
                            {
                                //Guardian
                                    Guardguardian = allguardians.Where(f => f.Relation.RelationId == GuardianRelation.RelationId).FirstOrDefault();
                                    if (Guardguardian != null)
                                    {
                                        guardianname = Guardguardian.FirstName + " " + Guardguardian.LastName;
                                        var grdcontactinfo = AllContactInfo.Where(f => f.ContactId == Guardguardian.GuardianId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == guardiancontacttypeid && f.Status == true).FirstOrDefault();
                                        if (grdcontactinfo != null && grdcontactinfo.ContactInfo1 != null && grdcontactinfo.ContactInfo1 != "")
                                            MobileNo = grdcontactinfo.ContactInfo1;
                                        else if (item.Student.SMSMobileNo != null && item.Student.SMSMobileNo != "")
                                        {
                                            MobileNo = item.Student.SMSMobileNo;
                                        }
                                    }
                            }

                            if (MobileNo != "")
                                TransportRouteModel.ContactNo = studentguardainreltion + guardianname + ": " + MobileNo;
                            else if (item.Student.SMSMobileNo != null && item.Student.SMSMobileNo != "")
                                MobileNo = item.Student.SMSMobileNo;
                        }
                        else if (MobileNo == "" && item.Student.SMSMobileNo != null && item.Student.SMSMobileNo != "")
                        {
                                //guardianname="Student";
                                //  MobileNo = item.Student.SMSMobileNo;
                                TransportRouteModel.ContactNo = "Student" + ": " + item.Student.SMSMobileNo; ;
                        }
                        else if (MobileNo == "")
                        {
                            var stdcontactinfo = AllContactInfo.Where(f => f.ContactId == item.StudentId && f.ContactInfoTypeId == contactinfotypeid && f.ContactTypeId == Studentcontacttypeid).FirstOrDefault();
                            if(stdcontactinfo!=null && !(string.IsNullOrEmpty(stdcontactinfo.ContactInfo1)))
                                TransportRouteModel.ContactNo = "Student" + ": " + stdcontactinfo.ContactInfo1; ;
                        }
                            //////if (contactinfo.Count > 0)
                            //////    TransportRouteModel.ContactNo = "Student: " + contactinfo.FirstOrDefault().ContactInfo1;
                            //////else
                            //////{
                            //////    // check custody rights
                            //////    KSModel.Models.Guardian Guardguardian = new KSModel.Models.Guardian();
                            //////    // get custody right
                            //////    var custodyright = item.Student.CustodyRight != null ? item.Student.CustodyRight.CustodyRight1 : "All";
                            //////    switch (custodyright)
                            //////    {
                            //////        case "All":
                            //////            if (allguardians.Count > 0)
                            //////            {
                            //////                Guardguardian = allguardians.Where(f => f.Relation.Relation1.ToLower() == "Father").FirstOrDefault();
                            //////                if (Guardguardian == null)
                            //////                {
                            //////                    Guardguardian = allguardians.Where(f => f.Relation.Relation1.ToLower() == "Mother").FirstOrDefault();
                            //////                }
                            //////                if (Guardguardian == null)
                            //////                {
                            //////                    Guardguardian = allguardians.Where(f => f.Relation.Relation1.ToLower() == "Guardian").FirstOrDefault();
                            //////                }
                            //////            }
                            //////            break;
                            //////        case "Father":
                            //////            Guardguardian = allguardians.Where(g => g.GuardianTypeId == FatherRelation.RelationId).FirstOrDefault();
                            //////            break;
                            //////        case "Mother":
                            //////            Guardguardian = allguardians.Where(g => g.GuardianTypeId == MotherRelation.RelationId).FirstOrDefault();
                            //////            break;
                            //////        case "Guardian":
                            //////            Guardguardian = allguardians.Where(g => g.GuardianTypeId == GuardianRelation.RelationId).FirstOrDefault();
                            //////            break;
                            //////    }

                            //////    if (Guardguardian != null)
                            //////    {
                            //////        var guardianname = !string.IsNullOrEmpty(Guardguardian.LastName) ? Guardguardian.FirstName + " " + Guardguardian.LastName : Guardguardian.FirstName;
                            //////        contactinfo = _IContactService.GetAllContactInfos(ref count, ContactId: Guardguardian.GuardianId, ContactInfoTypeId: contactinfotypeid, ContactTypeId: guardiancontacttypeid, status: true);
                            //////        if (contactinfo.Count > 0)
                            //////            TransportRouteModel.ContactNo = studentguardainreltion + guardianname + ": " + contactinfo.FirstOrDefault().ContactInfo1;
                            //////    }
                            //////}


                            TransportRouteList.Add(TransportRouteModel);
                    }
                    //}}
                }

                return Json(new
                {
                    status = "success",
                    data = TransportRouteList,
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }

        public ActionResult InsertBusRouteDetail(TransportRouteModel model)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                StringBuilder Validationerrors = new StringBuilder();
                BusRouteDetail BusRouteDetail = new BusRouteDetail();
                TimeSpan ts = DateTime.UtcNow.TimeOfDay;
                var prvboardingindex = new BusRouteDetail();
                var nextboardingindex = new BusRouteDetail();
                var curntdate = DateTime.UtcNow.Date;

                // get bus route detail by bus route for validations
                var busroutedetail = _IBusService.GetAllBusRouteDetails(ref count, BusRouteId: model.BusRouteId);
                // Transport facility
                var tptfacilties = _ITransportService.GetAllTptFacilitys(ref count);

                // check validation
                if (model.BoardingPointId == null)
                    Validationerrors.Append("Boarding point required ε");

                if (model.BoardingIndex == null)
                    Validationerrors.Append("Boarding Index required ε");

                model.IsDuplicateBoardingPointAllowed = false;
                var AllowDuplicateBoardingPointInRoute = _ISchoolSettingService.GetorSetSchoolData("AllowDuplicateBoardingPointInRoute", "0").AttributeValue;
                if (AllowDuplicateBoardingPointInRoute == "1")
                    model.IsDuplicateBoardingPointAllowed = true;

                if (model.IsPickRoot)
                {
                    if (model.BoardingPointId > 0)
                    {
                        if (model.IsDuplicateBoardingPointAllowed==false)
                        {
                            // get boarding point by Busroute
                            var busroutedetailrecord = busroutedetail.Where(b => b.BoardingPointId == model.BoardingPointId && b.PickupTime != null
                                && b.DropTime == null && b.BusRouteDetailId != model.BusRouteDetailId && b.EndDate == null && b.Status == true).ToList();
                            if (busroutedetailrecord.Count > 0)
                                Validationerrors.Append("Boarding point must be Unique ε");
                        }
                        // check if boarding point not assigned to student
                        if (model.BusRouteDetailId > 0)
                        {
                            var busroutedetailbyid = busroutedetail.Where(b => b.BusRouteDetailId == model.BusRouteDetailId).FirstOrDefault();
                            if (busroutedetailbyid != null && busroutedetailbyid.BoardingPointId != model.BoardingPointId)
                            {
                                tptfacilties = tptfacilties.Where(t => t.BoardingPointId != null && t.BoardingPointId == busroutedetailbyid.BoardingPointId &&
                                    t.EffectiveDate >= curntdate && ((t.EndDate != null && t.EndDate <= curntdate) || t.EndDate == null)).ToList();
                                if (tptfacilties.Count > 0)
                                    Validationerrors.Append("Boarding point assigned to Student.Please add new Boarding point. ε");
                            }
                        }
                    }
                    if (model.BoardingIndex != null)
                    {
                        // get boarding index by Busroute
                        var busroutedetailrecord = busroutedetail.Where(b => b.BoardingIndex == model.BoardingIndex
                            && b.PickupTime != null && b.DropTime == null && b.BusRouteDetailId != model.BusRouteDetailId && b.EndDate == null && b.Status == true).ToList();
                        if (busroutedetailrecord.Count > 0)
                            Validationerrors.Append("Boarding Index must be Unique ε");
                    }

                    if (string.IsNullOrEmpty(model.PickTime))
                        Validationerrors.Append("PickTime required ε");
                    else
                    {
                        DateTime t = DateTime.ParseExact(model.PickTime, "hh:mm tt", CultureInfo.InvariantCulture);
                        //if you really need a TimeSpan this will get the time elapsed since midnight:
                        // check time less than existing entries
                        // get busroutedetail by routeid

                        // get one last boarding index and one last boaring index
                        if (model.BoardingIndex != null)
                        {
                            prvboardingindex = busroutedetail.Where(b => b.PickupTime != null && b.DropTime == null && b.EndDate == null && b.Status == true &&
                                b.BoardingIndex == (model.BoardingIndex - 1)).FirstOrDefault();

                            nextboardingindex = busroutedetail.Where(b => b.PickupTime != null && b.DropTime == null && b.EndDate == null && b.Status == true &&
                                b.BoardingIndex == (model.BoardingIndex + 1)).FirstOrDefault();

                            if (prvboardingindex != null || nextboardingindex != null)
                            {
                                if (prvboardingindex != null && (prvboardingindex.PickupTime >= t.TimeOfDay && prvboardingindex.BusRouteDetailId != model.BusRouteDetailId))
                                    Validationerrors.Append("Invalid PickTime ε");

                                else if (nextboardingindex != null && (nextboardingindex.PickupTime <= t.TimeOfDay && nextboardingindex.BusRouteDetailId != model.BusRouteDetailId))
                                    Validationerrors.Append("Invalid PickTime ε");
                            }
                        }

                        // check  PIck time always less than drop time
                        var ispicktimevalid = busroutedetail.Where(b => b.DropTime <= t.TimeOfDay).ToList();
                        if (ispicktimevalid.Count > 0)
                            Validationerrors.Append("PickTime should be less than DropTime ε");
                    }
                }
                else
                {
                    if (model.BoardingPointId > 0)
                    {
                        if (model.IsDuplicateBoardingPointAllowed==false)
                        {
                            // get boarding point by Busroute
                            var busroutedetailrecord = busroutedetail.Where(b => b.BoardingPointId == model.BoardingPointId && b.PickupTime == null && b.Status == true
                                && b.DropTime != null && b.BusRouteDetailId != model.BusRouteDetailId && b.EndDate == null).ToList();
                            if (busroutedetailrecord.Count > 0)
                                Validationerrors.Append("Boarding point must be Unique ε");
                        }
                        // check if boarding point not assigned to student
                        if (model.BusRouteDetailId > 0)
                        {
                            var busroutedetailbyid = busroutedetail.Where(b => b.BusRouteDetailId == model.BusRouteDetailId).FirstOrDefault();
                            if (busroutedetailbyid != null && busroutedetailbyid.BoardingPointId != model.BoardingPointId)
                            {
                                tptfacilties = tptfacilties.Where(t => t.BoardingPointId != null && t.BoardingPointId == busroutedetailbyid.BoardingPointId &&
                                    t.EffectiveDate >= curntdate && ((t.EndDate != null && t.EndDate <= curntdate) || t.EndDate == null)).ToList();
                                if (tptfacilties.Count > 0)
                                    Validationerrors.Append("Boarding point assigned to Student.Please add new Boarding point. ε");
                            }
                        }
                    }
                    if (model.BoardingIndex != null)
                    {
                        // get boarding index by Busroute
                        var busroutedetailrecord = busroutedetail.Where(b => b.BoardingIndex == model.BoardingIndex && b.Status == true
                            && b.PickupTime == null && b.DropTime != null && b.BusRouteDetailId != model.BusRouteDetailId && b.EndDate == null).ToList();
                        if (busroutedetailrecord.Count > 0)
                            Validationerrors.Append("Boarding Index must be Unique ε");
                    }

                    if (string.IsNullOrEmpty(model.DropTime))
                        Validationerrors.Append("DropTime required ε");
                    else
                    {
                        DateTime t = DateTime.ParseExact(model.DropTime, "hh:mm tt", CultureInfo.InvariantCulture);
                        //if you really need a TimeSpan this will get the time elapsed since midnight:
                        // check time less than existing entries
                        // get busroutedetail by routeid
                        if (model.BoardingIndex != null)
                        {
                            prvboardingindex = busroutedetail.Where(b => b.PickupTime == null && b.DropTime != null && b.EndDate == null && b.Status == true &&
                                b.BoardingIndex == (model.BoardingIndex - 1)).FirstOrDefault();

                            nextboardingindex = busroutedetail.Where(b => b.PickupTime == null && b.DropTime != null && b.EndDate == null && b.Status == true &&
                                b.BoardingIndex == (model.BoardingIndex + 1)).FirstOrDefault();

                            if (prvboardingindex != null || nextboardingindex != null)
                            {
                                if (prvboardingindex != null && prvboardingindex.DropTime >= t.TimeOfDay && prvboardingindex.BusRouteDetailId != model.BusRouteDetailId)
                                    Validationerrors.Append("Invalid DropTime ε");

                                else if (nextboardingindex != null && nextboardingindex.DropTime <= t.TimeOfDay && nextboardingindex.BusRouteDetailId != model.BusRouteDetailId)
                                    Validationerrors.Append("Invalid DropTime ε");
                            }
                        }

                        // check  PIck time always less than drop time
                        var isdroptimevalid = busroutedetail.Where(b => b.PickupTime >= t.TimeOfDay).ToList();
                        if (isdroptimevalid.Count > 0)
                            Validationerrors.Append("DropTime should be greater than PickTime ε");
                    }
                }

                if (model.Distance == null)
                    Validationerrors.Append("Distance required ε");

                if (!string.IsNullOrEmpty(Validationerrors.ToString()))
                {
                    return Json(new
                    {
                        status = "failed",
                        data = Validationerrors.ToString()
                    });
                }
                else
                {
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    // TimeTable log Table
                    var BusRouteDetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "BusRouteDetail").FirstOrDefault();

                    // check for update
                    if (model.BusRouteDetailId > 0)
                    {
                        // get bus route detail 
                        BusRouteDetail = _IBusService.GetBusRouteDetailById((int)model.BusRouteDetailId, true);
                        if (BusRouteDetail != null)
                        {
                            if (BusRouteDetail.BoardingPointId != model.BoardingPointId)
                            {
                                BusRouteDetail.Status = false;
                                BusRouteDetail.EndDate = DateTime.UtcNow;

                                _IBusService.UpdateBusRouteDetail(BusRouteDetail);
                                if (BusRouteDetailtable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BusRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(BusRouteDetail.BusRouteDetailId, BusRouteDetailtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update BusRouteDetail with BusRouteDetailId:{0} and BusRouteId:{1}", BusRouteDetail.BusRouteDetailId, BusRouteDetail.BusRouteId), Request.Browser.Browser);
                                }

                                // insert new one
                                BusRouteDetail = new BusRouteDetail();
                                BusRouteDetail.EffectiveDate = DateTime.UtcNow.AddDays(1);
                                BusRouteDetail.EndDate = null;
                                BusRouteDetail.BusRouteId = model.BusRouteId;
                                BusRouteDetail.BoardingPointId = model.BoardingPointId;
                                BusRouteDetail.BoardingIndex = model.BoardingIndex;
                                BusRouteDetail.Status = true;
                                BusRouteDetail.Distance = model.Distance;
                                if (model.IsPickRoot)
                                {
                                    DateTime t = DateTime.ParseExact(model.PickTime, "hh:mm tt", CultureInfo.InvariantCulture);
                                    //if you really need a TimeSpan this will get the time elapsed since midnight:
                                    BusRouteDetail.PickupTime = t.TimeOfDay;
                                    TempData["RouteType"] = "Pick";
                                }
                                else
                                {
                                    DateTime t = DateTime.ParseExact(model.DropTime, "hh:mm tt", CultureInfo.InvariantCulture);
                                    //if you really need a TimeSpan this will get the time elapsed since midnight:
                                    BusRouteDetail.DropTime = t.TimeOfDay;
                                    TempData["RouteType"] = "Drop";
                                }

                                _IBusService.InsertBusRouteDetail(BusRouteDetail);
                                if (BusRouteDetailtable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BusRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(BusRouteDetail.BusRouteDetailId, BusRouteDetailtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add BusRouteDetail with BusRouteDetailId:{0} and BusRouteId:{1}", BusRouteDetail.BusRouteDetailId, BusRouteDetail.BusRouteId), Request.Browser.Browser);
                                }
                            }
                            else
                            {
                                BusRouteDetail.BoardingIndex = model.BoardingIndex;
                                BusRouteDetail.Distance = model.Distance;
                                if (model.IsPickRoot)
                                {
                                    DateTime t = DateTime.ParseExact(model.PickTime, "hh:mm tt", CultureInfo.InvariantCulture);
                                    //if you really need a TimeSpan this will get the time elapsed since midnight:
                                    BusRouteDetail.PickupTime = t.TimeOfDay;
                                    TempData["RouteType"] = "Pick";
                                }
                                else
                                {
                                    DateTime t = DateTime.ParseExact(model.DropTime, "hh:mm tt", CultureInfo.InvariantCulture);
                                    //if you really need a TimeSpan this will get the time elapsed since midnight:
                                    BusRouteDetail.DropTime = t.TimeOfDay;
                                    TempData["RouteType"] = "Drop";
                                }

                                _IBusService.UpdateBusRouteDetail(BusRouteDetail);
                                if (BusRouteDetailtable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BusRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(BusRouteDetail.BusRouteDetailId, BusRouteDetailtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update BusRouteDetail with BusRouteDetailId:{0} and BusRouteId:{1}", BusRouteDetail.BusRouteDetailId, BusRouteDetail.BusRouteId), Request.Browser.Browser);
                                }
                            }
                        }
                    }
                    else
                    {
                        BusRouteDetail = new BusRouteDetail();
                        BusRouteDetail.EffectiveDate = DateTime.UtcNow;
                        BusRouteDetail.EndDate = null;
                        BusRouteDetail.BusRouteId = model.BusRouteId;
                        BusRouteDetail.BoardingPointId = model.BoardingPointId;
                        BusRouteDetail.BoardingIndex = model.BoardingIndex;
                        BusRouteDetail.Status = true;
                        BusRouteDetail.Distance = model.Distance;
                        if (model.IsPickRoot)
                        {
                            DateTime t = DateTime.ParseExact(model.PickTime, "hh:mm tt", CultureInfo.InvariantCulture);
                            //if you really need a TimeSpan this will get the time elapsed since midnight:
                            BusRouteDetail.PickupTime = t.TimeOfDay;
                            TempData["RouteType"] = "Pick";
                        }
                        else
                        {
                            DateTime t = DateTime.ParseExact(model.DropTime, "hh:mm tt", CultureInfo.InvariantCulture);
                            //if you really need a TimeSpan this will get the time elapsed since midnight:
                            BusRouteDetail.DropTime = t.TimeOfDay;
                            TempData["RouteType"] = "Drop";
                        }

                        _IBusService.InsertBusRouteDetail(BusRouteDetail);
                        if (BusRouteDetailtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BusRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(BusRouteDetail.BusRouteDetailId, BusRouteDetailtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add BusRouteDetail with BusRouteDetailId:{0} and BusRouteId:{1}", BusRouteDetail.BusRouteDetailId, BusRouteDetail.BusRouteId), Request.Browser.Browser);
                        }
                    }

                    return Json(new
                    {
                        status = "success",
                        data = "saved successfully",
                        BusRouteDetailId = BusRouteDetail.BusRouteDetailId
                    });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult UpdateBusRouteDetail(TransportRouteModel model)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                BusRouteDetail BusRouteDetail = new BusRouteDetail();
                var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                // TimeTable log Table
                var BusRouteDetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "BusRouteDetail").FirstOrDefault();
                // check for update
                if (model.BusRouteDetailId > 0)
                {
                    // get bus route detail 
                    BusRouteDetail = _IBusService.GetBusRouteDetailById((int)model.BusRouteDetailId, true);
                    if (BusRouteDetail != null)
                    {
                        if (BusRouteDetail.Distance != model.Distance)
                        {
                            BusRouteDetail.Distance = model.Distance;
                            _IBusService.UpdateBusRouteDetail(BusRouteDetail);
                            if (BusRouteDetailtable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BusRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(BusRouteDetail.BusRouteDetailId, BusRouteDetailtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update BusRouteDetail with BusRouteDetailId:{0} and BusRouteId:{1}", BusRouteDetail.BusRouteDetailId, BusRouteDetail.BusRouteId), Request.Browser.Browser);
                            }
                        }
                    }
                }

                return Json(new
                {
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult DeleteBusRouteDetail(int BRDID)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                var logtypeupdate = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
                // TimeTable log Table
                var BusRouteDetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "BusRouteDetail").FirstOrDefault();
                // get tiem table by id for validation
                var BusRouteDetail = _IBusService.GetBusRouteDetailById(BRDID, true);
                if (BusRouteDetail != null)
                {
                    var curntdate = DateTime.UtcNow.Date;
                    // check if boarding point not assigned to student
                    var tptfacilties = _ITransportService.GetAllTptFacilitys(ref count);
                    tptfacilties = tptfacilties.Where(t => t.BoardingPointId != null && t.BusRouteId == BusRouteDetail.BusRouteId && t.BoardingPointId == BusRouteDetail.BoardingPointId &&
                        t.EffectiveDate >= curntdate && ((t.EndDate != null && t.EndDate <= curntdate) || t.EndDate == null)).ToList();

                    if (tptfacilties.Count == 0)
                    {
                        if (BusRouteDetail.BusRouteDetailUsers.Count == 0)
                        {
                            TempData["RouteType"] = BusRouteDetail.PickupTime.HasValue ? "Pick" : "Drop";
                            _IBusService.DeleteBusRouteDetail(BRDID);
                            if (BusRouteDetailtable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BusRouteDetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(BusRouteDetail.BusRouteDetailId, BusRouteDetailtable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete BusRouteDetail with BusRouteDetailId:{0} and BusRouteId:{1}", BusRouteDetail.BusRouteDetailId, BusRouteDetail.BusRouteId), Request.Browser.Browser);
                            }

                            return Json(new
                            {
                                status = "success",
                                data = "Deleted Successfully"
                            });
                        }
                    }
                }

                return Json(new
                {
                    status = "failed",
                    data = "You can't delete Route Boarding point.Already assigned to student."
                });

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }

        }

        #endregion

        #region RouteStudentsSendMessage

        public ActionResult GetStudentWithBoradingPionts(string RouteId, string EffectiveDate, string EndDate, int sessionId=0)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "BoardingPoint", "Delete");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }

            try
            {
                int BusRoute_Id = 0;
                if (!string.IsNullOrEmpty(RouteId))
                    int.TryParse(_ICustomEncryption.base64d(RouteId), out BusRoute_Id);

                DateTime? EffeDate = null;
                if (!string.IsNullOrEmpty(EffectiveDate) || !string.IsNullOrWhiteSpace(EffectiveDate))
                {
                    EffeDate = DateTime.ParseExact(EffectiveDate, "dd/MM/yyyy", null);
                    EffeDate = EffeDate.Value.Date;
                }
                DateTime? EnDate = null;
                if (!string.IsNullOrEmpty(EndDate) || !string.IsNullOrWhiteSpace(EndDate))
                {
                    EnDate = DateTime.ParseExact(EndDate, "dd/MM/yyyy", null);
                    EnDate = EnDate.Value.Date;
                }
                
                  int current_SessionId = _ICatalogMasterService.GetCurrentSession().SessionId;

                  if(sessionId>0){
                   current_SessionId=_ICatalogMasterService.GetAllSessions(ref count).Where(m=>m.SessionId==sessionId).FirstOrDefault().SessionId;
                  }
                // Guardian Relation type
                var GurdRelationType = _IGuardianService.GetAllRelationTypes(ref count, "Guardian").FirstOrDefault();
                // guardian relations
                var FatherRelation = _IGuardianService.GetAllRelations(ref count, Relation: "Father", RelationtypeId: GurdRelationType.RelationTypeId).FirstOrDefault();
                var MotherRelation = _IGuardianService.GetAllRelations(ref count, Relation: "Mother", RelationtypeId: GurdRelationType.RelationTypeId).FirstOrDefault();
                var GuardianRelation = _IGuardianService.GetAllRelations(ref count, Relation: "Guardian", RelationtypeId: GurdRelationType.RelationTypeId).FirstOrDefault();

                var RouteInfoList = new List<RouteInfoModel>();


                string CollaspePanelHtml = "";
                var Rowcounter = 0;

                DataTable dtRouteStudents = _IBusService.GetAllRouteStudents(ref count, BusRoute_Id, current_SessionId);
                if (dtRouteStudents.Rows.Count > 0)
                {
                    CollaspePanelHtml += "<div class='collapse-group'>";
                    var groupedByBoardingPointId = dtRouteStudents.AsEnumerable().GroupBy(row => row.Field<string>("BoardingPoint"));
                    foreach (var group in groupedByBoardingPointId)
                    {
                        Rowcounter++;

                        string formattedTime = "";
                        var getRoute = new BusRouteDetail();
                        //if (!string.IsNullOrEmpty(dtRouteStudents.ro("TptName").ToString()))
                        getRoute = _IBusService.GetAllBusRouteDetails(ref count, BusRouteId: BusRoute_Id)
                                            .Where(x => x.BoardingPoint.BoardingPoint1.ToLower() == group.Key.ToLower()
                                                        && x.PickupTime != null).FirstOrDefault();
                        if (getRoute != null)
                            formattedTime = DateTime.Parse(getRoute.PickupTime.Value.ToString(@"hh\:mm")).ToString("hh:mm tt", CultureInfo.GetCultureInfo("en-US"));

                        //var RouteInfo = new RouteInfoModel();

                        //    int BoardingPoint_Id = Convert.ToInt32(group["BoardingPointId"]);
                        //    string Route_Id = _ICustomEncryption.base64e(dr["BusRouteId"].ToString());

                        var Heading = "heading_" + Rowcounter;
                        var hrefCollaspe = "#collapse_" + Rowcounter;
                        var AriaControl = "collapse_" + Rowcounter;
                        var tableId = "tblcollapse_" + Rowcounter;
                        var CheckBoxParent = "chkParent_" + Rowcounter;
                        var anchorId = "Acollapse_" + Rowcounter;
                        var MainHeaderCheckboxId = "HeaderCheck_" + Rowcounter;
                        var Selected = "totalSelected_" + Rowcounter;
                        var Total = "totalrecords_" + Rowcounter;

                        //CollaspePanelHtml += "<input type='hidden' value='" + _ICustomEncryption.base64e(dr["BoardingPointId"].ToString()) + "'";
                        //CollaspePanelHtml += "<input type='hidden' value='" + Route_Id + "'";
                        CollaspePanelHtml += "<div class='panel panel-default'>";
                        CollaspePanelHtml += "<div class='panel-heading' role='tab' id='" + Heading + "'>";
                        CollaspePanelHtml += "<h4 class='panel-title'>";
                        CollaspePanelHtml += "<input type='checkbox' class='checkboxTrigger' id='" + MainHeaderCheckboxId + "'/><a role='button' data-toggle='collapse' href='" + hrefCollaspe + "'";
                        CollaspePanelHtml += " aria-expanded='true' aria-controls='" + AriaControl + "' class='trigger collapsed' id='" + anchorId + "'>";
                        CollaspePanelHtml += group.Key;
                        CollaspePanelHtml += "</a> ";
                        CollaspePanelHtml += "<span> (" + formattedTime + ")  </span>";
                        CollaspePanelHtml += "<div class='pull-right'><b>";
                        CollaspePanelHtml += "<span id='" + Selected + "'>0</span>/";
                        CollaspePanelHtml += "<span id='" + Total + "'>0</span></b>";
                        CollaspePanelHtml += "</div>";
                        CollaspePanelHtml += "</h4>";
                        CollaspePanelHtml += "</div>";

                        CollaspePanelHtml += "<div id='" + AriaControl + "' class='panel-collapse collapse' role='tabpanel' aria-labelledby='" + Heading + "'>";
                        CollaspePanelHtml += "<div class='panel-body'>";
                        CollaspePanelHtml += " <table class='table table-responsive table-condensed tbleStudents'  id='" + tableId + "' >";
                        CollaspePanelHtml += "<thead class='thead-default'>";
                        CollaspePanelHtml += "<tr>";
                        CollaspePanelHtml += "<th><input type='checkbox' id='" + CheckBoxParent + "' class='row-checkbox1'/></th>";
                        CollaspePanelHtml += "<th hidden>RouteId</th>";
                        CollaspePanelHtml += "<th hidden>BoradingPointId</th>";
                        CollaspePanelHtml += "<th hidden>StudentId</th>";
                        CollaspePanelHtml += "<th>Admn No</th>";
                        CollaspePanelHtml += "<th>Student Name</th>";
                        CollaspePanelHtml += "<th>Contact No</th>";
                        CollaspePanelHtml += "<th>Class</th>";
                        CollaspePanelHtml += "<th>Roll No</th>";
                        CollaspePanelHtml += "<th>Transport Type</th>";
                        CollaspePanelHtml += "<th hidden>HasMobileNo-HasAccount</th>";
                        CollaspePanelHtml += "</tr><tbody>";

                        int InnerRowCount = 0;

                        var CheckBoxChildClass = "CheckBoxChildClass_" + Rowcounter + "()";
                        foreach (DataRow row in group.Distinct())
                        {
                            InnerRowCount++;

                            var CheckBoxChild = "CheckBoxChild_" + Rowcounter + "-" + InnerRowCount;
                            #region BordingPiontStudents
                            CollaspePanelHtml += "<tr>";
                            int Student_Id = 0;
                            if (row.Field<int>("StudentId") != null)
                                Student_Id = row.Field<int>("StudentId");


                            //get contact type id of student
                            var Studentcontacttypeid = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1.ToLower().Contains("student")).FirstOrDefault().ContactTypeId;

                            //get contact info type id of contact type mobile
                            var contactinfotypeid = _IContactService.GetAllContactInfoTypes(ref count).Where(m => m.ContactInfoType1.ToLower().Contains("mobile")).FirstOrDefault().ContactInfoTypeId;

                            // Contact no 
                            var contactinfo = _IContactService.GetAllContactInfos(ref count, ContactId: Student_Id, ContactInfoTypeId: contactinfotypeid, ContactTypeId: Studentcontacttypeid);
                            string HasMobileNo = "0";
                            string HasAccount = "0";
                            if (contactinfo.FirstOrDefault() != null && !string.IsNullOrEmpty(contactinfo.FirstOrDefault().ContactInfo1))
                            {
                                HasMobileNo = "1";
                                var isAppuser = _IUserService.GetAllUsers(ref count).Where(x => x.UserContactId == Student_Id
                                                    && x.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                if (isAppuser != null)
                                    HasAccount = "1";
                            }
                            else
                            {
                                var isAppuser = _IUserService.GetAllUsers(ref count).Where(x => x.UserContactId == Student_Id
                                                    && x.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();
                                if (isAppuser != null)
                                    HasAccount = "1";
                            }


                            string studentguardainreltion = "";
                            if (row.Field<string>("Gender") == "Male")
                                studentguardainreltion = "S/O";
                            else
                                studentguardainreltion = "D/O";

                            //get contact type id of guardian/
                            var guardiancontacttypeid = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1.ToLower().Contains("guardian")).FirstOrDefault().ContactTypeId;
                            //get all guardians
                            var allguardians = _IGuardianService.GetAllGuardians(ref count, StudentId: Student_Id).ToList();
                            string ContactNo = "";

                            var studentinfo = _IStudentService.GetStudentById(Student_Id);
                            var studentClassInfo = _IStudentService.GetAllStudentClasss(ref count, SessionId: current_SessionId,
                                                        StudentId: Student_Id).OrderByDescending(x => x.StudentClassId).FirstOrDefault();

                            if (contactinfo.Count > 0)
                                ContactNo = "Student: " + contactinfo.FirstOrDefault().ContactInfo1;
                            else
                            {
                                // check custody rights
                                KSModel.Models.Guardian Guardguardian = new KSModel.Models.Guardian();
                                string custodyright = "";
                                //if (!string.IsNullOrEmpty(row.Field<string>("CustodyRight")))
                                //    custodyright = row.Field<string>("CustodyRight");
                                if (studentinfo != null)
                                    custodyright = studentinfo.CustodyRight != null ? studentinfo.CustodyRight.CustodyRight1 : "All";
                                else
                                    custodyright = "All";

                                switch (custodyright)
                                {
                                    case "All":
                                        Guardguardian = allguardians.FirstOrDefault();
                                        break;
                                    case "Father":
                                        Guardguardian = allguardians.Where(g => g.GuardianTypeId == FatherRelation.RelationId).FirstOrDefault();
                                        break;
                                    case "Mother":
                                        Guardguardian = allguardians.Where(g => g.GuardianTypeId == MotherRelation.RelationId).FirstOrDefault();
                                        break;
                                    case "Guardian":
                                        Guardguardian = allguardians.Where(g => g.GuardianTypeId == GuardianRelation.RelationId).FirstOrDefault();
                                        break;
                                }

                                if (Guardguardian != null)
                                {
                                    var guardianname = !string.IsNullOrEmpty(Guardguardian.LastName) ? Guardguardian.FirstName + " " + Guardguardian.LastName : Guardguardian.FirstName;
                                    contactinfo = _IContactService.GetAllContactInfos(ref count, ContactId: Guardguardian.GuardianId,
                                                        ContactInfoTypeId: contactinfotypeid, status: true, Isdefault: true)
                                                        .Where(x => x.ContactType.ContactType1.ToLower() == "guardian").ToList();
                                    if (contactinfo.Count > 0)
                                        ContactNo = studentguardainreltion + guardianname + ": " + contactinfo.FirstOrDefault().ContactInfo1;
                                }
                            }

                            CollaspePanelHtml += "<td><input type='checkbox' id='" + CheckBoxChild + "' onclick='" + CheckBoxChildClass + "' class='checkboxesssle'/></th></td>";
                            CollaspePanelHtml += "<td hidden>" + _ICustomEncryption.base64e(row.Field<int>("BusRouteId").ToString()) + "</td>";
                            CollaspePanelHtml += "<td hidden>" + _ICustomEncryption.base64e(row.Field<int>("BoardingPointId").ToString()) + "</td>";
                            CollaspePanelHtml += "<td hidden>" + _ICustomEncryption.base64e(row.Field<int>("StudentId").ToString()) + "</td>";
                            CollaspePanelHtml += "<td>" + (!string.IsNullOrEmpty(row.Field<string>("AdmnNo")) ? row.Field<string>("AdmnNo").ToString() : "") + "</td>";
                            CollaspePanelHtml += "<td>" + (!string.IsNullOrEmpty(row.Field<string>("StudentName")) ? row.Field<string>("StudentName").ToString() : "") + "</td>";
                            CollaspePanelHtml += "<td>" + ContactNo + "</td>";
                            CollaspePanelHtml += "<td>" + (studentClassInfo != null ? studentClassInfo.ClassMaster.Class : "") + "</td>";//row.Field<string>("Class").ToString() + "</td>";
                            CollaspePanelHtml += "<td>" + (studentClassInfo != null ? studentClassInfo.RollNo : "") + "</td>"; //(!string.IsNullOrEmpty(row.Field<string>("RollNo")) ? row.Field<string>("RollNo") : "") + "</td>";
                            CollaspePanelHtml += "<td>" + (!string.IsNullOrEmpty(row.Field<string>("TptName")) ? row.Field<string>("TptName").ToString() : "") + "</td>";
                            CollaspePanelHtml += "<td hidden>" + HasMobileNo + "-" + HasAccount + "</td>";
                            CollaspePanelHtml += "</tr>";
                            #endregion

                            //CollaspePanelHtml += "<script>var counter = 0;";
                            //CollaspePanelHtml += "$('#" + CheckBoxChild + "').click(function(){";
                            //CollaspePanelHtml += "if ($(this).is(':checked'))";
                            //CollaspePanelHtml += "{counter++;}";
                            //CollaspePanelHtml += "});";
                            //CollaspePanelHtml += "alert(counter);<script/>";
                        }
                        CollaspePanelHtml += "</tbody></table></div>";
                        CollaspePanelHtml += "</div>";

                        CollaspePanelHtml += "<script>";
                        CollaspePanelHtml += "$(document).ready(function() {";
                        CollaspePanelHtml += "$('#" + Heading + "').find('#" + Total + "').html(" + InnerRowCount + ");";


                        CollaspePanelHtml += "$('#checkAllSelect').click(function() {";
                        CollaspePanelHtml += " var isChecked = $(this).prop('checked');";
                        CollaspePanelHtml += "$('.checkboxTrigger').prop('checked', isChecked);";
                        CollaspePanelHtml += "$('#" + tableId + " tr:has(th)').find('.row-checkbox1').prop('checked', isChecked);";
                        CollaspePanelHtml += "$('#" + tableId + " tr:has(td)').find('.checkboxesssle').prop('checked', isChecked);";
                        CollaspePanelHtml += " if(isChecked)";
                        CollaspePanelHtml += "{$('#" + Heading + "').find('#" + Selected + "').html(" + InnerRowCount + ");}";
                        CollaspePanelHtml += "else{$('#" + Heading + "').find('#" + Selected + "').html(0);}";
                        CollaspePanelHtml += "});";

                        CollaspePanelHtml += "$('#" + MainHeaderCheckboxId + "').click(function() {";
                        CollaspePanelHtml += " var isChecked = $(this).prop('checked');";
                        CollaspePanelHtml += "$('#" + tableId + " tr:has(th)').find('input[type=\"checkbox\"]').prop('checked', isChecked);";
                        CollaspePanelHtml += "$('#" + tableId + " tr:has(td)').find('input[type=\"checkbox\"]').prop('checked', isChecked);";
                        CollaspePanelHtml += "});";

                        CollaspePanelHtml += "$('#" + CheckBoxParent + "').click(function() {";
                        CollaspePanelHtml += " var isChecked = $(this).prop('checked');";
                        CollaspePanelHtml += "$('#" + tableId + " tr:has(td)').find('input[type=\"checkbox\"]').prop('checked', isChecked);";
                        CollaspePanelHtml += "});";

                        CollaspePanelHtml += "$('#" + tableId + " tr:has(td)').find('input[type=\"checkbox\"]').click(function() {";
                        CollaspePanelHtml += "var isChecked = $(this).prop('checked');";
                        CollaspePanelHtml += "var isHeaderChecked = $('#" + CheckBoxParent + "').prop('checked');";

                        CollaspePanelHtml += "if (isChecked == false && isHeaderChecked)";
                        CollaspePanelHtml += "{$('#" + CheckBoxParent + "').prop('checked', isChecked);}";
                        CollaspePanelHtml += "else {";
                        CollaspePanelHtml += " $('#" + tableId + " tr:has(td)').find('input[type=\"checkbox\"]').each(function() {";
                        CollaspePanelHtml += "if ($(this).prop('checked') == false)";
                        CollaspePanelHtml += "{isChecked = false;}";
                        CollaspePanelHtml += " });";
                        CollaspePanelHtml += "$('#" + CheckBoxParent + "').prop('checked', isChecked);";
                        CollaspePanelHtml += "}";

                        CollaspePanelHtml += "});";


                        // CollaspePanelHtml += "$('#" + Heading + "').find('#" + Selected + "').html(countSelctedStus);";

                        CollaspePanelHtml += "$('#" + Heading + "').on('click', function(e){";
                        CollaspePanelHtml += "var $_target =  $('.panel');";
                        CollaspePanelHtml += "var $_panelBody = $_target.find('#" + AriaControl + "');";
                        CollaspePanelHtml += "if($_panelBody){";
                        CollaspePanelHtml += "$_panelBody.collapse('toggle');";
                        CollaspePanelHtml += "}";
                        CollaspePanelHtml += " var countBoardingPointStus = 0;";
                        CollaspePanelHtml += "$('#" + tableId + " tbody tr').each(function () {";
                        CollaspePanelHtml += "if ($(this).find('input').is(':checked')) {";
                        CollaspePanelHtml += " countBoardingPointStus++;";
                        CollaspePanelHtml += "}";
                        CollaspePanelHtml += " });";
                        CollaspePanelHtml += " $(this).find('#" + Selected + "').html(countBoardingPointStus);";
                        CollaspePanelHtml += " });";


                        //CollaspePanelHtml += "$('#" + Total + "').on('click', function(e){";
                        //CollaspePanelHtml += "var $_target =  $('.panel');";
                        //CollaspePanelHtml += "var $_panelBody = $_target.find('#" + AriaControl + "');";
                        //CollaspePanelHtml += "if($_panelBody){";
                        //CollaspePanelHtml += "$_panelBody.collapse('toggle');";
                        //CollaspePanelHtml += "}";
                        //CollaspePanelHtml += " });";

                        CollaspePanelHtml += "});";
                        CollaspePanelHtml += "</script>";
                        CollaspePanelHtml += "</div>";

                    }
                    CollaspePanelHtml += "</div>";
                }


                return Json(new
                {
                    CollaspePanelHtml
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        public ActionResult DeleteBoardingPoint(string BPI, int BPFI)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                // check authorization
                //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Transport", "BoardingPoint", "Delete");
                //if (!isauth)
                //    return RedirectToAction("AccessDenied", "Error");
            }

            try
            {
                var BoardingPointId = Convert.ToInt32(BPI);
                var boardingPointFeeId = BPFI;
                var logtypeupdate = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
                // TimeTable log Table
                var BoardingPointtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "BusRoute").FirstOrDefault();
                var TptRouteDetailstable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "TptRouteDetails").FirstOrDefault();
                // get tiem table by id for validation
                var BoardingPoint = _IBusService.GetBoardingPointById(BoardingPointId, true);
                var BoardingPointFee = _IBusService.GetBoardingPointFeeById(boardingPointFeeId, true);
                if (BoardingPointFee != null)
                {
                    _IBusService.DeleteBoardingPointFee(BoardingPointFee.BoardingPointFeeId);

                }
                var boardingpontfee = _IBusService.GetAllBoardingPointFees(ref count).Where(m => m.BoardingPointId == BoardingPoint.BoardingPointId).ToList();
                if (boardingpontfee.Count == 0)
                    if (BoardingPoint != null)
                    {
                        _IBusService.DeleteBoardingPoint(BoardingPoint.BoardingPointId);
                        // table log saved or not
                        var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BoardingPointtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                        // save activity log
                        if (tabledetail != null)
                            _IActivityLogService.SaveActivityLog(BoardingPoint.BoardingPointId, BoardingPointtable.TableId, logtypeupdate.ActivityLogTypeId, user, String.Format("Delete BoardingPoint with BoardingPointId:{0} and BoardingPoint1:{1}", BoardingPoint.BoardingPointId, BoardingPoint.BoardingPoint1), Request.Browser.Browser);

                        return Json(new
                        {
                            status = "success",
                            data = "Deleted Successfully"
                        });

                    }

                return Json(new
                {
                    status = "failed",
                    data = "Deleted failed"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }

        public ActionResult SelectBoardingPointsList(DataSourceRequest command, BoardingPointModel BoardingPointModel, bool Inactive = false, IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }


            var BoardingPointFee = _IBusService.GetAllBoardingPointFees(ref count).ToList();
            if (BoardingPointModel.RouteId != null && BoardingPointModel.RouteId>0)
            {
                var BusRouteDetails = _IBusService.GetAllBusRouteDetails(ref count).Where(m => m.BusRouteId == BoardingPointModel.RouteId).ToList();
                BoardingPointFee = BoardingPointFee.Join(BusRouteDetails, a => a.BoardingPointId, b => b.BoardingPointId, (a, b) => new {a }).Select(x=>x.a).ToList();
            }


            if (BoardingPointModel.BoardingPoint != null)
                BoardingPointFee = BoardingPointFee.Where(m => m.BoardingPoint.BoardingPoint1.ToLower() == BoardingPointModel.BoardingPoint.ToLower()).ToList();
           
            

            var bplist = BoardingPointFee.Select(x =>
            {
                var BPModel = new BoardingPointModel();
                var boardingpoint = _IBusService.GetBoardingPointById((int)x.BoardingPointId);
                
                BPModel.ExistIn="";
                BPModel.IsExistInAnotherTable=false;
                var TptRoute=_ITransportService.GetAllTptFacilitys(ref count).Where(m=>m.BoardingPointId==(int)x.BoardingPointId).ToList();
                if (TptRoute.Count > 0) { 
                    BPModel.IsExistInAnotherTable = true;
                    BPModel.ExistIn="BoardingPoint Is Found In StudentTransPort";
                 }
                var BusRouteDetails = _IBusService.GetAllBusRouteDetails(ref count).Where(m => m.BoardingPointId == x.BoardingPointId).ToList();
                if (BusRouteDetails.Count > 0) { 
                    BPModel.IsExistInAnotherTable=true;

                    if(string.IsNullOrEmpty(BPModel.ExistIn))
                     BPModel.ExistIn = "BoardingPoint Is Found In BusRoute";
                    else
                        BPModel.ExistIn += " & BusRoute.";
                                  
                }
                BPModel.BoardingPoint = boardingpoint.BoardingPoint1 != null ? boardingpoint.BoardingPoint1 : "";
                BPModel.BoardingPointLatitude = boardingpoint.Latitude != null ? boardingpoint.Latitude : "";
                BPModel.BoardingPointLongitude = boardingpoint.Longitude != null ? boardingpoint.Longitude : "";

                BPModel.BoardingPointId = x.BoardingPointId > 0 ? Convert.ToString(x.BoardingPointId) : "";

                BPModel.PickAndDropFee = x.TransportFee_2 != null ? x.TransportFee_2 : 0;
                BPModel.SinglePickOrDropFee = x.TransportFee_1 != null ? x.TransportFee_1 : 0;
                if (x.FeeFrequencyId != null)
                {
                    BPModel.FeeFrequencyId = (int)x.FeeFrequencyId;
                    var ferquency = _IFeeService.GetFeeFrequencyById(BPModel.FeeFrequencyId);
                    if (ferquency != null)
                        BPModel.FeeFrequency = ferquency.FeeFrequency1;
                }
                else
                {
                    BPModel.FeeFrequencyId = 0;
                    BPModel.FeeFrequency = "";
                }
                var busroute = _IBusService.GetAllBusRouteDetails(ref count, BoardingPointId: x.BoardingPointId);
                if (busroute.Count > 0)
                    BPModel.IsDeletable = false;
                else
                    BPModel.IsDeletable = true;
                BPModel.EffectiveDate = ConvertDate((DateTime)x.EfectiveDate);
                 BPModel.dtEffectiveDate = x.EfectiveDate;
                
                BPModel.EndDate = x.EndDate != null ? ConvertDate((DateTime)x.EndDate) : "";
                BPModel.dtEndDate = x.EndDate;

                BPModel.BoardingPointFeeId = x.BoardingPointFeeId;

                return BPModel;
            }).AsQueryable().Sort(sort);

            

            if (!string.IsNullOrEmpty(BoardingPointModel.EffectiveDate))
            {
                var startDate = (BoardingPointModel.EffectiveDate).Split('/');
                var ndate = DateTime.Parse(startDate[1] + '/' + startDate[0] + '/' + startDate[2]);
                bplist = bplist.Where(p => p.dtEffectiveDate >= ndate);
            }
            if (!string.IsNullOrEmpty(BoardingPointModel.EndDate))
            {
                var endDate = (BoardingPointModel.EndDate).Split('/');
                var ndate = DateTime.Parse(endDate[1] + '/' + endDate[0] + '/' + endDate[2]);
                bplist = bplist.Where(p => p.dtEndDate <= ndate);
            }
            
            var gridModel = new DataSourceResult
            {
                Data = bplist.PagedForCommand(command).ToList(),
                Total = bplist.Count()
            };

            return Json(gridModel);
        }

        public ActionResult OpenBoardingPointList()
        {
            try
            {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            var BoardingPointFee = _IBusService.GetAllBoardingPointFees(ref count).Where(m=>m.EndDate==null).ToList();
            var BPModel = new BoardingPointModel();
            var BoardingPointList = _IBusService.GetAllBoardingPoints(ref count).Join(BoardingPointFee, f => f.BoardingPointId, u => u.BoardingPointId, (f, u) =>
                            new
                            {
                                f.BoardingPoint1,
                                f.BoardingPointId,
                                f.Latitude,
                                f.Longitude,
                                u.BoardingPointFeeId,
                                u.EfectiveDate,
                                strEffDate = u.EfectiveDate.Value.Date.ToString("dd/MM/yyyy"),
                                u.EndDate,
                                u.FeeFrequencyId,
                                u.TransportFee_1,
                                u.TransportFee_2
                            }).Where(m => m.EndDate == null).ToList();
            return Json(new
            {
                status = "success",
                BoardingPointList = BoardingPointList
            });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public DataTable GetAllKendoGridInformation(BoardingPointModel BoardingPointModel, IEnumerable<Sort> sort = null)
        {
            DataTable dt = new DataTable();
            try
            {
                var BoardingPointFee = _IBusService.GetAllBoardingPointFees(ref count).ToList();
                if (BoardingPointModel.RouteId != null && BoardingPointModel.RouteId > 0)
                {
                    var BusRouteDetails = _IBusService.GetAllBusRouteDetails(ref count).Where(m => m.BusRouteId == BoardingPointModel.RouteId).ToList();
                    BoardingPointFee = BoardingPointFee.Join(BusRouteDetails, a => a.BoardingPointId, b => b.BoardingPointId, (a, b) => new { a }).Select(x => x.a).ToList();
                }


                if (BoardingPointModel.BoardingPoint != null && BoardingPointModel.BoardingPoint!="")
                    BoardingPointFee = BoardingPointFee.Where(m => m.BoardingPoint.BoardingPoint1.ToLower() == BoardingPointModel.BoardingPoint.ToLower()).ToList();

                var bplist = BoardingPointFee.Select(x =>
                {
                    var BPModel = new BoardingPointModel();
                    var boardingpoint = _IBusService.GetBoardingPointById((int)x.BoardingPointId);

                    BPModel.BoardingPoint = boardingpoint.BoardingPoint1 != null ? boardingpoint.BoardingPoint1 : "";
                    BPModel.BoardingPointLatitude = boardingpoint.Latitude != null ? boardingpoint.Latitude : "";
                    BPModel.BoardingPointLongitude = boardingpoint.Longitude != null ? boardingpoint.Longitude : "";

                    BPModel.BoardingPointId = x.BoardingPointId > 0 ? Convert.ToString(x.BoardingPointId) : "";

                    BPModel.PickAndDropFee = x.TransportFee_2 != null ? x.TransportFee_2 : 0;
                    BPModel.SinglePickOrDropFee = x.TransportFee_1 != null ? x.TransportFee_1 : 0;
                    if (x.FeeFrequencyId != null)
                    {
                        BPModel.FeeFrequencyId = (int)x.FeeFrequencyId;
                        var ferquency = _IFeeService.GetFeeFrequencyById(BPModel.FeeFrequencyId);
                        if (ferquency != null)
                            BPModel.FeeFrequency = ferquency.FeeFrequency1;
                    }
                    else
                    {
                        BPModel.FeeFrequencyId = 0;
                        BPModel.FeeFrequency = "";
                    }
                    var busroute = _IBusService.GetAllBusRouteDetails(ref count, BoardingPointId: x.BoardingPointId);
                    if (busroute.Count > 0)
                        BPModel.IsDeletable = false;
                    else
                        BPModel.IsDeletable = true;
                    BPModel.EffectiveDate = ConvertDate((DateTime)x.EfectiveDate);
                    BPModel.dtEffectiveDate = x.EfectiveDate;

                    BPModel.EndDate = x.EndDate != null ? ConvertDate((DateTime)x.EndDate) : "";
                    BPModel.dtEndDate = x.EndDate;

                    BPModel.BoardingPointFeeId = x.BoardingPointFeeId;

                    return BPModel;
                }).AsQueryable().Sort(sort);



                if (!string.IsNullOrEmpty(BoardingPointModel.EffectiveDate))
                {
                    var startDate = (BoardingPointModel.EffectiveDate).Split('/');
                    var ndate = DateTime.Parse(startDate[1] + '/' + startDate[0] + '/' + startDate[2]);
                    bplist = bplist.Where(p => p.dtEffectiveDate >= ndate);
                }
                if (!string.IsNullOrEmpty(BoardingPointModel.EndDate))
                {
                    var endDate = (BoardingPointModel.EndDate).Split('/');
                    var ndate = DateTime.Parse(endDate[1] + '/' + endDate[0] + '/' + endDate[2]);
                    bplist = bplist.Where(p => p.dtEndDate <= ndate);
                }
                  // Add columns in your DataTable
                dt.Columns.Add("BoardingPoint");
                dt.Columns.Add("Latitude");
                dt.Columns.Add("Longitude");
                dt.Columns.Add("FeeFrequency");
                dt.Columns.Add("Pick & DropFee");
                dt.Columns.Add("PickOrDropFee");
                dt.Columns.Add("EffectiveDate");
                dt.Columns.Add("EndDate");
                DataRow row;
                if (bplist!= null)
                {
                    foreach (var data in bplist)
                    {
                        row = dt.NewRow();
                        row[0] = data.BoardingPoint == null ? "" : data.BoardingPoint.ToString();
                        row[1] = data.BoardingPointLatitude == null ? "" : data.BoardingPointLatitude.ToString();
                        row[2] = data.BoardingPointLongitude == null ? "" : data.BoardingPointLongitude.ToString();
                        row[3] = data.FeeFrequency == null ? "" : data.FeeFrequency.ToString();
                        row[4] = data.PickAndDropFee == null ? "" : data.PickAndDropFee.ToString();
                        row[5] = data.SinglePickOrDropFee == null ? "" : data.SinglePickOrDropFee.ToString();
                        row[6] = data.EffectiveDate == null ? "" : data.EffectiveDate.ToString();
                        row[7] = data.EndDate == null ? "" : data.EndDate.ToString();
                        dt.Rows.Add(row);
                        dt.AcceptChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
            }
            return dt;
        }


        public ActionResult ExportData(string IsExcel,string BoardingPoint = null, string EffectiveDate = null, string EndDate = null, int RouteId=0)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                string Subtitle = "";
                string Title = "Boarding Points";
                var BoardingPointModel = new BoardingPointModel();
                BoardingPointModel.BoardingPoint = BoardingPoint;
                BoardingPointModel.EffectiveDate = EffectiveDate;
                BoardingPointModel.EndDate = EndDate;
                BoardingPointModel.RouteId = RouteId;

                DataTable dt = new DataTable();
                dt = GetAllKendoGridInformation(BoardingPointModel);
                if (dt.Rows.Count > 0)
                {
                    if (IsExcel == "1")
                    {
                        _IExportHelper.ExportToExcel(dt, Title: Title, Subtitle: Subtitle,HeadingName:"List Of Boarding Points");
                    }
                    else if (IsExcel == "2")
                    {
                        _IExportHelper.ExportToPrint(dt, Title: Title, Subtitle: Subtitle);
                    }
                    else
                    {
                        _IExportHelper.ExportToPDF(dt, Title: Title, Subtitle: Subtitle);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }


        public ActionResult GetClassList(int SessionId = 0)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                IList<SelectListItem> AvailableClasses = new List<SelectListItem>();
                AvailableClasses.Add(new SelectListItem { Value = "", Text = "--Select class-", Selected = true });
                int ssnid = 0;
                if (SessionId > 0)
                    ssnid = SessionId;
                else
                    ssnid = _ICatalogMasterService.GetAllSessions(ref count, Isdeafult: true).FirstOrDefault().SessionId;

                var StudentLeftStatusId = _IStudentMasterService.GetAllStudentStatus(ref count).Where(f => f.StudentStatus.ToLower() == "left").FirstOrDefault().StudentStatusId;
                var AllSessionStudentClasses = _IStudentService.GetAllStudentClasss(ref count, SessionId: SessionId).Where(f => f.StudentId != null && f.ClassId != null && (f.Student.StudentStatusId == null || f.Student.StudentStatusId != StudentLeftStatusId)).GroupBy(f => f.ClassId).ToList();
                foreach (var item in AllSessionStudentClasses)
                {
                    AvailableClasses.Add(new SelectListItem { Text = item.FirstOrDefault().ClassMaster.Class, Value = item.FirstOrDefault().ClassMaster.ClassId.ToString() });
                }

                //if(int sess)
                //IList<SelectListItem> AvailableClasses = new List<SelectListItem>();
                //AvailableClasses = _IDropDownService.ClassListSessionWise(SessionId);

                IList<SelectListItem> AvailableSessions = new List<SelectListItem>();
                AvailableSessions = _IDropDownService.SessionList();
                var currentSession = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.IsDefualt == true).FirstOrDefault().SessionId;

                return Json(new
                {
                    status = "success",
                    ClassList = AvailableClasses,
                    SessionList=AvailableSessions,
                    CurrentSession=currentSession
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllStudentClassesBySession(int SessionId = 0)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                IList<SelectListItem> AvailableClasses = new List<SelectListItem>();
                AvailableClasses.Add(new SelectListItem { Value = "", Text = "--Select class-", Selected = true });
                int ssnid = 0;
                if (SessionId > 0)
                    ssnid = SessionId;
                else
                    ssnid = _ICatalogMasterService.GetAllSessions(ref count, Isdeafult: true).FirstOrDefault().SessionId;

                var StudentLeftStatusId = _IStudentMasterService.GetAllStudentStatus(ref count).Where(f => f.StudentStatus.ToLower() == "left").FirstOrDefault().StudentStatusId;
                var AllSessionStudentClasses = _IStudentService.GetAllStudentClasss(ref count, SessionId: SessionId).Where(f=>f.StudentId!=null && f.ClassId!=null && (f.Student.StudentStatusId==null || f.Student.StudentStatusId!= StudentLeftStatusId)).GroupBy(f=>f.ClassId).ToList();
                foreach (var item in AllSessionStudentClasses)
                {
                    AvailableClasses.Add(new SelectListItem { Text = item.FirstOrDefault().ClassMaster.Class, Value = item.FirstOrDefault().ClassMaster.ClassId.ToString() });
                }
                
                //AvailableClasses = _IDropDownService.ClassList();

                //IList<SelectListItem> AvailableSessions = new List<SelectListItem>();
                //AvailableSessions = _IDropDownService.SessionList();
                //var currentSession = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.IsDefualt == true).FirstOrDefault().SessionId;

                return Json(new
                {
                    status = "success",
                    ClassList = AvailableClasses,
                 
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetStudentList(int ClassId, int SessionId = 0)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                IList<SelectListItem> AvailblStudent = new List<SelectListItem>();
                IList<SelectListItem> AvailableStudent = new List<SelectListItem>();
                if (ClassId > 0)
                {
                    var Session = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.IsDefualt == true).FirstOrDefault();
                    if (SessionId > 0)
                    {
                        Session = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.SessionId == SessionId).FirstOrDefault();
                    }
                    AvailblStudent = _IStudentService.StudentListbyClassId(Classid: ClassId,SessionId:Session.SessionId,AdmnStatus:true).ToList();

                   
                   //AvailableStudent = AvailblStudent.Select(s=>s.Value.Replace(s.Value,_ICustomEncryption.base64e(s.Value)));
                   foreach (var student in AvailblStudent)
                       AvailableStudent.Add(new SelectListItem { Selected = true, Text = student.Text , Value = _ICustomEncryption.base64e(student.Value) });


                return Json(new
                {
                    status = "success",
                    StudentList = AvailableStudent
                }, JsonRequestBehavior.AllowGet);
              }
                return Json(new
                {
                    status = "Error",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetRegNoByStudentId(string StudentId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            try
            {
                var intStudentId=0;
                if (!string.IsNullOrEmpty(StudentId))
                    int.TryParse(_ICustomEncryption.base64d(StudentId), out intStudentId);

                if (intStudentId > 0)
                {
                    var student = _IStudentService.GetStudentById(intStudentId);
                    if (student != null)
                    {
                        return Json(new
                        {
                            status = "success",
                            AdmnNo = student.AdmnNo
                        }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new
                {
                    status = "Error",
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetStudentClassByRegNo(string AdmnNo,int SessionId = 0)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                var session = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.IsDefualt == true).FirstOrDefault();
                if (SessionId > 0)
                {
                    session = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.SessionId == SessionId).FirstOrDefault();
                }
                var student = _IStudentService.GetAllStudents(ref count).Where(s => (s.AdmnNo != null && s.AdmnNo == AdmnNo &&session.SessionId==session.SessionId)).FirstOrDefault();
                if (student != null)
                {
                   var  EncStudentId = _ICustomEncryption.base64e(student.StudentId.ToString());
                    var classes = _IStudentService.GetAllStudentClasss(ref count, SessionId: (int)session.SessionId, StudentId: student.StudentId).FirstOrDefault();
                    if (classes != null)
                    {
                        return Json(new
                        {
                            status = "success",
                            Class = classes.ClassId,
                            Student = EncStudentId,
                        }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "Student Not Available In This Session",
                        }, JsonRequestBehavior.AllowGet);

                    }
                }
                return Json(new
                {
                    status = "InValid RegNo/AdmissonNo",
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult GetStudentTransportHistory(string StudentId)
        {
            if (!string.IsNullOrEmpty(StudentId))
            {
                int Studentid = 0;
                if (!string.IsNullOrEmpty(StudentId))
                    int.TryParse(_ICustomEncryption.base64d(StudentId), out Studentid);
                
                var Student = _IStudentService.GetStudentById(Studentid);

                if (Student != null)
                {
                    var model = new StudentModel();
                    #region Transport facility

                    // get deleted setting
                    int StudentTptDeletedDays = 0;
                    var Tptdeletedsetting = _ISchoolSettingService.GetAttributeValue("StdTptDelDays");
                    if (Tptdeletedsetting != null && !(string.IsNullOrEmpty(Tptdeletedsetting)))
                        StudentTptDeletedDays = Convert.ToInt32(Tptdeletedsetting);

                    model.StudentId = Studentid;

                    var tptfacilities = _ITransportService.GetAllTptFacilitys(ref count, StudentId: model.StudentId);
                    tptfacilities = tptfacilities.OrderByDescending(s => s.EffectiveDate).ToList();
                    // active 
                    model.IsAddNewTptFacility = true;
                    var allowershiptypes = _ITransportService.GetAllTptOwnerShips(ref count).ToList();
                    if (allowershiptypes.Count > 0)
                    {
                        model.TransportFacility.OwnershipType.Add(new SelectListItem { Value = "", Text = "--Select--", Selected = false });
                        foreach (var item in allowershiptypes)
                        {
                            model.TransportFacility.OwnershipType.Add(new SelectListItem { Value = item.TptOwnerShipId.ToString(), Text = item.TptOwnerShip1, Selected = false });
                        }
                    }
                    //if (tptfacilities.Count > 0)
                    //    model.IsTransport = true;

                    foreach (var tptfacility in tptfacilities)
                    {
                        var TransportFacilityHistory = new TransportFacilityHistory();
                        TransportFacilityHistory.TptFacilityId = tptfacility.TptFacilityId;

                        TransportFacilityHistory.BoardingPointId = tptfacility.BoardingPointId;
                        TransportFacilityHistory.BoardingPoint = tptfacility.BoardingPoint != null ? tptfacility.BoardingPoint.BoardingPoint1 : "";

                        if (tptfacility.DropPointId != null)
                        {
                            TransportFacilityHistory.DropBoardingPointId = tptfacility.DropPointId;
                            TransportFacilityHistory.DropBoardingPoint = _IBusService.GetBoardingPointById((int)tptfacility.DropPointId) != null ? _IBusService.GetBoardingPointById((int)tptfacility.DropPointId).BoardingPoint1 : "";
                        }

                        TransportFacilityHistory.BusRouteId = tptfacility.BusRouteId;
                        TransportFacilityHistory.BusRoute = tptfacility.BusRoute != null ? tptfacility.BusRoute.BusRoute1 : "";
                        TransportFacilityHistory.OmwnershipId = tptfacility.TptOwnerShipId != null ? tptfacility.TptOwnerShipId.ToString() : "";


                        if (tptfacility.DropRouteId != null)
                        {
                            TransportFacilityHistory.DropRouteId = tptfacility.DropRouteId;
                            TransportFacilityHistory.DropBusRoute = _IBusService.GetBusRouteById((int)tptfacility.DropRouteId) != null ? _IBusService.GetBusRouteById((int)tptfacility.DropRouteId).BusRoute1 : "";
                            TransportFacilityHistory.DropRouteNo = _IBusService.GetBusRouteById((int)tptfacility.DropRouteId) != null ? _IBusService.GetBusRouteById((int)tptfacility.DropRouteId).RouteNo : "";
                        }
                        TransportFacilityHistory.PickRouteNo = tptfacility.BusRoute != null ? tptfacility.BusRoute.RouteNo : "";


                        TransportFacilityHistory.ParkingRequired = tptfacility.ParkingRequired != null ? (bool)tptfacility.ParkingRequired : false;
                        TransportFacilityHistory.Parking = tptfacility.ParkingRequired != null && (bool)tptfacility.ParkingRequired == true ? "Yes" : "No";

                        TransportFacilityHistory.TptTypeId = tptfacility.TptTypeDetail.TptTypeId;
                        TransportFacilityHistory.TptTypeDetId = tptfacility.TptTypeDetailId;
                        TransportFacilityHistory.TptTypeDet = tptfacility.TptTypeDetail.TptName;

                        TransportFacilityHistory.VehicleNo = tptfacility.VehicleNo;
                        TransportFacilityHistory.VehicleTypeId = tptfacility.VehicleTypeId;
                        TransportFacilityHistory.VehicleType = tptfacility.VehicleType != null ? tptfacility.VehicleType.VehicleType1 : "";

                        TransportFacilityHistory.EffectiveDate = tptfacility.EffectiveDate.HasValue ? ConvertDate(tptfacility.EffectiveDate.Value) : "";
                        TransportFacilityHistory.EndDate = tptfacility.EndDate.HasValue ? ConvertDate(tptfacility.EndDate.Value) : "";
                        // check if editable or delete table
                        TransportFacilityHistory.IsDeleteable = false;
                        TransportFacilityHistory.IsEditable = true;
                        if (tptfacility.EndDate.HasValue)
                            TransportFacilityHistory.IsEditable = false;
                        else
                            model.IsAddNewTptFacility = false;

                        //if (StudentTptDeletedDays > 0)
                        //{
                        // add days into effective date
                        //if (tptfacility.EffectiveDate.HasValue)
                        //{
                        //    var newdate = tptfacility.EffectiveDate.Value.Date;//.AddDays(StudentTptDeletedDays);
                        //    if (DateTime.UtcNow.Date > newdate)
                        //        TransportFacilityHistory.IsDeleteable = false;
                        //    if (DateTime.UtcNow.Date < newdate)
                        //        TransportFacilityHistory.IsDeleteable = true;
                        //}
                        if (tptfacility.EffectiveDate != null && tptfacility.EndDate != null)
                        {
                            var feereceipt = _IFeeService.GetAllFeeReceipts(ref count).Where(m => (m.IsCancelled == false || m.IsCancelled == null) && m.StudentId == Student.StudentId && m.ReceiptDate.Value.Month >= tptfacility.EffectiveDate.Value.Month && m.ReceiptDate.Value.Month <= tptfacility.EndDate.Value.Month);
                            if (feereceipt.Count() > 0)
                            {
                                TransportFacilityHistory.IsDeleteable = false;
                            }
                            else
                            {
                                TransportFacilityHistory.IsDeleteable = true;
                            }

                            model.TransportFacilityHistorylist.Add(TransportFacilityHistory);
                        }
                        else if (tptfacility.EffectiveDate != null && tptfacility.EndDate == null)
                        {
                            var feereceipt = _IFeeService.GetAllFeeReceipts(ref count).Where(m => (m.IsCancelled == false || m.IsCancelled == null) && m.StudentId == Student.StudentId && m.ReceiptDate.Value.Month >= tptfacility.EffectiveDate.Value.Month);
                            if (feereceipt.Count() > 0)
                            {
                                TransportFacilityHistory.IsDeleteable = false;
                            }
                            else
                            {
                                TransportFacilityHistory.IsDeleteable = true;
                            }

                            model.TransportFacilityHistorylist.Add(TransportFacilityHistory);
                        }






                        //var TransportFacilityHistory = new TransportFacilityHistory();
                        //TransportFacilityHistory.TptFacilityId = tptfacility.TptFacilityId;

                        //TransportFacilityHistory.BoardingPointId = tptfacility.BoardingPointId;
                        //TransportFacilityHistory.BoardingPoint = tptfacility.BoardingPoint != null ? tptfacility.BoardingPoint.BoardingPoint1 : "";

                        //TransportFacilityHistory.BusRouteId = tptfacility.BusRouteId;
                        //TransportFacilityHistory.BusRoute = tptfacility.BusRoute != null ? tptfacility.BusRoute.BusRoute1 : "";

                        //TransportFacilityHistory.ParkingRequired = tptfacility.ParkingRequired != null ? (bool)tptfacility.ParkingRequired : false;
                        //TransportFacilityHistory.Parking = tptfacility.ParkingRequired != null && (bool)tptfacility.ParkingRequired == true ? "Yes" : "No";

                        //TransportFacilityHistory.TptTypeId = tptfacility.TptTypeDetail.TptTypeId;
                        //TransportFacilityHistory.TptTypeDetId = tptfacility.TptTypeDetailId;
                        //TransportFacilityHistory.TptTypeDet = tptfacility.TptTypeDetail.TptName;

                        //TransportFacilityHistory.VehicleNo = tptfacility.VehicleNo;
                        //TransportFacilityHistory.VehicleTypeId = tptfacility.VehicleTypeId;
                        //TransportFacilityHistory.VehicleType = tptfacility.VehicleType != null ? tptfacility.VehicleType.VehicleType1 : "";

                        //TransportFacilityHistory.EffectiveDate = tptfacility.EffectiveDate.HasValue ? ConvertDate(tptfacility.EffectiveDate.Value) : "";
                        //TransportFacilityHistory.EndDate = tptfacility.EndDate.HasValue ? ConvertDate(tptfacility.EndDate.Value) : "";
                        //// check if editable or delete table
                        //TransportFacilityHistory.IsDeleteable = false;
                        //TransportFacilityHistory.IsEditable = true;
                        //if (tptfacility.EndDate.HasValue)
                        //    TransportFacilityHistory.IsEditable = false;
                        //else
                        //    model.IsAddNewTptFacility = false;

                        //if (StudentTptDeletedDays > 0)
                        //{
                        //    // add days into effective date
                        //    if (tptfacility.EffectiveDate.HasValue)
                        //    {
                        //        var newdate = tptfacility.EffectiveDate.Value.AddDays(StudentTptDeletedDays);
                        //        if (DateTime.UtcNow.Date > newdate)
                        //            TransportFacilityHistory.IsDeleteable = false;
                        //        if (DateTime.UtcNow.Date < newdate)
                        //            TransportFacilityHistory.IsDeleteable = true;
                        //    }
                        //}

                        //model.TransportFacilityHistorylist.Add(TransportFacilityHistory);
                    }

                    #endregion
                    return Json(new
                    {
                        status = "success",
                        TransportFacilityHistorylist = model.TransportFacilityHistorylist,
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "failed",
                    message="Student Does Not Exist"
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                status = "failed",
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CloseBoardingPointFee(int BoardingPointFeeId, DateTime Date)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                var Validationmsg = "";
                if (BoardingPointFeeId > 0)
                {
                    var BoardingPointFeerecord = _IBusService.GetBoardingPointFeeById(BoardingPointFeeId);
                    if (BoardingPointFeerecord != null)
                    {
                        var lastpaymentdate = _IFeeService.GetAllFeeReceipts(ref count).OrderByDescending(m => m.PaymentDate).FirstOrDefault() != null ?
                             _IFeeService.GetAllFeeReceipts(ref count).OrderByDescending(m => m.PaymentDate).FirstOrDefault().PaymentDate : null;
                        //check errors
                        if (Date < lastpaymentdate)
                            Validationmsg += "Close date should be equal or greater than last payment dateε";
                        if (BoardingPointFeerecord.EfectiveDate.HasValue)
                        {
                            if (Date <= BoardingPointFeerecord.EfectiveDate.Value.Date)
                                Validationmsg += "Close date should be greater than Effective dateε";
                        }
                        if (!string.IsNullOrEmpty(Validationmsg))
                        {
                            return Json(new
                            {
                                status = "failed",
                                data = Validationmsg
                            });
                        }

                        // delete entity
                        var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                        var BoardingPointFeetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "BoardingPointFee").FirstOrDefault();

                        BoardingPointFeerecord.EndDate = Date;
                        _IBusService.UpdateBoardingPointFee(BoardingPointFeerecord);
                        if (BoardingPointFeetable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BoardingPointFeetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(BoardingPointFeerecord.BoardingPointFeeId, BoardingPointFeetable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update TptFacility with BoardingPointFeeId:{0} and BoardingPointId:{1}", BoardingPointFeerecord.BoardingPointFeeId, BoardingPointFeerecord.BoardingPointId), Request.Browser.Browser);
                        }
                        return Json(new
                        {
                            status = "success",
                        });
                    }
                }
                return Json(new
                {
                    status = "failed",
                    data = "Transport facility can't Closed"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                });
            }
        }

        public ActionResult ChangeRouteName(string Routename, int busrouteid)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                BusRoute BusRoute = new BusRoute();
                var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                // TimeTable log Table
                var BusRoutetable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "BusRoute").FirstOrDefault();
                // check for update
                if (busrouteid > 0)
                {
                    // get bus route detail 
                    BusRoute = _IBusService.GetBusRouteById((int)busrouteid, true);
                    if (BusRoute != null)
                    {

                        BusRoute.BusRoute1 = Routename;
                        _IBusService.UpdateBusRoute(BusRoute);
                        if (BusRoutetable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, BusRoutetable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(BusRoute.BusRouteId, BusRoutetable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update BusRoute with BusRouteId:{0} and BusRouteId:{1}", BusRoute.BusRouteId, BusRoute.BusRouteId), Request.Browser.Browser);
                        }

                    }
                }

                return Json(new
                {
                    status = "success",
                    data = "Route Name Updated Successfully"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }
        #endregion


        #region Change Driver Attendant
        public ActionResult GetDriverAttendantDateWise(string ChangeFor,string Date,int id=0)
        {
            if(ChangeFor!="" && Date != "")
            {
                var DataList = new List<SelectListItem>();
                var SelectedDate = FormatDate(Date);
                if (ChangeFor.ToLower() == "driver")
                {
                    var UniqueRouteDriverSchoolSetting = _ISchoolSettingService.GetorSetSchoolData("UniqueRouteDriver", "0").AttributeValue;
                    var DriverLimit = 0;
                    if (!string.IsNullOrEmpty(UniqueRouteDriverSchoolSetting))
                    {
                        DriverLimit = Convert.ToInt32(UniqueRouteDriverSchoolSetting);
                    }
                    var ExistDriverList = _IBusService.GetAllTptRouteDetails(ref count).Where(f =>f.DriverId!=null && f.EffectiveDate != null && f.EffectiveDate < SelectedDate && (f.EndDate == null || f.EndDate > SelectedDate));
                    var DriverList = _IBusService.GetAllTptDrivers(ref count).Where(f=> f.DateOfLeaving == null || f.DateOfLeaving > SelectedDate);
                    if (id > 0)
                        DriverList = DriverList.Where(f => f.TptDriverId != id);
                    DataList.Add(new SelectListItem { Text = "--Select Driver--", Selected = true, Value = "" });
                    if (DriverLimit > 0)
                    {
                        var GroupDriverList = ExistDriverList.GroupBy(f => f.DriverId);
                        foreach (var group in GroupDriverList)
                        {
                            if (group.Count() < DriverLimit)
                                DataList.Add(new SelectListItem { Text = group.FirstOrDefault().TptDriver.DriverName, Value = group.FirstOrDefault().TptDriver.TptDriverId.ToString() });
                        }
                    }
                    else
                    {
                        var ExistDriverIds = ExistDriverList.Select(f => f.DriverId).Distinct().ToArray();
                        if(ExistDriverIds!=null && ExistDriverIds.Count()>0)
                           DriverList = DriverList.Where(f => !ExistDriverIds.Contains(f.TptDriverId));
                       
                        foreach (var driver in DriverList)
                            DataList.Add(new SelectListItem { Text = driver.DriverName, Value = driver.TptDriverId.ToString() });
                    }
                     
                    return Json(new
                     {
                        status = "success",
                        DataList = DataList
                    },JsonRequestBehavior.AllowGet);
                }
                else if(ChangeFor.ToLower() == "attendant")
                {

                  //  var ExistAttendantList = _IBusService.GetAllTptRouteDetails(ref count).Where(f => f.EffectiveDate != null && (f.EffectiveDate < SelectedDate && (f.EndDate == null || f.EndDate > SelectedDate))).Select(f => f.TptAttendantId);
                    var AttendantList = _IBusService.GetAllTptAttendants(ref count).Where(f => f.DateOfLeaving == null || f.DateOfLeaving > SelectedDate);
                    if (id > 0)
                        AttendantList = AttendantList.Where(f => f.TptAttendantId!= id);
                    DataList.Add(new SelectListItem { Text = "--Select Attendant--", Selected = true, Value = "" });
                    foreach (var Attendant in AttendantList)
                        DataList.Add(new SelectListItem { Text = Attendant.AttendantName, Value = Attendant.TptAttendantId.ToString() });
                    return Json(new
                    {
                        status = "success",
                        DataList = DataList
                    },JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new
            {
                status = "failed",
                msg = "Invalid Details"
            });
        }

        public ActionResult UpdateDriverAttendantBusRoute(string ChangeFor, string Date, int TptRouteDetailId, int Id)
        {
            if (ChangeFor == "" || Date == "" || TptRouteDetailId == 0 || Id == 0)
            {
                return Json(new
                {
                    status = "failed",
                    msg = "Invalid Details"
                });
            }
            var SelectedDate = FormatDate(Date);
            var TransportRouteDetail = _IBusService.GetTptRouteDetailById(TptRouteDetailId);
            if (TransportRouteDetail != null) {
                if (ChangeFor.ToLower() == "driver")
                {
                    if (TransportRouteDetail.DriverId != null)
                    {
                        var IfExistTptRouteDriver = _IBusService.GetAllTptRouteDrivers(ref count, (int)TransportRouteDetail.DriverId).OrderByDescending(f => f.EffectiveDate).FirstOrDefault();
                        if (IfExistTptRouteDriver != null)
                        {
                            IfExistTptRouteDriver.EndDate = SelectedDate.AddDays(-1);
                            _IBusService.UpdateTptRouteDriver(IfExistTptRouteDriver);
                        }
                        else
                        {
                            var NewTptRouteDriver = new TptRouteDriver();
                            NewTptRouteDriver.TptDriverId = TransportRouteDetail.DriverId;
                            NewTptRouteDriver.TptRouteDetailId = TransportRouteDetail.TptRouteDetailId;
                            NewTptRouteDriver.EffectiveDate = TransportRouteDetail.EffectiveDate;
                            NewTptRouteDriver.EndDate = SelectedDate.AddDays(-1);
                            _IBusService.InsertTptRouteDriver(NewTptRouteDriver);
                        }
                    }
                    TransportRouteDetail.DriverId = Id;
                    TransportRouteDetail.TptDriver = null;
                    _IBusService.UpdateTptRouteDetail(TransportRouteDetail);

                    var tptRouteDriver = new TptRouteDriver();
                    tptRouteDriver.TptDriverId = Id;
                    tptRouteDriver.TptRouteDetailId = TransportRouteDetail.TptRouteDetailId;
                    tptRouteDriver.EffectiveDate = SelectedDate;
                    _IBusService.InsertTptRouteDriver(tptRouteDriver);
                    return Json(new
                    {
                        status = "success",
                        msg = "Driver Saved SuccessFully"
                    });
                }
                else if (ChangeFor.ToLower() == "attendant" && TransportRouteDetail.TptAttendantId != null)
                {
                    if (TransportRouteDetail.TptAttendantId != null)
                    {
                        var IfExistTptRouteAttendant = _IBusService.GetAllTptRouteAttendants(ref count, (int)TransportRouteDetail.TptAttendantId).OrderByDescending(f => f.EffectiveDate).FirstOrDefault();
                        if (IfExistTptRouteAttendant != null)
                        {
                            IfExistTptRouteAttendant.EndDate = SelectedDate.AddDays(-1);
                            _IBusService.UpdateTptRouteAttendant(IfExistTptRouteAttendant);
                        }
                        else
                        {
                            var NewTptRouteAttendant = new TptRouteAttendant();
                            NewTptRouteAttendant.TptAttendantId = TransportRouteDetail.TptAttendantId;
                            NewTptRouteAttendant.TptRouteDetailId = TransportRouteDetail.TptRouteDetailId;
                            NewTptRouteAttendant.EffectiveDate = TransportRouteDetail.EffectiveDate;
                            NewTptRouteAttendant.EndDate = SelectedDate.AddDays(-1);
                            _IBusService.InsertTptRouteAttendant(NewTptRouteAttendant);
                        }
                    }
                    TransportRouteDetail.TptAttendantId = Id;
                    TransportRouteDetail.TptAttendant= null;
                    _IBusService.UpdateTptRouteDetail(TransportRouteDetail);

                    var tptRouteAttendant = new TptRouteAttendant();
                    tptRouteAttendant.TptAttendantId= Id;
                    tptRouteAttendant.TptRouteDetailId = TransportRouteDetail.TptRouteDetailId;
                    tptRouteAttendant.EffectiveDate = SelectedDate;
                    _IBusService.InsertTptRouteAttendant(tptRouteAttendant);
                    return Json(new
                    {
                        status = "success",
                        msg = "Attendant Saved SuccessFully"
                    });
                }
            }
            return Json(new
            {
                status = "failed",
                msg = "Invalid Details"
            });
        }

        public ActionResult GetDriverAttendantHistory(string ChangeFor, int Id = 0)
        {
            if (!string.IsNullOrEmpty(ChangeFor) && Id > 0)
            {
                IList<DriverAttendantModel> datalist = new List<DriverAttendantModel>();
                if (ChangeFor.ToLower() == "driver")
                {
                    var histrylist = _IBusService.GetAllTptRouteDrivers(ref count).Where(f => f.TptDriverId == Id);
                    foreach (var histry in histrylist)
                    {
                        var BusRouteDetail = "";
                        if (histry.TptRouteDetailId != null && histry.TptRouteDetail.BusRouteId != null)
                            BusRouteDetail = histry.TptRouteDetail.BusRoute.BusRoute1;
                        var effctvdt = ""; var enddt = "";
                        if (histry.EffectiveDate != null)
                            effctvdt = histry.EffectiveDate.Value.Date.ToString("dd/MM/yyyy");
                        if (histry.EndDate != null)
                            enddt = histry.EndDate.Value.Date.ToString("dd/MM/yyyy");
                        datalist.Add(new DriverAttendantModel { Name = histry.TptDriver.DriverName, TptRouteDetail = BusRouteDetail, EffectiveDate = effctvdt, EndDate = enddt });
                    }
                    string date = "";
                    var DoJDriver = _IBusService.GetTptDriverById(Id).DateOfJoining;
                    var lastEndDate = histrylist.Where(f => f.EndDate != null).OrderByDescending(f => f.EndDate).FirstOrDefault();
                    if (lastEndDate != null)
                        DoJDriver = lastEndDate.EndDate.Value.AddDays(1);
                    else if (DoJDriver != null)
                        DoJDriver = DoJDriver.Value.AddDays(1);

                        if (DoJDriver != null)
                        date = DoJDriver.Value.Date.ToString("MM/dd/yyyy");
                    return Json(new
                    {
                        status = "success",
                        DataList = datalist,
                        MinDate = date
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var histrylist = _IBusService.GetAllTptRouteAttendants(ref count).Where(f => f.TptAttendantId == Id);
                    foreach (var histry in histrylist)
                    {
                        var BusRouteDetail = "";
                        if (histry.TptRouteDetailId != null && histry.TptRouteDetail.BusRouteId != null)
                            BusRouteDetail = histry.TptRouteDetail.BusRoute.BusRoute1;
                        var effctvdt = ""; var enddt = "";
                        if (histry.EffectiveDate != null)
                            effctvdt = histry.EffectiveDate.Value.Date.ToString("dd/MM/yyyy");
                        if (histry.EndDate != null)
                            enddt = histry.EndDate.Value.Date.ToString("dd/MM/yyyy");
                        datalist.Add(new DriverAttendantModel { Name = histry.TptAttendant.AttendantName, TptRouteDetail = BusRouteDetail, EffectiveDate = effctvdt, EndDate = enddt });
                    }
                    string date = "";
                    var DoJattendant = _IBusService.GetAllTptAttendants(ref count).Where(f=>f.TptAttendantId == Id).FirstOrDefault().DateOfJoining;
                    var lastEndDate = histrylist.Where(f => f.EndDate != null).OrderByDescending(f => f.EndDate).FirstOrDefault();

                    if (lastEndDate != null)
                        DoJattendant = lastEndDate.EndDate.Value.AddDays(1);
                    else if (DoJattendant != null)
                        DoJattendant = DoJattendant.Value.AddDays(1);

                    if (DoJattendant != null)
                        date = DoJattendant.Value.Date.ToString("MM/dd/yyyy");
                    return Json(new
                    {
                        status = "success",
                        DataList = datalist,
                        MinDate = date
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new
            {
                status = "failed",
                msg = "Invalid Details"
            });
        }
            #endregion

        }
}