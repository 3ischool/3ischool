﻿
using BAL.BAL.Common;
using BAL.BAL.Security;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.Kendo;
using DAL.DAL.SettingService;
using DAL.DAL.StockModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Stock;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KS_SmartChild.Controllers
{
    public class StockController : Controller
    {
        #region fields

        public int count = 0;
        private readonly IUserService _IUserService;
        private readonly ILogService _Logger;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IStockService _IStockService;
        private readonly IDropDownService _IDropDownService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly IExportHelper _IExportHelper;
        private readonly IWebHelper _WebHelper;
        #endregion

        #region ctor

        public StockController(IUserService IUserService,
             ILogService Logger, ICustomEncryption ICustomEncryption, IStockService IStockService,
            IDropDownService Dropdown, ISchoolSettingService ISchoolSettingService, IUserAuthorizationService IUserAuthorizationService
           , IExportHelper IExportHelper, IWebHelper WebHelper)
        {
            this._IUserService = IUserService;
            this._Logger = Logger;
            this._ICustomEncryption = ICustomEncryption;
            this._IStockService = IStockService;
            this._IDropDownService = Dropdown;
            this._ISchoolSettingService = ISchoolSettingService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._IExportHelper = IExportHelper;
            this._WebHelper = WebHelper;
        }
        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }

        public string ConvertDateForKendo(DateTime Date)
        {
            string sdisplayTime = Date.ToString("MM/dd/yyyy");
            return sdisplayTime;
        }

        #endregion

        #region"Manage Stock"
        public ActionResult Product(string Productid)
        {
            SchoolUser user = new SchoolUser();
            ProductInfo product = new ProductInfo();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);

                    // check authorization
                    //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Stock", "Product", "View");
                    //if (!isauth)
                    //    return RedirectToAction("AccessDenied", "Error");

                }
                product.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Stock", "Product", "Add Product");
                string packageid = "";
                if (Request.QueryString["Id"] != null)
                {
                    packageid = Request.QueryString["Id"].ToString();
                }

                int ProductIdk = 0, PackageIdK = 0;
                if (!string.IsNullOrEmpty(Productid))
                    int.TryParse(_ICustomEncryption.base64d(Productid), out ProductIdk);

                if (!string.IsNullOrEmpty(packageid))
                    int.TryParse(_ICustomEncryption.base64d(packageid), out  PackageIdK);

                product.productid = Productid;
                product.ProductGroup = _IDropDownService.ProductGroupList();
                product.ProductType = _IDropDownService.ProductTypeList();
                product.Unit = _IDropDownService.ProductUnitList();
                product.Class = _IDropDownService.GetAllClasses();
                product.Products = _IDropDownService.GetAllProducts();
                product.PackageId = packageid;
                if (ProductIdk > 0)
                {
                    var Products = _IStockService.GetProductById(ProductIdk, true);
                    product.ProductName = Products.ProductName;
                    product.ProductDescription = Products.ProductDescription;
                    product.ProductGroupId = Convert.ToString(Products.ProductGroupId);
                    product.ProductTypeId = Convert.ToString(Products.ProductTypeId);
                    product.ProductAbbreviation = Products.ProductAbbr;
                    product.OpeningBalance = Convert.ToString(Products.OpeningBalance);
                    product.Unitid = Convert.ToString(Products.UnitId);
                    product.MinStockqty = Convert.ToString(Products.MinStockQty);
                    product.MaxStockqty = Convert.ToString(Products.MaxStockQty);
                    product.Reordeqty = Convert.ToString(Products.ReOrderQty);
                    product.ProductCode = Products.ProductCode;
                    product.Barcode = Products.BarCode;
                    product.Isbatch = Convert.ToBoolean(Products.IsBatch);
                    product.Isexpiry = Convert.ToBoolean(Products.IsExpiry);
                    product.IsConsumable = Convert.ToBoolean(Products.IsConsumable);

                    product.UnitName = _IStockService.GetUnit(Convert.ToInt32(Products.UnitId));
                    product.ProductAttribute = _IDropDownService.ProductAttributeList(ProductIdk);
                    product.ProductAttributeValue = _IDropDownService.ProductAttributeValueList();

                    var PIImage = _IStockService.GetProductImageById(ProductIdk, true);
                    if (PIImage != null)
                    {
                        product.ProductImageId = _ICustomEncryption.base64e(Convert.ToString(PIImage.ProductImageId));
                        product.ProductImage = PIImage.Image;
                        string dir = "";
                        var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                        if (Request.Url.AbsoluteUri.Contains("localhost"))
                            dir = "/Images/" + SchoolDbId + "/ProductImages/";
                        else
                            dir = "../Images/" + SchoolDbId + "/ProductImages/";
                        //dir = _WebHelper.GetStoreLocation() + "/Images/" + SchoolDbId + "/ProductImages/";
                        product.ProductImagePath = (dir) + PIImage.Image;
                    }
                }
                else if (PackageIdK > 0)
                {
                    var Package = _IStockService.GetPackageById(PackageIdK, true);
                    product.PackageName = Package.PackageName;
                    product.ClassIds = Package.ClassIds;
                    product.PackageDetail = Package.PackageDetail;
                    product.UnitName = "";
                }
                else
                {
                    product.UnitName = "";
                }

                product.IsAuthToEdit = true;
                product.IsAuthToDelete = true;
                //paging
                product.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
                product.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return RedirectToAction("General", "Error");
            }
            return View(product);
        }

        [HttpPost]
        public ActionResult Product(ProductInfo model)
        {
            SchoolUser user = new SchoolUser();
            KSModel.Models.Product ProductM = new KSModel.Models.Product();
            string message = "";
            try
            {
                string errormsg = CheckStockError(model);
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                if (string.IsNullOrEmpty(errormsg))
                {
                    // decrypt values
                    int ProductIdK = 0;
                    if (!string.IsNullOrEmpty(model.productid))
                        int.TryParse(_ICustomEncryption.base64d(model.productid), out ProductIdK);


                    // decrypt values
                    int ProductTypeIdK = 0;
                    if (!string.IsNullOrEmpty(model.ProductTypeId))
                        int.TryParse((model.ProductTypeId), out ProductTypeIdK);

                    int ProductGroupIdK = 0;
                    if (!string.IsNullOrEmpty(model.ProductGroupId))
                        int.TryParse((model.ProductGroupId), out ProductGroupIdK);

                    int UnitIdK = 0;
                    if (!string.IsNullOrEmpty(model.Unitid))
                        int.TryParse((model.Unitid), out UnitIdK);

                    if (string.IsNullOrEmpty(model.MinStockqty))
                    {
                        model.MinStockqty = "0";
                    }
                    if (string.IsNullOrEmpty(model.MaxStockqty))
                    {
                        model.MaxStockqty = "0";
                    }
                    if (string.IsNullOrEmpty(model.Reordeqty))
                    {
                        model.Reordeqty = "0";
                    }
                    if (string.IsNullOrEmpty(model.OpeningBalance))
                    {
                        model.OpeningBalance = "0";
                    }
                    if (ProductIdK > 0)
                    {
                        ProductM = _IStockService.GetProductById(ProductIdK, true);
                        if (ProductM != null)
                        {
                            //// update Product
                            ProductM.ProductId = ProductIdK;
                            ProductM.ProductName = model.ProductName == null ? null : model.ProductName.Trim();
                            ProductM.ProductDescription = model.ProductDescription == null ? null : model.ProductDescription.Trim();
                            ProductM.ProductTypeId = ProductTypeIdK;
                            ProductM.ProductGroupId = ProductGroupIdK;
                            ProductM.StandardDiscount = 0;
                            ProductM.UnitId = UnitIdK;
                            ProductM.MinStockQty = Convert.ToDecimal(model.MinStockqty);
                            ProductM.MaxStockQty = Convert.ToDecimal(model.MaxStockqty);
                            ProductM.ReOrderQty = Convert.ToDecimal(model.Reordeqty);
                            ProductM.CreationDate = DateTime.Now;
                            ProductM.OpeningBalance = Convert.ToDecimal(model.OpeningBalance);
                            ProductM.ProductAbbr = model.ProductAbbreviation == null ? null : model.ProductAbbreviation.Trim();
                            ProductM.IsBatch = Convert.ToByte(model.Isbatch);
                            ProductM.IsExpiry = Convert.ToByte(model.Isexpiry);
                            ProductM.ProductCode = model.ProductCode;
                            ProductM.BarCode = model.Barcode;
                            ProductM.IsConsumable = model.IsConsumable;
                            message = "success";
                            _IStockService.UpdateProduct(ProductM);
                        }
                    }
                    else
                    {
                        // insert Product
                        ProductM.ProductName = model.ProductName == null ? null : model.ProductName.Trim();
                        ProductM.ProductDescription = model.ProductDescription == null ? null : model.ProductDescription.Trim();
                        ProductM.ProductTypeId = ProductTypeIdK;
                        ProductM.ProductGroupId = ProductGroupIdK;
                        ProductM.StandardDiscount = 0;
                        ProductM.UnitId = UnitIdK;
                        ProductM.MinStockQty = Convert.ToDecimal(model.MinStockqty);
                        ProductM.MaxStockQty = Convert.ToDecimal(model.MaxStockqty);
                        ProductM.ReOrderQty = Convert.ToDecimal(model.Reordeqty);
                        ProductM.CreationDate = DateTime.Now;
                        ProductM.OpeningBalance = Convert.ToDecimal(model.OpeningBalance);
                        ProductM.ProductAbbr = model.ProductAbbreviation == null ? null : model.ProductAbbreviation.Trim();
                        ProductM.IsBatch = Convert.ToByte(model.Isbatch);
                        ProductM.IsExpiry = Convert.ToByte(model.Isexpiry);
                        ProductM.ProductCode = model.ProductCode;
                        ProductM.BarCode = model.Barcode;
                        ProductM.IsConsumable = model.IsConsumable;
                        message = "success";
                        _IStockService.InsertProduct(ProductM);
                    }


                    TempData["successMsg"] = message;
                    return RedirectToAction("Product", new { ProductId = _ICustomEncryption.base64e(ProductM.ProductId.ToString()) });
                }
                else
                {
                    TempData["ErrorMsg"] = errormsg;
                    return RedirectToAction("Product");
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);
                return RedirectToAction("General", "Error");
            }

        }

        public string CheckStockError(ProductInfo model)
        {
            StringBuilder errormsg = new StringBuilder();

            if (model.ProductTypeId == null)
                errormsg.Append("Product Type ε");

            if (model.ProductGroupId == null)
                errormsg.Append("Product Group ε");

            if (string.IsNullOrEmpty(model.ProductName) || string.IsNullOrWhiteSpace(model.ProductName))
                errormsg.Append("Product Name required ε");
            else // check unique
            {
                // decrypt values
                int ProductIdK = 0;
                if (!string.IsNullOrEmpty(model.productid))
                    int.TryParse(_ICustomEncryption.base64d(model.productid), out ProductIdK);

                // decrypt values


                var IsProductExist = _IStockService.GetAllProducts(ref count, ProductName: model.ProductName, ProductAbbr: model.ProductAbbreviation,
                    ProductDescription: model.ProductDescription, ProductTypeId: Convert.ToInt32(model.ProductTypeId), ProductGroupId: Convert.ToInt32(model.ProductGroupId));
                IsProductExist = IsProductExist.Where(m => m.ProductId != ProductIdK && m.ProductGroupId == Convert.ToInt32(model.ProductGroupId) && m.ProductTypeId == Convert.ToInt32(model.ProductTypeId)).ToList();
                if (IsProductExist.Count > 0)
                    errormsg.Append("This Product Name already exist ε");
            }
            return errormsg.ToString();
        }

        public ActionResult GetAllStocks(DataSourceRequest command, ProductInfo model, IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int ProductType_Id = 0;
                int ProductGroup_Id = 0;
                if (!string.IsNullOrEmpty(model.ProductTypeId))
                {
                    int.TryParse((model.ProductTypeId), out ProductType_Id);
                }
                if (!string.IsNullOrEmpty(model.ProductGroupId))
                {
                    int.TryParse((model.ProductGroupId), out ProductGroup_Id);
                }
                if (string.IsNullOrEmpty(model.ProductName))
                {
                    model.ProductName = "";
                }
                var ProductList = _IStockService.GetAllStocksBysP(ref count, ProductName: model.ProductName, ProductGroupId: ProductGroup_Id
                                             , ProductTypeId: ProductType_Id);
                var IsAuthToView = _IUserAuthorizationService.IsUserAuthorize(user, "Stock", "Product", "View Record");
                var IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Stock", "Product", "Edit Record");
                var IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Stock", "Product", "Delete Record");

                var Data = ProductList.PagedForCommand(command).Select(p =>
                 {
                     var stocklist = new ProductInfo();
                     stocklist.productid = (_ICustomEncryption.base64e(p.productid.ToString()));
                     stocklist.ProductName = p.ProductName;
                     stocklist.ProductAbbreviation = p.ProductAbbreviation;
                     stocklist.OpeningBalance = p.OpeningBalance;
                     stocklist.ProductGroupName = p.ProductGroupName;
                     stocklist.ProductTypeName = p.ProductTypeName;
                     stocklist.UnitName = p.UnitName;
                     stocklist.IsAuthToEdit = IsAuthToEdit;
                     stocklist.IsAuthToDelete = _IStockService.GetDeleteStatus(IsAuthToDelete, p.productid);
                     stocklist.IsAuthToAdd = IsAuthToView;
                     return stocklist;
                 }).AsQueryable().Sort(sort);
                var gridModel = new DataSourceResult
            {
                Data = Data.ToList(),
                Total = ProductList.Count()
            };

                return Json(gridModel);

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult DeleteProduct(string ProductId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                // decrypt values
                int ProductIdIK = 0;
                if (!string.IsNullOrEmpty(ProductId))
                    int.TryParse(_ICustomEncryption.base64d(ProductId), out ProductIdIK);

                if (ProductIdIK > 0)
                {
                    _IStockService.DeleteProduct(ProductIdIK);
                    message = "Deleted successfully";
                }
                return Json(new
                {
                    status = "success",
                    data = message
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ViewProductInformation(string ProductId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int ProductIdK = 0;
                if (!string.IsNullOrEmpty(ProductId))
                    int.TryParse(_ICustomEncryption.base64d(ProductId), out ProductIdK);
                List<ProductInfo> productinfo = new List<ProductInfo>();
                if (ProductIdK > 0)
                {
                    var getMasterProduct = _IStockService.GetProductById(ProductId: ProductIdK);
                    var Detailmaster = _IStockService.GetProductInfoById(ProductId: ProductIdK);
                    productinfo.Add(new ProductInfo
                    {
                        ProductName = getMasterProduct.ProductName == null ? string.Empty : getMasterProduct.ProductName.Trim(),
                        ProductDescription = getMasterProduct.ProductDescription == null ? string.Empty : getMasterProduct.ProductDescription.Trim(),
                        ProductGroupName = _IStockService.GetGroupName(Convert.ToInt32(getMasterProduct.ProductGroupId)),
                        ProductTypeName = _IStockService.GetProductType(Convert.ToInt32(getMasterProduct.ProductTypeId)),
                        UnitName = _IStockService.GetUnit(Convert.ToInt32(getMasterProduct.UnitId)),
                        OpeningBalance = getMasterProduct.OpeningBalance == null ? string.Empty : Convert.ToString(getMasterProduct.OpeningBalance).Trim(),
                        MinStockqty = getMasterProduct.MinStockQty == null ? string.Empty : Convert.ToString(getMasterProduct.MinStockQty).Trim(),
                        MaxStockqty = getMasterProduct.MaxStockQty == null ? string.Empty : Convert.ToString(getMasterProduct.MaxStockQty).Trim(),
                        IsConsumable = Convert.ToBoolean(getMasterProduct.IsConsumable),
                        RateHistoryDetail = Detailmaster.RateHistoryDetail,
                        SpecificationDetail = Detailmaster.SpecificationDetail,
                        UnitConversionDetail = Detailmaster.UnitConversionDetail
                    });

                }

                return Json(new { productinfo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region "Product Rate History"
        public ActionResult GetRateHistory(DataSourceRequest command, string ProductId, IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int ProductIdIK = 0;
                if (!string.IsNullOrEmpty(ProductId))
                    int.TryParse(_ICustomEncryption.base64d(ProductId), out ProductIdIK);
                // var IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Product Rate History", "Delete Record");
                var IsAuthToDelete = true;

                var RHList = _IStockService.GetAllRateHistoryBysP(ref count, ProductId: ProductIdIK);
                var gridModel = new DataSourceResult
                {
                    Data = RHList.Select(p =>
                    {
                        var stocklist = new ProductInfo();
                        stocklist.productid = (_ICustomEncryption.base64e(p.productid.ToString()));
                        stocklist.ProductRateHistoryId = (_ICustomEncryption.base64e(p.ProductRateHistoryId.ToString())); ;
                        stocklist.PurRate = p.PurRate;
                        stocklist.MRP = p.MRP;
                        stocklist.EffectiveFrom = p.EffectiveFrom;
                        stocklist.EffectiveTill = p.EffectiveTill;
                        stocklist.IsAuthToDelete = _IStockService.GetDeleteStatus(IsAuthToDelete, p.productid);
                        return stocklist;
                    }).AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = RHList.Count()
                };

                return Json(gridModel);

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public string CheckRHError(ProductInfo model)
        {
            StringBuilder errormsg = new StringBuilder();

            if (!string.IsNullOrEmpty(model.PurRate.ToString()) || !string.IsNullOrWhiteSpace(model.PurRate.ToString()))
            {
                Decimal price = Convert.ToDecimal(model.PurRate);
                if (price == 0)
                {
                    errormsg.Append("Purchase Rate should be more than zero ε");
                }
            }
            if (!string.IsNullOrEmpty(model.MRP.ToString()) || !string.IsNullOrWhiteSpace(model.MRP.ToString()))
            {
                Decimal price = Convert.ToDecimal(model.MRP);
                if (price == 0)
                {
                    errormsg.Append("MRP should be more than zero ε");
                }
            }
            return errormsg.ToString();
        }

        public ActionResult InsertRateHistory(ProductInfo model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                string errormsg = CheckRHError(model);
                if (string.IsNullOrEmpty(errormsg))
                {
                    // decrypt values
                    int ProductIDK = 0, RHIDK = 0;

                    if (!string.IsNullOrEmpty(model.productid))
                        int.TryParse(_ICustomEncryption.base64d(model.productid), out ProductIDK);

                    KSModel.Models.ProductRateHistory PRH = new KSModel.Models.ProductRateHistory();
                    if (ProductIDK > 0)
                    {
                        _IStockService.UpdateEffectiveTillDate(ProductIDK);
                        PRH.ProductId = ProductIDK;
                        PRH.ProductRateHistoryId = RHIDK;
                        PRH.MRP = model.MRP;
                        PRH.PurRate = model.PurRate;
                        PRH.EffectiveFrom = DateTime.Now;
                        PRH.EffectiveTill = null;
                        _IStockService.InserRateHistory(PRH);
                        message = "Added successfully";
                    }
                }
                else
                {
                    return Json(new { status = "failed", data = errormsg });
                }
                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult DeleteProductRateHistory(string RateHistoryId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                // decrypt values
                int HistoryIdK = 0;
                if (!string.IsNullOrEmpty(RateHistoryId))
                    int.TryParse(_ICustomEncryption.base64d(RateHistoryId), out HistoryIdK);

                if (HistoryIdK > 0)
                {
                    _IStockService.DeleteProductRateHistory(HistoryIdK);
                    message = "Deleted successfully";
                }
                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }
        #endregion

        #region"Product Specification"
        public ActionResult GetProductSpecification(DataSourceRequest command, string ProductId
                                            , IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int ProductIdIK = 0;
                if (!string.IsNullOrEmpty(ProductId))
                    int.TryParse(_ICustomEncryption.base64d(ProductId), out ProductIdIK);

                //var IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Product Specification", "Add Record");
                //var IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Product Specification", "Edit Record");
                //var IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Product Specification", "Delete Record");

                var IsAuthToAdd = true;
                var IsAuthToEdit = true;
                var IsAuthToDelete = true;

                var PSList = _IStockService.GetAllProductSpecificationBysP(ref count, ProductId: ProductIdIK);
                var gridModel = new DataSourceResult
                {
                    Data = PSList.Select(p =>
                    {
                        var stocklist = new ProductInfo();
                        stocklist.productid = (_ICustomEncryption.base64e(p.productid.ToString()));
                        stocklist.ProductDetailId = (_ICustomEncryption.base64e(p.ProductDetailId.ToString())); ;
                        stocklist.ProductAttributeId = p.ProductAttributeId;
                        stocklist.ProductAttributeValueName = p.ProductAttributeValueName;
                        stocklist.ProductAttributeValueId = p.ProductAttributeValueId;
                        stocklist.ProductAttributename = p.ProductAttributename;
                        stocklist.IsAuthToEdit = IsAuthToEdit;
                        stocklist.IsAuthToDelete = _IStockService.GetDeleteStatus(IsAuthToDelete, p.productid);
                        stocklist.IsAuthToAdd = IsAuthToAdd;
                        return stocklist;
                    }).AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = PSList.Count()
                };

                return Json(gridModel);

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public string CheckPSError(ProductInfo model)
        {
            StringBuilder errormsg = new StringBuilder();

            if (string.IsNullOrEmpty(model.ProductAttributeId.ToString()))
            {
                errormsg.Append("Please provide Product Attribute ε");
            }
            if (string.IsNullOrEmpty(model.ProductAttributeValue.ToString()))
            {
                errormsg.Append("Please provide Product Attribute Value ε");
            }
            return errormsg.ToString();
        }

        public ActionResult InsertProductSpecification(ProductInfo model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                string errormsg = CheckPSError(model);
                if (string.IsNullOrEmpty(errormsg))
                {
                    // decrypt values
                    int ProductIDK = 0, PSIDK = 0;

                    if (!string.IsNullOrEmpty(model.productid))
                        int.TryParse(_ICustomEncryption.base64d(model.productid), out ProductIDK);

                    if (!string.IsNullOrEmpty(model.ProductDetailId))
                        int.TryParse(_ICustomEncryption.base64d(model.ProductDetailId), out PSIDK);

                    KSModel.Models.ProductDetail PD = new KSModel.Models.ProductDetail();
                    PD.ProductAttributeId = Convert.ToInt32(model.ProductAttributeId);
                    PD.ProductAttributeValue = model.ProductAttributeValueName;
                    PD.ProductId = ProductIDK;
                    PD.ProductDetailId = PSIDK;
                    _IStockService.InserorUpdateProductSpecification(PD);

                    //_IStockService.InsertOrUpdateProductSpecificationbySp(ProductIDK, PSIDK, model.ProductAttributeValueName, Convert.ToInt32(model.ProductAttributeId));
                    if (PSIDK > 0)
                    {
                        message = "Updated Successfully";
                    }
                    else
                    {
                        message = "Added Successfully";
                    }

                }
                else
                {
                    return Json(new { status = "failed", data = errormsg });
                }
                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult ViewProSpecification(string DetailId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                // decrypt values
                int DetailIdK = 0;
                if (!string.IsNullOrEmpty(DetailId))
                    int.TryParse(_ICustomEncryption.base64d(DetailId), out DetailIdK);
                List<ProductInfo> DetailInfo = new List<ProductInfo>();
                if (DetailIdK > 0)
                {
                    var getProductDetail = _IStockService.GetProductDetailById(DetailId: DetailIdK);
                    DetailInfo.Add(new ProductInfo
                    {
                        productid = getProductDetail.ProductId == null ? string.Empty : getProductDetail.ProductId.ToString().Trim(),
                        ProductDetailId = getProductDetail.ProductDetailId == null ? string.Empty : getProductDetail.ProductDetailId.ToString().Trim(),
                        ProductAttributeId = getProductDetail.ProductAttributeId == null ? string.Empty : getProductDetail.ProductAttributeId.ToString().Trim(),
                        ProductAttributeValueName = getProductDetail.ProductAttributeValue == null ? string.Empty : getProductDetail.ProductAttributeValue.ToString().Trim(),
                        ProductAttribute = _IDropDownService.ProductAttributeList(0),
                        ProductAttributeValue = _IDropDownService.ProductAttributeValueList(ProductAtrributeId: Convert.ToInt32(getProductDetail.ProductAttributeId.ToString()))
                    });

                }
                return Json(new { DetailInfo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult GetCurrentAttributes(string ProductId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                int ProductIdk = 0;
                if (!string.IsNullOrEmpty(ProductId))
                    int.TryParse(_ICustomEncryption.base64d(ProductId), out ProductIdk);

                List<SelectListItem> attributelst = new List<SelectListItem>();
                attributelst = _IDropDownService.ProductAttributeList(ProductIdk);
                return Json(new { attributelst }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }

        }

        public ActionResult GetAttributeValues(string AttributeId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                int AttrId = 0;
                if (!string.IsNullOrEmpty(AttributeId))
                    AttrId = Convert.ToInt32(AttributeId);

                List<SelectListItem> attributelst = new List<SelectListItem>();
                attributelst = _IDropDownService.ProductAttributeValueList(AttrId);
                return Json(new { attributelst }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }

        }

        public ActionResult DeleteProductDetail(string detailid)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                // decrypt values
                int DetailIdIK = 0;
                if (!string.IsNullOrEmpty(detailid))
                    int.TryParse(_ICustomEncryption.base64d(detailid), out DetailIdIK);

                if (DetailIdIK > 0)
                {
                    _IStockService.DeleteProductDetail(DetailIdIK);
                    message = "Deleted successfully";
                }
                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult SaveAttributevalue(string AttributeId, string AttributeValue)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                int AttrId = 0;
                AttrId = Convert.ToInt32(AttributeId);
                KSModel.Models.ProductAttributeValue PAV = new KSModel.Models.ProductAttributeValue();
                PAV.ProductAttributeId = AttrId;
                PAV.ProductAttributeValue1 = AttributeValue;
                bool exist = _IStockService.InsertProductAttributeValue(PAV);

                List<SelectListItem> attributevallst = new List<SelectListItem>();
                attributevallst = _IDropDownService.ProductAttributeValueList(AttrId);
                return Json(new { attributevallst, Isexist = exist }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }
        #endregion

        #region "Product Unit Conversion"
        public ActionResult GetProductUnitCoversion(DataSourceRequest command, string ProductId
                                            , IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int ProductIdIK = 0;
                if (!string.IsNullOrEmpty(ProductId))
                    int.TryParse(_ICustomEncryption.base64d(ProductId), out ProductIdIK);

                //var IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Product Specification", "Add Record");
                //var IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Product Specification", "Edit Record");
                //var IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Product Specification", "Delete Record");

                var IsAuthToAdd = true;
                var IsAuthToEdit = true;
                var IsAuthToDelete = true;

                var PUCList = _IStockService.GetAllProductUnitConversionBysP(ref count, ProductId: ProductIdIK);
                var gridModel = new DataSourceResult
                {
                    Data = PUCList.Select(p =>
                    {
                        var stocklist = new ProductInfo();
                        stocklist.productid = (_ICustomEncryption.base64e(p.productid.ToString()));
                        stocklist.ProductUnitConversionId = (_ICustomEncryption.base64e(p.ProductUnitConversionId.ToString())); ;
                        stocklist.ConversionUnitId = p.ConversionUnitId;
                        stocklist.Conversionfactor = p.Conversionfactor;
                        stocklist.ConversionUnitName = p.ConversionUnitName;
                        stocklist.IsAuthToEdit = IsAuthToEdit;
                        stocklist.IsAuthToDelete = _IStockService.GetDeleteStatus(IsAuthToDelete, p.productid);
                        stocklist.IsAuthToAdd = IsAuthToAdd;
                        return stocklist;
                    }).AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = PUCList.Count()
                };

                return Json(gridModel);

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public string CheckPUCError(ProductInfo model)
        {
            StringBuilder errormsg = new StringBuilder();

            if (string.IsNullOrEmpty(model.ConversionUnitId.ToString()))
            {
                errormsg.Append("Please provide Conversion Unit ε");
            }
            if (string.IsNullOrEmpty(model.Conversionfactor.ToString()))
            {
                errormsg.Append("Please provide Conversion Factor ε");
            }
            return errormsg.ToString();
        }

        public ActionResult InsertProductUnitConversion(ProductInfo model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                string errormsg = CheckPUCError(model);
                if (string.IsNullOrEmpty(errormsg))
                {
                    // decrypt values
                    int ProductIDK = 0, PUCIDK = 0;

                    if (!string.IsNullOrEmpty(model.productid))
                        int.TryParse(_ICustomEncryption.base64d(model.productid), out ProductIDK);

                    if (!string.IsNullOrEmpty(model.ProductUnitConversionId))
                        int.TryParse(_ICustomEncryption.base64d(model.ProductUnitConversionId), out PUCIDK);

                    KSModel.Models.ProductUnitConversion PUC = new KSModel.Models.ProductUnitConversion();
                    PUC.ProductId = ProductIDK;
                    PUC.CoversionUnitId = Convert.ToInt32(model.ConversionUnitId);
                    PUC.ConversionFactor = Convert.ToDecimal(model.Conversionfactor);
                    _IStockService.InsertProductUnitConversion(PUC);
                    message = "Added Successfully";
                }
                else
                {
                    return Json(new { status = "failed", data = errormsg });
                }
                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult DeleteProductUnitConversion(string UnitConversionId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                // decrypt values
                int UnitConversionIdK = 0;
                if (!string.IsNullOrEmpty(UnitConversionId))
                    int.TryParse(_ICustomEncryption.base64d(UnitConversionId), out UnitConversionIdK);

                if (UnitConversionIdK > 0)
                {
                    _IStockService.DeleteProductUnitConversion(UnitConversionIdK);
                    message = "Deleted successfully";
                }
                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult UploadProductImage(string ProductId)
        {
            string message = "";
            if (Request.Files.Count > 0)
            {
                try
                {
                    string pid = "", pname = "", pImageId = "";
                    if (System.Web.HttpContext.Current.Request.Form.AllKeys.Any())
                    {
                        pid = System.Web.HttpContext.Current.Request.Form["productId"];
                        pname = System.Web.HttpContext.Current.Request.Form["productName"];
                        pImageId = System.Web.HttpContext.Current.Request.Form["productImageId"];
                    }

                    string filename = pid + "ProductImage";
                    string dir = "";

                    var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);



                    if (Request.Url.AbsoluteUri.Contains("localhost"))
                        // dir = _WebHelper.GetStoreLocation() + "/Images/" + SchoolDbId + "/ProductImages/";
                        dir = "/Images/" + SchoolDbId + "/ProductImages/";
                    else
                        dir = "../Images/" + SchoolDbId + "/ProductImages/";

                    // dir = _WebHelper.GetStoreLocation() + "/Images/" + SchoolDbId + "/ProductImages/";
                    //if (!System.IO.Directory.Exists((dir)))
                    //{
                    //    System.IO.Directory.CreateDirectory((dir));
                    //}


                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                            string ext = Path.GetExtension(fname);
                            fname = filename + ext.ToString();

                        }
                        else
                        {
                            string ext = Path.GetExtension(file.FileName);
                            fname = filename + ext.ToString();
                        }
                        filename = fname;
                        fname = Path.Combine(Server.MapPath(dir), fname);

                        WebImage img = new WebImage(file.InputStream);
                        if (img.Width > 300)
                            img.Resize(300, 250);
                        img.Save(fname);
                        // file.SaveAs(fname);
                        int ProductIDK = 0, PIDK = 0;

                        if (!string.IsNullOrEmpty(pid))
                            int.TryParse(_ICustomEncryption.base64d(pid), out ProductIDK);

                        if (!string.IsNullOrEmpty(pImageId))
                            int.TryParse(_ICustomEncryption.base64d(pImageId), out PIDK);

                        KSModel.Models.ProductImage PI = new KSModel.Models.ProductImage();
                        if (PIDK > 0)
                        {
                            PI.ProductId = ProductIDK;
                            PI.Image = filename;
                            PI.ProductImageId = PIDK;
                            _IStockService.UpdateProductImage(PI);
                            message = "Product Image Updated Successfully";
                        }
                        else
                        {
                            PI.ProductId = ProductIDK;
                            PI.Image = filename;
                            _IStockService.InsertProductImage(PI);
                            message = "Product Image submitted Successfully";
                        }
                    }
                    // Returns message that successfully uploaded  
                    return Json(message);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }
        #endregion

        #region"Export Options"

        public ActionResult Export_Product()
        {
            return Json(new
            {
                status = "success",
                data = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportData_Product(string ProductGroup, string ProductType, string Productname, string IsExcel)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                string message = "";
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                int ProductGroupId = 0, ProductTypeId = 0;

                if (!string.IsNullOrEmpty(ProductGroup))
                {
                    ProductGroupId = Convert.ToInt32(ProductGroup);
                }
                if (!string.IsNullOrEmpty(ProductType))
                {
                    ProductTypeId = Convert.ToInt32(ProductType);
                }

                var ProductList = _IStockService.GetAllStocksBysP(ref count, ProductName: Productname, ProductGroupId: ProductGroupId
                                            , ProductTypeId: ProductTypeId);



                List<ProductList> productinfo = new List<ProductList>();
                for (int i = 0; i < ProductList.Count; i++)
                {
                    productinfo.Add(new ProductList
                        {
                            ProductName = ProductList[i].ProductName,
                            ProductAbbreviation = ProductList[i].ProductAbbreviation,
                            ProductGroup = ProductList[i].ProductGroupName,
                            ProductType = ProductList[i].ProductTypeName,
                            OpeningBalance = ProductList[i].OpeningBalance,
                            Unit = ProductList[i].UnitName
                        });

                }

                DataTable dt = new DataTable();
                dt = ToDataTable(productinfo);
                if (dt.Rows.Count > 0)
                {
                    string title = "Products";

                    if (IsExcel == "1")
                    {
                        _IExportHelper.ExportToExcel(dt, Title: title);
                    }
                    else if (IsExcel == "2")
                    {
                        _IExportHelper.ExportToPrint(dt, Title: title);
                    }
                    else
                    {
                        _IExportHelper.ExportToPDF(dt, Title: title);
                    }
                    return null;
                }
                else
                {
                    TempData["error"] = "No record found!";
                    return RedirectToAction("Product");
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                },
                    JsonRequestBehavior.AllowGet);
            }

        }

        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        #endregion

        #region"Packages"
        public string CheckPackageError(ProductInfo model)
        {
            StringBuilder errormsg = new StringBuilder();

            if (string.IsNullOrEmpty(model.PackageName.ToString()))
            {
                errormsg.Append("Please provide Package Name ε");
            }
            return errormsg.ToString();
        }

        public ActionResult SubmitPackage(ProductInfo model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                string errormsg = CheckPackageError(model);
                if (string.IsNullOrEmpty(errormsg))
                {
                    // decrypt values
                    int PackageIDK = 0;

                    if (!string.IsNullOrEmpty(model.PackageId))
                        int.TryParse(_ICustomEncryption.base64d(model.PackageId), out PackageIDK);

                    KSModel.Models.StockPackage stockPack = new KSModel.Models.StockPackage();
                    stockPack.StockPackagesId = PackageIDK;
                    stockPack.StockPackages = model.PackageName;

                    int packageId = _IStockService.InsertorUpdatePackage(stockPack);

                    string[] clsarr = model.ClassIds;
                    for (int i = 0; i < clsarr.Length; i++)
                    {
                        KSModel.Models.StockPackageClass StockClass = new KSModel.Models.StockPackageClass();
                        int classid = 0;
                        if (clsarr[i].ToString() == "")
                        {

                        }
                        else
                        {
                            classid = Convert.ToInt32(clsarr[i].ToString());
                            StockClass.ClassId = classid;
                            StockClass.StockpackageId = packageId;
                            _IStockService.InsertStockPackageClass(StockClass);
                        }

                    }

                    string[] prodarr = model.productid.ToString().Split(',');
                    string[] detailarr = model.PackageDetailId.ToString().Split(',');
                    string[] Qantarr = model.Quantity.ToString().Split(',');
                    for (int i = 0; i < prodarr.Length; i++)
                    {
                        KSModel.Models.StockPackageDetail StockDetail = new KSModel.Models.StockPackageDetail();
                        if (string.IsNullOrEmpty(model.PackageId))
                        {
                            StockDetail.PackageDetailId = 0;
                        }
                        else
                        {
                            StockDetail.PackageDetailId = Convert.ToInt32(detailarr[i].ToString());
                        }
                        int proId = 0;
                        if (string.IsNullOrEmpty(prodarr[i].ToString()))
                        {
                        }
                        else
                        {
                            proId = Convert.ToInt32(prodarr[i].ToString());
                            StockDetail.ProductId = proId;
                            StockDetail.StockPackageId = packageId;
                            StockDetail.Quantity = Convert.ToDecimal(Qantarr[i].ToString());
                            _IStockService.InsertorUpdateStockPackageDetail(StockDetail);
                        }
                    }
                    if (PackageIDK > 0)
                    {
                        message = "Updated Successfully";
                    }
                    else
                    {
                        message = "Added Successfully";
                    }
                }
                else
                {
                    return Json(new { status = "failed", data = errormsg });
                }
                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult GetAllPackages(DataSourceRequest command, ProductInfo model
                                              , IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values

                if (string.IsNullOrEmpty(model.PackageName))
                {
                    model.PackageName = "";
                }
                var PackageList = _IStockService.GetAllPackagesBysP(ref count, PackageName: model.PackageName);
                //var IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Manage Package", "Add Record");
                //var IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Manage Package", "Edit Record");
                //var IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Manage Package", "Delete Record");

                var IsAuthToAdd = true;
                var IsAuthToEdit = true;
                var IsAuthToDelete = true;

                var Data = PackageList.PagedForCommand(command).Select(p =>
                {
                    var stocklist = new ProductInfo();
                    stocklist.PackageId = (_ICustomEncryption.base64e(p.PackageId.ToString()));
                    stocklist.PackageName = p.PackageName;
                    stocklist.NumberofClasses = p.NumberofClasses;
                    stocklist.NumberofProducts = p.NumberofProducts;
                    stocklist.Quantity = p.Quantity;
                    stocklist.IsAuthToEdit = IsAuthToEdit;
                    stocklist.IsAuthToDelete = true;
                    stocklist.IsAuthToAdd = IsAuthToAdd;
                    return stocklist;
                }).AsQueryable().Sort(sort);
                var gridModel = new DataSourceResult
                {
                    Data = Data.ToList(),
                    Total = PackageList.Count()
                };
                return Json(gridModel);

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult DeletePackage(string PackageId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                // decrypt values
                int PackageIdK = 0;
                if (!string.IsNullOrEmpty(PackageId))
                    int.TryParse(_ICustomEncryption.base64d(PackageId), out PackageIdK);

                if (PackageIdK > 0)
                {
                    _IStockService.DeletePackage(PackageIdK);
                    message = "Deleted successfully";
                }
                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        [HttpGet]
        public ActionResult ViewPackageInformation(string PackageId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int PackageIdK = 0;
                if (!string.IsNullOrEmpty(PackageId))
                    int.TryParse(_ICustomEncryption.base64d(PackageId), out PackageIdK);
                List<ProductInfo> productinfo = new List<ProductInfo>();
                if (PackageIdK > 0)
                {
                    var getMasterPackage = _IStockService.GetPackageById(PackageId: PackageIdK);
                    productinfo.Add(new ProductInfo
                    {
                        PackageName = getMasterPackage.PackageName == null ? string.Empty : getMasterPackage.PackageName.Trim(),
                        Classnames = getMasterPackage.Classnames,
                        PackageDetail = getMasterPackage.PackageDetail
                    });
                }

                return Json(new { productinfo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult PackageDetail(string PackageId)
        {
            ProductInfo product = new ProductInfo();
            if (PackageId == "")
            {
                string[] classids = new string[0];
                List<PackageDetail> pckdet = new List<ViewModel.ViewModel.Stock.PackageDetail>();
                product.PackageName = "";
                product.ClassIds = classids;
                product.PackageDetail = pckdet;
                product.PackageId = "";
                product.Products = _IDropDownService.GetAllProducts();
                product.UnitName = "";
                product.Class = _IDropDownService.GetAllClasses();
                return PartialView("_PackageDetail", product);
            }
            else
            {

                int PackageIdK = 0;
                if (!string.IsNullOrEmpty(PackageId))
                    int.TryParse(_ICustomEncryption.base64d(PackageId), out  PackageIdK);
                var Package = _IStockService.GetPackageById(PackageIdK, true);
                product.PackageName = Package.PackageName;
                product.ClassIds = Package.ClassIds;
                product.PackageDetail = Package.PackageDetail;
                product.PackageId = PackageId;
                product.Products = _IDropDownService.GetAllProducts();
                product.UnitName = "";
                product.Class = _IDropDownService.GetAllClasses();
                return PartialView("_PackageDetail", product);
            }
        }

        #region"Export Options"

        public ActionResult Export_Package()
        {
            return Json(new
            {
                status = "success",
                data = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportData_Package(string PackageName, string IsExcel)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                string message = "";
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                var PackageList = _IStockService.GetAllPackagesBysP(ref count, PackageName: PackageName);
                DataTable dt = new DataTable();
                dt.Columns.Add("Package Name");
                dt.Columns.Add("Classes");
                dt.Columns.Add("Product");
                dt.Columns.Add("Quantity");

                List<ProductInfo> Packageinfo = new List<ProductInfo>();
                foreach (var pack in PackageList)
                {
                    //for classes detail
                    var getclasses = _IStockService.GetPackageById(PackageId: Convert.ToInt32(pack.PackageId));
                    var names = "";
                    foreach (var classes in getclasses.Classnames)
                    {
                        names += classes + ",";
                    }
                    names = names.TrimEnd(',');


                    //for package detail
                    List<PackageDetail> packagelists = new List<PackageDetail>();
                    foreach (var products in getclasses.PackageDetail)
                    {
                        packagelists.Add(new PackageDetail
                        {
                            ProductName = products.ProductName,
                            Quantity = products.Quantity
                        });
                    }

                    //pass list of package for datatable
                    Packageinfo.Add(new ProductInfo
                    {
                        PackageName = pack.PackageName,
                        ClassesNames = names,
                        PackageDetail = packagelists
                    });
                }



                DataRow dr;
                DataRow dr2;
                foreach (var item in Packageinfo)
                {
                    dr = dt.NewRow();
                    dr[0] = item.PackageName;
                    dr[1] = item.ClassesNames;
                    dt.Rows.Add(dr);
                    foreach (var i in item.PackageDetail)
                    {
                        dr = dt.NewRow();
                        dr[2] = i.ProductName;
                        dr[3] = i.Quantity;
                        dt.Rows.Add(dr);
                    }
                    dr = dt.NewRow();
                    dt.Rows.Add(dr);
                    dt.AcceptChanges();
                }

                if (dt.Rows.Count > 0)
                {
                    string title = "Packages";

                    if (IsExcel == "1")
                    {
                        _IExportHelper.ExportToExcel(dt, Title: title);
                    }
                    else if (IsExcel == "2")
                    {
                        _IExportHelper.ExportToPrint(dt, Title: title);
                    }
                    else
                    {
                        _IExportHelper.ExportToPDF(dt, Title: title);
                    }
                    return null;
                }
                else
                {
                    TempData["error"] = "No record found!";
                    return RedirectToAction("Product");
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                },
                    JsonRequestBehavior.AllowGet);
            }

        }
        #endregion


        #endregion

        #region"Vouchers"
        public ActionResult StockTransaction(string VoucherId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                int UserId = user.UserId;
                VoucherInfo model = new VoucherInfo();
                model.StoreList = _IDropDownService.StoreList(UserId);
                model.VouchertypeList = _IDropDownService.VoucherTypeList();
                model.AccountTypeList = _IDropDownService.AccountTypeList();
                model.PackageList = _IDropDownService.PackageList();
                model.TaxTypeList = _IDropDownService.TaxTypeList();
                model.ProductGroupList = _IDropDownService.ProductGroupList();
                model.ProductTypeList = _IDropDownService.ProductTypeList();
                model.ProductList = _IDropDownService.ProductList();
                int voucherIdk = 0;
                if (!string.IsNullOrEmpty(VoucherId))
                    int.TryParse(_ICustomEncryption.base64d(VoucherId), out voucherIdk);
                if (voucherIdk > 0)
                {
                    model.VoucherId = VoucherId;
                    var voucherdetail = _IStockService.GetVoucherDetail(voucherIdk, 2);
                    if (voucherdetail != null)
                    {
                        model.Voucher = voucherdetail.Voucher;
                        model.VoucherDate = voucherdetail.VoucherDate;
                        model.AccountTypeId = voucherdetail.AccountTypeId;
                        model.AccountList = _IDropDownService.AccountList(Convert.ToInt32(model.AccountTypeId));
                        model.AccountId = voucherdetail.AccountId;
                        model.DocumentDate = voucherdetail.DocumentDate;
                        model.DocumentNo = voucherdetail.DocumentNo;
                        model.StoreId = voucherdetail.StoreId;
                        model.TaxTypeId = model.TaxTypeId;
                        model.VoucherTypeId = voucherdetail.VoucherTypeId;
                    }
                }
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult StockTransaction(VoucherInfo model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }




                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetPackageDetail(string PackageId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int PackageIdK = 0;
                if (!string.IsNullOrEmpty(PackageId))
                    PackageIdK = Convert.ToInt32(PackageId);
                List<ProductInfo> productinfo = new List<ProductInfo>();
                if (PackageIdK > 0)
                {
                    var getMasterPackage = _IStockService.GetPackageById(PackageId: PackageIdK);
                    productinfo.Add(new ProductInfo
                    {
                        PackageName = getMasterPackage.PackageName == null ? string.Empty : getMasterPackage.PackageName.Trim(),
                        Classnames = getMasterPackage.Classnames,
                        PackageDetail = getMasterPackage.PackageDetail
                    });
                }

                return Json(new { productinfo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllProductsDetail(DataSourceRequest command, VoucherInfo model, string PackageId = null, string ProductArray = null
                                          , IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int PackageIdIK = 0, VoucherIdK = 0;
                if (!string.IsNullOrEmpty(PackageId))
                    PackageIdIK = Convert.ToInt32(PackageId);
                if (!string.IsNullOrEmpty(model.VoucherId))
                    int.TryParse(_ICustomEncryption.base64d(model.VoucherId), out VoucherIdK);
                //var getMasterPackage = new List<productde
                if (VoucherIdK > 0)
                {
                    var getMasterPackage = _IStockService.GetProductDetailList(VoucherId: VoucherIdK);

                    if (!string.IsNullOrEmpty(ProductArray))
                    {

                        dynamic stuff = JsonConvert.DeserializeObject(ProductArray);
                        foreach (var item in stuff)
                        {
                            var productlist = new ProductsDetailList();
                            productlist.ProductId = item.id;
                            productlist.ProductName = item.name;
                            productlist.Quantity = item.quantity;
                            productlist.Price = "0";
                            productlist.GSTRate = "0";
                            productlist.CGSTValue = "0";
                            productlist.SGSTValue = "0";
                            productlist.IGSTValue = "0";
                            productlist.CessValue = "0";
                            productlist.TotalAmount = "0";
                            getMasterPackage.Add(productlist);
                        }
                    }
                    decimal TotalAmount = 0;
                    var gridModel = new DataSourceResult
                    {
                        Data = getMasterPackage.PagedForCommand(command).Select(p =>
                        {
                            var productlist = new ProductsDetailList();
                            productlist.ProductId = p.ProductId;
                            productlist.ProductName = p.ProductName.ToString();
                            productlist.Quantity = p.Quantity.ToString();
                            productlist.Price = p.Price.ToString();
                            productlist.GSTRate = p.GSTRate.ToString();
                            productlist.CGSTValue = p.CGSTValue.ToString();
                            productlist.SGSTValue = p.SGSTValue.ToString();
                            productlist.IGSTValue = p.IGSTValue.ToString();
                            productlist.CessValue = p.CessValue.ToString();
                            TotalAmount = Convert.ToDecimal(p.TaxableAmount) + Convert.ToDecimal(p.Price);
                            productlist.TotalAmount = TotalAmount.ToString();
                            return productlist;
                        }).AsQueryable().Sort(sort).ToList(),
                        Total = getMasterPackage.Count()
                    };
                    return Json(gridModel);
                }
                else if (PackageIdIK > 0)
                {

                    var getMasterPackage = _IStockService.GetPackageById(PackageId: PackageIdIK);

                    if (!string.IsNullOrEmpty(ProductArray))
                    {
                        dynamic stuff = JsonConvert.DeserializeObject(ProductArray);
                        foreach (var item in stuff)
                        {
                            getMasterPackage.PackageDetail.Add(new PackageDetail { productid = item.id, ProductName = item.name, Quantity = item.quantity });
                        }
                    }
                    var Data = getMasterPackage.PackageDetail.PagedForCommand(command).Select(p =>
                {
                    var productlist = new ProductsDetailList();
                    productlist.ProductId = p.productid.ToString();
                    productlist.ProductName = p.ProductName.ToString();
                    productlist.Quantity = p.Quantity.ToString();
                    productlist.Price = "0";
                    productlist.GSTRate = "0";
                    productlist.CGSTValue = "0";
                    productlist.SGSTValue = "0";
                    productlist.IGSTValue = "0";
                    productlist.CessValue = "0";
                    productlist.TotalAmount = "0";
                    return productlist;
                }).AsQueryable().Sort(sort);


                    var gridModel = new DataSourceResult
                    {
                        Data = Data.ToList(),
                        Total = getMasterPackage.PackageDetail.Count()
                    };
                    return Json(gridModel);
                }
                var data1 = new List<ProductsDetailList>();
                var gridModel1 = new DataSourceResult
                 {
                     Data = data1,
                     Total = 0
                 };
                return Json(gridModel1);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult GetProductList(string GroupId, string ProductTypeId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                int PGId = 0, PTId = 0;
                if (!string.IsNullOrEmpty(GroupId))
                    PGId = Convert.ToInt32(GroupId);
                if (!string.IsNullOrEmpty(ProductTypeId))
                    PTId = Convert.ToInt32(ProductTypeId);
                List<SelectListItem> productList = new List<SelectListItem>();
                productList = _IDropDownService.ProductList(groupid: PGId, producttypeid: PTId);
                return Json(new { productList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult GetAccountList(string AccountTypeid)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                int ATId = 0;
                if (!string.IsNullOrEmpty(AccountTypeid))
                    ATId = Convert.ToInt32(AccountTypeid);

                List<SelectListItem> accountList = new List<SelectListItem>();
                accountList = _IDropDownService.AccountList(ATId);
                return Json(new { accountList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult AddNewProduct(string GroupId, string ProductTypeId, string ProductName)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                int PGId = 0, PTId = 0;
                if (!string.IsNullOrEmpty(GroupId))
                    PGId = Convert.ToInt32(GroupId);
                if (!string.IsNullOrEmpty(ProductTypeId))
                    PTId = Convert.ToInt32(ProductTypeId);
                Product ProductM = new KSModel.Models.Product();
                ProductM.ProductName = ProductName;
                ProductM.ProductGroupId = PGId;
                ProductM.ProductTypeId = PTId;
                _IStockService.InsertProduct(ProductM);

                List<SelectListItem> productList = new List<SelectListItem>();
                productList = _IDropDownService.ProductList(groupid: PGId, producttypeid: PTId);
                return Json(new { productList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        #endregion
    }
}