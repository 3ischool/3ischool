﻿using BAL.BAL.Common;
using BAL.BAL.Security;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.Kendo;
using DAL.DAL.LibraryModule;
using DAL.DAL.SettingService;
using DAL.DAL.StaffModule;
using DAL.DAL.StudentModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Library;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace KS_SmartChild.Controllers
{
    public class LibraryController : Controller
    {
        // GET: Library

        #region fields

        public int count = 0;
        public static int? AllowedDaysForBook = 0;
        public static decimal? FinePerDay = 0;

        private readonly IStaffService _IStaffService;
        private readonly IStudentService _IStudentService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly IBookMasterService _IBookMasterService;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ILogService _Logger;
        private readonly IWebHelper _WebHelper;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IExportHelper _IExportHelper;
        private readonly IUserManagementService _IUserManagementService;
        private readonly ICommonMethodService _ICommonMethodService;
        #endregion

        #region ctor

        public LibraryController(
            IConnectionService IConnectionService, IBookMasterService IBookMasterService,
            IDropDownService IDropDownService, IAddressMasterService IAddressMasterService,
            IActivityLogMasterService IActivityLogMasterService, IStudentService IStudentService,
            IActivityLogService IActivityLogService, IStaffService IStaffService,
            ILogService Logger, IUserService IUserService, IUserManagementService IUserManagementService,
            IWebHelper WebHelper, ICatalogMasterService ICatalogMasterService,
            ISchoolSettingService ISchoolSettingService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption, IExportHelper IExportHelper,
            ICommonMethodService ICommonMethodService)
        {
            this._IBookMasterService = IBookMasterService;
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._Logger = Logger;
            this._WebHelper = WebHelper;
            this._ISchoolSettingService = ISchoolSettingService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IAddressMasterService = IAddressMasterService;
            this._IStudentService = IStudentService;
            this._IStaffService = IStaffService;
            this._IExportHelper = IExportHelper;
            this._IUserManagementService = IUserManagementService;
            this._ICommonMethodService = ICommonMethodService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        #endregion

        #region BookSeller

        public BookSellerViewModel PrepareBookSellerModel(BookSellerViewModel model)
        {
            //paging
            model.gridPageSizes = _ISchoolSettingService.GetorSetSchoolData("gridPageSizes", "20,50,100,250").AttributeValue;
            model.defaultGridPageSize = _ISchoolSettingService.GetorSetSchoolData("defaultGridPageSize", "20").AttributeValue;

            // cities
            model.AddressModel.AvailableCities = _IDropDownService.AddressCityList();
            // cities for filter
            model.AvailableCities = _IDropDownService.AddressCityList();

            // states
            model.AddressModel.AddressCityModel.AvailableStates = _IDropDownService.AddressStateList();
            // countries
            model.AddressModel.AddressCityModel.AddressStateModel.AvailableCountries = _IDropDownService.AddressCountryList();

            return model;
        }

        public ActionResult ManageBookSeller()
        {
            SchoolUser user = new SchoolUser();
            var model = new BookSellerViewModel();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);

                    var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookSeller", "View");
                    if (!isauth)
                        return RedirectToAction("AccessDenied", "Error");
                }
                model = PrepareBookSellerModel(model);

                //authentication
                model.IsAuthToGirdAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookSeller", "Add Record");
                model.IsAuthToGridDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookSeller", "Delete Record");
                model.IsAuthToGridEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookSeller", "Edit Record");

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return View(model);
            }
        }

        public ActionResult GetBookSellers(DataSourceRequest command, BookSellerViewModel model
                                                , IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int CityIdK = 0;
                int? encCityId = null;
                if (!string.IsNullOrEmpty(model.CityId))
                {
                    encCityId = Convert.ToInt32(model.CityId);
                }

                var BooksellersList = _IBookMasterService.GetAllBookSellers(ref count, BookSellerName: model.BookSellerName,
                                                                            CityId: encCityId, Isfilter: true);
                var gridModel = new DataSourceResult
                {
                    Data = BooksellersList.Select(p =>
                    {
                        var sellerslist = new BookSellerViewModel();
                        sellerslist.BookSellerId = _ICustomEncryption.base64e(p.BookSellerId.ToString());
                        sellerslist.BookSellerName = p.BookSellerName;
                        sellerslist.Address1 = p.Address1;
                        sellerslist.Address2 = p.Address2;
                        sellerslist.CityId = p.CityId != null ? p.CityId.ToString() : "";

                        if (p.CityId != null)
                        {
                            var getCity = _IAddressMasterService.GetAddressCityById(AddressCityId: (int)p.CityId);
                            if (getCity != null)
                            {
                                sellerslist.CityName = getCity.City;
                            }
                        }
                        sellerslist.Landline = p.Landline;
                        sellerslist.Mobile = p.Mobile;
                        sellerslist.Emails = p.Emails;
                        sellerslist.WebSite = p.WebSite;
                        sellerslist.IsAuthToDelete = false;

                        var hasBookEdition = p.BookEditions.Count();
                        if (hasBookEdition > 0)
                            sellerslist.IsAuthToDelete = true;

                        return sellerslist;
                    }).AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = BooksellersList.Count()
                };

                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public string CheckBookSellerError(BookSellerViewModel model)
        {
            StringBuilder errormsg = new StringBuilder();

            if (string.IsNullOrEmpty(model.BookSellerName) || string.IsNullOrWhiteSpace(model.BookSellerName))
                errormsg.Append("Seller Name required ε");

            if (string.IsNullOrEmpty(model.CityId) || string.IsNullOrWhiteSpace(model.CityId))
                errormsg.Append("City required ε");

            return errormsg.ToString();
        }

        public ActionResult GetFilterCities()
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                List<SelectListItem> paramCities = new List<SelectListItem>();

                var cities = _IDropDownService.AddressCityList().ToList();
                if (cities.Count() > 0)
                {
                    foreach (var city in cities)
                    {
                        paramCities.Add(new SelectListItem
                        {
                            Text = city.Text,
                            Value = city.Value.ToString(),
                            Selected = false
                        });
                    }
                }
                return Json(new { paramCities }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult InsertOrUpdateBookSeller(BookSellerViewModel model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                string errormsg = CheckBookSellerError(model);
                if (string.IsNullOrEmpty(errormsg))
                {
                    // decrypt values
                    int BookSellerIdK = 0;
                    if (!string.IsNullOrEmpty(model.BookSellerId))
                        int.TryParse(_ICustomEncryption.base64d(model.BookSellerId), out BookSellerIdK);

                    KSModel.Models.BookSeller SellerData = new KSModel.Models.BookSeller();
                    if (BookSellerIdK > 0)
                    {
                        //update module
                        SellerData = _IBookMasterService.GetBookSellerById(BookSellerIdK, true);
                        if (SellerData != null)
                        {
                            SellerData.BookSellerName = model.BookSellerName == null ? null : model.BookSellerName.Trim();
                            SellerData.Address1 = model.Address1 == null ? null : model.Address1.Trim();
                            SellerData.Address2 = model.Address2 == null ? null : model.Address2.Trim();
                            SellerData.CityId = model.CityId == null ? 0 : Convert.ToInt32(model.CityId);
                            SellerData.Landline = model.Landline == null ? null : model.Landline.Trim().ToString().TrimEnd(',');
                            SellerData.Mobile = model.Mobile == null ? null : model.Mobile.Trim();
                            SellerData.Emails = model.Emails == null ? null : model.Emails.Trim().ToString().TrimEnd(',');
                            SellerData.WebSite = model.WebSite == null ? null : model.WebSite.Trim();

                            _IBookMasterService.UpdateBookSeller(SellerData);

                            message = "Update successfully";
                        }
                    }
                    else
                    {
                        // Insert new module
                        SellerData.BookSellerName = model.BookSellerName == null ? null : model.BookSellerName.Trim();
                        SellerData.Address1 = model.Address1 == null ? null : model.Address1.Trim();
                        SellerData.Address2 = model.Address2 == null ? null : model.Address2.Trim();
                        SellerData.CityId = model.CityId == null ? 0 : Convert.ToInt32(model.CityId);
                        SellerData.Landline = model.Landline == null ? null : model.Landline.Trim().ToString().TrimEnd(',');
                        SellerData.Mobile = model.Mobile == null ? null : model.Mobile.Trim();
                        SellerData.Emails = model.Emails == null ? null : model.Emails.Trim().ToString().TrimEnd(',');
                        SellerData.WebSite = model.WebSite == null ? null : model.WebSite.Trim();

                        _IBookMasterService.InsertBookSeller(SellerData);

                        message = "Added successfully";
                    }
                }
                else
                    return Json(new { status = "failed", data = errormsg });

                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult DeleteBookSeller(string SellerId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                // decrypt values
                int SellerIdIK = 0;
                if (!string.IsNullOrEmpty(SellerId))
                    int.TryParse(_ICustomEncryption.base64d(SellerId), out SellerIdIK);

                if (SellerIdIK > 0)
                {
                    _IBookMasterService.DeleteBookSeller(SellerIdIK);
                    message = "Deleted successfully";
                }
                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        #endregion

        #region BookTransaction/BookInfromations

        #region BookInformation

        public ActionResult BookInformation(string BookId)
        {
            SchoolUser user = new SchoolUser();
            var model = new BookTransactionViewModel();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                model = PrepareBookInformation(model, BookId);

                // decrypt values
                int BookIdK = 0;
                if (!string.IsNullOrEmpty(BookId))
                    int.TryParse(_ICustomEncryption.base64d(BookId), out BookIdK);


                //authentication
                model.IsAuthToAddTransaction = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookTransactions", "Detail");
                model.IsAuthToIssue = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookTransactions", "Issue Copies");
                model.IsAuthToViewIssued = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookTransactions", "View Details");
                model.IsAuthToViewRecord = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookTransactions", "View Record");
                model.IsAuthToReturn = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookTransactions", "Update Record");
                model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookTransactions", "Delete Record");


                if (BookIdK > 0)
                {
                    var BooksList = _IBookMasterService.GetAllBookMastersByLinq(ref count, BookId: BookIdK).FirstOrDefault();

                    model.AllBookInformation.Add(new BookAdditionalInfoViewModel
                    {
                        BookId = _ICustomEncryption.base64e(BooksList.intbookId.ToString()),
                        BookTitle = BooksList.BookTitle,
                        SubTitle = BooksList.SubTitle,
                        ISBN = BooksList.ISBN,
                        MaterialTypeId = _ICustomEncryption.base64e(BooksList.intMaterialTypeId.ToString()),
                        MaterialType = BooksList.MaterialType,
                        Description = BooksList.Description,
                        BooKMarkAutoIncrement = BooksList.BooKMarkAutoIncrement,
                        TotalEditions = BooksList.TotalEditions == "0" ? "0" : BooksList.TotalEditions,
                        TotalCopies = BooksList.TotalCopies == "0" ? "0" : BooksList.TotalCopies,
                        TotalIssuedBooks = BooksList.TotalIssuedBooks == "0" ? "0" : BooksList.TotalIssuedBooks,
                        AvaiableCopies = BooksList.AvaiableCopies == "0" ? "0" : BooksList.AvaiableCopies
                    });

                    model.Description = BooksList.Description == null || BooksList.Description == "" ? "No Description Found.." : BooksList.Description;
                    model.SubTitle = BooksList.SubTitle == null || BooksList.SubTitle == "" ? "No Subtitle Found..." : BooksList.SubTitle;
                    model.ISBN = BooksList.ISBN;

                }
                else
                {
                    model.ShowBookTitle = true;
                }

                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return View(model);
            }
        }

        public ActionResult IssuedBookList(DataSourceRequest command, BookTransactionViewModel model,
            IEnumerable<Sort> sort = null, string SearchToDate = "", string SearchFromDate = "")
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int? EncBookIdK = null;
                int BookIdK = 0;
                if (!string.IsNullOrEmpty(model.BookId))
                {
                    int.TryParse(_ICustomEncryption.base64d(model.BookId), out BookIdK);
                    EncBookIdK = BookIdK;
                }
                DateTime? FilteredToDate = null, FilteredFromDate = null;
                if (!string.IsNullOrEmpty(SearchToDate) || !string.IsNullOrWhiteSpace(SearchToDate))
                {
                    FilteredToDate = DateTime.ParseExact(SearchToDate, "dd/MM/yyyy", null);
                    FilteredToDate = FilteredToDate.Value.Date;
                }
                if (!string.IsNullOrEmpty(SearchFromDate) || !string.IsNullOrWhiteSpace(SearchFromDate))
                {
                    FilteredFromDate = DateTime.ParseExact(SearchFromDate, "dd/MM/yyyy", null);
                    FilteredFromDate = FilteredFromDate.Value.Date;
                }
                if (model.SearchDateId == 0)
                {
                    model.SearchDateId = null;
                }

                var BooksList = _IBookMasterService.GetAllIssuedBooks(ref count, BookId: EncBookIdK,
                                         DateFilterId: model.SearchDateId, ToDateFilter: FilteredToDate,
                                         SearchText: model.SearchText == null ? "" : model.SearchText,
                                         FromDateFilter: FilteredFromDate)
                                        .Where(x => x.ActualReturnDate == null).ToList();
                if (model.SessionId != null && model.SessionId > 0)
                {
                    var SelectedSession = _ICatalogMasterService.GetSessionById((int)model.SessionId);
                    if (SelectedSession != null)
                        BooksList = BooksList.Where(f => f.TransactionDate != null && f.TransactionDate >= SelectedSession.StartDate && f.TransactionDate <= SelectedSession.EndDate).ToList();
                }
                if (!string.IsNullOrEmpty(model.bkIssuedMemberTypeId) && model.bkIssuedMemberTypeId == "student")
                {
                    BooksList = BooksList.Where(f => f.BookMemberTypeId != null && f.BookMemberType.BookMemberType1.ToLower() == "student").ToList();
                    if (!string.IsNullOrEmpty(model.bKIssuedStudentId))
                    {
                        int studentid = 0;
                        if (!string.IsNullOrEmpty(model.bKIssuedStudentId))
                            int.TryParse(_ICustomEncryption.base64d(model.bKIssuedStudentId), out studentid);
                        if (studentid > 0)
                            BooksList = BooksList.Where(f => f.IssuedToId == studentid).ToList();
                    }
                    else if (model.bkIssuedClassId != null && model.bkIssuedClassId > 0)
                    {
                        var ClassById = _ICatalogMasterService.GetClassMasterById((int)model.bkIssuedClassId);
                        BooksList = BooksList.Where(f => f.StudentClass != null && ClassById.Class.ToLower() == f.StudentClass.ToLower()).ToList();
                    }
                }
                else if (!string.IsNullOrEmpty(model.bkIssuedMemberTypeId) && model.bkIssuedMemberTypeId == "staff")
                {
                    BooksList = BooksList.Where(f => f.BookMemberTypeId != null && f.BookMemberType.BookMemberType1.ToLower() == "staff").ToList();
                    if (model.bKIssuedStaffId > 0)
                    {
                        BooksList = BooksList.Where(f => f.IssuedToId == model.bKIssuedStaffId).ToList();
                    }
                }
                //if (!string.IsNullOrEmpty(model.bkIssuedMemberTypeId) && model.bkIssuedMemberTypeId == "student")
                //{
                //    BooksList = BooksList.Where(f => f.BookMemberTypeId != null && f.BookMemberType.BookMemberType1.ToLower() == "student").ToList();
                //    if (model.SessionId != null && model.SessionId > 0)
                //    {
                //        Session = _ICatalogMasterService.GetAllSessions(ref count, SessionId: (int)model.SessionId).FirstOrDefault();
                //        if (StudentIds != null && StudentIds.Count() > 0)
                //        {
                //            var InSession = _IStudentService.GetAllStudentClasss(ref count).Where(f => StudentIds.Contains(f.StudentId) && f.SessionId == model.SessionId).Select(f => f.StudentId).ToArray();
                //            if (InSession != null && InSession.Count() > 0)
                //            {
                //                var ExistInSession = InSession.Intersect(StudentIds).ToArray();
                //                if (ExistInSession != null && ExistInSession.Count() > 0)
                //                    BooksList = BooksList.Where(f => ExistInSession.Contains((int)f.IssuedToId)).ToList();
                //                else
                //                    BooksList.Clear();
                //            }
                //            else
                //            {
                //                BooksList.Clear();
                //            }
                //        }
                //    }
                //    if (!string.IsNullOrEmpty(model.bKIssuedStudentId))
                //    {
                //        int studentid = 0;
                //        if (!string.IsNullOrEmpty(model.bKIssuedStudentId))
                //            int.TryParse(_ICustomEncryption.base64d(model.bKIssuedStudentId), out studentid);
                //        if (studentid > 0)
                //            BooksList = BooksList.Where(f => f.IssuedToId == studentid).ToList();
                //    }
                //    else if (model.bkIssuedClassId != null && model.bkIssuedClassId > 0)
                //    {

                //        var StudentIdsByClass = _IStudentService.GetAllStudentClasss(ref count, ClassId: model.bkIssuedClassId).
                //            Where(m => m.SessionId == Session.SessionId).Select(f => f.StudentId).ToArray();
                //        if (StudentIdsByClass != null && StudentIdsByClass.Count() > 0)
                //            BooksList = BooksList.Where(f => StudentIdsByClass.Contains((int)f.IssuedToId)).ToList();
                //        else
                //            BooksList.Clear();
                //    }
                //}
                //else if(!string.IsNullOrEmpty(model.bkIssuedMemberTypeId) && model.bkIssuedMemberTypeId == "staff")
                //{
                //    BooksList = BooksList.Where(f => f.BookMemberTypeId != null && f.BookMemberType.BookMemberType1.ToLower() == "staff").ToList();
                //    if (model.bKIssuedStaffId > 0)
                //    {
                //        BooksList = BooksList.Where(f => f.IssuedToId == model.bKIssuedStaffId).ToList();
                //    }
                //}

                var CrrntSession = _ICatalogMasterService.GetAllSessions(ref count, Isdeafult: true).FirstOrDefault();
                var AllStudentClasses = _IStudentService.GetAllStudentClasss(ref count, CrrntSession.SessionId);
                var AllSatffs = _IStaffService.GetAllStaffs(ref count);
                var AllBookIdentity = _IBookMasterService.GetAllBookIdentities(ref count);

                var InActiveBookStatus = _IBookMasterService.GetAllBookStatuss(ref count).Where(f=>f.IsInactive==true);
                var gridModel = new DataSourceResult
                {
                    Data = BooksList.OrderByDescending(x => x.ExpectedReturnDate).Select(p =>
                    {
                        var BookInfolist = new BookTransactionViewModel();
                        BookInfolist.BookTransactionId = _ICustomEncryption.base64e(p.BookTransactionId.ToString());
                        BookInfolist.TransactionDate = p.TransactionDate.Value.Date.ToString("dd/MM/yyyy");
                        BookInfolist.BookMemberTypeId = _ICustomEncryption.base64e(p.BookMemberTypeId.ToString());
                        BookInfolist.BookMemberType = p.BookMemberType.BookMemberType1;
                        BookInfolist.IssuedToId = _ICustomEncryption.base64e(p.IssuedToId.ToString());
                        BookInfolist.BookMemberTypeId = _ICustomEncryption.base64e(p.BookMemberTypeId.ToString());



                        switch (BookInfolist.BookMemberType)
                        {
                            case "Student":
                                var getMembers = AllStudentClasses.Where(f => f.StudentId == p.IssuedToId).FirstOrDefault();
                                if (getMembers != null)
                                {

                                    string RollNo = "";
                                    string Class = "";
                                    if (!string.IsNullOrEmpty(getMembers.RollNo))
                                        RollNo = " (" + getMembers.RollNo + ")";
                                    if (!string.IsNullOrEmpty(getMembers.ClassMaster.Class))
                                        Class = " (- " + getMembers.ClassMaster.Class + ")";

                                    BookInfolist.IssuedToUserName = (getMembers.Student.FName + " " + getMembers.Student.LName)
                                                                    + RollNo + Class;
                                }
                                else
                                {
                                    var getstudent = _IStudentService.GetStudentById((int)p.IssuedToId);
                                    if (getstudent != null)
                                    {
                                        BookInfolist.IssuedToUserName = getstudent.FName + " " + getstudent.LName;
                                    }
                                }

                                break;
                            case "Staff":
                                var getuserStaffInfo = AllSatffs.Where(f => f.StaffId == (int)p.IssuedToId).FirstOrDefault();
                                if (getuserStaffInfo != null)
                                {
                                    string EmpCode = "";
                                    if (!string.IsNullOrEmpty(getuserStaffInfo.EmpCode))
                                        EmpCode = " (" + getuserStaffInfo.EmpCode + ")";

                                    BookInfolist.IssuedToUserName = (getuserStaffInfo.FName + " " + getuserStaffInfo.LName)
                                                                     + EmpCode;
                                }
                                break;
                        }

                        BookInfolist.ExpectedReturnDate = p.ExpectedReturnDate.Value.Date.ToString("dd/MM/yyyy");
                        BookInfolist.ActualReturnDate = p.ActualReturnDate == null ? "" : p.ActualReturnDate.Value.Date.ToString("dd/MM/yyyy");

                        //if (p.IsLost == true)
                        //    BookInfolist.IsLost = p.IsLost;
                       if (p.BookIdentityId != null && p.BookIdentity.BookStatusId!=null) {
                            var bkstsats = InActiveBookStatus.Where(f => f.StatusCode == p.BookIdentity.BookStatusId).FirstOrDefault();
                            if (bkstsats != null)
                                BookInfolist.IsInActiveBook = true;
                            else
                                BookInfolist.IsInActiveBook = false;
                        }
                        else
                            BookInfolist.IsInActiveBook = false;



                        BookInfolist.Fine = p.Fine.ToString();
                        BookInfolist.PaidAmount = p.PaidAmount == null ? "" : p.PaidAmount.ToString();
                        BookInfolist.PaidOn = p.PaidOn == null ? "" : p.PaidOn.Value.Date.ToString("dd/MM/yyyy");
                        BookInfolist.BookIdentityId = _ICustomEncryption.base64e(p.BookIdentityId.ToString());
                        BookInfolist.BookId = _ICustomEncryption.base64e(p.BookIdentity.BookId.ToString());
                        BookInfolist.BookTitle = p.BookIdentity.BookMaster.BookTitle;

                        var luiAcessionNo = "";
                        var getUniqueId = AllBookIdentity.Where(f => f.BookIdentityId == p.BookIdentityId).FirstOrDefault();
                        if (getUniqueId != null)
                        {
                            if (!string.IsNullOrEmpty(getUniqueId.LUI))
                                luiAcessionNo = "-" + getUniqueId.LUI.ToString();
                            BookInfolist.UniqueId = getUniqueId.UniqueId + luiAcessionNo;
                        }
                        BookInfolist.AllowedDays = Convert.ToString(p.BookMemberType.AllowedDays == null ? AllowedDaysForBook : p.BookMemberType.AllowedDays);
                        BookInfolist.FinePerDay = Convert.ToString(p.BookMemberType.FinePerDay == null ? FinePerDay : p.BookMemberType.FinePerDay);

                        BookInfolist.setColor = false;
                        if (BookInfolist.ActualReturnDate == "")
                        {
                            BookInfolist.setColor = true;
                        }
                        BookInfolist.IsAuthToDelete = false;

                        if (BookInfolist.TransactionDate == DateTime.Today.Date.ToString("dd/MM/yyyy"))
                        {
                            BookInfolist.IsAuthToDelete = true;
                        }

                        return BookInfolist;
                    }).AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = BooksList.Count()
                };
                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult CopiesWithIssuedBookList(DataSourceRequest command, BookTransactionViewModel model, string BookId,
            IEnumerable<Sort> sort = null, string SearchToDate = "", string SearchFromDate = "")
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int? EncBookIdK = null;
                int BookIdK = 0;
                if (!string.IsNullOrEmpty(model.BookId))
                {
                    int.TryParse(_ICustomEncryption.base64d(model.BookId), out BookIdK);
                    EncBookIdK = BookIdK;
                }
                DateTime? FilteredToDate = null, FilteredFromDate = null;
                IEnumerable<BookTransaction> BooksList = new List<BookTransaction>();
                var Session = _ICatalogMasterService.GetAllSessions(ref count, Isdeafult: true).FirstOrDefault();
                if (BookIdK > 0)
                {
                    if (!string.IsNullOrEmpty(SearchToDate) || !string.IsNullOrWhiteSpace(SearchToDate))
                    {
                        FilteredToDate = DateTime.ParseExact(SearchToDate, "dd/MM/yyyy", null);
                        FilteredToDate = FilteredToDate.Value.Date;
                    }
                    if (!string.IsNullOrEmpty(SearchFromDate) || !string.IsNullOrWhiteSpace(SearchFromDate))
                    {
                        FilteredFromDate = DateTime.ParseExact(SearchFromDate, "dd/MM/yyyy", null);
                        FilteredFromDate = FilteredFromDate.Value.Date;
                    }
                    if (model.SearchDateId == 0)
                    {
                        model.SearchDateId = null;
                    }

                    BooksList = _IBookMasterService.GetAllIssuedBooks(ref count, BookId: EncBookIdK,
                                             DateFilterId: model.SearchDateId, ToDateFilter: FilteredToDate,
                                             SearchText: model.SearchText == null ? "" : model.SearchText,
                                             FromDateFilter: FilteredFromDate)
                                            .Where(x => x.ActualReturnDate == null);

                    var StudentIds = BooksList.Where(f => f.BookMemberType.BookMemberType1.ToLower() == "student").Select(f => f.IssuedToId).ToArray();

                    if (model.SessionId != null && model.SessionId > 0)
                    {
                        Session = _ICatalogMasterService.GetAllSessions(ref count, SessionId: (int)model.SessionId).FirstOrDefault();
                        if (StudentIds != null && StudentIds.Count() > 0)
                        {
                            var InSession = _IStudentService.GetAllStudentClasss(ref count).Where(f => StudentIds.Contains(f.StudentId) && f.SessionId == model.SessionId).Select(f => f.StudentId).ToArray();
                            if (InSession != null && InSession.Count() > 0)
                            {
                                var ExistInSession = InSession.Intersect(StudentIds).ToArray();
                                if (ExistInSession != null && ExistInSession.Count() > 0)
                                    BooksList = BooksList.Where(f => ExistInSession.Contains((int)f.IssuedToId));
                            }
                        }
                    }
                }
                var InActiveBookStatus = _IBookMasterService.GetAllBookStatuss(ref count).Where(f => f.IsInactive == true);
                var gridModel = new DataSourceResult
                {
                    Data = BooksList.OrderByDescending(x => x.ExpectedReturnDate).Select(p =>
                    {
                        var BookInfolist = new BookTransactionViewModel();
                        BookInfolist.BookTransactionId = _ICustomEncryption.base64e(p.BookTransactionId.ToString());
                        BookInfolist.TransactionDate = p.TransactionDate.Value.Date.ToString("dd/MM/yyyy");
                        BookInfolist.BookMemberTypeId = _ICustomEncryption.base64e(p.BookMemberTypeId.ToString());
                        BookInfolist.BookMemberType = p.BookMemberType.BookMemberType1;
                        BookInfolist.IssuedToId = _ICustomEncryption.base64e(p.IssuedToId.ToString());
                        BookInfolist.BookMemberTypeId = _ICustomEncryption.base64e(p.BookMemberTypeId.ToString());



                        switch (BookInfolist.BookMemberType)
                        {
                            case "Student":
                                var getMembers = _IStudentService.GetAllStudentClasss(ref count, Session.SessionId, StudentId: p.IssuedToId).FirstOrDefault();
                                if (getMembers != null)
                                {

                                    string RollNo = "";
                                    string Class = "";
                                    if (!string.IsNullOrEmpty(getMembers.RollNo))
                                        RollNo = " (" + getMembers.RollNo + ")";
                                    if (!string.IsNullOrEmpty(getMembers.ClassMaster.Class))
                                        Class = " (- " + getMembers.ClassMaster.Class + ")";

                                    BookInfolist.IssuedToUserName = (getMembers.Student.FName + " " + getMembers.Student.LName)
                                                                    + RollNo + Class;
                                }
                                else
                                {
                                    var getstudent = _IStudentService.GetStudentById((int)p.IssuedToId);
                                    if (getstudent != null)
                                    {
                                        BookInfolist.IssuedToUserName = getstudent.FName + " " + getstudent.LName;
                                    }
                                }

                                break;
                            case "Staff":
                                var getuserStaffInfo = _IStaffService.GetStaffById(StaffId: (int)p.IssuedToId);
                                if (getuserStaffInfo != null)
                                {
                                    string EmpCode = "";
                                    if (!string.IsNullOrEmpty(getuserStaffInfo.EmpCode))
                                        EmpCode = " (" + getuserStaffInfo.EmpCode + ")";

                                    BookInfolist.IssuedToUserName = (getuserStaffInfo.FName + " " + getuserStaffInfo.LName)
                                                                     + EmpCode;
                                }
                                break;
                        }

                        BookInfolist.ExpectedReturnDate = p.ExpectedReturnDate.Value.Date.ToString("dd/MM/yyyy");
                        BookInfolist.ActualReturnDate = p.ActualReturnDate == null ? "" : p.ActualReturnDate.Value.Date.ToString("dd/MM/yyyy");

                        //if (p.IsLost == true)
                        //    BookInfolist.IsLost = p.IsLost;
                        //else
                        //    BookInfolist.IsLost = false;
                        if (p.BookIdentityId != null && p.BookIdentity.BookStatusId != null)
                        {
                            var bkstsats = InActiveBookStatus.Where(f => f.StatusCode == p.BookIdentity.BookStatusId).FirstOrDefault();
                            if (bkstsats != null)
                                BookInfolist.IsInActiveBook = true;
                            else
                                BookInfolist.IsInActiveBook = false;
                        }
                        else
                            BookInfolist.IsInActiveBook = false;



                        BookInfolist.Fine = p.Fine.ToString();
                        BookInfolist.PaidAmount = p.PaidAmount == null ? "" : p.PaidAmount.ToString();
                        BookInfolist.PaidOn = p.PaidOn == null ? "" : p.PaidOn.Value.Date.ToString("dd/MM/yyyy");
                        BookInfolist.BookIdentityId = _ICustomEncryption.base64e(p.BookIdentityId.ToString());
                        BookInfolist.BookId = _ICustomEncryption.base64e(p.BookIdentity.BookId.ToString());
                        BookInfolist.BookTitle = p.BookIdentity.BookMaster.BookTitle;

                        var getUniqueId = _IBookMasterService.GetBookIdentityById(BookIdentityId: p.BookIdentityId);
                        if (getUniqueId != null && getUniqueId.UniqueId != null)
                        {
                            BookInfolist.UniqueId = getUniqueId.UniqueId;
                            if (!string.IsNullOrEmpty(getUniqueId.LUI))
                                BookInfolist.UniqueId = getUniqueId.UniqueId + "-" + getUniqueId.LUI;
                        }
                        BookInfolist.AllowedDays = Convert.ToString(p.BookMemberType.AllowedDays == null ? AllowedDaysForBook : p.BookMemberType.AllowedDays);
                        BookInfolist.FinePerDay = Convert.ToString(p.BookMemberType.FinePerDay == null ? FinePerDay : p.BookMemberType.FinePerDay);

                        BookInfolist.setColor = false;
                        if (BookInfolist.ActualReturnDate == "")
                        {
                            BookInfolist.setColor = true;
                        }
                        BookInfolist.IsAuthToDelete = false;

                        if (BookInfolist.TransactionDate == DateTime.Today.Date.ToString("dd/MM/yyyy"))
                        {
                            BookInfolist.IsAuthToDelete = true;
                        }

                        return BookInfolist;
                    }).AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = BooksList.Count()
                };
                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult GetBookStock(string BookId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                int BookIdK = 0;
                if (!string.IsNullOrEmpty(BookId))
                    int.TryParse(_ICustomEncryption.base64d(BookId), out BookIdK);

                List<BookAdditionalInfoViewModel> paramBookStockInfo = new List<BookAdditionalInfoViewModel>();

                var BooksList = _IBookMasterService.GetAllBookMastersByLinq(ref count, BookId: BookIdK).FirstOrDefault();
                paramBookStockInfo.Add(new BookAdditionalInfoViewModel
                {
                    TotalCopies = BooksList.TotalCopies == "0" ? "0" : BooksList.TotalCopies,
                    TotalIssuedBooks = BooksList.TotalIssuedBooks == "0" ? "0" : BooksList.TotalIssuedBooks,
                    AvaiableCopies = BooksList.AvaiableCopies == "0" ? "0" : BooksList.AvaiableCopies
                });
                return Json(new { paramBookStockInfo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region BookTransaction

        public BookTransactionViewModel PrepareBookTransactionModel(BookTransactionViewModel model, string BookId = "")
        {
            //paging
            model.gridPageSizes = _ISchoolSettingService.GetorSetSchoolData("gridPageSizes", "20,50,100,250").AttributeValue;
            model.defaultGridPageSize = _ISchoolSettingService.GetorSetSchoolData("defaultGridPageSize", "20").AttributeValue;

            //MaterialType
            var getMaterialType = _IBookMasterService.GetAllBookMaterialTypes(ref count);
            if (getMaterialType.Count() > 0)
            {
                model.MaterialTypes.Add(new SelectListItem { Text = "--Select--", Value = "" });
                foreach (var type in getMaterialType)
                {
                    model.MaterialTypes.Add(new SelectListItem
                    {
                        Text = type.MaterialType,
                        Value = _ICustomEncryption.base64e(type.MaterialTypeId.ToString())
                    });
                }
            }
            //
            model.AvailbleSessions = _IDropDownService.SessionList();
            model.CurrentSessionId = _ICatalogMasterService.GetAllSessions(ref count, Isdeafult: true).FirstOrDefault() == null ? 0 : _ICatalogMasterService.GetAllSessions(ref count, Isdeafult: true).FirstOrDefault().SessionId;
            var getAttrs = _IBookMasterService.GetAllBookAttributes(ref count);
            if (getAttrs.Count() > 0)
            {
                BookAttributeValueModel AttrValues = new BookAttributeValueModel();
                foreach (var attr in getAttrs)
                {
                    AttrValues = new BookAttributeValueModel();
                    AttrValues.BookAttributeId = _ICustomEncryption.base64e(attr.BookAttributeId.ToString());
                    AttrValues.BookAttributeName = attr.Attribute;

                    model.BookAttributes.Add(AttrValues);
                }
            }

            var getMasterAttrs = _IBookMasterService.GetAllBookMasterAttributes(ref count, IsActive: true);
            var getAllMasterData = _IBookMasterService.GetAllBookData(ref count, IsActive: true);
            BookData Data = new BookData();
            BookData Data2 = new BookData();
            foreach (var MasterAttr in getMasterAttrs)
            {
                Data = new BookData();
                var getMasterData = getAllMasterData.Where(x => x.BookMasterAttributeId == MasterAttr.BookMasterAttributeId);
                if (getMasterData.Count() > 0)
                {
                    foreach (var dta in getMasterData)
                    {
                        Data2 = new BookData();
                        Data2.BookMasterDataId = _ICustomEncryption.base64e(dta.BookMasterDataId.ToString());
                        Data2.AttributeValue = dta.AttributeValue;

                        Data.BookDatas.Add(Data2);
                    }
                }
                Data.BookMasterAttributeId = _ICustomEncryption.base64e(MasterAttr.BookMasterAttributeId.ToString());
                Data.BookMasterAttributeName = MasterAttr.MasterAttribute;
                model.BookMasterAttributes.Add(Data);
            }

            if (!string.IsNullOrEmpty(BookId) || !string.IsNullOrWhiteSpace(BookId))
            {
                // decrypt values
                int BookIdK = 0;
                if (!string.IsNullOrEmpty(BookId))
                    int.TryParse(_ICustomEncryption.base64d(BookId), out BookIdK);

                model.BookId = BookId;
                var getTitle = _IBookMasterService.GetBookMasterById(BookIdK);
                model.BookTitle = getTitle.BookTitle;
                model.Description = getTitle.Description;
            }

            var getMembertypes = _IBookMasterService.GetAllBookMemberTypes(ref count, IsActive: true);
            model.AviableMembers.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
            if (getMembertypes.Count() > 0)
            {
                foreach (var member in getMembertypes)
                {
                    model.AviableMembers.Add(new SelectListItem
                    {
                        Text = member.BookMemberType1,
                        Value = _ICustomEncryption.base64e(member.BookMemberTypeId.ToString()),
                        Selected = false
                    });
                }
            }
            model.AvailbleBookStatus.Add(new SelectListItem { Text = "--All--", Value = "", Selected = true });
            model.AvailbleBookStatus.Add(new SelectListItem { Text = "Available", Value = "available", Selected = false });
            model.AvailbleBookStatus.Add(new SelectListItem { Text = "Lost", Value = "lost", Selected = false });
            model.AvailbleBookStatus.Add(new SelectListItem { Text = "Due For Return", Value = "dueforreturn", Selected = false });
            model.AvailbleBookStatus.Add(new SelectListItem { Text = "Issued", Value = "issued", Selected = false });
            model.AvailbleBookStatus.Add(new SelectListItem { Text = "Returned", Value = "Returned", Selected = false });
            model.AvailbleBookStatus.Add(new SelectListItem { Text = "Pending Fine", Value = "pendingfine", Selected = false });
            model.AvailbleBookStatus.Add(new SelectListItem { Text = "Late Returned", Value = "latereturned", Selected = false });
            var getFutureReturnDate = _ISchoolSettingService.GetorSetSchoolData("AllowedFutureReturnDate", "False");
            model.SetFutureReturnCalendarDate = getFutureReturnDate.AttributeValue;

            var languages = _ICatalogMasterService.GetAllLanguages(ref count);
            var DefaultLang = languages.Where(f => f.IsActive == true).FirstOrDefault();
            if (DefaultLang != null)
            {
                model.DefaultLanguage = DefaultLang.Language1;
                model.DefaultLanguageAbbr = DefaultLang.LanguageAbbr.ToLower();
            }
            foreach (var lang in languages)
            {
                if (model.DefaultLanguageAbbr != lang.LanguageAbbr.ToLower())
                {
                    if (model.DestinationLanguage == "" || model.DestinationLanguage == null)
                        model.DestinationLanguage = lang.LanguageAbbr.ToLower();
                    else
                        model.DestinationLanguage += "," + lang.LanguageAbbr.ToLower();
                }
            }

            IList<ClassMaster> IssuedClasses = new List<ClassMaster>();
            IList<ClassMaster> Classes = new List<ClassMaster>();
            IList<Staff> IssuedStaff = new List<Staff>();
            IList<Staff> StaffList = new List<Staff>();
            var CurrSession = _ICatalogMasterService.GetAllSessions(ref count).Where(f => f.IsDefualt == true).FirstOrDefault();
            var CurrentSession = CurrSession.SessionId;
            var AllBookMemberType = _IBookMasterService.GetAllBookMemberTypes(ref count);
            var BookMemberType = AllBookMemberType.Where(f => f.BookMemberType1 == "Student").FirstOrDefault();
            var StaffBookMember = AllBookMemberType.Where(f => f.BookMemberType1 == "Staff").FirstOrDefault();
            var AllStudnetClasses = _IStudentService.GetAllStudentClasss(ref count).Where(f => f.SessionId == CurrentSession);
            var CurrentSessionStudentIds = AllStudnetClasses.Select(f => f.StudentId).ToArray();
            var AllBookTransaction = _IBookMasterService.GetAllBookTransactions(ref count);
            if (CurrSession != null)
                AllBookTransaction = AllBookTransaction.Where(f => f.TransactionDate >= CurrSession.StartDate && f.TransactionDate <= CurrSession.EndDate).ToList();
            if (CurrentSessionStudentIds != null && CurrentSessionStudentIds.Count() > 0)
            {
                var TransactionStudentIds = AllBookTransaction.
                    Where(f => f.BookMemberTypeId == BookMemberType.BookMemberTypeId && CurrentSessionStudentIds.Contains(f.IssuedToId)).Select(f => f.IssuedToId).Distinct().ToArray();
                if (TransactionStudentIds != null)
                {
                    var StudentClasIds = AllStudnetClasses.Where(f => TransactionStudentIds.Contains(f.StudentId)).Select(f => f.ClassId).Distinct().ToArray();
                    if (StudentClasIds != null && StudentClasIds.Count() > 0)
                    {
                        Classes = _ICatalogMasterService.GetAllClassMasters(ref count).Where(f => StudentClasIds.Contains(f.ClassId)).ToList();
                    }
                }
            }
            model.AvailableClasses.Add(new SelectListItem { Selected = true, Value = "", Text = "--Select Class--" });
            foreach (var cls in Classes)
            {
                model.AvailableClasses.Add(new SelectListItem { Value = cls.ClassId.ToString(), Text = cls.Class });
            }
            var issuedBooksList = _IBookMasterService.GetAllIssuedBooks(ref count).Where(x => x.ActualReturnDate == null).ToList();
            if (issuedBooksList.Count > 0)
            {
                var IssuedStdIds = issuedBooksList.Where(f => f.BookMemberTypeId != null && f.BookMemberType.BookMemberType1.ToLower() == "student").Select(f => f.IssuedToId).Distinct().ToArray();
                if (IssuedStdIds != null && IssuedStdIds.Count() > 0)
                {
                    var StudentClasIds = AllStudnetClasses.Where(f => IssuedStdIds.Contains(f.StudentId)).Select(f => f.ClassId).Distinct().ToArray();
                    if (StudentClasIds != null && StudentClasIds.Count() > 0)
                        IssuedClasses = _ICatalogMasterService.GetAllClassMasters(ref count).Where(f => StudentClasIds.Contains(f.ClassId)).ToList();
                }
                var IssuedStffIds = issuedBooksList.Where(f => f.BookMemberTypeId != null && f.BookMemberType.BookMemberType1.ToLower() == "staff").Select(f => f.IssuedToId).Distinct().ToArray();
                if (IssuedStffIds != null && IssuedStffIds.Count() > 0)
                {
                    IssuedStaff = _IStaffService.GetAllStaffs(ref count).Where(f => IssuedStffIds.Contains(f.StaffId)).ToList();
                }
            }
            model.AvailableIssuedClasses.Add(new SelectListItem { Selected = true, Value = "", Text = "--Select Class--" });
            foreach (var cls in IssuedClasses)
            {
                model.AvailableIssuedClasses.Add(new SelectListItem { Value = cls.ClassId.ToString(), Text = cls.Class });
            }

            var StaffTransactionIds = AllBookTransaction.Where(f => f.BookMemberTypeId == StaffBookMember.BookMemberTypeId).Select(f => f.IssuedToId).Distinct().ToArray();
            if (StaffTransactionIds != null && StaffTransactionIds.Count() > 0)
                StaffList = _IStaffService.GetAllStaffs(ref count).Where(f => StaffTransactionIds.Contains(f.StaffId)).ToList();
            model.AvailableStaff.Add(new SelectListItem { Selected = true, Value = "", Text = "--Select Staff--" });
            foreach (var stf in StaffList)
            {
                var name = stf.FName;
                if (!string.IsNullOrEmpty(stf.LName))
                    name = name + " " + stf.LName;
                if (!string.IsNullOrEmpty(stf.EmpCode))
                    name = name + "(" + stf.EmpCode + ")";
                model.AvailableStaff.Add(new SelectListItem { Selected = false, Value = stf.StaffId.ToString(), Text = name });
            }
            foreach (var stf in IssuedStaff)
            {
                var name = stf.FName;
                if (!string.IsNullOrEmpty(stf.LName))
                    name = name + " " + stf.LName;
                if (!string.IsNullOrEmpty(stf.EmpCode))
                    name = name + "(" + stf.EmpCode + ")";
                model.AvailablIssuedeStaff.Add(new SelectListItem { Selected = false, Value = stf.StaffId.ToString(), Text = name });
            }

            return model;
        }

        public BookTransactionViewModel PrepareBookInformation(BookTransactionViewModel model, string BookId = "")
        {
            //paging
            model = PrepareBookTransactionModel(model, BookId);

            return model;
        }

        public ActionResult ManageBookTransactions()
        {
            SchoolUser user = new SchoolUser();
            var model = new BookTransactionViewModel();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookTransactions", "View");
                    if (!isauth)
                        return RedirectToAction("AccessDenied", "Error");
                }
                model = PrepareBookTransactionModel(model);

                //authentication
                model.IsAuthToAddTransaction = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookTransactions", "Detail");
                model.IsAuthToIssue = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookTransactions", "Issue Copies");
                model.IsAuthToViewIssued = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookTransactions", "View Details");
                model.IsAuthToViewRecord = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookTransactions", "View Record");
                model.IsAuthToReturn = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookTransactions", "Update Record");
                model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Library", "ManageBookTransactions", "Delete Record");

                model.IsAuthToEditFine = _ISchoolSettingService.GetorSetSchoolData("IsLateFineEditableInBookReturn", "1").AttributeValue == "1" ? true : false;
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return View(model);
            }
        }

        public ActionResult SearchAllBooksData(DataSourceRequest command, BookTransactionViewModel model
                                               , IEnumerable<Sort> sort = null)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int MaterialTypeIdK = 0;
                int? encMaterialTypeId = null;
                if (!string.IsNullOrEmpty(model.MaterialTypeId))
                {
                    int.TryParse(_ICustomEncryption.base64d(model.MaterialTypeId), out MaterialTypeIdK);
                    encMaterialTypeId = MaterialTypeIdK;
                }

                if (model.ArrayMasterAttr != null || !string.IsNullOrEmpty(model.ArrayLocationMasterAttr))
                {
                    System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                    IList<BookMasterViewModel> objAttrArray = js.Deserialize<List<BookMasterViewModel>>(model.ArrayMasterAttr);

                    string AttrbuteValues = "";
                    string Attrbutes = "";

                    if (objAttrArray.Count() > 0)
                    {
                        foreach (var item in objAttrArray)
                        {
                            if (!string.IsNullOrEmpty(item.BookMasterDataId))
                            {
                                // decrypt values
                                int BookMasterDataIdIdK = 0;
                                if (!string.IsNullOrEmpty(item.BookMasterDataId))
                                    int.TryParse(_ICustomEncryption.base64d(item.BookMasterDataId), out BookMasterDataIdIdK);

                                AttrbuteValues += BookMasterDataIdIdK + ",";
                            }

                            if (!string.IsNullOrEmpty(item.BookAttributeValue))
                            {
                                Attrbutes += item.BookAttributeValue + ',';
                            }
                        }
                        AttrbuteValues = AttrbuteValues.TrimEnd(',');
                        Attrbutes = Attrbutes.TrimEnd(',');

                    }
                    var getBookMaterial = _IBookMasterService.GetBookMaterialTypeById(MaterialTypeId: encMaterialTypeId);
                    var BookCopies = _IBookMasterService.GetAllBookIdentities(ref count).ToList();
                    var BookLocationDetails = _IBookMasterService.GetAllBookLocationDetails(ref count).ToList();
                    string MaterialType = "";
                    if (getBookMaterial != null)
                    {
                        MaterialType = getBookMaterial.MaterialType;
                    }
                    var AdvancedBookList = _IBookMasterService.AvancedSearchBookMasters(ref count, ISBN: model.ISBN, AttributeValues: AttrbuteValues
                                                                            , Attribute: Attrbutes, MaterialType: MaterialType
                                                                            , SubTitle: model.SubTitle,AdvancedSearch:model.SearchText);
                    if (!string.IsNullOrEmpty(model.bkTransactionAccessionLui))
                    {
                        var AccessionCopiesBookIds = BookCopies.Where(f=> f.LUI!=null && f.LUI.ToLower()== model.bkTransactionAccessionLui.ToLower()).Where(f => f.BookStatusId == null).Select(f => f.BookId).Distinct().ToArray();
                        if (AccessionCopiesBookIds != null && AccessionCopiesBookIds.Count() > 0)
                            AdvancedBookList = AdvancedBookList.Where(f => AccessionCopiesBookIds.Contains(f.BookId)).ToList();
                        else
                            AdvancedBookList.Clear();
                    }
                    if (!string.IsNullOrEmpty(model.ArrayLocationMasterAttr))
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer jsq = new System.Web.Script.Serialization.JavaScriptSerializer();
                        IList<BookLocationDataModel> objLocationAttrArray = jsq.Deserialize<List<BookLocationDataModel>>(model.ArrayLocationMasterAttr);
                        if (objLocationAttrArray.Count > 0)
                        {
                            objLocationAttrArray = objLocationAttrArray.Where(f => !string.IsNullOrEmpty(f.BookLocationAttributeId) && !string.IsNullOrEmpty(f.BookLocationDataId)).ToList();
                            foreach (var item in objLocationAttrArray)
                            {
                                var bookCopiesIdsByBookLocationDataId = BookLocationDetails.Where(f => f.BookLocationDataId == Convert.ToInt32(item.BookLocationDataId)).Select(f => f.BookIdentityId).ToArray();
                                if (bookCopiesIdsByBookLocationDataId != null && bookCopiesIdsByBookLocationDataId.Count() > 0)
                                {
                                    var bookidsbycopiesids = BookCopies.Where(f => bookCopiesIdsByBookLocationDataId.Contains(f.BookIdentityId)).Select(f => f.BookId).Distinct().ToArray();
                                    if (bookidsbycopiesids != null && bookidsbycopiesids.Count() > 0)
                                        AdvancedBookList = AdvancedBookList.Where(f => bookidsbycopiesids.Contains(f.BookId)).ToList();
                                    else
                                        AdvancedBookList.Clear();
                                }
                                else
                                    AdvancedBookList.Clear();
                            }
                        }
                    }
                    var Data = AdvancedBookList.PagedForCommand(command).Select(p =>
                    {
                        var BookInfolist = new BookDefaulterListModel();
                        BookInfolist.BookId = _ICustomEncryption.base64e(p.BookId.ToString());
                        BookInfolist.BookTitle = p.BookTitle;
                        BookInfolist.ISBN = p.ISBN == null ? string.Empty : p.ISBN;
                        BookInfolist.IsAuthToEdit = true;
                        string Description = "";
                        if (p.Description != null)
                        {
                            Description = p.Description;
                            if (p.Description.Length > 150)
                            {
                                Description = p.Description.Substring(0, 150) + " ...";
                            }
                        }
                        BookInfolist.Description = Description;
                        BookInfolist.MaterialTypeId = _ICustomEncryption.base64e(p.MaterialTypeId.ToString());
                        BookInfolist.MaterialType = p.MaterialType;
                        BookInfolist.AvaiableCopies = p.AvialableCopies == "0" ? "" : p.AvialableCopies;
                        BookInfolist.strAccessionNo = !string.IsNullOrEmpty(p.AccessionNo) ? p.AccessionNo : "";
                        BookInfolist.IsAuthToDelete = false;

                        if (BookInfolist.AvaiableCopies == "")
                            BookInfolist.IsAuthToEdit = false;

                        var totalcount = BookCopies.Where(f => f.BookId == p.BookId && f.BookStatusId == null).Count();
                        BookInfolist.TotalCopies = totalcount == 0 ? "" : totalcount.ToString();

                        return BookInfolist;
                    }).AsQueryable().Sort(sort).ToList();
                   var AvailableCount = Data.Where(f => f.AvaiableCopies != "" && f.AvaiableCopies != "0" && f.AvaiableCopies != "null").Sum(f => Convert.ToInt32(f.AvaiableCopies)).ToString();
                   var TotalCount= Data.Where(f => f.TotalCopies != "" && f.TotalCopies != "0" && f.TotalCopies != "null").Sum(f => Convert.ToInt32(f.TotalCopies)).ToString();

                    var gridModel = new DataSourceResult
                    {
                        Data = Data,
                        Total = AdvancedBookList.Count(),
                      Value1= AvailableCount,
                      Value2= TotalCount
                    };
                    
                    return Json(gridModel);
                }
                else
                {
                    var BooksList = _IBookMasterService.SeacrhAllBookMastersBySp(ref count, Searchtext: model.SearchText);
                    var AllBookMemberType = _IBookMasterService.GetAllBookMemberTypes(ref count);
                    var BookMemberTypeStudent = AllBookMemberType.Where(m => m.BookMemberType1.ToLower() == "student").FirstOrDefault();
                    var BookMemberTypeStaff = AllBookMemberType.Where(m => m.BookMemberType1.ToLower() == "staff").FirstOrDefault();
                    var BookTransAction = _IBookMasterService.GetAllBookTransactions(ref count);
                    var BookCopies = _IBookMasterService.GetAllBookIdentities(ref count).ToList();
                    var BookLocationDetails = _IBookMasterService.GetAllBookLocationDetails(ref count).ToList();
                    if (!string.IsNullOrEmpty(model.bkMemberTypeId) && model.bkMemberTypeId.ToLower() == "student")
                    {
                        if (!string.IsNullOrEmpty(model.bKTransactionStudentId))
                        {
                            int studentid = 0;
                            if (!string.IsNullOrEmpty(model.bKTransactionStudentId))
                                int.TryParse(_ICustomEncryption.base64d(model.bKTransactionStudentId), out studentid);

                            var AllBookTransactionsByStudnet = BookTransAction.Where(m => m.IssuedToId == studentid && m.BookMemberTypeId == BookMemberTypeStudent.BookMemberTypeId).Select(f => f.BookIdentity.BookId).ToArray();
                            //var BookIdsByStudent = BookTransAction.Where(f => AllBookTransactionsByStudnet.Contains(f.)).Select(f=>f.BookIdentity.BookId).Distinct().ToArray();
                            BooksList = BooksList.Where(f => AllBookTransactionsByStudnet.Contains(f.BookId)).ToList();
                        }
                        else if (model.bkTransactionClassId != null && model.bkTransactionClassId > 0)
                        {
                            var CurrentSession = _ICatalogMasterService.GetAllSessions(ref count).Where(f => f.IsDefualt == true).FirstOrDefault();
                            var StudentIdsByClass = _IStudentService.GetAllStudentClasss(ref count, ClassId: model.bkTransactionClassId).
                                Where(m => m.SessionId == CurrentSession.SessionId).Select(f => f.StudentId).ToArray();
                            var BookIdsByClassStudents = BookTransAction.
                                Where(f => StudentIdsByClass.Contains(f.IssuedToId) && f.BookMemberTypeId == BookMemberTypeStudent.BookMemberTypeId).Select(f => f.BookIdentity.BookId).ToArray();
                            BooksList = BooksList.Where(f => BookIdsByClassStudents.Contains(f.BookId)).ToList();
                        }
                        else
                        {
                            var AllBookTransactionsByStudnet = BookTransAction.Where(m => m.BookMemberTypeId == BookMemberTypeStudent.BookMemberTypeId).Select(f => f.BookIdentity.BookId).ToArray();
                            //var BookIdsByStudent = BookTransAction.Where(f => AllBookTransactionsByStudnet.Contains(f.)).Select(f=>f.BookIdentity.BookId).Distinct().ToArray();
                            if (AllBookTransactionsByStudnet != null && AllBookTransactionsByStudnet.Count() > 0)
                                BooksList = BooksList.Where(f => AllBookTransactionsByStudnet.Contains(f.BookId)).ToList();
                            else
                                BooksList.Clear();
                        }
                    }
                    if (!string.IsNullOrEmpty(model.bkMemberTypeId) && model.bkMemberTypeId.ToLower() == "staff")
                    {

                        if (model.bKTransactionStaffId > 0)
                        {
                            var BookTransactionStaffIds = BookTransAction.Where(f => f.BookMemberTypeId == BookMemberTypeStaff.BookMemberTypeId && f.IssuedToId == model.bKTransactionStaffId).Select(f => f.BookIdentity.BookId);
                            if (BookTransactionStaffIds != null && BookTransactionStaffIds.Count() > 0)
                                BooksList = BooksList.Where(f => BookTransactionStaffIds.Contains(f.BookId)).ToList();
                            else
                                BooksList.Clear();
                        }
                        else
                        {
                            var BookTransactionStaffIds = BookTransAction.Where(f => f.BookMemberTypeId == BookMemberTypeStaff.BookMemberTypeId).Select(f => f.BookIdentity.BookId);
                            if (BookTransactionStaffIds != null && BookTransactionStaffIds.Count() > 0)
                                BooksList = BooksList.Where(f => BookTransactionStaffIds.Contains(f.BookId)).ToList();
                            else
                                BooksList.Clear();
                        }
                    }
                    if (!string.IsNullOrEmpty(model.bkTransactionAccessionLui))
                    {
                        var AccessionCopiesBookIds = _IBookMasterService.GetAllBookIdentities(ref count, LUI: model.bkTransactionAccessionLui).Where(f => f.BookStatusId == null).Select(f => f.BookId).Distinct().ToArray();
                        if (AccessionCopiesBookIds != null && AccessionCopiesBookIds.Count() > 0)
                            BooksList = BooksList.Where(f => AccessionCopiesBookIds.Contains(f.BookId)).ToList();
                        else
                        {
                            BooksList.Clear();
                        }
                    }
                    if (!string.IsNullOrEmpty(model.ArrayLocationMasterAttr))
                    {
                            System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                            IList<BookLocationDataModel> objLocationAttrArray = js.Deserialize<List<BookLocationDataModel>>(model.ArrayLocationMasterAttr);
                            if (objLocationAttrArray.Count > 0)
                            {
                                objLocationAttrArray = objLocationAttrArray.Where(f => !string.IsNullOrEmpty(f.BookLocationAttributeId) && !string.IsNullOrEmpty(f.BookLocationDataId)).ToList();
                                foreach (var item in objLocationAttrArray)
                                {
                                    var bookCopiesIdsByBookLocationDataId = BookLocationDetails.Where(f => f.BookLocationDataId == Convert.ToInt32(item.BookLocationDataId)).Select(f => f.BookIdentityId).ToArray();
                                if (bookCopiesIdsByBookLocationDataId != null && bookCopiesIdsByBookLocationDataId.Count() > 0)
                                {
                                    var bookidsbycopiesids = BookCopies.Where(f => bookCopiesIdsByBookLocationDataId.Contains(f.BookIdentityId)).Select(f => f.BookId).Distinct().ToArray();
                                    if (bookidsbycopiesids != null && bookidsbycopiesids.Count() > 0)
                                        BooksList = BooksList.Where(f => bookidsbycopiesids.Contains(f.BookId)).ToList();
                                    else
                                        BooksList.Clear();
                                }
                                else
                                    BooksList.Clear();
                                
                                }

                            }
                    }

                    var Data = BooksList.PagedForCommand(command).Select(p =>
                    {
                        var BookInfolist = new BookDefaulterListModel();
                        BookInfolist.BookId = _ICustomEncryption.base64e(p.BookId.ToString());
                        BookInfolist.BookTitle = p.BookTitle;
                        BookInfolist.ISBN = p.ISBN == null ? string.Empty : p.ISBN;
                        BookInfolist.IsAuthToEdit = true;
                        BookInfolist.strAccessionNo = !string.IsNullOrEmpty(p.AccessionNo) ? p.AccessionNo : "";
                        BookInfolist.intfilterAccessionNo = !string.IsNullOrEmpty(p.AccessionNo) ? Convert.ToInt64(p.AccessionNo) : 0;

                        string Description = "";
                        if (p.Description != null)
                        {
                            Description = p.Description;
                            if (p.Description.Length > 150)
                                Description = p.Description.Substring(0, 150) + " ...";
                        }
                        BookInfolist.Description = Description;

                        BookInfolist.MaterialTypeId = _ICustomEncryption.base64e(p.MaterialTypeId.ToString());
                        BookInfolist.MaterialType = p.MaterialType;
                        BookInfolist.AvaiableCopies = p.AvialableCopies == "0" ? "" : p.AvialableCopies;

                        if (BookInfolist.AvaiableCopies == "")
                            BookInfolist.IsAuthToEdit = false;
                        var totalcount = BookCopies.Where(f => f.BookId == p.BookId && f.BookStatusId == null).Count();
                        BookInfolist.TotalCopies = totalcount == 0 ? "" : totalcount.ToString();

                        BookInfolist.IsAuthToDelete = false;


                        return BookInfolist;
                    }).AsQueryable().Sort(sort);
                    //Order By AccessionNo
                    Data = Data.OrderBy(f => f.intfilterAccessionNo);
                    var AvailableCount = Data.Where(f => f.AvaiableCopies != "" && f.AvaiableCopies != "0" && f.AvaiableCopies != "null").Sum(f => Convert.ToInt32(f.AvaiableCopies)).ToString();
                    var TotalCount = Data.Where(f => f.TotalCopies != "" && f.TotalCopies != "0" && f.TotalCopies != "null").Sum(f => Convert.ToInt32(f.TotalCopies)).ToString();

                    var gridModel = new DataSourceResult
                    {
                        Data = Data.ToList(),
                        Total = BooksList.Count(),
                        Value1 = AvailableCount,
                        Value2 = TotalCount
                    };
                    return Json(gridModel);
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult GetStudentList(int ClassId, int SessionId = 0)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                IList<SelectListItem> AvailblStudent = new List<SelectListItem>();
                IList<SelectListItem> AvailableStudent = new List<SelectListItem>();
                if (ClassId > 0)
                {
                    var Session = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.IsDefualt == true).FirstOrDefault();
                    if (SessionId > 0)
                    {
                        Session = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.SessionId == SessionId).FirstOrDefault();
                    }
                    var StudentMemberTypeId = _IBookMasterService.GetAllBookMemberTypes(ref count, BookMemberType: "Student").FirstOrDefault().BookMemberTypeId;
                    var ClassStudentIds = _IStudentService.GetAllStudentClasss(ref count, ClassId: ClassId, SessionId: Session.SessionId).Select(f => f.StudentId).ToArray();
                    //AvailblStudent = _IStudentService.StudentListbyClassId(Classid: ClassId, SessionId: Session.SessionId, AdmnStatus: true).ToList();
                    var AvailableStudentIds = AvailblStudent.Select(f => f.Value).ToArray();
                    if (ClassStudentIds != null)
                    {
                        var TransactionStudentIds = _IBookMasterService.GetAllBookTransactions(ref count).Where(f => f.TransactionDate!=null && f.TransactionDate>= Session.StartDate && f.TransactionDate <= Session.EndDate &&  f.BookMemberTypeId == StudentMemberTypeId && ClassStudentIds.Contains(f.IssuedToId)).Select(f => f.IssuedToId).ToList();
                        if (TransactionStudentIds != null)
                        {
                            AvailblStudent = _IStudentService.StudentListbyClassId(Classid: ClassId, SessionId: Session.SessionId, AdmnStatus: true, Studentidlist: TransactionStudentIds).ToList();
                        }
                    }
                    //AvailableStudent = AvailblStudent.Select(s=>s.Value.Replace(s.Value,_ICustomEncryption.base64e(s.Value)));
                    foreach (var student in AvailblStudent)
                        AvailableStudent.Add(new SelectListItem { Selected = true, Text = student.Text, Value = _ICustomEncryption.base64e(student.Value) });


                    return Json(new
                    {
                        status = "success",
                        StudentList = AvailableStudent
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "Error",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetAllClassesTransactionList(int sessionId = 0)
        {
            try
            {
                IList<SelectListItem> AvailableStaff = new List<SelectListItem>();
                IList<SelectListItem> AvailableClass = new List<SelectListItem>();
                AvailableClass.Add(new SelectListItem { Value = "",Selected=true, Text = "--Select--" });
                AvailableStaff.Add(new SelectListItem { Value = "",Selected=true, Text = "--Select--" });
                    var Session = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.IsDefualt == true).FirstOrDefault();
                    if (sessionId > 0)
                    {
                        Session = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.SessionId == sessionId).FirstOrDefault();
                    }

                    var StudentMemberTypeId = _IBookMasterService.GetAllBookMemberTypes(ref count, BookMemberType: "Student").FirstOrDefault().BookMemberTypeId;
                    var StaffMemberTypeId = _IBookMasterService.GetAllBookMemberTypes(ref count, BookMemberType: "Staff").FirstOrDefault().BookMemberTypeId;
                if (Session != null)
                {
                    var AllTransactions = _IBookMasterService.GetAllBookTransactions(ref count).Where(f => f.TransactionDate != null && f.TransactionDate >= Session.StartDate && f.TransactionDate <= Session.EndDate);
                    var TransactionStudentIds = AllTransactions.Where(f => f.BookMemberTypeId == StudentMemberTypeId).Select(f => f.IssuedToId).ToList();
                    var TransactionStaffIds = AllTransactions.Where(f => f.BookMemberTypeId == StaffMemberTypeId).Select(f => f.IssuedToId).Distinct().ToArray();
                    if (TransactionStudentIds != null && TransactionStudentIds.Count > 0)
                    {
                        var AllClassStd = _IStudentService.GetAllStudentClasss(ref count, SessionId: Session.SessionId);
                        var TransClasses = AllClassStd.Where(f => TransactionStudentIds.Contains(f.StudentId)).Where(f => f.ClassId != null).GroupBy(f => f.ClassId);
                        foreach (var item in TransClasses)
                        {
                            AvailableClass.Add(new SelectListItem { Value = item.FirstOrDefault().ClassId.ToString(), Text = item.FirstOrDefault().ClassMaster.Class });
                        }
                    }
                    if (TransactionStaffIds != null && TransactionStaffIds.Count() > 0)
                    {
                        var AllStaffs = _IStaffService.GetAllStaffs(ref count).Where(f=> TransactionStaffIds.Contains(f.StaffId));
                        foreach (var item in AllStaffs)
                        {
                            AvailableStaff.Add(new SelectListItem { Value = item.StaffId.ToString(), Text = item.FName+item.LName+"("+ item.EmpCode+")" });
                        }
                    }
                }

                    return Json(new
                    {
                        status = "success",
                        ClassList = AvailableClass,
                        StaffList=AvailableStaff
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetIssuedStudentList(int ClassId, int SessionId = 0)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                IList<SelectListItem> AvailblStudent = new List<SelectListItem>();
                IList<SelectListItem> AvailableStudent = new List<SelectListItem>();
                if (ClassId > 0)
                {
                    var Session = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.IsDefualt == true).FirstOrDefault();
                    if (SessionId > 0)
                    {
                        Session = _ICatalogMasterService.GetAllSessions(ref count).Where(m => m.SessionId == SessionId).FirstOrDefault();
                    }
                    var StudentMemberTypeId = _IBookMasterService.GetAllBookMemberTypes(ref count, BookMemberType: "Student").FirstOrDefault().BookMemberTypeId;
                    var ClassStudentIds = _IStudentService.GetAllStudentClasss(ref count, ClassId: ClassId, SessionId: Session.SessionId).Select(f => f.StudentId).ToArray();
                    //AvailblStudent = _IStudentService.StudentListbyClassId(Classid: ClassId, SessionId: Session.SessionId, AdmnStatus: true).ToList();
                    var AvailableStudentIds = AvailblStudent.Select(f => f.Value).ToArray();
                    if (ClassStudentIds != null)
                    {
                        var issuedBooksList = _IBookMasterService.GetAllIssuedBooks(ref count).Where(x => x.ActualReturnDate == null && x.BookMemberTypeId != null && x.BookMemberType.BookMemberType1.ToLower() == "student").Select(f => f.IssuedToId).Distinct().ToList();
                        if (issuedBooksList != null && issuedBooksList.Count > 0)
                        {
                            AvailblStudent = _IStudentService.StudentListbyClassId(Classid: ClassId, SessionId: Session.SessionId, AdmnStatus: true, Studentidlist: issuedBooksList).ToList();
                        }
                    }
                    //AvailableStudent = AvailblStudent.Select(s=>s.Value.Replace(s.Value,_ICustomEncryption.base64e(s.Value)));
                    foreach (var student in AvailblStudent)
                        AvailableStudent.Add(new SelectListItem { Selected = true, Text = student.Text, Value = _ICustomEncryption.base64e(student.Value) });


                    return Json(new
                    {
                        status = "success",
                        StudentList = AvailableStudent
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    status = "Error",
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);

                return Json(new
                {
                    status = "Error",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpGet]
        public ActionResult IssueBookUserInformation(string MemeberId, string IssueToUser = "")
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                List<BookTransactionViewModel> paramsList = new List<BookTransactionViewModel>();
                int UserAllowDays = 0;
                string UserName = "";
                if (!string.IsNullOrEmpty(MemeberId) || !string.IsNullOrWhiteSpace(MemeberId))
                {
                    // decrypt values
                    int MemeberIdK = 0, IssueToId = 0;
                    if (!string.IsNullOrEmpty(MemeberId))
                        int.TryParse(_ICustomEncryption.base64d(MemeberId), out MemeberIdK);

                    if (!string.IsNullOrEmpty(IssueToUser))
                        int.TryParse(_ICustomEncryption.base64d(IssueToUser), out IssueToId);


                    var gettype = _IBookMasterService.GetBookMemberTypeById(BookMemberTypeId: MemeberIdK);
                    UserAllowDays = Convert.ToInt32(gettype.AllowedDays == null ? AllowedDaysForBook : gettype.AllowedDays);

                    var Session = _ICatalogMasterService.GetCurrentSession();

                    if (gettype.BookMemberType1 == "Student")
                    {
                        string RollNo = "";
                        string Class = "";
                        paramsList = new List<BookTransactionViewModel>();
                        var getMembers = _IStudentService.GetAllStudentClasss(ref count, Session.SessionId)
                                        .Where(x => x.RollNo != null || x.RollNo != "").ToList();
                        var members = getMembers;
                        if (IssueToId > 0)
                        {
                            string StudentName = "";
                            var getuser = members.Where(x => x.StudentId == IssueToId).FirstOrDefault();
                            if (getuser != null)
                            {
                                StudentName = _ICommonMethodService.TrimStudentName(getuser.Student, StudentName);
                                UserName = StudentName;

                                if (!string.IsNullOrEmpty(getuser.Student.AdmnNo))
                                    UserName += "-" + getuser.Student.AdmnNo;
                                if (!string.IsNullOrEmpty(getuser.ClassMaster.Class) || !string.IsNullOrEmpty(getuser.RollNo))
                                {
                                    UserName += "(";
                                    if (!string.IsNullOrEmpty(getuser.ClassMaster.Class))
                                        UserName += getuser.ClassMaster.Class;
                                    if (!string.IsNullOrEmpty(getuser.RollNo))
                                        UserName += "-" + getuser.RollNo;

                                    UserName += ")";
                                }
                            }
                        }
                        foreach (var stu in members)
                        {
                            string StudentName = "";
                            StudentName = _ICommonMethodService.TrimStudentName(stu.Student, StudentName);
                            if (!string.IsNullOrEmpty(stu.Student.AdmnNo))
                                StudentName += "-" + stu.Student.AdmnNo;
                            if (!string.IsNullOrEmpty(stu.ClassMaster.Class) || !string.IsNullOrEmpty(stu.RollNo))
                            {
                                StudentName += "(";
                                if (!string.IsNullOrEmpty(stu.ClassMaster.Class))
                                    StudentName += stu.ClassMaster.Class;
                                if (!string.IsNullOrEmpty(stu.RollNo))
                                    StudentName += "-" + stu.RollNo;
                                StudentName += ")";
                            }
                            paramsList.Add(new BookTransactionViewModel
                            {
                                IssuedToUserName = StudentName,
                                IssuedToId = _ICustomEncryption.base64e(stu.Student.StudentId.ToString()),
                                // AllowedDays = UserAllowDays.ToString()

                            });
                        }
                    }
                    if (gettype.BookMemberType1 == "Staff")
                    {
                        string EmpCode = "";
                        paramsList = new List<BookTransactionViewModel>();
                        if (IssueToId > 0)
                        {
                            var getuser = _IStaffService.GetStaffById(StaffId: IssueToId);

                            if (!string.IsNullOrEmpty(getuser.EmpCode))
                                EmpCode = " (" + getuser.EmpCode + ")";

                            UserName = getuser.FName + " " + getuser.LName + EmpCode;
                        }
                        var getMembers = _IStaffService.GetAllStaffs(ref count).OrderBy(x => x.FName);
                        foreach (var staff in getMembers)
                        {
                            EmpCode = "";
                            if (!string.IsNullOrEmpty(staff.EmpCode))
                                EmpCode = " (" + staff.EmpCode + ")";

                            paramsList.Add(new BookTransactionViewModel
                            {
                                IssuedToUserName = staff.FName + " " + staff.LName + EmpCode,
                                IssuedToId = _ICustomEncryption.base64e(staff.StaffId.ToString()),
                                //AllowedDays = UserAllowDays.ToString(),
                            });
                        }
                    }
                }

                var jsonResult = Json(new { paramsList, UserAllowDays, UserName }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult GetUsersInfo(string MemeberId, string IssueToUser = "")
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                List<BookTransactionViewModel> paramsList = new List<BookTransactionViewModel>();
                int UserAllowDays = 0;
                string UserName = "";
                if (!string.IsNullOrEmpty(MemeberId) || !string.IsNullOrWhiteSpace(MemeberId))
                {
                    // decrypt values
                    int MemeberIdK = 0, IssueToId = 0;
                    if (!string.IsNullOrEmpty(MemeberId))
                        int.TryParse(_ICustomEncryption.base64d(MemeberId), out MemeberIdK);

                    if (!string.IsNullOrEmpty(IssueToUser))
                        int.TryParse(_ICustomEncryption.base64d(IssueToUser), out IssueToId);


                    var gettype = _IBookMasterService.GetBookMemberTypeById(BookMemberTypeId: MemeberIdK);
                    UserAllowDays = Convert.ToInt32(gettype.AllowedDays == null ? AllowedDaysForBook : gettype.AllowedDays);

                    var Session = _ICatalogMasterService.GetCurrentSession();

                    if (gettype.BookMemberType1 == "Student")
                    {
                        string RollNo = "";
                        string Class = "";
                        paramsList = new List<BookTransactionViewModel>();
                        var getMembers = _IStudentService.GetAllStudentClasss(ref count, Session.SessionId).ToList();
                        var members = getMembers;
                        if (IssueToId > 0)
                        {
                            string StudentName = "";
                            var getuser = members.Where(x => x.StudentId == IssueToId).FirstOrDefault();
                            if (getuser != null)
                            {
                                StudentName = _ICommonMethodService.TrimStudentName(getuser.Student, StudentName);
                                UserName = StudentName;

                                if (!string.IsNullOrEmpty(getuser.Student.AdmnNo))
                                    UserName += "-" + getuser.Student.AdmnNo;
                                if (!string.IsNullOrEmpty(getuser.ClassMaster.Class) || !string.IsNullOrEmpty(getuser.RollNo))
                                {
                                    UserName += "(";
                                    if (!string.IsNullOrEmpty(getuser.ClassMaster.Class))
                                        UserName += getuser.ClassMaster.Class;
                                    if (!string.IsNullOrEmpty(getuser.RollNo))
                                        UserName += "-" + getuser.RollNo;

                                    UserName += ")";
                                }
                            }
                        }
                        foreach (var stu in members)
                        {
                            string StudentName = "";
                            StudentName = _ICommonMethodService.TrimStudentName(stu.Student, StudentName);
                            if (!string.IsNullOrEmpty(stu.Student.AdmnNo))
                                StudentName += "-" + stu.Student.AdmnNo;
                            if (!string.IsNullOrEmpty(stu.ClassMaster.Class) || !string.IsNullOrEmpty(stu.RollNo))
                            {
                                StudentName += "(";
                                if (!string.IsNullOrEmpty(stu.ClassMaster.Class))
                                    StudentName += stu.ClassMaster.Class;
                                if (!string.IsNullOrEmpty(stu.RollNo))
                                    StudentName += "-" + stu.RollNo;
                                StudentName += ")";
                            }
                            paramsList.Add(new BookTransactionViewModel
                            {
                                IssuedToUserName = StudentName,
                                IssuedToId = _ICustomEncryption.base64e(stu.Student.StudentId.ToString()),
                                // AllowedDays = UserAllowDays.ToString()

                            });
                        }
                    }
                    if (gettype.BookMemberType1 == "Staff")
                    {
                        string EmpCode = "";
                        paramsList = new List<BookTransactionViewModel>();
                        if (IssueToId > 0)
                        {
                            var getuser = _IStaffService.GetStaffById(StaffId: IssueToId);

                            if (!string.IsNullOrEmpty(getuser.EmpCode))
                                EmpCode = " (" + getuser.EmpCode + ")";

                            UserName = getuser.FName + " " + getuser.LName + EmpCode;
                        }
                        var getMembers = _IStaffService.GetAllStaffs(ref count).OrderBy(x => x.FName);
                        foreach (var staff in getMembers)
                        {
                            EmpCode = "";
                            if (!string.IsNullOrEmpty(staff.EmpCode))
                                EmpCode = " (" + staff.EmpCode + ")";

                            paramsList.Add(new BookTransactionViewModel
                            {
                                IssuedToUserName = staff.FName + " " + staff.LName + EmpCode,
                                IssuedToId = _ICustomEncryption.base64e(staff.StaffId.ToString()),
                                //AllowedDays = UserAllowDays.ToString(),
                            });
                        }
                    }
                }

                var jsonResult = Json(new { paramsList, UserAllowDays, UserName }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;
                return jsonResult;
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult GetTotalFine(string MemberTypeId, string ExpectedReturnDate, string ActualReturnDate)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                string TotalFine = "";
                DateTime? ExpectedDate = null;
                DateTime? ActualDate = null;
                if (!string.IsNullOrEmpty(ExpectedReturnDate) || !string.IsNullOrWhiteSpace(ExpectedReturnDate))
                {
                    ExpectedDate = DateTime.ParseExact(ExpectedReturnDate, "dd/MM/yyyy", null);
                    ExpectedDate = ExpectedDate.Value.Date;
                }
                if (!string.IsNullOrEmpty(ActualReturnDate) || !string.IsNullOrWhiteSpace(ActualReturnDate))
                {
                    ActualDate = DateTime.ParseExact(ActualReturnDate, "dd/MM/yyyy", null);
                    ActualDate = ActualDate.Value.Date;
                }

                int BookMemberTypeId = 0;
                if (!string.IsNullOrEmpty(MemberTypeId))
                    int.TryParse(_ICustomEncryption.base64d(MemberTypeId), out BookMemberTypeId);

                if (ActualDate > ExpectedDate)
                {
                    TimeSpan? diff = ActualDate - ExpectedDate;
                    int days = (int)Math.Abs(Math.Round(diff.Value.TotalDays)) - 1;
                    int freequencyDays = 0;

                    var getFine = _IBookMasterService.GetBookMemberTypeById(BookMemberTypeId: BookMemberTypeId);
                    if (getFine != null)
                    {
                        if ((bool)getFine.IsActive)
                        {
                            FinePerDay = getFine.FinePerDay;
                            freequencyDays = getFine.DaysFrequency != null ? (int)getFine.DaysFrequency : 0;
                        }
                    }
                    var FreqDays = days;
                    decimal totalFine = 0;
                    if (freequencyDays > 0)
                    {
                        FreqDays = days / freequencyDays;
                        totalFine = Convert.ToDecimal(FinePerDay + (FreqDays * FinePerDay));
                    }
                    else
                        totalFine = Convert.ToDecimal(FinePerDay + (FreqDays * FinePerDay));

                    TotalFine = Convert.ToString(totalFine);
                }

                return Json(new { TotalFine }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult GetBookCopies(string BookId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                int BookIdK = 0;
                if (!string.IsNullOrEmpty(BookId))
                    int.TryParse(_ICustomEncryption.base64d(BookId), out BookIdK);

                var getIssuedCopies = _IBookMasterService.GetAllBookTransactions(ref count).Where(x =>x.ActualReturnDate == null).ToList();

                List<SelectListItem> copies = new List<SelectListItem>();
                var getIdentity = _IBookMasterService.GetAllBookIdentities(ref count, BookId: BookIdK, IsInactive: true);
                if (getIdentity.Count() > 0)
                {
                    foreach (var copy in getIdentity)
                    {
                        var luiAccessionNo = "";
                        if (!string.IsNullOrEmpty(copy.LUI))
                        {
                            luiAccessionNo = " (" + copy.LUI.ToString() + ")";
                        }
                        //get Identity id by checking issued/returned copies of a book
                        
                       var IsIssuedCopies = getIssuedCopies.Where(x => x.BookIdentityId == copy.BookIdentityId).ToList();
                        if (IsIssuedCopies.Count() > 0)
                        {
                            continue;
                        }

                        copies.Add(new SelectListItem
                        {
                            Text = copy.UniqueId + luiAccessionNo,
                            Value = _ICustomEncryption.base64e(copy.BookIdentityId.ToString()),
                            Selected = false
                        });
                    }
                }
                return Json(new { copies }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetBookCopiesForDiscard(string BookId)
        {
                int BookIdK = 0;
                if (!string.IsNullOrEmpty(BookId))
                    int.TryParse(_ICustomEncryption.base64d(BookId), out BookIdK);
                if (BookIdK>0)
                {
                var getIdentity = _IBookMasterService.GetAllBookIdentities(ref count, BookId: BookIdK);
                var SelectCopyIds = getIdentity.Select(f => f.BookIdentityId).ToArray();
                var IssuedCopies = new List<BookTransaction>();
                if (SelectCopyIds != null && SelectCopyIds.Count() > 0)
                    IssuedCopies = _IBookMasterService.GetAllBookTransactions(ref count).Where(f => f.ActualReturnDate == null && SelectCopyIds.Contains((int)f.BookIdentityId)).ToList();
                   
                var AllBookStatus = _IBookMasterService.GetAllBookStatuss(ref count).Where(f=>f.StatusCode!=null);
                var DiscardBookStatus = AllBookStatus.Where(f => f.StatusCode==2).FirstOrDefault();
                List<BookAdditionalInfoViewModel> BookCopies = new List<BookAdditionalInfoViewModel>();
                var copymodel = new BookAdditionalInfoViewModel();
              var discarded = false; var IsIssued = false; var bkStatus = "";var IsStatusInActive = false;
                foreach (var Copies in getIdentity)
                {
                   discarded = false; IsIssued = false;bkStatus = "Available"; IsStatusInActive = false;
                    if (Copies.BookStatusId != null) {
                        if (DiscardBookStatus != null && DiscardBookStatus.StatusCode == Copies.BookStatusId)
                        {
                            discarded = true;
                            bkStatus = DiscardBookStatus.BookStatus;
                        }
                        else if (AllBookStatus.Count()>0)
                        {
                            var IsExistBookStatus = AllBookStatus.Where(f => f.StatusCode == Copies.BookStatusId).FirstOrDefault();
                            if (IsExistBookStatus != null)
                            {
                                bkStatus = IsExistBookStatus.BookStatus;
                                if (IsExistBookStatus.IsInactive!=null)
                                    IsStatusInActive = (bool)IsExistBookStatus.IsInactive;
                            }
                        }
                        else if (IssuedCopies.Where(f => f.BookIdentityId == Copies.BookIdentityId).Count() > 0)
                        {
                            IsIssued = true; IsStatusInActive = true;
                            bkStatus = "Issued";
                        }
                    }
                    else if (IssuedCopies.Where(f => f.BookIdentityId == Copies.BookIdentityId).Count() > 0) 
                    {
                        IsIssued = true; IsStatusInActive = true;
                         bkStatus = "Issued";
                    }
                    BookCopies.Add(new BookAdditionalInfoViewModel
                    {
                        //get book info
                        UniqueId = Copies.UniqueId == null ? "" : Copies.UniqueId,
                        LUIId = !String.IsNullOrEmpty(Copies.LUI) ? Copies.LUI : "",
                        BookEdition = Copies.BookEdition == null ? "" : Copies.BookEdition.Edition,
                        BarCode = Copies.BarCode == null ? "" : Copies.BarCode,
                        EntryDate = Copies.EntryDate == null ? "" : Copies.EntryDate.Value.ToString("dd/MM/yyyy"),
                        BookIdentityId = Copies.BookIdentityId.ToString(),
                        IsDiscarded = discarded,
                        IsIssued= IsIssued,
                        BookCopyStatus = bkStatus,
                        IsInActiveBook= IsStatusInActive,
                        //strBookLocation = BookLocation
                    });
                }
                return Json(new
                {
                    status = "success",
                    BookCopies
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                status = "error",
                mes = "Invalid Book"
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateDiscardCopies(string CheckedCopyIds,string UnchekedCopyIds)
        {
            if (!string.IsNullOrEmpty(CheckedCopyIds) || !string.IsNullOrEmpty(UnchekedCopyIds))
            {
                var CheckedIdArr = CheckedCopyIds.Split(',');
                var UnCheckedIdArr = UnchekedCopyIds.Split(',');
                var BookIdentities = _IBookMasterService.GetAllBookIdentities(ref count);
                var DiscardedStatus = _IBookMasterService.GetAllBookStatuss(ref count).Where(f=> f.StatusCode!=null && f.StatusCode==2).FirstOrDefault();
                if (DiscardedStatus != null)
                {
                    foreach (var item in CheckedIdArr)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            var bookcopy = BookIdentities.Where(f => f.BookIdentityId == Convert.ToInt32(item)).FirstOrDefault();
                            if (bookcopy != null)
                            {
                                bookcopy.StatusChangedOn = DateTime.Now.Date;
                                bookcopy.BookStatusId = DiscardedStatus.StatusCode;
                                _IBookMasterService.UpdateBookIdentity(bookcopy);
                            }
                        }
                    }
                }
                foreach(var tem in UnCheckedIdArr)
                {
                    if (!string.IsNullOrEmpty(tem))
                    {
                        var bookcopy = BookIdentities.Where(f => f.BookIdentityId == Convert.ToInt32(tem)).FirstOrDefault();
                        if (bookcopy != null)
                        {
                            bookcopy.StatusChangedOn = DateTime.Now.Date;
                            bookcopy.BookStatusId = null;
                            _IBookMasterService.UpdateBookIdentity(bookcopy);
                        }
                    }
                }
                return Json(new
                {
                    status = "success",
                    mes = "Books Status Updated Successfully."
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                status = "error",
                data = "Invalid Books"
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDates(string BookMemberTypeId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                List<BookTransactionViewModel> paramsList = new List<BookTransactionViewModel>();
                if (!string.IsNullOrEmpty(BookMemberTypeId) || !string.IsNullOrWhiteSpace(BookMemberTypeId))
                {
                    // decrypt values
                    int BookMemberTypeIdK = 0;
                    if (!string.IsNullOrEmpty(BookMemberTypeId))
                        int.TryParse(_ICustomEncryption.base64d(BookMemberTypeId), out BookMemberTypeIdK);

                    var getMemberType = _IBookMasterService.GetBookMemberTypeById(BookMemberTypeId: BookMemberTypeIdK);
                    int UserAllowDays = Convert.ToInt32(getMemberType.AllowedDays == null ? AllowedDaysForBook : getMemberType.AllowedDays);

                    paramsList.Add(new BookTransactionViewModel
                    {
                        ExpectedReturnDate = DateTime.Today.Date.AddDays(Convert.ToDouble(UserAllowDays)).ToString("dd/MM/yyy")
                    });

                }
                return Json(new { paramsList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult UpdateBookTransactions(BookTransactionViewModel model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                string message = "";

                // decrypt values
                int TransactionId = 0;
                if (!string.IsNullOrEmpty(model.BookTransactionId))
                    int.TryParse(_ICustomEncryption.base64d(model.BookTransactionId), out TransactionId);

                DateTime? ActualReturnDate = null;
                DateTime? ExpectedReturnDate = null;
                DateTime? PainOn = null;var ReIssueDate = "";var BookMemberTypeReIssue = ""; var IssuedToIdReIssue = "";var BookMaterialType = "";var MaxIssueDate = "";
                if (!string.IsNullOrEmpty(model.ExpectedReturnDate) || !string.IsNullOrWhiteSpace(model.ExpectedReturnDate))
                {
                    ExpectedReturnDate = DateTime.ParseExact(model.ExpectedReturnDate, "dd/MM/yyyy", null);
                    ExpectedReturnDate = ExpectedReturnDate.Value.Date;
                }
                if (!string.IsNullOrEmpty(model.ActualReturnDate) || !string.IsNullOrWhiteSpace(model.ActualReturnDate))
                {
                    ActualReturnDate = DateTime.ParseExact(model.ActualReturnDate, "dd/MM/yyyy", null);
                    ActualReturnDate = ActualReturnDate.Value.Date;
                }
                if (!string.IsNullOrEmpty(model.PaidOn) || !string.IsNullOrWhiteSpace(model.PaidOn))
                {
                    PainOn = DateTime.ParseExact(model.PaidOn, "dd/MM/yyyy", null);
                    PainOn = PainOn.Value.Date;
                }

                var IssueToList = new List<SelectListItem>();

                KSModel.Models.BookTransaction TransactionData = new KSModel.Models.BookTransaction();
                if (TransactionId > 0)
                {
                    TransactionData = _IBookMasterService.GetBookTransactionById(TransactionId, true);
                    if (TransactionData != null)
                    {
                        TransactionData.ActualReturnDate = ActualReturnDate;
                        //if (!string.IsNullOrEmpty(model.PendingFine) || !string.IsNullOrWhiteSpace(model.PendingFine))
                        //{
                        //    decimal lastPaidAmount = Convert.ToDecimal(model.PaidAmount);
                        //    decimal currentPaidAmount = Convert.ToDecimal(model.PendingFine);

                        //    TransactionData.PaidAmount = lastPaidAmount;
                        //    TransactionData.PaidOn = PainOn;
                        //}
                        //else
                        //{
                        ReIssueDate = ActualReturnDate.Value.AddDays(1).ToString("dd/MM/yyyy");
                        BookMemberTypeReIssue = _ICustomEncryption.base64e(TransactionData.BookMemberTypeId.ToString());
                        IssuedToIdReIssue = _ICustomEncryption.base64e(TransactionData.IssuedToId.ToString());
                        var CurrntDt = DateTime.Now.Date;
                        if (CurrntDt > ActualReturnDate.Value.AddDays(1))
                            MaxIssueDate = CurrntDt.Date.ToString("dd/MM/yyyy");
                        else
                            MaxIssueDate = ActualReturnDate.Value.AddDays(1).ToString("dd/MM/yyyy");

                        if (TransactionData.BookIdentity.BookMaster.MaterialTypeId != null)
                            BookMaterialType = TransactionData.BookIdentity.BookMaster.BookMaterialType.MaterialType;
                        else
                            BookMaterialType = "Re Issue Book";
                        if (ActualReturnDate > ExpectedReturnDate)
                        {
                            TransactionData.Fine = Convert.ToDecimal(model.Fine);
                            if (model.PaidAmount != null && model.PaidAmount != "0" && model.PaidAmount != "")
                            {
                                TransactionData.PaidAmount = Convert.ToDecimal(model.PaidAmount);
                                TransactionData.PaidOn = PainOn;
                            }
                        }
                        else if (model.PaidAmount != null && model.PaidAmount != "0" && model.PaidAmount != "")
                        {
                            TransactionData.PaidAmount = Convert.ToDecimal(model.PaidAmount);
                            TransactionData.PaidOn = PainOn;
                        }

                        TransactionData.Remarks = model.IssuedBookRemarks;
                        _IBookMasterService.UpdateBookTransaction(TransactionData);

                        message = "Book is returned Successfully";
                        TempData["successTrns"] = message;

                        //    if (model.strIsReIssue == "true"&& TransactionData.BookMemberTypeId!=null)
                        //    {
                        //        if (TransactionData.BookMemberType.BookMemberType1.ToLower() == "student")
                        //        {
                        //            var student = _IStudentService.GetStudentById((int)TransactionData.IssuedToId);
                        //            if (student != null)
                        //                IssueToList.Add(new SelectListItem { Value = student.FName + student.LName + "(" + student.AdmnNo + ")" });
                        //        }
                        //        else if (TransactionData.BookMemberType.BookMemberType1.ToLower() == "staff")
                        //        {
                        //            var staff = _IStaffService.GetStaffById((int)TransactionData.IssuedToId);
                        //            if (staff != null)
                        //                IssueToList.Add(new SelectListItem { Value = staff.FName + staff.LName + "(" + staff.EmpCode+ ")" });
                        //        }
                        //    }



                        return Json(new
                        {
                            status = "success",
                            data = message,
                            ReIssueDate,
                            BookMemberTypeReIssue,
                            IssuedToIdReIssue,
                            BookMaterialType,
                            MaxIssueDate
                        });
                    }
                }
                return Json(new
                {
                    status = "error",
                    data="Invalid Transaction"
                });

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult InsertBookTransactions(BookTransactionViewModel model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                string message = "";
                int BookTransactionId = 0;
                if (!string.IsNullOrEmpty(model.BookTransactionId))
                    int.TryParse(_ICustomEncryption.base64d(model.BookTransactionId), out BookTransactionId);

                int BookMemberTypeId = 0;
                if (!string.IsNullOrEmpty(model.BookMemberTypeId))
                    int.TryParse(_ICustomEncryption.base64d(model.BookMemberTypeId), out BookMemberTypeId);

                int BookId = 0;
                if (!string.IsNullOrEmpty(model.BookId))
                    int.TryParse(_ICustomEncryption.base64d(model.BookId), out BookId);

                int IssuedToId = 0;
                if (!string.IsNullOrEmpty(model.IssuedToId))
                    int.TryParse(_ICustomEncryption.base64d(model.IssuedToId), out IssuedToId);

                int BookIdentityId = 0;
                if (!string.IsNullOrEmpty(model.BookIdentityId))
                    int.TryParse(_ICustomEncryption.base64d(model.BookIdentityId), out BookIdentityId);

                DateTime? IssueDate = null, ExpectedReturnDate = null;
                DateTime? ActualReturnDate = null;
                DateTime? PaidOn = null;
                if (!string.IsNullOrEmpty(model.TransactionDate) || !string.IsNullOrWhiteSpace(model.TransactionDate))
                {
                    IssueDate = DateTime.ParseExact(model.TransactionDate, "dd/MM/yyyy", null);
                    IssueDate = IssueDate.Value.Date;
                }

                if (!string.IsNullOrEmpty(model.ExpectedReturnDate) || !string.IsNullOrWhiteSpace(model.ExpectedReturnDate))
                {
                    ExpectedReturnDate = DateTime.ParseExact(model.ExpectedReturnDate, "dd/MM/yyyy", null);
                    ExpectedReturnDate = ExpectedReturnDate.Value.Date;
                }
                if (!string.IsNullOrEmpty(model.ActualReturnDate) || !string.IsNullOrWhiteSpace(model.ActualReturnDate))
                {
                    ActualReturnDate = DateTime.ParseExact(model.ActualReturnDate, "dd/MM/yyyy", null);
                    ActualReturnDate = ActualReturnDate.Value.Date;
                }
                if (!string.IsNullOrEmpty(model.PaidOn) || !string.IsNullOrWhiteSpace(model.PaidOn))
                {
                    PaidOn = DateTime.ParseExact(model.PaidOn, "dd/MM/yyyy", null);
                    PaidOn = PaidOn.Value.Date;
                }
                var strStudentClass = "";
                var IsStudent = false;
                var StudentBookMemberType = _IBookMasterService.GetAllBookMemberTypes(ref count, BookMemberType: "Student").FirstOrDefault();
                if (StudentBookMemberType != null && StudentBookMemberType.BookMemberTypeId == BookMemberTypeId)
                    IsStudent = true;
                if (IsStudent)
                {
                    var CrrSession = _ICatalogMasterService.GetAllSessions(ref count, Isdeafult: true).FirstOrDefault();
                    if (CrrSession != null)
                    {
                        var StudentClass = _IStudentService.GetAllStudentClasss(ref count, SessionId: CrrSession.SessionId, StudentId: IssuedToId).FirstOrDefault();
                        if (StudentClass != null)
                        {
                            strStudentClass = StudentClass.ClassMaster.Class;
                        }
                    }
                }
                KSModel.Models.BookTransaction TransactionData = new KSModel.Models.BookTransaction();
                TransactionData.TransactionDate = IssueDate;
                TransactionData.BookMemberTypeId = BookMemberTypeId;
                TransactionData.IssuedToId = IssuedToId;
                TransactionData.ExpectedReturnDate = ExpectedReturnDate;
                TransactionData.BookIdentityId = BookIdentityId;
                if (!string.IsNullOrEmpty(strStudentClass))
                    TransactionData.StudentClass = strStudentClass;

                if (model.ActualReturnDate == null)
                {
                    TransactionData.ActualReturnDate = null;
                    TransactionData.Fine = null;
                    TransactionData.PaidAmount = null;
                    TransactionData.PaidOn = null;

                    message = "Issued";
                }
                //else
                //{
                //    TransactionData.ActualReturnDate = ActualReturnDate;
                //    if (ActualReturnDate > ExpectedReturnDate)
                //    {
                //        TransactionData.Fine = Convert.ToDecimal(model.Fine == null ? "0" : model.Fine);
                //        TransactionData.PaidAmount = Convert.ToDecimal(model.PaidAmount == null ? "0" : model.PaidAmount);
                //        TransactionData.PaidOn = PaidOn;
                //    }
                //    message = "Return";
                //}
                if (message == "Issued")
                {
                    message = "This Book is Issued Successfull";
                }
                else
                {
                    message = "This Book is returned Successfull";
                }
                _IBookMasterService.InsertBookTransaction(TransactionData);

                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult DeleteBookTransaction(string TransactionId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                string message = "";
                // decrypt values
                int TransactionIdIK = 0;
                if (!string.IsNullOrEmpty(TransactionId))
                    int.TryParse(_ICustomEncryption.base64d(TransactionId), out TransactionIdIK);

                if (TransactionIdIK > 0)
                {
                    _IBookMasterService.DeleteBookTransaction(TransactionIdIK);
                    message = "Deleted successfully";
                }
                return Json(new
                {
                    status = "success",
                    data = message
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        #endregion

        #region TransactionHistory

        public ActionResult TransactionHistoryList(DataSourceRequest command, BookTransactionViewModel model
                                              , IEnumerable<Sort> sort = null, string SearchToDate = "", string SearchFromDate = "")
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                // decrypt values
                int? EncBookIdK = null;
                int BookIdK = 0;
                if (!string.IsNullOrEmpty(model.BookId))
                {
                    int.TryParse(_ICustomEncryption.base64d(model.BookId), out BookIdK);
                    EncBookIdK = BookIdK;
                }

                DateTime? FilteredToDate = null, FilteredFromDate = null;
                if (!string.IsNullOrEmpty(SearchToDate) || !string.IsNullOrWhiteSpace(SearchToDate))
                {
                    FilteredToDate = DateTime.ParseExact(SearchToDate, "dd/MM/yyyy", null);
                    FilteredToDate = FilteredToDate.Value.Date;
                }
                if (!string.IsNullOrEmpty(SearchFromDate) || !string.IsNullOrWhiteSpace(SearchFromDate))
                {
                    FilteredFromDate = DateTime.ParseExact(SearchFromDate, "dd/MM/yyyy", null);
                    FilteredFromDate = FilteredFromDate.Value.Date;
                }
                if (model.SearchDateId == 0)
                {
                    model.SearchDateId = null;
                }
                var AllBookMemberType = _IBookMasterService.GetAllBookMemberTypes(ref count);
                var BookMemberTypeStudent = AllBookMemberType.Where(m => m.BookMemberType1.ToLower() == "student").FirstOrDefault();
                var BookMemberTypeStaff = AllBookMemberType.Where(m => m.BookMemberType1.ToLower() == "staff").FirstOrDefault();

                var BooksList = _IBookMasterService.GetAllIssuedBooks(ref count, BookId: EncBookIdK,
                                        DateFilterId: model.SearchDateId, ToDateFilter: FilteredToDate,
                                        SearchText: model.SearchText == null ? "" : model.SearchText,
                                        FromDateFilter: FilteredFromDate);
                var CurrentSession = _ICatalogMasterService.GetAllSessions(ref count, Isdeafult: true).FirstOrDefault();
                if (model.SessionId != null && model.SessionId > 0)
                {
                    var SelectedSession = _ICatalogMasterService.GetSessionById((int)model.SessionId);
                    if (SelectedSession != null)
                        BooksList = BooksList.Where(f => f.TransactionDate != null && f.TransactionDate >= SelectedSession.StartDate && f.TransactionDate <= SelectedSession.EndDate).ToList();
                }
                if (!string.IsNullOrEmpty(model.bkMemberTypeId) && model.bkMemberTypeId == "student")
                {
                    BooksList = BooksList.Where(f => f.BookMemberTypeId != null && f.BookMemberType.BookMemberType1.ToLower() == "student").ToList();
                    if (!string.IsNullOrEmpty(model.bKTransactionStudentId))
                    {
                        int studentid = 0;
                        if (!string.IsNullOrEmpty(model.bKTransactionStudentId))
                            int.TryParse(_ICustomEncryption.base64d(model.bKTransactionStudentId), out studentid);
                        if (studentid > 0)
                            BooksList = BooksList.Where(f => f.IssuedToId == studentid).ToList();
                    }
                    else if (model.bkTransactionClassId != null && model.bkTransactionClassId > 0)
                    {
                        var ClassById = _ICatalogMasterService.GetClassMasterById((int)model.bkTransactionClassId);
                        BooksList = BooksList.Where(f => f.StudentClass != null && ClassById.Class.ToLower() == f.StudentClass.ToLower()).ToList();
                    }
                }
                else if (!string.IsNullOrEmpty(model.bkMemberTypeId) && model.bkMemberTypeId == "staff")
                {
                    BooksList = BooksList.Where(f => f.BookMemberTypeId != null && f.BookMemberType.BookMemberType1.ToLower() == "staff").ToList();
                    if (model.bKTransactionStaffId > 0)
                    {
                        BooksList = BooksList.Where(f => f.IssuedToId == model.bKTransactionStaffId).ToList();
                    }
                }
                // var StudentMemberType=
                //var StudentIds = BooksList.Where(f => f.BookMemberType.BookMemberType1.ToLower() == "student").Select(f => f.IssuedToId).ToArray();
                //var Session = _ICatalogMasterService.GetAllSessions(ref count, Isdeafult: true).FirstOrDefault();
                //int selectedssn = 0;
                //if (model.SessionId != null && model.SessionId > 0)
                //{
                //    selectedssn = (int)model.SessionId;
                //    Session = _ICatalogMasterService.GetAllSessions(ref count, SessionId: (int)model.SessionId).FirstOrDefault();
                //    if (StudentIds != null && StudentIds.Count() > 0)
                //    {
                //        var InSession = _IStudentService.GetAllStudentClasss(ref count).Where(f => StudentIds.Contains(f.StudentId) && f.SessionId == model.SessionId).Select(f => f.StudentId).ToArray();
                //        if (InSession != null && InSession.Count() > 0)
                //        {
                //            var ExistInSession = InSession.Intersect(StudentIds).ToArray();
                //            if (ExistInSession != null && ExistInSession.Count() > 0)
                //                BooksList = BooksList.Where(f => ExistInSession.Contains((int)f.IssuedToId)).ToList();
                //        }
                //    }
                //}
                //else
                //{
                //    selectedssn = Session.SessionId;
                //}
                //if (model.bkMemberTypeId != null && model.bkMemberTypeId.ToLower() == "student")
                //{
                //    BooksList = BooksList.Where(f => f.BookMemberTypeId == BookMemberTypeStudent.BookMemberTypeId).ToList();
                //    if (model.bkTransactionClassId != null && model.bkTransactionClassId > 0)
                //    {
                //        var ClassStudents = _IStudentService.GetAllStudentClasss(ref count, SessionId: selectedssn, ClassId: model.bkTransactionClassId).Select(f => f.StudentId).ToArray();
                //        if (ClassStudents != null && ClassStudents.Count() > 0)
                //        {
                //            BooksList = BooksList.Where(f => ClassStudents.Contains((int)f.IssuedToId)).ToList();
                //        }
                //    }
                //    if (!string.IsNullOrEmpty(model.bKTransactionStudentId))
                //    {
                //        int studentid = 0;
                //        if (!string.IsNullOrEmpty(model.bKTransactionStudentId))
                //            int.TryParse(_ICustomEncryption.base64d(model.bKTransactionStudentId), out studentid);
                //        if (studentid > 0)
                //            BooksList = BooksList.Where(f => f.IssuedToId == studentid).ToList();
                //    }
                //}
                //else if(model.bkMemberTypeId != null && model.bkMemberTypeId.ToLower() == "staff")
                //{
                //    BooksList = BooksList.Where(f => f.BookMemberTypeId == BookMemberTypeStaff.BookMemberTypeId).ToList();
                //    if(model.bKTransactionStaffId>0)
                //    {
                //        BooksList = BooksList.Where(f => f.IssuedToId==model.bKTransactionStaffId).ToList();
                //    }
                //}

                if (!string.IsNullOrEmpty(model.TransHistoryBookStatus))
                {
                    if (model.TransHistoryBookStatus.ToLower() == "available")
                    {
                        BooksList = BooksList.Where(f => f.IsLost != true).ToList();
                    }
                    else if (model.TransHistoryBookStatus.ToLower() == "lost")
                    {
                        BooksList = BooksList.Where(f => f.IsLost == true).ToList();
                    }
                    else if (model.TransHistoryBookStatus.ToLower() == "dueforreturn")
                    {
                        BooksList = BooksList.Where(f => f.ActualReturnDate == null && f.ExpectedReturnDate != null && f.ExpectedReturnDate.Value.Date < DateTime.Now.Date && f.IsLost != true).ToList();
                    }
                    else if (model.TransHistoryBookStatus.ToLower() == "issued")
                    {
                        BooksList = BooksList.Where(f => f.ActualReturnDate == null && f.IsLost != true).ToList();
                    }
                    else if (model.TransHistoryBookStatus.ToLower() == "returned")
                    {
                        BooksList = BooksList.Where(f => f.ActualReturnDate != null & f.IsLost != true).ToList();
                    }
                    else if (model.TransHistoryBookStatus.ToLower() == "pendingfine")
                    {
                        IList<BookTransaction> PendingFineList = new List<BookTransaction>();
                        List<int> booktransIds = new List<int>();
                        BooksList = BooksList.Where(f => (f.LostPenalty != null && f.LostPenalty > 0.00m) || (f.Fine != null && f.Fine > 0.00m)).ToList();
                        foreach (var item in BooksList)
                        {
                            decimal Fine = 0.00m; decimal lostpenalty = 0.00m;
                            if (item.Fine != null)
                                Fine = (decimal)item.Fine;
                            if (item.LostPenalty != null)
                                lostpenalty = (decimal)item.LostPenalty;
                            var sum = Fine + lostpenalty;
                            if (item.PaidAmount == null || sum > item.PaidAmount)
                            {
                                booktransIds.Add(item.BookTransactionId);
                            }
                        }
                        if (booktransIds != null)
                        {
                            BooksList = BooksList.Where(f => booktransIds.Contains(f.BookTransactionId)).ToList();
                        }
                        else
                        {
                            BooksList.Clear();
                        }
                    }
                    else if (model.TransHistoryBookStatus.ToLower() == "latereturned")
                    {
                        BooksList = BooksList.Where(f => f.ActualReturnDate != null && f.ExpectedReturnDate != null && f.ActualReturnDate.Value.Date > f.ExpectedReturnDate.Value.Date).ToList();
                    }
                }
                var gridModel = new DataSourceResult
                {
                    Data = BooksList.OrderByDescending(x => x.ActualReturnDate).Select(p =>
                    {
                        var BookInfolist = new BookTransactionViewModel();
                        BookInfolist.BookTransactionId = _ICustomEncryption.base64e(p.BookTransactionId.ToString());
                        BookInfolist.TransactionDate = p.TransactionDate.Value.Date.ToString("dd/MM/yyyy");
                        BookInfolist.BookMemberTypeId = _ICustomEncryption.base64e(p.BookMemberTypeId.ToString());
                        BookInfolist.BookMemberType = p.BookMemberType.BookMemberType1;
                        BookInfolist.IssuedToId = _ICustomEncryption.base64e(p.IssuedToId.ToString());
                        BookInfolist.BookMemberTypeId = _ICustomEncryption.base64e(p.BookMemberTypeId.ToString());
                        BookInfolist.strAccessionNo = "";
                        if (!string.IsNullOrEmpty(p.BookIdentity.LUI))
                        {
                            if (p.BookIdentity.BookMaster.AccessionNo != null)
                                BookInfolist.strAccessionNo = p.BookIdentity.BookMaster.AccessionNo.ToString() + "-";

                            BookInfolist.strAccessionNo += p.BookIdentity.LUI.ToString();
                        }
                        switch (BookInfolist.BookMemberType)
                        {
                            case "Student":
                                var getMembers = _IStudentService.GetAllStudentClasss(ref count, CurrentSession.SessionId, StudentId: p.IssuedToId).FirstOrDefault();
                                if (getMembers != null)
                                {
                                    string RollNo = "";
                                    string Class = "";
                                    if (!string.IsNullOrEmpty(getMembers.RollNo))
                                        RollNo = " (" + getMembers.RollNo + ")";
                                    if (!string.IsNullOrEmpty(getMembers.ClassMaster.Class))
                                        Class = " ( -" + getMembers.ClassMaster.Class + ")";

                                    BookInfolist.IssuedToUserName = getMembers.Student.FName + " " + getMembers.Student.LName
                                                                   + RollNo + Class;

                                }
                                else
                                {
                                    var getStudent = _IStudentService.GetStudentById((int)p.IssuedToId);
                                    if (getStudent != null)
                                    {
                                        BookInfolist.IssuedToUserName = getStudent.FName + " " + getStudent.LName;
                                    }
                                }
                                break;
                            case "Staff":
                                var getuserStaffInfo = _IStaffService.GetStaffById(StaffId: (int)p.IssuedToId);
                                if (getuserStaffInfo != null)
                                {
                                    string EmpCode = "";
                                    if (!string.IsNullOrEmpty(getuserStaffInfo.EmpCode))
                                        EmpCode = " (" + getuserStaffInfo.EmpCode + ")";

                                    BookInfolist.IssuedToUserName = (getuserStaffInfo.FName + " " + getuserStaffInfo.LName)
                                                                    + EmpCode;
                                }
                                break;
                        }

                        //var getUniqueId = _IBookMasterService.GetBookIdentityById(BookIdentityId: p.BookIdentityId);
                        // BookInfolist.UniqueId = getUniqueId.UniqueId;
                        if (p.IsLost == true)
                        {
                            BookInfolist.IsLost = p.IsLost;
                            BookInfolist.LostPenalty = p.LostPenalty.ToString();
                        }
                        else
                            BookInfolist.IsLost = false;

                        BookInfolist.ExpectedReturnDate = p.ExpectedReturnDate.Value.Date.ToString("dd/MM/yyyy");
                        BookInfolist.ActualReturnDate = p.ActualReturnDate == null ? "" : p.ActualReturnDate.Value.Date.ToString("dd/MM/yyyy");
                        BookInfolist.Fine = p.Fine.ToString();
                        BookInfolist.PaidAmount = p.PaidAmount == null ? "" : p.PaidAmount.ToString();
                        BookInfolist.BookTitle = p.BookIdentity.BookMaster.BookTitle;
                        BookInfolist.ShowBookTitle = false;

                        decimal fine = 0.00m, paidAmt = 0.00m, lostPenalty = 0.00m;
                        if (p.Fine != null)
                            fine = (decimal)p.Fine;
                        if (p.PaidAmount != null)
                            paidAmt = (decimal)p.PaidAmount;
                        if (p.LostPenalty != null)
                            lostPenalty = (decimal)p.LostPenalty;

                        decimal totalfine = fine;
                        if (p.IsLost == true)
                            totalfine = fine + lostPenalty;

                        decimal? PendingFine = null;
                        PendingFine = totalfine - paidAmt;

                        if (PendingFine > 0)
                            BookInfolist.PendingFine = Convert.ToString(PendingFine);


                        if (EncBookIdK == null)
                        {
                            BookInfolist.ShowBookTitle = true;
                        }

                        BookInfolist.PaidOn = p.PaidOn == null ? "" : p.PaidOn.Value.Date.ToString("dd/MM/yyyy");
                        BookInfolist.BookIdentityId = _ICustomEncryption.base64e(p.BookIdentityId.ToString());
                        BookInfolist.BookId = _ICustomEncryption.base64e(p.BookIdentity.BookId.ToString());

                        BookInfolist.AllowedDays = Convert.ToString(p.BookMemberType.AllowedDays == null ? AllowedDaysForBook : p.BookMemberType.AllowedDays);
                        BookInfolist.FinePerDay = Convert.ToString(p.BookMemberType.FinePerDay == null ? FinePerDay : p.BookMemberType.FinePerDay);
                        BookInfolist.IssuedBookRemarks = "";
                        if (!string.IsNullOrEmpty(p.Remarks))
                            BookInfolist.IssuedBookRemarks = p.Remarks;
                        BookInfolist.setColor = false;
                        if (BookInfolist.ActualReturnDate == "")
                        {
                            BookInfolist.setColor = true;
                        }
                        BookInfolist.IsAuthToDelete = false;

                        if (BookInfolist.TransactionDate == DateTime.Today.Date.ToString("dd/MM/yyyy"))
                        {
                            BookInfolist.IsAuthToDelete = true;
                        }

                        return BookInfolist;
                    }).AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = BooksList.Count()
                };
                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        #endregion

        #endregion

        #region ExportMethods
        public DataTable GetAllKendoGridInformation(string CityId, string Sellername)
        {
            DataTable dt = new DataTable();
            try
            {

                // decrypt values
                int? encCityId = null;
                if (!string.IsNullOrEmpty(CityId))
                {
                    encCityId = Convert.ToInt32(CityId);
                }

                var BooksellersList = _IBookMasterService.GetAllBookSellers(ref count, BookSellerName: Sellername,
                                                                            CityId: encCityId, Isfilter: true);

                // Add columns in your DataTable
                dt.Columns.Add("Seller Name");
                dt.Columns.Add("City");
                dt.Columns.Add("Mobile No.");
                dt.Columns.Add("WebSite");

                DataRow row;
                if (BooksellersList.Count() > 0)
                {
                    foreach (var data in BooksellersList)
                    {
                        row = dt.NewRow();
                        row[0] = data.BookSellerName == null ? "" : data.BookSellerName.ToString();

                        //get cityname
                        var getcity = _IAddressMasterService.GetAddressCityById(AddressCityId: (int)data.CityId);
                        if (getcity != null)
                        {
                            row[1] = getcity.City == null ? "" : getcity.City.ToString();
                        }
                        row[2] = data.Mobile == null ? "" : data.Mobile.ToString();
                        row[3] = data.WebSite == null ? "" : data.WebSite.ToString();

                        dt.Rows.Add(row);
                        dt.AcceptChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
            }
            return dt;
        }

        public ActionResult Export()
        {
            return Json(new
            {
                status = "success",
                data = ""
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ExportData(string CityId, string Sellername, string IsExcel)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                string title = "";
                string Subtitle = "";

                var getForm = _IUserManagementService.GetAllAppForms(ref count, AppForm: "Book Seller").FirstOrDefault();
                if (getForm != null)
                {
                    var getFormGirdinfo = _IUserManagementService.GetAllAppFormGrids(ref count, AppFormId: getForm.AppFormId);
                    if (getFormGirdinfo.Count > 0)
                    {
                        title = getFormGirdinfo.ElementAt(0).GridTitle;
                        Subtitle = getFormGirdinfo.ElementAt(0).SubTitlePrefix + " " + getFormGirdinfo.ElementAt(0).SubTitleSuffix;
                    }
                    else
                    {
                        title = ViewBag.Title;
                        Subtitle = "";
                    }
                }
                DataTable dt = new DataTable();
                dt = GetAllKendoGridInformation(CityId, Sellername);
                if (dt.Rows.Count > 0)
                {
                    if (IsExcel == "1")
                    {
                        _IExportHelper.ExportToExcelForLibrary(dt, Title: title, Subtitle: Subtitle);
                    }
                    else if (IsExcel == "2")
                    {
                        _IExportHelper.ExportToPrint(dt, Title: title, Subtitle: Subtitle);
                    }
                    else
                    {
                        _IExportHelper.ExportToPDF(dt, Title: title, Subtitle: Subtitle);
                    }
                }
            }
            catch (Exception ex)
            { }
            return null;
        }
        #endregion

        #region AjaxMethods
        public ActionResult GetBookPrice(string BookTransactionId)
        {
            try
            {
                var msg = "Invalid BookTransaction";
                if (!string.IsNullOrEmpty(BookTransactionId))
                {
                    int bookTransactnId = 0;
                    int.TryParse(_ICustomEncryption.base64d(BookTransactionId), out bookTransactnId);
                    if (bookTransactnId > 0)
                    {
                        var BookTransaction = _IBookMasterService.GetBookTransactionById(bookTransactnId);
                        if (BookTransaction != null)
                        {
                            var BookIdentity = _IBookMasterService.GetBookIdentityById(BookTransaction.BookIdentityId);
                            msg = "InValid BookIdentity";
                            if (BookIdentity != null)
                            {
                                var BookEdition = _IBookMasterService.GetBookEditionById(BookIdentity.BookEditionId);
                                msg = "InValid BookEdition";
                                if (BookEdition != null)
                                {
                                    var BookPrice = BookEdition.ListPrice != null ? BookEdition.ListPrice : 0;
                                    return Json(new
                                    {
                                        status = "success",
                                        BookPrice = BookPrice
                                    }, JsonRequestBehavior.AllowGet);
                                }
                            }
                        }
                    }
                }
                return Json(new
                {
                    status = "error",
                    msg = msg
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult UpdateLostBook(BookTransactionViewModel model)
        {
            try
            {
                var msg = "Invalid BookTransaction";
                if (!string.IsNullOrEmpty(model.BookTransactionId))
                {
                    int bookTransactnId = 0;
                    int.TryParse(_ICustomEncryption.base64d(model.BookTransactionId), out bookTransactnId);
                    if (bookTransactnId > 0)
                    {
                        var BookTransaction = _IBookMasterService.GetBookTransactionById(bookTransactnId);
                        if (BookTransaction != null)
                        {
                            var bookStatus = _IBookMasterService.GetAllBookStatuss(ref count, BookStatus: "lost").FirstOrDefault();
                            var BookIdentity = _IBookMasterService.GetBookIdentityById(BookTransaction.BookIdentityId);
                            msg = "InValid BookIdentity";
                            if (BookIdentity != null && bookStatus != null)
                            {
                                BookIdentity.BookStatusId = bookStatus.StatusCode;
                                _IBookMasterService.UpdateBookIdentity(BookIdentity);
                            }
                            DateTime? ActualReturnDate = null;
                            DateTime? ExpectedReturnDate = null;
                            DateTime? PainOn = null;
                            if (!string.IsNullOrEmpty(model.ExpectedReturnDate) || !string.IsNullOrWhiteSpace(model.ExpectedReturnDate))
                            {
                                ExpectedReturnDate = DateTime.ParseExact(model.ExpectedReturnDate, "dd/MM/yyyy", null);
                                ExpectedReturnDate = ExpectedReturnDate.Value.Date;
                            }
                            if (!string.IsNullOrEmpty(model.ActualReturnDate) || !string.IsNullOrWhiteSpace(model.ActualReturnDate))
                            {
                                ActualReturnDate = DateTime.ParseExact(model.ActualReturnDate, "dd/MM/yyyy", null);
                                ActualReturnDate = ActualReturnDate.Value.Date;
                            }
                            if (!string.IsNullOrEmpty(model.PaidOn) || !string.IsNullOrWhiteSpace(model.PaidOn))
                            {
                                PainOn = DateTime.ParseExact(model.PaidOn, "dd/MM/yyyy", null);
                                PainOn = PainOn.Value.Date;
                            }
                            BookTransaction.ActualReturnDate = ActualReturnDate;
                            if (!string.IsNullOrEmpty(model.PendingFine) || !string.IsNullOrWhiteSpace(model.PendingFine))
                            {
                                decimal PaidAmount = Convert.ToDecimal(model.PaidAmount);
                                decimal currentPaidAmount = Convert.ToDecimal(model.PendingFine);

                                BookTransaction.PaidAmount = PaidAmount + currentPaidAmount;
                                BookTransaction.PaidOn = PainOn;
                            }
                            else
                            {
                                if (ActualReturnDate > ExpectedReturnDate && !string.IsNullOrEmpty(model.Fine))
                                {
                                    BookTransaction.Fine = Convert.ToDecimal(model.Fine);
                                }
                                if (model.PaidAmount != null && model.PaidAmount != "0")
                                {
                                    BookTransaction.PaidAmount = Convert.ToDecimal(model.PaidAmount);
                                    BookTransaction.PaidOn = PainOn;
                                }

                            }
                            decimal Lostpenalty = Convert.ToDecimal(model.LostPenalty);
                            BookTransaction.LostPenalty = Lostpenalty;
                            BookTransaction.IsLost = true;
                            BookTransaction.Remarks = model.IssuedBookRemarks;
                            _IBookMasterService.UpdateBookTransaction(BookTransaction);
                            return Json(new
                            {
                                status = "success",
                                msg = "Book Transaction Updated"
                            }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                return Json(new
                {
                    status = "error",
                    msg = msg
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult GetBookTransactionDropDowns()
        {
            try
            {
                var BookLocationAttribute = _IBookMasterService.GetAllBookLocationAttributes(ref count, IsActive: true);
                var BookLocationData = _IBookMasterService.GetAllBookLocationDatas(ref count, IsActive: true).Where(f=>f.BookLocationAttributeId!=null);
                IList<BookLocationDataViewModel> BookLocationList= new List<BookLocationDataViewModel>();
                var booklocationmodel = new BookLocationDataViewModel();
                foreach (var item in BookLocationAttribute.OrderBy(f=>f.SortingIndex))
                {
                    booklocationmodel = new BookLocationDataViewModel();
                    booklocationmodel.AttributeValue = item.LocationAttribute;
                    var LocationDataList = BookLocationData.Where(f => f.BookLocationAttributeId == item.BookLocationAttributeId);
                    booklocationmodel.BookLocationDataList.Add(new BookLocationDataModel { BookLocationAttributeId = "", BookLocationAttributeValue = "--Select--", BookLocationDataId = "" });
                    foreach (var item1 in LocationDataList)
                    {
                        booklocationmodel.BookLocationDataList.Add(new BookLocationDataModel { BookLocationAttributeId = item1.BookLocationAttributeId.ToString(),
                            BookLocationAttributeValue = item1.AttributeValue, BookLocationDataId = item1.BookLocationDataId.ToString() });
                    }
                    BookLocationList.Add(booklocationmodel);
                }
                return Json(new { status = "success", BookLocationList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "error",data = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}