﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using DAL.DAL.Common;
using KSModel.Models;
using DAL.DAL.ErrorLogModule;
using ViewModel.ViewModel.Security;
using DAL.DAL.UserModule;
using DAL.DAL.Security;
using DAL.DAL.Authentication;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using ViewModel.ViewModel.Common;
using DAL.DAL.StudentModule;
using DAL.DAL.FeeModule;
using DAL.DAL.StaffModule;
using DAL.DAL.ContactModule;
using BAL.BAL;
using DAL.DAL.SettingService;
using System.Security.Cryptography;
using System.Text;
using DAL.DAL.GuardianModule;
using BAL.BAL.Common;
using System.Web.Security;
using DAL.DAL.MessengerModule;
using DAL.DAL.Schedular;
using DAL.DAL.ExaminationModule;
using DAL.DAL.DocumentModule;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.Data.SqlClient;
using DAL.DAL.MenuModel;
using System.Web.Script.Serialization;
using DAL.DAL.ActivityLogModule;

//using Mechanism;


namespace KS_SmartChild.Controllers
{

    public class HomeController : Controller
    {
        #region field

        public int count;
        private readonly IConnectionService _IConnectionService;
        private readonly IUserService _IUserService;
        private readonly ILogService _Logger;
        private readonly IAuthenticationService _IAuthenticationService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStudentService _IStudentService;
        private readonly IStudentMasterService _IStudentMasterService;
        private readonly IFeeService _IFeeService;
        private readonly IStaffService _IStaffService;
        private readonly IWebHelper _WebHelper;
        private readonly IContactService _IContactService;
        private readonly IMainService _IMainService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IGuardianService _IGuardianService;
        private readonly IEncryptionService _IEncryptionService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IDropDownService _IDropDownService;
        private readonly ISubjectService _ISubjectService;
        private readonly IMessengerService _IMessengerService;
        private readonly ISchedularService _ISchedularService;
        private readonly IExamService _IExamService;
        private readonly IDocumentService _IDocumentService;
        private readonly ISMSSender _ISMSSender;
        private readonly ITempOTP _ITempOTP;
        private readonly ISchoolDataService _ISchoolDataService;
        private readonly INavigationService _INavigationService;
        private readonly IUserManagementService _IUserManagementService;
        private readonly ICommonMethodService _ICommonMethodService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        #endregion

        #region ctor

        public HomeController(
            //IConnectionService IConnectionService,
            ILogService Logger,
            IUserService IUserService,
            IAuthenticationService IAuthenticationService,
            IAddressMasterService IAddressMasterService,
            ICatalogMasterService ICatalogMasterService,
            IStudentService IStudentService,
            IStudentMasterService IStudentMasterService,
            IFeeService IFeeService,
            IStaffService IStaffService,
            IWebHelper WebHelper,
            IContactService IContactService,
            IMainService IMainService,
            ISchoolSettingService ISchoolSettingService,
            IGuardianService IGuardianService,
            IEncryptionService IEncryptionService,
            ICustomEncryption ICustomEncryption,
            IDropDownService IDropDownService,
            ISubjectService ISubjectService,
            IMessengerService IMessengerService,
            ISchedularService ISchedularService,
            IExamService IExamService,
            IDocumentService IDocumentService,
            ISMSSender ISMSSender,
            ITempOTP ITempOTP, ISchoolDataService ISchoolDataService,
            INavigationService INavigationService,
            IUserManagementService IUserManagementService,
            ICommonMethodService ICommonMethodService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService)
        {
            count = 0;
            //this._IConnectionService = IConnectionService;
            this._IUserService = IUserService;
            this._Logger = Logger;
            this._IAuthenticationService = IAuthenticationService;
            this._IAddressMasterService = IAddressMasterService;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IStudentService = IStudentService;
            this._IStudentMasterService = IStudentMasterService;
            this._IFeeService = IFeeService;
            this._IStaffService = IStaffService;
            this._WebHelper = WebHelper;
            this._IContactService = IContactService;
            this._IMainService = IMainService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._IGuardianService = IGuardianService;
            this._IEncryptionService = IEncryptionService;
            this._ICustomEncryption = ICustomEncryption;
            this._IDropDownService = IDropDownService;
            this._ISubjectService = ISubjectService;
            this._IMessengerService = IMessengerService;
            this._ISchedularService = ISchedularService;
            this._IExamService = IExamService;
            this._IDocumentService = IDocumentService;
            this._ISMSSender = ISMSSender;
            this._ITempOTP = ITempOTP;
            this._ISchoolDataService = ISchoolDataService;
            this._INavigationService = INavigationService;
            this._IUserManagementService = IUserManagementService;
            this._ICommonMethodService = ICommonMethodService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._IActivityLogService = IActivityLogService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }
        public string ConvertDate(DateTime Date)
        {
            string sdisplayTime = Date.ToString("dd/MM/yyyy");
            return sdisplayTime;
        }
        #endregion

        #region Methods
        public ActionResult Index()
        {
            try
            {

                // current user
                SchoolUser user = new SchoolUser();
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }


                // get data for DashBoard
                DashBoardModel model = new DashBoardModel();
                var getcontactTye = _IAddressMasterService.GetAllContactTypes(ref count)
                                    .Where(x => x.ContactType1.ToLower() == user.ContactType.ContactType1.ToLower())
                                    .FirstOrDefault();

                //var Is_Lite = _ISchoolSettingService.GetorSetSchoolData("Is_Lite", "0").AttributeValue == "1" ? true : false;
                //_ISchoolSettingService.GetorSetSchoolData("IncludePreviousSessionFeePending", "0");
                //var CustomNavigation = _ISchoolSettingService.GetorSetSchoolData("CustomNavigation", "0").AttributeValue == "1" ? true : false;

                model.Usertype = "";
                if (getcontactTye != null)
                {
                    var SchoolUser = _IUserService.GetAllUsers(ref count).Where(x => x.UserContactId == user.UserContactId
                                                     && x.UserTypeId == getcontactTye.ContactTypeId).FirstOrDefault();
                    model.Usertype = _ICommonMethodService.GetUserSuperRole(model.Usertype, SchoolUser);
                }
                //session dropdown
                var currentSession = _ICatalogMasterService.GetCurrentSession();
                model.SessionId = currentSession.SessionId;
                foreach (var item in _ICatalogMasterService.GetAllSessions(ref count))
                {
                    if (currentSession.SessionId == item.SessionId)
                        model.SessionList.Add(new SelectListItem { Selected = true, Text = item.Session1, Value = item.SessionId.ToString() });
                    else
                        model.SessionList.Add(new SelectListItem { Selected = false, Text = item.Session1, Value = item.SessionId.ToString() });
                }
                //current session
                var session = _ICatalogMasterService.GetCurrentSession();
                //// get admitted students
                //var Status = _IStudentMasterService.GetAllStudentStatus(ref count, "Active").FirstOrDefault();
                //    var currentSessionStudentCount = _IStudentService.GetAllActiveStudentCount(ref count,
                //                                SessionId: session.SessionId, StudentStatusId :Status.StudentStatusId)
                //                                .Where(s => s.AdmnDate != null && s.AdmnNo != "").ToList();
                //model.StudentStrength = currentSessionStudentCount.Count.ToString();

                ////current year new  enrollment
                //var currentSessionEnrolledStudents = 0;
                //currentSessionEnrolledStudents = _IStudentService.GetAdmittedList(ref count, TypeId: "Admit", SessionId: session.SessionId).Count;
                //model.Enrolled = currentSessionEnrolledStudents.ToString();
                ////fees
                //var allfeereceipts = _IFeeService.GetAllFeeReceipts(ref count);
                //decimal? currentSessionTotalAmount = 0;
                //decimal? currentSessionConessionTotalAmount = 0;
                //if (allfeereceipts.Count > 0)
                //{
                //    var currentSessionFeeReceiptsIds = allfeereceipts.Where(p => p.ReceiptDate >= session.StartDate && p.ReceiptDate <= session.EndDate).Select(p => p.ReceiptId).ToList();
                //    if (currentSessionFeeReceiptsIds.Count > 0)
                //    {
                //        var allFeeReceiptsDetails = _IFeeService.GetAllFeeReceiptDetails(ref count);
                //        currentSessionTotalAmount += allFeeReceiptsDetails.Where(p => currentSessionFeeReceiptsIds.Contains((int)p.ReceiptId) && p.GroupFeeHeadId != null).Sum(p => p.Amount);
                //        currentSessionConessionTotalAmount = allFeeReceiptsDetails.Where(p => currentSessionFeeReceiptsIds.Contains((int)p.ReceiptId) && p.ConsessionId != null).Sum(p => p.Amount);
                //    }
                //}
                //model.Fees = (currentSessionTotalAmount - currentSessionConessionTotalAmount).ToString();
                //// Teacher
                //var teachers = 0;
                //var stafftypeteacher = _IStaffService.GetAllStaffTypesUpdated(ref count, IsTeacher: true).FirstOrDefault();
                //if (stafftypeteacher != null)
                //    teachers = _IStaffService.GetAllStaffs(ref count, StaffTypeId: stafftypeteacher.StaffTypeId).Count;
                //model.Teacher = teachers.ToString();

                //// Staff
                //var staffs = 0;
                //var stafftypenonteacher = _IStaffService.GetAllStaffTypesUpdated(ref count, IsTeacher: false).FirstOrDefault();
                //if (stafftypenonteacher != null)
                //    staffs = _IStaffService.GetAllStaffs(ref count, StaffTypeId: stafftypenonteacher.StaffTypeId).Count;
                //model.Staff = staffs.ToString();

                ////atendance of current day
                //var presentStudent = 0;
                //var presentStatus = _ISubjectService.GetAllAttendanceStatus(ref count, "Present").SingleOrDefault();
                //if (presentStatus != null)
                //    presentStudent = _ISubjectService.GetAllAttendances(ref count, Date: DateTime.UtcNow, AttendanceStatusId: presentStatus.AttendanceStatusId).Count;
                //model.Attendance = presentStudent.ToString() + "/" + currentSessionStudentCount.Count.ToString();

                //// unread meassage
                //model.UnreadMessage = "0";
                //var allUnReadedMessages = new List<Message>();
                //DataTable dtMsgesCount = _IMessengerService.GetAllUnreadMsgsCount(ref count, user.UserId);
                //if (dtMsgesCount.Rows.Count > 0)
                //{
                //    model.UnreadMessage = dtMsgesCount.Rows[0]["UnreadMsges"].ToString()
                //                        + "/" + dtMsgesCount.Rows[0]["TotalMsges"].ToString();
                //}

                //var allMessages = _IMessengerService.GetAllMessageList(ref count);
                //allUnReadedMessages = allMessages.Where(m => m.IsRead == false).ToList();
                //if (allMessages.Count > 0)
                //{
                //    allUnReadedMessages = allMessages.Where(p => p.IsRead == false).ToList();
                //    model.UnreadMessage = allUnReadedMessages.Count.ToString() + "/" + allMessages.Count.ToString();
                //}
                //Check student class and Standard
                var studentid = 0;

                switch (user.ContactType.ContactType1.ToLower())
                {
                    case "student":
                        studentid = (int)user.UserContactId;
                        model.Id = _ICustomEncryption.base64e(studentid.ToString());
                        break;

                    case "guardian":
                        var guardian = _IGuardianService.GetGuardianById((int)user.UserContactId);
                        studentid = guardian != null ? (int)guardian.StudentId : 0;
                        model.Id = _ICustomEncryption.base64e(studentid.ToString());
                        break;

                    default:
                        studentid = 0;
                        break;
                }

                //// get student current class
                //int studentclassid = 0;
                //int studentStandardid = 0;
                //if (studentid > 0)
                //{
                //    // student class
                //    var studentclass = _IStudentService.GetAllStudentClasss(ref count, StudentId: studentid, SessionId: session.SessionId).FirstOrDefault();
                //    if (studentclass != null && !string.IsNullOrEmpty(studentclass.RollNo))
                //    {
                //        studentclassid = (int)studentclass.ClassId;
                //        studentStandardid = (int)studentclass.ClassMaster.StandardId;
                //    }
                //}

                //var allEventTypes=_ISchedularService.GetAllEventTypeList(ref count).ToList();
                ////all type of events

                //var allEvents = _ISchedularService.GetAllEventList(ref count);
                //var allAnnoucements = allEvents.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Announcement" && p.StartDate >= DateTime.Now.Date.AddDays(-30) && (p.EndDate <= session.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                //var allHolidays = allEvents.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Holiday" && p.StartDate >= DateTime.Now.Date.AddDays(-30) && (p.EndDate <= session.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                //var allVacations = allEvents.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Vacation" && p.StartDate >= DateTime.Now.Date.AddDays(-30) && (p.EndDate <= session.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                //var allActivity = allEvents.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Activity" && p.StartDate >= DateTime.Now.Date.AddDays(-30) && (p.EndDate <= session.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();

                //var SortAnnouncement=allEventTypes.Where(m=>m.EventType1=="Announcement").FirstOrDefault().SortingMethod;
                //if (SortAnnouncement==1)
                //    allAnnoucements = allAnnoucements.OrderByDescending(p => p.StartDate).ToList();

                //var SortHoliday = allEventTypes.Where(m => m.EventType1 == "Holiday").FirstOrDefault().SortingMethod;
                //if (SortHoliday == 1)
                //    allHolidays = allHolidays.OrderByDescending(p => p.StartDate).ToList();

                //var SortVacation = allEventTypes.Where(m => m.EventType1 == "Vacation").FirstOrDefault().SortingMethod;
                //if (SortVacation == 1)
                //    allVacations = allVacations.OrderByDescending(p => p.StartDate).ToList();

                //var SortActivity = allEventTypes.Where(m => m.EventType1 == "Activity").FirstOrDefault().SortingMethod;
                //if (SortActivity == 1)
                //    allActivity = allActivity.OrderByDescending(p => p.StartDate).ToList();

                //if (allAnnoucements.Count > 0)
                //{
                //    foreach (var item in allAnnoucements)
                //    {
                //        var ActivityEvent = new ActivityEvent();
                //        ActivityEvent.StartDate = item.StartDate.HasValue ? item.StartDate : null;
                //        ActivityEvent.StartTime = item.StartTime.HasValue ? item.StartTime : null;
                //        ActivityEvent.Venue = item.Venue;
                //        ActivityEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                //        ActivityEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");

                //        model.AnnouncementList.Add(ActivityEvent);
                //    }
                //}
                //if (allHolidays.Count > 0)
                //{
                //    foreach (var item in allHolidays)
                //    {
                //        var ActivityEvent = new ActivityEvent();
                //        ActivityEvent.StartDate = item.StartDate.HasValue ? item.StartDate : null;
                //        ActivityEvent.StartTime = item.StartTime.HasValue ? item.StartTime : null;
                //        ActivityEvent.Venue = item.Venue;
                //        ActivityEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                //        ActivityEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");

                //        model.HolidayList.Add(ActivityEvent);
                //    }
                //}
                //if (allVacations.Count > 0)
                //{
                //    foreach (var item in allVacations)
                //    {
                //        var ActivityEvent = new ActivityEvent();
                //        ActivityEvent.StartDate = item.StartDate.HasValue ? item.StartDate : null;
                //        ActivityEvent.EndDate = item.EndDate.HasValue ? item.EndDate : null;
                //        ActivityEvent.StartTime = item.StartTime.HasValue ? item.StartTime : null;
                //        ActivityEvent.Venue = item.Venue;
                //        ActivityEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                //        ActivityEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");

                //        model.VacationsList.Add(ActivityEvent);
                //    }
                //}
                //if (allActivity.Count > 0)
                //{
                //    var allclasses = _ICatalogMasterService.GetAllClassMasters(ref count);
                //    bool IsExistInActivity = false;

                //    foreach (var item in allActivity)
                //    {
                //        IsExistInActivity = false;
                //        var ActivityEvent = new ActivityEvent();
                //        ActivityEvent.StartDate = item.StartDate;
                //        ActivityEvent.EndDate = item.EndDate;
                //        ActivityEvent.StartTime = item.StartTime;
                //        ActivityEvent.EndTime = item.EndTime;
                //        ActivityEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");
                //        ActivityEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                //        ActivityEvent.Venue = item.Venue;

                //        if (item.EventDetails.Count > 0)
                //        {
                //            var eventClasses = "";
                //            var classids = item.EventDetails.Select(e => e.ClassId).ToArray();
                //            eventClasses = String.Join(",", allclasses.Where(c => classids.Contains(c.ClassId)).Select(c => c.Class).ToList());

                //            if (studentclassid > 0 && classids.Contains(studentclassid))
                //            {
                //                IsExistInActivity = true;
                //                eventClasses = _ICatalogMasterService.GetClassMasterById(studentclassid).Class;
                //            }

                //            ActivityEvent.ActivityClass = eventClasses;
                //        }
                //        else
                //        {
                //            IsExistInActivity = true;
                //            ActivityEvent.ActivityClass = "All Classes";
                //        }

                //        if (studentclassid > 0)
                //        {
                //            if (IsExistInActivity)
                //                model.ActivityList.Add(ActivityEvent);
                //        }
                //        else
                //            model.ActivityList.Add(ActivityEvent);
                //    }
                //}

                ////exams
                //var Exams = _IExamService.GetAllExamDateSheetDetails(ref count, StandardId: studentStandardid).ToList();
                //var ExamDetail = Exams.GroupBy(p => p.StandardId).Select(m => m.First()).ToList();
                //var ExamResultDates = _IExamService.GetAllExamResultDates(ref count).Where(p => (Exams.Select(m => m.ExamDateSheetId).ToArray()).Contains(p.ExamDateSheetId)).ToList();
                //if (ExamDetail.Count > 0)
                //{
                //    foreach (var item in ExamDetail)
                //    {
                //        var standard = _ICatalogMasterService.GetStandardMasterById((int)item.StandardId);
                //        var RstPublishDate = ExamResultDates.Where(p => p.StandardId == item.StandardId && p.ResultPublishDate != null && p.ResultPublishDate >= DateTime.Now.Date).Select(p => p.ResultPublishDate).FirstOrDefault();
                //        if (RstPublishDate != null)
                //            model.ExamResultList.Add(new ExamResultNotification { Standard = standard.Standard, ResultDate = Convert.ToDateTime(RstPublishDate), ExamDateSheet = item.ExamDateSheet.ExamDateSheet1 });

                //    }

                //}
                //var datesheetAcctoPublisheddate = _IExamService.GetAllExamPublishDetails(ref count);
                //if (datesheetAcctoPublisheddate.Count > 0)
                //{
                //    datesheetAcctoPublisheddate = datesheetAcctoPublisheddate.Where(m => m.ExamPublishDate <= DateTime.Now.Date).ToList();
                //    Exams = Exams.Where(m => (datesheetAcctoPublisheddate.Select(n => n.ExamDateSheetId).ToArray()).Contains((int)m.ExamDateSheetId)).ToList();
                //    Exams = Exams.Where(p => p.ExamDate >= DateTime.Now.Date).OrderBy(e => e.ExamDate).ToList();
                //    if (Exams.Count > 0)
                //    {
                //        foreach (var item in Exams)
                //        {
                //            var standard = _ICatalogMasterService.GetStandardMasterById((int)item.StandardId);
                //            var subject = _ISubjectService.GetSubjectById((int)item.SubjectId);
                //            var timeSlot = item.ExamTimeSlot.ExamTimeSlot1;

                //            if (standard != null && subject != null)
                //                model.ExamsList.Add(new ExamNotification { ClassSubjectName = standard.Standard + "/" + subject.Subject1, TimeSlot = timeSlot, ExamDate = item.ExamDate });
                //        }
                //    }
                //}



                //if (user.ContactType.ContactType1.ToLower() == "admin")
                //{
                //    DateTime? TodayDate = DateTime.Now.Date;
                //    var ReminderTypes = _IDocumentService.GetAllDocReminderTypes(ref count).ToList();
                //    var WeeklyReminder = ReminderTypes.Where(f => f.ReminderType.ToLower() == "weekly").FirstOrDefault();
                //    var DailyReminder = ReminderTypes.Where(f => f.ReminderType.ToLower() == "daily").FirstOrDefault();

                //    var DocumentNotification = new DocumentNotification();

                //    var allDocumentDetails = _IDocumentService.GetAllDocumentDetail(ref count).ToList();
                //    allDocumentDetails = allDocumentDetails.Where(a => a.ExpiryDate.Value.Date >= TodayDate.Value.Date && a.ReminderDate.Value.Date <= TodayDate.Value.Date).ToList();

                //    //Weekly Reminder DocuMents                
                //    if (WeeklyReminder != null)
                //    {
                //        var weeklyDocuments = allDocumentDetails.Where(f => f.ReminderType == WeeklyReminder.ReminderTypeId).ToList();

                //        foreach (var detail in weeklyDocuments)
                //        {
                //            if (detail.ReminderDate.Value.Date <= TodayDate)
                //            {
                //                DateTime ReminderDate = detail.ReminderDate.Value.Date;
                //                var daysdiff = ((DateTime)TodayDate - ReminderDate).TotalDays;
                //                if (daysdiff == 0 || daysdiff % 7 == 0)
                //                {

                //                    DocumentNotification = new DocumentNotification();
                //                    DocumentNotification.Document = detail.Document.DocumentTitle;
                //                    DocumentNotification.DocumentCategoryId = (int)detail.Document.DocumentCategoryId;
                //                    DocumentNotification.DocumentExp = detail.ExpiryDate.Value.Date.ToString("dd/MM/yyyy");
                //                    model.DocumentList.Add(DocumentNotification);
                //                }
                //            }
                //        }

                //    }
                //    //Daily Reminder DocuMents   
                //    if (DailyReminder != null)
                //    {
                //        var dailyDocuments = allDocumentDetails.Where(f => f.ReminderType == DailyReminder.ReminderTypeId).ToList();
                //        foreach (var detail in dailyDocuments)
                //        {
                //            DocumentNotification = new DocumentNotification();
                //            DocumentNotification.Document = detail.Document.DocumentTitle;
                //            DocumentNotification.DocumentCategoryId = (int)detail.Document.DocumentCategoryId;
                //            DocumentNotification.DocumentExp = detail.ExpiryDate.Value.Date.ToString("dd/MM/yyyy");
                //            model.DocumentList.Add(DocumentNotification);
                //        }
                //    }
                //    var DocumentCategory = new DocumentCategorys();
                //    var DocumentCategorys = _IDocumentService.GetAllDocumentCategory(ref count).ToList();
                //    foreach (var categry in DocumentCategorys)
                //    {
                //        DocumentCategory = new DocumentCategorys();
                //        DocumentCategory.DocumentCategoryId = categry.DocumentCategoryId;
                //        DocumentCategory.DocumentCategory = categry.DocumentCategory1;
                //        model.DocumentCategoryList.Add(DocumentCategory);
                //    }
                //}

                // DashBoard Navigation 
                model.IsAdmin = false;
                if (user.ContactType != null && user.UserRoleId != null && user.ContactType.ContactType1.ToLower() == "admin")
                {
                    //var dynamicnavigationpath = "";
                    //var Is_Lite = _ISchoolSettingService.GetorSetSchoolData("Is_Lite", "0").AttributeValue == "1" ? true : false;
                    //if (Is_Lite)
                    //    dynamicnavigationpath = "litenavigationpath";
                    //else
                    //    dynamicnavigationpath = "navigationpath";
                    //var CustomNavigation = _ISchoolSettingService.GetorSetSchoolData("CustomNavigation", "0").AttributeValue == "1" ? true : false;
                    ////string file = "";
                    //var SchoolDbId = Convert.ToString(System.Web.HttpContext.Current.Session["SchoolId"]);

                    //string file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath])
                    //           + Convert.ToString(WebConfigurationManager.AppSettings["userroletypejson"]));
                    //////deserialize JSON from file  
                    //string Json = System.IO.File.ReadAllText(file);
                    //JavaScriptSerializer ser = new JavaScriptSerializer();

                    //var list = ser.Deserialize<List<UserRoleTypeListModel>>(Json);
                    //if (list.Count > 0)
                    //{
                    //    var getRole = list.Where(x => x.UserRoleType.ToLower() == "admin").FirstOrDefault();
                    //    model.UserTypeRoleId = getRole != null ? getRole.UserRoleTypeId.ToString() : "";
                    //}

                    model.IsAdmin = true;
                    var UserRole = _IUserService.GetUserRoleById((int)user.UserRoleId);
                    if (UserRole != null && UserRole.UserRoleTypeId != null)
                        model.UserRoleTypeId = (int)UserRole.UserRoleTypeId;
                    //var TypeId = ;
                    model.IsDashBoardNavigationShow = _ISchoolSettingService.GetorSetSchoolData("IsDashBoardNavigationShow", "1").AttributeValue == "1" ? true : false;
                    model.IsDashBoardPermissionShow = _ISchoolSettingService.GetorSetSchoolData("IsDashBoardPermissionShow", "1").AttributeValue == "1" ? true : false;

                    model.UserRoleId = (int)user.UserRoleId;
                    model.UserRoleTypeId = model.UserRoleTypeId;
                    var userrole = _IUserService.GetUserRoleById(UserRoleId: model.UserRoleId).UserRole1;
                    model.UserRole = userrole;
                    var dynamicnavigationpath = "";
                    var Is_Lite = _ISchoolSettingService.GetorSetSchoolData("Is_Lite", "0").AttributeValue == "1" ? true : false;
                    if (Is_Lite)
                        dynamicnavigationpath = "litenavigationpath";
                    else
                        dynamicnavigationpath = "navigationpath";

                    var CustomNavigation = _ISchoolSettingService.GetorSetSchoolData("CustomNavigation", "0").AttributeValue == "1" ? true : false;

                    string file = "";
                    var SchoolDbId = Convert.ToString(System.Web.HttpContext.Current.Session["SchoolId"]);


                    if (model.IsDashBoardNavigationShow == true)
                    {
                        try
                        {
                            //get the Json filepath  
                            if (CustomNavigation)
                            {
                                file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + "/" + SchoolDbId + "/" + Convert.ToString(WebConfigurationManager.AppSettings["leftjson"]));
                            }
                            else
                            {
                                file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(WebConfigurationManager.AppSettings["leftjson"]));
                            }
                            //deserialize JSON from file  
                            string Json = System.IO.File.ReadAllText(file);
                            JavaScriptSerializer ser = new JavaScriptSerializer();
                            var list = ser.Deserialize<List<UserNavigationPermissionModel>>(Json);

                            var IsNavigationExist = _INavigationService.GetAllUserRoleNavigationItems(ref count).Where(p => p.UserRoleId == model.UserRoleId && p.IsActive == true).ToList();
                            if (IsNavigationExist.Count > 0)
                            {
                                var navmodel = new GrandNavigationModel();
                                IList<GrandNavigationModel> grandList = new List<GrandNavigationModel>();
                                for (int i = 0; i < list.Count; i++)
                                {
                                    model.List.Add(new ViewModel.ViewModel.Security.UserNavigationPermissionModel { });
                                    model.List[i].user_role_place = list[i].user_role_place;
                                    model.List[i].user_role_type = list[i].user_role_type;
                                    model.List[i].user_role_type_id = list[i].user_role_type_id;
                                    if (list[i].user_role_type_id == model.UserRoleTypeId)
                                    {
                                        int grand = 0;
                                        int parent = 0;
                                        int child = 0;
                                        int grnd = -1;
                                        int prnt = -1;
                                        //grand
                                        for (int m = 0; m < list[i].navigation.Count; m++)
                                        {
                                            grand = 0; parent = 0; child = 0; prnt = -1;
                                            if (IsNavigationExist.Where(p => p.NavigationItemId == list[i].navigation[m].NavigationItemId).FirstOrDefault() != null && IsNavigationExist.Where(p => p.UserRoleTypeId == list[i].user_role_type_id).FirstOrDefault() != null) { }
                                            // model.List[i].navigation.Add(new KS_SmartChild.ViewModel.Security.UserParentNavigationPermissionModel { NavigationItemId = list[i].navigation[m].NavigationItemId, NavigationItem = list[i].navigation[m].NavigationItem, IsActive = true });
                                            else
                                            {
                                                grandList.Add(new GrandNavigationModel { NavigationItemId = list[i].navigation[m].NavigationItemId, NavigationItem = list[i].navigation[m].NavigationItem, IsActive = false });
                                                grand = 1;
                                                grnd++;
                                            }
                                            //parent
                                            for (int j = 0; j < list[i].navigation[m].Child.Count; j++)
                                            {
                                                parent = 0; child = 0;
                                                if (IsNavigationExist.Where(p => p.NavigationItemId == list[i].navigation[m].Child[j].NavigationItemId).FirstOrDefault() != null && IsNavigationExist.Where(p => p.UserRoleTypeId == list[i].user_role_type_id).FirstOrDefault() != null) { }
                                                //model.List[i].navigation[m].Child.Add(new KS_SmartChild.ViewModel.Security.UserSecondLevNavigationPermissionModel { NavigationItemId = list[i].navigation[m].Child[j].NavigationItemId, NavigationItem = list[i].navigation[m].Child[j].NavigationItem, IsActive = true });
                                                else
                                                {
                                                    if (grand == 0)
                                                    {
                                                        grandList.Add(new GrandNavigationModel { NavigationItemId = list[i].navigation[m].NavigationItemId, NavigationItem = list[i].navigation[m].NavigationItem, IsActive = false });
                                                        grand = 1;
                                                        grnd++;
                                                    }
                                                    grandList[grnd].ParentList.Add(new ParentNavigationModel { NavigationItemId = list[i].navigation[m].Child[j].NavigationItemId, NavigationItem = list[i].navigation[m].Child[j].NavigationItem, IsActive = false });
                                                    parent = 1;
                                                    prnt++;
                                                }
                                                //child
                                                for (int k = 0; k < list[i].navigation[m].Child[j].Child.Count; k++)
                                                {
                                                    child = 0;
                                                    if (IsNavigationExist.Where(p => p.NavigationItemId == list[i].navigation[m].Child[j].Child[k].NavigationItemId).FirstOrDefault() != null && IsNavigationExist.Where(p => p.UserRoleTypeId == list[i].user_role_type_id).FirstOrDefault() != null) { }
                                                    // model.List[i].navigation[m].Child[j].Child.Add(new KS_SmartChild.ViewModel.Security.UserThirdLevNavigationPermissionModel { NavigationItemId = list[i].navigation[m].Child[j].Child[k].NavigationItemId, NavigationItem = list[i].navigation[m].Child[j].Child[k].NavigationItem, IsActive = true });
                                                    else
                                                    {
                                                        if (grand == 0)
                                                        {
                                                            grandList.Add(new GrandNavigationModel { NavigationItemId = list[i].navigation[m].NavigationItemId, NavigationItem = list[i].navigation[m].NavigationItem, IsActive = false });
                                                            grand = 1;
                                                            grnd++;
                                                        }
                                                        if (parent == 0)
                                                        {
                                                            grandList[grnd].ParentList.Add(new ParentNavigationModel { NavigationItemId = list[i].navigation[m].Child[j].NavigationItemId, NavigationItem = list[i].navigation[m].Child[j].NavigationItem, IsActive = false });
                                                            parent = 1;
                                                            prnt++;
                                                        }
                                                        grandList[grnd].ParentList[prnt].ChildList.Add(new ChildNavigationModel { NavigationItemId = list[i].navigation[m].Child[j].Child[k].NavigationItemId, NavigationItem = list[i].navigation[m].Child[j].Child[k].NavigationItem, IsActive = false });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                model.NavigationList = grandList;
                                model.IsNavigationWithoutException = true;
                            }

                        }
                        catch (Exception ex)
                        {
                            model.IsNavigationWithoutException = false;
                            //    throw;
                        }
                    }
                    if (model.IsDashBoardPermissionShow == true)
                    {
                        try
                        {
                            model.IsPermissionActionWithoutException = true;
                            UserRoleModulePermissin UserRoleModulePermissin = new UserRoleModulePermissin(); // Module
                            UserRoleAppForms UserRoleAppForms = new UserRoleAppForms(); // App from Obj
                            UserRoleChildAppForms UserRoleChildAppForms = new UserRoleChildAppForms(); // App from Obj
                            UserRoleChildAppForms1 UserRoleChildAppForms1 = new UserRoleChildAppForms1(); // App from Obj
                            UserRoleAppFormActions UserRoleAppFormActions = new UserRoleAppFormActions(); // App form action obj

                            if (CustomNavigation)
                            {
                                file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + "/" + SchoolDbId + "/" + Convert.ToString(WebConfigurationManager.AppSettings["userroleappformjson"]));
                            }
                            else
                            {
                                file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(WebConfigurationManager.AppSettings["userroleappformjson"]));
                            }
                            //string file = System.Web.HttpContext.Current.Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings["userroleappformjson"]));
                            //deserialize JSON from file
                            string Json = System.IO.File.ReadAllText(file);
                            System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                            var list = ser.Deserialize<List<AppFormPermissionModel>>(Json);

                            var appformpermissiondata = list.Where(p => p.UserRoleTypeId == model.UserRoleTypeId.ToString()).FirstOrDefault().AppFormData;
                            appformpermissiondata = appformpermissiondata.Where(p => p.NavigationItemId != null).ToList();
                            var AllUserRoleActionPermissions = _IUserManagementService.GetAllUserRoleActionPermissions(ref count, UserRoleId: Convert.ToInt32(model.UserRoleId));

                            var AllUserRoleNavigation= _INavigationService.GetAllUserRoleNavigationItems(ref count).Where(f=>f.UserRoleTypeId==model.UserRoleTypeId && f.UserRoleId==model.UserRoleId && f.IsActive==true).ToList();
                            foreach (var item in appformpermissiondata)
                            {
                                if (AllUserRoleNavigation.Where(f => f.NavigationItemId == Convert.ToInt32(item.NavigationItemId)).Count() > 0)
                                {
                                    UserRoleModulePermissin = new UserRoleModulePermissin();
                                    UserRoleAppForms = new UserRoleAppForms();
                                    UserRoleAppForms.AppForm = item.AppForm;
                                    UserRoleAppForms.AppFormId = item.AppFormId;
                                    UserRoleAppForms.IsPermitted = false;
                                    UserRoleAppForms.IsForm = true;

                                    var appformid = Convert.ToInt32(item.AppFormId);
                                    var ThisUserRoleActionPermissionsList = AllUserRoleActionPermissions.Where(f => f.AppFormId == appformid).ToList();
                                    var Isuserrolepermission = ThisUserRoleActionPermissionsList.FirstOrDefault();
                                    if (Isuserrolepermission != null)
                                    {

                                        UserRoleAppForms.UserRoleFormPermissionId = Isuserrolepermission.UserRoleActionPermissionId.ToString();
                                        UserRoleAppForms.IsPermitted = Isuserrolepermission.IsPermitted != null && Isuserrolepermission.IsPermitted == true ? true : false;

                                        var userroleactionpermissions = ThisUserRoleActionPermissionsList;
                                        var appformactionids = userroleactionpermissions.Where(u => u.IsForm == false || u.IsForm == null).Select(u => u.AppFormActionId.ToString()).ToArray();


                                        var appformactions = new List<ViewModel.ViewModel.Security.Action>();
                                        foreach (var action in item.Action)
                                            appformactions.Add(action);

                                        if (appformactionids.Count() > 0)
                                            appformactions = appformactions.Where(a => appformactionids.Contains(a.AppFormActionId)).ToList();

                                        foreach (var action in appformactions)
                                        {
                                            UserRoleAppFormActions = new UserRoleAppFormActions();
                                            UserRoleAppFormActions.AppFormAction = action.FormActionType;
                                            UserRoleAppFormActions.AppFormActionId = action.AppFormActionId;
                                            UserRoleAppFormActions.IsPermitted = false;
                                            UserRoleAppFormActions.IsForm = false;

                                            var appformactionid = Convert.ToInt32(action.AppFormActionId);

                                            // check if App from Action permission already exist
                                            var Isuserroleactionpermission = userroleactionpermissions.Where(u => u.AppFormActionId == appformactionid).FirstOrDefault();
                                            if (Isuserroleactionpermission != null)
                                            {
                                                UserRoleAppFormActions.UserRoleActionPermissionId = Isuserroleactionpermission.UserRoleActionPermissionId.ToString();
                                                UserRoleAppFormActions.IsPermitted = Isuserroleactionpermission.IsPermitted != null && Isuserroleactionpermission.IsPermitted == true ? true : false;
                                            }
                                            if (UserRoleAppFormActions.IsPermitted != true)
                                                UserRoleAppForms.UserRoleAppFormActionsList.Add(UserRoleAppFormActions);
                                        }

                                        foreach (var itm in item.ChildAppForm)
                                        {
                                            if (AllUserRoleNavigation.Where(f => f.NavigationItemId == Convert.ToInt32(itm.NavigationItemId)).Count() > 0)
                                            {
                                                UserRoleChildAppForms = new UserRoleChildAppForms();
                                                UserRoleChildAppForms.AppForm = itm.AppForm;
                                                UserRoleChildAppForms.AppFormId = itm.AppFormId;
                                                UserRoleChildAppForms.IsPermitted = false;
                                                UserRoleChildAppForms.IsForm = true;


                                                var childappformid = Convert.ToInt32(itm.AppFormId);


                                                var Ischilduserrolepermission = AllUserRoleActionPermissions.Where(f => f.AppFormId == childappformid).FirstOrDefault();
                                                var ThisuserrolechildactionpermissionsList = AllUserRoleActionPermissions.Where(f => f.AppFormId == childappformid).ToList();
                                                if (Ischilduserrolepermission != null)
                                                {

                                                    UserRoleChildAppForms.UserRoleFormPermissionId = Ischilduserrolepermission.UserRoleActionPermissionId.ToString();
                                                    UserRoleChildAppForms.IsPermitted = Ischilduserrolepermission.IsPermitted != null && Ischilduserrolepermission.IsPermitted == true ? true : false;

                                                    var userrolechildactionpermissions = ThisuserrolechildactionpermissionsList;
                                                    var childappformactionids = userrolechildactionpermissions.Where(u => u.IsForm == false || u.IsForm == null).Select(u => u.AppFormActionId.ToString()).ToArray();

                                                    var childappformactions = new List<ViewModel.ViewModel.Security.Action>();
                                                    foreach (var action in itm.Action)
                                                        childappformactions.Add(action);


                                                    foreach (var action in childappformactions)
                                                    {
                                                        UserRoleAppFormActions = new UserRoleAppFormActions();
                                                        UserRoleAppFormActions.AppFormAction = action.FormActionType;
                                                        UserRoleAppFormActions.AppFormActionId = action.AppFormActionId;
                                                        UserRoleAppFormActions.IsPermitted = false;
                                                        UserRoleAppFormActions.IsForm = false;


                                                        var appformactionid = Convert.ToInt32(action.AppFormActionId);

                                                        // check if App from Action permission already exist
                                                        var Isuserroleactionpermission = userrolechildactionpermissions.Where(u => u.AppFormActionId == appformactionid).FirstOrDefault();
                                                        if (Isuserroleactionpermission != null)
                                                        {
                                                            UserRoleAppFormActions.UserRoleActionPermissionId = Isuserroleactionpermission.UserRoleActionPermissionId.ToString();
                                                            UserRoleAppFormActions.IsPermitted = Isuserroleactionpermission.IsPermitted != null && Isuserroleactionpermission.IsPermitted == true ? true : false;
                                                        }
                                                        if (UserRoleAppFormActions.IsPermitted != true)
                                                        {
                                                            UserRoleChildAppForms.UserRoleAppFormActionsList.Add(UserRoleAppFormActions);
                                                        }
                                                    }
                                                    //loop for masters
                                                    foreach (var form in itm.ChildAppForm1)
                                                    {
                                                        if (AllUserRoleNavigation.Where(f => f.NavigationItemId == Convert.ToInt32(form.NavigationItemId)).Count() > 0)
                                                        {
                                                            UserRoleChildAppForms1 = new UserRoleChildAppForms1();
                                                            UserRoleChildAppForms1.AppForm = form.AppForm;
                                                            UserRoleChildAppForms1.AppFormId = form.AppFormId;
                                                            UserRoleChildAppForms1.IsPermitted = false;
                                                            UserRoleChildAppForms1.IsForm = true;


                                                            var childappformid1 = Convert.ToInt32(form.AppFormId);
                                                            var Ischilduserrolepermission1 = AllUserRoleActionPermissions.Where(f => f.AppFormId == childappformid1).FirstOrDefault();
                                                            var Thisuserrolechildactionpermissions1List = AllUserRoleActionPermissions.Where(f => f.AppFormId == childappformid1);

                                                            if (Ischilduserrolepermission1 != null)
                                                            {
                                                                UserRoleChildAppForms1.UserRoleFormPermissionId = Ischilduserrolepermission1.UserRoleActionPermissionId.ToString();
                                                                UserRoleChildAppForms1.IsPermitted = Ischilduserrolepermission1.IsPermitted != null && Ischilduserrolepermission1.IsPermitted == true ? true : false;

                                                                // check if App from Action permission already exist
                                                                var userrolechildactionpermissions1 = Thisuserrolechildactionpermissions1List;
                                                                var childappformactionids1 = userrolechildactionpermissions1.Where(u => u.IsForm == false || u.IsForm == null).Select(u => u.AppFormActionId.ToString()).ToArray();

                                                                var childappformactions1 = new List<ViewModel.ViewModel.Security.Action>();
                                                                foreach (var action in form.Action)
                                                                    childappformactions1.Add(action);

                                                                if (childappformactionids1.Count() > 0)
                                                                    childappformactions1 = childappformactions1.Where(a => childappformactionids1.Contains(a.AppFormActionId)).ToList();

                                                                foreach (var action in childappformactions1)
                                                                {
                                                                    UserRoleAppFormActions = new UserRoleAppFormActions();
                                                                    UserRoleAppFormActions.AppFormAction = action.FormActionType;
                                                                    UserRoleAppFormActions.AppFormActionId = action.AppFormActionId;
                                                                    UserRoleAppFormActions.IsPermitted = false;
                                                                    UserRoleAppFormActions.IsForm = false;


                                                                    var appformactionid = Convert.ToInt32(action.AppFormActionId);

                                                                    // check if App from Action permission already exist
                                                                    var Isuserroleactionpermission = userrolechildactionpermissions.Where(u => u.AppFormActionId == appformactionid).FirstOrDefault();
                                                                    if (Isuserroleactionpermission != null)
                                                                    {
                                                                        UserRoleAppFormActions.UserRoleActionPermissionId = Isuserroleactionpermission.UserRoleActionPermissionId.ToString();
                                                                        UserRoleAppFormActions.IsPermitted = Isuserroleactionpermission.IsPermitted != null && Isuserroleactionpermission.IsPermitted == true ? true : false;
                                                                    }
                                                                    if (UserRoleAppFormActions.IsPermitted != true)
                                                                    {
                                                                        UserRoleChildAppForms1.UserRoleAppFormActionsList.Add(UserRoleAppFormActions);
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                var childappformactions1 = new List<ViewModel.ViewModel.Security.Action>();
                                                                foreach (var action in form.Action)
                                                                    childappformactions1.Add(action);
                                                                foreach (var action in childappformactions1)
                                                                {
                                                                    UserRoleAppFormActions = new UserRoleAppFormActions();
                                                                    UserRoleAppFormActions.AppFormAction = action.FormActionType;
                                                                    UserRoleAppFormActions.AppFormActionId = action.AppFormActionId;
                                                                    UserRoleAppFormActions.IsPermitted = false;
                                                                    UserRoleAppFormActions.IsForm = false;
                                                                    UserRoleChildAppForms1.UserRoleAppFormActionsList.Add(UserRoleAppFormActions);
                                                                }

                                                            }
                                                            if (UserRoleChildAppForms1.UserRoleAppFormActionsList.Where(f => f.IsPermitted != true).Count() > 0)
                                                                UserRoleChildAppForms.UserRoleChildAppFormsList.Add(UserRoleChildAppForms1);
                                                        }
                                                    }
                                                }
                                                else
                                                {


                                                    var childappformactions = new List<ViewModel.ViewModel.Security.Action>();
                                                    foreach (var action in itm.Action)
                                                        childappformactions.Add(action);
                                                    foreach (var action in childappformactions)
                                                    {
                                                        UserRoleAppFormActions = new UserRoleAppFormActions();
                                                        UserRoleAppFormActions.AppFormAction = action.FormActionType;
                                                        UserRoleAppFormActions.AppFormActionId = action.AppFormActionId;
                                                        UserRoleAppFormActions.IsPermitted = false;
                                                        UserRoleAppFormActions.IsForm = false;


                                                        UserRoleChildAppForms.UserRoleAppFormActionsList.Add(UserRoleAppFormActions);
                                                    }

                                                    //loop for masters
                                                    foreach (var form in itm.ChildAppForm1)
                                                    {

                                                        UserRoleChildAppForms1 = new UserRoleChildAppForms1();
                                                        UserRoleChildAppForms1.AppForm = form.AppForm;
                                                        UserRoleChildAppForms1.AppFormId = form.AppFormId;
                                                        UserRoleChildAppForms1.IsPermitted = false;
                                                        UserRoleChildAppForms1.IsForm = true;


                                                        var childappformid1 = Convert.ToInt32(form.AppFormId);


                                                        var childappformactions1 = new List<ViewModel.ViewModel.Security.Action>();
                                                        foreach (var action in form.Action)
                                                            childappformactions1.Add(action);
                                                        foreach (var action in childappformactions1)
                                                        {
                                                            UserRoleAppFormActions = new UserRoleAppFormActions();
                                                            UserRoleAppFormActions.AppFormAction = action.FormActionType;
                                                            UserRoleAppFormActions.AppFormActionId = action.AppFormActionId;
                                                            UserRoleAppFormActions.IsPermitted = false;
                                                            UserRoleAppFormActions.IsForm = false;
                                                            UserRoleChildAppForms1.UserRoleAppFormActionsList.Add(UserRoleAppFormActions);
                                                        }
                                                        UserRoleChildAppForms.UserRoleChildAppFormsList.Add(UserRoleChildAppForms1);
                                                    }
                                                }
                                                if (UserRoleChildAppForms.UserRoleChildAppFormsList.Where(f => f.IsPermitted == false || f.UserRoleAppFormActionsList.Count()>0).Count() > 0 || UserRoleChildAppForms.UserRoleAppFormActionsList.Where(f => f.IsPermitted == false).Count() > 0)
                                                    UserRoleAppForms.UserRoleChildAppFormsList.Add(UserRoleChildAppForms);
                                            }
                                        }
                                        if (UserRoleAppForms.UserRoleAppFormActionsList.Count() > 0 || UserRoleAppForms.UserRoleChildAppFormsList.Count() > 0)
                                            UserRoleModulePermissin.UserRoleAppFormsList.Add(UserRoleAppForms);
                                    }
                                    else
                                    {
                                        var appformactions = new List<ViewModel.ViewModel.Security.Action>();
                                        foreach (var action in item.Action)
                                            appformactions.Add(action);

                                        foreach (var action in appformactions)
                                        {
                                            UserRoleAppFormActions = new UserRoleAppFormActions();
                                            UserRoleAppFormActions.AppFormAction = action.FormActionType;
                                            UserRoleAppFormActions.AppFormActionId = action.AppFormActionId;
                                            UserRoleAppFormActions.IsPermitted = false;
                                            UserRoleAppFormActions.IsForm = false;

                                            UserRoleAppForms.UserRoleAppFormActionsList.Add(UserRoleAppFormActions);
                                        }

                                        foreach (var itm in item.ChildAppForm)
                                        {
                                            UserRoleChildAppForms = new UserRoleChildAppForms();
                                            UserRoleChildAppForms.AppForm = itm.AppForm;
                                            UserRoleChildAppForms.AppFormId = itm.AppFormId;
                                            UserRoleChildAppForms.IsPermitted = false;
                                            UserRoleChildAppForms.IsForm = true;



                                            var childappformactions = new List<ViewModel.ViewModel.Security.Action>();
                                            foreach (var action in itm.Action)
                                                childappformactions.Add(action);

                                            foreach (var action in childappformactions)
                                            {
                                                UserRoleAppFormActions = new UserRoleAppFormActions();
                                                UserRoleAppFormActions.AppFormAction = action.FormActionType;
                                                UserRoleAppFormActions.AppFormActionId = action.AppFormActionId;
                                                UserRoleAppFormActions.IsPermitted = false;
                                                UserRoleAppFormActions.IsForm = false;

                                                UserRoleChildAppForms.UserRoleAppFormActionsList.Add(UserRoleAppFormActions);
                                            }

                                            foreach (var form in itm.ChildAppForm1)
                                            {
                                                UserRoleChildAppForms1 = new UserRoleChildAppForms1();
                                                UserRoleChildAppForms1.AppForm = form.AppForm;
                                                UserRoleChildAppForms1.AppFormId = form.AppFormId;
                                                UserRoleChildAppForms1.IsPermitted = false;
                                                UserRoleChildAppForms1.IsForm = true;



                                                var childappformactions1 = new List<ViewModel.ViewModel.Security.Action>();
                                                foreach (var action in form.Action)
                                                    childappformactions1.Add(action);

                                                foreach (var action in childappformactions1)
                                                {
                                                    UserRoleAppFormActions = new UserRoleAppFormActions();
                                                    UserRoleAppFormActions.AppFormAction = action.FormActionType;
                                                    UserRoleAppFormActions.AppFormActionId = action.AppFormActionId;
                                                    UserRoleAppFormActions.IsPermitted = false;
                                                    UserRoleAppFormActions.IsForm = false;

                                                    UserRoleChildAppForms1.UserRoleAppFormActionsList.Add(UserRoleAppFormActions);
                                                }
                                                UserRoleChildAppForms.UserRoleChildAppFormsList.Add(UserRoleChildAppForms1);
                                            }
                                            UserRoleAppForms.UserRoleChildAppFormsList.Add(UserRoleChildAppForms);
                                        }

                                        UserRoleModulePermissin.UserRoleAppFormsList.Add(UserRoleAppForms);
                                    }
                                    if (UserRoleModulePermissin.UserRoleAppFormsList.Count() > 0)
                                        model.UserRoleModulePermissinList.Add(UserRoleModulePermissin);
                                }
                            }
                            model.IsPermissionActionWithoutException = true;
                        }
                        catch (Exception ex)
                        {
                            model.IsPermissionActionWithoutException = false;
                        }
                    }
                }
                return View(model);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return RedirectToAction("General", "Error");
            }
        }
        public UserRoleModel prepareuserrolemodel(UserRoleModel model)
        {
            model.AvailableContactType = _IDropDownService.ContactTypeList();
            var dynamicnavigationpath = "";
            var Is_Lite = _ISchoolSettingService.GetorSetSchoolData("Is_Lite", "0").AttributeValue == "1" ? true : false;
            if (Is_Lite)
                dynamicnavigationpath = "litenavigationpath";
            else
                dynamicnavigationpath = "navigationpath";
            var CustomNavigation = _ISchoolSettingService.GetorSetSchoolData("CustomNavigation", "0").AttributeValue == "1" ? true : false;
            //string file = "";
            var SchoolDbId = Convert.ToString(System.Web.HttpContext.Current.Session["SchoolId"]);
            ////get the Json filepath  
            //if (CustomNavigation)
            //{
            //    file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + "/" + SchoolDbId + "/" + Convert.ToString(WebConfigurationManager.AppSettings["userroletypejson"]));
            //}
            //else
            //{
            //    file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(WebConfigurationManager.AppSettings["userroletypejson"]));
            //}
            string file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath])
                                + Convert.ToString(WebConfigurationManager.AppSettings["userroletypejson"]));
            ////deserialize JSON from file  
            string Json = System.IO.File.ReadAllText(file);
            JavaScriptSerializer ser = new JavaScriptSerializer();

            var list = ser.Deserialize<List<UserRoleTypeListModel>>(Json);
            if (list.Count > 0)
            {
                model.UserRoleTypeList.Add(new SelectListItem { Selected = false, Value = "", Text = "--Select--" });
                foreach (var item in list)
                    model.UserRoleTypeList.Add(new SelectListItem { Selected = false, Value = item.UserRoleTypeId.ToString(), Text = item.UserRoleType });

                var getRole = list.Where(x => x.UserRoleType.ToLower() == "admin").FirstOrDefault();
                model.UserTypeRoleId = getRole != null ? getRole.UserRoleTypeId.ToString() : "";
            }
            string file1 = "";
            if (CustomNavigation)
            {
                file1 = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + "/" + SchoolDbId + "/" + Convert.ToString(WebConfigurationManager.AppSettings["leftjson"]));
            }
            else
            {
                file1 = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(WebConfigurationManager.AppSettings["leftjson"]));
            }
            //string file1 = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(WebConfigurationManager.AppSettings["leftjson"]));
            //deserialize JSON from file  
            var UserRoleNavigationItems = _INavigationService.GetAllUserRoleNavigationItems(ref count).ToList();
            var allactionpermissions = _IUserManagementService.GetAllUserRoleActionPermissions(ref count).ToList();
            // user role list
            // get all user role
            var userrolelist = _IUserService.GetAllUserRoles(ref count);
            foreach (var item in userrolelist)
            {
                UserRoleList UserRoleList = new UserRoleList();
                UserRoleList.UserRoleId = item.UserRoleId;

                //in admin case only
                UserRoleList.UserContactId = "";
                if (!string.IsNullOrEmpty(model.UserTypeRoleId))
                {
                    int UserRoleType_Id = Convert.ToInt32(model.UserTypeRoleId);
                    if (item.UserRole1.ToLower() == "administrator" && item.UserRoleTypeId == (int)UserRoleType_Id)
                    {
                        var getAdminschoolUser = _IUserService.GetAllUsers(ref count).Where(x => x.ContactType.ContactType1.ToLower() == "admin"
                                                && x.UserRoleId == item.UserRoleId).FirstOrDefault();
                        if (getAdminschoolUser != null)
                        {
                            UserRoleList.UserContactId = _ICustomEncryption.base64e(getAdminschoolUser.UserContactId.ToString());
                        }
                    }
                }

                UserRoleList.EncUserRoleId = _ICustomEncryption.base64e(item.UserRoleId.ToString());
                UserRoleList.UserRole = item.UserRole1;
                UserRoleList.ContactType = item.UserRoleContactTypes.FirstOrDefault() != null ? item.UserRoleContactTypes.FirstOrDefault().ContactType.ContactType1 : "";
                UserRoleList.ContactTypeId = item.UserRoleContactTypes.FirstOrDefault() != null ? item.UserRoleContactTypes.FirstOrDefault().ContactTypeId : 0;
                UserRoleList.UserRoleContactTypeId = item.UserRoleContactTypes.FirstOrDefault() != null ? item.UserRoleContactTypes.FirstOrDefault().UserRoleContactTypeId : 0;
                UserRoleList.Status = item.IsActive != null && item.IsActive == true ? "True" : "False";
                UserRoleList.IsDeleted = true;
                UserRoleList.UserRoleType = list.Where(p => p.UserRoleTypeId == item.UserRoleTypeId).FirstOrDefault() != null ? list.Where(p => p.UserRoleTypeId == item.UserRoleTypeId).FirstOrDefault().UserRoleType : "";
                UserRoleList.UserRoleTypeId = list.Where(p => p.UserRoleTypeId == item.UserRoleTypeId).FirstOrDefault() != null ? list.Where(p => p.UserRoleTypeId == item.UserRoleTypeId).FirstOrDefault().UserRoleTypeId : null;
                var IsNavigationPerExist = UserRoleNavigationItems.Where(p => p.UserRoleId == item.UserRoleId).ToList();
                if (IsNavigationPerExist.Count > 0)
                    UserRoleList.IsNavigationPerExist = true;

                var IsActionPerExist = allactionpermissions.Where(p => p.UserRoleId == item.UserRoleId).ToList();
                if (IsActionPerExist.Count > 0)
                    UserRoleList.IsActionPerExist = true;

                if (item.UserMobNavigationItems.Count > 0 || item.UserRoleActionPermissions.Count > 0 || UserRoleNavigationItems.Where(p => p.UserRoleId == item.UserRoleId && p.IsActive == true).Count() > 0)
                    UserRoleList.IsDeleted = false;

                model.UserRoleList.Add(UserRoleList);
            }

            model.AvailableStaffs = _IDropDownService.AllActiveStaffs(true);

            return model;
        }
        public DataTable dtGetKey()
        {
            DataTable dt = new DataTable();
            SqlConnection connection;
            SqlCommand command;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string DataSource = Convert.ToString(WebConfigurationManager.AppSettings["DataSource"]);
            string Password = Convert.ToString(WebConfigurationManager.AppSettings["SuperDatabasePassword"]);
            string Catatlog = Convert.ToString(WebConfigurationManager.AppSettings["SuperDatabase"]);
            string UserId = Convert.ToString(WebConfigurationManager.AppSettings["SuperDatabaseUsername"]);
            string sqlConnectionString = @"Data Source=" + DataSource.ToString() + ";Initial Catalog=" + Catatlog.ToString() + ";User ID=" + UserId.ToString() + ";Password=" + Password.ToString() + "";
            connection = new SqlConnection(sqlConnectionString);
            connection.Open();
            command = new SqlCommand("Select * from DeviceType", connection);
            adapter.SelectCommand = command;
            adapter.Fill(dt);
            connection.Close();
            return dt;
        }
        // login 
        public ActionResult Login()
        {
            try
            {
                // current user
                SchoolUser user = new SchoolUser();
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    if (HttpContext.Session["LoginUserId"] != null)
                    {
                        return RedirectToAction("Index");
                    }
                    return View();
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            SchoolUser user = new SchoolUser();
            UserLoginCheck userchekdetail = new UserLoginCheck();
            ContactType usertypecheck = new ContactType();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                if (ModelState.IsValid)
                {
                    if (model.UserName != null)
                        model.UserName = model.UserName.Trim();

                    var loginResult = _IUserService.ValidateUserWithLoginCheck(out user, out userchekdetail, out usertypecheck, model.UserName, model.Password);

                    var ActivateSurveillanceAlarms = _ISchoolSettingService.GetorSetSchoolData("ActivateSurveillanceAlarms", "0").AttributeValue;
                    _ISchoolSettingService.GetorSetSchoolData("IncludePreviousSessionFeePending", "0");
                    var countdown = _ISchoolSettingService.GetorSetSchoolData("MessagePopupTimeBeforelogout", "300").AttributeValue;

                    switch (loginResult)
                    {
                        case UserLoginResults.Successful:
                            {
                                //optimization
                                //user = _IUserService.GetUserByEmail(model.UserName);
                                // var usercheck = _IUserService.GetAllUserLoginChecks(ref count, UserId: user.UserId);
                                TimeSpan? usercheckstarttime = DateTime.Now.TimeOfDay;
                                TimeSpan? usercheckstimetill = null;
                                if (userchekdetail != null)
                                {
                                    //var userchekdetail = usercheck;
                                    //if (userchekdetail.TimeFrom != null)
                                    //{
                                    //    usercheckstarttime = userchekdetail.TimeFrom;
                                    //}
                                    if (userchekdetail.TimeTill != null)
                                    {
                                        usercheckstimetill = userchekdetail.TimeTill;
                                    }
                                    TimeSpan timediffs = (TimeSpan)usercheckstimetill - (TimeSpan)usercheckstarttime;
                                    var timediff = (int)TimeSpan.Parse(Convert.ToString(timediffs)).TotalSeconds;
                                    TimeSpan Timediffprio5mins = timediffs.Add(new TimeSpan(0, -1, 0));
                                    var Timediffprio5min = (int)TimeSpan.Parse(Convert.ToString(Timediffprio5mins)).TotalSeconds;

                                }

                                //var usertypecheck = _IAddressMasterService.GetContactTypeById((int)user.UserTypeId);
                                if (usertypecheck.ContactType1.ToLower() == "student")
                                {
                                    var studentclass = _IStudentService.GetAllStudentClasss(ref count).Where(m => m.Student.StudentId == user.UserContactId && m.ClassId != null).FirstOrDefault();
                                    if (studentclass == null)
                                    {

                                        ModelState.AddModelError("UserName", "Class Not Assigned");
                                        return View(model);
                                    }
                                }
                                //sign in new customer
                                string Host = Request.Url.AbsolutePath;
                                Host = "_" + Host.Split('/')[1];
                                if (Request.Url.AbsoluteUri.Contains("localhost"))
                                    Host = "_localhost";

                                _IAuthenticationService.SignIn(user, true, Host);

                                // insert support user login info
                                UserLoginInfo logininfo = new UserLoginInfo();
                                logininfo.LoginTime = DateTime.UtcNow;
                                logininfo.UserId = user.UserId;
                                _IUserService.InsertUserLoginInfo(logininfo);
                                // get user type
                                //var usertype = _IAddressMasterService.GetContactTypeById((int)user.UserTypeId);
                                var usertype = usertypecheck;
                                // get current session
                                var currentsession = _ICatalogMasterService.GetCurrentSession();
                                if (currentsession != null)
                                {
                                    HttpContext.Session["LoginUserId"] = user.UserName;
                                    HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
                                    HttpContext.Session["LoginUserType"] = usertype.ContactType1;
                                    HttpContext.Session["UserTypeId"] = usertype.ContactTypeId;
                                    HttpContext.Session["SchoolCurrentSession"] = currentsession.Session1;
                                    // get all staff types
                                    var stafftypes = _IStaffService.GetAllStaffTypes(ref count);
                                    // check if gurdian or student

                                    // default image
                                    var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
                                    string defaultimg = _WebHelper.GetStoreLocation() + "Images/default.png";
                                    string defaultmaleimg = _WebHelper.GetStoreLocation() + "Images/male-avtar.png";
                                    string defaultFemaleimg = _WebHelper.GetStoreLocation() + "Images/Female-avtar.png";

                                    if (usertype.ContactType1.ToLower() == "admin")
                                    {
                                        HttpContext.Session["UserImage"] = _WebHelper.GetStoreLocation() + "Images/male-avtar.png";
                                        HttpContext.Session["StudentId"] = "";
                                        HttpContext.Session["StaffId"] = "";
                                    }
                                    else if (usertype.ContactType1.ToLower() == "student")
                                    {

                                        // Student image path
                                        // get doc type
                                        string StudentImage = "";
                                        var doctypeid = _IDocumentService.GetAllDocumentTypes(ref count, "Image").FirstOrDefault();
                                        //var image = _IDocumentService.GetAllAttachedDocuments(ref count,
                                        //    doctypeid.DocumentTypeId,
                                        //    StudentId: user.UserContactId
                                        //    ).FirstOrDefault();
                                        var image = _IDocumentService.GetImageByStudentId(ref count, StudentId: user.UserContactId);
                                        var studentgender = _IStudentService.GetStudentById((int)user.UserContactId);
                                        StudentImage = image != null ? _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/StudentPhotos/" + image : "";
                                        if (string.IsNullOrEmpty(StudentImage))
                                        {
                                            // get student and check gender
                                            if (studentgender.Gender.Gender1.ToLower() == "male")
                                                StudentImage = _WebHelper.GetStoreLocation() + "Images/male-avtar.png";
                                            else if (studentgender.Gender.Gender1.ToLower() == "female")
                                                StudentImage = _WebHelper.GetStoreLocation() + "Images/Female-avtar.png";
                                        }

                                        HttpContext.Session["LoginUserId"] = !string.IsNullOrEmpty(studentgender.LName) ? studentgender.FName + " " + studentgender.LName : studentgender.FName;
                                        HttpContext.Session["UserImage"] = StudentImage;
                                        HttpContext.Session["StudentId"] = _ICustomEncryption.base64e(user.UserContactId.ToString());
                                        HttpContext.Session["StaffId"] = "";
                                    }
                                    else if (usertype.ContactType1.ToLower() == "guardian")
                                    {
                                        // Student image path
                                        // get doc type
                                        string StudentImage = "";
                                        //var studentid = _IGuardianService.GetGuardianById((int)user.UserContactId).StudentId;
                                        var doctypeid = _IDocumentService.GetAllDocumentTypes(ref count, "Image").FirstOrDefault();

                                        var studentgender = _IGuardianService.GetGuardianById((int)user.UserContactId);
                                        StudentImage = studentgender.Image != null ? _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/GuardianPhotos/" + studentgender.Image : "";


                                        HttpContext.Session["LoginUserId"] = !string.IsNullOrEmpty(studentgender.FirstName) ? studentgender.FirstName + " " + studentgender.LastName : studentgender.FirstName;
                                        HttpContext.Session["UserImage"] = StudentImage;
                                        HttpContext.Session["StudentId"] = _ICustomEncryption.base64e(user.UserContactId.ToString());
                                        HttpContext.Session["StaffId"] = "";
                                    }
                                    else if (stafftypes.Where(s => s.StaffType1.ToLower() == usertype.ContactType1.ToLower()).FirstOrDefault() != null)
                                    {
                                        // Student image path
                                        // get doc type
                                        string StaffImage = "";
                                        var doctypeid = _IStaffService.GetAllStaffDocumentTypes(ref count, "Image").FirstOrDefault();
                                        var image = _IStaffService.GetAllStaffDocuments(ref count,
                                            doctypeid.DocumentTypeId,
                                            StaffId: user.UserContactId
                                            ).FirstOrDefault();

                                        // get staff and check gender
                                        var staffgender = _IStaffService.GetStaffById((int)user.UserContactId);
                                        StaffImage = image != null ? _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/Teacher/Photos/" + image.Image : "";
                                        if (string.IsNullOrEmpty(StaffImage))
                                        {
                                            if (staffgender.GenderId > 0)
                                            {
                                                // get gender by id
                                                var gender = _ICatalogMasterService.GetGenderById((int)staffgender.GenderId);
                                                if (gender.Gender1.ToLower() == "male")
                                                    StaffImage = _WebHelper.GetStoreLocation() + "Images/male-avtar.png";
                                                else if (gender.Gender1.ToLower() == "female")
                                                    StaffImage = _WebHelper.GetStoreLocation() + "Images/Female-avtar.png";
                                            }
                                        }

                                        HttpContext.Session["LoginUserId"] = !string.IsNullOrEmpty(staffgender.LName) ? staffgender.FName + " " + staffgender.LName : staffgender.FName;
                                        HttpContext.Session["UserImage"] = StaffImage;
                                        HttpContext.Session["StudentId"] = "";
                                        HttpContext.Session["StaffId"] = _ICustomEncryption.base64e(user.UserContactId.ToString());
                                    }
                                    else if (usertype.ContactType1.ToLower() == "teacher")
                                    {
                                        // Student image path
                                        // get doc type
                                        string StaffImage = "";
                                        var doctypeid = _IStaffService.GetAllStaffDocumentTypes(ref count, "Image").FirstOrDefault();
                                        var image = _IStaffService.GetAllStaffDocuments(ref count,
                                            doctypeid.DocumentTypeId,
                                            StaffId: user.UserContactId
                                            ).FirstOrDefault();

                                        //check user gender
                                        var genderid = _IStaffService.GetStaffById((int)user.UserContactId).GenderId;
                                        var genderdata = _ICatalogMasterService.GetGenderById((int)genderid).Gender1;
                                        var defaultpic = defaultmaleimg;
                                        if (genderdata.ToLower().Contains("female"))
                                            defaultpic = defaultFemaleimg;

                                        // get staff and check gender
                                        var staffgender = _IStaffService.GetStaffById((int)user.UserContactId);
                                        StaffImage = image != null ? _WebHelper.GetStoreLocation() + "Images/" + SchoolDbId + "/Teacher/Photos/" + image.Image : defaultpic;


                                        HttpContext.Session["LoginUserId"] = !string.IsNullOrEmpty(staffgender.LName) ? staffgender.FName + " " + staffgender.LName : staffgender.FName;
                                        HttpContext.Session["UserImage"] = StaffImage;
                                        HttpContext.Session["StudentId"] = "";
                                        HttpContext.Session["StaffId"] = _ICustomEncryption.base64e(user.UserContactId.ToString());
                                    }


                                    #region //create cookie for send php form user information
                                    TimeSpan expiretime = new TimeSpan(20, 0, 0);
                                    string cookievalue;
                                    string CookieName = Host.Trim('_') + "_weeebxjslopzyfekpcskz0v6oxjmmra4";

                                    string CookieValue = user.UserRoleId.ToString() + "~" + user.UserId.ToString() + "~" + user.UserName;
                                    if (Request.Cookies[CookieName] != null)
                                    {
                                        cookievalue = Request.Cookies[CookieName].ToString();
                                    }
                                    else
                                    {
                                        Response.Cookies[CookieName].Value = _ICustomEncryption.Encrypt(CookieValue);
                                        Response.Cookies[CookieName].Expires = DateTime.Now.AddHours(20); // add expiry time
                                    }
                                    #endregion



                                    #region//get all User Role action permission

                                    var ListUserActionPermission = new List<UserRoleActionPermission>();
                                    ListUserActionPermission = _IUserManagementService.GetAllUserRoleActionPermissionLessColumns(ref count, user.UserRoleId).ToList();

                                    _ICommonMethodService.UpdateCreateUserRoleActionPermissionTextFile(SchoolDbId, ListUserActionPermission);
                                    #endregion

                                    return RedirectToAction("Index");
                                }
                                else
                                {
                                    ModelState.AddModelError("UserName", "Session is invalid or have invalid dates.");
                                    break;
                                }
                            }
                        case UserLoginResults.UserNotActive:
                            ModelState.AddModelError("UserName", "User Not Active");
                            break;
                        case UserLoginResults.CustomerNotExist:
                            ModelState.AddModelError("UserName", "User Not Exist");
                            break;
                        case UserLoginResults.UserLeft:
                            ModelState.AddModelError("UserName", "User Left");
                            break;
                        case UserLoginResults.WrongPassword:
                        default:
                            ModelState.AddModelError("Password", "Wrong Credentials");
                            break;
                        case UserLoginResults.NotAuthorizedIP:
                            ModelState.AddModelError("UserName", "User is not Authorized to login. Please conatct your admin");
                            break;
                        case UserLoginResults.TryAgainLater:
                            ModelState.AddModelError("UserName", "You cannot logged in yet. Please conatct your admin and try again later");
                            break;
                        case UserLoginResults.InActiveUserRole:
                            ModelState.AddModelError("UserName", "User Role Status Inactive, please contact Administrator");
                            break;
                        case UserLoginResults.PasswordExpired:
                            //RegisterModel registerModel = new RegisterModel();
                            //registerModel.UserId = model.UserName;
                            // ModelState.AddModelError("UserName", "As per password change policy, your password has been expired");
                            TempData["UserId"] = user.UserId;
                            return RedirectToAction("ChangePassword", "Home");
                            //  ModelState.AddModelError("UserName", "User Not Active");
                            break;
                    }
                }
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public ActionResult ChangePassword()
        {
            try
            {
                // current user
                //SchoolUser user = new SchoolUser();
                //if (HttpContext.Session["SchoolDB"] != null)
                //{
                //    // re-assign session
                //    var sessiondb = HttpContext.Session["SchoolDB"];
                //    HttpContext.Session["SchoolDB"] = sessiondb;

                //    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                //    if (user == null)
                //    {
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    var model = new ResetPasswordModel();
                    if (TempData["UserId"] != null)
                        model.UserId = Convert.ToInt32(TempData["UserId"]);
                    ModelState.AddModelError("UserName", "Account password has expired");


                    return View(model);
                }

                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }


            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return RedirectToAction("General", "Error");
            }
        }
        [HttpPost]
        public ActionResult ChangePassword(ResetPasswordModel model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    if (ModelState.IsValid)
                    {
                        user = _IUserService.GetUserById(model.UserId);
                        if (user.Password == _IEncryptionService.EncryptText(model.OldPassword))
                        {
                            var oldpassword = user.Password;
                            user.Password = _IEncryptionService.EncryptText(model.Password);
                            _IUserService.UpdateUser(user);

                            //set user pwd history
                            UserPwdHistory userPwdHistory = new UserPwdHistory();
                            userPwdHistory.OldPassword = oldpassword;
                            userPwdHistory.NewPassword = user.Password;
                            userPwdHistory.UserId = user.UserId;
                            userPwdHistory.ChangedOn = DateTime.UtcNow;
                            _IUserService.InsertUserPwdHistory(userPwdHistory);
                            TempData["NewUserID"] = user.UserName;
                            return RedirectToAction("Login", "Home");
                        }
                        else
                        {
                            var errormessage = string.Empty;

                            ModelState.AddModelError("OldPassword", "Old Password is wrong");
                            return View(model);
                        }
                    }
                    else
                    {
                        return View(model);
                    }
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        public ActionResult Logout()
        {
            // update support user login info with logout time
            var supportlogininfo = HttpContext.Session["LoginInfoId"] != null ? _IUserService.GetUserLoginInfoById(Convert.ToInt32(HttpContext.Session["LoginInfoId"])) : null;
            if (supportlogininfo != null)
            {
                supportlogininfo.LogoutTime = DateTime.UtcNow;
                _IUserService.UpdateUserLoginInfo(supportlogininfo);
            }

            // destory session
            Session.Abandon();
            // sign out method call
            _IAuthenticationService.SignOut();

            // clear authentication cookie
            string domain = Request.Url.AbsolutePath;
            domain = "_" + domain.Split('/')[1];
            if (Request.Url.AbsoluteUri.Contains("localhost"))
                domain = "_localhost";

            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName + domain, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);

            // clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
            SessionStateSection sessionStateSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
            HttpCookie cookie2 = new HttpCookie(sessionStateSection.CookieName, "");
            cookie2.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie2);

            //delete userRoleAction permission txt file 
            var SchoolDbId = Convert.ToString(HttpContext.Session["SchoolId"]);
            string path = Server.MapPath("~/Images/" + SchoolDbId + "/UserDataFile");
            var pathString = System.IO.Path.Combine(path, "UserRoleActionPermissions.txt");
            if ((System.IO.File.Exists(pathString)))
            {
                System.IO.File.Delete(pathString);
            }

            FormsAuthentication.RedirectToLoginPage();

            ExpireAllCookies();

            return RedirectToAction("Login");
        }

        public void ExpireAllCookies()
        {
            if (Request.Cookies != null)
            {
                int cookieCount = Request.Cookies.Count;
                for (var i = 0; i < cookieCount; i++)
                {
                    var cookie = Request.Cookies[i];
                    if (cookie != null)
                    {
                        var expiredCookie = new HttpCookie(cookie.Name)
                        {
                            Expires = DateTime.Now.AddDays(-1),
                            Domain = cookie.Domain
                        };
                        Response.Cookies.Add(expiredCookie); // overwrite it
                    }
                }

                // clear cookies server side
                Request.Cookies.Clear();
            }
        }

        // register
        // login 
        public ActionResult Register()
        {
            try
            {
                // current user
                SchoolUser user = new SchoolUser();
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                    {
                        var model = new RegisterModel();
                        model.IsFromPost = false;
                        return View(model);
                    }
                    else
                        return RedirectToAction("Index");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                if (!string.IsNullOrEmpty(model.MobileNo))
                {

                    if (Session["kspregmobno"].ToString() == model.MobileNo)
                    {
                        MD5CryptoServiceProvider md5hasher = new MD5CryptoServiceProvider();
                        Byte[] hashedbytes;
                        UTF8Encoding encoder = new UTF8Encoding();
                        hashedbytes = md5hasher.ComputeHash(encoder.GetBytes(model.OTP.ToString()));

                        DateTime currenttiem = Convert.ToDateTime(Session["kspregdt"]);
                        currenttiem = currenttiem.AddMinutes(5);
                        DateTime now = DateTime.UtcNow;

                        if (now <= currenttiem)
                        {
                            StringBuilder kspregotp = new StringBuilder();
                            foreach (var h in hashedbytes)
                                kspregotp.Append(h.ToString("x2"));

                            if (Session["kspregotp"].ToString() != kspregotp.ToString())
                                ModelState.AddModelError("OTP", "Invalid OTP");
                        }
                        else
                            ModelState.AddModelError("OTP", "OTP timeout");
                    }
                    else
                        ModelState.AddModelError("MobileNo", "Incorrect Mobile No. or OTP");
                }
                else
                    ModelState.AddModelError("MobileNo", "Mobile No. required");

                // check user type by mobile no
                var mobtype = _IContactService.GetAllContactInfoTypes(ref count, "Mobile").FirstOrDefault();
                var contactinfos = _IContactService.GetAllContactInfos(ref count, ContactInfo: model.MobileNo);
                if (mobtype != null)
                    contactinfos = contactinfos.Where(c => c.ContactInfoTypeId == mobtype.ContactInfoTypeId).ToList();


                var ismobilnoexist = new ContactInfo();

                if (contactinfos.Count > 0)
                {
                    ///multiple profiles teacher /gaurdian then teacher is priority than guardian 
                    var ContactTypes = contactinfos.Where(c => c.ContactType.ContactType1.ToLower() == "teacher").FirstOrDefault();
                    if (ContactTypes == null)
                        ContactTypes = contactinfos.Where(c => c.ContactType.ContactType1.ToLower() == "non-teaching").FirstOrDefault();
                    if (ContactTypes == null)
                        ContactTypes = contactinfos.Where(c => c.ContactType.ContactType1.ToLower() == "guardian").FirstOrDefault();
                    if (ContactTypes == null)
                        ContactTypes = contactinfos.Where(c => c.ContactType.ContactType1.ToLower() == "student").FirstOrDefault();

                    if (ContactTypes != null)
                    {
                        switch (ContactTypes.ContactType.ContactType1.ToLower())
                        {
                            case "teacher":
                                var Staffs = _IStaffService.GetAllStaffs(ref count).Where(x => x.StaffId == ContactTypes.ContactId).FirstOrDefault();
                                ismobilnoexist = contactinfos.Where(x => x.ContactTypeId == ContactTypes.ContactTypeId).FirstOrDefault();
                                break;
                            case "non-teaching":
                                var NonStaffs = _IStaffService.GetAllStaffs(ref count).Where(x => x.StaffId == ContactTypes.ContactId).FirstOrDefault();
                                ismobilnoexist = contactinfos.Where(x => x.ContactTypeId == ContactTypes.ContactTypeId).FirstOrDefault();
                                break;
                            case "student":
                                var Students1 = _IStudentService.GetAllStudents(ref count).Where(x => x.StudentId == ContactTypes.ContactId).FirstOrDefault();
                                ismobilnoexist = contactinfos.Where(x => x.ContactTypeId == ContactTypes.ContactTypeId).FirstOrDefault();
                                break;
                            case "guardian":
                                var Students = _IGuardianService.GetAllGuardians(ref count).Where(x => x.GuardianId == ContactTypes.ContactId).FirstOrDefault();
                                ismobilnoexist = contactinfos.Where(x => x.ContactTypeId == ContactTypes.ContactTypeId).FirstOrDefault();
                                break;
                        }
                    }
                    else
                        ismobilnoexist = contactinfos.FirstOrDefault();

                    var usertypeid = ismobilnoexist.ContactType.ContactTypeId;
                    var userId = ismobilnoexist.ContactId;

                    // checkuser already exist
                    var isuserexist = _IUserService.GetAllUsers(ref count, UserTypeId: usertypeid, UserContactId: userId);
                    if (isuserexist.Count > 0)
                    {
                        ModelState.AddModelError("MobileNo", "User already exist");
                    }
                }

                if (ModelState.IsValid)
                {
                    //var ismobilnoexist = contactinfos.FirstOrDefault();
                    var usertype = ismobilnoexist.ContactType.ContactType1;
                    var userId = ismobilnoexist.ContactId;

                    // add new School User
                    SchoolUser user = new SchoolUser();
                    user.RegdDate = DateTime.UtcNow;
                    user.Password = _IEncryptionService.EncryptText(model.Password);
                    user.Status = true;
                    user.UserTypeId = ismobilnoexist.ContactType.ContactTypeId;
                    user.UserContactId = userId;
                    user.UserRoleId = _IUserService.GetAllUserRoleContactTypes(ref count).Where(p => p.ContactTypeId == ismobilnoexist.ContactType.ContactTypeId && p.IsDefault == true).FirstOrDefault().UserRoleId;

                    //get username according to usertype
                    GetUserName(usertype, userId, user);

                    _IUserService.InsertUser(user);
                    UserPwdHistory userPwdHistory = new UserPwdHistory();

                    userPwdHistory.NewPassword = user.Password;
                    userPwdHistory.UserId = user.UserId;
                    userPwdHistory.ChangedOn = DateTime.UtcNow;
                    _IUserService.InsertUserPwdHistory(userPwdHistory);
                    TempData["NewUserID"] = user.UserName;
                    var errormessage = "";
                    var content = "";
                    var schoolInfo = _ISchoolDataService.GetSchoolDataDetail(ref count).FirstOrDefault();
                    var SchoolName = (!string.IsNullOrEmpty(schoolInfo.SchoolName)) ? schoolInfo.SchoolName : "";
                    _ISMSSender.SendSMS(model.MobileNo, "Your Login Credentials for " + SchoolName + " are: UserId:" + user.UserName
                                                    + " Password:" + model.Password, true, out errormessage, out content);

                    return RedirectToAction("Login", "Home");
                }

                model.IsFromPost = true;
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }

        public void GetUserName(string usertype, int? userId, SchoolUser user)
        {

            if (usertype.ToLower() == "student")
            {
                // get by id
                var student = _IStudentService.GetStudentById((int)userId);
                if (student != null)
                    user.UserName = "S" + userId;
            }
            else if (usertype.ToLower() == "guardian")
            {
                // get gurdian by id
                var guardian = _IGuardianService.GetGuardianById((int)userId);
                if (guardian != null)
                {
                    user.UserName = "P" + guardian.StudentId;  // get gurdian student Id
                    var usernamexist = _IUserService.GetAllUsers(ref count).Where(p => p.UserName.Contains(user.UserName)).ToList();
                    if (usernamexist != null)
                    {
                        if (usernamexist.Count > 0)
                        {
                            string randomkey = "0" + Convert.ToString((0 + usernamexist.Count));
                            user.UserName = "P" + guardian.StudentId + randomkey;
                        }
                    }
                }
                //user.UserName = "P" + guardian.StudentId;  // get gurdian student Id
            }
            else if (usertype.ToLower() == "staff" || usertype.ToLower() == "teacher" || usertype.ToLower() == "non-teaching")
            {
                // get staff by id
                var staff = _IStaffService.GetStaffById((int)userId);
                if (staff != null)
                {
                    user.UserName = staff.FName;
                    user.UserName += !string.IsNullOrEmpty(staff.LName) ? " " + staff.LName + userId.ToString() : userId.ToString();
                    user.UserName = user.UserName.Replace(" ", "");
                }
            }
        }

        // password reset
        public ActionResult PasswordRecovery()
        {
            try
            {
                // current user
                SchoolUser user = new SchoolUser();
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                    {
                        var model = new RegisterModel();
                        model.IsFromPost = false;


                        //if (TempData["UserName"] != null && TempData["UserName"]!="")
                        //{

                        //    model.UserId = TempData["UserName"].ToString();
                        //}
                        return View(model);
                    }
                    else
                        return RedirectToAction("Index");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }


            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return RedirectToAction("General", "Error");
            }
        }

        [HttpPost]
        public ActionResult PasswordRecovery(RegisterModel model)
        {
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                if (!string.IsNullOrEmpty(model.UserId))
                {
                    if (Session["kspreguser"].ToString() == model.UserId.ToLower())
                    {
                        MD5CryptoServiceProvider md5hasher = new MD5CryptoServiceProvider();
                        Byte[] hashedbytes;
                        UTF8Encoding encoder = new UTF8Encoding();
                        hashedbytes = md5hasher.ComputeHash(encoder.GetBytes(model.OTP.ToString()));

                        DateTime currenttiem = Convert.ToDateTime(Session["kspreguserdt"]);
                        currenttiem = currenttiem.AddMinutes(5);
                        DateTime now = DateTime.UtcNow;

                        if (now <= currenttiem)
                        {
                            StringBuilder kspregotp = new StringBuilder();
                            foreach (var h in hashedbytes)
                                kspregotp.Append(h.ToString("x2"));

                            if (Session["kspreguserotp"].ToString() != kspregotp.ToString())
                                ModelState.AddModelError("OTP", "Invalid OTP");
                        }
                        else
                            ModelState.AddModelError("OTP", "OTP timeout");
                    }
                    else
                        ModelState.AddModelError("UserId", "Incorrect UserId or OTP");
                }
                else
                    ModelState.AddModelError("UserId", "UserId required");

                // checkuser already exist
                var isuserexist = _IUserService.GetAllUsers(ref count);
                if (!string.IsNullOrEmpty(model.UserId))
                    isuserexist = isuserexist.Where(u => u.UserName.ToLower() == model.UserId.ToLower()).ToList();
                if (isuserexist.Count == 0)
                    ModelState.AddModelError("UserId", "User not exist");

                if (ModelState.IsValid)
                {
                    var user = isuserexist.FirstOrDefault();
                    var oldpassword = user.Password;
                    user.Password = _IEncryptionService.EncryptText(model.Password);
                    _IUserService.UpdateUser(user);
                    //set user pwd history

                    UserPwdHistory userPwdHistory = new UserPwdHistory();
                    userPwdHistory.OldPassword = oldpassword;
                    userPwdHistory.NewPassword = user.Password;
                    userPwdHistory.UserId = user.UserId;
                    userPwdHistory.ChangedOn = DateTime.UtcNow;
                    _IUserService.InsertUserPwdHistory(userPwdHistory);

                    TempData["NewUserID"] = user.UserName;
                    var contactInfoTypeId = _IContactService.GetAllContactInfoTypes(ref count, ContactInfoType: "Mobile").Select(m => m.ContactInfoTypeId).FirstOrDefault();
                    var coninfo = _IContactService.GetAllContactInfos(ref count);
                    var contactinfo = coninfo.Where(c => c.ContactId == user.UserContactId && c.ContactTypeId == user.UserTypeId && c.ContactInfoTypeId == contactInfoTypeId).FirstOrDefault();
                    var ContactInfo = contactinfo.ContactInfo1;
                    var errormessage = "";
                    var content = "";
                    _ISMSSender.SendSMS(ContactInfo, "Your Login Credentials for 3iSchools are: UserId:" + user.UserName + " Password:" + model.Password, true, out errormessage, out content);
                    return RedirectToAction("Login", "Home");
                }

                model.IsFromPost = true;
                return View(model);
            }
            else
                return RedirectToAction("Index", "Home");
        }


        // GET: HomeWork
        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }
                if (ModelState.IsValid)
                {
                    if (user.Password == _IEncryptionService.EncryptText(model.OldPassword))
                    {
                        var oldpassword = user.Password;
                        user.Password = _IEncryptionService.EncryptText(model.Password);
                        _IUserService.UpdateUser(user);

                        //set user pwd history
                        UserPwdHistory userPwdHistory = new UserPwdHistory();
                        userPwdHistory.OldPassword = oldpassword;
                        userPwdHistory.NewPassword = user.Password;
                        userPwdHistory.UserId = user.UserId;
                        userPwdHistory.ChangedOn = DateTime.UtcNow;
                        _IUserService.InsertUserPwdHistory(userPwdHistory);
                        return Json(new
                        {
                            status = "success",
                            msg = "Password Changed Successfully"
                        });
                    }
                    else
                    {
                        var errormessage = string.Empty;

                        return Json(new
                        {
                            status = "failed",
                            msg = "Old PassWord is wrong.ε"
                        });
                    }
                }
                else
                {
                    var errormessage = string.Empty;
                    foreach (var m in ModelState.Values.ToList())
                    {

                        foreach (var error in m.Errors)
                        {
                            errormessage += error.ErrorMessage;
                            errormessage += "ε";
                        }
                    }
                    return Json(new
                    {
                        status = "failed",
                        msg = errormessage
                    });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }
        #endregion

        #region Ajax Method
        public ActionResult GetHomePageFirstLists()
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                var studentid = 0;

                switch (user.ContactType.ContactType1.ToLower())
                {
                    case "student":
                        studentid = (int)user.UserContactId;
                        // model.Id = _ICustomEncryption.base64e(studentid.ToString());
                        break;

                    case "guardian":
                        var guardian = _IGuardianService.GetGuardianById((int)user.UserContactId);
                        studentid = guardian != null ? (int)guardian.StudentId : 0;
                        // model.Id = _ICustomEncryption.base64e(studentid.ToString());
                        break;

                    default:
                        studentid = 0;
                        break;
                }
                var session = _ICatalogMasterService.GetCurrentSession();
                // get student current class
                int studentclassid = 0;
                int studentStandardid = 0;
                if (studentid > 0)
                {
                    // student class
                    var studentclass = _IStudentService.GetAllStudentClasss(ref count, StudentId: studentid, SessionId: session.SessionId).FirstOrDefault();
                    if (studentclass != null && !string.IsNullOrEmpty(studentclass.RollNo))
                    {
                        studentclassid = (int)studentclass.ClassId;
                        studentStandardid = (int)studentclass.ClassMaster.StandardId;
                    }
                }

                var allEventTypes = _ISchedularService.GetAllEventTypeList(ref count).ToList();
                //all type of events
                IList<ActivityEvent> AnnouncementList = new List<ActivityEvent>();



                IList<ExamResultNotification> ExamResultList = new List<ExamResultNotification>();

                var allEvents = _ISchedularService.GetAllEventList(ref count);
                var allAnnoucements = allEvents.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Announcement" && p.StartDate >= DateTime.Now.Date.AddDays(-30) && (p.EndDate <= session.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();

                var SortAnnouncement = allEventTypes.Where(m => m.EventType1 == "Announcement").FirstOrDefault().SortingMethod;
                if (SortAnnouncement == 1)
                    allAnnoucements = allAnnoucements.OrderByDescending(p => p.StartDate).ToList();


                if (allAnnoucements.Count > 0)
                {
                    foreach (var item in allAnnoucements)
                    {
                        var ActivityEvent = new ActivityEvent();
                        ActivityEvent.StartDate = item.StartDate.HasValue ? ((DateTime)item.StartDate).ToString("dd MMMM yyyy") : "";
                        ActivityEvent.StartTime = item.StartTime.HasValue ? item.StartTime.ToString() : "";
                        ActivityEvent.stdate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : "";
                        ActivityEvent.Venue = item.Venue != null ? item.Venue : "";
                        ActivityEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                        ActivityEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");
                        ActivityEvent.EndDate = "";
                        ActivityEvent.endate = "";
                        ActivityEvent.EndTime = "";
                        AnnouncementList.Add(ActivityEvent);
                    }
                }


                //exams
                var Exams = _IExamService.GetAllExamDateSheetDetails(ref count, StandardId: studentStandardid).ToList();
                var ExamDetail = Exams.GroupBy(p => p.StandardId).Select(m => m.First()).ToList();
                var ExamResultDates = _IExamService.GetAllExamResultDates(ref count).Where(p => (Exams.Select(m => m.ExamDateSheetId).ToArray()).Contains(p.ExamDateSheetId)).ToList();
                if (ExamDetail.Count > 0)
                {
                    foreach (var item in ExamDetail)
                    {
                        var standard = _ICatalogMasterService.GetStandardMasterById((int)item.StandardId);
                        var RstPublishDate = ExamResultDates.Where(p => p.StandardId == item.StandardId && p.ResultPublishDate != null && p.ResultPublishDate >= DateTime.Now.Date).Select(p => p.ResultPublishDate).FirstOrDefault();
                        if (RstPublishDate != null)
                            ExamResultList.Add(new ExamResultNotification { Standard = standard.Standard, ResDate = ((DateTime)RstPublishDate).ToString("dd MMMM yyyy"), ExamDateSheet = item.ExamDateSheet.ExamDateSheet1 });

                    }

                }


                return Json(new
                {
                    status = "success",

                    AnnouncementList = AnnouncementList,

                    ExamResultList = ExamResultList
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }
        public ActionResult GetHomePageLists()
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                var studentid = 0;

                switch (user.ContactType.ContactType1.ToLower())
                {
                    case "student":
                        studentid = (int)user.UserContactId;
                        // model.Id = _ICustomEncryption.base64e(studentid.ToString());
                        break;

                    case "guardian":
                        var guardian = _IGuardianService.GetGuardianById((int)user.UserContactId);
                        studentid = guardian != null ? (int)guardian.StudentId : 0;
                        // model.Id = _ICustomEncryption.base64e(studentid.ToString());
                        break;

                    default:
                        studentid = 0;
                        break;
                }
                var session = _ICatalogMasterService.GetCurrentSession();
                // get student current class
                int studentclassid = 0;
                int studentStandardid = 0;
                if (studentid > 0)
                {
                    // student class
                    var studentclass = _IStudentService.GetAllStudentClasss(ref count, StudentId: studentid, SessionId: session.SessionId).FirstOrDefault();
                    if (studentclass != null && !string.IsNullOrEmpty(studentclass.RollNo))
                    {
                        studentclassid = (int)studentclass.ClassId;
                        studentStandardid = (int)studentclass.ClassMaster.StandardId;
                    }
                }

                var allEventTypes = _ISchedularService.GetAllEventTypeList(ref count).ToList();
                //all type of events
                IList<ActivityEvent> AnnouncementList = new List<ActivityEvent>();

                IList<ActivityEvent> HolidayList = new List<ActivityEvent>();

                IList<ActivityEvent> VacationsList = new List<ActivityEvent>();

                IList<ActivityEvent> ActivityList = new List<ActivityEvent>();

                IList<ExamNotification> ExamsList = new List<ExamNotification>();

                IList<ExamResultNotification> ExamResultList = new List<ExamResultNotification>();

                var allEvents = _ISchedularService.GetAllEventList(ref count);
                var allAnnoucements = allEvents.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Announcement" && p.StartDate >= DateTime.Now.Date.AddDays(-30) && (p.EndDate <= session.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                var allHolidays = allEvents.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Holiday" && p.StartDate >= DateTime.Now.Date.AddDays(-30) && (p.EndDate <= session.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                var allVacations = allEvents.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Vacation" && p.StartDate >= DateTime.Now.Date.AddDays(-30) && (p.EndDate <= session.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();
                var allActivity = allEvents.Where(p => p.IsActive == true && p.IsDeleted == false && p.EventType.EventType1 == "Activity" && p.StartDate >= DateTime.Now.Date.AddDays(-30) && (p.EndDate <= session.EndDate || p.EndDate == null)).OrderBy(p => p.StartDate).ToList();

                var SortAnnouncement = allEventTypes.Where(m => m.EventType1 == "Announcement").FirstOrDefault().SortingMethod;
                if (SortAnnouncement == 1)
                    allAnnoucements = allAnnoucements.OrderByDescending(p => p.StartDate).ToList();

                var SortHoliday = allEventTypes.Where(m => m.EventType1 == "Holiday").FirstOrDefault().SortingMethod;
                if (SortHoliday == 1)
                    allHolidays = allHolidays.OrderByDescending(p => p.StartDate).ToList();

                var SortVacation = allEventTypes.Where(m => m.EventType1 == "Vacation").FirstOrDefault().SortingMethod;
                if (SortVacation == 1)
                    allVacations = allVacations.OrderByDescending(p => p.StartDate).ToList();

                var SortActivity = allEventTypes.Where(m => m.EventType1 == "Activity").FirstOrDefault().SortingMethod;
                if (SortActivity == 1)
                    allActivity = allActivity.OrderByDescending(p => p.StartDate).ToList();

                if (allAnnoucements.Count > 0)
                {
                    foreach (var item in allAnnoucements)
                    {
                        var ActivityEvent = new ActivityEvent();
                        ActivityEvent.StartDate = item.StartDate.HasValue ? ((DateTime)item.StartDate).ToString("dd MMMM yyyy") : "";
                        ActivityEvent.StartTime = item.StartTime.HasValue ? item.StartTime.ToString() : "";
                        ActivityEvent.stdate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : "";
                        ActivityEvent.Venue = item.Venue != null ? item.Venue : "";
                        ActivityEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                        ActivityEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");
                        ActivityEvent.EndDate = "";
                        ActivityEvent.endate = "";
                        ActivityEvent.EndTime = "";
                        AnnouncementList.Add(ActivityEvent);
                    }
                }
                if (allHolidays.Count > 0)
                {
                    foreach (var item in allHolidays)
                    {
                        var ActivityEvent = new ActivityEvent();
                        ActivityEvent.StartDate = item.StartDate.HasValue ? ((DateTime)item.StartDate).ToString("dd MMMM yyyy") : "";
                        ActivityEvent.StartTime = item.StartTime.HasValue ? item.StartTime.ToString() : "";
                        ActivityEvent.Venue = item.Venue != null ? item.Venue : "";
                        ActivityEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                        ActivityEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");
                        ActivityEvent.endate = "";
                        ActivityEvent.stdate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : "";
                        ActivityEvent.EndTime = "";
                        HolidayList.Add(ActivityEvent);
                    }
                }
                if (allVacations.Count > 0)
                {
                    foreach (var item in allVacations)
                    {
                        var ActivityEvent = new ActivityEvent();
                        ActivityEvent.StartDate = item.StartDate.HasValue ? ((DateTime)item.StartDate).ToString("dd MMMM yyyy") : "";
                        ActivityEvent.EndDate = item.EndDate.HasValue ? ((DateTime)item.EndDate).ToString("dd MMMM yyyy") : "";
                        ActivityEvent.StartTime = item.StartTime.HasValue ? item.StartTime.ToString() : "";
                        ActivityEvent.Venue = item.Venue != null ? item.Venue : "";
                        ActivityEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                        ActivityEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");
                        ActivityEvent.endate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : "";
                        ActivityEvent.stdate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : "";
                        ActivityEvent.EndTime = "";
                        VacationsList.Add(ActivityEvent);
                    }
                }
                if (allActivity.Count > 0)
                {
                    var allclasses = _ICatalogMasterService.GetAllClassMasters(ref count);
                    bool IsExistInActivity = false;

                    foreach (var item in allActivity)
                    {
                        IsExistInActivity = false;
                        var ActivityEvent = new ActivityEvent();
                        ActivityEvent.StartDate = item.StartDate.HasValue ? ((DateTime)item.StartDate).ToString("dd MMMM yyyy") : "";
                        ActivityEvent.EndDate = item.EndDate.HasValue ? ((DateTime)item.EndDate).ToString("dd MMMM yyyy") : "";
                        ActivityEvent.stdate = item.StartDate.HasValue ? ConvertDate((DateTime)item.StartDate) : "";
                        ActivityEvent.endate = item.EndDate.HasValue ? ConvertDate((DateTime)item.EndDate) : "";
                        ActivityEvent.StartTime = item.StartTime.HasValue ? (DateTime.Today.Add((TimeSpan)item.StartTime)).ToString("hh:mm tt") : "";
                        ActivityEvent.EndTime = item.EndTime.HasValue ? (DateTime.Today.Add((TimeSpan)item.EndTime)).ToString("hh:mm tt") : "";
                        ActivityEvent.EventTitle = item.EventTitle.Replace("<", "&lt;");
                        ActivityEvent.Description = item.Description.Replace("<", "&lt;").Replace("\n", "<br>");
                        ActivityEvent.Venue = item.Venue != null ? item.Venue : "";

                        if (item.EventDetails.Count > 0)
                        {
                            var eventClasses = "";
                            var classids = item.EventDetails.Select(e => e.ClassId).ToArray();
                            eventClasses = String.Join(",", allclasses.Where(c => classids.Contains(c.ClassId)).Select(c => c.Class).ToList());

                            if (studentclassid > 0 && classids.Contains(studentclassid))
                            {
                                IsExistInActivity = true;
                                eventClasses = _ICatalogMasterService.GetClassMasterById(studentclassid).Class;
                            }

                            ActivityEvent.ActivityClass = eventClasses;
                        }
                        else
                        {
                            IsExistInActivity = true;
                            ActivityEvent.ActivityClass = "All Classes";
                        }

                        if (studentclassid > 0)
                        {
                            if (IsExistInActivity)
                                ActivityList.Add(ActivityEvent);
                        }
                        else
                            ActivityList.Add(ActivityEvent);
                    }
                }

                //exams
                var Exams = _IExamService.GetAllExamDateSheetDetails(ref count, StandardId: studentStandardid).ToList();
                // var ExamDetail = Exams.GroupBy(p => p.StandardId).Select(m => m.First()).ToList();
                //var ExamResultDates = _IExamService.GetAllExamResultDates(ref count).Where(p => (Exams.Select(m => m.ExamDateSheetId).ToArray()).Contains(p.ExamDateSheetId)).ToList();
                //if (ExamDetail.Count > 0)
                //{
                //    foreach (var item in ExamDetail)
                //    {
                //        var standard = _ICatalogMasterService.GetStandardMasterById((int)item.StandardId);
                //        var RstPublishDate = ExamResultDates.Where(p => p.StandardId == item.StandardId && p.ResultPublishDate != null && p.ResultPublishDate >= DateTime.Now.Date).Select(p => p.ResultPublishDate).FirstOrDefault();
                //        if (RstPublishDate != null)
                //            ExamResultList.Add(new ExamResultNotification { Standard = standard.Standard, ResDate = ((DateTime)RstPublishDate).ToString("dd MMMM yyyy"), ExamDateSheet = item.ExamDateSheet.ExamDateSheet1 });

                //    }

                //}
                var datesheetAcctoPublisheddate = _IExamService.GetAllExamPublishDetails(ref count);
                if (datesheetAcctoPublisheddate.Count > 0)
                {
                    datesheetAcctoPublisheddate = datesheetAcctoPublisheddate.Where(m => m.ExamPublishDate <= DateTime.Now.Date).ToList();
                    Exams = Exams.Where(m => (datesheetAcctoPublisheddate.Select(n => n.ExamDateSheetId).ToArray()).Contains((int)m.ExamDateSheetId)).ToList();
                    Exams = Exams.Where(p => p.ExamDate >= DateTime.Now.Date).OrderBy(e => e.ExamDate).ToList();
                    if (Exams.Count > 0)
                    {
                        foreach (var item in Exams)
                        {
                            var standard = _ICatalogMasterService.GetStandardMasterById((int)item.StandardId);
                            var subject = _ISubjectService.GetSubjectById((int)item.SubjectId);
                            var timeSlot = item.ExamTimeSlot.ExamTimeSlot1;

                            if (standard != null && subject != null)
                                ExamsList.Add(new ExamNotification { ClassSubjectName = standard.Standard + "/" + subject.Subject1, TimeSlot = timeSlot, exadate = ((DateTime)item.ExamDate).ToString("dd MMMM yyyy") });
                        }
                    }
                }
                IList<DocumentNotification> DocumentList = new List<DocumentNotification>();
                IList<DocumentDashboard> DocumentDashboard = new List<DocumentDashboard>();
                DocumentDashboard DocumentDashboardmodel = new DocumentDashboard();
                IList<DocumentCategorys> DocumentCategoryList = new List<DocumentCategorys>();
                if (user.ContactType.ContactType1.ToLower() == "admin")
                {
                    DateTime? TodayDate = DateTime.Now.Date;
                    var ReminderTypes = _IDocumentService.GetAllDocReminderTypes(ref count).ToList();
                    var WeeklyReminder = ReminderTypes.Where(f => f.ReminderType.ToLower() == "weekly").FirstOrDefault();
                    var DailyReminder = ReminderTypes.Where(f => f.ReminderType.ToLower() == "daily").FirstOrDefault();

                    var DocumentNotification = new DocumentNotification();

                    var allDocumentDetails = _IDocumentService.GetAllDocumentDetail(ref count).ToList();
                    allDocumentDetails = allDocumentDetails.Where(a => a.ExpiryDate.Value.Date >= TodayDate.Value.Date && a.ReminderDate.Value.Date <= TodayDate.Value.Date).ToList();

                    //Weekly Reminder DocuMents                
                    if (WeeklyReminder != null)
                    {
                        var weeklyDocuments = allDocumentDetails.Where(f => f.ReminderType == WeeklyReminder.ReminderTypeId).ToList();

                        foreach (var detail in weeklyDocuments)
                        {
                            if (detail.ReminderDate.Value.Date <= TodayDate)
                            {
                                DateTime ReminderDate = detail.ReminderDate.Value.Date;
                                var daysdiff = ((DateTime)TodayDate - ReminderDate).TotalDays;
                                if (daysdiff == 0 || daysdiff % 7 == 0)
                                {

                                    DocumentNotification = new DocumentNotification();
                                    DocumentNotification.Document = detail.Document.DocumentTitle;
                                    DocumentNotification.DocumentCategoryId = (int)detail.Document.DocumentCategoryId;
                                    DocumentNotification.DocumentExp = detail.ExpiryDate.Value.Date.ToString("dd/MM/yyyy");
                                    DocumentList.Add(DocumentNotification);
                                }
                            }
                        }

                    }
                    //Daily Reminder DocuMents   
                    if (DailyReminder != null)
                    {
                        var dailyDocuments = allDocumentDetails.Where(f => f.ReminderType == DailyReminder.ReminderTypeId).ToList();
                        foreach (var detail in dailyDocuments)
                        {
                            DocumentNotification = new DocumentNotification();
                            DocumentNotification.Document = detail.Document.DocumentTitle;
                            DocumentNotification.DocumentCategoryId = (int)detail.Document.DocumentCategoryId;
                            DocumentNotification.DocumentExp = detail.ExpiryDate.Value.Date.ToString("dd/MM/yyyy");
                            DocumentList.Add(DocumentNotification);
                        }
                    }
                    //var DocumentCategory = new DocumentCategorys();
                    var DocumentCategorys = _IDocumentService.GetAllDocumentCategory(ref count).ToList();
                    foreach (var categry in DocumentCategorys)
                    {

                        DocumentDashboardmodel = new DocumentDashboard();
                        DocumentDashboardmodel.DocumentCategory = categry.DocumentCategory1;
                        DocumentDashboardmodel.DocumentList = DocumentList.Where(m => m.DocumentCategoryId == categry.DocumentCategoryId).ToList();
                        if (DocumentDashboardmodel.DocumentList.Count() > 0)
                            DocumentDashboard.Add(DocumentDashboardmodel);
                    }


                }

                return Json(new
                {
                    status = "success",
                    ExamsList = ExamsList,
                    //AnnouncementList=AnnouncementList,
                    HolidayList = HolidayList,
                    VacationsList = VacationsList,
                    ActivityList = ActivityList,
                    //ExamResultList=ExamResultList
                    DocumentDashboard = DocumentDashboard,

                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }
        public ActionResult GetHomePagedetails()
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                var session = _ICatalogMasterService.GetCurrentSession();
                // get admitted students
                var Status = _IStudentMasterService.GetAllStudentStatus(ref count, "Active").FirstOrDefault();
                var currentSessionStudentCount = _IStudentService.GetAllActiveStudentCount(ref count,
                                            SessionId: session.SessionId, StudentStatusId: Status.StudentStatusId)
                                            .Where(s => s.AdmnDate != null && s.AdmnNo != "").ToList();
                var StudentStrength = currentSessionStudentCount.Count.ToString();

                //current year new  enrollment
                var currentSessionEnrolledStudents = 0;
                currentSessionEnrolledStudents = _IStudentService.GetAdmittedList(ref count, TypeId: "Admit", SessionId: session.SessionId).Count;
                var Enrolled = currentSessionEnrolledStudents.ToString();

                var presentStudent = 0;
                var presentStatus = _ISubjectService.GetAllAttendanceStatus(ref count, "Present").SingleOrDefault();
                if (presentStatus != null)
                    presentStudent = _ISubjectService.GetAllAttendances(ref count, Date: DateTime.UtcNow, AttendanceStatusId: presentStatus.AttendanceStatusId).Count;
                var Attendance = presentStudent.ToString() + "/" + currentSessionStudentCount.Count.ToString();

                //fees
                //commented while optimization fee value not used anywhere
                //var allfeereceipts = _IFeeService.GetAllFeeReceipts(ref count);
                //decimal? currentSessionTotalAmount = 0;
                //decimal? currentSessionConessionTotalAmount = 0;
                //if (allfeereceipts.Count > 0)
                //{
                //    var currentSessionFeeReceiptsIds = allfeereceipts.Where(p => p.ReceiptDate >= session.StartDate && p.ReceiptDate <= session.EndDate).Select(p => p.ReceiptId).ToList();
                //    if (currentSessionFeeReceiptsIds.Count > 0)
                //    {
                //        var allFeeReceiptsDetails = _IFeeService.GetAllFeeReceiptDetails(ref count);
                //        currentSessionTotalAmount += allFeeReceiptsDetails.Where(p => currentSessionFeeReceiptsIds.Contains((int)p.ReceiptId) && p.GroupFeeHeadId != null).Sum(p => p.Amount);
                //        currentSessionConessionTotalAmount = allFeeReceiptsDetails.Where(p => currentSessionFeeReceiptsIds.Contains((int)p.ReceiptId) && p.ConsessionId != null).Sum(p => p.Amount);
                //    }
                //}
                //var Fees = (currentSessionTotalAmount - currentSessionConessionTotalAmount).ToString();
                // Teacher
                var teachers = 0;
                var stafftypeteacher = _IStaffService.GetAllStaffTypesUpdated(ref count, IsTeacher: true).FirstOrDefault();
                if (stafftypeteacher != null)
                    teachers = _IStaffService.GetAllStaffs(ref count, StaffTypeId: stafftypeteacher.StaffTypeId, IsActive: true).Count;
                var Teacher = teachers.ToString();

                // Staff
                var staffs = 0;
                var stafftypenonteacher = _IStaffService.GetAllStaffTypesUpdated(ref count, IsTeacher: false).FirstOrDefault();
                if (stafftypenonteacher != null)
                    staffs = _IStaffService.GetAllStaffs(ref count, StaffTypeId: stafftypenonteacher.StaffTypeId, IsActive: true).Count;
                var Staff = staffs.ToString();

                // unread meassage
                var UnreadMessage = "0";
                var allUnReadedMessages = new List<Message>();
                DataTable dtMsgesCount = _IMessengerService.GetAllUnreadMsgsCount(ref count, user.UserId);
                if (dtMsgesCount.Rows.Count > 0)
                {
                    UnreadMessage = dtMsgesCount.Rows[0]["UnreadMsges"].ToString()
                                        + "/" + dtMsgesCount.Rows[0]["TotalMsges"].ToString();
                }

                return Json(new
                {
                    status = "success",
                    StudentStrength = StudentStrength,
                    Enrolled = Enrolled,
                    Attendance = Attendance,
                    //Fees = Fees,
                    Teacher = Teacher,
                    Staff = Staff,
                    UnreadMessage = UnreadMessage
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }
        public ActionResult SetSchoolSettings()
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                var Is_Lite = _ISchoolSettingService.GetorSetSchoolData("Is_Lite", "0").AttributeValue == "1" ? true : false;
                _ISchoolSettingService.GetorSetSchoolData("IncludePreviousSessionFeePending", "0");
                var CustomNavigation = _ISchoolSettingService.GetorSetSchoolData("CustomNavigation", "0").AttributeValue == "1" ? true : false;
                var AutoFeeReminderSMS = _ISchoolSettingService.GetorSetSchoolData("AutoFeeReminderSMS", "0", null).AttributeValue;
                var AutoFeeReminderNotification = _ISchoolSettingService.GetorSetSchoolData("AutoFeeReminderNotification", "0", null).AttributeValue;
                var AutoFeeReminderSMSNonAppUser = _ISchoolSettingService.GetorSetSchoolData("AutoFeeReminderSMSNonAppUser", "0", null).AttributeValue;
                var schoolSettingSMSMobileNo = _ISchoolSettingService.GetorSetSchoolData("AutoFeeReminderSmsMobileNoSelectionForStudentRecepient", "SMS",
                                                                                                              Desc: "SMS : Student SMS Mobile No , SMN: Student mobile no. , GMN : Guardian Mobile No")
                                                                                                             .AttributeValue;
                return Json(new
                {
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }
        public bool CompressJPEGImage(Bitmap bmp1, string tempImgNameWithPath)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo ici = null;


            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.MimeType == "image/jpeg")
                    ici = codec;
            }
            // for the Quality parameter category.
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;

            EncoderParameters myEncoderParameters = new EncoderParameters(1);
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 90L);
            myEncoderParameters.Param[0] = myEncoderParameter;
            try
            {
                bmp1.Save(tempImgNameWithPath, ici, myEncoderParameters);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        // ajax
        [HttpPost]
        public ActionResult UploadFile()
        {
            if (Request.Files.Count > 0)
            {
                string path = String.Empty;
                try
                {
                    int maxfilesize = 100; // default size
                    var ProfilePicMaxSizeSetting = _ISchoolSettingService.GetAttributeValue("ProfilePicMaxSize");
                    if (ProfilePicMaxSizeSetting != null && !string.IsNullOrEmpty(ProfilePicMaxSizeSetting))
                        maxfilesize = Convert.ToInt32(ProfilePicMaxSizeSetting);

                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //file
                        HttpPostedFileBase file = files[i];
                        // get Filename
                        string fileName;
                        string uniqueno = Request.Form["uniqueno"];
                        string uniqueimage = Request.Form["uniqueimage"];
                        var Isphoto = Convert.ToInt32(Request.Form["IsPhoto"]);
                        if (Isphoto == 1)
                        {
                            fileName = "temp_" + uniqueimage + Path.GetExtension(file.FileName).ToLower();
                        }
                        else
                        {
                            fileName = uniqueno + "_doc" + uniqueimage + Path.GetExtension(file.FileName).ToLower();
                        }

                        //Is the file too big to upload?
                        int fileSize = file.ContentLength;
                        if (fileSize > (maxfilesize * 1024))
                        {
                            return Json(new
                            {
                                status = "error",
                                msg = "Filesize of image is too large. Maximum file size permitted is " + maxfilesize + "KB"
                            });
                        }

                        //check that the file is of the permitted file type
                        string fileExtension = Path.GetExtension(fileName).ToLower();

                        string[] acceptedFileTypes = new string[3];
                        acceptedFileTypes[0] = ".jpg";
                        acceptedFileTypes[1] = ".jpeg";
                        acceptedFileTypes[2] = ".png";

                        bool acceptFile = false;
                        //should we accept the file?
                        for (int j = 0; j < acceptedFileTypes.Count(); j++)
                        {
                            if (fileExtension == acceptedFileTypes[j])
                            {
                                //accept the file, yay!
                                acceptFile = true;
                                break;
                            }
                        }

                        if (!acceptFile)
                            return Json(new
                            {
                                status = "error",
                                msg = "The file you are trying to upload is not a permitted file type!"
                            });

                        // check directory exist or not
                        var schoolid = HttpContext.Session["SchoolId"];
                        if (Isphoto == 1)
                        {
                            var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/StudentPhotos"));
                            if (!Directory.Exists(directory))
                                Directory.CreateDirectory(directory);

                            path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/StudentPhotos"), fileName);
                        }
                        else
                        {
                            var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/" + uniqueno));
                            if (!Directory.Exists(directory))
                                Directory.CreateDirectory(directory);

                            path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/" + uniqueno), fileName);
                        }
                        // Get the complete folder path and store the file inside it.  

                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);

                        if (Isphoto == 1)
                        {

                            var resizeratio = _ISchoolSettingService.GetAttributeValue("StudentPhotoRatio");

                            var height = Convert.ToInt32(resizeratio.Split(',')[0]);
                            var width = Convert.ToInt32(resizeratio.Split(',')[1]);

                            var resizedimg = _ISchoolDataService.ResizeImage(file, "studentimage", height, width);
                            resizedimg.Save(path);
                        }
                        else
                            file.SaveAs(path);

                        //var image1 = new Bitmap(path,true);
                        //CompressJPEGImage(image1, fileName);
                        //file.SaveAs(path);
                    }
                    // Returns message that successfully uploaded  
                    return Json(new
                    {
                        status = "success",
                        path = path,
                        msg = "File Uploaded Successfully!"
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        status = "error",
                        msg = "Error occurred. Error details: " + ex.Message
                    });
                }
            }
            else
            {
                return Json(new
                {
                    status = "error",
                    msg = "No files selected."
                });
            }
        }

        [HttpPost]
        public ActionResult UploadFileGuardian()
        {
            if (Request.Files.Count > 0)
            {
                string path = String.Empty;
                try
                {
                    int maxfilesize = 100; // default size
                    var ProfilePicMaxSizeSetting = _ISchoolSettingService.GetAttributeValue("ProfilePicMaxSize");
                    if (ProfilePicMaxSizeSetting != null && !string.IsNullOrEmpty(ProfilePicMaxSizeSetting))
                        maxfilesize = Convert.ToInt32(ProfilePicMaxSizeSetting);

                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //file
                        HttpPostedFileBase file = files[i];
                        // get Filename
                        string fileName;
                        string uniqueno = Request.Form["uniqueno"];
                        string uniqueimage = Request.Form["uniqueimage"];
                        var Isphoto = Convert.ToInt32(Request.Form["IsPhoto"]);
                        if (Isphoto == 1)
                        {
                            fileName = "temp_" + uniqueimage + Path.GetExtension(file.FileName).ToLower();
                        }
                        else
                        {
                            fileName = uniqueno + "_doc" + uniqueimage + Path.GetExtension(file.FileName).ToLower();
                        }

                        //Is the file too big to upload?
                        int fileSize = file.ContentLength;
                        if (fileSize > (maxfilesize * 1024))
                        {
                            return Json(new
                            {
                                status = "error",
                                msg = "Filesize of image is too large. Maximum file size permitted is " + maxfilesize + "KB"
                            });
                        }

                        //check that the file is of the permitted file type
                        string fileExtension = Path.GetExtension(fileName).ToLower();

                        string[] acceptedFileTypes = new string[3];
                        acceptedFileTypes[0] = ".jpg";
                        acceptedFileTypes[1] = ".jpeg";
                        acceptedFileTypes[2] = ".png";

                        bool acceptFile = false;
                        //should we accept the file?
                        for (int j = 0; j < acceptedFileTypes.Count(); j++)
                        {
                            if (fileExtension == acceptedFileTypes[j])
                            {
                                //accept the file, yay!
                                acceptFile = true;
                                break;
                            }
                        }

                        if (!acceptFile)
                            return Json(new
                            {
                                status = "error",
                                msg = "The file you are trying to upload is not a permitted file type!"
                            });

                        // check directory exist or not
                        var schoolid = HttpContext.Session["SchoolId"];
                        if (Isphoto == 1)
                        {
                            var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/GuardianPhotos"));
                            if (!Directory.Exists(directory))
                                Directory.CreateDirectory(directory);

                            path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/GuardianPhotos"), fileName);
                        }
                        else
                        {
                            var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/" + uniqueno));
                            if (!Directory.Exists(directory))
                                Directory.CreateDirectory(directory);

                            path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/" + uniqueno), fileName);
                        }
                        // Get the complete folder path and store the file inside it.  
                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);

                        if (Isphoto == 1)
                        {
                            var resizeratio = _ISchoolSettingService.GetAttributeValue("GuardianPhotoRatio");

                            var height = Convert.ToInt32(resizeratio.Split(',')[0]);
                            var width = Convert.ToInt32(resizeratio.Split(',')[1]);

                            var resizedimg = _ISchoolDataService.ResizeImage(file, "guardianimage", height, width);
                            resizedimg.Save(path, fileExtension.Replace(".", ""));
                        }
                        else
                            file.SaveAs(path);
                    }
                    // Returns message that successfully uploaded  
                    return Json(new
                    {
                        status = "success",
                        path = path,
                        msg = "File Uploaded Successfully!"
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        status = "error",
                        msg = "Error occurred. Error details: " + ex.Message
                    });
                }
            }
            else
            {
                return Json(new
                {
                    status = "error",
                    msg = "No files selected."
                });
            }
        }

        public ActionResult SetDynamicNavItem(string NavItem)
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                if (!string.IsNullOrEmpty(NavItem))
                {
                    Session["CokNavItem"] = NavItem;
                }

                return Json(new
                {
                    status = "success"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }

        public ActionResult CheckAndValiadteMobileNo(string MobNo)
        {
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;
            }
            else
            {
                return RedirectToAction("DummyAction", "Common", new { ctrl = "Home", act = "PasswordRecovery" });
            }

            try
            {
                // check mobile no exist
                // mobile type
                var mobtype = _IContactService.GetAllContactInfoTypes(ref count, "Mobile").FirstOrDefault();
                var contactinfos = _IContactService.GetAllContactInfos(ref count, ContactInfo: MobNo);
                if (mobtype != null)
                    contactinfos = contactinfos.Where(c => c.ContactInfoTypeId == mobtype.ContactInfoTypeId).ToList();

                if (contactinfos.Count > 0)
                {
                    var ismobilnoexist = contactinfos.FirstOrDefault();
                    // get otp length setting
                    var usertype = ismobilnoexist.ContactType.ContactType1;

                    if (ismobilnoexist.ContactType.ContactType1 == "Guardian")
                    {
                        var studentid = _IGuardianService.GetGuardianById(GuardianId: (int)ismobilnoexist.ContactId).StudentId;
                        var isStudentAdmitted = _IStudentService.GetStudentById(StudentId: (int)studentid);
                        if (isStudentAdmitted.AdmnNo == null)
                            return Json(new { status = "failed", data = "The student is not admitted yet." });

                        var Studentclassrecord = _IStudentService.GetAllStudentClasss(ref count, StudentId: (int)studentid).FirstOrDefault();
                        if (Studentclassrecord != null)
                        {
                            var isstudentassignedsection = Studentclassrecord.ClassId;
                            if (isstudentassignedsection == 0 || isstudentassignedsection == null)
                            {
                                return Json(new { status = "failed", data = "The Student has not been assigned any Section yet" });
                            }
                            var isstudentassignedrollno = Studentclassrecord.RollNo;
                            if (String.IsNullOrEmpty(isstudentassignedrollno))
                            {
                                return Json(new { status = "failed", data = "The Student has not been assigned any Roll No yet" });
                            }
                        }
                        var studentStatus = _IStudentMasterService.GetAllStudentStatusDetail(ref count, StudentId: studentid);
                        if (studentStatus.Count > 0)
                        {
                            var status = studentStatus.OrderByDescending(p => p.StatusChangeDate).ThenByDescending(p => p.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus;
                            if (status == "Left")
                                return Json(new { status = "failed", data = "Member Inactive" });

                        }
                        if (ismobilnoexist.ContactType.SignUpOTP != true)
                        {
                            return Json(new { status = "failed", data = "OTP Services Blocked.Please Contact School." });
                        }
                    }
                    if (ismobilnoexist.ContactType.ContactType1 == "Student")
                    {
                        var isStudentAdmitted = _IStudentService.GetStudentById(StudentId: (int)ismobilnoexist.ContactId);
                        if (isStudentAdmitted.AdmnNo == null)
                            return Json(new { status = "failed", data = "The student is not admitted yet." });

                        var Studentclassrecord = _IStudentService.GetAllStudentClasss(ref count, StudentId: (int)ismobilnoexist.ContactId).FirstOrDefault();
                        if (Studentclassrecord != null)
                        {
                            var isstudentassignedsection = Studentclassrecord.ClassId;
                            if (isstudentassignedsection == 0 || isstudentassignedsection == null)
                            {
                                return Json(new { status = "failed", data = "The Student has not been assigned any Section yet" });
                            }
                            var isstudentassignedrollno = Studentclassrecord.RollNo;
                            if (String.IsNullOrEmpty(isstudentassignedrollno))
                            {
                                return Json(new { status = "failed", data = "The Student has not been assigned any Roll No yet" });
                            }
                        }

                        var studentStatus = _IStudentMasterService.GetAllStudentStatusDetail(ref count, StudentId: (int)ismobilnoexist.ContactId);
                        if (studentStatus.Count > 0)
                        {
                            var status = studentStatus.OrderByDescending(p => p.StatusChangeDate).ThenByDescending(p => p.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus;
                            if (status == "Left")
                                return Json(new { status = "failed", data = "Member Inactive" });
                        }
                        if (ismobilnoexist.ContactType.SignUpOTP != true)
                        {
                            return Json(new { status = "failed", data = "OTP Services Blocked.Please Contact School." });
                        }
                    }

                    if (usertype.ToLower() == "teacher")
                    {
                        var staff = _IStaffService.GetStaffById(StaffId: (int)ismobilnoexist.ContactId);
                        if (staff == null)
                            return Json(new { status = "failed", data = "The Teacher not found." });
                        else
                        {
                            if (!(bool)staff.IsActive)
                                return Json(new { status = "failed", data = "Member Inactive" });
                        }
                        if (ismobilnoexist.ContactType.SignUpOTP != true)
                        {
                            return Json(new { status = "failed", data = "OTP Services Blocked By School.Please Contact School." });
                        }
                    }
                    if (usertype.ToLower() == "non-teaching")
                    {
                        var staff = _IStaffService.GetStaffById(StaffId: (int)ismobilnoexist.ContactId);
                        if (staff == null)
                            return Json(new { status = "failed", data = "The Teacher not found." });
                        else
                        {
                            if (!(bool)staff.IsActive)
                                return Json(new { status = "failed", data = "Member Inactive" });
                        }
                        if (ismobilnoexist.ContactType.SignUpOTP != true)
                        {
                            return Json(new { status = "failed", data = "OTP Services Blocked.Please Contact School." });
                        }
                    }
                    //var contactInfoTypeId = _IContactService.GetAllContactInfoTypes(ref count, ContactInfoType: "Mobile").Select(m => m.ContactInfoTypeId).FirstOrDefault();
                    //var coninfo = _IContactService.GetAllContactInfos(ref count);
                    //var contactinfo = coninfo.Where(c => c.ContactId == user.UserContactId && c.ContactTypeId == user.UserTypeId && c.ContactInfoTypeId == contactInfoTypeId).FirstOrDefault();
                    //var ContactInfo = contactinfo.ContactInfo1;

                    int? lengthofotp = null;
                    var otplength = _ISchoolSettingService.GetAttributeValue("LengthOfOTP");
                    if (otplength != null && otplength != "")
                        lengthofotp = Convert.ToInt32(otplength);

                    // generate otp
                    string retOTP = "";
                    var otp = _IMainService.OneTimePasswordGenerate(ref retOTP, lengthofotp);
                    Session["kspregotp"] = otp; // otp
                    Session["kspregdt"] = DateTime.UtcNow;
                    Session["kspregmobno"] = MobNo;

                    // send otp on mobile no
                    temp_otp_detail optdetial = new temp_otp_detail();
                    optdetial.OTP = otp;
                    optdetial.OTPCreatedTime = DateTime.Now;
                    optdetial.PhoneNo = MobNo;
                    optdetial.ValidateTime = 15;
                    _ITempOTP.InsertTempOtp(optdetial);
                    //db.temp_otp_detail.Add(optdetial);
                    //db.SaveChanges();
                    var errormessage = "";
                    var content = "";
                    _ISMSSender.SendSMS(MobNo, "Your One Time Password for KSSmart is " + retOTP + " . Valid Untill 15 mins ", true, out errormessage, out content);
                    return Json(new
                    {
                        status = "success",
                        otp = retOTP
                    });
                    //return Json(new
                    //{
                    //    status = "success",
                    //    otp = retOTP
                    //});
                }
                else
                {
                    return Json(new
                    {
                        status = "failed"
                    });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult CheckAndValiadteUserId(string UserId)
        {
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;
            }
            else
            {
                return RedirectToAction("DummyAction", "Common", new { ctrl = "Home", act = "PasswordRecovery" });
            }

            try
            {
                // check user no exist
                var isuserexist = _IUserService.GetAllUsers(ref count);
                isuserexist = isuserexist.Where(u => u.UserName != null && u.UserName.ToLower() == UserId.ToLower()).ToList();
                var user = isuserexist.FirstOrDefault();


                if (user != null)
                {
                    if (user.ContactType.ContactType1.ToLower() == "student")
                    {
                        var studentstatusdetail = _IStudentMasterService.GetAllStudentStatusDetail(ref count, StudentId: user.UserContactId);
                        if (studentstatusdetail.Count > 0)
                        {
                            var currentstudentstatus = studentstatusdetail.OrderByDescending(m => m.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus;
                            if (currentstudentstatus.ToLower() == "left")
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    data = "Student is Left"
                                });
                            }
                        }
                        if (user.ContactType.ResetPwdOTP != true)
                        {
                            return Json(new
                            {
                                status = "failed",
                                data = "OTP Service Blocked.Please Contact School."
                            });
                        }

                    }
                    else if (user.ContactType.ContactType1.ToLower() == "guardian")
                    {
                        var studentid = _IGuardianService.GetGuardianById((int)user.UserContactId).StudentId;
                        var studentstatusdetail = _IStudentMasterService.GetAllStudentStatusDetail(ref count, StudentId: studentid);
                        if (studentstatusdetail.Count > 0)
                        {
                            var currentstudentstatus = studentstatusdetail.OrderByDescending(m => m.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus;
                            if (currentstudentstatus.ToLower() == "left")
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    data = "Student is Left"
                                });
                            }
                            if (user.ContactType.ResetPwdOTP != true)
                            {
                                return Json(new
                                {
                                    status = "failed",
                                    data = "OTP Service Blocked.Please Contact School."
                                });
                            }
                        }

                    }
                    else if (user.ContactType.ContactType1.ToLower() == "teacher")
                    {
                        var staffstatusdetail = _IStaffService.GetStaffById((int)user.UserContactId).IsActive;

                        if (!(bool)staffstatusdetail)
                        {
                            return Json(new
                            {
                                status = "failed",
                                data = "Teacher is InActive"
                            });
                        }
                        if (user.ContactType.ResetPwdOTP != true)
                        {
                            return Json(new
                            {
                                status = "failed",
                                data = "OTP Service Blocked.Please Contact School."
                            });
                        }
                    }
                    else if (user.ContactType.ContactType1.ToLower() == "non-teaching")
                    {
                        var staffstatusdetail = _IStaffService.GetStaffById((int)user.UserContactId).IsActive;

                        if (!(bool)staffstatusdetail)
                        {
                            return Json(new
                            {
                                status = "failed",
                                data = "Teacher is InActive"
                            });
                        }
                        if (user.ContactType.ResetPwdOTP != true)
                        {
                            return Json(new
                            {
                                status = "failed",
                                data = "OTP Service Blocked.Please Contact School."
                            });
                        }
                    }
                    var contactInfoTypeId = _IContactService.GetAllContactInfoTypes(ref count, ContactInfoType: "Mobile").Select(m => m.ContactInfoTypeId).FirstOrDefault();
                    var coninfo = _IContactService.GetAllContactInfos(ref count);
                    var contactinfo = coninfo.Where(c => c.ContactId == user.UserContactId && c.ContactTypeId == user.UserTypeId && c.ContactInfoTypeId == contactInfoTypeId).FirstOrDefault();
                    var ContactInfo = "";
                    if (contactinfo != null)
                    {
                        ContactInfo = contactinfo.ContactInfo1;
                    }
                    if (!String.IsNullOrEmpty(ContactInfo))
                    {
                        // get otp length setting
                        int? lengthofotp = null;
                        var otplength = _ISchoolSettingService.GetAttributeValue("LengthOfOTP");
                        if (otplength != null && otplength != "")
                            lengthofotp = Convert.ToInt32(otplength);

                        // generate otp
                        string retOTP = "";
                        var otp = _IMainService.OneTimePasswordGenerate(ref retOTP, lengthofotp);
                        Session["kspreguserotp"] = otp; // otp
                        Session["kspreguserdt"] = DateTime.UtcNow;
                        Session["kspreguser"] = user.UserName.ToLower();

                        // send otp on mobile no
                        temp_otp_detail optdetial = new temp_otp_detail();
                        optdetial.OTP = otp;
                        optdetial.OTPCreatedTime = DateTime.Now;
                        optdetial.PhoneNo = ContactInfo;
                        optdetial.ValidateTime = 15;
                        _ITempOTP.InsertTempOtp(optdetial);
                        //db.temp_otp_detail.Add(optdetial);
                        //db.SaveChanges();
                        var errormessage = "";
                        var content = "";
                        _ISMSSender.SendSMS(ContactInfo, "Your One Time Password for KSSmart is " + retOTP + " . Valid Untill 15 mins ", true, out errormessage, out content);
                        return Json(new
                        {
                            status = "success",
                            otp = retOTP
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            data = "Please Update Mobile No."
                        });
                    }

                }
                else
                {
                    return Json(new
                    {
                        status = "failed"
                    });
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult CheckOTPValid(RegisterModel model)
        {
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;
            }
            else
            {
                return RedirectToAction("DummyAction", "Common", new { ctrl = "Home", act = "PasswordRecovery" });
            }

            try
            {
                string result = "";
                if (model.MobileNo != "")
                {
                    if (Session["kspregmobno"].ToString() == model.MobileNo)
                    {
                        MD5CryptoServiceProvider md5hasher = new MD5CryptoServiceProvider();
                        Byte[] hashedbytes;
                        UTF8Encoding encoder = new UTF8Encoding();
                        hashedbytes = md5hasher.ComputeHash(encoder.GetBytes(model.OTP.ToString()));

                        DateTime currenttiem = Convert.ToDateTime(Session["kspregdt"]);
                        currenttiem = currenttiem.AddMinutes(5);
                        DateTime now = DateTime.UtcNow;

                        if (now <= currenttiem)
                        {
                            StringBuilder kspregotp = new StringBuilder();
                            foreach (var h in hashedbytes)
                                kspregotp.Append(h.ToString("x2"));

                            if (Session["kspregotp"].ToString() != kspregotp.ToString())
                                result = "Invalid otp";
                            else
                                return Json(new
                                {
                                    status = "success"
                                });
                        }
                        else
                            result = "Time out";
                    }
                    else
                        result = "Mobile No. and OTP combination not correct";
                }
                else
                    result = "Invalid OTP";

                return Json(new
                {
                    status = "failed",
                    data = result
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult CheckUserOTPValid(RegisterModel model)
        {
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;
            }
            else
            {
                return RedirectToAction("DummyAction", "Common", new { ctrl = "Home", act = "PasswordRecovery" });
            }

            try
            {
                string result = "";
                // check user no exist
                var isuserexist = _IUserService.GetAllUsers(ref count, username: model.UserId);
                isuserexist = isuserexist.Where(u => u.UserName.ToLower() == model.UserId.ToLower()).ToList();
                var user = isuserexist.FirstOrDefault();
                if (user != null)
                {
                    if (Session["kspreguser"].ToString() == model.UserId.ToLower())
                    {
                        MD5CryptoServiceProvider md5hasher = new MD5CryptoServiceProvider();
                        Byte[] hashedbytes;
                        UTF8Encoding encoder = new UTF8Encoding();
                        hashedbytes = md5hasher.ComputeHash(encoder.GetBytes(model.OTP.ToString()));

                        DateTime currenttiem = Convert.ToDateTime(Session["kspreguserdt"]);
                        currenttiem = currenttiem.AddMinutes(5);
                        DateTime now = DateTime.UtcNow;

                        if (now <= currenttiem)
                        {
                            StringBuilder kspregotp = new StringBuilder();
                            foreach (var h in hashedbytes)
                                kspregotp.Append(h.ToString("x2"));

                            if (Session["kspreguserotp"].ToString() != kspregotp.ToString())
                                result = "Invalid otp";
                            else
                                return Json(new
                                {
                                    status = "success"
                                });
                        }
                        else
                            result = "Time out";
                    }
                    else
                        result = "UserId and OTP combination not correct";
                }
                else
                    result = "Invalid OTP";

                return Json(new
                {
                    status = "failed",
                    data = result
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult GetCityList()
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                var citylist = new List<SelectListItem>();

                var cities = _IAddressMasterService.GetAllAddressCitys(ref count).ToList();
                if (cities.Count > 0)
                {
                    citylist.Add(new SelectListItem { Selected = false, Value = "", Text = "--select--" });
                    foreach (var item in cities)
                        citylist.Add(new SelectListItem { Selected = false, Value = item.CityId.ToString(), Text = item.City });
                }

                var schooldata = _ISchoolDataService.GetSchoolDataDetail(ref count).FirstOrDefault();

                var cityid = "";
                var schooladdress = "";

                if (schooldata != null)
                {
                    cityid = schooldata.CityId.ToString();
                    schooladdress = schooldata.SchoolAddress;
                }

                var latitude = "";
                var longitude = "";

                latitude = _ISchoolSettingService.GetAttributeValue("SchoolLatitude");
                longitude = _ISchoolSettingService.GetAttributeValue("SchoolLongitude");

                return Json(new
                {
                    status = "success",
                    citylist = citylist,
                    cityid = cityid,
                    latitude = latitude,
                    longitude = longitude,
                    schooladdress = schooladdress
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult SaveSchoolData(string SchoolName, string SchoolLogo, string SchoolAddress = "", string City = "", string lati = "", string longi = "")
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                var schoollat = _ISchoolSettingService.GetSchoolSettingByAttribute("SchoolLatitude");
                var schoollong = _ISchoolSettingService.GetSchoolSettingByAttribute("SchoolLongitude");
                var schoolName = _ISchoolSettingService.GetSchoolSettingByAttribute("SchoolName");

                var schooldata = _ISchoolDataService.GetSchoolDataDetail(ref count).FirstOrDefault();
                if (schooldata == null)
                {
                    var imagearray = SchoolLogo.Split('/');
                    var data = new SchoolData();
                    data.SchoolName = SchoolName;
                    data.Logo = imagearray[(SchoolLogo.Split('/').Length - 1)];
                    //schooldata.Logo = "Ksmartlogo.png";
                    data.CityId = (City != null && City != "") ? Convert.ToInt32(City) : (int?)null;
                    data.SchoolAddress = SchoolAddress;
                    _ISchoolDataService.InsertSchoolData(data);

                    schoolName.AttributeValue = SchoolName;
                    _ISchoolSettingService.UpdateSchoolSetting(schoolName);


                    schoollat.AttributeValue = lati;
                    _ISchoolSettingService.UpdateSchoolSetting(schoollat);

                    schoollong.AttributeValue = longi;
                    _ISchoolSettingService.UpdateSchoolSetting(schoollong);

                    return Json(new
                    {
                        status = "success",
                        SchoolName = data.SchoolName,
                        Logo = data.Logo
                    });
                }
                else
                {
                    var imagearray = SchoolLogo.Split('/');
                    schooldata.SchoolName = SchoolName;
                    schooldata.Logo = imagearray[(SchoolLogo.Split('/').Length - 1)];
                    //schooldata.Logo = "Ksmartlogo.png";
                    schooldata.CityId = (City != null && City != "") ? Convert.ToInt32(City) : (int?)null;
                    schooldata.SchoolAddress = SchoolAddress;
                    _ISchoolDataService.UpdateSchoolData(schooldata);

                    schoolName.AttributeValue = SchoolName;
                    _ISchoolSettingService.UpdateSchoolSetting(schoolName);


                    schoollat.AttributeValue = lati;
                    _ISchoolSettingService.UpdateSchoolSetting(schoollat);

                    schoollong.AttributeValue = longi;
                    _ISchoolSettingService.UpdateSchoolSetting(schoollong);

                    return Json(new
                    {
                        status = "success",
                        SchoolName = schooldata.SchoolName,
                        Logo = SchoolLogo
                    });
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        //upload school logo
        public ActionResult UploadSchoolLogo()
        {
            if (Request.Files.Count > 0)
            {
                string storedlogo = String.Empty;
                string path = String.Empty;
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //file
                        HttpPostedFileBase file = files[i];
                        // get Filename
                        string fileName = file.FileName.Replace(" ", "");

                        //check that the file is of the permitted file type
                        string fileExtension = Path.GetExtension(fileName).ToLower();

                        string[] acceptedFileTypes = new string[3];
                        acceptedFileTypes[0] = ".jpg";
                        acceptedFileTypes[1] = ".jpeg";
                        acceptedFileTypes[2] = ".png";

                        bool acceptFile = false;
                        //should we accept the file?
                        for (int j = 0; j < acceptedFileTypes.Count(); j++)
                        {
                            if (fileExtension == acceptedFileTypes[j])
                            {
                                //accept the file, yay!
                                acceptFile = true;
                                break;
                            }
                        }

                        if (!acceptFile)
                        {
                            return Json(new
                            {
                                status = "error",
                                msg = "The file you are trying to upload is not a permitted file type!"
                            });
                        }

                        // check directory exist or not
                        var schoolid = HttpContext.Session["SchoolId"];

                        var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);


                        var extensions = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase) { ".jpg", ".jpeg", ".png" };

                        var filestodelete = new DirectoryInfo(Server.MapPath("~/Images/" + schoolid)).GetFiles()
                                                             .Where(p => extensions.Contains(p.Extension));
                        foreach (var fl in filestodelete)
                        {
                            fl.Attributes = FileAttributes.Normal;
                            System.IO.File.Delete(fl.FullName);
                        }

                        path = Path.Combine(Server.MapPath("~/Images/" + schoolid), fileName.Replace(" ", ""));

                        var resizeratio = _ISchoolSettingService.GetorSetSchoolData("SchoolLogoSize", "200,200").AttributeValue;
                        var height = Convert.ToInt32(resizeratio.Split(',')[0]);
                        var width = Convert.ToInt32(resizeratio.Split(',')[1]);

                        var resizedimg = _ISchoolDataService.ResizeImage(file, "SchoolLogo", height, width);
                        //var resizedimg = _ISchoolDataService.ResizeImage(file, "SchoolLogo");

                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);

                        resizedimg.Save(path);

                        storedlogo = _WebHelper.GetStoreLocation() + "Images/" + schoolid + "/" + fileName;
                    }
                    // Returns message that successfully uploaded  
                    return Json(new
                    {
                        status = "success",
                        path = path,
                        storedlogo = storedlogo,
                        msg = "File Uploaded Successfully!"
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        status = "error",
                        msg = "Error occurred. Error details: " + ex.Message
                    });
                }
            }
            else
            {
                return Json(new
                {
                    status = "error",
                    msg = "No files selected."
                });
            }
        }

        public ActionResult GenerateLoginOTP()
        {
            // current user
            SchoolUser user = new SchoolUser();
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return Json(new
                    {
                        status = "failed",
                        data = "Session expire"
                    });
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }

            try
            {
                if (user.UserContactId != null && user.UserContactId > 0)
                {

                    var staff = _IStaffService.GetStaffById((int)user.UserContactId);

                    if (staff == null)
                    {
                        return Json(new
                        {
                            status = "failed",
                            data = "No User Found for Admin"
                        });
                    }
                    var contactInfoTypeId = _IContactService.GetAllContactInfoTypes(ref count, ContactInfoType: "Mobile").Select(m => m.ContactInfoTypeId).FirstOrDefault();
                    var coninfo = _IContactService.GetAllContactInfos(ref count);
                    var contactinfo = coninfo.Where(c => c.ContactId == user.UserContactId && c.ContactTypeId == staff.ContactTypeId && c.ContactInfoTypeId == contactInfoTypeId).FirstOrDefault();
                    var ContactInfo = "";
                    if (contactinfo != null)
                    {
                        ContactInfo = contactinfo.ContactInfo1;
                    }
                    if (!String.IsNullOrEmpty(ContactInfo))
                    {
                        // get otp length setting
                        int? lengthofotp = null;
                        var otplength = _ISchoolSettingService.GetAttributeValue("LengthOfOTP");
                        if (otplength != null && otplength != "")
                            lengthofotp = Convert.ToInt32(otplength);

                        // generate otp
                        string retOTP = "";
                        var otp = _IMainService.OneTimePasswordGenerate(ref retOTP, lengthofotp);
                        //_ISchoolSettingService.GetorSetSchoolData("OTP for Admin Login", otp, null, null);
                        user.LoginOTP = retOTP;
                        _IUserService.UpdateUser(user);

                        var errormessage = "";
                        var content = "";
                        _ISMSSender.SendSMS(ContactInfo, "Your One Time Password for KSSmart is " + retOTP + " . Valid Untill 15 mins ", true, out errormessage, out content);
                        return Json(new
                        {
                            status = "success",
                            otp = retOTP
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            status = "failed",
                            data = "No Mobile No. found for User"
                        });
                    }

                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        data = "No User Found for Admin"
                    });
                }



            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
                return Json(new
                {
                    status = "failed",
                    msg = "NotValid",
                    data = ex.Message
                });
            }
        }
        #endregion


        [HttpPost]
        public ActionResult SaveUnSelectedNav(int UserRoleId, int UserRoleTypeId, string ClickedNavigationId)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                var permarray = ClickedNavigationId.Split(',');
                var userNavigationItems = _INavigationService.GetAllUserRoleNavigationItems(ref count).Where(p => p.UserRoleId == UserRoleId).ToList();
                if (userNavigationItems.Count > 0)
                {
                    if (userNavigationItems.FirstOrDefault().UserRoleTypeId == UserRoleTypeId)
                    {
                        foreach (var item in permarray)
                        {
                            var record = userNavigationItems.Where(p => p.NavigationItemId == Convert.ToInt32(item) && p.UserRoleId == UserRoleId && p.UserRoleTypeId == UserRoleTypeId).FirstOrDefault();
                            if (record != null)
                            {
                                record.NavigationItemId = Convert.ToInt32(item);
                                record.UserRoleId = UserRoleId;
                                record.IsActive = true;
                                record.UserRoleTypeId = UserRoleTypeId;
                                _INavigationService.UpdateUserRoleNavigationItem(record);
                            }
                            else
                            {
                                var recordd = new UserRoleNavigationItem();
                                recordd.UserRoleId = UserRoleId;
                                recordd.IsActive = true;
                                recordd.NavigationItemId = Convert.ToInt32(item);
                                recordd.UserRoleTypeId = UserRoleTypeId;
                                _INavigationService.InsertUserRoleNavigationItem(recordd);
                            }
                        }
                    }
                }
                else
                {
                    foreach (var item in permarray)
                    {
                        var record = new UserRoleNavigationItem();
                        record.UserRoleId = UserRoleId;
                        record.IsActive = true;
                        record.NavigationItemId = Convert.ToInt32(item);
                        record.UserRoleTypeId = UserRoleTypeId;
                        _INavigationService.InsertUserRoleNavigationItem(record);
                    }
                }
                //return RedirectToAction("Index");
                return Json(new
                {
                    status = "success",
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "error",
                    data = ex.Message
                });
            }
        }

        [HttpPost]
        public ActionResult SaveUnSelectedPer(UserRolePermissionModel model)
        {
            SchoolUser user = new SchoolUser();
            // current user
            if (HttpContext.Session["SchoolDB"] != null)
            {
                // re-assign session
                var sessiondb = HttpContext.Session["SchoolDB"];
                HttpContext.Session["SchoolDB"] = sessiondb;

                user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                if (user == null)
                    return RedirectToAction("Login", "Home");
                SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
            }
            else
            {
                // get action name
                var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
            }
            try
            {
                if (!string.IsNullOrEmpty(model.UserRoleId) && !string.IsNullOrEmpty(model.UserRoleTypeId))
                {
                    int? nullvar = null;
                    var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                    var logtypeEdit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                    var premtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "UserRoleActionPermission").FirstOrDefault();

                    System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                    IList<UserRoleActionPermissionModel> permarray = js.Deserialize<List<UserRoleActionPermissionModel>>(model.permarray);
                    var UserRoleActionPermission = new UserRoleActionPermission();
                    // loop
                    foreach (var item in permarray)
                    {
                        if (item.UserRoleActionPermissionId > 0)
                        {
                            UserRoleActionPermission = _IUserManagementService.GetUserRoleActionPermissionById(item.UserRoleActionPermissionId);
                            if (UserRoleActionPermission != null)
                            {
                                UserRoleActionPermission.IsPermitted = item.IsPermitted;
                                _IUserManagementService.UpdateUserRoleActionPermission(UserRoleActionPermission);
                                if (premtable != null)
                                {
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, premtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(UserRoleActionPermission.UserRoleActionPermissionId, premtable.TableId, logtypeEdit.ActivityLogTypeId, user, String.Format("Update UserRoleActionPermission with UserRoleActionPermissionId:{0} and AppFormId:{1}", UserRoleActionPermission.UserRoleActionPermissionId, UserRoleActionPermission.AppFormId), Request.Browser.Browser);
                                }

                            }
                        }
                        else
                        {
                            UserRoleActionPermission = new UserRoleActionPermission();
                            UserRoleActionPermission.UserRoleId = Convert.ToInt32(model.UserRoleId);
                            UserRoleActionPermission.AppFormId = item.AppFormId;
                            UserRoleActionPermission.AppFormActionId = item.AppFormActionId > 0 ? item.AppFormActionId : nullvar;
                            UserRoleActionPermission.IsForm = item.IsForm;
                            UserRoleActionPermission.IsPermitted = item.IsPermitted;
                            _IUserManagementService.InsertUserRoleActionPermission(UserRoleActionPermission);
                            if (premtable != null)
                            {
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, premtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(UserRoleActionPermission.UserRoleActionPermissionId, premtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add UserRoleActionPermission with UserRoleActionPermissionId:{0} and AppFormId:{1}", UserRoleActionPermission.UserRoleActionPermissionId, UserRoleActionPermission.AppFormId), Request.Browser.Browser);
                            }
                        }
                    }

                    //save latest permissions
                    var SchoolDbId = Convert.ToString(System.Web.HttpContext.Current.Session["SchoolId"]);
                    var ListUserActionPermission = new List<UserRoleActionPermission>();
                    ListUserActionPermission = _IUserManagementService.GetAllUserRoleActionPermissionLessColumns(ref count, user.UserRoleId).ToList();

                    _ICommonMethodService.UpdateCreateUserRoleActionPermissionTextFile(SchoolDbId, ListUserActionPermission);
                    return Json(new
                    {
                        status = "success",
                    });
                }
                return Json(new
                {
                    status = "error",
                    msg="Invalid User Details"
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "error",
                    data = ex.Message
                });
            }
        }
    }


}