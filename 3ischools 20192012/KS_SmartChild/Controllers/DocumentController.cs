﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Windows;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BAL.BAL.Common;
using ViewModel.ViewModel.Library;
using System.Data;
using System.Text;
using DAL.DAL.LibraryModule;
using DAL.DAL.UserModule;
using DAL.DAL.Common;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.SettingService;
using DAL.DAL.CatalogMaster;
using BAL.BAL.Security;
using KSModel.Models;
using DAL.DAL.DocumentModule;
using ViewModel.ViewModel.DocumentManager;
using DAL.DAL.Kendo;

namespace KS_SmartChild.Controllers
{
    public class DocumentController : Controller
    {
        #region fields
        public int count = 0;
        public static string BookID = "";
        public static string BookTitle = "";
        private readonly IBookMasterService _IBookMasterService;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ILogService _Logger;
        private readonly IWebHelper _WebHelper;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IExportHelper _IExportHelper;
        private readonly IDocumentService _IDocumentService;
        private readonly ICommonMethodService _ICommonMethodService;
        #endregion

        #region ctor

        public DocumentController(
            IConnectionService IConnectionService, IBookMasterService IBookMasterService,
            IDropDownService IDropDownService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            ILogService Logger, IUserService IUserService,
            IWebHelper WebHelper, ICatalogMasterService ICatalogMasterService,
            ISchoolSettingService ISchoolSettingService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption,
            IExportHelper IExportHelper,
            IDocumentService IDocumentService,
            ICommonMethodService ICommonMethodService)
        {
            this._IBookMasterService = IBookMasterService;
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._Logger = Logger;
            this._WebHelper = WebHelper;
            this._ISchoolSettingService = ISchoolSettingService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IExportHelper = IExportHelper;
            this._IDocumentService = IDocumentService;
            this._ICommonMethodService = ICommonMethodService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        #endregion

        public ActionResult Document()
        {
            SchoolUser user = new SchoolUser();
            var model = new DocumentManager();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                //paging
                model.gridPageSizes = _ISchoolSettingService.GetorSetSchoolData("gridPageSizes", "20,50,100,250").AttributeValue;
                model.defaultGridPageSize = _ISchoolSettingService.GetorSetSchoolData("defaultGridPageSize", "20").AttributeValue;

                var DcoumentCategories = _IDocumentService.GetAllDocumentCategory(ref count, IsActive: true);
                model.AvailableDocCategories.Add(new SelectListItem()
                {
                    Text = "--Select--",
                    Value = ""
                });
                foreach (var doc in DcoumentCategories)
                {
                    model.AvailableDocCategories.Add(new SelectListItem()
                    {
                        Text = doc.DocumentCategory1,
                        Value = doc.DocumentCategoryId.ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult DocumentType()
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                ////////
                var DocCtgryList = new List<SelectListItem>();

                var DcoumentCategories = _IDocumentService.GetAllDocumentCategory(ref count, IsActive: true);
                DocCtgryList.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });

                foreach (var Doc in DcoumentCategories)
                    DocCtgryList.Add(new SelectListItem { Text = Doc.DocumentCategory1, Value = Doc.DocumentCategoryId.ToString(), Selected = false });

                return Json(DocCtgryList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", Message = "Error Happened" });
            }
        }

        [HttpGet]
        public ActionResult TitleCategory(int dcmntid)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                string Title = "";
                var Docs = _IDocumentService.GetDocumentById(dcmntid);
                if (Docs != null)
                {
                    Title = Docs.DocumentTitle;
                }
                //using (DevKsEntities dc = new DevKsEntities())
                //{
                //    var Datas = dc.Documents.Where(a => a.DocumentId == dcmntid);
                //    var Data = Datas.Join(dc.Documents, a => a.DocumentId, 
                //            b => b.DocumentId, (a, b) => new { a.DocumentCategoryId, 
                //                DocumentCategory = a.DocumentCategory.DocumentCategory1, 
                //                a.DocumentDescription, a.DocumentTitle, a.DocumentId }).FirstOrDefault();
                return Json(Title, JsonRequestBehavior.AllowGet);
                //}
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", Message = "Error Happened" });
            }
        }
      

        [HttpPost]
        public ActionResult GetDocuments(DataSourceRequest command, DocmentCategoryModel model, IEnumerable<Sort> sort = null,
            string TitleSearch="",string DescriptionSearch="",int GroupIdSearch=0)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                var DocumentCatlist = new List<DocmentCategoryModel>();
                var DocumentList = _IDocumentService.GetAllDocument(ref count);
                // Filter 
                if (GroupIdSearch > 0)
                    DocumentList = DocumentList.Where(m => m.DocumentCategoryId == GroupIdSearch).ToList();
                if (!string.IsNullOrEmpty(TitleSearch))
                    DocumentList = DocumentList.Where(m => m.DocumentTitle.ToLower().Contains(TitleSearch.ToLower())).ToList();
                if (!string.IsNullOrEmpty(DescriptionSearch))
                    DocumentList = DocumentList.Where(m => m.DocumentDescription.ToLower().Contains(DescriptionSearch.ToLower())).ToList();

                var DocumentDetails=_IDocumentService.GetAllDocumentDetail(ref count).ToList();
                var expdt="";
                foreach (var i in DocumentList)
                {
                expdt="";
                var dcmntdtl=DocumentDetails.Where(m=>m.DocumentId==i.DocumentId).ToList();
                if (dcmntdtl.Count > 0)
                   expdt= dcmntdtl.OrderByDescending(m => m.ExpiryDate).FirstOrDefault().ExpiryDate.Value.Date.ToString("dd/MM/yyyy");

                    DocumentCatlist.Add(new DocmentCategoryModel()
                     {
                         DocumentCategoryId = (int)i.DocumentCategoryId,
                         DocumentCategory = i.DocumentCategory.DocumentCategory1,
                         DocumentDescription = i.DocumentDescription,
                         documntdscrptn = i.DocumentDescription,
                         DocumentTitle = i.DocumentTitle,
                         DocumentId = i.DocumentId,
                         SR = i.DocumentDetails.Where(m=>m.DocumentId==i.DocumentId).Count(),
                         ExpiryDate = expdt,
                    });
                }
                var gridModel = new DataSourceResult
                {
                    Data = DocumentCatlist.AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = DocumentCatlist.Count()
                };

                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }

        public ActionResult GridManageData(DataSourceRequest command, IEnumerable<Sort> sort = null, int Id = 0)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                int documentid = 0;
                var DocDetail = new List<DocumentDetail>();
                DocDetail=   _IDocumentService.GetAllDocumentDetail(ref count).ToList();
                var DocumentDetailList = new List<DocmentDetailModel>();
                if (Id > 0)
                {
                    documentid = Id;
                    DocDetail = DocDetail.Where(m=>m.DocumentId==documentid).ToList();
                }

                var schoolid = HttpContext.Session["SchoolId"];

                foreach (var i in DocDetail)
                {
                    DocumentDetailList.Add(new DocmentDetailModel()
                    {
                        DocumentDetailId = (int)i.DocumentDetailId,
                        DocumentImage = _WebHelper.GetStoreLocation() + "Images/" + schoolid + "/Documents/" + i.DocumentImage,
                        ImagePathSpilt = i.DocumentImage.Split('.').Last(),
                        ExpiryDt = i.ExpiryDate.Value.Date.ToString("dd/MM/yyyy"),
                        IssuedDate = i.IssuedOn.Value.Date.ToString("dd/MM/yyyy"),
                        DocumentId = (int)i.DocumentId,
                        Remarks = i.Remarks,
                        Remarkssubstring = i.Remarks,
                        ReminderDt = i.ReminderDate.Value.Date.ToString("dd/MM/yyyy"),
                        UplodedOn = (DateTime)i.UploadedOn,
                        DocumentTitle = i.Document.DocumentTitle,
                        MangeGridCount = DocDetail.Count(),
                        ReminderType = i.DocReminderType.ReminderType
                       
                    });
                }

                var gridModel = new DataSourceResult
                {
                    Data = DocumentDetailList.AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = DocDetail.Count()
                };

                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }
     
        public ActionResult DocumentExport()
        {
            return Json(new
            {
                status = "success",
                data = ""
            }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DocumentReport(int DocumentId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                if (DocumentId > 0)
                {
                    var tableHtml = "";
                    var DocDetailData = _IDocumentService.GetAllDocumentDetail(ref count, DocumentId);
                    if (DocDetailData.Count() > 0)
                    {
                        var Document = _IDocumentService.GetDocumentById(DocumentId);
                        tableHtml += "<html><body><label><b>Category:</b>" + Document.DocumentCategory.DocumentCategory1 + "</label><br/><label><b>Title:</b>"
                                        + Document.DocumentTitle + "</label><table border='1'><thead><th>S.No</th><th>IssuedOn</th><th>ExpireDate</th><th>UploadedOn</th><th>Remarks</th></thead><tbody>";
                        var sn = 0;
                        foreach (var det in DocDetailData)
                        {
                            sn += 1;
                            tableHtml += "<tr><td>" + sn + "</td><td>" + det.IssuedOn.Value.Date.ToString("dd/MM/yyyy")
                                            + "</td><td>" + det.ExpiryDate.Value.Date.ToString("dd/MM/yyyy")
                                            + "</td><td>" + det.UploadedOn.Value.Date.ToString("dd/MM/yyyy")
                                            + "</td><td>" + det.Remarks + "</td></tr>";
                        }
                        tableHtml += "</tbody></table></body></html>";
                        _IExportHelper.ExportToPrint(Title: "Document", html: tableHtml);
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", Message = "Error Happened" });
            }
        }

        [HttpPost]
        public ActionResult SaveDocument(int DocumentCategoryId, string Title, string Description)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }

                if (Title != null)
                {
                    Document dc = new Document();
                    dc.DocumentTitle = Title;
                    dc.DocumentDescription = Description;
                    dc.DocumentCategoryId = DocumentCategoryId;

                    _IDocumentService.InsertDocument(dc);
                }
                return Json(new { status = "success", message = "Added succssfully" });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", Message = "Error Happened" });
            }
        }

        [HttpPost]
        public ActionResult UpdateDocument(int DocumentId, string Title, string Description, int DocumentCategoryId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                if (Title != null)
                {
                    var UpdateData = _IDocumentService.GetDocumentById(DocumentId,false);
                    UpdateData.DocumentTitle = Title;
                    UpdateData.DocumentCategoryId = DocumentCategoryId;
                    UpdateData.DocumentDescription = Description;
                    _IDocumentService.UpdateDocument(UpdateData);
                }

                return Json(new { status = "success", message = "Updated succssfully" });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", Message = "Error Happened" });
            }
        }

        [HttpPost]
        public ActionResult DeleteDocument(int DocumentId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                if (DocumentId > 0)
                    _IDocumentService.DeleteDocument(DocumentId);

                return Json(new { status = "success", message = "Deleted succssfully" });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", Message = "Error Happened" });
            }
        }

        [HttpPost]
        public ActionResult UploadFiles()
        {
            string path = "";
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  

                        HttpPostedFileBase file = files[i];
                        string fname;

                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = file.FileName;
                        }
                        // check directory exist or not
                        var schoolid = HttpContext.Session["SchoolId"];
                        var directory = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Documents"));
                        if (!Directory.Exists(directory))
                            Directory.CreateDirectory(directory);

                        path = Path.Combine(Server.MapPath("~/Images/" + schoolid + "/Documents"), fname);

                        // Get the complete folder path and store the file inside it.  

                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);

                        file.SaveAs(path);
                        //// Get the complete folder path and store the file inside it.  
                        //fname = Path.Combine(Server.MapPath("~/Images/"), fname);
                        //file.SaveAs(fname);
                    }
                    // Returns message that successfully uploaded  
                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }

        [HttpGet]
        public ActionResult RemainderTypes()
        {
            SchoolUser user = new SchoolUser();
            try
            {
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                var DocReminderTypeList = new List<SelectListItem>();
                var ReminderTypeList = _IDocumentService.GetAllDocReminderTypes(ref count, IsActive: true).ToList();
                DocReminderTypeList.Add(new SelectListItem { Text = "--Select--", Value = "", Selected = false });
                foreach (var DocRmndr in ReminderTypeList)
                    DocReminderTypeList.Add(new SelectListItem { Text = DocRmndr.ReminderType, Value = DocRmndr.ReminderTypeId.ToString(), Selected = false });
                return Json(DocReminderTypeList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", Message = "Error Happened" });
            }
        }

        [HttpGet]
        public ActionResult RemainderTypeWeekly()
        {
            SchoolUser user = new SchoolUser();
            try
            {
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                var Data = _IDocumentService.GetAllDocReminderTypes(ref count, IsActive: true, ReminderType: "daily");
                return Json(Data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", Message = "Error Happened" });
            }

        }

        [HttpPost]
        public ActionResult AddDetail(int DocumentId, string IssueDate, string ExpiryDate, string RemiderTypeDate,
                                    int ReminderType, string img, string Remarks)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                if (DocumentId > 0)
                {
                    DocumentDetail dd = new DocumentDetail();
                    DateTime IssuedOn = Convert.ToDateTime(IssueDate);
                    DateTime ExpireDate = Convert.ToDateTime(ExpiryDate);
                    DateTime ReminderDate = Convert.ToDateTime(RemiderTypeDate);
                    dd.DocumentId = DocumentId;
                    dd.IssuedOn = IssuedOn;
                    dd.ExpiryDate = ExpireDate;
                    dd.ReminderDate = ReminderDate;
                    dd.ReminderType = ReminderType;
                    dd.Remarks = Remarks;
                    dd.UploadedOn = DateTime.Now;
                    dd.DocumentImage = img;
                    _IDocumentService.InsertDocumentDetail(dd);
                }
                return Json(new { status = "success", message = "Added succssfully" });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", Message = "Error Happened" });
            }
        }
        [HttpGet]
        public ActionResult DetailEdit(int DocumentDetailId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                var data = new DocmentDetailModel();
                var schoolid = HttpContext.Session["SchoolId"];

                var Datas = _IDocumentService.GetDocumentDetailById(DocumentDetailId);
                data.SchoolId = schoolid.ToString();
                data.DocumentDetailId = Datas.DocumentDetailId;
                data.DocumentId = (int)Datas.DocumentId;
                data.DocumentImage = Datas.DocumentImage;
                data.ExpiryDt = Datas.ExpiryDate.Value.Date.ToString("dd/MM/yyyy");
                data.IssuedDate = Datas.IssuedOn.Value.Date.ToString("dd/MM/yyyy");
                data.Remarks = Datas.Remarks;
                data.ReminderDt = Datas.ReminderDate.Value.Date.ToString("dd/MM/yyyy");
                data.ReminderTypeId = (int)Datas.ReminderType;
                data.ImagePathSpilt = Datas.DocumentImage;
                return Json(new { status = "success", Data = data }, JsonRequestBehavior.AllowGet);
                // a.ExpiryDt,a.IssuedDate,a.ReminderDt,
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", Message = "Error Happened" });
            }
        }

        [HttpPost]
        public ActionResult DocumentDetailDelete(int DocumentDetailId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                if (DocumentDetailId > 0)
                {
                    _IDocumentService.DeleteDocumentDetail(DocumentDetailId);
                }
                return Json(new { status = "success", message = "Deleted succssfully" });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", Message = "Error Happened" });
            }
        }

        [HttpPost]
        public ActionResult UpdateDocumentDetail(int DocumentDetailId, string IssuedDate, string ExpDate, string ReminderDate, int ReminderType, string img, string Remarks)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                if (DocumentDetailId > 0)
                {
                    var dd = _IDocumentService.GetDocumentDetailById(DocumentDetailId);
                    DateTime IssuedOn = Convert.ToDateTime(IssuedDate);
                    DateTime ExpireDate = Convert.ToDateTime(ExpDate);
                    DateTime RemindeDate = Convert.ToDateTime(ReminderDate);
                    dd.IssuedOn = IssuedOn;
                    dd.ExpiryDate = ExpireDate;
                    dd.ReminderDate = RemindeDate;
                    dd.ReminderType = ReminderType;
                    dd.Remarks = Remarks;
                    dd.DocumentImage = img;

                    _IDocumentService.UpdateDocumentDetail(dd);
                }
                return Json(new { status = "success", message = "Updated succssfully" });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", Message = "Error Happened" });
            }
        }

        public ActionResult FilterDocumentDetail()
        {
            SchoolUser user = new SchoolUser();
            var model = new DocmentDetailModel();
            //paging
            model.gridPageSizes = _ISchoolSettingService.GetorSetSchoolData("gridPageSizes", "20,50,100,250").AttributeValue;
            model.defaultGridPageSize = _ISchoolSettingService.GetorSetSchoolData("defaultGridPageSize", "20").AttributeValue;

            return View(model);
        }

        public ActionResult FilterDetail(DataSourceRequest command, IEnumerable<Sort> sort = null, string IssueDate = "", string ExpiryDate = "", string UploadedDate = "",
            string FromDate = "", string ToDate = "", string CategoryId = "")
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                var fromdatearry = FromDate.Split('/');
                var todatearry = ToDate.Split('/');
                var fromDate = fromdatearry[1] + "/" + fromdatearry[0] + "/" + fromdatearry[2];
                var toDate = todatearry[1] + "/" + todatearry[0] + "/" + todatearry[2];
                if (CategoryId == "")
                {
                    CategoryId = "0";
                }
                int ctgryid = Convert.ToInt32(CategoryId);
                DateTime FilterFromDate = Convert.ToDateTime(fromDate);
                DateTime FilterToDate = Convert.ToDateTime(toDate);

                var DataDoc = _IDocumentService.GetAllDocumentDetail(ref count);
                if (ctgryid > 0)
                {
                    DataDoc = DataDoc.Where(a => a.Document.DocumentCategoryId == ctgryid).ToList();
                }
                if (ExpiryDate == "true")
                {
                    DataDoc = DataDoc.Where(a => a.ExpiryDate.Value.Date <= FilterToDate.Date).ToList();
                    DataDoc = DataDoc.Where(a => a.ExpiryDate.Value.Date >= FilterFromDate.Date).ToList();
                }
                if (IssueDate == "true")
                {
                    DataDoc = DataDoc.Where(a => a.IssuedOn.Value.Date <= FilterToDate.Date).ToList();
                    DataDoc = DataDoc.Where(a => a.IssuedOn.Value.Date >= FilterFromDate.Date).ToList();
                }
                if (UploadedDate == "true")
                {
                    DataDoc = DataDoc.Where(a => a.UploadedOn.Value.Date <= FilterToDate.Date).ToList();
                    DataDoc = DataDoc.Where(a => a.UploadedOn.Value.Date >= FilterFromDate.Date).ToList();
                }

                var Documentslist = new List<DocmentDetailModel>();
                foreach (var i in DataDoc)
                {
                    Documentslist.Add(new DocmentDetailModel()
                    {
                        DocumentId = (int)i.DocumentId,
                        DocumentCategory = i.Document.DocumentCategory.DocumentCategory1,
                        ExpiryDate = (DateTime)i.ExpiryDate,
                        ExpiryDt = i.ExpiryDate.Value.Date.ToString("dd/MM/yyyy"),
                        IssuedDate = i.IssuedOn.Value.Date.ToString("dd/MM/yyyy"),
                        issuedte = (DateTime)i.IssuedOn,
                        UploadedDate = i.UploadedOn.Value.Date.ToString("dd/MM/yyyy"),
                        UplodedOn = (DateTime)i.UploadedOn,
                        DocumentTitle = i.Document.DocumentTitle,
                        DocumentCategoryId = (int)i.Document.DocumentCategoryId,
                    });
                }
                var gridModel = new DataSourceResult
                {
                    Data = Documentslist.AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = Documentslist.Count()
                };

                return Json(gridModel);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new
                {
                    status = "failed",
                    data = ex.Message
                });
            }
        }
       
        public ActionResult DocumentDetailReport(int filteridby, string fromdate, string todate, string catgryid)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                var tableHtml = "";
                var fromdatearry = fromdate.Split('/');
                var todatearry = todate.Split('/');
                var fromDate = fromdatearry[1] + "/" + fromdatearry[0] + "/" + fromdatearry[2];
                var toDate = todatearry[1] + "/" + todatearry[0] + "/" + todatearry[2];
                DateTime FilterFromDate = Convert.ToDateTime(fromDate);
                DateTime FilterToDate = Convert.ToDateTime(toDate);
                if (catgryid == "")
                {
                    catgryid = "0";
                }
                int ctgryid = Convert.ToInt32(catgryid);
                var DataDoc = _IDocumentService.GetAllDocumentDetail(ref count);
                if (ctgryid > 0)
                {
                    DataDoc = DataDoc.Where(a => a.Document.DocumentCategoryId == ctgryid).ToList();
                }
                if (filteridby == 1)
                {
                    DataDoc = DataDoc.Where(a => a.ExpiryDate.Value.Date <= FilterToDate.Date).ToList();
                    DataDoc = DataDoc.Where(a => a.ExpiryDate.Value.Date >= FilterFromDate.Date).ToList();
                }
                if (filteridby == 2)
                {
                    DataDoc = DataDoc.Where(a => a.IssuedOn.Value.Date <= FilterToDate.Date).ToList();
                    DataDoc = DataDoc.Where(a => a.IssuedOn.Value.Date >= FilterFromDate.Date).ToList();
                }
                if (filteridby == 3)
                {
                    DataDoc = DataDoc.Where(a => a.UploadedOn.Value.Date <= FilterToDate.Date).ToList();
                    DataDoc = DataDoc.Where(a => a.UploadedOn.Value.Date >= FilterFromDate.Date).ToList();
                }

                if (DataDoc.Count > 0)
                {
                    tableHtml += "<html><body><table border='1'><thead><th>S.No</th><th>Category</th><th>Title</th><th>Issued On</th><th>Uploaded On</th><th>Expire On</th></thead><tbody>";
                    var sn = 0;
                    foreach (var data in DataDoc)
                    {
                        sn += 1;
                        tableHtml += "<tr><td>" + sn + "</td><td>" + data.Document.DocumentCategory.DocumentCategory1 + "</td><td>"
                            + data.Document.DocumentTitle + "</td><td>" + data.IssuedOn.Value.Date.ToString("dd/MM/yyyy") + "</td><td>"
                            + data.UploadedOn.Value.Date.ToString("dd/MM/yyyy") + "</td><td>" + data.ExpiryDate.Value.Date.ToString("dd/MM/yyyy") + "</td></tr>";
                    }
                    tableHtml += "</tbody></table></body></html>";
                    _IExportHelper.ExportToPrint(Title: "Document", html: tableHtml);
                    return null;
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return null;
            }

        }


        [HttpGet]
        public ActionResult ImgDocDetails(DataSourceRequest command, IEnumerable<Sort> sort = null, int dcmntid=0)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;

                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                var schoolid = HttpContext.Session["SchoolId"];
              var DocumentDetailList = new List<DocmentDetailModel>();
                var Docs = _IDocumentService.GetAllDocumentDetail(ref count).Where(m => m.DocumentId == dcmntid && (!string.IsNullOrEmpty(m.DocumentImage)));
                foreach (var i in Docs)
                {
                    DocumentDetailList.Add(new DocmentDetailModel()
                    {
                        DocumentImage = _WebHelper.GetStoreLocation() + "Images/" + schoolid + "/Documents/" + i.DocumentImage,
                        ImagePathSpilt = i.DocumentImage.Split('.').Last(),
                });
                }
                var gridModel = new DataSourceResult
                {
                    Data = DocumentDetailList.AsQueryable().Sort(sort).PagedForCommand(command).ToList(),
                    Total = DocumentDetailList.Count()
                };
                return Json(gridModel, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return Json(new { status = "failed", Message = "Error Happened" });
            }
        }
    }
}
