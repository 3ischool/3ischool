﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.ViewModel.InfoBrowser;
using KSModel.Models;
using BAL.BAL.Security;
using DAL.DAL.UserModule;
using DAL.DAL.Common;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.LateFeeModule;
using DAL.DAL.SettingService;
using DAL.DAL.CatalogMaster;
using DAL.DAL.StudentModule;
using BAL.BAL.Common;
using DAL.DAL.GuardianModule;
using DAL.DAL.AddressModule;
using DAL.DAL.ContactModule;
using DAL.DAL.TransportModule;
using System.Web.Configuration;
using System.IO;
using Newtonsoft.Json;
using DAL.DAL.InfoBrowser;
using DAL.DAL.Security;
using System.Data;

namespace KS_SmartChild.Controllers
{
    public class InfoBrowserController : Controller
    {
        #region"Fields"
        public int count = 0;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ILogService _Logger;
        private readonly IWebHelper _WebHelper;
        private readonly ILateFeeService _ILateFeeService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStudentService _IStudentService;
        private readonly IStudentAddOnService _IStudentAddOnService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IGuardianService _IGuardianService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly IContactService _IContactService;
        private readonly IStudentMasterService _IStudentMasterService;
        private readonly IBusService _IBusService;
        private readonly ITransportService _ITransportService;
        private readonly ISchoolDataService _ISchoolDataService;
        private readonly IObjectBrowserInfo _IObjectBrowserInfo;
        private readonly IEncryptionService _IEncryptionService;
        #endregion
        #region ctor

        public InfoBrowserController(IUserService IUserService,
            IConnectionService IConnectionService,
            IDropDownService IDropDownService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            ILogService Logger, ISchoolDataService ISchoolDataService,
            IWebHelper WebHelper,
            ILateFeeService ILateFeeService,
            ISchoolSettingService ISchoolSettingService,
            ICatalogMasterService ICatalogMasterService,
            IStudentService IStudentService,
            IStudentAddOnService IStudentAddOnService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption,
            IGuardianService IGuardianService,
            IAddressMasterService IAddressMasterService,
            IContactService IContactService,
            IStudentMasterService IStudentMasterService,
            IBusService IBusService,
            ITransportService ITransportService,
            IObjectBrowserInfo IObjectBrowserInfo)
        {
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._Logger = Logger;
            this._WebHelper = WebHelper;
            this._ISchoolSettingService = ISchoolSettingService;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IStudentService = IStudentService;
            this._IStudentAddOnService = IStudentAddOnService;
            this._ILateFeeService = ILateFeeService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._IGuardianService = IGuardianService;
            this._IAddressMasterService = IAddressMasterService;
            this._IContactService = IContactService;
            this._IStudentMasterService = IStudentMasterService;
            this._IBusService = IBusService;
            this._ITransportService = ITransportService;
            this._ISchoolDataService = ISchoolDataService;
            this._IObjectBrowserInfo = IObjectBrowserInfo;
        }
        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }

        #endregion

        // GET: InfoBrowser
        public ActionResult InfoBrowser()
        {

            //_IUserService.ValidateUserWithLoginCheck("Reena Kapoor", "0JiUfWwZBUod3+ke3dw7+KI7UNJ7wvPM");
            BrowserInfoDetail model = new BrowserInfoDetail();
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;


                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Fee ReceiptList", "View");
                    //if (!isauth)
                    //return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }
                List<Browser_Object> Browserobjlist = new List<Browser_Object>();
                Browserobjlist = GetBrowserOjectList();
                model.BrowserObjectList.Add(new SelectListItem { Selected = false, Value = "", Text = "--select--" });
                foreach (var item in Browserobjlist)
                {
                    model.BrowserObjectList.Add(new SelectListItem { Selected = false, Value = item.BrowserObjectId.ToString(), Text = item.BrowserObject });
                }
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }

            return View(model);
        }
        public ActionResult LoadDataacctoObjectType(string ObjectTypeId, string buttonName)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                DataTable dt = new DataTable();
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;


                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    // var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Fee ReceiptList", "View");
                    //if (!isauth)
                    // return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }
                int ObjectId = 0;
                if (!string.IsNullOrEmpty(ObjectTypeId))
                    ObjectId = Convert.ToInt32(ObjectTypeId);
                List<Browser_Object> Browserobjlist = new List<Browser_Object>();
                Browserobjlist = GetBrowserOjectList();
                Browserobjlist = Browserobjlist.Where(f => f.BrowserObjectId == ObjectId).ToList();
                int filtercount = 0;
                int ViewCount = 0;
                int editcount = 0;
                List<FilterList> FilteredList = new List<FilterList>();
                string StoreprocedureName = "";
                foreach (var item in Browserobjlist)
                {
                    StoreprocedureName = item.StoredProcedureName;
                    if (item.MaxFilterColCount != null)
                        filtercount = (int)item.MaxFilterColCount;
                    if (item.MaxViewColCount != null)
                        ViewCount = (int)item.MaxViewColCount;
                    if (item.MaxColCount != null)
                        editcount = (int)(item.MaxColCount - item.MaxViewColCount);
                    foreach (var itemcol in item.BrowserObjectCol)
                    {

                        if (itemcol.CanFilter == true)
                        {

                            foreach (var itemcontrol in itemcol.BrowserObjectColRef)
                            {
                                FilterList filter = new FilterList();
                                filter.FilterName = itemcol.BrowserObjectCol;
                                filter.FilterType = itemcontrol.ControlType;
                                if (itemcontrol.ControlType == "DropDown")
                                {
                                    filter.FilterData = _IObjectBrowserInfo.GetdataFromMastertables(itemcontrol.RefTable,itemcontrol.RefTableCol);
                                }
                                FilteredList.Add(filter);
                            }
                        }
                    }
                }
                var currentsession = _ICatalogMasterService.GetCurrentSession();
                dt = _IObjectBrowserInfo.GetUserDatafromStoreProcedure(StoreprocedureName:StoreprocedureName,SessionId:Convert.ToString(currentsession.SessionId));
                string ColsName = "";
                string RowsList = "";
                if (dt != null)
                {
                    int colcount = dt.Columns.Count;
                    foreach (var item in dt.Columns)
                    {
                        ColsName += item.ToString() + "ε";
                    }

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        for (int j = 0; j < colcount; j++)
                        {
                            RowsList += dt.Rows[i][j] + ",";
                        }
                        RowsList += "ε";
                    }
                }
                return Json(new
                {
                    status = "success",
                    FilterCount = filtercount,
                    ViewColCount = ViewCount,
                    EditCount = editcount,
                    FilterList = FilteredList,
                    RowsList = RowsList.ToString().TrimEnd('ε'),
                    ColumnNames = ColsName.ToString().TrimEnd('ε')
                }); 
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }


        public List<Browser_Object> GetBrowserOjectList()
        {
            List<Browser_Object> Browserobjlist = new List<Browser_Object>();
            string fileName = Convert.ToString(WebConfigurationManager.AppSettings["BrowserObjjson"]);
            var dynamicnavigationpath = "";
            var Is_Lite = _ISchoolSettingService.GetorSetSchoolData("Is_Lite", "0").AttributeValue == "1" ? true : false;
            if (Is_Lite)
                dynamicnavigationpath = "litenavigationpath";
            else
                dynamicnavigationpath = "navigationpath";
            string path = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + fileName.ToString());
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                Browserobjlist = JsonConvert.DeserializeObject<List<Browser_Object>>(json);
            }
            return Browserobjlist;
        }

        public ActionResult GetColumnListForSelection(string ObjectTypeId, string buttonName)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;


                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                    // check authorization
                    // var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Fee ReceiptList", "View");
                    // if (!isauth)
                    // return RedirectToAction("AccessDenied", "Error");
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }
                int ObjectId = 0;
                if (!string.IsNullOrEmpty(ObjectTypeId))
                    ObjectId = Convert.ToInt32(ObjectTypeId);
                List<Browser_Object> Browserobjlist = new List<Browser_Object>();
                Browserobjlist = GetBrowserOjectList();
                Browserobjlist = Browserobjlist.Where(f => f.BrowserObjectId == ObjectId).ToList();
                int filtercount = 0;
                int ViewCount = 0;
                int editcount = 0;
                List<ColumnList> colmnlist = new List<ColumnList>();
                foreach (var item in Browserobjlist)
                {
                    if (item.MaxViewColCount != null)
                        ViewCount = (int)item.MaxViewColCount;
                    if (item.MaxColCount != null)
                        editcount = (int)(item.MaxColCount - item.MaxViewColCount);
                    foreach (var itemcol in item.BrowserObjectCol)
                    {
                        if (buttonName == "View")
                        {
                            if (itemcol.CanView == true)
                            {
                                colmnlist.Add(new ColumnList { ColumnId = itemcol.BrowserObjectColId, ColumnName = itemcol.BrowserObjectCol });
                            }
                        }
                        else if (buttonName == "Edit")
                        {
                            if (itemcol.CanEdit == true)
                            {
                                colmnlist.Add(new ColumnList { ColumnId = itemcol.BrowserObjectColId, ColumnName = itemcol.BrowserObjectCol });
                            }
                        }
                    }
                }

                return Json(new
                {
                    status = "success",
                    ColumnList = colmnlist
                });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }

        }

        public ActionResult GetObjectColumnList(string ObjectTypeId)
        {
            SchoolUser user = new SchoolUser();
            try
            {
                // current user
                if (HttpContext.Session["SchoolDB"] != null)
                {
                    // re-assign session
                    var sessiondb = HttpContext.Session["SchoolDB"];
                    HttpContext.Session["SchoolDB"] = sessiondb;
                    user = System.Web.HttpContext.Current.User.Identity.Name != "" ? _IUserService.GetUserByEmail(System.Web.HttpContext.Current.User.Identity.Name) : null;
                    if (user == null)
                        return RedirectToAction("Login", "Home");
                    SetSessionData(System.Web.HttpContext.Current.User.Identity.Name);
                }
                else
                {
                    // get action name
                    var UserRequest = _WebHelper.GetControllerandActionNameFromUrl(HttpContext.Request.Url.ToString());
                    return RedirectToAction("DummyAction", "Common", new { ctrl = UserRequest.ContollerName, act = UserRequest.ActionName });
                }
                int ObjectId = 0;
                if (!string.IsNullOrEmpty(ObjectTypeId))
                    ObjectId = Convert.ToInt32(ObjectTypeId);
                List<Browser_Object> Browserobjlist = new List<Browser_Object>();
                Browserobjlist = GetBrowserOjectList();
                Browserobjlist = Browserobjlist.Where(f => f.BrowserObjectId == ObjectId).ToList();
                List<ColumnList> collist = new List<ColumnList>();
                foreach (var item in Browserobjlist)
                {
                    foreach (var itemcol in item.BrowserObjectCol)
                    {
                        if (itemcol.CanView == true)
                        {
                            collist.Add(new ColumnList { ColumnId = itemcol.BrowserObjectColId, ColumnName = itemcol.BrowserObjectCol });
                        }
                        else if (itemcol.CanEdit == true)
                        {
                            collist.Add(new ColumnList { ColumnId = itemcol.BrowserObjectColId, ColumnName = itemcol.BrowserObjectCol });
                        }
                        else if(itemcol.CanFilter==true)
                        {
                            collist.Add(new ColumnList { ColumnId = itemcol.BrowserObjectColId, ColumnName = itemcol.BrowserObjectCol });
                        }

                    }
                }
                return Json(new
                    {
                        status = "success",

                    });
            }
            catch (Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, user);
                return RedirectToAction("General", "Error");
            }
        }



    }
}