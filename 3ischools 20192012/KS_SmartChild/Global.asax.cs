﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using KS_SmartChild.App_Start;
using KS_SmartChild.Controllers;
using DAL.DAL.Task;
//using Mechanism;
using Quartz.Spi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Management;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace KS_SmartChild
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static IWindsorContainer Container { get; private set; }

        protected void Application_Start()
        {


            //Remove All View Engine including Webform and Razor
            ViewEngines.Engines.Clear();
            //Register Razor View Engine
            ViewEngines.Engines.Add(new CSharpRazorViewEngine());
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            UnityWebActivator.Start(); // dependency injection
            // execute in ths project (asembly)
            //JobManager jobManager = new JobManager();
            //jobManager.ExecuteAllJobs();
            //Scheduler(new WindsorContainer());
            //Container.Resolve<IQuartzInitializer>().Start();
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            try
            {
                var domain = HttpContext.Current.Request.Url.AbsolutePath.Split('/')[1];
                domain = "_" + domain;
                if (Request.Url.AbsoluteUri.Contains("localhost"))
                    domain = "_localhost";

                var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName + domain];
                if (authCookie != null)
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    if (authTicket != null && !authTicket.Expired)
                    {
                        var roles = authTicket.UserData.Split(',');
                        HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(new FormsIdentity(authTicket), roles);
                        if (HttpContext.Current.User.Identity.Name == "")
                        {
                            FormsAuthentication.SignOut();
                            FormsAuthentication.RedirectToLoginPage();
                            return;
                        }
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        FormsAuthentication.RedirectToLoginPage();
                        return;
                    }
                }
            }
            catch (CryptographicException cex)
            {
                FormsAuthentication.SignOut();
            }
        }

        // handle GC
        //protected void Application_BeginRequest(object sender, EventArgs e)
        //{
        //    var shouldForceGarbageCollection = Request.QueryString["ForceGC"] != null;
        //    if (shouldForceGarbageCollection)
        //        ForceGarbageCollection();
        //}

        //void ForceGarbageCollection()
        //{
        //    /// This will garbage collect all generations (including large object), 
        //    GC.Collect(3);
        //    /// then execute any finalizers
        //    GC.WaitForPendingFinalizers();
        //    /// and then garbage collect again to take care of all the objects that had finalizers.            
        //    GC.Collect(3);
        //}

        protected void Application_Error(Object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();    

            // Log the exception.
            //ILogger logger = Container.Resolve<ILogger>();
            //logger.Error(exception);
            Response.Clear();

            HttpException httpException = exception as HttpException ?? exception.InnerException as HttpException;

            RouteData routeData = new RouteData();
            routeData.Values.Add("controller", "Error");

            if (httpException == null)
                routeData.Values.Add("action", "General");
            if (httpException.WebEventCode == WebEventCodes.RuntimeErrorPostTooLarge)
                Response.Write("Filesize too large"); //handle the error
            else //It's an Http Exception, Let's handle it.
            {
                switch (httpException.GetHttpCode())
                {
                    case 404:
                        // Page not found.
                        routeData.Values.Add("action", "HttpError404");
                        break;
                    case 500:
                        // Server error.
                        routeData.Values.Add("action", "HttpError500");
                        break;

                    // Here you can handle Views to other error codes.
                    // I choose a General error template  
                    default:
                        routeData.Values.Add("action", "General");
                        break;
                }
            }

            // Pass exception details to the target error View.
            routeData.Values.Add("error", exception);

            // Clear the error on server.
            Server.ClearError();

            // Avoid IIS7 getting in the middle
            Response.TrySkipIisCustomErrors = true;

            // Call target Controller and pass the routeData.
            IController errorController = new ErrorController();
            errorController.Execute(new RequestContext(
                 new HttpContextWrapper(Context), routeData));
        }

        protected void Application_EndRequest()
         {
            if (Context.Response.StatusCode == 404)
            {
                Response.Clear();

                var routeData = new RouteData();
                HttpContextBase currentContext = new HttpContextWrapper(HttpContext.Current);
                routeData.Values["controller"] = "Error";
                routeData.Values["action"] = "HttpError404";

                IController customErrorController = new ErrorController();
                customErrorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
            }
        }

        public static void Scheduler(IWindsorContainer container)
        {
            Container = container;
            IJobFactory jobFactory = new WindsorJobFactory(Container);

            Container.Register(Component.For<IJobFactory>().Instance(jobFactory).LifeStyle.Transient);
            Container.Register(Component.For<IQuartzInitializer>().ImplementedBy<QuartzInitializer>().LifeStyle.Transient);

            JobRegistrar jobRegistrar = new JobRegistrar(Container);
            jobRegistrar.RegisterJobs();
        }

    }
}
