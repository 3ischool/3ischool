﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace KS_SmartChild.BAL
{
    public class MainService : IMainService
    {
        public string OneTimePasswordGenerate(ref string OTP, int? OTPLength = null)
        {
            int lenthofpass = 6;
            if (OTPLength > 0)
                lenthofpass = (int)OTPLength;

            string OTPString = "";
            Random rand = new Random();
            for (int i = 0; i < lenthofpass; i++)
            {
                int randotp = rand.Next(0, 9);
                //check to avoid starting of otp with 0
                if (i == 0 && randotp == 0)
                    randotp = rand.Next(1, 9);

                OTPString += randotp;

            }

            OTP = OTPString;

            MD5CryptoServiceProvider md5hasher = new MD5CryptoServiceProvider();
            Byte[] hashedbytes;
            UTF8Encoding encoder = new UTF8Encoding();
            hashedbytes = md5hasher.ComputeHash(encoder.GetBytes(OTPString));
            StringBuilder asdfotphjkl = new StringBuilder();
            foreach (var h in hashedbytes)
                asdfotphjkl.Append(h.ToString("x2"));

            OTPString = asdfotphjkl.ToString();
            return OTPString;
        }

        public IList<DateTime> GetSundayCountBetweenDates(ref int Count, DateTime StartDate, DateTime EndDate)
        {
            IList<DateTime> SundayList = new List<DateTime>();

            TimeSpan diff = EndDate - StartDate;
            int days = diff.Days;
            for (var i = 0; i <= days; i++)
            {
                var testDate = StartDate.AddDays(i);
                switch (testDate.DayOfWeek)
                {
                    case DayOfWeek.Sunday:
                        Count++;
                        SundayList.Add(testDate);
                        break;
                }
            }

            return SundayList;
        }

        public IList<DateTime> GetMonthCountBetweenDates(ref int Count, DateTime StartDate, DateTime EndDate)
        {
            IList<DateTime> MonthList = new List<DateTime>();
            int months = (EndDate.Year - StartDate.Year) * 12 + EndDate.Month - StartDate.Month;
            for (var i = 0; i <= months; i++)
            {
                Count++;

                var testmonth = StartDate.AddMonths(i);

                MonthList.Add(testmonth);

            }

            return MonthList;
        }

    }
}