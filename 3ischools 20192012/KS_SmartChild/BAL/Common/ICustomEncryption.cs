﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS_SmartChild.BAL.Common
{
    public interface ICustomEncryption
    {
        string base64e(string pure_string);

        string base64d(string encrypted_string);

        string Encrypt(string plainText);

        string Decrypt(string cipherText);

        string Encrypt_new(string plainText);

        string Decrypt_new(string cipherText);
        
    }
}
