﻿using ClosedXML.Excel;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using iTextSharp.tool.xml.html.table;
using KS_SmartChild.DAL.ErrorLogModule;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;

namespace KS_SmartChild.BAL.Common
{
    public class ExportHelper : IExportHelper
    {
        private readonly ILogService _Logger;

        public ExportHelper(ILogService LogService)
        {
            this._Logger = LogService;
        }

        public void ExportToPDF(System.Data.DataTable dataTable = null, string Title = "", string Subtitle = "",
                    string GroupingBy = "", string subTotal = "", string GrandTotal = "", string html = "")
        {
            try
            {
                using (System.IO.MemoryStream mStream = new System.IO.MemoryStream())
                {
                    if (dataTable != null)
                    {
                        Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 140f, 10f);
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, mStream);
                        iTextSharp.text.Font font5 = iTextSharp.text.FontFactory.GetFont(FontFactory.COURIER, 8);
                        pdfDoc.Open();

                        int cols = dataTable.Columns.Count;
                        int rows = dataTable.Rows.Count;

                        PdfPTable Pdftable = new PdfPTable(dataTable.Columns.Count);
                        PdfPCell cell = new PdfPCell();
                        Pdftable.WidthPercentage = 100;
                        cell.Colspan = dataTable.Columns.Count;

                        iTextSharp.text.Font RowFont = FontFactory.GetFont(FontFactory.HELVETICA, 12);

                        //Header
                        Paragraph para = new Paragraph();
                        para.Alignment = Element.ALIGN_MIDDLE;
                        
                        if (!string.IsNullOrEmpty(Title))
                            para.Add(new Chunk(string.Format("Title: List of {0}\n", Title, font5)));

                        if (!string.IsNullOrEmpty(Subtitle))
                            para.Add(new Chunk(string.Format("SubTitle: {0}\n", Subtitle, font5)));
                        
                        pdfDoc.Add(para);
                        pdfDoc.Add(new Paragraph("\n\n"));


                        if (Title == "Packages")
                        {
                            var Counter = 0;
                            ////creating table headers 
                            for (int k = 0; k < rows; k++)
                            {
                                //creating table headers 
                                for (int i = 0; i < cols; i++)
                                {
                                    if (dataTable.Rows[k][0].ToString() != "")
                                    {
                                        if (i < 2)
                                        {
                                            Counter = 0;
                                            cell = new PdfPCell();
                                            Chunk chunkCols = new Chunk();
                                            cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#eceeef"));
                                            iTextSharp.text.Font ColFont = FontFactory.GetFont(FontFactory.HELVETICA, 14, iTextSharp.text.Font.BOLD,
                                                                                                iTextSharp.text.BaseColor.BLACK);

                                            chunkCols = new Chunk(dataTable.Columns[i].ColumnName, ColFont);
                                            cell.AddElement(chunkCols);
                                            Pdftable.AddCell(cell);


                                            cell = new PdfPCell();
                                            cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#ffffff")); ;
                                            Chunk chunkRows = new Chunk(dataTable.Rows[k][i].ToString(), RowFont);
                                            cell.AddElement(chunkRows);
                                            Pdftable.AddCell(cell);

                                            Pdftable.AddCell("");
                                            Pdftable.AddCell("");
                                        }
                                    }

                                    //for three rows  headers only of excel pattern  
                                    if (dataTable.Rows[k][2].ToString().ToString() != "" && Counter == 0)
                                    {
                                        //Counter = 1;
                                        if (i == 0)
                                        {
                                            cell = new PdfPCell();
                                            Chunk chunkCols = new Chunk();
                                            cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#eceeef"));
                                            iTextSharp.text.Font ColFont = FontFactory.GetFont(FontFactory.HELVETICA, 14, iTextSharp.text.Font.BOLD,
                                                                                                iTextSharp.text.BaseColor.BLACK);

                                            chunkCols = new Chunk("List Of Products", ColFont);
                                            cell.AddElement(chunkCols);
                                            Pdftable.AddCell(cell);
                                        }
                                        if (i >= 2)
                                        {
                                            cell = new PdfPCell();
                                            Chunk chunkCols = new Chunk();
                                            cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#eceeef"));
                                            iTextSharp.text.Font ColFont = FontFactory.GetFont(FontFactory.HELVETICA, 14, iTextSharp.text.Font.BOLD,
                                                                                                iTextSharp.text.BaseColor.BLACK);

                                            chunkCols = new Chunk(dataTable.Columns[i].ColumnName, ColFont);
                                            cell.AddElement(chunkCols);
                                            Pdftable.AddCell(cell);
                                        }
                                    }
                                }

                                //for fourth rows  data of products of excel pattern  
                                for (int j = 0; j < dataTable.Columns.Count; j++)
                                {
                                    if (dataTable.Rows[k][2].ToString() != "")
                                    {
                                        Counter = 1;
                                        if (j == 0)
                                        {
                                            Pdftable.AddCell("");
                                            Pdftable.AddCell("");
                                        }
                                        if (j >= 2)
                                        {
                                            cell = new PdfPCell();
                                            cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#ffffff")); ;
                                            Chunk chunkRows = new Chunk(dataTable.Rows[k][j].ToString(), RowFont);
                                            cell.AddElement(chunkRows);
                                            Pdftable.AddCell(cell);

                                            if (j == 4)
                                            {
                                                Pdftable.AddCell("");
                                                Pdftable.AddCell("");
                                            }
                                        }
                                    }
                                }
                                //for empty/merged row per package detail
                                if (dataTable.Rows[k][2].ToString() == "" && dataTable.Rows[k][0].ToString() == "")
                                {
                                    Pdftable.AddCell("");
                                    Pdftable.AddCell("");
                                    Pdftable.AddCell("");
                                    Pdftable.AddCell("");
                                    Pdftable.AddCell("");
                                }
                            }
                        }
                        else
                        {
                            ////creating table headers 
                            for (int i = 0; i < cols; i++)
                            {
                                cell = new PdfPCell();
                                Chunk chunkCols = new Chunk();
                                cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#eceeef"));
                                iTextSharp.text.Font ColFont = FontFactory.GetFont(FontFactory.HELVETICA, 14, iTextSharp.text.Font.BOLD,
                                                                                    iTextSharp.text.BaseColor.BLACK);

                                chunkCols = new Chunk(dataTable.Columns[i].ColumnName, ColFont);

                                cell.AddElement(chunkCols);
                                Pdftable.AddCell(cell);
                            }

                            ////creating table data (actual result)   
                            for (int k = 0; k < rows; k++)
                            {
                                for (int j = 0; j < cols; j++)
                                {
                                    cell = new PdfPCell();
                                    //if (k % 2 == 0)
                                    //{
                                    cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#ffffff")); ;
                                    //}
                                    //else { cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#ffffff")); }

                                    Chunk chunkRows = new Chunk(dataTable.Rows[k][j].ToString(), RowFont);
                                    cell.AddElement(chunkRows);

                                    Pdftable.AddCell(cell);
                                }
                            }
                        }

                        pdfDoc.Add(Pdftable);

                        //empty space
                        pdfDoc.Add(new Paragraph("\n\n"));

                        //Footer
                        Paragraph parafooter = new Paragraph();
                        parafooter.Alignment = Element.ALIGN_RIGHT;
                        parafooter.Add(new Chunk(string.Format("Date: {0}\n", DateTime.Now.ToString("dd/MM/yyy")), RowFont));
                        pdfDoc.Add(parafooter);
                        pdfDoc.Add(new Paragraph("\n\n"));
                        pdfDoc.Close();
                    }
                    else
                    {
                        //for student admission/Registration
                        StringReader srHtml = new StringReader(html);
                        Document pdfDocHtml = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                        PdfWriter writerHtml = PdfWriter.GetInstance(pdfDocHtml, mStream);
                        pdfDocHtml.Open();
                        pdfDocHtml.HtmlStyleClass = System.Windows.Forms.BorderStyle.FixedSingle.ToString();
                        XMLWorkerHelper.GetInstance().ParseXHtml(writerHtml, pdfDocHtml, srHtml);
                        pdfDocHtml.Close();
                    }
                    byte[] content = mStream.ToArray();
                    HttpContext.Current.Response.Clear();           // Already have this
                    HttpContext.Current.Response.ClearContent();    // Add this line
                    HttpContext.Current.Response.ClearHeaders();
                    HttpContext.Current.Response.ContentType = "application/pdf";
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename="
                                                            + Title + "_" + DateTime.Now.ToString() + ".pdf");
                    HttpContext.Current.Response.BinaryWrite(content);
                    HttpContext.Current.Response.End();
                }
            }
            catch (Exception ex)
            {

            }
        }

        //public void ExportToPDFtoserver(System.Data.DataTable dataTable = null, string Title = "", string Subtitle = "",
        //          string GroupingBy = "", string subTotal = "", string GrandTotal = "", string html = "")
        //{
        //    try
        //    {
        //        using (System.IO.MemoryStream mStream = new System.IO.MemoryStream())
        //        {
        //            if (dataTable != null)
        //            {
        //                Document pdfDoc = new Document(PageSize.A2, 10f, 10f, 140f, 10f);
        //                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, mStream);
        //                iTextSharp.text.Font font5 = iTextSharp.text.FontFactory.GetFont(FontFactory.COURIER, 8);
        //                pdfDoc.Open();

        //                int cols = dataTable.Columns.Count;
        //                int rows = dataTable.Rows.Count;

        //                PdfPTable Pdftable = new PdfPTable(dataTable.Columns.Count);
        //                PdfPCell cell = new PdfPCell();
        //                Pdftable.WidthPercentage = 100;
        //                cell.Colspan = dataTable.Columns.Count;

        //                iTextSharp.text.Font RowFont = FontFactory.GetFont(FontFactory.HELVETICA, 12);

        //                //Header
        //                Paragraph para = new Paragraph();
        //                para.Alignment = Element.ALIGN_MIDDLE;

        //                if (!string.IsNullOrEmpty(Title))
        //                    para.Add(new Chunk(string.Format("Title: List of {0}\n", Title, font5)));

        //                if (!string.IsNullOrEmpty(Subtitle))
        //                    para.Add(new Chunk(string.Format("SubTitle: {0}\n", Subtitle, font5)));

        //                pdfDoc.Add(para);
        //                pdfDoc.Add(new Paragraph("\n\n"));


        //                if (Title == "Packages")
        //                {
        //                    var Counter = 0;
        //                    ////creating table headers 
        //                    for (int k = 0; k < rows; k++)
        //                    {
        //                        //creating table headers 
        //                        for (int i = 0; i < cols; i++)
        //                        {
        //                            if (dataTable.Rows[k][0].ToString() != "")
        //                            {
        //                                if (i < 2)
        //                                {
        //                                    Counter = 0;
        //                                    cell = new PdfPCell();
        //                                    Chunk chunkCols = new Chunk();
        //                                    cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#eceeef"));
        //                                    iTextSharp.text.Font ColFont = FontFactory.GetFont(FontFactory.HELVETICA, 14, iTextSharp.text.Font.BOLD,
        //                                                                                        iTextSharp.text.BaseColor.BLACK);

        //                                    chunkCols = new Chunk(dataTable.Columns[i].ColumnName, ColFont);
        //                                    cell.AddElement(chunkCols);
        //                                    Pdftable.AddCell(cell);


        //                                    cell = new PdfPCell();
        //                                    cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#ffffff")); ;
        //                                    Chunk chunkRows = new Chunk(dataTable.Rows[k][i].ToString(), RowFont);
        //                                    cell.AddElement(chunkRows);
        //                                    Pdftable.AddCell(cell);

        //                                    Pdftable.AddCell("");
        //                                    Pdftable.AddCell("");
        //                                }
        //                            }

        //                            //for three rows  headers only of excel pattern  
        //                            if (dataTable.Rows[k][2].ToString().ToString() != "" && Counter == 0)
        //                            {
        //                                //Counter = 1;
        //                                if (i == 0)
        //                                {
        //                                    cell = new PdfPCell();
        //                                    Chunk chunkCols = new Chunk();
        //                                    cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#eceeef"));
        //                                    iTextSharp.text.Font ColFont = FontFactory.GetFont(FontFactory.HELVETICA, 14, iTextSharp.text.Font.BOLD,
        //                                                                                        iTextSharp.text.BaseColor.BLACK);

        //                                    chunkCols = new Chunk("List Of Products", ColFont);
        //                                    cell.AddElement(chunkCols);
        //                                    Pdftable.AddCell(cell);
        //                                }
        //                                if (i >= 2)
        //                                {
        //                                    cell = new PdfPCell();
        //                                    Chunk chunkCols = new Chunk();
        //                                    cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#eceeef"));
        //                                    iTextSharp.text.Font ColFont = FontFactory.GetFont(FontFactory.HELVETICA, 14, iTextSharp.text.Font.BOLD,
        //                                                                                        iTextSharp.text.BaseColor.BLACK);

        //                                    chunkCols = new Chunk(dataTable.Columns[i].ColumnName, ColFont);
        //                                    cell.AddElement(chunkCols);
        //                                    Pdftable.AddCell(cell);
        //                                }
        //                            }
        //                        }

        //                        //for fourth rows  data of products of excel pattern  
        //                        for (int j = 0; j < dataTable.Columns.Count; j++)
        //                        {
        //                            if (dataTable.Rows[k][2].ToString() != "")
        //                            {
        //                                Counter = 1;
        //                                if (j == 0)
        //                                {
        //                                    Pdftable.AddCell("");
        //                                    Pdftable.AddCell("");
        //                                }
        //                                if (j >= 2)
        //                                {
        //                                    cell = new PdfPCell();
        //                                    cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#ffffff")); ;
        //                                    Chunk chunkRows = new Chunk(dataTable.Rows[k][j].ToString(), RowFont);
        //                                    cell.AddElement(chunkRows);
        //                                    Pdftable.AddCell(cell);

        //                                    if (j == 4)
        //                                    {
        //                                        Pdftable.AddCell("");
        //                                        Pdftable.AddCell("");
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        //for empty/merged row per package detail
        //                        if (dataTable.Rows[k][2].ToString() == "" && dataTable.Rows[k][0].ToString() == "")
        //                        {
        //                            Pdftable.AddCell("");
        //                            Pdftable.AddCell("");
        //                            Pdftable.AddCell("");
        //                            Pdftable.AddCell("");
        //                            Pdftable.AddCell("");
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    ////creating table headers 
        //                    for (int i = 0; i < cols; i++)
        //                    {
        //                        cell = new PdfPCell();
        //                        Chunk chunkCols = new Chunk();
        //                        cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#eceeef"));
        //                        iTextSharp.text.Font ColFont = FontFactory.GetFont(FontFactory.HELVETICA, 14, iTextSharp.text.Font.BOLD,
        //                                                                            iTextSharp.text.BaseColor.BLACK);

        //                        chunkCols = new Chunk(dataTable.Columns[i].ColumnName, ColFont);

        //                        cell.AddElement(chunkCols);
        //                        Pdftable.AddCell(cell);
        //                    }

        //                    ////creating table data (actual result)   
        //                    for (int k = 0; k < rows; k++)
        //                    {
        //                        for (int j = 0; j < cols; j++)
        //                        {
        //                            cell = new PdfPCell();
        //                            //if (k % 2 == 0)
        //                            //{
        //                            cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#ffffff")); ;
        //                            //}
        //                            //else { cell.BackgroundColor = new iTextSharp.text.BaseColor(System.Drawing.ColorTranslator.FromHtml("#ffffff")); }

        //                            Chunk chunkRows = new Chunk(dataTable.Rows[k][j].ToString(), RowFont);
        //                            cell.AddElement(chunkRows);

        //                            Pdftable.AddCell(cell);
        //                        }
        //                    }
        //                }

        //                pdfDoc.Add(Pdftable);

        //                //empty space
        //                pdfDoc.Add(new Paragraph("\n\n"));

        //                //Footer
        //                Paragraph parafooter = new Paragraph();
        //                parafooter.Alignment = Element.ALIGN_RIGHT;
        //                parafooter.Add(new Chunk(string.Format("Date: {0}\n", DateTime.Now.ToString("dd/MM/yyy")), RowFont));
        //                pdfDoc.Add(parafooter);
        //                pdfDoc.Add(new Paragraph("\n\n"));
        //                pdfDoc.Close();
        //            }
        //            else
        //            {
        //                //for student admission/Registration
        //                StringReader srHtml = new StringReader(html);
        //                Document pdfDocHtml = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
        //                PdfWriter writerHtml = PdfWriter.GetInstance(pdfDocHtml, mStream);
        //                pdfDocHtml.Open();
        //                pdfDocHtml.HtmlStyleClass = System.Windows.Forms.BorderStyle.FixedSingle.ToString();
        //                XMLWorkerHelper.GetInstance().ParseXHtml(writerHtml, pdfDocHtml, srHtml);
        //                pdfDocHtml.Close();
        //            }
                  
        //            byte[] content = mStream.ToArray();
        //            HttpContext.Current.Response.Clear();           // Already have this
        //            HttpContext.Current.Response.ClearContent();    // Add this line
        //            HttpContext.Current.Response.ClearHeaders();
        //            HttpContext.Current.Response.ContentType = "application/pdf";
        //            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename="
        //                                                    + Title + "_" + DateTime.Now.ToString() + ".pdf");
        //            HttpContext.Current.Response.BinaryWrite(content);
        //            HttpContext.Current.Response.End();

        //            String path_name = "~/PDF/";
        //            var pdfPath = Path.Combine(Server.MapPath(path_name));
        //            var formFieldMap = PDFHelper.GetFormFieldNames(pdfPath);

        //            string username = "Test";
        //            string password = "12345";
        //            String file_name_pdf = "Test.pdf";

        //            var pdfContents = PDFHelper.GeneratePDF(pdfPath, formFieldMap);

        //            File.WriteAllBytes(Path.Combine(pdfPath, file_name_pdf), pdfContents);

        //            WebRequest request = WebRequest.Create(Server.MapPath("~/PDF/" + pdfContents));
        //            request.Method = WebRequestMethods.Ftp.UploadFile;

        //            request.Credentials = new NetworkCredential(username, password);
        //            Stream reqStream = request.GetRequestStream();
        //            reqStream.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
        public void ExportToExcelForLibrary(System.Data.DataTable dt = null, string Title = "", string Subtitle = "",
                    string GroupingBy = "", string SubTotal = "", string GrandTotal = "", string html = "", string SheetName = "", string HeadingName = "")
        {
            try
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearContent();
                System.Web.HttpContext.Current.Response.ClearHeaders();
                System.Web.HttpContext.Current.Response.Buffer = true;
                System.Web.HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
                System.Web.HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                System.Web.HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + Title + "_"
                                                                            + DateTime.Now.ToString("dd/MM/yyy") + ".xls");
                //HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                //HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + Title
                //                                        + "_" + DateTime.Now.ToString() + ".xls");
                //HttpContext.Current.Response.AddHeader("Content-Type", "application/vnd.ms-excel");

                if (dt != null)
                {
                    //string headerTable = @"<Table><tr><td>Report Header</td></tr><tr></tr></Table>";
                    //HttpContext.Current.Response.Write(headerTable);

                    string str = string.Empty;
                    string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                    HttpContext.Current.Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");

                    HttpContext.Current.Response.Write("<head>");
                    //HttpContext.Current.Response.Write("<meta http-equiv='Content-Type' content='text/html;charset=windows-1252'>");
                    HttpContext.Current.Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");

                    HttpContext.Current.Response.Write("<!--[if gte mso 9]>");
                    HttpContext.Current.Response.Write("<xml>");
                    HttpContext.Current.Response.Write("<x:ExcelWorkbook>");
                    HttpContext.Current.Response.Write("<x:ExcelWorksheets>");
                    HttpContext.Current.Response.Write("<x:ExcelWorksheet>");
                    //this line names the worksheet
                    if (!string.IsNullOrEmpty(SheetName))
                        HttpContext.Current.Response.Write("<x:Name>" + SheetName + "</x:Name>");
                    else
                        HttpContext.Current.Response.Write("<x:Name>gridlineTest</x:Name>");

                    HttpContext.Current.Response.Write("<x:WorksheetOptions>");
                    //these 2 lines are what works the magic
                    HttpContext.Current.Response.Write("<x:Panes>");
                    HttpContext.Current.Response.Write("</x:Panes>");
                    HttpContext.Current.Response.Write("</x:WorksheetOptions>");
                    HttpContext.Current.Response.Write("</x:ExcelWorksheet>");
                    HttpContext.Current.Response.Write("</x:ExcelWorksheets>");
                    HttpContext.Current.Response.Write("</x:ExcelWorkbook>");
                    HttpContext.Current.Response.Write("</xml>");
                    HttpContext.Current.Response.Write("<![endif]-->");
                    HttpContext.Current.Response.Write("</head>");
                    HttpContext.Current.Response.Write(style);
                    HttpContext.Current.Response.Write("<body>");

                    if (Title == "Packages")
                    {
                        var Counter = 0;

                        // data of table
                        foreach (DataRow dr in dt.Rows)
                        {
                            HttpContext.Current.Response.Write("<Table border='1' >");
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                //for firt two rows of excel pattern 
                                if (dr[0].ToString() != "")
                                {
                                    if (j < 2)
                                    {
                                        Counter = 0;
                                        HttpContext.Current.Response.Write("<TR>");
                                        HttpContext.Current.Response.Write("<TD><B>");
                                        HttpContext.Current.Response.Write(dt.Columns[j].ColumnName);
                                        HttpContext.Current.Response.Write("</B></TD>");
                                        HttpContext.Current.Response.Write("<TD>");
                                        HttpContext.Current.Response.Write(dr[j].ToString());
                                        HttpContext.Current.Response.Write("</TD>");
                                        HttpContext.Current.Response.Write("<TD>");
                                        HttpContext.Current.Response.Write("</TD>");
                                        HttpContext.Current.Response.Write("</TR>");
                                    }
                                }

                                //for three rows  headers only of excel pattern  
                                if (dr[2].ToString() != "" && Counter == 0)
                                {
                                    //Counter = 1;
                                    if (j == 0)
                                    {
                                        HttpContext.Current.Response.Write("<TR><TD><B>List Of Products </B></TD>");
                                    }
                                    if (j >= 2)
                                    {
                                        HttpContext.Current.Response.Write("<TD><B>");
                                        HttpContext.Current.Response.Write(dt.Columns[j].ColumnName);
                                        HttpContext.Current.Response.Write("</B></TD>");
                                    }

                                }
                            }

                            //for fourth rows  data of products of excel pattern  
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                if (dr[2].ToString() != "")
                                {
                                    Counter = 1;
                                    if (j == 0)
                                    {
                                        HttpContext.Current.Response.Write("<TR><TD></TD>");
                                    }
                                    if (j >= 2)
                                    {
                                        HttpContext.Current.Response.Write("<TD>");
                                        HttpContext.Current.Response.Write(dr[j].ToString());
                                        HttpContext.Current.Response.Write("</TD>");
                                        if (j == 3)
                                        {
                                            HttpContext.Current.Response.Write("</TR>");
                                        }
                                    }
                                }
                            }

                            //for empty/merged row per package detail
                            if (dr[2].ToString() == "" && dr[0].ToString() == "")
                            {
                                HttpContext.Current.Response.Write("<TR><TD colspan='3'></TD></TR>");
                            }
                            HttpContext.Current.Response.Write("</TR>");
                            HttpContext.Current.Response.Write("</Table>");
                        }

                    }

                    else
                    {
                        // headers columns of table
                        HttpContext.Current.Response.Write("<Table border='1' >");

                        if (!string.IsNullOrEmpty(HeadingName))
                        {
                            if (dt.Columns.Count > 0)
                                HttpContext.Current.Response.Write("<TR><TD colspan='" + dt.Columns.Count + "' style='text-align:center;'>" + HeadingName + "</TD></TR>");
                            else
                                HttpContext.Current.Response.Write("<TR><TD colspan='0' style='text-align:center;'>" + HeadingName + "</TD></TR>");
                        }
                        HttpContext.Current.Response.Write("<TR>");
                        foreach (DataColumn dtcol in dt.Columns)
                        {
                            HttpContext.Current.Response.Write("<TD>");
                            HttpContext.Current.Response.Write(@"<style> TD { mso-number-format:\@; } </style>");
                            HttpContext.Current.Response.Write(dtcol.ColumnName);
                            HttpContext.Current.Response.Write("</TD>");
                            ///str = "\t";
                        }
                        HttpContext.Current.Response.Write("</TR>");
                        // HttpContext.Current.Response.Write("\n");

                        // data of table
                        foreach (DataRow dr in dt.Rows)
                        {
                            str = "";
                            HttpContext.Current.Response.Write("<TR>");
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                HttpContext.Current.Response.Write("<TD>");
                                HttpContext.Current.Response.Write(@"<style> TD { mso-number-format:\@; } </style>");
                                HttpContext.Current.Response.Write(Convert.ToString(dr[j]));
                                HttpContext.Current.Response.Write("</TD>");
                                //str = "\t";
                            }
                            HttpContext.Current.Response.Write("</TR>");
                            //HttpContext.Current.Response.Write("\n");
                        }
                        HttpContext.Current.Response.Write("</Table>");
                    }
                    HttpContext.Current.Response.Write("</body>");
                    HttpContext.Current.Response.Write("</html>");
                    //}
                }
                else
                {
                    HttpContext.Current.Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
                    HttpContext.Current.Response.Write("<head>");
                    HttpContext.Current.Response.Write("<meta http-equiv='Content-Type' content='text/html;charset=windows-1252'>");
                    HttpContext.Current.Response.Write("<!--[if gte mso 9]>");
                    HttpContext.Current.Response.Write("<xml>");
                    HttpContext.Current.Response.Write("<x:ExcelWorkbook>");
                    HttpContext.Current.Response.Write("<x:ExcelWorksheets>");
                    HttpContext.Current.Response.Write("<x:ExcelWorksheet>");
                    //this line names the worksheet
                    if (!string.IsNullOrEmpty(SheetName))
                        HttpContext.Current.Response.Write("<x:Name>" + SheetName + "</x:Name>");
                    else
                        HttpContext.Current.Response.Write("<x:Name>gridlineTest</x:Name>");
                    HttpContext.Current.Response.Write("<x:WorksheetOptions>");
                    //these 2 lines are what works the magic
                    HttpContext.Current.Response.Write("<x:Panes>");
                    HttpContext.Current.Response.Write("</x:Panes>");
                    HttpContext.Current.Response.Write("</x:WorksheetOptions>");
                    HttpContext.Current.Response.Write("</x:ExcelWorksheet>");
                    HttpContext.Current.Response.Write("</x:ExcelWorksheets>");
                    HttpContext.Current.Response.Write("</x:ExcelWorkbook>");
                    HttpContext.Current.Response.Write("</xml>");
                    HttpContext.Current.Response.Write("<![endif]-->");
                    HttpContext.Current.Response.Write("</head>");
                    HttpContext.Current.Response.Write("<body>");
                    HttpContext.Current.Response.ContentEncoding = Encoding.UTF8;
                    HttpContext.Current.Response.Output.Write(html);

                    HttpContext.Current.Response.Write("</body>");
                    HttpContext.Current.Response.Write("</html>");
                }
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {

            }

        }
        public void ExportToExcel(System.Data.DataTable dt = null, string Title = "", string Subtitle = "",
                    string GroupingBy = "", string SubTotal = "", string GrandTotal = "", string html = "",string SheetName="",string HeadingName="")
        {
            try
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearContent();
                System.Web.HttpContext.Current.Response.ClearHeaders();
                System.Web.HttpContext.Current.Response.Buffer = true;
                System.Web.HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
                System.Web.HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                System.Web.HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                System.Web.HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename="+Title+ "_" 
                                                                            + DateTime.Now.ToString("dd/MM/yyy") + ".xls");
                //HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                //HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + Title
                //                                        + "_" + DateTime.Now.ToString() + ".xls");
                //HttpContext.Current.Response.AddHeader("Content-Type", "application/vnd.ms-excel");

                if (dt != null)
                {
                    //string headerTable = @"<Table><tr><td>Report Header</td></tr><tr></tr></Table>";
                    //HttpContext.Current.Response.Write(headerTable);

                    string str = string.Empty;
                        string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                        HttpContext.Current.Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
                    
                    HttpContext.Current.Response.Write("<head>");
                        HttpContext.Current.Response.Write("<meta http-equiv='Content-Type' content='text/html;charset=windows-1252'>");
                      //  HttpContext.Current.Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");

                        HttpContext.Current.Response.Write("<!--[if gte mso 9]>");
                        HttpContext.Current.Response.Write("<xml>");
                        HttpContext.Current.Response.Write("<x:ExcelWorkbook>");
                        HttpContext.Current.Response.Write("<x:ExcelWorksheets>");
                        HttpContext.Current.Response.Write("<x:ExcelWorksheet>");
                        //this line names the worksheet
                        if (!string.IsNullOrEmpty(SheetName))
                            HttpContext.Current.Response.Write("<x:Name>"+SheetName+"</x:Name>");
                        else
                            HttpContext.Current.Response.Write("<x:Name>gridlineTest</x:Name>");
                        
                        HttpContext.Current.Response.Write("<x:WorksheetOptions>");
                        //these 2 lines are what works the magic
                        HttpContext.Current.Response.Write("<x:Panes>");
                        HttpContext.Current.Response.Write("</x:Panes>");
                        HttpContext.Current.Response.Write("</x:WorksheetOptions>");
                        HttpContext.Current.Response.Write("</x:ExcelWorksheet>");
                        HttpContext.Current.Response.Write("</x:ExcelWorksheets>");
                        HttpContext.Current.Response.Write("</x:ExcelWorkbook>");
                        HttpContext.Current.Response.Write("</xml>");
                        HttpContext.Current.Response.Write("<![endif]-->");
                        HttpContext.Current.Response.Write("</head>");
                        HttpContext.Current.Response.Write(style);
                        HttpContext.Current.Response.Write("<body>");

                        if (Title == "Packages")
                        {
                            var Counter = 0;
                      
                        // data of table
                            foreach (DataRow dr in dt.Rows)
                            {
                                HttpContext.Current.Response.Write("<Table border='1' >");
                                for (int j = 0; j < dt.Columns.Count; j++)
                                {
                                    //for firt two rows of excel pattern 
                                    if (dr[0].ToString() != "")
                                    {
                                        if (j < 2)
                                        {
                                            Counter = 0;
                                            HttpContext.Current.Response.Write("<TR>");
                                            HttpContext.Current.Response.Write("<TD><B>");
                                            HttpContext.Current.Response.Write(dt.Columns[j].ColumnName);
                                            HttpContext.Current.Response.Write("</B></TD>");
                                            HttpContext.Current.Response.Write("<TD>");
                                            HttpContext.Current.Response.Write(dr[j].ToString());
                                            HttpContext.Current.Response.Write("</TD>");
                                            HttpContext.Current.Response.Write("<TD>");
                                            HttpContext.Current.Response.Write("</TD>");
                                            HttpContext.Current.Response.Write("</TR>");
                                        }
                                    }

                                    //for three rows  headers only of excel pattern  
                                    if (dr[2].ToString() != "" && Counter == 0)
                                    {
                                        //Counter = 1;
                                        if (j == 0)
                                        {
                                            HttpContext.Current.Response.Write("<TR><TD><B>List Of Products </B></TD>");
                                        }
                                        if (j >= 2)
                                        {
                                            HttpContext.Current.Response.Write("<TD><B>");
                                            HttpContext.Current.Response.Write(dt.Columns[j].ColumnName);
                                            HttpContext.Current.Response.Write("</B></TD>");
                                        }

                                    }
                                }

                                //for fourth rows  data of products of excel pattern  
                                for (int j = 0; j < dt.Columns.Count; j++)
                                {
                                    if (dr[2].ToString() != "")
                                    {
                                        Counter = 1;
                                        if (j == 0)
                                        {
                                            HttpContext.Current.Response.Write("<TR><TD></TD>");
                                        }
                                        if (j >= 2)
                                        {
                                            HttpContext.Current.Response.Write("<TD>");
                                            HttpContext.Current.Response.Write(dr[j].ToString());
                                            HttpContext.Current.Response.Write("</TD>");
                                            if (j == 3)
                                            {
                                                HttpContext.Current.Response.Write("</TR>");
                                            }
                                        }
                                    }
                                }

                                //for empty/merged row per package detail
                                if (dr[2].ToString() == "" && dr[0].ToString() == "")
                                {
                                    HttpContext.Current.Response.Write("<TR><TD colspan='3'></TD></TR>");
                                }
                                HttpContext.Current.Response.Write("</TR>");
                                HttpContext.Current.Response.Write("</Table>");
                            }

                        }

                        else
                        {
                            // headers columns of table
                            HttpContext.Current.Response.Write("<Table border='1' >");

                            if (!string.IsNullOrEmpty(HeadingName))
                            {
                                if(dt.Columns.Count>0)
                                    HttpContext.Current.Response.Write("<TR><TD colspan='"+dt.Columns.Count+ "' style='text-align:center;'>" + HeadingName + "</TD></TR>");
                                else
                                    HttpContext.Current.Response.Write("<TR><TD colspan='0' style='text-align:center;'>" + HeadingName + "</TD></TR>");
                            }
                            HttpContext.Current.Response.Write("<TR>");
                            foreach (DataColumn dtcol in dt.Columns)
                            {
                                HttpContext.Current.Response.Write("<TD>");
                                HttpContext.Current.Response.Write(@"<style> TD { mso-number-format:\@; } </style>");
                                HttpContext.Current.Response.Write(dtcol.ColumnName);
                                HttpContext.Current.Response.Write("</TD>");
                                ///str = "\t";
                            }
                            HttpContext.Current.Response.Write("</TR>");
                            // HttpContext.Current.Response.Write("\n");

                            // data of table
                            foreach (DataRow dr in dt.Rows)
                            {
                                str = "";
                                HttpContext.Current.Response.Write("<TR>");
                                for (int j = 0; j < dt.Columns.Count; j++)
                                {
                                    HttpContext.Current.Response.Write("<TD>");
                                    HttpContext.Current.Response.Write(@"<style> TD { mso-number-format:\@; } </style>");
                                    HttpContext.Current.Response.Write(Convert.ToString(dr[j]));
                                    HttpContext.Current.Response.Write("</TD>");
                                    //str = "\t";
                                }
                                HttpContext.Current.Response.Write("</TR>");
                                //HttpContext.Current.Response.Write("\n");
                            }
                            HttpContext.Current.Response.Write("</Table>");
                        }
                        HttpContext.Current.Response.Write("</body>");
                        HttpContext.Current.Response.Write("</html>");
                    //}
                }
                else
                {
                    HttpContext.Current.Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
                    HttpContext.Current.Response.Write("<head>");
                    HttpContext.Current.Response.Write("<meta http-equiv='Content-Type' content='text/html;charset=windows-1252'>");
                    HttpContext.Current.Response.Write("<!--[if gte mso 9]>");
                    HttpContext.Current.Response.Write("<xml>");
                    HttpContext.Current.Response.Write("<x:ExcelWorkbook>");
                    HttpContext.Current.Response.Write("<x:ExcelWorksheets>");
                    HttpContext.Current.Response.Write("<x:ExcelWorksheet>");
                    //this line names the worksheet
                    if (!string.IsNullOrEmpty(SheetName))
                        HttpContext.Current.Response.Write("<x:Name>" + SheetName + "</x:Name>");
                    else
                        HttpContext.Current.Response.Write("<x:Name>gridlineTest</x:Name>");
                    HttpContext.Current.Response.Write("<x:WorksheetOptions>");
                    //these 2 lines are what works the magic
                    HttpContext.Current.Response.Write("<x:Panes>");
                    HttpContext.Current.Response.Write("</x:Panes>");
                    HttpContext.Current.Response.Write("</x:WorksheetOptions>");
                    HttpContext.Current.Response.Write("</x:ExcelWorksheet>");
                    HttpContext.Current.Response.Write("</x:ExcelWorksheets>");
                    HttpContext.Current.Response.Write("</x:ExcelWorkbook>");
                    HttpContext.Current.Response.Write("</xml>");
                    HttpContext.Current.Response.Write("<![endif]-->");
                    HttpContext.Current.Response.Write("</head>");
                    HttpContext.Current.Response.Write("<body>");
                    HttpContext.Current.Response.ContentEncoding = Encoding.UTF8;
                    HttpContext.Current.Response.Output.Write(html);

                    HttpContext.Current.Response.Write("</body>");
                    HttpContext.Current.Response.Write("</html>");
                }
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {

            }

        }

        public void ExportToPrint(System.Data.DataTable dt = null, string Title = "", string Subtitle = "",
                    string GroupingBy = "", string SubTotal = "", string GrandTotal = "", string html = "", 
                    string footer = "")
        {
            try
            {
                string href1 = "", href2 = "";
                string Host = HttpContext.Current.Request.Url.AbsolutePath;
                Host = Host.Split('/')[1];
                if (HttpContext.Current.Request.Url.AbsoluteUri.Contains("localhost"))
                {
                    Host = "";
                }
                if (HttpContext.Current.Request.Url.AbsoluteUri.Contains("localhost"))
                {
                    href1 = "http://" + HttpContext.Current.Request.Url.Authority + "/" + Host + "/Content/assets/css/bootstrap.min.css";
                    href2 = "http://" + HttpContext.Current.Request.Url.Authority + "/" + Host + "/Content/assets/css/style.css";
                }
                else if (HttpContext.Current.Request.Url.AbsoluteUri.Contains("digitech.com"))
                {
                    href1 = "http://" + HttpContext.Current.Request.Url.Authority + "/" + Host + "/Content/assets/css/bootstrap.min.css";
                    href2 = "http://" + HttpContext.Current.Request.Url.Authority + "/" + Host + "/Content/assets/css/style.css";
                }
                else
                {
                    href1 = "http://" + HttpContext.Current.Request.Url.Authority + "/" + Host + "/Content/assets/css/bootstrap.min.css";
                    href2 = "http://" + HttpContext.Current.Request.Url.Authority + "/" + Host + "/Content/assets/css/style.css";
                }

                StringBuilder strHTMLBuilder = new StringBuilder();
                strHTMLBuilder.Append("<html>");
                strHTMLBuilder.Append("<head>");
                strHTMLBuilder.Append("</head><meta charset='utf-8' /><link href=" + href1 + " rel='stylesheet' />");
                strHTMLBuilder.Append("<link href=" + href2 + " rel='stylesheet' /><style> html { font: 11pt sans-serif; } ");
                strHTMLBuilder.Append(" .thead {color: #464a4c;background-color: #eceeef;}");
                strHTMLBuilder.Append(" </style>");
                strHTMLBuilder.Append("<body>");

                //heading 
                if(!string.IsNullOrEmpty(Title))
                    strHTMLBuilder.Append("<h2 style='text-align:center;font-size:25px !important'> List of " + Title + "</h2>");
                if (!string.IsNullOrEmpty(Subtitle))
                    strHTMLBuilder.Append("<h4 style='text-align:center;'>" + Subtitle + "</h4>");

                strHTMLBuilder.Append("<table class='table table-bordered top-space-20' border = '1px solid'>");

                string DateText = "Date";
                if (!string.IsNullOrEmpty(footer))
                {
                    if (footer.ToLower().Contains("no date"))
                        DateText = "";
                    else
                        DateText = footer;
                    
                }
                    


                if (dt != null)
                {
                    if (Title == "Packages")
                    {
                        var Counter = 0;
                        // data of table
                        foreach (DataRow dr in dt.Rows)
                        {
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                //for firt two rows of excel pattern 
                                if (dr[0].ToString() != "")
                                {
                                    if (j < 2)
                                    {
                                        Counter = 0;
                                        strHTMLBuilder.Append("<TR>");
                                        strHTMLBuilder.Append("<TD class='thead'><B>");
                                        strHTMLBuilder.Append(dt.Columns[j].ColumnName);
                                        strHTMLBuilder.Append("</B></TD>");
                                        strHTMLBuilder.Append("<TD>");
                                        strHTMLBuilder.Append(dr[j].ToString());
                                        strHTMLBuilder.Append("</TD>");
                                        strHTMLBuilder.Append("<TD>");
                                        strHTMLBuilder.Append("</TD>");
                                        strHTMLBuilder.Append("</TR>");
                                    }
                                }

                                //for three rows  headers only of excel pattern  
                                if (dr[2].ToString() != "" && Counter == 0)
                                {
                                    //Counter = 1;
                                    if (j == 0)
                                    {
                                        strHTMLBuilder.Append("<TR class='thead'><TD><B>List Of Products </B></TD>");
                                    }
                                    if (j >= 2)
                                    {
                                        strHTMLBuilder.Append("<TD><B>");
                                        strHTMLBuilder.Append(dt.Columns[j].ColumnName);
                                        strHTMLBuilder.Append("</B></TD>");
                                        //strHTMLBuilder.Append("<TD></TD>");
                                    }

                                }
                            }

                            //for fourth rows  data of products of excel pattern  
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                if (dr[2].ToString() != "")
                                {
                                    Counter = 1;
                                    if (j == 0)
                                    {
                                        strHTMLBuilder.Append("<TR><TD></TD>");
                                    }
                                    if (j >= 2)
                                    {
                                        strHTMLBuilder.Append("<TD>");
                                        strHTMLBuilder.Append(dr[j].ToString());
                                        strHTMLBuilder.Append("</TD>");
                                        if (j == 3)
                                        {
                                            strHTMLBuilder.Append("</TR>");
                                        }
                                    }
                                }
                            }
                            //for empty/merged row per package detail
                            if (dr[2].ToString() == "" && dr[0].ToString() == "")
                            {
                                strHTMLBuilder.Append("<TR><TD colspan='3'></TD></TR>");
                            }

                        }
                        strHTMLBuilder.Append("</TR>");
                        strHTMLBuilder.Append("</Table>");
                        strHTMLBuilder.Append("</body>");
                        strHTMLBuilder.Append("</html>");

                    }
                    else
                    {
                        strHTMLBuilder.Append("<tr class='thead'>");

                        //total columns
                        foreach (DataColumn col in dt.Columns)
                        {
                            strHTMLBuilder.Append("<td >");
                            strHTMLBuilder.Append(col.ColumnName);
                            strHTMLBuilder.Append("</td>");

                        }
                        strHTMLBuilder.Append("</tr>");

                        //total rows
                        foreach (DataRow row in dt.Rows)
                        {

                            strHTMLBuilder.Append("<tr >");
                            foreach (DataColumn col in dt.Columns)
                            {
                                strHTMLBuilder.Append("<td >");
                                strHTMLBuilder.Append(row[col.ColumnName].ToString());
                                strHTMLBuilder.Append("</td>");

                            }
                            strHTMLBuilder.Append("</tr>");
                        }

                        //Close tags.  
                        strHTMLBuilder.Append("</table>");
                        strHTMLBuilder.Append("</body>");
                        strHTMLBuilder.Append("</html>");
                    }
                }
                else
                {
                    //html case
                    strHTMLBuilder.Append(html);
                    strHTMLBuilder.Append("</table>");
                    strHTMLBuilder.Append("</body>");
                    strHTMLBuilder.Append("</html>");
                }
                //footer
                if (!string.IsNullOrEmpty(DateText))
                {
                    strHTMLBuilder.Append("<h4 style='text-align:right;padding-left:10px'>" + DateText + ": "
                                        + DateTime.Now.ToString("dd/MM/yyy") + "</h4>");
                }
                var strHTML = strHTMLBuilder.ToString();

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Write(strHTML);
                HttpContext.Current.Response.Write("<script>window.print();</script>");
                HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {

            }

        }
    
        public void ExportToExcelXLSX(System.Data.DataTable dt = null, string Title = "", string Subtitle = "",
                  string GroupingBy = "", string SubTotal = "", string GrandTotal = "", string html = "", string SheetName = "", string HeadingName="")
        {
            try
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + Title
                                                        + "_" + DateTime.Now.ToString() + ".xlsx");
                //HttpContext.Current.Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                HttpContext.Current.Response.Buffer = true;

                if (dt != null)
                {
                    string str = string.Empty;
                    string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                    HttpContext.Current.Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
                    HttpContext.Current.Response.Write("<head>");
                    HttpContext.Current.Response.Write("<meta http-equiv='Content-Type' content='text/html;charset=windows-1252'>");
                    HttpContext.Current.Response.Write("<!--[if gte mso 9]>");
                    HttpContext.Current.Response.Write("<xml>");
                    HttpContext.Current.Response.Write("<x:ExcelWorkbook>");
                    HttpContext.Current.Response.Write("<x:ExcelWorksheets>");
                    HttpContext.Current.Response.Write("<x:ExcelWorksheet>");
                    //this line names the worksheet
                    if (!string.IsNullOrEmpty(SheetName))
                        HttpContext.Current.Response.Write("<x:Name>" + SheetName + "</x:Name>");
                    else
                        HttpContext.Current.Response.Write("<x:Name>gridlineTest</x:Name>");
                    HttpContext.Current.Response.Write("<x:WorksheetOptions>");
                    //these 2 lines are what works the magic
                    HttpContext.Current.Response.Write("<x:Panes>");
                    HttpContext.Current.Response.Write("</x:Panes>");
                    HttpContext.Current.Response.Write("</x:WorksheetOptions>");
                    HttpContext.Current.Response.Write("</x:ExcelWorksheet>");
                    HttpContext.Current.Response.Write("</x:ExcelWorksheets>");
                    HttpContext.Current.Response.Write("</x:ExcelWorkbook>");
                    HttpContext.Current.Response.Write("</xml>");
                    HttpContext.Current.Response.Write("<![endif]-->");
                    HttpContext.Current.Response.Write("</head>");
                    HttpContext.Current.Response.Write(style);
                    HttpContext.Current.Response.Write("<body>");

                    if (Title == "Packages")
                    {
                        var Counter = 0;



                        // data of table
                        foreach (DataRow dr in dt.Rows)
                        {
                            HttpContext.Current.Response.Write("<Table border='1' >");
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                //for firt two rows of excel pattern 
                                if (dr[0].ToString() != "")
                                {
                                    if (j < 2)
                                    {
                                        Counter = 0;
                                        HttpContext.Current.Response.Write("<TR>");
                                        HttpContext.Current.Response.Write("<TD><B>");
                                        HttpContext.Current.Response.Write(dt.Columns[j].ColumnName);
                                        HttpContext.Current.Response.Write("</B></TD>");
                                        HttpContext.Current.Response.Write("<TD>");
                                        HttpContext.Current.Response.Write(dr[j].ToString());
                                        HttpContext.Current.Response.Write("</TD>");
                                        HttpContext.Current.Response.Write("<TD>");
                                        HttpContext.Current.Response.Write("</TD>");
                                        HttpContext.Current.Response.Write("</TR>");
                                    }
                                }

                                //for three rows  headers only of excel pattern  
                                if (dr[2].ToString() != "" && Counter == 0)
                                {
                                    //Counter = 1;
                                    if (j == 0)
                                    {
                                        HttpContext.Current.Response.Write("<TR><TD><B>List Of Products </B></TD>");
                                    }
                                    if (j >= 2)
                                    {
                                        HttpContext.Current.Response.Write("<TD><B>");
                                        HttpContext.Current.Response.Write(dt.Columns[j].ColumnName);
                                        HttpContext.Current.Response.Write("</B></TD>");
                                    }

                                }
                            }

                            //for fourth rows  data of products of excel pattern  
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                if (dr[2].ToString() != "")
                                {
                                    Counter = 1;
                                    if (j == 0)
                                    {
                                        HttpContext.Current.Response.Write("<TR><TD></TD>");
                                    }
                                    if (j >= 2)
                                    {
                                        HttpContext.Current.Response.Write("<TD>");
                                        HttpContext.Current.Response.Write(dr[j].ToString());
                                        HttpContext.Current.Response.Write("</TD>");
                                        if (j == 3)
                                        {
                                            HttpContext.Current.Response.Write("</TR>");
                                        }
                                    }
                                }
                            }

                            //for empty/merged row per package detail
                            if (dr[2].ToString() == "" && dr[0].ToString() == "")
                            {
                                HttpContext.Current.Response.Write("<TR><TD colspan='3'></TD></TR>");
                            }
                            HttpContext.Current.Response.Write("</TR>");
                            HttpContext.Current.Response.Write("</Table>");
                        }

                    }
                    else
                    {
                        // headers columns of table
                        HttpContext.Current.Response.Write("<Table border='1' >");
                        HttpContext.Current.Response.Write("<TR>");
                        foreach (DataColumn dtcol in dt.Columns)
                        {
                            HttpContext.Current.Response.Write("<TD>");
                            HttpContext.Current.Response.Write(@"<style> TD { mso-number-format:\@; } </style>");
                            HttpContext.Current.Response.Write(dtcol.ColumnName);
                            HttpContext.Current.Response.Write("</TD>");
                            ///str = "\t";
                        }
                        HttpContext.Current.Response.Write("</TR>");
                        // HttpContext.Current.Response.Write("\n");

                        // data of table
                        foreach (DataRow dr in dt.Rows)
                        {
                            str = "";
                            HttpContext.Current.Response.Write("<TR>");
                            for (int j = 0; j < dt.Columns.Count; j++)
                            {
                                HttpContext.Current.Response.Write("<TD>");
                                HttpContext.Current.Response.Write(@"<style> TD { mso-number-format:\@; } </style>");
                                HttpContext.Current.Response.Write(Convert.ToString(dr[j]));
                                HttpContext.Current.Response.Write("</TD>");
                                //str = "\t";
                            }
                            HttpContext.Current.Response.Write("</TR>");
                            //HttpContext.Current.Response.Write("\n");
                        }
                        HttpContext.Current.Response.Write("</Table>");
                    }
                    HttpContext.Current.Response.Write("</body>");
                    HttpContext.Current.Response.Write("</html>");

                }
                else
                {
                    HttpContext.Current.Response.Write("<html xmlns:x='urn:schemas-microsoft-com:office:excel'>");
                    HttpContext.Current.Response.Write("<head>");
                    HttpContext.Current.Response.Write("<meta http-equiv='Content-Type' content='text/html;charset=windows-1252'>");
                    HttpContext.Current.Response.Write("<!--[if gte mso 9]>");
                    HttpContext.Current.Response.Write("<xml>");
                    HttpContext.Current.Response.Write("<x:ExcelWorkbook>");
                    HttpContext.Current.Response.Write("<x:ExcelWorksheets>");
                    HttpContext.Current.Response.Write("<x:ExcelWorksheet>");
                    //this line names the worksheet
                    if (!string.IsNullOrEmpty(SheetName))
                        HttpContext.Current.Response.Write("<x:Name>" + SheetName + "</x:Name>");
                    else
                        HttpContext.Current.Response.Write("<x:Name>gridlineTest</x:Name>");
                    HttpContext.Current.Response.Write("<x:WorksheetOptions>");
                    //these 2 lines are what works the magic
                    HttpContext.Current.Response.Write("<x:Panes>");
                    HttpContext.Current.Response.Write("</x:Panes>");
                    HttpContext.Current.Response.Write("</x:WorksheetOptions>");
                    HttpContext.Current.Response.Write("</x:ExcelWorksheet>");
                    HttpContext.Current.Response.Write("</x:ExcelWorksheets>");
                    HttpContext.Current.Response.Write("</x:ExcelWorkbook>");
                    HttpContext.Current.Response.Write("</xml>");
                    HttpContext.Current.Response.Write("<![endif]-->");
                    HttpContext.Current.Response.Write("</head>");
                    HttpContext.Current.Response.Write("<body>");
                    HttpContext.Current.Response.ContentEncoding = Encoding.UTF8;
                    HttpContext.Current.Response.Output.Write(html);

                    HttpContext.Current.Response.Write("</body>");
                    HttpContext.Current.Response.Write("</html>");
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                }
                HttpContext.Current.Response.End();
            }
            catch (Exception ex)
            {

            }

        }


        public DataTable READExcel(string path)
        {
            //Create a new DataTable.
            DataTable dt = new DataTable();
            try
            {
                //Open the Excel file using ClosedXML.
                using (XLWorkbook workBook = new XLWorkbook(path))
                {
                    //Read the first Sheet from Excel file.
                    IXLWorksheet workSheet = workBook.Worksheet(1);



                    //Loop through the Worksheet rows.
                    bool firstRow = true;
                    foreach (IXLRow row in workSheet.Rows())
                    {
                        //Use the first row to add columns to DataTable.
                        if (firstRow)
                        {
                            foreach (IXLCell cell in row.Cells())
                            {
                                dt.Columns.Add(cell.Value.ToString());
                            }
                            firstRow = false;
                        }
                        else
                        {
                            //Add rows to DataTable.
                            dt.Rows.Add();
                            int i = 0;
                            foreach (IXLCell cell in row.Cells())
                            {
                                dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
                                i++;
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                _Logger.InsertLog(LogLevel.Error, ex.Message, ex.StackTrace, null);
            }
            return dt;
        }
    }
}

