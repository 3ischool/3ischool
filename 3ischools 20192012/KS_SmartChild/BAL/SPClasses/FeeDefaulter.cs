﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.BAL.SPClasses
{
    public class FeeDefaulter
    {
    }
    public class FeeDefaulterList
    {
        public string Class { get; set; }
        public int ClassId { get; set; }
        public decimal Amount { get; set; }
        public string StudentName { get; set; }
        public string AdmissionNo { get; set; }
        public string RollNo { get; set; }
        public int StudentId { get; set; }
        public int FeeTypeId { get; set; }
        public int CurrentPeriodId { get; set; }
        public decimal PayableAmount { get; set; }

    }

    public class FeeCollectionListmodel
    {
        public int ReceiptId { get; set; }
        public string RollNo { get; set; }
        public string AdmissionNo { get; set; }
        public string StudentName { get; set; }

        public string DOJ { get; set; }

        public string PaymentDate { get; set; }

        public string BankName { get; set; }
        public bool IsHostler { get; set; }
        public string RecieptType { get; set; }
        public string Class { get; set; }
        public string PaymentMethod { get; set; }
        public string ChequeNo { get; set; }
        public string Details { get; set; }
        public decimal Amount { get; set; }
        public bool IsOld { get; set; }
        public int ReceiptNo { get; set; }
    }

    public class StudentList
    {
        public string Class { get; set; }
        public int ClassId { get; set; }
        public decimal Amount { get; set; }
        public string StudentName { get; set; }
        public string AdmissionNo { get; set; }
        public string RollNo { get; set; }
        public int StudentId { get; set; }
        public int FeeTypeId { get; set; }
        public int CurrentPeriodId { get; set; }
        public decimal PayableAmount { get; set; }
        public decimal MonthlyAmount { get; set; }

    }
}