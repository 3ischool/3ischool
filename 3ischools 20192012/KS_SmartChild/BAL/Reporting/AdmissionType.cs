﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.BAL.Reporting
{
    public enum AdmissionType
    {
        Admitted = 1,
        Registered = 2,
        Waiting = 3,
        Rejected = 4
    }
}