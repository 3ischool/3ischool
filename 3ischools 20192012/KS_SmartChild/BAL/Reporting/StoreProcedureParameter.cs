﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.BAL.Reporting
{
    public class StoreProcedureParameter
    {
        public string ParaName { get; set; }

        public dynamic Value { get; set; }
    }
}