﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.BAL.Reporting
{
    public class CustomList
    {
        public string Value { get; set; }

        public bool Selected { get; set; }

        public string Text { get; set; }

        public bool Disabled { get; set; }

        public string ExtraData { get; set; }
    }
}