﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KS_SmartChild.BAL.Message
{
    public enum MsgQueueStatusEnum
    {
        Delivered = 1,
        Failed = 2,
        Rejected=16,
        StatusUnknown=17
    }
}