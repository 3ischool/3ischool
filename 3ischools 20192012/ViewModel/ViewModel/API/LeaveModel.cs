﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.API
{
    public class LeaveModel
    {
        public int LeaveId { get; set; }

        public string LeaveType { get; set; }

        public string LeaveFrom { get; set; }

        public string LeaveTill { get; set; }

        public string Reason { get; set; }

        public string Status { get; set; }

        public string ReasonDesc { get; set; }
    }
    public class LeaveApprovalModel
    {
        public int LeaveId { get; set; }

        public string LeaveFrom { get; set; }

        public string LeaveTill { get; set; }

        public string Name { get; set; }

        public string AppliedDate { get; set; }

        public string Class { get; set; }

        public string RollNo { get; set; }


        public string LeaveType { get; set; }



        public string Reason { get; set; }

        public string Status { get; set; }

        public string ReasonDesc { get; set; }
    }
}