﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.API
{
    public class HomeWorkModel
    {
        public HomeWorkModel() {
            HomeworkDetails =new List<HomeWorkDetails>();
            HomeWorkDetail = new HomeWorkDetails();
        }
        public string Type { get; set; }
        public string Class { get; set; }
        public string Subject { get; set; }
        public string SubjectCode { get; set; }
        public string Date { get; set; }
        public HomeWorkDetails HomeWorkDetail { get; set; }
        public IList<HomeWorkDetails> HomeworkDetails { get; set; }
        public string teacher { get; set; }
    }
    public class HomeWork
    {
       
        public string Type { get; set; }
        public string Class { get; set; }
        public string Subject { get; set; }
        public string SubjectCode { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string Remarks { get; set; }
        public string File { get; set; }
        public string teacher { get; set; }
    }
    public class HomeWorkDetails {
        public string Description { get; set; }
        public string Remarks { get; set; }
        public string File { get; set; }
    }
    public class HomeWorkTypeModel
    {
        public string HomeWorkType { get; set; }
    }
    public class PeriodModel
    {
        public string Period { get; set; }
        public string ClassSubjectId { get; set; }
        public bool IsCurrentPeriod { get; set; }
    }
    public class classlist
    {
        public classlist()
        {
            subjectlist = new List<SubjectList>();
            
        }
        public string Class { get; set; }
        public IList<SubjectList> subjectlist = new List<SubjectList>();
       
    }
    public class SubjectList
    {
        public string Subject { get; set; }
        public string ClassSubjectId { get; set; }
    }
    public class nextdatesmodel
    {
        public string currentTimetableStartDate { get; set; }
        public string currentTimeTableEndDate { get; set; }
        public bool issundayenabled { get; set; }
    }
}