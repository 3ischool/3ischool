﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.API
{
    public class StudentInfoModel
    {
        public StudentInfoModel()
        {
        }

        // Info 
        public string AdmissionNo { get; set; }

        public string AdmissionDate { get; set; }

        public string DOB { get; set; }

        public string DOJ { get; set; }

        public string Email { get; set; }

        public string Gender { get; set; }

        public string Category { get; set; }

        public string BloodGroup { get; set; }

        public string Allergy { get; set; }

        public string PhoneNo { get; set; }

        public string CurrentClass { get; set; }

        public string CurrentClassRollno { get; set; }

        public string BoardRollno { get; set; }

        // Address

        public string AddressType { get; set; }

        public string PlotNo { get; set; }

        public string SectorOrStreet { get; set; }

        public string Locality { get; set; }

        public string Zip { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        // Guardian Info

        public string FatherName { get; set; }

        public string MotherName { get; set; }

        public string GuardianName { get; set; }

    }

    public class StudentHealthDetail
    {
        public string Class { get; set; }

        public string Weight { get; set; }

        public string Height { get; set; }

        public string VisionL { get; set; }

        public string VisionR { get; set; }
    }

    public class AppStudentActivity
    {
        public string Activity { get; set; }

        public string Description { get; set; }

        public string EffectiveDate { get; set; }

        public string EndDate { get; set; }
    }

    public class AppStudentFee
    {
        public string ReceiptNo { get; set; }

        public string ReceiptDate { get; set; }

        public string FeeType { get; set; }

        public string FeePeriod { get; set; }

        public string Amount { get; set; }

        public string TotalAmount { get; set; }

        public string PendingAmount { get; set; }

    }

    public class AppStudentTimeTable
    {
        public string Period { get; set; }

        public string PeriodTime { get; set; }

        public string Subject { get; set; }

        public string Teacher { get; set; }

    }

    public class AppStudentExams
    {
        public string Subject { get; set; }

        public string ExamDate { get; set; }

        public string TimeSlot { get; set; }

        public string MarksPattern { get; set; }
    }

    public class AppStudentExamsList
    {
        public AppStudentExamsList()
        {
            DateSheetDetail = new List<ExaminationDateSheetDetail>();
        }

        public string ExamActivity { get; set; }

        public string DateSheetName { get; set; }

        public string FromDate { get; set; }

        public string TillDate { get; set; }

        public string Status { get; set; }

        public IList<ExaminationDateSheetDetail> DateSheetDetail { get; set; }
      
    }

    public class ExaminationDateSheetDetail
    {
        public string Subject { get; set; }

        public string ExamDate { get; set; }

        public string TimeSlot { get; set; }

        public string MarksPattern { get; set; }

        public string MaxMarks { get; set; }

        public string obtainedMarks { get; set; }

        public string SubjectId { get; set; }

        public string DateSheetDetailId { get; set; }
    }

    public class AppStudentTransport
    {
        public string TransportType { get; set; }

        public string BoardingPoint { get; set; }

        public string VehicleType { get; set; }

        public string VehicleNo { get; set; }

        public string EffectiveDate { get; set; }

        public string EndDate { get; set; }

        public string BusRoute { get; set; }

        public string Parking { get; set; }
    }

    public class AppStudentEvent
    {
        public AppStudentEvent()
        {
            EventImages = new List<AppStudentEventImages>();
            EventTitles = new List<AppStudentEventTitles>();
        }

        public string Status { get; set; }

        public string Class { get; set; }

        public string StartDate { get; set; }

        public string StartTime { get; set; }

        public string EndDate { get; set; }
        public string EventTitle { get; set; }
        public string Description { get; set; }
        public string Venue { get; set; }

        public IList<AppStudentEventImages> EventImages { get; set; }

        public IList<AppStudentEventTitles> EventTitles { get; set; }

    }
    public class Anouncements
    {
        public string Date { get; set; }
        public string EventTitle { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
    }

    public class DashBoardNew
    {
        public string Type { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        public string PostedOn { get; set; }
        public string PostedBy { get; set; }
    }

    public class Holidays
    {
        public string Date { get; set; }
        public string EventTitle { get; set; }
        public string Description { get; set; }
    }
    public class Vacations
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string EventTitle { get; set; }
        public string Description { get; set; }
    }
    public class Activities
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Venue { get; set; }
        public string EventTitle { get; set; }
        public string Description { get; set; }
        public string Class { get; set; }
    }
    public class Dashboard
    {
        public Dashboard()
        {
            DashBoardList = new List<DashBoardNew>();

        }
        public string Type { get; set; }

        public IList<DashBoardNew> DashBoardList { get; set; }
        //event
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Venue { get; set; }
        public string EventTitle { get; set; }
        public string Description { get; set; }
        public string Class { get; set; }

        //exam
        public string ClassSubjectName { get; set; }
        public string TimeSlot { get; set; }
        public string ExamDate { get; set; }

        //fee
        public string Fee { get; set; }

        //hw
        public string Subject { get; set; }
        public string teacher { get; set; }
        public string Date { get; set; }
    }


    public class AppStudentEventImages
    {
        public string Image { get; set; }
    }

    public class AppStudentEventTitles
    {
        public string Title { get; set; }
    }

    public class AppStudentAttendanceModel
    {
        public AppStudentAttendanceModel()
        {

            AttendanceList = new List<AppMothlyAttendanceStatusModel>();

        }


        public string Month { get; set; }

        // public string Attendance { get; set; }

        public string Absent { get; set; }

        public string Leave { get; set; }

        public string Present { get; set; }

        public string Total { get; set; }

        public IList<AppMothlyAttendanceStatusModel> AttendanceList { get; set; }
    }

    public class AppMothlyAttendanceStatusModel
    {

        public string Date { get; set; }

        public string Status { get; set; }

    }
    public class ExamNotification
    {

        public string ClassSubjectName { get; set; }
        public string TimeSlot { get; set; }
        public string ExamDate { get; set; }

    }

    public class CalenderModel
    {

        public CalenderModel()
        {
            DailyData = new List<DailyData>();
        }
        public string Date { get; set; }
        public string Activities { get; set; }
        public string Announcements { get; set; }
        public string Vacations { get; set; }
        public string Holidays { get; set; }
        public string Exams { get; set; }
        public string ExamResult { get; set; }
        public IList<DailyData> DailyData { get; set; }


    }
    public class DailyData
    {
        public DailyData()
        {
            Details = new List<dynamic>();
        }
      
        public IList<dynamic> Details { get; set; }
    }

    public class CalenderList
    {
        public CalenderList()
        {
            Details = new List<dynamic>();
        }
     
        public IList<dynamic> Details { get; set; }
    }
    public class CommonDetailEvents
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Venue { get; set; }
        public string EventTitle { get; set; }
        public string Description { get; set; }
        public string Class { get; set; }
        public string Type { get; set; }
    }
    public class ExamResultNotification
    {
        public string Standard { get; set; }

        public DateTime? ResultDate { get; set; }

        public string ExamDateSheet { get; set; }
    }

    public class ClassList
    {
        public string ClassId { get; set; }

        public string Class { get; set; }
    }
    public class StudentMarksList
    {
        public string StudentName { get; set; }
        public string RollNo { get; set; }
        public string Marks { get; set; }
    }
}