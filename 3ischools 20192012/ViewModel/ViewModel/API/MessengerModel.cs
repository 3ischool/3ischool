﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.API
{
    public class MessengerModel
    {

    }
    public class inboxoutboxmessage
    {
        public int messageid { get; set; }
        public string MessageDescription { get; set; }
        public string MessageTitle { get; set; }
        public string username { get; set; }
        public string messagetime { get; set; }
        public bool Inactive { get; set; }
        public int MessageSenderId { get; set; }
        public bool Isdeleted { get; set; }
        public string currentusername { get; set; }
        public string UserImage { get; set; }
        public string From { get; set; }
    }
    public class MessagesListModel
    {
        public int MessageId { get; set; }
        public string MessageDescription { get; set; }
        public string MessageTitle { get; set; }
        public DateTime MessagesTime { get; set; }
        public string classname { get; set; }
        public string sendernames { get; set; }
        public string messagesender { get; set; }
        public string MessageDatetime { get; set; }
        public DateTime? KendoMessageDatetime { get; set; }
        public string IOTSelection { get; set; }
        public bool Inactive { get; set; }
        public int SenderId { get; set; }
        public string users { get; set; }
        public DateTime? fromdate { get; set; }
        public DateTime? todate { get; set; }
        public string currentusername { get; set; }
        public string EncMessageId { get; set; }
        public string EncSenderId { get; set; }
    }
    public class users
    {
        public int selecteduserid { get; set; }
    }
    public class lists
    {
        public string Text { get; set; }
    }
}