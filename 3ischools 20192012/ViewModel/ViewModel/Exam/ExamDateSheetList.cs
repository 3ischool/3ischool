﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.Exam
{
    public class ExamDateSheetList
    {
        public int? DateSheetId { get; set; }

        public string DateSheet { get; set; }

        public int? ActivityId { get; set; }

        public string ActivityName { get; set; }
        public string strExamTerm { get; set; }
        public string strExamActivity { get; set; }
        public string strClassGroup { get; set; }
        

        public string FromDate { get; set; }

        public DateTime? dtFromDate { get; set; }

        public DateTime? DateSheetFromDate { get; set; }

        public DateTime? DateSheetTillDate { get; set; }

        public string ToDate { get; set; }

        public bool IsSetClasses { get; set; }

        public bool SetDateSheetPublish { get; set; }

        public bool IsEdited { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsCreated { get; set; }

        public bool IsFillTheMarks { get; set; }

        public bool IsPublishResult { get; set; }

        public int IsPublished { get; set; }

        public string Publish { get; set; }

        public string EncDateSheetId { get; set; }

        public bool IsTermEnd { get; set; }

        public bool IsDateSheetDetails { get; set; }

        public bool IsDeleteShow { get; set; }

        public int ExamTermActivityId { get; set; }

        public bool? AllResultPublished { get; set; }

        public bool? AnyResultPublished{ get; set; }
    }
}