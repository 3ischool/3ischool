﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Exam
{
    public class ClassDateSheetDetailModel
    {
        public ClassDateSheetDetailModel()
        {
            StandardClassesList = new List<string>();
            ClassIdList = new List<int>();
            ClassWiseList = new List<ClassDateSheetDetailClassWiseModel>();
            ExamDateStrList = new List<string>();
            ExamTimeSlotList = new List<string>();
            MarksPatternIdList = new List<string>();
            IsInternalAssessmentApplicableList = new List<bool>();
            ExamClassDateSheetDetailIdList = new List<int>();
        }
        public int ExamDateSheetDetailId { get; set; }

        public int? ExamDateSheetId { get; set; }

        public string StandardClasses { get; set; }

        public List<string> StandardClassesList { get; set; }
        public List<int> ClassIdList { get; set; }

        public List<ClassDateSheetDetailClassWiseModel> ClassWiseList { get; set; }

        public string ClassId { get; set; }

        public int? SubjectId { get; set; }

        public int? SkillId { get; set; }

        public int? ClassSubjectId { get; set; }

        public string SubjectName { get; set; }

        public DateTime? ExamDate { get; set; }

        public string ExamDateStr { get; set; }
        public List<string> ExamDateStrList { get; set; }

        public int? ExamTimeSlot { get; set; }
        public List<string> ExamTimeSlotList { get; set; }

        public string ExamTimeSlotStr { get; set; }

        public int? MarksPatternId { get; set; }
        public List<string> MarksPatternIdList { get; set; }

        public string MarksPatternStr { get; set; }
        public int? GradePatternId { get; set; }

        public string ClassName { get; set; }

        public bool OnlyShow { get; set; }

        public bool IsInternalAssessmentApplicable { get; set; }

        public List<bool> IsInternalAssessmentApplicableList { get; set; }

        public bool SetExamDateTimeSlotDisabled { get; set; }

        public bool SetMarksPatternDisabled { get; set; }

        public bool SetInternalAssessmentDisabled { get; set; }

        public bool NotShowChekboxforInternalassesment { get; set; }
        public List<int> ExamClassDateSheetDetailIdList { get; set; }

        public int? StandardId { get; set; }
        public string ReportCardText { get; set; }
        public int? Index { get; set; }
        public bool IsOmitted { get; set; }
        public int? ExamSubjectGroupTypeId { get; set; }
        public int ExamStandardSubjectGroupId { get; set; }

        public string BindedEndTermSkills { get; set; }
    }

    public class ClassDateSheetDetailClassWiseModel
    {
       
      

        public int? ExamClassDateSheetDetailId { get; set; }

        public string Classid { get; set; }

        

        

        public DateTime? ExamDat { get; set; }

       

        public int? ExamTimeSlotId { get; set; }

      

        public int? MarksPatternid { get; set; }

       

        public bool IsInternalAssessmentApplicabl { get; set; }

        
    }
}