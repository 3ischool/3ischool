﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Exam
{
    public class ExamDateSheetModel
    {
        public ExamDateSheetModel()
        {
            ExamDateSheetList = new List<ExamDateSheetList>(); 
             AvailableExamActivities = new List<SelectListItem>();
            AvailableClassGroup = new List<SelectListItem>(); 
             AvailableExamActivitiesTable = new List<ExamActivitiesModel>();
            AvailableFilterExamActivitiesTable = new List<ExamActivitiesModel>();
            AvailableStandards = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
            AvailableExamTimeSlots = new List<SelectListItem>();
            AvailableMarksPatterns = new List<SelectListItem>();
            AvailableGradePattern = new List<SelectListItem>(); 
             AvailableSessions = new List<SelectListItem>();
            AvailableSessionsWithDates = new List<SessionWithDate>();
            DateSheetClassesList = new List<DateSheetClassesModel>();
            ScholasticSkillsList = new List<ExamTermEndSkillsModel>();
            OtherStudentDetailsList = new List<OtherStudentDetails>();
            AvailableSendMessageMobileNoOptions = new List<SelectListItem>();
            AvailableExamsubjectgrouptype = new List<SelectListItem>();
        }

        public bool IsAdmin { get; set; }

        public IList<ExamDateSheetList> ExamDateSheetList { get; set; }

        public int? ExamTermActivityId { get; set; }
        public int? ClassGroupId { get; set; }

        public IList<SelectListItem> AvailableExamActivities { get; set; }
        public IList<SelectListItem> AvailableClassGroup { get; set; }
        public IList<ExamActivitiesModel> AvailableExamActivitiesTable { get; set; }
        public IList<ExamActivitiesModel> AvailableFilterExamActivitiesTable { get; set; }
        

        public int? StandardId { get; set; }

        public IList<SelectListItem> AvailableStandards { get; set; }

        public int? ClassId { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }

        public int? ExamDateSheetId { get; set; }

        public string DateSheetName { get; set; }

        public DateTime? FDate { get; set; }

        public DateTime? TDate { get; set; }

        public DateTime? ExamResultDate { get; set; }

        public DateTime? ExamDate { get; set; }

        public int? ExamTimeSlot { get; set; }
        public int? filterClassGroupId { get; set; }
        
        public IList<SelectListItem> AvailableExamTimeSlots { get; set; }

        public int? MarksPatternId { get; set; }

        public IList<SelectListItem> AvailableMarksPatterns { get; set; }

        public string DateSheetDetailArray { get; set; }

        public int? MaxCountSingleClassSubject { get; set; }

        public bool StuSMS { get; set; }

        public bool StuNonAppUser { get; set; }

        public bool StuNotification { get; set; }

        public bool SMS { get; set; }

        public bool NonAppUser { get; set; }

        public bool Notification { get; set; }

        public bool GuardSMS { get; set; }

        public bool GuardNonAppUser { get; set; }

        public bool GuardNotification { get; set; }

        public bool ShowLeftStudentInFillMarks { get; set; }
        public string SMSSelectionStudentMobileNo { get; set; }

        public List<SelectListItem> AvailableSendMessageMobileNoOptions { get; set; }

        // Marks Pattern Properties
        public int? ClassSubjectId { get; set; }

        public int? GradePatternId { get; set; }

        public IList<SelectListItem> AvailableGradePattern { get; set; }

        public decimal? MaxMarks { get; set; }

        public decimal? Passmarks { get; set; }

        public bool IsPublish { get; set; }

        public DateTime? PublishDate { get; set; }

        public DateTime? UnPublishDate { get; set; }

        public string SchoolName { get; set; }

        public string SchoolAddress { get; set; }

        public string SchoolLogoPath { get; set; }

        public int? SessionId { get; set; }
        
        public IList<SelectListItem> AvailableSessions { get; set; }
        public IList<SessionWithDate> AvailableSessionsWithDates { get; set; }

        public IList<DateSheetClassesModel> DateSheetClassesList { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToSet { get; set; }

        public bool IsAuthToSetDetail { get; set; }

        public bool IsAuthToPublish { get; set; }

        public bool IsAuthToFill { get; set; }

        public bool IsAuthToAnnounce { get; set; }

        public bool IsAuthToViewResult { get; set; }

        public bool IsAuthToDelete { get; set; }

        public string EncExamDateSheetId { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public bool IsAuthToviewresultDate { get; set; }

        public bool IsAuthTotermEndFill { get; set; }

        public IList<ExamTermEndSkillsModel> ScholasticSkillsList { get; set; }

        public IList<OtherStudentDetails> OtherStudentDetailsList { get; set; }

        public bool ExamInternalAssesmentApplicable { get; set; }

        public string ExamInternalAssessmentText { get; set; }

        public bool ClassWiseDateSheetApplicable { get; set; }
        public string FillExamTermSkillMethod { get; set; }
        public int ExamsubjectgrouptypeId { get; set; }
        public IList<SelectListItem> AvailableExamsubjectgrouptype { get; set; }

        public bool IsSetGroup { get; set; }

        public bool IsFreezeTermEndSkillMarksAfterResultAnnounceMent { get; set; }

        public bool IsFreezeTermEndSubjectMarksAfterResultAnnounceMent { get; set; }

    }

    public class ExamActivitiesModel
    {
        public string ExamTermActivityId { get; set; }
        public string ExamTermActivity { get; set; }
        public string filterExamTermActivity { get; set; }
        public string ExamActivity { get; set; }
        public string Examterm { get; set; }
        public string ClassGroup { get; set; }
    }
    public class SessionWithDate
    {
        public bool Selected { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }
        public string startdate { get; set; }
        public string enddate { get; set; }
    }
    public class DateSheetClassesModel
    {
        public int? ExamResultDateId { get; set; }

        public int? ClassId { get; set; }

        public string ClassName { get; set; }

        public DateTime? ExamResultDate { get; set; }

        public string ClassExamResultDate { get; set; }

        public bool? IsChangeable { get; set; }

        public string DateSheetClassesArray { get; set; }

        public string IfResultPublishDate { get; set; }

        public bool IsDeleted { get; set; }

        public bool AllowExamResultDateChange { get; set; }

        public bool AllowExamResultDateSeletion { get; set; }

        public bool AllowExamResultDateSeletionifExamdateset { get; set; }

    }

    public class ExamTermEndSkillsModel
    {
        public string ExamTermEndSkill { get; set; }
        public string ExamtermendSkillDetail { get; set; }
        public int ExamTermEndSkillId { get; set; }
        public int ExamtermendSkillDetailId { get; set; }

        public int ExamTermActivitySkillId { get; set; }
        public int ExamTermActivityDetailId { get; set; }
        public string Value { get; set; }
        public bool IsBinded { get; set; }
        public bool? IsSkillDetail { get; set; }
    }

    public class ExamtermendSkilldetail
    {
        public int ExamTermEndSkillDetailId { get; set; }

        public string ExamTermEndSkillDetail { get; set; }

        public int ExamTermEndSkillsId { get; set; }

    }
    public class OtherStudentDetails
    {
        public OtherStudentDetails()
        {
            AvailableBloodGroups = new List<SelectListItem>();
        }
        public string StdName { get; set; }
        public string ROllNo { get; set; }
        public string AdmnNo { get; set; }
        public int StdId { get; set; }
        public int? ExamStudentAttendanceId { get; set; }
        public string Height { get; set; }
        public int? StudentClassDetailsId { get; set; }
        public decimal? Weight { get; set; }
        public int? BloodGroupId { get; set; }
        public int TotalPresentDays { get; set; }
        public int? TotalDays { get; set; }
        public int? StudentClassId { get; set; }
        public IList<SelectListItem> AvailableBloodGroups { get; set; }

    }
    public class ExamTermEndSkillSSubjectWiseModel
    {
        public ExamTermEndSkillSSubjectWiseModel()
        {
            ExamTermEndSkillsList = new List<ExamTermEndSkillsModel>();
            OtherStudentDetails = new List<OtherStudentDetails>();
        }
        public int ClassSubjectId { get; set; }
        public IList<ExamTermEndSkillsModel> ExamTermEndSkillsList { get; set; }
        public IList<OtherStudentDetails> OtherStudentDetails { get; set; }
    }

    public class ExamDateSheetSubjectSkillModel
    {
        public int? ExamDateSheetSubjectSkillId { get; set; }
        public int? ExamTermEndSkillId { get; set; }
        public int? ExamDatesheetDetailId { get; set; }
        public string ExamTermEndSkill { get; set; }
        public bool? IsAlreadySaved { get; set; }
    }

    public class ExamTermEndSkillMarksModel
    {
        public ExamTermEndSkillMarksModel()
        {
            StudentMarksList = new List<StudentExamTermSkillModel>();
            StudentDetailsList = new List<OtherStudentDetails>();
        }
        public int ExamDateSheetId { get; set; }
        public int ExamTermEndSkillId { get; set; }
        public int ClassSubjectId { get; set; }
        public int?[] ExamTermEndSkillIdArray { get; set; }
        public int?[] ExamTermEndSkillDetailIdArray { get; set; }
        public int?[] ExamTermActivitySkillIdArray { get; set; }
        public IList<OtherStudentDetails> StudentDetailsList { get; set; }
        public IList<StudentExamTermSkillModel> StudentMarksList { get; set; }
    }
    public class StudentExamTermSkillModel
    {
        public StudentExamTermSkillModel()
        {
            StudentSkillMarksList = new List<StudentExamTermEndSkillMarks>();
        }
        public int StudentId { get; set; }
        public int ExamTermActivitySkillId { get; set; }
       public IList<StudentExamTermEndSkillMarks> StudentSkillMarksList { get; set; }

    }
    public class StudentExamTermEndSkillMarks
    {

        public int ExamTermEndSkillId { get; set; }
        public int ExamtermendSkillDetailId { get; set; }
        public string ExamTermEndSkillValue { get; set; }

    }
}