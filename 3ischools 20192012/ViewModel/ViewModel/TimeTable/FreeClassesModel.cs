﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.TimeTable
{
    public class FreeClassesModel
    {
        public FreeClassesModel()
        {
            FreeClassesList = new List<FreeClassesList>();
        }

        public DateTime? CurrentDate { get; set; }

        public string EndSessionDate { get; set; }
        
        public string Date { get; set; }

        public IList<FreeClassesList> FreeClassesList { get; set; }
    }

    public class FreeClassesList
    {
        public string ClassName { get; set; }

        public string Periods { get; set; }
    }

}