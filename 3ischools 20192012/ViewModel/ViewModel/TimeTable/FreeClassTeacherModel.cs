﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.TimeTable
{
    public class FreeClassTeacherModel
    {
        public FreeClassTeacherModel()
        {
            AvailablePeriodList = new List<SelectListItem>();
            FreeTeacherList = new List<FreeTeacherList>();
          
        }

        public DateTime? CurrentDate { get; set; }

        public string Date { get; set; }

        public int? PeriodId { get; set; }

        public string PeriodTime { get; set; }

        public string NoPeriodFound { get; set; }

        public IList<SelectListItem> AvailablePeriodList { get; set; }

        public IList<FreeTeacherList> FreeTeacherList { get; set; }
    }

    public class FreeTeacherList
    {
        public string TeacherName { get; set; }

        public string AssignedClasses { get; set; }

        public string AssignedSubjects { get; set; }

        public string PhoneNo { get; set; }

        public string Email { get; set; }
    }
    public class periodlist
    {
        public string periodId { get; set; }

        public string Period { get; set; }
    }
}