﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.TimeTable
{
    public class TimeTableAdjustmentModel : RealTimeViewTimeTable
    {
        public TimeTableAdjustmentModel()
        {
            TimeTableViewList = new List<TimeTableViewList>();
            TeacherList = new List<SelectListItem>();
            TimeTableType = new List<SelectListItem>();
            AvailableAttendanceStatus = new List<SelectListItem>();
        }

        public int? CurntSchedule { get; set; }

        public int? Day { get; set; }

        public IList<TimeTableViewList> TimeTableViewList { get; set; }

        public int? TeacherId { get; set; }

        public List<SelectListItem> TeacherList { get; set; }

        public bool IsLeave { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToExport { get; set; }

        public bool IsAuthToPrint { get; set; }

        public int? TypeId { get; set; }

        public List<SelectListItem> TimeTableType { get; set; }

        public IList<SelectListItem> AvailableAttendanceStatus { get; set; }

        public int AttendanceStatusId { get; set; }

        public bool ShowAdjustmentOnly { get; set; }

        public bool IsExport { get; set; }

    }

    public class TimeTableViewList
    {
        public string ClassName { get; set; }

        public string TeacherName { get; set; }

        public string SubjectName { get; set; }

        public int? ClassPeriodId { get; set; }

        public int? ClassPeriodDetailId { get; set; }

        public int? StaffId { get; set; }

        public int? ClassId { get; set; }

        public int? SubjectId { get; set; }

        public DateTime? date { get; set; }

        public int? PeriodId { get; set; }

        public string AttendanceStatus { get; set; }

        public int? TimeTableId { get; set; }

        public string Remarks { get; set; }
        public string SubstituteId { get; set; }

        public string SubstitutorId { get; set; }

        public int? TimeTableAdjustmentId { get; set; }

        public int? DayId { get; set; }

        public string AttendaceStatus { get; set; }

        public string PeriodName { get; set; }

        public string AsPerAdjust {get;set; }

        public int PeriodIndex { get; set; }

        public int VirtualClassId { get; set; }

    }
}