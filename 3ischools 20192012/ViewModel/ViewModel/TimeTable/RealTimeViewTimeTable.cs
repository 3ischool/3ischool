﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.TimeTable
{
    public class RealTimeViewTimeTable
    {
        public RealTimeViewTimeTable()
        {
            AvailablePeriodList = new List<SelectListItem>();
            RealTimeViewList = new List<RealTimeViewList>();
            PeriodList = new List<periodlist>();
        }

        public DateTime? CurrentDate { get; set; }

        public string Date { get; set; }

        public int? PeriodId { get; set; }

        public string PeriodTime { get; set; }

        public string NoPeriodFound { get; set; }

        public DateTime? SessionStartDate { get; set; }

        public bool IsAdjustmentOnly { get; set; }

        public DateTime? SessionEndDate { get; set; }

        public IList<SelectListItem> AvailablePeriodList { get; set; }
        public IList<periodlist> PeriodList { get; set; }
        public IList<RealTimeViewList> RealTimeViewList { get; set; }

        public bool IsAuthToExport { get; set; }

        public bool IsAuthToPrint { get; set; }

        public string ScrollSpeed { get; set; }

    }

    public class RealTimeViewList
    {
        public string ClassName { get; set; }

        public string TeacherName { get; set; }

        public string SubjectName { get; set; }

        public string AdjTeacherName { get; set; }

        public string AdjSubjectName { get; set; }

        public string AttendanceStatus { get; set; }
    }

}