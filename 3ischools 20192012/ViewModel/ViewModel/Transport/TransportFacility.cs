﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Transport
{
    public class TransportFacility
    {
        public TransportFacility()
        {
            AvailableTptType = new List<SelectListItem>();
            AvailableBusRoute = new List<SelectListItem>();
            AvailableBoardingPoint = new List<SelectListItem>();
            AvailableVehicleType = new List<SelectListItem>();
            AvailableTptTypeDet = new List<SelectListItem>();
            OwnershipType = new List<SelectListItem>();
        }

        public int? TptFacilityId { get; set; }

        public int? TptTypeId { get; set; }

        public int? TptTypeDetailId { get; set; }

        public int? BusRouteId { get; set; }
        public int? DropRouteId { get; set; }

        public int? BoardingPointId { get; set; }
        public int? DropBoardingPointId { get; set; }

        public int? VehicleTypeId { get; set; }

        public string VehicleNo { get; set; }

        public bool ParkingRequired { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public string EffDate { get; set; }

        public IList<SelectListItem> AvailableTptType { get; set; }

        public IList<SelectListItem> AvailableTptTypeDet { get; set; }

        public IList<SelectListItem> AvailableBusRoute { get; set; }

        public IList<SelectListItem> AvailableBoardingPoint { get; set; }

        public IList<SelectListItem> AvailableVehicleType { get; set; }

        public string TransportUpdated { get; set; }

        public string Ownershipid { get; set; }

        public IList<SelectListItem> OwnershipType { get; set; }


    }
}