﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Transport
{
    public class TransportRouteModel
    {
        public TransportRouteModel()
        {
            AvailableBuses = new List<SelectListItem>();
            AvailableDrivers = new List<SelectListItem>();
            BusRouteList = new List<BusRouteList>();
            AvailableBoardingPoint = new List<BoardingPointModel>();
            BusRouteDetailList = new List<BusRouteDetailModel>();
            AvailableAttendants = new List<SelectListItem>();
            FeeFrequencyList = new List<SelectListItem>();
            StudentModel = new Student.StudentModel();
            SessionList = new List<SelectListItem>();
        }

        // bus route 

        public bool IsAdmin { get; set; }

        public int? BusRouteId { get; set; }

        public string BusRouteName { get; set; }

        public string BusRouteNo { get; set; }

        public int? BusId { get; set; }

        public IList<SelectListItem> AvailableBuses { get; set; }

        public int? DriverId { get; set; }

        public IList<SelectListItem> AvailableDrivers { get; set; }

        public string EffDate { get; set; }

        public string ToDate { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public IList<BusRouteList> BusRouteList { get; set; }

        public IList<BoardingPointModel> AvailableBoardingPoint { get; set; }

        public bool IsSchoolLocationSet { get; set; }

        public bool IsShowActiveOnly { get; set; }

        public int? ModuleId { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToClose { get; set; }

        public bool IsAuthToDelete { get; set; }

        public IList<BusRouteDetailModel> BusRouteDetailList { get; set; }

        public bool IsAuthToCreateRouteDetail { get; set; }

        public int? BusRouteDetailId { get; set; }

        public int? BoardingPointId { get; set; }

        public int? BoardingIndex { get; set; }

        public bool? IsDuplicateBoardingPointAllowed { get; set; }

        public string PickTime { get; set; }

        public string DropTime { get; set; }

        public decimal? Distance { get; set; }

        public bool IsPickRoot { get; set; }

        public DateTime? MinEffectiveDate { get; set; }

        public DateTime? MaxEffectiveDate { get; set; }

        public int? AttendantId { get; set; }

        public IList<SelectListItem> AvailableAttendants { get; set; }

        public IList<SelectListItem> BoardingPointList { get; set; }

        public IList<SelectListItem> SessionList { get; set; }

        public int SessionId { get; set; }

        public bool IsAuthToViewDetail { get; set; }

        public bool IsAuthToViewRecord { get; set; }
        public decimal? PickAndDropFee { get; set; }
        public decimal? SinglePickOrDropFee { get; set; }
        public int FeeFrequencyId { get; set; }
        public IList<SelectListItem> FeeFrequencyList { get; set; }

        public Student.StudentModel StudentModel {get; set;}
    }

    public class RouteInfoModel
    {
        public RouteInfoModel()
        {
            RoutStudentList = new List<RouteStudentInfoList>();
        }

        public string RouteId { get; set; }

        public string BoradingPiontId { get; set; }

        public string BoradingPiont { get; set; }

        public List<RouteStudentInfoList> RoutStudentList { get; set; }

    }

    public class RouteStudentInfoList
    {
        public string RouteId { get; set; }

        public string StudentId { get; set; }

        public string StudentName { get; set; }

        public string RollNo { get; set; }

        public string ContactNo { get; set; }

        public string Class { get; set; }

        public string TptFacilityType { get; set; }

        public string AdmnNo { get; set; }
    }

    public class BusRouteList
    {
        public int BusRouteId { get; set; }

        public int BoardingPointId { get; set; }

        public string EncBusRouteId { get; set; }

        public string BusRouteName { get; set; }

        public string BusRouteNo { get; set; }

        public bool BusRouteStatus { get; set; }

        public int BusId { get; set; }

        public string BusName { get; set; }

        public int DriverId { get; set; }

        public string DriverName { get; set; }

        public string EffectiveDate { get; set; }

        public DateTime dtEffectiveDate { get; set; }

        public string EndDate { get; set; }

        public DateTime? dtEndDate { get; set; }

        public bool NoAction { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsAuthToViewDetails { get; set; }

        public string StudentCount { get; set; }

        public string PickCount { get; set; }

        public string DropCount { get; set; }

        public DateTime? MinEndDate { get; set; }

        public DateTime? MaxEndDate { get; set; }
        public string PickBoardingPoints { get; set; }
        public string DropBoardingPoints { get; set; }
        public int AttendantId { get; set; }

        public string AttendantName { get; set; }
        public string TptRouteDetailId { get; set; }
    }

    public class BoardingPointModel
    {
        public BoardingPointModel()
        {
            BoardingPointList = new List<BoardingPointModel>();
            FeeFrequencyList = new List<SelectListItem>();
            BusRouteList = new List<SelectListItem>();
            AvailableBoardingPoint = new List<BoardingPointModel>();
        }
        public int BoardingPointFeeId { get; set; }
        public string BoardingPointId { get; set; }
        public IList<BoardingPointModel> AvailableBoardingPoint { get; set; }
        public string BoardingPoint { get; set; }
        public int? RouteId { get; set; }
        public string Lat { get; set; }
        public string CurrentSessionStartDate { get; set; }
        public string CurrentDateForTodate { get; set; }
        public string Long { get; set; }

        public string BoardingPointLatitude { get; set; }
        public string BoardingPointLongitude { get; set; }

        public decimal? PickAndDropFee { get; set; }
        public decimal? SinglePickOrDropFee { get; set; }

        public IList<BoardingPointModel> BoardingPointList { get; set; }

        public bool IsDeletable { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public int FeeFrequencyId { get; set; }

        public string FeeFrequency { get; set; }

        public IList<SelectListItem> FeeFrequencyList { get; set; }

        public IList<SelectListItem> BusRouteList { get; set; }

        public string EffectiveDate { get; set; }

        public string EndDate { get; set; }

        public DateTime? dtEffectiveDate { get; set; }

        public DateTime? dtEndDate { get; set; }

        public string Sessionstartdate { get; set; }

        public bool? IsExistInAnotherTable { get;set;}

        public string strIsExistInAnotherTable { get; set; }

        public string ExistIn { get;set;}
    }

    public class BoardingPointListModel
    {

        public string BoardingPointId { get; set; }

        public string BoardingPoint { get; set; }

        public string Lat { get; set; }

        public string Long { get; set; }

        public string BoardingPointLatitude { get; set; }
        public string BoardingPointLongitude { get; set; }

        public decimal? PickAndDropFee { get; set; }
        public decimal? SinglePickOrDropFee { get; set; }

    }

    public class BusRouteDetailModel
    {
        public int? BusRouteDetailId { get; set; }

        public int? BoardingPointId { get; set; }

        public string BoardingPoint { get; set; }

        public int? BoardingIndex { get; set; }

        public string PickTime { get; set; }

        public string DropTime { get; set; }

        public string DisplayPickTime { get; set; }

        public string DisplayDropTime { get; set; }

        public decimal? Distance { get; set; }

        public decimal? RelventDistance { get; set; }

        public bool IsPickRoot { get; set; }

        public string EffectiveDate { get; set; }

        public string EndDate { get; set; }

        public string Lat { get; set; }

        public string Long { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsInUse { get; set; }

    }

    public class BusRouteStudents
    {
        public string StudentName { get; set; }

        public string ClassName { get; set; }

        public string RollNo { get; set; }

        public string AdmnNo { get; set; }

        public string BoardingPoint { get; set; }

        public string TptType { get; set; }

        public string ContactNo { get; set; }

    }

    public class DriverAttendantModel
    {
        public int DriverId { get; set; }
        public int AttendantId { get; set; }
        public string DriverName { get; set; }
        public string AttendantName { get; set; }
        public string Name { get; set; }
        public string TptRouteDetail { get; set; }
        public int TptRouteDetailId { get; set; }
        public string EffectiveDate { get; set; }
        public string EndDate { get; set; }
    }
}