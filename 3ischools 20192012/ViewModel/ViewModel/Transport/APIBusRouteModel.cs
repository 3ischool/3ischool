﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.Transport
{
    public class APIBusRouteModel
    {
        public APIBusRouteModel()
        {
            BusRouteDetail = new List<APIBusRouteDetail>();
        }
        public string BusRouteIk { get; set; }
        public IList<APIBusRouteDetail> BusRouteDetail { get; set; }
    }

    public class APIBusRouteDetail
    {
        public string RoutePointType { get; set; }
        public string RoutePointName { get; set; }
        public string BusRouteLat { get; set; }
        public string BusRouteLong { get; set; }
    }


    public class APIBusGPSDataModel
    {
        public string Latitude { get; set; }
        public string LatitudeDirection { get; set; }
        public string Longitude { get; set; }
        public string LongitudeDirection { get; set; }
        public string Direction { get; set; }
        public string Speed { get; set; }
    }

}