﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.Security
{
    public class SessionModel
    {
        public string SessionId { get; set; }

        public string Session { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public DateTime? StartDateToPost { get; set; }

        public DateTime? EndDateToPost { get; set; }

        public bool IsDetault { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public bool IsDeletable { get; set; }

        public bool IsEditable { get; set; }

        public bool IsAuthToAdd { get; set; }
        public bool IsAuthtoEdit { get; set; }
        public bool IsAuthToDelete { get; set; }
    }
}