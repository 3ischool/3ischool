﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.Security
{
    public class AppFormPermissionModel
    {
        public AppFormPermissionModel()
        {
            AppFormData = new List<AppFormData>();
        }
        public string UserRoleTypeId { get; set; }
        public string UserRoleType { get; set; }
        public List<AppFormData> AppFormData { get; set; }
    }

    public class AppFormActionPermissionModel
    {
        public AppFormActionPermissionModel()
        {
        }
        public int UserRoleActionPermissionId { get; set; }
        public int UserRoleId { get; set; }
        public int? AppFormActionId { get; set; }
        public int AppFormId { get; set; }
        public bool IsForm { get; set; }
        public bool IsPermitted { get; set; }
    }
    public class AppFormData
    {
        public AppFormData()
        {
            Action = new List<Action>();
            ChildAppForm = new List<ChildAppForm>();
        }
        public string AppFormId { get; set; }
        public string AppForm { get; set; }
        public string AppFormParentId { get; set; }
        public string NavigationItemId { get; set; }
        public string NavigationItem { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string QueryString { get; set; }
        public List<Action> Action { get; set; }
        public List<ChildAppForm> ChildAppForm { get; set; }
    }

    public class Action
    {
        public string AppFormActionId { get; set; }
        public string ActionName { get; set; }
        public string FormActionTypeId { get; set; }
        public string FormActionType { get; set; }
    }

    public class ChildAppForm
    {
        public ChildAppForm()
        {
            Action = new List<Action>();
            ChildAppForm1 = new List<ChildAppForm1>();
        }
        public string AppFormId { get; set; }
        public string AppForm { get; set; }
        public string AppFormParentId { get; set; }
        public string NavigationItemId { get; set; }
        public string NavigationItem { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string QueryString { get; set; }
        public List<Action> Action { get; set; }
        public List<ChildAppForm1> ChildAppForm1 { get; set; }
    }

    public class ChildAppForm1
    {
        public ChildAppForm1()
        {
            Action = new List<Action>();
        }
        public string AppFormId { get; set; }
        public string AppForm { get; set; }
        public string AppFormParentId { get; set; }
        public string NavigationItemId { get; set; }
        public string NavigationItem { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string QueryString { get; set; }
        public List<Action> Action { get; set; }
    }
    public class APIDatabaseModel
    {
        public string DBName { get; set; }
        public int DBId { get; set; }
    }
}