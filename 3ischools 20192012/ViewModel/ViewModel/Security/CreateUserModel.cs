﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Security
{
    public class CreateUserModel
    {
        public CreateUserModel()
        {
            AvailableUserType = new List<SelectListItem>();

            AvailableStaff = new List<SelectListItem>();

            AvailableClasses = new List<SelectListItem>();

            AvailableGuardians = new List<SelectListItem>();

            AvailableStudents = new List<SelectListItem>();

            UserRoleList = new List<SelectListItem>();
        }

        public DateTime? RegdDate { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string VerifyPassword { get; set; }

        public bool? Status { get; set; }

        public int UserTypeId { get; set; }

        public int UserContactId { get; set; }

        public string EncUserContactId { get; set; }

        public DateTime PwdActivatedOn { get; set; }

        public int PwdActivatedById { get; set; }

        public DateTime PwdLastUpatedOn { get; set; }

        public int PwdLastUpatedbyId { get; set; }

        public int UserRoleId { get; set; }

        public int? DefaultProfileTypeId { get; set; }

        public string EncUserId { get; set; }

        public IList<SelectListItem> AvailableUserType { get; set; }

        public List<SelectListItem> AvailableStaff { get; set; }
        
        public List<SelectListItem> AvailableClasses { get; set; }

        public List<SelectListItem> AvailableGuardians { get; set; }

        public List<SelectListItem> AvailableStudents { get; set; }

        public List<SelectListItem> UserRoleList { get; set; }
    }
}