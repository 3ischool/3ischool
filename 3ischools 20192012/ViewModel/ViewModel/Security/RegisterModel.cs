﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.Security
{
    public class RegisterModel
    {
        [RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid mobile no.")]
        [StringLength(10, ErrorMessage = "Please enter valid mobile no.", MinimumLength = 10)]
        public string MobileNo { get; set; }

        public string UserId { get; set; }

        public int OTP { get; set; }

        [Required(ErrorMessage = "Password is required")]
      //  [StringLength(50, ErrorMessage = "Password must be at least 8 characters", MinimumLength = 8)]
        [RegularExpression(@"^(?=(.*\d){2})(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z\d]).{8,}$", ErrorMessage = "Password must contain at least 1 Special character,2 digits, Small, Capital letter and must be of 8 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password is required")]
       // [StringLength(50, ErrorMessage = "Confirm Password must be at least 8 characters", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        public bool IsFromPost { get; set; }

       
    }
    public class ResetPasswordModel
    {

        [Required(ErrorMessage = "New Password is required")]
        [StringLength(50, ErrorMessage = "New Password must be at least 8 characters", MinimumLength = 8)]
        [RegularExpression(@"^(?=(.*\d){2})(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z\d]).{8,}$", ErrorMessage = "Password must contain at least 1 Special character,2 digits, Small, Capital letter and must be of 8 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password is required")]
        [StringLength(50, ErrorMessage = "Confirm Password must be at least 8 characters", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }


        [Required(ErrorMessage = "Old Password is required")]
        [StringLength(50, ErrorMessage = "Old Password must be at least 8 characters", MinimumLength = 8)]
        [DataType(DataType.Password)]
        //[RegularExpression(@"^(?=(.*\d){2})(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z\d]).{8,}$", ErrorMessage = "Old Password must contain at least 1 Special character,2 digits, Small, Capital letter and must be of 8 characters")]
        public string OldPassword { get; set; }

        public int UserId { get; set; }

        public string UserName { get; set; }
    }
}