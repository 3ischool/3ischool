﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Stock
{
    public class StockMasterModule
    {
        public string defaultGridPageSize { get; set; }
        public string gridPageSizes { get; set; }
        public bool IsAuthToDelete { get; set; }
        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToAdd { get; set; }
        public string ProductName { get; set; }
        public string ProductType { get; set; }
        public string ProductGroup { get; set; }
    }

    public class ProductInfo : StockMasterModule
    {
        public ProductInfo()
        {
            ProductType = new List<SelectListItem>();
            ProductGroup = new List<SelectListItem>();
            Unit = new List<SelectListItem>();
            ProductAttribute = new List<SelectListItem>();
            ProductAttributeValue = new List<SelectListItem>();
            Class = new List<SelectListItem>();
            Products = new List<SelectListItem>();
            StockProductList = new List<ProductList>();
            PackageDetail = new List<PackageDetail>();
            RateHistoryDetail = new List<RateHistoryDetail>();
            SpecificationDetail = new List<ProductSpecificationDetail>();
            UnitConversionDetail = new List<ProductUnitConversionDetail>();
        }
        public string productid { get; set; }
        public string ProductName { get; set; }
        public string ClassesNames { get; set; }
        public string ProductDescription { get; set; }
        public List<SelectListItem> ProductType { get; set; }
        public List<SelectListItem> ProductGroup { get; set; }
        public IList<ProductList> StockProductList { get; set; }

        public string ProductAbbreviation { get; set; }
        public string OpeningBalance { get; set; }
        public List<SelectListItem> Unit { get; set; }
        public string MinStockqty { get; set; }
        public string MaxStockqty { get; set; }
        public string Reordeqty { get; set; }
        public bool Isbatch { get; set; }
        public bool Isexpiry { get; set; }
        public string ProductTypeId { get; set; }
        public string ProductGroupId { get; set; }
        public string Unitid { get; set; }
        public string ProductCode { get; set; }
        public string Barcode { get; set; }
        public bool IsConsumable { get; set; }
        public string ProductTypeName { get; set; }
        public string ProductGroupName { get; set; }
        public string UnitName { get; set; }

        //ProductRateHistory Properties
        public string ProductRateHistoryId { get; set; }
        public string EffectiveFrom { get; set; }
        public string EffectiveTill { get; set; }
        public decimal PurRate { get; set; }
        public decimal MRP { get; set; }

        // Product SpecificATION


        public string ProductAttributeId { get; set; }
        public List<SelectListItem> ProductAttribute { get; set; }
        public string ProductAttributeValueId { get; set; }
        public string ProductAttributeValueName { get; set; }
        public string ProductDetailId { get; set; }
        public string ProductAttributename { get; set; }
        public List<SelectListItem> ProductAttributeValue { get; set; }

        // Product Unit conversion
        public string ProductUnitConversionId { get; set; }
        public string ConversionUnitId { get; set; }
        public string Conversionfactor { get; set; }
        public string ConversionUnitName { get; set; }

        //ProductImage
        public string ProductImageId { get; set; }
        public string ProductImage { get; set; }
        public string ProductImagePath { get; set; }

        // Packages
        public string PackageId { get; set; }
        public string PackageName { get; set; }
        public List<SelectListItem> Class { get; set; }
        public List<SelectListItem> Products { get; set; }
        public string ClassId { get; set; }
        public string[] ClassIds { get; set; }
        public string[] Classnames { get; set; }
        public string Quantity { get; set; }
        public string PackageDetailId { get; set; }


        public string NumberofProducts { get; set; }
        public string NumberofClasses { get; set; }

        public IList<PackageDetail> PackageDetail { get; set; }

        public IList<RateHistoryDetail> RateHistoryDetail { get; set; }
        public IList<ProductSpecificationDetail> SpecificationDetail { get; set; }
        public IList<ProductUnitConversionDetail> UnitConversionDetail { get; set; }

    }

    public class ProductList
    {
        public string ProductName { get; set; }
        public string ProductAbbreviation { get; set; }
        public string ProductGroup { get; set; }
        public string ProductType { get; set; }
        public string OpeningBalance { get; set; }
        public string Unit { get; set; }
    }

    public class PackageDetail
    {
        public string PackageDetailId { get; set; }
        public string productid { get; set; }
        public string Quantity { get; set; }
        public string ProductName { get; set; }
    }

    public class RateHistoryDetail
    {
        public decimal PurRate { get; set; }
        public decimal MRP { get; set; }
        public string EffectiveFrom { get; set; }
        public string EffectiveTill { get; set; }
    }

    public class ProductSpecificationDetail
    {
        public string ProductAttributeName { get; set; }
        public string ProductAttributeValue { get; set; }
    }
    public class ProductUnitConversionDetail
    {
        public string Conversionfactor { get; set; }
        public string ConversionUnitName { get; set; }
    }
}