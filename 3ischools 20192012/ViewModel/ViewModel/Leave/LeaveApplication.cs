﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Leave
{
    public class LeaveApplication
    {
        public string LeaveId { get; set; }

        public string LeaveFrom { get; set; }

        public string FromTimeLimit { get; set; }

        public string ToTimeLimit { get; set; }

        public string LeaveTill { get; set; }

        public bool CanLeaveApply { get; set; }

        public int? LeaveTypeId { get; set; }

        public int? MaxLeaveApplyDays { get; set; }

        public string ContactType { get; set; }
        public IList<SelectListItem> LeaveTypeList { get; set; }

        public string Reason { get; set; }

        public string CurrentSessionStartDate { get; set; }

        public string CurrentSessionEndDate { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public bool IsAuthToApply { get; set; }
    }
}