﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Reporting
{
    public class ViewReportModel
    {
        public ViewReportModel()
        {
            ReportControlsList = new List<ReportControlsModel>();
            ColumnList = new List<ViewReportColumns>();
            RowList = new List<ViewReportRows>();
        }

        public string ReportMasterId { get; set; }

        public string FileName { get; set; }

        public IList<ReportControlsModel> ReportControlsList { get; set; }

        public IList<ViewReportColumns> ColumnList { get; set; }

        public IList<ViewReportRows> RowList { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        [AllowHtml]
        public string Grid { get; set; }
    }

    public class ReportControlsModel
    {
        public ReportControlsModel()
        {
            selectlist = new List<SelectListItem>();
        }

        public string Label { get; set; }

        public string ControlType { get; set; }

        public string ControlTypeDisplayName { get; set; }

        public IList<SelectListItem> selectlist { get; set; }
    }

    public class ViewReportColumns
    {
        public string ColumName { get; set; }

        public bool Ishidden { get; set; }

        public bool IsGroupable { get; set; }

        public bool IsSortable { get; set; }

    }

    public class ViewReportRows
    {
        public ViewReportRows()
        {
            ColValue = new List<dynamic>();
        }

        public IList<dynamic> ColValue { get; set; }
    }

}