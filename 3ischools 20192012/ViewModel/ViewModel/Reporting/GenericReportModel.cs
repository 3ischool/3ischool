﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Reporting
{
    public class GenericReportModel
    {
        public GenericReportModel()
        {
            GenericReportColsList = new List<GenericReportColsModel>();
            GenericReportParametersList = new List<GenericReportParametersModel>();
        }

        public int? RptMasterId { get; set; }

        public string EncRptMasterId { get; set; }

        public string RptMasterName { get; set; }

        public string RptStoredProcedureName { get; set; }

        public string RptHeaderName { get; set; }

        public string RptGenericName { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreateDate { get; set; }

        public bool IsActive { get; set; }

        public string Status { get; set; }

        public string RptColArray { get; set; }

        public string RptFltrArray { get; set; }

        public IList<GenericReportColsModel> GenericReportColsList { get; set; }

        public IList<GenericReportParametersModel> GenericReportParametersList { get; set; }

        public bool IsViewReport { get; set; }
    }

    public class GenericReportParametersModel
    {
        public GenericReportParametersModel()
        {
        }

         public int? RptGenericColId { get; set; }

        public string FilterName { get; set; }

        public string FilterLabel { get; set; }

        public int? FilterIndex { get; set; }

        public bool IsFilter { get; set; }


    }

    public class GenericReportColsModel
    {
        public GenericReportColsModel()
        {
        }

        public int? RptGenericColId { get; set; }

        public string ColName { get; set; }

        public string ColLabel { get; set; }

        public int? ColIndex { get; set; }

        public int? ColWidth { get; set; }

        public bool IsVisible { get; set; }

        public bool IsGroup { get; set; }

        public int? GroupIndex { get; set; }

        public bool IsSum { get; set; }
    }
    public enum AdmissionType
    {
        Admitted = 1,
        Registered = 2,
        Waiting = 3,
        Rejected = 4
    }
    public class CustomList
    {
        public string Value { get; set; }

        public bool Selected { get; set; }

        public string Text { get; set; }

        public bool Disabled { get; set; }

        public string ExtraData { get; set; }
    }
    public enum SortingMethod
    {
        Assending = 1,
        Descending = 2
    }
    public class StoreProcedureParameter
    {
        public string ParaName { get; set; }

        public dynamic Value { get; set; }
    }
}