﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Master
{
    public class ManagePeriod
    {
        public int? PeriodId { get; set; }

        public string PeriodName { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public string EndTimehi { get; set; }

        public string StartTimehi { get; set; }

        public bool IsPeriodTimeInValid { get; set; }

        public bool IsPeriodDeletable { get; set; }

        public bool IsPeriodExist { get; set; }

        public int? PeriodIndex { get; set; }

        public bool IsSameStartEndTimes { get; set; }

        public IList<SelectListItem> PeriodIndexList { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToDelete { get; set; }
    }
    public class Periods
    {
        public List<ManagePeriod> PeriodsDetail { get; set; }

        public bool IsAuthToDelete { get; set; }

        public bool IsAuthToEdit { get; set; }
    }
}