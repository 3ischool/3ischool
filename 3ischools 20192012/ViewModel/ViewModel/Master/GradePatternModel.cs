﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Master
{
    public class GradePatternModel
    {
        public GradePatternModel()
        {
            AvailableGrade = new List<SelectListItem>();
            GradePatternList = new List<GradePatternModel>();
            AvailableGradePattern = new List<SelectListItem>();
        }
        //grade pattern
        public int? GradePatternId { get; set; }
        public string GradePattern { get; set; }
        public int? BoardId { get; set; }
        //Grade Pattern Detail
        public int? GradePatternDetailId { get; set; }
        public Decimal? StartingMarks { get; set; }
        public Decimal? EndingMarks { get; set; }
        public string EffectiveDate { get; set; }
        public string EndDate { get; set; }
        public int? GradeId { get; set; }
        public string Grade { get; set; }
        public IList<SelectListItem> AvailableGrade { get; set; }
        public string Board { get; set; }
        public IList<GradePatternModel> GradePatternList { get; set; }
        public int hasgradepatterndetial { get; set; }
        public IList<SelectListItem> AvailableGradePattern { get; set; }
        public bool IsAuthToAdd { get; set; }
        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToDelete { get; set; }
        public bool IsAuthToAddGradePattern { get; set; }
        public bool IsAuthToClose { get; set; }
        public string EncGradePatternDetailId { get; set; }
    }
}