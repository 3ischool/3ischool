﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Master
{
    public class ExamTermActivityModel
    {
        public ExamTermActivityModel()
        {
            AvailableExamTerm = new List<SelectListItem>();
            AvailableSessions = new List<SelectListItem>();
        }

        public string ExamTermId { get; set; }
        public string filtSessionId { get; set; }

        public IList<SelectListItem> AvailableExamTerm { get; set; }
        public IList<SelectListItem> AvailableSessions { get; set; }

        public string ActivityArray { get; set; }

        public string StandardArray { get; set; }

    } 

    public class ExamActivityModel
    {
        public string ExamTermActivityId { get; set; }

        public string ExamActivityId { get; set; }

        public string ExamActivity { get; set; }

        public bool IsTermEnd { get; set; }

        public int IsDeleted { get; set; }

        public bool IsDisabled { get; set; }
        public bool? IsTermEndDisabled { get; set; }
        public bool? IsAnyTermMarksFilled { get; set; }

    }

    public class ExamTermStandardModel
    {
        public string ExamTermStandardId { get; set; }

        public string StandardName { get; set; }

        public string StandardId { get; set; }

        public int IsDeleted { get; set; }
        public bool? IsStandardMarksFilled { get; set; }
        public string ExamTermStandardTitle{ get; set; }
    }

}