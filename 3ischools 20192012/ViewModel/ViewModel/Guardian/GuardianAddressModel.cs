﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.Guardian
{
    public class GuardianAddressModel
    {
        public GuardianAddressModel()
        {
        }

        public int? GuardianAddressId { get; set; }

        public int? GuardianContactId { get; set; }

        public int GuardianAddressTypeId { get; set; }

        public int? GuardianContactTypeId { get; set; }

        public string GuardianPlotNo { get; set; }

        public string GuardianSector_Street { get; set; }

        public string GuardianLocality { get; set; }

        public string GuardianLandmark { get; set; }
        public string GuardianDistrict { get; set; }

        public string GuardianZip { get; set; }

        public int? GuardianCityId { get; set; }

        public string GuardianCityName { get; set; }

        public int? GuardianStateId { get; set; }

        public int? GuardianCountryId { get; set; }
        public string AddressGuardianId { get; set; }

        public string GuardianStateName { get; set; }

        public string GuardianCountryName { get; set; }
    }
}