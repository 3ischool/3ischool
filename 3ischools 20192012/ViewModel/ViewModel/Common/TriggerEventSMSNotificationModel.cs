﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.Common
{
    public class TriggerEventSMSNotificationModel
    {
        public bool IsTeachingStaffSMS { get; set; }
        public bool IsStudentSMS { get; set; }
        public bool IsGuardianSMS { get; set; }
        public bool IsTeachingStaffNotifications { get; set; }
        public bool IsStudentNotifications { get; set; }
        public bool IsGuardianNotifications { get; set; }
        public string ExamDateSheetId { get; set; }
        public DateTime? ExamPublishDate { get; set; }

    }
}
