﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.ViewModel.Security;

namespace ViewModel.ViewModel.Common
{
    public class DashBoardModel
    {
        public DashBoardModel()
        {
            SessionList = new List<SelectListItem>();
            AnnouncementList = new List<ActivityEvent>();
            HolidayList = new List<ActivityEvent>();
            VacationsList = new List<ActivityEvent>();
            ActivityList = new List<ActivityEvent>();
            ExamsList = new List<ExamNotification>();
            ExamResultList = new List<ExamResultNotification>();
            DocumentList = new List<DocumentNotification>();
            DocumentCategoryList = new List<DocumentCategorys>();
            List = new List<UserNavigationPermissionModel>();
            UserRoleList = new List<UserRoleList>();
            UserRoleModulePermissinList = new List<UserRoleModulePermissin>();
            NavigationList = new List<GrandNavigationModel>();
        }

        public string Usertype { get; set; }

        public string StudentStrength { get; set; }

        public string Enrolled { get; set; }

        public string Fees { get; set; }

        public string UnreadMessage { get; set; }

        public string Teacher { get; set; }

        public string Id { get; set; }

        public string Staff { get; set; }

        public string Attendance { get; set; }

        public int? SessionId { get; set; }

        public bool? IsDashBoardNavigationShow { get; set; }
        public bool? IsDashBoardPermissionShow { get; set; }

        public bool? IsNavigationWithoutException { get; set; }
        public bool? IsPermissionActionWithoutException { get; set; }

        public int UserRoleId { get; set; }

        public string PermArray { get; set; }

        public bool? IsAdmin { get; set; }

        public int UserRoleTypeId { get; set; }

        public string UserTypeRoleId { get; set; }

        public string UserRole { get; set; }

        public bool IsNavigationExist { get; set; }
        public IList<SelectListItem> SessionList { get; set; }

        public List<SelectListItem> UserRoleTypeList { get; set; }
        public IList<ActivityEvent> AnnouncementList { get; set; }

        public IList<ActivityEvent> HolidayList { get; set; }

        public IList<ActivityEvent> VacationsList { get; set; }

        public IList<ActivityEvent> ActivityList { get; set; }

        public IList<ExamNotification> ExamsList { get; set; }

        public IList<ExamResultNotification> ExamResultList { get; set; }

        public IList<DocumentNotification> DocumentList { get; set; }

        public IList<DocumentCategorys> DocumentCategoryList { get; set; }

        public List<UserNavigationPermissionModel> List { get; set; }

        public IList<UserRoleList> UserRoleList { get; set; }

        public IList<GrandNavigationModel> NavigationList { get; set; }

        public UserRolePermissionModel UserRolePermissionModel { get; set; }
        public IList<UserRoleModulePermissin> UserRoleModulePermissinList { get; set; }
    }
    public class ExamNotification
    {
        public string ClassSubjectName { get; set; }

        public string TimeSlot { get; set; }

        public DateTime? ExamDate { get; set; }

        public string exadate { get; set; }
    }

    public class ExamResultNotification
    {
        public string Standard { get; set; }

        public DateTime? ResultDate { get; set; }
        public string ResDate { get; set; }

        public string ExamDateSheet { get; set; }
    }

    public class ActivityEvent
    {
        public int EventId { get; set; }

        public string EventTitle { get; set; }

        public bool? IsAllSchool { get; set; }

        public string StartDate { get; set; }

        public string StartTime { get; set; }
        public string stdate { get; set; }
        public string endate { get; set; }
        public string Description { get; set; }

        public bool? IsActive { get; set; }

        public string Venue { get; set; }

        public int EventTypeId { get; set; }

        public bool? IsDeleted { get; set; }

        public string EndDate { get; set; }

        public string EndTime { get; set; }

        public string ActivityClass { get; set; }
    }

    public class DocumentNotification
    {
        public int DocumentCategoryId { get; set; }

        public string Document { get; set; }

        public string TimeSlot { get; set; }

        public string DocumentExp { get; set; }

        public string DocReminder { get; set; }

    }
    public class DocumentCategorys
    {
        public int DocumentCategoryId { get; set; }

        public string DocumentCategory { get; set; }

    }
    public class DocumentDashboard
    {
        public DocumentDashboard()
        {
            DocumentList = new List<DocumentNotification>();
        }
        public IList<DocumentNotification> DocumentList;
        public string DocumentCategory { get; set; }
    }

    public class GrandNavigationModel
    {
        public GrandNavigationModel()
        {
            ParentList = new List<ParentNavigationModel>();
        }
        public int NavigationItemId { get; set; }
        public string NavigationItem { get; set; }
        public bool IsActive { get; set; }
        public IList<ParentNavigationModel> ParentList;
    }
    public class ParentNavigationModel
    {
        public ParentNavigationModel()
        {
            ChildList = new List<ChildNavigationModel>();
        }
        public int NavigationItemId { get; set; }
        public string NavigationItem { get; set; }
        public bool IsActive { get; set; }
        public IList<ChildNavigationModel> ChildList;
    }
    public class ChildNavigationModel
    {
        public int NavigationItemId { get; set; }
        public string NavigationItem { get; set; }
        public bool IsActive { get; set; }
    }
}