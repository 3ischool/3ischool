﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Common
{
    public partial class CustomSelectListItem:SelectListItem
    {
        public string Data { get; set; }
    }
}