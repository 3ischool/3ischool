﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Subject
{
    public class TeacherSubjectClassModel
    {
        public TeacherSubjectClassModel()
        {
            AvailableTeachers = new List<SelectListItem>();
            AvailableSelectedSubjects = new List<SelectListItem>();
            AvailableSelectedClasses = new List<SelectListItem>();
            AvailableAllSubjects = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
        }

        public int TeacherId { get; set; }

        public IList<SelectListItem> AvailableTeachers { get; set; }

        public int[] SelectedSubjectIds { get; set; }

        public IList<SelectListItem> AvailableSelectedSubjects { get; set; }

        public int[] SelectedClassIds { get; set; }

        public IList<SelectListItem> AvailableSelectedClasses { get; set; }

        public int[] AllSubjectIds { get; set; }

        public IList<SelectListItem> AvailableAllSubjects { get; set; }

        public int[] AllClassIds { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }

        public bool IsAuthToAdd { get; set; }
    }


    public class TeacherClassSubAssignmentModel : TeacherSubjectClassModel
    {
        public TeacherClassSubAssignmentModel()
        {
            AvailableSubjects = new List<SelectListItem>();
        }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public bool IsUpdate { get; set; }

        public string EncTeacherSubjectAssignId { get; set; }

        public int TeacherSubjectAssignId { get; set; }

        public int ClassId { get; set; }

        public int VirtualId { get; set; }

        public string EncClassIdVirtualId { get; set; }

        public string Class { get; set; }

        public string EncSubjectId { get; set; }

        public string Subject { get; set; }

        public string EncTeacherId { get; set; }

        public string Teacher { get; set; }

        public int SubjectId { get; set; }

        public IList<SelectListItem> AvailableSubjects { get; set; }
    }
}