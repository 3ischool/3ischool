﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.App
{
    public class AppFormModel
    {
        public AppFormModel()
        {
            AvailableModule = new List<SelectListItem>();
            AvailableNavigationItem = new List<SelectListItem>();
            AvailableFormActionType = new List<SelectListItem>();
            AppFormActionList = new List<AppFormActionList>();
        }

        // App Form
        public int? AppFormId { get; set; }

        public string AppForm { get; set; }

        public int? Module { get; set; }

        public IList<SelectListItem> AvailableModule { get; set; }

        public int? AppFormDisplayOrder { get; set; }

        public int? NavigationItem { get; set; }

        public IList<SelectListItem> AvailableNavigationItem { get; set; }

        // App form Action
        public int? AppFormActionId { get; set; }

        public string AppFormAction { get; set; }

        public string AppFormActionInfo { get; set; }

        public int? FormActionType { get; set; }

        public IList<SelectListItem> AvailableFormActionType { get; set; }

        public int? AppFormActionDisplayOrder { get; set; }

        public IList<AppFormActionList> AppFormActionList { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

    }

    public class AppFormList
    {
        public string AppForm { get; set; }

        public int AppFormId { get; set; }

        public string Module { get; set; }

        public string DisplayOrder { get; set; }

        public string NavigationItem { get; set; }

        public bool IsDeleted { get; set; }

        public string EncAppFormId { get; set; }
    }

    public class AppFormActionList
    {
        public string AppForm { get; set; }

        public int AppFormActionId { get; set; }

        public string AppFormAction { get; set; }

        public int AppFormActionTypeId { get; set; }

        public string AppFormActionType { get; set; }

        public int DisplayOrder { get; set; }

        public string AppFormActionInfo { get; set; }

        public bool IsDeleted { get; set; }
    }

}