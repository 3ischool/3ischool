﻿//using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.ViewModel.Common;
using ViewModel.ViewModel.Staff;
namespace ViewModel.ViewModel.Messenger
{
    public class MessengerModel
    {
        public MessengerModel()
        {
            AvailableMessagePermisssions = new List<SelectListItem>();
            AvailableContacttype = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
            AvailableUsers = new List<SelectListItem>();
            messageslist = new List<MessagesListModel>();
            InboxMessages = new List<MessagesListModel>();
            deletedmessageslist = new List<MessagesListModel>();
            MessageHistory = new List<MessageHistory>();
            AvailableSendMessageMobileNoOptions = new List<SelectListItem>();
            AvailableSessions = new List<SelectListItem>();
        }
        public IList<MessagesListModel> messageslist { get; set; }

        public IList<MessagesListModel> InboxMessages { get; set; }

        public IList<MessagesListModel> deletedmessageslist { get; set; }

        public IList<SelectListItem> AvailableMessagePermisssions { get; set; }

        public IList<SelectListItem> AvailableContacttype { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }

        public IList<SelectListItem> AvailableUsers { get; set; }

        public IList<SelectListItem> AvailablePriority { get; set; }

        public IList<SelectListItem> AvailableSessions { get; set; }

        public IList<MessageHistory> MessageHistory { get; set; }

        public bool IsAdmin { get; set; }

        public bool StuSMS { get; set; }

        public bool StuNotification { get; set; }

        public bool StuNonAppUser { get; set; }

        public bool AdminSMS { get; set; }

        public bool AdminNotification { get; set; }

        public bool AdminNonAppUser { get; set; }

        public bool SMS { get; set; }

        public bool Notification { get; set; }

        public bool NonAppUser { get; set; }

        public bool NonStaffSMS { get; set; }

        public bool NonStaffNotification { get; set; }

        public bool NonStaffNonAppUser { get; set; }

        public bool GuardSMS { get; set; }

        public bool GuardNotification { get; set; }

        public bool GuardNonAppUser { get; set; }

        public string SMSSelectionStudentMobileNo { get; set; }

        public List<SelectListItem> AvailableSendMessageMobileNoOptions { get; set; }

        public int PriorityId { get; set; }
        public string Selectedvalue { get; set; }
        [AllowHtml]
        public string MessageTitle { get; set; }
        [AllowHtml]
        public string MessageDecription { get; set; }

        public int MessagePermissionId { get; set; }
        public int SessionId { get; set; }

        public int ContacttypeId { get; set; }
        public int UserTypeId { get; set; }
        public int SelectedClassId { get; set; }
        public int UserId { get; set; }

        public bool ischecked { get; set; }

        public bool selectedselection { get; set; }
        public bool allselection { get; set; }
        public string users { get; set; }
        public DateTime fromdate { get; set; }
        public DateTime todate { get; set; }
        public string defaultGridPageSize { get; set; }
        public string gridPageSizes { get; set; }
        public string SelectedUsersArray { get; set; }
        public MessagesListModel message { get; set; }
        public string MessageRefId { get; set; }
        public bool IsAuthToAddMessage { get; set; }
        public bool IsAuthToViewMessage { get; set; }
        public bool IsAuthToDeleteMessage { get; set; }
        public bool IsAuthToReplyMessage { get; set; }
        public bool IsAuthToUndoDeleteMessage { get; set; }
        public string EncUserTypeId { get; set; }
        public string EncUserId { get; set; }
        public string MessageIdArray { get; set; }

        public string Contacttype { get; set; }

        public string UserTypeFrom { get; set; }

        public string UserTypeTo { get; set; }

        public string[] MsgUserFrom { get; set; }

        public string[] MsgUserTo { get; set; }


    }

     public class SMSStudentInfoModel
     {
         public string StudentId { get; set; }
         public string StudentName { get; set; }
         public string HasAccount { get; set; }
         public string HasMobileNo { get; set; }
     }
    public class Studentjson{
        public Studentjson()
        {
            items = new List<Studentjson>();
        }
        public string id { get; set; }
        public string text { get; set; }
        
        public IList<Studentjson> items { get; set; }


    }
    public class Studentjson1
    {
       
        public string id { get; set; }
        public string text { get; set; }

     


    }
    public class SMSNotificationModel
    {
        public SMSNotificationModel()
        {
            AvailableMessagePermisssions = new List<SelectListItem>();
            AvailableContacttype = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
            AvailableUsers = new List<SelectListItem>();
            messageslist = new List<MessagesListModel>();
            InboxMessages = new List<MessagesListModel>();
            deletedmessageslist = new List<MessagesListModel>();
            MessageHistory = new List<MessageHistory>();
            AvailableLanguage = new List<CustomSelectListItem>();
            AvailableRecipientStudentType = new List<SelectListItem>();
            AvailableAttendanceStatus = new List<SelectListItem>();
            UserList = new List<dynamicusers>();
            SendMessageStudentsList = new List<SMSStudentInfoModel>();
            AvailableSendMessageMobileNoOptions = new List<SelectListItem>();
            AvailableSessions = new List<SelectListItem>();
            treeviewdata = new List<Studentjson>();
        }
        public string MessagePermission { get; set; }
        public List<Studentjson> treeviewdata { get; set; }
        public string treeviewdata1 { get; set; }
        public string treeviewdata2 { get; set; }
        public dynamic RecipientArray{ get; set; }
        public string ModuleName { get; set; }

        public bool IsFromStudentModule { get; set; }
        public bool HasMobileNo { get; set; }

        public bool HasAccount { get; set; }

        public int? ModuleId { get; set; }

        public string SendMessageMobileNoSelectionId { get; set; }

        public IList<SelectListItem> AvailableSendMessageMobileNoOptions { get; set; }

        public int RecipientStudentTypeId { get; set; }

        public string SMSSelectionStudentMobileNo { get; set; }

        public IList<SMSStudentInfoModel> SendMessageStudentsList { get; set; }

        public IList<dynamicusers> UserList { get; set; }

        public IList<MessagesListModel> messageslist { get; set; }

        public IList<MessagesListModel> InboxMessages { get; set; }

        public IList<MessagesListModel> deletedmessageslist { get; set; }

        public IList<SelectListItem> AvailableMessagePermisssions { get; set; }

        public IList<SelectListItem> AvailableContacttype { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }

        public IList<SelectListItem> AvailableUsers { get; set; }

        public IList<SelectListItem> AvailableRecipientStudentType { get; set; }

        public IList<SelectListItem> AvailableAttendanceStatus { get; set; }

        public IList<SelectListItem> AvailableSessions { get; set; }

        public string AttendanceStatus { get; set; }

        public string RecipientStudentType { get; set; }

        public IList<CustomSelectListItem> AvailableLanguage { get; set; }

        public IList<MessageHistory> MessageHistory { get; set; }
        public string Lang { get; set; }
        public string Language { get; set; }
        public int LanguageId { get; set; }
        public string Selectedvalue { get; set; }
        [AllowHtml]
        public string MessageTitle { get; set; }
        [AllowHtml]
        [Column(TypeName = "ntext")]
        public string MessageDecription { get; set; }

        public int MessagePermissionId { get; set; }

        public int ContacttypeId { get; set; }
        public int UserTypeId { get; set; }
        public int SelectedClassId { get; set; }

        public int[] ClassIds { get; set; }
        public int UserId { get; set; }

        public bool ischecked { get; set; }

        public bool selectedselection { get; set; }
        public bool allselection { get; set; }
        public string users { get; set; }
        public DateTime fromdate { get; set; }
        public DateTime todate { get; set; }
        public string defaultGridPageSize { get; set; }
        public string gridPageSizes { get; set; }
        public string SelectedUsersArray { get; set; }
        public MessagesListModel message { get; set; }
        public string MessageRefId { get; set; }
        public bool IsAuthToAddMessage { get; set; }
        public bool IsAuthToViewMessage { get; set; }
        public bool IsAuthToDeleteMessage { get; set; }
        public bool IsAuthToReplyMessage { get; set; }
        public bool IsAuthToUndoDeleteMessage { get; set; }

        public bool IsWebhookTrue{ get; set; }

        public bool IsAuthToViewBalance { get; set; }
        
        public string EncUserTypeId { get; set; }
        public string EncUserId { get; set; }
        public string MessageIdArray { get; set; }
        public int SessionId { get; set; }
        public string Contacttype { get; set; }

        public string UserTypeFrom { get; set; }

        public string UserTypeTo { get; set; }

        public string[] MsgUserFrom { get; set; }

        public string[] MsgUserTo { get; set; }

        public bool IsSMS { get; set; }

        public bool IsNotification { get; set; }

        public bool IsNonAppUserSMS { get; set; }

        public bool TOAbsentUsers { get; set; }

    }

    public class SelectedUserList
    {
        public SelectedUserList()
        {
            availableList = new List<SendSmsNotiSelectedUser>();
        }

        public string Class { get; set; }
        public string ClassId { get; set; }

        public List<SendSmsNotiSelectedUser> availableList { get; set; }
    }

    public class SendSmsNotiSelectedUser
    {
        public string Username { get; set; }
        public string Id { get; set; }
        public string HasAccount { get; set; }
        public string HasMobileNo { get; set; }
    }

    public class MessagePermission
    {
        public int MessagePermissionId { get; set; }

        public int ContactTypeId { get; set; }

        public int ReceipentTypeId { get; set; }

        public string ContactName { get; set; }
    }
    public class users
    {
        public int selecteduserid { get; set; }
    }
     public class user
    {
        public string selecteduserid { get; set; }
        public string contacttype { get; set; }
    }
    public class MessagesListModel
    {
        public int MessageId { get; set; }
        [AllowHtml]
        public string MessageDescription { get; set; }
        [AllowHtml]
        public string fullMessageDescription { get; set; }
        [AllowHtml]
        public string MessageTitle { get; set; }
        [AllowHtml]
        public string fullMessageTitle { get; set; }
        public DateTime MessagesTime { get; set; }
        public string classname { get; set; }
        public string sendernames { get; set; }
        public string fullsendernames { get; set; }
        public string messagesender { get; set; }
        public string MessageDatetime { get; set; }
        public DateTime? KendoMessageDatetime { get; set; }
        public string IOTSelection { get; set; }
        public bool Inactive { get; set; }
        public int SenderId { get; set; }
        public string users { get; set; }
        public DateTime? fromdate { get; set; }
        public DateTime? todate { get; set; }
        public string currentusername { get; set; }
        public string fullcurrentusername { get; set; }
        public string EncMessageId { get; set; }
        public string EncSenderId { get; set; }
        public string[] Recipientid { get; set; }
        public string[] SendersId { get; set; }
        public string sendertype { get; set; }

        public bool IsRead { get; set; }

        public bool CanReply { get; set; }

        public int UserFromType { get; set; }
        public int UserToType { get; set; }

        public string RecipientStudentType { get; set; }

        public int contacttype { get; set; }
    }

    public class MessageHistory
    {
        public string MessageId { get; set; }
        public string SenderId { get; set; }
        public string ReceiverId { get; set; }
        public string SenderName { get; set; }
        public string ReceiverName { get; set; }
        public string Messagetitle { get; set; }
        public string MessageTime { get; set; }
        public string MessageDescription { get; set; }
        public string MessageReceiver { get; set; }
        public string MessageSender{ get; set; }
        public string Class { get; set; }
        public string Studentname { get; set; }
        public string RollNo { get; set; }
    }
    public class ReplyMessageHistory
    {
        public string MessageId { get; set; }
        public string SenderName { get; set; }
        public string Message { get; set; }
        public string MessageTime { get; set; }
        public string Status { get; set; }
    }
    public class dynamicusers
    {
        public int UserId { get; set; }
        public int UserContactId { get; set; }
        public bool IsSMS { get; set; }
        public bool IsNotification { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string CreatedDate { get; set; }
        public int UserTypeId { get; set; }
        public string UserName { get; set; }
    }

    public class SMSAPIResponseStatus
    {
        public int MessageId { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string StatusCode { get; set; }
        public string PhoneNo { get; set; }

    }

    public class TriggerTemplateModel
    {
        public TriggerTemplateModel()
        {
            AvailableTriggerTypes = new List<SelectListItem>();
            AvailableTriggers = new List<SelectListItem>();
        }
        public int TriggerId { get; set; }
        public int TriggerTypeId { get; set; }
        public IList<SelectListItem> AvailableTriggerTypes { get; set; }
        public IList<SelectListItem> AvailableTriggers { get; set; }

    }

    public class MessageQueueModel      
    {
        public MessageQueueModel()
        {
            AvailableClass = new List<SelectListItem>();
            AvailableStudent = new List<SelectListItem>();
            AvailableStatus = new List<SelectListItem>();
        }
        public string defaultGridPageSize { get; set; }
        public string gridPageSizes { get; set; }

        public string EncMessageId { get; set; }
        public string EncStudentId { get; set; }
        public int? StudentId { get; set; }
        public string AdmnNo{ get; set; }
        public string UserName{ get; set; }
        //public string StaffName { get; set; }

        public string UserType { get; set; }
        public int? ClassId { get; set; }
        public string Class { get; set; }
        public string Message { get; set; }
        public string SentTime { get; set; }
        public DateTime? dtSentTime { get; set; }
        public int? Status { get; set; }
        public string DelieveryStatus { get; set; }
        public string DelieveryTime{ get; set; }
        public string SentFrom { get; set; }
        public string SentTo { get; set; }
        public DateTime? DatetimeSentFrom { get; set; }
        public DateTime? DatetimeSentTo { get; set; }
        public IList<SelectListItem> AvailableClass { get; set; }
        public IList<SelectListItem> AvailableStudent { get; set; }
        public IList<SelectListItem> AvailableStatus { get; set; }
    }


    public class MessageSettings
    {
        public MessageSettings()
        {
            MsgGroupList = new List<MsgGroupList>();
        }
        public IList<MsgGroupList> MsgGroupList { get; set; }
        public string PermArray { get; set; }
    }
    public class MsgGroupList
    {
        public MsgGroupList()
        {
            MsgPermissionList = new List<MsgPermissionList>();
        }
        public string MsgGroupName { get; set; }
        public int MsgGroupId { get; set; }
        public bool IsMsgGroupActive { get; set; }
        public IList<MsgPermissionList> MsgPermissionList { get; set; }
    }
    public class MsgPermissionList
    {
        public MsgPermissionList()
        {
            MsgRecipients = new List<MsgRecipients>();
        }
        public string MsgPermissionName { get; set; }
        public int MsgPermissionId { get; set; }
        public bool IsPermissionAllowed { get; set; }
        public List<MsgRecipients> MsgRecipients { get; set; }
    }

    public class MsgRecipients
    {
        public string RecipientName { get; set; }
        public int RecipientId { get; set; }
        public int MsgGroupPermissionId { get; set; }
        public bool IsReceipientActive { get; set; }
    }

  

    public class MessageGroupPermission
    {

        public int MsgPermissionId { get; set; }
        public int MsgGroupId { get; set; }

        public int RecipientId { get; set; }

        public int MsgGroupPermissionId { get; set; }

        public bool IsActive { get; set; }

    }

    public class SMSCustomGroupModel
    {
        public SMSCustomGroupModel()
        {
            AvailableSmsCustomGroupList = new List<SelectListItem>();
            ContactTypeList = new List<SelectListItem>();
            UserList = new List<UserDetail>();
        }
        public string defaultGridPageSize { get; set; }
        public string gridPageSizes { get; set; }
        public int SMSCustomGroupId { get; set; }
        public string SMSCustomGroupName { get; set; }
        public bool IsActiveSMSCustomGroup { get; set; }
        public int? SMSCustomGroupRecipientId { get; set; }
        public string MobileNo { get; set; }
        public string RecipientName { get; set; }
        public string RecipientTitle { get; set; }
        public string strContactType { get; set; }
        public string StaffImagepath { get; set; }
        public string RecipientImage { get; set; }
        public int? UserId { get; set; }
        public int? ContactType { get; set; }
        public IList<SelectListItem> ContactTypeList { get; set; }
        public IList<UserDetail> UserList { get; set; }
       
        public IList<SelectListItem> AvailableSmsCustomGroupList { get; set; }
    }
    public class UserDetail
    {
        public string Username { get; set; }
        public string ContactType { get; set; }
        public int? UserId { get; set; }
        public int? ContactId { get; set; }
        public string AdmsnNo { get; set; }
        public string Class { get; set; }
        public string RollNo { get; set; }
        public string GuardianName { get; set; }
        public string EmpCode { get; set; }
        public string GuardianStudentName { get; set; }
        public string GuardianStudentClass { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string GuardianRelation { get; set; }
        public string ImagePath { get; set; }
        public bool IsAlreadyAdded { get; set; }
    }
}