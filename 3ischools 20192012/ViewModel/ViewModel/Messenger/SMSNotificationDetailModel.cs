﻿//using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Messenger
{
    public class SMSNotificationDetailModel
    {
        public SMSNotificationDetailModel()
        {
            AvailableStatus = new List<SelectListItem>();
        }
        public string gridPageSizes { get; set; }
        public string defaultGridPageSize { get; set; }
        public bool IsAdmin { get; set; }
        public bool StuSMS { get; set; }
        public bool StuNotification { get; set; }
        public bool StuNonAppUser { get; set; }
        public bool AdminSMS { get; set; }
        public bool AdminNotification { get; set; }

        public string EncMessageId { get; set; }
        public string  Name  { get; set; }
        public string  Status { get; set; }
        public int? StatusId { get; set; }
        public string PhoneNo { get; set; }
        public string DeleiverTime { get; set; }
        public IList<SelectListItem> AvailableStatus { get; set; }
        public int Sent { get; set; }
        public int DeleiverCount { get; set; }
        public int RejectedCount { get; set; }
        public int FailedCount { get; set; }
        public int OptOut { get; set; }
        public string MessageDesc { get; set; }
        public string MessageTime { get; set; }
    }
}