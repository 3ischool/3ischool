﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ViewModel.ViewModel.Fee
{
    public class ManageFeePeriod
    {
        public ManageFeePeriod()
        {
            FeeFrequencyList = new List<SelectListItem>();
            AvailableSession = new List<SelectListItem>();
        }
        public int? FeeFrequencyId { get; set; }
        public string PrintText { get; set; }

        public List<SelectListItem> FeeFrequencyList { get; set; }

        public bool IsAllFeePeriodAdded { get; set; }

        public IList<SelectListItem> AvailableSession { get; set; }

        public int? SessionId { get; set; }

        public bool IsAuthToAdd { get; set; }
    }

    public class FeePeriodDetailList
    {
        public int FeePeriodDetailId { get; set; }
        public string FeeDesc { get; set; }

        public string FeeFreq { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public string PrintText { get; set; }
        public bool isDeleteable { get; set; }
    }
}