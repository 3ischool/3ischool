﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Fee
{
    public class ReceiptTypeFee
    {
        public ReceiptTypeFee() {

            AvailableReceiptSeriesRef = new List<SelectListItem>();
        }
        public string receiptType { get; set; }

        public string ExtraPrintText { get; set; }

        public int receiptSeries { get; set; }

        public bool linkStudentAddon { get; set; }

        public bool tptFee { get; set; }

        public bool active { get; set; }

        public bool pendingFee { get; set; }

        public bool IsMiscFee { get; set; }

        public string Isdefault { get; set; }

        public bool includeHeader { get; set; }

        public bool newStudenrtFee { get; set; }

        public bool groupFeeHead { get; set; }

        public bool defaultSchoolFee { get; set; }

        
        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToDelete { get; set; }



        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string  FilterReceiptTypeId { get; set; }
        
        public int? receiptId { get; set; }

        public int? ReceiptTypeId { get; set; }


        public string Receipt { get; set; }

        public IList<SelectListItem> AvailableReceiptTypeFilter { get; set; }

        public string receiptTemplate { get; set; }

        public bool IsReceiptSeries { get; set; }

        public IList<SelectListItem> AvailableReceiptSeriesRef { get; set; }

        public int ReceiptSeriesRefId { get; set; }

        public string RefReceiptSeries { get; set; }
    }
}