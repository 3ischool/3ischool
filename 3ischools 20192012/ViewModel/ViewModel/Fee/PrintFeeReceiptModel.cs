﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.Fee
{
    public class PrintFeeReceiptModel : StudentFeeReceiptModel
    {
        public PrintFeeReceiptModel()
        {
            PrintFeeReceiptHeadsList = new List<PrintFeeReceiptHeads>();
        }

        public bool IsAuthPrint { get; set; }

        public string NoofBreaksbefore { get; set; }
        public string NoofBreaksAfter { get; set; }
        public string lowerDivHeight { get; set; }
        public string UpperDivHeight { get; set; }
        public string SchoolHeader { get; set; }

        public string SessionName { get; set; }

        public string RollNo { get; set; }

        public string PaymentMode { get; set; }

        public string BankBranch { get; set; }

        public string ChequeNo { get; set; }

        public string ChequeDate { get; set; }

        public string TransactionNo { get; set; }

        public string TransactionDate { get; set; }

        public string SubTotal { get; set; }

        public string TotalDiscount { get; set; }

        public string TotalDiscountRemarks { get; set; }

        public string TotalAmount { get; set; }

        public string PaidAmount { get; set; }

        public string SchoolLogo { get; set; }

        public string SchoolContactNo { get; set; }

        public IList<PrintFeeReceiptHeads> PrintFeeReceiptHeadsList { get; set; }

        public string CurrencyIcon { get; set; }

        public string Balance { get; set; }

        public string UserName { get; set; }

        public string Header1 { get; set; }

        public string Header2 { get; set; }

        public bool Isheaderrequired { get; set; }

        public bool FeereceiptHalfPageView { get; set; }

        public string HeaderHeight { get; set; }

        public bool PrintLandScapeOrPortrait { get; set; }

        public bool ShowReceiptType { get; set; }

        public bool ShowCopyText { get; set; }

        public string ReceiptType { get; set; }

        public string ReceiptTypeExtraText { get; set; }

        public string ReceiptTemplate { get; set; }
        public string RegistrationNo { get; set; }
        public bool IsPrintSchoolCopy { get; set; }

        public bool IsPrintStudentCopy { get; set; }

        public string Remarks { get; set; }

        public string  ShowRemarks { get; set; }

        public string Modetype { get; set; }
        public string lblChkDDNAme { get; set; }
        public string lblChkDDDate { get; set; }

        public string Affiliationboard { get; set; }
        public string AffiliationNo { get; set; }
        public string DisplayAddress { get; set; }
        public string DisplayPhone { get; set; }

        public string TotalPayable { get; set; }
        public string TotalPaidAmount { get; set; }
        public string TotalBalance { get; set; }
        public string TotalExcessAmount { get; set; }

        public bool ShowlimitedDetail { get;set;}
        public bool ShowBalance { get; set; }
        public bool IsCancel { get; set; }
        public string CancelWaterMarktext { get; set; }

        public bool ConsolidatedFeeReceiptPrint { get; set; }
        public bool regno_FeeReceiptPrint { get; set; }
        public string TotalPaidAmountInWords { get; set; }
        public string StudentAddress { get; set; }

        public bool OneFourthPageSetUp { get; set; }

        public string Category { get; set; }
        public string FileName { get; set; }

        public bool IsApp { get; set; }

    }

    public class FeeReceiptCertificate
    {
        public string ReceiptNo { get; set; }
        public string HeadName { get; set; }
        public string HeadAmount { get; set; }
    }


    public class PrintFeeReceiptHeads
    {
        public string HeadName { get; set; }

        public string HeadAmount { get; set; }

        public string Deduction { get; set; }

        public string PayableAmount { get; set; }

        public string PaidAmount { get; set; }

        public string Balance { get; set; }

        public string ExcessAmount { get; set; }

    }
}