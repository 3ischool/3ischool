﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Fee
{
    public class TransportFeeCollectionModel
    {
        public TransportFeeCollectionModel()
        {
            AvailableRoutes = new List<SelectListItem>();
        }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToPrint { get; set; }

        public bool IsAuthToDelete { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public string RouteId { get; set; }

        public IList<SelectListItem> AvailableRoutes { get; set; }


    }

    public class TransportFeeInfolist
    {
        public int RouteId { get; set; }
        public string Route { get; set; }
        public decimal Payable { get; set; }
        public decimal Received { get; set; }
        public decimal Pending { get; set; }
    }

    public class TransportRoutewiseList
    {
        public TransportRoutewiseList()
        {
            StudentDetail = new List<StudentinfoDetail>();
        }
        public string RouteId { get; set; }
        public string RouteName { get; set; }
        public IList<StudentinfoDetail> StudentDetail { get; set; }
    }
    public class StudentinfoDetail
    {
        public StudentinfoDetail()
        {
            FeeList = new List<MonthlyFeeList>();
        }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string Class { get; set; }
        public IList<MonthlyFeeList> FeeList { get; set; }
    }
    public class MonthlyFeeList
    {
        public string FeePeriod { get; set; }
        public decimal Payable { get; set; }
        public decimal Received { get; set; }
        public decimal Pending { get; set; }
    }
}