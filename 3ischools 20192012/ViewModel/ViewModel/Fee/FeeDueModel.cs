﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Fee
{
    public class FeeDueModel
    {
        public  FeeDueModel() {

            FeedueStudentList = new DataTable();
            AvailableClasses = new List<SelectListItem>();
            AvailableSendMessageMobileNoOptions = new List<SelectListItem>();
        }
        public DataTable FeedueStudentList { get; set; }
        public string StudentIdList { get; set; }
        public IList<SelectListItem> AvailableClasses { get; set; }
        public int[] ClassId { get; set; }
        public IList<SelectListItem> AvailableSendMessageMobileNoOptions { get; set; }
        public string SMSSelectionStudentMobileNo { get; set; }
        public string SendMessageMobileNoSelectionId { get; set; }
    }
    public class Feeduestudentlist {

        public string FName { get; set; }
        public string LName { get; set; }
        public string AdmnNo { get; set; }
        public string Class { get; set; }
        public string Rollno { get; set; }
        public string StudentId { get; set; }


    }
    public class StudentlistModel
    {
        public int StudentId { get; set; }

        public string StudentPendingFee { get; set; }
    }
}