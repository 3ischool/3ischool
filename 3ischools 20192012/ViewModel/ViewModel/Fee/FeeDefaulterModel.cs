﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Fee
{
    public class FeeDefaulterModel
    {
        public FeeDefaulterModel()
        {
            FeeDefaulterList = new List<FeeDefaulterModel>();
            AvailableClasses = new List<SelectListItem>();
            AvailableFeePeriods = new List<SelectListItem>();
            AvailableReceipttype = new List<SelectListItem>();
            AvailableFeeClassGroupTypes = new List<SelectListItem>();
            AvailableSessions = new List<SelectListItem>();
            AvailableFeeFrequency = new List<SelectListItem>();
            FeeReceiptDetailList = new List<FeeReceiptDetailModel>();
            AvailableClassesExport = new List<SelectListItem>();
        }
        public  IList<FeeReceiptDetailModel> FeeReceiptDetailList { get; set; }
        public string Class { get; set; }

        public int ClassId { get; set; }
        public string StudentPendingFee { get; set; }
        
        public IList<SelectListItem> AvailableFeeFrequency { get; set; }
        public IList<SelectListItem> AvailableClasses { get; set; }
        public IList<SelectListItem> AvailableClassesExport { get; set; }
        public IList<SelectListItem> AvailableSessions { get; set; }
        public int SessionId { get; set; }
        public int FeePeriodId { get; set; }
        public int FeeClassGroupTypeId  { get; set; }

        public IList<SelectListItem> AvailableFeePeriods { get; set; }
        public IList<SelectListItem> AvailableFeeClassGroupTypes { get; set; }
        

        public int ReceiptTypeId { get; set; }

        public int FeeFrequencyId { get; set; }

        public IList<SelectListItem> AvailableReceipttype { get; set; }

        public int StudentId { get; set; }

        public decimal Amount { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal PayableAmount { get; set; }

        public decimal TotalPayableAmount { get; set; }

        public string StudentName { get; set; }

        public string AdmissionNo { get; set; }

        public string RollNo { get; set; }

        public string FatherName { get; set; }

        public string ContactNo { get; set; }

        public IList<FeeDefaulterModel> FeeDefaulterList { get; set; }

        public int FeeTypeId { get; set; }

        public int CurrentPeriodId { get; set; }

        public string CurrencyIcon { get; set; }

        public DateTime CurrentDate { get; set; }

        public bool IsAuthToExport { get; set; }

        public bool IsAuthToPrint { get; set; }

        public bool IsAuthToView { get; set; }

        public bool IsExportToExcel { get; set; }

        public string SMSSelectionStudentMobileNo { get; set; }

        public string StudentAddress { get; set; }
        public string RouteNo { get; set; }
        public string StudentCategory { get; set; }
    }

    public class BookDefaulter
    {

    }
    public class BookDefaulterList
    {
        public int BookId { get; set; }
        public string BookTitle { get; set; }
        public string BookAbbr { get; set; }
        public string SubTitle { get; set; }
        public string ISBN { get; set; }
        public string Description { get; set; }
        public int MaterialTypeId { get; set; }
        public string MaterialType { get; set; }
        public string AvialableCopies { get; set; }
        public string AccessionNo { get; set; }
    }
    public class FeeDefaulter
    {
    }
    public class FeeDefaulterList
    {
        public string Class { get; set; }
        public int ClassId { get; set; }
        public decimal Amount { get; set; }
        public string StudentName { get; set; }
        public string AdmissionNo { get; set; }
        public string RollNo { get; set; }
        public int StudentId { get; set; }
        public int FeeTypeId { get; set; }
        public int CurrentPeriodId { get; set; }
        public decimal PayableAmount { get; set; }

    }

    public class FeeCollectionListmodel
    {
        public int ReceiptId { get; set; }
        public string RollNo { get; set; }
        public string AdmissionNo { get; set; }
        public string StudentName { get; set; }

        public string DOJ { get; set; }

        public string PaymentDate { get; set; }

        public string BankName { get; set; }
        public bool IsHostler { get; set; }
        public string RecieptType { get; set; }
        public string Class { get; set; }
        public string PaymentMethod { get; set; }
        public string ChequeNo { get; set; }
        public string Details { get; set; }
        public decimal Amount { get; set; }
        public bool IsOld { get; set; }
        public int ReceiptNo { get; set; }

        public string Session { get; set; }

        public string FeePeriod { get; set; }
    }

    public class StudentList
    {
        public string Class { get; set; }
        public int ClassId { get; set; }
        public decimal Amount { get; set; }
        public string StudentName { get; set; }
        public string AdmissionNo { get; set; }
        public string RollNo { get; set; }
        public int StudentId { get; set; }
        public int FeeTypeId { get; set; }
        public int CurrentPeriodId { get; set; }
        public decimal PayableAmount { get; set; }
        public decimal MonthlyAmount { get; set; }

    }
}