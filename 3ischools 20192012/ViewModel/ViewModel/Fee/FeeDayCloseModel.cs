﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.Fee
{
    public class FeeDayCloseModel
    {
        public FeeDayCloseModel()
        {
            ReceiptTypeList = new List<ReceiptTypeList>();
        }
        public string UserName { get; set; }
        public List<ReceiptTypeList> ReceiptTypeList { get; set; }
        public decimal TotalDeposit { get; set; }
        public decimal TotalPayable { get; set; }
    }

    public class ReceiptTypeList
    {
        public ReceiptTypeList()
        {
            PreviousList = new List<BeforePeriodList>();
            CurrentList = new List<DuringPeriodList>();
        }
        public int ReceiptTypeId { get; set; }
        public string ReceiptType { get; set; }
        public string BeforeHeadText { get; set; }
        public string AfterHeadtext { get; set; }
        public List<BeforePeriodList> PreviousList { get; set; }
        public List<DuringPeriodList> CurrentList { get; set; }
      
    }

    public class BeforePeriodList
    {
        public int? UserId { get; set; }
        public int? ReceiptTypeId { get; set; }
        public int? PaymentModeId { get; set; }
        public string PaymentMode { get; set; }
        public string  HeadType { get; set; }
        public decimal Collected { get; set; }
        public decimal Deposited { get; set; }
        public decimal Balance { get; set; }
    }
    public class DuringPeriodList
    {
        public int? UserId { get; set; }
        public int? ReceiptTypeId { get; set; }
        public int? PaymentModeId { get; set; }
        public string PaymentMode { get; set; }
        public string HeadType { get; set; }
        public decimal PrevBalance { get; set; }
        public decimal Collected { get; set; }
        public decimal Payable { get; set; }
        public decimal Deposited { get; set; }
        public decimal Balance { get; set; }
       
    }
}