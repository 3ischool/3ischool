﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Fee
{
    public class ManageLateFee : IDisposable
    {
        public ManageLateFee()
        {
            LateFeeTypeList = new List<SelectListItem>();
            ReceiptTypeList = new List<SelectListItem>();
        }
        public int? LateFeeId { get; set; }

        public int? LateFeeTypeId { get; set; }

        public string LateFeeType { get; set; }
        public int? ReceiptTypeId { get; set; }
        public string ReceiptType { get; set; }
        public IList<SelectListItem> LateFeeTypeList { get; set; }
        public IList<SelectListItem> ReceiptTypeList { get; set; }
        public decimal? FromDays { get; set; }
        public bool IsMonthLastDay { get; set; }

        public string ToDays { get; set; }

        public decimal? LateFeeAmount { get; set; }

        public string CurrencyIcon { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToDelete { get; set; }

        public bool IsAuthToEdit { get; set; }
        public string[] ReceiptTypeIds { get; set; }

        public bool IsPreviousShow { get;set;}

        public bool IncludePrevious { get; set; }
        public string LateFeeText { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }

    public class LateFeeList:IDisposable
    {
        public LateFeeList()
        {
            LateFeeeList = new List<ManageLateFee>();
        }
        public List<ManageLateFee> LateFeeeList { get; set; }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}