﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Fee
{
    public class FeeReceiptModel
    {
        public FeeReceiptModel()
        {
            AvailablePaymentMode = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
            AvailableStudents = new List<SelectListItem>();
            AvailableFeeType = new List<SelectListItem>();
            AvailableFeePeriod = new List<SelectListItem>();
            AvailableReceiptTypes = new List<SelectListItem>();
            AvailableSession = new List<SelectListItem>();
            AvailableSendMessageMobileNoOptions = new List<SelectListItem>();
            AvailableStandard = new List<SelectListItem>();
        }
        public bool CombinedVouchers { get; set; }
        public bool UseConsession { get; set; }
        public string FeePeriodIdArray { get; set; }
        public int? FeeReciptId { get; set; }

        public List<SelectListItem> AvailableSendMessageMobileNoOptions { get; set; }

        public int? FeeReciptNo { get; set; }

        public bool SMS { get; set; }

        public bool NonAppUser { get; set; }

        public bool Notification { get; set; }

        public DateTime? FeeReciptDate { get; set; }

        public DateTime? FeePaymentDate { get; set; }

        public string SMSSelectionStudentMobileNo { get; set; }

        public string SendMessageMobileNoSelectionId { get; set; }

        public int? PaymentModeId { get; set; }

        public IList<SelectListItem> AvailablePaymentMode { get; set; }

        public int? ClassId { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }
        public IList<SelectListItem> AvailableStandard { get; set; }
        public int? StudentId { get; set; }

        public int? admnStudentId { get; set; }

        public IList<SelectListItem> AvailableStudents { get; set; }

        public int? FeeTypeId { get; set; }

        public IList<SelectListItem> AvailableFeeType { get; set; }

        public int? FeePeriodId { get; set; }

        public IList<SelectListItem> AvailableFeePeriod { get; set; }

        public decimal? PaidAmount { get; set; }

        public string groupfeeheadarray { get; set; }

        public bool IsReceiptDateEnable { get; set; }

        public bool IsReceiptNoEnable { get; set; }

        public string ChequeNo { get; set; }

        public DateTime? ChequeDate { get; set; }

        public string BankTransactionNo { get; set; }

        public DateTime? BankTransactionDate { get; set; }

        public string BankName { get; set; }

        public bool IsAuthToAdd { get; set; }

        public string CurrencyIcon { get; set; }

        public int? ChequeNoLength { get; set; }

        public string AdmissionNo { get; set; }

        public bool ClassNotAssigned { get; set; }

        public int StandardId { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        public string EncFeeReceiptId { get; set; }

        public IList<SelectListItem> AvailableReceiptTypes { get; set; }

        public int[] ReceiptTypeIds { get; set; }
        public int RecieptTypeId { get; set; }

        public bool IsDiscount { get; set; }

        public IList<SelectListItem> AvailableSession { get; set; }

        public int? SessionId { get; set; }

        public string SessionStartDate { get; set; }

        public bool IsForPreviousSession { get; set; }

        public bool Isfromadmission { get; set; }

        public string Headid { get; set; }

        public IList<SelectListItem> AvailableMiscHeads { get; set; }

        public bool IsInstallmentEnabled { get; set; }

        public string InstallmentData { get; set; }

        public bool IsAuthToPrint { get; set; }

        public string Remarks { get; set; }

        public string ModeType { get; set; }

        public bool IsReceiptNoShow { get; set; }

        public bool IsMisc { get; set; }

        public int ReceiptTypeSchoolFeeValue { get; set; }

        public decimal? PendingAmount { get; set; }
        public bool LateFeeApply { get; set; }
    }
    public class VouchersModel
    {
        public VouchersModel()
        {
            VouchersFeeHeadsList = new List<List<FeeReceiptDetailModel>>();
            VouchersCopyType = new List<string>();
        }
        public List<List<FeeReceiptDetailModel>> VouchersFeeHeadsList { get; set; }
        public IList<string> VouchersCopyType { get; set; }
        public string BankName { get; set; }
        public string BankAccountNo { get; set; }
        public string BankIfsCode { get; set; }
        public string BankAddress { get; set; }
        public string FromPayableDate { get; set; }
        public string ToPayableDate { get; set; }
        public string Timming { get; set; }
        public string AdmissionNo { get; set; }
       
        public string StudentName { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string Class { get; set; }
        public string RollNo { get; set; }
        public string VoucherFooter1 { get; set; }
        public string VoucherFooter2 { get; set; }
        public string SchoolName { get; set; }
        public string SchoolAddress { get; set; }
    }
    public class FeeReceiptDetailModel
    {
        public int? GroupFeeHeadId { get; set; }

        public int? GroupFeeHeadDetailId { get; set; }

        public string GroupFeeHeadName { get; set; }

        public decimal? Amount { get; set; }

        public decimal? TotalAmount { get; set; }

        public decimal? PayableAmount { get; set; }

        public string HeadType { get; set; }

        public int PendingFeePeriodId { get; set; }

        public int LateFeePeriodId { get; set; }

        public bool IsLateFee { get; set; }

        public bool IsDiscount { get; set; }

        public bool IsConsession { get; set; }
        public string FeePeriod { get; set; }
        public bool IsHeadWise { get; set; }

        public decimal? TotalHeadAmount { get; set; }
        public decimal? CalculatedHeadAmount {get;set;}

        public decimal? FeeHeadConcession { get; set; }

        public decimal? EWSDiscount { get; set; }

        public Boolean ShowEWS { get; set; }

        public Boolean ShowHeadConcession { get; set; }

        public int FeeperiodId { get; set; }

        public decimal? FinalTotalAmount { get; set; }
        public string FromPayableDate { get; set; }
        public string ToPayableDate { get; set; }
        public int periodwiseReceiptId { get; set; }
        public int Feeperiodindex { get; set; }
    }

    public class PaymentOnlineGateWay
    {
        public string PaymentGatewayId { get; set; }

        public string PaymentGatewayName { get; set; }

        public string PaymentGatewayCode { get; set; }

        public string PGImage { get; set; }

        public string PgModuleUrl { get; set; }

        public bool IsDefault { get; set; }

        public decimal MinimumTransferAmt { get; set; }
    }

    public class StudentFeeReceiptModel
    {
        public int PaymentModeId { get; set; }
        public int FeeReceiptId { get; set; }
        public int CompareFeeReceiptId { get; set; }
        public string FeeTypeId { get; set; }

        public string PaidAmount { get; set; }

        public string CarryForwardFees { get; set; }

        public string AdmnNo { get; set; }

        public string EncFeePeriodId { get; set; }

        public string ReceiptNo { get; set; }

        public bool IsPayOnline { get; set; }

        public int RcptNo { get; set; }

        public string ReciptDate { get; set; }

        public DateTime RcptDate { get; set; }

        public string FeeType { get; set; }

        public string PaidFee { get; set; }

        public decimal FeePaid { get; set; }

        public string PendingFee { get; set; }

        public decimal FeePending { get; set; }

        public string PayableFee { get; set; }

        public string FeePeriod { get; set; }

        public string FeePeriodText { get; set; }

        public string EncFeeReceiptId { get; set; }

        public string EncStudentId { get; set; }


        public string StudentName { get; set; }
        public string StudentNameonly { get; set; }

        public string FatherName { get; set; }

        public string ClassName { get; set; }

        public decimal WaivedoffAmount { get; set; }

        public string ReceiptType { get; set; }

        public bool IsAuthToDelete { get; set; }

        public bool IsAuthToView { get; set; }

        public bool IsAuthToPrint { get; set; }
        public string PaymentMode { get; set; }
        public string Class { get; set; }

        public string Session { get; set; }

        public string StudentFee { get; set; }
        public string TransportFee { get; set; }

        public bool IsAuthToCancel { get; set; }

        public bool Iscancel { get; set; }

        public bool IsPrinted { get; set; }
    }

    public class FeeReceiptSubmitArrayModel
    {
        public int? GroupHeadIdOrConsessionId { get; set; }

        public string HeadType { get; set; }

        public decimal Amount { get; set; }

        public int? PendingFeePeriodId { get; set; }

        public int? LateFeePeriodId { get; set; }

        public decimal WaveOffAmount { get; set; }

        public int? SessionId { get; set; }

        public string Remarks { get; set; }

        public decimal PaidAmount { get; set; }

        public decimal? EWSAmount { get; set; }

        public decimal? FeeHeadConcession { get; set; }

        public int FeePeriodId { get; set; }

        public decimal? ExcessAmount { get; set; }

        public int ConsessionId { get; set; }

        public int periodwiseReceiptId { get; set; }
    }

    public class FeeReceiptDetailView
    {
        public string HeadName { get; set; }

        public decimal Amount { get; set; }

        public bool IsHead { get; set; }
    }

    public class IntallmentDetail
    {
        public string Date { get; set; }

        public string Amount { get; set; }
    }

    public class FeeConcessionModel
    {
        public string gridPageSizes { get; set; }
        public string defaultGridPageSize { get; set; }

        public string ConcessionAmount  { get; set; }

        public string Consession { get; set; }

        public string RecieptNo { get; set; }

        public string Feehead { get; set; }

        public string Feeperiod { get; set; }
    }





}