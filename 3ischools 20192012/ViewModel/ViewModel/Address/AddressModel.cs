﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Address
{
    public class AddressModel
    {
        public AddressModel()
        {
            AvailableAddresstype = new List<SelectListItem>();
            AvailableCities = new List<SelectListItem>();
            AddressCityModel = new AddressCityModel();
            AddressModelList = new List<AddressModel>();
        }

        public int? AddressId { get; set; }

        public int? ContactId { get; set; }

        [Required(ErrorMessage = "Address Type required")]
        public int AddressTypeId { get; set; }

        public int? ContactTypeId { get; set; }

        [Required(ErrorMessage = "PlotNo required")]
        public string PlotNo { get; set; }

        [Required(ErrorMessage = "Sector/Street required")]
        public string Sector_Street { get; set; }

        [Required(ErrorMessage = "Locality required")]
        public string Locality { get; set; }

        public string Landmark { get; set; }

        [Required(ErrorMessage = "City required")]
        public int Address_CityId { get; set; }

        [Required(ErrorMessage = "Zip required")]
        [RegularExpression(@"\d{6}", ErrorMessage = "Invalid Zipcode")]
        public string Zip { get; set; }

        public IList<SelectListItem> AvailableAddresstype { get; set; }

        public IList<SelectListItem> AvailableAddresstypeForGuardian { get; set; }

        public IList<SelectListItem> AvailableCities { get; set; }

        public AddressCityModel AddressCityModel { get; set; }

        public string AddressType { get; set; }

        public string AddressCity { get; set; }
        public string AddressDistrict { get; set; }
        public IList<AddressModel> AddressModelList { get; set; }
        public bool IsauthToAdd { get; set; }

        public string EncAddressId { get; set; }

        public string State { get; set; }

        public string AddressUpdated { get; set; }
    }
}