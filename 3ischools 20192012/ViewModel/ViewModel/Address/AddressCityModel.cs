﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Address
{
    public class AddressCityModel
    {
        public AddressCityModel()
        {
            AvailableStates = new List<SelectListItem>();
            AddressStateModel = new AddressStateModel();
        }

        public int? CityId { get; set; }

        public int? City_StateId { get; set; }

        public string CityName { get; set; }

        public IList<SelectListItem> AvailableStates { get; set; }

        public AddressStateModel AddressStateModel { get; set; }
    }
}