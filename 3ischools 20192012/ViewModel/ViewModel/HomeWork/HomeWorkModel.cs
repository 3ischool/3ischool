﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.HomeWork
{
    public class HomeWorkModel
    {
        public HomeWorkModel()
        {
            AvailableHomwWorkType = new List<SelectListItem>();
            AvailableClassList = new List<CustomSelectClassitem>();
            AvailableSubjectList = new List<SelectListItem>();
            AvailableTeacherList = new List<SelectListItem>();
            HWList = new List<HomeWorkModel>();
        }

        public int HomeWorkTypeId { get; set; }

        public int ClassId { get; set; }

        public string InchargedClassName { get; set; }

        public int SubjectId { get; set; }

        public string Description { get; set; }

        public string Remarks { get; set; }

        public string ShortDescription { get; set; }

        public string ShortRemarks { get; set; }

        public string SMSSelectionStudentMobileNo { get; set; }

        public bool? IsAllowHWBackDateEntry { get;set;}
        public bool? IsAllowHWDescription { get; set; }
        
        public bool? IsAllowHWFutureDateEntry { get; set; }

        public string minDate { get; set; }

        public string maxDate { get; set; }

        public string SendMessageMobileNoSelectionId { get; set; }

        public string Date { get; set; }

        public DateTime? SessionStartDate { get; set; }

        public DateTime? SessionEndDate { get; set; }

        public IList<SelectListItem> AvailableHomwWorkType { get; set; }

        public IList<CustomSelectClassitem> AvailableClassList { get; set; }

        public IList<SelectListItem> AvailableSubjectList { get; set; }

        public IList<SelectListItem> AvailableTeacherList { get; set; }

        public bool IsAuthToAddHomeWork { get; set; }

        public int HWId { get; set; }

        public int ClassSubjectId { get; set; }

        public int StaffId { get; set; }

        public string ClassName { get; set; }

        public string HomeWorkType { get; set; }

        public string Subject { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public bool IsAuthToViewHomeWork { get; set; }

        public bool IsAuthToDeleteHomeWork { get; set; }

        public bool IsAuthToEditHomeWork { get; set; }

        public int HWDetailId { get; set; }

        public string HomeworkDetailId { get; set; }

        public string File { get; set; }

        public string FileName { get; set; }

        public bool  IsEditable{ get; set; }

        public bool IsDeleteable { get; set; }

        public string DocumentArray { get; set; }

        public IList<HomeWorkModel> HWList { get; set; }

        public bool IsTeacher { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsStudent { get; set; }

        public string Month { get; set; }

        public string IsDateormonth { get; set; }

        public string TeacherName { get; set; }

        public string ContactType { get; set; }

        public int SubId { get; set; }

        public bool IsAuthToDownload { get; set; }

        public int VirtualClassId { get; set; }

        public bool IsClassIncharge { get; set; }
    }

    public class CustomSelectClassitem {

        public string Text { get; set; }

        public string Value { get; set; }

        public string VirtualClassValue { get; set; }
        public bool Selected { get; set; }
    
    }
}