﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Schedular
{
    public class SchedularModel
    {
        public SchedularModel()
        {
            AvailableEventTypeList = new List<SelectListItem>();
            AvailableClassList = new List<SelectListItem>();
            imagelist = new List<SchedularImagesModel>();
            AvaiableContactTypes = new List<SelectListItem>();
            AvailableSendMessageMobileNoOptions = new List<SelectListItem>();
        }

        public bool IsAdmin { get; set; }

        public bool SMS { get; set; }

        public bool Notification { get; set; }

        public bool NonAppUser { get; set; }

        public bool StuSMS { get; set; }

        public bool StuNotification { get; set; }

        public bool StuNonAppUser { get; set; }

        public bool GuardSMS { get; set; }

        public bool GuardNotification { get; set; }

        public bool GuardNonAppUser { get; set; }

        public string SMSSelectionStudentMobileNo { get; set; }

        public IList<SelectListItem> AvailableSendMessageMobileNoOptions { get; set; }

        public int EventId { get; set; }
        [AllowHtml]
        public string EventHeader { get; set; }
        public string EventTitle { get; set; }
        public string EventCode { get; set; }
        public string fullEventTitle { get; set; }
        public string ClassId { get; set; }
        public string ClassName { get; set; }
        public string fullClassName { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public string fullDescription { get; set; }
        public string StartDate { get; set; }
        public string StartTime { get; set; }
        public string EndDate { get; set; }
        public string EndTime { get; set; }
        public bool IsActive { get; set; }
        public string isActivekendo { get; set; }
        public string defaultGridPageSize { get; set; }
        public string gridPageSizes { get; set; }
        public string EventType { get; set; }
        public int EventTypeId { get; set; }
        public IList<SelectListItem> AvailableEventTypeList { get; set; }
        public IList<SelectListItem> AvailableClassList { get; set; }
        public string Venue { get; set; }
        public SchedularImagesModel imageslistmodel { get; set; }
        public IList<SchedularImagesModel> imagelist { get; set; }
        public bool IsAuthToAdd { get; set; }
        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToDelete { get; set; }
        public bool IsAuthToViewImages { get; set; }
        public string EncEventId { get; set; }
        public string EncEventTypeId { get; set; }
        public DateTime? SessionStartDate { get; set; }
        public DateTime? SessionEndDate { get; set; }
        public bool IsBackDateAllowed { get; set; }
        public string CircularDocCaption { get; set; }
        public string CircularDocId { get; set; }
        public string CircularDocFileName { get; set; }
        public bool HasCircularDocs { get; set; }
        //public string ImageName { get; set; }
        //public int EventImageid { get; set; }
        //public DateTime? StartDate { get; set; }
        //public DateTime? EndDate { get; set; }
        public List<Int32> ClassIdArray { get; set; }
        public EventDetailModel EventDetailModel { get; set; }
        public bool IsStartDatePasses { get; set; }

        public DateTime? FilterEndDate { get; set; }
        public DateTime? FilterStartDate { get; set; }

        public bool IsAuthToDownloadDoc { get; set; }

        public string AllowedTo { get; set; }
        public string UserType { get; set; }

        public IList<SelectListItem> AvaiableContactTypes { get; set; }
    }
    public class EventDetailModel
    {
        public string ClassId { get; set; }
    }
    public class SchedularImagesModel
    {
        public SchedularImagesModel()
        {
            imagelist = new List<SchedularImagesModel>();
        }
        public string ImageCaption { get; set; }
        public int EventTypeid { get; set; }
        public int EventType { get; set; }
        public string EventImagepath { get; set; }
        public string ImageName { get; set; }
        public IList<SchedularImagesModel> imagelist { get; set; }
        public int EventImageid { get; set; }
        public bool IsAuthToAdd { get; set; }
        public bool IsAuthToDelete { get; set; }
        public string EncEventid { get; set; }
        public string EncEventImageid { get; set; }
        public string EncEventTypeid { get; set; }
        public string EventCode { get; set; }
    }


    public class SchedularDocumentModel : SchedularModel
    {
        public SchedularDocumentModel()
        {
        }
        public string DocumentCaption { get; set; }
        public int EventTypeid { get; set; }
        public int EventType { get; set; }
        public string EventDocpath { get; set; }
        public string DocumentName { get; set; }
        public int EventImageid { get; set; }
        public bool IsAuthToAdd { get; set; }
        public bool IsAuthToDelete { get; set; }
        public string EncEventid { get; set; }
        public string EncEventImageid { get; set; }
        public string EncEventTypeid { get; set; }
    }
}