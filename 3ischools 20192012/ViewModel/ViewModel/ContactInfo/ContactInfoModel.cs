﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.ContactInfo
{
    public class ContactInfoModel
    {
        public ContactInfoModel()
        {
        }

        public int? ContactInfoId { get; set; }

        public string ContactInfo { get; set; }

        public string status1 { get; set; }

        public string statusId { get; set; }

        public Nullable<int> ContactInfoTypeId { get; set; }

        public string ContactInfoType { get; set; }

        public Nullable<int> ContactId { get; set; }

        public Nullable<bool> Status { get; set; }

        public Nullable<bool> IsDefault { get; set; }

        public Nullable<int> ContactTypeId { get; set; }

        public string ContactToShow { get; set; }

    }

    public class ContactTypeModel
    {
        public ContactTypeModel()
        {
        }

        public int? ContactTypeId { get; set; }

        public string ContactType { get; set; }

        public bool? SignUpOTP { get; set; }

        public bool? ResetPwdOTP { get; set; }

    }
}