﻿using KSModel.Models;
using ViewModel.ViewModel.Address;
using ViewModel.ViewModel.Common;
using ViewModel.ViewModel.Fee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Student
{
    public class StudentProfileModel
    {
        public StudentProfileModel()
        {
            StudentParentInfolist = new List<StudentParentInfo>();
            StudentSiblingInfolist = new List<StudentSiblingInfo>();
            StudentDocumentInfolist = new List<StudentDocumentInfo>();
            TransportFacilityHistorylist = new List<TransportFacilityHistory>();
            IssueDocTypeList = new List<IssueDocTypeItem>();
            Sessionlist = new List<SelectListItem>();
            ExtraActivitylist = new List<SelectListItem>();
            StudentActivityList = new List<Studentactivity>();
            ClassList = new List<SelectListItem>();
            UserRoleList = new List<SelectListItem>();
            StudentClassDetailList = new List<StudentClassDetailModel>();
            StudentFeeAddonTypeList = new List<SelectListItem>();
            GroupFeeHeadList = new List<SelectListItem>();
            StudentFeeAddOnModelList = new List<StudentFeeAddOnModel>();
            StudentFeeReceiptList = new List<StudentFeeReceiptModel>();
            PreviousClassDetailList = new List<PreviousClassDetailModel>();
            StudentExtraClassDetailList = new List<StudentExtraClassDetailModel>();
            // StudentParentInfolistforprofile = new List<StudentParentInfo>();
            AddressModel = new AddressModel();
            AvailableStudents = new List<SelectListItem>();
            FeeFrequnecyList = new List<SelectListItem>();
            AvailableFacilities = new List<SelectListItem>();
            StudentFacilityDetails = new StudentFacilityDetails();
            StudentFacilityDetailList = new List<StudentFacilityDetails>();
            ReceiptTypeList = new List<SelectListItem>();
            UseInFeeCertificateReceiptTypeList = new List<SelectListItem>();
            AvailableSessions = new List<SelectListItem>();
            AvailableReceiptTypes = new List<SelectListItem>();
            AvailableGuardianList = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
        }
        public string FeeType { get; set; }
        public IList<SelectListItem> AvailableSessions { get; set; }

        public IList<SelectListItem> AvailableReceiptTypes { get; set; }

        public IList<SelectListItem> AvailableGuardianList { get; set; }

        public string startdateSession { get; set; }

        public string ErrorMessage { get; set; }

        public string LoggedInUserId { get; set; }

        public int PaymentRequestId { get; set; }

        public string EncFeeReceipt { get; set; }

        public bool IsAuthToPrint { get; set; }

        public string FeePeriodId { get; set; }

        public string SRN { get; set; }

        public string PortalEnrolno { get; set; }

        public string TransactionId { get; set; }

        public string FeePeriod { get; set; }

        public string TotalPayAmount { get; set; }

        public string EncFeeTypeId { get; set; }

        public bool IsPaymentSucceed { get; set; }

        public string parentname { get; set; }
        public string SchoolName { get; set; }
        public string Standard { get; set; }
        public string SchoolLogoPath { get; set; }
        public string multipleallowed { get; set; }
        public bool IncludeHeader { get; set; }

        public string GFName { get; set; }

        public string GLName { get; set; }

        public string GImage { get; set; }

        public string  PaymentGId { get; set; }

        public bool IsEdit { get; set; }

        public bool IsStaffChild{ get; set; }

        public bool FeeCheck { get; set; }

        public string EncFeeReceiptId { get; set; }

        public bool IsAdmin { get; set; }

        public string ReceiptTypeId { get; set; }

        public string ViewName { get; set; }

        public int StudentId { get; set; }

        public string StudentName { get; set; }

        public string FName { get; set; }

        public int? GuardianId { get; set; }

        public int? GuardianTypeId { get; set; }

        public string GuardianImagepath { get; set; }

        public string GuardianType { get; set; }

        public string AadharCard { get; set; }

        public string Enrollment { get; set; }

        public string CurrentClass { get; set; }

        public string UserType { get; set; }

        public string PayableFee { get; set; }

        public string CurrentClassRollno { get; set; }

        public string BoardRollno { get; set; }

        public string  StuClassId { get; set; }

        public string StudentImage { get; set; }

        public string RegistrationNo { get; set; }

        public string AdmissionNo { get; set; }

        public int SelectedSessionId { get; set; }

        public string AdmissionDate { get; set; }

        public string House { get; set; }

        public string Status { get; set; }

        public string Email { get; set; }

        public string CustomerEmailId { get; set; }

        public string CustomerMobileNo { get; set; }

        public string Gender { get; set; }

        public string SMSMobileNo { get; set; }

        public string DOB { get; set; }

        public string PhoneNo { get; set; }

        public string BillingAddress { get; set; }

        public string DOJ { get; set; }

        public string EnteranceStandard { get; set; }

        public string EnteranceClass { get; set; }

        public string Nationality { get; set; }

        public string Category { get; set; }

        public string Religion { get; set; }
     
        // student address
        public AddressModel AddressModel { get; set; }

        public string AddressType { get; set; }

        public string PlotNo { get; set; }

        public string SectorOrStreet { get; set; }

        public string Locality { get; set; }

        public string Landmark { get; set; }
        public string District { get; set; }

        public string Zip { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        // Previous School
        public bool IsPrvSchool { get; set; }

        public string PrvSchoolName { get; set; }

        public string PrvBoardName { get; set; }

        public string PrvStandard { get; set; }

        public string PrvPercentage { get; set; }

        public string PrvGrade { get; set; }

        public string PrvResult { get; set; }

        public string PrvAddress { get; set; }

        public string CustodyRight { get; set; }

        // Parent Detail 
        public IList<StudentParentInfo> StudentParentInfolist { get; set; }

        //public IList<StudentParentInfo> StudentParentInfolistforprofile { get; set; }

        // Sibling Detail 
        //public bool IsSibling { get; set; }

        public IList<StudentSiblingInfo> StudentSiblingInfolist { get; set; }

        // transport facility
        public bool IsTransport { get; set; }

        public string TransportType { get; set; }

        public string TransportTypeDet { get; set; }

        public string VehicleType { get; set; }

        public string VehicleNo { get; set; }

        public string BusRoute { get; set; }

        public string BoardingPoint { get; set; }

        public string ParkingRequired { get; set; }

        public IList<TransportFacilityHistory> TransportFacilityHistorylist { get; set; }

        // Other or medical
        public string BloodGroup { get; set; }

        public string Allergy { get; set; }

        // documrmt Info List
        public IList<StudentDocumentInfo> StudentDocumentInfolist { get; set; }

        //certificate 
        public IList<IssueDocTypeItem> IssueDocTypeList { get; set; }

        public IList<SelectListItem> Sessionlist { get; set; }

        public int IssueDocTypeId { get; set; }

        public string IssueDocType { get; set; }

        public bool CerificateMoneyAvaiable { get; set; }

        public string PayableAmount { get; set; }

        public string PaidAmount { get; set; }

        public string Charges { get; set; }

        public string Receipt { get; set; }

        public string ArrayTokenValues { get; set; }

        [AllowHtml]
        public string DocNo { get; set; }

        public int SessionId { get; set; }

        public string IssueDate { get; set; }

        public DateTime? KendoIssueDate { get; set; }

        public int IssueDocId { get; set; }

        public bool IsPrinted { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public Studentactivity StudentActivity { get; set; }

        public IList<SelectListItem> ExtraActivitylist { get; set; }

        public IList<Studentactivity> StudentActivityList { get; set; }

        public string sessionname { get; set; }

        public string EncSessionId { get; set; }

        public int IsCertificateBackdateAllowed { get; set; }

        public string IssueInAYear { get; set; }

        public string issueonceonly { get; set; }

        public string issuefeecetificateonly { get; set; }

        public bool sessiondivhidden { get; set; }

        public string EncIssueDocId { get; set; }

        public string chargestype { get; set; }

        //Attendance list
        public int monthId { get; set; }

        public IList<SelectListItem> MonthList { get; set; }

        public IList<StudentDailyAttendance> studentAttendanceList { get; set; }

        //All period list
        public List<Period> PeriodList { get; set; }

        //Student Class Details
        public StudentClassDetailModel StudentOtherDetails { get; set; }

        public IList<SelectListItem> ClassList { get; set; }

        public IList<SelectListItem> UserRoleList { get; set; }
        
        public IList<StudentClassDetailModel> StudentClassDetailList { get; set; }

        public IList<StudentExtraClassDetailModel> StudentExtraClassDetailList { get; set; }

        //Fee Addon
        public StudentFeeAddOnModel StudentFeeAddon { get; set; }

        public IList<SelectListItem> StudentFeeAddonTypeList { get; set; }

        public IList<SelectListItem> ConsessionList { get; set; }

        public IList<SelectListItem> GroupFeeHeadList { get; set; }

        public IList<StudentFeeAddOnModel> StudentFeeAddOnModelList { get; set; }

        // fee list
        public IList<StudentFeeReceiptModel> StudentFeeReceiptList { get; set; }

        public string FeeGroupHeadId { get; set; }

        public string EncStudentId { get; set; }

        public bool IsUserExist { get; set; }

        public string CurrencyIcon { get; set; }

        public string FatherName { get; set; }

        public string SchoolHeader { get; set; }

        public string SchoolAddress { get; set; }

        public string SchoolLogo { get; set; }

        //prv class details
        public IList<PreviousClassDetailModel> PreviousClassDetailList { get; set; }

        public DateTime SessionEndDate { get; set; }

        public DateTime SessionStartDate { get; set; }

        public bool IsBackDateAllowed { get; set; }

        public int ClassId { get; set; }
        public string AdmnNo { get; set; }
        public IList<SelectListItem> AvailableClasses { get; set; }
        public IList<SelectListItem> AvailableStudents { get; set; }

        public string IssueDateFrom { get; set; }

        public string IssueDateTo { get; set; }

        public DateTime Year { get; set; }

        public IList<SelectListItem> FeeFrequnecyList { get; set; }

        public string FeeFrequencyId { get; set; }

        public string MedicalHistory { get; set; }

        public string RegularMedicine { get; set; }

        //Student Facility

        public IList<SelectListItem> AvailableFacilities { get; set; }

        public StudentFacilityDetails StudentFacilityDetails { get; set; }

        public bool IsStudentFacilityforpastDateAllowed { get; set; }

        public IList<StudentFacilityDetails> StudentFacilityDetailList { get; set; }

        public IList<SelectListItem> ReceiptTypeList { get; set; }

        public IList<SelectListItem> UseInFeeCertificateReceiptTypeList { get; set; }
        public string RegDate { get; set; }
    }

    public class StudentPaymentTransactionInfo
    {
        public int bank_transaction_id { get; set; }
        public string bank_name { get; set; }
        public string payment_mode { get; set; }
        public string card_details { get; set; }
    }

    public class StudentPaymentPGReuqest
    {
        public string Amount { get; set; }

        public string CustomerName { get; set; }

        public string CustomerEmail { get; set; }

        public string CustomerBillingAddress { get; set; }

        public string CustomerMobile { get; set; }

        public string CustomerAccount { get; set; }

        public string Admnno { get; set; }
        public string Class { get; set; }
        public string FeePeriod { get; set; }

        public string EncStudentId { get; set; }
        public string SessionId { get; set; }
        public string FeePeriodId { get; set; }
        public string ReceiptTypeId { get; set; }
        public string LoggedInUserId { get; set; }
        public string UserType { get; set; }
        public string FeeType { get; set; }

    }


    public class StudentFacilityDetails
    {

        public int StudentFacilityDetailId { get; set; }

        public int StudentId { get; set; }

        public int FacilityId { get; set; }

        public string Facility { get; set; }
        public decimal Amount { get; set; }

        public string EffectiveDate { get; set; }

        public string EndDate { get; set; }

        public string EncStudentId { get; set; }
    }

    public class StudentParentInfo
    {
        public string ParentType { get; set; }

        public string ParentName { get; set; }

        public string Occupation { get; set; }

        public string Qualification { get; set; }

        public string Income { get; set; }

        public string AddressInfo { get; set; }

        public string ContactInfo { get; set; }

        public string Contact { get; set; }

        public string Image { get; set; }

        public int ImageIndex { get; set; }
    }

    public class StudentSiblingInfo
    {
        public string SiblingClass { get; set; }

        public string SiblingName { get; set; }

        public string SiblingRelation { get; set; }
    }

    public class StudentDocumentInfo
    {
        public string DocumentType { get; set; }

        public string Description { get; set; }

        public string ImagePath { get; set; }
    }

    public class TransportFacilityHistory
    {
        public int? TptFacilityId { get; set; }

        public int? TptTypeId { get; set; }

        public string TptType { get; set; }

        public int? TptTypeDetId { get; set; }

        public string TptTypeDet { get; set; }

        public int? BoardingPointId { get; set; }

        public string BoardingPoint { get; set; }

        public int? BusRouteId { get; set; }

        public int? DropRouteId { get; set; }

        public int? DropBoardingPointId { get; set; }

        public string DropBoardingPoint { get; set; }

        public string BusRoute { get; set; }
        public string DropBusRoute { get; set; }

        public int? VehicleTypeId { get; set; }

        public string VehicleType { get; set; }

        public string VehicleNo { get; set; }

        public string EffectiveDate { get; set; }

        public string EndDate { get; set; }

        public bool ParkingRequired { get; set; }

        public string Parking { get; set; }

        public bool IsEditable { get; set; }

        public bool IsDeleteable { get; set; }

        public string PickRouteNo { get; set; }
        public string DropRouteNo { get; set; }

        public string OmwnershipId { get;set; }
        public bool EnddateEditable { get; set; }

    }
    public class Studentactivity
    {

        public int StudentActivityId { get; set; }

        public int StudentId { get; set; }

        public int ExtraActivityId { get; set; }

        [AllowHtml]
        public string Description { get; set; }

        public string ExtraActivity { get; set; }

        public string EffectiveDate { get; set; }

        public string EndDate { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsPastDate { get; set; }

        public string EncStudentActivityId { get; set; }
    }

    public class StudentDailyAttendance
    {
        public string AttendanceStatusAbbr { get; set; }

        public string Date { get; set; }

        public IList<StudentPeriodWiseAttendance> studentPeriodWiseAttendanceList { get; set; }
    }

    public class StudentPeriodWiseAttendance
    {
        public string period { get; set; }

        public string AttendancePeriodWiseStatusAbbr { get; set; }
    }

    // Student Other Detail Class Wise Model
    public class StudentClassDetailModel
    {

        public int StudentClassDetailsId { get; set; }

        public int StudentClassId { get; set; }

        public string Height { get; set; }

        public decimal Weight { get; set; }

        public string Vision_L { get; set; }

        public string Vision_R { get; set; }

        public string studentclass { get; set; }

        public bool iscurrentsession { get; set; }

        public string RecordDate { get; set; }

        public string EncStudentClassDetailsId { get; set; }

        public string StudentExtraDetailMapping { get; set; }

    }

    public class StudentExtraClassDetailModel
    {
        public string StudentExtraDetail { get; set; }

        public int? StudentExtraDetailId { get; set; }

        public string StudentExtraDetailValue { get; set; }
    }

    // Student Fee Add'Ons Model
    public class StudentFeeAddOnModel
    {
        public bool IsPercentage { get; set; }
        public int StudentFeeAddonId { get; set; }

        public int StudentFeeAddOnDetailId { get; set; }

        public int StudentFeeAddonTypeId { get; set; }

        public int ConsessionId { get; set; }

        public int GroupFeeHeadId { get; set; }

        public string StudentFeeAddonHead { get; set; }

        public string StudentFeeAddon { get; set; }

        public decimal Amount { get; set; }

        public string EffectiveDate { get; set; }

        public string StudentFeeAddonType { get; set; }

        public string EndDate { get; set; }

        public bool CanDelete { get; set; }

        public int Addondetailscount { get; set; }

        public string EncStudentFeeAddonId { get; set; }

        public string EncStudentFeeAddOnDetailId { get; set; }

        public int FeeFrequencyId { get; set; }

        public int? ReceiptTypeId { get; set; }

        public bool IsFeeAddOnUpdated { get; set; }
        public string ReceiptType { get; set; }
        public string FeeFrequency { get; set; }
        public bool IsConsessionTagOther { get; set; }
        public bool IsConsessionTagStudentSpecific { get; set; }
    }

    public class PreviousClassDetailModel
    {
        public string PrvClass { get; set; }

        public string PrvSession { get; set; }

        public string PrvRollNo { get; set; }

        public string PrvBoardRollNo { get; set; }

        public string PrvHouse { get; set; }
    }

}