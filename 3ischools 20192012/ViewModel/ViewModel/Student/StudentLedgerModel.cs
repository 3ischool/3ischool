﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Student
{
    public class StudentLedgerModel
    {
        public StudentLedgerModel()
        {
            AvailableClasses = new List<SelectListItem>();
            AvailableStudents = new List<SelectListItem>();
        }

        public int? ClassId { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToPrint { get; set; }

        public bool IsAuthToDelete { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public string AdmnNo { get; set; }

        public int? StudentId { get; set; }

        public string Ledgertype { get; set; }

        public IList<SelectListItem> AvailableStudents { get; set; }

    }
    public class LedgerInfo
    {
        public string Headername { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
        public decimal WaivedAmount { get; set; }
        public decimal Balance { get; set; }
        public string Date { get; set; }
        public string LedgerType { get; set; }
    }
}