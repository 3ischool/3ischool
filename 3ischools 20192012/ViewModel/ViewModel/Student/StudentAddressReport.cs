﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Student
{
    public class StudentAddressReport
    {
        public StudentAddressReport()
        {
            ClassList = new List<SelectListItem>();
            StutusList = new List<SelectListItem>();
            ReqAddList = new List<SelectListItem>();
            AddressTypeList = new List<SelectListItem>();
            AddressList = new List<StudentAddress>();
        }

        public string Hostler { get; set; }

        public string ClassId { get; set; }

        public List<SelectListItem> ClassList { get; set; }

        public string StutusId { get; set; }

        public IList<SelectListItem> StutusList { get; set; }

        public bool IsHostler { get; set; }

        public string ReqAddId { get; set; }

        public List<SelectListItem> ReqAddList { get; set; }

        public string AddressTypeId { get; set; }

        public IList<SelectListItem> AddressTypeList { get; set; }

        public bool AddressWithOutStudent { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public string classfilter { get; set; }

        public string statusfilter { get; set; }

        public string hostlercheck { get; set; }

        public List<StudentAddress> AddressList { get; set; }
    }

    public class StudentAddress
    {
        public string StudentName { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Pincode { get; set; }
    }
}