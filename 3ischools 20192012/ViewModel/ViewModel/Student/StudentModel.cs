﻿using ViewModel.ViewModel.Address;
using ViewModel.ViewModel.ContactInfo;
using ViewModel.ViewModel.Guardian;
using ViewModel.ViewModel.Session;
using ViewModel.ViewModel.Transport;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Student
{
    public class StudentModel
    {
        public StudentModel()
        {
            AvailableStudentCategory = new List<SelectListItem>();
            AvailableBloodGroup = new List<SelectListItem>();
            AvailableStudentStatus = new List<SelectListItem>(); 
            AvailableNationality = new List<SelectListItem>();
            AvailableGender = new List<SelectListItem>();
            AvailableStudentActivity = new List<SelectListItem>();
            AvailableSessions = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
            AvailableSections = new List<SelectListItem>();
            AvailableStandards = new List<SelectListItem>();
            AvailableReligions = new List<SelectListItem>();
            AvailableSectionlist = new List<SelectListItem>();
            AvailableStudents = new List<SelectListItem>();

            AddressModel = new AddressModel();
            AvailableCustodyRights = new AvailableCustodyRights();
            GuardianModel = new GuardianModel();
            AvailableActiveUsers = new List<SelectListItem>();
            AvailableGuardians = new List<SelectListItem>();

            AvailableRightsForIstContact = new List<AvailableCustodyRights>();
            GuardianModelList = new List<GuardianModel>();
            ContactInfoModel = new ContactInfoModel();
            ContactInfoModelList = new List<ContactInfoModel>();
            AvailableContactInfoType = new List<SelectListItem>();
            AvailableStatus = new List<SelectListItem>();
            PreviousSchoolModel = new PreviousSchoolModel();
            SiblingInfoModel = new SiblingInfoModel();
            SiblingInfoModelList = new List<SiblingInfoModel>();
            TransportFacility = new TransportFacility();
            AttachedDocumentModel = new AttachedDocumentModel();
            AttachedDocumentModellist = new List<AttachedDocumentModel>();
            SessionModel = new SessionModel();
            TransportFacilityHistorylist = new List<TransportFacilityHistory>();
            StudentParentInfolist = new List<StudentParentInfo>();
            StudentParentInfolistforprofile = new List<StudentParentInfo>();
            HouseList = new List<SelectListItem>();
            StepList = new List<StepList>();
            StudentFeeAddOnModelList = new List<StudentFeeAddOnModel>();
            StudentFeeAddonTypeList = new List<SelectListItem>();
            FeeFrequnecyList = new List<SelectListItem>();
            ReceiptTypeList = new List<SelectListItem>();
            GroupFeeHeadList = new List<SelectListItem>();
            ConsessionList = new List<SelectListItem>();
            AvailableStaff = new List<SelectListItem>();
            AdmnAvailableSessions = new List<SelectListItem>();
        }
        public bool AllowRegistrationFeeinRegistrationform { get; set; }
        public bool RegistrationFeeCompulsoryinRegistrationform { get; set; }
        public bool RegistrationFeePaid { get; set; }
        public bool IsLite { get; set; }
        public bool ShowRollNo { get; set; }
        public bool IsConsessionTagOther { get; set; }
        public bool IsConsessionTagStudentSpecific { get; set; }
        public string startdateSession { get; set; }
        public bool ShowSRN { get; set; }
        public bool IsPortalEnrollmentNoValidForStandard { get; set; }

        public int? IstContactId { get; set; }
        public string SRN { get; set; }
        public string PortalEnrollmentNo { get; set; }
        public int SelectedSessionId { get; set; }
        public bool IsHostler { get; set; }
        public bool IsNewAdmission { get; set; }
        public bool IsStaffChild { get; set; }
        public bool ShowHostlerDetails { get; set; }
        public bool StudentMobileMandatory { get; set; }
        [AllowHtml]
        public string htmldata { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsRegistrationOpen { get; set; }

        public int Formtype { get; set; }
        public int Formtypereg { get; set; }
        public int? AdmnType { get; set; }

        public int? TypeId { get; set; }

        public bool FeeCheck { get; set; }

        public IList<AvailableCustodyRights> AvailableRightsForIstContact { get; set; }
        public IList<SelectListItem> AvailableTypes { get; set; }

        [Required(ErrorMessage = "Session required")]
        public int SessionId { get; set; }
        public int? AdmnSessoinid { get; set; }
        public string refurl { get; set; }
        public int? StudentId { get; set; }

        public int? StudentContactInfoId { get; set; }

        public int? StudentPhoneContactInfoId { get; set; }

        public int? StudentImageId { get; set; }

        public string siblingSessionId { get; set; }

        public int? NextStudentId { get; set; }

        [Required(ErrorMessage = "First Name required")]
        [AllowHtml]
        public string FName { get; set; }

        [AllowHtml]
        public string LName { get; set; }

        public string FullName { get; set; }

        public string AadharCard { get; set; }

        public string Enrollment { get; set; }

        [AllowHtml]
        public string Email { get; set; }

        public string BoardRollNo { get; set; }

        public string RollNo { get; set; }

        public DateTime? DOB { get; set; }

        public DateTime? DOJ { get; set; }

        public DateTime? DOR { get; set; }

        public int? ParentStaffId { get; set; }

        public string PhoneNo { get; set; }

        public string PhoneNoForProfile { get; set; }

        public string DateofBirth { get; set; }

        public string DateofJoining { get; set; }

        public string DateofAdmit { get; set; }

        public string DateOfRegd { get; set; }

        public string House { get; set; }

        public IList<SelectListItem> HouseList { get; set; }

        public Nullable<int> GenderId { get; set; }

        public string RegNo { get; set; }

        public Nullable<System.DateTime> RegDate { get; set; }

        public string AdmnNo { get; set; }

        public string AdmnDate { get; set; }

        public string SMSMobileNo { get; set; }

        public Nullable<int> StudentCategoryId { get; set; }

        public Nullable<int> StudentActivityId { get; set; }

        public Nullable<int> HouseId { get; set; }

        public Nullable<int> BloodGroupId { get; set; }

        public Nullable<int> StudentStatusId { get; set; }

        public Nullable<int> NationalityId { get; set; }

        public string StudentImagepath { get; set; }

        public int? ClassId { get; set; }

        public int? RollNoInt { get; set; }

        public int? SectionId { get; set; }

        [Required(ErrorMessage = "Standard required")]
        public int StandardId { get; set; }

        public int? IsStandardIdEditable { get; set; }
        public bool feeReceipthasentry { get; set; }
        public int? ReligionId { get; set; }

        [RegularExpression(@"\d{10}", ErrorMessage = "Invalid Mobile No.")]
        [StringLength(10, ErrorMessage = "Mobile No. cannot exceed 10 characters")]
        public string MobileNo { get; set; }

        public string Allergies { get; set; }

        public string FullAddress { get; set; }

        [AllowHtml]
        public string ParentDetailArray { get; set; }

        public int? InfoStatusId { get; set; }
        // dropdowns

        // header prop

        public string CurrentClass { get; set; }

        public string CurrentClassRollno { get; set; }

        public string Status { get; set; }

        public string StatusChangeDate { get; set; }

        public string StatusChangeMaxDate { get; set; }

        public IList<SelectListItem> AvailableGuardians { get; set; }

        // Parent Detail 
        

        public IList<SelectListItem> AvailableActiveUsers { get; set; }

        public IList<StudentParentInfo> StudentParentInfolist { get; set; }

        public IList<SelectListItem> AvailableSessions { get; set; }
        public IList<SelectListItem> AdmnAvailableSessions { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }

        public IList<SelectListItem> AvailableStudents { get; set; }

        public IList<SelectListItem> AvailableSections { get; set; }

        public IList<SelectListItem> AvailableSectionlist { get; set; }

        public IList<SelectListItem> AvailableStandards { get; set; }

        public IList<SelectListItem> AvailableReligions { get; set; }

        public IList<SelectListItem> AvailableStudentCategory { get; set; }

        public IList<SelectListItem> AvailableBloodGroup { get; set; }

        public IList<SelectListItem> AvailableStudentStatus { get; set; }

        public IList<SelectListItem> AvailableNationality { get; set; }

        public IList<SelectListItem> AvailableGender { get; set; }

        public IList<SelectListItem> AvailableStudentActivity { get; set; }

        public IList<SelectListItem> AvailableContactInfoType { get; set; }

        public IList<SelectListItem> AvailableStatus { get; set; }

        public IList<SelectListItem> StudentFeeAddonTypeList { get; set; }

        public IList<SelectListItem> AvailableStaff { get; set; }

        public string FeeFrequencyId { get; set; }

        public IList<SelectListItem> FeeFrequnecyList { get; set; }

        public string ReceiptTypeId { get; set; }

        public IList<SelectListItem> ReceiptTypeList { get; set; }

        public string GroupFeeHeadId { get; set; }

        public IList<SelectListItem> GroupFeeHeadList { get; set; }

        public AddressModel AddressModel { get; set; }

        public GuardianModel GuardianModel { get; set; }

        public AvailableCustodyRights AvailableCustodyRights { get; set; }

        public IList<GuardianModel> GuardianModelList { get; set; }

        public ContactInfoModel ContactInfoModel { get; set; }

        public IList<ContactInfoModel> ContactInfoModelList { get; set; }

        //public bool IsPrvSchool { get; set; }

        public PreviousSchoolModel PreviousSchoolModel { get; set; }

        public StudentFeeAddOnModel StudentFeeAddon { get; set; }

        public IList<StudentFeeAddOnModel> StudentFeeAddOnModelList { get; set; }

        public IList<SelectListItem> ConsessionList { get; set; }

        public string CurrencyIcon { get; set; }

        public string CustodyRight { get; set; }

        public bool IsSibling { get; set; }

        public SiblingInfoModel SiblingInfoModel { get; set; }

        public IList<SiblingInfoModel> SiblingInfoModelList { get; set; }

        public string SiblingArray { get; set; }

        //public bool IsTransport { get; set; }

        public bool IsAddNewTptFacility { get; set; }

        public IList<TransportFacilityHistory> TransportFacilityHistorylist { get; set; }

        public TransportFacility TransportFacility { get; set; }

        public bool IsConcession { get; set; }

        public AttachedDocumentModel AttachedDocumentModel { get; set; }

        public IList<AttachedDocumentModel> AttachedDocumentModellist { get; set; }

        public string DocumentArray { get; set; }

        public SessionModel SessionModel { get; set; }

        [AllowHtml]
        public string ParentDetailHtml { get; set; }

        [AllowHtml]
        public string SiblingDetailHtml { get; set; }

        public bool ApplySiblingConcession { get; set; }

        public bool IsApplySiblingAnyConcession { get; set; }

        public bool IsauthToAdd { get; set; }

        public bool IsAuthToViewStudentList { get; set; }

        public string EncStudentId { get; set; }

        public string EncStudentImageId { get; set; }

        public string EncStudentContactInfoId { get; set; }

        public string EncStudentPhoneContactInfoId { get; set; }

        public IList<StudentParentInfo> StudentParentInfolistforprofile { get; set; }

        public IList<StepList> StepList { get; set; }

        //taken for persistency of registration page 
        public string InfoUpdated { get; set; }

        public string GuardianUpdated { get; set; }

        public string PrvSchoolUpdated { get; set; }

        public string SiblingUpdated { get; set; }

        public string DocumentsUpdated { get; set; }

        public string OtherDetailUpdated { get; set; }

        public DateTime? MinTptFacilityEffectiveDate { get; set; }

        public DateTime? MaxTptFacilityEffectiveDate { get; set; }

        public DateTime? MinTptFacilityEndDate { get; set; }

        // buttons for Auth
        public bool IsauthToFeeDetail { get; set; }

        public bool IsauthToTransportDetail { get; set; }

        public bool IsauthToAddOnDetail { get; set; }

        public bool IsauthToAttendanceDetail { get; set; }

        public bool IsauthToCertificateDetail { get; set; }

        public bool IsauthToPrvDetail { get; set; }

        public bool IsauthToActivitiesDetail { get; set; }

        public bool IsauthToOtherDetail { get; set; }

        public int? AssignedSectionId { get; set; }

        public string AssignedRollNo { get; set; }

        public string SchoolName { get; set; }

        public string SchoolLogoPath { get; set; }

        public string SchoolAddress { get; set; }

        public string SchoolContactNo { get; set; }

        public string GuardianImagepath { get; set; }

        public string TransportFee { get; set; }

        public bool IsSetAdmissionNoAfterFeePaid { get; set; }

        public string MedicalHistory { get; set; }

        public string RegularMedicine { get; set; }

        public bool IsEdit { get; set; }

        public bool IsFeeAddOnForAdmission { get; set; }

        public bool HasStudentAddress { get; set; }

        public bool IsStudentRegistrationForminSinglePage { get; set; }

        public bool IsPercentage { get; set; }
    }


    public class AppStudentAttendanceModel
    {
        public AppStudentAttendanceModel()
        {

            AttendanceList = new List<AppMothlyAttendanceStatusModel>();

        }


        public string Month { get; set; }

        // public string Attendance { get; set; }

        public string Absent { get; set; }

        public string OnDuty { get; set; }
        public string Leave { get; set; }

        public string Present { get; set; }

        public string Total { get; set; }
        public int TotalWorkingDays { get; set; }

        public string Others { get; set; }

        public IList<AppMothlyAttendanceStatusModel> AttendanceList { get; set; }
    }

    public class AppMothlyAttendanceStatusModel
    {

        public string Date { get; set; }

        public string Status { get; set; }

        public string ColorCode { get; set; }

    }

    public class StepList
    {
        public string StepNo { get; set; }

        public string StepInfo { get; set; }

        public string StepMode { get; set; }
    }

    public class StudentPendingDocumentReportModel
    {
        public StudentPendingDocumentReportModel()
        {
            AvailableSessions = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
            AvailableMandatoryDocumentTypes = new List<SelectListItem>();
        }
        public int SessionId { get; set; }
        public int ClassId { get; set; }
        public int DocumentTypeId { get; set; }
        public IList<SelectListItem> AvailableSessions { get; set; }
        public IList<SelectListItem> AvailableClasses { get; set; }
        public IList<SelectListItem> AvailableMandatoryDocumentTypes { get; set; }

        public string StudentName { get; set; }
        public string ParentName { get; set; }
        public string Class { get; set; }
        public string AdmnNo { get; set; }
        public string PendingDocuments { get; set; }

        public string defaultGridPageSize { get; set; }
        public string gridPageSizes { get; set; }

    }

    public class StudentExamCorrectionModel
    {
        public StudentExamCorrectionModel()
        {
            AvailableSessions = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
            StudentInformation = new List<StudentInfo>();
            AvailableStudent = new List<SelectListItem>();
        }

        public int StudentId { get; set; }

        public int SessionId { get; set; }
        public int ClassId { get; set; }

        public IList<SelectListItem> AvailableStudent { get; set; }

        public IList<SelectListItem> AvailableSessions { get; set; }
        public IList<SelectListItem> AvailableClasses { get; set; }

        public IList<StudentInfo> StudentInformation { get; set; }
        public IList<StudentSubjectInfo> StuSubjcets { get;set; }

        public string SchoolLogo { get; set; }
        public string SchoolHeader { get; set; }
        public string SchoolAddress { get; set; }
        public bool includeSubjectCode { get; set; }

        public bool IsAuthToPrint { get; set; }

        public string defaultGridPageSize { get; set; }
        public string gridPageSizes { get; set; }

    }
    
    public class StudentInfo
    {
        public int StudentId { get; set; }
        public string AdmnDate { get; set; }
        public string RegNo { get; set; }
        public string AdmnNo { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string CustodyRight { get; set; }
        public string DOB { get; set; }
        public string AadharCardNo { get; set; }
        public string Session { get; set; }
        public string SessionId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StudentStatus { get; set; }
        public string GuardianName { get; set; }
        public string Standard { get; set; }
        public string Class { get; set; }
        public string RollNo { get; set; }
        public string Contact { get; set; }
        public string FatherName { get; set; }
        public string SchoolName { get; set; }
        public string Document { get; set; }
        public string House { get; set; }
        public string ClassId { get; set; }
        public string StudentClassId { get; set; }
    }

    public class StudentSubjectInfo
    {
        public int SubjettId { get; set; }
        public string Subject { get; set; }
        public string SubjectCode { get; set; }
    }
    public class AvailableCustodyRights
    {
        public string CustodyRightName { get; set; }
        public bool? IsActive { get; set; }
        public int? CustodyRightId { get; set; }
 
    }
    public class PrintPendingDocumentReportModel
    {
        public PrintPendingDocumentReportModel()
        {
            PendingDocumentReportList = new List<StudentPendingDocumentReportModel>();
        }

        public IList<StudentPendingDocumentReportModel> PendingDocumentReportList { get; set; }

    }

    public class PreferredContactModel
    {
        public PreferredContactModel()
        {
            AvaiableSessions = new List<SelectListItem>();
            AvaiableClasses = new List<SelectListItem>();
            AvaiableStudents = new List<SelectListItem>();
        }
        public string EncStudentId { get; set; }
        public string StudentName { get; set; }
        public int SessionId { get; set; }
        public int ClassId { get; set; }
        public bool IsMissingUserAccount { get; set; }
        public bool IsMissingContactNo { get; set; }
        public bool IsMissingPreferredContact { get; set; }

        public string Class { get; set; }
        public int GuardianId { get; set; }
        public int StudentId { get; set; }
        public string ContactNo { get; set; }
        public string UserAccount { get; set; }
        public string Guardian { get; set; }
        public int CustodyRightId { get; set; }
        public bool IsPreferredGuardian { get; set; }
        public string defaultGridPageSize { get; set; }
        public string gridPageSizes { get; set; }

        public IList<SelectListItem> AvaiableSessions { get; set; }
        public List<SelectListItem> AvaiableClasses { get; set; }
        public List<SelectListItem> AvaiableStudents { get; set; }
        
    }
}