﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Student
{
    public class PreviousSchoolModel
    {
        public PreviousSchoolModel()
        {
            AvailablePrvSchoolStandards = new List<SelectListItem>();
            AvailablePrvSchoolBoards = new List<SelectListItem>();
        }

        public int? PrvSchoolId { get; set; }

        public string PrvSchoolName { get; set; }

        public int? PrvBoardId { get; set; }

        public int? PrvStandardId { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:P2}")]
        [Range(0, 100, ErrorMessage = "Please enter a value between 0 to 100")]
        public decimal? Percentage { get; set; }

        public string Grade { get; set; }

        public string Result { get; set; }

        public string PrvSchoolAddress { get; set; }

        public IList<SelectListItem> AvailablePrvSchoolBoards { get; set; }

        public IList<SelectListItem> AvailablePrvSchoolStandards { get; set; }


    }
}