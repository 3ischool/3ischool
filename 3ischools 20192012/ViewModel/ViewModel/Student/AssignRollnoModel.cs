﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Student
{
    public class AssignRollnoModel
    {
        public AssignRollnoModel()
        {
            AvailableStandard = new List<SelectListItem>();
            AvailableStudents = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
            AvailableSections = new List<SelectListItem>();
            AdmitStudentslist = new List<AdmitStudents>();
            AvailableSessions = new List<SelectListItem>();
        }

        public int? StandardId { get; set; }

        public int? CurSessionId { get; set; }

        public IList<SelectListItem> AvailableSessions { get; set; }

        public IList<SelectListItem> AvailableStandard { get; set; }

        public int StudentId { get; set; }

        public IList<SelectListItem> AvailableStudents { get; set; }

        public int ClassId { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }

        public int SectionId { get; set; }

        public IList<SelectListItem> AvailableSections { get; set; }

        public string RollNoStartFrom { get; set; }

        public string AssignedArray { get; set; }

        public IList<AdmitStudents> AdmitStudentslist { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToExport { get; set; }

        public bool IsSeprateTable { get; set; }
        [AllowHtml]
        public string htmldata { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }
    }

    public class AdmitStudents
    {
        public int StudentId { get; set; }

        public int StudentClassId { get; set; }

        public string Regno { get; set; }

        public string Admnno { get; set; }

        public string StudentName { get; set; }

        public string FatherName { get; set; }

        public string MotherName { get; set; }

        public string Standard { get; set; }

        public string Class { get; set; }

        public string RollNo { get; set; }

        public string Status { get; set; }

        public string StudentStatus { get; set; }

        public string Color { get; set; }

        public string FName { get; set; }
        public string LName { get; set; }
        public string Session { get; set; }
        public string AdhaarNo { get; set; }
         public string DOB { get; set; }
         public string Email { get; set; }
         public string MobileNo { get; set; }

         public string SchoolName { get; set; }
         public string SchoolAddress{ get; set; }
         public string SchoolLogoPath { get; set; }
         public string Gender { get; set; }
        
        

    }

    public class Standard
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}