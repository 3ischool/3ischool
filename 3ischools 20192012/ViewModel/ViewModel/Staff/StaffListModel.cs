﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Staff
{
    public class StaffListModel
    {
        public StaffListModel()
        {
            DesignationList = new List<SelectListItem>();
            DepartmentList = new List<SelectListItem>();
            AvailableStaffServiceTypes = new List<SelectListItem>();
            StaffTypeList = new List<SelectListItem>(); 
             AvailableStatus = new List<SelectListItem>();
        }
        public int? StaffId { get; set; }
        public bool StatusId { get; set; }

        public string FilterStatus { get; set; }

        public IList<SelectListItem> AvailableStatus { get; set; }

        public string Name { get; set; }

        public string EmpCode { get; set; }

        public bool IsActive { get; set; }

        public int? PunchMcId { get; set; }

        public int? DesignationId { get; set; }

        public IList<SelectListItem> DesignationList { get; set; }

        public int? DepartmentId { get; set; }

        public IList<SelectListItem> DepartmentList { get; set; }
        public IList<SelectListItem> AvailableStaffServiceTypes { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToExport { get; set; }

        public bool IsAuthToDelete { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public IList<SelectListItem> StaffTypeList { get; set; }
        public int? StaffTypeId { get; set; }
        public bool IsTeacher { get; set; }
        public string FilterStatusId { get; set; }
    }
    public class StaffGridModel
    {
        public int? StaffId { get; set; }

        public string Name { get; set; }

        public string IsActive { get; set; }

        public bool IsStaffDeletable { get; set; }

        public bool? IsExpAcademicsDetail { get; set; }
        public string StaffType { get; set; }

        public int? PunchMcId { get; set; }

        public string MachineId { get; set; }

        public int? StaffTypeId { get; set; }

        public string EmpCode { get; set; }

        public string EncStaffId { get; set; }

        public string Designation { get; set; }

        public string Department { get; set; }

        public bool? status { get; set; }

        public string DateOfjoin { get; set; }
        public string MobileNo { get; set; }
        public string ServiceType { get; set; }
    }
}