﻿using ViewModel.ViewModel.Address;
using ViewModel.ViewModel.ContactInfo;
using ViewModel.ViewModel.Student;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Staff
{
    public class StaffModel
    {
        public StaffModel()
        {
            AddressModel = new AddressModel();
            StaffTypeList = new List<SelectListItem>();
            AttachedDocumentModel = new StaffAttachedDocumentModel();
            AttachedDocumentModellist = new List<StaffAttachedDocumentModel>();
            ContactInfoModellist = new List<ContactInfoModel>();
            DepartmentsList = new List<SelectListItem>();
            DesignationsList = new List<SelectListItem>();
            GenderList = new List<SelectListItem>();
            ContactInfoModel = new ContactInfoModel();
            AvailableStaffCategories = new List<SelectListItem>(); 
             AvailableBloodGroups = new List<SelectListItem>();
            UserRoleList = new List<SelectListItem>();
            StaffQualification = new StaffQualification();
            StaffExperience = new StaffExperience();
            StaffQualificationList = new List<StaffQualification>(); 
             StaffExperienceList = new List<StaffExperience>();
            StaffBankDetailList = new List<StaffBankDetails>();
            AvailableStaffServiceTypes = new List<SelectListItem>();

        }
        public int? StaffId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MobileNumber { get; set; }

        public string PhoneNo { get; set; }

        public string AddressType { get; set; }

        public string PlotNo { get; set; }

        public string SectorOrStreet { get; set; }

        public string Locality { get; set; }

        public string EmpCodePrefixWithSplitter { get; set; }

        public string EmpCodeSufix { get; set; }

        public string Zip { get; set; }

        public string Department { get; set; }

        public string Designation { get; set; }

        public string Gender { get; set; }

        public string City { get; set; }

        public string ContactNUmber { get; set; }

        public string Email { get; set; }

        public string DateOfJoin { get; set; }

        public DateTime? DOJ { get; set; }

        public string Employeecode { get; set; }

        public string Empcode { get; set; }

        public int? StaffImageId { get; set; }

        public string StaffImagepath { get; set; }

        public string StaffType { get; set; }

        public string StaffTypeToShow { get; set; }

        public int? StaffTypeId { get; set; }

        public string PunchMcId { get; set; }

        public IList<SelectListItem> StaffTypeList { get; set; }

        public bool IsActive { get; set; }

        public bool AlreadyInAttendace { get; set; }

        public string AadharCard { get; set; }

        public bool IsDefault { get; set; }

        public AddressModel AddressModel { get; set; }

        public ContactInfoModel ContactInfoModel { get; set; }

        public IList<ContactInfoModel> ContactInfoModellist { get; set; }

        public StaffAttachedDocumentModel AttachedDocumentModel { get; set; }

        public IList<StaffAttachedDocumentModel> AttachedDocumentModellist { get; set; }

        public IList<SelectListItem> AvailableContactInfoType { get; set; }

        public IList<SelectListItem> UserRoleList { get; set; }

        public IList<SelectListItem> AvailableStatus { get; set; }

        public int? GenderId { get; set; }

        public IList<SelectListItem> GenderList { get; set; }

        public int? DepartmentId { get; set; }

        public IList<SelectListItem> DepartmentsList { get; set; }

        public int? DesignationId { get; set; }

        public IList<SelectListItem> DesignationsList { get; set; }

        public int? InfoStatusId { get; set; }

        public string DocumentArray { get; set; }

        public string ContactsArray { get; set; }

        public string ContactInfoId { get; set; }

        public string EncStaffId { get; set; }

        public bool IsauthToAdd { get; set; }

        public bool IsauthTodelete { get; set; }

        public bool IsauthToEdit { get; set; }

        public int? StaffCategoryId { get; set; }

        public IList<SelectListItem> AvailableStaffCategories { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsPassingYearRequiredInStaffQualification { get; set; }

        public string MobileNo { get; set; }
        public string PanNo { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string FatherMobNo { get; set; }
        public string MotherMobNo { get; set; }
        public bool IsMarried { get; set; }
        public string SpouseName { get; set; }
        public string SpouseMobNo { get; set; }
        public string DateOfBirth { get; set; }

        public DateTime? DOB { get; set; }

        public int? BloodGroupId { get; set; }

        public IList<SelectListItem> AvailableBloodGroups { get; set; }
        public IList<SelectListItem> AvailableStaffServiceTypes { get; set; }

        public string StaffStatus { get; set; }

        public string StatusDate { get; set; }

        public DateTime? MaxDateDOJ { get; set; }

        public bool? IsUserExist { get; set; }

        public int? UserRoleId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public bool? IsStaffDependency { get; set; }
        public bool? IsStaffUser { get; set; }
        public string EncUserId { get; set; }
        public StaffQualification StaffQualification { get; set; }
        public StaffBankDetails StaffBankDetails { get; set; }
        public IList<StaffQualification> StaffQualificationList { get; set; }
        public StaffExperience StaffExperience { get; set; }
        public IList<StaffExperience> StaffExperienceList { get; set; }
        public IList<StaffBankDetails> StaffBankDetailList { get; set; }
        

        public int ServiceTypeId { get; set; }
        public int? StaffServiceTypeId { get; set; }
        

        public IList<SelectListItem> AvailableServiceType { get; set; }
    }
    public class StaffQualification
    {
        public int StaffQualificationId { get; set; }
        //public int StaffId { get; set; }
        public string ExamPassed { get; set; }
        public string BoardUniversity { get; set; }
        public string InstitutionName { get; set; }
        public string PassingYear { get; set; }
        public Nullable<decimal> TotalMarks { get; set; }
        public Nullable<decimal> ObtainedMarks { get; set; }
        public string QualificationImage { get; set; }
        public string imgDocumentType { get; set; }
        public bool? IsQualificationDocument { get; set; }

    }

    public class StaffExperience
    {
        public int StaffExperienceId { get; set; }
        //   public int StaffId { get; set; }
        public string JoiningDate { get; set; }
        public string LeavingDate { get; set; }
        public string Organsation { get; set; }
        public string City { get; set; }
        public string Designation { get; set; }
        public string ExperienceImage { get; set; }
        public string imgExDocumentType { get; set; }
        public bool? IsExperienceDocument { get; set; }
    }

    public class StaffBankDetails
    {
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public string Branch { get; set; }
        public string BranchCode { get; set; }
        public string IFSCCode { get; set; }
        public string MICR { get; set; }

        public int StaffBankDetailId { get; set; }
        public string BankDetailFromDate { get; set; }
        public string BankDetailToDate { get; set; }
    }
}