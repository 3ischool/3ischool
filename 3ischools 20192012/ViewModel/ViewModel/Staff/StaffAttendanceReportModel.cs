﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ViewModel.ViewModel.Staff
{
    public class StaffAttendanceReportModel
    {
        public StaffAttendanceReportModel()
        {
            MonthList = new List<SelectListItem>();
            AvailableSessions = new List<SelectListItem>();
            ClassList = new List<SelectListItem>();
            AttendanceStatusList = new List<SelectListItem>();
            AttendanceTypes = new List<SelectListItem>();
            AttendanceModes = new List<SelectListItem>();

        }
        public string Year { get; set; }

        public string MonthId { get; set; }

        public string Date { get; set; }
        public IList<SelectListItem> AttendanceTypes { get; set; }
        public string AttendanceTypeId { get; set; }

        public IList<SelectListItem> AttendanceModes { get; set; }
        public string AttendaceModeId { get; set; }
        public DateTime? AttendanceDate { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        [AllowHtml]
        public string htmldata { get; set; }

        public string ViewAttnStatus { get; set; }

        public List<SelectListItem> MonthList { get; set; }

        public string ClassId { get; set; }

        public int StaffId { get; set; }

        public string Staff_Id { get; set; }

        public bool ShowViewPunchesBtn { get; set; }

        public string StaffName { get; set; }

        public string EmpCode { get; set; }

        public string Designation { get; set; }

        public string AttendanceStatus { get; set; }

        public string AttendanceAbbr { get; set; }

        public string AttendanceInTime { get; set; }

        public string AttendanceOutTime { get; set; }

        public string AttendanceMode { get; set; }

        public string ModeOutTime { get; set; }

        public string ModeInTime { get; set; }

        public string IsAuto { get; set; }

        public string IsOutAuto { get; set; }

        public IList<SelectListItem> ClassList { get; set; }

        public int SessionId { get; set; }

        public int AttendanceStatusId { get; set; }

        public int[] AttendanceStatusIds { get; set; }

        public IList<SelectListItem> AvailableSessions { get; set; }

        public IList<SelectListItem> AttendanceStatusList { get; set; }
    }
    public class StaffAttendanceDetailReport
    {
        public StaffAttendanceDetailReport()
        {
            StaffAttendanceDetailReportList = new List<StaffAttendanceDetailReport>();
        }
        public IList<StaffAttendanceDetailReport> StaffAttendanceDetailReportList { get; set; }

        public int SN { get; set; }

        public string StaffId { get; set; }

        public string EmpCode { get; set; }

        public string StaffName { get; set; }

        public string Time { get; set; }

        public string Location { get; set; }

        public string Date { get; set; }

        public string DeviceSerialNo { get; set; }

    }

    public class AttendanceReport
    {
        public AttendanceReport()
        {
            AttendanceStatus = new List<string>();
        }
        public string EmpCode { get; set; }

        public string TeacherName { get; set; }

        public List<string> AttendanceStatus { get; set; }

        public string MonthName { get; set; }
    }

    public class StaffLeaveListReport
    {
        public StaffLeaveListReport()
        {
            MonthList = new List<SelectListItem>();
            StaffList = new List<SelectListItem>();
            DataList = new List<StaffLeaveReport>();
            StaffTypelist = new List<SelectListItem>();
            DetailList = new List<StaffLeaveDetail>();
        }
        public string MonthId { get; set; }
        public string SessionId { get; set; }
        public List<SelectListItem> MonthList { get; set; }
        public string StafId { get; set; }
        public List<SelectListItem> StaffList { get; set; }
        public string Year { get; set; }
        public string StaffTypeId { get; set; }
        public List<SelectListItem> StaffTypelist { get; set; }
        public List<StaffLeaveReport> DataList { get; set; }
        public List<StaffLeaveDetail> DetailList { get; set; }
        public string[] StaffIds { get; set; }
    }

    public class StaffLeaveReport
    {
        public string Name { get; set; }
        public string Count { get; set; }
        public string StaffId { get; set; }
        public string Date { get; set; }
    }

    public class StaffLeaveDetail
    {

        public string ApprovedBy { get; set; }
        public string Date { get; set; }
        public string LeaveType { get; set; }
        public string Reason { get; set; }
    }

    public class StaffTimeTable
    {
        public string TimeTableFrom { get; set; }
        public string TimeTableTill { get; set; }
        public string Class { get; set; }
        public string Period { get; set; }
        public string Teacher { get; set; }
        public string Subject { get; set; }
        public string DayName { get; set; }
        public string ClassPeriodDetail { get; set; }

    }
}