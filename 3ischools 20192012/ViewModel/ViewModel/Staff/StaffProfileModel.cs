﻿using ViewModel.ViewModel.Address;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ViewModel.ViewModel.Staff
{
    public class StaffProfileModel
    {

        public StaffProfileModel()
        {
            ContactInfoList = new List<ContactInfos>();
            StaffDocumentList = new List<StaffDocuments>();
            AddressModelList = new List<AddressModel>();
        }
        public string StaffImage { get; set; }
        public string StaffName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string EmpCode { get; set; }
        public string MobileNo { get; set; }
        public string Department { get; set; }
        public string StaffType { get; set; }
        public bool CurrentStatus { get; set; }
        public string Designation { get; set; }
        
        public string EmployeeCode { get; set; }
        public string Email { get; set; }
        public string AdhaarNo { get; set; }
        public string Gender { get; set; }
        public string StaffCategory { get; set; }
        public bool IsActive { get; set; }



        public IList<AddressModel> AddressModelList { get; set; }
        public IList<ContactInfos> ContactInfoList { get; set; }
        public IList<StaffDocuments> StaffDocumentList { get; set; }
    }

    public class ContactInfos
    {
        public string ContactType { get; set; }
        public string ContactInfo1 { get; set; }
        public string ContactStatus { get; set; }
        public string IsDefault { get; set; }
    }

    public class StaffDocuments
    {
        public string DocumentType { get; set; }
        public string DocumentDescription { get; set; }
        public string DocumentImagePath { get; set; }
    }
}