﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Messaging
{
    public class MessagingModel
    {
        public EmailTemplateModel EmailTemplate { get; set; }
    }

    public class EmailTemplateModel
    {
       public EmailTemplateModel()
        {
            AvailableDelayPeriodList = new List<SelectListItem>();
            AvailableMessageDurationList = new List<SelectListItem>();
            AvailableEmailAccountList = new List<SelectListItem>();
            EmailTemplateList = new List<EmailTemplateModel>();
        }
        public int MsgEmailTemplateId { get; set; }
        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }

        public string BccEmailAddress { get; set; }

        public string CCEmailAddress { get; set; }

        [Required(ErrorMessage = "Subject is Required")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Body is Required")]
        [AllowHtml]
        public string Body { get; set; }

        public bool IsActive { get; set; }

        public bool IsSendImmediately { get; set; }

        public bool IsInAdvance { get; set; }

        public int? DelayBeforeSend { get; set; }

        public int? DelayPeriodId { get; set; }

        public int? MsgDurationId { get; set; }

        public int? MsgFrequencyId { get; set; }

        public int? EmailAccountId { get; set; }

        public IList<SelectListItem> AvailableDelayPeriodList { get; set; }

        public IList<SelectListItem> AvailableMessageDurationList { get; set; }

        public IList<SelectListItem> AvailableEmailAccountList { get; set; }

        public List<EmailTemplateModel> EmailTemplateList { get; set; }

        public string EncMsgEmailTemplateId { get; set; }

        public bool IsAuthToAdd { get; set; }
        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToDelete { get; set; }

    }

    public class SMSTemplateModel
    {
        public SMSTemplateModel()
        {
            AvailableMessageDurationList = new List<SelectListItem>();
            SMSTemplateList = new List<SMSTemplateModel>();
        }
        public int MsgSMSTemplateId { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Body is Required")]
        [AllowHtml]
        public string Body { get; set; }

        public bool IsActive { get; set; }

        public bool IsInAdvance { get; set; }

        public string SendTo { get; set; }

        public int? MsgDurationId { get; set; }

        public int? MsgFrequencyId { get; set; }

        public IList<SelectListItem> AvailableMessageDurationList { get; set; }

        public List<SMSTemplateModel> SMSTemplateList { get; set; }

        public string EncMsgSMSTemplateId { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToDelete { get; set; }


    }

    public class EmailAccount
    {
        /// <summary>
        /// Gets or sets an email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets an email display name
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets an email host
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Gets or sets an email port
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Gets or sets an email user name
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets an email password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value that controls whether the SmtpClient uses Secure Sockets Layer (SSL) to encrypt the connection
        /// </summary>
        public bool EnableSsl { get; set; }

        /// <summary>
        /// Gets or sets a value that controls whether the default system credentials of the application are sent with requests.
        /// </summary>
        public bool UseDefaultCredentials { get; set; }
    }
    public enum MsgDelayPeriodEnum
    {
        Hours = 1,
        Days = 2
    }
    public enum MsgQueueStatusEnum
    {
        Delivered = 1,
        Failed = 2,
        Rejected = 16,
        StatusUnknown = 17
    }
    public sealed class Token
    {
        private readonly string _key;
        private readonly string _value;
        private readonly bool _neverHtmlEncoded;

        public Token(string key, string value) :
            this(key, value, false)
        {

        }
        public Token(string key, string value, bool neverHtmlEncoded)
        {
            this._key = key;
            this._value = value;
            this._neverHtmlEncoded = neverHtmlEncoded;
        }

        /// <summary>
        /// Token key
        /// </summary>
        public string Key { get { return _key; } }
        /// <summary>
        /// Token value
        /// </summary>
        public string Value { get { return _value; } }
        /// <summary>
        /// Indicates whether this token should not be HTML encoded
        /// </summary>
        public bool NeverHtmlEncoded { get { return _neverHtmlEncoded; } }

        public override string ToString()
        {
            return string.Format("{0}: {1}", Key, Value);
        }
    }
    public class SenderInfo
    {


    }
}