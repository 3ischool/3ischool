﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace ViewModel.ViewModel.User
{
    public partial class UserWithDetail
    {
        public int UserId { get; set; }
        public DateTime? RegdDate { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool? Status { get; set; }
        public int? UserTypeId { get; set; }
        public int? UserContactId { get; set; }
        public DateTime? PwdActivatedOn { get; set; }
        public int? PwdActivatedById { get; set; }
        public DateTime? PwdLastUpatedOn { get; set; }
        public int? PwdLastUpatedbyId { get; set; }
        public int? UserRoleId { get; set; }
        public int? DefaultProfileTypeId { get; set; }
        public bool? IsDeleted { get; set; }
    }

    public class StaffUser : UserWithDetail
    {
        public int StaffId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public bool? IsActive { get; set; }
        public int? StaffTypeId { get; set; }
        public int? PunchMcId { get; set; }
        public string EmpCode { get; set; }
        public int? DesignationId { get; set; }
        public int? DepartmentId { get; set; }
        public string AadharCardNo { get; set; }
        public int? GenderId { get; set; }
        public int? StaffCategoryId { get; set; }
        public int? ContactTypeId { get; set; }
        public DateTime? DOJ { get; set; }
        public DateTime? DOL { get; set; }
        public DateTime? DOB { get; set; }
        public int? BloodGroupId { get; set; }
    }

    public class StudentUser : UserWithDetail
    {
        public int StudentId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public DateTime? DOB { get; set; }
        public int? GenderId { get; set; }
        public string RegNo { get; set; }
        public DateTime? RegDate { get; set; }
        public string AdmnNo { get; set; }
        public DateTime? AdmnDate { get; set; }
        public int? StudentCategoryId { get; set; }
        public int? StudentActivityId { get; set; }
        public int? BloodGroupId { get; set; }
        public int? NationalityId { get; set; }
        public int? CustodyRightId { get; set; }
        public int? PunchMcId { get; set; }
        public string Allergies { get; set; }
        public int? ReligionId { get; set; }
        public int? AdmnStandardId { get; set; }
        public int? AdmnSessionId { get; set; }
        public DateTime? DOJ { get; set; }
        public int? AdmnStatus { get; set; }
        public string AadharCardNo { get; set; }
        public bool? IsOnlineRegistration { get; set; }
        public string OnlineRegnEmail { get; set; }
        public bool? IsExisting { get; set; }
        public bool? AdmissionFeePaid { get; set; }
        public string RejectionReason { get; set; }
        public bool? IsHostler { get; set; }
        public string MedicalHistory { get; set; }
        public string RegularMedicine { get; set; }
        public bool? IsOldStudent { get; set; }
        public string SRN { get; set; }
        public string AadharEnrlNo { get; set; }
        public bool? IsStaffChild { get; set; }
        public string PortalEnrolNo { get; set; }
        public string SMSMobileNo { get; set; }
        public int? StudentStatusId { get; set; }
        public string ApplRegnNo { get; set; }
        public int? ParentStaffId { get; set; }
        public decimal? RegnFeePayable { get; set; }
        public decimal? RegnFeePaid { get; set; }
    }

    public class GuardianUser : UserWithDetail
    {
        public int GuardianId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? QualificationId { get; set; }
        public int? OccupationId { get; set; }
        public int? GuardianTypeId { get; set; }
        public int? StudentId { get; set; }
        public string IncomeSlabId { get; set; }
        public string Image { get; set; }
        public string AadharNo { get; set; }
    }
 

}