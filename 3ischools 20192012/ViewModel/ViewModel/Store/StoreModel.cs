﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Store
{
    public class StoreModel
    {
        public string defaultGridPageSize { get; set; }
        public string gridPageSizes { get; set; }
        public bool IsAuthToDelete { get; set; }
        public bool IsAuthToEdit { get; set; }
        public bool IsAuthToAdd { get; set; }
    }

    public class StoreInfo: StoreModel
    {
        public StoreInfo()
        {
            StoreTypeList = new List<SelectListItem>();
            StoreList=new List<SelectListItem>();
            UserRoleList = new List<SelectListItem>();
            UserList = new List<SelectListItem>();
        }
        public string StoreId { get; set; }
        public string StoreName { get; set; }
        public string StroTypeId { get; set; }
        public List<SelectListItem> StoreTypeList { get; set; }
        public List<SelectListItem> StoreList { get; set; }
        public List<SelectListItem> UserRoleList { get; set; }
        public List<SelectListItem> UserList { get; set; }
        public bool IsPurchase { get; set; }
        public string UserId { get; set; }
        public string UserRoleId { get; set; }
    }
}