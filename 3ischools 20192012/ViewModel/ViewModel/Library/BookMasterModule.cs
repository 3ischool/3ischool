﻿using ViewModel.ViewModel.Address;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.ViewModel.Common;

namespace ViewModel.ViewModel.Library
{
    public class BookMasterModule
    {
        public BookMasterModule()
        {
            AvailableStatus = new List<SelectListItem>();
            AvailbleSessions = new List<SelectListItem>();
            AddressCityModel = new AddressCityModel();
            AddressModel = new AddressModel();
            AvailbleBookStatus = new List<SelectListItem>();
            
        }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }

        public string Status { get; set; }
        public string StatusChangedDate { get; set; }

        public string strBookLocation { get; set; }

        public bool setColor { get; set; }

        public int CurrentSessionId { get; set; }

        public int? SelectedSessionId { get; set; }

        public IList<SelectListItem> AvailableStatus { get; set; }

        public IList<SelectListItem> AvailbleSessions { get; set; }

        public IList<SelectListItem> AvailbleBookStatus { get; set; }

        public bool IsAuthToGridDelete { get; set; }

        public bool IsAuthToGridEdit { get; set; }

        public bool IsAuthToGridView { get; set; }

        public bool IsAuthToGirdAdd { get; set; }

        public bool IsAuthToDelete { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToAdd { get; set; }

        public bool IsAuthToEditFine{ get; set; }

        public AddressCityModel AddressCityModel { get; set; }

        public AddressModel AddressModel { get; set; }

    }

    public class BookMasterAttributeViewModel : BookMasterModule
    {
        public string BookMasterAttributeId { get; set; }

        public string MasterAttribute { get; set; }

        public int? AttrMaxLength { get; set; }

        public bool IsActive { get; set; }

        public string SortingIndex { get; set; }
    }

    public class BookMasterData2Model
    {
        public string BookMasterDataId { get; set; }
        public string AttributeValue { get; set; }
    }

    public class BookMasterDataModel
    {
        public BookMasterDataModel()
        {
            BookDatas = new List<BookMasterData2Model>();
        }
        public string BookMasterAttributeId { get; set; }
        public string BookMasterAttributeName { get; set; }
        public int? BookMasterAttrMaxLength { get; set; }
        public IList<BookMasterData2Model> BookDatas { get; set; }
    }

    public class BookData : BookMasterModule
    {
        public BookData()
        {
            BookAttribute = new List<SelectListItem>();
            BookDatas = new List<BookData>();
            BookDetails = new List<BookDetail>();
        }

        public string BookMasterDataId { get; set; }

        public string AttributeValue { get; set; }

        public string BookMasterAttributeId { get; set; }

        public string BookAttributeId { get; set; }

        public string BookMasterAttributeName { get; set; }

        public int? BookMasterAttrMaxLength { get; set; }

        public IList<SelectListItem> BookAttribute { get; set; }

        public IList<BookData> BookDatas { get; set; }

        public IList<BookDetail> BookDetails { get; set; }

        public bool IsActive { get; set; }
    }

    public class BookDetail : BookMasterModule
    {
        public BookDetail()
        {
            BookDatas = new List<SelectListItem>();
            Books = new List<SelectListItem>();
            BookMasterAttributes = new List<BookData>();
            BookAttributes = new List<BookAttributeValueModel>();
        }

        public string BookMasterDetailId { get; set; }

        public string BookMasterDataId { get; set; }

        public string BookMasterDataAttributeValue { get; set; }

        public string BookAttributeId { get; set; }

        public string BookMasterAttribute { get; set; }

        public string BookAttributeValue { get; set; }

        public string BookAttribute { get; set; }

        public string BookId { get; set; }

        public string AccessionNo { get; set; }

        public string TotalEditions { get; set; }
        public int intEditions { get; set; }
        public string TotalCopies { get; set; }
        public bool? IsInActiveBook { get; set; }
        public string TotalLost { get; set; }
        public int  intLost { get; set; }
        public int  intDiscard { get; set; }
        public int InActiveBookCopies { get; set; }
        public string strInActiveBookCopies { get; set; }
        public int? intTotalCopies { get; set; }
        public int? intTotalIssuedBooks { get; set; }
        public int? intAvaiableCopies { get; set; }
        public int? intIsLost { get; set; }

        public string TotalIssuedBooks { get; set; }
     
        public string AvaiableCopies { get; set; }

        public int intIssuedBooks { get; set; }

        public int IsBookInTransaction { get; set; }

        public int intCopies { get; set; }

        public IList<SelectListItem> BookDatas { get; set; }

        public IList<SelectListItem> Books { get; set; }

        public IList<BookData> BookMasterAttributes { get; set; }

        public IList<BookAttributeValueModel> BookAttributes { get; set; }
    }

    public class BookMasterViewModel : BookDetail
    {
        public BookMasterViewModel()
        {
            MaterialTypes = new List<SelectListItem>();
        }

        public string BookTitle { get; set; }

        public string BookAbbr { get; set; }

        public bool? BarCodeAutoIncrement { get; set; }

        public string BookMasterAttributeId { get; set; }

        public string SubTitle { get; set; }

        public string ISBN { get; set; }

        public string Description { get; set; }

        
        public long? AccessionNo { get; set; }
        
        public long? getsetAccessionNo { get; set; }

        public string strAccessionNo { get; set; }

        public bool? EnableAccessionNoWhileAdd { get; set; }

        public bool? AllowAcessionNoEditing { get; set; }

        public string MaterialTypeId { get; set; }

        public string MaterialType { get; set; }

        public IList<SelectListItem> MaterialTypes { get; set; }

        public string ArrayMasterAttr { get; set; }
        public string ArrayLocationMasterAttr { get; set; }

        public string ArrayAttr { get; set; }
    }

    public class BookAttributeViewModel : BookMasterModule
    {
        public string BookAttributeId { get; set; }

        public string Attribute { get; set; }

        public int? AttrMaxLength { get; set; }

        public bool IsActive { get; set; }

    }

    public class BookAttributeValueModel : BookAttributeViewModel
    {
        public BookAttributeValueModel()
        {
            BookAttribute = new List<SelectListItem>();
            Books = new List<SelectListItem>();
            BookAttributeValues = new List<BookAttributeValueModel>();
        }

        public string BookAttributeValueId { get; set; }

        public string BookAttributeId { get; set; }

        public IList<SelectListItem> BookAttribute { get; set; }

        public string BookAttributeName { get; set; }

        public string AttributeValue { get; set; }

        public string BooksId { get; set; }

        public IList<SelectListItem> Books { get; set; }

        public string BookName { get; set; }

        public string ISBN { get; set; }

        public IList<BookAttributeValueModel> BookAttributeValues { get; set; }
    }

    public class BookSellerViewModel : BookMasterModule
    {
        public BookSellerViewModel()
        {
            AvailableCities = new List<SelectListItem>();
            //AvailableStates = new List<SelectListItem>();
        }

        public string BookSellerId { get; set; }

        public string BookSellerName { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string CityId { get; set; }

        public string CityName { get; set; }

        public IList<SelectListItem> AvailableCities { get; set; }

        //public IList<SelectListItem> AvailableStates { get; set; }

        public string Landline { get; set; }

        public string Mobile { get; set; }

        public string Emails { get; set; }

        public string WebSite { get; set; }

    }

    public class BookLocationAttributeViewModel : BookMasterAttributeViewModel
    {
        public string BookLocationAttributeId { get; set; }

        public string LocationAttribute { get; set; }
    }

    public class BookLocationDataViewModel : BookData
    {
        public BookLocationDataViewModel()
        {
            BookLocationAttributes = new List<SelectListItem>();
            BookLocationDatas = new List<BookLocationDataViewModel>();
            BookLocationDataList = new List<BookLocationDataModel>();
        }

        public string BookLocationDataId { get; set; }

        public string BookLocationAttributeId { get; set; }

        public IList<SelectListItem> BookLocationAttributes { get; set; }

        public IList<BookLocationDataViewModel> BookLocationDatas { get; set; }
        public IList<BookLocationDataModel> BookLocationDataList { get; set; }
    }


    #region BookEdition/BookIdentity

    public class BookAdditionalInfoViewModel : BookMasterViewModel
    {
        public BookAdditionalInfoViewModel()
        {
            AviableBookSellers = new List<SelectListItem>();
            AvialableBooks = new List<SelectListItem>();
            AvialableEditions = new List<SelectListItem>();
            BookLocAttributes = new List<BookLocationDataViewModel>();
            AvialableEdVol = new List<SelectListItem>();
            AvialableIdentites = new List<SelectListItem>();
            BookMasterTransaction = new BookMasterTransaction();
            AvialableLanguages = new List<LanguageModel>();
        }
        public string strAccessionNo { get; set; }

        public Int64  intfilterAccessionNo { get; set; }
        public string strBookLocation { get; set; }
        public string DefaultLanguage { get; set; }

        public string DefaultLanguageAbbr { get; set; }

        public string DestinationLanguage { get; set; }
        public bool IdentityIsLost { get; set; }

        public bool BooKMarkAutoIncrement { get; set; }

        public int intbookId { get; set; }

        public string  LUID { get; set; }

        public int? NoOfPages { get; set; }

        public bool showExcel { get; set; }

        public int intMaterialTypeId { get; set; }

        public string BookEditionId { get; set; }

        public string Edition { get; set; }

        public string Volume { get; set; }

        public string PublishingDate { get; set; }

        public string ListPrice { get; set; }

        public string BookCopyStatus { get; set; }
        

        public string BookSellerId { get; set; }

        public string BookSellerName { get; set; }

        public string BookIdentityId { get; set; }

        public string UniqueId { get; set; }

        public string LUIId { get; set; }

        public string BookEdition { get; set; }

        public string Ids { get; set; }
        public string LUIAcessionNo { get; set; }

        public string BookEditionMaxId { get; set; }
        public int? BookEditionCount { get; set; }

        public int? BookCopiesCount { get; set; }
        public IList<SelectListItem> AvialableBooks { get; set; }

        public IList<SelectListItem> AvialableIdentites { get; set; }

        public IList<SelectListItem> AvialableEditions { get; set; }

        public IList<SelectListItem> AvialableEdVol { get; set; }

        public IList<LanguageModel> AvialableLanguages { get; set; }

        public string BookLocationDetailId { get; set; }

        public string BarCode { get; set; }

        public bool IsCopyLost { get; set; }
        public bool IsDiscarded { get; set; }
        public bool IsIssued { get; set; }
        public string EntryDate { get; set; }

        public BookMasterTransaction BookMasterTransaction { get; set; }

        public IList<SelectListItem> AviableBookSellers { get; set; }

        public IList<BookLocationDataViewModel> BookLocAttributes { get; set; }

    }

    #endregion

    public class BookTransactionViewModel : BookAdditionalInfoViewModel
    {
        public BookTransactionViewModel()
        {
            AllBookInformation = new List<BookAdditionalInfoViewModel>();
            AviableMembers = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
            AvailableIssuedClasses = new List<SelectListItem>();
            AvailableStaff = new List<SelectListItem>();
            AvailablIssuedeStaff = new List<SelectListItem>();
        }
        public string TotalCopies { get; set; }
        public bool IsAuthToAddTransaction { get; set; }
        public bool IsAuthToIssue { get; set; }
        public string bkTransactionAccessionLui { get; set; }
        public bool IsAuthToViewIssued { get; set; }
        public bool IsAuthToViewRecord { get; set; }
        public bool IsAuthToReturn { get; set; }
        public string BookMemberTypeId { get; set; }
        public int? SessionId { get; set; }
        public string BookMemberType { get; set; }
        public string strIsReIssue { get; set; }
        public string IsActive { get; set; }

        public string AllowedDays { get; set; }

        public string FinePerDay { get; set; }

        public string PendingFine { get; set; }

        public string SetFutureReturnCalendarDate { get; set; }

        public string SearchText { get; set; }

        public string BookTransactionId { get; set; }

        public string TransactionDate { get; set; }

        public string IssuedToId { get; set; }

        public string IssuedToUserName { get; set; }

        public string ExpectedReturnDate { get; set; }

        public string ToDateFilter { get; set; }

        public string FromDateFilter { get; set; }

        public int? SearchDateId { get; set; }

        public string ActualReturnDate { get; set; }

        public int? bkTransactionClassId { get; set; }

        public string bKTransactionStudentId { get; set; }

        public string bkMemberTypeId { get; set; }
        public int bKTransactionStaffId { get; set; }

        public int? bkIssuedClassId { get; set; }

        public string bKIssuedStudentId { get; set; }

        public string bkIssuedMemberTypeId { get; set; }
        public int bKIssuedStaffId { get; set; }
        public bool ShowBookTitle { get; set; }

        public string Fine { get; set; }

        public string LostPenalty { get; set; }

        public string PaidAmount { get; set; }

        public string PaidOn { get; set; }

        public string TransHistoryBookStatus { get; set; }
        
        public bool? IsLost { get; set; }
        public string IssuedBookRemarks { get; set; }
        public IList<SelectListItem> AvailableClasses { get; set; }
        public IList<SelectListItem> AvailableIssuedClasses { get; set; }

        public IList<SelectListItem> AvailableStaff { get; set; }
        public IList<SelectListItem> AvailablIssuedeStaff { get; set; }
        public IList<SelectListItem> AviableMembers { get; set; }

        public IList<BookAdditionalInfoViewModel> AllBookInformation { get; set; }
        public int filterSessionId { get; set; }
    }

    public class BookDefaulterListModel : BookTransactionViewModel
    {
        public BookDefaulterListModel()
        {
            BookIdentities = new List<SelectListItem>();
            BookMemberTypes = new List<SelectListItem>();
            BookTrsTypes = new List<SelectListItem>();
        }

        public IList<SelectListItem> BookIdentities { get; set; }

        public IList<SelectListItem> BookMemberTypes { get; set; }

        public IList<SelectListItem> BookTrsTypes { get; set; }

        
    }

    public class FilterExport
    {
        public string BookSellerName { get; set; }

        public string CityId { get; set; }


    }

    public class BookMasterTransaction
    {
        public BookMasterTransaction()
        {
            // AllBookInformation = new List<BookAdditionalInfoViewModel>();
            MaterialTypes = new List<SelectListItem>();
            AvailbleSessions = new List<SelectListItem>();
            AviableMembers = new List<SelectListItem>();
            BookAttributes = new List<BookAttributeValueModel>();
            BookMasterAttributes = new List<BookData>();
        }

        public bool IsAuthToAddTransaction { get; set; }

        public bool IsAuthToIssue { get; set; }

        public bool IsAuthToEditFine { get; set; }
        public bool IsAuthToViewIssued { get; set; }
        public bool IsAuthToViewRecord { get; set; }
        public bool IsAuthToReturn { get; set; }
        public string BookMemberTypeId { get; set; }
        public int? SessionId { get; set; }
        public string BookMemberType { get; set; }

        public string IsActive { get; set; }

        public string AllowedDays { get; set; }

        public string FinePerDay { get; set; }

        public string PendingFine { get; set; }

        public string SetFutureReturnCalendarDate { get; set; }

        public string SearchText { get; set; }

        public string BookTransactionId { get; set; }

        public string TransactionDate { get; set; }

        public string IssuedToId { get; set; }

        public string IssuedToUserName { get; set; }

        public string ExpectedReturnDate { get; set; }

        public string ToDateFilter { get; set; }

        public string FromDateFilter { get; set; }

        public int? SearchDateId { get; set; }

        public string ActualReturnDate { get; set; }

        public bool ShowBookTitle { get; set; }

        public string Fine { get; set; }

        public string LostPenalty { get; set; }

        public string PaidAmount { get; set; }

        public string PaidOn { get; set; }

        public string TransHistoryBookStatus { get; set; }

        public int CurrentSessionId { get; set; }
        public bool? IsLost { get; set; }
        public string BookTitle { get; set; }
        public string BookId { get; set; }
        public string IssuedBookRemarks { get; set; }
        public IList<SelectListItem> MaterialTypes { get; set; }
        public IList<SelectListItem> AviableMembers { get; set; }
        public IList<BookAttributeValueModel> BookAttributes { get; set; }
        public IList<SelectListItem> AvailbleSessions { get; set; }
        public string BookIdentityId { get; set; }
        public IList<BookData> BookMasterAttributes { get; set; }
        public string Description { get; set; }
    }

    public class BookLocationDataModel
    {
        public string BookLocationDataId { get; set; }
        public string BookLocationAttributeId { get; set; }
        public string BookLocationAttributeValue { get; set; }
    }

    public class GetAllBooksDataSourceModel
    {
        public string BookId { get; set; }
        public string TotalEditions { get; set; }
        public int intEditions { get; set; }
        public string TotalCopies { get; set; }
        public int InActiveBookCopies { get; set; }
        public string strInActiveBookCopies { get; set; }
        public int? intAvaiableCopies { get; set; }
        public string TotalIssuedBooks { get; set; }
        public string AvaiableCopies { get; set; }
        public int intIssuedBooks { get; set; }
        public int IsBookInTransaction { get; set; }
        public int intCopies { get; set; }
        public int intLost { get; set; }
        public int intDiscard { get; set; }
        public string BookTitle { get; set; }
        public string SubTitle { get; set; }
        public string ISBN { get; set; }
        public string Description { get; set; }
        public string strAccessionNo { get; set; }
        public string MaterialTypeId { get; set; }
        public string MaterialType { get; set; }
        public bool BooKMarkAutoIncrement { get; set; }
        public bool IsAuthToDelete { get; set; }
        public long? AccessionNo { get; set; }
    }
}