﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KS_SmartChild.ViewModel.User;

namespace KS_SmartChild.ViewModel.Student
{
    public class StudentListModel
    {
        public StudentListModel()
        {
            AvailableTypes = new List<SelectListItem>();
            AvailableSessions = new List<SelectListItem>();
            AvailableStandards = new List<SelectListItem>();
            AvailableClasses = new List<SelectListItem>();
            AvailableStatus = new List<SelectListItem>();
            AvailableCategory = new List<SelectListItem>();
            ReqAddList = new List<SelectListItem>();
            AddressTypeList = new List<SelectListItem>();
            AddressList = new List<StudentAddressData>();
            HostlerStatusList = new List<SelectListItem>();
            AdmissionList = new List<SelectListItem>();
            HouseList = new List<SelectListItem>();
        }
        public ManageUser ManageUser { get; set; }
        public bool IsStaffChild { get; set; }

        public int ModuleId { get; set; }

        public int AdmittedListTotalCount { get; set; }

        public string StaffChild { get; set; }

        public bool IsAdmissionNoAfterFeePaid { get; set; }

        public bool IsEnableBordingNo { get; set; }

        public string StudentName { get; set; }

        public bool IsHostler { get; set; }
        public string Hostler { get; set; }
        public int TypeId { get; set; }
        public string Type_Id { get; set; }

        public string[] SelectedType_Id { get; set; }

        public IList<SelectListItem> AvailableTypes { get; set; }

        public int? SessionId { get; set; }

        public int CurrentSession { get; set; }

        public IList<SelectListItem> AvailableSessions { get; set; }

        public int? StandardId { get; set; }

        public IList<SelectListItem> AvailableStandards { get; set; }

        public int[] ClassId { get; set; }

        public IList<SelectListItem> AvailableClasses { get; set; }

        public int StatusId { get; set; }

        public IList<SelectListItem> AvailableStatus { get; set; }

        public bool IsAuthToEdit { get; set; }

        public bool IsAuthToPrint { get; set; }

        public bool IsAuthToDelete { get; set; }

        public bool IsAuthToGridAssignBoradingNo { get; set; }

        public int? CategoryId { get; set; }

        public IList<SelectListItem> AvailableCategory { get; set; }

        public string FromDate { get; set; }

        public string ToDate { get; set; }

        public string gridPageSizes { get; set; }

        public string defaultGridPageSize { get; set; }

        public string AdmnNo { get; set; }

        public string AdvanceSearch { get; set; }

        public bool? ShowHostlerFilter { get; set; }

        public bool IsAuthToExport { get; set; }

        public bool IsAuthToView { get; set; }

        public bool IsAuthToSet { get; set; }

        public string ReqAddId { get; set; }

        public List<SelectListItem> ReqAddList { get; set; }

        public string AddressTypeId { get; set; }

        public IList<SelectListItem> AddressTypeList { get; set; }

        public bool AddressWithOutStudent { get; set; }

        public int[] classfilter { get; set; }

        public string statusfilter { get; set; }

        public string hostlercheck { get; set; }

        public bool IsPickGuardinaAddress { get; set; }

        public bool IsPickStudentAddress { get; set; }

        public List<StudentAddressData> AddressList { get; set; }

        public List<SelectListItem> HostlerStatusList { get; set; }

        public List<SelectListItem> AdmissionList { get; set; }
        public int? AdmissionId { get;set;}

        public string HouseId { get; set; }
        public List<SelectListItem> HouseList { get; set; }
        public string House { get; set; }
        public bool AllowRegistrationFeeinRegistrationform { get; set; }
    }

    public class StudentAddressData
    {
        public string StudentName { get; set; }

        public string Address { get; set; }

        public string City { get; set; }
    }

    public class StudentGridModel
    {
        public int StudentId { get; set; }

        public string StudentName { get; set; }

        public string FatherName { get; set; }

        public string RegNo { get; set; }

        public string AdmnNo { get; set; }

        public string Standard { get; set; }

        public string Class { get; set; }

        public string ContactNo { get; set; }

        public string HasMobileNoAndHasAccount { get; set; }

        public string StudentBoradingNo { get; set; }

        public int RollNo { get; set; }

        public string Status { get; set; }

        public string EncStudentId { get; set; }

        public string Date { get; set; }

        public string RegDate { get; set; }

        public string AdmnDate { get; set; }

        public int? Admnstatus { get; set; }

        public int? StudentCategoryId { get; set; }

        public int? CustodyRightId { get; set; }

        public string TypeStatus { get; set; }

        public DateTime? StdDate { get; set; }

        public string StudentStatusColor { get; set; }

        public bool? status { get; set; }

        public bool IsPreviousSession { get; set; }

        public bool IsExistInNextSession { get; set; }

        public bool IsNewStudent { get; set; }

        public int? statusId { get; set; }

        public string Session { get; set; }

        public string SessionId { get; set; }

        public string Section { get; set; }

        public string DateofBirth { get; set; }

        public string AadhaarNo { get; set; }

        public string MobileNo { get; set; }

        public string SchoolName { get; set; }

        public string SchoolImagePath { get; set; }

        public string LName { get; set; }

        public string FName { get; set; }

        public string StudentImagepath { get; set; }
        public string House { get; set; }

        public string ClassId { get; set; }

        public bool IsHostler { get; set; }

        public string StudentClassId { get; set; }
        public string GuardianMobileno { get; set; }
        public decimal? PendingRegistrationfee { get; set; }
    }

    public class StudentInfoList
    {
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string AdmnDate { get; set; }
        public string RegNo { get; set; }
        public string AdmnNo { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string CustodyRight { get; set; }
        public string DOB { get; set; }
        public string AadharCardNo { get; set; }
        public string Session { get; set; }
        public string SessionId { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StudentStatus { get; set; }
        public string GuardianName { get; set; }
        public string Standard { get; set; }
        public string Class { get; set; }
        public string RollNo { get; set; }
        public string BoardingNo { get; set; }
        public string Contact { get; set; }
        public string FatherName { get; set; }
        public string SchoolName { get; set; }
        public string Document { get; set; }
        public string House { get; set; }
        public string ClassId { get; set; }
        public string StudentClassId { get; set; }
        public string GuardianMobileno { get; set; }
        public bool IsHostler { get; set; }
        public string AdmnSessionId { get;set;}
    }


}