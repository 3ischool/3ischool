﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.Reporting
{
    public class StandardReportModel
    {
        public StandardReportModel()
        {
            StandardReportColsList = new List<StandardReportColsModel>();
            StandardReportLayout = new StandardReportLayoutModel();
            StandardReportParametersList = new List<StandardReportParametersModel>();
        }

        public int? RptMasterId { get; set; }

        public string EncRptMasterId { get; set; }

        public string RptMasterName { get; set; }

        public string RptStoredProcedureName { get; set; }

        public string RptHeaderName { get; set; }

        public string RptGenericName { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreateDate { get; set; }

        public bool IsActive { get; set; }

        public string Status { get; set; }

        public string RptColArray { get; set; }

        public string RptFltrArray { get; set; }

        public StandardReportLayoutModel StandardReportLayout { get; set; }

        public IList<StandardReportColsModel> StandardReportColsList { get; set; }

        public IList<StandardReportParametersModel> StandardReportParametersList { get; set; }

        public bool IsStdViewReport { get; set; }

        public bool IsGenViewReport { get; set; }

    }

    public class StandardReportLayoutModel
    {
        public StandardReportLayoutModel()
        {
            AvailablePositionTypes = new List<SelectListItem>();
        }

        public int? ReportLayoutId { get; set; }

        public bool IsPageTotal { get; set; }

        public bool IsTotalOpening { get; set; }

        public bool IsPageNo { get; set; }

        public int? PageNoPostionId { get; set; }

        public IList<SelectListItem> AvailablePositionTypes { get; set; }

        public DateTime? CurrentDate { get; set; }

        public string Date { get; set; }

        public TimeSpan CurrentTime { get; set; }

        public string Time { get; set; }

        public bool IsOrgName { get; set; }

        public bool IsOrgAddress { get; set; }

        public bool IsOrgLogo { get; set; }
       
    }

    public class StandardReportParametersModel
    {
        public StandardReportParametersModel()
        {
            AvailableFilerControlType = new List<SelectListItem>();
        }

        public int? RptStandardColId { get; set; }

        public string FilterName { get; set; }

        public string FilterLabel { get; set; }

        public int? FilterIndex { get; set; }

        public bool IsFilter { get; set; }

        public int? FilerControlTypeId { get; set; }

        public IList<SelectListItem> AvailableFilerControlType { get; set; }

    }

    public class StandardReportColsModel
    {
        public StandardReportColsModel()
        {
            AvailableSortingMethod = new List<SelectListItem>();
            AvailableTables = new List<SelectListItem>();
            AvailableTableCols = new List<SelectListItem>();
        }

        public int? RptStandardColId { get; set; }

        public string ColName { get; set; }

        public string ColLabel { get; set; }

        public int? ColIndex { get; set; }

        public int? ColWidth { get; set; }

        public bool IsVisible { get; set; }

        public bool IsSorted { get; set; }

        public string SortingMethod { get; set; }

        public IList<SelectListItem> AvailableSortingMethod { get; set; }

        public int? SortingIndex { get; set; }

        public bool IsGroup { get; set; }

        public int? GroupIndex { get; set; }

        public bool IsSum { get; set; }

        public string TableName { get; set; }

        public IList<SelectListItem> AvailableTables { get; set; }

        public string ColumnName { get; set; }

        public IList<SelectListItem> AvailableTableCols { get; set; }
    }


}