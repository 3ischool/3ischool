﻿using DAL.DAL.Common;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DAL.DAL.UserModule
{
    public partial class ModuleVersionService : IModuleVersionService
    {
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        private readonly IConnectionService _IConnectionService;

        public ModuleVersionService(IConnectionService IConnectionService)
        {
            this._IConnectionService = IConnectionService;
            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #region  Methods

        #region  ModuleType

        public KSModel.Models.ModuleType GetModuleTypeById(int ModuleTypeId, bool IsTrack = false)
        {
            if (ModuleTypeId == 0)
                throw new ArgumentNullException("ModuleType");

            var ModuleType = new ModuleType();
            if(IsTrack)
                ModuleType = (from s in db.ModuleTypes where s.ModuleTypeId == ModuleTypeId select s).FirstOrDefault();
            else
                ModuleType = (from s in db.ModuleTypes.AsNoTracking() where s.ModuleTypeId == ModuleTypeId select s).FirstOrDefault();

            return ModuleType;
        }

        public List<KSModel.Models.ModuleType> GetAllModuleTypes(ref int totalcount, string ModuleType = "", int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.ModuleTypes.ToList();
            if (!String.IsNullOrEmpty(ModuleType))
                query = query.Where(e => e.ModuleType1 == ModuleType).ToList();

            totalcount = query.Count;
            var ModuleTypes = new PagedList<KSModel.Models.ModuleType>(query, PageIndex, PageSize);
            return ModuleTypes;
        }

        public void InsertModuleType(KSModel.Models.ModuleType ModuleType)
        {
            if (ModuleType == null)
                throw new ArgumentNullException("ModuleType");

            db.ModuleTypes.Add(ModuleType);
            db.SaveChanges();
        }

        public void UpdateModuleType(KSModel.Models.ModuleType ModuleType)
        {
            if (ModuleType == null)
                throw new ArgumentNullException("ModuleType");

            db.Entry(ModuleType).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteModuleType(int ModuleTypeId = 0)
        {
            if (ModuleTypeId == 0)
                throw new ArgumentNullException("ModuleType");

            var ModuleType = (from s in db.ModuleTypes where s.ModuleTypeId == ModuleTypeId select s).FirstOrDefault();
            db.ModuleTypes.Remove(ModuleType);
            db.SaveChanges();
        }

        #endregion

        #region Module

        public KSModel.Models.Module GetModuleById(int ModuleId, bool IsTrack = false)
        {
            if (ModuleId == 0)
                throw new ArgumentNullException("Module");

            var Module = new Module();
            if(IsTrack)
                Module = (from s in db.Modules where s.ModuleId == ModuleId select s).FirstOrDefault();
            else
                Module = (from s in db.Modules.AsNoTracking() where s.ModuleId == ModuleId select s).FirstOrDefault();

            return Module;
        }

        public List<KSModel.Models.Module> GetAllModules(ref int totalcount, int? ModuleTypeId = null, string Module = "", bool? status = false, int PageIndex = 0, int PageSize = int.MaxValue)
        {
            var query = db.Modules.ToList();
            if (ModuleTypeId != null && ModuleTypeId > 0)
                query = query.Where(e => e.ModuleTypeId == ModuleTypeId).ToList();
            if (!String.IsNullOrEmpty(Module))
                query = query.Where(e => e.Module1 == Module).ToList();

            totalcount = query.Count;
            var Modules = new PagedList<KSModel.Models.Module>(query, PageIndex, PageSize);
            return Modules;
        }

        public void InsertModule(KSModel.Models.Module Module)
        {
            if (Module == null)
                throw new ArgumentNullException("Module");

            db.Modules.Add(Module);
            db.SaveChanges();
        }

        public void UpdateModule(KSModel.Models.Module Module)
        {
            if (Module == null)
                throw new ArgumentNullException("Module");

            db.Entry(Module).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteModule(int ModuleId = 0)
        {
            if (ModuleId == 0)
                throw new ArgumentNullException("TableType");

            var Module = (from s in db.Modules where s.ModuleId == ModuleId select s).FirstOrDefault();
            db.Modules.Remove(Module);
            db.SaveChanges();
        }

        #endregion

        #endregion


    }
}