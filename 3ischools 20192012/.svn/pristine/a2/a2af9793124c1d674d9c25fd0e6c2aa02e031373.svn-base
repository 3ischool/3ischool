﻿@model ViewModel.ViewModel.Master.GradePatternModel
@{
    ViewBag.Title = "Grade Pattern";
}

<div class="row top-space-10 page-heading">
    <div class="col-md-12" id="error_notification_div" hidden="">
        <div class="error_notification"><i class="fa fa-ban"></i><span></span></div>
    </div>
    <div class="col-md-12" id="success_notification_div" hidden="">
        <div class="success_notification"><i class="fa fa-check"></i><span></span></div>
    </div>
    <div class="col-md-6">
        <h1>Manage Grade Pattern</h1>
    </div>
    @if (Model.IsAuthToAdd)
    {
        <div class="col-md-6 text-right">
            <a href="javascript:void(0)" id="Add_GradePattern" class="btn btn-xs btn-default ">Add New</a>
        </div>
    }
</div>

@*hidden fields*@
@Html.HiddenFor(m => m.GradePatternId)
@Html.HiddenFor(m => m.GradePatternDetailId)

<input id="rowindex_gardepattern" name="rowindex" type="hidden" value="" />
<form class="form-horizontal ks-custom-form">
    <!-- Form Row -->
    <div class=" top-space-10" @*style="display:none"*@>
        <div class="row">
            <!-- Form Coulmn -->
            <div class="col-md-3">
                <label class=" control-label">Grade Pattern<i class="fa fa-asterisk ks-required"></i></label>
                <!-- Button Combo -->
                <div class="btn-combo-ks">
                    @Html.DropDownListFor(model => model.GradePatternId, Model.AvailableGradePattern, new { @class = "form-control", @id = "grade_pattern_id" })
                </div>
                @if (Model.IsAuthToAddGradePattern)
                {
                    <a style="margin-top: -28px;" href="javascript:void(0)" class="btn-combo-plus-ks" data-toggle="tooltip" data-placement="top" id="add_gradepattern">
                        <i class="fa fa-plus"></i>
                    </a>
                }
                <!-- //Button Combo -->
            </div>
            <!-- //Form Coulmn -->
        </div>
    </div>
    <div class=" create_garde_pattern_div" style=" display:none">
        <div class="row top-space-10">

            <!-- Form Coulmn -->
            <div class="col-md-2">
                <label class=" control-label">Grade<i class="fa fa-asterisk ks-required"></i></label>
                <div class="">
                    @Html.DropDownListFor(model => model.GradeId, Model.AvailableGrade, new { @class = " form-control" })
                </div>
            </div>
            <!-- //Form Coulmn -->
            <!-- Form Coulmn -->
            <div class="col-md-2">
                <label class=" control-label">Starting Marks<i class="fa fa-asterisk ks-required"></i></label>
                @Html.TextBoxFor(model => model.StartingMarks, new { @class = "form-control", @maxlength = "3" })
            </div>
            <!-- //Form Coulmn -->
            <!-- Form Coulmn -->
            <div class="col-md-2">
                <label class=" control-label">Ending Marks<i class="fa fa-asterisk ks-required"></i></label>
                @Html.TextBoxFor(model => model.EndingMarks, new { @class = "form-control", @maxlength = "3" })
            </div>
            <!-- //Form Coulmn -->
            <!-- Form Coulmn -->
            <div class="col-md-2">
                <div class="">
                    <label class=" control-label">Effective Date <i class="fa fa-asterisk ks-required"></i></label><!-- Form Label -->
                    @Html.TextBox("Effective Date", "", new { @id = "EffectiveDate" })
                    <!-- Form Field -->
                </div>
            </div>
            <!-- //Form Coulmn -->
            <div class="col-md-4">
                <button type="button" id="Save" name="save" class="btn btn-success" style="margin-top:15px">
                    <i class="fa fa-floppy-o"></i>
                    Save
                </button>
                <button type="button" id="Cancel_GradePattern" name="save" class="btn btn-danger" style="margin-top:15px">
                    <i class="fa fa-ban"></i>
                    Cancel
                </button>

            </div>
        </div>
    </div>

    <div class="modal-popup" id="gradepattern_div" hidden>
        <div class="modal-popup-body">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Add Grade Pattern</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="text-danger" id="ErrorNotification_Div" hidden>
                            <div class="col-md-10 col-md-offset-2">
                                <div class="errornotification"><i class="fa fa-ban"></i><span></span></div>
                            </div>
                        </div>
                        <div class="text-success" id="SuccessNotification_Div" hidden>
                            <div class="col-md-10  col-md-offset-2">
                                <div class="successnotification"><i class="fa fa-check"></i><span></span></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Form Coulmn -->
                        <div class="col-md-5">
                            <label class=" control-label">Grade Pattern</label>
                            @Html.TextBoxFor(model => model.GradePattern, new { @class = "form-control", maxlength = "30" })
                        </div>
                        <!-- //Form Coulmn -->
                    </div>
                </div>
                <div class="modal-footer">
                    @if (Model.IsAuthToAddGradePattern)
                    {
                        <button type="button" id="Save_gradepattern" name="save" class="btn btn-success">
                            <i class="fa fa-floppy-o"></i>
                            Save
                        </button>}
                    <button type="button" class="btn btn-danger" id="close_gardepattern_div">
                        <i class="fa fa-ban"></i>
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>
@*table for displaying Marks Pattern*@
<div class="row  top-space-10">
    <div class="col-md-12">
        <table id="gradepattern_grid" class="table table-bordered table-responsive">
            <thead class="thead-default">
                <tr>
                    <th style="width: 15%; display:none">Grade Pattern</th>
                    <th style="width: 10%; display:none">Board</th>
                    <th style="width: 10%">Grade</th>
                    <th style="width: 15%">Starting Marks (%)</th>
                    <th style="width: 15%">Ending Marks (%)</th>
                    <th style="width: 10%">Effective Date</th>
                    <th style="width: 10%">End Date</th>
                    @if (Model.IsAuthToEdit || Model.IsAuthToClose || Model.IsAuthToDelete)
                    {
                        <th style="width: 10%">Action</th>
                    }
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

</div>

<!-- Extend and close dates blocks -->
<div class="view_dialog_box viewbox_div" id="ViewBox" hidden>
    <div style="width:100%; height:25px; float:left;">
        <font style="float:left; padding:2px 10px;">
            <b>Close Grade Pattern</b>
        </font>
        <a class="text-danger" href="javascript:void(0);" id="close_ViewBox">
            <i class="fa fa-2x fa-times-circle-o" style="float:right;"></i>
        </a>
    </div>
    <div class="row top-space-10">
        <div class="col-md-12">
            <label class="control-label col-md-3">End Date</label>
            <div class="col-md-8">
                @Html.TextBox("End Date", "", new { @class = "KendoDatepickerenddate ", @id = "EndDate" })
            </div>
        </div>
        <div class="col-md-12 top-space-10 text-danger" id="inner_validation_div" hidden>
            <div class="col-md-12">
                <label class="control-label col-md-3"></label>
                <span id="inner_validation_span"></span>
            </div>
        </div>
        <div class="col-md-12 top-space-10">
            <label class="col-md-3"></label>
            <div class="col-md-3">
                @if (Model.IsAuthToClose)
                {
                    <button type="button" data-id="" id="save_ViewBox" class="btn btn-success">
                        <i class="fa fa-floppy-o"></i>
                        Save
                    </button>}
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {

        //$("#add_gradepattern").attr('title', 'Add New Grade Pattern');
        $("#grade_pattern_id").change(function () {
            if ($("#grade_pattern_id option:selected").text() == "Add New") {
                $("#add_gradepattern").attr('data-original-title', 'Add New Grade Pattern');
            }
            else {
                $("#add_gradepattern").attr('data-original-title', 'Edit Grade Pattern');
            }
        })

        //get list of records on grade pattern change
        $('#grade_pattern_id').change(function () {

            $('#ks_loader').show();

            $('#Save').text("");
            $('#Save').append('<i class="fa fa-floppy-o"></i> Save');
            $('.create_garde_pattern_div').css('display', 'none');
            //$('#grade_pattern_id').val("").trigger('change');
            $('#GradeId').val("").trigger('change');
            $('#StartingMarks').val("");
            $("#EndingMarks").val("");
            $("#EffectiveDate").val("");
            $("#GradePatternDetailId").val("");
            $("#GradePatternId").val("");
            $('#GradePattern').attr('disabled', false);
            $('#Add_GradePattern').show();
            $('#gradepattern_grid tbody').html('');

            var gradepatternid = $(this).val();
            if (gradepatternid != "") {

                $.ajax({
                    type: "POST",
                    url: '@Html.Raw(Url.Action("getgradepatterndetaillist", "Master"))',
                    data: { "gradepatternid": gradepatternid },
                    success: function (result) {
                        if (result.status == "success") {
                            $.each(result.data, function () {
                                var column = this.EndDate;
                                var row = '<tr>' +
                                   '<td class="">' + this.Grade + '</td>' +
                                   '<td class="">' + this.StartingMarks + '</td>' +
                                   '<td class="">' + this.EndingMarks + '</td>' +
                                   '<td class="">' + this.EffectiveDate + '</td>' +
                                   '<td class="">' + this.EndDate + '</td>' +
                                   '<td class="" hidden>' + this.GradeId + '</td>' +
                                   '<td class="" hidden>' + this.GradePatternId + '</td>' +
                                   '<td class="" hidden>' + this.EncGradePatternDetailId + '</td>' +
                                   '<td class="" hidden>' + this.hasgradepatterndetial + '</td>';

                                if (column != "") {
                                    @if (Model.IsAuthToEdit || Model.IsAuthToClose || Model.IsAuthToDelete) {
                                        <text>
                                    row += '<td></td>';
                                    </text>
                                    }
                                }
                                else {
                                    row += '<td>' +
                                    @if (Model.IsAuthToEdit){
                                                  <text>
                                    ' <a title="Edit" data-toggle="tooltip" data-placement="top" href="javascript:void(0)" class="Edit_gradepattern btn btn-xs btn-default" data-id=' + this.GradePatternId + ' data-id1=' + this.EncGradePatternDetailId + ' title="Edit"><i class="fa fa-pencil "></i></a>' +
                                    </text>
                                              }
                                    @if (Model.IsAuthToDelete){
                                                    <text>
                                                    ' <a title="Delete" data-toggle="tooltip" data-placement="top" href="javascript:void(0)" class="Delete_gradepattern btn btn-xs btn-default" data-id=' + this.GradePatternId + ' data-id1=' + this.EncGradePatternDetailId + ' title="Delete"><i class="fa fa-trash "></i></a>' +
                                    </text>
                                                }
                                    @if (Model.IsAuthToClose){
                                                     <text>
                                                    ' <a title="Close" data-toggle="tooltip" data-placement="top" href="javascript:void(0)" data-id=' + this.GradePatternId + ' data-id1=' + this.EncGradePatternDetailId + ' class="clsclose btn btn-xs btn-default"><i class="fa fa-ban"></i></a>' +
                                    </text>
                                                 }
                                    '</td>';
                                }

                                row += '</tr>';

                                $('#gradepattern_grid tbody').append(row);

                            });
                            $('#ks_loader').hide();
                            $('[data-toggle="tooltip"]').tooltip()
                        }

                        else {
                            ToastMessage(result.msg, "error", true, 5000);
                            $('#ks_loader').hide();
                        }
                    },
                    error: function (xhr, status) {
                        var err = "Error " + " " + status;
                        ToastMessage(err, "error", true, 5000);
                    }
                });
            } else {
                $('#gradepattern_grid tbody').text('');
                $('#ks_loader').hide();
            }
        })

        //add grade pattern
        $('#add_gradepattern').click(function () {
            $('#gradepattern_div').show();
            $('#GradePattern').val("");
            $('#data_validation_div_Grade_Pattern ul').text("");
            $('#data_validation_div_Grade_Pattern ul').text("");
            if ($('#grade_pattern_id').val() != "") {
                $('#GradePattern').val($('#grade_pattern_id option:selected').text());
            }
        })

        $('#close_gardepattern_div').click(function () {
            $('#gradepattern_div').hide();
            $('#GradePattern').val("");
        })


        $('#Save_gradepattern').click(function () {
            $('#ks_loader').show();
            var errors = ValidateGradepattern();
            if (errors) {
                var GradePatternModel = new Object();
                GradePatternModel.GradePattern = $('#GradePattern').val();
                GradePatternModel.GradePatternId = $('#grade_pattern_id').val();
                $.ajax({
                    type: "POST",
                    url: '@Html.Raw(Url.Action("SaveGradePattern", "Master"))',
                    data: GradePatternModel,
                    success: function (result) {
                        if (result.status == "success") {

                            $('#grade_pattern_id').text('');
                            $.each(result.SelectList, function () {
                                $('#grade_pattern_id').append('<option value="' + this.Value + '">' + this.Text + '</option>');
                            });
                            $('#grade_pattern_id').val(result.GradePatternId);
                            $('#gradepattern_div').hide();
                            $('#ks_loader').hide();
                        }
                        else {
                            var validationaray = result.data.split('ε');
                            for (var i = 0; i < validationaray.length - 1; i++) {
                                ToastMessage(validationaray[i], "error", true, 5000);
                            }
                            $('#ks_loader').hide();
                        }
                    },
                    error: function (xhr, status) {
                        var err = "Error " + " " + status;
                        ToastMessage(err, "error", true, 5000);
                        $('#ks_loader').hide();
                    }
                });
            }
            else {
                var validationaray = validationmessage.split('ε');
                for (var i = 0; i < validationaray.length - 1; i++) {
                    ToastMessage(validationaray[i], "error", true, 5000);
                }
                $('#ks_loader').hide();
            }
        });


        var valid = false;
        var validationmessage = "";
        var ctrlfocus = "";
        function ValidateGradepattern() {
            valid = true;
            validationmessage = "";
            ctrlfocus = "";

            if ($('#GradePattern').val() == "") {
                if (ctrlfocus == "")
                    ctrlfocus = "#GradePattern";
                validationmessage = validationmessage + "Please enter Grade Pattern ε";
                valid = false;
            }
            return valid;
        }
        //------add grade pattern--------
        //add grade pattern detail
        $('#Cancel_GradePattern').click(function () {

            $('#Save').text("");
            $('#Save').append('<i class="fa fa-floppy-o"></i> Save');
            $('.create_garde_pattern_div').css('display', 'none');
            //$('#grade_pattern_id').val("").trigger('change');
            $('#GradeId').val("").trigger('change');
            $('#StartingMarks').val("");
            $("#EndingMarks").val("");
            $("#EffectiveDate").val("");
            $("#GradePatternDetailId").val("");
            $("#GradePatternId").val("");
            $('#GradePattern').attr('disabled', false);
            $('#Add_GradePattern').show();

        })

        $('#Add_GradePattern').click(function () {
            $('#Save').text("");
            $('#Save').append('<i class="fa fa-floppy-o"></i> Save');
            $('.create_garde_pattern_div').css('display', 'block');
            //$('#grade_pattern_id').val("").trigger('change');
            $('#GradeId').val("").trigger('change');
            $('#StartingMarks').val("");
            $("#EndingMarks").val("");
            $("#EffectiveDate").val("");
            $("#GradePatternDetailId").val("");
            $("#GradePatternId").val("");
            $('#GradePattern').attr('disabled', false);
        });

        //date picker
        $("#EffectiveDate,#EndDate").kendoDatePicker({
            format: "dd/MM/yyyy",
        })
        $("#EffectiveDate,#EndDate").attr('readonly', true);
        $("#StartingMarks,#EndingMarks").keydown(function (e) {
            NumericOnly(e);
        });

        $('#close_div').click(function () {
            $('.create_garde_pattern_div').css('display', 'none');
        });

        $('#Save').click(function () {
            $('#ks_loader').show();
            var formvalid = ValidateGradePatternDetails();
            if (formvalid) {

                var GradePatternModel = new Object();

                GradePatternModel.GradeId = $('#GradeId').val();
                GradePatternModel.StartingMarks = $("#StartingMarks").val();
                GradePatternModel.EndingMarks = $('#EndingMarks').val();

                var date = $('#EffectiveDate').val();
                if (date != null && date != "")
                    GradePatternModel.EffectiveDate = GetDateFromString(date);
                else
                    GradePatternModel.EffectiveDate = "";

                var gradepatternid = $('#grade_pattern_id').val();
                if (gradepatternid != null)
                    GradePatternModel.GradePatternId = gradepatternid;
                else
                    GradePatternModel.GradePatternId = 0;

                var gradepatterndetailid = $('#GradePatternDetailId').val();
                if (gradepatterndetailid != null)
                    GradePatternModel.EncGradePatternDetailId = gradepatterndetailid;
                else
                    GradePatternModel.EncGradePatternDetailId = "";

                var rowindex = $('#rowindex_gardepattern').val();
                $.ajax({
                    type: "POST",
                    url: '@Html.Raw(Url.Action("SaveUpdateGradePatternDetail", "Master"))',
                    data: GradePatternModel,
                    success: function (result) {
                        if (result.status == "success") {
                            ToastMessage(result.msg, "success", true, 5000);
                            $('#markspattern_div').hide();
                            $('#grade_pattern_id').trigger("change");
                            $('#ks_loader').hide();
                        }
                        else if (result.status == "validationfailed") {
                            var validationaray = result.data.split('ε');
                            for (var i = 0; i < validationaray.length - 1; i++) {
                                ToastMessage(validationaray[i], "error", true, 5000);
                            }
                            $('#ks_loader').hide();
                        }
                        else {
                            $('#ks_loader').hide();
                        }
                    },
                    error: function (xhr, status) {
                        var err = "Error " + " " + status;
                        ToastMessage(err, "error", true, 5000);
                        $('#ks_loader').hide();
                    }
                });
            }
            else {
                var validationaray = validationmessageDetails.split('ε');
                for (var i = 0; i < validationaray.length - 1; i++) {
                    ToastMessage(validationaray[i], "error", true, 5000);
                }
                $('#ks_loader').hide();
            }
        });

        var valid = true;
        var validationmessageDetails = "";
        var ctrlfocus = "";
        function ValidateGradePatternDetails() {
            valid = true;
            validationmessageDetails = "";
            ctrlfocus = "";

            if ($('#grade_pattern_id').val() == "") {
                if (ctrlfocus == "")
                    ctrlfocus = "#GradePattern";
                validationmessageDetails = validationmessageDetails + "Please enter Grade Pattern ε";
                valid = false;
            }
            if ($('#GradeId').val() == "") {
                if (ctrlfocus == "")
                    ctrlfocus = "#GradeId";
                validationmessageDetails = validationmessageDetails + "Please select Grade ε";
                valid = false;
            }
            if ($('#StartingMarks').val() == "") {
                if (ctrlfocus == "")
                    ctrlfocus = "#StartingMarks";
                validationmessageDetails = validationmessageDetails + "Please enter Starting Marks ε";
                valid = false;
            }
            if ($('#EndingMarks').val() == "") {
                if (ctrlfocus == "")
                    ctrlfocus = "#EndingMarks";
                validationmessageDetails = validationmessageDetails + "Please enter Ending Marks ε";
                valid = false;
            }
            if ($('#EffectiveDate').val() == "") {
                if (ctrlfocus == "")
                    ctrlfocus = "#EffectiveDate";
                validationmessageDetails = validationmessageDetails + "Please enter Effective Date ε";
                valid = false;
            }
            if ($('#StartingMarks').val() != "" && $('#EndingMarks').val() != "") {
                var startmarks = parseFloat($('#StartingMarks').val());
                var endmarks = parseFloat($('#EndingMarks').val());
                if (startmarks > endmarks) {
                    if (ctrlfocus == "")
                        ctrlfocus = "#StartingMarks";
                    validationmessageDetails = validationmessageDetails + "Starting Marks should be less than from Ending Marks ε";
                    valid = false;
                }
            }

            return valid;
        }

        //edit Student Fee AddOn
        $(document).on('click', '.Edit_gradepattern', function () {
            $('#Add_GradePattern').hide();
            $('#rowindex_gardepattern').val("");
            $('#rowindex_gardepattern').val($(this).parent().parent().index());
            $('#Save').text("");
            $('#Save').append('<i class="fa fa-floppy-o"></i> Update');
            $('.create_garde_pattern_div').css('display', 'block');
            //$('#GradePattern').attr('disabled', false);
            $('#GradePattern').val("");
            $('#StartingMarks').val("");
            $('#EndingMarks').val("");
            $('#GradeId').val("").trigger('change');
            $('#EffectiveDate').val("");
            $('#EndDate').val("");
            $('#GradePatternId').val("");
            $('#GradePatternDetailId').val("");

            $('#StartingMarks').val($(this).parent().parent().find("td:nth-child(2)").text());
            $('#EndingMarks').val($(this).parent().parent().find("td:nth-child(3)").text());
            $('#GradeId').val($(this).parent().parent().find("td:nth-child(6)").text()).trigger('change');
            $('#EffectiveDate').val($(this).parent().parent().find("td:nth-child(4)").text());
            $('#GradePatternId').val($(this).attr('data-id'));
            //$('#grade_pattern_id').val($(this).attr('data-id'));
            $('#GradePatternDetailId').val($(this).attr('data-id1'));

        });

        //Delete Grade Pattern
        $(document).on('click', '.Delete_gradepattern', function () {
            $('#ks_loader').show();
            var gardepatterndetailid = $(this).attr('data-id1'); // grade pattern detail id
            $('#rowindex_gardepattern').val($(this).parent().parent().index());
            var rowindex = $('#rowindex_gardepattern').val();

            if (confirm('Are you sure you want to delete this?')) {
                // ajax call fro save row closing date
                $.ajax({
                    type: "POST",
                    url: '@Html.Raw(Url.Action("DeleteGradePatternDetail", "Master"))',
                    data: { "gardepatterndetailid": gardepatterndetailid },
                    success: function (result) {
                        if (result.status == "success") {
                            ToastMessage(result.msg, "success", true, 5000);

                            var tbl = $('#gradepattern_grid');
                            var rows = $('tr', tbl);
                            rowindex++;
                            var row = rows.eq(rowindex);
                            row.remove();

                            $('#Save').text("");
                            $('#Save').append('<i class="fa fa-floppy-o"></i> Save');
                            $('.create_garde_pattern_div').css('display', 'none');
                            //$('#grade_pattern_id').val("").trigger('change');
                            $('#GradeId').val("").trigger('change');
                            $('#StartingMarks').val("");
                            $("#EndingMarks").val("");
                            $("#EffectiveDate").val("");
                            $("#GradePatternDetailId").val("");
                            $("#GradePatternId").val("");
                            $('#GradePattern').attr('disabled', false);
                            $('#Add_GradePattern').show();

                            $('#rowindex').val("");
                        }
                        else {
                            ToastMessage(result.msg, "error", true, 5000);
                        }
                        $('#ks_loader').hide();
                    },
                    error: function (xhr, status) {
                        var err = "Error " + " " + status;
                        ToastMessage(err, "error", true, 5000);
                        $('#ks_loader').hide();
                    }
                });
            } else
                $('#ks_loader').hide();
        });


        //save end date
        $('#close_ViewBox').click(function () {
            closedialog();
        });
        $(document).on('click', '.clsclose', function () {
            $('#EndDate').val("");
            opendialog();
            var gradepatterndetailid = $(this).attr('data-id1');
            $('#save_ViewBox').attr('data-id', gradepatterndetailid);
            $('#rowindex_gardepattern').val($(this).parent().parent().index()); // row index
        })

        //close dialog box
        function closedialog() {
            $('#rowindex').val('');
            $("#ViewBox").hide(800);
        }

        //open dialog for end Date
        function opendialog() {

            $(".viewbox_div").hide(800);
            $("#ViewBox").show(800);

        }
        //save end date StudentActivity
        $('#save_ViewBox').click(function () {
            $('#ks_loader').show();

            var gradepatterndetailid = $(this).attr('data-id'); // student activity Id
            var rowindex = $('#rowindex_gardepattern').val();

            if ($('#EndDate').val() != "") {
                var EndDate = $('#EndDate').val();
                var date = GetDateFromString(EndDate);
                // ajax call fro save row closing date
                $.ajax({
                    type: "POST",
                    url: '@Html.Raw(Url.Action("CloseGradePattern", "Master"))',
                    data: { "gradepatterndetailid": gradepatterndetailid, "EndDate": date },
                    success: function (result) {
                        if (result.status == "success") {
                            ToastMessage(result.msg, "success", true, 5000);
                            var tbl = $('#gradepattern_grid');
                            var rows = $('tr', tbl);
                            rowindex++;
                            var row = rows.eq(rowindex);
                            row.find("td:nth-child(5)").text(result.data);
                            row.find("td:nth-child(10)").text("");
                            //clear fields
                            $('#Save').text("");
                            $('#Save').append('<i class="fa fa-floppy-o"></i> Save');
                            $('.create_garde_pattern_div').css('display', 'none');
                            //$('#grade_pattern_id').val("").trigger('change');
                            $('#GradeId').val("").trigger('change');
                            $('#StartingMarks').val("");
                            $("#EndingMarks").val("");
                            $("#EffectiveDate").val("");
                            $("#GradePatternDetailId").val("");
                            $("#GradePatternId").val("");
                            $('#GradePattern').attr('disabled', false);
                            $('#Add_GradePattern').show();
                            //--clear fields

                            closedialog();
                            $('#rowindex_gardepattern').val("");
                        }
                        else {
                            //$('#data_validation_div_GradePattern').show();
                            //$('#data_validation_div_GradePattern ul').text(result.msg);
                            ToastMessage(result.msg, "error", true, 5000);
                            //$('#grid_error_studentactivity').show();
                            //$('#grid_error_studentactivity span').text(result.msg);
                        }
                        $('#ks_loader').hide();
                    },
                    error: function (xhr, status) {
                        var err = "Error " + " " + status;
                        ToastMessage(err, "error", true, 5000);
                        $('#ks_loader').hide();
                    }
                });
            }
            else {
                ToastMessage("Please enter End Date", "error", true, 5000);
                $('#ks_loader').hide();
            }
        });


        //-----save end date ----
        // function get date as string and return date
        function GetDateFromString(date) {
            var datearry = date.split('/');
            var returnFromDate = datearry[1] + "/" + datearry[0] + "/" + datearry[2];
            return returnFromDate;
        }
    });
</script>

