﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DAL.DAL.TransportModule
{
    public partial interface IBusService
    {
        #region Bus

        Bus GetBusById(int BusId, bool IsTrack = false);

        List<Bus> GetAllBuses(ref int totalcount, string BusNo = "", int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertBus(Bus Bus);

        void UpdateBus(Bus Bus);

        void DeleteBus(int BusId = 0);

        #endregion

        #region BusRoute

        BusRoute GetBusRouteById(int BusRouteId, bool IsTrack = false);
        List<KSModel.Models.BusRoute> GetBusRouteNoTracking();
        List<BusRoute> GetAllBusRoutes(ref int totalcount, string BusRoute = "", string Routeno = "",
            bool? Routestatus = true, DateTime? EffectiveDate = null, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertBusRoute(BusRoute BusRoute);

        void UpdateBusRoute(BusRoute BusRoute);

        void DeleteBusRoutes(int BusRouteId = 0);

        #endregion

        #region BusRouteDetail

        BusRouteDetail GetBusRouteDetailById(int BusRouteDetailId, bool IsTrack = false);

        List<BusRouteDetail> GetAllBusRouteDetails(ref int totalcount, DateTime? EffectiveDate = null, int? BusRouteId = null,
            int? BoardingPointId = null, int? BoardingIndex = null, bool? status = null, DateTime? EndDate = null, decimal? Distance = null,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertBusRouteDetail(BusRouteDetail BusRouteDetail);

        void UpdateBusRouteDetail(BusRouteDetail BusRouteDetail);

        void DeleteBusRouteDetail(int BusRouteDetailId = 0);

        #endregion

        #region TptRouteDetail

        TptRouteDetail GetTptRouteDetailById(int TptRouteDetailId, bool IsTrack = false);

        List<TptRouteDetail> GetAllTptRouteDetails(ref int totalcount, int? BusRouteId = null, DateTime? EffectiveDate = null,
            DateTime? EndDate = null, int? BusId = null, int? DriverId = null, int PageIndex = 0, int PageSize = int.MaxValue);

        DataTable GetAllRouteStudents(ref int totalcount, int? BusRouteId = null, int? SessionId = null);

        void InsertTptRouteDetail(TptRouteDetail TptRouteDetail);

        void UpdateTptRouteDetail(TptRouteDetail TptRouteDetail);

        void DeleteTptRouteDetail(int TptRouteDetailId = 0);

        #endregion

        #region BoardingPoint

        BoardingPoint GetBoardingPointById(int BoardingPointId, bool IsTrack = false);

        List<BoardingPoint> GetAllBoardingPoints(ref int totalcount, string Boardingpoint = "",
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertBoardingPoint(BoardingPoint BoardingPoint);

        void UpdateBoardingPoint(BoardingPoint BoardingPoint);

        void DeleteBoardingPoint(int BoardingPointId = 0);

        #endregion

        #region BoardingPointFee

        BoardingPointFee GetBoardingPointFeeById(int BoardingPointFeeId, bool IsTrack = false);

        List<BoardingPointFee> GetAllBoardingPointFees(ref int totalcount,int BoardingPointId=0,
            int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertBoardingPointFee(BoardingPointFee BoardingPointFee);

        void UpdateBoardingPointFee(BoardingPointFee BoardingPointFee);

        void DeleteBoardingPointFee(int BoardingPointFeeId = 0);

        #endregion

        #region TptDriver

        TptDriver GetTptDriverById(int TptDriverId, bool IsTrack = false);

        List<TptDriver> GetAllTptDrivers(ref int totalcount, string DriverName = "", string LicenseNo = "", DateTime? LicenseExpiryDate = null,
            int PageIndex = 0, int PageSize = int.MaxValue);


        void InsertTptDriver(TptDriver TptDriver);

        void UpdateTptDriver(TptDriver TptDriver);

        void DeleteTptDriver(int TptDriverId = 0);

        #endregion

        #region Bus route selectlist // bus route autocomplete

        IList<SelectListItem> BusRouteBoardingPoints(string TptType = "");

        IList<SelectListItem> BusRouteByBoardingPoint(int BoardingPointId, string TptType, DateTime? EffectiveDate = null);

        #endregion

        #region  "TptAttendant"
        List<TptAttendant> GetAllTptAttendants(ref int totalcount, string AttendantName = "", string MobileNo = "",
           int PageIndex = 0, int PageSize = int.MaxValue);
        #endregion

        #region TptRouteDriver

        TptRouteDriver GetTptRouteDriverById(int TptRouteDriverId, bool IsTrack = false);


        List<TptRouteDriver> GetAllTptRouteDrivers(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTptRouteDriver(TptRouteDriver TptRouteDriver);

        void UpdateTptRouteDriver(TptRouteDriver TptRouteDriver);

        void DeleteTptRouteDriver(int TptRouteDriverId = 0);


        #endregion
        #region TptRouteAttendant

        TptRouteAttendant GetTptRouteAttendantById(int TptRouteAttendantId, bool IsTrack = false);


        List<TptRouteAttendant> GetAllTptRouteAttendants(ref int totalcount, int PageIndex = 0, int PageSize = int.MaxValue);

        void InsertTptRouteAttendant(TptRouteAttendant TptRouteAttendant);

        void UpdateTptRouteAttendant(TptRouteAttendant TptRouteAttendant);

        void DeleteTptRouteAttendant(int TptRouteAttendantId = 0);


        #endregion
    }
}
