﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ViewModel.ViewModel.IssueDocument
{
    public class IssueDocTemplateModel
    {
        public IssueDocTemplateModel()
        {
            AvailableIssueDocType = new List<SelectListItem>();
            AvailableIssueDocHTMLTemplate = new List<SelectListItem>();
            IssueDocTemplateList = new List<IssueDocTemplateList>();
            AvailableFilterIssueDocType = new List<SelectListItem>();
        }

        public int? IssueDocTemplateId { get; set; }

        public int IssueDocTypeId { get; set; }

        public IList<SelectListItem> AvailableIssueDocType { get; set; }

        public IList<SelectListItem> AvailableFilterIssueDocType { get; set; }

        public string TemplateTitle { get; set; }

        public string TemplateSubTitle { get; set; }

        public int? IssueDocTemplateDetailId { get; set; }

        [AllowHtml]
        public string TemplateText { get; set; }

        public string EffDate { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public IList<IssueDocTemplateList> IssueDocTemplateList { get; set; }

        public int? IssueDocHTMLTemplateId { get; set; }

        public IList<SelectListItem> AvailableIssueDocHTMLTemplate { get; set; }

        public string defaultGridPageSize { get; set; }

        public string gridPageSizes { get; set; }
    }

    public class IssueDocTypeModel : IssueDocTemplateModel
    {
        public IssueDocTypeModel()
        {
            AvaliableTokens = new List<TokenList>();
            RightTokenLists = new List<SelectListItem>();
            EditableTokenLists = new List<SelectListItem>();
            SaveTokenLists = new List<SelectListItem>();
            LeftTokenLists = new List<SelectListItem>();
        }

        public bool tokenReadOnly { get; set; }

        public string EncIssueDocTypeId { get; set; }

        public string IssueDocType { get; set; }

        public bool? IssueOnceOnly { get; set; }

        public string CertificateFee { get; set; }

        public bool? OnceInAYear { get; set; }

        public bool IsFeeCertificate { get; set; }

        public bool IsAuthToDelete { get; set; }

        public bool IsExistTokens { get; set; }

        public bool IsCertificateIssued { get; set; }

        public IList<TokenList> AvaliableTokens { get; set; }

        public int?[] RightTokenId { get; set; }
        public int EditableTokenId { get; set; }
        public int EditableListTokenId { get; set; }

        public string RightTokenVal { get; set; }

        public int?[] LeftTokenId { get; set; }

        public string LeftTokenVal { get; set; }

        public IList<SelectListItem> RightTokenLists { get; set; }
        public IList<SelectListItem> EditableTokenLists { get; set; }
        public IList<SelectListItem> SaveTokenLists { get; set; }

        public IList<SelectListItem> LeftTokenLists { get; set; }
    }

    public class TokenList
    {
        public string StudentId { get; set; }
        public string AdmnDate { get; set; }
        public string RegNo { get; set; }
        public string Nationality { get; set; }
        public string IssueDocSrNo { get; set; }
        public string AdmnNo { get; set; }
        public string StudentName { get; set; }
        public string DOB { get; set; }
        public string AadharCardNo { get; set; }
        public string Session { get; set; }
        public string AadharEnrlNo { get; set; }
        public string Status { get; set; }
        public string GuardianName { get; set; }
        public string Class { get; set; }
        public string Standard { get; set; }
        public string RollNo { get; set; }
        public string Contact { get; set; }
        public string BloodGroup { get; set; }
        public string FatherName { get; set; }
        public string FatherNumber { get; set; }
        public string MotherName { get; set; }
        public string MotherNumber { get; set; }
        public string SchoolName { get; set; }
        public string Document { get; set; }
        public string Gender { get; set; }
        public string Religion { get; set; }
        public string Category { get; set; }
        public string House { get; set; }
        public string Address { get; set; }
    }

    public class IssueDocTemplateList
    {
        public int? IssueDocTemplateId { get; set; }

        public string IssueDocType { get; set; }

        public int? IssueDocTypeId { get; set; }

        public string EndDate { get; set; }

        public DateTime? DTEndDate { get; set; }

        public string EffDate { get; set; }

        public string TemplateTitle { get; set; }

        public string TemplateSubTitle { get; set; }

        public bool IsDeleted { get; set; }

        public string HtmlTitle { get; set; }

        public bool IsAssignHtml { get; set; }

        public bool hasAnyTemplate { get; set; }

        public int? IssueDocHTMLTemplateId { get; set; }

        public string EncIssueDocHTMLTemplateId { get; set; }

        public string EncIssueDocTemplateId { get; set; }
    }

    public class IssueDocHTMLTemplateMapping
    {
        public int? IssueDocTemplateId { get; set; }

        public string IssueDocTempId { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public int? IssueDocHTMLTemplateId { get; set; }
    }

    public class IssueDocColsValue
    {
        public string ColsName { get; set; }

        public string ColsValue { get; set; }

        public string ColsId { get; set; }

    }

    public class IssueDocInfoViewModel
    {
        public string IssueDocId { get; set; }

        public string IssueDate { get; set; }

        public string DocNo { get; set; }

        public string FullDocNo { get; set; }

        public string IssueDocTypeId { get; set; }

        public string IssueDocType { get; set; }

        public string SessionId { get; set; }

        public string Session { get; set; }

        public string StudentId { get; set; }

        public string StudentName { get; set; }

        public string StudentAdmiNo { get; set; }

        public string ClassId { get; set; }

        public string Class { get; set; }

        public string DocTemplateId { get; set; }

        public string PayableAmount { get; set; }

        public string PaidAmount { get; set; }

        public bool IsPrinted { get; set; }

        public bool IsTaxCertificate { get; set; }

        public string IsFeeCertificate { get; set; }

        public string Charges { get; set; }

        public string Receipt { get; set; }
    }

}