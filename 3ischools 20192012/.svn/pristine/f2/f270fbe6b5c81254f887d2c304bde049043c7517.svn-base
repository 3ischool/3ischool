﻿@model ViewModel.ViewModel.GatePass.GatePassModel
@{
    var defaultGridPageSize = Model.defaultGridPageSize;
    var gridPageSizes = Model.gridPageSizes;
}
@{
    ViewBag.Title = "Gate Pass Approval";
}

<style>
     div#cpfilter_div span.select2, div#stufilter_div span.select2, div#stafilter_div span.select2 {
        width: 100% !important;
    }

    #Concernedpersonfilter, #Studentfilter, #Stafffilter {
        width: 100% !important;
    }
    .timepicker_wrap {
        left: -30px !important;

        width: 162px;
    }
</style>

<div class="row top-space-10 page-heading">
    <div class="col-md-6">
        <h1>Gate Pass Approval</h1>
    </div>
    <div class="col-md-6 text-right">
        <button type="button" id="btnFilter" class="btn btn-xs btn-default">Filter</button>
    </div>
</div>


    <div class="top-space-10" id="filter_div" hidden>
        <div class="row">
            <div class="col-md-2">
                <label>Pass Type</label>
                @Html.DropDownList("Passtypefilter", Model.GatePassTypeList, new { @class = "form-control" })
            </div>
            <div class="col-md-2" id="cpfilter_div">
                <label>Authority</label>
                @Html.DropDownList("Concernedpersonfilter", Model.ConcernedStaffList, new { @class = "form-control js-example-templating" })
            </div>
            <div class="col-md-2" id="stufilter_div">
                <label>Student</label>
                @Html.DropDownList("Studentfilter", Model.StudentListForFilter, new { @class = "form-control js-example-templating" })
            </div>
            <div class="col-md-2" id="stufilterSession_div">
                <label>Session</label>
                @Html.DropDownList("StuFilterSession", Model.SessionFilterList, new { @class = "form-control", @style = "width: 150px;" })
            </div>
            <div class="col-md-2" id="stufilterClass_div" hidden>
                <label>Class</label>
                @Html.DropDownList("ClassFilter", Model.ClassFilterList, new { @class = "form-control", @style = "width: 150px;" })
            </div>

            <div class="col-md-2" id="stafilter_div">
                <label>Staff</label>
                @Html.DropDownList("Stafffilter", Model.StaffList, new { @class = "form-control js-example-templating" })
            </div>
            <div class="col-md-3" id="visfilter_div" hidden>
                <label>Visitor</label>
                @Html.TextBox("visitorfilter", "", new { @class = "form-control" })
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <label>From Date</label>
                @Html.TextBox("Issuefromdatefilter", "", new { @class = "form-control" })
            </div>
            <div class="col-md-2">
                <label>To Date</label>
                @Html.TextBox("Issuetodatefilter", "", new { @class = "form-control" })
            </div>
            <div class="col-md-2">
                <label>From Time</label>
                @Html.TextBox("Intimefilter", "", new { @class = "form-control" })
            </div>

            <div class="col-md-2">
                <label>To Time</label>
                @Html.TextBox("Outtimefilter", "", new { @class = "form-control" })
            </div>

            &nbsp; &nbsp; &nbsp;
            <div class="col-md-3">
                <button type="button" id="Filter_gatepass" name="save" class="btn btn-default" style="margin-top:25px">
                    Filter
                </button>
                <button type="button" class="btn btn-danger" id="Reset_filter_gatepass" style="margin-top:25px">
                    <i class="fa fa-ban"></i>
                    Reset
                </button>
            </div>
        </div>
    </div>

    <div class="row top-space-10">
        <div class="col-md-12">
            <div id="GatePassApproval-grid">
            </div>
        </div>
    </div>

<script>
    $(function () {
        $("#GatePassApproval-grid").kendoGrid({
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        url: "@Html.Raw(Url.Action("GetGatePass", "GatePass"))",
                        type: "POST",
                        dataType: "json",
                        contentType: "application/json",
                        data:additionalData
                    },
                    parameterMap: function(data) {
                        return JSON.stringify(data);
                    }
                },
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors"
                },
                error: function(e) {
                    display_kendoui_grid_error(e);
                    // Cancel the changes
                    this.cancelChanges();
                },
                pageSize: @(defaultGridPageSize),
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            dataBound: onDataBound,
            pageable: {
                refresh: true,
                pageSizes: [@(gridPageSizes)]
            },
            scrollable: false,
            sortable: true,
            columns: [{
                field: "GatePassId",
                hidden: true
            },{
                field: "SerialNo",
                title: "Gate Pass No",
            },{
                field: "GatePassType",
                title: "Pass Type",
            },{
                field: "IssueDate",
                title: "Issue Date",
                sortable: true
            },{
                field: "InTime",
                title:"In Time",
            },{
                field: "OutTime",
                title:"Out Time",
            },{
                field: "IssuedTo",
                title:"Issued To",
                template: '#if(IssuedToPopUp!=null){#<span class="description_tooltip_cls" href="javascript:void(0)" data-toggle="tooltip" data-placement="top"  title="#=IssuedToPopUp#"><font color="black">#=IssuedTo#</span>#}#',
                hidden: false
            },{
                field: "NoOfPersons",
                title: "Persons",
            },{
                field: "Purpose",
                title: "Purpose",
                template: '#if(PurposePopUp!=null){#<span class="description_tooltip_cls" href="javascript:void(0)" data-toggle="tooltip" data-placement="top"  title="#=PurposePopUp#"><font color="black">#=Purpose#</font></span>#}#',
            },{
                field: "ConcernedPerson",
                title:"Authority",
            },{
                field: "Session_Id",
                hidden: true
            },{
                field: "SessionName",
                hidden: true
            },{
                field: "GatePassId",
                title: "Action",
                template: '#if(GatePassStatus==""){#<a title="Approve" data-toggle="tooltip" data-placement="top" class="clsAaproveGatepass btn btn-xs btn-default"><i class="fa fa-check"></i></a>#'
                                +  '#<a title="Disapprove" data-toggle="tooltip" data-placement="top" class="clsDisaaproveGatepass btn btn-xs btn-default"><i class="fa fa-ban"></i></a>#}'
                           +'else{#<label>#=GatePassStatus#</label>#}#'
            }]
        });
    });

    function onDataBound(arg) {
        $('[data-toggle="tooltip"]').tooltip();
    }

    function additionalData() {
        var data = {
            GatePassTypeId:$("#Passtypefilter").val(),
            InTime: $('#Intimefilter').val(),
            OutTime: $('#Outtimefilter').val(),
            StaffId: $('#Stafffilter').val(),
            StudentId:$('#Studentfilter').val(),
            ConcernedStaffId:$('#Concernedpersonfilter').val(),
            IssueDate:$('#Issuefromdatefilter').val(),
            IssueToDate:$('#Issuetodatefilter').val(),
            visitor:$("#visitorfilter").val(),
            SessionId :$("#StuFilterSession").val(),
            ClassId : $("#ClassFilter").val()

        };
        addAntiForgeryToken(data);
        return data;
    }

    addAntiForgeryToken = function (data) {
        data._RequestVerificationToken = $('#input[name=__RequestVerificationToken]').val();
        return data;
    };

</script>

<script>
    $(function () {
        $(".js-example-templating").select2({
        });

        $("#Issuefromdatefilter,#Issuetodatefilter").kendoDatePicker({
            format: "dd/MM/yyyy"
        });

        $('#Outtimefilter,#Intimefilter').timepicki();
    });

    $("#btnFilter").click(function(){
        $("#filter_div").toggle();
    });

    $('#Filter_gatepass').click(function () {
        var grid = $('#GatePassApproval-grid').data('kendoGrid');
        grid.dataSource.page(1); //new search. Set page size to 1
        return false;
    });



    $("#Reset_filter_gatepass").click(function(){
     //   $("#filter_div").hide();
        $("#add_gatepass").show();
        $("#Passtypefilter").val('');
        $("#Concernedpersonfilter").val('').trigger('change');
        $("#Studentfilter").val('').trigger('change');
        $("#Stafffilter").val('').trigger('change');
        $("#Intimefilter").val('');
        $("#Outtimefilter").val('');
        $("#Issuefromdatefilter").val('');
        $("#Issuetodatefilter").val('');
        $("#visitorfilter").val('');
        $("#ClassFilter").val('');
       // $("#stufilterClass_div").hide();

         $("#StuFilterSession").val('@Model.CurrentSessionId').trigger('change');
        var grid = $('#GatePassApproval-grid').data('kendoGrid');
        grid.dataSource.page(1); //new search. Set page size to 1
        return false;
    })
   

    $("#StuFilterSession").change(function(){
        $("#ClassFilter").html("");
        if($(this).val() != "")
        {
            $("#stufilterClass_div").show();
            $.ajax({
                type: "GET",
                url: '@Html.Raw(Url.Action("fillGatePassClassesBySession", "GatePass"))',
                data: {"SessionId": $("#StuFilterSession").val()},
                success: function (res) {
                    var html = "<option Value=''>--Select-- </option>";
                    $("#ClassFilter").append(html);
                    if (res.gatePassClassList.length > 0) {
                        $.each(res.gatePassClassList,function(i){
                            var html = "<option Value='"+res.gatePassClassList[i].Value +"'>"
                                                        +res.gatePassClassList[i].Text+"</option>"
                            $("#ClassFilter").append(html);
                        });
                    }
                },
                error: function (xhr, status) {
                    var err = "Error " + " " + status;
                    ToastMessage(err, "error", true, 5000);
                }
            });
        }
        else
            $("#stufilterClass_div").hide();

        if($("#Passtypefilter option:selected").text().indexOf('Student')!=-1)
        {
            $("#stufilterClass_div").show();
        }
        else if($("#Passtypefilter option:selected").text().indexOf('Visitor')!=-1)
        {
            $("#stufilterClass_div").hide();
        }
        else if($("#Passtypefilter option:selected").text().indexOf('Staff')!=-1)
        {
            $("#stufilterClass_div").hide();
        }
        else if($("#Passtypefilter option:selected").text().indexOf('Guardian')!=-1)
        {
            $("#stufilterClass_div").show();
        }
        else
        {
            $("#stufilterClass_div").show();
        }
    });



    $("#Passtypefilter").change(function(){
        $("#Concernedpersonfilter").val('').trigger('change');
        $("#Studentfilter").val('').trigger('change');
        $("#Stafffilter").val('').trigger('change');
        $("#visitorfilter").val('');
        $("#stufilterClass_div").hide();
        if($("#Passtypefilter option:selected").text().indexOf('Student')!=-1)
        {
            $("#stafilter_div").hide();
            $("#stufilter_div").show();
            $("#visfilter_div").hide();
                
        }
        else if($("#Passtypefilter option:selected").text().indexOf('Visitor')!=-1)
        {
            $("#stufilter_div").hide();
            $("#stafilter_div").hide();
            $("#visfilter_div").show();
        }
        else if($("#Passtypefilter option:selected").text().indexOf('Staff')!=-1)
        {
            $("#stufilter_div").hide();
            $("#stafilter_div").show();
            $("#visfilter_div").hide();
        }
        else if($("#Passtypefilter option:selected").text().indexOf('Guardian')!=-1)
        {
            $("#stafilter_div").hide();
            $("#stufilter_div").show();
            $("#visfilter_div").hide();
        }
        else
        {
            $("#stafilter_div").show();
            $("#stufilter_div").show();
            $("#visfilter_div").hide();
        }
    });

    $(document).on("click", ".clsAaproveGatepass", function () {
        var gatepassId=$(this).parent().parent().find('td:nth-child(1)').text()
        $('#ks_loader').show();
        $.ajax({
            type: "POST",
            url: '@Html.Raw(Url.Action("ApprovalAction", "GatePass"))',
            data: { "Id": gatepassId,"status":"Approved" },
            success: function (result) {
                $('#ks_loader').hide();
                if (result.status == "success") {
                    window.location.reload();
                }
            },
            error: function (xhr, status, p3) {
                var err = "Error: " + " " + p3;
                ToastMessage(err, "error", true, 5000);
                $('#ks_loader').hide();
            }
        });
    });

    $(document).on("click", ".clsDisaaproveGatepass", function () {
        var gatepassId=$(this).parent().parent().find('td:nth-child(1)').text()
        $('#ks_loader').show();
        $.ajax({
            type: "POST",
            url: '@Html.Raw(Url.Action("ApprovalAction", "GatePass"))',
            data: { "Id": gatepassId,"status":"Disapproved" },
            success: function (result) {
                $('#ks_loader').hide();
                if (result.status == "success") {
                    window.location.reload();
                }
            },
            error: function (xhr, status, p3) {
                var err = "Error: " + " " + p3;
                ToastMessage(err, "error", true, 5000);
                $('#ks_loader').hide();
            }
        });
    });
     $("#StuFilterSession").val('@Model.CurrentSessionId').trigger('change');
</script>


