﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BAL.BAL.Message
{
    public enum MsgDelayPeriodEnum
    {
        Hours = 1,
        Days = 2
    }
}