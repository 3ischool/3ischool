﻿using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.TriggerEventModule;
using ViewModel.ViewModel.Schedular;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using ViewModel.ViewModel.Messaging;

namespace BAL.BAL.Message
{
    public class WorkflowMessageService : IWorkflowMessageService
    {
        #region fields
        public int count = 0;
        private readonly ITokenizer _tokenizer;
        private readonly ITriggerEventService _ITriggerEventService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly ISubjectService _ISubjectService;
        #endregion

        #region

        public WorkflowMessageService(ITokenizer tokenizer,
            ITriggerEventService TriggerEventService,
            IAddressMasterService AddressMasterService,
            ISubjectService SubjectService
            )
        {
            this._tokenizer = tokenizer;
            this._ITriggerEventService = TriggerEventService;
            this._IAddressMasterService = AddressMasterService;
            this._ISubjectService = SubjectService;
        }


        #endregion

        #region Utilities

        protected virtual int SendNotification(//MessageTemplate messageTemplate,
            EmailAccount emailAccount, int languageId, IEnumerable<Token> tokens,
            string toEmailAddress, string toName,
            string attachmentFilePath = null, string attachmentFileName = null,
            string replyToEmailAddress = null, string replyToName = null, string Cc = null, bool htmlencode = true)
        {
            //if (messageTemplate == null)
            //    throw new ArgumentNullException("messageTemplate");
            //if (emailAccount == null)
            //    throw new ArgumentNullException("emailAccount");

            ////retrieve localized message template data
            //var bcc = messageTemplate.GetLocalized(mt => mt.BccEmailAddresses, languageId);
            //var subject = messageTemplate.GetLocalized(mt => mt.Subject, languageId);
            //var body = messageTemplate.GetLocalized(mt => mt.Body, languageId);

            ////Replace subject and body tokens 
            //var subjectReplaced = _tokenizer.Replace(subject, tokens, false);
            //var bodyReplaced = _tokenizer.Replace(body, tokens, htmlencode);

            ////limit name length
            //toName = CommonHelper.EnsureMaximumLength(toName, 300);

            //var email = new QueuedEmail
            //{
            //    Priority = QueuedEmailPriority.High,
            //    From = emailAccount.Email,
            //    FromName = emailAccount.DisplayName,
            //    To = toEmailAddress,
            //    ToName = toName,
            //    ReplyTo = replyToEmailAddress,
            //    ReplyToName = replyToName,
            //    CC = Cc,
            //    Bcc = bcc,
            //    Subject = subjectReplaced,
            //    Body = bodyReplaced,
            //    AttachmentFilePath = attachmentFilePath,
            //    AttachmentFileName = attachmentFileName,
            //    AttachedDownloadId = messageTemplate.AttachedDownloadId,
            //    CreatedOnUtc = DateTime.UtcNow,
            //    EmailAccountId = emailAccount.Id,
            //    DontSendBeforeDateUtc = !messageTemplate.DelayBeforeSend.HasValue ? null
            //        : (DateTime?)(DateTime.UtcNow + TimeSpan.FromHours(messageTemplate.DelayPeriod.ToHours(messageTemplate.DelayBeforeSend.Value)))
            //};

            //_queuedEmailService.InsertQueuedEmail(email);
            //return email.Id;

            return 0;
        }

        protected virtual string GetActiveMessageTemplate(string messageTemplateName)
        {
            //var messageTemplate = _messageTemplateService.GetMessageTemplateByName(messageTemplateName, storeId);

            //no template found
            //if (messageTemplate == null)
            //  return null;

            //ensure it's active
            // var isActive = messageTemplate.IsActive;
            // if (!isActive)
            //    return null;

            //return messageTemplate;
            return null;
        }

        //protected virtual EmailAccount GetEmailAccountOfMessageTemplate(MessageTemplate messageTemplate, int languageId)
        protected virtual string GetEmailAccountOfMessageTemplate()
        {
            // var emailAccountId = messageTemplate.GetLocalized(mt => mt.EmailAccountId, languageId);
            //some 0 validation (for localizable "Email account" dropdownlist which saves 0 if "Standard" value is chosen)
            //  if (emailAccountId == 0)
            //  emailAccountId = messageTemplate.EmailAccountId;

            //var emailAccount = _emailAccountService.GetEmailAccountById(emailAccountId);
            //if (emailAccount == null)
            //    emailAccount = _emailAccountService.GetEmailAccountById(_emailAccountSettings.DefaultEmailAccountId);
            //if (emailAccount == null)
            //    emailAccount = _emailAccountService.GetAllEmailAccounts().FirstOrDefault();
            // return emailAccount;

            return null;
        }

        #endregion

        /// <summary>
        /// Sends 'New customer' notification message to a store owner
        /// </summary>
        /// <param name="customer">Customer instance</param>
        /// <param name="languageId">Message language identifier</param>
        /// <returns>Queued email identifier</returns>
        public virtual int SendEmailNotificationMessage(SenderInfo senderInfo, string TemplateName)
        {
            if (senderInfo == null)
                throw new ArgumentNullException("SenderInfo");

            var messageTemplate = GetActiveMessageTemplate(TemplateName);
            if (messageTemplate == null)
                return 0;

            //email account
            //  var emailAccount = GetEmailAccountOfMessageTemplate(messageTemplate);

            //tokens
            var tokens = new List<Token>();
            // _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
            // _messageTokenProvider.AddCustomerTokens(tokens, customer);

            //event notification
            //  _eventPublisher.MessageTokensAdded(messageTemplate, tokens);

            //  var toEmail = emailAccount.Email;
            //  var toName = emailAccount.DisplayName;
            //  return SendNotification(messageTemplate, emailAccount,languageId, tokens,toEmail, toName);

            return 0;
        }
        public string SendMessageNotification(string[] regID,string title="", string message="",string Devicetype="",
            string collapseKey = "")
        {
            try
            {
                string applicationID = "AIzaSyAUy5SLz_JavWBPBE4QPOXBLOstY5ZPoO0";
                //string senderId = "416335188242";
               // string deviceId = "fW-XYaBVlrY:APA91bG9LuMwf8Zg2gyBk-SRETu5t6iyCkTM8suCrW79ua6dGYRxWZpf1UJJDoB_NQA-a0xtJdlLL9jtJ5lDzOgAv_IMQnO0wO6FNBSa5YjCL1myk37Wj8TYNUdHNCY5p98zsZYmjYc3";

                //var client = new RestClient("https://fcm.googleapis.com/fcm/send");
                //var request = new RestRequest(Method.POST);
                //request.AddHeader("postman-token", "b9807305-5c83-fc21-ae16-a5062b6922fe");
                //request.AddHeader("cache-control", "no-cache");
                //request.AddHeader("authorization", "key=AIzaSyAUy5SLz_JavWBPBE4QPOXBLOstY5ZPoO0");
                //request.AddHeader("content-type", "application/json");
                //request.AddParameter("application/json", "{\n\t \"registration_ids\":[\"fW-XYaBVlrY:APA91bG9LuMwf8Zg2gyBk-SRETu5t6iyCkTM8suCrW79ua6dGYRxWZpf1UJJDoB_NQA-a0xtJdlLL9jtJ5lDzOgAv_IMQnO0wO6FNBSa5YjCL1myk37Wj8TYNUdHNCY5p98zsZYmjYc3\",\"fYRTdEQp7Yg:APA91bHe7g0QhOHr_gjX80FZAHUXyDMK_hUTTS0AHZDUCMi4xXAWfFzYKv2iPSIioEocJYJK-QFsaNCkZA5tRh18egZUOT260EBxGGa2VSKsLWTZzMrwnBPQTQobwTzVGGP_eCXPDbvJmz2MtmQnUhlSFYk7Bmydag\"],\n \n\n \"collapse_key\" : \"type_a\",\n \"notification\" : {\n     \"body\" : \"First Notification\",\n     \"title\": \"Collapsing A\"\n },\n\n}", ParameterType.RequestBody);
                //IRestResponse response = client.Execute(request);

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var serializer = new JavaScriptSerializer();
                var json = "";
                if (Devicetype == "Android")
                {
                    var data = new
                    {

                        registration_ids = regID,

                        content_available= true,
                        collapse_key = collapseKey,
                        data = new
                        {
                            body = message,
                            title = title,
                            sound = "default",
                            badge=1
                        }

                    };
                     json = serializer.Serialize(data);
                }
                else
                {
                    var data = new
                    {

                        registration_ids = regID,

                        content_available= true,
                        apns_collapse_id = collapseKey,
                        notification = new
                        {
                            body = message,
                            title = title,
                            sound = "default"
                        }

                    };
                     json = serializer.Serialize(data);
                }

               
               
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                // tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                //tRequest.Headers.Add(string.Format("Content-Type:", "application/json"));
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
            return "sdf";
        }
        //public string SendMessageNotification()
        //{
        //    string serverKey = Convert.ToString(WebConfigurationManager.AppSettings["PushNotificationServerKey"]);
        //    try
        //    {
        //        var result = "-1";
        //        string webAddr = Convert.ToString(WebConfigurationManager.AppSettings["PushNotificationWebAddress"]);
        //        string senderId = Convert.ToString(WebConfigurationManager.AppSettings["PushNotificationSenderId"]);
        //        // var regID = "dCqHNytWyzc:APA91bGrPMZRNvPhe0GF6ubi01vRJOxOZX_1Ps2knd2ikACLr6_AA8GJ32wuowZURuTysv2ByxDa60t8IuNGTszlK4zPf3307F-MROJEVfbF6RXTE5nJPmlwn5UrgciQUl0-K9wMuMak";

        //        var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
        //        httpWebRequest.ContentType = "application/json";
        //        httpWebRequest.Headers.Add("Authorization:key=" + serverKey);
        //        // httpWebRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
        //        httpWebRequest.Method = "POST";

        //        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        //        {
        //            string json = "{\"registration_ids\": \"" + regID + "\",\"notification\": {\"Message\": \"" + message + "\"}}";
        //            //registration_ids, array of strings -  to, single recipient
        //            streamWriter.Write(json);
        //            streamWriter.Flush();
        //        }

        //        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        //        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        //        {
        //            result = streamReader.ReadToEnd();
        //        }

        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.ToString());
        //        return "err";
        //    }
        //    return "sdf";
        //}
        public string SendMessageNotification()
        {
            try
            {
                string applicationID = "AIzaSyAUy5SLz_JavWBPBE4QPOXBLOstY5ZPoO0";
                string senderId = "416335188242";
                // string deviceId = "fW-XYaBVlrY:APA91bG9LuMwf8Zg2gyBk-SRETu5t6iyCkTM8suCrW79ua6dGYRxWZpf1UJJDoB_NQA-a0xtJdlLL9jtJ5lDzOgAv_IMQnO0wO6FNBSa5YjCL1myk37Wj8TYNUdHNCY5p98zsZYmjYc3";
                string[] deviceId = { "fW-XYaBVlrY:APA91bG9LuMwf8Zg2gyBk-SRETu5t6iyCkTM8suCrW79ua6dGYRxWZpf1UJJDoB_NQA-a0xtJdlLL9jtJ5lDzOgAv_IMQnO0wO6FNBSa5YjCL1myk37Wj8TYNUdHNCY5p98zsZYmjYc3", "fYRTdEQp7Yg:APA91bHe7g0QhOHr_gjX80FZAHUXyDMK_hUTTS0AHZDUCMi4xXAWfFzYKv2iPSIioEocJYJK-QFsaNCkZA5tRh18egZUOT260EBxGGa2VSKsLWTZzMrwnBPQTQobwTzVGGP_eCXPDbvJmz2MtmQnUhlSFYk7Bmydag" };
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {

                    registration_ids = deviceId,


                    collapse_key = "type_a",
                    notification = new
                    {
                        body = "First Notification",
                        title = "Collapsing A",
                        sound = "default"
                    }
                   
                };

                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                // tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                //tRequest.Headers.Add(string.Format("Content-Type:", "application/json"));
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {

                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
            return "sdf";
        }
        //public string SendMessageNotification(string[] regID,string message)
        //{
        //    string serverKey = Convert.ToString(WebConfigurationManager.AppSettings["PushNotificationServerKey"]);
        //    try
        //    {
        //        var result = "-1";
        //        string webAddr = Convert.ToString(WebConfigurationManager.AppSettings["PushNotificationWebAddress"]);
        //        string senderId = Convert.ToString(WebConfigurationManager.AppSettings["PushNotificationSenderId"]);
        //       // var regID = "dCqHNytWyzc:APA91bGrPMZRNvPhe0GF6ubi01vRJOxOZX_1Ps2knd2ikACLr6_AA8GJ32wuowZURuTysv2ByxDa60t8IuNGTszlK4zPf3307F-MROJEVfbF6RXTE5nJPmlwn5UrgciQUl0-K9wMuMak";

        //        var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
        //        httpWebRequest.ContentType = "application/json";
        //        httpWebRequest.Headers.Add("Authorization:key=" + serverKey);
        //        httpWebRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
        //        httpWebRequest.Method = "POST";

        //        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        //        {
        //            string json = "{\"registration_ids\": \"" + regID + "\",\"notification\": {\"Message\": \"" + message + "\"}}";
        //            //registration_ids, array of strings -  to, single recipient
        //            streamWriter.Write(json);
        //            streamWriter.Flush();
        //        }

        //        var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        //        using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        //        {
        //            result = streamReader.ReadToEnd();
        //        }

        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.ToString());
        //        return "err";
        //    }
        //}
        public void SchedularTrigger(SchedularModel SchedularModel)
        {
            var trigger = _ITriggerEventService.GetAllTriggerEventList(ref count).Where(m => m.TriggerEvent1 == "Schedular" && m.IsActive == true).FirstOrDefault();
            if (trigger != null)
            {
                var triggershooter = trigger.TriggerShooters.Where(m => m.TriggerTemplate != null && m.TriggerTemplate.IsActive == true && m.TriggerTemplate.TriggerMaster != null && m.TriggerTemplate.TriggerMaster.IsActive == true && m.TriggerTemplate.TriggerMaster.TriggerType != null && m.TriggerTemplate.TriggerMaster.TriggerType.IsActive == true &&
                    m.TriggerTemplate.TriggerMaster.TriggerType.TriggerType1 == "Push Notification" && m.TriggerTemplate.TriggerMsgTemplate != null && m.TriggerTemplate.TriggerMsgTemplate.IsActive == true && m.IsActive == true).FirstOrDefault();
                if (triggershooter != null)
                {
                    var triggermessage = triggershooter.TriggerTemplate.TriggerMsgTemplate.TriggerMsgTemplate1;
                    var triggermessageTilte = triggershooter.TriggerTemplate.TriggerMsgTemplate.MsgTemplateName;

                    triggermessage = triggermessage.Replace("%Title%", SchedularModel.EventTitle);
                    triggermessageTilte = triggermessageTilte.Replace("%Detail%", SchedularModel.Description);
                    
                    // var studentclass=_IStudentService.GetAllStudentClasss(ref count).Where(m=>m.Student.StudentStatusDetails.LastOrDefault().StudentStatu.StudentStatus=="Active" && m.ClassId=_ISubjectService.GetAllClassSubjects(ref count).Where(m => m.ClassSubjectId == homeworkdetail.HW.ClassSubjectId).FirstOrDefault().cla)
                    //var contactType = _IAddressMasterService.GetAllContactTypes(ref count).Where(m => m.ContactType1 == "Student").FirstOrDefault();
                    //foreach (var item in Students)
                    //{
                        //var schooluser = _IUserService.GetAllUsers(ref count).Where(m => m.UserTypeId == contactType.ContactTypeId && m.UserContactId == item.StudentId).FirstOrDefault();
                        //if (schooluser != null)
                        //{

                           // var userdeviceIdsArray = _IUserService.GetAllUserDevices(ref count).Where(m => (m.UserId == schooluser.UserContactId) && m.UserDeviceName == "IOS").Select(N => N.UserDeviceUniqueId).ToArray();
                           // var userdeviceIdsArrayAndroid = _IUserService.GetAllUserDevices(ref count).Where(m => m.UserId == schooluser.UserContactId && m.UserDeviceName == "Android").Select(N => N.UserDeviceUniqueId).ToArray();

                           //SendMessageNotification(userdeviceIdsArray, triggermessageTilte, triggermessage, "IOS");
                           // SendMessageNotification(userdeviceIdsArrayAndroid, triggermessageTilte, triggermessage, "Android");
                        //}
                    //}



                }
            }
        }
    }
}