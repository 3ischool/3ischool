﻿using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.BAL.Security
{
    public interface IUserAuthorizationService
    {
        bool IsUserAuthorize(SchoolUser user, string ControllerName, string ActionName, string Action);

    }
}
 