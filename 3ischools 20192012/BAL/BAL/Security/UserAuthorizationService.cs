﻿using DAL.DAL.UserModule;
using KSModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DAL.DAL.Common;
using ViewModel.ViewModel.Security;
using System.Data;
using System.Data.SqlClient;
using DAL.DAL.SettingService;
using BAL.BAL.Common;
//using System;

namespace BAL.BAL.Security
{
    public class UserAuthorizationService : IUserAuthorizationService
    {
        #region fields

        public int count = 0;
        //private readonly KS_ChildEntities db;
        //private readonly string DataSource;
        private string DataSource
        {
            get
            {
                var dtsource = _IConnectionService.Connectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"]));

                return dtsource;
            }
        }

        private KS_ChildEntities Context;
        public KS_ChildEntities db
        {

            get
            {
                if (Context == null)
                {
                    Context = new KS_ChildEntities(DataSource);
                    return Context;
                }
                return Context;
            }
        }
        private string SqlDataSource { get { return _IConnectionService.SqlConnectionstring(Convert.ToString(HttpContext.Current.Session["SchoolDB"])); } }

        public readonly IUserService _IUserService;
        private readonly IUserManagementService _IUserManagementService;
        private readonly IConnectionService _IConnectionService;
        private readonly ICommonMethodService _ICommonMethodService;
        private readonly ISchoolSettingService _ISchoolSettingService;

        #endregion

        #region ctor

        public UserAuthorizationService(IUserService IUserService,
            IUserManagementService IUserManagementService, IConnectionService _IConnectionService,
            ICommonMethodService ICommonMethodService, ISchoolSettingService ISchoolSettingService)
        {
            this._IUserManagementService = IUserManagementService;
            this._IUserService = IUserService;
            this._IConnectionService = _IConnectionService;
            this._ICommonMethodService = ICommonMethodService;
            this._ISchoolSettingService = ISchoolSettingService;

            //HttpContext context = HttpContext.Current;
            //this.DataSource = _IConnectionService.Connectionstring(Convert.ToString(context.Session["SchoolDB"]));
            //this.db = new KS_ChildEntities(DataSource);
        }

        #endregion

        public bool IsUserAuthorize(SchoolUser user, string ControllerName, string ActionName, string Action)
        {
            bool IsAuth = false;
            // get user role 
            if (user != null)
            {
                var userrole = db.UserRoles.FirstOrDefault(p => p.UserRoleId == user.UserRoleId);

                var userroleId = userrole.UserRoleId;
                var userroletypeId = userrole.UserRoleTypeId.ToString();
                // get app form and action
                var dynamicnavigationpath = "";
                  var Is_Lite=  _ISchoolSettingService.GetorSetSchoolData("Is_Lite", "0").AttributeValue == "1" ? true : false;
                if(Is_Lite)
                    dynamicnavigationpath="litenavigationpath";
                else
                    dynamicnavigationpath = "navigationpath";
                //get the Json filepath
                var CustomNavigation = _ISchoolSettingService.GetorSetSchoolData("CustomNavigation", "0").AttributeValue == "1" ? true : false;
                string file = "";
                var SchoolDbId = Convert.ToString(System.Web.HttpContext.Current.Session["SchoolId"]);
                //get the Json filepath  
                if (CustomNavigation)
                {
                    file = System.Web.HttpContext.Current.Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + "/" + SchoolDbId + "/" + Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings["userroleappformjson"]));
                   // file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + "/" + SchoolDbId + "/" + Convert.ToString(WebConfigurationManager.AppSettings["userroleappformjson"]));
                }
                else
                {
                    file = System.Web.HttpContext.Current.Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings["userroleappformjson"]));
                    //file = Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(WebConfigurationManager.AppSettings["userroleappformjson"]));
                }
               // string file = System.Web.HttpContext.Current.Server.MapPath(Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings[dynamicnavigationpath]) + Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings["userroleappformjson"]));
                //deserialize JSON from file
                string Json = System.IO.File.ReadAllText(file);
                System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();
                var list = ser.Deserialize<List<AppFormPermissionModel>>(Json);
                var appformdata = list.Where(p => p.UserRoleTypeId == userroletypeId).FirstOrDefault().AppFormData;
                appformdata = appformdata.Where(p => p.NavigationItemId != null).ToList();

                //find the app form from parent app forms
                var appform = appformdata.Where(p => p.ControllerName == ControllerName
                                    && p.ActionName == ActionName).FirstOrDefault();

                var childappform = new List<List<ChildAppForm>>();
                if (appform == null)
                    childappform = appformdata.Where(p => (p.ControllerName == "" && p.ActionName == "") || (p.ControllerName == null && p.ActionName == null)).Select(p => p.ChildAppForm).ToList();

                var appformid = appform != null ? Convert.ToInt32(appform.AppFormId) : 0;

                //find the app form from child app forms
                if (childappform.Count > 0 && appformid == 0)
                {
                    foreach (var item in childappform)
                    {
                        foreach (var itm in item)
                        {
                            if (itm.ControllerName == ControllerName && itm.ActionName == ActionName)
                            {
                                //var appformactions = itm.Action;
                                //var requiredaction = appformactions.Where(p => p.FormActionType == Action).FirstOrDefault();
                                //appformid = Convert.ToInt32(itm.AppFormParentId);
                                appformid = Convert.ToInt32(itm.AppFormId);
                            }
                                
                        }
                    }
                }

                //find the app form from chidofchild app forms
                var childappform1 = new List<List<ChildAppForm1>>();
                if (childappform.Count > 0 && appformid == 0)
                {
                    foreach (var item in childappform)
                    {
                        childappform1 = item.Where(p => (p.ControllerName == ""
                                            && p.ActionName == "")
                                             || (p.ControllerName == null && p.ActionName == null)).Select(p => p.ChildAppForm1).ToList();
                        if (childappform1.Count > 0)
                        {
                            foreach (var itm in childappform1)
                            {
                                foreach (var ch1 in itm)
                                {
                                    if (ch1.ControllerName == ControllerName && ch1.ActionName == ActionName)
                                    {
                                        appformid = Convert.ToInt32(ch1.AppFormId);
                                        //var appformactions = ch1.Action;
                                        //var requiredaction = appformactions.Where(p => p.FormActionType == Action).FirstOrDefault();
                                        //appformid = Convert.ToInt32(ch1.AppFormParentId);
                                    }
                                }
                            }
                        }
                        if (appformid > 0)
                            break;
                    }
                }


                //get the Json file for action permission of user
               // var SchoolDbId = Convert.ToString(System.Web.HttpContext.Current.Session["SchoolId"]);
                //update file or create 
                var ListUserActionPermission = new List<UserRoleActionPermission>();
                ListUserActionPermission = _IUserManagementService.GetAllUserRoleActionPermissionLessColumns(ref count, user.UserRoleId).ToList();
                _ICommonMethodService.UpdateCreateUserRoleActionPermissionTextFile(SchoolDbId, ListUserActionPermission);

                string path = System.Web.HttpContext.Current.Server.MapPath("~/Images/" + SchoolDbId + "/UserDataFile");
                var pathString = System.IO.Path.Combine(path, "UserRoleActionPermissions.txt");
                //deserialize JSON from file
                string ActionJson = System.IO.File.ReadAllText(pathString);
                System.Web.Script.Serialization.JavaScriptSerializer Actionser = new System.Web.Script.Serialization.JavaScriptSerializer();
                var Actionlist = ser.Deserialize<List<AppFormActionPermissionModel>>(ActionJson);

                if (appformid > 0)
                {
                    //Actionlist = Actionlist.Where(x => x.AppFormId == appformid).ToList();
                    //var UserRoleActionPermission_Ids = Actionlist.Where().Select(x => x.UserRoleActionPermissionId).ToArray();

                    //var formActions = _IUserManagementService.GetAllUserRoleActionPermissions(ref count)
                    //                            .Where(x => UserRoleActionPermission_Ids.Contains(x.UserRoleActionPermissionId)).ToList();

                    //var AppFormAction_Ids = formActions.Select(x => x.AppFormActionId).ToArray();
                    //var AppformActions = _IUserManagementService.GetAllAppFormActions(ref count)
                    //                           .Where(x => AppFormAction_Ids.Contains(x.AppFormActionId)).ToList();


                    if (Action.ToLower().Contains("view"))
                    {
                        // check user role permission
                        var userrolepermissions = Actionlist.Where(x=>x.UserRoleId==userroleId && x.AppFormId ==  appformid).ToList();
                        var isformpermission = userrolepermissions.Where(u => u.AppFormActionId == null).FirstOrDefault();
                        if (isformpermission != null)
                        {
                            if (isformpermission.IsForm == true && isformpermission.IsPermitted == true)
                                IsAuth = true;
                        }
                    }
                    else
                    {
                        var requiredactionid = "";

                        //get the appformactionid from the appform if exist
                        if (appform != null)
                        {
                            // check user role permission
                            var appformactions = appform.Action;
                            var requiredaction = appformactions.Where(p => p.FormActionType == Action).FirstOrDefault();
                            requiredactionid = requiredaction != null ? requiredaction.AppFormActionId : "";
                        }

                        //get the appformactionid from the childappform if exist
                        if (childappform.Count > 0 && childappform1.Count == 0 && requiredactionid == "")
                        {
                            foreach (var item in childappform)
                            {
                                foreach (var itm in item)
                                {
                                    if (itm.ControllerName == ControllerName && itm.ActionName == ActionName)
                                    {
                                        // check user role permission
                                        var appformactions = itm.Action;
                                        var requiredaction = appformactions.Where(p => p.FormActionType == Action).FirstOrDefault();
                                        requiredactionid = requiredaction != null ? requiredaction.AppFormActionId : "";
                                    }
                                }
                            }
                        }

                        //get the appformactionid from the childofchildappform if exist
                        if (childappform1.Count > 0 && requiredactionid == "")
                        {
                            foreach (var item in childappform1)
                            {
                                foreach (var itm in item)
                                {
                                    if (itm.ControllerName == ControllerName && itm.ActionName == ActionName)
                                    {
                                        // check user role permission
                                        var appformactions = itm.Action;
                                        var requiredaction = appformactions.Where(p => p.FormActionType == Action).FirstOrDefault();
                                        requiredactionid = requiredaction != null ? requiredaction.AppFormActionId : "";
                                        appformid = Convert.ToInt32(itm.AppFormParentId);
                                    }
                                }
                            }
                        }

                        if (requiredactionid != null && requiredactionid != "")
                        {

                            var isformpermission = Actionlist.Where(x => x.AppFormActionId == Convert.ToInt32(requiredactionid)
                                                         && x.UserRoleId == userroleId
                                                         && x.AppFormId == appformid).FirstOrDefault();
                            //var isformpermission = _IUserManagementService.GetAllUserRoleActionPermissions(ref count, UserRoleId: userroleId,
                            //    AppFormId: appformid, AppFormActionId: Convert.ToInt32(requiredactionid)).FirstOrDefault();
                            if (isformpermission != null)
                            {
                                if (isformpermission.IsPermitted != null && isformpermission.IsPermitted == true)
                                    IsAuth = true;
                            }
                        }
                    }
                }
            }
            return IsAuth;
        }

        public DataTable dtRoleType(int AppFormId = 0)
        {
            DataTable dt = new DataTable();
            SqlConnection connection;
            SqlCommand command;
            SqlDataAdapter adapter = new SqlDataAdapter();


            string DataSource = "";
            string Password = "";
            string Catatlog = "";
            string UserId = "";
            if ((HttpContext.Current.Request.Url.AbsoluteUri.Contains("localhost"))
                || (HttpContext.Current.Request.Url.AbsoluteUri.Contains("172.16.1.2")))
            {
                //172 super and local 
                DataSource = "172.16.1.2";
                Password = "sa@123";
                Catatlog = "ks_super";
                UserId = "sa";

            }
            else if (HttpContext.Current.Request.Url.AbsoluteUri.Contains("digitech.co.in"))
            {
                //digitech supper
                DataSource = "184.173.237.195,7426";
                Password = "ogLq215%";
                Catatlog = "tst_super";
                UserId = "tst_super_user";
            }
            else
            {
                //live super
                DataSource = "184.173.237.195,7426";
                Password = "Pf8lu52#@$%tQweO97";
                Catatlog = "3is_super";
                UserId = "3is_super";
            }

            string sqlConnectionString = @"Data Source=" + DataSource.ToString() + ";Initial Catalog=" + Catatlog.ToString() + ";User ID=" + UserId.ToString() + ";Password=" + Password.ToString() + "";
            connection = new SqlConnection(sqlConnectionString);
            connection.Open();
            if (AppFormId > 0)
                command = new SqlCommand("Select * from AppFormAction Where appFormId=" + AppFormId, connection);
            else
                command = new SqlCommand("Select * from AppFormAction", connection);
            adapter.SelectCommand = command;
            adapter.Fill(dt);
            connection.Close();
            return dt;
        }

    }
}