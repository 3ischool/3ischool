﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BAL.BAL.SPClasses
{
    public class BookDefaulter
    {
       
    }
     public class BookDefaulterList
    {
        public int BookId { get; set; }
        public string BookTitle { get; set; }
        public string BookAbbr { get; set; }
        public string SubTitle { get; set; }
        public string ISBN { get; set; }
        public string Description { get; set; }
        public int MaterialTypeId { get; set; }
        public string MaterialType { get; set; }
        public string AvialableCopies { get; set; }
        public string AccessionNo { get; set; }
    }
}