﻿using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.ViewModel.Reporting;

namespace BAL.BAL.Reporting
{
    public interface IReportingLogicService
    {
        List<StoreProcedureParameter> GetStoredProcedureParameterList(string SPName, string ReportMasterId, bool? IsStdrpt = null,
            bool? IsGenrpt = null, bool? IsUserrpt = null, bool? IsCol = null, bool? IsPara = null);

        Stylesheet CreateStylesheet();
    }
}
