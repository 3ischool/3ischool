﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BAL.BAL.Reporting
{
    public class StoreProcedureParameter
    {
        public string ParaName { get; set; }

        public dynamic Value { get; set; }
    }
}