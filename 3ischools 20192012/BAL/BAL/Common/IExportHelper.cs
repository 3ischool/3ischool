﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BAL.BAL.Common
{
    public interface IExportHelper
    {
        void ExportToPDF(DataTable dt = null, string Title = "", string Subtitle = "",
                    string GroupingBy = "", string SubTotal = "", string GrandTotal = "", string html = "");
        //void ExportToPDFtoserver(System.Data.DataTable dataTable = null, string Title = "", string Subtitle = "",
        //         string GroupingBy = "", string subTotal = "", string GrandTotal = "", string html = "");

        void ExportToExcel(System.Data.DataTable dt = null, string Title = "", string Subtitle = "",
                              string GroupingBy = "", string SubTotal = "", string GrandTotal = "", string html = "", string SheetName = "",string HeadingName = "");

        void ExportToExcelForLibrary(System.Data.DataTable dt = null, string Title = "", string Subtitle = "",
        string GroupingBy = "", string SubTotal = "", string GrandTotal = "", string html = "", string SheetName = "",string HeadingName = "");

        void ExportToPrint(DataTable dt= null, string Title = "", string Subtitle = "",
                    string GroupingBy = "", string SubTotal = "", string GrandTotal = "",string html ="",string footer = "");


        void ExportToExcelXLSX(System.Data.DataTable dt = null, string Title = "", string Subtitle = "",
                    string GroupingBy = "", string SubTotal = "", string GrandTotal = "", string html = "", string SheetName = "", string HeadingName = "");

        DataTable READExcel(string path);

       }
}
