﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BAL.BAL.Common
{
    public class DatabaseModel
    {
        public string DataSource { get; set; }
        public int DBId { get; set; }
        public string DBName { get; set; }
    }
}