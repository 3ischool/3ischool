insert into AddressType
values
('Parmanent'),
('Coresspondes')


insert into ContactType
values
('Student'),
('Teacher'),
('Non-Teaching'),
('Guardian')


insert into ActivityLogType
values
('Add'),
('Edit'),
('Delete'),
('Print')


insert into AddressCountry
values
('India')


insert into AddressState
values
('Punjab',1),
('Haryana',1),
('Himachal Pardesh',1),
('Delhi',1)


insert into AddressCity
values
('Mohali',1),
('Chandigarh',1),
('Panchkula',2)



insert into BloodGroup
values
('O+'),
('O-'),
('A+'),
('A-'),
('B+'),
('B-'),
('AB+'),
('AB-')


insert into ContactInfoType
values
('Mobile'),
('Landline'),
('Email')


insert into DocumentType
values
('School Leaving Certificate'),
('Academic Certificate'),
('DOB Certificate'),
('Character Certificate'),
('Sports'),
('Images')


insert into GuardianType
values
 ('Father'),
('Mother'),
('Guardian')


insert into Nationality
values
('Indian')


insert into Occupation
values
('Business'),
('Agriculture')


insert into Qualification
values
('Graduation'),
('Post Graduation')


insert into Religion
values
('Sikh'),
('Hindu'),
('Muslim'),
('christian')


insert into Religion
values
('Sikh'),
('Hindu'),
('Muslim'),
('christian')


insert into [Session]
values
('2016-2017','2016-04-01','2017-03-31'),
('2017-2018','2017-04-01','2018-03-31')


insert into StaffType
values
('Teacher'),
('Non-Teaching')


insert into StudentCategory
values
('SC'),
('BC'),
('EWS'),
('General'),
('ST')


insert into SubjectType
values
('1st Language'),
('2nd language'),
('Compulsory'),
('Optional')


insert into TableType
values
('MasterGroup'),
('Master'),
('Transaction'),
('TranasctionDetail'),
('Option'),
('MasterDetail')


insert into TptType
values
('Own'),
('School'),
('Pick & Drop')


insert into TrsType
values
('Dr'),
('Cr')


insert into VehicleType
values
('Bike'),
('Cycle')
