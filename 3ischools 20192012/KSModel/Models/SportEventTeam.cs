//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SportEventTeam
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SportEventTeam()
        {
            this.SportEventTeamPlayers = new HashSet<SportEventTeamPlayer>();
        }
    
        public int SportEventTeamId { get; set; }
        public Nullable<int> SportEventId { get; set; }
        public string TeamName { get; set; }
        public Nullable<int> AgeYears { get; set; }
        public Nullable<int> AgeMonths { get; set; }
        public Nullable<System.DateTime> AgeAsOn { get; set; }
        public string PerformanceRemarks { get; set; }
    
        public virtual SportEvent SportEvent { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SportEventTeamPlayer> SportEventTeamPlayers { get; set; }
    }
}
