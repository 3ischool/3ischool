//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SportAchievementLevel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SportAchievementLevel()
        {
            this.SportStudents = new HashSet<SportStudent>();
        }
    
        public int SportAchievementLevelId { get; set; }
        public string SportAchievementLevel1 { get; set; }
        public Nullable<int> SortingIndex { get; set; }
        public Nullable<int> StarCount { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SportStudent> SportStudents { get; set; }
    }
}
