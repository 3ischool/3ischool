//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MsgSMSAccount
    {
        public int MsgSMSAccountId { get; set; }
        public string AuthKey { get; set; }
        public string SenderId { get; set; }
        public string SMSUri { get; set; }
        public string Route { get; set; }
    }
}
