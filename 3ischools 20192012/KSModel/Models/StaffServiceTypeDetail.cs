//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class StaffServiceTypeDetail
    {
        public int StaffServiceTypeDetailId { get; set; }
        public Nullable<int> StaffId { get; set; }
        public Nullable<int> StaffServiceTypeId { get; set; }
        public Nullable<System.DateTime> ChangeDate { get; set; }
        public string REmarks { get; set; }
    
        public virtual Staff Staff { get; set; }
        public virtual StaffServiceType StaffServiceType { get; set; }
    }
}
