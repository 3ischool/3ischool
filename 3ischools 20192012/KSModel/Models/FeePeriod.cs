//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FeePeriod
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FeePeriod()
        {
            this.DeletedFeeReceipts = new HashSet<DeletedFeeReceipt>();
            this.FeePeriodDetails = new HashSet<FeePeriodDetail>();
            this.FeeReceipts = new HashSet<FeeReceipt>();
        }
    
        public int FeePeriodId { get; set; }
        public string FeePeriodDescription { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<int> FeeFrequencyId { get; set; }
        public Nullable<int> FeePeriodIndex { get; set; }
        public string PrintText { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeletedFeeReceipt> DeletedFeeReceipts { get; set; }
        public virtual FeeFrequency FeeFrequency { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FeePeriodDetail> FeePeriodDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FeeReceipt> FeeReceipts { get; set; }
    }
}
