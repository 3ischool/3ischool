//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PayStaffSalary
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PayStaffSalary()
        {
            this.PayStaffSalaryDetails = new HashSet<PayStaffSalaryDetail>();
            this.PayStaffSalaryPaids = new HashSet<PayStaffSalaryPaid>();
        }
    
        public int PayStaffSalaryId { get; set; }
        public Nullable<int> PaySalaryDaysId { get; set; }
        public Nullable<decimal> TotalSalary { get; set; }
        public Nullable<System.DateTime> AccountedOn { get; set; }
    
        public virtual PaySalaryDay PaySalaryDay { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PayStaffSalaryDetail> PayStaffSalaryDetails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PayStaffSalaryPaid> PayStaffSalaryPaids { get; set; }
    }
}
