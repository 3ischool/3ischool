//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class StoreUser
    {
        public int StoreUserId { get; set; }
        public Nullable<int> StoreId { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual SchoolUser SchoolUser { get; set; }
        public virtual Store Store { get; set; }
    }
}
