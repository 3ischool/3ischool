//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AppFormException
    {
        public int AppFormExceptionId { get; set; }
        public string AppFormEvent { get; set; }
        public string AppFormException1 { get; set; }
        public string AppFormExceptionMsg { get; set; }
        public Nullable<int> AppFormId { get; set; }
    
        public virtual AppForm AppForm { get; set; }
    }
}
