//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SMSNotificationRecipient
    {
        public int MessageRecipientId { get; set; }
        public int RecipientId { get; set; }
        public int MessageId { get; set; }
        public Nullable<int> ContactTypeId { get; set; }
        public Nullable<bool> IsSMS { get; set; }
        public Nullable<bool> IsNotification { get; set; }
        public string SMSMobileNo { get; set; }
    
        public virtual SMSNotification SMSNotification { get; set; }
    }
}
