//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AttendanceStatusType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AttendanceStatusType()
        {
            this.AttendanceStatus = new HashSet<AttendanceStatu>();
            this.AttenStatusContactTypes = new HashSet<AttenStatusContactType>();
            this.StaffAttendanceStatus = new HashSet<StaffAttendanceStatu>();
        }
    
        public int AttendanceStatusTypeId { get; set; }
        public string AttendanceStatusType1 { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AttendanceStatu> AttendanceStatus { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AttenStatusContactType> AttenStatusContactTypes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StaffAttendanceStatu> StaffAttendanceStatus { get; set; }
    }
}
