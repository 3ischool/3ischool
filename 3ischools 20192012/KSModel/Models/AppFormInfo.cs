//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AppFormInfo
    {
        public int AppFormInfoId { get; set; }
        public Nullable<int> AppFormId { get; set; }
        public string AppFormUse { get; set; }
        public string AppFormNote { get; set; }
        public Nullable<int> SortingIndex { get; set; }
    
        public virtual AppForm AppForm { get; set; }
    }
}
