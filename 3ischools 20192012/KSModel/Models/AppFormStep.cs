//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AppFormStep
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AppFormStep()
        {
            this.AppFormStepDetails = new HashSet<AppFormStepDetail>();
        }
    
        public int AppFormStepId { get; set; }
        public Nullable<int> AppFormId { get; set; }
        public Nullable<int> StepNo { get; set; }
        public string StepInfo { get; set; }
    
        public virtual AppForm AppForm { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppFormStepDetail> AppFormStepDetails { get; set; }
    }
}
