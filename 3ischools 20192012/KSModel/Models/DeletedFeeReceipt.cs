//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DeletedFeeReceipt
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DeletedFeeReceipt()
        {
            this.DeletedFeeReceiptDetails = new HashSet<DeletedFeeReceiptDetail>();
        }
    
        public int ReceiptId { get; set; }
        public Nullable<int> ReceiptNo { get; set; }
        public Nullable<System.DateTime> ReceiptDate { get; set; }
        public Nullable<int> StudentId { get; set; }
        public Nullable<int> FeePeriodId { get; set; }
        public Nullable<int> PaymentModeId { get; set; }
        public Nullable<decimal> PaidAmount { get; set; }
        public string InstrumentNo { get; set; }
        public Nullable<System.DateTime> InstrumentDate { get; set; }
        public string BankName { get; set; }
        public Nullable<bool> IsPrint { get; set; }
        public Nullable<int> PrintCount { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<int> ReceiptTypeId { get; set; }
        public Nullable<int> SessionId { get; set; }
        public Nullable<int> DeletedBy { get; set; }
        public Nullable<System.DateTime> VerifiedDateTime { get; set; }
        public string IPAddress { get; set; }
        public string Browser { get; set; }
    
        public virtual FeePeriod FeePeriod { get; set; }
        public virtual PaymentMode PaymentMode { get; set; }
        public virtual ReceiptType ReceiptType { get; set; }
        public virtual Student Student { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeletedFeeReceiptDetail> DeletedFeeReceiptDetails { get; set; }
    }
}
