//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PayCompOffRecord
    {
        public int PayCompOffRecordId { get; set; }
        public Nullable<int> PaySalaryDaysId { get; set; }
        public Nullable<int> CompOffAdded { get; set; }
        public Nullable<int> CompOffAdjusted { get; set; }
    
        public virtual PaySalaryDay PaySalaryDay { get; set; }
    }
}
