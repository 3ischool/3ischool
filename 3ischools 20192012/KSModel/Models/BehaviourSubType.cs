//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BehaviourSubType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BehaviourSubType()
        {
            this.BehaviourIncidents = new HashSet<BehaviourIncident>();
        }
    
        public int BehaviourSubTypeId { get; set; }
        public string BehaviourSubCategory { get; set; }
        public Nullable<int> BehaviourId { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual Behaviour Behaviour { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BehaviourIncident> BehaviourIncidents { get; set; }
    }
}
