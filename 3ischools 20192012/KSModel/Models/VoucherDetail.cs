//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class VoucherDetail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VoucherDetail()
        {
            this.VoucherAdjustments = new HashSet<VoucherAdjustment>();
        }
    
        public int VoucherDetailId { get; set; }
        public Nullable<int> VoucherId { get; set; }
        public Nullable<int> AccountId { get; set; }
        public string Narration { get; set; }
        public Nullable<decimal> Credit { get; set; }
        public Nullable<decimal> Debit { get; set; }
    
        public virtual Account Account { get; set; }
        public virtual AccountVoucher AccountVoucher { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VoucherAdjustment> VoucherAdjustments { get; set; }
    }
}
