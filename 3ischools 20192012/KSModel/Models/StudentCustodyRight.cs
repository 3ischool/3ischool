//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class StudentCustodyRight
    {
        public int StudentCustodyRightId { get; set; }
        public Nullable<int> StudentId { get; set; }
        public Nullable<int> CustodyRightId { get; set; }
        public Nullable<bool> IsActive { get; set; }
    
        public virtual CustodyRight CustodyRight { get; set; }
        public virtual Student Student { get; set; }
    }
}
