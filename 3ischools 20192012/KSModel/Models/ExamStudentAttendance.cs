//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExamStudentAttendance
    {
        public int ExamStudentAttendanceId { get; set; }
        public Nullable<int> ExamDateSheetId { get; set; }
        public Nullable<int> StudentId { get; set; }
        public Nullable<int> TotalDays { get; set; }
        public int PresentDays { get; set; }
        public Nullable<System.DateTime> AsOn { get; set; }
    
        public virtual ExamDateSheet ExamDateSheet { get; set; }
        public virtual Student Student { get; set; }
    }
}
