//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class StockPackageDetail
    {
        public int PackageDetailId { get; set; }
        public Nullable<int> ProductId { get; set; }
        public Nullable<int> StockPackageId { get; set; }
        public Nullable<decimal> Quantity { get; set; }
    
        public virtual Product Product { get; set; }
        public virtual StockPackage StockPackage { get; set; }
    }
}
