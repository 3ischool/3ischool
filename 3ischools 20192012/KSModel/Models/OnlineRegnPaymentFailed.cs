//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KSModel.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class OnlineRegnPaymentFailed
    {
        public int PaymentFailedId { get; set; }
        public string PaymentFailedData { get; set; }
        public int PrePaymentDataId { get; set; }
        public System.DateTime LogDateTime { get; set; }
    
        public virtual OnlineRegnPrePaymentData OnlineRegnPrePaymentData { get; set; }
    }
}
