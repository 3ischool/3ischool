﻿using BAL.BAL.Common;
using BAL.BAL.Message;
using BAL.BAL.Security;
//using BAL.BAL.SPClasses;
using DAL.DAL.ActivityLogModule;
using DAL.DAL.AddressModule;
using DAL.DAL.CatalogMaster;
using DAL.DAL.Common;
using DAL.DAL.ContactModule;
using DAL.DAL.ErrorLogModule;
using DAL.DAL.FeeModule;
using DAL.DAL.GuardianModule;
using DAL.DAL.Kendo;
using DAL.DAL.LateFeeModule;
using DAL.DAL.SettingService;
using DAL.DAL.StaffModule;
using DAL.DAL.StudentModule;
using DAL.DAL.TransportModule;
using DAL.DAL.TriggerEventModule;
using DAL.DAL.UserModule;
using KSModel.Models;
using ViewModel.ViewModel.Fee;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using ExcelDataReader;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
//using OfficeOpenXml;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System.Collections;
//using IronPdf;
using SelectPdf;
using DAL.DAL.Message;

namespace FeeModule
{
    public class FeeMethods : Controller
    {

        #region fields

        public int count = 0;
        private readonly IUserService _IUserService;
        private readonly IConnectionService _IConnectionService;
        private readonly IDropDownService _IDropDownService;
        private readonly IActivityLogMasterService _IActivityLogMasterService;
        private readonly IActivityLogService _IActivityLogService;
        private readonly ILogService _Logger;
        private readonly IWebHelper _WebHelper;
        private readonly IFeeService _IFeeService;
        private readonly ILateFeeService _ILateFeeService;
        private readonly ISchoolSettingService _ISchoolSettingService;
        private readonly ICatalogMasterService _ICatalogMasterService;
        private readonly IStudentService _IStudentService;
        private readonly IStudentAddOnService _IStudentAddOnService;
        private readonly IUserAuthorizationService _IUserAuthorizationService;
        private readonly ICustomEncryption _ICustomEncryption;
        private readonly IGuardianService _IGuardianService;
        private readonly IAddressMasterService _IAddressMasterService;
        private readonly IContactService _IContactService;
        private readonly IStudentMasterService _IStudentMasterService;
        private readonly IBusService _IBusService;
        private readonly ITransportService _ITransportService;
        private readonly ISchoolDataService _ISchoolDataService;
        private readonly IExportHelper _IExportHelper;
        private readonly IAddressService _IAddressService;
        private readonly ITriggerEventService _ITriggerEventService;
        private readonly IWorkflowMessageService _IWorkflowMessageService;
        private readonly ISMSSender _ISMSSender;
        private readonly ICommonMethodService _ICommonMethodService;
        private readonly IStaffService _IStaffService;

        #endregion

        #region ctor

        public FeeMethods(IUserService IUserService,
            IConnectionService IConnectionService,
            IDropDownService IDropDownService,
            IActivityLogMasterService IActivityLogMasterService,
            IActivityLogService IActivityLogService,
            ILogService Logger, ISchoolDataService ISchoolDataService,
            IWebHelper WebHelper,
            IFeeService IFeeService,
            ILateFeeService ILateFeeService,
            ISchoolSettingService ISchoolSettingService,
            ICatalogMasterService ICatalogMasterService,
            IStudentService IStudentService,
            IStudentAddOnService IStudentAddOnService,
            IUserAuthorizationService IUserAuthorizationService,
            ICustomEncryption ICustomEncryption,
            IGuardianService IGuardianService,
            IAddressMasterService IAddressMasterService,
            IContactService IContactService,
            IStudentMasterService IStudentMasterService,
            IBusService IBusService,
            ITransportService ITransportService, IExportHelper IExportHelper, IAddressService IAddressService,
            ITriggerEventService ITriggerEventService,
            IWorkflowMessageService IWorkflowMessageService,
             ISMSSender ISMSSender, ICommonMethodService ICommonMethodService,
            IStaffService IStaffService)
        {
            this._IUserService = IUserService;
            this._IConnectionService = IConnectionService;
            this._IDropDownService = IDropDownService;
            this._IActivityLogService = IActivityLogService;
            this._IActivityLogMasterService = IActivityLogMasterService;
            this._Logger = Logger;
            this._WebHelper = WebHelper;
            this._IFeeService = IFeeService;
            this._ISchoolSettingService = ISchoolSettingService;
            this._ICatalogMasterService = ICatalogMasterService;
            this._IStudentService = IStudentService;
            this._IStudentAddOnService = IStudentAddOnService;
            this._ILateFeeService = ILateFeeService;
            this._IUserAuthorizationService = IUserAuthorizationService;
            this._ICustomEncryption = ICustomEncryption;
            this._IGuardianService = IGuardianService;
            this._IAddressMasterService = IAddressMasterService;
            this._IContactService = IContactService;
            this._IStudentMasterService = IStudentMasterService;
            this._IBusService = IBusService;
            this._ITransportService = ITransportService;
            this._ISchoolDataService = ISchoolDataService;
            this._IExportHelper = IExportHelper;
            this._IAddressService = IAddressService;
            this._ITriggerEventService = ITriggerEventService;
            this._IWorkflowMessageService = IWorkflowMessageService;
            this._ISMSSender = ISMSSender;
            this._ICommonMethodService = ICommonMethodService;
            this._IStaffService = IStaffService;
        }

        #endregion

        #region utility

        public void SetSessionData(string name)
        {
            var user = _IUserService.GetUserByEmail(name);
            HttpContext.Session["UserId"] = user.UserName;

            var logininfo = _IUserService.GetAllUserLoginInfos(ref count, user.UserId).LastOrDefault();
            if (logininfo != null)
                HttpContext.Session["LoginInfoId"] = logininfo.LoginInfoId;
        }
        public string FormatDate(string Date)
        {
            string sdisplayTime = Date.Trim('"');
            var dd = Date.Split('/');
            sdisplayTime = dd[1] + "-" + dd[0] + "-" + dd[2];
            return (sdisplayTime);
        }
        #endregion

        #region Methods

        #region Group Fee Head

        public GroupFeeHeadModel preparefeeheadmodel(GroupFeeHeadModel model)
        {
            // prepare model 
            // Drop Down Lists
            model.AvailableGroupFeeType = _IDropDownService.GroupFeeTypeList();
            model.AvailableReceiptTypes = _IDropDownService.RecieptTypeListForGroupFeehead();
            model.AvailableFeeType = _IDropDownService.FeeTypeList();
            model.AvailableFeeClassGroup = _IDropDownService.FeeClassGroupList();
            model.AvailableGroupFeeTypeDropTable= _IDropDownService.GroupFeeTypeDropTableList();

            var Dropdownlist = new List<SelectListItem>();
            Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            var usedConcIdlist = _IFeeService.GetAllGrouptfeeHeadConsessions(ref count).Where(n => n.ConcessionId != null).Select(m => m.ConcessionId).ToArray();
            foreach (var item in _IStudentAddOnService.GetAllConsessions(ref count, Status: true, ReceipttypeId: 0, GeneralConcession: false).Where(m => !usedConcIdlist.Contains(m.ConsessionId)))
                Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.Consession1, Value = item.ConsessionId.ToString() });


            model.AvailableConcessionList = Dropdownlist;
            // model.AvailableFeeHead = _IDropDownService.FeeHeadList();
            var Allfeeheadtype = _IFeeService.GetAllFeeHeadTypes(ref count);
            model.IsLate = false;
            model.IsDiscount = false;
            model.IsSingleTime = false;
            var feeheadtypeId = 0;
            //if (Allfeeheadtype.Count > 0)
            //{
            //    var feeheadtype = Allfeeheadtype.Where(m => m.FeeHeadType1.Contains("StudentFee")).FirstOrDefault();
            //    if (feeheadtype != null)
            //    {
            //        feeheadtypeId = feeheadtype.FeeHeadTypeId;
            //    }
            //}

            //List<SelectListItem> Dropdownlist = new List<SelectListItem>();
            //Dropdownlist.Add(new SelectListItem { Selected = false, Text = "---Select---", Value = "" });
            //// feeheads
            //var feeheads = _IFeeService.GetAllFeeHeads(ref count, FeeHeadTypeId: feeheadtypeId);
            //feeheads = feeheads.Where(f => !f.FeeHead1.Contains("Late") && !f.FeeHead1.Contains("Pending")).ToList();
            //foreach (var item in feeheads)
            //    Dropdownlist.Add(new SelectListItem { Selected = false, Text = item.FeeHead1, Value = item.FeeHeadId.ToString() });
            //model.AvailableFeeHead = Dropdownlist;
            // curency icon
            model.CurrencyIcon = _ISchoolSettingService.GetAttributeValue("CurrencyIcon");
            var currentsession = _ICatalogMasterService.GetCurrentSession();
            model.Sessionstartdate = currentsession.StartDate.ToString();
            return model;
        }

        public GroupFeeHeadModel FeeHeadData(SchoolUser user)
        {
            var model = new GroupFeeHeadModel();
            model = preparefeeheadmodel(model);
            model.AvailableConcessionAmountTypeList = _IDropDownService.AmounttypeList();
            model.IsAuthToAddGroupFeeType = _IUserAuthorizationService.IsUserAuthorize(user, "Fee", "FeeHead", "Add Record");
            model.IsAuthToAdd = _IUserAuthorizationService.IsUserAuthorize(user, "Fee", "FeeHead", "Add Record");
            model.IsAuthToEdit = _IUserAuthorizationService.IsUserAuthorize(user, "Fee", "FeeHead", "Edit Record");
            model.IsAuthToClose = _IUserAuthorizationService.IsUserAuthorize(user, "Fee", "FeeHead", "Close");
            model.IsAuthToDelete = _IUserAuthorizationService.IsUserAuthorize(user, "Fee", "FeeHead", "Delete Record");
            return model;
        }
         
        public string checkgroupfeeheadmodelerrors(GroupFeeHeadModel model)
        {
            string validation = "";
            if (model.GroupFeeTypeId == null || model.GroupFeeTypeId == 0)
                validation = "Please select Group FeeType ε";

            if (model.FeeHeadId == null || model.GroupFeeTypeId == 0)
                validation = "Please select FeeHead ε";

            if (!model.Date.HasValue)
                validation = "Please enter Date ε";
            else
            {
                //if (model.Date.Value < DateTime.UtcNow.Date)
                //    validation = "Effecive date should not be less than today e";
            }

            if (model.IsPending == false && model.IsLate == false)
            {
                if (!(model.Amt > 0))
                    validation = "Please enter Amount ε";
            }

            if (!string.IsNullOrEmpty(model.EndDate))
                validation = "This Fee head is already closed ε";

            // encrypt ids
            // check id exist
            if (!string.IsNullOrEmpty(model.EncGroupFeeHeadId))
            {
                // decrypt Id
                var qsid = _ICustomEncryption.base64d(model.EncGroupFeeHeadId);
                int groupfeehead_id = 0;
                if (int.TryParse(qsid, out groupfeehead_id))
                    model.GroupFeeHeadId = groupfeehead_id;
            }
            // check id exist
            if (!string.IsNullOrEmpty(model.EncGroupFeeHeadDetailId))
            {
                // decrypt Id
                var qsid = _ICustomEncryption.base64d(model.EncGroupFeeHeadDetailId);
                int groupfeeheaddetail_id = 0;
                if (int.TryParse(qsid, out groupfeeheaddetail_id))
                    model.GroupFeeHeadDetailId = groupfeeheaddetail_id;
            }

            if (model.GroupFeeTypeId > 0 && model.FeeHeadId > 0)
            {
                // get all group fee head and check if Is pending and is late already set or not
                var allgroupfeeheadbygroupfeetype = _IFeeService.GetAllGroupFeeHeads(ref count, model.GroupFeeTypeId);
                if (model.IsPending == true)
                {
                    var IsPendingHead = allgroupfeeheadbygroupfeetype.Where(g => g.IsPending == true).FirstOrDefault();
                    if (IsPendingHead != null)
                    {
                        //check  group fee head detail end date not passed
                        var groupfeeheaddetail = IsPendingHead.GroupFeeHeadDetails.ToList();
                        groupfeeheaddetail = groupfeeheaddetail.Where(g => g.EndDate == null || (g.EndDate != null && g.EndDate >= model.Date)).ToList();
                        if (groupfeeheaddetail.Count > 0)
                            validation = "Pending fee head already exist ε";
                    }
                }

                //if (model.IsLate == true)
                //{
                //    var IsLateFeeHead = allgroupfeeheadbygroupfeetype.Where(g => g.IsLateFee == true).FirstOrDefault();
                //    if (IsLateFeeHead != null)
                //    {
                //        //check  group fee head detail end date not passed
                //        var groupfeeheaddetail = IsLateFeeHead.GroupFeeHeadDetails.ToList();
                //        groupfeeheaddetail = groupfeeheaddetail.Where(g => g.EndDate == null || (g.EndDate != null && g.EndDate >= model.Date)).ToList();
                //        if (groupfeeheaddetail.Count > 0)
                //            validation = "Late fee head already exist e";
                //    }
                //}

                //var existGroupFeeHead = allgroupfeeheadbygroupfeetype.Where(g => g.FeeHeadId == model.FeeHeadId && g.GroupFeeHeadId != model.GroupFeeHeadId).FirstOrDefault();
                //if (existGroupFeeHead != null)
                //{
                //    var GroupFeeHeadDetailList = existGroupFeeHead.GroupFeeHeadDetails.ToList();
                //    GroupFeeHeadDetailList = GroupFeeHeadDetailList.Where(g => g.EndDate == null || (g.EndDate != null && g.EndDate >= model.Date)).ToList();
                //    if (GroupFeeHeadDetailList.Count > 0)
                //        validation = "Group Fee Head already exist with same datee";
                //}
            }

            return validation;
        }
        public ActionResult FeeHeadSave(GroupFeeHeadModel model, SchoolUser user,HttpRequestBase Request)
        {
            // Grid array
            string validation = checkgroupfeeheadmodelerrors(model);
            if (string.IsNullOrEmpty(validation))
            {
                // Activity log type add , Edit , Delete
                var logtype = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Add").FirstOrDefault();
                var logtypeedit = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Edit").FirstOrDefault();
                var logtypedel = _IActivityLogMasterService.GetAllActivityLogTypes(ref count, "Delete").FirstOrDefault();
                // ClassPeriod log Table
                var groupFeeHeadtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "GroupFeeHead").FirstOrDefault();
                // classPeriodDetail log Table
                var groupFeeHeaddetailtable = _IActivityLogMasterService.GetAllTableMasters(ref count, TableName: "GroupFeeHeadDetail").FirstOrDefault();
                // objects
                GroupFeeHead GroupFeeHead = new GroupFeeHead();
                GroupFeeHeadDetail GroupFeeHeadDetail = new GroupFeeHeadDetail();
                var ReceipttypeFeehead = _IFeeService.GetAllReceiptTypeFeeHeads(ref count).Where(m => m.FeeHeadId == model.FeeHeadId && m.ReceiptTypeId == model.RecieptTypeId).FirstOrDefault();
                var receipttypefeeeheadId = 0;
                if (ReceipttypeFeehead != null)
                {
                    receipttypefeeeheadId = ReceipttypeFeehead.ReceiptTypeFeeHeadId;
                }
                if (model.GroupFeeHeadId == null || model.GroupFeeHeadId == 0)
                {
                    // check authorization
                    //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Fee", "FeeHead", "Add Record");
                    //if (!isauth)
                    //    return RedirectToAction("AccessDenied", "Error");
                    var existGroupFeeHeadDetail = new GroupFeeHeadDetail();
                    // check Group fee Head Exist and Group Fee Head Detail Y/N
                    var existGroupFeeHead = _IFeeService.GetAllGroupFeeHeads(ref count, model.GroupFeeTypeId, model.FeeHeadId, ReceiptTypeId: (int)model.RecieptTypeId).FirstOrDefault();
                    if (existGroupFeeHead != null)
                        existGroupFeeHeadDetail = existGroupFeeHead.GroupFeeHeadDetails.Where(f => f.EffectiveDate <= model.Date && f.EndDate >= model.Date).FirstOrDefault();
                    else
                        existGroupFeeHeadDetail = null;
                    if (existGroupFeeHead == null && existGroupFeeHeadDetail == null)
                    {
                        // group fee head
                        GroupFeeHead.FeeHeadId = model.FeeHeadId;
                        GroupFeeHead.GroupFeeTypeId = model.GroupFeeTypeId;
                        GroupFeeHead.IsPending = true;
                        GroupFeeHead.IsLateFee = model.IsLate;
                        GroupFeeHead.IsCommon = true;
                        GroupFeeHead.IsDiscountable = model.IsDiscount;
                        GroupFeeHead.IsSingleTime = model.IsSingleTime;
                        GroupFeeHead.ReceiptTypeId = model.RecieptTypeId;
                        GroupFeeHead.ReceiptTypeFeeHeadId = receipttypefeeeheadId;
                        _IFeeService.InsertGroupFeeHead(GroupFeeHead);
                        if (groupFeeHeadtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, groupFeeHeadtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(GroupFeeHead.GroupFeeHeadId, groupFeeHeadtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add GroupFeeHead with GroupFeeHeadId:{0} and GroupFeeTypeId:{1}", GroupFeeHead.GroupFeeHeadId, GroupFeeHead.GroupFeeTypeId), Request.Browser.Browser);
                        }

                        // group fee head detail
                        GroupFeeHeadDetail.Amount = model.Amt;
                        GroupFeeHeadDetail.NewStdAmount = model.NewAmt;
                        GroupFeeHeadDetail.EffectiveDate = model.Date;
                        GroupFeeHeadDetail.EndDate = null;
                        GroupFeeHeadDetail.GroupFeeHeadId = GroupFeeHead.GroupFeeHeadId;
                        GroupFeeHeadDetail.IsDefault = true;
                        GroupFeeHeadDetail.EWSDiscount = model.EWSDiscount;
                        _IFeeService.InsertGroupFeeHeadDetail(GroupFeeHeadDetail);
                        if (groupFeeHeaddetailtable != null)
                        {
                            // table log saved or not
                            var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, groupFeeHeaddetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                            // save activity log
                            if (tabledetail != null)
                                _IActivityLogService.SaveActivityLog(GroupFeeHeadDetail.GroupFeeHeadDetailId, groupFeeHeaddetailtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add GroupFeeHeadDetail with GroupFeeHeadDetailId:{0} and GroupFeeHeadId:{1}", GroupFeeHeadDetail.GroupFeeHeadDetailId, GroupFeeHeadDetail.GroupFeeHeadId), Request.Browser.Browser);
                        }
                    }
                    else
                    {

                        if (existGroupFeeHeadDetail == null)
                        {
                            existGroupFeeHead.IsPending = true;
                            existGroupFeeHead.IsLateFee = model.IsLate;
                            existGroupFeeHead.IsDiscountable = model.IsDiscount;
                            existGroupFeeHead.IsCommon = true;
                            existGroupFeeHead.IsSingleTime = model.IsSingleTime;
                            existGroupFeeHead.ReceiptTypeFeeHeadId = receipttypefeeeheadId;
                            existGroupFeeHead.ReceiptTypeId = model.RecieptTypeId;
                            _IFeeService.UpdateGroupFeeHead(existGroupFeeHead);
                            if (groupFeeHeadtable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, groupFeeHeadtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(existGroupFeeHead.GroupFeeHeadId, groupFeeHeadtable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update GroupFeeHead with GroupFeeHeadId:{0} and GroupFeeTypeId:{1}", existGroupFeeHead.GroupFeeHeadId, existGroupFeeHead.GroupFeeTypeId), Request.Browser.Browser);
                            }

                            GroupFeeHeadDetail.Amount = model.Amt;
                            GroupFeeHeadDetail.NewStdAmount = model.NewAmt;
                            GroupFeeHeadDetail.EffectiveDate = model.Date;
                            GroupFeeHeadDetail.EndDate = null;
                            GroupFeeHeadDetail.GroupFeeHeadId = existGroupFeeHead.GroupFeeHeadId;
                            GroupFeeHeadDetail.IsDefault = true;
                            GroupFeeHeadDetail.EWSDiscount = model.EWSDiscount;
                            _IFeeService.InsertGroupFeeHeadDetail(GroupFeeHeadDetail);
                            if (groupFeeHeaddetailtable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, groupFeeHeaddetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(GroupFeeHeadDetail.GroupFeeHeadDetailId, groupFeeHeaddetailtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Add GroupFeeHeadDetail with GroupFeeHeadDetailId:{0} and GroupFeeHeadId:{1}", GroupFeeHeadDetail.GroupFeeHeadDetailId, GroupFeeHeadDetail.GroupFeeHeadId), Request.Browser.Browser);
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                status = "failed",
                                data = "If you want to change Feehead, add Effective Date greater than previous End Date ε"
                            });
                        }
                    }
                }
                else
                {
                    // check authorization
                    //var isauth = _IUserAuthorizationService.IsUserAuthorize(user, "Fee", "FeeHead", "Edit Record");
                    //if (!isauth)
                    //    return RedirectToAction("AccessDenied", "Error");

                    // get Group Fee Head
                    GroupFeeHead = _IFeeService.GetGroupFeeHeadById((int)model.GroupFeeHeadId, true);

                    // existing Group Fee Head Detail
                    GroupFeeHeadDetail = _IFeeService.GetGroupFeeHeadDetailById((int)model.GroupFeeHeadDetailId, true);

                    if (GroupFeeHead != null && GroupFeeHeadDetail != null)
                    {
                        // check into FeeReceipt Detail
                        var feereceiptdetail = _IFeeService.GetAllFeeReceiptDetails(ref count, GroupFeeHeadId: GroupFeeHead.GroupFeeHeadId);
                        var feereciptids = feereceiptdetail.Select(f => f.ReceiptId).ToArray();
                        // get all feereipts
                        var feereceipts = _IFeeService.GetAllFeeReceipts(ref count);
                        feereceipts = feereceipts.Where(f => feereciptids.Contains(f.ReceiptId) && f.ReceiptDate >= GroupFeeHeadDetail.EffectiveDate).ToList();
                        // check date exist 
                        if (feereceipts.Count == 0)
                        {
                            //if (GroupFeeHead.FeeHeadId != model.FeeHeadId)
                            //{
                            GroupFeeHead.FeeHeadId = model.FeeHeadId;
                            GroupFeeHead.IsPending = true;
                            GroupFeeHead.IsLateFee = model.IsLate;
                            GroupFeeHead.IsDiscountable = model.IsDiscount;
                            GroupFeeHead.IsCommon = true;
                            GroupFeeHead.IsSingleTime = model.IsSingleTime;
                            GroupFeeHead.ReceiptTypeId = model.RecieptTypeId;
                            GroupFeeHead.ReceiptTypeFeeHeadId = receipttypefeeeheadId;
                            _IFeeService.UpdateGroupFeeHead(GroupFeeHead);
                            if (groupFeeHeadtable != null)
                            {
                                // table log saved or not
                                var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, groupFeeHeadtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                // save activity log
                                if (tabledetail != null)
                                    _IActivityLogService.SaveActivityLog(GroupFeeHead.GroupFeeHeadId, groupFeeHeadtable.TableId, logtypeedit.ActivityLogTypeId, user, String.Format("Update GroupFeeHead with GroupFeeHeadId:{0} and GroupFeeTypeId:{1}", GroupFeeHead.GroupFeeHeadId, GroupFeeHead.GroupFeeTypeId), Request.Browser.Browser);
                            }
                            //}

                            if (GroupFeeHeadDetail.Amount != model.Amt || GroupFeeHeadDetail.NewStdAmount != model.NewAmt || GroupFeeHeadDetail.EffectiveDate.Value != model.Date.Value || GroupFeeHeadDetail.EWSDiscount != model.EWSDiscount)
                            {
                                // update exsiting entry
                                GroupFeeHeadDetail.Amount = model.Amt;
                                GroupFeeHeadDetail.NewStdAmount = model.NewAmt;
                                GroupFeeHeadDetail.EffectiveDate = model.Date;
                                GroupFeeHeadDetail.EWSDiscount = model.EWSDiscount;
                                _IFeeService.UpdateGroupFeeHeadDetail(GroupFeeHeadDetail);
                                if (groupFeeHeaddetailtable != null)
                                {
                                    // table log saved or not
                                    var tabledetail = _IActivityLogMasterService.GetAllTableMasterDetails(ref count, groupFeeHeaddetailtable.TableId, DateTime.UtcNow, true).LastOrDefault();
                                    // save activity log
                                    if (tabledetail != null)
                                        _IActivityLogService.SaveActivityLog(GroupFeeHeadDetail.GroupFeeHeadDetailId, groupFeeHeaddetailtable.TableId, logtype.ActivityLogTypeId, user, String.Format("Update GroupFeeHeadDetail with GroupFeeHeadDetailId:{0} and GroupFeeHeadId:{1}", GroupFeeHeadDetail.GroupFeeHeadDetailId, GroupFeeHeadDetail.GroupFeeHeadId), Request.Browser.Browser);
                                }
                            }
                        }
                        else
                        {
                            // check if Fee Head Changed By User
                            if (GroupFeeHead.FeeHeadId != model.FeeHeadId)
                                return Json(new
                                {
                                    status = "failed",
                                    data = "If you want to change Feehead, close it and add new Feeheade"
                                });
                            else if (GroupFeeHeadDetail.Amount != model.Amt)
                            {
                                if (GroupFeeHeadDetail.EffectiveDate == model.Date)
                                    return Json(new
                                    {
                                        status = "failed",
                                        data = "You can't change the Feehead amount.Group Feehead already used in FeeReceipte"
                                    });
                                else
                                    return Json(new
                                    {
                                        status = "failed",
                                        data = "If you want to change Feehead amount, close it and add new Feeheade"
                                    });
                            }
                        }
                    }
                }

                return Json(new
                {
                    status = "success",
                    msg = "Group Fee Head has been saved successfully",
                    GroupFeeHeadId = _ICustomEncryption.base64e(GroupFeeHead.GroupFeeHeadId.ToString()),
                    GroupFeeHeadDetailId = _ICustomEncryption.base64e(GroupFeeHeadDetail.GroupFeeHeadDetailId.ToString())
                });
            }

            return Json(new
            {
                status = "failed",
                data = validation
            });
        }
        #endregion

        #region Feereceipt
        public FeeReceiptListFilterModel preparefeereceiptlist(FeeReceiptListFilterModel model)
        {
            model.AvailableClasses = _IDropDownService.ClassList();
            model.AvailableStudents.Add(new SelectListItem { Selected = true, Text = "--Select--", Value = "" });
            model.AvailableFeePeriods.Add(new SelectListItem { Selected = true, Text = "--Select--", Value = "" });
            model.AvailableReceiptTypes = _IDropDownService.RecieptTypeList();
            model.AvailableStandard = _IDropDownService.StandardList();
            model.IsAllClasses = true;
            model.AvailableSession = _IDropDownService.SessionList();
            var currentsession = _ICatalogMasterService.GetCurrentSession();
            model.SessionId = currentsession.SessionId;
            //values for kendo paging
            model.gridPageSizes = _ISchoolSettingService.GetAttributeValue("gridPageSizes");
            model.defaultGridPageSize = _ISchoolSettingService.GetAttributeValue("defaultGridPageSize");
            // curency icon
            model.CurrencyIcon = _ISchoolSettingService.GetAttributeValue("CurrencyIcon");

            return model;
        }

        public ActionResult StudentAdmnoByStudentId(string Student)
        {
            var StudentId = Convert.ToInt32(Student);
            var student = _IStudentService.GetStudentById(StudentId);
            if (student != null)
            {
                return Json(new
                {
                    status = "success",
                    AdmnNo = student.AdmnNo,
                });
            }
            else
            {
                return Json(new
                {
                    status = "failed",
                    msg = "No Student found"
                });
            }
        }

        public ActionResult StudentListbyClass(string Class, string Id, bool CurrentSession)
        {
            int student_id = 0;
            if (!string.IsNullOrEmpty(Id))
                int.TryParse(_ICustomEncryption.base64d(Id), out student_id);

            if (string.IsNullOrEmpty(Class))
                return Json(new
                {
                    status = "failed",
                    msg = "Class should not be empty"
                });

            int classid = Convert.ToInt32(Class);
            // current session
            var Session = new Session();
            if (CurrentSession)
                Session = _ICatalogMasterService.GetCurrentSession();
            var studentlist = _IStudentService.StudentListbyClassId(classid, student_id, Session.SessionId, AdmnStatus: true);
            var studentStatusDetail = _IStudentMasterService.GetAllStudentStatusDetail(ref count);

            IList<SelectListItem> StudentDDL = new List<SelectListItem>();
            foreach (var item in studentlist)
            {
                if (string.IsNullOrEmpty(item.Value))
                {
                    StudentDDL.Add(item);
                    continue;
                }

                var IsStudentActive = studentStatusDetail.Where(p => p.StudentId == Convert.ToInt32(item.Value)).OrderByDescending(p => p.StatusChangeDate).ThenByDescending(p => p.StudentStatusDetailId).FirstOrDefault().StudentStatu.StudentStatus;
                StudentDDL.Add(item);
            }

            return Json(new
            {
                status = "success",
                data = StudentDDL
            });
        }

        public ActionResult StudentInfobyAdmnNo(string AdmnNo)
        {
            var session = _ICatalogMasterService.GetCurrentSession();
            var students = _IStudentService.GetAllStudents(ref count);
            var student = students.Where(s => (s.AdmnNo != null && s.AdmnNo == AdmnNo)).FirstOrDefault();
            if (student == null)
                student = students.Where(s => (s.RegNo != null && s.RegNo == AdmnNo)).FirstOrDefault();

            if (student != null)
            {
                var StudentClass = student.StudentClasses.Where(s => s.SessionId == session.SessionId).FirstOrDefault();
                if (StudentClass != null)
                {
                    return Json(new
                    {
                        status = "success",
                        StudentVal = student.StudentId,
                        ClassVal = StudentClass.ClassId,
                    });
                }
                else
                {
                    return Json(new
                    {
                        status = "failed",
                        msg = "Student class not found"
                    });
                }
            }
            else
            {
                return Json(new
                {
                    status = "failed",
                    msg = "Invalid Reg./Admn. No."
                });
            }
        }

        #endregion


        #endregion


    }
}
